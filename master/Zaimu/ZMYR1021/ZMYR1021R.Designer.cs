﻿namespace jp.co.fsi.zm.zmyr1021
{
    /// <summary>
    /// ZMYR1021R の概要の説明です。
    /// </summary>
    partial class ZMYR1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZMYR1021R));
			this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.groupId = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore003 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore004 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore005 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore006 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore002 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.group01_H = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTitle08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line235 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line236 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group01_F = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtScore007 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore009 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore010 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore012 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore008 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore013 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore015 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore016 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore017 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore018 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore014 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore019 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore021 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore022 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore023 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore024 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore020 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore025 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore027 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore028 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore029 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore030 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore026 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore031 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore033 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore034 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore035 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore036 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore032 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore037 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore039 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore040 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore041 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore042 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore038 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore043 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore045 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore046 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore047 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore048 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore044 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore049 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore051 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore052 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore053 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore054 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore050 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line227 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line228 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line229 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line230 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line231 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line232 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line233 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line234 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.group02_H = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group02_F = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore161 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore055 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore057 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore058 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore059 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore060 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore056 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore061 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore063 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore064 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore065 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore066 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore062 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore067 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore069 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore070 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore071 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore072 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore068 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore073 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore075 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore076 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore077 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore078 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore074 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore079 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore081 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore082 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore083 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore084 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore080 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore085 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore087 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore088 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore089 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore090 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore086 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore091 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore093 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore094 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore095 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore096 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore092 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore097 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore099 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore101 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore098 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore103 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore105 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore106 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore107 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore108 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore104 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore109 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore113 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore164 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore115 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore120 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore116 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore123 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore125 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore126 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore122 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore127 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore129 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore130 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore131 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore132 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore128 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore133 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore135 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore136 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore137 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore138 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore134 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore139 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore141 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore142 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore143 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore144 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore140 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore145 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore147 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore148 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore149 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore150 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore146 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore151 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore153 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore154 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore155 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore156 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore152 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore157 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore159 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore160 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore162 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore158 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore163 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line203 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line204 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line205 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line206 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line207 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line208 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line209 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line210 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line211 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line212 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line213 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line214 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line215 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line216 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line217 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line218 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line219 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line220 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line221 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line222 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line223 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line224 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line225 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line226 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group03_H = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.textBox193 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox194 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox195 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox196 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox197 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox198 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox199 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.pageBreak1 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
			this.line95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group03_F = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtScore229 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore225 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore165 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore167 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore168 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore169 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore170 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore166 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore171 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore173 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore174 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore175 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore176 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore172 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore177 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore179 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore180 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore181 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore182 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore178 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore183 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore185 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore186 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore187 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore184 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore189 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore191 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore192 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore193 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore194 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore190 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore195 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore197 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore198 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore199 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore200 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore196 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore201 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore203 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore204 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore205 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore206 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore202 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore207 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore209 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore210 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore211 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore212 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore208 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore213 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore215 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore216 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore217 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore218 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore214 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore219 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore220 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore222 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore223 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore224 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore221 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtScore227 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore228 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore226 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line187 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line188 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line189 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line190 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line191 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line192 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line193 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line194 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line195 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line196 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line197 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line198 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line199 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line200 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line201 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line202 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group04_H = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox132 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox133 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group04_F = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtScore294 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore230 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore232 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore233 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore234 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore235 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore231 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore236 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore238 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore239 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore240 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore241 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore237 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore242 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore244 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore245 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore246 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore247 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore243 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore248 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore250 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore251 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore252 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore253 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore249 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore254 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore256 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore257 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore258 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore259 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore255 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore260 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore262 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore263 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore264 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore265 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore261 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore266 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore268 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore269 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore270 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore271 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore267 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore272 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore274 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore275 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore276 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore277 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore273 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore278 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore280 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore281 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore282 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore283 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore279 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore284 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore285 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore287 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore288 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore289 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore286 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtScore290 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore292 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore293 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore291 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line160 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line171 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line172 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line173 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line175 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line176 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line177 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line178 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line179 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line180 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line181 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line182 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line183 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line184 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line185 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line186 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group05_H = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.textBox495 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox494 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox496 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox497 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox498 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox499 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox500 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.pageBreak2 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
			this.line93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group05_F = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtScore295 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore297 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore298 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore299 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore300 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore296 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore301 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore303 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore304 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore305 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore306 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore302 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore307 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore309 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore310 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore311 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore312 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore308 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore313 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore315 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore318 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore314 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore319 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore321 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore322 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore323 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore324 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore320 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore325 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore327 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore328 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore329 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore330 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore326 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore331 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore333 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore334 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore335 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore336 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore332 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore337 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore339 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore340 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore341 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore342 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore338 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore343 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore345 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore346 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore347 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore348 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore344 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore349 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line156 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line157 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line158 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line159 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line161 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line162 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line163 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line164 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line165 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line166 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line167 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line168 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line169 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line170 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group06_H = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.textBox501 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox502 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox503 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox504 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox505 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox506 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox507 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line154 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group06_F = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.textBox382 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox383 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore350 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore352 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore353 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore354 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore355 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore351 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore356 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore358 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore359 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore360 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore361 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore357 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore362 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore364 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore365 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore366 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore367 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore363 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore368 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore370 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore371 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore372 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore373 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore369 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore374 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore376 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore377 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore378 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore379 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore375 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore380 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore382 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore383 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore384 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore385 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore381 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore386 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore388 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore389 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore390 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore391 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore387 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore392 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore394 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore395 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore396 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore397 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore393 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore398 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore400 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore401 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore402 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore403 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore399 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore404 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore405 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore407 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore408 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore409 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore459 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore406 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore410 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore412 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore413 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore414 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore415 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore411 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore416 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore418 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore419 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore420 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore421 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore417 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore422 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore424 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore425 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore426 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore427 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore423 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore428 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore430 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore431 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore432 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore433 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore429 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore434 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore436 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore437 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore438 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore439 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore435 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore440 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore442 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore443 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore444 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore445 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore441 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore446 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore448 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore449 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore450 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore451 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore447 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore452 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore454 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore455 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore456 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore457 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore453 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore458 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line130 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line131 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line132 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line133 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line134 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line135 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line136 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line137 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line138 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line139 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line140 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line141 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line142 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line143 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line144 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line145 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line146 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line147 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line148 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line149 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line150 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line151 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line152 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line153 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group07_H = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.textBox563 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox564 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox565 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox566 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox567 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox568 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox569 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line77 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line78 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line79 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.pageBreak3 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
			this.line91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line155 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group07_F = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtScore460 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore462 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore463 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore464 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore465 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore461 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore466 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore468 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore469 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore470 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore471 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore467 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore472 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore474 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore475 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore476 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore477 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore473 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore478 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore480 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore481 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore482 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore483 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore479 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore484 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore486 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore487 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore488 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore489 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore485 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore490 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore492 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore493 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore494 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore495 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore491 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore496 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore498 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore499 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore500 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore501 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore497 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore502 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore504 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore505 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore506 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore507 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore503 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore508 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore510 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore511 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore512 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore513 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore509 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore514 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line116 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line117 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line118 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line119 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line120 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line121 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line122 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line123 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line124 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line125 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line126 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line127 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line128 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line129 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line174 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line237 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line238 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line239 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group08_H = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.textBox625 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox626 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox627 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox628 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox629 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox630 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox631 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line83 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line84 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line85 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line86 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line87 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line88 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.group08_F = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.textBox632 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore575 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore515 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore517 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore518 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore519 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore520 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore516 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore521 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore523 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore524 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore525 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore526 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore522 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore527 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore529 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore530 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore531 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore532 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore528 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore533 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore535 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore536 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore537 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore538 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore534 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore539 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore541 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore542 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore543 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore544 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore540 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore545 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore547 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore548 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore549 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore550 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore546 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore551 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore553 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore554 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore555 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox610 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore552 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox612 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox613 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox614 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore556 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore557 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox617 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore558 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore560 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore561 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore562 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore563 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore559 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore564 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line82 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtScore573 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore574 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore576 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore577 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore578 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore579 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore565 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore566 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore567 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore568 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore569 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore570 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore571 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtScore572 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line81 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line80 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line101 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line103 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line104 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line105 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line106 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line107 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line108 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line109 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line110 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line111 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line112 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line113 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line114 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line115 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.groupId)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore001)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore003)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore004)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore005)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore006)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore002)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle07)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle08)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle09)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore007)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore009)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore010)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore011)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore012)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore008)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore013)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore015)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore016)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore017)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore018)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore014)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore019)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore021)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore022)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore023)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore024)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore020)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore025)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore027)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore028)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore029)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore030)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore026)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore031)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore033)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore034)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore035)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore036)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore032)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore037)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore039)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore040)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore041)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore042)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore038)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore043)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore045)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore046)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore047)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore048)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore044)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore049)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore051)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore052)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore053)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore054)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore050)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore161)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore055)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore057)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore058)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore059)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore060)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore056)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore061)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore063)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore064)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore065)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore066)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore062)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore067)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore069)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore070)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore071)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore072)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore068)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore073)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore075)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore076)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore077)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore078)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore074)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore079)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore081)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore082)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore083)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore084)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore080)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore085)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore087)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore088)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore089)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore090)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore086)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore091)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore093)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore094)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore095)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore096)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore092)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore097)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore099)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore098)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore105)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore164)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore138)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore134)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore139)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore141)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore142)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore143)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore144)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore140)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore145)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore147)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore148)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore149)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore150)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore146)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore151)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore153)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore154)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore155)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore156)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore152)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore157)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore159)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore160)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore162)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore158)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore163)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox193)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox194)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox195)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox196)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox197)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox198)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox199)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore229)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore225)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore165)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore167)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore168)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore169)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore170)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore166)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore171)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore173)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore174)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore172)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore179)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore180)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore181)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore182)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore183)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore185)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore186)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore187)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore188)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore184)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore189)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore191)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore192)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore193)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore194)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore190)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore195)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore197)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore198)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore199)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore200)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore196)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore201)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore203)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore204)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore205)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore206)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore202)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore207)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore209)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore210)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore211)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore212)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore208)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore213)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore215)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore216)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore217)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore218)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore214)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore219)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore220)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore222)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore223)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore224)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore221)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore227)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore228)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore226)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore294)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore230)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore232)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore233)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore234)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore235)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore231)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore236)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore238)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore239)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore240)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore241)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore237)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore242)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore244)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore245)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore246)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore247)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore243)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore248)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore250)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore251)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore252)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore253)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore249)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore254)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore256)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore257)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore258)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore259)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore255)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore260)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore262)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore263)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore264)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore265)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore261)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore266)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore268)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore269)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore270)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore271)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore267)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore272)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore274)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore275)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore276)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore277)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore273)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore278)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore280)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore281)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore282)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore283)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore279)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore284)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore285)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore287)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore288)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore289)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore286)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore290)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore292)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore293)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore291)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox495)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox494)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox496)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox497)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox498)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox499)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox500)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore295)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore297)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore298)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore299)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore300)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore296)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore301)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore303)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore304)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore305)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore306)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore302)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore307)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore309)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore310)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore311)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore312)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore308)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore313)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore315)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore316)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore317)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore318)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore314)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore319)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore321)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore322)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore323)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore324)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore320)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore325)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore327)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore328)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore329)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore330)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore326)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore331)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore333)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore334)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore335)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore336)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore332)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore337)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore339)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore340)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore341)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore342)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore338)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore343)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore345)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore346)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore347)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore348)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore344)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore349)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox501)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox502)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox503)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox504)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox505)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox506)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox507)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox382)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox383)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore350)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore352)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore353)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore354)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore355)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore351)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore356)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore358)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore359)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore360)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore361)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore357)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore362)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore364)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore365)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore366)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore367)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore363)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore368)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore370)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore371)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore372)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore373)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore369)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore374)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore376)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore377)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore378)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore379)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore375)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore380)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore382)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore383)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore384)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore385)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore381)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore386)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore388)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore389)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore390)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore391)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore387)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore392)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore394)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore395)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore396)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore397)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore393)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore398)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore400)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore401)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore402)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore403)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore399)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore404)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore405)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore407)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore408)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore409)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore459)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore406)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore410)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore412)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore413)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore414)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore415)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore411)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore416)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore418)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore419)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore420)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore421)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore417)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore422)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore424)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore425)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore426)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore427)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore423)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore428)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore430)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore431)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore432)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore433)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore429)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore434)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore436)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore437)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore438)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore439)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore435)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore440)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore442)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore443)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore444)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore445)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore441)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore446)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore448)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore449)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore450)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore451)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore447)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore452)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore454)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore455)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore456)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore457)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore453)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore458)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox563)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox564)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox565)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox566)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox567)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox568)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox569)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore460)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore462)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore463)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore464)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore465)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore461)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore466)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore468)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore469)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore470)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore471)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore467)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore472)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore474)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore475)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore476)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore477)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore473)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore478)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore480)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore481)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore482)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore483)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore479)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore484)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore486)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore487)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore488)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore489)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore485)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore490)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore492)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore493)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore494)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore495)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore491)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore496)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore498)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore499)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore500)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore501)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore497)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore502)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore504)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore505)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore506)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore507)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore503)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore508)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore510)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore511)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore512)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore513)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore509)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore514)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox625)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox626)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox627)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox628)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox629)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox630)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox631)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox632)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore575)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore515)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore517)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore518)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore519)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore520)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore516)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore521)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore523)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore524)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore525)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore526)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore522)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore527)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore529)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore530)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore531)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore532)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore528)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore533)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore535)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore536)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore537)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore538)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore534)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore539)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore541)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore542)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore543)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore544)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore540)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore545)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore547)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore548)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore549)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore550)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore546)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore551)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore553)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore554)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore555)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox610)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore552)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox612)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox613)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox614)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore556)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore557)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox617)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore558)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore560)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore561)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore562)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore563)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore559)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore564)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore573)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore574)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore576)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore577)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore578)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore579)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore565)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore566)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore567)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore568)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore569)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore570)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore571)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore572)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.Height = 0F;
			this.pageHeader.Name = "pageHeader";
			// 
			// detail
			// 
			this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.groupId});
			this.detail.Height = 0.25F;
			this.detail.Name = "detail";
			this.detail.Visible = false;
			// 
			// groupId
			// 
			this.groupId.DataField = "ITEM01";
			this.groupId.Height = 0.2393701F;
			this.groupId.Left = 0F;
			this.groupId.MultiLine = false;
			this.groupId.Name = "groupId";
			this.groupId.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.groupId.Text = "ITEM01";
			this.groupId.Top = 0F;
			this.groupId.Width = 1.239764F;
			// 
			// txtScore001
			// 
			this.txtScore001.DataField = "ITEM02";
			this.txtScore001.Height = 0.2393701F;
			this.txtScore001.Left = 0.06259843F;
			this.txtScore001.MultiLine = false;
			this.txtScore001.Name = "txtScore001";
			this.txtScore001.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: justify; " +
    "vertical-align: middle";
			this.txtScore001.Text = "02";
			this.txtScore001.Top = 0F;
			this.txtScore001.Width = 1.091339F;
			// 
			// txtScore003
			// 
			this.txtScore003.DataField = "ITEM04";
			this.txtScore003.Height = 0.2393701F;
			this.txtScore003.Left = 2.34567F;
			this.txtScore003.MultiLine = false;
			this.txtScore003.Name = "txtScore003";
			this.txtScore003.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore003.Text = "004";
			this.txtScore003.Top = 0F;
			this.txtScore003.Width = 1.088188F;
			// 
			// txtScore004
			// 
			this.txtScore004.DataField = "ITEM05";
			this.txtScore004.Height = 0.2393701F;
			this.txtScore004.Left = 3.48937F;
			this.txtScore004.MultiLine = false;
			this.txtScore004.Name = "txtScore004";
			this.txtScore004.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore004.Text = "005";
			this.txtScore004.Top = 0F;
			this.txtScore004.Width = 1.129528F;
			// 
			// txtScore005
			// 
			this.txtScore005.DataField = "ITEM06";
			this.txtScore005.Height = 0.2393701F;
			this.txtScore005.Left = 4.66063F;
			this.txtScore005.MultiLine = false;
			this.txtScore005.Name = "txtScore005";
			this.txtScore005.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore005.Text = "006";
			this.txtScore005.Top = 0F;
			this.txtScore005.Width = 1.180315F;
			// 
			// txtScore006
			// 
			this.txtScore006.DataField = "ITEM07";
			this.txtScore006.Height = 0.2393701F;
			this.txtScore006.Left = 5.882678F;
			this.txtScore006.MultiLine = false;
			this.txtScore006.Name = "txtScore006";
			this.txtScore006.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore006.Text = "007";
			this.txtScore006.Top = 0F;
			this.txtScore006.Width = 1.161417F;
			// 
			// txtScore002
			// 
			this.txtScore002.DataField = "ITEM03";
			this.txtScore002.Height = 0.2393701F;
			this.txtScore002.Left = 1.153937F;
			this.txtScore002.MultiLine = false;
			this.txtScore002.Name = "txtScore002";
			this.txtScore002.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore002.Text = "003";
			this.txtScore002.Top = 0F;
			this.txtScore002.Width = 1.143701F;
			// 
			// line3
			// 
			this.line3.Height = 0F;
			this.line3.Left = 0.007874016F;
			this.line3.LineWeight = 1F;
			this.line3.Name = "line3";
			this.line3.Top = 2.154331F;
			this.line3.Width = 7.07874F;
			this.line3.X1 = 7.086614F;
			this.line3.X2 = 0.007874016F;
			this.line3.Y1 = 2.154331F;
			this.line3.Y2 = 2.154331F;
			// 
			// pageFooter
			// 
			this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtPageCount,
            this.lblPage01,
            this.lblPage02});
			this.pageFooter.Height = 0.2083333F;
			this.pageFooter.Name = "pageFooter";
			// 
			// txtPageCount
			// 
			this.txtPageCount.Height = 0.2070866F;
			this.txtPageCount.Left = 3.433858F;
			this.txtPageCount.MultiLine = false;
			this.txtPageCount.Name = "txtPageCount";
			this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "";
			this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPageCount.Text = "999";
			this.txtPageCount.Top = 0F;
			this.txtPageCount.Width = 0.2433071F;
			// 
			// lblPage01
			// 
			this.lblPage01.Height = 0.2070866F;
			this.lblPage01.HyperLink = null;
			this.lblPage01.Left = 3.309449F;
			this.lblPage01.Name = "lblPage01";
			this.lblPage01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
			this.lblPage01.Text = "-";
			this.lblPage01.Top = 7.450581E-09F;
			this.lblPage01.Width = 0.09645677F;
			// 
			// lblPage02
			// 
			this.lblPage02.Height = 0.2070866F;
			this.lblPage02.HyperLink = null;
			this.lblPage02.Left = 3.708268F;
			this.lblPage02.Name = "lblPage02";
			this.lblPage02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
			this.lblPage02.Text = "-";
			this.lblPage02.Top = 7.450581E-09F;
			this.lblPage02.Width = 0.1173232F;
			// 
			// group01_H
			// 
			this.group01_H.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox15,
            this.lblTitle02,
            this.lblTitle03,
            this.lblTitle04,
            this.lblTitle05,
            this.lblTitle06,
            this.lblTitle07,
            this.line6,
            this.line7,
            this.lblTitle08,
            this.lblTitle09,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line97,
            this.line235,
            this.line236});
			this.group01_H.DataField = "ITEM01";
			this.group01_H.Height = 0.6822835F;
			this.group01_H.Name = "group01_H";
			this.group01_H.Format += new System.EventHandler(this.group01_Format);
			// 
			// textBox15
			// 
			this.textBox15.Height = 0.4433071F;
			this.textBox15.Left = 0.007874016F;
			this.textBox15.MultiLine = false;
			this.textBox15.Name = "textBox15";
			this.textBox15.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: justify; vertical-align: middle";
			this.textBox15.Text = null;
			this.textBox15.Top = 0.2389764F;
			this.textBox15.Width = 7.073623F;
			// 
			// lblTitle02
			// 
			this.lblTitle02.Height = 0.2389764F;
			this.lblTitle02.Left = 0.02716536F;
			this.lblTitle02.MultiLine = false;
			this.lblTitle02.Name = "lblTitle02";
			this.lblTitle02.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: justify; ve" +
    "rtical-align: middle";
			this.lblTitle02.Text = "（１）預け金";
			this.lblTitle02.Top = 0F;
			this.lblTitle02.Width = 6.622835F;
			// 
			// lblTitle03
			// 
			this.lblTitle03.Height = 0.4429134F;
			this.lblTitle03.Left = 0.02755904F;
			this.lblTitle03.MultiLine = false;
			this.lblTitle03.Name = "lblTitle03";
			this.lblTitle03.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; te" +
    "xt-justify: auto; vertical-align: middle";
			this.lblTitle03.Text = "種     類";
			this.lblTitle03.Top = 0.2393701F;
			this.lblTitle03.Width = 1.126378F;
			// 
			// lblTitle04
			// 
			this.lblTitle04.Height = 0.4255905F;
			this.lblTitle04.Left = 1.153937F;
			this.lblTitle04.MultiLine = false;
			this.lblTitle04.Name = "lblTitle04";
			this.lblTitle04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.lblTitle04.Text = "前年度末残高";
			this.lblTitle04.Top = 0.2393701F;
			this.lblTitle04.Width = 1.191733F;
			// 
			// lblTitle05
			// 
			this.lblTitle05.Height = 0.4259843F;
			this.lblTitle05.Left = 2.34567F;
			this.lblTitle05.MultiLine = false;
			this.lblTitle05.Name = "lblTitle05";
			this.lblTitle05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.lblTitle05.Text = "本年度預け額";
			this.lblTitle05.Top = 0.2389764F;
			this.lblTitle05.Width = 1.143701F;
			// 
			// lblTitle06
			// 
			this.lblTitle06.Height = 0.4255905F;
			this.lblTitle06.Left = 3.48937F;
			this.lblTitle06.MultiLine = false;
			this.lblTitle06.Name = "lblTitle06";
			this.lblTitle06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.lblTitle06.Text = "本年度引出額";
			this.lblTitle06.Top = 0.2393701F;
			this.lblTitle06.Width = 1.17126F;
			// 
			// lblTitle07
			// 
			this.lblTitle07.Height = 0.3015748F;
			this.lblTitle07.Left = 4.668504F;
			this.lblTitle07.MultiLine = false;
			this.lblTitle07.Name = "lblTitle07";
			this.lblTitle07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.lblTitle07.Text = "本 年 度 末";
			this.lblTitle07.Top = 0.2389764F;
			this.lblTitle07.Width = 2.425984F;
			// 
			// line6
			// 
			this.line6.Height = 0F;
			this.line6.Left = 0.007874016F;
			this.line6.LineWeight = 1F;
			this.line6.Name = "line6";
			this.line6.Top = 0.2389764F;
			this.line6.Width = 7.07874F;
			this.line6.X1 = 7.086614F;
			this.line6.X2 = 0.007874016F;
			this.line6.Y1 = 0.2389764F;
			this.line6.Y2 = 0.2389764F;
			// 
			// line7
			// 
			this.line7.Height = 0F;
			this.line7.Left = 0.007874016F;
			this.line7.LineWeight = 1F;
			this.line7.Name = "line7";
			this.line7.Top = 0.6822835F;
			this.line7.Width = 7.07874F;
			this.line7.X1 = 7.086614F;
			this.line7.X2 = 0.007874016F;
			this.line7.Y1 = 0.6822835F;
			this.line7.Y2 = 0.6822835F;
			// 
			// lblTitle08
			// 
			this.lblTitle08.Height = 0.1870079F;
			this.lblTitle08.Left = 4.66063F;
			this.lblTitle08.MultiLine = false;
			this.lblTitle08.Name = "lblTitle08";
			this.lblTitle08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.lblTitle08.Text = "残     高";
			this.lblTitle08.Top = 0.4779528F;
			this.lblTitle08.Width = 1.222047F;
			// 
			// lblTitle09
			// 
			this.lblTitle09.Height = 0.1870079F;
			this.lblTitle09.Left = 5.882678F;
			this.lblTitle09.MultiLine = false;
			this.lblTitle09.Name = "lblTitle09";
			this.lblTitle09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.lblTitle09.Text = "うち系統外残";
			this.lblTitle09.Top = 0.4779528F;
			this.lblTitle09.Width = 1.207874F;
			// 
			// line8
			// 
			this.line8.Height = 0.4433071F;
			this.line8.Left = 4.66063F;
			this.line8.LineWeight = 1F;
			this.line8.Name = "line8";
			this.line8.Top = 0.2389764F;
			this.line8.Width = 0F;
			this.line8.X1 = 4.66063F;
			this.line8.X2 = 4.66063F;
			this.line8.Y1 = 0.2389764F;
			this.line8.Y2 = 0.6822835F;
			// 
			// line9
			// 
			this.line9.Height = 0.4429134F;
			this.line9.Left = 3.489371F;
			this.line9.LineWeight = 1F;
			this.line9.Name = "line9";
			this.line9.Top = 0.2393701F;
			this.line9.Width = 0F;
			this.line9.X1 = 3.489371F;
			this.line9.X2 = 3.489371F;
			this.line9.Y1 = 0.2393701F;
			this.line9.Y2 = 0.6822835F;
			// 
			// line10
			// 
			this.line10.Height = 0.4433071F;
			this.line10.Left = 2.34567F;
			this.line10.LineWeight = 1F;
			this.line10.Name = "line10";
			this.line10.Top = 0.2389764F;
			this.line10.Width = 0F;
			this.line10.X1 = 2.34567F;
			this.line10.X2 = 2.34567F;
			this.line10.Y1 = 0.2389764F;
			this.line10.Y2 = 0.6822835F;
			// 
			// line11
			// 
			this.line11.Height = 0.4433068F;
			this.line11.Left = 1.153937F;
			this.line11.LineWeight = 1F;
			this.line11.Name = "line11";
			this.line11.Top = 0.2389764F;
			this.line11.Width = 0F;
			this.line11.X1 = 1.153937F;
			this.line11.X2 = 1.153937F;
			this.line11.Y1 = 0.2389764F;
			this.line11.Y2 = 0.6822832F;
			// 
			// line12
			// 
			this.line12.Height = 0.4433071F;
			this.line12.Left = 7.086615F;
			this.line12.LineWeight = 1F;
			this.line12.Name = "line12";
			this.line12.Top = 0.2389764F;
			this.line12.Width = 0F;
			this.line12.X1 = 7.086615F;
			this.line12.X2 = 7.086615F;
			this.line12.Y1 = 0.2389764F;
			this.line12.Y2 = 0.6822835F;
			// 
			// line97
			// 
			this.line97.Height = 0.443307F;
			this.line97.Left = 0.007874016F;
			this.line97.LineWeight = 1F;
			this.line97.Name = "line97";
			this.line97.Top = 0.2389764F;
			this.line97.Width = 0F;
			this.line97.X1 = 0.007874016F;
			this.line97.X2 = 0.007874016F;
			this.line97.Y1 = 0.2389764F;
			this.line97.Y2 = 0.6822834F;
			// 
			// line235
			// 
			this.line235.Height = 0.2043307F;
			this.line235.Left = 5.886614F;
			this.line235.LineWeight = 1F;
			this.line235.Name = "line235";
			this.line235.Top = 0.4779528F;
			this.line235.Width = 0F;
			this.line235.X1 = 5.886614F;
			this.line235.X2 = 5.886614F;
			this.line235.Y1 = 0.4779528F;
			this.line235.Y2 = 0.6822835F;
			// 
			// line236
			// 
			this.line236.Height = 1.192093E-07F;
			this.line236.Left = 4.660826F;
			this.line236.LineWeight = 1F;
			this.line236.Name = "line236";
			this.line236.Top = 0.4779527F;
			this.line236.Width = 2.424607F;
			this.line236.X1 = 7.085433F;
			this.line236.X2 = 4.660826F;
			this.line236.Y1 = 0.4779528F;
			this.line236.Y2 = 0.4779527F;
			// 
			// group01_F
			// 
			this.group01_F.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtScore001,
            this.txtScore003,
            this.txtScore004,
            this.txtScore005,
            this.txtScore006,
            this.txtScore002,
            this.txtScore007,
            this.txtScore009,
            this.txtScore010,
            this.txtScore011,
            this.txtScore012,
            this.txtScore008,
            this.txtScore013,
            this.txtScore015,
            this.txtScore016,
            this.txtScore017,
            this.txtScore018,
            this.txtScore014,
            this.txtScore019,
            this.txtScore021,
            this.txtScore022,
            this.txtScore023,
            this.txtScore024,
            this.txtScore020,
            this.txtScore025,
            this.txtScore027,
            this.txtScore028,
            this.txtScore029,
            this.txtScore030,
            this.txtScore026,
            this.txtScore031,
            this.txtScore033,
            this.txtScore034,
            this.txtScore035,
            this.txtScore036,
            this.txtScore032,
            this.txtScore037,
            this.txtScore039,
            this.txtScore040,
            this.txtScore041,
            this.txtScore042,
            this.txtScore038,
            this.txtScore043,
            this.txtScore045,
            this.txtScore046,
            this.txtScore047,
            this.txtScore048,
            this.txtScore044,
            this.txtScore049,
            this.txtScore051,
            this.txtScore052,
            this.txtScore053,
            this.txtScore054,
            this.txtScore050,
            this.line3,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line227,
            this.line228,
            this.line229,
            this.line230,
            this.line231,
            this.line232,
            this.line233,
            this.line234});
			this.group01_F.Height = 2.604216F;
			this.group01_F.Name = "group01_F";
			// 
			// txtScore007
			// 
			this.txtScore007.DataField = "ITEM08";
			this.txtScore007.Height = 0.2393701F;
			this.txtScore007.Left = 0.06259843F;
			this.txtScore007.MultiLine = false;
			this.txtScore007.Name = "txtScore007";
			this.txtScore007.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: justify; " +
    "vertical-align: middle";
			this.txtScore007.Text = "08";
			this.txtScore007.Top = 0.2393701F;
			this.txtScore007.Width = 1.091339F;
			// 
			// txtScore009
			// 
			this.txtScore009.DataField = "ITEM10";
			this.txtScore009.Height = 0.2393701F;
			this.txtScore009.Left = 2.34567F;
			this.txtScore009.MultiLine = false;
			this.txtScore009.Name = "txtScore009";
			this.txtScore009.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore009.Text = "010";
			this.txtScore009.Top = 0.2393701F;
			this.txtScore009.Width = 1.088188F;
			// 
			// txtScore010
			// 
			this.txtScore010.DataField = "ITEM11";
			this.txtScore010.Height = 0.2393701F;
			this.txtScore010.Left = 3.48937F;
			this.txtScore010.MultiLine = false;
			this.txtScore010.Name = "txtScore010";
			this.txtScore010.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore010.Text = "011";
			this.txtScore010.Top = 0.2393701F;
			this.txtScore010.Width = 1.129528F;
			// 
			// txtScore011
			// 
			this.txtScore011.DataField = "ITEM12";
			this.txtScore011.Height = 0.2393701F;
			this.txtScore011.Left = 4.660631F;
			this.txtScore011.MultiLine = false;
			this.txtScore011.Name = "txtScore011";
			this.txtScore011.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore011.Text = "012";
			this.txtScore011.Top = 0.2393701F;
			this.txtScore011.Width = 1.180315F;
			// 
			// txtScore012
			// 
			this.txtScore012.DataField = "ITEM13";
			this.txtScore012.Height = 0.2393701F;
			this.txtScore012.Left = 5.882678F;
			this.txtScore012.MultiLine = false;
			this.txtScore012.Name = "txtScore012";
			this.txtScore012.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore012.Text = "013";
			this.txtScore012.Top = 0.2393701F;
			this.txtScore012.Width = 1.161416F;
			// 
			// txtScore008
			// 
			this.txtScore008.DataField = "ITEM09";
			this.txtScore008.Height = 0.2393701F;
			this.txtScore008.Left = 1.153937F;
			this.txtScore008.MultiLine = false;
			this.txtScore008.Name = "txtScore008";
			this.txtScore008.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore008.Text = "009";
			this.txtScore008.Top = 0.2393701F;
			this.txtScore008.Width = 1.143701F;
			// 
			// txtScore013
			// 
			this.txtScore013.DataField = "ITEM14";
			this.txtScore013.Height = 0.2393701F;
			this.txtScore013.Left = 0.06259843F;
			this.txtScore013.MultiLine = false;
			this.txtScore013.Name = "txtScore013";
			this.txtScore013.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: justify; " +
    "vertical-align: middle";
			this.txtScore013.Text = "14";
			this.txtScore013.Top = 0.4787402F;
			this.txtScore013.Width = 1.091339F;
			// 
			// txtScore015
			// 
			this.txtScore015.DataField = "ITEM16";
			this.txtScore015.Height = 0.2393701F;
			this.txtScore015.Left = 2.34567F;
			this.txtScore015.MultiLine = false;
			this.txtScore015.Name = "txtScore015";
			this.txtScore015.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore015.Text = "016";
			this.txtScore015.Top = 0.4787402F;
			this.txtScore015.Width = 1.088188F;
			// 
			// txtScore016
			// 
			this.txtScore016.DataField = "ITEM17";
			this.txtScore016.Height = 0.2393701F;
			this.txtScore016.Left = 3.48937F;
			this.txtScore016.MultiLine = false;
			this.txtScore016.Name = "txtScore016";
			this.txtScore016.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore016.Text = "017";
			this.txtScore016.Top = 0.4787402F;
			this.txtScore016.Width = 1.129528F;
			// 
			// txtScore017
			// 
			this.txtScore017.DataField = "ITEM18";
			this.txtScore017.Height = 0.2393701F;
			this.txtScore017.Left = 4.660631F;
			this.txtScore017.MultiLine = false;
			this.txtScore017.Name = "txtScore017";
			this.txtScore017.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore017.Text = "018";
			this.txtScore017.Top = 0.4787402F;
			this.txtScore017.Width = 1.180315F;
			// 
			// txtScore018
			// 
			this.txtScore018.DataField = "ITEM19";
			this.txtScore018.Height = 0.2393701F;
			this.txtScore018.Left = 5.882678F;
			this.txtScore018.MultiLine = false;
			this.txtScore018.Name = "txtScore018";
			this.txtScore018.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore018.Text = "019";
			this.txtScore018.Top = 0.4787402F;
			this.txtScore018.Width = 1.161416F;
			// 
			// txtScore014
			// 
			this.txtScore014.DataField = "ITEM15";
			this.txtScore014.Height = 0.2393701F;
			this.txtScore014.Left = 1.153937F;
			this.txtScore014.MultiLine = false;
			this.txtScore014.Name = "txtScore014";
			this.txtScore014.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore014.Text = "015";
			this.txtScore014.Top = 0.4787402F;
			this.txtScore014.Width = 1.143701F;
			// 
			// txtScore019
			// 
			this.txtScore019.DataField = "ITEM20";
			this.txtScore019.Height = 0.2393701F;
			this.txtScore019.Left = 0.06259843F;
			this.txtScore019.MultiLine = false;
			this.txtScore019.Name = "txtScore019";
			this.txtScore019.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: justify; " +
    "vertical-align: middle";
			this.txtScore019.Text = "20";
			this.txtScore019.Top = 0.7181102F;
			this.txtScore019.Width = 1.091339F;
			// 
			// txtScore021
			// 
			this.txtScore021.DataField = "ITEM22";
			this.txtScore021.Height = 0.2393701F;
			this.txtScore021.Left = 2.34567F;
			this.txtScore021.MultiLine = false;
			this.txtScore021.Name = "txtScore021";
			this.txtScore021.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore021.Text = "022";
			this.txtScore021.Top = 0.7181102F;
			this.txtScore021.Width = 1.088188F;
			// 
			// txtScore022
			// 
			this.txtScore022.DataField = "ITEM23";
			this.txtScore022.Height = 0.2393701F;
			this.txtScore022.Left = 3.48937F;
			this.txtScore022.MultiLine = false;
			this.txtScore022.Name = "txtScore022";
			this.txtScore022.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore022.Text = "023";
			this.txtScore022.Top = 0.7181102F;
			this.txtScore022.Width = 1.129528F;
			// 
			// txtScore023
			// 
			this.txtScore023.DataField = "ITEM24";
			this.txtScore023.Height = 0.2393701F;
			this.txtScore023.Left = 4.660631F;
			this.txtScore023.MultiLine = false;
			this.txtScore023.Name = "txtScore023";
			this.txtScore023.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore023.Text = "024";
			this.txtScore023.Top = 0.7181102F;
			this.txtScore023.Width = 1.180315F;
			// 
			// txtScore024
			// 
			this.txtScore024.DataField = "ITEM25";
			this.txtScore024.Height = 0.2393701F;
			this.txtScore024.Left = 5.882678F;
			this.txtScore024.MultiLine = false;
			this.txtScore024.Name = "txtScore024";
			this.txtScore024.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore024.Text = "025";
			this.txtScore024.Top = 0.7181102F;
			this.txtScore024.Width = 1.161416F;
			// 
			// txtScore020
			// 
			this.txtScore020.DataField = "ITEM21";
			this.txtScore020.Height = 0.2393701F;
			this.txtScore020.Left = 1.153937F;
			this.txtScore020.MultiLine = false;
			this.txtScore020.Name = "txtScore020";
			this.txtScore020.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore020.Text = "021";
			this.txtScore020.Top = 0.7181102F;
			this.txtScore020.Width = 1.143701F;
			// 
			// txtScore025
			// 
			this.txtScore025.DataField = "ITEM26";
			this.txtScore025.Height = 0.2393701F;
			this.txtScore025.Left = 0.06259843F;
			this.txtScore025.MultiLine = false;
			this.txtScore025.Name = "txtScore025";
			this.txtScore025.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: justify; " +
    "vertical-align: middle";
			this.txtScore025.Text = "26";
			this.txtScore025.Top = 0.9574804F;
			this.txtScore025.Width = 1.091339F;
			// 
			// txtScore027
			// 
			this.txtScore027.DataField = "ITEM28";
			this.txtScore027.Height = 0.2393701F;
			this.txtScore027.Left = 2.34567F;
			this.txtScore027.MultiLine = false;
			this.txtScore027.Name = "txtScore027";
			this.txtScore027.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore027.Text = "028";
			this.txtScore027.Top = 0.9574804F;
			this.txtScore027.Width = 1.088188F;
			// 
			// txtScore028
			// 
			this.txtScore028.DataField = "ITEM29";
			this.txtScore028.Height = 0.2393701F;
			this.txtScore028.Left = 3.48937F;
			this.txtScore028.MultiLine = false;
			this.txtScore028.Name = "txtScore028";
			this.txtScore028.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore028.Text = "029";
			this.txtScore028.Top = 0.9574804F;
			this.txtScore028.Width = 1.129528F;
			// 
			// txtScore029
			// 
			this.txtScore029.DataField = "ITEM30";
			this.txtScore029.Height = 0.2393701F;
			this.txtScore029.Left = 4.660631F;
			this.txtScore029.MultiLine = false;
			this.txtScore029.Name = "txtScore029";
			this.txtScore029.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore029.Text = "030";
			this.txtScore029.Top = 0.9574804F;
			this.txtScore029.Width = 1.180315F;
			// 
			// txtScore030
			// 
			this.txtScore030.DataField = "ITEM31";
			this.txtScore030.Height = 0.2393701F;
			this.txtScore030.Left = 5.882678F;
			this.txtScore030.MultiLine = false;
			this.txtScore030.Name = "txtScore030";
			this.txtScore030.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore030.Text = "031";
			this.txtScore030.Top = 0.9574804F;
			this.txtScore030.Width = 1.161416F;
			// 
			// txtScore026
			// 
			this.txtScore026.DataField = "ITEM27";
			this.txtScore026.Height = 0.2393701F;
			this.txtScore026.Left = 1.153937F;
			this.txtScore026.MultiLine = false;
			this.txtScore026.Name = "txtScore026";
			this.txtScore026.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore026.Text = "027";
			this.txtScore026.Top = 0.9574804F;
			this.txtScore026.Width = 1.143701F;
			// 
			// txtScore031
			// 
			this.txtScore031.DataField = "ITEM32";
			this.txtScore031.Height = 0.2393701F;
			this.txtScore031.Left = 0.06259843F;
			this.txtScore031.MultiLine = false;
			this.txtScore031.Name = "txtScore031";
			this.txtScore031.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: justify; " +
    "vertical-align: middle";
			this.txtScore031.Text = "32";
			this.txtScore031.Top = 1.19685F;
			this.txtScore031.Width = 1.091339F;
			// 
			// txtScore033
			// 
			this.txtScore033.DataField = "ITEM34";
			this.txtScore033.Height = 0.2393701F;
			this.txtScore033.Left = 2.34567F;
			this.txtScore033.MultiLine = false;
			this.txtScore033.Name = "txtScore033";
			this.txtScore033.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore033.Text = "034";
			this.txtScore033.Top = 1.19685F;
			this.txtScore033.Width = 1.088188F;
			// 
			// txtScore034
			// 
			this.txtScore034.DataField = "ITEM35";
			this.txtScore034.Height = 0.2393701F;
			this.txtScore034.Left = 3.48937F;
			this.txtScore034.MultiLine = false;
			this.txtScore034.Name = "txtScore034";
			this.txtScore034.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore034.Text = "035";
			this.txtScore034.Top = 1.19685F;
			this.txtScore034.Width = 1.129528F;
			// 
			// txtScore035
			// 
			this.txtScore035.DataField = "ITEM36";
			this.txtScore035.Height = 0.2393701F;
			this.txtScore035.Left = 4.660631F;
			this.txtScore035.MultiLine = false;
			this.txtScore035.Name = "txtScore035";
			this.txtScore035.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore035.Text = "036";
			this.txtScore035.Top = 1.19685F;
			this.txtScore035.Width = 1.180315F;
			// 
			// txtScore036
			// 
			this.txtScore036.DataField = "ITEM37";
			this.txtScore036.Height = 0.2393701F;
			this.txtScore036.Left = 5.882678F;
			this.txtScore036.MultiLine = false;
			this.txtScore036.Name = "txtScore036";
			this.txtScore036.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore036.Text = "037";
			this.txtScore036.Top = 1.19685F;
			this.txtScore036.Width = 1.161416F;
			// 
			// txtScore032
			// 
			this.txtScore032.DataField = "ITEM33";
			this.txtScore032.Height = 0.2393701F;
			this.txtScore032.Left = 1.153937F;
			this.txtScore032.MultiLine = false;
			this.txtScore032.Name = "txtScore032";
			this.txtScore032.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore032.Text = "033";
			this.txtScore032.Top = 1.19685F;
			this.txtScore032.Width = 1.143701F;
			// 
			// txtScore037
			// 
			this.txtScore037.DataField = "ITEM38";
			this.txtScore037.Height = 0.2393701F;
			this.txtScore037.Left = 0.06259843F;
			this.txtScore037.MultiLine = false;
			this.txtScore037.Name = "txtScore037";
			this.txtScore037.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: justify; " +
    "vertical-align: middle";
			this.txtScore037.Text = "38";
			this.txtScore037.Top = 1.436221F;
			this.txtScore037.Width = 1.091339F;
			// 
			// txtScore039
			// 
			this.txtScore039.DataField = "ITEM40";
			this.txtScore039.Height = 0.2393701F;
			this.txtScore039.Left = 2.34567F;
			this.txtScore039.MultiLine = false;
			this.txtScore039.Name = "txtScore039";
			this.txtScore039.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore039.Text = "040";
			this.txtScore039.Top = 1.436221F;
			this.txtScore039.Width = 1.088188F;
			// 
			// txtScore040
			// 
			this.txtScore040.DataField = "ITEM41";
			this.txtScore040.Height = 0.2393701F;
			this.txtScore040.Left = 3.48937F;
			this.txtScore040.MultiLine = false;
			this.txtScore040.Name = "txtScore040";
			this.txtScore040.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore040.Text = "041";
			this.txtScore040.Top = 1.436221F;
			this.txtScore040.Width = 1.129528F;
			// 
			// txtScore041
			// 
			this.txtScore041.DataField = "ITEM42";
			this.txtScore041.Height = 0.2393701F;
			this.txtScore041.Left = 4.660631F;
			this.txtScore041.MultiLine = false;
			this.txtScore041.Name = "txtScore041";
			this.txtScore041.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore041.Text = "042";
			this.txtScore041.Top = 1.436221F;
			this.txtScore041.Width = 1.180315F;
			// 
			// txtScore042
			// 
			this.txtScore042.DataField = "ITEM43";
			this.txtScore042.Height = 0.2393701F;
			this.txtScore042.Left = 5.882678F;
			this.txtScore042.MultiLine = false;
			this.txtScore042.Name = "txtScore042";
			this.txtScore042.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore042.Text = "043";
			this.txtScore042.Top = 1.436221F;
			this.txtScore042.Width = 1.161416F;
			// 
			// txtScore038
			// 
			this.txtScore038.DataField = "ITEM39";
			this.txtScore038.Height = 0.2393701F;
			this.txtScore038.Left = 1.153937F;
			this.txtScore038.MultiLine = false;
			this.txtScore038.Name = "txtScore038";
			this.txtScore038.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore038.Text = "039";
			this.txtScore038.Top = 1.436221F;
			this.txtScore038.Width = 1.143701F;
			// 
			// txtScore043
			// 
			this.txtScore043.DataField = "ITEM44";
			this.txtScore043.Height = 0.2393701F;
			this.txtScore043.Left = 0.06259843F;
			this.txtScore043.MultiLine = false;
			this.txtScore043.Name = "txtScore043";
			this.txtScore043.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: justify; " +
    "vertical-align: middle";
			this.txtScore043.Text = "44";
			this.txtScore043.Top = 1.675591F;
			this.txtScore043.Width = 1.091339F;
			// 
			// txtScore045
			// 
			this.txtScore045.DataField = "ITEM46";
			this.txtScore045.Height = 0.2393701F;
			this.txtScore045.Left = 2.34567F;
			this.txtScore045.MultiLine = false;
			this.txtScore045.Name = "txtScore045";
			this.txtScore045.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore045.Text = "046";
			this.txtScore045.Top = 1.675591F;
			this.txtScore045.Width = 1.088188F;
			// 
			// txtScore046
			// 
			this.txtScore046.DataField = "ITEM47";
			this.txtScore046.Height = 0.2393701F;
			this.txtScore046.Left = 3.48937F;
			this.txtScore046.MultiLine = false;
			this.txtScore046.Name = "txtScore046";
			this.txtScore046.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore046.Text = "047";
			this.txtScore046.Top = 1.675591F;
			this.txtScore046.Width = 1.129528F;
			// 
			// txtScore047
			// 
			this.txtScore047.DataField = "ITEM48";
			this.txtScore047.Height = 0.2393701F;
			this.txtScore047.Left = 4.660631F;
			this.txtScore047.MultiLine = false;
			this.txtScore047.Name = "txtScore047";
			this.txtScore047.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore047.Text = "048";
			this.txtScore047.Top = 1.675591F;
			this.txtScore047.Width = 1.180315F;
			// 
			// txtScore048
			// 
			this.txtScore048.DataField = "ITEM49";
			this.txtScore048.Height = 0.2393701F;
			this.txtScore048.Left = 5.882678F;
			this.txtScore048.MultiLine = false;
			this.txtScore048.Name = "txtScore048";
			this.txtScore048.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore048.Text = "049";
			this.txtScore048.Top = 1.675591F;
			this.txtScore048.Width = 1.161416F;
			// 
			// txtScore044
			// 
			this.txtScore044.DataField = "ITEM45";
			this.txtScore044.Height = 0.2393701F;
			this.txtScore044.Left = 1.153937F;
			this.txtScore044.MultiLine = false;
			this.txtScore044.Name = "txtScore044";
			this.txtScore044.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore044.Text = "045";
			this.txtScore044.Top = 1.675591F;
			this.txtScore044.Width = 1.143701F;
			// 
			// txtScore049
			// 
			this.txtScore049.DataField = "ITEM50";
			this.txtScore049.Height = 0.2393701F;
			this.txtScore049.Left = 0.06259843F;
			this.txtScore049.MultiLine = false;
			this.txtScore049.Name = "txtScore049";
			this.txtScore049.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: justify; " +
    "vertical-align: middle";
			this.txtScore049.Text = "50";
			this.txtScore049.Top = 1.914961F;
			this.txtScore049.Width = 1.091339F;
			// 
			// txtScore051
			// 
			this.txtScore051.DataField = "ITEM52";
			this.txtScore051.Height = 0.2393701F;
			this.txtScore051.Left = 2.34567F;
			this.txtScore051.MultiLine = false;
			this.txtScore051.Name = "txtScore051";
			this.txtScore051.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore051.Text = "052";
			this.txtScore051.Top = 1.914961F;
			this.txtScore051.Width = 1.088188F;
			// 
			// txtScore052
			// 
			this.txtScore052.DataField = "ITEM53";
			this.txtScore052.Height = 0.2393701F;
			this.txtScore052.Left = 3.48937F;
			this.txtScore052.MultiLine = false;
			this.txtScore052.Name = "txtScore052";
			this.txtScore052.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore052.Text = "053";
			this.txtScore052.Top = 1.916929F;
			this.txtScore052.Width = 1.129528F;
			// 
			// txtScore053
			// 
			this.txtScore053.DataField = "ITEM54";
			this.txtScore053.Height = 0.2393701F;
			this.txtScore053.Left = 4.660631F;
			this.txtScore053.MultiLine = false;
			this.txtScore053.Name = "txtScore053";
			this.txtScore053.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore053.Text = "054";
			this.txtScore053.Top = 1.914961F;
			this.txtScore053.Width = 1.180315F;
			// 
			// txtScore054
			// 
			this.txtScore054.DataField = "ITEM55";
			this.txtScore054.Height = 0.2393701F;
			this.txtScore054.Left = 5.882678F;
			this.txtScore054.MultiLine = false;
			this.txtScore054.Name = "txtScore054";
			this.txtScore054.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore054.Text = "055";
			this.txtScore054.Top = 1.914961F;
			this.txtScore054.Width = 1.161416F;
			// 
			// txtScore050
			// 
			this.txtScore050.DataField = "ITEM51";
			this.txtScore050.Height = 0.2393701F;
			this.txtScore050.Left = 1.153937F;
			this.txtScore050.MultiLine = false;
			this.txtScore050.Name = "txtScore050";
			this.txtScore050.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore050.Text = "051";
			this.txtScore050.Top = 1.914961F;
			this.txtScore050.Width = 1.143701F;
			// 
			// line18
			// 
			this.line18.Height = 2.154331F;
			this.line18.Left = 1.153937F;
			this.line18.LineWeight = 1F;
			this.line18.Name = "line18";
			this.line18.Top = 0F;
			this.line18.Width = 0F;
			this.line18.X1 = 1.153937F;
			this.line18.X2 = 1.153937F;
			this.line18.Y1 = 2.154331F;
			this.line18.Y2 = 0F;
			// 
			// line19
			// 
			this.line19.Height = 2.154331F;
			this.line19.Left = 2.34567F;
			this.line19.LineWeight = 1F;
			this.line19.Name = "line19";
			this.line19.Top = 0F;
			this.line19.Width = 0F;
			this.line19.X1 = 2.34567F;
			this.line19.X2 = 2.34567F;
			this.line19.Y1 = 2.154331F;
			this.line19.Y2 = 0F;
			// 
			// line20
			// 
			this.line20.Height = 2.154331F;
			this.line20.Left = 3.48937F;
			this.line20.LineWeight = 1F;
			this.line20.Name = "line20";
			this.line20.Top = 0F;
			this.line20.Width = 0F;
			this.line20.X1 = 3.48937F;
			this.line20.X2 = 3.48937F;
			this.line20.Y1 = 2.154331F;
			this.line20.Y2 = 0F;
			// 
			// line21
			// 
			this.line21.Height = 2.154331F;
			this.line21.Left = 4.66063F;
			this.line21.LineWeight = 1F;
			this.line21.Name = "line21";
			this.line21.Top = 0F;
			this.line21.Width = 0F;
			this.line21.X1 = 4.66063F;
			this.line21.X2 = 4.66063F;
			this.line21.Y1 = 2.154331F;
			this.line21.Y2 = 0F;
			// 
			// line22
			// 
			this.line22.Height = 2.154331F;
			this.line22.Left = 5.886615F;
			this.line22.LineWeight = 1F;
			this.line22.Name = "line22";
			this.line22.Top = 0F;
			this.line22.Width = 0F;
			this.line22.X1 = 5.886615F;
			this.line22.X2 = 5.886615F;
			this.line22.Y1 = 2.154331F;
			this.line22.Y2 = 0F;
			// 
			// line23
			// 
			this.line23.Height = 2.154331F;
			this.line23.Left = 7.086614F;
			this.line23.LineWeight = 1F;
			this.line23.Name = "line23";
			this.line23.Top = 0F;
			this.line23.Width = 0F;
			this.line23.X1 = 7.086614F;
			this.line23.X2 = 7.086614F;
			this.line23.Y1 = 2.154331F;
			this.line23.Y2 = 0F;
			// 
			// line24
			// 
			this.line24.Height = 2.154331F;
			this.line24.Left = 0.007874016F;
			this.line24.LineWeight = 1F;
			this.line24.Name = "line24";
			this.line24.Top = 0F;
			this.line24.Width = 0F;
			this.line24.X1 = 0.007874016F;
			this.line24.X2 = 0.007874016F;
			this.line24.Y1 = 2.154331F;
			this.line24.Y2 = 0F;
			// 
			// line227
			// 
			this.line227.Height = 0F;
			this.line227.Left = 0.007874016F;
			this.line227.LineWeight = 1F;
			this.line227.Name = "line227";
			this.line227.Top = 1.916929F;
			this.line227.Width = 7.07874F;
			this.line227.X1 = 7.086614F;
			this.line227.X2 = 0.007874016F;
			this.line227.Y1 = 1.916929F;
			this.line227.Y2 = 1.916929F;
			// 
			// line228
			// 
			this.line228.Height = 0F;
			this.line228.Left = 0.007874016F;
			this.line228.LineWeight = 1F;
			this.line228.Name = "line228";
			this.line228.Top = 1.675591F;
			this.line228.Width = 7.07874F;
			this.line228.X1 = 7.086614F;
			this.line228.X2 = 0.007874016F;
			this.line228.Y1 = 1.675591F;
			this.line228.Y2 = 1.675591F;
			// 
			// line229
			// 
			this.line229.Height = 0F;
			this.line229.Left = 0.007874016F;
			this.line229.LineWeight = 1F;
			this.line229.Name = "line229";
			this.line229.Top = 1.436221F;
			this.line229.Width = 7.07874F;
			this.line229.X1 = 7.086614F;
			this.line229.X2 = 0.007874016F;
			this.line229.Y1 = 1.436221F;
			this.line229.Y2 = 1.436221F;
			// 
			// line230
			// 
			this.line230.Height = 0F;
			this.line230.Left = 0.007874016F;
			this.line230.LineWeight = 1F;
			this.line230.Name = "line230";
			this.line230.Top = 0.2393701F;
			this.line230.Width = 7.07874F;
			this.line230.X1 = 7.086614F;
			this.line230.X2 = 0.007874016F;
			this.line230.Y1 = 0.2393701F;
			this.line230.Y2 = 0.2393701F;
			// 
			// line231
			// 
			this.line231.Height = 0F;
			this.line231.Left = 0.007874016F;
			this.line231.LineWeight = 1F;
			this.line231.Name = "line231";
			this.line231.Top = 0.4787402F;
			this.line231.Width = 7.07874F;
			this.line231.X1 = 7.086614F;
			this.line231.X2 = 0.007874016F;
			this.line231.Y1 = 0.4787402F;
			this.line231.Y2 = 0.4787402F;
			// 
			// line232
			// 
			this.line232.Height = 0F;
			this.line232.Left = 0.007874016F;
			this.line232.LineWeight = 1F;
			this.line232.Name = "line232";
			this.line232.Top = 0.7181103F;
			this.line232.Width = 7.07874F;
			this.line232.X1 = 7.086614F;
			this.line232.X2 = 0.007874016F;
			this.line232.Y1 = 0.7181103F;
			this.line232.Y2 = 0.7181103F;
			// 
			// line233
			// 
			this.line233.Height = 0F;
			this.line233.Left = 0.007874016F;
			this.line233.LineWeight = 1F;
			this.line233.Name = "line233";
			this.line233.Top = 0.9574804F;
			this.line233.Width = 7.07874F;
			this.line233.X1 = 7.086614F;
			this.line233.X2 = 0.007874016F;
			this.line233.Y1 = 0.9574804F;
			this.line233.Y2 = 0.9574804F;
			// 
			// line234
			// 
			this.line234.Height = 0F;
			this.line234.Left = 0.007874016F;
			this.line234.LineWeight = 1F;
			this.line234.Name = "line234";
			this.line234.Top = 1.19685F;
			this.line234.Width = 7.07874F;
			this.line234.X1 = 7.086614F;
			this.line234.X2 = 0.007874016F;
			this.line234.Y1 = 1.19685F;
			this.line234.Y2 = 1.19685F;
			// 
			// reportHeader1
			// 
			this.reportHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle01});
			this.reportHeader1.Height = 0.7476375F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// lblTitle01
			// 
			this.lblTitle01.Height = 0.2700787F;
			this.lblTitle01.Left = 2.345473F;
			this.lblTitle01.MultiLine = false;
			this.lblTitle01.Name = "lblTitle01";
			this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: middle";
			this.lblTitle01.Text = "貸借対照表付属明細書";
			this.lblTitle01.Top = 0.2387793F;
			this.lblTitle01.Width = 2.816339F;
			// 
			// reportFooter1
			// 
			this.reportFooter1.Height = 0F;
			this.reportFooter1.Name = "reportFooter1";
			// 
			// group02_H
			// 
			this.group02_H.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox14,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.line1,
            this.line2,
            this.line4,
            this.line5,
            this.line13,
            this.line14,
            this.line15,
            this.line96});
			this.group02_H.DataField = "ITEM01";
			this.group02_H.Height = 0.6822835F;
			this.group02_H.Name = "group02_H";
			this.group02_H.Format += new System.EventHandler(this.group02_Format);
			// 
			// textBox14
			// 
			this.textBox14.Height = 0.4433071F;
			this.textBox14.Left = 0.007874016F;
			this.textBox14.MultiLine = false;
			this.textBox14.Name = "textBox14";
			this.textBox14.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: justify; vertical-align: middle";
			this.textBox14.Text = null;
			this.textBox14.Top = 0.2389764F;
			this.textBox14.Width = 7.086614F;
			// 
			// textBox1
			// 
			this.textBox1.Height = 0.2389764F;
			this.textBox1.Left = 0.02716536F;
			this.textBox1.MultiLine = false;
			this.textBox1.Name = "textBox1";
			this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: justify; ve" +
    "rtical-align: middle";
			this.textBox1.Text = "（２）受取手形、経済事業未収金、経済事業雑資産";
			this.textBox1.Top = 0F;
			this.textBox1.Width = 4.237402F;
			// 
			// textBox2
			// 
			this.textBox2.Height = 0.4429134F;
			this.textBox2.Left = 0F;
			this.textBox2.MultiLine = false;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; te" +
    "xt-justify: auto; vertical-align: middle";
			this.textBox2.Text = "科目又は部門";
			this.textBox2.Top = 0.2393701F;
			this.textBox2.Width = 2.365355F;
			// 
			// textBox3
			// 
			this.textBox3.Height = 0.4255905F;
			this.textBox3.Left = 2.366929F;
			this.textBox3.MultiLine = false;
			this.textBox3.Name = "textBox3";
			this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox3.Text = "前年度末残高";
			this.textBox3.Top = 0.2393701F;
			this.textBox3.Width = 1.169685F;
			// 
			// textBox4
			// 
			this.textBox4.Height = 0.4259843F;
			this.textBox4.Left = 3.538583F;
			this.textBox4.MultiLine = false;
			this.textBox4.Name = "textBox4";
			this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox4.Text = "本年度増加額";
			this.textBox4.Top = 0.238189F;
			this.textBox4.Width = 1.180315F;
			// 
			// textBox5
			// 
			this.textBox5.Height = 0.4255905F;
			this.textBox5.Left = 4.720473F;
			this.textBox5.MultiLine = false;
			this.textBox5.Name = "textBox5";
			this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox5.Text = "本年度減少額";
			this.textBox5.Top = 0.238189F;
			this.textBox5.Width = 1.199606F;
			// 
			// textBox6
			// 
			this.textBox6.Height = 0.4259844F;
			this.textBox6.Left = 5.918898F;
			this.textBox6.MultiLine = false;
			this.textBox6.Name = "textBox6";
			this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox6.Text = "本年度末残高";
			this.textBox6.Top = 0.2515748F;
			this.textBox6.Width = 1.166536F;
			// 
			// line1
			// 
			this.line1.Height = 0F;
			this.line1.Left = 0.007874016F;
			this.line1.LineWeight = 1F;
			this.line1.Name = "line1";
			this.line1.Top = 0.2389764F;
			this.line1.Width = 7.07874F;
			this.line1.X1 = 7.086614F;
			this.line1.X2 = 0.007874016F;
			this.line1.Y1 = 0.2389764F;
			this.line1.Y2 = 0.2389764F;
			// 
			// line2
			// 
			this.line2.Height = 0F;
			this.line2.Left = 0.007874016F;
			this.line2.LineWeight = 1F;
			this.line2.Name = "line2";
			this.line2.Top = 0.6822835F;
			this.line2.Width = 7.07874F;
			this.line2.X1 = 7.086614F;
			this.line2.X2 = 0.007874016F;
			this.line2.Y1 = 0.6822835F;
			this.line2.Y2 = 0.6822835F;
			// 
			// line4
			// 
			this.line4.Height = 0.4429134F;
			this.line4.Left = 5.920079F;
			this.line4.LineWeight = 1F;
			this.line4.Name = "line4";
			this.line4.Top = 0.2393701F;
			this.line4.Width = 0F;
			this.line4.X1 = 5.920079F;
			this.line4.X2 = 5.920079F;
			this.line4.Y1 = 0.2393701F;
			this.line4.Y2 = 0.6822835F;
			// 
			// line5
			// 
			this.line5.Height = 0.4440945F;
			this.line5.Left = 4.718898F;
			this.line5.LineWeight = 1F;
			this.line5.Name = "line5";
			this.line5.Top = 0.238189F;
			this.line5.Width = 0F;
			this.line5.X1 = 4.718898F;
			this.line5.X2 = 4.718898F;
			this.line5.Y1 = 0.238189F;
			this.line5.Y2 = 0.6822835F;
			// 
			// line13
			// 
			this.line13.Height = 0.4440945F;
			this.line13.Left = 3.536614F;
			this.line13.LineWeight = 1F;
			this.line13.Name = "line13";
			this.line13.Top = 0.238189F;
			this.line13.Width = 0F;
			this.line13.X1 = 3.536614F;
			this.line13.X2 = 3.536614F;
			this.line13.Y1 = 0.238189F;
			this.line13.Y2 = 0.6822835F;
			// 
			// line14
			// 
			this.line14.Height = 0.4429134F;
			this.line14.Left = 2.368898F;
			this.line14.LineWeight = 1F;
			this.line14.Name = "line14";
			this.line14.Top = 0.2393701F;
			this.line14.Width = 0F;
			this.line14.X1 = 2.368898F;
			this.line14.X2 = 2.368898F;
			this.line14.Y1 = 0.2393701F;
			this.line14.Y2 = 0.6822835F;
			// 
			// line15
			// 
			this.line15.Height = 0.4433071F;
			this.line15.Left = 7.086614F;
			this.line15.LineWeight = 1F;
			this.line15.Name = "line15";
			this.line15.Top = 0.2389764F;
			this.line15.Width = 0F;
			this.line15.X1 = 7.086614F;
			this.line15.X2 = 7.086614F;
			this.line15.Y1 = 0.2389764F;
			this.line15.Y2 = 0.6822835F;
			// 
			// line96
			// 
			this.line96.Height = 0.4440944F;
			this.line96.Left = 0.007874016F;
			this.line96.LineWeight = 1F;
			this.line96.Name = "line96";
			this.line96.Top = 0.238189F;
			this.line96.Width = 0F;
			this.line96.X1 = 0.007874016F;
			this.line96.X2 = 0.007874016F;
			this.line96.Y1 = 0.238189F;
			this.line96.Y2 = 0.6822834F;
			// 
			// group02_F
			// 
			this.group02_F.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox24,
            this.txtScore161,
            this.textBox77,
            this.txtScore055,
            this.txtScore057,
            this.txtScore058,
            this.txtScore059,
            this.txtScore060,
            this.txtScore056,
            this.txtScore061,
            this.txtScore063,
            this.txtScore064,
            this.txtScore065,
            this.txtScore066,
            this.txtScore062,
            this.txtScore067,
            this.txtScore069,
            this.txtScore070,
            this.txtScore071,
            this.txtScore072,
            this.txtScore068,
            this.txtScore073,
            this.txtScore075,
            this.txtScore076,
            this.txtScore077,
            this.txtScore078,
            this.txtScore074,
            this.txtScore079,
            this.txtScore081,
            this.txtScore082,
            this.txtScore083,
            this.txtScore084,
            this.txtScore080,
            this.txtScore085,
            this.txtScore087,
            this.txtScore088,
            this.txtScore089,
            this.txtScore090,
            this.txtScore086,
            this.txtScore091,
            this.txtScore093,
            this.txtScore094,
            this.txtScore095,
            this.txtScore096,
            this.txtScore092,
            this.txtScore097,
            this.txtScore099,
            this.txtScore100,
            this.txtScore101,
            this.txtScore102,
            this.txtScore098,
            this.txtScore103,
            this.txtScore105,
            this.txtScore106,
            this.txtScore107,
            this.txtScore108,
            this.txtScore104,
            this.txtScore109,
            this.txtScore110,
            this.txtScore112,
            this.txtScore113,
            this.txtScore114,
            this.txtScore164,
            this.txtScore111,
            this.txtScore115,
            this.txtScore117,
            this.txtScore118,
            this.txtScore119,
            this.txtScore120,
            this.txtScore116,
            this.txtScore121,
            this.txtScore123,
            this.txtScore124,
            this.txtScore125,
            this.txtScore126,
            this.txtScore122,
            this.txtScore127,
            this.txtScore129,
            this.txtScore130,
            this.txtScore131,
            this.txtScore132,
            this.txtScore128,
            this.txtScore133,
            this.txtScore135,
            this.txtScore136,
            this.txtScore137,
            this.txtScore138,
            this.txtScore134,
            this.txtScore139,
            this.txtScore141,
            this.txtScore142,
            this.txtScore143,
            this.txtScore144,
            this.txtScore140,
            this.txtScore145,
            this.txtScore147,
            this.txtScore148,
            this.txtScore149,
            this.txtScore150,
            this.txtScore146,
            this.txtScore151,
            this.txtScore153,
            this.txtScore154,
            this.txtScore155,
            this.txtScore156,
            this.txtScore152,
            this.txtScore157,
            this.txtScore159,
            this.txtScore160,
            this.txtScore162,
            this.txtScore158,
            this.txtScore163,
            this.line16,
            this.line17,
            this.line25,
            this.line26,
            this.line27,
            this.line203,
            this.line204,
            this.line205,
            this.line206,
            this.line207,
            this.line208,
            this.line209,
            this.line210,
            this.line211,
            this.line212,
            this.line213,
            this.line214,
            this.line215,
            this.line216,
            this.line217,
            this.line218,
            this.line219,
            this.line220,
            this.line221,
            this.line222,
            this.line223,
            this.line224,
            this.line225,
            this.line226});
			this.group02_F.Height = 5.370309F;
			this.group02_F.KeepTogether = true;
			this.group02_F.Name = "group02_F";
			// 
			// textBox24
			// 
			this.textBox24.Height = 2.633071F;
			this.textBox24.Left = 0F;
			this.textBox24.Name = "textBox24";
			this.textBox24.Style = resources.GetString("textBox24.Style");
			this.textBox24.Text = "経\r\n済\r\n事\r\n業\r\n未\r\n収\r\n金";
			this.textBox24.Top = 0F;
			this.textBox24.Width = 0.5767717F;
			// 
			// txtScore161
			// 
			this.txtScore161.DataField = "ITEM108";
			this.txtScore161.Height = 0.2393701F;
			this.txtScore161.Left = 2.368897F;
			this.txtScore161.MultiLine = false;
			this.txtScore161.Name = "txtScore161";
			this.txtScore161.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore161.Text = "108";
			this.txtScore161.Top = 5.026772F;
			this.txtScore161.Width = 1.124016F;
			// 
			// textBox77
			// 
			this.textBox77.Height = 2.633071F;
			this.textBox77.Left = 0.02755906F;
			this.textBox77.Name = "textBox77";
			this.textBox77.Style = "font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: normal; text-align: center; " +
    "vertical-align: middle; ddo-font-vertical: true";
			this.textBox77.Text = "経\r\n済\r\n事\r\n業\r\n雑\r\n資\r\n産";
			this.textBox77.Top = 2.633071F;
			this.textBox77.Width = 0.5492127F;
			// 
			// txtScore055
			// 
			this.txtScore055.DataField = "ITEM02";
			this.txtScore055.Height = 0.2393701F;
			this.txtScore055.Left = 0.6228347F;
			this.txtScore055.MultiLine = false;
			this.txtScore055.Name = "txtScore055";
			this.txtScore055.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore055.Text = "02";
			this.txtScore055.Top = 1.490116E-08F;
			this.txtScore055.Width = 1.743307F;
			// 
			// txtScore057
			// 
			this.txtScore057.DataField = "ITEM04";
			this.txtScore057.Height = 0.2393701F;
			this.txtScore057.Left = 3.537402F;
			this.txtScore057.MultiLine = false;
			this.txtScore057.Name = "txtScore057";
			this.txtScore057.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore057.Text = "004";
			this.txtScore057.Top = 0F;
			this.txtScore057.Width = 1.124016F;
			// 
			// txtScore058
			// 
			this.txtScore058.DataField = "ITEM05";
			this.txtScore058.Height = 0.2393701F;
			this.txtScore058.Left = 4.72126F;
			this.txtScore058.MultiLine = false;
			this.txtScore058.Name = "txtScore058";
			this.txtScore058.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore058.Text = "005";
			this.txtScore058.Top = 0F;
			this.txtScore058.Width = 1.124016F;
			// 
			// txtScore059
			// 
			this.txtScore059.DataField = "ITEM06";
			this.txtScore059.Height = 0.2393701F;
			this.txtScore059.Left = 5.919685F;
			this.txtScore059.MultiLine = false;
			this.txtScore059.Name = "txtScore059";
			this.txtScore059.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore059.Text = "006";
			this.txtScore059.Top = 0F;
			this.txtScore059.Width = 1.124016F;
			// 
			// txtScore060
			// 
			this.txtScore060.DataField = "ITEM07";
			this.txtScore060.Height = 0.2393701F;
			this.txtScore060.Left = 0.622441F;
			this.txtScore060.MultiLine = false;
			this.txtScore060.Name = "txtScore060";
			this.txtScore060.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore060.Text = "07";
			this.txtScore060.Top = 0.2393701F;
			this.txtScore060.Width = 1.7437F;
			// 
			// txtScore056
			// 
			this.txtScore056.DataField = "ITEM03";
			this.txtScore056.Height = 0.2393701F;
			this.txtScore056.Left = 2.365355F;
			this.txtScore056.MultiLine = false;
			this.txtScore056.Name = "txtScore056";
			this.txtScore056.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore056.Text = "003";
			this.txtScore056.Top = 1.490116E-08F;
			this.txtScore056.Width = 1.124016F;
			// 
			// txtScore061
			// 
			this.txtScore061.DataField = "ITEM08";
			this.txtScore061.Height = 0.2393701F;
			this.txtScore061.Left = 2.365355F;
			this.txtScore061.MultiLine = false;
			this.txtScore061.Name = "txtScore061";
			this.txtScore061.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore061.Text = "008";
			this.txtScore061.Top = 0.2393701F;
			this.txtScore061.Width = 1.124016F;
			// 
			// txtScore063
			// 
			this.txtScore063.DataField = "ITEM10";
			this.txtScore063.Height = 0.2393701F;
			this.txtScore063.Left = 4.72126F;
			this.txtScore063.MultiLine = false;
			this.txtScore063.Name = "txtScore063";
			this.txtScore063.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore063.Text = "010";
			this.txtScore063.Top = 0.2393701F;
			this.txtScore063.Width = 1.124016F;
			// 
			// txtScore064
			// 
			this.txtScore064.DataField = "ITEM11";
			this.txtScore064.Height = 0.2393701F;
			this.txtScore064.Left = 5.919685F;
			this.txtScore064.MultiLine = false;
			this.txtScore064.Name = "txtScore064";
			this.txtScore064.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore064.Text = "011";
			this.txtScore064.Top = 0.2393701F;
			this.txtScore064.Width = 1.124016F;
			// 
			// txtScore065
			// 
			this.txtScore065.DataField = "ITEM12";
			this.txtScore065.Height = 0.2393701F;
			this.txtScore065.Left = 0.622441F;
			this.txtScore065.MultiLine = false;
			this.txtScore065.Name = "txtScore065";
			this.txtScore065.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore065.Text = "12";
			this.txtScore065.Top = 0.4787402F;
			this.txtScore065.Width = 1.7437F;
			// 
			// txtScore066
			// 
			this.txtScore066.DataField = "ITEM13";
			this.txtScore066.Height = 0.2393701F;
			this.txtScore066.Left = 2.365355F;
			this.txtScore066.MultiLine = false;
			this.txtScore066.Name = "txtScore066";
			this.txtScore066.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore066.Text = "013";
			this.txtScore066.Top = 0.4787402F;
			this.txtScore066.Width = 1.124016F;
			// 
			// txtScore062
			// 
			this.txtScore062.DataField = "ITEM09";
			this.txtScore062.Height = 0.2393701F;
			this.txtScore062.Left = 3.537402F;
			this.txtScore062.MultiLine = false;
			this.txtScore062.Name = "txtScore062";
			this.txtScore062.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore062.Text = "009";
			this.txtScore062.Top = 0.2393701F;
			this.txtScore062.Width = 1.124016F;
			// 
			// txtScore067
			// 
			this.txtScore067.DataField = "ITEM14";
			this.txtScore067.Height = 0.2393701F;
			this.txtScore067.Left = 3.537402F;
			this.txtScore067.MultiLine = false;
			this.txtScore067.Name = "txtScore067";
			this.txtScore067.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore067.Text = "014";
			this.txtScore067.Top = 0.4787401F;
			this.txtScore067.Width = 1.124016F;
			// 
			// txtScore069
			// 
			this.txtScore069.DataField = "ITEM16";
			this.txtScore069.Height = 0.2393701F;
			this.txtScore069.Left = 5.919685F;
			this.txtScore069.MultiLine = false;
			this.txtScore069.Name = "txtScore069";
			this.txtScore069.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore069.Text = "016";
			this.txtScore069.Top = 0.4787401F;
			this.txtScore069.Width = 1.124016F;
			// 
			// txtScore070
			// 
			this.txtScore070.DataField = "ITEM17";
			this.txtScore070.Height = 0.2393701F;
			this.txtScore070.Left = 0.622441F;
			this.txtScore070.MultiLine = false;
			this.txtScore070.Name = "txtScore070";
			this.txtScore070.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore070.Text = "17";
			this.txtScore070.Top = 0.7181103F;
			this.txtScore070.Width = 1.7437F;
			// 
			// txtScore071
			// 
			this.txtScore071.DataField = "ITEM18";
			this.txtScore071.Height = 0.2393701F;
			this.txtScore071.Left = 2.365355F;
			this.txtScore071.MultiLine = false;
			this.txtScore071.Name = "txtScore071";
			this.txtScore071.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore071.Text = "018";
			this.txtScore071.Top = 0.7181103F;
			this.txtScore071.Width = 1.124016F;
			// 
			// txtScore072
			// 
			this.txtScore072.DataField = "ITEM19";
			this.txtScore072.Height = 0.2393701F;
			this.txtScore072.Left = 3.537402F;
			this.txtScore072.MultiLine = false;
			this.txtScore072.Name = "txtScore072";
			this.txtScore072.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore072.Text = "019";
			this.txtScore072.Top = 0.7181103F;
			this.txtScore072.Width = 1.124016F;
			// 
			// txtScore068
			// 
			this.txtScore068.DataField = "ITEM15";
			this.txtScore068.Height = 0.2393701F;
			this.txtScore068.Left = 4.722048F;
			this.txtScore068.MultiLine = false;
			this.txtScore068.Name = "txtScore068";
			this.txtScore068.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore068.Text = "015";
			this.txtScore068.Top = 0.4787401F;
			this.txtScore068.Width = 1.124016F;
			// 
			// txtScore073
			// 
			this.txtScore073.DataField = "ITEM20";
			this.txtScore073.Height = 0.2393701F;
			this.txtScore073.Left = 4.72126F;
			this.txtScore073.MultiLine = false;
			this.txtScore073.Name = "txtScore073";
			this.txtScore073.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore073.Text = "020";
			this.txtScore073.Top = 0.7181103F;
			this.txtScore073.Width = 1.124016F;
			// 
			// txtScore075
			// 
			this.txtScore075.DataField = "ITEM22";
			this.txtScore075.Height = 0.2393701F;
			this.txtScore075.Left = 0.6228349F;
			this.txtScore075.MultiLine = false;
			this.txtScore075.Name = "txtScore075";
			this.txtScore075.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore075.Text = "22";
			this.txtScore075.Top = 0.9574803F;
			this.txtScore075.Width = 1.743306F;
			// 
			// txtScore076
			// 
			this.txtScore076.DataField = "ITEM23";
			this.txtScore076.Height = 0.2393701F;
			this.txtScore076.Left = 2.365355F;
			this.txtScore076.MultiLine = false;
			this.txtScore076.Name = "txtScore076";
			this.txtScore076.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore076.Text = "023";
			this.txtScore076.Top = 0.9574803F;
			this.txtScore076.Width = 1.124016F;
			// 
			// txtScore077
			// 
			this.txtScore077.DataField = "ITEM24";
			this.txtScore077.Height = 0.2393701F;
			this.txtScore077.Left = 3.537402F;
			this.txtScore077.MultiLine = false;
			this.txtScore077.Name = "txtScore077";
			this.txtScore077.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore077.Text = "024";
			this.txtScore077.Top = 0.9574803F;
			this.txtScore077.Width = 1.124016F;
			// 
			// txtScore078
			// 
			this.txtScore078.DataField = "ITEM25";
			this.txtScore078.Height = 0.2393701F;
			this.txtScore078.Left = 4.72126F;
			this.txtScore078.MultiLine = false;
			this.txtScore078.Name = "txtScore078";
			this.txtScore078.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore078.Text = "025";
			this.txtScore078.Top = 0.9574803F;
			this.txtScore078.Width = 1.124016F;
			// 
			// txtScore074
			// 
			this.txtScore074.DataField = "ITEM21";
			this.txtScore074.Height = 0.2393701F;
			this.txtScore074.Left = 5.919685F;
			this.txtScore074.MultiLine = false;
			this.txtScore074.Name = "txtScore074";
			this.txtScore074.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore074.Text = "021";
			this.txtScore074.Top = 0.7181103F;
			this.txtScore074.Width = 1.124016F;
			// 
			// txtScore079
			// 
			this.txtScore079.DataField = "ITEM26";
			this.txtScore079.Height = 0.2393701F;
			this.txtScore079.Left = 5.919685F;
			this.txtScore079.MultiLine = false;
			this.txtScore079.Name = "txtScore079";
			this.txtScore079.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore079.Text = "026";
			this.txtScore079.Top = 0.9574803F;
			this.txtScore079.Width = 1.124016F;
			// 
			// txtScore081
			// 
			this.txtScore081.DataField = "ITEM28";
			this.txtScore081.Height = 0.2393701F;
			this.txtScore081.Left = 2.365355F;
			this.txtScore081.MultiLine = false;
			this.txtScore081.Name = "txtScore081";
			this.txtScore081.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore081.Text = "028";
			this.txtScore081.Top = 1.19685F;
			this.txtScore081.Width = 1.124016F;
			// 
			// txtScore082
			// 
			this.txtScore082.DataField = "ITEM29";
			this.txtScore082.Height = 0.2393701F;
			this.txtScore082.Left = 3.537402F;
			this.txtScore082.MultiLine = false;
			this.txtScore082.Name = "txtScore082";
			this.txtScore082.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore082.Text = "029";
			this.txtScore082.Top = 1.19685F;
			this.txtScore082.Width = 1.124016F;
			// 
			// txtScore083
			// 
			this.txtScore083.DataField = "ITEM30";
			this.txtScore083.Height = 0.2393701F;
			this.txtScore083.Left = 4.72126F;
			this.txtScore083.MultiLine = false;
			this.txtScore083.Name = "txtScore083";
			this.txtScore083.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore083.Text = "030";
			this.txtScore083.Top = 1.19685F;
			this.txtScore083.Width = 1.124016F;
			// 
			// txtScore084
			// 
			this.txtScore084.DataField = "ITEM31";
			this.txtScore084.Height = 0.2393701F;
			this.txtScore084.Left = 5.919685F;
			this.txtScore084.MultiLine = false;
			this.txtScore084.Name = "txtScore084";
			this.txtScore084.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore084.Text = "031";
			this.txtScore084.Top = 1.19685F;
			this.txtScore084.Width = 1.124016F;
			// 
			// txtScore080
			// 
			this.txtScore080.DataField = "ITEM27";
			this.txtScore080.Height = 0.2393701F;
			this.txtScore080.Left = 0.6224411F;
			this.txtScore080.MultiLine = false;
			this.txtScore080.Name = "txtScore080";
			this.txtScore080.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore080.Text = "27";
			this.txtScore080.Top = 1.19685F;
			this.txtScore080.Width = 1.7437F;
			// 
			// txtScore085
			// 
			this.txtScore085.DataField = "ITEM32";
			this.txtScore085.Height = 0.2393701F;
			this.txtScore085.Left = 0.6224411F;
			this.txtScore085.MultiLine = false;
			this.txtScore085.Name = "txtScore085";
			this.txtScore085.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore085.Text = "32";
			this.txtScore085.Top = 1.436221F;
			this.txtScore085.Width = 1.7437F;
			// 
			// txtScore087
			// 
			this.txtScore087.DataField = "ITEM34";
			this.txtScore087.Height = 0.2393701F;
			this.txtScore087.Left = 3.537796F;
			this.txtScore087.MultiLine = false;
			this.txtScore087.Name = "txtScore087";
			this.txtScore087.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore087.Text = "034";
			this.txtScore087.Top = 1.436221F;
			this.txtScore087.Width = 1.124016F;
			// 
			// txtScore088
			// 
			this.txtScore088.DataField = "ITEM35";
			this.txtScore088.Height = 0.2393701F;
			this.txtScore088.Left = 4.72126F;
			this.txtScore088.MultiLine = false;
			this.txtScore088.Name = "txtScore088";
			this.txtScore088.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore088.Text = "035";
			this.txtScore088.Top = 1.436221F;
			this.txtScore088.Width = 1.124016F;
			// 
			// txtScore089
			// 
			this.txtScore089.DataField = "ITEM36";
			this.txtScore089.Height = 0.2393701F;
			this.txtScore089.Left = 5.919685F;
			this.txtScore089.MultiLine = false;
			this.txtScore089.Name = "txtScore089";
			this.txtScore089.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore089.Text = "036";
			this.txtScore089.Top = 1.436221F;
			this.txtScore089.Width = 1.124016F;
			// 
			// txtScore090
			// 
			this.txtScore090.DataField = "ITEM37";
			this.txtScore090.Height = 0.2393701F;
			this.txtScore090.Left = 0.6228349F;
			this.txtScore090.MultiLine = false;
			this.txtScore090.Name = "txtScore090";
			this.txtScore090.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore090.Text = "37";
			this.txtScore090.Top = 1.675591F;
			this.txtScore090.Width = 1.743306F;
			// 
			// txtScore086
			// 
			this.txtScore086.DataField = "ITEM33";
			this.txtScore086.Height = 0.2393701F;
			this.txtScore086.Left = 2.365355F;
			this.txtScore086.MultiLine = false;
			this.txtScore086.Name = "txtScore086";
			this.txtScore086.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore086.Text = "033";
			this.txtScore086.Top = 1.436221F;
			this.txtScore086.Width = 1.124016F;
			// 
			// txtScore091
			// 
			this.txtScore091.DataField = "ITEM38";
			this.txtScore091.Height = 0.2393701F;
			this.txtScore091.Left = 2.365355F;
			this.txtScore091.MultiLine = false;
			this.txtScore091.Name = "txtScore091";
			this.txtScore091.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore091.Text = "038";
			this.txtScore091.Top = 1.675591F;
			this.txtScore091.Width = 1.124016F;
			// 
			// txtScore093
			// 
			this.txtScore093.DataField = "ITEM40";
			this.txtScore093.Height = 0.2393701F;
			this.txtScore093.Left = 4.721654F;
			this.txtScore093.MultiLine = false;
			this.txtScore093.Name = "txtScore093";
			this.txtScore093.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore093.Text = "040";
			this.txtScore093.Top = 1.675591F;
			this.txtScore093.Width = 1.124016F;
			// 
			// txtScore094
			// 
			this.txtScore094.DataField = "ITEM41";
			this.txtScore094.Height = 0.2393701F;
			this.txtScore094.Left = 5.919685F;
			this.txtScore094.MultiLine = false;
			this.txtScore094.Name = "txtScore094";
			this.txtScore094.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore094.Text = "041";
			this.txtScore094.Top = 1.675591F;
			this.txtScore094.Width = 1.124016F;
			// 
			// txtScore095
			// 
			this.txtScore095.DataField = "ITEM42";
			this.txtScore095.Height = 0.2393701F;
			this.txtScore095.Left = 0.6228349F;
			this.txtScore095.MultiLine = false;
			this.txtScore095.Name = "txtScore095";
			this.txtScore095.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore095.Text = "42";
			this.txtScore095.Top = 1.914961F;
			this.txtScore095.Width = 1.743306F;
			// 
			// txtScore096
			// 
			this.txtScore096.DataField = "ITEM43";
			this.txtScore096.Height = 0.2393701F;
			this.txtScore096.Left = 2.365355F;
			this.txtScore096.MultiLine = false;
			this.txtScore096.Name = "txtScore096";
			this.txtScore096.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore096.Text = "043";
			this.txtScore096.Top = 1.914961F;
			this.txtScore096.Width = 1.124016F;
			// 
			// txtScore092
			// 
			this.txtScore092.DataField = "ITEM39";
			this.txtScore092.Height = 0.2393701F;
			this.txtScore092.Left = 3.537402F;
			this.txtScore092.MultiLine = false;
			this.txtScore092.Name = "txtScore092";
			this.txtScore092.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore092.Text = "039";
			this.txtScore092.Top = 1.675591F;
			this.txtScore092.Width = 1.124016F;
			// 
			// txtScore097
			// 
			this.txtScore097.DataField = "ITEM44";
			this.txtScore097.Height = 0.2393701F;
			this.txtScore097.Left = 3.537796F;
			this.txtScore097.MultiLine = false;
			this.txtScore097.Name = "txtScore097";
			this.txtScore097.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore097.Text = "044";
			this.txtScore097.Top = 1.914961F;
			this.txtScore097.Width = 1.124016F;
			// 
			// txtScore099
			// 
			this.txtScore099.DataField = "ITEM46";
			this.txtScore099.Height = 0.2393701F;
			this.txtScore099.Left = 5.920079F;
			this.txtScore099.MultiLine = false;
			this.txtScore099.Name = "txtScore099";
			this.txtScore099.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore099.Text = "046";
			this.txtScore099.Top = 1.914961F;
			this.txtScore099.Width = 1.124016F;
			// 
			// txtScore100
			// 
			this.txtScore100.DataField = "ITEM47";
			this.txtScore100.Height = 0.2393701F;
			this.txtScore100.Left = 0.622441F;
			this.txtScore100.MultiLine = false;
			this.txtScore100.Name = "txtScore100";
			this.txtScore100.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore100.Text = "47";
			this.txtScore100.Top = 2.154331F;
			this.txtScore100.Width = 1.7437F;
			// 
			// txtScore101
			// 
			this.txtScore101.DataField = "ITEM48";
			this.txtScore101.Height = 0.2393701F;
			this.txtScore101.Left = 2.365355F;
			this.txtScore101.MultiLine = false;
			this.txtScore101.Name = "txtScore101";
			this.txtScore101.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore101.Text = "048";
			this.txtScore101.Top = 2.154331F;
			this.txtScore101.Width = 1.124016F;
			// 
			// txtScore102
			// 
			this.txtScore102.DataField = "ITEM49";
			this.txtScore102.Height = 0.2393701F;
			this.txtScore102.Left = 3.537402F;
			this.txtScore102.MultiLine = false;
			this.txtScore102.Name = "txtScore102";
			this.txtScore102.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore102.Text = "049";
			this.txtScore102.Top = 2.154331F;
			this.txtScore102.Width = 1.124016F;
			// 
			// txtScore098
			// 
			this.txtScore098.DataField = "ITEM45";
			this.txtScore098.Height = 0.2393701F;
			this.txtScore098.Left = 4.72126F;
			this.txtScore098.MultiLine = false;
			this.txtScore098.Name = "txtScore098";
			this.txtScore098.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore098.Text = "045";
			this.txtScore098.Top = 1.914961F;
			this.txtScore098.Width = 1.124016F;
			// 
			// txtScore103
			// 
			this.txtScore103.DataField = "ITEM50";
			this.txtScore103.Height = 0.2393701F;
			this.txtScore103.Left = 4.720473F;
			this.txtScore103.MultiLine = false;
			this.txtScore103.Name = "txtScore103";
			this.txtScore103.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore103.Text = "050";
			this.txtScore103.Top = 2.154331F;
			this.txtScore103.Width = 1.124016F;
			// 
			// txtScore105
			// 
			this.txtScore105.DataField = "ITEM52";
			this.txtScore105.Height = 0.2393701F;
			this.txtScore105.Left = 0.6228349F;
			this.txtScore105.MultiLine = false;
			this.txtScore105.Name = "txtScore105";
			this.txtScore105.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore105.Text = "52";
			this.txtScore105.Top = 2.393701F;
			this.txtScore105.Width = 1.743306F;
			// 
			// txtScore106
			// 
			this.txtScore106.DataField = "ITEM53";
			this.txtScore106.Height = 0.2393701F;
			this.txtScore106.Left = 2.365355F;
			this.txtScore106.MultiLine = false;
			this.txtScore106.Name = "txtScore106";
			this.txtScore106.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore106.Text = "053";
			this.txtScore106.Top = 2.393701F;
			this.txtScore106.Width = 1.124016F;
			// 
			// txtScore107
			// 
			this.txtScore107.DataField = "ITEM54";
			this.txtScore107.Height = 0.2393701F;
			this.txtScore107.Left = 3.536614F;
			this.txtScore107.MultiLine = false;
			this.txtScore107.Name = "txtScore107";
			this.txtScore107.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore107.Text = "054";
			this.txtScore107.Top = 2.393701F;
			this.txtScore107.Width = 1.124016F;
			// 
			// txtScore108
			// 
			this.txtScore108.DataField = "ITEM55";
			this.txtScore108.Height = 0.2393701F;
			this.txtScore108.Left = 4.722047F;
			this.txtScore108.MultiLine = false;
			this.txtScore108.Name = "txtScore108";
			this.txtScore108.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore108.Text = "055";
			this.txtScore108.Top = 2.393701F;
			this.txtScore108.Width = 1.124016F;
			// 
			// txtScore104
			// 
			this.txtScore104.DataField = "ITEM51";
			this.txtScore104.Height = 0.2393701F;
			this.txtScore104.Left = 5.919685F;
			this.txtScore104.MultiLine = false;
			this.txtScore104.Name = "txtScore104";
			this.txtScore104.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore104.Text = "051";
			this.txtScore104.Top = 2.154331F;
			this.txtScore104.Width = 1.124016F;
			// 
			// txtScore109
			// 
			this.txtScore109.DataField = "ITEM56";
			this.txtScore109.Height = 0.2393701F;
			this.txtScore109.Left = 5.919685F;
			this.txtScore109.MultiLine = false;
			this.txtScore109.Name = "txtScore109";
			this.txtScore109.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore109.Text = "056,456,789,000";
			this.txtScore109.Top = 2.393701F;
			this.txtScore109.Width = 1.124016F;
			// 
			// txtScore110
			// 
			this.txtScore110.DataField = "ITEM57";
			this.txtScore110.Height = 0.2393701F;
			this.txtScore110.Left = 0.6275591F;
			this.txtScore110.MultiLine = false;
			this.txtScore110.Name = "txtScore110";
			this.txtScore110.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore110.Text = "57";
			this.txtScore110.Top = 2.633071F;
			this.txtScore110.Width = 1.738582F;
			// 
			// txtScore112
			// 
			this.txtScore112.DataField = "ITEM59";
			this.txtScore112.Height = 0.2393701F;
			this.txtScore112.Left = 3.538977F;
			this.txtScore112.MultiLine = false;
			this.txtScore112.Name = "txtScore112";
			this.txtScore112.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore112.Text = "059";
			this.txtScore112.Top = 2.633071F;
			this.txtScore112.Width = 1.124016F;
			// 
			// txtScore113
			// 
			this.txtScore113.DataField = "ITEM60";
			this.txtScore113.Height = 0.2393701F;
			this.txtScore113.Left = 4.720079F;
			this.txtScore113.MultiLine = false;
			this.txtScore113.Name = "txtScore113";
			this.txtScore113.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore113.Text = "060";
			this.txtScore113.Top = 2.633071F;
			this.txtScore113.Width = 1.124016F;
			// 
			// txtScore114
			// 
			this.txtScore114.DataField = "ITEM61";
			this.txtScore114.Height = 0.2393701F;
			this.txtScore114.Left = 5.919685F;
			this.txtScore114.MultiLine = false;
			this.txtScore114.Name = "txtScore114";
			this.txtScore114.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore114.Text = "061";
			this.txtScore114.Top = 2.633071F;
			this.txtScore114.Width = 1.124016F;
			// 
			// txtScore164
			// 
			this.txtScore164.DataField = "ITEM111";
			this.txtScore164.Height = 0.2393701F;
			this.txtScore164.Left = 5.919686F;
			this.txtScore164.MultiLine = false;
			this.txtScore164.Name = "txtScore164";
			this.txtScore164.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore164.Text = "111,456,789,000";
			this.txtScore164.Top = 5.026772F;
			this.txtScore164.Width = 1.127559F;
			// 
			// txtScore111
			// 
			this.txtScore111.DataField = "ITEM58";
			this.txtScore111.Height = 0.2393701F;
			this.txtScore111.Left = 2.366929F;
			this.txtScore111.MultiLine = false;
			this.txtScore111.Name = "txtScore111";
			this.txtScore111.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore111.Text = "058";
			this.txtScore111.Top = 2.633071F;
			this.txtScore111.Width = 1.124016F;
			// 
			// txtScore115
			// 
			this.txtScore115.DataField = "ITEM62";
			this.txtScore115.Height = 0.2393701F;
			this.txtScore115.Left = 0.6251971F;
			this.txtScore115.MultiLine = false;
			this.txtScore115.Name = "txtScore115";
			this.txtScore115.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore115.Text = "62";
			this.txtScore115.Top = 2.872441F;
			this.txtScore115.Width = 1.737401F;
			// 
			// txtScore117
			// 
			this.txtScore117.DataField = "ITEM64";
			this.txtScore117.Height = 0.2393701F;
			this.txtScore117.Left = 3.538584F;
			this.txtScore117.MultiLine = false;
			this.txtScore117.Name = "txtScore117";
			this.txtScore117.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore117.Text = "064";
			this.txtScore117.Top = 2.872441F;
			this.txtScore117.Width = 1.124016F;
			// 
			// txtScore118
			// 
			this.txtScore118.DataField = "ITEM65";
			this.txtScore118.Height = 0.2393701F;
			this.txtScore118.Left = 4.718899F;
			this.txtScore118.MultiLine = false;
			this.txtScore118.Name = "txtScore118";
			this.txtScore118.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore118.Text = "065";
			this.txtScore118.Top = 2.872441F;
			this.txtScore118.Width = 1.124016F;
			// 
			// txtScore119
			// 
			this.txtScore119.DataField = "ITEM66";
			this.txtScore119.Height = 0.2393701F;
			this.txtScore119.Left = 5.918898F;
			this.txtScore119.MultiLine = false;
			this.txtScore119.Name = "txtScore119";
			this.txtScore119.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore119.Text = "066";
			this.txtScore119.Top = 2.872441F;
			this.txtScore119.Width = 1.12677F;
			// 
			// txtScore120
			// 
			this.txtScore120.DataField = "ITEM67";
			this.txtScore120.Height = 0.2393701F;
			this.txtScore120.Left = 0.6244096F;
			this.txtScore120.MultiLine = false;
			this.txtScore120.Name = "txtScore120";
			this.txtScore120.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore120.Text = "67";
			this.txtScore120.Top = 3.111811F;
			this.txtScore120.Width = 1.738189F;
			// 
			// txtScore116
			// 
			this.txtScore116.DataField = "ITEM63";
			this.txtScore116.Height = 0.2393701F;
			this.txtScore116.Left = 2.366929F;
			this.txtScore116.MultiLine = false;
			this.txtScore116.Name = "txtScore116";
			this.txtScore116.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore116.Text = "063";
			this.txtScore116.Top = 2.872441F;
			this.txtScore116.Width = 1.124016F;
			// 
			// txtScore121
			// 
			this.txtScore121.DataField = "ITEM68";
			this.txtScore121.Height = 0.2393701F;
			this.txtScore121.Left = 2.365354F;
			this.txtScore121.MultiLine = false;
			this.txtScore121.Name = "txtScore121";
			this.txtScore121.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore121.Text = "068";
			this.txtScore121.Top = 3.111811F;
			this.txtScore121.Width = 1.124016F;
			// 
			// txtScore123
			// 
			this.txtScore123.DataField = "ITEM70";
			this.txtScore123.Height = 0.2393701F;
			this.txtScore123.Left = 4.719686F;
			this.txtScore123.MultiLine = false;
			this.txtScore123.Name = "txtScore123";
			this.txtScore123.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore123.Text = "070";
			this.txtScore123.Top = 3.111811F;
			this.txtScore123.Width = 1.124016F;
			// 
			// txtScore124
			// 
			this.txtScore124.DataField = "ITEM71";
			this.txtScore124.Height = 0.2393701F;
			this.txtScore124.Left = 5.918898F;
			this.txtScore124.MultiLine = false;
			this.txtScore124.Name = "txtScore124";
			this.txtScore124.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore124.Text = "071";
			this.txtScore124.Top = 3.111811F;
			this.txtScore124.Width = 1.127165F;
			// 
			// txtScore125
			// 
			this.txtScore125.DataField = "ITEM72";
			this.txtScore125.Height = 0.2393701F;
			this.txtScore125.Left = 0.6251971F;
			this.txtScore125.MultiLine = false;
			this.txtScore125.Name = "txtScore125";
			this.txtScore125.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore125.Text = "72";
			this.txtScore125.Top = 3.351181F;
			this.txtScore125.Width = 1.737401F;
			// 
			// txtScore126
			// 
			this.txtScore126.DataField = "ITEM73";
			this.txtScore126.Height = 0.2393701F;
			this.txtScore126.Left = 2.367716F;
			this.txtScore126.MultiLine = false;
			this.txtScore126.Name = "txtScore126";
			this.txtScore126.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore126.Text = "073";
			this.txtScore126.Top = 3.351181F;
			this.txtScore126.Width = 1.124016F;
			// 
			// txtScore122
			// 
			this.txtScore122.DataField = "ITEM69";
			this.txtScore122.Height = 0.2393701F;
			this.txtScore122.Left = 3.538584F;
			this.txtScore122.MultiLine = false;
			this.txtScore122.Name = "txtScore122";
			this.txtScore122.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore122.Text = "069";
			this.txtScore122.Top = 3.111811F;
			this.txtScore122.Width = 1.124016F;
			// 
			// txtScore127
			// 
			this.txtScore127.DataField = "ITEM74";
			this.txtScore127.Height = 0.2393701F;
			this.txtScore127.Left = 3.53819F;
			this.txtScore127.MultiLine = false;
			this.txtScore127.Name = "txtScore127";
			this.txtScore127.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore127.Text = "074";
			this.txtScore127.Top = 3.351181F;
			this.txtScore127.Width = 1.124016F;
			// 
			// txtScore129
			// 
			this.txtScore129.DataField = "ITEM76";
			this.txtScore129.Height = 0.2393701F;
			this.txtScore129.Left = 5.919292F;
			this.txtScore129.MultiLine = false;
			this.txtScore129.Name = "txtScore129";
			this.txtScore129.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore129.Text = "076";
			this.txtScore129.Top = 3.351181F;
			this.txtScore129.Width = 1.12677F;
			// 
			// txtScore130
			// 
			this.txtScore130.DataField = "ITEM77";
			this.txtScore130.Height = 0.2393701F;
			this.txtScore130.Left = 0.6251971F;
			this.txtScore130.MultiLine = false;
			this.txtScore130.Name = "txtScore130";
			this.txtScore130.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore130.Text = "77";
			this.txtScore130.Top = 3.590551F;
			this.txtScore130.Width = 1.737401F;
			// 
			// txtScore131
			// 
			this.txtScore131.DataField = "ITEM78";
			this.txtScore131.Height = 0.2393701F;
			this.txtScore131.Left = 2.367716F;
			this.txtScore131.MultiLine = false;
			this.txtScore131.Name = "txtScore131";
			this.txtScore131.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore131.Text = "078";
			this.txtScore131.Top = 3.590551F;
			this.txtScore131.Width = 1.124016F;
			// 
			// txtScore132
			// 
			this.txtScore132.DataField = "ITEM79";
			this.txtScore132.Height = 0.2393701F;
			this.txtScore132.Left = 3.539765F;
			this.txtScore132.MultiLine = false;
			this.txtScore132.Name = "txtScore132";
			this.txtScore132.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore132.Text = "079";
			this.txtScore132.Top = 3.590551F;
			this.txtScore132.Width = 1.124016F;
			// 
			// txtScore128
			// 
			this.txtScore128.DataField = "ITEM75";
			this.txtScore128.Height = 0.2393701F;
			this.txtScore128.Left = 4.721261F;
			this.txtScore128.MultiLine = false;
			this.txtScore128.Name = "txtScore128";
			this.txtScore128.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore128.Text = "075";
			this.txtScore128.Top = 3.351181F;
			this.txtScore128.Width = 1.124016F;
			// 
			// txtScore133
			// 
			this.txtScore133.DataField = "ITEM80";
			this.txtScore133.Height = 0.2393701F;
			this.txtScore133.Left = 4.718899F;
			this.txtScore133.MultiLine = false;
			this.txtScore133.Name = "txtScore133";
			this.txtScore133.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore133.Text = "080";
			this.txtScore133.Top = 3.590551F;
			this.txtScore133.Width = 1.124016F;
			// 
			// txtScore135
			// 
			this.txtScore135.DataField = "ITEM82";
			this.txtScore135.Height = 0.2393701F;
			this.txtScore135.Left = 0.6251971F;
			this.txtScore135.MultiLine = false;
			this.txtScore135.Name = "txtScore135";
			this.txtScore135.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore135.Text = "82";
			this.txtScore135.Top = 3.829921F;
			this.txtScore135.Width = 1.737402F;
			// 
			// txtScore136
			// 
			this.txtScore136.DataField = "ITEM83";
			this.txtScore136.Height = 0.2393701F;
			this.txtScore136.Left = 2.367716F;
			this.txtScore136.MultiLine = false;
			this.txtScore136.Name = "txtScore136";
			this.txtScore136.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore136.Text = "083";
			this.txtScore136.Top = 3.829921F;
			this.txtScore136.Width = 1.124016F;
			// 
			// txtScore137
			// 
			this.txtScore137.DataField = "ITEM84";
			this.txtScore137.Height = 0.2393701F;
			this.txtScore137.Left = 3.540158F;
			this.txtScore137.MultiLine = false;
			this.txtScore137.Name = "txtScore137";
			this.txtScore137.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore137.Text = "084";
			this.txtScore137.Top = 3.829921F;
			this.txtScore137.Width = 1.124016F;
			// 
			// txtScore138
			// 
			this.txtScore138.DataField = "ITEM85";
			this.txtScore138.Height = 0.2393701F;
			this.txtScore138.Left = 4.719686F;
			this.txtScore138.MultiLine = false;
			this.txtScore138.Name = "txtScore138";
			this.txtScore138.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore138.Text = "085";
			this.txtScore138.Top = 3.829921F;
			this.txtScore138.Width = 1.124016F;
			// 
			// txtScore134
			// 
			this.txtScore134.DataField = "ITEM81";
			this.txtScore134.Height = 0.2393701F;
			this.txtScore134.Left = 5.919292F;
			this.txtScore134.MultiLine = false;
			this.txtScore134.Name = "txtScore134";
			this.txtScore134.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore134.Text = "081";
			this.txtScore134.Top = 3.590551F;
			this.txtScore134.Width = 1.127165F;
			// 
			// txtScore139
			// 
			this.txtScore139.DataField = "ITEM86";
			this.txtScore139.Height = 0.2393701F;
			this.txtScore139.Left = 5.919292F;
			this.txtScore139.MultiLine = false;
			this.txtScore139.Name = "txtScore139";
			this.txtScore139.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore139.Text = "086";
			this.txtScore139.Top = 3.829921F;
			this.txtScore139.Width = 1.127165F;
			// 
			// txtScore141
			// 
			this.txtScore141.DataField = "ITEM88";
			this.txtScore141.Height = 0.2393701F;
			this.txtScore141.Left = 2.36811F;
			this.txtScore141.MultiLine = false;
			this.txtScore141.Name = "txtScore141";
			this.txtScore141.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore141.Text = "088";
			this.txtScore141.Top = 4.069292F;
			this.txtScore141.Width = 1.124016F;
			// 
			// txtScore142
			// 
			this.txtScore142.DataField = "ITEM89";
			this.txtScore142.Height = 0.2393701F;
			this.txtScore142.Left = 3.540158F;
			this.txtScore142.MultiLine = false;
			this.txtScore142.Name = "txtScore142";
			this.txtScore142.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore142.Text = "089";
			this.txtScore142.Top = 4.069292F;
			this.txtScore142.Width = 1.124016F;
			// 
			// txtScore143
			// 
			this.txtScore143.DataField = "ITEM90";
			this.txtScore143.Height = 0.2393701F;
			this.txtScore143.Left = 4.719686F;
			this.txtScore143.MultiLine = false;
			this.txtScore143.Name = "txtScore143";
			this.txtScore143.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore143.Text = "090";
			this.txtScore143.Top = 4.069292F;
			this.txtScore143.Width = 1.124016F;
			// 
			// txtScore144
			// 
			this.txtScore144.DataField = "ITEM91";
			this.txtScore144.Height = 0.2393701F;
			this.txtScore144.Left = 5.919686F;
			this.txtScore144.MultiLine = false;
			this.txtScore144.Name = "txtScore144";
			this.txtScore144.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore144.Text = "091";
			this.txtScore144.Top = 4.069292F;
			this.txtScore144.Width = 1.126771F;
			// 
			// txtScore140
			// 
			this.txtScore140.DataField = "ITEM87";
			this.txtScore140.Height = 0.2393701F;
			this.txtScore140.Left = 0.6251971F;
			this.txtScore140.MultiLine = false;
			this.txtScore140.Name = "txtScore140";
			this.txtScore140.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore140.Text = "87";
			this.txtScore140.Top = 4.069292F;
			this.txtScore140.Width = 1.737402F;
			// 
			// txtScore145
			// 
			this.txtScore145.DataField = "ITEM92";
			this.txtScore145.Height = 0.2393701F;
			this.txtScore145.Left = 0.6267719F;
			this.txtScore145.MultiLine = false;
			this.txtScore145.Name = "txtScore145";
			this.txtScore145.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore145.Text = "92";
			this.txtScore145.Top = 4.308662F;
			this.txtScore145.Width = 1.735827F;
			// 
			// txtScore147
			// 
			this.txtScore147.DataField = "ITEM94";
			this.txtScore147.Height = 0.2393701F;
			this.txtScore147.Left = 3.540552F;
			this.txtScore147.MultiLine = false;
			this.txtScore147.Name = "txtScore147";
			this.txtScore147.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore147.Text = "094";
			this.txtScore147.Top = 4.308662F;
			this.txtScore147.Width = 1.124016F;
			// 
			// txtScore148
			// 
			this.txtScore148.DataField = "ITEM95";
			this.txtScore148.Height = 0.2393701F;
			this.txtScore148.Left = 4.719686F;
			this.txtScore148.MultiLine = false;
			this.txtScore148.Name = "txtScore148";
			this.txtScore148.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore148.Text = "095";
			this.txtScore148.Top = 4.308662F;
			this.txtScore148.Width = 1.124016F;
			// 
			// txtScore149
			// 
			this.txtScore149.DataField = "ITEM96";
			this.txtScore149.Height = 0.2393701F;
			this.txtScore149.Left = 5.919686F;
			this.txtScore149.MultiLine = false;
			this.txtScore149.Name = "txtScore149";
			this.txtScore149.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore149.Text = "096";
			this.txtScore149.Top = 4.308662F;
			this.txtScore149.Width = 1.127165F;
			// 
			// txtScore150
			// 
			this.txtScore150.DataField = "ITEM97";
			this.txtScore150.Height = 0.2393701F;
			this.txtScore150.Left = 0.6228349F;
			this.txtScore150.MultiLine = false;
			this.txtScore150.Name = "txtScore150";
			this.txtScore150.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore150.Text = "97";
			this.txtScore150.Top = 4.548032F;
			this.txtScore150.Width = 1.741732F;
			// 
			// txtScore146
			// 
			this.txtScore146.DataField = "ITEM93";
			this.txtScore146.Height = 0.2393701F;
			this.txtScore146.Left = 2.366141F;
			this.txtScore146.MultiLine = false;
			this.txtScore146.Name = "txtScore146";
			this.txtScore146.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore146.Text = "093";
			this.txtScore146.Top = 4.308662F;
			this.txtScore146.Width = 1.124016F;
			// 
			// txtScore151
			// 
			this.txtScore151.DataField = "ITEM98";
			this.txtScore151.Height = 0.2393701F;
			this.txtScore151.Left = 2.366141F;
			this.txtScore151.MultiLine = false;
			this.txtScore151.Name = "txtScore151";
			this.txtScore151.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore151.Text = "098";
			this.txtScore151.Top = 4.548032F;
			this.txtScore151.Width = 1.124016F;
			// 
			// txtScore153
			// 
			this.txtScore153.DataField = "ITEM100";
			this.txtScore153.Height = 0.2393701F;
			this.txtScore153.Left = 4.720473F;
			this.txtScore153.MultiLine = false;
			this.txtScore153.Name = "txtScore153";
			this.txtScore153.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore153.Text = "100";
			this.txtScore153.Top = 4.548032F;
			this.txtScore153.Width = 1.124016F;
			// 
			// txtScore154
			// 
			this.txtScore154.DataField = "ITEM101";
			this.txtScore154.Height = 0.2393701F;
			this.txtScore154.Left = 5.919686F;
			this.txtScore154.MultiLine = false;
			this.txtScore154.Name = "txtScore154";
			this.txtScore154.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore154.Text = "101";
			this.txtScore154.Top = 4.548032F;
			this.txtScore154.Width = 1.127559F;
			// 
			// txtScore155
			// 
			this.txtScore155.DataField = "ITEM102";
			this.txtScore155.Height = 0.2393701F;
			this.txtScore155.Left = 0.6267719F;
			this.txtScore155.MultiLine = false;
			this.txtScore155.Name = "txtScore155";
			this.txtScore155.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore155.Text = "102";
			this.txtScore155.Top = 4.787402F;
			this.txtScore155.Width = 1.737795F;
			// 
			// txtScore156
			// 
			this.txtScore156.DataField = "ITEM103";
			this.txtScore156.Height = 0.2393701F;
			this.txtScore156.Left = 2.365354F;
			this.txtScore156.MultiLine = false;
			this.txtScore156.Name = "txtScore156";
			this.txtScore156.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore156.Text = "103";
			this.txtScore156.Top = 4.787402F;
			this.txtScore156.Width = 1.124016F;
			// 
			// txtScore152
			// 
			this.txtScore152.DataField = "ITEM99";
			this.txtScore152.Height = 0.2393701F;
			this.txtScore152.Left = 3.537796F;
			this.txtScore152.MultiLine = false;
			this.txtScore152.Name = "txtScore152";
			this.txtScore152.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore152.Text = "099";
			this.txtScore152.Top = 4.548032F;
			this.txtScore152.Width = 1.124016F;
			// 
			// txtScore157
			// 
			this.txtScore157.DataField = "ITEM104";
			this.txtScore157.Height = 0.2393701F;
			this.txtScore157.Left = 3.537796F;
			this.txtScore157.MultiLine = false;
			this.txtScore157.Name = "txtScore157";
			this.txtScore157.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore157.Text = "104";
			this.txtScore157.Top = 4.787402F;
			this.txtScore157.Width = 1.124016F;
			// 
			// txtScore159
			// 
			this.txtScore159.DataField = "ITEM106";
			this.txtScore159.Height = 0.2393701F;
			this.txtScore159.Left = 5.919292F;
			this.txtScore159.MultiLine = false;
			this.txtScore159.Name = "txtScore159";
			this.txtScore159.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore159.Text = "106";
			this.txtScore159.Top = 4.787402F;
			this.txtScore159.Width = 1.127165F;
			// 
			// txtScore160
			// 
			this.txtScore160.DataField = "ITEM107";
			this.txtScore160.Height = 0.2393701F;
			this.txtScore160.Left = 0.6267719F;
			this.txtScore160.MultiLine = false;
			this.txtScore160.Name = "txtScore160";
			this.txtScore160.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore160.Text = "107";
			this.txtScore160.Top = 5.026772F;
			this.txtScore160.Width = 1.735827F;
			// 
			// txtScore162
			// 
			this.txtScore162.DataField = "ITEM109";
			this.txtScore162.Height = 0.2393701F;
			this.txtScore162.Left = 3.537796F;
			this.txtScore162.MultiLine = false;
			this.txtScore162.Name = "txtScore162";
			this.txtScore162.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore162.Text = "109";
			this.txtScore162.Top = 5.026772F;
			this.txtScore162.Width = 1.124016F;
			// 
			// txtScore158
			// 
			this.txtScore158.DataField = "ITEM105";
			this.txtScore158.Height = 0.2393701F;
			this.txtScore158.Left = 4.721261F;
			this.txtScore158.MultiLine = false;
			this.txtScore158.Name = "txtScore158";
			this.txtScore158.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore158.Text = "105";
			this.txtScore158.Top = 4.787402F;
			this.txtScore158.Width = 1.124016F;
			// 
			// txtScore163
			// 
			this.txtScore163.DataField = "ITEM110";
			this.txtScore163.Height = 0.2393701F;
			this.txtScore163.Left = 4.719686F;
			this.txtScore163.MultiLine = false;
			this.txtScore163.Name = "txtScore163";
			this.txtScore163.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore163.Text = "110";
			this.txtScore163.Top = 5.026772F;
			this.txtScore163.Width = 1.124016F;
			// 
			// line16
			// 
			this.line16.Height = 0F;
			this.line16.Left = 0.007874016F;
			this.line16.LineWeight = 1F;
			this.line16.Name = "line16";
			this.line16.Top = 5.266142F;
			this.line16.Width = 7.07874F;
			this.line16.X1 = 7.086614F;
			this.line16.X2 = 0.007874016F;
			this.line16.Y1 = 5.266142F;
			this.line16.Y2 = 5.266142F;
			// 
			// line17
			// 
			this.line17.Height = 0F;
			this.line17.Left = 0.007874016F;
			this.line17.LineWeight = 1F;
			this.line17.Name = "line17";
			this.line17.Top = 2.633071F;
			this.line17.Width = 7.07874F;
			this.line17.X1 = 7.086614F;
			this.line17.X2 = 0.007874016F;
			this.line17.Y1 = 2.633071F;
			this.line17.Y2 = 2.633071F;
			// 
			// line25
			// 
			this.line25.Height = 5.266142F;
			this.line25.Left = 7.086614F;
			this.line25.LineWeight = 1F;
			this.line25.Name = "line25";
			this.line25.Top = 0F;
			this.line25.Width = 0F;
			this.line25.X1 = 7.086614F;
			this.line25.X2 = 7.086614F;
			this.line25.Y1 = 5.266142F;
			this.line25.Y2 = 0F;
			// 
			// line26
			// 
			this.line26.Height = 5.266142F;
			this.line26.Left = 0.007874016F;
			this.line26.LineWeight = 1F;
			this.line26.Name = "line26";
			this.line26.Top = 0F;
			this.line26.Width = 0F;
			this.line26.X1 = 0.007874016F;
			this.line26.X2 = 0.007874016F;
			this.line26.Y1 = 5.266142F;
			this.line26.Y2 = 0F;
			// 
			// line27
			// 
			this.line27.Height = 5.266142F;
			this.line27.Left = 0.5767717F;
			this.line27.LineWeight = 1F;
			this.line27.Name = "line27";
			this.line27.Top = 0F;
			this.line27.Width = 0F;
			this.line27.X1 = 0.5767717F;
			this.line27.X2 = 0.5767717F;
			this.line27.Y1 = 5.266142F;
			this.line27.Y2 = 0F;
			// 
			// line203
			// 
			this.line203.Height = 0F;
			this.line203.Left = 0.5767717F;
			this.line203.LineWeight = 1F;
			this.line203.Name = "line203";
			this.line203.Top = 0.2393701F;
			this.line203.Width = 6.509842F;
			this.line203.X1 = 7.086614F;
			this.line203.X2 = 0.5767717F;
			this.line203.Y1 = 0.2393701F;
			this.line203.Y2 = 0.2393701F;
			// 
			// line204
			// 
			this.line204.Height = 0F;
			this.line204.Left = 0.5767717F;
			this.line204.LineWeight = 1F;
			this.line204.Name = "line204";
			this.line204.Top = 0.4787402F;
			this.line204.Width = 6.509842F;
			this.line204.X1 = 7.086614F;
			this.line204.X2 = 0.5767717F;
			this.line204.Y1 = 0.4787402F;
			this.line204.Y2 = 0.4787402F;
			// 
			// line205
			// 
			this.line205.Height = 0F;
			this.line205.Left = 0.5767717F;
			this.line205.LineWeight = 1F;
			this.line205.Name = "line205";
			this.line205.Top = 0.7181103F;
			this.line205.Width = 6.509842F;
			this.line205.X1 = 7.086614F;
			this.line205.X2 = 0.5767717F;
			this.line205.Y1 = 0.7181103F;
			this.line205.Y2 = 0.7181103F;
			// 
			// line206
			// 
			this.line206.Height = 0F;
			this.line206.Left = 0.5767717F;
			this.line206.LineWeight = 1F;
			this.line206.Name = "line206";
			this.line206.Top = 0.9574804F;
			this.line206.Width = 6.509842F;
			this.line206.X1 = 7.086614F;
			this.line206.X2 = 0.5767717F;
			this.line206.Y1 = 0.9574804F;
			this.line206.Y2 = 0.9574804F;
			// 
			// line207
			// 
			this.line207.Height = 0F;
			this.line207.Left = 0.5767717F;
			this.line207.LineWeight = 1F;
			this.line207.Name = "line207";
			this.line207.Top = 1.19685F;
			this.line207.Width = 6.509842F;
			this.line207.X1 = 7.086614F;
			this.line207.X2 = 0.5767717F;
			this.line207.Y1 = 1.19685F;
			this.line207.Y2 = 1.19685F;
			// 
			// line208
			// 
			this.line208.Height = 0F;
			this.line208.Left = 0.5767717F;
			this.line208.LineWeight = 1F;
			this.line208.Name = "line208";
			this.line208.Top = 1.436221F;
			this.line208.Width = 6.509842F;
			this.line208.X1 = 7.086614F;
			this.line208.X2 = 0.5767717F;
			this.line208.Y1 = 1.436221F;
			this.line208.Y2 = 1.436221F;
			// 
			// line209
			// 
			this.line209.Height = 0F;
			this.line209.Left = 0.5767717F;
			this.line209.LineWeight = 1F;
			this.line209.Name = "line209";
			this.line209.Top = 1.675591F;
			this.line209.Width = 6.509842F;
			this.line209.X1 = 7.086614F;
			this.line209.X2 = 0.5767717F;
			this.line209.Y1 = 1.675591F;
			this.line209.Y2 = 1.675591F;
			// 
			// line210
			// 
			this.line210.Height = 0F;
			this.line210.Left = 0.5767717F;
			this.line210.LineWeight = 1F;
			this.line210.Name = "line210";
			this.line210.Top = 1.914961F;
			this.line210.Width = 6.509842F;
			this.line210.X1 = 7.086614F;
			this.line210.X2 = 0.5767717F;
			this.line210.Y1 = 1.914961F;
			this.line210.Y2 = 1.914961F;
			// 
			// line211
			// 
			this.line211.Height = 0F;
			this.line211.Left = 0.5767717F;
			this.line211.LineWeight = 1F;
			this.line211.Name = "line211";
			this.line211.Top = 2.154331F;
			this.line211.Width = 6.509842F;
			this.line211.X1 = 7.086614F;
			this.line211.X2 = 0.5767717F;
			this.line211.Y1 = 2.154331F;
			this.line211.Y2 = 2.154331F;
			// 
			// line212
			// 
			this.line212.Height = 0F;
			this.line212.Left = 0.5767717F;
			this.line212.LineWeight = 1F;
			this.line212.Name = "line212";
			this.line212.Top = 2.393701F;
			this.line212.Width = 6.509842F;
			this.line212.X1 = 7.086614F;
			this.line212.X2 = 0.5767717F;
			this.line212.Y1 = 2.393701F;
			this.line212.Y2 = 2.393701F;
			// 
			// line213
			// 
			this.line213.Height = 0F;
			this.line213.Left = 0.5767717F;
			this.line213.LineWeight = 1F;
			this.line213.Name = "line213";
			this.line213.Top = 2.872441F;
			this.line213.Width = 6.509842F;
			this.line213.X1 = 7.086614F;
			this.line213.X2 = 0.5767717F;
			this.line213.Y1 = 2.872441F;
			this.line213.Y2 = 2.872441F;
			// 
			// line214
			// 
			this.line214.Height = 0F;
			this.line214.Left = 0.5768504F;
			this.line214.LineWeight = 1F;
			this.line214.Name = "line214";
			this.line214.Top = 5.026772F;
			this.line214.Width = 6.509764F;
			this.line214.X1 = 7.086614F;
			this.line214.X2 = 0.5768504F;
			this.line214.Y1 = 5.026772F;
			this.line214.Y2 = 5.026772F;
			// 
			// line215
			// 
			this.line215.Height = 0F;
			this.line215.Left = 0.5767717F;
			this.line215.LineWeight = 1F;
			this.line215.Name = "line215";
			this.line215.Top = 4.787402F;
			this.line215.Width = 6.509842F;
			this.line215.X1 = 7.086614F;
			this.line215.X2 = 0.5767717F;
			this.line215.Y1 = 4.787402F;
			this.line215.Y2 = 4.787402F;
			// 
			// line216
			// 
			this.line216.Height = 0F;
			this.line216.Left = 0.5767717F;
			this.line216.LineWeight = 1F;
			this.line216.Name = "line216";
			this.line216.Top = 4.548032F;
			this.line216.Width = 6.509842F;
			this.line216.X1 = 7.086614F;
			this.line216.X2 = 0.5767717F;
			this.line216.Y1 = 4.548032F;
			this.line216.Y2 = 4.548032F;
			// 
			// line217
			// 
			this.line217.Height = 0F;
			this.line217.Left = 0.5767717F;
			this.line217.LineWeight = 1F;
			this.line217.Name = "line217";
			this.line217.Top = 4.308662F;
			this.line217.Width = 6.509842F;
			this.line217.X1 = 7.086614F;
			this.line217.X2 = 0.5767717F;
			this.line217.Y1 = 4.308662F;
			this.line217.Y2 = 4.308662F;
			// 
			// line218
			// 
			this.line218.Height = 0F;
			this.line218.Left = 0.5767717F;
			this.line218.LineWeight = 1F;
			this.line218.Name = "line218";
			this.line218.Top = 4.069292F;
			this.line218.Width = 6.509842F;
			this.line218.X1 = 7.086614F;
			this.line218.X2 = 0.5767717F;
			this.line218.Y1 = 4.069292F;
			this.line218.Y2 = 4.069292F;
			// 
			// line219
			// 
			this.line219.Height = 0F;
			this.line219.Left = 0.5767717F;
			this.line219.LineWeight = 1F;
			this.line219.Name = "line219";
			this.line219.Top = 3.829921F;
			this.line219.Width = 6.509842F;
			this.line219.X1 = 7.086614F;
			this.line219.X2 = 0.5767717F;
			this.line219.Y1 = 3.829921F;
			this.line219.Y2 = 3.829921F;
			// 
			// line220
			// 
			this.line220.Height = 0F;
			this.line220.Left = 0.5767717F;
			this.line220.LineWeight = 1F;
			this.line220.Name = "line220";
			this.line220.Top = 3.590551F;
			this.line220.Width = 6.509842F;
			this.line220.X1 = 7.086614F;
			this.line220.X2 = 0.5767717F;
			this.line220.Y1 = 3.590551F;
			this.line220.Y2 = 3.590551F;
			// 
			// line221
			// 
			this.line221.Height = 0F;
			this.line221.Left = 0.5767717F;
			this.line221.LineWeight = 1F;
			this.line221.Name = "line221";
			this.line221.Top = 3.351181F;
			this.line221.Width = 6.509842F;
			this.line221.X1 = 7.086614F;
			this.line221.X2 = 0.5767717F;
			this.line221.Y1 = 3.351181F;
			this.line221.Y2 = 3.351181F;
			// 
			// line222
			// 
			this.line222.Height = 0F;
			this.line222.Left = 0.5767717F;
			this.line222.LineWeight = 1F;
			this.line222.Name = "line222";
			this.line222.Top = 3.111811F;
			this.line222.Width = 6.509842F;
			this.line222.X1 = 7.086614F;
			this.line222.X2 = 0.5767717F;
			this.line222.Y1 = 3.111811F;
			this.line222.Y2 = 3.111811F;
			// 
			// line223
			// 
			this.line223.Height = 5.266142F;
			this.line223.Left = 2.368898F;
			this.line223.LineWeight = 1F;
			this.line223.Name = "line223";
			this.line223.Top = 0F;
			this.line223.Width = 0F;
			this.line223.X1 = 2.368898F;
			this.line223.X2 = 2.368898F;
			this.line223.Y1 = 5.266142F;
			this.line223.Y2 = 0F;
			// 
			// line224
			// 
			this.line224.Height = 5.266142F;
			this.line224.Left = 3.536614F;
			this.line224.LineWeight = 1F;
			this.line224.Name = "line224";
			this.line224.Top = 0F;
			this.line224.Width = 0F;
			this.line224.X1 = 3.536614F;
			this.line224.X2 = 3.536614F;
			this.line224.Y1 = 5.266142F;
			this.line224.Y2 = 0F;
			// 
			// line225
			// 
			this.line225.Height = 5.266142F;
			this.line225.Left = 4.718898F;
			this.line225.LineWeight = 1F;
			this.line225.Name = "line225";
			this.line225.Top = 0F;
			this.line225.Width = 0F;
			this.line225.X1 = 4.718898F;
			this.line225.X2 = 4.718898F;
			this.line225.Y1 = 5.266142F;
			this.line225.Y2 = 0F;
			// 
			// line226
			// 
			this.line226.Height = 5.266142F;
			this.line226.Left = 5.920079F;
			this.line226.LineWeight = 1F;
			this.line226.Name = "line226";
			this.line226.Top = 0F;
			this.line226.Width = 0F;
			this.line226.X1 = 5.920079F;
			this.line226.X2 = 5.920079F;
			this.line226.Y1 = 5.266142F;
			this.line226.Y2 = 0F;
			// 
			// group03_H
			// 
			this.group03_H.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox193,
            this.textBox194,
            this.textBox195,
            this.textBox196,
            this.textBox197,
            this.textBox198,
            this.textBox199,
            this.line28,
            this.line29,
            this.line30,
            this.line31,
            this.line32,
            this.line33,
            this.line34,
            this.pageBreak1,
            this.line95});
			this.group03_H.DataField = "ITEM01";
			this.group03_H.Height = 0.6822835F;
			this.group03_H.Name = "group03_H";
			this.group03_H.Format += new System.EventHandler(this.group03_Format);
			// 
			// textBox193
			// 
			this.textBox193.Height = 0.4440945F;
			this.textBox193.Left = 0.007874016F;
			this.textBox193.MultiLine = false;
			this.textBox193.Name = "textBox193";
			this.textBox193.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: justify; vertical-align: middle";
			this.textBox193.Text = null;
			this.textBox193.Top = 0.238189F;
			this.textBox193.Width = 7.073622F;
			// 
			// textBox194
			// 
			this.textBox194.Height = 0.2389764F;
			this.textBox194.Left = 0.02716541F;
			this.textBox194.MultiLine = false;
			this.textBox194.Name = "textBox194";
			this.textBox194.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: justify; ve" +
    "rtical-align: middle";
			this.textBox194.Text = "（３）棚卸資産";
			this.textBox194.Top = 0F;
			this.textBox194.Width = 4.237402F;
			// 
			// textBox195
			// 
			this.textBox195.Height = 0.4429134F;
			this.textBox195.Left = 0F;
			this.textBox195.MultiLine = false;
			this.textBox195.Name = "textBox195";
			this.textBox195.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; te" +
    "xt-justify: auto; vertical-align: middle";
			this.textBox195.Text = "科目及び部門";
			this.textBox195.Top = 0.2393701F;
			this.textBox195.Width = 2.044095F;
			// 
			// textBox196
			// 
			this.textBox196.Height = 0.4492126F;
			this.textBox196.Left = 2.046457F;
			this.textBox196.MultiLine = false;
			this.textBox196.Name = "textBox196";
			this.textBox196.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox196.Text = "前年度末残高";
			this.textBox196.Top = 0.238189F;
			this.textBox196.Width = 1.264961F;
			// 
			// textBox197
			// 
			this.textBox197.Height = 0.4259843F;
			this.textBox197.Left = 3.309449F;
			this.textBox197.MultiLine = false;
			this.textBox197.Name = "textBox197";
			this.textBox197.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox197.Text = "本年度増加額";
			this.textBox197.Top = 0.238189F;
			this.textBox197.Width = 1.265354F;
			// 
			// textBox198
			// 
			this.textBox198.Height = 0.4255905F;
			this.textBox198.Left = 4.574803F;
			this.textBox198.MultiLine = false;
			this.textBox198.Name = "textBox198";
			this.textBox198.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox198.Text = "本年度減少額";
			this.textBox198.Top = 0.238189F;
			this.textBox198.Width = 1.265355F;
			// 
			// textBox199
			// 
			this.textBox199.Height = 0.4259844F;
			this.textBox199.Left = 5.840158F;
			this.textBox199.MultiLine = false;
			this.textBox199.Name = "textBox199";
			this.textBox199.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox199.Text = "本年度末残高";
			this.textBox199.Top = 0.2389764F;
			this.textBox199.Width = 1.246457F;
			// 
			// line28
			// 
			this.line28.Height = 0F;
			this.line28.Left = 0.007874016F;
			this.line28.LineWeight = 1F;
			this.line28.Name = "line28";
			this.line28.Top = 0.2389764F;
			this.line28.Width = 7.07874F;
			this.line28.X1 = 7.086614F;
			this.line28.X2 = 0.007874016F;
			this.line28.Y1 = 0.2389764F;
			this.line28.Y2 = 0.2389764F;
			// 
			// line29
			// 
			this.line29.Height = 0F;
			this.line29.Left = 0.007874016F;
			this.line29.LineWeight = 1F;
			this.line29.Name = "line29";
			this.line29.Top = 0.6822835F;
			this.line29.Width = 7.07874F;
			this.line29.X1 = 7.086614F;
			this.line29.X2 = 0.007874016F;
			this.line29.Y1 = 0.6822835F;
			this.line29.Y2 = 0.6822835F;
			// 
			// line30
			// 
			this.line30.Height = 0.4440945F;
			this.line30.Left = 5.846063F;
			this.line30.LineWeight = 1F;
			this.line30.Name = "line30";
			this.line30.Top = 0.238189F;
			this.line30.Width = 0F;
			this.line30.X1 = 5.846063F;
			this.line30.X2 = 5.846063F;
			this.line30.Y1 = 0.238189F;
			this.line30.Y2 = 0.6822835F;
			// 
			// line31
			// 
			this.line31.Height = 0.4440945F;
			this.line31.Left = 4.577166F;
			this.line31.LineWeight = 1F;
			this.line31.Name = "line31";
			this.line31.Top = 0.238189F;
			this.line31.Width = 0F;
			this.line31.X1 = 4.577166F;
			this.line31.X2 = 4.577166F;
			this.line31.Y1 = 0.238189F;
			this.line31.Y2 = 0.6822835F;
			// 
			// line32
			// 
			this.line32.Height = 0.4437008F;
			this.line32.Left = 3.312598F;
			this.line32.LineWeight = 1F;
			this.line32.Name = "line32";
			this.line32.Top = 0.2385827F;
			this.line32.Width = 0F;
			this.line32.X1 = 3.312598F;
			this.line32.X2 = 3.312598F;
			this.line32.Y1 = 0.2385827F;
			this.line32.Y2 = 0.6822835F;
			// 
			// line33
			// 
			this.line33.Height = 0.4440945F;
			this.line33.Left = 2.044095F;
			this.line33.LineWeight = 1F;
			this.line33.Name = "line33";
			this.line33.Top = 0.238189F;
			this.line33.Width = 0F;
			this.line33.X1 = 2.044095F;
			this.line33.X2 = 2.044095F;
			this.line33.Y1 = 0.238189F;
			this.line33.Y2 = 0.6822835F;
			// 
			// line34
			// 
			this.line34.Height = 0.4433071F;
			this.line34.Left = 7.086614F;
			this.line34.LineWeight = 1F;
			this.line34.Name = "line34";
			this.line34.Top = 0.2389764F;
			this.line34.Width = 0F;
			this.line34.X1 = 7.086614F;
			this.line34.X2 = 7.086614F;
			this.line34.Y1 = 0.2389764F;
			this.line34.Y2 = 0.6822835F;
			// 
			// pageBreak1
			// 
			this.pageBreak1.Height = 0.01F;
			this.pageBreak1.Left = 0F;
			this.pageBreak1.Name = "pageBreak1";
			this.pageBreak1.Size = new System.Drawing.SizeF(6F, 0.01F);
			this.pageBreak1.Top = 0F;
			this.pageBreak1.Width = 6F;
			// 
			// line95
			// 
			this.line95.Height = 0.446063F;
			this.line95.Left = 0.007874016F;
			this.line95.LineWeight = 1F;
			this.line95.Name = "line95";
			this.line95.Top = 0.2362205F;
			this.line95.Width = 0F;
			this.line95.X1 = 0.007874016F;
			this.line95.X2 = 0.007874016F;
			this.line95.Y1 = 0.2362205F;
			this.line95.Y2 = 0.6822835F;
			// 
			// group03_F
			// 
			this.group03_F.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtScore229,
            this.txtScore225,
            this.txtScore165,
            this.txtScore167,
            this.txtScore168,
            this.txtScore169,
            this.txtScore170,
            this.txtScore166,
            this.txtScore171,
            this.txtScore173,
            this.txtScore174,
            this.txtScore175,
            this.txtScore176,
            this.txtScore172,
            this.txtScore177,
            this.txtScore179,
            this.txtScore180,
            this.txtScore181,
            this.txtScore182,
            this.txtScore178,
            this.txtScore183,
            this.txtScore185,
            this.txtScore186,
            this.txtScore187,
            this.txtScore188,
            this.txtScore184,
            this.txtScore189,
            this.txtScore191,
            this.txtScore192,
            this.txtScore193,
            this.txtScore194,
            this.txtScore190,
            this.txtScore195,
            this.txtScore197,
            this.txtScore198,
            this.txtScore199,
            this.txtScore200,
            this.txtScore196,
            this.txtScore201,
            this.txtScore203,
            this.txtScore204,
            this.txtScore205,
            this.txtScore206,
            this.txtScore202,
            this.txtScore207,
            this.txtScore209,
            this.txtScore210,
            this.txtScore211,
            this.txtScore212,
            this.txtScore208,
            this.txtScore213,
            this.txtScore215,
            this.txtScore216,
            this.txtScore217,
            this.txtScore218,
            this.txtScore214,
            this.txtScore219,
            this.txtScore220,
            this.txtScore222,
            this.txtScore223,
            this.txtScore224,
            this.txtScore221,
            this.line36,
            this.txtScore227,
            this.txtScore228,
            this.txtScore226,
            this.line46,
            this.line35,
            this.line187,
            this.line188,
            this.line189,
            this.line190,
            this.line191,
            this.line192,
            this.line193,
            this.line194,
            this.line195,
            this.line196,
            this.line197,
            this.line198,
            this.line199,
            this.line200,
            this.line201,
            this.line202});
			this.group03_F.Height = 3.741837F;
			this.group03_F.Name = "group03_F";
			// 
			// txtScore229
			// 
			this.txtScore229.DataField = "ITEM66";
			this.txtScore229.Height = 0.2393701F;
			this.txtScore229.Left = 5.844488F;
			this.txtScore229.MultiLine = false;
			this.txtScore229.Name = "txtScore229";
			this.txtScore229.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore229.Text = "066";
			this.txtScore229.Top = 2.872441F;
			this.txtScore229.Width = 1.189765F;
			// 
			// txtScore225
			// 
			this.txtScore225.DataField = "ITEM62";
			this.txtScore225.Height = 0.2393701F;
			this.txtScore225.Left = 0.07795276F;
			this.txtScore225.MultiLine = false;
			this.txtScore225.Name = "txtScore225";
			this.txtScore225.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore225.Text = "ITEM62";
			this.txtScore225.Top = 2.872441F;
			this.txtScore225.Width = 1.970078F;
			// 
			// txtScore165
			// 
			this.txtScore165.DataField = "ITEM02";
			this.txtScore165.Height = 0.2393701F;
			this.txtScore165.Left = 0.07795276F;
			this.txtScore165.MultiLine = false;
			this.txtScore165.Name = "txtScore165";
			this.txtScore165.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore165.Text = "02";
			this.txtScore165.Top = 0F;
			this.txtScore165.Width = 1.968898F;
			// 
			// txtScore167
			// 
			this.txtScore167.DataField = "ITEM04";
			this.txtScore167.Height = 0.2393701F;
			this.txtScore167.Left = 3.311024F;
			this.txtScore167.MultiLine = false;
			this.txtScore167.Name = "txtScore167";
			this.txtScore167.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore167.Text = "004";
			this.txtScore167.Top = -8.881784E-16F;
			this.txtScore167.Width = 1.213386F;
			// 
			// txtScore168
			// 
			this.txtScore168.DataField = "ITEM05";
			this.txtScore168.Height = 0.2393701F;
			this.txtScore168.Left = 4.572441F;
			this.txtScore168.MultiLine = false;
			this.txtScore168.Name = "txtScore168";
			this.txtScore168.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore168.Text = "005";
			this.txtScore168.Top = 0F;
			this.txtScore168.Width = 1.214567F;
			// 
			// txtScore169
			// 
			this.txtScore169.DataField = "ITEM06";
			this.txtScore169.Height = 0.2393701F;
			this.txtScore169.Left = 5.842126F;
			this.txtScore169.MultiLine = false;
			this.txtScore169.Name = "txtScore169";
			this.txtScore169.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore169.Text = "006";
			this.txtScore169.Top = 0F;
			this.txtScore169.Width = 1.201575F;
			// 
			// txtScore170
			// 
			this.txtScore170.DataField = "ITEM07";
			this.txtScore170.Height = 0.2393701F;
			this.txtScore170.Left = 0.07795276F;
			this.txtScore170.MultiLine = false;
			this.txtScore170.Name = "txtScore170";
			this.txtScore170.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore170.Text = "007";
			this.txtScore170.Top = 0.2393701F;
			this.txtScore170.Width = 1.968898F;
			// 
			// txtScore166
			// 
			this.txtScore166.DataField = "ITEM03";
			this.txtScore166.Height = 0.2393701F;
			this.txtScore166.Left = 2.046457F;
			this.txtScore166.MultiLine = false;
			this.txtScore166.Name = "txtScore166";
			this.txtScore166.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore166.Text = "003";
			this.txtScore166.Top = 2.980232E-08F;
			this.txtScore166.Width = 1.211024F;
			// 
			// txtScore171
			// 
			this.txtScore171.DataField = "ITEM08";
			this.txtScore171.Height = 0.2393701F;
			this.txtScore171.Left = 2.046457F;
			this.txtScore171.MultiLine = false;
			this.txtScore171.Name = "txtScore171";
			this.txtScore171.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore171.Text = "008";
			this.txtScore171.Top = 0.2393701F;
			this.txtScore171.Width = 1.211024F;
			// 
			// txtScore173
			// 
			this.txtScore173.DataField = "ITEM10";
			this.txtScore173.Height = 0.2393701F;
			this.txtScore173.Left = 4.572441F;
			this.txtScore173.MultiLine = false;
			this.txtScore173.Name = "txtScore173";
			this.txtScore173.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore173.Text = "010";
			this.txtScore173.Top = 0.2393701F;
			this.txtScore173.Width = 1.21496F;
			// 
			// txtScore174
			// 
			this.txtScore174.DataField = "ITEM11";
			this.txtScore174.Height = 0.2393701F;
			this.txtScore174.Left = 5.842126F;
			this.txtScore174.MultiLine = false;
			this.txtScore174.Name = "txtScore174";
			this.txtScore174.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore174.Text = "011";
			this.txtScore174.Top = 0.2393701F;
			this.txtScore174.Width = 1.201575F;
			// 
			// txtScore175
			// 
			this.txtScore175.DataField = "ITEM12";
			this.txtScore175.Height = 0.2393701F;
			this.txtScore175.Left = 0.07795276F;
			this.txtScore175.MultiLine = false;
			this.txtScore175.Name = "txtScore175";
			this.txtScore175.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore175.Text = "012";
			this.txtScore175.Top = 0.4787402F;
			this.txtScore175.Width = 1.968898F;
			// 
			// txtScore176
			// 
			this.txtScore176.DataField = "ITEM13";
			this.txtScore176.Height = 0.2393701F;
			this.txtScore176.Left = 2.046457F;
			this.txtScore176.MultiLine = false;
			this.txtScore176.Name = "txtScore176";
			this.txtScore176.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore176.Text = "013";
			this.txtScore176.Top = 0.4787402F;
			this.txtScore176.Width = 1.210236F;
			// 
			// txtScore172
			// 
			this.txtScore172.DataField = "ITEM09";
			this.txtScore172.Height = 0.2393701F;
			this.txtScore172.Left = 3.311024F;
			this.txtScore172.MultiLine = false;
			this.txtScore172.Name = "txtScore172";
			this.txtScore172.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore172.Text = "009";
			this.txtScore172.Top = 0.2393701F;
			this.txtScore172.Width = 1.213386F;
			// 
			// txtScore177
			// 
			this.txtScore177.DataField = "ITEM14";
			this.txtScore177.Height = 0.2393701F;
			this.txtScore177.Left = 3.311024F;
			this.txtScore177.MultiLine = false;
			this.txtScore177.Name = "txtScore177";
			this.txtScore177.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore177.Text = "014";
			this.txtScore177.Top = 0.47874F;
			this.txtScore177.Width = 1.213386F;
			// 
			// txtScore179
			// 
			this.txtScore179.DataField = "ITEM16";
			this.txtScore179.Height = 0.2393701F;
			this.txtScore179.Left = 5.842126F;
			this.txtScore179.MultiLine = false;
			this.txtScore179.Name = "txtScore179";
			this.txtScore179.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore179.Text = "016";
			this.txtScore179.Top = 0.4787401F;
			this.txtScore179.Width = 1.201575F;
			// 
			// txtScore180
			// 
			this.txtScore180.DataField = "ITEM17";
			this.txtScore180.Height = 0.2393701F;
			this.txtScore180.Left = 0.07795276F;
			this.txtScore180.MultiLine = false;
			this.txtScore180.Name = "txtScore180";
			this.txtScore180.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore180.Text = "017";
			this.txtScore180.Top = 0.7181104F;
			this.txtScore180.Width = 1.968898F;
			// 
			// txtScore181
			// 
			this.txtScore181.DataField = "ITEM18";
			this.txtScore181.Height = 0.2393701F;
			this.txtScore181.Left = 2.046457F;
			this.txtScore181.MultiLine = false;
			this.txtScore181.Name = "txtScore181";
			this.txtScore181.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore181.Text = "018";
			this.txtScore181.Top = 0.7181104F;
			this.txtScore181.Width = 1.210236F;
			// 
			// txtScore182
			// 
			this.txtScore182.DataField = "ITEM19";
			this.txtScore182.Height = 0.2393701F;
			this.txtScore182.Left = 3.311024F;
			this.txtScore182.MultiLine = false;
			this.txtScore182.Name = "txtScore182";
			this.txtScore182.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore182.Text = "019";
			this.txtScore182.Top = 0.7181104F;
			this.txtScore182.Width = 1.213386F;
			// 
			// txtScore178
			// 
			this.txtScore178.DataField = "ITEM15";
			this.txtScore178.Height = 0.2393701F;
			this.txtScore178.Left = 4.573229F;
			this.txtScore178.MultiLine = false;
			this.txtScore178.Name = "txtScore178";
			this.txtScore178.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore178.Text = "015";
			this.txtScore178.Top = 0.4787401F;
			this.txtScore178.Width = 1.213779F;
			// 
			// txtScore183
			// 
			this.txtScore183.DataField = "ITEM20";
			this.txtScore183.Height = 0.2393701F;
			this.txtScore183.Left = 4.572441F;
			this.txtScore183.MultiLine = false;
			this.txtScore183.Name = "txtScore183";
			this.txtScore183.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore183.Text = "020";
			this.txtScore183.Top = 0.7181104F;
			this.txtScore183.Width = 1.21496F;
			// 
			// txtScore185
			// 
			this.txtScore185.DataField = "ITEM22";
			this.txtScore185.Height = 0.2393701F;
			this.txtScore185.Left = 0.07795276F;
			this.txtScore185.MultiLine = false;
			this.txtScore185.Name = "txtScore185";
			this.txtScore185.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore185.Text = "022";
			this.txtScore185.Top = 0.9574803F;
			this.txtScore185.Width = 1.968898F;
			// 
			// txtScore186
			// 
			this.txtScore186.DataField = "ITEM23";
			this.txtScore186.Height = 0.2393701F;
			this.txtScore186.Left = 2.046457F;
			this.txtScore186.MultiLine = false;
			this.txtScore186.Name = "txtScore186";
			this.txtScore186.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore186.Text = "023";
			this.txtScore186.Top = 0.9574805F;
			this.txtScore186.Width = 1.210237F;
			// 
			// txtScore187
			// 
			this.txtScore187.DataField = "ITEM24";
			this.txtScore187.Height = 0.2393701F;
			this.txtScore187.Left = 3.311024F;
			this.txtScore187.MultiLine = false;
			this.txtScore187.Name = "txtScore187";
			this.txtScore187.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore187.Text = "024";
			this.txtScore187.Top = 0.9574805F;
			this.txtScore187.Width = 1.213386F;
			// 
			// txtScore188
			// 
			this.txtScore188.DataField = "ITEM25";
			this.txtScore188.Height = 0.2393701F;
			this.txtScore188.Left = 4.572441F;
			this.txtScore188.MultiLine = false;
			this.txtScore188.Name = "txtScore188";
			this.txtScore188.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore188.Text = "025";
			this.txtScore188.Top = 0.9574805F;
			this.txtScore188.Width = 1.21496F;
			// 
			// txtScore184
			// 
			this.txtScore184.DataField = "ITEM21";
			this.txtScore184.Height = 0.2393701F;
			this.txtScore184.Left = 5.842126F;
			this.txtScore184.MultiLine = false;
			this.txtScore184.Name = "txtScore184";
			this.txtScore184.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore184.Text = "021";
			this.txtScore184.Top = 0.7181104F;
			this.txtScore184.Width = 1.201575F;
			// 
			// txtScore189
			// 
			this.txtScore189.DataField = "ITEM26";
			this.txtScore189.Height = 0.2393701F;
			this.txtScore189.Left = 5.842127F;
			this.txtScore189.MultiLine = false;
			this.txtScore189.Name = "txtScore189";
			this.txtScore189.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore189.Text = "026";
			this.txtScore189.Top = 0.9574805F;
			this.txtScore189.Width = 1.201574F;
			// 
			// txtScore191
			// 
			this.txtScore191.DataField = "ITEM28";
			this.txtScore191.Height = 0.2393701F;
			this.txtScore191.Left = 2.046457F;
			this.txtScore191.MultiLine = false;
			this.txtScore191.Name = "txtScore191";
			this.txtScore191.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore191.Text = "028";
			this.txtScore191.Top = 1.196851F;
			this.txtScore191.Width = 1.210236F;
			// 
			// txtScore192
			// 
			this.txtScore192.DataField = "ITEM29";
			this.txtScore192.Height = 0.2393701F;
			this.txtScore192.Left = 3.311024F;
			this.txtScore192.MultiLine = false;
			this.txtScore192.Name = "txtScore192";
			this.txtScore192.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore192.Text = "029";
			this.txtScore192.Top = 1.196851F;
			this.txtScore192.Width = 1.213386F;
			// 
			// txtScore193
			// 
			this.txtScore193.DataField = "ITEM30";
			this.txtScore193.Height = 0.2393701F;
			this.txtScore193.Left = 4.572441F;
			this.txtScore193.MultiLine = false;
			this.txtScore193.Name = "txtScore193";
			this.txtScore193.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore193.Text = "030";
			this.txtScore193.Top = 1.196851F;
			this.txtScore193.Width = 1.214566F;
			// 
			// txtScore194
			// 
			this.txtScore194.DataField = "ITEM31";
			this.txtScore194.Height = 0.2393701F;
			this.txtScore194.Left = 5.842126F;
			this.txtScore194.MultiLine = false;
			this.txtScore194.Name = "txtScore194";
			this.txtScore194.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore194.Text = "031";
			this.txtScore194.Top = 1.196851F;
			this.txtScore194.Width = 1.201575F;
			// 
			// txtScore190
			// 
			this.txtScore190.DataField = "ITEM27";
			this.txtScore190.Height = 0.2393701F;
			this.txtScore190.Left = 0.07795276F;
			this.txtScore190.MultiLine = false;
			this.txtScore190.Name = "txtScore190";
			this.txtScore190.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore190.Text = "027";
			this.txtScore190.Top = 1.196851F;
			this.txtScore190.Width = 1.968898F;
			// 
			// txtScore195
			// 
			this.txtScore195.DataField = "ITEM32";
			this.txtScore195.Height = 0.2393701F;
			this.txtScore195.Left = 0.07795276F;
			this.txtScore195.MultiLine = false;
			this.txtScore195.Name = "txtScore195";
			this.txtScore195.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore195.Text = "032";
			this.txtScore195.Top = 1.43622F;
			this.txtScore195.Width = 1.968898F;
			// 
			// txtScore197
			// 
			this.txtScore197.DataField = "ITEM34";
			this.txtScore197.Height = 0.2393701F;
			this.txtScore197.Left = 3.311418F;
			this.txtScore197.MultiLine = false;
			this.txtScore197.Name = "txtScore197";
			this.txtScore197.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore197.Text = "034";
			this.txtScore197.Top = 1.43622F;
			this.txtScore197.Width = 1.213386F;
			// 
			// txtScore198
			// 
			this.txtScore198.DataField = "ITEM35";
			this.txtScore198.Height = 0.2393701F;
			this.txtScore198.Left = 4.572441F;
			this.txtScore198.MultiLine = false;
			this.txtScore198.Name = "txtScore198";
			this.txtScore198.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore198.Text = "035";
			this.txtScore198.Top = 1.43622F;
			this.txtScore198.Width = 1.214566F;
			// 
			// txtScore199
			// 
			this.txtScore199.DataField = "ITEM36";
			this.txtScore199.Height = 0.2393701F;
			this.txtScore199.Left = 5.842126F;
			this.txtScore199.MultiLine = false;
			this.txtScore199.Name = "txtScore199";
			this.txtScore199.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore199.Text = "036";
			this.txtScore199.Top = 1.43622F;
			this.txtScore199.Width = 1.201575F;
			// 
			// txtScore200
			// 
			this.txtScore200.DataField = "ITEM37";
			this.txtScore200.Height = 0.2393701F;
			this.txtScore200.Left = 0.07795276F;
			this.txtScore200.MultiLine = false;
			this.txtScore200.Name = "txtScore200";
			this.txtScore200.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore200.Text = "037";
			this.txtScore200.Top = 1.675591F;
			this.txtScore200.Width = 1.968504F;
			// 
			// txtScore196
			// 
			this.txtScore196.DataField = "ITEM33";
			this.txtScore196.Height = 0.2393701F;
			this.txtScore196.Left = 2.046457F;
			this.txtScore196.MultiLine = false;
			this.txtScore196.Name = "txtScore196";
			this.txtScore196.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore196.Text = "033";
			this.txtScore196.Top = 1.43622F;
			this.txtScore196.Width = 1.210237F;
			// 
			// txtScore201
			// 
			this.txtScore201.DataField = "ITEM38";
			this.txtScore201.Height = 0.2393701F;
			this.txtScore201.Left = 2.046457F;
			this.txtScore201.MultiLine = false;
			this.txtScore201.Name = "txtScore201";
			this.txtScore201.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore201.Text = "038";
			this.txtScore201.Top = 1.675591F;
			this.txtScore201.Width = 1.210236F;
			// 
			// txtScore203
			// 
			this.txtScore203.DataField = "ITEM40";
			this.txtScore203.Height = 0.2393701F;
			this.txtScore203.Left = 4.572835F;
			this.txtScore203.MultiLine = false;
			this.txtScore203.Name = "txtScore203";
			this.txtScore203.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore203.Text = "040";
			this.txtScore203.Top = 1.675591F;
			this.txtScore203.Width = 1.214173F;
			// 
			// txtScore204
			// 
			this.txtScore204.DataField = "ITEM41";
			this.txtScore204.Height = 0.2393701F;
			this.txtScore204.Left = 5.842126F;
			this.txtScore204.MultiLine = false;
			this.txtScore204.Name = "txtScore204";
			this.txtScore204.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore204.Text = "041";
			this.txtScore204.Top = 1.675591F;
			this.txtScore204.Width = 1.201575F;
			// 
			// txtScore205
			// 
			this.txtScore205.DataField = "ITEM42";
			this.txtScore205.Height = 0.2393701F;
			this.txtScore205.Left = 0.07795276F;
			this.txtScore205.MultiLine = false;
			this.txtScore205.Name = "txtScore205";
			this.txtScore205.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore205.Text = "042";
			this.txtScore205.Top = 1.914961F;
			this.txtScore205.Width = 1.968504F;
			// 
			// txtScore206
			// 
			this.txtScore206.DataField = "ITEM43";
			this.txtScore206.Height = 0.2393701F;
			this.txtScore206.Left = 2.046457F;
			this.txtScore206.MultiLine = false;
			this.txtScore206.Name = "txtScore206";
			this.txtScore206.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore206.Text = "043";
			this.txtScore206.Top = 1.914961F;
			this.txtScore206.Width = 1.210236F;
			// 
			// txtScore202
			// 
			this.txtScore202.DataField = "ITEM39";
			this.txtScore202.Height = 0.2393701F;
			this.txtScore202.Left = 3.311024F;
			this.txtScore202.MultiLine = false;
			this.txtScore202.Name = "txtScore202";
			this.txtScore202.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore202.Text = "039";
			this.txtScore202.Top = 1.675591F;
			this.txtScore202.Width = 1.213386F;
			// 
			// txtScore207
			// 
			this.txtScore207.DataField = "ITEM44";
			this.txtScore207.Height = 0.2393701F;
			this.txtScore207.Left = 3.311418F;
			this.txtScore207.MultiLine = false;
			this.txtScore207.Name = "txtScore207";
			this.txtScore207.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore207.Text = "044";
			this.txtScore207.Top = 1.914961F;
			this.txtScore207.Width = 1.212992F;
			// 
			// txtScore209
			// 
			this.txtScore209.DataField = "ITEM46";
			this.txtScore209.Height = 0.2393701F;
			this.txtScore209.Left = 5.84252F;
			this.txtScore209.MultiLine = false;
			this.txtScore209.Name = "txtScore209";
			this.txtScore209.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore209.Text = "046";
			this.txtScore209.Top = 1.914961F;
			this.txtScore209.Width = 1.201181F;
			// 
			// txtScore210
			// 
			this.txtScore210.DataField = "ITEM47";
			this.txtScore210.Height = 0.2393701F;
			this.txtScore210.Left = 0.07795276F;
			this.txtScore210.MultiLine = false;
			this.txtScore210.Name = "txtScore210";
			this.txtScore210.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore210.Text = "047";
			this.txtScore210.Top = 2.154331F;
			this.txtScore210.Width = 1.968898F;
			// 
			// txtScore211
			// 
			this.txtScore211.DataField = "ITEM48";
			this.txtScore211.Height = 0.2393701F;
			this.txtScore211.Left = 2.046457F;
			this.txtScore211.MultiLine = false;
			this.txtScore211.Name = "txtScore211";
			this.txtScore211.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore211.Text = "048";
			this.txtScore211.Top = 2.154331F;
			this.txtScore211.Width = 1.210236F;
			// 
			// txtScore212
			// 
			this.txtScore212.DataField = "ITEM49";
			this.txtScore212.Height = 0.2393701F;
			this.txtScore212.Left = 3.311024F;
			this.txtScore212.MultiLine = false;
			this.txtScore212.Name = "txtScore212";
			this.txtScore212.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore212.Text = "049";
			this.txtScore212.Top = 2.154331F;
			this.txtScore212.Width = 1.212599F;
			// 
			// txtScore208
			// 
			this.txtScore208.DataField = "ITEM45";
			this.txtScore208.Height = 0.2393701F;
			this.txtScore208.Left = 4.572441F;
			this.txtScore208.MultiLine = false;
			this.txtScore208.Name = "txtScore208";
			this.txtScore208.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore208.Text = "045";
			this.txtScore208.Top = 1.914961F;
			this.txtScore208.Width = 1.214567F;
			// 
			// txtScore213
			// 
			this.txtScore213.DataField = "ITEM50";
			this.txtScore213.Height = 0.2393701F;
			this.txtScore213.Left = 4.571654F;
			this.txtScore213.MultiLine = false;
			this.txtScore213.Name = "txtScore213";
			this.txtScore213.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore213.Text = "050";
			this.txtScore213.Top = 2.154331F;
			this.txtScore213.Width = 1.215354F;
			// 
			// txtScore215
			// 
			this.txtScore215.DataField = "ITEM52";
			this.txtScore215.Height = 0.2393701F;
			this.txtScore215.Left = 0.07795276F;
			this.txtScore215.MultiLine = false;
			this.txtScore215.Name = "txtScore215";
			this.txtScore215.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore215.Text = "ITEM52";
			this.txtScore215.Top = 2.393701F;
			this.txtScore215.Width = 1.968504F;
			// 
			// txtScore216
			// 
			this.txtScore216.DataField = "ITEM53";
			this.txtScore216.Height = 0.2393701F;
			this.txtScore216.Left = 2.046457F;
			this.txtScore216.MultiLine = false;
			this.txtScore216.Name = "txtScore216";
			this.txtScore216.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore216.Text = "053";
			this.txtScore216.Top = 2.393701F;
			this.txtScore216.Width = 1.211024F;
			// 
			// txtScore217
			// 
			this.txtScore217.DataField = "ITEM54";
			this.txtScore217.Height = 0.2393701F;
			this.txtScore217.Left = 3.311418F;
			this.txtScore217.MultiLine = false;
			this.txtScore217.Name = "txtScore217";
			this.txtScore217.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore217.Text = "054";
			this.txtScore217.Top = 2.393701F;
			this.txtScore217.Width = 1.212992F;
			// 
			// txtScore218
			// 
			this.txtScore218.DataField = "ITEM55";
			this.txtScore218.Height = 0.2393701F;
			this.txtScore218.Left = 4.572441F;
			this.txtScore218.MultiLine = false;
			this.txtScore218.Name = "txtScore218";
			this.txtScore218.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore218.Text = "055";
			this.txtScore218.Top = 2.393701F;
			this.txtScore218.Width = 1.214567F;
			// 
			// txtScore214
			// 
			this.txtScore214.DataField = "ITEM51";
			this.txtScore214.Height = 0.2393701F;
			this.txtScore214.Left = 5.842126F;
			this.txtScore214.MultiLine = false;
			this.txtScore214.Name = "txtScore214";
			this.txtScore214.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore214.Text = "051";
			this.txtScore214.Top = 2.154331F;
			this.txtScore214.Width = 1.201575F;
			// 
			// txtScore219
			// 
			this.txtScore219.DataField = "ITEM56";
			this.txtScore219.Height = 0.2393701F;
			this.txtScore219.Left = 5.84252F;
			this.txtScore219.MultiLine = false;
			this.txtScore219.Name = "txtScore219";
			this.txtScore219.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore219.Text = "056,456,789,000";
			this.txtScore219.Top = 2.393701F;
			this.txtScore219.Width = 1.201181F;
			// 
			// txtScore220
			// 
			this.txtScore220.DataField = "ITEM57";
			this.txtScore220.Height = 0.2393701F;
			this.txtScore220.Left = 0.07795276F;
			this.txtScore220.MultiLine = false;
			this.txtScore220.Name = "txtScore220";
			this.txtScore220.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore220.Text = "ITEM57";
			this.txtScore220.Top = 2.633071F;
			this.txtScore220.Width = 1.968898F;
			// 
			// txtScore222
			// 
			this.txtScore222.DataField = "ITEM59";
			this.txtScore222.Height = 0.2393701F;
			this.txtScore222.Left = 3.310631F;
			this.txtScore222.MultiLine = false;
			this.txtScore222.Name = "txtScore222";
			this.txtScore222.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore222.Text = "059";
			this.txtScore222.Top = 2.633071F;
			this.txtScore222.Width = 1.213386F;
			// 
			// txtScore223
			// 
			this.txtScore223.DataField = "ITEM60";
			this.txtScore223.Height = 0.2393701F;
			this.txtScore223.Left = 4.572047F;
			this.txtScore223.MultiLine = false;
			this.txtScore223.Name = "txtScore223";
			this.txtScore223.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore223.Text = "060";
			this.txtScore223.Top = 2.633071F;
			this.txtScore223.Width = 1.21496F;
			// 
			// txtScore224
			// 
			this.txtScore224.DataField = "ITEM61";
			this.txtScore224.Height = 0.2393701F;
			this.txtScore224.Left = 5.841733F;
			this.txtScore224.MultiLine = false;
			this.txtScore224.Name = "txtScore224";
			this.txtScore224.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore224.Text = "061";
			this.txtScore224.Top = 2.633071F;
			this.txtScore224.Width = 1.201575F;
			// 
			// txtScore221
			// 
			this.txtScore221.DataField = "ITEM58";
			this.txtScore221.Height = 0.2393701F;
			this.txtScore221.Left = 2.046064F;
			this.txtScore221.MultiLine = false;
			this.txtScore221.Name = "txtScore221";
			this.txtScore221.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore221.Text = "058";
			this.txtScore221.Top = 2.633071F;
			this.txtScore221.Width = 1.211417F;
			// 
			// line36
			// 
			this.line36.Height = 3.111811F;
			this.line36.Left = 7.086614F;
			this.line36.LineWeight = 1F;
			this.line36.Name = "line36";
			this.line36.Top = 0F;
			this.line36.Width = 0F;
			this.line36.X1 = 7.086614F;
			this.line36.X2 = 7.086614F;
			this.line36.Y1 = 3.111811F;
			this.line36.Y2 = 0F;
			// 
			// txtScore227
			// 
			this.txtScore227.DataField = "ITEM64";
			this.txtScore227.Height = 0.2393701F;
			this.txtScore227.Left = 3.312599F;
			this.txtScore227.MultiLine = false;
			this.txtScore227.Name = "txtScore227";
			this.txtScore227.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore227.Text = "064";
			this.txtScore227.Top = 2.872441F;
			this.txtScore227.Width = 1.212599F;
			// 
			// txtScore228
			// 
			this.txtScore228.DataField = "ITEM65";
			this.txtScore228.Height = 0.2393701F;
			this.txtScore228.Left = 4.577167F;
			this.txtScore228.MultiLine = false;
			this.txtScore228.Name = "txtScore228";
			this.txtScore228.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore228.Text = "065";
			this.txtScore228.Top = 2.872441F;
			this.txtScore228.Width = 1.215353F;
			// 
			// txtScore226
			// 
			this.txtScore226.DataField = "ITEM63";
			this.txtScore226.Height = 0.2393701F;
			this.txtScore226.Left = 2.046457F;
			this.txtScore226.MultiLine = false;
			this.txtScore226.Name = "txtScore226";
			this.txtScore226.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore226.Text = "063";
			this.txtScore226.Top = 2.872441F;
			this.txtScore226.Width = 1.210236F;
			// 
			// line46
			// 
			this.line46.Height = 3.111811F;
			this.line46.Left = 0.007874016F;
			this.line46.LineWeight = 1F;
			this.line46.Name = "line46";
			this.line46.Top = 0F;
			this.line46.Width = 0F;
			this.line46.X1 = 0.007874016F;
			this.line46.X2 = 0.007874016F;
			this.line46.Y1 = 3.111811F;
			this.line46.Y2 = 0F;
			// 
			// line35
			// 
			this.line35.Height = 0F;
			this.line35.Left = 0.007874489F;
			this.line35.LineWeight = 1F;
			this.line35.Name = "line35";
			this.line35.Top = 3.111811F;
			this.line35.Width = 7.07874F;
			this.line35.X1 = 7.086614F;
			this.line35.X2 = 0.007874489F;
			this.line35.Y1 = 3.111811F;
			this.line35.Y2 = 3.111811F;
			// 
			// line187
			// 
			this.line187.Height = 3.111811F;
			this.line187.Left = 2.044095F;
			this.line187.LineWeight = 1F;
			this.line187.Name = "line187";
			this.line187.Top = 0F;
			this.line187.Width = 0F;
			this.line187.X1 = 2.044095F;
			this.line187.X2 = 2.044095F;
			this.line187.Y1 = 3.111811F;
			this.line187.Y2 = 0F;
			// 
			// line188
			// 
			this.line188.Height = 3.111811F;
			this.line188.Left = 3.312599F;
			this.line188.LineWeight = 1F;
			this.line188.Name = "line188";
			this.line188.Top = 0F;
			this.line188.Width = 0F;
			this.line188.X1 = 3.312599F;
			this.line188.X2 = 3.312599F;
			this.line188.Y1 = 3.111811F;
			this.line188.Y2 = 0F;
			// 
			// line189
			// 
			this.line189.Height = 3.111811F;
			this.line189.Left = 4.577166F;
			this.line189.LineWeight = 1F;
			this.line189.Name = "line189";
			this.line189.Top = 0F;
			this.line189.Width = 0F;
			this.line189.X1 = 4.577166F;
			this.line189.X2 = 4.577166F;
			this.line189.Y1 = 3.111811F;
			this.line189.Y2 = 0F;
			// 
			// line190
			// 
			this.line190.Height = 3.111811F;
			this.line190.Left = 5.846064F;
			this.line190.LineWeight = 1F;
			this.line190.Name = "line190";
			this.line190.Top = 0F;
			this.line190.Width = 0F;
			this.line190.X1 = 5.846064F;
			this.line190.X2 = 5.846064F;
			this.line190.Y1 = 3.111811F;
			this.line190.Y2 = 0F;
			// 
			// line191
			// 
			this.line191.Height = 0F;
			this.line191.Left = 0.007874016F;
			this.line191.LineWeight = 1F;
			this.line191.Name = "line191";
			this.line191.Top = 0.2393701F;
			this.line191.Width = 7.07874F;
			this.line191.X1 = 7.086614F;
			this.line191.X2 = 0.007874016F;
			this.line191.Y1 = 0.2393701F;
			this.line191.Y2 = 0.2393701F;
			// 
			// line192
			// 
			this.line192.Height = 0F;
			this.line192.Left = 0.007874016F;
			this.line192.LineWeight = 1F;
			this.line192.Name = "line192";
			this.line192.Top = 0.4787402F;
			this.line192.Width = 7.07874F;
			this.line192.X1 = 7.086614F;
			this.line192.X2 = 0.007874016F;
			this.line192.Y1 = 0.4787402F;
			this.line192.Y2 = 0.4787402F;
			// 
			// line193
			// 
			this.line193.Height = 0F;
			this.line193.Left = 0.007874016F;
			this.line193.LineWeight = 1F;
			this.line193.Name = "line193";
			this.line193.Top = 0.7181103F;
			this.line193.Width = 7.07874F;
			this.line193.X1 = 7.086614F;
			this.line193.X2 = 0.007874016F;
			this.line193.Y1 = 0.7181103F;
			this.line193.Y2 = 0.7181103F;
			// 
			// line194
			// 
			this.line194.Height = 0F;
			this.line194.Left = 0.007874016F;
			this.line194.LineWeight = 1F;
			this.line194.Name = "line194";
			this.line194.Top = 0.9574804F;
			this.line194.Width = 7.07874F;
			this.line194.X1 = 7.086614F;
			this.line194.X2 = 0.007874016F;
			this.line194.Y1 = 0.9574804F;
			this.line194.Y2 = 0.9574804F;
			// 
			// line195
			// 
			this.line195.Height = 0F;
			this.line195.Left = 0.007874016F;
			this.line195.LineWeight = 1F;
			this.line195.Name = "line195";
			this.line195.Top = 1.19685F;
			this.line195.Width = 7.07874F;
			this.line195.X1 = 7.086614F;
			this.line195.X2 = 0.007874016F;
			this.line195.Y1 = 1.19685F;
			this.line195.Y2 = 1.19685F;
			// 
			// line196
			// 
			this.line196.Height = 0F;
			this.line196.Left = 0.007874016F;
			this.line196.LineWeight = 1F;
			this.line196.Name = "line196";
			this.line196.Top = 1.436221F;
			this.line196.Width = 7.07874F;
			this.line196.X1 = 7.086614F;
			this.line196.X2 = 0.007874016F;
			this.line196.Y1 = 1.436221F;
			this.line196.Y2 = 1.436221F;
			// 
			// line197
			// 
			this.line197.Height = 0F;
			this.line197.Left = 0.007874016F;
			this.line197.LineWeight = 1F;
			this.line197.Name = "line197";
			this.line197.Top = 1.675591F;
			this.line197.Width = 7.07874F;
			this.line197.X1 = 7.086614F;
			this.line197.X2 = 0.007874016F;
			this.line197.Y1 = 1.675591F;
			this.line197.Y2 = 1.675591F;
			// 
			// line198
			// 
			this.line198.Height = 0F;
			this.line198.Left = 0.007874016F;
			this.line198.LineWeight = 1F;
			this.line198.Name = "line198";
			this.line198.Top = 2.633071F;
			this.line198.Width = 7.07874F;
			this.line198.X1 = 7.086614F;
			this.line198.X2 = 0.007874016F;
			this.line198.Y1 = 2.633071F;
			this.line198.Y2 = 2.633071F;
			// 
			// line199
			// 
			this.line199.Height = 0F;
			this.line199.Left = 0.007874016F;
			this.line199.LineWeight = 1F;
			this.line199.Name = "line199";
			this.line199.Top = 2.872441F;
			this.line199.Width = 7.07874F;
			this.line199.X1 = 7.086614F;
			this.line199.X2 = 0.007874016F;
			this.line199.Y1 = 2.872441F;
			this.line199.Y2 = 2.872441F;
			// 
			// line200
			// 
			this.line200.Height = 0F;
			this.line200.Left = 0.007874016F;
			this.line200.LineWeight = 1F;
			this.line200.Name = "line200";
			this.line200.Top = 2.393701F;
			this.line200.Width = 7.07874F;
			this.line200.X1 = 7.086614F;
			this.line200.X2 = 0.007874016F;
			this.line200.Y1 = 2.393701F;
			this.line200.Y2 = 2.393701F;
			// 
			// line201
			// 
			this.line201.Height = 0F;
			this.line201.Left = 0.007874016F;
			this.line201.LineWeight = 1F;
			this.line201.Name = "line201";
			this.line201.Top = 2.154331F;
			this.line201.Width = 7.07874F;
			this.line201.X1 = 7.086614F;
			this.line201.X2 = 0.007874016F;
			this.line201.Y1 = 2.154331F;
			this.line201.Y2 = 2.154331F;
			// 
			// line202
			// 
			this.line202.Height = 0F;
			this.line202.Left = 0.007874016F;
			this.line202.LineWeight = 1F;
			this.line202.Name = "line202";
			this.line202.Top = 1.914961F;
			this.line202.Width = 7.07874F;
			this.line202.X1 = 7.086614F;
			this.line202.X2 = 0.007874016F;
			this.line202.Y1 = 1.914961F;
			this.line202.Y2 = 1.914961F;
			// 
			// group04_H
			// 
			this.group04_H.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox25,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.textBox76,
            this.textBox132,
            this.textBox133,
            this.line37,
            this.line38,
            this.line39,
            this.line40,
            this.line41,
            this.line42,
            this.line43,
            this.line94});
			this.group04_H.DataField = "ITEM01";
			this.group04_H.Height = 0.6822835F;
			this.group04_H.Name = "group04_H";
			this.group04_H.Format += new System.EventHandler(this.group04_Format);
			// 
			// textBox25
			// 
			this.textBox25.Height = 0.4440945F;
			this.textBox25.Left = 0.007874016F;
			this.textBox25.MultiLine = false;
			this.textBox25.Name = "textBox25";
			this.textBox25.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: justify; vertical-align: middle";
			this.textBox25.Text = null;
			this.textBox25.Top = 0.238189F;
			this.textBox25.Width = 7.086614F;
			// 
			// textBox73
			// 
			this.textBox73.Height = 0.2389764F;
			this.textBox73.Left = 0.02716541F;
			this.textBox73.MultiLine = false;
			this.textBox73.Name = "textBox73";
			this.textBox73.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: justify; ve" +
    "rtical-align: middle";
			this.textBox73.Text = "（４）その他の流動資産";
			this.textBox73.Top = 0F;
			this.textBox73.Width = 4.237402F;
			// 
			// textBox74
			// 
			this.textBox74.Height = 0.4429134F;
			this.textBox74.Left = 0F;
			this.textBox74.MultiLine = false;
			this.textBox74.Name = "textBox74";
			this.textBox74.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; te" +
    "xt-justify: auto; vertical-align: middle";
			this.textBox74.Text = "科目及び部門";
			this.textBox74.Top = 0.238189F;
			this.textBox74.Width = 2.040158F;
			// 
			// textBox75
			// 
			this.textBox75.Height = 0.4429134F;
			this.textBox75.Left = 2.040158F;
			this.textBox75.MultiLine = false;
			this.textBox75.Name = "textBox75";
			this.textBox75.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox75.Text = "前年度末残高";
			this.textBox75.Top = 0.2393701F;
			this.textBox75.Width = 1.27126F;
			// 
			// textBox76
			// 
			this.textBox76.Height = 0.4440945F;
			this.textBox76.Left = 3.309449F;
			this.textBox76.MultiLine = false;
			this.textBox76.Name = "textBox76";
			this.textBox76.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox76.Text = "本年度増加額";
			this.textBox76.Top = 0.238189F;
			this.textBox76.Width = 1.265354F;
			// 
			// textBox132
			// 
			this.textBox132.Height = 0.4440945F;
			this.textBox132.Left = 4.574803F;
			this.textBox132.MultiLine = false;
			this.textBox132.Name = "textBox132";
			this.textBox132.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox132.Text = "本年度減少額";
			this.textBox132.Top = 0.238189F;
			this.textBox132.Width = 1.267718F;
			// 
			// textBox133
			// 
			this.textBox133.Height = 0.4433071F;
			this.textBox133.Left = 5.83504F;
			this.textBox133.MultiLine = false;
			this.textBox133.Name = "textBox133";
			this.textBox133.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox133.Text = "本年度末残高";
			this.textBox133.Top = 0.2377953F;
			this.textBox133.Width = 1.246457F;
			// 
			// line37
			// 
			this.line37.Height = 0F;
			this.line37.Left = 0.007874016F;
			this.line37.LineWeight = 1F;
			this.line37.Name = "line37";
			this.line37.Top = 0.2389764F;
			this.line37.Width = 7.07874F;
			this.line37.X1 = 7.086614F;
			this.line37.X2 = 0.007874016F;
			this.line37.Y1 = 0.2389764F;
			this.line37.Y2 = 0.2389764F;
			// 
			// line38
			// 
			this.line38.Height = 0F;
			this.line38.Left = 0.007874016F;
			this.line38.LineWeight = 1F;
			this.line38.Name = "line38";
			this.line38.Top = 0.6822835F;
			this.line38.Width = 7.07874F;
			this.line38.X1 = 7.086614F;
			this.line38.X2 = 0.007874016F;
			this.line38.Y1 = 0.6822835F;
			this.line38.Y2 = 0.6822835F;
			// 
			// line39
			// 
			this.line39.Height = 0.4440945F;
			this.line39.Left = 5.846063F;
			this.line39.LineWeight = 1F;
			this.line39.Name = "line39";
			this.line39.Top = 0.238189F;
			this.line39.Width = 0F;
			this.line39.X1 = 5.846063F;
			this.line39.X2 = 5.846063F;
			this.line39.Y1 = 0.238189F;
			this.line39.Y2 = 0.6822835F;
			// 
			// line40
			// 
			this.line40.Height = 0.4440945F;
			this.line40.Left = 4.571654F;
			this.line40.LineWeight = 1F;
			this.line40.Name = "line40";
			this.line40.Top = 0.238189F;
			this.line40.Width = 0F;
			this.line40.X1 = 4.571654F;
			this.line40.X2 = 4.571654F;
			this.line40.Y1 = 0.238189F;
			this.line40.Y2 = 0.6822835F;
			// 
			// line41
			// 
			this.line41.Height = 0.4437008F;
			this.line41.Left = 3.315354F;
			this.line41.LineWeight = 1F;
			this.line41.Name = "line41";
			this.line41.Top = 0.2385827F;
			this.line41.Width = 0F;
			this.line41.X1 = 3.315354F;
			this.line41.X2 = 3.315354F;
			this.line41.Y1 = 0.2385827F;
			this.line41.Y2 = 0.6822835F;
			// 
			// line42
			// 
			this.line42.Height = 0.4429134F;
			this.line42.Left = 2.040158F;
			this.line42.LineWeight = 1F;
			this.line42.Name = "line42";
			this.line42.Top = 0.2393701F;
			this.line42.Width = 0F;
			this.line42.X1 = 2.040158F;
			this.line42.X2 = 2.040158F;
			this.line42.Y1 = 0.2393701F;
			this.line42.Y2 = 0.6822835F;
			// 
			// line43
			// 
			this.line43.Height = 0.4433071F;
			this.line43.Left = 7.086614F;
			this.line43.LineWeight = 1F;
			this.line43.Name = "line43";
			this.line43.Top = 0.2389764F;
			this.line43.Width = 0F;
			this.line43.X1 = 7.086614F;
			this.line43.X2 = 7.086614F;
			this.line43.Y1 = 0.2389764F;
			this.line43.Y2 = 0.6822835F;
			// 
			// line94
			// 
			this.line94.Height = 0.4440944F;
			this.line94.Left = 0.007874016F;
			this.line94.LineWeight = 1F;
			this.line94.Name = "line94";
			this.line94.Top = 0.238189F;
			this.line94.Width = 9.313226E-10F;
			this.line94.X1 = 0.007874017F;
			this.line94.X2 = 0.007874016F;
			this.line94.Y1 = 0.238189F;
			this.line94.Y2 = 0.6822834F;
			// 
			// group04_F
			// 
			this.group04_F.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtScore294,
            this.txtScore230,
            this.txtScore232,
            this.txtScore233,
            this.txtScore234,
            this.txtScore235,
            this.txtScore231,
            this.txtScore236,
            this.txtScore238,
            this.txtScore239,
            this.txtScore240,
            this.txtScore241,
            this.txtScore237,
            this.txtScore242,
            this.txtScore244,
            this.txtScore245,
            this.txtScore246,
            this.txtScore247,
            this.txtScore243,
            this.txtScore248,
            this.txtScore250,
            this.txtScore251,
            this.txtScore252,
            this.txtScore253,
            this.txtScore249,
            this.txtScore254,
            this.txtScore256,
            this.txtScore257,
            this.txtScore258,
            this.txtScore259,
            this.txtScore255,
            this.txtScore260,
            this.txtScore262,
            this.txtScore263,
            this.txtScore264,
            this.txtScore265,
            this.txtScore261,
            this.txtScore266,
            this.txtScore268,
            this.txtScore269,
            this.txtScore270,
            this.txtScore271,
            this.txtScore267,
            this.txtScore272,
            this.txtScore274,
            this.txtScore275,
            this.txtScore276,
            this.txtScore277,
            this.txtScore273,
            this.txtScore278,
            this.txtScore280,
            this.txtScore281,
            this.txtScore282,
            this.txtScore283,
            this.txtScore279,
            this.txtScore284,
            this.txtScore285,
            this.txtScore287,
            this.txtScore288,
            this.txtScore289,
            this.txtScore286,
            this.line45,
            this.txtScore290,
            this.txtScore292,
            this.txtScore293,
            this.txtScore291,
            this.line44,
            this.line47,
            this.line160,
            this.line171,
            this.line172,
            this.line173,
            this.line175,
            this.line176,
            this.line177,
            this.line178,
            this.line179,
            this.line180,
            this.line181,
            this.line182,
            this.line183,
            this.line184,
            this.line185,
            this.line186});
			this.group04_F.Height = 3.179462F;
			this.group04_F.KeepTogether = true;
			this.group04_F.Name = "group04_F";
			// 
			// txtScore294
			// 
			this.txtScore294.DataField = "ITEM66";
			this.txtScore294.Height = 0.2393701F;
			this.txtScore294.Left = 5.838977F;
			this.txtScore294.MultiLine = false;
			this.txtScore294.Name = "txtScore294";
			this.txtScore294.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore294.Text = "066";
			this.txtScore294.Top = 2.872441F;
			this.txtScore294.Width = 1.195276F;
			// 
			// txtScore230
			// 
			this.txtScore230.DataField = "ITEM02";
			this.txtScore230.Height = 0.2393701F;
			this.txtScore230.Left = 0.06811024F;
			this.txtScore230.MultiLine = false;
			this.txtScore230.Name = "txtScore230";
			this.txtScore230.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore230.Text = "ITEM02";
			this.txtScore230.Top = 0F;
			this.txtScore230.Width = 1.979134F;
			// 
			// txtScore232
			// 
			this.txtScore232.DataField = "ITEM04";
			this.txtScore232.Height = 0.2393701F;
			this.txtScore232.Left = 3.311024F;
			this.txtScore232.MultiLine = false;
			this.txtScore232.Name = "txtScore232";
			this.txtScore232.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore232.Text = "004";
			this.txtScore232.Top = 0F;
			this.txtScore232.Width = 1.213386F;
			// 
			// txtScore233
			// 
			this.txtScore233.DataField = "ITEM05";
			this.txtScore233.Height = 0.2393701F;
			this.txtScore233.Left = 4.572441F;
			this.txtScore233.MultiLine = false;
			this.txtScore233.Name = "txtScore233";
			this.txtScore233.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore233.Text = "005";
			this.txtScore233.Top = 0F;
			this.txtScore233.Width = 1.203937F;
			// 
			// txtScore234
			// 
			this.txtScore234.DataField = "ITEM06";
			this.txtScore234.Height = 0.2393701F;
			this.txtScore234.Left = 5.842127F;
			this.txtScore234.MultiLine = false;
			this.txtScore234.Name = "txtScore234";
			this.txtScore234.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore234.Text = "006";
			this.txtScore234.Top = 0F;
			this.txtScore234.Width = 1.201574F;
			// 
			// txtScore235
			// 
			this.txtScore235.DataField = "ITEM07";
			this.txtScore235.Height = 0.2393701F;
			this.txtScore235.Left = 0.06811024F;
			this.txtScore235.MultiLine = false;
			this.txtScore235.Name = "txtScore235";
			this.txtScore235.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore235.Text = "ITEM07";
			this.txtScore235.Top = 0.2393701F;
			this.txtScore235.Width = 1.979134F;
			// 
			// txtScore231
			// 
			this.txtScore231.DataField = "ITEM03";
			this.txtScore231.Height = 0.2393701F;
			this.txtScore231.Left = 2.046457F;
			this.txtScore231.MultiLine = false;
			this.txtScore231.Name = "txtScore231";
			this.txtScore231.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore231.Text = "003";
			this.txtScore231.Top = 2.980232E-08F;
			this.txtScore231.Width = 1.209448F;
			// 
			// txtScore236
			// 
			this.txtScore236.DataField = "ITEM08";
			this.txtScore236.Height = 0.2393701F;
			this.txtScore236.Left = 2.046457F;
			this.txtScore236.MultiLine = false;
			this.txtScore236.Name = "txtScore236";
			this.txtScore236.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore236.Text = "008";
			this.txtScore236.Top = 0.2393701F;
			this.txtScore236.Width = 1.209448F;
			// 
			// txtScore238
			// 
			this.txtScore238.DataField = "ITEM10";
			this.txtScore238.Height = 0.2393701F;
			this.txtScore238.Left = 4.572441F;
			this.txtScore238.MultiLine = false;
			this.txtScore238.Name = "txtScore238";
			this.txtScore238.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore238.Text = "010";
			this.txtScore238.Top = 0.2393701F;
			this.txtScore238.Width = 1.20433F;
			// 
			// txtScore239
			// 
			this.txtScore239.DataField = "ITEM11";
			this.txtScore239.Height = 0.2393701F;
			this.txtScore239.Left = 5.842127F;
			this.txtScore239.MultiLine = false;
			this.txtScore239.Name = "txtScore239";
			this.txtScore239.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore239.Text = "011";
			this.txtScore239.Top = 0.2393701F;
			this.txtScore239.Width = 1.201574F;
			// 
			// txtScore240
			// 
			this.txtScore240.DataField = "ITEM12";
			this.txtScore240.Height = 0.2393701F;
			this.txtScore240.Left = 0.06811024F;
			this.txtScore240.MultiLine = false;
			this.txtScore240.Name = "txtScore240";
			this.txtScore240.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore240.Text = "ITEM12";
			this.txtScore240.Top = 0.4787402F;
			this.txtScore240.Width = 1.979134F;
			// 
			// txtScore241
			// 
			this.txtScore241.DataField = "ITEM13";
			this.txtScore241.Height = 0.2393701F;
			this.txtScore241.Left = 2.046457F;
			this.txtScore241.MultiLine = false;
			this.txtScore241.Name = "txtScore241";
			this.txtScore241.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore241.Text = "013";
			this.txtScore241.Top = 0.4787402F;
			this.txtScore241.Width = 1.208661F;
			// 
			// txtScore237
			// 
			this.txtScore237.DataField = "ITEM09";
			this.txtScore237.Height = 0.2393701F;
			this.txtScore237.Left = 3.311024F;
			this.txtScore237.MultiLine = false;
			this.txtScore237.Name = "txtScore237";
			this.txtScore237.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore237.Text = "009";
			this.txtScore237.Top = 0.2393701F;
			this.txtScore237.Width = 1.213386F;
			// 
			// txtScore242
			// 
			this.txtScore242.DataField = "ITEM14";
			this.txtScore242.Height = 0.2393701F;
			this.txtScore242.Left = 3.311024F;
			this.txtScore242.MultiLine = false;
			this.txtScore242.Name = "txtScore242";
			this.txtScore242.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore242.Text = "014";
			this.txtScore242.Top = 0.4787401F;
			this.txtScore242.Width = 1.209843F;
			// 
			// txtScore244
			// 
			this.txtScore244.DataField = "ITEM16";
			this.txtScore244.Height = 0.2393701F;
			this.txtScore244.Left = 5.842127F;
			this.txtScore244.MultiLine = false;
			this.txtScore244.Name = "txtScore244";
			this.txtScore244.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore244.Text = "016";
			this.txtScore244.Top = 0.4787401F;
			this.txtScore244.Width = 1.201574F;
			// 
			// txtScore245
			// 
			this.txtScore245.DataField = "ITEM17";
			this.txtScore245.Height = 0.2393701F;
			this.txtScore245.Left = 0.06811024F;
			this.txtScore245.MultiLine = false;
			this.txtScore245.Name = "txtScore245";
			this.txtScore245.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore245.Text = "ITEM17";
			this.txtScore245.Top = 0.7181104F;
			this.txtScore245.Width = 1.979134F;
			// 
			// txtScore246
			// 
			this.txtScore246.DataField = "ITEM18";
			this.txtScore246.Height = 0.2393701F;
			this.txtScore246.Left = 2.046457F;
			this.txtScore246.MultiLine = false;
			this.txtScore246.Name = "txtScore246";
			this.txtScore246.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore246.Text = "018";
			this.txtScore246.Top = 0.7181104F;
			this.txtScore246.Width = 1.208661F;
			// 
			// txtScore247
			// 
			this.txtScore247.DataField = "ITEM19";
			this.txtScore247.Height = 0.2393701F;
			this.txtScore247.Left = 3.311024F;
			this.txtScore247.MultiLine = false;
			this.txtScore247.Name = "txtScore247";
			this.txtScore247.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore247.Text = "019";
			this.txtScore247.Top = 0.7181104F;
			this.txtScore247.Width = 1.213386F;
			// 
			// txtScore243
			// 
			this.txtScore243.DataField = "ITEM15";
			this.txtScore243.Height = 0.2393701F;
			this.txtScore243.Left = 4.57323F;
			this.txtScore243.MultiLine = false;
			this.txtScore243.Name = "txtScore243";
			this.txtScore243.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore243.Text = "015";
			this.txtScore243.Top = 0.4787401F;
			this.txtScore243.Width = 1.203149F;
			// 
			// txtScore248
			// 
			this.txtScore248.DataField = "ITEM20";
			this.txtScore248.Height = 0.2393701F;
			this.txtScore248.Left = 4.572441F;
			this.txtScore248.MultiLine = false;
			this.txtScore248.Name = "txtScore248";
			this.txtScore248.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore248.Text = "020";
			this.txtScore248.Top = 0.7181104F;
			this.txtScore248.Width = 1.20433F;
			// 
			// txtScore250
			// 
			this.txtScore250.DataField = "ITEM22";
			this.txtScore250.Height = 0.2393701F;
			this.txtScore250.Left = 0.06850399F;
			this.txtScore250.MultiLine = false;
			this.txtScore250.Name = "txtScore250";
			this.txtScore250.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore250.Text = "ITEM22";
			this.txtScore250.Top = 0.9574805F;
			this.txtScore250.Width = 1.97874F;
			// 
			// txtScore251
			// 
			this.txtScore251.DataField = "ITEM23";
			this.txtScore251.Height = 0.2393701F;
			this.txtScore251.Left = 2.046457F;
			this.txtScore251.MultiLine = false;
			this.txtScore251.Name = "txtScore251";
			this.txtScore251.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore251.Text = "023";
			this.txtScore251.Top = 0.9574805F;
			this.txtScore251.Width = 1.208661F;
			// 
			// txtScore252
			// 
			this.txtScore252.DataField = "ITEM24";
			this.txtScore252.Height = 0.2393701F;
			this.txtScore252.Left = 3.311024F;
			this.txtScore252.MultiLine = false;
			this.txtScore252.Name = "txtScore252";
			this.txtScore252.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore252.Text = "024";
			this.txtScore252.Top = 0.9574805F;
			this.txtScore252.Width = 1.213386F;
			// 
			// txtScore253
			// 
			this.txtScore253.DataField = "ITEM25";
			this.txtScore253.Height = 0.2393701F;
			this.txtScore253.Left = 4.572441F;
			this.txtScore253.MultiLine = false;
			this.txtScore253.Name = "txtScore253";
			this.txtScore253.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore253.Text = "025";
			this.txtScore253.Top = 0.9574805F;
			this.txtScore253.Width = 1.20433F;
			// 
			// txtScore249
			// 
			this.txtScore249.DataField = "ITEM21";
			this.txtScore249.Height = 0.2393701F;
			this.txtScore249.Left = 5.842127F;
			this.txtScore249.MultiLine = false;
			this.txtScore249.Name = "txtScore249";
			this.txtScore249.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore249.Text = "021";
			this.txtScore249.Top = 0.7181104F;
			this.txtScore249.Width = 1.201574F;
			// 
			// txtScore254
			// 
			this.txtScore254.DataField = "ITEM26";
			this.txtScore254.Height = 0.2393701F;
			this.txtScore254.Left = 5.842128F;
			this.txtScore254.MultiLine = false;
			this.txtScore254.Name = "txtScore254";
			this.txtScore254.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore254.Text = "026";
			this.txtScore254.Top = 0.9574805F;
			this.txtScore254.Width = 1.201573F;
			// 
			// txtScore256
			// 
			this.txtScore256.DataField = "ITEM28";
			this.txtScore256.Height = 0.2393701F;
			this.txtScore256.Left = 2.046457F;
			this.txtScore256.MultiLine = false;
			this.txtScore256.Name = "txtScore256";
			this.txtScore256.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore256.Text = "028";
			this.txtScore256.Top = 1.196851F;
			this.txtScore256.Width = 1.208661F;
			// 
			// txtScore257
			// 
			this.txtScore257.DataField = "ITEM29";
			this.txtScore257.Height = 0.2393701F;
			this.txtScore257.Left = 3.311024F;
			this.txtScore257.MultiLine = false;
			this.txtScore257.Name = "txtScore257";
			this.txtScore257.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore257.Text = "029";
			this.txtScore257.Top = 1.196851F;
			this.txtScore257.Width = 1.213386F;
			// 
			// txtScore258
			// 
			this.txtScore258.DataField = "ITEM30";
			this.txtScore258.Height = 0.2393701F;
			this.txtScore258.Left = 4.572441F;
			this.txtScore258.MultiLine = false;
			this.txtScore258.Name = "txtScore258";
			this.txtScore258.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore258.Text = "030";
			this.txtScore258.Top = 1.196851F;
			this.txtScore258.Width = 1.203936F;
			// 
			// txtScore259
			// 
			this.txtScore259.DataField = "ITEM31";
			this.txtScore259.Height = 0.2393701F;
			this.txtScore259.Left = 5.842127F;
			this.txtScore259.MultiLine = false;
			this.txtScore259.Name = "txtScore259";
			this.txtScore259.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore259.Text = "031";
			this.txtScore259.Top = 1.196851F;
			this.txtScore259.Width = 1.201574F;
			// 
			// txtScore255
			// 
			this.txtScore255.DataField = "ITEM27";
			this.txtScore255.Height = 0.2393701F;
			this.txtScore255.Left = 0.06811024F;
			this.txtScore255.MultiLine = false;
			this.txtScore255.Name = "txtScore255";
			this.txtScore255.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore255.Text = "ITEM27";
			this.txtScore255.Top = 1.196851F;
			this.txtScore255.Width = 1.979134F;
			// 
			// txtScore260
			// 
			this.txtScore260.DataField = "ITEM32";
			this.txtScore260.Height = 0.2393701F;
			this.txtScore260.Left = 0.06811024F;
			this.txtScore260.MultiLine = false;
			this.txtScore260.Name = "txtScore260";
			this.txtScore260.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore260.Text = "ITEM32";
			this.txtScore260.Top = 1.43622F;
			this.txtScore260.Width = 1.979134F;
			// 
			// txtScore262
			// 
			this.txtScore262.DataField = "ITEM34";
			this.txtScore262.Height = 0.2393701F;
			this.txtScore262.Left = 3.311418F;
			this.txtScore262.MultiLine = false;
			this.txtScore262.Name = "txtScore262";
			this.txtScore262.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore262.Text = "034";
			this.txtScore262.Top = 1.43622F;
			this.txtScore262.Width = 1.213386F;
			// 
			// txtScore263
			// 
			this.txtScore263.DataField = "ITEM35";
			this.txtScore263.Height = 0.2393701F;
			this.txtScore263.Left = 4.572441F;
			this.txtScore263.MultiLine = false;
			this.txtScore263.Name = "txtScore263";
			this.txtScore263.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore263.Text = "035";
			this.txtScore263.Top = 1.43622F;
			this.txtScore263.Width = 1.203936F;
			// 
			// txtScore264
			// 
			this.txtScore264.DataField = "ITEM36";
			this.txtScore264.Height = 0.2393701F;
			this.txtScore264.Left = 5.842127F;
			this.txtScore264.MultiLine = false;
			this.txtScore264.Name = "txtScore264";
			this.txtScore264.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore264.Text = "036";
			this.txtScore264.Top = 1.43622F;
			this.txtScore264.Width = 1.201574F;
			// 
			// txtScore265
			// 
			this.txtScore265.DataField = "ITEM37";
			this.txtScore265.Height = 0.2393701F;
			this.txtScore265.Left = 0.06850399F;
			this.txtScore265.MultiLine = false;
			this.txtScore265.Name = "txtScore265";
			this.txtScore265.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore265.Text = "ITEM37";
			this.txtScore265.Top = 1.675591F;
			this.txtScore265.Width = 1.97874F;
			// 
			// txtScore261
			// 
			this.txtScore261.DataField = "ITEM33";
			this.txtScore261.Height = 0.2393701F;
			this.txtScore261.Left = 2.046457F;
			this.txtScore261.MultiLine = false;
			this.txtScore261.Name = "txtScore261";
			this.txtScore261.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore261.Text = "033";
			this.txtScore261.Top = 1.43622F;
			this.txtScore261.Width = 1.208661F;
			// 
			// txtScore266
			// 
			this.txtScore266.DataField = "ITEM38";
			this.txtScore266.Height = 0.2393701F;
			this.txtScore266.Left = 2.046457F;
			this.txtScore266.MultiLine = false;
			this.txtScore266.Name = "txtScore266";
			this.txtScore266.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore266.Text = "038";
			this.txtScore266.Top = 1.675591F;
			this.txtScore266.Width = 1.208661F;
			// 
			// txtScore268
			// 
			this.txtScore268.DataField = "ITEM40";
			this.txtScore268.Height = 0.2393701F;
			this.txtScore268.Left = 4.572835F;
			this.txtScore268.MultiLine = false;
			this.txtScore268.Name = "txtScore268";
			this.txtScore268.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore268.Text = "040";
			this.txtScore268.Top = 1.675591F;
			this.txtScore268.Width = 1.203543F;
			// 
			// txtScore269
			// 
			this.txtScore269.DataField = "ITEM41";
			this.txtScore269.Height = 0.2393701F;
			this.txtScore269.Left = 5.842127F;
			this.txtScore269.MultiLine = false;
			this.txtScore269.Name = "txtScore269";
			this.txtScore269.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore269.Text = "041";
			this.txtScore269.Top = 1.675591F;
			this.txtScore269.Width = 1.201574F;
			// 
			// txtScore270
			// 
			this.txtScore270.DataField = "ITEM42";
			this.txtScore270.Height = 0.2393701F;
			this.txtScore270.Left = 0.06850399F;
			this.txtScore270.MultiLine = false;
			this.txtScore270.Name = "txtScore270";
			this.txtScore270.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore270.Text = "ITEM42";
			this.txtScore270.Top = 1.914961F;
			this.txtScore270.Width = 1.97874F;
			// 
			// txtScore271
			// 
			this.txtScore271.DataField = "ITEM43";
			this.txtScore271.Height = 0.2393701F;
			this.txtScore271.Left = 2.046457F;
			this.txtScore271.MultiLine = false;
			this.txtScore271.Name = "txtScore271";
			this.txtScore271.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore271.Text = "043";
			this.txtScore271.Top = 1.914961F;
			this.txtScore271.Width = 1.208661F;
			// 
			// txtScore267
			// 
			this.txtScore267.DataField = "ITEM39";
			this.txtScore267.Height = 0.2393701F;
			this.txtScore267.Left = 3.311024F;
			this.txtScore267.MultiLine = false;
			this.txtScore267.Name = "txtScore267";
			this.txtScore267.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore267.Text = "039";
			this.txtScore267.Top = 1.675591F;
			this.txtScore267.Width = 1.213386F;
			// 
			// txtScore272
			// 
			this.txtScore272.DataField = "ITEM44";
			this.txtScore272.Height = 0.2393701F;
			this.txtScore272.Left = 3.311418F;
			this.txtScore272.MultiLine = false;
			this.txtScore272.Name = "txtScore272";
			this.txtScore272.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore272.Text = "044";
			this.txtScore272.Top = 1.914961F;
			this.txtScore272.Width = 1.212992F;
			// 
			// txtScore274
			// 
			this.txtScore274.DataField = "ITEM46";
			this.txtScore274.Height = 0.2393701F;
			this.txtScore274.Left = 5.84252F;
			this.txtScore274.MultiLine = false;
			this.txtScore274.Name = "txtScore274";
			this.txtScore274.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore274.Text = "046";
			this.txtScore274.Top = 1.914961F;
			this.txtScore274.Width = 1.201181F;
			// 
			// txtScore275
			// 
			this.txtScore275.DataField = "ITEM47";
			this.txtScore275.Height = 0.2393701F;
			this.txtScore275.Left = 0.06811024F;
			this.txtScore275.MultiLine = false;
			this.txtScore275.Name = "txtScore275";
			this.txtScore275.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore275.Text = "ITEM47";
			this.txtScore275.Top = 2.154331F;
			this.txtScore275.Width = 1.979134F;
			// 
			// txtScore276
			// 
			this.txtScore276.DataField = "ITEM48";
			this.txtScore276.Height = 0.2393701F;
			this.txtScore276.Left = 2.046457F;
			this.txtScore276.MultiLine = false;
			this.txtScore276.Name = "txtScore276";
			this.txtScore276.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore276.Text = "048";
			this.txtScore276.Top = 2.154331F;
			this.txtScore276.Width = 1.208661F;
			// 
			// txtScore277
			// 
			this.txtScore277.DataField = "ITEM49";
			this.txtScore277.Height = 0.2393701F;
			this.txtScore277.Left = 3.311024F;
			this.txtScore277.MultiLine = false;
			this.txtScore277.Name = "txtScore277";
			this.txtScore277.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore277.Text = "049";
			this.txtScore277.Top = 2.154331F;
			this.txtScore277.Width = 1.212599F;
			// 
			// txtScore273
			// 
			this.txtScore273.DataField = "ITEM45";
			this.txtScore273.Height = 0.2393701F;
			this.txtScore273.Left = 4.572441F;
			this.txtScore273.MultiLine = false;
			this.txtScore273.Name = "txtScore273";
			this.txtScore273.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore273.Text = "045";
			this.txtScore273.Top = 1.914961F;
			this.txtScore273.Width = 1.203937F;
			// 
			// txtScore278
			// 
			this.txtScore278.DataField = "ITEM50";
			this.txtScore278.Height = 0.2393701F;
			this.txtScore278.Left = 4.571654F;
			this.txtScore278.MultiLine = false;
			this.txtScore278.Name = "txtScore278";
			this.txtScore278.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore278.Text = "050";
			this.txtScore278.Top = 2.154331F;
			this.txtScore278.Width = 1.204724F;
			// 
			// txtScore280
			// 
			this.txtScore280.DataField = "ITEM52";
			this.txtScore280.Height = 0.2393701F;
			this.txtScore280.Left = 0.06850399F;
			this.txtScore280.MultiLine = false;
			this.txtScore280.Name = "txtScore280";
			this.txtScore280.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore280.Text = "ITEM52";
			this.txtScore280.Top = 2.393701F;
			this.txtScore280.Width = 1.97874F;
			// 
			// txtScore281
			// 
			this.txtScore281.DataField = "ITEM53";
			this.txtScore281.Height = 0.2393701F;
			this.txtScore281.Left = 2.046457F;
			this.txtScore281.MultiLine = false;
			this.txtScore281.Name = "txtScore281";
			this.txtScore281.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore281.Text = "053";
			this.txtScore281.Top = 2.393701F;
			this.txtScore281.Width = 1.209448F;
			// 
			// txtScore282
			// 
			this.txtScore282.DataField = "ITEM54";
			this.txtScore282.Height = 0.2393701F;
			this.txtScore282.Left = 3.311418F;
			this.txtScore282.MultiLine = false;
			this.txtScore282.Name = "txtScore282";
			this.txtScore282.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore282.Text = "054";
			this.txtScore282.Top = 2.393701F;
			this.txtScore282.Width = 1.212992F;
			// 
			// txtScore283
			// 
			this.txtScore283.DataField = "ITEM55";
			this.txtScore283.Height = 0.2393701F;
			this.txtScore283.Left = 4.572441F;
			this.txtScore283.MultiLine = false;
			this.txtScore283.Name = "txtScore283";
			this.txtScore283.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore283.Text = "055";
			this.txtScore283.Top = 2.393701F;
			this.txtScore283.Width = 1.203937F;
			// 
			// txtScore279
			// 
			this.txtScore279.DataField = "ITEM51";
			this.txtScore279.Height = 0.2393701F;
			this.txtScore279.Left = 5.842127F;
			this.txtScore279.MultiLine = false;
			this.txtScore279.Name = "txtScore279";
			this.txtScore279.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore279.Text = "051";
			this.txtScore279.Top = 2.154331F;
			this.txtScore279.Width = 1.201574F;
			// 
			// txtScore284
			// 
			this.txtScore284.DataField = "ITEM56";
			this.txtScore284.Height = 0.2393701F;
			this.txtScore284.Left = 5.84252F;
			this.txtScore284.MultiLine = false;
			this.txtScore284.Name = "txtScore284";
			this.txtScore284.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore284.Text = "056,456,789,000";
			this.txtScore284.Top = 2.393701F;
			this.txtScore284.Width = 1.201181F;
			// 
			// txtScore285
			// 
			this.txtScore285.DataField = "ITEM57";
			this.txtScore285.Height = 0.2393701F;
			this.txtScore285.Left = 0.06771685F;
			this.txtScore285.MultiLine = false;
			this.txtScore285.Name = "txtScore285";
			this.txtScore285.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore285.Text = "ITEM57";
			this.txtScore285.Top = 2.633071F;
			this.txtScore285.Width = 1.979134F;
			// 
			// txtScore287
			// 
			this.txtScore287.DataField = "ITEM59";
			this.txtScore287.Height = 0.2393701F;
			this.txtScore287.Left = 3.310632F;
			this.txtScore287.MultiLine = false;
			this.txtScore287.Name = "txtScore287";
			this.txtScore287.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore287.Text = "059";
			this.txtScore287.Top = 2.633071F;
			this.txtScore287.Width = 1.213386F;
			// 
			// txtScore288
			// 
			this.txtScore288.DataField = "ITEM60";
			this.txtScore288.Height = 0.2393701F;
			this.txtScore288.Left = 4.572047F;
			this.txtScore288.MultiLine = false;
			this.txtScore288.Name = "txtScore288";
			this.txtScore288.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore288.Text = "060";
			this.txtScore288.Top = 2.633071F;
			this.txtScore288.Width = 1.20433F;
			// 
			// txtScore289
			// 
			this.txtScore289.DataField = "ITEM61";
			this.txtScore289.Height = 0.2393701F;
			this.txtScore289.Left = 5.841733F;
			this.txtScore289.MultiLine = false;
			this.txtScore289.Name = "txtScore289";
			this.txtScore289.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore289.Text = "061";
			this.txtScore289.Top = 2.633071F;
			this.txtScore289.Width = 1.201574F;
			// 
			// txtScore286
			// 
			this.txtScore286.DataField = "ITEM58";
			this.txtScore286.Height = 0.2393701F;
			this.txtScore286.Left = 2.046064F;
			this.txtScore286.MultiLine = false;
			this.txtScore286.Name = "txtScore286";
			this.txtScore286.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore286.Text = "058";
			this.txtScore286.Top = 2.633071F;
			this.txtScore286.Width = 1.209841F;
			// 
			// line45
			// 
			this.line45.Height = 3.111811F;
			this.line45.Left = 7.086614F;
			this.line45.LineWeight = 1F;
			this.line45.Name = "line45";
			this.line45.Top = 0F;
			this.line45.Width = 0F;
			this.line45.X1 = 7.086614F;
			this.line45.X2 = 7.086614F;
			this.line45.Y1 = 3.111811F;
			this.line45.Y2 = 0F;
			// 
			// txtScore290
			// 
			this.txtScore290.DataField = "ITEM62";
			this.txtScore290.Height = 0.2393701F;
			this.txtScore290.Left = 0.07165386F;
			this.txtScore290.MultiLine = false;
			this.txtScore290.Name = "txtScore290";
			this.txtScore290.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore290.Text = "ITEM62";
			this.txtScore290.Top = 2.872441F;
			this.txtScore290.Width = 1.977559F;
			// 
			// txtScore292
			// 
			this.txtScore292.DataField = "ITEM64";
			this.txtScore292.Height = 0.2393701F;
			this.txtScore292.Left = 3.315355F;
			this.txtScore292.MultiLine = false;
			this.txtScore292.Name = "txtScore292";
			this.txtScore292.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore292.Text = "064";
			this.txtScore292.Top = 2.872441F;
			this.txtScore292.Width = 1.209843F;
			// 
			// txtScore293
			// 
			this.txtScore293.DataField = "ITEM65";
			this.txtScore293.Height = 0.2393701F;
			this.txtScore293.Left = 4.576773F;
			this.txtScore293.MultiLine = false;
			this.txtScore293.Name = "txtScore293";
			this.txtScore293.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore293.Text = "065";
			this.txtScore293.Top = 2.872441F;
			this.txtScore293.Width = 1.204723F;
			// 
			// txtScore291
			// 
			this.txtScore291.DataField = "ITEM63";
			this.txtScore291.Height = 0.2393701F;
			this.txtScore291.Left = 2.040158F;
			this.txtScore291.MultiLine = false;
			this.txtScore291.Name = "txtScore291";
			this.txtScore291.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore291.Text = "063";
			this.txtScore291.Top = 2.872441F;
			this.txtScore291.Width = 1.209843F;
			// 
			// line44
			// 
			this.line44.Height = 0F;
			this.line44.Left = 0.007874016F;
			this.line44.LineWeight = 1F;
			this.line44.Name = "line44";
			this.line44.Top = 3.111811F;
			this.line44.Width = 7.07874F;
			this.line44.X1 = 7.086614F;
			this.line44.X2 = 0.007874016F;
			this.line44.Y1 = 3.111811F;
			this.line44.Y2 = 3.111811F;
			// 
			// line47
			// 
			this.line47.Height = 3.111811F;
			this.line47.Left = 0.007874016F;
			this.line47.LineWeight = 1F;
			this.line47.Name = "line47";
			this.line47.Top = 0F;
			this.line47.Width = 0F;
			this.line47.X1 = 0.007874016F;
			this.line47.X2 = 0.007874016F;
			this.line47.Y1 = 3.111811F;
			this.line47.Y2 = 0F;
			// 
			// line160
			// 
			this.line160.Height = 3.111811F;
			this.line160.Left = 5.846064F;
			this.line160.LineWeight = 1F;
			this.line160.Name = "line160";
			this.line160.Top = 0F;
			this.line160.Width = 0F;
			this.line160.X1 = 5.846064F;
			this.line160.X2 = 5.846064F;
			this.line160.Y1 = 3.111811F;
			this.line160.Y2 = 0F;
			// 
			// line171
			// 
			this.line171.Height = 3.111811F;
			this.line171.Left = 4.571654F;
			this.line171.LineWeight = 1F;
			this.line171.Name = "line171";
			this.line171.Top = 0F;
			this.line171.Width = 0F;
			this.line171.X1 = 4.571654F;
			this.line171.X2 = 4.571654F;
			this.line171.Y1 = 3.111811F;
			this.line171.Y2 = 0F;
			// 
			// line172
			// 
			this.line172.Height = 3.111811F;
			this.line172.Left = 2.040158F;
			this.line172.LineWeight = 1F;
			this.line172.Name = "line172";
			this.line172.Top = 0F;
			this.line172.Width = 0F;
			this.line172.X1 = 2.040158F;
			this.line172.X2 = 2.040158F;
			this.line172.Y1 = 3.111811F;
			this.line172.Y2 = 0F;
			// 
			// line173
			// 
			this.line173.Height = 3.111811F;
			this.line173.Left = 3.315355F;
			this.line173.LineWeight = 1F;
			this.line173.Name = "line173";
			this.line173.Top = 0F;
			this.line173.Width = 0F;
			this.line173.X1 = 3.315355F;
			this.line173.X2 = 3.315355F;
			this.line173.Y1 = 3.111811F;
			this.line173.Y2 = 0F;
			// 
			// line175
			// 
			this.line175.Height = 0F;
			this.line175.Left = 0.007877827F;
			this.line175.LineWeight = 1F;
			this.line175.Name = "line175";
			this.line175.Top = 0.2393701F;
			this.line175.Width = 7.078737F;
			this.line175.X1 = 7.086615F;
			this.line175.X2 = 0.007877827F;
			this.line175.Y1 = 0.2393701F;
			this.line175.Y2 = 0.2393701F;
			// 
			// line176
			// 
			this.line176.Height = 0F;
			this.line176.Left = 0.007874016F;
			this.line176.LineWeight = 1F;
			this.line176.Name = "line176";
			this.line176.Top = 0.4787402F;
			this.line176.Width = 7.07874F;
			this.line176.X1 = 7.086614F;
			this.line176.X2 = 0.007874016F;
			this.line176.Y1 = 0.4787402F;
			this.line176.Y2 = 0.4787402F;
			// 
			// line177
			// 
			this.line177.Height = 0F;
			this.line177.Left = 0.007874016F;
			this.line177.LineWeight = 1F;
			this.line177.Name = "line177";
			this.line177.Top = 2.872441F;
			this.line177.Width = 7.07874F;
			this.line177.X1 = 7.086614F;
			this.line177.X2 = 0.007874016F;
			this.line177.Y1 = 2.872441F;
			this.line177.Y2 = 2.872441F;
			// 
			// line178
			// 
			this.line178.Height = 0F;
			this.line178.Left = 0.007874016F;
			this.line178.LineWeight = 1F;
			this.line178.Name = "line178";
			this.line178.Top = 2.633071F;
			this.line178.Width = 7.07874F;
			this.line178.X1 = 7.086614F;
			this.line178.X2 = 0.007874016F;
			this.line178.Y1 = 2.633071F;
			this.line178.Y2 = 2.633071F;
			// 
			// line179
			// 
			this.line179.Height = 0F;
			this.line179.Left = 0.007874016F;
			this.line179.LineWeight = 1F;
			this.line179.Name = "line179";
			this.line179.Top = 0.7181103F;
			this.line179.Width = 7.07874F;
			this.line179.X1 = 7.086614F;
			this.line179.X2 = 0.007874016F;
			this.line179.Y1 = 0.7181103F;
			this.line179.Y2 = 0.7181103F;
			// 
			// line180
			// 
			this.line180.Height = 0F;
			this.line180.Left = 0.007874016F;
			this.line180.LineWeight = 1F;
			this.line180.Name = "line180";
			this.line180.Top = 2.393701F;
			this.line180.Width = 7.07874F;
			this.line180.X1 = 7.086614F;
			this.line180.X2 = 0.007874016F;
			this.line180.Y1 = 2.393701F;
			this.line180.Y2 = 2.393701F;
			// 
			// line181
			// 
			this.line181.Height = 0F;
			this.line181.Left = 0.007874016F;
			this.line181.LineWeight = 1F;
			this.line181.Name = "line181";
			this.line181.Top = 0.9574804F;
			this.line181.Width = 7.07874F;
			this.line181.X1 = 7.086614F;
			this.line181.X2 = 0.007874016F;
			this.line181.Y1 = 0.9574804F;
			this.line181.Y2 = 0.9574804F;
			// 
			// line182
			// 
			this.line182.Height = 0F;
			this.line182.Left = 0.007874016F;
			this.line182.LineWeight = 1F;
			this.line182.Name = "line182";
			this.line182.Top = 2.154331F;
			this.line182.Width = 7.07874F;
			this.line182.X1 = 7.086614F;
			this.line182.X2 = 0.007874016F;
			this.line182.Y1 = 2.154331F;
			this.line182.Y2 = 2.154331F;
			// 
			// line183
			// 
			this.line183.Height = 0F;
			this.line183.Left = 0.007874016F;
			this.line183.LineWeight = 1F;
			this.line183.Name = "line183";
			this.line183.Top = 1.19685F;
			this.line183.Width = 7.07874F;
			this.line183.X1 = 7.086614F;
			this.line183.X2 = 0.007874016F;
			this.line183.Y1 = 1.19685F;
			this.line183.Y2 = 1.19685F;
			// 
			// line184
			// 
			this.line184.Height = 0F;
			this.line184.Left = 0.007874016F;
			this.line184.LineWeight = 1F;
			this.line184.Name = "line184";
			this.line184.Top = 1.436221F;
			this.line184.Width = 7.07874F;
			this.line184.X1 = 7.086614F;
			this.line184.X2 = 0.007874016F;
			this.line184.Y1 = 1.436221F;
			this.line184.Y2 = 1.436221F;
			// 
			// line185
			// 
			this.line185.Height = 0F;
			this.line185.Left = 0.007877827F;
			this.line185.LineWeight = 1F;
			this.line185.Name = "line185";
			this.line185.Top = 1.675591F;
			this.line185.Width = 7.078737F;
			this.line185.X1 = 7.086615F;
			this.line185.X2 = 0.007877827F;
			this.line185.Y1 = 1.675591F;
			this.line185.Y2 = 1.675591F;
			// 
			// line186
			// 
			this.line186.Height = 0F;
			this.line186.Left = 0.007874016F;
			this.line186.LineWeight = 1F;
			this.line186.Name = "line186";
			this.line186.Top = 1.914961F;
			this.line186.Width = 7.07874F;
			this.line186.X1 = 7.086614F;
			this.line186.X2 = 0.007874016F;
			this.line186.Y1 = 1.914961F;
			this.line186.Y2 = 1.914961F;
			// 
			// group05_H
			// 
			this.group05_H.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox495,
            this.textBox494,
            this.textBox496,
            this.textBox497,
            this.textBox498,
            this.textBox499,
            this.textBox500,
            this.line56,
            this.line57,
            this.line58,
            this.line59,
            this.line60,
            this.line61,
            this.line62,
            this.pageBreak2,
            this.line93});
			this.group05_H.DataField = "ITEM01";
			this.group05_H.Height = 0.6822835F;
			this.group05_H.Name = "group05_H";
			this.group05_H.Format += new System.EventHandler(this.group05_Format);
			// 
			// textBox495
			// 
			this.textBox495.Height = 0.2389764F;
			this.textBox495.Left = 0.02716541F;
			this.textBox495.MultiLine = false;
			this.textBox495.Name = "textBox495";
			this.textBox495.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: justify; ve" +
    "rtical-align: middle";
			this.textBox495.Text = "（５）その他の固定資産";
			this.textBox495.Top = 0F;
			this.textBox495.Width = 4.237402F;
			// 
			// textBox494
			// 
			this.textBox494.Height = 0.4433071F;
			this.textBox494.Left = 0.007874016F;
			this.textBox494.MultiLine = false;
			this.textBox494.Name = "textBox494";
			this.textBox494.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: justify; vertical-align: middle";
			this.textBox494.Text = null;
			this.textBox494.Top = 0.2389764F;
			this.textBox494.Width = 7.086614F;
			// 
			// textBox496
			// 
			this.textBox496.Height = 0.4429134F;
			this.textBox496.Left = 0F;
			this.textBox496.MultiLine = false;
			this.textBox496.Name = "textBox496";
			this.textBox496.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; te" +
    "xt-justify: auto; vertical-align: middle";
			this.textBox496.Text = "科     目";
			this.textBox496.Top = 0.2393701F;
			this.textBox496.Width = 2.020473F;
			// 
			// textBox497
			// 
			this.textBox497.Height = 0.4429134F;
			this.textBox497.Left = 2.020473F;
			this.textBox497.MultiLine = false;
			this.textBox497.Name = "textBox497";
			this.textBox497.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox497.Text = "前年度末残高";
			this.textBox497.Top = 0.2393701F;
			this.textBox497.Width = 1.290945F;
			// 
			// textBox498
			// 
			this.textBox498.Height = 0.4440946F;
			this.textBox498.Left = 3.309449F;
			this.textBox498.MultiLine = false;
			this.textBox498.Name = "textBox498";
			this.textBox498.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox498.Text = "本年度増加額";
			this.textBox498.Top = 0.238189F;
			this.textBox498.Width = 1.265354F;
			// 
			// textBox499
			// 
			this.textBox499.Height = 0.4440945F;
			this.textBox499.Left = 4.574803F;
			this.textBox499.MultiLine = false;
			this.textBox499.Name = "textBox499";
			this.textBox499.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox499.Text = "本年度減少額";
			this.textBox499.Top = 0.238189F;
			this.textBox499.Width = 1.267717F;
			// 
			// textBox500
			// 
			this.textBox500.Height = 0.4433071F;
			this.textBox500.Left = 5.83504F;
			this.textBox500.MultiLine = false;
			this.textBox500.Name = "textBox500";
			this.textBox500.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox500.Text = "本年度末残高";
			this.textBox500.Top = 0.238189F;
			this.textBox500.Width = 1.246457F;
			// 
			// line56
			// 
			this.line56.Height = 1.043081E-07F;
			this.line56.Left = 0.007874016F;
			this.line56.LineWeight = 1F;
			this.line56.Name = "line56";
			this.line56.Top = 0.2389763F;
			this.line56.Width = 7.07874F;
			this.line56.X1 = 7.086614F;
			this.line56.X2 = 0.007874016F;
			this.line56.Y1 = 0.2389763F;
			this.line56.Y2 = 0.2389764F;
			// 
			// line57
			// 
			this.line57.Height = 0.4440945F;
			this.line57.Left = 5.840158F;
			this.line57.LineWeight = 1F;
			this.line57.Name = "line57";
			this.line57.Top = 0.238189F;
			this.line57.Width = 0F;
			this.line57.X1 = 5.840158F;
			this.line57.X2 = 5.840158F;
			this.line57.Y1 = 0.238189F;
			this.line57.Y2 = 0.6822835F;
			// 
			// line58
			// 
			this.line58.Height = 0.4440945F;
			this.line58.Left = 4.574803F;
			this.line58.LineWeight = 1F;
			this.line58.Name = "line58";
			this.line58.Top = 0.238189F;
			this.line58.Width = 0F;
			this.line58.X1 = 4.574803F;
			this.line58.X2 = 4.574803F;
			this.line58.Y1 = 0.238189F;
			this.line58.Y2 = 0.6822835F;
			// 
			// line59
			// 
			this.line59.Height = 0.4437008F;
			this.line59.Left = 3.315354F;
			this.line59.LineWeight = 1F;
			this.line59.Name = "line59";
			this.line59.Top = 0.2385827F;
			this.line59.Width = 0F;
			this.line59.X1 = 3.315354F;
			this.line59.X2 = 3.315354F;
			this.line59.Y1 = 0.2385827F;
			this.line59.Y2 = 0.6822835F;
			// 
			// line60
			// 
			this.line60.Height = 0.4440945F;
			this.line60.Left = 2.020473F;
			this.line60.LineWeight = 1F;
			this.line60.Name = "line60";
			this.line60.Top = 0.238189F;
			this.line60.Width = 0F;
			this.line60.X1 = 2.020473F;
			this.line60.X2 = 2.020473F;
			this.line60.Y1 = 0.238189F;
			this.line60.Y2 = 0.6822835F;
			// 
			// line61
			// 
			this.line61.Height = 0.4433072F;
			this.line61.Left = 7.086614F;
			this.line61.LineWeight = 1F;
			this.line61.Name = "line61";
			this.line61.Top = 0.2389763F;
			this.line61.Width = 0F;
			this.line61.X1 = 7.086614F;
			this.line61.X2 = 7.086614F;
			this.line61.Y1 = 0.2389763F;
			this.line61.Y2 = 0.6822835F;
			// 
			// line62
			// 
			this.line62.Height = 0F;
			this.line62.Left = 0.007874016F;
			this.line62.LineWeight = 1F;
			this.line62.Name = "line62";
			this.line62.Top = 0.6822835F;
			this.line62.Width = 7.07874F;
			this.line62.X1 = 7.086614F;
			this.line62.X2 = 0.007874016F;
			this.line62.Y1 = 0.6822835F;
			this.line62.Y2 = 0.6822835F;
			// 
			// pageBreak2
			// 
			this.pageBreak2.Height = 0.01F;
			this.pageBreak2.Left = 0F;
			this.pageBreak2.Name = "pageBreak2";
			this.pageBreak2.Size = new System.Drawing.SizeF(6F, 0.01F);
			this.pageBreak2.Top = 0F;
			this.pageBreak2.Width = 6F;
			// 
			// line93
			// 
			this.line93.Height = 0.4440945F;
			this.line93.Left = 0.007874016F;
			this.line93.LineWeight = 1F;
			this.line93.Name = "line93";
			this.line93.Top = 0.238189F;
			this.line93.Width = 0F;
			this.line93.X1 = 0.007874016F;
			this.line93.X2 = 0.007874016F;
			this.line93.Y1 = 0.238189F;
			this.line93.Y2 = 0.6822835F;
			// 
			// group05_F
			// 
			this.group05_F.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtScore295,
            this.txtScore297,
            this.txtScore298,
            this.txtScore299,
            this.txtScore300,
            this.txtScore296,
            this.txtScore301,
            this.txtScore303,
            this.txtScore304,
            this.txtScore305,
            this.txtScore306,
            this.txtScore302,
            this.txtScore307,
            this.txtScore309,
            this.txtScore310,
            this.txtScore311,
            this.txtScore312,
            this.txtScore308,
            this.txtScore313,
            this.txtScore315,
            this.txtScore316,
            this.txtScore317,
            this.txtScore318,
            this.txtScore314,
            this.txtScore319,
            this.txtScore321,
            this.txtScore322,
            this.txtScore323,
            this.txtScore324,
            this.txtScore320,
            this.txtScore325,
            this.txtScore327,
            this.txtScore328,
            this.txtScore329,
            this.txtScore330,
            this.txtScore326,
            this.txtScore331,
            this.txtScore333,
            this.txtScore334,
            this.txtScore335,
            this.txtScore336,
            this.txtScore332,
            this.txtScore337,
            this.txtScore339,
            this.txtScore340,
            this.txtScore341,
            this.txtScore342,
            this.txtScore338,
            this.txtScore343,
            this.txtScore345,
            this.txtScore346,
            this.txtScore347,
            this.txtScore348,
            this.txtScore344,
            this.txtScore349,
            this.line48,
            this.line49,
            this.line50,
            this.line156,
            this.line157,
            this.line158,
            this.line159,
            this.line161,
            this.line162,
            this.line163,
            this.line164,
            this.line165,
            this.line166,
            this.line167,
            this.line168,
            this.line169,
            this.line170});
			this.group05_F.Height = 3.239583F;
			this.group05_F.Name = "group05_F";
			// 
			// txtScore295
			// 
			this.txtScore295.DataField = "ITEM02";
			this.txtScore295.Height = 0.2393701F;
			this.txtScore295.Left = 0.06220473F;
			this.txtScore295.MultiLine = false;
			this.txtScore295.Name = "txtScore295";
			this.txtScore295.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore295.Text = "ITEM02";
			this.txtScore295.Top = 0F;
			this.txtScore295.Width = 1.958268F;
			// 
			// txtScore297
			// 
			this.txtScore297.DataField = "ITEM04";
			this.txtScore297.Height = 0.2393701F;
			this.txtScore297.Left = 3.310631F;
			this.txtScore297.MultiLine = false;
			this.txtScore297.Name = "txtScore297";
			this.txtScore297.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore297.Text = "004";
			this.txtScore297.Top = 0F;
			this.txtScore297.Width = 1.213385F;
			// 
			// txtScore298
			// 
			this.txtScore298.DataField = "ITEM05";
			this.txtScore298.Height = 0.2393701F;
			this.txtScore298.Left = 4.572048F;
			this.txtScore298.MultiLine = false;
			this.txtScore298.Name = "txtScore298";
			this.txtScore298.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore298.Text = "005";
			this.txtScore298.Top = 0F;
			this.txtScore298.Width = 1.214566F;
			// 
			// txtScore299
			// 
			this.txtScore299.DataField = "ITEM06";
			this.txtScore299.Height = 0.2393701F;
			this.txtScore299.Left = 5.835433F;
			this.txtScore299.MultiLine = false;
			this.txtScore299.Name = "txtScore299";
			this.txtScore299.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore299.Text = "006";
			this.txtScore299.Top = 0F;
			this.txtScore299.Width = 1.208269F;
			// 
			// txtScore300
			// 
			this.txtScore300.DataField = "ITEM07";
			this.txtScore300.Height = 0.2393701F;
			this.txtScore300.Left = 0.06220473F;
			this.txtScore300.MultiLine = false;
			this.txtScore300.Name = "txtScore300";
			this.txtScore300.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore300.Text = "ITEM07";
			this.txtScore300.Top = 0.2393701F;
			this.txtScore300.Width = 1.958268F;
			// 
			// txtScore296
			// 
			this.txtScore296.DataField = "ITEM03";
			this.txtScore296.Height = 0.2393701F;
			this.txtScore296.Left = 2.020473F;
			this.txtScore296.MultiLine = false;
			this.txtScore296.Name = "txtScore296";
			this.txtScore296.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore296.Text = "003";
			this.txtScore296.Top = 0F;
			this.txtScore296.Width = 1.250788F;
			// 
			// txtScore301
			// 
			this.txtScore301.DataField = "ITEM08";
			this.txtScore301.Height = 0.2393701F;
			this.txtScore301.Left = 2.020473F;
			this.txtScore301.MultiLine = false;
			this.txtScore301.Name = "txtScore301";
			this.txtScore301.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore301.Text = "008";
			this.txtScore301.Top = 0.2393701F;
			this.txtScore301.Width = 1.250788F;
			// 
			// txtScore303
			// 
			this.txtScore303.DataField = "ITEM10";
			this.txtScore303.Height = 0.2393701F;
			this.txtScore303.Left = 4.572048F;
			this.txtScore303.MultiLine = false;
			this.txtScore303.Name = "txtScore303";
			this.txtScore303.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore303.Text = "010";
			this.txtScore303.Top = 0.2393701F;
			this.txtScore303.Width = 1.21496F;
			// 
			// txtScore304
			// 
			this.txtScore304.DataField = "ITEM11";
			this.txtScore304.Height = 0.2393701F;
			this.txtScore304.Left = 5.835433F;
			this.txtScore304.MultiLine = false;
			this.txtScore304.Name = "txtScore304";
			this.txtScore304.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore304.Text = "011";
			this.txtScore304.Top = 0.2393701F;
			this.txtScore304.Width = 1.208269F;
			// 
			// txtScore305
			// 
			this.txtScore305.DataField = "ITEM12";
			this.txtScore305.Height = 0.2393701F;
			this.txtScore305.Left = 0.06220473F;
			this.txtScore305.MultiLine = false;
			this.txtScore305.Name = "txtScore305";
			this.txtScore305.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore305.Text = "ITEM12";
			this.txtScore305.Top = 0.4787402F;
			this.txtScore305.Width = 1.958268F;
			// 
			// txtScore306
			// 
			this.txtScore306.DataField = "ITEM13";
			this.txtScore306.Height = 0.2393701F;
			this.txtScore306.Left = 2.020473F;
			this.txtScore306.MultiLine = false;
			this.txtScore306.Name = "txtScore306";
			this.txtScore306.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore306.Text = "013";
			this.txtScore306.Top = 0.4787402F;
			this.txtScore306.Width = 1.250787F;
			// 
			// txtScore302
			// 
			this.txtScore302.DataField = "ITEM09";
			this.txtScore302.Height = 0.2393701F;
			this.txtScore302.Left = 3.310631F;
			this.txtScore302.MultiLine = false;
			this.txtScore302.Name = "txtScore302";
			this.txtScore302.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore302.Text = "009";
			this.txtScore302.Top = 0.2393701F;
			this.txtScore302.Width = 1.213385F;
			// 
			// txtScore307
			// 
			this.txtScore307.DataField = "ITEM14";
			this.txtScore307.Height = 0.2393701F;
			this.txtScore307.Left = 3.310631F;
			this.txtScore307.MultiLine = false;
			this.txtScore307.Name = "txtScore307";
			this.txtScore307.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore307.Text = "014";
			this.txtScore307.Top = 0.4787401F;
			this.txtScore307.Width = 1.212597F;
			// 
			// txtScore309
			// 
			this.txtScore309.DataField = "ITEM16";
			this.txtScore309.Height = 0.2393701F;
			this.txtScore309.Left = 5.835433F;
			this.txtScore309.MultiLine = false;
			this.txtScore309.Name = "txtScore309";
			this.txtScore309.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore309.Text = "016";
			this.txtScore309.Top = 0.4787401F;
			this.txtScore309.Width = 1.208269F;
			// 
			// txtScore310
			// 
			this.txtScore310.DataField = "ITEM17";
			this.txtScore310.Height = 0.2393701F;
			this.txtScore310.Left = 0.06220473F;
			this.txtScore310.MultiLine = false;
			this.txtScore310.Name = "txtScore310";
			this.txtScore310.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore310.Text = "ITEM17";
			this.txtScore310.Top = 0.7181104F;
			this.txtScore310.Width = 1.958268F;
			// 
			// txtScore311
			// 
			this.txtScore311.DataField = "ITEM18";
			this.txtScore311.Height = 0.2393701F;
			this.txtScore311.Left = 2.020473F;
			this.txtScore311.MultiLine = false;
			this.txtScore311.Name = "txtScore311";
			this.txtScore311.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore311.Text = "018";
			this.txtScore311.Top = 0.7181104F;
			this.txtScore311.Width = 1.250787F;
			// 
			// txtScore312
			// 
			this.txtScore312.DataField = "ITEM19";
			this.txtScore312.Height = 0.2393701F;
			this.txtScore312.Left = 3.310631F;
			this.txtScore312.MultiLine = false;
			this.txtScore312.Name = "txtScore312";
			this.txtScore312.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore312.Text = "019";
			this.txtScore312.Top = 0.7181104F;
			this.txtScore312.Width = 1.213385F;
			// 
			// txtScore308
			// 
			this.txtScore308.DataField = "ITEM15";
			this.txtScore308.Height = 0.2393701F;
			this.txtScore308.Left = 4.572836F;
			this.txtScore308.MultiLine = false;
			this.txtScore308.Name = "txtScore308";
			this.txtScore308.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore308.Text = "015";
			this.txtScore308.Top = 0.4787401F;
			this.txtScore308.Width = 1.213778F;
			// 
			// txtScore313
			// 
			this.txtScore313.DataField = "ITEM20";
			this.txtScore313.Height = 0.2393701F;
			this.txtScore313.Left = 4.572048F;
			this.txtScore313.MultiLine = false;
			this.txtScore313.Name = "txtScore313";
			this.txtScore313.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore313.Text = "020";
			this.txtScore313.Top = 0.7181104F;
			this.txtScore313.Width = 1.21496F;
			// 
			// txtScore315
			// 
			this.txtScore315.DataField = "ITEM22";
			this.txtScore315.Height = 0.2393701F;
			this.txtScore315.Left = 0.06259848F;
			this.txtScore315.MultiLine = false;
			this.txtScore315.Name = "txtScore315";
			this.txtScore315.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore315.Text = "ITEM22";
			this.txtScore315.Top = 0.9574805F;
			this.txtScore315.Width = 1.957874F;
			// 
			// txtScore316
			// 
			this.txtScore316.DataField = "ITEM23";
			this.txtScore316.Height = 0.2393701F;
			this.txtScore316.Left = 2.020473F;
			this.txtScore316.MultiLine = false;
			this.txtScore316.Name = "txtScore316";
			this.txtScore316.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore316.Text = "023";
			this.txtScore316.Top = 0.9574805F;
			this.txtScore316.Width = 1.250787F;
			// 
			// txtScore317
			// 
			this.txtScore317.DataField = "ITEM24";
			this.txtScore317.Height = 0.2393701F;
			this.txtScore317.Left = 3.310631F;
			this.txtScore317.MultiLine = false;
			this.txtScore317.Name = "txtScore317";
			this.txtScore317.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore317.Text = "024";
			this.txtScore317.Top = 0.9574805F;
			this.txtScore317.Width = 1.213385F;
			// 
			// txtScore318
			// 
			this.txtScore318.DataField = "ITEM25";
			this.txtScore318.Height = 0.2393701F;
			this.txtScore318.Left = 4.572048F;
			this.txtScore318.MultiLine = false;
			this.txtScore318.Name = "txtScore318";
			this.txtScore318.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore318.Text = "025";
			this.txtScore318.Top = 0.9574805F;
			this.txtScore318.Width = 1.21496F;
			// 
			// txtScore314
			// 
			this.txtScore314.DataField = "ITEM21";
			this.txtScore314.Height = 0.2393701F;
			this.txtScore314.Left = 5.835433F;
			this.txtScore314.MultiLine = false;
			this.txtScore314.Name = "txtScore314";
			this.txtScore314.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore314.Text = "021";
			this.txtScore314.Top = 0.7181104F;
			this.txtScore314.Width = 1.208269F;
			// 
			// txtScore319
			// 
			this.txtScore319.DataField = "ITEM26";
			this.txtScore319.Height = 0.2393701F;
			this.txtScore319.Left = 5.835433F;
			this.txtScore319.MultiLine = false;
			this.txtScore319.Name = "txtScore319";
			this.txtScore319.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore319.Text = "026";
			this.txtScore319.Top = 0.9574805F;
			this.txtScore319.Width = 1.208268F;
			// 
			// txtScore321
			// 
			this.txtScore321.DataField = "ITEM28";
			this.txtScore321.Height = 0.2393701F;
			this.txtScore321.Left = 2.020473F;
			this.txtScore321.MultiLine = false;
			this.txtScore321.Name = "txtScore321";
			this.txtScore321.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore321.Text = "028";
			this.txtScore321.Top = 1.196851F;
			this.txtScore321.Width = 1.250787F;
			// 
			// txtScore322
			// 
			this.txtScore322.DataField = "ITEM29";
			this.txtScore322.Height = 0.2393701F;
			this.txtScore322.Left = 3.310631F;
			this.txtScore322.MultiLine = false;
			this.txtScore322.Name = "txtScore322";
			this.txtScore322.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore322.Text = "029";
			this.txtScore322.Top = 1.196851F;
			this.txtScore322.Width = 1.213385F;
			// 
			// txtScore323
			// 
			this.txtScore323.DataField = "ITEM30";
			this.txtScore323.Height = 0.2393701F;
			this.txtScore323.Left = 4.572048F;
			this.txtScore323.MultiLine = false;
			this.txtScore323.Name = "txtScore323";
			this.txtScore323.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore323.Text = "030";
			this.txtScore323.Top = 1.196851F;
			this.txtScore323.Width = 1.214566F;
			// 
			// txtScore324
			// 
			this.txtScore324.DataField = "ITEM31";
			this.txtScore324.Height = 0.2393701F;
			this.txtScore324.Left = 5.835433F;
			this.txtScore324.MultiLine = false;
			this.txtScore324.Name = "txtScore324";
			this.txtScore324.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore324.Text = "031";
			this.txtScore324.Top = 1.196851F;
			this.txtScore324.Width = 1.208269F;
			// 
			// txtScore320
			// 
			this.txtScore320.DataField = "ITEM27";
			this.txtScore320.Height = 0.2393701F;
			this.txtScore320.Left = 0.06220473F;
			this.txtScore320.MultiLine = false;
			this.txtScore320.Name = "txtScore320";
			this.txtScore320.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore320.Text = "ITEM27";
			this.txtScore320.Top = 1.196851F;
			this.txtScore320.Width = 1.958268F;
			// 
			// txtScore325
			// 
			this.txtScore325.DataField = "ITEM32";
			this.txtScore325.Height = 0.2393701F;
			this.txtScore325.Left = 0.06220473F;
			this.txtScore325.MultiLine = false;
			this.txtScore325.Name = "txtScore325";
			this.txtScore325.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore325.Text = "ITEM32";
			this.txtScore325.Top = 1.43622F;
			this.txtScore325.Width = 1.958268F;
			// 
			// txtScore327
			// 
			this.txtScore327.DataField = "ITEM34";
			this.txtScore327.Height = 0.2393701F;
			this.txtScore327.Left = 3.311025F;
			this.txtScore327.MultiLine = false;
			this.txtScore327.Name = "txtScore327";
			this.txtScore327.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore327.Text = "034";
			this.txtScore327.Top = 1.43622F;
			this.txtScore327.Width = 1.213385F;
			// 
			// txtScore328
			// 
			this.txtScore328.DataField = "ITEM35";
			this.txtScore328.Height = 0.2393701F;
			this.txtScore328.Left = 4.572048F;
			this.txtScore328.MultiLine = false;
			this.txtScore328.Name = "txtScore328";
			this.txtScore328.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore328.Text = "035";
			this.txtScore328.Top = 1.43622F;
			this.txtScore328.Width = 1.214566F;
			// 
			// txtScore329
			// 
			this.txtScore329.DataField = "ITEM36";
			this.txtScore329.Height = 0.2393701F;
			this.txtScore329.Left = 5.835433F;
			this.txtScore329.MultiLine = false;
			this.txtScore329.Name = "txtScore329";
			this.txtScore329.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore329.Text = "036";
			this.txtScore329.Top = 1.43622F;
			this.txtScore329.Width = 1.208269F;
			// 
			// txtScore330
			// 
			this.txtScore330.DataField = "ITEM37";
			this.txtScore330.Height = 0.2393701F;
			this.txtScore330.Left = 0.06259848F;
			this.txtScore330.MultiLine = false;
			this.txtScore330.Name = "txtScore330";
			this.txtScore330.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore330.Text = "ITEM37";
			this.txtScore330.Top = 1.675591F;
			this.txtScore330.Width = 1.957874F;
			// 
			// txtScore326
			// 
			this.txtScore326.DataField = "ITEM33";
			this.txtScore326.Height = 0.2393701F;
			this.txtScore326.Left = 2.020473F;
			this.txtScore326.MultiLine = false;
			this.txtScore326.Name = "txtScore326";
			this.txtScore326.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore326.Text = "033";
			this.txtScore326.Top = 1.43622F;
			this.txtScore326.Width = 1.250787F;
			// 
			// txtScore331
			// 
			this.txtScore331.DataField = "ITEM38";
			this.txtScore331.Height = 0.2393701F;
			this.txtScore331.Left = 2.020473F;
			this.txtScore331.MultiLine = false;
			this.txtScore331.Name = "txtScore331";
			this.txtScore331.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore331.Text = "038";
			this.txtScore331.Top = 1.675591F;
			this.txtScore331.Width = 1.250787F;
			// 
			// txtScore333
			// 
			this.txtScore333.DataField = "ITEM40";
			this.txtScore333.Height = 0.2393701F;
			this.txtScore333.Left = 4.572441F;
			this.txtScore333.MultiLine = false;
			this.txtScore333.Name = "txtScore333";
			this.txtScore333.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore333.Text = "040";
			this.txtScore333.Top = 1.675591F;
			this.txtScore333.Width = 1.214172F;
			// 
			// txtScore334
			// 
			this.txtScore334.DataField = "ITEM41";
			this.txtScore334.Height = 0.2393701F;
			this.txtScore334.Left = 5.835433F;
			this.txtScore334.MultiLine = false;
			this.txtScore334.Name = "txtScore334";
			this.txtScore334.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore334.Text = "041";
			this.txtScore334.Top = 1.675591F;
			this.txtScore334.Width = 1.208269F;
			// 
			// txtScore335
			// 
			this.txtScore335.DataField = "ITEM42";
			this.txtScore335.Height = 0.2393701F;
			this.txtScore335.Left = 0.06259848F;
			this.txtScore335.MultiLine = false;
			this.txtScore335.Name = "txtScore335";
			this.txtScore335.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore335.Text = "ITEM42";
			this.txtScore335.Top = 1.914961F;
			this.txtScore335.Width = 1.957874F;
			// 
			// txtScore336
			// 
			this.txtScore336.DataField = "ITEM43";
			this.txtScore336.Height = 0.2393701F;
			this.txtScore336.Left = 2.020473F;
			this.txtScore336.MultiLine = false;
			this.txtScore336.Name = "txtScore336";
			this.txtScore336.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore336.Text = "043";
			this.txtScore336.Top = 1.914961F;
			this.txtScore336.Width = 1.250787F;
			// 
			// txtScore332
			// 
			this.txtScore332.DataField = "ITEM39";
			this.txtScore332.Height = 0.2393701F;
			this.txtScore332.Left = 3.310631F;
			this.txtScore332.MultiLine = false;
			this.txtScore332.Name = "txtScore332";
			this.txtScore332.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore332.Text = "039";
			this.txtScore332.Top = 1.675591F;
			this.txtScore332.Width = 1.213385F;
			// 
			// txtScore337
			// 
			this.txtScore337.DataField = "ITEM44";
			this.txtScore337.Height = 0.2393701F;
			this.txtScore337.Left = 3.311025F;
			this.txtScore337.MultiLine = false;
			this.txtScore337.Name = "txtScore337";
			this.txtScore337.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore337.Text = "044";
			this.txtScore337.Top = 1.914961F;
			this.txtScore337.Width = 1.212991F;
			// 
			// txtScore339
			// 
			this.txtScore339.DataField = "ITEM46";
			this.txtScore339.Height = 0.2393701F;
			this.txtScore339.Left = 5.835825F;
			this.txtScore339.MultiLine = false;
			this.txtScore339.Name = "txtScore339";
			this.txtScore339.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore339.Text = "046";
			this.txtScore339.Top = 1.914961F;
			this.txtScore339.Width = 1.207876F;
			// 
			// txtScore340
			// 
			this.txtScore340.DataField = "ITEM47";
			this.txtScore340.Height = 0.2393701F;
			this.txtScore340.Left = 0.06220473F;
			this.txtScore340.MultiLine = false;
			this.txtScore340.Name = "txtScore340";
			this.txtScore340.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore340.Text = "ITEM47";
			this.txtScore340.Top = 2.154331F;
			this.txtScore340.Width = 1.958268F;
			// 
			// txtScore341
			// 
			this.txtScore341.DataField = "ITEM48";
			this.txtScore341.Height = 0.2393701F;
			this.txtScore341.Left = 2.020473F;
			this.txtScore341.MultiLine = false;
			this.txtScore341.Name = "txtScore341";
			this.txtScore341.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore341.Text = "048";
			this.txtScore341.Top = 2.154331F;
			this.txtScore341.Width = 1.250787F;
			// 
			// txtScore342
			// 
			this.txtScore342.DataField = "ITEM49";
			this.txtScore342.Height = 0.2393701F;
			this.txtScore342.Left = 3.310631F;
			this.txtScore342.MultiLine = false;
			this.txtScore342.Name = "txtScore342";
			this.txtScore342.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore342.Text = "049";
			this.txtScore342.Top = 2.154331F;
			this.txtScore342.Width = 1.212598F;
			// 
			// txtScore338
			// 
			this.txtScore338.DataField = "ITEM45";
			this.txtScore338.Height = 0.2393701F;
			this.txtScore338.Left = 4.572048F;
			this.txtScore338.MultiLine = false;
			this.txtScore338.Name = "txtScore338";
			this.txtScore338.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore338.Text = "045";
			this.txtScore338.Top = 1.914961F;
			this.txtScore338.Width = 1.214566F;
			// 
			// txtScore343
			// 
			this.txtScore343.DataField = "ITEM50";
			this.txtScore343.Height = 0.2393701F;
			this.txtScore343.Left = 4.571261F;
			this.txtScore343.MultiLine = false;
			this.txtScore343.Name = "txtScore343";
			this.txtScore343.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore343.Text = "050";
			this.txtScore343.Top = 2.154331F;
			this.txtScore343.Width = 1.215353F;
			// 
			// txtScore345
			// 
			this.txtScore345.DataField = "ITEM52";
			this.txtScore345.Height = 0.2393701F;
			this.txtScore345.Left = 0.06259848F;
			this.txtScore345.MultiLine = false;
			this.txtScore345.Name = "txtScore345";
			this.txtScore345.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore345.Text = "ITEM52";
			this.txtScore345.Top = 2.393701F;
			this.txtScore345.Width = 1.957874F;
			// 
			// txtScore346
			// 
			this.txtScore346.DataField = "ITEM53";
			this.txtScore346.Height = 0.2393701F;
			this.txtScore346.Left = 2.020473F;
			this.txtScore346.MultiLine = false;
			this.txtScore346.Name = "txtScore346";
			this.txtScore346.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore346.Text = "053";
			this.txtScore346.Top = 2.393701F;
			this.txtScore346.Width = 1.250788F;
			// 
			// txtScore347
			// 
			this.txtScore347.DataField = "ITEM54";
			this.txtScore347.Height = 0.2393701F;
			this.txtScore347.Left = 3.311025F;
			this.txtScore347.MultiLine = false;
			this.txtScore347.Name = "txtScore347";
			this.txtScore347.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore347.Text = "054";
			this.txtScore347.Top = 2.393701F;
			this.txtScore347.Width = 1.212991F;
			// 
			// txtScore348
			// 
			this.txtScore348.DataField = "ITEM55";
			this.txtScore348.Height = 0.2393701F;
			this.txtScore348.Left = 4.572048F;
			this.txtScore348.MultiLine = false;
			this.txtScore348.Name = "txtScore348";
			this.txtScore348.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore348.Text = "055";
			this.txtScore348.Top = 2.393701F;
			this.txtScore348.Width = 1.214566F;
			// 
			// txtScore344
			// 
			this.txtScore344.DataField = "ITEM51";
			this.txtScore344.Height = 0.2393701F;
			this.txtScore344.Left = 5.835433F;
			this.txtScore344.MultiLine = false;
			this.txtScore344.Name = "txtScore344";
			this.txtScore344.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore344.Text = "051";
			this.txtScore344.Top = 2.154331F;
			this.txtScore344.Width = 1.208269F;
			// 
			// txtScore349
			// 
			this.txtScore349.DataField = "ITEM56";
			this.txtScore349.Height = 0.2393701F;
			this.txtScore349.Left = 5.835825F;
			this.txtScore349.MultiLine = false;
			this.txtScore349.Name = "txtScore349";
			this.txtScore349.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore349.Text = "056,456,789,000";
			this.txtScore349.Top = 2.393701F;
			this.txtScore349.Width = 1.207876F;
			// 
			// line48
			// 
			this.line48.Height = 2.633071F;
			this.line48.Left = 7.086614F;
			this.line48.LineWeight = 1F;
			this.line48.Name = "line48";
			this.line48.Top = 0F;
			this.line48.Width = 0F;
			this.line48.X1 = 7.086614F;
			this.line48.X2 = 7.086614F;
			this.line48.Y1 = 2.633071F;
			this.line48.Y2 = 0F;
			// 
			// line49
			// 
			this.line49.Height = 0F;
			this.line49.Left = 0.007874016F;
			this.line49.LineWeight = 1F;
			this.line49.Name = "line49";
			this.line49.Top = 2.633071F;
			this.line49.Width = 7.07874F;
			this.line49.X1 = 7.086614F;
			this.line49.X2 = 0.007874016F;
			this.line49.Y1 = 2.633071F;
			this.line49.Y2 = 2.633071F;
			// 
			// line50
			// 
			this.line50.Height = 2.633071F;
			this.line50.Left = 0.007874016F;
			this.line50.LineWeight = 1F;
			this.line50.Name = "line50";
			this.line50.Top = 0F;
			this.line50.Width = 0F;
			this.line50.X1 = 0.007874016F;
			this.line50.X2 = 0.007874016F;
			this.line50.Y1 = 2.633071F;
			this.line50.Y2 = 0F;
			// 
			// line156
			// 
			this.line156.Height = 2.633071F;
			this.line156.Left = 2.020473F;
			this.line156.LineWeight = 1F;
			this.line156.Name = "line156";
			this.line156.Top = 0F;
			this.line156.Width = 0F;
			this.line156.X1 = 2.020473F;
			this.line156.X2 = 2.020473F;
			this.line156.Y1 = 2.633071F;
			this.line156.Y2 = 0F;
			// 
			// line157
			// 
			this.line157.Height = 2.633071F;
			this.line157.Left = 3.315355F;
			this.line157.LineWeight = 1F;
			this.line157.Name = "line157";
			this.line157.Top = 0F;
			this.line157.Width = 0F;
			this.line157.X1 = 3.315355F;
			this.line157.X2 = 3.315355F;
			this.line157.Y1 = 2.633071F;
			this.line157.Y2 = 0F;
			// 
			// line158
			// 
			this.line158.Height = 2.633071F;
			this.line158.Left = 4.57126F;
			this.line158.LineWeight = 1F;
			this.line158.Name = "line158";
			this.line158.Top = 0F;
			this.line158.Width = 0F;
			this.line158.X1 = 4.57126F;
			this.line158.X2 = 4.57126F;
			this.line158.Y1 = 2.633071F;
			this.line158.Y2 = 0F;
			// 
			// line159
			// 
			this.line159.Height = 2.633071F;
			this.line159.Left = 5.840158F;
			this.line159.LineWeight = 1F;
			this.line159.Name = "line159";
			this.line159.Top = 0F;
			this.line159.Width = 0F;
			this.line159.X1 = 5.840158F;
			this.line159.X2 = 5.840158F;
			this.line159.Y1 = 2.633071F;
			this.line159.Y2 = 0F;
			// 
			// line161
			// 
			this.line161.Height = 0F;
			this.line161.Left = 0.007877827F;
			this.line161.LineWeight = 1F;
			this.line161.Name = "line161";
			this.line161.Top = 0.2393701F;
			this.line161.Width = 7.078737F;
			this.line161.X1 = 7.086615F;
			this.line161.X2 = 0.007877827F;
			this.line161.Y1 = 0.2393701F;
			this.line161.Y2 = 0.2393701F;
			// 
			// line162
			// 
			this.line162.Height = 0F;
			this.line162.Left = 0.007877827F;
			this.line162.LineWeight = 1F;
			this.line162.Name = "line162";
			this.line162.Top = 0.4787402F;
			this.line162.Width = 7.078737F;
			this.line162.X1 = 7.086615F;
			this.line162.X2 = 0.007877827F;
			this.line162.Y1 = 0.4787402F;
			this.line162.Y2 = 0.4787402F;
			// 
			// line163
			// 
			this.line163.Height = 0F;
			this.line163.Left = 0.007877827F;
			this.line163.LineWeight = 1F;
			this.line163.Name = "line163";
			this.line163.Top = 0.7181103F;
			this.line163.Width = 7.078737F;
			this.line163.X1 = 7.086615F;
			this.line163.X2 = 0.007877827F;
			this.line163.Y1 = 0.7181103F;
			this.line163.Y2 = 0.7181103F;
			// 
			// line164
			// 
			this.line164.Height = 0F;
			this.line164.Left = 0.007877827F;
			this.line164.LineWeight = 1F;
			this.line164.Name = "line164";
			this.line164.Top = 0.9574804F;
			this.line164.Width = 7.078737F;
			this.line164.X1 = 7.086615F;
			this.line164.X2 = 0.007877827F;
			this.line164.Y1 = 0.9574804F;
			this.line164.Y2 = 0.9574804F;
			// 
			// line165
			// 
			this.line165.Height = 0F;
			this.line165.Left = 0.007877827F;
			this.line165.LineWeight = 1F;
			this.line165.Name = "line165";
			this.line165.Top = 2.393701F;
			this.line165.Width = 7.078737F;
			this.line165.X1 = 7.086615F;
			this.line165.X2 = 0.007877827F;
			this.line165.Y1 = 2.393701F;
			this.line165.Y2 = 2.393701F;
			// 
			// line166
			// 
			this.line166.Height = 0F;
			this.line166.Left = 0.007877827F;
			this.line166.LineWeight = 1F;
			this.line166.Name = "line166";
			this.line166.Top = 2.154331F;
			this.line166.Width = 7.078737F;
			this.line166.X1 = 7.086615F;
			this.line166.X2 = 0.007877827F;
			this.line166.Y1 = 2.154331F;
			this.line166.Y2 = 2.154331F;
			// 
			// line167
			// 
			this.line167.Height = 0F;
			this.line167.Left = 0.007877827F;
			this.line167.LineWeight = 1F;
			this.line167.Name = "line167";
			this.line167.Top = 1.914961F;
			this.line167.Width = 7.078737F;
			this.line167.X1 = 7.086615F;
			this.line167.X2 = 0.007877827F;
			this.line167.Y1 = 1.914961F;
			this.line167.Y2 = 1.914961F;
			// 
			// line168
			// 
			this.line168.Height = 0F;
			this.line168.Left = 0.007877827F;
			this.line168.LineWeight = 1F;
			this.line168.Name = "line168";
			this.line168.Top = 1.19685F;
			this.line168.Width = 7.078737F;
			this.line168.X1 = 7.086615F;
			this.line168.X2 = 0.007877827F;
			this.line168.Y1 = 1.19685F;
			this.line168.Y2 = 1.19685F;
			// 
			// line169
			// 
			this.line169.Height = 0F;
			this.line169.Left = 0.007877827F;
			this.line169.LineWeight = 1F;
			this.line169.Name = "line169";
			this.line169.Top = 1.675591F;
			this.line169.Width = 7.078737F;
			this.line169.X1 = 7.086615F;
			this.line169.X2 = 0.007877827F;
			this.line169.Y1 = 1.675591F;
			this.line169.Y2 = 1.675591F;
			// 
			// line170
			// 
			this.line170.Height = 0F;
			this.line170.Left = 0.007877827F;
			this.line170.LineWeight = 1F;
			this.line170.Name = "line170";
			this.line170.Top = 1.436221F;
			this.line170.Width = 7.078737F;
			this.line170.X1 = 7.086615F;
			this.line170.X2 = 0.007877827F;
			this.line170.Y1 = 1.436221F;
			this.line170.Y2 = 1.436221F;
			// 
			// group06_H
			// 
			this.group06_H.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox501,
            this.textBox502,
            this.textBox503,
            this.textBox504,
            this.textBox505,
            this.textBox506,
            this.textBox507,
            this.line63,
            this.line64,
            this.line65,
            this.line66,
            this.line67,
            this.line68,
            this.line69,
            this.line92,
            this.line154});
			this.group06_H.DataField = "ITEM01";
			this.group06_H.Height = 0.6822835F;
			this.group06_H.Name = "group06_H";
			this.group06_H.Format += new System.EventHandler(this.group06_Format);
			// 
			// textBox501
			// 
			this.textBox501.Height = 0.4440945F;
			this.textBox501.Left = 0.007874017F;
			this.textBox501.MultiLine = false;
			this.textBox501.Name = "textBox501";
			this.textBox501.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: justify; vertical-align: middle";
			this.textBox501.Text = null;
			this.textBox501.Top = 0.238189F;
			this.textBox501.Width = 7.073623F;
			// 
			// textBox502
			// 
			this.textBox502.Height = 0.2389764F;
			this.textBox502.Left = 0.02716541F;
			this.textBox502.MultiLine = false;
			this.textBox502.Name = "textBox502";
			this.textBox502.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: justify; ve" +
    "rtical-align: middle";
			this.textBox502.Text = "（６）支払手形、経済事業未払金、経済事業雑負債";
			this.textBox502.Top = 0F;
			this.textBox502.Width = 4.237402F;
			// 
			// textBox503
			// 
			this.textBox503.Height = 0.4429134F;
			this.textBox503.Left = 0F;
			this.textBox503.MultiLine = false;
			this.textBox503.Name = "textBox503";
			this.textBox503.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; te" +
    "xt-justify: auto; vertical-align: middle";
			this.textBox503.Text = "科目又は部門";
			this.textBox503.Top = 0.238189F;
			this.textBox503.Width = 2.368898F;
			// 
			// textBox504
			// 
			this.textBox504.Height = 0.4433071F;
			this.textBox504.Left = 2.365355F;
			this.textBox504.MultiLine = false;
			this.textBox504.Name = "textBox504";
			this.textBox504.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox504.Text = "前年度末残高";
			this.textBox504.Top = 0.2393701F;
			this.textBox504.Width = 1.17126F;
			// 
			// textBox505
			// 
			this.textBox505.Height = 0.444882F;
			this.textBox505.Left = 3.536614F;
			this.textBox505.MultiLine = false;
			this.textBox505.Name = "textBox505";
			this.textBox505.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox505.Text = "本年度増加額";
			this.textBox505.Top = 0.238189F;
			this.textBox505.Width = 1.182283F;
			// 
			// textBox506
			// 
			this.textBox506.Height = 0.4444882F;
			this.textBox506.Left = 4.718898F;
			this.textBox506.MultiLine = false;
			this.textBox506.Name = "textBox506";
			this.textBox506.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox506.Text = "本年度減少額";
			this.textBox506.Top = 0.238189F;
			this.textBox506.Width = 1.201181F;
			// 
			// textBox507
			// 
			this.textBox507.Height = 0.4448821F;
			this.textBox507.Left = 5.920079F;
			this.textBox507.MultiLine = false;
			this.textBox507.Name = "textBox507";
			this.textBox507.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox507.Text = "本年度末残高";
			this.textBox507.Top = 0.2389764F;
			this.textBox507.Width = 1.166536F;
			// 
			// line63
			// 
			this.line63.Height = 0F;
			this.line63.Left = 0.007874016F;
			this.line63.LineWeight = 1F;
			this.line63.Name = "line63";
			this.line63.Top = 0.2389764F;
			this.line63.Width = 7.07874F;
			this.line63.X1 = 7.086614F;
			this.line63.X2 = 0.007874016F;
			this.line63.Y1 = 0.2389764F;
			this.line63.Y2 = 0.2389764F;
			// 
			// line64
			// 
			this.line64.Height = 0F;
			this.line64.Left = 0.007874016F;
			this.line64.LineWeight = 1F;
			this.line64.Name = "line64";
			this.line64.Top = 0.6822835F;
			this.line64.Width = 7.07874F;
			this.line64.X1 = 7.086614F;
			this.line64.X2 = 0.007874016F;
			this.line64.Y1 = 0.6822835F;
			this.line64.Y2 = 0.6822835F;
			// 
			// line65
			// 
			this.line65.Height = 0.4440945F;
			this.line65.Left = 5.918898F;
			this.line65.LineWeight = 1F;
			this.line65.Name = "line65";
			this.line65.Top = 0.238189F;
			this.line65.Width = 0F;
			this.line65.X1 = 5.918898F;
			this.line65.X2 = 5.918898F;
			this.line65.Y1 = 0.238189F;
			this.line65.Y2 = 0.6822835F;
			// 
			// line66
			// 
			this.line66.Height = 0.4440945F;
			this.line66.Left = 4.718898F;
			this.line66.LineWeight = 1F;
			this.line66.Name = "line66";
			this.line66.Top = 0.238189F;
			this.line66.Width = 0F;
			this.line66.X1 = 4.718898F;
			this.line66.X2 = 4.718898F;
			this.line66.Y1 = 0.238189F;
			this.line66.Y2 = 0.6822835F;
			// 
			// line67
			// 
			this.line67.Height = 0.4437008F;
			this.line67.Left = 3.536614F;
			this.line67.LineWeight = 1F;
			this.line67.Name = "line67";
			this.line67.Top = 0.2385827F;
			this.line67.Width = 0F;
			this.line67.X1 = 3.536614F;
			this.line67.X2 = 3.536614F;
			this.line67.Y1 = 0.2385827F;
			this.line67.Y2 = 0.6822835F;
			// 
			// line68
			// 
			this.line68.Height = 0.4429134F;
			this.line68.Left = 2.365354F;
			this.line68.LineWeight = 1F;
			this.line68.Name = "line68";
			this.line68.Top = 0.2393701F;
			this.line68.Width = 9.536743E-07F;
			this.line68.X1 = 2.365355F;
			this.line68.X2 = 2.365354F;
			this.line68.Y1 = 0.2393701F;
			this.line68.Y2 = 0.6822835F;
			// 
			// line69
			// 
			this.line69.Height = 0.4433071F;
			this.line69.Left = 7.086614F;
			this.line69.LineWeight = 1F;
			this.line69.Name = "line69";
			this.line69.Top = 0.2389764F;
			this.line69.Width = 0F;
			this.line69.X1 = 7.086614F;
			this.line69.X2 = 7.086614F;
			this.line69.Y1 = 0.2389764F;
			this.line69.Y2 = 0.6822835F;
			// 
			// line92
			// 
			this.line92.Height = 0.4429134F;
			this.line92.Left = 0.007874016F;
			this.line92.LineWeight = 1F;
			this.line92.Name = "line92";
			this.line92.Top = 0.2393701F;
			this.line92.Width = 0F;
			this.line92.X1 = 0.007874016F;
			this.line92.X2 = 0.007874016F;
			this.line92.Y1 = 0.2393701F;
			this.line92.Y2 = 0.6822835F;
			// 
			// line154
			// 
			this.line154.Height = 0.4429133F;
			this.line154.Left = 0.007874016F;
			this.line154.LineWeight = 1F;
			this.line154.Name = "line154";
			this.line154.Top = 0.2393701F;
			this.line154.Width = 0F;
			this.line154.X1 = 0.007874016F;
			this.line154.X2 = 0.007874016F;
			this.line154.Y1 = 0.2393701F;
			this.line154.Y2 = 0.6822834F;
			// 
			// group06_F
			// 
			this.group06_F.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox382,
            this.textBox383,
            this.txtScore350,
            this.txtScore352,
            this.txtScore353,
            this.txtScore354,
            this.txtScore355,
            this.txtScore351,
            this.txtScore356,
            this.txtScore358,
            this.txtScore359,
            this.txtScore360,
            this.txtScore361,
            this.txtScore357,
            this.txtScore362,
            this.txtScore364,
            this.txtScore365,
            this.txtScore366,
            this.txtScore367,
            this.txtScore363,
            this.txtScore368,
            this.txtScore370,
            this.txtScore371,
            this.txtScore372,
            this.txtScore373,
            this.txtScore369,
            this.txtScore374,
            this.txtScore376,
            this.txtScore377,
            this.txtScore378,
            this.txtScore379,
            this.txtScore375,
            this.txtScore380,
            this.txtScore382,
            this.txtScore383,
            this.txtScore384,
            this.txtScore385,
            this.txtScore381,
            this.txtScore386,
            this.txtScore388,
            this.txtScore389,
            this.txtScore390,
            this.txtScore391,
            this.txtScore387,
            this.txtScore392,
            this.txtScore394,
            this.txtScore395,
            this.txtScore396,
            this.txtScore397,
            this.txtScore393,
            this.txtScore398,
            this.txtScore400,
            this.txtScore401,
            this.txtScore402,
            this.txtScore403,
            this.txtScore399,
            this.txtScore404,
            this.txtScore405,
            this.txtScore407,
            this.txtScore408,
            this.txtScore409,
            this.txtScore459,
            this.txtScore406,
            this.txtScore410,
            this.txtScore412,
            this.txtScore413,
            this.txtScore414,
            this.txtScore415,
            this.txtScore411,
            this.txtScore416,
            this.txtScore418,
            this.txtScore419,
            this.txtScore420,
            this.txtScore421,
            this.txtScore417,
            this.txtScore422,
            this.txtScore424,
            this.txtScore425,
            this.txtScore426,
            this.txtScore427,
            this.txtScore423,
            this.txtScore428,
            this.txtScore430,
            this.txtScore431,
            this.txtScore432,
            this.txtScore433,
            this.txtScore429,
            this.txtScore434,
            this.txtScore436,
            this.txtScore437,
            this.txtScore438,
            this.txtScore439,
            this.txtScore435,
            this.txtScore440,
            this.txtScore442,
            this.txtScore443,
            this.txtScore444,
            this.txtScore445,
            this.txtScore441,
            this.txtScore446,
            this.txtScore448,
            this.txtScore449,
            this.txtScore450,
            this.txtScore451,
            this.txtScore447,
            this.txtScore452,
            this.txtScore454,
            this.txtScore455,
            this.txtScore456,
            this.txtScore457,
            this.txtScore453,
            this.txtScore458,
            this.line51,
            this.line52,
            this.line53,
            this.line54,
            this.line55,
            this.line130,
            this.line131,
            this.line132,
            this.line133,
            this.line134,
            this.line135,
            this.line136,
            this.line137,
            this.line138,
            this.line139,
            this.line140,
            this.line141,
            this.line142,
            this.line143,
            this.line144,
            this.line145,
            this.line146,
            this.line147,
            this.line148,
            this.line149,
            this.line150,
            this.line151,
            this.line152,
            this.line153});
			this.group06_F.Height = 5.491057F;
			this.group06_F.KeepTogether = true;
			this.group06_F.Name = "group06_F";
			// 
			// textBox382
			// 
			this.textBox382.Height = 2.633071F;
			this.textBox382.Left = 0.02755906F;
			this.textBox382.Name = "textBox382";
			this.textBox382.Style = "font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: normal; text-align: center; " +
    "vertical-align: middle; ddo-font-vertical: true";
			this.textBox382.Text = "経\r\n済\r\n事\r\n業\r\n未\r\n払\r\n金";
			this.textBox382.Top = 0F;
			this.textBox382.Width = 0.5492126F;
			// 
			// textBox383
			// 
			this.textBox383.Height = 2.633071F;
			this.textBox383.Left = 0.02755904F;
			this.textBox383.Name = "textBox383";
			this.textBox383.Style = "font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: normal; text-align: center; " +
    "vertical-align: middle; ddo-font-vertical: true";
			this.textBox383.Text = "経\r\n済\r\n事\r\n業\r\n雑\r\n負\r\n債";
			this.textBox383.Top = 2.633071F;
			this.textBox383.Width = 0.5492126F;
			// 
			// txtScore350
			// 
			this.txtScore350.DataField = "ITEM02";
			this.txtScore350.Height = 0.2393701F;
			this.txtScore350.Left = 0.6299213F;
			this.txtScore350.MultiLine = false;
			this.txtScore350.Name = "txtScore350";
			this.txtScore350.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore350.Text = "ITEM02";
			this.txtScore350.Top = 0F;
			this.txtScore350.Width = 1.708662F;
			// 
			// txtScore352
			// 
			this.txtScore352.DataField = "ITEM04";
			this.txtScore352.Height = 0.2393701F;
			this.txtScore352.Left = 3.540551F;
			this.txtScore352.MultiLine = false;
			this.txtScore352.Name = "txtScore352";
			this.txtScore352.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore352.Text = "004";
			this.txtScore352.Top = 0F;
			this.txtScore352.Width = 1.124016F;
			// 
			// txtScore353
			// 
			this.txtScore353.DataField = "ITEM05";
			this.txtScore353.Height = 0.2393701F;
			this.txtScore353.Left = 4.722835F;
			this.txtScore353.MultiLine = false;
			this.txtScore353.Name = "txtScore353";
			this.txtScore353.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore353.Text = "005";
			this.txtScore353.Top = 0F;
			this.txtScore353.Width = 1.159842F;
			// 
			// txtScore354
			// 
			this.txtScore354.DataField = "ITEM06";
			this.txtScore354.Height = 0.2393701F;
			this.txtScore354.Left = 5.91693F;
			this.txtScore354.MultiLine = false;
			this.txtScore354.Name = "txtScore354";
			this.txtScore354.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore354.Text = "006";
			this.txtScore354.Top = 0F;
			this.txtScore354.Width = 1.124016F;
			// 
			// txtScore355
			// 
			this.txtScore355.DataField = "ITEM07";
			this.txtScore355.Height = 0.2393701F;
			this.txtScore355.Left = 0.6299213F;
			this.txtScore355.MultiLine = false;
			this.txtScore355.Name = "txtScore355";
			this.txtScore355.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore355.Text = "ITEM07";
			this.txtScore355.Top = 0.2393701F;
			this.txtScore355.Width = 1.708662F;
			// 
			// txtScore351
			// 
			this.txtScore351.DataField = "ITEM03";
			this.txtScore351.Height = 0.2393701F;
			this.txtScore351.Left = 2.373622F;
			this.txtScore351.MultiLine = false;
			this.txtScore351.Name = "txtScore351";
			this.txtScore351.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore351.Text = "003";
			this.txtScore351.Top = 0F;
			this.txtScore351.Width = 1.124016F;
			// 
			// txtScore356
			// 
			this.txtScore356.DataField = "ITEM08";
			this.txtScore356.Height = 0.2393701F;
			this.txtScore356.Left = 2.373622F;
			this.txtScore356.MultiLine = false;
			this.txtScore356.Name = "txtScore356";
			this.txtScore356.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore356.Text = "008";
			this.txtScore356.Top = 0.2393701F;
			this.txtScore356.Width = 1.124016F;
			// 
			// txtScore358
			// 
			this.txtScore358.DataField = "ITEM10";
			this.txtScore358.Height = 0.2393701F;
			this.txtScore358.Left = 4.722835F;
			this.txtScore358.MultiLine = false;
			this.txtScore358.Name = "txtScore358";
			this.txtScore358.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore358.Text = "010";
			this.txtScore358.Top = 0.2393701F;
			this.txtScore358.Width = 1.159842F;
			// 
			// txtScore359
			// 
			this.txtScore359.DataField = "ITEM11";
			this.txtScore359.Height = 0.2393701F;
			this.txtScore359.Left = 5.91693F;
			this.txtScore359.MultiLine = false;
			this.txtScore359.Name = "txtScore359";
			this.txtScore359.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore359.Text = "011";
			this.txtScore359.Top = 0.2393701F;
			this.txtScore359.Width = 1.124016F;
			// 
			// txtScore360
			// 
			this.txtScore360.DataField = "ITEM12";
			this.txtScore360.Height = 0.2393701F;
			this.txtScore360.Left = 0.6299213F;
			this.txtScore360.MultiLine = false;
			this.txtScore360.Name = "txtScore360";
			this.txtScore360.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore360.Text = "ITEM12";
			this.txtScore360.Top = 0.4787402F;
			this.txtScore360.Width = 1.708662F;
			// 
			// txtScore361
			// 
			this.txtScore361.DataField = "ITEM13";
			this.txtScore361.Height = 0.2393701F;
			this.txtScore361.Left = 2.373622F;
			this.txtScore361.MultiLine = false;
			this.txtScore361.Name = "txtScore361";
			this.txtScore361.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore361.Text = "013";
			this.txtScore361.Top = 0.4787402F;
			this.txtScore361.Width = 1.124016F;
			// 
			// txtScore357
			// 
			this.txtScore357.DataField = "ITEM09";
			this.txtScore357.Height = 0.2393701F;
			this.txtScore357.Left = 3.540551F;
			this.txtScore357.MultiLine = false;
			this.txtScore357.Name = "txtScore357";
			this.txtScore357.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore357.Text = "009";
			this.txtScore357.Top = 0.2393701F;
			this.txtScore357.Width = 1.124016F;
			// 
			// txtScore362
			// 
			this.txtScore362.DataField = "ITEM14";
			this.txtScore362.Height = 0.2393701F;
			this.txtScore362.Left = 3.540551F;
			this.txtScore362.MultiLine = false;
			this.txtScore362.Name = "txtScore362";
			this.txtScore362.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore362.Text = "014";
			this.txtScore362.Top = 0.4787401F;
			this.txtScore362.Width = 1.124016F;
			// 
			// txtScore364
			// 
			this.txtScore364.DataField = "ITEM16";
			this.txtScore364.Height = 0.2393701F;
			this.txtScore364.Left = 5.91693F;
			this.txtScore364.MultiLine = false;
			this.txtScore364.Name = "txtScore364";
			this.txtScore364.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore364.Text = "016";
			this.txtScore364.Top = 0.4787401F;
			this.txtScore364.Width = 1.124016F;
			// 
			// txtScore365
			// 
			this.txtScore365.DataField = "ITEM17";
			this.txtScore365.Height = 0.2393701F;
			this.txtScore365.Left = 0.6299213F;
			this.txtScore365.MultiLine = false;
			this.txtScore365.Name = "txtScore365";
			this.txtScore365.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore365.Text = "ITEM17";
			this.txtScore365.Top = 0.7181104F;
			this.txtScore365.Width = 1.708662F;
			// 
			// txtScore366
			// 
			this.txtScore366.DataField = "ITEM18";
			this.txtScore366.Height = 0.2393701F;
			this.txtScore366.Left = 2.373622F;
			this.txtScore366.MultiLine = false;
			this.txtScore366.Name = "txtScore366";
			this.txtScore366.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore366.Text = "018";
			this.txtScore366.Top = 0.7181104F;
			this.txtScore366.Width = 1.124016F;
			// 
			// txtScore367
			// 
			this.txtScore367.DataField = "ITEM19";
			this.txtScore367.Height = 0.2393701F;
			this.txtScore367.Left = 3.540551F;
			this.txtScore367.MultiLine = false;
			this.txtScore367.Name = "txtScore367";
			this.txtScore367.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore367.Text = "019";
			this.txtScore367.Top = 0.7181104F;
			this.txtScore367.Width = 1.124016F;
			// 
			// txtScore363
			// 
			this.txtScore363.DataField = "ITEM15";
			this.txtScore363.Height = 0.2393701F;
			this.txtScore363.Left = 4.723623F;
			this.txtScore363.MultiLine = false;
			this.txtScore363.Name = "txtScore363";
			this.txtScore363.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore363.Text = "015";
			this.txtScore363.Top = 0.4787401F;
			this.txtScore363.Width = 1.159055F;
			// 
			// txtScore368
			// 
			this.txtScore368.DataField = "ITEM20";
			this.txtScore368.Height = 0.2393701F;
			this.txtScore368.Left = 4.722835F;
			this.txtScore368.MultiLine = false;
			this.txtScore368.Name = "txtScore368";
			this.txtScore368.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore368.Text = "020";
			this.txtScore368.Top = 0.7181104F;
			this.txtScore368.Width = 1.159842F;
			// 
			// txtScore370
			// 
			this.txtScore370.DataField = "ITEM22";
			this.txtScore370.Height = 0.2393701F;
			this.txtScore370.Left = 0.622441F;
			this.txtScore370.MultiLine = false;
			this.txtScore370.Name = "txtScore370";
			this.txtScore370.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore370.Text = "ITEM22";
			this.txtScore370.Top = 0.9574804F;
			this.txtScore370.Width = 1.708268F;
			// 
			// txtScore371
			// 
			this.txtScore371.DataField = "ITEM23";
			this.txtScore371.Height = 0.2393701F;
			this.txtScore371.Left = 2.373622F;
			this.txtScore371.MultiLine = false;
			this.txtScore371.Name = "txtScore371";
			this.txtScore371.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore371.Text = "023";
			this.txtScore371.Top = 0.9574805F;
			this.txtScore371.Width = 1.124016F;
			// 
			// txtScore372
			// 
			this.txtScore372.DataField = "ITEM24";
			this.txtScore372.Height = 0.2393701F;
			this.txtScore372.Left = 3.540551F;
			this.txtScore372.MultiLine = false;
			this.txtScore372.Name = "txtScore372";
			this.txtScore372.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore372.Text = "024";
			this.txtScore372.Top = 0.9574805F;
			this.txtScore372.Width = 1.124016F;
			// 
			// txtScore373
			// 
			this.txtScore373.DataField = "ITEM25";
			this.txtScore373.Height = 0.2393701F;
			this.txtScore373.Left = 4.722835F;
			this.txtScore373.MultiLine = false;
			this.txtScore373.Name = "txtScore373";
			this.txtScore373.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore373.Text = "025";
			this.txtScore373.Top = 0.9574805F;
			this.txtScore373.Width = 1.159842F;
			// 
			// txtScore369
			// 
			this.txtScore369.DataField = "ITEM21";
			this.txtScore369.Height = 0.2393701F;
			this.txtScore369.Left = 5.91693F;
			this.txtScore369.MultiLine = false;
			this.txtScore369.Name = "txtScore369";
			this.txtScore369.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore369.Text = "021";
			this.txtScore369.Top = 0.7181104F;
			this.txtScore369.Width = 1.124016F;
			// 
			// txtScore374
			// 
			this.txtScore374.DataField = "ITEM26";
			this.txtScore374.Height = 0.2393701F;
			this.txtScore374.Left = 5.91693F;
			this.txtScore374.MultiLine = false;
			this.txtScore374.Name = "txtScore374";
			this.txtScore374.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore374.Text = "026";
			this.txtScore374.Top = 0.9574805F;
			this.txtScore374.Width = 1.124016F;
			// 
			// txtScore376
			// 
			this.txtScore376.DataField = "ITEM28";
			this.txtScore376.Height = 0.2393701F;
			this.txtScore376.Left = 2.373622F;
			this.txtScore376.MultiLine = false;
			this.txtScore376.Name = "txtScore376";
			this.txtScore376.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore376.Text = "028";
			this.txtScore376.Top = 1.196851F;
			this.txtScore376.Width = 1.124016F;
			// 
			// txtScore377
			// 
			this.txtScore377.DataField = "ITEM29";
			this.txtScore377.Height = 0.2393701F;
			this.txtScore377.Left = 3.540551F;
			this.txtScore377.MultiLine = false;
			this.txtScore377.Name = "txtScore377";
			this.txtScore377.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore377.Text = "029";
			this.txtScore377.Top = 1.196851F;
			this.txtScore377.Width = 1.124016F;
			// 
			// txtScore378
			// 
			this.txtScore378.DataField = "ITEM30";
			this.txtScore378.Height = 0.2393701F;
			this.txtScore378.Left = 4.722835F;
			this.txtScore378.MultiLine = false;
			this.txtScore378.Name = "txtScore378";
			this.txtScore378.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore378.Text = "030";
			this.txtScore378.Top = 1.196851F;
			this.txtScore378.Width = 1.159842F;
			// 
			// txtScore379
			// 
			this.txtScore379.DataField = "ITEM31";
			this.txtScore379.Height = 0.2393701F;
			this.txtScore379.Left = 5.91693F;
			this.txtScore379.MultiLine = false;
			this.txtScore379.Name = "txtScore379";
			this.txtScore379.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore379.Text = "031";
			this.txtScore379.Top = 1.196851F;
			this.txtScore379.Width = 1.124016F;
			// 
			// txtScore375
			// 
			this.txtScore375.DataField = "ITEM27";
			this.txtScore375.Height = 0.2393701F;
			this.txtScore375.Left = 0.6299213F;
			this.txtScore375.MultiLine = false;
			this.txtScore375.Name = "txtScore375";
			this.txtScore375.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore375.Text = "ITEM27";
			this.txtScore375.Top = 1.196851F;
			this.txtScore375.Width = 1.708662F;
			// 
			// txtScore380
			// 
			this.txtScore380.DataField = "ITEM32";
			this.txtScore380.Height = 0.2393701F;
			this.txtScore380.Left = 0.6299213F;
			this.txtScore380.MultiLine = false;
			this.txtScore380.Name = "txtScore380";
			this.txtScore380.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore380.Text = "ITEM32";
			this.txtScore380.Top = 1.43622F;
			this.txtScore380.Width = 1.708662F;
			// 
			// txtScore382
			// 
			this.txtScore382.DataField = "ITEM34";
			this.txtScore382.Height = 0.2393701F;
			this.txtScore382.Left = 3.540946F;
			this.txtScore382.MultiLine = false;
			this.txtScore382.Name = "txtScore382";
			this.txtScore382.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore382.Text = "034";
			this.txtScore382.Top = 1.43622F;
			this.txtScore382.Width = 1.124016F;
			// 
			// txtScore383
			// 
			this.txtScore383.DataField = "ITEM35";
			this.txtScore383.Height = 0.2393701F;
			this.txtScore383.Left = 4.722835F;
			this.txtScore383.MultiLine = false;
			this.txtScore383.Name = "txtScore383";
			this.txtScore383.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore383.Text = "035";
			this.txtScore383.Top = 1.43622F;
			this.txtScore383.Width = 1.159842F;
			// 
			// txtScore384
			// 
			this.txtScore384.DataField = "ITEM36";
			this.txtScore384.Height = 0.2393701F;
			this.txtScore384.Left = 5.91693F;
			this.txtScore384.MultiLine = false;
			this.txtScore384.Name = "txtScore384";
			this.txtScore384.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore384.Text = "036";
			this.txtScore384.Top = 1.43622F;
			this.txtScore384.Width = 1.124016F;
			// 
			// txtScore385
			// 
			this.txtScore385.DataField = "ITEM37";
			this.txtScore385.Height = 0.2393701F;
			this.txtScore385.Left = 0.622441F;
			this.txtScore385.MultiLine = false;
			this.txtScore385.Name = "txtScore385";
			this.txtScore385.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore385.Text = "ITEM37";
			this.txtScore385.Top = 1.675591F;
			this.txtScore385.Width = 1.708268F;
			// 
			// txtScore381
			// 
			this.txtScore381.DataField = "ITEM33";
			this.txtScore381.Height = 0.2393701F;
			this.txtScore381.Left = 2.373622F;
			this.txtScore381.MultiLine = false;
			this.txtScore381.Name = "txtScore381";
			this.txtScore381.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore381.Text = "033";
			this.txtScore381.Top = 1.43622F;
			this.txtScore381.Width = 1.124016F;
			// 
			// txtScore386
			// 
			this.txtScore386.DataField = "ITEM38";
			this.txtScore386.Height = 0.2393701F;
			this.txtScore386.Left = 2.373622F;
			this.txtScore386.MultiLine = false;
			this.txtScore386.Name = "txtScore386";
			this.txtScore386.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore386.Text = "038";
			this.txtScore386.Top = 1.675591F;
			this.txtScore386.Width = 1.124016F;
			// 
			// txtScore388
			// 
			this.txtScore388.DataField = "ITEM40";
			this.txtScore388.Height = 0.2393701F;
			this.txtScore388.Left = 4.723229F;
			this.txtScore388.MultiLine = false;
			this.txtScore388.Name = "txtScore388";
			this.txtScore388.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore388.Text = "040";
			this.txtScore388.Top = 1.675591F;
			this.txtScore388.Width = 1.159448F;
			// 
			// txtScore389
			// 
			this.txtScore389.DataField = "ITEM41";
			this.txtScore389.Height = 0.2393701F;
			this.txtScore389.Left = 5.91693F;
			this.txtScore389.MultiLine = false;
			this.txtScore389.Name = "txtScore389";
			this.txtScore389.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore389.Text = "041";
			this.txtScore389.Top = 1.675591F;
			this.txtScore389.Width = 1.124016F;
			// 
			// txtScore390
			// 
			this.txtScore390.DataField = "ITEM42";
			this.txtScore390.Height = 0.2393701F;
			this.txtScore390.Left = 0.622441F;
			this.txtScore390.MultiLine = false;
			this.txtScore390.Name = "txtScore390";
			this.txtScore390.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore390.Text = "ITEM42";
			this.txtScore390.Top = 1.914961F;
			this.txtScore390.Width = 1.708268F;
			// 
			// txtScore391
			// 
			this.txtScore391.DataField = "ITEM43";
			this.txtScore391.Height = 0.2393701F;
			this.txtScore391.Left = 2.373622F;
			this.txtScore391.MultiLine = false;
			this.txtScore391.Name = "txtScore391";
			this.txtScore391.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore391.Text = "043";
			this.txtScore391.Top = 1.914961F;
			this.txtScore391.Width = 1.124016F;
			// 
			// txtScore387
			// 
			this.txtScore387.DataField = "ITEM39";
			this.txtScore387.Height = 0.2393701F;
			this.txtScore387.Left = 3.540551F;
			this.txtScore387.MultiLine = false;
			this.txtScore387.Name = "txtScore387";
			this.txtScore387.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore387.Text = "039";
			this.txtScore387.Top = 1.675591F;
			this.txtScore387.Width = 1.124016F;
			// 
			// txtScore392
			// 
			this.txtScore392.DataField = "ITEM44";
			this.txtScore392.Height = 0.2393701F;
			this.txtScore392.Left = 3.540946F;
			this.txtScore392.MultiLine = false;
			this.txtScore392.Name = "txtScore392";
			this.txtScore392.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore392.Text = "044";
			this.txtScore392.Top = 1.914961F;
			this.txtScore392.Width = 1.124016F;
			// 
			// txtScore394
			// 
			this.txtScore394.DataField = "ITEM46";
			this.txtScore394.Height = 0.2393701F;
			this.txtScore394.Left = 5.917323F;
			this.txtScore394.MultiLine = false;
			this.txtScore394.Name = "txtScore394";
			this.txtScore394.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore394.Text = "046";
			this.txtScore394.Top = 1.914961F;
			this.txtScore394.Width = 1.124016F;
			// 
			// txtScore395
			// 
			this.txtScore395.DataField = "ITEM47";
			this.txtScore395.Height = 0.2393701F;
			this.txtScore395.Left = 0.6299213F;
			this.txtScore395.MultiLine = false;
			this.txtScore395.Name = "txtScore395";
			this.txtScore395.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore395.Text = "ITEM47";
			this.txtScore395.Top = 2.154331F;
			this.txtScore395.Width = 1.708662F;
			// 
			// txtScore396
			// 
			this.txtScore396.DataField = "ITEM48";
			this.txtScore396.Height = 0.2393701F;
			this.txtScore396.Left = 2.373622F;
			this.txtScore396.MultiLine = false;
			this.txtScore396.Name = "txtScore396";
			this.txtScore396.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore396.Text = "048";
			this.txtScore396.Top = 2.154331F;
			this.txtScore396.Width = 1.124016F;
			// 
			// txtScore397
			// 
			this.txtScore397.DataField = "ITEM49";
			this.txtScore397.Height = 0.2393701F;
			this.txtScore397.Left = 3.540551F;
			this.txtScore397.MultiLine = false;
			this.txtScore397.Name = "txtScore397";
			this.txtScore397.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore397.Text = "049";
			this.txtScore397.Top = 2.154331F;
			this.txtScore397.Width = 1.124016F;
			// 
			// txtScore393
			// 
			this.txtScore393.DataField = "ITEM45";
			this.txtScore393.Height = 0.2393701F;
			this.txtScore393.Left = 4.722835F;
			this.txtScore393.MultiLine = false;
			this.txtScore393.Name = "txtScore393";
			this.txtScore393.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore393.Text = "045";
			this.txtScore393.Top = 1.914961F;
			this.txtScore393.Width = 1.159842F;
			// 
			// txtScore398
			// 
			this.txtScore398.DataField = "ITEM50";
			this.txtScore398.Height = 0.2393701F;
			this.txtScore398.Left = 4.722048F;
			this.txtScore398.MultiLine = false;
			this.txtScore398.Name = "txtScore398";
			this.txtScore398.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore398.Text = "050";
			this.txtScore398.Top = 2.154331F;
			this.txtScore398.Width = 1.16063F;
			// 
			// txtScore400
			// 
			this.txtScore400.DataField = "ITEM52";
			this.txtScore400.Height = 0.2393701F;
			this.txtScore400.Left = 0.622441F;
			this.txtScore400.MultiLine = false;
			this.txtScore400.Name = "txtScore400";
			this.txtScore400.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore400.Text = "ITEM52";
			this.txtScore400.Top = 2.393701F;
			this.txtScore400.Width = 1.708268F;
			// 
			// txtScore401
			// 
			this.txtScore401.DataField = "ITEM53";
			this.txtScore401.Height = 0.2393701F;
			this.txtScore401.Left = 2.373622F;
			this.txtScore401.MultiLine = false;
			this.txtScore401.Name = "txtScore401";
			this.txtScore401.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore401.Text = "053";
			this.txtScore401.Top = 2.393701F;
			this.txtScore401.Width = 1.124016F;
			// 
			// txtScore402
			// 
			this.txtScore402.DataField = "ITEM54";
			this.txtScore402.Height = 0.2393701F;
			this.txtScore402.Left = 3.540946F;
			this.txtScore402.MultiLine = false;
			this.txtScore402.Name = "txtScore402";
			this.txtScore402.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore402.Text = "054";
			this.txtScore402.Top = 2.393701F;
			this.txtScore402.Width = 1.124016F;
			// 
			// txtScore403
			// 
			this.txtScore403.DataField = "ITEM55";
			this.txtScore403.Height = 0.2393701F;
			this.txtScore403.Left = 4.722835F;
			this.txtScore403.MultiLine = false;
			this.txtScore403.Name = "txtScore403";
			this.txtScore403.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore403.Text = "055";
			this.txtScore403.Top = 2.393701F;
			this.txtScore403.Width = 1.159842F;
			// 
			// txtScore399
			// 
			this.txtScore399.DataField = "ITEM51";
			this.txtScore399.Height = 0.2393701F;
			this.txtScore399.Left = 5.91693F;
			this.txtScore399.MultiLine = false;
			this.txtScore399.Name = "txtScore399";
			this.txtScore399.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore399.Text = "051";
			this.txtScore399.Top = 2.154331F;
			this.txtScore399.Width = 1.124016F;
			// 
			// txtScore404
			// 
			this.txtScore404.DataField = "ITEM56";
			this.txtScore404.Height = 0.2393701F;
			this.txtScore404.Left = 5.91693F;
			this.txtScore404.MultiLine = false;
			this.txtScore404.Name = "txtScore404";
			this.txtScore404.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore404.Text = "056,456,789,000";
			this.txtScore404.Top = 2.393701F;
			this.txtScore404.Width = 1.124016F;
			// 
			// txtScore405
			// 
			this.txtScore405.DataField = "ITEM57";
			this.txtScore405.Height = 0.2393701F;
			this.txtScore405.Left = 0.6299213F;
			this.txtScore405.MultiLine = false;
			this.txtScore405.Name = "txtScore405";
			this.txtScore405.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore405.Text = "ITEM57";
			this.txtScore405.Top = 2.633071F;
			this.txtScore405.Width = 1.712206F;
			// 
			// txtScore407
			// 
			this.txtScore407.DataField = "ITEM59";
			this.txtScore407.Height = 0.2393701F;
			this.txtScore407.Left = 3.536614F;
			this.txtScore407.MultiLine = false;
			this.txtScore407.Name = "txtScore407";
			this.txtScore407.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore407.Text = "059";
			this.txtScore407.Top = 2.633071F;
			this.txtScore407.Width = 1.124016F;
			// 
			// txtScore408
			// 
			this.txtScore408.DataField = "ITEM60";
			this.txtScore408.Height = 0.2393701F;
			this.txtScore408.Left = 4.722048F;
			this.txtScore408.MultiLine = false;
			this.txtScore408.Name = "txtScore408";
			this.txtScore408.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore408.Text = "060";
			this.txtScore408.Top = 2.633071F;
			this.txtScore408.Width = 1.16063F;
			// 
			// txtScore409
			// 
			this.txtScore409.DataField = "ITEM61";
			this.txtScore409.Height = 0.2393701F;
			this.txtScore409.Left = 5.917323F;
			this.txtScore409.MultiLine = false;
			this.txtScore409.Name = "txtScore409";
			this.txtScore409.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore409.Text = "061";
			this.txtScore409.Top = 2.633071F;
			this.txtScore409.Width = 1.124015F;
			// 
			// txtScore459
			// 
			this.txtScore459.DataField = "ITEM111";
			this.txtScore459.Height = 0.2393701F;
			this.txtScore459.Left = 5.918898F;
			this.txtScore459.MultiLine = false;
			this.txtScore459.Name = "txtScore459";
			this.txtScore459.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore459.Text = "111,456,789,000";
			this.txtScore459.Top = 5.026772F;
			this.txtScore459.Width = 1.124016F;
			// 
			// txtScore406
			// 
			this.txtScore406.DataField = "ITEM58";
			this.txtScore406.Height = 0.2393701F;
			this.txtScore406.Left = 2.372441F;
			this.txtScore406.MultiLine = false;
			this.txtScore406.Name = "txtScore406";
			this.txtScore406.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore406.Text = "058";
			this.txtScore406.Top = 2.633071F;
			this.txtScore406.Width = 1.124016F;
			// 
			// txtScore410
			// 
			this.txtScore410.DataField = "ITEM62";
			this.txtScore410.Height = 0.2393701F;
			this.txtScore410.Left = 0.6283463F;
			this.txtScore410.MultiLine = false;
			this.txtScore410.Name = "txtScore410";
			this.txtScore410.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore410.Text = "ITEM62";
			this.txtScore410.Top = 2.872441F;
			this.txtScore410.Width = 1.710237F;
			// 
			// txtScore412
			// 
			this.txtScore412.DataField = "ITEM64";
			this.txtScore412.Height = 0.2393701F;
			this.txtScore412.Left = 3.536614F;
			this.txtScore412.MultiLine = false;
			this.txtScore412.Name = "txtScore412";
			this.txtScore412.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore412.Text = "064";
			this.txtScore412.Top = 2.872441F;
			this.txtScore412.Width = 1.124016F;
			// 
			// txtScore413
			// 
			this.txtScore413.DataField = "ITEM65";
			this.txtScore413.Height = 0.2393701F;
			this.txtScore413.Left = 4.722048F;
			this.txtScore413.MultiLine = false;
			this.txtScore413.Name = "txtScore413";
			this.txtScore413.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore413.Text = "065";
			this.txtScore413.Top = 2.872442F;
			this.txtScore413.Width = 1.16063F;
			// 
			// txtScore414
			// 
			this.txtScore414.DataField = "ITEM66";
			this.txtScore414.Height = 0.2393701F;
			this.txtScore414.Left = 5.91811F;
			this.txtScore414.MultiLine = false;
			this.txtScore414.Name = "txtScore414";
			this.txtScore414.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore414.Text = "066";
			this.txtScore414.Top = 2.872441F;
			this.txtScore414.Width = 1.124016F;
			// 
			// txtScore415
			// 
			this.txtScore415.DataField = "ITEM67";
			this.txtScore415.Height = 0.2393701F;
			this.txtScore415.Left = 0.6275587F;
			this.txtScore415.MultiLine = false;
			this.txtScore415.Name = "txtScore415";
			this.txtScore415.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore415.Text = "ITEM67";
			this.txtScore415.Top = 3.111811F;
			this.txtScore415.Width = 1.711024F;
			// 
			// txtScore411
			// 
			this.txtScore411.DataField = "ITEM63";
			this.txtScore411.Height = 0.2393701F;
			this.txtScore411.Left = 2.372441F;
			this.txtScore411.MultiLine = false;
			this.txtScore411.Name = "txtScore411";
			this.txtScore411.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore411.Text = "063";
			this.txtScore411.Top = 2.872441F;
			this.txtScore411.Width = 1.124016F;
			// 
			// txtScore416
			// 
			this.txtScore416.DataField = "ITEM68";
			this.txtScore416.Height = 0.2393701F;
			this.txtScore416.Left = 2.370866F;
			this.txtScore416.MultiLine = false;
			this.txtScore416.Name = "txtScore416";
			this.txtScore416.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore416.Text = "068";
			this.txtScore416.Top = 3.111811F;
			this.txtScore416.Width = 1.124016F;
			// 
			// txtScore418
			// 
			this.txtScore418.DataField = "ITEM70";
			this.txtScore418.Height = 0.2393701F;
			this.txtScore418.Left = 4.722048F;
			this.txtScore418.MultiLine = false;
			this.txtScore418.Name = "txtScore418";
			this.txtScore418.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore418.Text = "070";
			this.txtScore418.Top = 3.111811F;
			this.txtScore418.Width = 1.16063F;
			// 
			// txtScore419
			// 
			this.txtScore419.DataField = "ITEM71";
			this.txtScore419.Height = 0.2393701F;
			this.txtScore419.Left = 5.91811F;
			this.txtScore419.MultiLine = false;
			this.txtScore419.Name = "txtScore419";
			this.txtScore419.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore419.Text = "071";
			this.txtScore419.Top = 3.111811F;
			this.txtScore419.Width = 1.124016F;
			// 
			// txtScore420
			// 
			this.txtScore420.DataField = "ITEM72";
			this.txtScore420.Height = 0.2393701F;
			this.txtScore420.Left = 0.6283463F;
			this.txtScore420.MultiLine = false;
			this.txtScore420.Name = "txtScore420";
			this.txtScore420.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore420.Text = "ITEM72";
			this.txtScore420.Top = 3.351181F;
			this.txtScore420.Width = 1.710237F;
			// 
			// txtScore421
			// 
			this.txtScore421.DataField = "ITEM73";
			this.txtScore421.Height = 0.2393701F;
			this.txtScore421.Left = 2.373228F;
			this.txtScore421.MultiLine = false;
			this.txtScore421.Name = "txtScore421";
			this.txtScore421.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore421.Text = "073";
			this.txtScore421.Top = 3.351181F;
			this.txtScore421.Width = 1.124016F;
			// 
			// txtScore417
			// 
			this.txtScore417.DataField = "ITEM69";
			this.txtScore417.Height = 0.2393701F;
			this.txtScore417.Left = 3.536614F;
			this.txtScore417.MultiLine = false;
			this.txtScore417.Name = "txtScore417";
			this.txtScore417.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore417.Text = "069";
			this.txtScore417.Top = 3.111811F;
			this.txtScore417.Width = 1.124016F;
			// 
			// txtScore422
			// 
			this.txtScore422.DataField = "ITEM74";
			this.txtScore422.Height = 0.2393701F;
			this.txtScore422.Left = 3.536614F;
			this.txtScore422.MultiLine = false;
			this.txtScore422.Name = "txtScore422";
			this.txtScore422.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore422.Text = "074";
			this.txtScore422.Top = 3.351181F;
			this.txtScore422.Width = 1.124016F;
			// 
			// txtScore424
			// 
			this.txtScore424.DataField = "ITEM76";
			this.txtScore424.Height = 0.2393701F;
			this.txtScore424.Left = 5.918504F;
			this.txtScore424.MultiLine = false;
			this.txtScore424.Name = "txtScore424";
			this.txtScore424.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore424.Text = "076";
			this.txtScore424.Top = 3.351181F;
			this.txtScore424.Width = 1.124016F;
			// 
			// txtScore425
			// 
			this.txtScore425.DataField = "ITEM77";
			this.txtScore425.Height = 0.2393701F;
			this.txtScore425.Left = 0.6283463F;
			this.txtScore425.MultiLine = false;
			this.txtScore425.Name = "txtScore425";
			this.txtScore425.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore425.Text = "ITEM77";
			this.txtScore425.Top = 3.590551F;
			this.txtScore425.Width = 1.710237F;
			// 
			// txtScore426
			// 
			this.txtScore426.DataField = "ITEM78";
			this.txtScore426.Height = 0.2393701F;
			this.txtScore426.Left = 2.373228F;
			this.txtScore426.MultiLine = false;
			this.txtScore426.Name = "txtScore426";
			this.txtScore426.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore426.Text = "078";
			this.txtScore426.Top = 3.590551F;
			this.txtScore426.Width = 1.124016F;
			// 
			// txtScore427
			// 
			this.txtScore427.DataField = "ITEM79";
			this.txtScore427.Height = 0.2393701F;
			this.txtScore427.Left = 3.536614F;
			this.txtScore427.MultiLine = false;
			this.txtScore427.Name = "txtScore427";
			this.txtScore427.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore427.Text = "079";
			this.txtScore427.Top = 3.590551F;
			this.txtScore427.Width = 1.124016F;
			// 
			// txtScore423
			// 
			this.txtScore423.DataField = "ITEM75";
			this.txtScore423.Height = 0.2393701F;
			this.txtScore423.Left = 4.722048F;
			this.txtScore423.MultiLine = false;
			this.txtScore423.Name = "txtScore423";
			this.txtScore423.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore423.Text = "075";
			this.txtScore423.Top = 3.351181F;
			this.txtScore423.Width = 1.16063F;
			// 
			// txtScore428
			// 
			this.txtScore428.DataField = "ITEM80";
			this.txtScore428.Height = 0.2393701F;
			this.txtScore428.Left = 4.722048F;
			this.txtScore428.MultiLine = false;
			this.txtScore428.Name = "txtScore428";
			this.txtScore428.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore428.Text = "080";
			this.txtScore428.Top = 3.590551F;
			this.txtScore428.Width = 1.16063F;
			// 
			// txtScore430
			// 
			this.txtScore430.DataField = "ITEM82";
			this.txtScore430.Height = 0.2393701F;
			this.txtScore430.Left = 0.6283463F;
			this.txtScore430.MultiLine = false;
			this.txtScore430.Name = "txtScore430";
			this.txtScore430.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore430.Text = "ITEM82";
			this.txtScore430.Top = 3.829921F;
			this.txtScore430.Width = 1.710237F;
			// 
			// txtScore431
			// 
			this.txtScore431.DataField = "ITEM83";
			this.txtScore431.Height = 0.2393701F;
			this.txtScore431.Left = 2.373228F;
			this.txtScore431.MultiLine = false;
			this.txtScore431.Name = "txtScore431";
			this.txtScore431.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore431.Text = "083";
			this.txtScore431.Top = 3.829921F;
			this.txtScore431.Width = 1.124016F;
			// 
			// txtScore432
			// 
			this.txtScore432.DataField = "ITEM84";
			this.txtScore432.Height = 0.2393701F;
			this.txtScore432.Left = 3.536614F;
			this.txtScore432.MultiLine = false;
			this.txtScore432.Name = "txtScore432";
			this.txtScore432.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore432.Text = "084";
			this.txtScore432.Top = 3.829921F;
			this.txtScore432.Width = 1.124016F;
			// 
			// txtScore433
			// 
			this.txtScore433.DataField = "ITEM85";
			this.txtScore433.Height = 0.2393701F;
			this.txtScore433.Left = 4.722048F;
			this.txtScore433.MultiLine = false;
			this.txtScore433.Name = "txtScore433";
			this.txtScore433.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore433.Text = "085";
			this.txtScore433.Top = 3.829921F;
			this.txtScore433.Width = 1.16063F;
			// 
			// txtScore429
			// 
			this.txtScore429.DataField = "ITEM81";
			this.txtScore429.Height = 0.2393701F;
			this.txtScore429.Left = 5.918504F;
			this.txtScore429.MultiLine = false;
			this.txtScore429.Name = "txtScore429";
			this.txtScore429.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore429.Text = "081";
			this.txtScore429.Top = 3.590551F;
			this.txtScore429.Width = 1.124016F;
			// 
			// txtScore434
			// 
			this.txtScore434.DataField = "ITEM86";
			this.txtScore434.Height = 0.2393701F;
			this.txtScore434.Left = 5.918504F;
			this.txtScore434.MultiLine = false;
			this.txtScore434.Name = "txtScore434";
			this.txtScore434.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore434.Text = "086";
			this.txtScore434.Top = 3.829921F;
			this.txtScore434.Width = 1.124016F;
			// 
			// txtScore436
			// 
			this.txtScore436.DataField = "ITEM88";
			this.txtScore436.Height = 0.2393701F;
			this.txtScore436.Left = 2.373622F;
			this.txtScore436.MultiLine = false;
			this.txtScore436.Name = "txtScore436";
			this.txtScore436.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore436.Text = "088";
			this.txtScore436.Top = 4.069291F;
			this.txtScore436.Width = 1.124016F;
			// 
			// txtScore437
			// 
			this.txtScore437.DataField = "ITEM89";
			this.txtScore437.Height = 0.2393701F;
			this.txtScore437.Left = 3.536614F;
			this.txtScore437.MultiLine = false;
			this.txtScore437.Name = "txtScore437";
			this.txtScore437.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore437.Text = "089";
			this.txtScore437.Top = 4.069292F;
			this.txtScore437.Width = 1.124016F;
			// 
			// txtScore438
			// 
			this.txtScore438.DataField = "ITEM90";
			this.txtScore438.Height = 0.2393701F;
			this.txtScore438.Left = 4.722048F;
			this.txtScore438.MultiLine = false;
			this.txtScore438.Name = "txtScore438";
			this.txtScore438.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore438.Text = "090";
			this.txtScore438.Top = 4.069292F;
			this.txtScore438.Width = 1.16063F;
			// 
			// txtScore439
			// 
			this.txtScore439.DataField = "ITEM91";
			this.txtScore439.Height = 0.2393701F;
			this.txtScore439.Left = 5.918898F;
			this.txtScore439.MultiLine = false;
			this.txtScore439.Name = "txtScore439";
			this.txtScore439.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore439.Text = "091";
			this.txtScore439.Top = 4.069291F;
			this.txtScore439.Width = 1.124016F;
			// 
			// txtScore435
			// 
			this.txtScore435.DataField = "ITEM87";
			this.txtScore435.Height = 0.2393701F;
			this.txtScore435.Left = 0.6283463F;
			this.txtScore435.MultiLine = false;
			this.txtScore435.Name = "txtScore435";
			this.txtScore435.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore435.Text = "ITEM87";
			this.txtScore435.Top = 4.069291F;
			this.txtScore435.Width = 1.710237F;
			// 
			// txtScore440
			// 
			this.txtScore440.DataField = "ITEM92";
			this.txtScore440.Height = 0.2393701F;
			this.txtScore440.Left = 0.6299211F;
			this.txtScore440.MultiLine = false;
			this.txtScore440.Name = "txtScore440";
			this.txtScore440.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore440.Text = "ITEM92";
			this.txtScore440.Top = 4.308661F;
			this.txtScore440.Width = 1.708662F;
			// 
			// txtScore442
			// 
			this.txtScore442.DataField = "ITEM94";
			this.txtScore442.Height = 0.2393701F;
			this.txtScore442.Left = 3.536614F;
			this.txtScore442.MultiLine = false;
			this.txtScore442.Name = "txtScore442";
			this.txtScore442.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore442.Text = "094";
			this.txtScore442.Top = 4.308661F;
			this.txtScore442.Width = 1.124016F;
			// 
			// txtScore443
			// 
			this.txtScore443.DataField = "ITEM95";
			this.txtScore443.Height = 0.2393701F;
			this.txtScore443.Left = 4.722048F;
			this.txtScore443.MultiLine = false;
			this.txtScore443.Name = "txtScore443";
			this.txtScore443.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore443.Text = "095";
			this.txtScore443.Top = 4.308661F;
			this.txtScore443.Width = 1.16063F;
			// 
			// txtScore444
			// 
			this.txtScore444.DataField = "ITEM96";
			this.txtScore444.Height = 0.2393701F;
			this.txtScore444.Left = 5.918898F;
			this.txtScore444.MultiLine = false;
			this.txtScore444.Name = "txtScore444";
			this.txtScore444.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore444.Text = "096";
			this.txtScore444.Top = 4.308661F;
			this.txtScore444.Width = 1.124016F;
			// 
			// txtScore445
			// 
			this.txtScore445.DataField = "ITEM97";
			this.txtScore445.Height = 0.2393701F;
			this.txtScore445.Left = 0.6299213F;
			this.txtScore445.MultiLine = false;
			this.txtScore445.Name = "txtScore445";
			this.txtScore445.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore445.Text = "ITEM97";
			this.txtScore445.Top = 4.548032F;
			this.txtScore445.Width = 1.71063F;
			// 
			// txtScore441
			// 
			this.txtScore441.DataField = "ITEM93";
			this.txtScore441.Height = 0.2393701F;
			this.txtScore441.Left = 2.371653F;
			this.txtScore441.MultiLine = false;
			this.txtScore441.Name = "txtScore441";
			this.txtScore441.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore441.Text = "093";
			this.txtScore441.Top = 4.308661F;
			this.txtScore441.Width = 1.124016F;
			// 
			// txtScore446
			// 
			this.txtScore446.DataField = "ITEM98";
			this.txtScore446.Height = 0.2393701F;
			this.txtScore446.Left = 2.371653F;
			this.txtScore446.MultiLine = false;
			this.txtScore446.Name = "txtScore446";
			this.txtScore446.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore446.Text = "098";
			this.txtScore446.Top = 4.548032F;
			this.txtScore446.Width = 1.124016F;
			// 
			// txtScore448
			// 
			this.txtScore448.DataField = "ITEM100";
			this.txtScore448.Height = 0.2393701F;
			this.txtScore448.Left = 4.722048F;
			this.txtScore448.MultiLine = false;
			this.txtScore448.Name = "txtScore448";
			this.txtScore448.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore448.Text = "100";
			this.txtScore448.Top = 4.548032F;
			this.txtScore448.Width = 1.16063F;
			// 
			// txtScore449
			// 
			this.txtScore449.DataField = "ITEM101";
			this.txtScore449.Height = 0.2393701F;
			this.txtScore449.Left = 5.918898F;
			this.txtScore449.MultiLine = false;
			this.txtScore449.Name = "txtScore449";
			this.txtScore449.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore449.Text = "101";
			this.txtScore449.Top = 4.548032F;
			this.txtScore449.Width = 1.124016F;
			// 
			// txtScore450
			// 
			this.txtScore450.DataField = "ITEM102";
			this.txtScore450.Height = 0.2393701F;
			this.txtScore450.Left = 0.6299211F;
			this.txtScore450.MultiLine = false;
			this.txtScore450.Name = "txtScore450";
			this.txtScore450.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore450.Text = "102";
			this.txtScore450.Top = 4.787401F;
			this.txtScore450.Width = 1.71063F;
			// 
			// txtScore451
			// 
			this.txtScore451.DataField = "ITEM103";
			this.txtScore451.Height = 0.2393701F;
			this.txtScore451.Left = 2.370866F;
			this.txtScore451.MultiLine = false;
			this.txtScore451.Name = "txtScore451";
			this.txtScore451.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore451.Text = "103";
			this.txtScore451.Top = 4.787401F;
			this.txtScore451.Width = 1.124016F;
			// 
			// txtScore447
			// 
			this.txtScore447.DataField = "ITEM99";
			this.txtScore447.Height = 0.2393701F;
			this.txtScore447.Left = 3.536614F;
			this.txtScore447.MultiLine = false;
			this.txtScore447.Name = "txtScore447";
			this.txtScore447.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore447.Text = "099";
			this.txtScore447.Top = 4.548032F;
			this.txtScore447.Width = 1.124016F;
			// 
			// txtScore452
			// 
			this.txtScore452.DataField = "ITEM104";
			this.txtScore452.Height = 0.2393701F;
			this.txtScore452.Left = 3.536614F;
			this.txtScore452.MultiLine = false;
			this.txtScore452.Name = "txtScore452";
			this.txtScore452.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore452.Text = "104";
			this.txtScore452.Top = 4.787401F;
			this.txtScore452.Width = 1.124016F;
			// 
			// txtScore454
			// 
			this.txtScore454.DataField = "ITEM106";
			this.txtScore454.Height = 0.2393701F;
			this.txtScore454.Left = 5.918504F;
			this.txtScore454.MultiLine = false;
			this.txtScore454.Name = "txtScore454";
			this.txtScore454.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore454.Text = "106";
			this.txtScore454.Top = 4.787401F;
			this.txtScore454.Width = 1.124016F;
			// 
			// txtScore455
			// 
			this.txtScore455.DataField = "ITEM107";
			this.txtScore455.Height = 0.2393701F;
			this.txtScore455.Left = 0.6299211F;
			this.txtScore455.MultiLine = false;
			this.txtScore455.Name = "txtScore455";
			this.txtScore455.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore455.Text = "107";
			this.txtScore455.Top = 5.026772F;
			this.txtScore455.Width = 1.708662F;
			// 
			// txtScore456
			// 
			this.txtScore456.DataField = "ITEM108";
			this.txtScore456.Height = 0.2393701F;
			this.txtScore456.Left = 2.373622F;
			this.txtScore456.MultiLine = false;
			this.txtScore456.Name = "txtScore456";
			this.txtScore456.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore456.Text = "108";
			this.txtScore456.Top = 5.026772F;
			this.txtScore456.Width = 1.124016F;
			// 
			// txtScore457
			// 
			this.txtScore457.DataField = "ITEM109";
			this.txtScore457.Height = 0.2393701F;
			this.txtScore457.Left = 3.536614F;
			this.txtScore457.MultiLine = false;
			this.txtScore457.Name = "txtScore457";
			this.txtScore457.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore457.Text = "109";
			this.txtScore457.Top = 5.026772F;
			this.txtScore457.Width = 1.124016F;
			// 
			// txtScore453
			// 
			this.txtScore453.DataField = "ITEM105";
			this.txtScore453.Height = 0.2393701F;
			this.txtScore453.Left = 4.722048F;
			this.txtScore453.MultiLine = false;
			this.txtScore453.Name = "txtScore453";
			this.txtScore453.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore453.Text = "105";
			this.txtScore453.Top = 4.787402F;
			this.txtScore453.Width = 1.16063F;
			// 
			// txtScore458
			// 
			this.txtScore458.DataField = "ITEM110";
			this.txtScore458.Height = 0.2393701F;
			this.txtScore458.Left = 4.722048F;
			this.txtScore458.MultiLine = false;
			this.txtScore458.Name = "txtScore458";
			this.txtScore458.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore458.Text = "110";
			this.txtScore458.Top = 5.026772F;
			this.txtScore458.Width = 1.16063F;
			// 
			// line51
			// 
			this.line51.Height = 0F;
			this.line51.Left = 0.007874016F;
			this.line51.LineWeight = 1F;
			this.line51.Name = "line51";
			this.line51.Top = 5.266142F;
			this.line51.Width = 7.07874F;
			this.line51.X1 = 7.086614F;
			this.line51.X2 = 0.007874016F;
			this.line51.Y1 = 5.266142F;
			this.line51.Y2 = 5.266142F;
			// 
			// line52
			// 
			this.line52.Height = 0F;
			this.line52.Left = 0.007874016F;
			this.line52.LineWeight = 1F;
			this.line52.Name = "line52";
			this.line52.Top = 2.633071F;
			this.line52.Width = 7.07874F;
			this.line52.X1 = 7.086614F;
			this.line52.X2 = 0.007874016F;
			this.line52.Y1 = 2.633071F;
			this.line52.Y2 = 2.633071F;
			// 
			// line53
			// 
			this.line53.Height = 5.266142F;
			this.line53.Left = 7.086614F;
			this.line53.LineWeight = 1F;
			this.line53.Name = "line53";
			this.line53.Top = 0F;
			this.line53.Width = 0F;
			this.line53.X1 = 7.086614F;
			this.line53.X2 = 7.086614F;
			this.line53.Y1 = 5.266142F;
			this.line53.Y2 = 0F;
			// 
			// line54
			// 
			this.line54.Height = 5.266142F;
			this.line54.Left = 0.007874016F;
			this.line54.LineWeight = 1F;
			this.line54.Name = "line54";
			this.line54.Top = 0F;
			this.line54.Width = 0F;
			this.line54.X1 = 0.007874016F;
			this.line54.X2 = 0.007874016F;
			this.line54.Y1 = 5.266142F;
			this.line54.Y2 = 0F;
			// 
			// line55
			// 
			this.line55.Height = 5.266142F;
			this.line55.Left = 0.5767717F;
			this.line55.LineWeight = 1F;
			this.line55.Name = "line55";
			this.line55.Top = 0F;
			this.line55.Width = 0F;
			this.line55.X1 = 0.5767717F;
			this.line55.X2 = 0.5767717F;
			this.line55.Y1 = 5.266142F;
			this.line55.Y2 = 0F;
			// 
			// line130
			// 
			this.line130.Height = 0F;
			this.line130.Left = 0.5767717F;
			this.line130.LineWeight = 1F;
			this.line130.Name = "line130";
			this.line130.Top = 0.2393701F;
			this.line130.Width = 6.509843F;
			this.line130.X1 = 7.086615F;
			this.line130.X2 = 0.5767717F;
			this.line130.Y1 = 0.2393701F;
			this.line130.Y2 = 0.2393701F;
			// 
			// line131
			// 
			this.line131.Height = 0F;
			this.line131.Left = 0.5767717F;
			this.line131.LineWeight = 1F;
			this.line131.Name = "line131";
			this.line131.Top = 0.4787402F;
			this.line131.Width = 6.509842F;
			this.line131.X1 = 7.086614F;
			this.line131.X2 = 0.5767717F;
			this.line131.Y1 = 0.4787402F;
			this.line131.Y2 = 0.4787402F;
			// 
			// line132
			// 
			this.line132.Height = 0F;
			this.line132.Left = 0.5767717F;
			this.line132.LineWeight = 1F;
			this.line132.Name = "line132";
			this.line132.Top = 0.7181103F;
			this.line132.Width = 6.509842F;
			this.line132.X1 = 7.086614F;
			this.line132.X2 = 0.5767717F;
			this.line132.Y1 = 0.7181103F;
			this.line132.Y2 = 0.7181103F;
			// 
			// line133
			// 
			this.line133.Height = 0F;
			this.line133.Left = 0.5767717F;
			this.line133.LineWeight = 1F;
			this.line133.Name = "line133";
			this.line133.Top = 0.9574804F;
			this.line133.Width = 6.509842F;
			this.line133.X1 = 7.086614F;
			this.line133.X2 = 0.5767717F;
			this.line133.Y1 = 0.9574804F;
			this.line133.Y2 = 0.9574804F;
			// 
			// line134
			// 
			this.line134.Height = 0F;
			this.line134.Left = 0.5767717F;
			this.line134.LineWeight = 1F;
			this.line134.Name = "line134";
			this.line134.Top = 1.19685F;
			this.line134.Width = 6.509842F;
			this.line134.X1 = 7.086614F;
			this.line134.X2 = 0.5767717F;
			this.line134.Y1 = 1.19685F;
			this.line134.Y2 = 1.19685F;
			// 
			// line135
			// 
			this.line135.Height = 0F;
			this.line135.Left = 0.5767717F;
			this.line135.LineWeight = 1F;
			this.line135.Name = "line135";
			this.line135.Top = 1.436221F;
			this.line135.Width = 6.509842F;
			this.line135.X1 = 7.086614F;
			this.line135.X2 = 0.5767717F;
			this.line135.Y1 = 1.436221F;
			this.line135.Y2 = 1.436221F;
			// 
			// line136
			// 
			this.line136.Height = 0F;
			this.line136.Left = 0.5767717F;
			this.line136.LineWeight = 1F;
			this.line136.Name = "line136";
			this.line136.Top = 1.675591F;
			this.line136.Width = 6.509842F;
			this.line136.X1 = 7.086614F;
			this.line136.X2 = 0.5767717F;
			this.line136.Y1 = 1.675591F;
			this.line136.Y2 = 1.675591F;
			// 
			// line137
			// 
			this.line137.Height = 0F;
			this.line137.Left = 0.5767717F;
			this.line137.LineWeight = 1F;
			this.line137.Name = "line137";
			this.line137.Top = 1.914961F;
			this.line137.Width = 6.509842F;
			this.line137.X1 = 7.086614F;
			this.line137.X2 = 0.5767717F;
			this.line137.Y1 = 1.914961F;
			this.line137.Y2 = 1.914961F;
			// 
			// line138
			// 
			this.line138.Height = 0F;
			this.line138.Left = 0.5767717F;
			this.line138.LineWeight = 1F;
			this.line138.Name = "line138";
			this.line138.Top = 2.154331F;
			this.line138.Width = 6.509842F;
			this.line138.X1 = 7.086614F;
			this.line138.X2 = 0.5767717F;
			this.line138.Y1 = 2.154331F;
			this.line138.Y2 = 2.154331F;
			// 
			// line139
			// 
			this.line139.Height = 0F;
			this.line139.Left = 0.5767717F;
			this.line139.LineWeight = 1F;
			this.line139.Name = "line139";
			this.line139.Top = 2.393701F;
			this.line139.Width = 6.509842F;
			this.line139.X1 = 7.086614F;
			this.line139.X2 = 0.5767717F;
			this.line139.Y1 = 2.393701F;
			this.line139.Y2 = 2.393701F;
			// 
			// line140
			// 
			this.line140.Height = 0F;
			this.line140.Left = 0.5767717F;
			this.line140.LineWeight = 1F;
			this.line140.Name = "line140";
			this.line140.Top = 2.872441F;
			this.line140.Width = 6.509842F;
			this.line140.X1 = 7.086614F;
			this.line140.X2 = 0.5767717F;
			this.line140.Y1 = 2.872441F;
			this.line140.Y2 = 2.872441F;
			// 
			// line141
			// 
			this.line141.Height = 0F;
			this.line141.Left = 0.5767717F;
			this.line141.LineWeight = 1F;
			this.line141.Name = "line141";
			this.line141.Top = 3.111811F;
			this.line141.Width = 6.509842F;
			this.line141.X1 = 7.086614F;
			this.line141.X2 = 0.5767717F;
			this.line141.Y1 = 3.111811F;
			this.line141.Y2 = 3.111811F;
			// 
			// line142
			// 
			this.line142.Height = 0F;
			this.line142.Left = 0.5767717F;
			this.line142.LineWeight = 1F;
			this.line142.Name = "line142";
			this.line142.Top = 3.351181F;
			this.line142.Width = 6.509842F;
			this.line142.X1 = 7.086614F;
			this.line142.X2 = 0.5767717F;
			this.line142.Y1 = 3.351181F;
			this.line142.Y2 = 3.351181F;
			// 
			// line143
			// 
			this.line143.Height = 0F;
			this.line143.Left = 0.5767717F;
			this.line143.LineWeight = 1F;
			this.line143.Name = "line143";
			this.line143.Top = 3.590551F;
			this.line143.Width = 6.509842F;
			this.line143.X1 = 7.086614F;
			this.line143.X2 = 0.5767717F;
			this.line143.Y1 = 3.590551F;
			this.line143.Y2 = 3.590551F;
			// 
			// line144
			// 
			this.line144.Height = 0F;
			this.line144.Left = 0.5767717F;
			this.line144.LineWeight = 1F;
			this.line144.Name = "line144";
			this.line144.Top = 3.829921F;
			this.line144.Width = 6.509842F;
			this.line144.X1 = 7.086614F;
			this.line144.X2 = 0.5767717F;
			this.line144.Y1 = 3.829921F;
			this.line144.Y2 = 3.829921F;
			// 
			// line145
			// 
			this.line145.Height = 0F;
			this.line145.Left = 0.5767717F;
			this.line145.LineWeight = 1F;
			this.line145.Name = "line145";
			this.line145.Top = 4.069292F;
			this.line145.Width = 6.509842F;
			this.line145.X1 = 7.086614F;
			this.line145.X2 = 0.5767717F;
			this.line145.Y1 = 4.069292F;
			this.line145.Y2 = 4.069292F;
			// 
			// line146
			// 
			this.line146.Height = 0F;
			this.line146.Left = 0.5767717F;
			this.line146.LineWeight = 1F;
			this.line146.Name = "line146";
			this.line146.Top = 4.308662F;
			this.line146.Width = 6.509842F;
			this.line146.X1 = 7.086614F;
			this.line146.X2 = 0.5767717F;
			this.line146.Y1 = 4.308662F;
			this.line146.Y2 = 4.308662F;
			// 
			// line147
			// 
			this.line147.Height = 0F;
			this.line147.Left = 0.5767717F;
			this.line147.LineWeight = 1F;
			this.line147.Name = "line147";
			this.line147.Top = 4.548032F;
			this.line147.Width = 6.509842F;
			this.line147.X1 = 7.086614F;
			this.line147.X2 = 0.5767717F;
			this.line147.Y1 = 4.548032F;
			this.line147.Y2 = 4.548032F;
			// 
			// line148
			// 
			this.line148.Height = 0F;
			this.line148.Left = 0.5767717F;
			this.line148.LineWeight = 1F;
			this.line148.Name = "line148";
			this.line148.Top = 4.787402F;
			this.line148.Width = 6.509842F;
			this.line148.X1 = 7.086614F;
			this.line148.X2 = 0.5767717F;
			this.line148.Y1 = 4.787402F;
			this.line148.Y2 = 4.787402F;
			// 
			// line149
			// 
			this.line149.Height = 0F;
			this.line149.Left = 0.5767717F;
			this.line149.LineWeight = 1F;
			this.line149.Name = "line149";
			this.line149.Top = 5.026772F;
			this.line149.Width = 6.509842F;
			this.line149.X1 = 7.086614F;
			this.line149.X2 = 0.5767717F;
			this.line149.Y1 = 5.026772F;
			this.line149.Y2 = 5.026772F;
			// 
			// line150
			// 
			this.line150.Height = 5.266142F;
			this.line150.Left = 2.365354F;
			this.line150.LineWeight = 1F;
			this.line150.Name = "line150";
			this.line150.Top = 0F;
			this.line150.Width = 0F;
			this.line150.X1 = 2.365354F;
			this.line150.X2 = 2.365354F;
			this.line150.Y1 = 5.266142F;
			this.line150.Y2 = 0F;
			// 
			// line151
			// 
			this.line151.Height = 5.266142F;
			this.line151.Left = 3.536614F;
			this.line151.LineWeight = 1F;
			this.line151.Name = "line151";
			this.line151.Top = 0F;
			this.line151.Width = 0F;
			this.line151.X1 = 3.536614F;
			this.line151.X2 = 3.536614F;
			this.line151.Y1 = 5.266142F;
			this.line151.Y2 = 0F;
			// 
			// line152
			// 
			this.line152.Height = 5.266142F;
			this.line152.Left = 4.718898F;
			this.line152.LineWeight = 1F;
			this.line152.Name = "line152";
			this.line152.Top = 0F;
			this.line152.Width = 0F;
			this.line152.X1 = 4.718898F;
			this.line152.X2 = 4.718898F;
			this.line152.Y1 = 5.266142F;
			this.line152.Y2 = 0F;
			// 
			// line153
			// 
			this.line153.Height = 5.266142F;
			this.line153.Left = 5.918898F;
			this.line153.LineWeight = 1F;
			this.line153.Name = "line153";
			this.line153.Top = 0F;
			this.line153.Width = 0F;
			this.line153.X1 = 5.918898F;
			this.line153.X2 = 5.918898F;
			this.line153.Y1 = 5.266142F;
			this.line153.Y2 = 0F;
			// 
			// group07_H
			// 
			this.group07_H.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox563,
            this.textBox564,
            this.textBox565,
            this.textBox566,
            this.textBox567,
            this.textBox568,
            this.textBox569,
            this.line73,
            this.line74,
            this.line75,
            this.line76,
            this.line77,
            this.line78,
            this.line79,
            this.pageBreak3,
            this.line91,
            this.line155});
			this.group07_H.DataField = "ITEM01";
			this.group07_H.Height = 0.6822835F;
			this.group07_H.Name = "group07_H";
			this.group07_H.Format += new System.EventHandler(this.group07_Format);
			// 
			// textBox563
			// 
			this.textBox563.Height = 0.4433071F;
			this.textBox563.Left = 0.007874016F;
			this.textBox563.MultiLine = false;
			this.textBox563.Name = "textBox563";
			this.textBox563.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: justify; vertical-align: middle";
			this.textBox563.Text = null;
			this.textBox563.Top = 0.2389764F;
			this.textBox563.Width = 7.086614F;
			// 
			// textBox564
			// 
			this.textBox564.Height = 0.2389764F;
			this.textBox564.Left = 0.02716541F;
			this.textBox564.MultiLine = false;
			this.textBox564.Name = "textBox564";
			this.textBox564.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: justify; ve" +
    "rtical-align: middle";
			this.textBox564.Text = "（７）その他の流動負債";
			this.textBox564.Top = 0F;
			this.textBox564.Width = 4.237402F;
			// 
			// textBox565
			// 
			this.textBox565.Height = 0.4429134F;
			this.textBox565.Left = 0F;
			this.textBox565.MultiLine = false;
			this.textBox565.Name = "textBox565";
			this.textBox565.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; te" +
    "xt-justify: auto; vertical-align: middle";
			this.textBox565.Text = "科     目";
			this.textBox565.Top = 0.2362205F;
			this.textBox565.Width = 2.044095F;
			// 
			// textBox566
			// 
			this.textBox566.Height = 0.4429134F;
			this.textBox566.Left = 2.044095F;
			this.textBox566.MultiLine = false;
			this.textBox566.Name = "textBox566";
			this.textBox566.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox566.Text = "前年度末残高";
			this.textBox566.Top = 0.2393701F;
			this.textBox566.Width = 1.267322F;
			// 
			// textBox567
			// 
			this.textBox567.Height = 0.4440946F;
			this.textBox567.Left = 3.309449F;
			this.textBox567.MultiLine = false;
			this.textBox567.Name = "textBox567";
			this.textBox567.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox567.Text = "本年度増加額";
			this.textBox567.Top = 0.238189F;
			this.textBox567.Width = 1.265354F;
			// 
			// textBox568
			// 
			this.textBox568.Height = 0.4440945F;
			this.textBox568.Left = 4.574803F;
			this.textBox568.MultiLine = false;
			this.textBox568.Name = "textBox568";
			this.textBox568.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox568.Text = "本年度減少額";
			this.textBox568.Top = 0.238189F;
			this.textBox568.Width = 1.267717F;
			// 
			// textBox569
			// 
			this.textBox569.Height = 0.4433072F;
			this.textBox569.Left = 5.83504F;
			this.textBox569.MultiLine = false;
			this.textBox569.Name = "textBox569";
			this.textBox569.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox569.Text = "本年度末残高";
			this.textBox569.Top = 0.2393701F;
			this.textBox569.Width = 1.246457F;
			// 
			// line73
			// 
			this.line73.Height = 0F;
			this.line73.Left = 0.007874016F;
			this.line73.LineWeight = 1F;
			this.line73.Name = "line73";
			this.line73.Top = 0.2389763F;
			this.line73.Width = 7.07874F;
			this.line73.X1 = 7.086614F;
			this.line73.X2 = 0.007874016F;
			this.line73.Y1 = 0.2389763F;
			this.line73.Y2 = 0.2389763F;
			// 
			// line74
			// 
			this.line74.Height = 0.4440945F;
			this.line74.Left = 5.840158F;
			this.line74.LineWeight = 1F;
			this.line74.Name = "line74";
			this.line74.Top = 0.238189F;
			this.line74.Width = 0F;
			this.line74.X1 = 5.840158F;
			this.line74.X2 = 5.840158F;
			this.line74.Y1 = 0.238189F;
			this.line74.Y2 = 0.6822835F;
			// 
			// line75
			// 
			this.line75.Height = 0.4440945F;
			this.line75.Left = 4.574803F;
			this.line75.LineWeight = 1F;
			this.line75.Name = "line75";
			this.line75.Top = 0.238189F;
			this.line75.Width = 0F;
			this.line75.X1 = 4.574803F;
			this.line75.X2 = 4.574803F;
			this.line75.Y1 = 0.238189F;
			this.line75.Y2 = 0.6822835F;
			// 
			// line76
			// 
			this.line76.Height = 0.4437008F;
			this.line76.Left = 3.309449F;
			this.line76.LineWeight = 1F;
			this.line76.Name = "line76";
			this.line76.Top = 0.2385827F;
			this.line76.Width = 0F;
			this.line76.X1 = 3.309449F;
			this.line76.X2 = 3.309449F;
			this.line76.Y1 = 0.2385827F;
			this.line76.Y2 = 0.6822835F;
			// 
			// line77
			// 
			this.line77.Height = 0.4437008F;
			this.line77.Left = 2.044095F;
			this.line77.LineWeight = 1F;
			this.line77.Name = "line77";
			this.line77.Top = 0.2385827F;
			this.line77.Width = 0F;
			this.line77.X1 = 2.044095F;
			this.line77.X2 = 2.044095F;
			this.line77.Y1 = 0.2385827F;
			this.line77.Y2 = 0.6822835F;
			// 
			// line78
			// 
			this.line78.Height = 0.446063F;
			this.line78.Left = 7.086614F;
			this.line78.LineWeight = 1F;
			this.line78.Name = "line78";
			this.line78.Top = 0.2362205F;
			this.line78.Width = 0F;
			this.line78.X1 = 7.086614F;
			this.line78.X2 = 7.086614F;
			this.line78.Y1 = 0.2362205F;
			this.line78.Y2 = 0.6822835F;
			// 
			// line79
			// 
			this.line79.Height = 0F;
			this.line79.Left = 0.007874016F;
			this.line79.LineWeight = 1F;
			this.line79.Name = "line79";
			this.line79.Top = 0.6822835F;
			this.line79.Width = 7.07874F;
			this.line79.X1 = 7.086614F;
			this.line79.X2 = 0.007874016F;
			this.line79.Y1 = 0.6822835F;
			this.line79.Y2 = 0.6822835F;
			// 
			// pageBreak3
			// 
			this.pageBreak3.Height = 0.01F;
			this.pageBreak3.Left = 0F;
			this.pageBreak3.Name = "pageBreak3";
			this.pageBreak3.Size = new System.Drawing.SizeF(6F, 0.01F);
			this.pageBreak3.Top = 0F;
			this.pageBreak3.Width = 6F;
			// 
			// line91
			// 
			this.line91.Height = 0.4429134F;
			this.line91.Left = 0.007874016F;
			this.line91.LineWeight = 1F;
			this.line91.Name = "line91";
			this.line91.Top = 0.2393701F;
			this.line91.Width = 0F;
			this.line91.X1 = 0.007874016F;
			this.line91.X2 = 0.007874016F;
			this.line91.Y1 = 0.2393701F;
			this.line91.Y2 = 0.6822835F;
			// 
			// line155
			// 
			this.line155.Height = 0.4440945F;
			this.line155.Left = 0.007874016F;
			this.line155.LineWeight = 1F;
			this.line155.Name = "line155";
			this.line155.Top = 0.2362205F;
			this.line155.Width = 0F;
			this.line155.X1 = 0.007874016F;
			this.line155.X2 = 0.007874016F;
			this.line155.Y1 = 0.2362205F;
			this.line155.Y2 = 0.680315F;
			// 
			// group07_F
			// 
			this.group07_F.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtScore460,
            this.txtScore462,
            this.txtScore463,
            this.txtScore464,
            this.txtScore465,
            this.txtScore461,
            this.txtScore466,
            this.txtScore468,
            this.txtScore469,
            this.txtScore470,
            this.txtScore471,
            this.txtScore467,
            this.txtScore472,
            this.txtScore474,
            this.txtScore475,
            this.txtScore476,
            this.txtScore477,
            this.txtScore473,
            this.txtScore478,
            this.txtScore480,
            this.txtScore481,
            this.txtScore482,
            this.txtScore483,
            this.txtScore479,
            this.txtScore484,
            this.txtScore486,
            this.txtScore487,
            this.txtScore488,
            this.txtScore489,
            this.txtScore485,
            this.txtScore490,
            this.txtScore492,
            this.txtScore493,
            this.txtScore494,
            this.txtScore495,
            this.txtScore491,
            this.txtScore496,
            this.txtScore498,
            this.txtScore499,
            this.txtScore500,
            this.txtScore501,
            this.txtScore497,
            this.txtScore502,
            this.txtScore504,
            this.txtScore505,
            this.txtScore506,
            this.txtScore507,
            this.txtScore503,
            this.txtScore508,
            this.txtScore510,
            this.txtScore511,
            this.txtScore512,
            this.txtScore513,
            this.txtScore509,
            this.txtScore514,
            this.line70,
            this.line71,
            this.line72,
            this.line116,
            this.line117,
            this.line118,
            this.line119,
            this.line120,
            this.line121,
            this.line122,
            this.line123,
            this.line124,
            this.line125,
            this.line126,
            this.line127,
            this.line128,
            this.line129,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.line174,
            this.line237,
            this.line238,
            this.line239});
			this.group07_F.Height = 3.145834F;
			this.group07_F.Name = "group07_F";
			// 
			// txtScore460
			// 
			this.txtScore460.DataField = "ITEM02";
			this.txtScore460.Height = 0.2393701F;
			this.txtScore460.Left = 0.05748032F;
			this.txtScore460.MultiLine = false;
			this.txtScore460.Name = "txtScore460";
			this.txtScore460.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore460.Text = "ITEM02";
			this.txtScore460.Top = 0F;
			this.txtScore460.Width = 1.977953F;
			// 
			// txtScore462
			// 
			this.txtScore462.DataField = "ITEM04";
			this.txtScore462.Height = 0.2393701F;
			this.txtScore462.Left = 3.310632F;
			this.txtScore462.MultiLine = false;
			this.txtScore462.Name = "txtScore462";
			this.txtScore462.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore462.Text = "004";
			this.txtScore462.Top = 0F;
			this.txtScore462.Width = 1.22362F;
			// 
			// txtScore463
			// 
			this.txtScore463.DataField = "ITEM05";
			this.txtScore463.Height = 0.2393701F;
			this.txtScore463.Left = 4.572048F;
			this.txtScore463.MultiLine = false;
			this.txtScore463.Name = "txtScore463";
			this.txtScore463.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore463.Text = "005";
			this.txtScore463.Top = 0F;
			this.txtScore463.Width = 1.225197F;
			// 
			// txtScore464
			// 
			this.txtScore464.DataField = "ITEM06";
			this.txtScore464.Height = 0.2393701F;
			this.txtScore464.Left = 5.840158F;
			this.txtScore464.MultiLine = false;
			this.txtScore464.Name = "txtScore464";
			this.txtScore464.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore464.Text = "006";
			this.txtScore464.Top = 0F;
			this.txtScore464.Width = 1.207087F;
			// 
			// txtScore465
			// 
			this.txtScore465.DataField = "ITEM07";
			this.txtScore465.Height = 0.2393701F;
			this.txtScore465.Left = 0.05748032F;
			this.txtScore465.MultiLine = false;
			this.txtScore465.Name = "txtScore465";
			this.txtScore465.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore465.Text = "ITEM07";
			this.txtScore465.Top = 0.2393701F;
			this.txtScore465.Width = 1.977953F;
			// 
			// txtScore461
			// 
			this.txtScore461.DataField = "ITEM03";
			this.txtScore461.Height = 0.2393701F;
			this.txtScore461.Left = 2.04685F;
			this.txtScore461.MultiLine = false;
			this.txtScore461.Name = "txtScore461";
			this.txtScore461.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore461.Text = "003";
			this.txtScore461.Top = 0F;
			this.txtScore461.Width = 1.239764F;
			// 
			// txtScore466
			// 
			this.txtScore466.DataField = "ITEM08";
			this.txtScore466.Height = 0.2393701F;
			this.txtScore466.Left = 2.04685F;
			this.txtScore466.MultiLine = false;
			this.txtScore466.Name = "txtScore466";
			this.txtScore466.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore466.Text = "008";
			this.txtScore466.Top = 0.2393701F;
			this.txtScore466.Width = 1.239764F;
			// 
			// txtScore468
			// 
			this.txtScore468.DataField = "ITEM10";
			this.txtScore468.Height = 0.2393701F;
			this.txtScore468.Left = 4.572048F;
			this.txtScore468.MultiLine = false;
			this.txtScore468.Name = "txtScore468";
			this.txtScore468.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore468.Text = "010";
			this.txtScore468.Top = 0.2393701F;
			this.txtScore468.Width = 1.22559F;
			// 
			// txtScore469
			// 
			this.txtScore469.DataField = "ITEM11";
			this.txtScore469.Height = 0.2393701F;
			this.txtScore469.Left = 5.840158F;
			this.txtScore469.MultiLine = false;
			this.txtScore469.Name = "txtScore469";
			this.txtScore469.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore469.Text = "011";
			this.txtScore469.Top = 0.2393701F;
			this.txtScore469.Width = 1.207087F;
			// 
			// txtScore470
			// 
			this.txtScore470.DataField = "ITEM12";
			this.txtScore470.Height = 0.2393701F;
			this.txtScore470.Left = 0.05748032F;
			this.txtScore470.MultiLine = false;
			this.txtScore470.Name = "txtScore470";
			this.txtScore470.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore470.Text = "ITEM12";
			this.txtScore470.Top = 0.4787402F;
			this.txtScore470.Width = 1.977953F;
			// 
			// txtScore471
			// 
			this.txtScore471.DataField = "ITEM13";
			this.txtScore471.Height = 0.2393701F;
			this.txtScore471.Left = 2.04685F;
			this.txtScore471.MultiLine = false;
			this.txtScore471.Name = "txtScore471";
			this.txtScore471.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore471.Text = "013";
			this.txtScore471.Top = 0.4787402F;
			this.txtScore471.Width = 1.238977F;
			// 
			// txtScore467
			// 
			this.txtScore467.DataField = "ITEM09";
			this.txtScore467.Height = 0.2393701F;
			this.txtScore467.Left = 3.310632F;
			this.txtScore467.MultiLine = false;
			this.txtScore467.Name = "txtScore467";
			this.txtScore467.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore467.Text = "009";
			this.txtScore467.Top = 0.2393701F;
			this.txtScore467.Width = 1.22362F;
			// 
			// txtScore472
			// 
			this.txtScore472.DataField = "ITEM14";
			this.txtScore472.Height = 0.2393701F;
			this.txtScore472.Left = 3.310632F;
			this.txtScore472.MultiLine = false;
			this.txtScore472.Name = "txtScore472";
			this.txtScore472.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore472.Text = "014";
			this.txtScore472.Top = 0.4787401F;
			this.txtScore472.Width = 1.222833F;
			// 
			// txtScore474
			// 
			this.txtScore474.DataField = "ITEM16";
			this.txtScore474.Height = 0.2393701F;
			this.txtScore474.Left = 5.840158F;
			this.txtScore474.MultiLine = false;
			this.txtScore474.Name = "txtScore474";
			this.txtScore474.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore474.Text = "016";
			this.txtScore474.Top = 0.4787401F;
			this.txtScore474.Width = 1.207087F;
			// 
			// txtScore475
			// 
			this.txtScore475.DataField = "ITEM17";
			this.txtScore475.Height = 0.2393701F;
			this.txtScore475.Left = 0.05748032F;
			this.txtScore475.MultiLine = false;
			this.txtScore475.Name = "txtScore475";
			this.txtScore475.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore475.Text = "ITEM17";
			this.txtScore475.Top = 0.7181103F;
			this.txtScore475.Width = 1.977953F;
			// 
			// txtScore476
			// 
			this.txtScore476.DataField = "ITEM18";
			this.txtScore476.Height = 0.2393701F;
			this.txtScore476.Left = 2.04685F;
			this.txtScore476.MultiLine = false;
			this.txtScore476.Name = "txtScore476";
			this.txtScore476.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore476.Text = "018";
			this.txtScore476.Top = 0.7181103F;
			this.txtScore476.Width = 1.238977F;
			// 
			// txtScore477
			// 
			this.txtScore477.DataField = "ITEM19";
			this.txtScore477.Height = 0.2393701F;
			this.txtScore477.Left = 3.310632F;
			this.txtScore477.MultiLine = false;
			this.txtScore477.Name = "txtScore477";
			this.txtScore477.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore477.Text = "019";
			this.txtScore477.Top = 0.7181103F;
			this.txtScore477.Width = 1.22362F;
			// 
			// txtScore473
			// 
			this.txtScore473.DataField = "ITEM15";
			this.txtScore473.Height = 0.2393701F;
			this.txtScore473.Left = 4.572836F;
			this.txtScore473.MultiLine = false;
			this.txtScore473.Name = "txtScore473";
			this.txtScore473.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore473.Text = "015";
			this.txtScore473.Top = 0.4787401F;
			this.txtScore473.Width = 1.224409F;
			// 
			// txtScore478
			// 
			this.txtScore478.DataField = "ITEM20";
			this.txtScore478.Height = 0.2393701F;
			this.txtScore478.Left = 4.572048F;
			this.txtScore478.MultiLine = false;
			this.txtScore478.Name = "txtScore478";
			this.txtScore478.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore478.Text = "020";
			this.txtScore478.Top = 0.7181103F;
			this.txtScore478.Width = 1.22559F;
			// 
			// txtScore480
			// 
			this.txtScore480.DataField = "ITEM22";
			this.txtScore480.Height = 0.2393701F;
			this.txtScore480.Left = 0.06259843F;
			this.txtScore480.MultiLine = false;
			this.txtScore480.Name = "txtScore480";
			this.txtScore480.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore480.Text = "ITEM22";
			this.txtScore480.Top = 0.9574804F;
			this.txtScore480.Width = 1.977559F;
			// 
			// txtScore481
			// 
			this.txtScore481.DataField = "ITEM23";
			this.txtScore481.Height = 0.2393701F;
			this.txtScore481.Left = 2.04685F;
			this.txtScore481.MultiLine = false;
			this.txtScore481.Name = "txtScore481";
			this.txtScore481.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore481.Text = "023";
			this.txtScore481.Top = 0.9574805F;
			this.txtScore481.Width = 1.238977F;
			// 
			// txtScore482
			// 
			this.txtScore482.DataField = "ITEM24";
			this.txtScore482.Height = 0.2393701F;
			this.txtScore482.Left = 3.310632F;
			this.txtScore482.MultiLine = false;
			this.txtScore482.Name = "txtScore482";
			this.txtScore482.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore482.Text = "024";
			this.txtScore482.Top = 0.9574805F;
			this.txtScore482.Width = 1.22362F;
			// 
			// txtScore483
			// 
			this.txtScore483.DataField = "ITEM25";
			this.txtScore483.Height = 0.2393701F;
			this.txtScore483.Left = 4.572048F;
			this.txtScore483.MultiLine = false;
			this.txtScore483.Name = "txtScore483";
			this.txtScore483.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore483.Text = "025";
			this.txtScore483.Top = 0.9574805F;
			this.txtScore483.Width = 1.22559F;
			// 
			// txtScore479
			// 
			this.txtScore479.DataField = "ITEM21";
			this.txtScore479.Height = 0.2393701F;
			this.txtScore479.Left = 5.840158F;
			this.txtScore479.MultiLine = false;
			this.txtScore479.Name = "txtScore479";
			this.txtScore479.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore479.Text = "021";
			this.txtScore479.Top = 0.7181103F;
			this.txtScore479.Width = 1.207087F;
			// 
			// txtScore484
			// 
			this.txtScore484.DataField = "ITEM26";
			this.txtScore484.Height = 0.2393701F;
			this.txtScore484.Left = 5.840159F;
			this.txtScore484.MultiLine = false;
			this.txtScore484.Name = "txtScore484";
			this.txtScore484.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore484.Text = "026";
			this.txtScore484.Top = 0.9574805F;
			this.txtScore484.Width = 1.207086F;
			// 
			// txtScore486
			// 
			this.txtScore486.DataField = "ITEM28";
			this.txtScore486.Height = 0.2393701F;
			this.txtScore486.Left = 2.04685F;
			this.txtScore486.MultiLine = false;
			this.txtScore486.Name = "txtScore486";
			this.txtScore486.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore486.Text = "028";
			this.txtScore486.Top = 1.196851F;
			this.txtScore486.Width = 1.238977F;
			// 
			// txtScore487
			// 
			this.txtScore487.DataField = "ITEM29";
			this.txtScore487.Height = 0.2393701F;
			this.txtScore487.Left = 3.310632F;
			this.txtScore487.MultiLine = false;
			this.txtScore487.Name = "txtScore487";
			this.txtScore487.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore487.Text = "029";
			this.txtScore487.Top = 1.196851F;
			this.txtScore487.Width = 1.22362F;
			// 
			// txtScore488
			// 
			this.txtScore488.DataField = "ITEM30";
			this.txtScore488.Height = 0.2393701F;
			this.txtScore488.Left = 4.572048F;
			this.txtScore488.MultiLine = false;
			this.txtScore488.Name = "txtScore488";
			this.txtScore488.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore488.Text = "030";
			this.txtScore488.Top = 1.196851F;
			this.txtScore488.Width = 1.225196F;
			// 
			// txtScore489
			// 
			this.txtScore489.DataField = "ITEM31";
			this.txtScore489.Height = 0.2393701F;
			this.txtScore489.Left = 5.840158F;
			this.txtScore489.MultiLine = false;
			this.txtScore489.Name = "txtScore489";
			this.txtScore489.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore489.Text = "031";
			this.txtScore489.Top = 1.196851F;
			this.txtScore489.Width = 1.207087F;
			// 
			// txtScore485
			// 
			this.txtScore485.DataField = "ITEM27";
			this.txtScore485.Height = 0.2393701F;
			this.txtScore485.Left = 0.05748032F;
			this.txtScore485.MultiLine = false;
			this.txtScore485.Name = "txtScore485";
			this.txtScore485.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore485.Text = "ITEM27";
			this.txtScore485.Top = 1.196851F;
			this.txtScore485.Width = 1.977953F;
			// 
			// txtScore490
			// 
			this.txtScore490.DataField = "ITEM32";
			this.txtScore490.Height = 0.2393701F;
			this.txtScore490.Left = 0.05748032F;
			this.txtScore490.MultiLine = false;
			this.txtScore490.Name = "txtScore490";
			this.txtScore490.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore490.Text = "ITEM32";
			this.txtScore490.Top = 1.43622F;
			this.txtScore490.Width = 1.977953F;
			// 
			// txtScore492
			// 
			this.txtScore492.DataField = "ITEM34";
			this.txtScore492.Height = 0.2393701F;
			this.txtScore492.Left = 3.311024F;
			this.txtScore492.MultiLine = false;
			this.txtScore492.Name = "txtScore492";
			this.txtScore492.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore492.Text = "034";
			this.txtScore492.Top = 1.43622F;
			this.txtScore492.Width = 1.22362F;
			// 
			// txtScore493
			// 
			this.txtScore493.DataField = "ITEM35";
			this.txtScore493.Height = 0.2393701F;
			this.txtScore493.Left = 4.572048F;
			this.txtScore493.MultiLine = false;
			this.txtScore493.Name = "txtScore493";
			this.txtScore493.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore493.Text = "035";
			this.txtScore493.Top = 1.43622F;
			this.txtScore493.Width = 1.225196F;
			// 
			// txtScore494
			// 
			this.txtScore494.DataField = "ITEM36";
			this.txtScore494.Height = 0.2393701F;
			this.txtScore494.Left = 5.840158F;
			this.txtScore494.MultiLine = false;
			this.txtScore494.Name = "txtScore494";
			this.txtScore494.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore494.Text = "036";
			this.txtScore494.Top = 1.43622F;
			this.txtScore494.Width = 1.207087F;
			// 
			// txtScore495
			// 
			this.txtScore495.DataField = "ITEM37";
			this.txtScore495.Height = 0.2393701F;
			this.txtScore495.Left = 0.05787407F;
			this.txtScore495.MultiLine = false;
			this.txtScore495.Name = "txtScore495";
			this.txtScore495.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore495.Text = "ITEM37";
			this.txtScore495.Top = 1.675591F;
			this.txtScore495.Width = 1.977559F;
			// 
			// txtScore491
			// 
			this.txtScore491.DataField = "ITEM33";
			this.txtScore491.Height = 0.2393701F;
			this.txtScore491.Left = 2.04685F;
			this.txtScore491.MultiLine = false;
			this.txtScore491.Name = "txtScore491";
			this.txtScore491.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore491.Text = "033";
			this.txtScore491.Top = 1.43622F;
			this.txtScore491.Width = 1.238977F;
			// 
			// txtScore496
			// 
			this.txtScore496.DataField = "ITEM38";
			this.txtScore496.Height = 0.2393701F;
			this.txtScore496.Left = 2.04685F;
			this.txtScore496.MultiLine = false;
			this.txtScore496.Name = "txtScore496";
			this.txtScore496.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore496.Text = "038";
			this.txtScore496.Top = 1.675591F;
			this.txtScore496.Width = 1.238977F;
			// 
			// txtScore498
			// 
			this.txtScore498.DataField = "ITEM40";
			this.txtScore498.Height = 0.2393701F;
			this.txtScore498.Left = 4.572441F;
			this.txtScore498.MultiLine = false;
			this.txtScore498.Name = "txtScore498";
			this.txtScore498.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore498.Text = "040";
			this.txtScore498.Top = 1.675591F;
			this.txtScore498.Width = 1.224803F;
			// 
			// txtScore499
			// 
			this.txtScore499.DataField = "ITEM41";
			this.txtScore499.Height = 0.2393701F;
			this.txtScore499.Left = 5.840158F;
			this.txtScore499.MultiLine = false;
			this.txtScore499.Name = "txtScore499";
			this.txtScore499.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore499.Text = "041";
			this.txtScore499.Top = 1.675591F;
			this.txtScore499.Width = 1.207087F;
			// 
			// txtScore500
			// 
			this.txtScore500.DataField = "ITEM42";
			this.txtScore500.Height = 0.2393701F;
			this.txtScore500.Left = 0.05787407F;
			this.txtScore500.MultiLine = false;
			this.txtScore500.Name = "txtScore500";
			this.txtScore500.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore500.Text = "ITEM42";
			this.txtScore500.Top = 1.914961F;
			this.txtScore500.Width = 1.977559F;
			// 
			// txtScore501
			// 
			this.txtScore501.DataField = "ITEM43";
			this.txtScore501.Height = 0.2393701F;
			this.txtScore501.Left = 2.04685F;
			this.txtScore501.MultiLine = false;
			this.txtScore501.Name = "txtScore501";
			this.txtScore501.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore501.Text = "043";
			this.txtScore501.Top = 1.914961F;
			this.txtScore501.Width = 1.238977F;
			// 
			// txtScore497
			// 
			this.txtScore497.DataField = "ITEM39";
			this.txtScore497.Height = 0.2393701F;
			this.txtScore497.Left = 3.310632F;
			this.txtScore497.MultiLine = false;
			this.txtScore497.Name = "txtScore497";
			this.txtScore497.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore497.Text = "039";
			this.txtScore497.Top = 1.675591F;
			this.txtScore497.Width = 1.22362F;
			// 
			// txtScore502
			// 
			this.txtScore502.DataField = "ITEM44";
			this.txtScore502.Height = 0.2393701F;
			this.txtScore502.Left = 3.311024F;
			this.txtScore502.MultiLine = false;
			this.txtScore502.Name = "txtScore502";
			this.txtScore502.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore502.Text = "044";
			this.txtScore502.Top = 1.914961F;
			this.txtScore502.Width = 1.223226F;
			// 
			// txtScore504
			// 
			this.txtScore504.DataField = "ITEM46";
			this.txtScore504.Height = 0.2393701F;
			this.txtScore504.Left = 5.840551F;
			this.txtScore504.MultiLine = false;
			this.txtScore504.Name = "txtScore504";
			this.txtScore504.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore504.Text = "046";
			this.txtScore504.Top = 1.914961F;
			this.txtScore504.Width = 1.206694F;
			// 
			// txtScore505
			// 
			this.txtScore505.DataField = "ITEM47";
			this.txtScore505.Height = 0.2393701F;
			this.txtScore505.Left = 0.05748032F;
			this.txtScore505.MultiLine = false;
			this.txtScore505.Name = "txtScore505";
			this.txtScore505.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore505.Text = "製氷冷凍未収金貸倒引当金";
			this.txtScore505.Top = 2.154331F;
			this.txtScore505.Width = 1.977953F;
			// 
			// txtScore506
			// 
			this.txtScore506.DataField = "ITEM48";
			this.txtScore506.Height = 0.2393701F;
			this.txtScore506.Left = 2.04685F;
			this.txtScore506.MultiLine = false;
			this.txtScore506.Name = "txtScore506";
			this.txtScore506.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore506.Text = "048";
			this.txtScore506.Top = 2.154331F;
			this.txtScore506.Width = 1.238977F;
			// 
			// txtScore507
			// 
			this.txtScore507.DataField = "ITEM49";
			this.txtScore507.Height = 0.2393701F;
			this.txtScore507.Left = 3.310632F;
			this.txtScore507.MultiLine = false;
			this.txtScore507.Name = "txtScore507";
			this.txtScore507.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore507.Text = "049";
			this.txtScore507.Top = 2.154331F;
			this.txtScore507.Width = 1.222833F;
			// 
			// txtScore503
			// 
			this.txtScore503.DataField = "ITEM45";
			this.txtScore503.Height = 0.2393701F;
			this.txtScore503.Left = 4.572048F;
			this.txtScore503.MultiLine = false;
			this.txtScore503.Name = "txtScore503";
			this.txtScore503.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore503.Text = "045";
			this.txtScore503.Top = 1.914961F;
			this.txtScore503.Width = 1.225197F;
			// 
			// txtScore508
			// 
			this.txtScore508.DataField = "ITEM50";
			this.txtScore508.Height = 0.2393701F;
			this.txtScore508.Left = 4.571261F;
			this.txtScore508.MultiLine = false;
			this.txtScore508.Name = "txtScore508";
			this.txtScore508.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore508.Text = "050";
			this.txtScore508.Top = 2.154331F;
			this.txtScore508.Width = 1.225984F;
			// 
			// txtScore510
			// 
			this.txtScore510.DataField = "ITEM52";
			this.txtScore510.Height = 0.2393698F;
			this.txtScore510.Left = 0.05787407F;
			this.txtScore510.MultiLine = false;
			this.txtScore510.Name = "txtScore510";
			this.txtScore510.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore510.Text = "ITEM52";
			this.txtScore510.Top = 2.393701F;
			this.txtScore510.Width = 1.977559F;
			// 
			// txtScore511
			// 
			this.txtScore511.DataField = "ITEM53";
			this.txtScore511.Height = 0.2393701F;
			this.txtScore511.Left = 2.04685F;
			this.txtScore511.MultiLine = false;
			this.txtScore511.Name = "txtScore511";
			this.txtScore511.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore511.Text = "053";
			this.txtScore511.Top = 2.393701F;
			this.txtScore511.Width = 1.239764F;
			// 
			// txtScore512
			// 
			this.txtScore512.DataField = "ITEM54";
			this.txtScore512.Height = 0.2393701F;
			this.txtScore512.Left = 3.311024F;
			this.txtScore512.MultiLine = false;
			this.txtScore512.Name = "txtScore512";
			this.txtScore512.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore512.Text = "054";
			this.txtScore512.Top = 2.393701F;
			this.txtScore512.Width = 1.223226F;
			// 
			// txtScore513
			// 
			this.txtScore513.DataField = "ITEM55";
			this.txtScore513.Height = 0.2393701F;
			this.txtScore513.Left = 4.572048F;
			this.txtScore513.MultiLine = false;
			this.txtScore513.Name = "txtScore513";
			this.txtScore513.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore513.Text = "055";
			this.txtScore513.Top = 2.393701F;
			this.txtScore513.Width = 1.225197F;
			// 
			// txtScore509
			// 
			this.txtScore509.DataField = "ITEM51";
			this.txtScore509.Height = 0.2393701F;
			this.txtScore509.Left = 5.840158F;
			this.txtScore509.MultiLine = false;
			this.txtScore509.Name = "txtScore509";
			this.txtScore509.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore509.Text = "051";
			this.txtScore509.Top = 2.154331F;
			this.txtScore509.Width = 1.207087F;
			// 
			// txtScore514
			// 
			this.txtScore514.DataField = "ITEM56";
			this.txtScore514.Height = 0.2393701F;
			this.txtScore514.Left = 5.840158F;
			this.txtScore514.MultiLine = false;
			this.txtScore514.Name = "txtScore514";
			this.txtScore514.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore514.Text = "056,456,789,000";
			this.txtScore514.Top = 2.393701F;
			this.txtScore514.Width = 1.207087F;
			// 
			// line70
			// 
			this.line70.Height = 2.88937F;
			this.line70.Left = 7.084F;
			this.line70.LineWeight = 1F;
			this.line70.Name = "line70";
			this.line70.Top = 0F;
			this.line70.Width = 0F;
			this.line70.X1 = 7.084F;
			this.line70.X2 = 7.084F;
			this.line70.Y1 = 2.88937F;
			this.line70.Y2 = 0F;
			// 
			// line71
			// 
			this.line71.Height = 0F;
			this.line71.Left = 0.007874016F;
			this.line71.LineWeight = 1F;
			this.line71.Name = "line71";
			this.line71.Top = 2.633071F;
			this.line71.Width = 7.07874F;
			this.line71.X1 = 7.086614F;
			this.line71.X2 = 0.007874016F;
			this.line71.Y1 = 2.633071F;
			this.line71.Y2 = 2.633071F;
			// 
			// line72
			// 
			this.line72.Height = 2.885827F;
			this.line72.Left = 0.007874016F;
			this.line72.LineWeight = 1F;
			this.line72.Name = "line72";
			this.line72.Top = 0F;
			this.line72.Width = 0F;
			this.line72.X1 = 0.007874016F;
			this.line72.X2 = 0.007874016F;
			this.line72.Y1 = 2.885827F;
			this.line72.Y2 = 0F;
			// 
			// line116
			// 
			this.line116.Height = 2.633071F;
			this.line116.Left = 2.044095F;
			this.line116.LineWeight = 1F;
			this.line116.Name = "line116";
			this.line116.Top = 0F;
			this.line116.Width = 0F;
			this.line116.X1 = 2.044095F;
			this.line116.X2 = 2.044095F;
			this.line116.Y1 = 2.633071F;
			this.line116.Y2 = 0F;
			// 
			// line117
			// 
			this.line117.Height = 2.633071F;
			this.line117.Left = 3.309449F;
			this.line117.LineWeight = 1F;
			this.line117.Name = "line117";
			this.line117.Top = 0F;
			this.line117.Width = 0F;
			this.line117.X1 = 3.309449F;
			this.line117.X2 = 3.309449F;
			this.line117.Y1 = 2.633071F;
			this.line117.Y2 = 0F;
			// 
			// line118
			// 
			this.line118.Height = 2.633071F;
			this.line118.Left = 4.574803F;
			this.line118.LineWeight = 1F;
			this.line118.Name = "line118";
			this.line118.Top = 0F;
			this.line118.Width = 0F;
			this.line118.X1 = 4.574803F;
			this.line118.X2 = 4.574803F;
			this.line118.Y1 = 2.633071F;
			this.line118.Y2 = 0F;
			// 
			// line119
			// 
			this.line119.Height = 2.885827F;
			this.line119.Left = 5.840158F;
			this.line119.LineWeight = 1F;
			this.line119.Name = "line119";
			this.line119.Top = 0F;
			this.line119.Width = 0F;
			this.line119.X1 = 5.840158F;
			this.line119.X2 = 5.840158F;
			this.line119.Y1 = 2.885827F;
			this.line119.Y2 = 0F;
			// 
			// line120
			// 
			this.line120.Height = 0F;
			this.line120.Left = 0.007874016F;
			this.line120.LineWeight = 1F;
			this.line120.Name = "line120";
			this.line120.Top = 0.2393701F;
			this.line120.Width = 7.07874F;
			this.line120.X1 = 7.086614F;
			this.line120.X2 = 0.007874016F;
			this.line120.Y1 = 0.2393701F;
			this.line120.Y2 = 0.2393701F;
			// 
			// line121
			// 
			this.line121.Height = 0F;
			this.line121.Left = 0.007874016F;
			this.line121.LineWeight = 1F;
			this.line121.Name = "line121";
			this.line121.Top = 2.393701F;
			this.line121.Width = 7.07874F;
			this.line121.X1 = 7.086614F;
			this.line121.X2 = 0.007874016F;
			this.line121.Y1 = 2.393701F;
			this.line121.Y2 = 2.393701F;
			// 
			// line122
			// 
			this.line122.Height = 0F;
			this.line122.Left = 0.007874016F;
			this.line122.LineWeight = 1F;
			this.line122.Name = "line122";
			this.line122.Top = 0.4787402F;
			this.line122.Width = 7.07874F;
			this.line122.X1 = 7.086614F;
			this.line122.X2 = 0.007874016F;
			this.line122.Y1 = 0.4787402F;
			this.line122.Y2 = 0.4787402F;
			// 
			// line123
			// 
			this.line123.Height = 0F;
			this.line123.Left = 0.007874016F;
			this.line123.LineWeight = 1F;
			this.line123.Name = "line123";
			this.line123.Top = 2.154331F;
			this.line123.Width = 7.07874F;
			this.line123.X1 = 7.086614F;
			this.line123.X2 = 0.007874016F;
			this.line123.Y1 = 2.154331F;
			this.line123.Y2 = 2.154331F;
			// 
			// line124
			// 
			this.line124.Height = 0F;
			this.line124.Left = 0.007874016F;
			this.line124.LineWeight = 1F;
			this.line124.Name = "line124";
			this.line124.Top = 0.7181103F;
			this.line124.Width = 7.07874F;
			this.line124.X1 = 7.086614F;
			this.line124.X2 = 0.007874016F;
			this.line124.Y1 = 0.7181103F;
			this.line124.Y2 = 0.7181103F;
			// 
			// line125
			// 
			this.line125.Height = 0F;
			this.line125.Left = 0.007874016F;
			this.line125.LineWeight = 1F;
			this.line125.Name = "line125";
			this.line125.Top = 1.914961F;
			this.line125.Width = 7.07874F;
			this.line125.X1 = 7.086614F;
			this.line125.X2 = 0.007874016F;
			this.line125.Y1 = 1.914961F;
			this.line125.Y2 = 1.914961F;
			// 
			// line126
			// 
			this.line126.Height = 0F;
			this.line126.Left = 0.007874016F;
			this.line126.LineWeight = 1F;
			this.line126.Name = "line126";
			this.line126.Top = 0.9574804F;
			this.line126.Width = 7.07874F;
			this.line126.X1 = 7.086614F;
			this.line126.X2 = 0.007874016F;
			this.line126.Y1 = 0.9574804F;
			this.line126.Y2 = 0.9574804F;
			// 
			// line127
			// 
			this.line127.Height = 0F;
			this.line127.Left = 0.007874016F;
			this.line127.LineWeight = 1F;
			this.line127.Name = "line127";
			this.line127.Top = 1.675591F;
			this.line127.Width = 7.07874F;
			this.line127.X1 = 7.086614F;
			this.line127.X2 = 0.007874016F;
			this.line127.Y1 = 1.675591F;
			this.line127.Y2 = 1.675591F;
			// 
			// line128
			// 
			this.line128.Height = 0F;
			this.line128.Left = 0.007874016F;
			this.line128.LineWeight = 1F;
			this.line128.Name = "line128";
			this.line128.Top = 1.19685F;
			this.line128.Width = 7.07874F;
			this.line128.X1 = 7.086614F;
			this.line128.X2 = 0.007874016F;
			this.line128.Y1 = 1.19685F;
			this.line128.Y2 = 1.19685F;
			// 
			// line129
			// 
			this.line129.Height = 0F;
			this.line129.Left = 0.007874016F;
			this.line129.LineWeight = 1F;
			this.line129.Name = "line129";
			this.line129.Top = 1.436221F;
			this.line129.Width = 7.07874F;
			this.line129.X1 = 7.086614F;
			this.line129.X2 = 0.007874016F;
			this.line129.Y1 = 1.436221F;
			this.line129.Y2 = 1.436221F;
			// 
			// textBox7
			// 
			this.textBox7.DataField = "ITEM57";
			this.textBox7.Height = 0.2393698F;
			this.textBox7.Left = 0.05787407F;
			this.textBox7.MultiLine = false;
			this.textBox7.Name = "textBox7";
			this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.textBox7.Text = "ITEM57";
			this.textBox7.Top = 2.650082F;
			this.textBox7.Width = 1.977559F;
			// 
			// textBox8
			// 
			this.textBox8.DataField = "ITEM58";
			this.textBox8.Height = 0.2393701F;
			this.textBox8.Left = 2.04685F;
			this.textBox8.MultiLine = false;
			this.textBox8.Name = "textBox8";
			this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.textBox8.Text = "ITEM58";
			this.textBox8.Top = 2.650082F;
			this.textBox8.Width = 1.239764F;
			// 
			// textBox9
			// 
			this.textBox9.DataField = "ITEM59";
			this.textBox9.Height = 0.2393701F;
			this.textBox9.Left = 3.309057F;
			this.textBox9.MultiLine = false;
			this.textBox9.Name = "textBox9";
			this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.textBox9.Text = "ITEM59";
			this.textBox9.Top = 2.650082F;
			this.textBox9.Width = 1.223226F;
			// 
			// textBox10
			// 
			this.textBox10.DataField = "ITEM60";
			this.textBox10.Height = 0.2393701F;
			this.textBox10.Left = 4.570082F;
			this.textBox10.MultiLine = false;
			this.textBox10.Name = "textBox10";
			this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.textBox10.Text = "060";
			this.textBox10.Top = 2.650082F;
			this.textBox10.Width = 1.225197F;
			// 
			// textBox11
			// 
			this.textBox11.DataField = "ITEM61";
			this.textBox11.Height = 0.2393701F;
			this.textBox11.Left = 5.840158F;
			this.textBox11.MultiLine = false;
			this.textBox11.Name = "textBox11";
			this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.textBox11.Text = "056,456,789,000";
			this.textBox11.Top = 2.650082F;
			this.textBox11.Width = 1.207087F;
			// 
			// line174
			// 
			this.line174.Height = 0F;
			this.line174.Left = 0.005907059F;
			this.line174.LineWeight = 1F;
			this.line174.Name = "line174";
			this.line174.Top = 2.889452F;
			this.line174.Width = 7.078737F;
			this.line174.X1 = 7.084644F;
			this.line174.X2 = 0.005907059F;
			this.line174.Y1 = 2.889452F;
			this.line174.Y2 = 2.889452F;
			// 
			// line237
			// 
			this.line237.Height = 2.63307F;
			this.line237.Left = 2.042128F;
			this.line237.LineWeight = 1F;
			this.line237.Name = "line237";
			this.line237.Top = 0.2563815F;
			this.line237.Width = 0F;
			this.line237.X1 = 2.042128F;
			this.line237.X2 = 2.042128F;
			this.line237.Y1 = 2.889452F;
			this.line237.Y2 = 0.2563815F;
			// 
			// line238
			// 
			this.line238.Height = 2.63307F;
			this.line238.Left = 3.307482F;
			this.line238.LineWeight = 1F;
			this.line238.Name = "line238";
			this.line238.Top = 0.2563815F;
			this.line238.Width = 0F;
			this.line238.X1 = 3.307482F;
			this.line238.X2 = 3.307482F;
			this.line238.Y1 = 2.889452F;
			this.line238.Y2 = 0.2563815F;
			// 
			// line239
			// 
			this.line239.Height = 2.63307F;
			this.line239.Left = 4.572836F;
			this.line239.LineWeight = 1F;
			this.line239.Name = "line239";
			this.line239.Top = 0.2563815F;
			this.line239.Width = 0F;
			this.line239.X1 = 4.572836F;
			this.line239.X2 = 4.572836F;
			this.line239.Y1 = 2.889452F;
			this.line239.Y2 = 0.2563815F;
			// 
			// group08_H
			// 
			this.group08_H.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox625,
            this.textBox626,
            this.textBox627,
            this.textBox628,
            this.textBox629,
            this.textBox630,
            this.textBox631,
            this.line83,
            this.line84,
            this.line85,
            this.line86,
            this.line87,
            this.line88,
            this.line89,
            this.line90});
			this.group08_H.DataField = "ITEM01";
			this.group08_H.Height = 0.6822835F;
			this.group08_H.KeepTogether = true;
			this.group08_H.Name = "group08_H";
			this.group08_H.Format += new System.EventHandler(this.group08_Format);
			// 
			// textBox625
			// 
			this.textBox625.Height = 0.4433071F;
			this.textBox625.Left = 0.007874016F;
			this.textBox625.MultiLine = false;
			this.textBox625.Name = "textBox625";
			this.textBox625.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: justify; vertical-align: middle";
			this.textBox625.Text = null;
			this.textBox625.Top = 0.2389764F;
			this.textBox625.Width = 7.086614F;
			// 
			// textBox626
			// 
			this.textBox626.Height = 0.2389764F;
			this.textBox626.Left = 0.02716541F;
			this.textBox626.MultiLine = false;
			this.textBox626.Name = "textBox626";
			this.textBox626.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: justify; ve" +
    "rtical-align: middle";
			this.textBox626.Text = "（８）資本";
			this.textBox626.Top = 0F;
			this.textBox626.Width = 4.237402F;
			// 
			// textBox627
			// 
			this.textBox627.Height = 0.4429134F;
			this.textBox627.Left = 0.007874012F;
			this.textBox627.MultiLine = false;
			this.textBox627.Name = "textBox627";
			this.textBox627.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; te" +
    "xt-justify: auto; vertical-align: middle";
			this.textBox627.Text = "科     目";
			this.textBox627.Top = 0.2393701F;
			this.textBox627.Width = 2.040158F;
			// 
			// textBox628
			// 
			this.textBox628.Height = 0.4429134F;
			this.textBox628.Left = 2.044095F;
			this.textBox628.MultiLine = false;
			this.textBox628.Name = "textBox628";
			this.textBox628.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox628.Text = "前年度末残高";
			this.textBox628.Top = 0.2393701F;
			this.textBox628.Width = 1.267322F;
			// 
			// textBox629
			// 
			this.textBox629.Height = 0.4440946F;
			this.textBox629.Left = 3.309449F;
			this.textBox629.MultiLine = false;
			this.textBox629.Name = "textBox629";
			this.textBox629.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox629.Text = "本年度増加額";
			this.textBox629.Top = 0.238189F;
			this.textBox629.Width = 1.265354F;
			// 
			// textBox630
			// 
			this.textBox630.Height = 0.4440945F;
			this.textBox630.Left = 4.574803F;
			this.textBox630.MultiLine = false;
			this.textBox630.Name = "textBox630";
			this.textBox630.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox630.Text = "本年度減少額";
			this.textBox630.Top = 0.238189F;
			this.textBox630.Width = 1.267717F;
			// 
			// textBox631
			// 
			this.textBox631.Height = 0.4433071F;
			this.textBox631.Left = 5.840158F;
			this.textBox631.MultiLine = false;
			this.textBox631.Name = "textBox631";
			this.textBox631.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox631.Text = "本年度末残高";
			this.textBox631.Top = 0.238189F;
			this.textBox631.Width = 1.246457F;
			// 
			// line83
			// 
			this.line83.Height = 0F;
			this.line83.Left = 0.007874016F;
			this.line83.LineWeight = 1F;
			this.line83.Name = "line83";
			this.line83.Top = 0.2389763F;
			this.line83.Width = 7.07874F;
			this.line83.X1 = 7.086614F;
			this.line83.X2 = 0.007874016F;
			this.line83.Y1 = 0.2389763F;
			this.line83.Y2 = 0.2389763F;
			// 
			// line84
			// 
			this.line84.Height = 0.4440945F;
			this.line84.Left = 5.840158F;
			this.line84.LineWeight = 1F;
			this.line84.Name = "line84";
			this.line84.Top = 0.238189F;
			this.line84.Width = 0F;
			this.line84.X1 = 5.840158F;
			this.line84.X2 = 5.840158F;
			this.line84.Y1 = 0.238189F;
			this.line84.Y2 = 0.6822835F;
			// 
			// line85
			// 
			this.line85.Height = 0.4440945F;
			this.line85.Left = 4.574803F;
			this.line85.LineWeight = 1F;
			this.line85.Name = "line85";
			this.line85.Top = 0.238189F;
			this.line85.Width = 0F;
			this.line85.X1 = 4.574803F;
			this.line85.X2 = 4.574803F;
			this.line85.Y1 = 0.238189F;
			this.line85.Y2 = 0.6822835F;
			// 
			// line86
			// 
			this.line86.Height = 0.4437008F;
			this.line86.Left = 3.309449F;
			this.line86.LineWeight = 1F;
			this.line86.Name = "line86";
			this.line86.Top = 0.2385827F;
			this.line86.Width = 0F;
			this.line86.X1 = 3.309449F;
			this.line86.X2 = 3.309449F;
			this.line86.Y1 = 0.2385827F;
			this.line86.Y2 = 0.6822835F;
			// 
			// line87
			// 
			this.line87.Height = 0.4437008F;
			this.line87.Left = 2.044095F;
			this.line87.LineWeight = 1F;
			this.line87.Name = "line87";
			this.line87.Top = 0.2385827F;
			this.line87.Width = 0F;
			this.line87.X1 = 2.044095F;
			this.line87.X2 = 2.044095F;
			this.line87.Y1 = 0.2385827F;
			this.line87.Y2 = 0.6822835F;
			// 
			// line88
			// 
			this.line88.Height = 0.4433072F;
			this.line88.Left = 7.086614F;
			this.line88.LineWeight = 1F;
			this.line88.Name = "line88";
			this.line88.Top = 0.2389763F;
			this.line88.Width = 0F;
			this.line88.X1 = 7.086614F;
			this.line88.X2 = 7.086614F;
			this.line88.Y1 = 0.2389763F;
			this.line88.Y2 = 0.6822835F;
			// 
			// line89
			// 
			this.line89.Height = 0F;
			this.line89.Left = 0.007874016F;
			this.line89.LineWeight = 1F;
			this.line89.Name = "line89";
			this.line89.Top = 0.6822835F;
			this.line89.Width = 7.07874F;
			this.line89.X1 = 7.086614F;
			this.line89.X2 = 0.007874016F;
			this.line89.Y1 = 0.6822835F;
			this.line89.Y2 = 0.6822835F;
			// 
			// line90
			// 
			this.line90.Height = 0.443307F;
			this.line90.Left = 0.007874016F;
			this.line90.LineWeight = 1F;
			this.line90.Name = "line90";
			this.line90.Top = 0.2389764F;
			this.line90.Width = 0F;
			this.line90.X1 = 0.007874016F;
			this.line90.X2 = 0.007874016F;
			this.line90.Y1 = 0.2389764F;
			this.line90.Y2 = 0.6822834F;
			// 
			// group08_F
			// 
			this.group08_F.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox632,
            this.txtScore575,
            this.txtScore515,
            this.txtScore517,
            this.txtScore518,
            this.txtScore519,
            this.txtScore520,
            this.txtScore516,
            this.txtScore521,
            this.txtScore523,
            this.txtScore524,
            this.txtScore525,
            this.txtScore526,
            this.txtScore522,
            this.txtScore527,
            this.txtScore529,
            this.txtScore530,
            this.txtScore531,
            this.txtScore532,
            this.txtScore528,
            this.txtScore533,
            this.txtScore535,
            this.txtScore536,
            this.txtScore537,
            this.txtScore538,
            this.txtScore534,
            this.txtScore539,
            this.txtScore541,
            this.txtScore542,
            this.txtScore543,
            this.txtScore544,
            this.txtScore540,
            this.txtScore545,
            this.txtScore547,
            this.txtScore548,
            this.txtScore549,
            this.txtScore550,
            this.txtScore546,
            this.txtScore551,
            this.txtScore553,
            this.txtScore554,
            this.txtScore555,
            this.textBox610,
            this.txtScore552,
            this.textBox612,
            this.textBox613,
            this.textBox614,
            this.txtScore556,
            this.txtScore557,
            this.textBox617,
            this.txtScore558,
            this.txtScore560,
            this.txtScore561,
            this.txtScore562,
            this.txtScore563,
            this.txtScore559,
            this.txtScore564,
            this.line82,
            this.txtScore573,
            this.txtScore574,
            this.txtScore576,
            this.txtScore577,
            this.txtScore578,
            this.txtScore579,
            this.txtScore565,
            this.txtScore566,
            this.txtScore567,
            this.txtScore568,
            this.txtScore569,
            this.txtScore570,
            this.txtScore571,
            this.txtScore572,
            this.line81,
            this.line80,
            this.line98,
            this.line99,
            this.line100,
            this.line101,
            this.line102,
            this.line103,
            this.line104,
            this.line105,
            this.line106,
            this.line107,
            this.line108,
            this.line109,
            this.line110,
            this.line111,
            this.line112,
            this.line113,
            this.line114,
            this.line115});
			this.group08_F.Height = 3.57774F;
			this.group08_F.KeepTogether = true;
			this.group08_F.Name = "group08_F";
			// 
			// textBox632
			// 
			this.textBox632.Height = 0.494882F;
			this.textBox632.Left = 0F;
			this.textBox632.Name = "textBox632";
			this.textBox632.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; tex" +
    "t-justify: auto; vertical-align: middle";
			this.textBox632.Text = "当期未処分剰余金";
			this.textBox632.Top = 2.633071F;
			this.textBox632.Width = 0.7318898F;
			// 
			// txtScore575
			// 
			this.txtScore575.DataField = "ITEM67";
			this.txtScore575.Height = 0.2232282F;
			this.txtScore575.Left = 0F;
			this.txtScore575.MultiLine = false;
			this.txtScore575.Name = "txtScore575";
			this.txtScore575.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle";
			this.txtScore575.Text = "ITEM67";
			this.txtScore575.Top = 3.127953F;
			this.txtScore575.Width = 2.041339F;
			// 
			// txtScore515
			// 
			this.txtScore515.DataField = "ITEM02";
			this.txtScore515.Height = 0.2393701F;
			this.txtScore515.Left = 0.06220473F;
			this.txtScore515.MultiLine = false;
			this.txtScore515.Name = "txtScore515";
			this.txtScore515.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore515.Text = "ITEM02";
			this.txtScore515.Top = 0F;
			this.txtScore515.Width = 1.977953F;
			// 
			// txtScore517
			// 
			this.txtScore517.DataField = "ITEM04";
			this.txtScore517.Height = 0.2393701F;
			this.txtScore517.Left = 3.315355F;
			this.txtScore517.MultiLine = false;
			this.txtScore517.Name = "txtScore517";
			this.txtScore517.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore517.Text = "004";
			this.txtScore517.Top = 0F;
			this.txtScore517.Width = 1.218898F;
			// 
			// txtScore518
			// 
			this.txtScore518.DataField = "ITEM05";
			this.txtScore518.Height = 0.2393701F;
			this.txtScore518.Left = 4.572049F;
			this.txtScore518.MultiLine = false;
			this.txtScore518.Name = "txtScore518";
			this.txtScore518.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore518.Text = "005";
			this.txtScore518.Top = 0F;
			this.txtScore518.Width = 1.235038F;
			// 
			// txtScore519
			// 
			this.txtScore519.DataField = "ITEM06";
			this.txtScore519.Height = 0.2393701F;
			this.txtScore519.Left = 5.847638F;
			this.txtScore519.MultiLine = false;
			this.txtScore519.Name = "txtScore519";
			this.txtScore519.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore519.Text = "006";
			this.txtScore519.Top = 0F;
			this.txtScore519.Width = 1.203544F;
			// 
			// txtScore520
			// 
			this.txtScore520.DataField = "ITEM07";
			this.txtScore520.Height = 0.2393701F;
			this.txtScore520.Left = 0.06220473F;
			this.txtScore520.MultiLine = false;
			this.txtScore520.Name = "txtScore520";
			this.txtScore520.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore520.Text = "ITEM07";
			this.txtScore520.Top = 0.2393701F;
			this.txtScore520.Width = 1.977953F;
			// 
			// txtScore516
			// 
			this.txtScore516.DataField = "ITEM03";
			this.txtScore516.Height = 0.2393701F;
			this.txtScore516.Left = 2.046063F;
			this.txtScore516.MultiLine = false;
			this.txtScore516.Name = "txtScore516";
			this.txtScore516.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore516.Text = "003";
			this.txtScore516.Top = 0F;
			this.txtScore516.Width = 1.230315F;
			// 
			// txtScore521
			// 
			this.txtScore521.DataField = "ITEM08";
			this.txtScore521.Height = 0.2393701F;
			this.txtScore521.Left = 2.046063F;
			this.txtScore521.MultiLine = false;
			this.txtScore521.Name = "txtScore521";
			this.txtScore521.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore521.Text = "008";
			this.txtScore521.Top = 0.2393701F;
			this.txtScore521.Width = 1.230315F;
			// 
			// txtScore523
			// 
			this.txtScore523.DataField = "ITEM10";
			this.txtScore523.Height = 0.2393701F;
			this.txtScore523.Left = 4.572049F;
			this.txtScore523.MultiLine = false;
			this.txtScore523.Name = "txtScore523";
			this.txtScore523.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore523.Text = "010";
			this.txtScore523.Top = 0.2393701F;
			this.txtScore523.Width = 1.235431F;
			// 
			// txtScore524
			// 
			this.txtScore524.DataField = "ITEM11";
			this.txtScore524.Height = 0.2393701F;
			this.txtScore524.Left = 5.847638F;
			this.txtScore524.MultiLine = false;
			this.txtScore524.Name = "txtScore524";
			this.txtScore524.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore524.Text = "011";
			this.txtScore524.Top = 0.2393701F;
			this.txtScore524.Width = 1.203544F;
			// 
			// txtScore525
			// 
			this.txtScore525.DataField = "ITEM12";
			this.txtScore525.Height = 0.2393701F;
			this.txtScore525.Left = 0.06220473F;
			this.txtScore525.MultiLine = false;
			this.txtScore525.Name = "txtScore525";
			this.txtScore525.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore525.Text = "ITEM12";
			this.txtScore525.Top = 0.4787402F;
			this.txtScore525.Width = 1.977953F;
			// 
			// txtScore526
			// 
			this.txtScore526.DataField = "ITEM13";
			this.txtScore526.Height = 0.2393701F;
			this.txtScore526.Left = 2.046063F;
			this.txtScore526.MultiLine = false;
			this.txtScore526.Name = "txtScore526";
			this.txtScore526.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore526.Text = "013";
			this.txtScore526.Top = 0.4787402F;
			this.txtScore526.Width = 1.229528F;
			// 
			// txtScore522
			// 
			this.txtScore522.DataField = "ITEM09";
			this.txtScore522.Height = 0.2393701F;
			this.txtScore522.Left = 3.310632F;
			this.txtScore522.MultiLine = false;
			this.txtScore522.Name = "txtScore522";
			this.txtScore522.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore522.Text = "009";
			this.txtScore522.Top = 0.2393701F;
			this.txtScore522.Width = 1.22362F;
			// 
			// txtScore527
			// 
			this.txtScore527.DataField = "ITEM14";
			this.txtScore527.Height = 0.2393701F;
			this.txtScore527.Left = 3.310632F;
			this.txtScore527.MultiLine = false;
			this.txtScore527.Name = "txtScore527";
			this.txtScore527.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore527.Text = "014";
			this.txtScore527.Top = 0.4787401F;
			this.txtScore527.Width = 1.222833F;
			// 
			// txtScore529
			// 
			this.txtScore529.DataField = "ITEM16";
			this.txtScore529.Height = 0.2393701F;
			this.txtScore529.Left = 5.847638F;
			this.txtScore529.MultiLine = false;
			this.txtScore529.Name = "txtScore529";
			this.txtScore529.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore529.Text = "016";
			this.txtScore529.Top = 0.4787401F;
			this.txtScore529.Width = 1.203544F;
			// 
			// txtScore530
			// 
			this.txtScore530.DataField = "ITEM17";
			this.txtScore530.Height = 0.2393701F;
			this.txtScore530.Left = 0.06220473F;
			this.txtScore530.MultiLine = false;
			this.txtScore530.Name = "txtScore530";
			this.txtScore530.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore530.Text = "ITEM17";
			this.txtScore530.Top = 0.7181104F;
			this.txtScore530.Width = 1.977953F;
			// 
			// txtScore531
			// 
			this.txtScore531.DataField = "ITEM18";
			this.txtScore531.Height = 0.2393701F;
			this.txtScore531.Left = 2.04685F;
			this.txtScore531.MultiLine = false;
			this.txtScore531.Name = "txtScore531";
			this.txtScore531.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore531.Text = "018";
			this.txtScore531.Top = 0.7181104F;
			this.txtScore531.Width = 1.228741F;
			// 
			// txtScore532
			// 
			this.txtScore532.DataField = "ITEM19";
			this.txtScore532.Height = 0.2393701F;
			this.txtScore532.Left = 3.310632F;
			this.txtScore532.MultiLine = false;
			this.txtScore532.Name = "txtScore532";
			this.txtScore532.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore532.Text = "019";
			this.txtScore532.Top = 0.7181104F;
			this.txtScore532.Width = 1.22362F;
			// 
			// txtScore528
			// 
			this.txtScore528.DataField = "ITEM15";
			this.txtScore528.Height = 0.2393701F;
			this.txtScore528.Left = 4.572836F;
			this.txtScore528.MultiLine = false;
			this.txtScore528.Name = "txtScore528";
			this.txtScore528.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore528.Text = "015";
			this.txtScore528.Top = 0.4787401F;
			this.txtScore528.Width = 1.23425F;
			// 
			// txtScore533
			// 
			this.txtScore533.DataField = "ITEM20";
			this.txtScore533.Height = 0.2393701F;
			this.txtScore533.Left = 4.572049F;
			this.txtScore533.MultiLine = false;
			this.txtScore533.Name = "txtScore533";
			this.txtScore533.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore533.Text = "020";
			this.txtScore533.Top = 0.7181104F;
			this.txtScore533.Width = 1.235431F;
			// 
			// txtScore535
			// 
			this.txtScore535.DataField = "ITEM22";
			this.txtScore535.Height = 0.2393701F;
			this.txtScore535.Left = 0.06259848F;
			this.txtScore535.MultiLine = false;
			this.txtScore535.Name = "txtScore535";
			this.txtScore535.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore535.Text = "ITEM22";
			this.txtScore535.Top = 0.9574805F;
			this.txtScore535.Width = 1.984252F;
			// 
			// txtScore536
			// 
			this.txtScore536.DataField = "ITEM23";
			this.txtScore536.Height = 0.2393701F;
			this.txtScore536.Left = 2.046063F;
			this.txtScore536.MultiLine = false;
			this.txtScore536.Name = "txtScore536";
			this.txtScore536.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore536.Text = "023";
			this.txtScore536.Top = 0.9574805F;
			this.txtScore536.Width = 1.229528F;
			// 
			// txtScore537
			// 
			this.txtScore537.DataField = "ITEM24";
			this.txtScore537.Height = 0.2393701F;
			this.txtScore537.Left = 3.310632F;
			this.txtScore537.MultiLine = false;
			this.txtScore537.Name = "txtScore537";
			this.txtScore537.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore537.Text = "024";
			this.txtScore537.Top = 0.9574805F;
			this.txtScore537.Width = 1.22362F;
			// 
			// txtScore538
			// 
			this.txtScore538.DataField = "ITEM25";
			this.txtScore538.Height = 0.2393701F;
			this.txtScore538.Left = 4.572049F;
			this.txtScore538.MultiLine = false;
			this.txtScore538.Name = "txtScore538";
			this.txtScore538.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore538.Text = "025";
			this.txtScore538.Top = 0.9574805F;
			this.txtScore538.Width = 1.235431F;
			// 
			// txtScore534
			// 
			this.txtScore534.DataField = "ITEM21";
			this.txtScore534.Height = 0.2393701F;
			this.txtScore534.Left = 5.847638F;
			this.txtScore534.MultiLine = false;
			this.txtScore534.Name = "txtScore534";
			this.txtScore534.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore534.Text = "021";
			this.txtScore534.Top = 0.7181104F;
			this.txtScore534.Width = 1.203544F;
			// 
			// txtScore539
			// 
			this.txtScore539.DataField = "ITEM26";
			this.txtScore539.Height = 0.2393701F;
			this.txtScore539.Left = 5.84764F;
			this.txtScore539.MultiLine = false;
			this.txtScore539.Name = "txtScore539";
			this.txtScore539.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore539.Text = "026";
			this.txtScore539.Top = 0.9574805F;
			this.txtScore539.Width = 1.203542F;
			// 
			// txtScore541
			// 
			this.txtScore541.DataField = "ITEM28";
			this.txtScore541.Height = 0.2393701F;
			this.txtScore541.Left = 2.046063F;
			this.txtScore541.MultiLine = false;
			this.txtScore541.Name = "txtScore541";
			this.txtScore541.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore541.Text = "028";
			this.txtScore541.Top = 1.196851F;
			this.txtScore541.Width = 1.229528F;
			// 
			// txtScore542
			// 
			this.txtScore542.DataField = "ITEM29";
			this.txtScore542.Height = 0.2393701F;
			this.txtScore542.Left = 3.310632F;
			this.txtScore542.MultiLine = false;
			this.txtScore542.Name = "txtScore542";
			this.txtScore542.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore542.Text = "029";
			this.txtScore542.Top = 1.196851F;
			this.txtScore542.Width = 1.22362F;
			// 
			// txtScore543
			// 
			this.txtScore543.DataField = "ITEM30";
			this.txtScore543.Height = 0.2393701F;
			this.txtScore543.Left = 4.572049F;
			this.txtScore543.MultiLine = false;
			this.txtScore543.Name = "txtScore543";
			this.txtScore543.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore543.Text = "030";
			this.txtScore543.Top = 1.196851F;
			this.txtScore543.Width = 1.235037F;
			// 
			// txtScore544
			// 
			this.txtScore544.DataField = "ITEM31";
			this.txtScore544.Height = 0.2393701F;
			this.txtScore544.Left = 5.847638F;
			this.txtScore544.MultiLine = false;
			this.txtScore544.Name = "txtScore544";
			this.txtScore544.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore544.Text = "031";
			this.txtScore544.Top = 1.196851F;
			this.txtScore544.Width = 1.203544F;
			// 
			// txtScore540
			// 
			this.txtScore540.DataField = "ITEM27";
			this.txtScore540.Height = 0.2393701F;
			this.txtScore540.Left = 0.06220473F;
			this.txtScore540.MultiLine = false;
			this.txtScore540.Name = "txtScore540";
			this.txtScore540.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore540.Text = "ITEM27";
			this.txtScore540.Top = 1.196851F;
			this.txtScore540.Width = 1.977953F;
			// 
			// txtScore545
			// 
			this.txtScore545.DataField = "ITEM32";
			this.txtScore545.Height = 0.2393701F;
			this.txtScore545.Left = 0.06220473F;
			this.txtScore545.MultiLine = false;
			this.txtScore545.Name = "txtScore545";
			this.txtScore545.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore545.Text = "ITEM32";
			this.txtScore545.Top = 1.43622F;
			this.txtScore545.Width = 1.977953F;
			// 
			// txtScore547
			// 
			this.txtScore547.DataField = "ITEM34";
			this.txtScore547.Height = 0.2393701F;
			this.txtScore547.Left = 3.311024F;
			this.txtScore547.MultiLine = false;
			this.txtScore547.Name = "txtScore547";
			this.txtScore547.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore547.Text = "034";
			this.txtScore547.Top = 1.43622F;
			this.txtScore547.Width = 1.22362F;
			// 
			// txtScore548
			// 
			this.txtScore548.DataField = "ITEM35";
			this.txtScore548.Height = 0.2393701F;
			this.txtScore548.Left = 4.572049F;
			this.txtScore548.MultiLine = false;
			this.txtScore548.Name = "txtScore548";
			this.txtScore548.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore548.Text = "035";
			this.txtScore548.Top = 1.43622F;
			this.txtScore548.Width = 1.235037F;
			// 
			// txtScore549
			// 
			this.txtScore549.DataField = "ITEM36";
			this.txtScore549.Height = 0.2393701F;
			this.txtScore549.Left = 5.847638F;
			this.txtScore549.MultiLine = false;
			this.txtScore549.Name = "txtScore549";
			this.txtScore549.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore549.Text = "036";
			this.txtScore549.Top = 1.43622F;
			this.txtScore549.Width = 1.203544F;
			// 
			// txtScore550
			// 
			this.txtScore550.DataField = "ITEM37";
			this.txtScore550.Height = 0.2393701F;
			this.txtScore550.Left = 0.06259848F;
			this.txtScore550.MultiLine = false;
			this.txtScore550.Name = "txtScore550";
			this.txtScore550.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore550.Text = "ITEM37";
			this.txtScore550.Top = 1.675591F;
			this.txtScore550.Width = 1.984252F;
			// 
			// txtScore546
			// 
			this.txtScore546.DataField = "ITEM33";
			this.txtScore546.Height = 0.2393701F;
			this.txtScore546.Left = 2.040158F;
			this.txtScore546.MultiLine = false;
			this.txtScore546.Name = "txtScore546";
			this.txtScore546.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore546.Text = "033";
			this.txtScore546.Top = 1.436221F;
			this.txtScore546.Width = 1.226772F;
			// 
			// txtScore551
			// 
			this.txtScore551.DataField = "ITEM38";
			this.txtScore551.Height = 0.2393701F;
			this.txtScore551.Left = 2.040158F;
			this.txtScore551.MultiLine = false;
			this.txtScore551.Name = "txtScore551";
			this.txtScore551.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore551.Text = "038";
			this.txtScore551.Top = 1.675591F;
			this.txtScore551.Width = 1.226772F;
			// 
			// txtScore553
			// 
			this.txtScore553.DataField = "ITEM40";
			this.txtScore553.Height = 0.2393701F;
			this.txtScore553.Left = 4.572441F;
			this.txtScore553.MultiLine = false;
			this.txtScore553.Name = "txtScore553";
			this.txtScore553.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore553.Text = "040";
			this.txtScore553.Top = 1.675591F;
			this.txtScore553.Width = 1.234644F;
			// 
			// txtScore554
			// 
			this.txtScore554.DataField = "ITEM41";
			this.txtScore554.Height = 0.2393701F;
			this.txtScore554.Left = 5.847638F;
			this.txtScore554.MultiLine = false;
			this.txtScore554.Name = "txtScore554";
			this.txtScore554.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore554.Text = "041";
			this.txtScore554.Top = 1.675591F;
			this.txtScore554.Width = 1.203544F;
			// 
			// txtScore555
			// 
			this.txtScore555.DataField = "ITEM42";
			this.txtScore555.Height = 0.2393701F;
			this.txtScore555.Left = 0.06259848F;
			this.txtScore555.MultiLine = false;
			this.txtScore555.Name = "txtScore555";
			this.txtScore555.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore555.Text = "ITEM42";
			this.txtScore555.Top = 1.914961F;
			this.txtScore555.Width = 1.984252F;
			// 
			// textBox610
			// 
			this.textBox610.DataField = "ITEM43";
			this.textBox610.Height = 0.2393701F;
			this.textBox610.Left = 2.046063F;
			this.textBox610.MultiLine = false;
			this.textBox610.Name = "textBox610";
			this.textBox610.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.textBox610.Text = "043";
			this.textBox610.Top = 1.914961F;
			this.textBox610.Width = 1.229528F;
			// 
			// txtScore552
			// 
			this.txtScore552.DataField = "ITEM39";
			this.txtScore552.Height = 0.2393701F;
			this.txtScore552.Left = 3.310632F;
			this.txtScore552.MultiLine = false;
			this.txtScore552.Name = "txtScore552";
			this.txtScore552.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore552.Text = "039";
			this.txtScore552.Top = 1.675591F;
			this.txtScore552.Width = 1.22362F;
			// 
			// textBox612
			// 
			this.textBox612.DataField = "ITEM44";
			this.textBox612.Height = 0.2393701F;
			this.textBox612.Left = 3.311024F;
			this.textBox612.MultiLine = false;
			this.textBox612.Name = "textBox612";
			this.textBox612.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.textBox612.Text = "044";
			this.textBox612.Top = 1.914961F;
			this.textBox612.Width = 1.223226F;
			// 
			// textBox613
			// 
			this.textBox613.DataField = "ITEM46";
			this.textBox613.Height = 0.2393701F;
			this.textBox613.Left = 5.84803F;
			this.textBox613.MultiLine = false;
			this.textBox613.Name = "textBox613";
			this.textBox613.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.textBox613.Text = "046";
			this.textBox613.Top = 1.914961F;
			this.textBox613.Width = 1.203152F;
			// 
			// textBox614
			// 
			this.textBox614.DataField = "ITEM47";
			this.textBox614.Height = 0.2393701F;
			this.textBox614.Left = 0.06220473F;
			this.textBox614.MultiLine = false;
			this.textBox614.Name = "textBox614";
			this.textBox614.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.textBox614.Text = "ITEM47";
			this.textBox614.Top = 2.154331F;
			this.textBox614.Width = 1.977953F;
			// 
			// txtScore556
			// 
			this.txtScore556.DataField = "ITEM48";
			this.txtScore556.Height = 0.2393701F;
			this.txtScore556.Left = 2.04685F;
			this.txtScore556.MultiLine = false;
			this.txtScore556.Name = "txtScore556";
			this.txtScore556.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore556.Text = "048";
			this.txtScore556.Top = 2.154331F;
			this.txtScore556.Width = 1.228741F;
			// 
			// txtScore557
			// 
			this.txtScore557.DataField = "ITEM49";
			this.txtScore557.Height = 0.2393701F;
			this.txtScore557.Left = 3.310632F;
			this.txtScore557.MultiLine = false;
			this.txtScore557.Name = "txtScore557";
			this.txtScore557.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore557.Text = "049";
			this.txtScore557.Top = 2.154331F;
			this.txtScore557.Width = 1.222833F;
			// 
			// textBox617
			// 
			this.textBox617.DataField = "ITEM45";
			this.textBox617.Height = 0.2393701F;
			this.textBox617.Left = 4.572049F;
			this.textBox617.MultiLine = false;
			this.textBox617.Name = "textBox617";
			this.textBox617.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.textBox617.Text = "045";
			this.textBox617.Top = 1.914961F;
			this.textBox617.Width = 1.235038F;
			// 
			// txtScore558
			// 
			this.txtScore558.DataField = "ITEM50";
			this.txtScore558.Height = 0.2393701F;
			this.txtScore558.Left = 4.571261F;
			this.txtScore558.MultiLine = false;
			this.txtScore558.Name = "txtScore558";
			this.txtScore558.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore558.Text = "050";
			this.txtScore558.Top = 2.154331F;
			this.txtScore558.Width = 1.235825F;
			// 
			// txtScore560
			// 
			this.txtScore560.DataField = "ITEM52";
			this.txtScore560.Height = 0.2393701F;
			this.txtScore560.Left = 0.06259848F;
			this.txtScore560.MultiLine = false;
			this.txtScore560.Name = "txtScore560";
			this.txtScore560.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtScore560.Text = "ITEM52";
			this.txtScore560.Top = 2.393701F;
			this.txtScore560.Width = 1.984252F;
			// 
			// txtScore561
			// 
			this.txtScore561.DataField = "ITEM53";
			this.txtScore561.Height = 0.2393701F;
			this.txtScore561.Left = 2.046063F;
			this.txtScore561.MultiLine = false;
			this.txtScore561.Name = "txtScore561";
			this.txtScore561.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore561.Text = "053";
			this.txtScore561.Top = 2.393701F;
			this.txtScore561.Width = 1.230315F;
			// 
			// txtScore562
			// 
			this.txtScore562.DataField = "ITEM54";
			this.txtScore562.Height = 0.2393701F;
			this.txtScore562.Left = 3.311024F;
			this.txtScore562.MultiLine = false;
			this.txtScore562.Name = "txtScore562";
			this.txtScore562.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore562.Text = "054";
			this.txtScore562.Top = 2.393701F;
			this.txtScore562.Width = 1.223226F;
			// 
			// txtScore563
			// 
			this.txtScore563.DataField = "ITEM55";
			this.txtScore563.Height = 0.2393701F;
			this.txtScore563.Left = 4.572049F;
			this.txtScore563.MultiLine = false;
			this.txtScore563.Name = "txtScore563";
			this.txtScore563.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore563.Text = "055";
			this.txtScore563.Top = 2.393701F;
			this.txtScore563.Width = 1.235038F;
			// 
			// txtScore559
			// 
			this.txtScore559.DataField = "ITEM51";
			this.txtScore559.Height = 0.2393701F;
			this.txtScore559.Left = 5.847638F;
			this.txtScore559.MultiLine = false;
			this.txtScore559.Name = "txtScore559";
			this.txtScore559.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore559.Text = "051";
			this.txtScore559.Top = 2.154331F;
			this.txtScore559.Width = 1.203544F;
			// 
			// txtScore564
			// 
			this.txtScore564.DataField = "ITEM56";
			this.txtScore564.Height = 0.2393701F;
			this.txtScore564.Left = 5.848032F;
			this.txtScore564.MultiLine = false;
			this.txtScore564.Name = "txtScore564";
			this.txtScore564.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore564.Text = "056,456,789,000";
			this.txtScore564.Top = 2.393701F;
			this.txtScore564.Width = 1.20315F;
			// 
			// line82
			// 
			this.line82.Height = 3.351181F;
			this.line82.Left = 0.007874016F;
			this.line82.LineWeight = 1F;
			this.line82.Name = "line82";
			this.line82.Top = 0F;
			this.line82.Width = 0F;
			this.line82.X1 = 0.007874016F;
			this.line82.X2 = 0.007874016F;
			this.line82.Y1 = 3.351181F;
			this.line82.Y2 = 0F;
			// 
			// txtScore573
			// 
			this.txtScore573.DataField = "ITEM65";
			this.txtScore573.Height = 0.2555121F;
			this.txtScore573.Left = 4.57126F;
			this.txtScore573.MultiLine = false;
			this.txtScore573.Name = "txtScore573";
			this.txtScore573.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; te" +
    "xt-justify: auto; vertical-align: middle";
			this.txtScore573.Tag = "";
			this.txtScore573.Text = "065";
			this.txtScore573.Top = 2.872441F;
			this.txtScore573.Width = 1.235431F;
			// 
			// txtScore574
			// 
			this.txtScore574.DataField = "ITEM66";
			this.txtScore574.Height = 0.2555121F;
			this.txtScore574.Left = 5.850393F;
			this.txtScore574.MultiLine = false;
			this.txtScore574.Name = "txtScore574";
			this.txtScore574.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; te" +
    "xt-justify: auto; vertical-align: middle";
			this.txtScore574.Text = "066";
			this.txtScore574.Top = 2.872441F;
			this.txtScore574.Width = 1.197244F;
			// 
			// txtScore576
			// 
			this.txtScore576.DataField = "ITEM68";
			this.txtScore576.Height = 0.2232282F;
			this.txtScore576.Left = 2.04685F;
			this.txtScore576.MultiLine = false;
			this.txtScore576.Name = "txtScore576";
			this.txtScore576.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore576.Text = "068";
			this.txtScore576.Top = 3.127953F;
			this.txtScore576.Width = 1.230315F;
			// 
			// txtScore577
			// 
			this.txtScore577.DataField = "ITEM69";
			this.txtScore577.Height = 0.2232282F;
			this.txtScore577.Left = 3.315355F;
			this.txtScore577.MultiLine = false;
			this.txtScore577.Name = "txtScore577";
			this.txtScore577.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore577.Text = "069";
			this.txtScore577.Top = 3.127953F;
			this.txtScore577.Visible = false;
			this.txtScore577.Width = 1.223227F;
			// 
			// txtScore578
			// 
			this.txtScore578.DataField = "ITEM70";
			this.txtScore578.Height = 0.2232282F;
			this.txtScore578.Left = 4.577953F;
			this.txtScore578.MultiLine = false;
			this.txtScore578.Name = "txtScore578";
			this.txtScore578.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore578.Text = "070";
			this.txtScore578.Top = 3.127953F;
			this.txtScore578.Visible = false;
			this.txtScore578.Width = 1.235038F;
			// 
			// txtScore579
			// 
			this.txtScore579.DataField = "ITEM71";
			this.txtScore579.Height = 0.2232282F;
			this.txtScore579.Left = 5.849999F;
			this.txtScore579.MultiLine = false;
			this.txtScore579.Name = "txtScore579";
			this.txtScore579.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore579.Text = "071,456,789,000";
			this.txtScore579.Top = 3.127953F;
			this.txtScore579.Width = 1.199214F;
			// 
			// txtScore565
			// 
			this.txtScore565.DataField = "ITEM57";
			this.txtScore565.Height = 0.2393701F;
			this.txtScore565.Left = 0.7629922F;
			this.txtScore565.MultiLine = false;
			this.txtScore565.Name = "txtScore565";
			this.txtScore565.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: left; verti" +
    "cal-align: middle";
			this.txtScore565.Text = "ITEM57";
			this.txtScore565.Top = 2.633071F;
			this.txtScore565.Width = 1.283465F;
			// 
			// txtScore566
			// 
			this.txtScore566.DataField = "ITEM58";
			this.txtScore566.Height = 0.2393701F;
			this.txtScore566.Left = 2.041732F;
			this.txtScore566.MultiLine = false;
			this.txtScore566.Name = "txtScore566";
			this.txtScore566.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore566.Text = "058";
			this.txtScore566.Top = 2.633071F;
			this.txtScore566.Width = 1.233858F;
			// 
			// txtScore567
			// 
			this.txtScore567.DataField = "ITEM59";
			this.txtScore567.Height = 0.2393701F;
			this.txtScore567.Left = 3.315355F;
			this.txtScore567.MultiLine = false;
			this.txtScore567.Name = "txtScore567";
			this.txtScore567.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore567.Text = "059";
			this.txtScore567.Top = 2.633071F;
			this.txtScore567.Width = 1.220077F;
			// 
			// txtScore568
			// 
			this.txtScore568.DataField = "ITEM60";
			this.txtScore568.Height = 0.2393701F;
			this.txtScore568.Left = 4.572835F;
			this.txtScore568.MultiLine = false;
			this.txtScore568.Name = "txtScore568";
			this.txtScore568.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore568.Text = "060";
			this.txtScore568.Top = 2.633071F;
			this.txtScore568.Width = 1.240156F;
			// 
			// txtScore569
			// 
			this.txtScore569.DataField = "ITEM61";
			this.txtScore569.Height = 0.2393701F;
			this.txtScore569.Left = 5.850393F;
			this.txtScore569.MultiLine = false;
			this.txtScore569.Name = "txtScore569";
			this.txtScore569.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore569.Text = "061";
			this.txtScore569.Top = 2.633071F;
			this.txtScore569.Width = 1.200788F;
			// 
			// txtScore570
			// 
			this.txtScore570.DataField = "ITEM62";
			this.txtScore570.Height = 0.255512F;
			this.txtScore570.Left = 0.7629922F;
			this.txtScore570.MultiLine = false;
			this.txtScore570.Name = "txtScore570";
			this.txtScore570.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: left; verti" +
    "cal-align: middle";
			this.txtScore570.Text = "(うち当期剰余金)";
			this.txtScore570.Top = 2.872441F;
			this.txtScore570.Width = 1.277166F;
			// 
			// txtScore571
			// 
			this.txtScore571.DataField = "ITEM63";
			this.txtScore571.Height = 0.255512F;
			this.txtScore571.Left = 2.040158F;
			this.txtScore571.MultiLine = false;
			this.txtScore571.Name = "txtScore571";
			this.txtScore571.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore571.Text = "063";
			this.txtScore571.Top = 2.872441F;
			this.txtScore571.Width = 1.226772F;
			// 
			// txtScore572
			// 
			this.txtScore572.DataField = "ITEM64";
			this.txtScore572.Height = 0.255512F;
			this.txtScore572.Left = 3.315355F;
			this.txtScore572.MultiLine = false;
			this.txtScore572.Name = "txtScore572";
			this.txtScore572.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
			this.txtScore572.Text = "064";
			this.txtScore572.Top = 2.872441F;
			this.txtScore572.Width = 1.220077F;
			// 
			// line81
			// 
			this.line81.Height = 0F;
			this.line81.Left = 0.007874016F;
			this.line81.LineWeight = 1F;
			this.line81.Name = "line81";
			this.line81.Top = 3.351181F;
			this.line81.Width = 7.082668F;
			this.line81.X1 = 7.090542F;
			this.line81.X2 = 0.007874016F;
			this.line81.Y1 = 3.351181F;
			this.line81.Y2 = 3.351181F;
			// 
			// line80
			// 
			this.line80.Height = 3.351181F;
			this.line80.Left = 7.086614F;
			this.line80.LineWeight = 1F;
			this.line80.Name = "line80";
			this.line80.Top = 0F;
			this.line80.Width = 0F;
			this.line80.X1 = 7.086614F;
			this.line80.X2 = 7.086614F;
			this.line80.Y1 = 3.351181F;
			this.line80.Y2 = 0F;
			// 
			// line98
			// 
			this.line98.Height = 3.351181F;
			this.line98.Left = 2.044095F;
			this.line98.LineWeight = 1F;
			this.line98.Name = "line98";
			this.line98.Top = 0F;
			this.line98.Width = 0F;
			this.line98.X1 = 2.044095F;
			this.line98.X2 = 2.044095F;
			this.line98.Y1 = 3.351181F;
			this.line98.Y2 = 0F;
			// 
			// line99
			// 
			this.line99.Height = 3.351181F;
			this.line99.Left = 3.309449F;
			this.line99.LineWeight = 1F;
			this.line99.Name = "line99";
			this.line99.Top = 0F;
			this.line99.Width = 0F;
			this.line99.X1 = 3.309449F;
			this.line99.X2 = 3.309449F;
			this.line99.Y1 = 3.351181F;
			this.line99.Y2 = 0F;
			// 
			// line100
			// 
			this.line100.Height = 3.351181F;
			this.line100.Left = 4.574803F;
			this.line100.LineWeight = 1F;
			this.line100.Name = "line100";
			this.line100.Top = 0F;
			this.line100.Width = 0F;
			this.line100.X1 = 4.574803F;
			this.line100.X2 = 4.574803F;
			this.line100.Y1 = 3.351181F;
			this.line100.Y2 = 0F;
			// 
			// line101
			// 
			this.line101.Height = 3.351181F;
			this.line101.Left = 5.840158F;
			this.line101.LineWeight = 1F;
			this.line101.Name = "line101";
			this.line101.Top = 1.490116E-08F;
			this.line101.Width = 0F;
			this.line101.X1 = 5.840158F;
			this.line101.X2 = 5.840158F;
			this.line101.Y1 = 3.351181F;
			this.line101.Y2 = 1.490116E-08F;
			// 
			// line102
			// 
			this.line102.Height = 0F;
			this.line102.Left = 0.007874016F;
			this.line102.LineWeight = 1F;
			this.line102.Name = "line102";
			this.line102.Top = 0.2393701F;
			this.line102.Width = 7.078737F;
			this.line102.X1 = 7.086611F;
			this.line102.X2 = 0.007874016F;
			this.line102.Y1 = 0.2393701F;
			this.line102.Y2 = 0.2393701F;
			// 
			// line103
			// 
			this.line103.Height = 0F;
			this.line103.Left = 0.007874016F;
			this.line103.LineWeight = 1F;
			this.line103.Name = "line103";
			this.line103.Top = 0.4787402F;
			this.line103.Width = 7.078737F;
			this.line103.X1 = 7.086611F;
			this.line103.X2 = 0.007874016F;
			this.line103.Y1 = 0.4787402F;
			this.line103.Y2 = 0.4787402F;
			// 
			// line104
			// 
			this.line104.Height = 0F;
			this.line104.Left = 0.007874016F;
			this.line104.LineWeight = 1F;
			this.line104.Name = "line104";
			this.line104.Top = 0.7181103F;
			this.line104.Width = 7.078737F;
			this.line104.X1 = 7.086611F;
			this.line104.X2 = 0.007874016F;
			this.line104.Y1 = 0.7181103F;
			this.line104.Y2 = 0.7181103F;
			// 
			// line105
			// 
			this.line105.Height = 0F;
			this.line105.Left = 0.007874016F;
			this.line105.LineWeight = 1F;
			this.line105.Name = "line105";
			this.line105.Top = 0.9574804F;
			this.line105.Width = 7.078737F;
			this.line105.X1 = 7.086611F;
			this.line105.X2 = 0.007874016F;
			this.line105.Y1 = 0.9574804F;
			this.line105.Y2 = 0.9574804F;
			// 
			// line106
			// 
			this.line106.Height = 0F;
			this.line106.Left = 0.007874016F;
			this.line106.LineWeight = 1F;
			this.line106.Name = "line106";
			this.line106.Top = 1.19685F;
			this.line106.Width = 7.078737F;
			this.line106.X1 = 7.086611F;
			this.line106.X2 = 0.007874016F;
			this.line106.Y1 = 1.19685F;
			this.line106.Y2 = 1.19685F;
			// 
			// line107
			// 
			this.line107.Height = 0F;
			this.line107.Left = 0.007874016F;
			this.line107.LineWeight = 1F;
			this.line107.Name = "line107";
			this.line107.Top = 1.436221F;
			this.line107.Width = 7.078737F;
			this.line107.X1 = 7.086611F;
			this.line107.X2 = 0.007874016F;
			this.line107.Y1 = 1.436221F;
			this.line107.Y2 = 1.436221F;
			// 
			// line108
			// 
			this.line108.Height = 0F;
			this.line108.Left = 0.007874016F;
			this.line108.LineWeight = 1F;
			this.line108.Name = "line108";
			this.line108.Top = 1.675591F;
			this.line108.Width = 7.07874F;
			this.line108.X1 = 7.086614F;
			this.line108.X2 = 0.007874016F;
			this.line108.Y1 = 1.675591F;
			this.line108.Y2 = 1.675591F;
			// 
			// line109
			// 
			this.line109.Height = 0F;
			this.line109.Left = 0.007874016F;
			this.line109.LineWeight = 1F;
			this.line109.Name = "line109";
			this.line109.Top = 1.914961F;
			this.line109.Width = 7.07874F;
			this.line109.X1 = 7.086614F;
			this.line109.X2 = 0.007874016F;
			this.line109.Y1 = 1.914961F;
			this.line109.Y2 = 1.914961F;
			// 
			// line110
			// 
			this.line110.Height = 0F;
			this.line110.Left = 0.007874016F;
			this.line110.LineWeight = 1F;
			this.line110.Name = "line110";
			this.line110.Top = 2.154331F;
			this.line110.Width = 7.078737F;
			this.line110.X1 = 7.086611F;
			this.line110.X2 = 0.007874016F;
			this.line110.Y1 = 2.154331F;
			this.line110.Y2 = 2.154331F;
			// 
			// line111
			// 
			this.line111.Height = 0F;
			this.line111.Left = 0.007874016F;
			this.line111.LineWeight = 1F;
			this.line111.Name = "line111";
			this.line111.Top = 2.393701F;
			this.line111.Width = 7.078737F;
			this.line111.X1 = 7.086611F;
			this.line111.X2 = 0.007874016F;
			this.line111.Y1 = 2.393701F;
			this.line111.Y2 = 2.393701F;
			// 
			// line112
			// 
			this.line112.Height = 0F;
			this.line112.Left = 0.007874016F;
			this.line112.LineWeight = 1F;
			this.line112.Name = "line112";
			this.line112.Top = 2.633071F;
			this.line112.Width = 7.078737F;
			this.line112.X1 = 7.086611F;
			this.line112.X2 = 0.007874016F;
			this.line112.Y1 = 2.633071F;
			this.line112.Y2 = 2.633071F;
			// 
			// line113
			// 
			this.line113.Height = 0F;
			this.line113.Left = 0.007874016F;
			this.line113.LineWeight = 1F;
			this.line113.Name = "line113";
			this.line113.Top = 3.127953F;
			this.line113.Width = 7.078737F;
			this.line113.X1 = 7.086611F;
			this.line113.X2 = 0.007874016F;
			this.line113.Y1 = 3.127953F;
			this.line113.Y2 = 3.127953F;
			// 
			// line114
			// 
			this.line114.Height = 0.0001070499F;
			this.line114.Left = 0.7248055F;
			this.line114.LineWeight = 1F;
			this.line114.Name = "line114";
			this.line114.Top = 2.872441F;
			this.line114.Width = 6.356692F;
			this.line114.X1 = 7.081497F;
			this.line114.X2 = 0.7248055F;
			this.line114.Y1 = 2.872441F;
			this.line114.Y2 = 2.872548F;
			// 
			// line115
			// 
			this.line115.Height = 0.4948821F;
			this.line115.Left = 0.7318898F;
			this.line115.LineWeight = 1F;
			this.line115.Name = "line115";
			this.line115.Top = 2.633071F;
			this.line115.Width = 2.384186E-07F;
			this.line115.X1 = 0.73189F;
			this.line115.X2 = 0.7318898F;
			this.line115.Y1 = 2.633071F;
			this.line115.Y2 = 3.127953F;
			// 
			// ZMYR1021R
			// 
			this.MasterReport = false;
			this.PageSettings.DefaultPaperSize = false;
			this.PageSettings.Margins.Bottom = 0.3937008F;
			this.PageSettings.Margins.Left = 0.5905512F;
			this.PageSettings.Margins.Right = 0.5905512F;
			this.PageSettings.Margins.Top = 0.5905512F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
			this.PageSettings.PaperHeight = 11.69291F;
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
			this.PageSettings.PaperWidth = 8.267716F;
			this.PrintWidth = 7.090551F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.group01_H);
			this.Sections.Add(this.group02_H);
			this.Sections.Add(this.group03_H);
			this.Sections.Add(this.group04_H);
			this.Sections.Add(this.group05_H);
			this.Sections.Add(this.group06_H);
			this.Sections.Add(this.group07_H);
			this.Sections.Add(this.group08_H);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.group08_F);
			this.Sections.Add(this.group07_F);
			this.Sections.Add(this.group06_F);
			this.Sections.Add(this.group05_F);
			this.Sections.Add(this.group04_F);
			this.Sections.Add(this.group03_F);
			this.Sections.Add(this.group02_F);
			this.Sections.Add(this.group01_F);
			this.Sections.Add(this.pageFooter);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.groupId)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore001)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore003)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore004)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore005)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore006)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore002)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle07)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle08)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle09)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore007)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore009)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore010)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore011)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore012)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore008)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore013)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore015)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore016)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore017)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore018)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore014)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore019)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore021)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore022)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore023)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore024)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore020)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore025)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore027)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore028)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore029)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore030)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore026)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore031)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore033)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore034)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore035)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore036)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore032)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore037)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore039)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore040)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore041)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore042)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore038)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore043)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore045)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore046)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore047)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore048)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore044)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore049)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore051)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore052)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore053)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore054)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore050)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore161)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore055)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore057)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore058)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore059)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore060)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore056)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore061)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore063)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore064)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore065)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore066)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore062)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore067)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore069)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore070)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore071)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore072)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore068)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore073)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore075)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore076)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore077)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore078)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore074)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore079)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore081)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore082)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore083)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore084)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore080)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore085)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore087)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore088)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore089)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore090)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore086)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore091)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore093)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore094)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore095)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore096)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore092)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore097)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore099)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore098)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore105)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore164)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore138)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore134)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore139)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore141)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore142)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore143)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore144)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore140)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore145)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore147)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore148)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore149)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore150)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore146)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore151)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore153)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore154)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore155)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore156)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore152)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore157)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore159)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore160)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore162)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore158)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore163)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox193)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox194)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox195)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox196)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox197)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox198)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox199)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore229)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore225)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore165)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore167)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore168)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore169)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore170)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore166)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore171)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore173)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore174)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore172)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore179)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore180)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore181)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore182)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore183)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore185)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore186)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore187)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore188)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore184)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore189)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore191)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore192)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore193)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore194)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore190)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore195)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore197)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore198)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore199)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore200)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore196)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore201)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore203)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore204)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore205)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore206)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore202)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore207)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore209)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore210)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore211)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore212)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore208)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore213)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore215)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore216)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore217)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore218)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore214)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore219)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore220)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore222)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore223)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore224)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore221)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore227)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore228)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore226)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore294)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore230)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore232)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore233)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore234)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore235)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore231)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore236)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore238)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore239)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore240)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore241)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore237)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore242)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore244)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore245)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore246)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore247)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore243)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore248)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore250)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore251)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore252)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore253)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore249)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore254)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore256)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore257)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore258)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore259)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore255)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore260)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore262)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore263)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore264)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore265)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore261)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore266)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore268)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore269)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore270)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore271)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore267)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore272)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore274)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore275)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore276)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore277)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore273)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore278)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore280)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore281)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore282)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore283)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore279)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore284)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore285)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore287)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore288)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore289)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore286)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore290)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore292)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore293)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore291)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox495)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox494)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox496)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox497)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox498)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox499)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox500)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore295)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore297)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore298)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore299)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore300)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore296)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore301)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore303)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore304)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore305)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore306)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore302)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore307)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore309)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore310)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore311)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore312)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore308)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore313)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore315)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore316)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore317)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore318)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore314)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore319)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore321)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore322)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore323)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore324)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore320)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore325)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore327)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore328)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore329)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore330)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore326)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore331)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore333)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore334)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore335)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore336)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore332)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore337)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore339)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore340)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore341)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore342)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore338)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore343)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore345)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore346)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore347)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore348)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore344)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore349)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox501)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox502)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox503)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox504)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox505)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox506)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox507)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox382)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox383)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore350)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore352)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore353)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore354)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore355)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore351)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore356)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore358)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore359)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore360)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore361)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore357)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore362)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore364)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore365)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore366)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore367)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore363)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore368)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore370)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore371)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore372)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore373)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore369)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore374)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore376)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore377)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore378)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore379)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore375)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore380)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore382)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore383)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore384)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore385)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore381)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore386)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore388)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore389)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore390)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore391)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore387)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore392)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore394)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore395)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore396)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore397)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore393)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore398)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore400)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore401)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore402)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore403)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore399)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore404)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore405)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore407)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore408)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore409)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore459)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore406)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore410)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore412)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore413)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore414)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore415)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore411)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore416)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore418)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore419)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore420)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore421)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore417)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore422)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore424)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore425)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore426)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore427)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore423)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore428)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore430)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore431)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore432)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore433)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore429)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore434)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore436)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore437)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore438)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore439)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore435)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore440)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore442)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore443)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore444)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore445)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore441)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore446)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore448)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore449)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore450)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore451)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore447)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore452)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore454)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore455)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore456)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore457)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore453)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore458)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox563)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox564)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox565)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox566)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox567)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox568)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox569)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore460)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore462)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore463)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore464)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore465)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore461)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore466)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore468)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore469)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore470)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore471)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore467)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore472)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore474)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore475)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore476)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore477)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore473)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore478)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore480)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore481)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore482)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore483)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore479)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore484)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore486)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore487)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore488)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore489)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore485)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore490)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore492)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore493)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore494)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore495)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore491)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore496)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore498)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore499)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore500)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore501)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore497)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore502)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore504)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore505)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore506)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore507)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore503)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore508)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore510)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore511)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore512)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore513)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore509)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore514)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox625)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox626)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox627)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox628)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox629)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox630)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox631)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox632)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore575)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore515)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore517)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore518)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore519)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore520)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore516)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore521)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore523)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore524)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore525)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore526)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore522)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore527)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore529)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore530)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore531)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore532)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore528)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore533)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore535)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore536)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore537)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore538)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore534)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore539)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore541)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore542)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore543)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore544)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore540)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore545)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore547)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore548)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore549)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore550)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore546)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore551)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore553)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore554)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore555)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox610)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore552)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox612)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox613)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox614)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore556)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore557)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox617)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore558)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore560)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore561)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore562)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore563)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore559)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore564)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore573)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore574)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore576)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore577)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore578)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore579)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore565)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore566)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore567)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore568)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore569)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore570)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore571)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtScore572)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader group01_H;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter group01_F;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader group02_H;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter group02_F;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore001;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore003;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore004;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore005;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore006;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore002;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader group03_H;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter group03_F;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle09;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore007;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore009;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore010;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore011;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore012;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore008;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore013;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore015;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore016;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore017;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore018;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore014;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore019;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore021;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore022;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore023;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore024;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore020;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore025;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore027;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore028;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore029;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore030;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore026;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore031;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore033;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore034;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore035;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore036;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore032;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore037;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore039;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore040;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore041;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore042;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore038;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore043;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore045;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore046;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore047;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore048;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore044;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore049;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore051;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore052;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore053;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore054;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore050;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore055;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore057;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore058;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore059;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore060;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore056;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore061;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore063;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore064;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore065;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore066;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore062;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore067;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore069;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore070;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore071;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore072;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore068;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore073;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore075;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore076;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore077;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore078;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore074;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore079;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore081;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore082;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore083;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore084;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore080;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore085;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore087;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore088;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore089;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore090;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore086;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore091;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore093;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore094;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore095;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore096;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore092;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore097;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore099;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore100;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore101;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore098;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore103;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore105;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore107;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore108;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore104;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader group04_H;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter group04_F;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader group05_H;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter group05_F;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader group06_H;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter group06_F;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader group07_H;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter group07_F;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader group08_H;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter group08_F;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore109;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore110;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore112;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore113;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore164;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore111;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore115;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore118;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore119;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore120;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore116;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore123;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore125;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore126;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore122;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore127;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore129;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore130;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore131;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore132;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore128;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore133;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore135;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore136;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore137;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore138;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore134;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore139;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore141;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore142;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore143;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore144;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore140;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore145;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore147;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore148;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore149;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore150;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore146;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore151;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore153;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore154;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore155;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore156;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore152;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore157;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore159;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore160;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore161;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore162;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore158;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore163;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox193;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox194;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox195;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox196;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox197;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox198;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox199;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore165;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore167;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore168;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore169;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore170;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore166;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore171;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore173;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore174;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore175;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore176;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore172;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore177;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore179;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore180;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore181;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore182;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore178;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore183;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore185;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore186;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore187;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore188;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore184;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore189;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore191;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore192;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore193;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore194;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore190;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore195;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore197;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore198;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore199;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore200;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore196;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore201;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore203;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore204;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore205;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore206;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore202;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore207;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore209;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore210;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore211;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore212;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore208;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore213;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore215;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore216;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore217;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore218;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore214;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore219;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore220;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore222;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore223;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore224;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore221;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore225;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore227;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore228;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore226;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore229;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox132;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox133;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line line43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore230;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore232;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore233;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore234;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore235;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore231;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore236;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore238;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore239;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore240;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore241;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore237;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore242;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore244;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore245;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore246;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore247;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore243;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore248;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore250;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore251;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore252;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore253;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore249;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore254;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore256;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore257;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore258;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore259;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore255;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore260;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore262;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore263;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore264;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore265;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore261;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore266;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore268;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore269;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore270;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore271;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore267;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore272;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore274;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore275;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore276;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore277;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore273;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore278;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore280;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore281;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore282;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore283;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore279;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore284;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore285;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore287;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore288;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore289;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore286;
        private GrapeCity.ActiveReports.SectionReportModel.Line line45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore290;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore292;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore293;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore294;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore291;
        private GrapeCity.ActiveReports.SectionReportModel.Line line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox494;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox495;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox496;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox497;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox498;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox499;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox500;
        private GrapeCity.ActiveReports.SectionReportModel.Line line56;
        private GrapeCity.ActiveReports.SectionReportModel.Line line57;
        private GrapeCity.ActiveReports.SectionReportModel.Line line58;
        private GrapeCity.ActiveReports.SectionReportModel.Line line59;
        private GrapeCity.ActiveReports.SectionReportModel.Line line60;
        private GrapeCity.ActiveReports.SectionReportModel.Line line61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore295;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore297;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore298;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore299;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore300;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore296;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore301;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore303;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore304;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore305;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore306;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore302;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore307;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore309;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore310;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore311;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore312;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore308;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore313;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore315;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore316;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore317;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore318;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore314;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore319;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore321;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore322;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore323;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore324;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore320;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore325;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore327;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore328;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore329;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore330;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore326;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore331;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore333;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore334;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore335;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore336;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore332;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore337;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore339;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore340;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore341;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore342;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore338;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore343;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore345;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore346;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore347;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore348;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore344;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore349;
        private GrapeCity.ActiveReports.SectionReportModel.Line line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line line49;
        private GrapeCity.ActiveReports.SectionReportModel.Line line50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox382;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox383;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore350;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore352;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore353;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore354;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore355;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore351;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore356;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore358;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore359;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore360;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore361;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore357;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore362;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore364;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore365;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore366;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore367;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore363;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore368;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore370;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore371;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore372;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore373;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore369;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore374;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore376;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore377;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore378;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore379;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore375;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore380;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore382;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore383;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore384;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore385;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore381;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore386;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore388;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore389;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore390;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore391;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore387;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore392;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore394;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore395;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore396;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore397;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore393;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore398;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore400;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore401;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore402;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore403;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore399;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore404;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore405;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore407;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore408;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore409;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore459;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore406;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore410;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore412;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore413;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore414;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore415;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore411;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore416;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore418;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore419;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore420;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore421;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore417;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore422;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore424;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore425;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore426;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore427;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore423;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore428;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore430;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore431;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore432;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore433;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore429;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore434;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore436;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore437;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore438;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore439;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore435;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore440;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore442;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore443;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore444;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore445;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore441;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore446;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore448;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore449;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore450;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore451;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore447;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore452;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore454;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore455;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore456;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore457;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore453;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore458;
        private GrapeCity.ActiveReports.SectionReportModel.Line line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line line52;
        private GrapeCity.ActiveReports.SectionReportModel.Line line53;
        private GrapeCity.ActiveReports.SectionReportModel.Line line54;
        private GrapeCity.ActiveReports.SectionReportModel.Line line55;
        private GrapeCity.ActiveReports.SectionReportModel.Line line62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox501;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox502;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox503;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox504;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox505;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox506;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox507;
        private GrapeCity.ActiveReports.SectionReportModel.Line line63;
        private GrapeCity.ActiveReports.SectionReportModel.Line line64;
        private GrapeCity.ActiveReports.SectionReportModel.Line line65;
        private GrapeCity.ActiveReports.SectionReportModel.Line line66;
        private GrapeCity.ActiveReports.SectionReportModel.Line line67;
        private GrapeCity.ActiveReports.SectionReportModel.Line line68;
        private GrapeCity.ActiveReports.SectionReportModel.Line line69;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox563;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox564;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox565;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox566;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox567;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox568;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox569;
        private GrapeCity.ActiveReports.SectionReportModel.Line line73;
        private GrapeCity.ActiveReports.SectionReportModel.Line line74;
        private GrapeCity.ActiveReports.SectionReportModel.Line line75;
        private GrapeCity.ActiveReports.SectionReportModel.Line line76;
        private GrapeCity.ActiveReports.SectionReportModel.Line line77;
        private GrapeCity.ActiveReports.SectionReportModel.Line line78;
        private GrapeCity.ActiveReports.SectionReportModel.Line line79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore460;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore462;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore463;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore464;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore465;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore461;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore466;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore468;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore469;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore470;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore471;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore467;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore472;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore474;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore475;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore476;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore477;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore473;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore478;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore480;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore481;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore482;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore483;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore479;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore484;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore486;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore487;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore488;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore489;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore485;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore490;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore492;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore493;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore494;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore495;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore491;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore496;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore498;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore499;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore500;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore501;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore497;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore502;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore504;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore505;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore506;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore507;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore503;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore508;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore510;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore511;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore512;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore513;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore509;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore514;
        private GrapeCity.ActiveReports.SectionReportModel.Line line70;
        private GrapeCity.ActiveReports.SectionReportModel.Line line71;
        private GrapeCity.ActiveReports.SectionReportModel.Line line72;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox625;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox626;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox627;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox628;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox629;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox630;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox631;
        private GrapeCity.ActiveReports.SectionReportModel.Line line83;
        private GrapeCity.ActiveReports.SectionReportModel.Line line84;
        private GrapeCity.ActiveReports.SectionReportModel.Line line85;
        private GrapeCity.ActiveReports.SectionReportModel.Line line86;
        private GrapeCity.ActiveReports.SectionReportModel.Line line87;
        private GrapeCity.ActiveReports.SectionReportModel.Line line88;
        private GrapeCity.ActiveReports.SectionReportModel.Line line89;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore515;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore517;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore518;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore519;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore520;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore516;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore521;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore523;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore524;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore525;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore526;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore522;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore527;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore529;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore530;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore531;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore532;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore528;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore533;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore535;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore536;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore537;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore538;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore534;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore539;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore541;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore542;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore543;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore544;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore540;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore545;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore547;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore548;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore549;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore550;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore546;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore551;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore553;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore554;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore555;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox610;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore552;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox612;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox613;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox614;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore556;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore557;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox617;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore558;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore560;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore561;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore562;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore563;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore559;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore564;
        private GrapeCity.ActiveReports.SectionReportModel.Line line82;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox632;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore573;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore574;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore575;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore576;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore577;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore578;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore579;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore565;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore566;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore567;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore568;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore569;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore570;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore571;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtScore572;
        private GrapeCity.ActiveReports.SectionReportModel.Line line81;
        private GrapeCity.ActiveReports.SectionReportModel.Line line80;
        private GrapeCity.ActiveReports.SectionReportModel.PageBreak pageBreak1;
        private GrapeCity.ActiveReports.SectionReportModel.PageBreak pageBreak2;
        private GrapeCity.ActiveReports.SectionReportModel.PageBreak pageBreak3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox groupId;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line97;
        private GrapeCity.ActiveReports.SectionReportModel.Line line96;
        private GrapeCity.ActiveReports.SectionReportModel.Line line95;
        private GrapeCity.ActiveReports.SectionReportModel.Line line94;
        private GrapeCity.ActiveReports.SectionReportModel.Line line93;
        private GrapeCity.ActiveReports.SectionReportModel.Line line92;
        private GrapeCity.ActiveReports.SectionReportModel.Line line91;
        private GrapeCity.ActiveReports.SectionReportModel.Line line90;
        private GrapeCity.ActiveReports.SectionReportModel.Line line98;
        private GrapeCity.ActiveReports.SectionReportModel.Line line99;
        private GrapeCity.ActiveReports.SectionReportModel.Line line100;
        private GrapeCity.ActiveReports.SectionReportModel.Line line101;
        private GrapeCity.ActiveReports.SectionReportModel.Line line102;
        private GrapeCity.ActiveReports.SectionReportModel.Line line103;
        private GrapeCity.ActiveReports.SectionReportModel.Line line104;
        private GrapeCity.ActiveReports.SectionReportModel.Line line105;
        private GrapeCity.ActiveReports.SectionReportModel.Line line106;
        private GrapeCity.ActiveReports.SectionReportModel.Line line107;
        private GrapeCity.ActiveReports.SectionReportModel.Line line108;
        private GrapeCity.ActiveReports.SectionReportModel.Line line109;
        private GrapeCity.ActiveReports.SectionReportModel.Line line110;
        private GrapeCity.ActiveReports.SectionReportModel.Line line111;
        private GrapeCity.ActiveReports.SectionReportModel.Line line112;
        private GrapeCity.ActiveReports.SectionReportModel.Line line113;
        private GrapeCity.ActiveReports.SectionReportModel.Line line114;
        private GrapeCity.ActiveReports.SectionReportModel.Line line115;
        private GrapeCity.ActiveReports.SectionReportModel.Line line116;
        private GrapeCity.ActiveReports.SectionReportModel.Line line117;
        private GrapeCity.ActiveReports.SectionReportModel.Line line118;
        private GrapeCity.ActiveReports.SectionReportModel.Line line119;
        private GrapeCity.ActiveReports.SectionReportModel.Line line120;
        private GrapeCity.ActiveReports.SectionReportModel.Line line121;
        private GrapeCity.ActiveReports.SectionReportModel.Line line122;
        private GrapeCity.ActiveReports.SectionReportModel.Line line123;
        private GrapeCity.ActiveReports.SectionReportModel.Line line124;
        private GrapeCity.ActiveReports.SectionReportModel.Line line125;
        private GrapeCity.ActiveReports.SectionReportModel.Line line126;
        private GrapeCity.ActiveReports.SectionReportModel.Line line127;
        private GrapeCity.ActiveReports.SectionReportModel.Line line128;
        private GrapeCity.ActiveReports.SectionReportModel.Line line129;
        private GrapeCity.ActiveReports.SectionReportModel.Line line156;
        private GrapeCity.ActiveReports.SectionReportModel.Line line157;
        private GrapeCity.ActiveReports.SectionReportModel.Line line158;
        private GrapeCity.ActiveReports.SectionReportModel.Line line159;
        private GrapeCity.ActiveReports.SectionReportModel.Line line161;
        private GrapeCity.ActiveReports.SectionReportModel.Line line162;
        private GrapeCity.ActiveReports.SectionReportModel.Line line163;
        private GrapeCity.ActiveReports.SectionReportModel.Line line164;
        private GrapeCity.ActiveReports.SectionReportModel.Line line165;
        private GrapeCity.ActiveReports.SectionReportModel.Line line166;
        private GrapeCity.ActiveReports.SectionReportModel.Line line167;
        private GrapeCity.ActiveReports.SectionReportModel.Line line168;
        private GrapeCity.ActiveReports.SectionReportModel.Line line169;
        private GrapeCity.ActiveReports.SectionReportModel.Line line170;
        private GrapeCity.ActiveReports.SectionReportModel.Line line154;
        private GrapeCity.ActiveReports.SectionReportModel.Line line130;
        private GrapeCity.ActiveReports.SectionReportModel.Line line131;
        private GrapeCity.ActiveReports.SectionReportModel.Line line132;
        private GrapeCity.ActiveReports.SectionReportModel.Line line133;
        private GrapeCity.ActiveReports.SectionReportModel.Line line134;
        private GrapeCity.ActiveReports.SectionReportModel.Line line135;
        private GrapeCity.ActiveReports.SectionReportModel.Line line136;
        private GrapeCity.ActiveReports.SectionReportModel.Line line137;
        private GrapeCity.ActiveReports.SectionReportModel.Line line138;
        private GrapeCity.ActiveReports.SectionReportModel.Line line139;
        private GrapeCity.ActiveReports.SectionReportModel.Line line140;
        private GrapeCity.ActiveReports.SectionReportModel.Line line141;
        private GrapeCity.ActiveReports.SectionReportModel.Line line142;
        private GrapeCity.ActiveReports.SectionReportModel.Line line143;
        private GrapeCity.ActiveReports.SectionReportModel.Line line144;
        private GrapeCity.ActiveReports.SectionReportModel.Line line145;
        private GrapeCity.ActiveReports.SectionReportModel.Line line146;
        private GrapeCity.ActiveReports.SectionReportModel.Line line147;
        private GrapeCity.ActiveReports.SectionReportModel.Line line148;
        private GrapeCity.ActiveReports.SectionReportModel.Line line149;
        private GrapeCity.ActiveReports.SectionReportModel.Line line150;
        private GrapeCity.ActiveReports.SectionReportModel.Line line151;
        private GrapeCity.ActiveReports.SectionReportModel.Line line152;
        private GrapeCity.ActiveReports.SectionReportModel.Line line153;
        private GrapeCity.ActiveReports.SectionReportModel.Line line155;
        private GrapeCity.ActiveReports.SectionReportModel.Line line160;
        private GrapeCity.ActiveReports.SectionReportModel.Line line171;
        private GrapeCity.ActiveReports.SectionReportModel.Line line172;
        private GrapeCity.ActiveReports.SectionReportModel.Line line173;
        private GrapeCity.ActiveReports.SectionReportModel.Line line175;
        private GrapeCity.ActiveReports.SectionReportModel.Line line176;
        private GrapeCity.ActiveReports.SectionReportModel.Line line177;
        private GrapeCity.ActiveReports.SectionReportModel.Line line178;
        private GrapeCity.ActiveReports.SectionReportModel.Line line179;
        private GrapeCity.ActiveReports.SectionReportModel.Line line180;
        private GrapeCity.ActiveReports.SectionReportModel.Line line181;
        private GrapeCity.ActiveReports.SectionReportModel.Line line182;
        private GrapeCity.ActiveReports.SectionReportModel.Line line183;
        private GrapeCity.ActiveReports.SectionReportModel.Line line184;
        private GrapeCity.ActiveReports.SectionReportModel.Line line185;
        private GrapeCity.ActiveReports.SectionReportModel.Line line186;
        private GrapeCity.ActiveReports.SectionReportModel.Line line187;
        private GrapeCity.ActiveReports.SectionReportModel.Line line188;
        private GrapeCity.ActiveReports.SectionReportModel.Line line189;
        private GrapeCity.ActiveReports.SectionReportModel.Line line190;
        private GrapeCity.ActiveReports.SectionReportModel.Line line191;
        private GrapeCity.ActiveReports.SectionReportModel.Line line192;
        private GrapeCity.ActiveReports.SectionReportModel.Line line193;
        private GrapeCity.ActiveReports.SectionReportModel.Line line194;
        private GrapeCity.ActiveReports.SectionReportModel.Line line195;
        private GrapeCity.ActiveReports.SectionReportModel.Line line196;
        private GrapeCity.ActiveReports.SectionReportModel.Line line197;
        private GrapeCity.ActiveReports.SectionReportModel.Line line198;
        private GrapeCity.ActiveReports.SectionReportModel.Line line199;
        private GrapeCity.ActiveReports.SectionReportModel.Line line200;
        private GrapeCity.ActiveReports.SectionReportModel.Line line201;
        private GrapeCity.ActiveReports.SectionReportModel.Line line202;
        private GrapeCity.ActiveReports.SectionReportModel.Line line203;
        private GrapeCity.ActiveReports.SectionReportModel.Line line204;
        private GrapeCity.ActiveReports.SectionReportModel.Line line205;
        private GrapeCity.ActiveReports.SectionReportModel.Line line206;
        private GrapeCity.ActiveReports.SectionReportModel.Line line207;
        private GrapeCity.ActiveReports.SectionReportModel.Line line208;
        private GrapeCity.ActiveReports.SectionReportModel.Line line209;
        private GrapeCity.ActiveReports.SectionReportModel.Line line210;
        private GrapeCity.ActiveReports.SectionReportModel.Line line211;
        private GrapeCity.ActiveReports.SectionReportModel.Line line212;
        private GrapeCity.ActiveReports.SectionReportModel.Line line213;
        private GrapeCity.ActiveReports.SectionReportModel.Line line214;
        private GrapeCity.ActiveReports.SectionReportModel.Line line215;
        private GrapeCity.ActiveReports.SectionReportModel.Line line216;
        private GrapeCity.ActiveReports.SectionReportModel.Line line217;
        private GrapeCity.ActiveReports.SectionReportModel.Line line218;
        private GrapeCity.ActiveReports.SectionReportModel.Line line219;
        private GrapeCity.ActiveReports.SectionReportModel.Line line220;
        private GrapeCity.ActiveReports.SectionReportModel.Line line221;
        private GrapeCity.ActiveReports.SectionReportModel.Line line222;
        private GrapeCity.ActiveReports.SectionReportModel.Line line223;
        private GrapeCity.ActiveReports.SectionReportModel.Line line224;
        private GrapeCity.ActiveReports.SectionReportModel.Line line225;
        private GrapeCity.ActiveReports.SectionReportModel.Line line226;
        private GrapeCity.ActiveReports.SectionReportModel.Line line227;
        private GrapeCity.ActiveReports.SectionReportModel.Line line228;
        private GrapeCity.ActiveReports.SectionReportModel.Line line229;
        private GrapeCity.ActiveReports.SectionReportModel.Line line230;
        private GrapeCity.ActiveReports.SectionReportModel.Line line231;
        private GrapeCity.ActiveReports.SectionReportModel.Line line232;
        private GrapeCity.ActiveReports.SectionReportModel.Line line233;
        private GrapeCity.ActiveReports.SectionReportModel.Line line234;
        private GrapeCity.ActiveReports.SectionReportModel.Line line235;
        private GrapeCity.ActiveReports.SectionReportModel.Line line236;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line174;
        private GrapeCity.ActiveReports.SectionReportModel.Line line237;
        private GrapeCity.ActiveReports.SectionReportModel.Line line238;
        private GrapeCity.ActiveReports.SectionReportModel.Line line239;
    }
}
