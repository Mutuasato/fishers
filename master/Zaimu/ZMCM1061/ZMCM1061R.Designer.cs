﻿namespace jp.co.fsi.zm.zmcm1061
{
    /// <summary>
    /// ZMCM1061R の概要の説明です。
    /// </summary>
    partial class ZMCM1061R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMCM1061R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday_tate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTitle03,
            this.txtTitle04,
            this.txtTitle02,
            this.txtTitle01,
            this.txtCompanyName,
            this.txtTitleName,
            this.txtToday_tate,
            this.lblPage,
            this.txtPageCount,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8});
            this.pageHeader.Height = 0.7890913F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.3149606F;
            this.txtTitle03.Left = 3.82441F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle03.Text = "取　引　区　分";
            this.txtTitle03.Top = 0.4740158F;
            this.txtTitle03.Width = 2.854331F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.3149606F;
            this.txtTitle04.Left = 6.684252F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle04.Text = "税率";
            this.txtTitle04.Top = 0.4740158F;
            this.txtTitle04.Width = 0.5992126F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.3149606F;
            this.txtTitle02.Left = 2.854331F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle02.Text = "課税区分";
            this.txtTitle02.Top = 0.4740158F;
            this.txtTitle02.Width = 0.9736221F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.3149606F;
            this.txtTitle01.Left = 0F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle01.Text = "税　区　分";
            this.txtTitle01.Top = 0.4740158F;
            this.txtTitle01.Width = 2.854331F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0.02401575F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 128";
            this.txtCompanyName.Text = null;
            this.txtCompanyName.Top = 0.2279528F;
            this.txtCompanyName.Width = 2.755118F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2043307F;
            this.txtTitleName.Left = 2.779134F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle";
            this.txtTitleName.Text = "税 区 分 表";
            this.txtTitleName.Top = 0F;
            this.txtTitleName.Width = 1.941733F;
            // 
            // txtToday_tate
            // 
            this.txtToday_tate.Height = 0.1968504F;
            this.txtToday_tate.Left = 5.6F;
            this.txtToday_tate.MultiLine = false;
            this.txtToday_tate.Name = "txtToday_tate";
            this.txtToday_tate.OutputFormat = resources.GetString("txtToday_tate.OutputFormat");
            this.txtToday_tate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtToday_tate.Text = "yyyy/MM/dd";
            this.txtToday_tate.Top = 0.007480316F;
            this.txtToday_tate.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.128348F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.007480316F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 6.812601F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.007480316F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.779134F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2279528F;
            this.line1.Width = 1.941732F;
            this.line1.X1 = 2.779134F;
            this.line1.X2 = 4.720866F;
            this.line1.Y1 = 0.2279528F;
            this.line1.Y2 = 0.2279528F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.4740158F;
            this.line2.Width = 7.287403F;
            this.line2.X1 = 0F;
            this.line2.X2 = 7.287403F;
            this.line2.Y1 = 0.4740158F;
            this.line2.Y2 = 0.4740158F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.7889764F;
            this.line3.Width = 7.287403F;
            this.line3.X1 = 0F;
            this.line3.X2 = 7.287403F;
            this.line3.Y1 = 0.7889764F;
            this.line3.Y2 = 0.7889764F;
            // 
            // line4
            // 
            this.line4.Height = 0.3149605F;
            this.line4.Left = 2.85433F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.4740158F;
            this.line4.Width = 9.536743E-07F;
            this.line4.X1 = 2.854331F;
            this.line4.X2 = 2.85433F;
            this.line4.Y1 = 0.4740158F;
            this.line4.Y2 = 0.7889763F;
            // 
            // line5
            // 
            this.line5.Height = 0.3149603F;
            this.line5.Left = 3.827952F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.4740158F;
            this.line5.Width = 1.192093E-06F;
            this.line5.X1 = 3.827953F;
            this.line5.X2 = 3.827952F;
            this.line5.Y1 = 0.4740158F;
            this.line5.Y2 = 0.7889761F;
            // 
            // line6
            // 
            this.line6.Height = 0.3149603F;
            this.line6.Left = 6.67874F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.4740158F;
            this.line6.Width = 9.536743E-07F;
            this.line6.X1 = 6.678741F;
            this.line6.X2 = 6.67874F;
            this.line6.Y1 = 0.4740158F;
            this.line6.Y2 = 0.7889761F;
            // 
            // line7
            // 
            this.line7.Height = 0.3149603F;
            this.line7.Left = 7.287402F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.4740158F;
            this.line7.Width = 0F;
            this.line7.X1 = 7.287402F;
            this.line7.X2 = 7.287402F;
            this.line7.Y1 = 0.4740158F;
            this.line7.Y2 = 0.7889761F;
            // 
            // line8
            // 
            this.line8.Height = 0.3149607F;
            this.line8.Left = 0F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.4740157F;
            this.line8.Width = 0F;
            this.line8.X1 = 0F;
            this.line8.X2 = 0F;
            this.line8.Y1 = 0.4740157F;
            this.line8.Y2 = 0.7889764F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue02,
            this.txtValue07,
            this.txtValue06,
            this.txtValue05,
            this.txtValue04,
            this.txtValue03,
            this.txtValue01,
            this.line9,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line17,
            this.line16,
            this.line11,
            this.line10});
            this.detail.Height = 0.2395833F;
            this.detail.Name = "detail";
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM03";
            this.txtValue02.Height = 0.2283465F;
            this.txtValue02.Left = 0.2740158F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal;" +
    " text-align: left; vertical-align: middle";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0F;
            this.txtValue02.Width = 2.56378F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM08";
            this.txtValue07.Height = 0.2283465F;
            this.txtValue07.Left = 6.722441F;
            this.txtValue07.MultiLine = false;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal;" +
    " text-align: right; vertical-align: middle";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0F;
            this.txtValue07.Width = 0.5468507F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM07";
            this.txtValue06.Height = 0.2283465F;
            this.txtValue06.Left = 4.078347F;
            this.txtValue06.MultiLine = false;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal;" +
    " text-align: left; vertical-align: middle";
            this.txtValue06.Text = null;
            this.txtValue06.Top = 0F;
            this.txtValue06.Width = 2.582283F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM06";
            this.txtValue05.Height = 0.2283465F;
            this.txtValue05.Left = 3.843701F;
            this.txtValue05.MultiLine = false;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal;" +
    " text-align: right; vertical-align: middle";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0F;
            this.txtValue05.Width = 0.1968504F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM05";
            this.txtValue04.Height = 0.2283465F;
            this.txtValue04.Left = 3.129134F;
            this.txtValue04.MultiLine = false;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal;" +
    " text-align: left; vertical-align: middle";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0F;
            this.txtValue04.Width = 0.6795273F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM04";
            this.txtValue03.Height = 0.2283465F;
            this.txtValue03.Left = 2.877953F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal;" +
    " text-align: right; vertical-align: middle";
            this.txtValue03.Text = null;
            this.txtValue03.Top = 0F;
            this.txtValue03.Width = 0.1968504F;
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM02";
            this.txtValue01.Height = 0.2283465F;
            this.txtValue01.Left = 0.02401575F;
            this.txtValue01.MultiLine = false;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal;" +
    " text-align: right; vertical-align: middle";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0F;
            this.txtValue01.Width = 0.1968504F;
            // 
            // line9
            // 
            this.line9.Height = 0.2362205F;
            this.line9.Left = 2.854331F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 2.854331F;
            this.line9.X2 = 2.854331F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.2362205F;
            // 
            // line12
            // 
            this.line12.Height = 0.2362205F;
            this.line12.Left = 3.827952F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 1.192093E-06F;
            this.line12.X1 = 3.827953F;
            this.line12.X2 = 3.827952F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.2362205F;
            // 
            // line13
            // 
            this.line13.Height = 0.2362205F;
            this.line13.Left = 4.061417F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 9.536743E-07F;
            this.line13.X1 = 4.061418F;
            this.line13.X2 = 4.061417F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.2362205F;
            // 
            // line14
            // 
            this.line14.Height = 0.2362205F;
            this.line14.Left = 6.678741F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 6.678741F;
            this.line14.X2 = 6.678741F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.2362205F;
            // 
            // line15
            // 
            this.line15.Height = 0.2362205F;
            this.line15.Left = 0F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 1.430511E-06F;
            this.line15.X1 = 1.430511E-06F;
            this.line15.X2 = 0F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.2362205F;
            // 
            // line17
            // 
            this.line17.Height = 0.2362205F;
            this.line17.Left = 3.090943F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0F;
            this.line17.Width = 1.907349E-06F;
            this.line17.X1 = 3.090945F;
            this.line17.X2 = 3.090943F;
            this.line17.Y1 = 0F;
            this.line17.Y2 = 0.2362205F;
            // 
            // line16
            // 
            this.line16.Height = 0.2362205F;
            this.line16.Left = 0.2287396F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 5.960464E-07F;
            this.line16.X1 = 0.2287402F;
            this.line16.X2 = 0.2287396F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.2362205F;
            // 
            // line11
            // 
            this.line11.Height = 0.2362205F;
            this.line11.Left = 7.287401F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 9.536743E-07F;
            this.line11.X1 = 7.287402F;
            this.line11.X2 = 7.287401F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.2362205F;
            // 
            // line10
            // 
            this.line10.Height = 0F;
            this.line10.Left = 0F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0.2362205F;
            this.line10.Width = 7.287403F;
            this.line10.X1 = 0F;
            this.line10.X2 = 7.287403F;
            this.line10.Y1 = 0.2362205F;
            this.line10.Y2 = 0.2362205F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // ZMCM1061R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.1968504F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.287403F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday_tate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
    }
}
