﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.zm.zmcm1061
{

    /// <summary>
    /// 税区分の検索(ZMCM1065)
    /// </summary>
    public partial class ZMCM1065 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1065()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Visible = true;
            this.btnEnter.Visible = true;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnEnter.Location = this.btnF2.Location;

            // データ取得してグリッドに設定
            this.SearchData();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを取得する
        /// </summary>
        private void SearchData()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            // 税区分ビューからデータを取得して表示
            Sql.AppendLine("SELECT");
            Sql.AppendLine(" 0 AS KBN,");
            Sql.AppendLine(" A.ZEI_KUBUN,");
            Sql.AppendLine(" A.ZEI_KUBUN_NM,");
            Sql.AppendLine(" A.ZEI_RITSU,");
            Sql.AppendLine(" A.KAZEI_KUBUN,");
            Sql.AppendLine(" A.TORIHIKI_KUBUN ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" TB_ZM_F_ZEI_KUBUN AS A ");
            Sql.AppendLine("UNION ALL ");
            Sql.AppendLine("SELECT");
            Sql.AppendLine(" 1 AS KBN,");
            Sql.AppendLine(" A.ZEI_KUBUN,");
            Sql.AppendLine(" B.ZEI_KUBUN_NM,");
            Sql.AppendLine(" A.SHIN_ZEI_RITSU AS ZEI_RITSU,");
            Sql.AppendLine(" B.KAZEI_KUBUN,");
            Sql.AppendLine(" B.TORIHIKI_KUBUN ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" TB_ZM_F_ZEI_KBN_SHIN_ZEI_RT AS A ");
            Sql.AppendLine("LEFT JOIN TB_ZM_F_ZEI_KUBUN AS B ");
            Sql.AppendLine("  ON A.ZEI_KUBUN = B.ZEI_KUBUN ");
            Sql.AppendLine("ORDER BY 2, 1");
            DataTable dtMailLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 該当データがなければエラーメッセージを表示
            if (dtMailLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
            }

            this.dgvList.DataSource = EditDataList(dtMailLoop);

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            this.dgvList.Columns[0].Width = 30;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 280;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[2].Width = 60;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[3].Visible = false;
            this.dgvList.Columns[4].Visible = false;
            this.dgvList.Columns[5].Visible = false;
            this.dgvList.Columns[6].Visible = false;

            int currentRow = -1;
            string targetCode = string.Empty;
            if (this.InData != null && !ValChk.IsEmpty(this.InData))
            {
                targetCode = (string)this.InData;
                if (targetCode.Length != 0)
                {
                    currentRow = dtMailLoop.Rows.IndexOf(
                        dtMailLoop.AsEnumerable().Where(a => a.Field<decimal>(1) == Util.ToDecimal(targetCode)).FirstOrDefault());
                }
            }
            if (currentRow != -1)
            {
                this.dgvList.FirstDisplayedScrollingRowIndex = currentRow;
                this.dgvList.Rows[currentRow].Selected = true;
                this.dgvList.CurrentCell = this.dgvList[0, currentRow];
            }
        }

        private DataTable EditDataList(DataTable dtData)
        {
            DataTable dtResult = new DataTable();
            DataRow drResult;
            dtResult.Columns.Add("区", typeof(int));
            dtResult.Columns.Add("税区分名称", typeof(string));
            dtResult.Columns.Add("税率", typeof(string));
            dtResult.Columns.Add("課税区分", typeof(string));
            dtResult.Columns.Add("取引区分", typeof(string));
            dtResult.Columns.Add("税区分非", typeof(int));
            dtResult.Columns.Add("税区分名称非", typeof(string));

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();
                if (Util.ToInt(dtData.Rows[i]["KBN"]) == 0)
                {
                    drResult["区"] = dtData.Rows[i]["ZEI_KUBUN"];
                    drResult["税区分名称"] = dtData.Rows[i]["ZEI_KUBUN_NM"];
                }
                drResult["税率"] = dtData.Rows[i]["ZEI_RITSU"];
                drResult["課税区分"] = dtData.Rows[i]["KAZEI_KUBUN"];
                drResult["取引区分"] = dtData.Rows[i]["TORIHIKI_KUBUN"];

                drResult["税区分非"] = dtData.Rows[i]["ZEI_KUBUN"];
                drResult["税区分名称非"] = dtData.Rows[i]["ZEI_KUBUN_NM"];

                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[5] {
                Util.ToString(this.dgvList.SelectedRows[0].Cells["税区分非"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["税区分名称非"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["税率"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["課税区分"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["取引区分"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
