﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Collections.Generic;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmdr1011
{
    /// <summary>
    /// 日計表(ZMDR1011)
    /// </summary>
    public partial class ZMDR1011 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblGengoFr.Text = jpDate[0];
            txtYearFr.Text = jpDate[2];
            txtMonthFr.Text = jpDate[3];
            txtDayFr.Text = jpDate[4];

            lblGengoTo.Text = jpDate[0];
            txtYearTo.Text = jpDate[2];
            txtMonthTo.Text = jpDate[3];
            txtDayTo.Text = jpDate[4];

            //rdoTujoShiwake.Checked = true;
            try
            {
                string val = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1011", "Setting", "ShiwakeKbn"));
                switch (val)
                {
                    case "0":
                        rdoTujoShiwake.Checked = true;
                        break;
                    case "1":
                        rdoKessanShiwake.Checked = true;
                        break;
                    case "2":
                        rdoZenShiwake.Checked = true;
                        break;
                    default:
                        rdoTujoShiwake.Checked = true;
                        break;
                }
            }
            catch (Exception)
            {
                rdoTujoShiwake.Checked = true;
            }

            rdoZeikomi.Checked = true;

            // 初期フォーカス
            txtYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付,船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYearFr":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

			switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtYearFr":
                    #region 元号検索
                    // アセンブリのロード
                    //System.Reflection.Assembly asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //Type t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpFr();                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "ZMDR10111R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtYearFr.SelectAll();
            }
            else
            {
                this.txtYearFr.Text = Util.ToString(IsValid.SetYear(this.txtYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthFr.SelectAll();
            }
            else
            {
                this.txtMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayFr.SelectAll();
            }
            else
            {
                this.txtDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDayFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtYearTo.SelectAll();
            }
            else
            {
                this.txtYearTo.Text = Util.ToString(IsValid.SetYear(this.txtYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthTo.SelectAll();
            }
            else
            {
                this.txtMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayTo.SelectAll();
            }
            else
            {
                this.txtDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDayTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }


        /// <summary>
        /// 消費税処理Enter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoZeikomi_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.rdoZeikomi.Focus();
                }
            }
        }

        /// <summary>
        /// 消費税処理Enter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoZeinuki_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.rdoZeinuki.Focus();
                }
            }
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayFr.Text) > lastDayInMonth)
            {
                this.txtDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoFr.Text,
                                  this.txtYearFr.Text,
                                  this.txtMonthFr.Text,
                                  this.txtDayFr.Text, this.Dba)), this.Dba));
            SetJpTo(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoTo.Text,
                                  this.txtYearTo.Text,
                                  this.txtMonthTo.Text,
                                  this.txtDayTo.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayTo.Text) > lastDayInMonth)
            {
                this.txtDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoTo.Text,
                                  this.txtYearTo.Text,
                                  this.txtMonthTo.Text,
                                  this.txtDayTo.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                this.txtYearFr.Focus();
                this.txtYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                this.txtMonthFr.Focus();
                this.txtMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                this.txtDayFr.Focus();
                this.txtDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                this.txtYearTo.Focus();
                this.txtYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                this.txtMonthTo.Focus();
                this.txtMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                this.txtDayTo.Focus();
                this.txtDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMDR10111R rpt = new ZMDR10111R(dtOutput);

                    // 捺印欄名
                    List<string> nmList = new List<string>() { "組合長", "課長", "係長", "照査", "起票" };
                    try
                    {
                        string[] nml;
                        nml = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1011", "Setting", "nameList")).Split(',');
                        nmList = new List<string>(nml);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message); ;
                    }
                    rpt.nmList = nmList;

                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            // 日付を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                    this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                    this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);
            DateTime nowDate = System.DateTime.Now;

            // 日付を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpnowDate = Util.ConvJpDate(nowDate, this.Dba);

            // 税区分設定
            string zeiKubun;
            string zeiKubunNm;
            if (this.rdoZeikomi.Checked == true)
            {
                zeiKubun = "ZEIKOMI";
                zeiKubunNm = "【" + this.rdoZeikomi.Text + "】";
            }
            else
            {
                zeiKubun = "ZEINUKI";
                zeiKubunNm = "【" + this.rdoZeinuki.Text + "】";
            }

            int i = 1; // ループ用カウント変数
            string kamokuNm;
            string[] shukeiCd = new string[7] { "A", "B", "C", "D", "E", "F", "FS" };
            int kanjoKamokuCD = 0; // 勘定科目コード
            // 決算区分
            int kessanKbn = 2;
            if (this.rdoTujoShiwake.Checked == true)
                kessanKbn = 0;
            else if (this.rdoKessanShiwake.Checked == true)
                kessanKbn = 1;
            // 支所コード
            int shishoCd = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));

            // 補助科目内訳表示を行う科目コードの取得（設定から取得）
            // 本来は勘定科目マスタの集計方法の値を設定しない物全てだが名護用が勘定科目を直接設定しているので
            // 設定がある場合は設定を優先、設定が無い場合はマスタ設定方式で動作する様に
            string[] dal;
            List<string> daList = new List<string>();
            try
            {
                dal = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1011", "Setting", "detailAccountList")).Split(',');
                daList = new List<string>(dal);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Print(ex.Message);
            }

            // 検索日付に発生しているデータを取得
            for (int l = 0; l < shukeiCd.Length; l++)
            {
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append(" SELECT");
                Sql.Append("     KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD,");
                Sql.Append("     KANJO_KAMOKU_NM     AS KANJO_KAMOKU_NM,");
                Sql.Append("     HOJO_SHIYO_KUBUN   AS HOJO_SHIYO_KUBUN,");
                Sql.Append("     TAISHAKU_KUBUN       AS TAISHAKU_KUBUN,");
                Sql.Append("     SHUKEI_KEISAN_KUBUN   AS SHUKEI_KEISAN_KUBUN,");
                Sql.Append("     KAMOKU_BUNRUI_CD AS KAMOKU_BUNRUI_CD,");
                Sql.Append("     CASE WHEN SUBSTRING(SHUKEI_CD, 3, 1) IS NULL THEN ' ' ELSE SUBSTRING(SHUKEI_CD, 3, 1) END AS SHUKEI_CD");
                Sql.Append(" FROM");
                Sql.Append("     TB_ZM_KANJO_KAMOKU ");
                Sql.Append(" WHERE");
                Sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO AND");
                Sql.Append("     KAISHA_CD =  @KAISHA_CD AND");
                Sql.Append("     (SUBSTRING(SHUKEI_CD, 1, 1) = '" + shukeiCd[l] + "' AND  SUBSTRING(SHUKEI_CD, 2, 1) != 'S')");
                Sql.Append(" ORDER BY");
                Sql.Append("     KANJO_KAMOKU_CD");

                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

                DataTable dtKamokuCd = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                foreach (DataRow dr in dtKamokuCd.Rows)
                {
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    // 集計コードによってSQL文変更
                    if (Util.ToInt(dr["SHUKEI_CD"]) == 1)
                    {
                        Sql.Append(" SELECT");
                        Sql.Append("     A.SHISHO_CD,");
                        Sql.Append("     A.KANJO_KAMOKU_CD AS KAMOKU_CD,");
                        Sql.Append("     B.KANJO_KAMOKU_NM     AS KAMOKU_NM,");
                        Sql.Append("     B.TAISHAKU_KUBUN       AS TAISHAKU_KUBUN,");
                        Sql.Append("     0                AS HOJO_KAMOKU_CD,");
                        Sql.Append("     B.SHUKEI_KEISAN_KUBUN   AS SHUKEI_KEISAN_KUBUN,");
                        Sql.Append("     ''               AS HOJO_KAMOKU_NM,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DENPYO_DATE_FR AS DATETIME) AND A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEIKOMI_KINGAKU ELSE 0 END     ELSE 0 END)  AS TOJITSU_ZEIKOMI_KARIKATA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DENPYO_DATE_FR AS DATETIME) AND A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEIKOMI_KINGAKU ELSE 0 END     ELSE 0 END)  AS TOJITSU_ZEIKOMI_KASHIKATA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DENPYO_DATE_FR AS DATETIME) AND A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEINUKI_KINGAKU ELSE 0 END     ELSE 0 END)  AS TOJITSU_ZEINUKI_KARIKATA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DENPYO_DATE_FR AS DATETIME) AND A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEINUKI_KINGAKU ELSE 0 END     ELSE 0 END)  AS TOJITSU_ZEINUKI_KASHIKATA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE < CAST(@DENPYO_DATE_TO AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN A.ZEIKOMI_KINGAKU ELSE A.ZEIKOMI_KINGAKU * -1 END     ELSE 0 END)  AS ZEIKOMI_ZENJITSU_ZANDAKA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE < CAST(@DENPYO_DATE_TO AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN A.ZEINUKI_KINGAKU ELSE A.ZEINUKI_KINGAKU * -1 END     ELSE 0 END)  AS ZEINUKI_ZENJITSU_ZANDAKA");
                        Sql.Append(" FROM");
                        Sql.Append("     TB_ZM_SHIWAKE_MEISAI AS A LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ON (A.KAISHA_CD = B.KAISHA_CD) AND (A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");
                        Sql.Append(" WHERE");
                        Sql.Append("  	 A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                        Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");

                        if (shishoCd != 0)
                            Sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");

                        Sql.Append("     A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) AND");
                        Sql.Append("     A.MEISAI_KUBUN <= 0 AND");

                        if (kessanKbn < 2)
                            Sql.Append("     A.KESSAN_KUBUN = @KESSAN_KUBUN AND");

                        Sql.Append("     A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
                        Sql.Append(" GROUP BY");
                        Sql.Append("     A.SHISHO_CD,");
                        Sql.Append("     A.KANJO_KAMOKU_CD,");
                        Sql.Append("     B.KANJO_KAMOKU_NM,");
                        Sql.Append("     B.TAISHAKU_KUBUN,");
                        Sql.Append("     B.SHUKEI_KEISAN_KUBUN");
                    }
                    else
                    {
                        // 勘定科目コードをintにして後の判定に使う
                        kanjoKamokuCD = Util.ToInt(dr["KANJO_KAMOKU_CD"]);

                        Sql.Append(" SELECT");
                        Sql.Append("     A.SHISHO_CD,");
                        Sql.Append("     A.KANJO_KAMOKU_CD AS KAMOKU_CD,");
                        Sql.Append("     B.KANJO_KAMOKU_NM     AS KAMOKU_NM,");

                        // 勘定科目が普通預金(コード=111)の場合だけ補助科目コードを出力する。その他は集計して1行にする
                        //if (kanjoKamokuCD == 111)
                        if (daList.Count != 0)
                        {
                            if (daList.Contains(kanjoKamokuCD.ToString()))
                            {
                                Sql.Append("     A.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD,");
                            }
                            else
                            {
                                Sql.Append("     0 AS HOJO_KAMOKU_CD,");
                            }
                        }
                        else
                        {
                            Sql.Append("     A.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD,");
                        }

                        Sql.Append("     B.TAISHAKU_KUBUN       AS TAISHAKU_KUBUN,");

                        //// 勘定科目が普通預金(コード=111)の場合だけ補助科目コードを出力する。その他は集計して1行にする
                        //if (kanjoKamokuCD == 111)
                        //{
                        //    Sql.Append("     A.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD,");
                        //}
                        //else
                        //{
                        //    Sql.Append("     0 AS HOJO_KAMOKU_CD,");
                        //}

                        Sql.Append("     B.SHUKEI_KEISAN_KUBUN   AS SHUKEI_KEISAN_KUBUN,");

                        // 勘定科目が普通預金(コード=111)の場合だけ補助科目名を出力する。その他は集計して1行にする
                        //if (kanjoKamokuCD == 111)
                        if (daList.Count != 0)
                        {
                            if (daList.Contains(kanjoKamokuCD.ToString()))
                            {
                                Sql.Append("     MAX(CASE WHEN B.HOJO_SHIYO_KUBUN = 1 THEN C.HOJO_KAMOKU_NM ELSE D.TORIHIKISAKI_NM END) AS HOJO_KAMOKU_NM,");
                            }
                            else
                            {
                                Sql.Append("     '' AS HOJO_KAMOKU_NM,");
                            }
                        }
                        else
                        {
                            Sql.Append("     MAX(CASE WHEN B.HOJO_SHIYO_KUBUN = 1 THEN C.HOJO_KAMOKU_NM ELSE D.TORIHIKISAKI_NM END) AS HOJO_KAMOKU_NM,");
                        }

                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DENPYO_DATE_FR AS DATETIME) AND A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) THEN CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEIKOMI_KINGAKU ELSE 0 END ELSE 0 END)  AS TOJITSU_ZEIKOMI_KARIKATA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DENPYO_DATE_FR AS DATETIME) AND A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) THEN CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEIKOMI_KINGAKU ELSE 0 END ELSE 0 END)  AS TOJITSU_ZEIKOMI_KASHIKATA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DENPYO_DATE_FR AS DATETIME) AND A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) THEN CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEINUKI_KINGAKU ELSE 0 END ELSE 0 END)  AS TOJITSU_ZEINUKI_KARIKATA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DENPYO_DATE_FR AS DATETIME) AND A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) THEN CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEINUKI_KINGAKU ELSE 0 END ELSE 0 END)  AS TOJITSU_ZEINUKI_KASHIKATA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE < CAST(@DENPYO_DATE_TO AS DATETIME) THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN A.ZEIKOMI_KINGAKU ELSE A.ZEIKOMI_KINGAKU * -1 END ELSE 0 END)  AS ZEIKOMI_ZENJITSU_ZANDAKA,");
                        Sql.Append("     SUM(CASE WHEN A.DENPYO_DATE < CAST(@DENPYO_DATE_TO AS DATETIME) THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN A.ZEINUKI_KINGAKU ELSE A.ZEINUKI_KINGAKU * -1 END ELSE 0 END)  AS ZEINUKI_ZENJITSU_ZANDAKA");
                        Sql.Append(" FROM");
                        Sql.Append("     TB_ZM_SHIWAKE_MEISAI AS A ");
                        Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ");
                        Sql.Append("     ON (A.KAISHA_CD = B.KAISHA_CD) AND (A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");
                        Sql.Append(" LEFT OUTER JOIN TB_ZM_HOJO_KAMOKU AS C ");
                        Sql.Append("     ON (A.KAISHA_CD = C.KAISHA_CD) AND (A.SHISHO_CD = C.SHISHO_CD) AND (A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD) AND (A.HOJO_KAMOKU_CD = C.HOJO_KAMOKU_CD) AND (A.KAIKEI_NENDO = C.KAIKEI_NENDO)");
                        Sql.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS D ");
                        Sql.Append("     ON (A.KAISHA_CD = D.KAISHA_CD) AND (A.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD)");
                        Sql.Append(" WHERE");
                        Sql.Append("     A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                        Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");

                        if (shishoCd != 0)
                            Sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");

                        Sql.Append("     A.DENPYO_DATE <= CAST(@DENPYO_DATE_TO AS DATETIME) AND");
                        Sql.Append("     A.MEISAI_KUBUN <= 0 AND");

                        if (kessanKbn < 2)
                            Sql.Append("     A.KESSAN_KUBUN = @KESSAN_KUBUN AND");

                        Sql.Append("     A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
                        Sql.Append(" GROUP BY");
                        Sql.Append("     A.SHISHO_CD,");
                        Sql.Append("     A.KANJO_KAMOKU_CD,");
                        Sql.Append("     B.KANJO_KAMOKU_NM,"); // TODO:不要かもしれない(勘定科目ｺｰﾄﾞがある為)

                        // 勘定科目が普通預金(コード=111)の場合だけ補助科目を出力する。その他は集計して1行にする
                        //if (kanjoKamokuCD == 111)
                        if (daList.Count != 0)
                        {
                            if (daList.Contains(kanjoKamokuCD.ToString()))
                            {
                                Sql.Append("     A.HOJO_KAMOKU_CD,"); // TODO:勘定科目ｺｰﾄﾞの次に記述すべきかと
                            }
                        }
                        else
                        {
                            Sql.Append("     A.HOJO_KAMOKU_CD,");
                        }

                        Sql.Append("     B.TAISHAKU_KUBUN,");

                        //// 勘定科目が普通預金(コード=111)の場合だけ補助科目を出力する。その他は集計して1行にする
                        //if (kanjoKamokuCD == 111)
                        //{
                        //    Sql.Append("     A.HOJO_KAMOKU_CD,"); // TODO:勘定科目ｺｰﾄﾞの次に記述すべきかと
                        //}

                        Sql.Append("     B.SHUKEI_KEISAN_KUBUN");
                        Sql.Append(" ORDER BY");
                        Sql.Append("     A.SHISHO_CD,");

                        // 勘定科目が普通預金(コード=111)の場合だけ補助科目を出力する。その他は集計して1行にする
                        //if (kanjoKamokuCD == 111)
                        if (daList.Count != 0)
                        {
                            if (daList.Contains(kanjoKamokuCD.ToString()))
                            {
                                Sql.Append("     A.KANJO_KAMOKU_CD,");
                                Sql.Append("     A.HOJO_KAMOKU_CD ");
                            }
                            else
                            {
                                Sql.Append("     A.KANJO_KAMOKU_CD "); // 最後なのでカンマ無し
                            }
                        }
                        else
                        {
                            Sql.Append("     A.KANJO_KAMOKU_CD,");
                            Sql.Append("     A.HOJO_KAMOKU_CD ");
                        }
                    }

                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                    // 検索する日付をセット
                    dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));

                    dpc.SetParam("KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, dr["KANJO_KAMOKU_CD"]);

                    if (kessanKbn < 2)
                    {
                        dpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 4, kessanKbn);
                    }

                    DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                    if (dtMainLoop.Rows.Count > 0)
                    {
                        #region 印刷ワークテーブルに登録
                        foreach (DataRow dr2 in dtMainLoop.Rows)
                        {
                            if (Util.ToDecimal(dr2["TOJITSU_" + zeiKubun + "_KARIKATA"]) != 0 || Util.ToDecimal(dr2["TOJITSU_" + zeiKubun + "_KASHIKATA"]) != 0)
                            {
                                Sql = new StringBuilder();
                                dpc = new DbParamCollection();
                                Sql.Append("INSERT INTO PR_ZM_TBL(");
                                Sql.Append("  GUID");
                                Sql.Append(" ,SORT");
                                Sql.Append(" ,ITEM01");
                                Sql.Append(" ,ITEM02");
                                Sql.Append(" ,ITEM03");
                                Sql.Append(" ,ITEM04");
                                Sql.Append(" ,ITEM05");
                                Sql.Append(" ,ITEM06");
                                Sql.Append(" ,ITEM07");
                                Sql.Append(" ,ITEM08");
                                Sql.Append(" ,ITEM09");
                                Sql.Append(" ,ITEM10");
                                Sql.Append(" ,ITEM11");
                                Sql.Append(" ,ITEM12");
                                Sql.Append(" ,ITEM13");
                                Sql.Append(") ");
                                Sql.Append("VALUES(");
                                Sql.Append("  @GUID");
                                Sql.Append(" ,@SORT");
                                Sql.Append(" ,@ITEM01");
                                Sql.Append(" ,@ITEM02");
                                Sql.Append(" ,@ITEM03");
                                Sql.Append(" ,@ITEM04");
                                Sql.Append(" ,@ITEM05");
                                Sql.Append(" ,@ITEM06");
                                Sql.Append(" ,@ITEM07");
                                Sql.Append(" ,@ITEM08");
                                Sql.Append(" ,@ITEM09");
                                Sql.Append(" ,@ITEM10");
                                Sql.Append(" ,@ITEM11");
                                Sql.Append(" ,@ITEM12");
                                Sql.Append(" ,@ITEM13");
                                Sql.Append(") ");

                                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                                int sortId = (Util.ToInt(Util.ToString(dr2["SHISHO_CD"])) * 1000000) + i;
                                dpc.SetParam("@SORT", SqlDbType.Decimal, 9, sortId);
                                
                                // ページヘッダーデータを設定
                                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                                if (tmpjpDateFr[5] == tmpjpDateTo[5])
                                {
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                                }
                                else
                                {
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                                }
                                // データを設定
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr2["KAMOKU_CD"]);
                                // 補助科目名があればセット
                                if (!ValChk.IsEmpty(Util.ToString(dr2["HOJO_KAMOKU_NM"])))
                                {
                                    kamokuNm = Util.ToString(dr2["KAMOKU_NM"]) + "(" + Util.ToString(dr2["HOJO_KAMOKU_NM"]) + ")";
                                }
                                else
                                {
                                    kamokuNm = Util.ToString(dr2["KAMOKU_NM"]);
                                }
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, kamokuNm);
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(dr2[zeiKubun + "_ZENJITSU_ZANDAKA"]));
                                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dr2["TOJITSU_" + zeiKubun + "_KARIKATA"]));
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dr2["TOJITSU_" + zeiKubun + "_KASHIKATA"]));
                                // 繰越残高の計算
                                Decimal kurikoshiZan = 0;
                                if (Util.ToInt(dr2["TAISHAKU_KUBUN"]) == 1)
                                {
                                    kurikoshiZan = Util.ToDecimal(dr2[zeiKubun + "_ZENJITSU_ZANDAKA"])
                                                         + Util.ToDecimal(dr2["TOJITSU_" + zeiKubun + "_KARIKATA"])
                                                         - Util.ToDecimal(dr2["TOJITSU_" + zeiKubun + "_KASHIKATA"]);
                                }
                                else
                                    if (Util.ToInt(dr2["TAISHAKU_KUBUN"]) == 2)
                                    {
                                        kurikoshiZan = Util.ToDecimal(dr2[zeiKubun + "_ZENJITSU_ZANDAKA"])
                                                             - Util.ToDecimal(dr2["TOJITSU_" + zeiKubun + "_KARIKATA"])
                                                             + Util.ToDecimal(dr2["TOJITSU_" + zeiKubun + "_KASHIKATA"]);
                                    }
                                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(kurikoshiZan));
                                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, tmpnowDate[5]);

                                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.ToString(dr2["SHISHO_CD"]));

                                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, zeiKubunNm);

                                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                i++;
                            }
                        }
                        #endregion
                    }
                }
            }
            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_ZM_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_ZM_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }

        /// <summary>
        /// 会計年度内日付に変換
        /// </summary>
        private DateTime FixNendoDate(DateTime date)
        {
            DateTime dateFr = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            DateTime dateTo = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }
        #endregion
    }
}
