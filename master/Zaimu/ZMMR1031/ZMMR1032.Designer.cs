﻿namespace jp.co.fsi.zm.zmmr1031
{
    partial class ZMMR1032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblKaishaSettei = new System.Windows.Forms.Label();
			this.lblJp = new System.Windows.Forms.Label();
			this.lblZei = new System.Windows.Forms.Label();
			this.lblGridViewTitle = new System.Windows.Forms.Label();
			this.mtbList = new System.Windows.Forms.DataGridView();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Zandaka = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KarikataHassei = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KashikataHassei = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TouZan = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.startKmkCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.endKmkCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.listKmkCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mtbList)).BeginInit();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 652);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1051, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1041, 31);
			this.lblTitle.Text = "";
			// 
			// lblKaishaSettei
			// 
			this.lblKaishaSettei.BackColor = System.Drawing.Color.Silver;
			this.lblKaishaSettei.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKaishaSettei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKaishaSettei.ForeColor = System.Drawing.Color.Black;
			this.lblKaishaSettei.Location = new System.Drawing.Point(0, 0);
			this.lblKaishaSettei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKaishaSettei.Name = "lblKaishaSettei";
			this.lblKaishaSettei.Size = new System.Drawing.Size(188, 38);
			this.lblKaishaSettei.TabIndex = 0;
			this.lblKaishaSettei.Tag = "CHANGE";
			this.lblKaishaSettei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJp
			// 
			this.lblJp.BackColor = System.Drawing.Color.Silver;
			this.lblJp.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJp.ForeColor = System.Drawing.Color.Black;
			this.lblJp.Location = new System.Drawing.Point(0, 0);
			this.lblJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJp.Name = "lblJp";
			this.lblJp.Size = new System.Drawing.Size(404, 38);
			this.lblJp.TabIndex = 4;
			this.lblJp.Tag = "CHANGE";
			this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblZei
			// 
			this.lblZei.BackColor = System.Drawing.Color.Silver;
			this.lblZei.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblZei.ForeColor = System.Drawing.Color.Black;
			this.lblZei.Location = new System.Drawing.Point(0, 0);
			this.lblZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZei.Name = "lblZei";
			this.lblZei.Size = new System.Drawing.Size(104, 38);
			this.lblZei.TabIndex = 5;
			this.lblZei.Tag = "CHANGE";
			this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblGridViewTitle
			// 
			this.lblGridViewTitle.BackColor = System.Drawing.Color.Silver;
			this.lblGridViewTitle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblGridViewTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGridViewTitle.ForeColor = System.Drawing.Color.Black;
			this.lblGridViewTitle.Location = new System.Drawing.Point(0, 0);
			this.lblGridViewTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGridViewTitle.Name = "lblGridViewTitle";
			this.lblGridViewTitle.Size = new System.Drawing.Size(327, 38);
			this.lblGridViewTitle.TabIndex = 3;
			this.lblGridViewTitle.Tag = "CHANGE";
			this.lblGridViewTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mtbList
			// 
			this.mtbList.AllowUserToAddRows = false;
			this.mtbList.AllowUserToDeleteRows = false;
			this.mtbList.AllowUserToResizeColumns = false;
			this.mtbList.AllowUserToResizeRows = false;
			this.mtbList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.mtbList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.mtbList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.mtbList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.KamokuNm,
            this.Zandaka,
            this.KarikataHassei,
            this.KashikataHassei,
            this.TouZan,
            this.startKmkCd,
            this.endKmkCd,
            this.listKmkCd});
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.mtbList.DefaultCellStyle = dataGridViewCellStyle4;
			this.mtbList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mtbList.EnableHeadersVisualStyles = false;
			this.mtbList.Location = new System.Drawing.Point(0, 0);
			this.mtbList.Margin = new System.Windows.Forms.Padding(4);
			this.mtbList.MultiSelect = false;
			this.mtbList.Name = "mtbList";
			this.mtbList.ReadOnly = true;
			this.mtbList.RowHeadersVisible = false;
			this.mtbList.RowTemplate.Height = 21;
			this.mtbList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.mtbList.Size = new System.Drawing.Size(1022, 604);
			this.mtbList.TabIndex = 902;
			this.mtbList.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.mtbList_RowEnter);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 35);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1032, 661);
			this.fsiTableLayoutPanel1.TabIndex = 903;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.mtbList);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 52);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(1022, 604);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.fsiPanel6);
			this.fsiPanel1.Controls.Add(this.fsiPanel5);
			this.fsiPanel1.Controls.Add(this.fsiPanel4);
			this.fsiPanel1.Controls.Add(this.fsiPanel3);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(1022, 38);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.lblJp);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel6.Location = new System.Drawing.Point(514, 0);
			this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(404, 38);
			this.fsiPanel6.TabIndex = 4;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.lblZei);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel5.Location = new System.Drawing.Point(918, 0);
			this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(104, 38);
			this.fsiPanel5.TabIndex = 3;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lblGridViewTitle);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel4.Location = new System.Drawing.Point(188, 0);
			this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(327, 38);
			this.fsiPanel4.TabIndex = 2;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblKaishaSettei);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel3.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(188, 38);
			this.fsiPanel3.TabIndex = 1;
			// 
			// KamokuNm
			// 
			this.KamokuNm.HeaderText = "科　目　名";
			this.KamokuNm.Name = "KamokuNm";
			this.KamokuNm.ReadOnly = true;
			this.KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.KamokuNm.Width = 230;
			// 
			// Zandaka
			// 
			this.Zandaka.HeaderText = "前月残高";
			this.Zandaka.Name = "Zandaka";
			this.Zandaka.ReadOnly = true;
			this.Zandaka.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.Zandaka.Width = 130;
			// 
			// KarikataHassei
			// 
			this.KarikataHassei.HeaderText = "借方発生";
			this.KarikataHassei.Name = "KarikataHassei";
			this.KarikataHassei.ReadOnly = true;
			this.KarikataHassei.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.KarikataHassei.Width = 130;
			// 
			// KashikataHassei
			// 
			this.KashikataHassei.HeaderText = "貸方発生";
			this.KashikataHassei.Name = "KashikataHassei";
			this.KashikataHassei.ReadOnly = true;
			this.KashikataHassei.Width = 130;
			// 
			// TouZan
			// 
			this.TouZan.HeaderText = "当　残";
			this.TouZan.Name = "TouZan";
			this.TouZan.ReadOnly = true;
			this.TouZan.Width = 130;
			// 
			// startKmkCd
			// 
			this.startKmkCd.HeaderText = "";
			this.startKmkCd.Name = "startKmkCd";
			this.startKmkCd.ReadOnly = true;
			this.startKmkCd.Visible = false;
			// 
			// endKmkCd
			// 
			this.endKmkCd.HeaderText = "";
			this.endKmkCd.Name = "endKmkCd";
			this.endKmkCd.ReadOnly = true;
			this.endKmkCd.Visible = false;
			// 
			// listKmkCd
			// 
			this.listKmkCd.HeaderText = "";
			this.listKmkCd.Name = "listKmkCd";
			this.listKmkCd.ReadOnly = true;
			this.listKmkCd.Visible = false;
			// 
			// ZMMR1032
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1041, 789);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "ZMMR1032";
			this.ShowFButton = true;
			this.Text = "";
			this.Shown += new System.EventHandler(this.ZMMR1032_Shown);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.mtbList)).EndInit();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKaishaSettei;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblZei;
        private System.Windows.Forms.Label lblGridViewTitle;
        private System.Windows.Forms.DataGridView mtbList;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
		private System.Windows.Forms.DataGridViewTextBoxColumn KamokuNm;
		private System.Windows.Forms.DataGridViewTextBoxColumn Zandaka;
		private System.Windows.Forms.DataGridViewTextBoxColumn KarikataHassei;
		private System.Windows.Forms.DataGridViewTextBoxColumn KashikataHassei;
		private System.Windows.Forms.DataGridViewTextBoxColumn TouZan;
		private System.Windows.Forms.DataGridViewTextBoxColumn startKmkCd;
		private System.Windows.Forms.DataGridViewTextBoxColumn endKmkCd;
		private System.Windows.Forms.DataGridViewTextBoxColumn listKmkCd;
	}
}