﻿namespace jp.co.fsi.zm.zmdr1031
{
    partial class ZMDR1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.rdoZenShiwake = new System.Windows.Forms.RadioButton();
            this.rdoTujoShiwake = new System.Windows.Forms.RadioButton();
            this.rdoKessanShiwake = new System.Windows.Forms.RadioButton();
            this.rdoDateJun = new System.Windows.Forms.RadioButton();
            this.rdoBangoJun = new System.Windows.Forms.RadioButton();
            this.lblDenpyoBangoBet = new System.Windows.Forms.Label();
            this.txtDenpyoBangoTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoBangoTo = new System.Windows.Forms.Label();
            this.txtDenpyoBangoFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoBangoFr = new System.Windows.Forms.Label();
            this.lblTantoshaBet = new System.Windows.Forms.Label();
            this.txtTantoshaTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaTo = new System.Windows.Forms.Label();
            this.txtTantoshaFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaFr = new System.Windows.Forms.Label();
            this.lblKanjoKamokuBet = new System.Windows.Forms.Label();
            this.txtKanjoKamokuTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuTo = new System.Windows.Forms.Label();
            this.txtKanjoKamokuFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuFr = new System.Windows.Forms.Label();
            this.lblBumonBet = new System.Windows.Forms.Label();
            this.txtBumonTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonTo = new System.Windows.Forms.Label();
            this.txtBumonFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonFr = new System.Windows.Forms.Label();
            this.rdoKamokuGokei = new System.Windows.Forms.RadioButton();
            this.rdoFurikaeDenpyo = new System.Windows.Forms.RadioButton();
            this.rdoNyukinDenpyo = new System.Windows.Forms.RadioButton();
            this.rdoShukkinDenpyo = new System.Windows.Forms.RadioButton();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel10.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "";
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.Location = new System.Drawing.Point(145, 3);
            this.lblGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(55, 24);
            this.lblGengoFr.TabIndex = 12;
            this.lblGengoFr.Tag = "DISPNAME";
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.Location = new System.Drawing.Point(269, 4);
            this.txtMonthFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(39, 23);
            this.txtMonthFr.TabIndex = 15;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearFr.Location = new System.Drawing.Point(203, 4);
            this.txtYearFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(39, 23);
            this.txtYearFr.TabIndex = 13;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.Location = new System.Drawing.Point(336, 4);
            this.txtDayFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(39, 23);
            this.txtDayFr.TabIndex = 17;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblYearFr
            // 
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.Location = new System.Drawing.Point(245, 3);
            this.lblYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(20, 24);
            this.lblYearFr.TabIndex = 14;
            this.lblYearFr.Tag = "CHANGE";
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.Location = new System.Drawing.Point(312, 3);
            this.lblMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(20, 24);
            this.lblMonthFr.TabIndex = 16;
            this.lblMonthFr.Tag = "CHANGE";
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.Location = new System.Drawing.Point(379, 3);
            this.lblDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(20, 24);
            this.lblDayFr.TabIndex = 18;
            this.lblDayFr.Tag = "CHANGE";
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(413, 3);
            this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(23, 24);
            this.lblCodeBetDate.TabIndex = 19;
            this.lblCodeBetDate.Tag = "CHANGE";
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblDayTo.Location = new System.Drawing.Point(695, 3);
            this.lblDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(20, 24);
            this.lblDayTo.TabIndex = 27;
            this.lblDayTo.Tag = "CHANGE";
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.Location = new System.Drawing.Point(617, 3);
            this.lblMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(20, 24);
            this.lblMonthTo.TabIndex = 25;
            this.lblMonthTo.Tag = "CHANGE";
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearTo
            // 
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.Location = new System.Drawing.Point(551, 3);
            this.lblYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(20, 24);
            this.lblYearTo.TabIndex = 23;
            this.lblYearTo.Tag = "CHANGE";
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDayTo.Location = new System.Drawing.Point(652, 4);
            this.txtDayTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(39, 23);
            this.txtDayTo.TabIndex = 26;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYearTo.Location = new System.Drawing.Point(508, 4);
            this.txtYearTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(39, 23);
            this.txtYearTo.TabIndex = 22;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.Location = new System.Drawing.Point(575, 4);
            this.txtMonthTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(39, 23);
            this.txtMonthTo.TabIndex = 24;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.Location = new System.Drawing.Point(451, 3);
            this.lblGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(55, 24);
            this.lblGengoTo.TabIndex = 21;
            this.lblGengoTo.Tag = "DISPNAME";
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoZenShiwake
            // 
            this.rdoZenShiwake.AutoSize = true;
            this.rdoZenShiwake.BackColor = System.Drawing.Color.Silver;
            this.rdoZenShiwake.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZenShiwake.Location = new System.Drawing.Point(382, 4);
            this.rdoZenShiwake.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZenShiwake.Name = "rdoZenShiwake";
            this.rdoZenShiwake.Size = new System.Drawing.Size(74, 20);
            this.rdoZenShiwake.TabIndex = 9;
            this.rdoZenShiwake.TabStop = true;
            this.rdoZenShiwake.Tag = "CHANGE";
            this.rdoZenShiwake.Text = "全仕訳";
            this.rdoZenShiwake.UseVisualStyleBackColor = false;
            // 
            // rdoTujoShiwake
            // 
            this.rdoTujoShiwake.AutoSize = true;
            this.rdoTujoShiwake.BackColor = System.Drawing.Color.Silver;
            this.rdoTujoShiwake.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoTujoShiwake.Location = new System.Drawing.Point(145, 4);
            this.rdoTujoShiwake.Margin = new System.Windows.Forms.Padding(4);
            this.rdoTujoShiwake.Name = "rdoTujoShiwake";
            this.rdoTujoShiwake.Size = new System.Drawing.Size(90, 20);
            this.rdoTujoShiwake.TabIndex = 7;
            this.rdoTujoShiwake.TabStop = true;
            this.rdoTujoShiwake.Tag = "CHANGE";
            this.rdoTujoShiwake.Text = "通常仕訳";
            this.rdoTujoShiwake.UseVisualStyleBackColor = false;
            // 
            // rdoKessanShiwake
            // 
            this.rdoKessanShiwake.AutoSize = true;
            this.rdoKessanShiwake.BackColor = System.Drawing.Color.Silver;
            this.rdoKessanShiwake.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessanShiwake.Location = new System.Drawing.Point(259, 4);
            this.rdoKessanShiwake.Margin = new System.Windows.Forms.Padding(4);
            this.rdoKessanShiwake.Name = "rdoKessanShiwake";
            this.rdoKessanShiwake.Size = new System.Drawing.Size(90, 20);
            this.rdoKessanShiwake.TabIndex = 8;
            this.rdoKessanShiwake.TabStop = true;
            this.rdoKessanShiwake.Tag = "CHANGE";
            this.rdoKessanShiwake.Text = "決算仕訳";
            this.rdoKessanShiwake.UseVisualStyleBackColor = false;
            // 
            // rdoDateJun
            // 
            this.rdoDateJun.AutoSize = true;
            this.rdoDateJun.BackColor = System.Drawing.Color.Silver;
            this.rdoDateJun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoDateJun.Location = new System.Drawing.Point(145, 5);
            this.rdoDateJun.Margin = new System.Windows.Forms.Padding(4);
            this.rdoDateJun.Name = "rdoDateJun";
            this.rdoDateJun.Size = new System.Drawing.Size(106, 20);
            this.rdoDateJun.TabIndex = 35;
            this.rdoDateJun.TabStop = true;
            this.rdoDateJun.Tag = "CHANGE";
            this.rdoDateJun.Text = "伝票日付順";
            this.rdoDateJun.UseVisualStyleBackColor = false;
            // 
            // rdoBangoJun
            // 
            this.rdoBangoJun.AutoSize = true;
            this.rdoBangoJun.BackColor = System.Drawing.Color.Silver;
            this.rdoBangoJun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoBangoJun.Location = new System.Drawing.Point(259, 5);
            this.rdoBangoJun.Margin = new System.Windows.Forms.Padding(4);
            this.rdoBangoJun.Name = "rdoBangoJun";
            this.rdoBangoJun.Size = new System.Drawing.Size(106, 20);
            this.rdoBangoJun.TabIndex = 36;
            this.rdoBangoJun.TabStop = true;
            this.rdoBangoJun.Tag = "CHANGE";
            this.rdoBangoJun.Text = "伝票番号順";
            this.rdoBangoJun.UseVisualStyleBackColor = false;
            // 
            // lblDenpyoBangoBet
            // 
            this.lblDenpyoBangoBet.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoBangoBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoBangoBet.Location = new System.Drawing.Point(292, 5);
            this.lblDenpyoBangoBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDenpyoBangoBet.Name = "lblDenpyoBangoBet";
            this.lblDenpyoBangoBet.Size = new System.Drawing.Size(23, 20);
            this.lblDenpyoBangoBet.TabIndex = 31;
            this.lblDenpyoBangoBet.Tag = "CHANGE";
            this.lblDenpyoBangoBet.Text = "～";
            this.lblDenpyoBangoBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDenpyoBangoTo
            // 
            this.txtDenpyoBangoTo.AutoSizeFromLength = false;
            this.txtDenpyoBangoTo.DisplayLength = null;
            this.txtDenpyoBangoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenpyoBangoTo.Location = new System.Drawing.Point(316, 4);
            this.txtDenpyoBangoTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDenpyoBangoTo.MaxLength = 6;
            this.txtDenpyoBangoTo.Name = "txtDenpyoBangoTo";
            this.txtDenpyoBangoTo.Size = new System.Drawing.Size(65, 23);
            this.txtDenpyoBangoTo.TabIndex = 32;
            this.txtDenpyoBangoTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDenpyoBangoTo
            // 
            this.lblDenpyoBangoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblDenpyoBangoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoBangoTo.Location = new System.Drawing.Point(385, 3);
            this.lblDenpyoBangoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDenpyoBangoTo.Name = "lblDenpyoBangoTo";
            this.lblDenpyoBangoTo.Size = new System.Drawing.Size(77, 24);
            this.lblDenpyoBangoTo.TabIndex = 33;
            this.lblDenpyoBangoTo.Tag = "DISPNAME";
            this.lblDenpyoBangoTo.Text = "最　後";
            this.lblDenpyoBangoTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDenpyoBangoFr
            // 
            this.txtDenpyoBangoFr.AutoSizeFromLength = false;
            this.txtDenpyoBangoFr.DisplayLength = null;
            this.txtDenpyoBangoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenpyoBangoFr.Location = new System.Drawing.Point(145, 4);
            this.txtDenpyoBangoFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDenpyoBangoFr.MaxLength = 6;
            this.txtDenpyoBangoFr.Name = "txtDenpyoBangoFr";
            this.txtDenpyoBangoFr.Size = new System.Drawing.Size(65, 23);
            this.txtDenpyoBangoFr.TabIndex = 29;
            this.txtDenpyoBangoFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDenpyoBangoFr
            // 
            this.lblDenpyoBangoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblDenpyoBangoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoBangoFr.Location = new System.Drawing.Point(215, 3);
            this.lblDenpyoBangoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDenpyoBangoFr.Name = "lblDenpyoBangoFr";
            this.lblDenpyoBangoFr.Size = new System.Drawing.Size(77, 24);
            this.lblDenpyoBangoFr.TabIndex = 30;
            this.lblDenpyoBangoFr.Tag = "DISPNAME";
            this.lblDenpyoBangoFr.Text = "先　頭";
            this.lblDenpyoBangoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTantoshaBet
            // 
            this.lblTantoshaBet.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaBet.Location = new System.Drawing.Point(415, 5);
            this.lblTantoshaBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoshaBet.Name = "lblTantoshaBet";
            this.lblTantoshaBet.Size = new System.Drawing.Size(23, 29);
            this.lblTantoshaBet.TabIndex = 40;
            this.lblTantoshaBet.Tag = "CHANGE";
            this.lblTantoshaBet.Text = "～";
            this.lblTantoshaBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaTo
            // 
            this.txtTantoshaTo.AutoSizeFromLength = false;
            this.txtTantoshaTo.DisplayLength = null;
            this.txtTantoshaTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaTo.Location = new System.Drawing.Point(452, 4);
            this.txtTantoshaTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTantoshaTo.MaxLength = 4;
            this.txtTantoshaTo.Name = "txtTantoshaTo";
            this.txtTantoshaTo.Size = new System.Drawing.Size(65, 23);
            this.txtTantoshaTo.TabIndex = 41;
            this.txtTantoshaTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaTo_Validating);
            // 
            // lblTantoshaTo
            // 
            this.lblTantoshaTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblTantoshaTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaTo.Location = new System.Drawing.Point(521, 3);
            this.lblTantoshaTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoshaTo.Name = "lblTantoshaTo";
            this.lblTantoshaTo.Size = new System.Drawing.Size(189, 24);
            this.lblTantoshaTo.TabIndex = 42;
            this.lblTantoshaTo.Tag = "DISPNAME";
            this.lblTantoshaTo.Text = "最　後";
            this.lblTantoshaTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaFr
            // 
            this.txtTantoshaFr.AutoSizeFromLength = false;
            this.txtTantoshaFr.DisplayLength = null;
            this.txtTantoshaFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaFr.Location = new System.Drawing.Point(145, 4);
            this.txtTantoshaFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtTantoshaFr.MaxLength = 4;
            this.txtTantoshaFr.Name = "txtTantoshaFr";
            this.txtTantoshaFr.Size = new System.Drawing.Size(65, 23);
            this.txtTantoshaFr.TabIndex = 38;
            this.txtTantoshaFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaFr_Validating);
            // 
            // lblTantoshaFr
            // 
            this.lblTantoshaFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblTantoshaFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaFr.Location = new System.Drawing.Point(215, 3);
            this.lblTantoshaFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoshaFr.Name = "lblTantoshaFr";
            this.lblTantoshaFr.Size = new System.Drawing.Size(191, 24);
            this.lblTantoshaFr.TabIndex = 39;
            this.lblTantoshaFr.Tag = "DISPNAME";
            this.lblTantoshaFr.Text = "先　頭";
            this.lblTantoshaFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuBet
            // 
            this.lblKanjoKamokuBet.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuBet.Location = new System.Drawing.Point(415, 4);
            this.lblKanjoKamokuBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuBet.Name = "lblKanjoKamokuBet";
            this.lblKanjoKamokuBet.Size = new System.Drawing.Size(23, 29);
            this.lblKanjoKamokuBet.TabIndex = 46;
            this.lblKanjoKamokuBet.Tag = "CHANGE";
            this.lblKanjoKamokuBet.Text = "～";
            this.lblKanjoKamokuBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuTo
            // 
            this.txtKanjoKamokuTo.AutoSizeFromLength = false;
            this.txtKanjoKamokuTo.DisplayLength = null;
            this.txtKanjoKamokuTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuTo.Location = new System.Drawing.Point(452, 4);
            this.txtKanjoKamokuTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamokuTo.MaxLength = 6;
            this.txtKanjoKamokuTo.Name = "txtKanjoKamokuTo";
            this.txtKanjoKamokuTo.Size = new System.Drawing.Size(65, 23);
            this.txtKanjoKamokuTo.TabIndex = 47;
            this.txtKanjoKamokuTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuTo_Validating);
            // 
            // lblKanjoKamokuTo
            // 
            this.lblKanjoKamokuTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuTo.Location = new System.Drawing.Point(521, 4);
            this.lblKanjoKamokuTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuTo.Name = "lblKanjoKamokuTo";
            this.lblKanjoKamokuTo.Size = new System.Drawing.Size(189, 24);
            this.lblKanjoKamokuTo.TabIndex = 48;
            this.lblKanjoKamokuTo.Tag = "DISPNAME";
            this.lblKanjoKamokuTo.Text = "最　後";
            this.lblKanjoKamokuTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuFr
            // 
            this.txtKanjoKamokuFr.AutoSizeFromLength = false;
            this.txtKanjoKamokuFr.DisplayLength = null;
            this.txtKanjoKamokuFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuFr.Location = new System.Drawing.Point(145, 4);
            this.txtKanjoKamokuFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamokuFr.MaxLength = 6;
            this.txtKanjoKamokuFr.Name = "txtKanjoKamokuFr";
            this.txtKanjoKamokuFr.Size = new System.Drawing.Size(65, 23);
            this.txtKanjoKamokuFr.TabIndex = 44;
            this.txtKanjoKamokuFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuFr_Validating);
            // 
            // lblKanjoKamokuFr
            // 
            this.lblKanjoKamokuFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuFr.Location = new System.Drawing.Point(215, 4);
            this.lblKanjoKamokuFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuFr.Name = "lblKanjoKamokuFr";
            this.lblKanjoKamokuFr.Size = new System.Drawing.Size(191, 24);
            this.lblKanjoKamokuFr.TabIndex = 45;
            this.lblKanjoKamokuFr.Tag = "DISPNAME";
            this.lblKanjoKamokuFr.Text = "先　頭";
            this.lblKanjoKamokuFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonBet
            // 
            this.lblBumonBet.BackColor = System.Drawing.Color.Silver;
            this.lblBumonBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonBet.Location = new System.Drawing.Point(415, 8);
            this.lblBumonBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonBet.Name = "lblBumonBet";
            this.lblBumonBet.Size = new System.Drawing.Size(23, 24);
            this.lblBumonBet.TabIndex = 52;
            this.lblBumonBet.Tag = "CHANGE";
            this.lblBumonBet.Text = "～";
            this.lblBumonBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonTo
            // 
            this.txtBumonTo.AutoSizeFromLength = false;
            this.txtBumonTo.DisplayLength = null;
            this.txtBumonTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonTo.Location = new System.Drawing.Point(452, 7);
            this.txtBumonTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtBumonTo.MaxLength = 4;
            this.txtBumonTo.Name = "txtBumonTo";
            this.txtBumonTo.Size = new System.Drawing.Size(65, 23);
            this.txtBumonTo.TabIndex = 53;
            this.txtBumonTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBumonTo_KeyDown);
            this.txtBumonTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonTo_Validating);
            // 
            // lblBumonTo
            // 
            this.lblBumonTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonTo.Location = new System.Drawing.Point(521, 5);
            this.lblBumonTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonTo.Name = "lblBumonTo";
            this.lblBumonTo.Size = new System.Drawing.Size(189, 24);
            this.lblBumonTo.TabIndex = 54;
            this.lblBumonTo.Tag = "DISPNAME";
            this.lblBumonTo.Text = "最　後";
            this.lblBumonTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonFr
            // 
            this.txtBumonFr.AutoSizeFromLength = false;
            this.txtBumonFr.DisplayLength = null;
            this.txtBumonFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonFr.Location = new System.Drawing.Point(145, 7);
            this.txtBumonFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtBumonFr.MaxLength = 4;
            this.txtBumonFr.Name = "txtBumonFr";
            this.txtBumonFr.Size = new System.Drawing.Size(65, 23);
            this.txtBumonFr.TabIndex = 50;
            this.txtBumonFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonFr_Validating);
            // 
            // lblBumonFr
            // 
            this.lblBumonFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonFr.Location = new System.Drawing.Point(215, 5);
            this.lblBumonFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonFr.Name = "lblBumonFr";
            this.lblBumonFr.Size = new System.Drawing.Size(191, 24);
            this.lblBumonFr.TabIndex = 51;
            this.lblBumonFr.Tag = "DISPNAME";
            this.lblBumonFr.Text = "先　頭";
            this.lblBumonFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoKamokuGokei
            // 
            this.rdoKamokuGokei.AutoSize = true;
            this.rdoKamokuGokei.BackColor = System.Drawing.Color.Silver;
            this.rdoKamokuGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKamokuGokei.Location = new System.Drawing.Point(499, 4);
            this.rdoKamokuGokei.Margin = new System.Windows.Forms.Padding(4);
            this.rdoKamokuGokei.Name = "rdoKamokuGokei";
            this.rdoKamokuGokei.Size = new System.Drawing.Size(138, 20);
            this.rdoKamokuGokei.TabIndex = 5;
            this.rdoKamokuGokei.TabStop = true;
            this.rdoKamokuGokei.Tag = "CHANGE";
            this.rdoKamokuGokei.Text = "科目合計で出力";
            this.rdoKamokuGokei.UseVisualStyleBackColor = false;
            // 
            // rdoFurikaeDenpyo
            // 
            this.rdoFurikaeDenpyo.AutoSize = true;
            this.rdoFurikaeDenpyo.BackColor = System.Drawing.Color.Silver;
            this.rdoFurikaeDenpyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoFurikaeDenpyo.Location = new System.Drawing.Point(382, 4);
            this.rdoFurikaeDenpyo.Margin = new System.Windows.Forms.Padding(4);
            this.rdoFurikaeDenpyo.Name = "rdoFurikaeDenpyo";
            this.rdoFurikaeDenpyo.Size = new System.Drawing.Size(90, 20);
            this.rdoFurikaeDenpyo.TabIndex = 4;
            this.rdoFurikaeDenpyo.TabStop = true;
            this.rdoFurikaeDenpyo.Tag = "CHANGE";
            this.rdoFurikaeDenpyo.Text = "振替伝票";
            this.rdoFurikaeDenpyo.UseVisualStyleBackColor = false;
            // 
            // rdoNyukinDenpyo
            // 
            this.rdoNyukinDenpyo.AutoSize = true;
            this.rdoNyukinDenpyo.BackColor = System.Drawing.Color.Silver;
            this.rdoNyukinDenpyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoNyukinDenpyo.ForeColor = System.Drawing.Color.Red;
            this.rdoNyukinDenpyo.Location = new System.Drawing.Point(145, 4);
            this.rdoNyukinDenpyo.Margin = new System.Windows.Forms.Padding(4);
            this.rdoNyukinDenpyo.Name = "rdoNyukinDenpyo";
            this.rdoNyukinDenpyo.Size = new System.Drawing.Size(90, 20);
            this.rdoNyukinDenpyo.TabIndex = 2;
            this.rdoNyukinDenpyo.TabStop = true;
            this.rdoNyukinDenpyo.Tag = "CHANGE";
            this.rdoNyukinDenpyo.Text = "入金伝票";
            this.rdoNyukinDenpyo.UseVisualStyleBackColor = false;
            // 
            // rdoShukkinDenpyo
            // 
            this.rdoShukkinDenpyo.AutoSize = true;
            this.rdoShukkinDenpyo.BackColor = System.Drawing.Color.Silver;
            this.rdoShukkinDenpyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShukkinDenpyo.ForeColor = System.Drawing.Color.Blue;
            this.rdoShukkinDenpyo.Location = new System.Drawing.Point(259, 4);
            this.rdoShukkinDenpyo.Margin = new System.Windows.Forms.Padding(4);
            this.rdoShukkinDenpyo.Name = "rdoShukkinDenpyo";
            this.rdoShukkinDenpyo.Size = new System.Drawing.Size(90, 20);
            this.rdoShukkinDenpyo.TabIndex = 3;
            this.rdoShukkinDenpyo.TabStop = true;
            this.rdoShukkinDenpyo.Tag = "CHANGE";
            this.rdoShukkinDenpyo.Text = "出金伝票";
            this.rdoShukkinDenpyo.UseVisualStyleBackColor = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(145, 4);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(193, 3);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(360, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
            this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(765, 31);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(765, 31);
            this.label1.TabIndex = 904;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "出力伝票";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel10, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel9, 0, 9);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 8);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 7);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 57);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 10;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(775, 406);
            this.fsiTableLayoutPanel1.TabIndex = 905;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.rdoDateJun);
            this.fsiPanel6.Controls.Add(this.rdoBangoJun);
            this.fsiPanel6.Controls.Add(this.label4);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(5, 205);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(765, 31);
            this.fsiPanel6.TabIndex = 5;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(765, 31);
            this.label4.TabIndex = 904;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "出力順位";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtDenpyoBangoFr);
            this.fsiPanel5.Controls.Add(this.lblDenpyoBangoFr);
            this.fsiPanel5.Controls.Add(this.lblDenpyoBangoBet);
            this.fsiPanel5.Controls.Add(this.lblDenpyoBangoTo);
            this.fsiPanel5.Controls.Add(this.txtDenpyoBangoTo);
            this.fsiPanel5.Controls.Add(this.label8);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(5, 165);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(765, 31);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(765, 31);
            this.label8.TabIndex = 904;
            this.label8.Tag = "CHANGE";
            this.label8.Text = "伝票番号範囲";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel10
            // 
            this.fsiPanel10.Controls.Add(this.label9);
            this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel10.Location = new System.Drawing.Point(4, 244);
            this.fsiPanel10.Name = "fsiPanel10";
            this.fsiPanel10.Size = new System.Drawing.Size(767, 33);
            this.fsiPanel10.TabIndex = 906;
            this.fsiPanel10.Tag = "CHANGE";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Silver;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(767, 33);
            this.label9.TabIndex = 905;
            this.label9.Tag = "CHANGE";
            this.label9.Text = "コード範囲";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblGengoFr);
            this.fsiPanel4.Controls.Add(this.txtMonthFr);
            this.fsiPanel4.Controls.Add(this.txtYearFr);
            this.fsiPanel4.Controls.Add(this.txtDayFr);
            this.fsiPanel4.Controls.Add(this.lblYearFr);
            this.fsiPanel4.Controls.Add(this.lblMonthFr);
            this.fsiPanel4.Controls.Add(this.lblDayFr);
            this.fsiPanel4.Controls.Add(this.lblCodeBetDate);
            this.fsiPanel4.Controls.Add(this.lblGengoTo);
            this.fsiPanel4.Controls.Add(this.txtMonthTo);
            this.fsiPanel4.Controls.Add(this.txtYearTo);
            this.fsiPanel4.Controls.Add(this.txtDayTo);
            this.fsiPanel4.Controls.Add(this.lblDayTo);
            this.fsiPanel4.Controls.Add(this.lblYearTo);
            this.fsiPanel4.Controls.Add(this.lblMonthTo);
            this.fsiPanel4.Controls.Add(this.label3);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 125);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(765, 31);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(765, 31);
            this.label3.TabIndex = 904;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "伝票日付範囲";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.rdoTujoShiwake);
            this.fsiPanel3.Controls.Add(this.rdoKessanShiwake);
            this.fsiPanel3.Controls.Add(this.rdoZenShiwake);
            this.fsiPanel3.Controls.Add(this.label2);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 85);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(765, 31);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(765, 31);
            this.label2.TabIndex = 904;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "仕訳種類";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.rdoKamokuGokei);
            this.fsiPanel2.Controls.Add(this.rdoShukkinDenpyo);
            this.fsiPanel2.Controls.Add(this.rdoNyukinDenpyo);
            this.fsiPanel2.Controls.Add(this.rdoFurikaeDenpyo);
            this.fsiPanel2.Controls.Add(this.label1);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 45);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(765, 31);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(765, 31);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.txtBumonFr);
            this.fsiPanel9.Controls.Add(this.lblBumonFr);
            this.fsiPanel9.Controls.Add(this.lblBumonBet);
            this.fsiPanel9.Controls.Add(this.lblBumonTo);
            this.fsiPanel9.Controls.Add(this.txtBumonTo);
            this.fsiPanel9.Controls.Add(this.label7);
            this.fsiPanel9.Location = new System.Drawing.Point(5, 365);
            this.fsiPanel9.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(765, 36);
            this.fsiPanel9.TabIndex = 8;
            this.fsiPanel9.Tag = "CHANGE";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(765, 36);
            this.label7.TabIndex = 904;
            this.label7.Tag = "CHANGE";
            this.label7.Text = "部門範囲";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.txtKanjoKamokuFr);
            this.fsiPanel8.Controls.Add(this.lblKanjoKamokuFr);
            this.fsiPanel8.Controls.Add(this.lblKanjoKamokuBet);
            this.fsiPanel8.Controls.Add(this.lblKanjoKamokuTo);
            this.fsiPanel8.Controls.Add(this.txtKanjoKamokuTo);
            this.fsiPanel8.Controls.Add(this.label6);
            this.fsiPanel8.Location = new System.Drawing.Point(5, 325);
            this.fsiPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(765, 31);
            this.fsiPanel8.TabIndex = 7;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(765, 31);
            this.label6.TabIndex = 904;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "勘定科目範囲";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.txtTantoshaFr);
            this.fsiPanel7.Controls.Add(this.lblTantoshaFr);
            this.fsiPanel7.Controls.Add(this.lblTantoshaBet);
            this.fsiPanel7.Controls.Add(this.lblTantoshaTo);
            this.fsiPanel7.Controls.Add(this.txtTantoshaTo);
            this.fsiPanel7.Controls.Add(this.label5);
            this.fsiPanel7.Location = new System.Drawing.Point(5, 285);
            this.fsiPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(765, 31);
            this.fsiPanel7.TabIndex = 6;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(765, 31);
            this.label5.TabIndex = 904;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "担当者範囲";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMDR1031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMDR1031";
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel10.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel9.PerformLayout();
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblGengoFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private System.Windows.Forms.Label lblYearFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.RadioButton rdoZenShiwake;
        private System.Windows.Forms.RadioButton rdoTujoShiwake;
        private System.Windows.Forms.RadioButton rdoKessanShiwake;
        private System.Windows.Forms.RadioButton rdoDateJun;
        private System.Windows.Forms.RadioButton rdoBangoJun;
        private System.Windows.Forms.Label lblDenpyoBangoBet;
        private jp.co.fsi.common.controls.FsiTextBox txtDenpyoBangoTo;
        private System.Windows.Forms.Label lblDenpyoBangoTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDenpyoBangoFr;
        private System.Windows.Forms.Label lblDenpyoBangoFr;
        private System.Windows.Forms.Label lblTantoshaBet;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaTo;
        private System.Windows.Forms.Label lblTantoshaTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaFr;
        private System.Windows.Forms.Label lblTantoshaFr;
        private System.Windows.Forms.Label lblKanjoKamokuBet;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuTo;
        private System.Windows.Forms.Label lblKanjoKamokuTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuFr;
        private System.Windows.Forms.Label lblKanjoKamokuFr;
        private System.Windows.Forms.Label lblBumonBet;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonTo;
        private System.Windows.Forms.Label lblBumonTo;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonFr;
        private System.Windows.Forms.Label lblBumonFr;
        private System.Windows.Forms.RadioButton rdoKamokuGokei;
        private System.Windows.Forms.RadioButton rdoFurikaeDenpyo;
        private System.Windows.Forms.RadioButton rdoNyukinDenpyo;
        private System.Windows.Forms.RadioButton rdoShukkinDenpyo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label label1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel8;
        private System.Windows.Forms.Label label7;
        private common.FsiPanel fsiPanel7;
        private System.Windows.Forms.Label label6;
        private common.FsiPanel fsiPanel6;
        private System.Windows.Forms.Label label5;
        private common.FsiPanel fsiPanel5;
        private System.Windows.Forms.Label label4;
        private common.FsiPanel fsiPanel4;
        private System.Windows.Forms.Label label3;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label8;
		private common.FsiPanel fsiPanel10;
        private System.Windows.Forms.Label label9;
    }
}