﻿using System.Data;
using System.Collections.Generic;

using jp.co.fsi.common.report;

namespace jp.co.fsi.zm.zmdr1031
{
    /// <summary>
    /// ZAMR1061R の概要の説明です。
    /// </summary>
    public partial class ZMDR10311R : BaseReport
    {
        public List<string> nmList;

        public ZMDR10311R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {
            //this.label1.Text = "組合長";
            //this.ラベル70.Text = "課長";
            //this.ラベル72.Text = "係長";
            //this.ラベル74.Text = "照査";
            //this.ラベル76.Text = "起票";
            this.label1.Text = nmList[0];
            this.ラベル70.Text = nmList[1];
            this.ラベル72.Text = nmList[2];
            this.ラベル74.Text = nmList[3];
            this.ラベル76.Text = nmList[4];
        }
    }
}
