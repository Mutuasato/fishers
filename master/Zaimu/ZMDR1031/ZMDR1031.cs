﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmdr1031
{
    /// <summary>
    /// 仕訳伝票(ZMDR1031)
    /// </summary>
    public partial class ZMDR1031 : BasePgForm
    { 
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDR1031()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblGengoFr.Text = jpDate[0];
            txtYearFr.Text = jpDate[2];
            txtMonthFr.Text = jpDate[3];
            txtDayFr.Text = jpDate[4];

            lblGengoTo.Text = jpDate[0];
            txtYearTo.Text = jpDate[2];
            txtMonthTo.Text = jpDate[3];
            txtDayTo.Text = jpDate[4];

            rdoKamokuGokei.Checked = true;
            //rdoTujoShiwake.Checked = true;
            rdoDateJun.Checked = true;
            try
            {
                string val = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1031", "Setting", "ShiwakeKbn"));
                switch (val)
                {
                    case "0":
                        rdoTujoShiwake.Checked = true;
                        break;
                    case "1":
                        rdoKessanShiwake.Checked = true;
                        break;
                    case "2":
                        rdoZenShiwake.Checked = true;
                        break;
                    default:
                        rdoTujoShiwake.Checked = true;
                        break;
                }
            }
            catch (Exception)
            {
                rdoTujoShiwake.Checked = true;
            }

            // 初期フォーカス
            txtYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付,船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYearFr":
                case "txtYearTo":
                case "txtTantoshaFr":
                case "txtTantoshaTo":
                case "txtKanjoKamokuFr":
                case "txtKanjoKamokuTo":
                case "txtBumonFr":
                case "txtBumonTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYearFr":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpFr();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYearTo":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpTo();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTantoshaFr":
                    #region 担当者検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9041.KOBC9041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoshaFr.Text = outData[0];
                                this.lblTantoshaFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTantoshaTo":
                    #region 担当者検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9041.KOBC9041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoshaTo.Text = outData[0];
                                this.lblTantoshaTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKanjoKamokuFr":
                    #region 勘定科目検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9011");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoKamokuFr.Text = outData[0];
                                this.lblKanjoKamokuFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKanjoKamokuTo":
                    #region 勘定科目検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9011");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoKamokuTo.Text = outData[0];
                                this.lblKanjoKamokuTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtBumonFr":
                case "txtBumonTo":
                    #region 部門検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    //asm = Assembly.LoadFrom("CMCM2041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "TB_CM_BUMON";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                if (this.ActiveCtlNm.Equals("txtBumonFr"))
                                {
                                    this.txtBumonFr.Text = result[0];
                                    this.lblBumonFr.Text = result[1];
                                }
                                else
                                {
                                    this.txtBumonTo.Text = result[0];
                                    this.lblBumonTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            // 科目設定画面を起動
            using (ZMDR1032 frm = new ZMDR1032())
            {
                DialogResult result = frm.ShowDialog(this);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            using (PrintSettingForm psForm = new PrintSettingForm(new string[1] { "ZMDR10311R" }))
            {
                psForm.ShowDialog();
            }
        }
        #endregion

        #region イベント

        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtYearFr.SelectAll();
            }
            else
            {
                this.txtYearFr.Text = Util.ToString(IsValid.SetYear(this.txtYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthFr.SelectAll();
            }
            else
            {
                this.txtMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayFr.SelectAll();
            }
            else
            {
                this.txtDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDayFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtYearTo.SelectAll();
            }
            else
            {
                this.txtYearTo.Text = Util.ToString(IsValid.SetYear(this.txtYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthTo.SelectAll();
            }
            else
            {
                this.txtMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayTo.SelectAll();
            }
            else
            {
                this.txtDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDayTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 担当者コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblTantoshaFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", Util.ToString(txtMizuageShishoCd.Text), this.txtTantoshaFr.Text);
            if (ValChk.IsEmpty(this.lblTantoshaFr.Text))
            {
                this.lblTantoshaFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 担当者コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblTantoshaTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", Util.ToString(txtMizuageShishoCd.Text), this.txtTantoshaTo.Text);
            if (ValChk.IsEmpty(this.lblTantoshaTo.Text))
            {
                this.lblTantoshaTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 勘定科目コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKanjoKamokuFr.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", Util.ToString(txtMizuageShishoCd.Text), this.txtKanjoKamokuFr.Text);
            if (ValChk.IsEmpty(this.lblKanjoKamokuFr.Text))
            {
                this.lblKanjoKamokuFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 勘定科目コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKanjoKamokuTo.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", Util.ToString(txtMizuageShishoCd.Text), this.txtKanjoKamokuTo.Text);
            if (ValChk.IsEmpty(this.lblKanjoKamokuTo.Text))
            {
                this.lblKanjoKamokuTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblBumonFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", Util.ToString(txtMizuageShishoCd.Text), this.txtBumonFr.Text);
            if (ValChk.IsEmpty(this.lblBumonFr.Text))
            {
                this.lblBumonFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblBumonTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", Util.ToString(txtMizuageShishoCd.Text), this.txtBumonTo.Text);
            if (ValChk.IsEmpty(this.lblBumonTo.Text))
            {
                this.lblBumonTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtBumonTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayFr.Text) > lastDayInMonth)
            {
                this.txtDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoFr.Text,
                                  this.txtYearFr.Text,
                                  this.txtMonthFr.Text,
                                  this.txtDayFr.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayTo.Text) > lastDayInMonth)
            {
                this.txtDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoTo.Text,
                                  this.txtYearTo.Text,
                                  this.txtMonthTo.Text,
                                  this.txtDayTo.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 伝票番号(自)の入力チェック
        /// </summary>
        private bool IsValidDenpyoBangoFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtDenpyoBangoFr.Text))
            {
                Msg.Error("伝票番号は数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 伝票番号(至)の入力チェック
        /// </summary>
        private bool IsValidDenpyoBangoTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDenpyoBangoTo.Text))
            {
                Msg.Error("伝票番号は数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 担当者コード(自)の入力チェック
        /// </summary>
        private bool IsValidTantoshaFr()
        {
            if (ValChk.IsEmpty(this.txtTantoshaFr.Text))
            {
                this.lblTantoshaFr.Text = "先　頭";
            }
            else
            {
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtTantoshaFr.Text))
                {
                    Msg.Error("担当者コードは数値のみで入力してください。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 担当者コード(至)の入力チェック
        /// </summary>
        private bool IsValidTantoshaTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoshaTo.Text))
            {
                Msg.Error("担当者コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 勘定科目コード(自)の入力チェック
        /// </summary>
        private bool IsValidKanjoKamokuFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamokuFr.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 勘定科目コード(至)の入力チェック
        /// </summary>
        private bool IsValidKanjoKamokuTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamokuTo.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 部門コード(自)の入力チェック
        /// </summary>
        private bool IsValidBumonFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonFr.Text))
            {
                Msg.Error("部門コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 部門コード(至)の入力チェック
        /// </summary>
        private bool IsValidBumonTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonTo.Text))
            {
                Msg.Error("部門コードは数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                this.txtYearFr.Focus();
                this.txtYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                this.txtMonthFr.Focus();
                this.txtMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                this.txtDayFr.Focus();
                this.txtDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                this.txtYearTo.Focus();
                this.txtYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                this.txtMonthTo.Focus();
                this.txtMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                this.txtDayTo.Focus();
                this.txtDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            // 伝票番号(自)の入力チェック
            if (!IsValidDenpyoBangoFr())
            {
                this.txtDenpyoBangoFr.Focus();
                this.txtDenpyoBangoFr.SelectAll();
                return false;
            }
            // 伝票番号(至)の入力チェック
            if (!IsValidDenpyoBangoTo())
            {
                this.txtDenpyoBangoTo.Focus();
                this.txtDenpyoBangoTo.SelectAll();
                return false;
            }

            // 担当者コード(自)の入力チェック
            if (!IsValidTantoshaFr())
            {
                this.txtTantoshaFr.Focus();
                this.txtTantoshaFr.SelectAll();
                return false;
            }
            // 担当者コード(至)の入力チェック
            if (!IsValidTantoshaTo())
            {
                this.txtTantoshaTo.Focus();
                this.txtTantoshaTo.SelectAll();
                return false;
            }

            // 勘定科目コード(自)の入力チェック
            if (!IsValidKanjoKamokuFr())
            {
                this.txtKanjoKamokuFr.Focus();
                this.txtKanjoKamokuFr.SelectAll();
                return false;
            }
            // 勘定科目コード(至)の入力チェック
            if (!IsValidKanjoKamokuTo())
            {
                this.txtKanjoKamokuTo.Focus();
                this.txtKanjoKamokuTo.SelectAll();
                return false;
            }

            // 部門コード(自)の入力チェック
            if (!IsValidBumonFr())
            {
                this.txtBumonFr.Focus();
                this.txtBumonFr.SelectAll();
                return false;
            }
            // 部門コード(至)の入力チェック
            if (!IsValidBumonTo())
            {
                this.txtBumonTo.Focus();
                this.txtBumonTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    #region 取得列の定義
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");
                    cols.Append(" ,ITEM28");
                    cols.Append(" ,ITEM29");
                    cols.Append(" ,ITEM30");
                    cols.Append(" ,ITEM31");
                    cols.Append(" ,ITEM32");
                    cols.Append(" ,ITEM33");
                    cols.Append(" ,ITEM34");
                    cols.Append(" ,ITEM35");
                    cols.Append(" ,ITEM36");
                    cols.Append(" ,ITEM37");
                    cols.Append(" ,ITEM38");
                    cols.Append(" ,ITEM39");
                    cols.Append(" ,ITEM40");
                    cols.Append(" ,ITEM41");
                    cols.Append(" ,ITEM42");
                    cols.Append(" ,ITEM43");
                    cols.Append(" ,ITEM44");
                    cols.Append(" ,ITEM45");
                    cols.Append(" ,ITEM46");
                    cols.Append(" ,ITEM47");
                    cols.Append(" ,ITEM48");
                    cols.Append(" ,ITEM49");
                    cols.Append(" ,ITEM50");
                    cols.Append(" ,ITEM51");
                    cols.Append(" ,ITEM52");
                    cols.Append(" ,ITEM53");
                    cols.Append(" ,ITEM54");
                    cols.Append(" ,ITEM55");
                    cols.Append(" ,ITEM56");
                    cols.Append(" ,ITEM57");
                    cols.Append(" ,ITEM58");
                    cols.Append(" ,ITEM59");
                    cols.Append(" ,ITEM60");
                    cols.Append(" ,ITEM61");
                    cols.Append(" ,ITEM62");
                    cols.Append(" ,ITEM63");
                    cols.Append(" ,ITEM64");
                    cols.Append(" ,ITEM65");
                    cols.Append(" ,ITEM66");
                    cols.Append(" ,ITEM67");
                    cols.Append(" ,ITEM68");
                    cols.Append(" ,ITEM69");
                    cols.Append(" ,ITEM70");
                    cols.Append(" ,ITEM71");
                    cols.Append(" ,ITEM72");
                    cols.Append(" ,ITEM73");
                    cols.Append(" ,ITEM74");
                    cols.Append(" ,ITEM75");
                    cols.Append(" ,ITEM76");
                    cols.Append(" ,ITEM77");
                    cols.Append(" ,ITEM78");
                    cols.Append(" ,ITEM79");
                    cols.Append(" ,ITEM80");
                    cols.Append(" ,ITEM81");
                    cols.Append(" ,ITEM82");
                    cols.Append(" ,ITEM83");
                    cols.Append(" ,ITEM84");
                    cols.Append(" ,ITEM85");
                    cols.Append(" ,ITEM86");
                    cols.Append(" ,ITEM87");
                    cols.Append(" ,ITEM88");
                    cols.Append(" ,ITEM89");
                    cols.Append(" ,ITEM90");
                    cols.Append(" ,ITEM91");
                    cols.Append(" ,ITEM92");
                    cols.Append(" ,ITEM93");
                    cols.Append(" ,ITEM94");
                    cols.Append(" ,ITEM95");
                    cols.Append(" ,ITEM96");
                    cols.Append(" ,ITEM97");
                    cols.Append(" ,ITEM98");
                    cols.Append(" ,ITEM99");
                    cols.Append(" ,ITEM100");
                    #endregion

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMDR10311R rpt = new ZMDR10311R(dtOutput);

                    // 捺印欄名
                    List<string> nmList = new List<string>() { "組合長", "課長", "係長", "照査", "起票" };
                    try
                    {
                        string[] nml;
                        nml = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1031", "Setting", "nameList")).Split(',');
                        nmList = new List<string>(nml);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message); ;
                    }
                    rpt.nmList = nmList;

                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            // 日付を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                    this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                    this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);

            // 日付を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 支所コード
            int shishoCd = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));

            // 伝票番号設定
            string denpyoFr;
            string denpyoTo;
            if (Util.ToString(txtDenpyoBangoFr.Text) != "")
            {
                denpyoFr = txtDenpyoBangoFr.Text;
            }
            else
            {
                denpyoFr = "0";
            }
            if (Util.ToString(txtDenpyoBangoTo.Text) != "")
            {
                denpyoTo = txtDenpyoBangoTo.Text;
            }
            else
            {
                denpyoTo = "999999";
            }

            // 担当者コード設定
            string tantoshaFr;
            string tantoshaTo;
            if (Util.ToString(txtTantoshaFr.Text) != "")
            {
                tantoshaFr = txtTantoshaFr.Text;
            }
            else
            {
                tantoshaFr = "0";
            }
            if (Util.ToString(txtTantoshaTo.Text) != "")
            {
                tantoshaTo = txtTantoshaTo.Text;
            }
            else
            {
                tantoshaTo = "9999";
            }

            // 勘定科目コード設定
            string kanjoKamokuFr;
            string kanjoKamokuTo;
            if (Util.ToString(txtKanjoKamokuFr.Text) != "")
            {
                kanjoKamokuFr = txtKanjoKamokuFr.Text;
            }
            else
            {
                kanjoKamokuFr = "0";
            }
            if (Util.ToString(txtKanjoKamokuTo.Text) != "")
            {
                kanjoKamokuTo = txtKanjoKamokuTo.Text;
            }
            else
            {
                kanjoKamokuTo = "999999";
            }

            // 部門設定
            string bumonFr;
            string bumonTo;
            if (Util.ToString(txtBumonFr.Text) != "")
            {
                bumonFr = txtBumonFr.Text;
            }
            else
            {
                bumonFr = "0";
            }
            if (Util.ToString(txtBumonTo.Text) != "")
            {
                bumonTo = txtBumonTo.Text;
            }
            else
            {
                bumonTo = "9999";
            }

            // 検索日付に発生しているデータの伝票番号を取得
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            // 入金伝票か出金伝票の場合
            if (rdoNyukinDenpyo.Checked == true || rdoShukkinDenpyo.Checked == true)
            {
                Sql.Append(" SELECT DISTINCT");
                Sql.Append("     SHISHO_CD,");
                Sql.Append("     DENPYO_DATE,");
                Sql.Append("     DENPYO_BANGO AS DENPYO_BANGO");
                Sql.Append(" FROM");
                Sql.Append("     TB_ZM_SHIWAKE_MEISAI");
                Sql.Append(" WHERE");
                Sql.Append("     KAISHA_CD = @KAISHA_CD AND");

                if (shishoCd != 0)
                    Sql.Append("     SHISHO_CD = @SHISHO_CD AND");

                Sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO AND");
                Sql.Append("     KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND @KANJO_KAMOKU_CD_TO AND");
                Sql.Append("     DENPYO_BANGO IN(");
                Sql.Append(" SELECT DISTINCT");
                Sql.Append("     DENPYO_BANGO AS DENPYO_BANGO");
                Sql.Append(" FROM");
                Sql.Append("     TB_ZM_SHIWAKE_MEISAI");
                Sql.Append(" WHERE");
                Sql.Append("     KAISHA_CD = @KAISHA_CD AND");

                if (shishoCd != 0)
                    Sql.Append("     SHISHO_CD = @SHISHO_CD AND");

                Sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO AND");
                Sql.Append("     DENPYO_DATE BETWEEN CAST(@DENPYO_DATE_FR AS DATETIME) AND CAST(@DENPYO_DATE_TO AS DATETIME) AND");
                Sql.Append("     DENPYO_BANGO BETWEEN @DENPYO_BANGO_FR AND @DENPYO_BANGO_TO AND");
                Sql.Append("     BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO AND");
                Sql.Append("     KANJO_KAMOKU_CD = @SETTEI_CD AND");
                if (rdoShukkinDenpyo.Checked == true)
                {
                    Sql.Append(" TAISHAKU_KUBUN = 2) AND");
                }
                else
                {
                    Sql.Append(" TAISHAKU_KUBUN = 1) AND");
                }
                if (rdoTujoShiwake.Checked == true)
                {
                    Sql.Append("     KESSAN_KUBUN = 0 AND");
                }
                else
                    if (rdoKessanShiwake.Checked == true)
                    {
                        Sql.Append("     KESSAN_KUBUN = 1 AND");
                    }
                Sql.Append("     DENPYO_KUBUN = 1");
                Sql.Append("     ORDER BY");
                Sql.Append("     SHISHO_CD,");
                if (rdoDateJun.Checked == true)
                {
                    Sql.Append("     DENPYO_DATE");
                }
                else
                {
                    Sql.Append("     DENPYO_BANGO");
                }
            }
            else
            {
                // 振替伝票か科目合計の場合
                Sql.Append(" SELECT DISTINCT");
                Sql.Append("     SHISHO_CD,");
                Sql.Append("     DENPYO_DATE,");
                Sql.Append("     DENPYO_BANGO AS DENPYO_BANGO");
                Sql.Append(" FROM");
                Sql.Append("     TB_ZM_SHIWAKE_MEISAI");
                Sql.Append(" WHERE");
                Sql.Append("     KAISHA_CD = @KAISHA_CD AND");

                if (shishoCd != 0)
                    Sql.Append("     SHISHO_CD = @SHISHO_CD AND");

                Sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO AND");
                Sql.Append("     DENPYO_DATE BETWEEN CAST(@DENPYO_DATE_FR AS DATETIME) AND CAST(@DENPYO_DATE_TO AS DATETIME) AND");
                Sql.Append("     DENPYO_BANGO BETWEEN @DENPYO_BANGO_FR AND @DENPYO_BANGO_TO AND");
                Sql.Append("     KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND @KANJO_KAMOKU_CD_TO AND");
                Sql.Append("     BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO AND");

                // 振替伝票の場合、勘定科目コードが設定された科目以外のものを取得する。
                if (rdoFurikaeDenpyo.Checked == true)
                {
                    Sql.Append(" DENPYO_BANGO NOT IN(");
                    Sql.Append(" SELECT DISTINCT");
                    Sql.Append("     DENPYO_BANGO AS DENPYO_BANGO");
                    Sql.Append(" FROM");
                    Sql.Append("     TB_ZM_SHIWAKE_MEISAI");
                    Sql.Append(" WHERE");
                    Sql.Append("     KAISHA_CD = @KAISHA_CD AND");

                    if (shishoCd != 0)
                        Sql.Append("     SHISHO_CD = @SHISHO_CD AND");

                    Sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO AND");
                    Sql.Append("     DENPYO_DATE BETWEEN CAST(@DENPYO_DATE_FR AS DATETIME) AND CAST(@DENPYO_DATE_TO AS DATETIME) AND");
                    Sql.Append("     KANJO_KAMOKU_CD = @SETTEI_CD) AND");
                }
                if (rdoTujoShiwake.Checked == true)
                {
                    Sql.Append("     KESSAN_KUBUN = 0 AND");
                }
                else
                    if (rdoKessanShiwake.Checked == true)
                    {
                        Sql.Append("     KESSAN_KUBUN = 1 AND");
                    }
                Sql.Append("     DENPYO_KUBUN = 1");
                Sql.Append("     ORDER BY");
                Sql.Append("     SHISHO_CD,");
                if (rdoDateJun.Checked == true)
                {
                    Sql.Append("     DENPYO_DATE");
                }
                else
                {
                    Sql.Append("     DENPYO_BANGO");
                }
            }
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            // 検索する日付
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            // 検索する伝票番号
            dpc.SetParam("@DENPYO_BANGO_FR", SqlDbType.Decimal, 6, denpyoFr);
            dpc.SetParam("@DENPYO_BANGO_TO", SqlDbType.Decimal, 6, denpyoTo);
            // 検索する担当者コード
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, tantoshaFr);
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, tantoshaTo);
            // 検索する勘定科目コード
            dpc.SetParam("@KANJO_KAMOKU_CD_FR", SqlDbType.Decimal, 6, kanjoKamokuFr);
            dpc.SetParam("@KANJO_KAMOKU_CD_TO", SqlDbType.Decimal, 6, kanjoKamokuTo);
            // 検索する部門
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonTo);
            // 設定された科目コード
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 6, this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1031", "Setting", "KamokuCd"));

            DataTable dtDenpyoBango = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            foreach (DataRow dr in dtDenpyoBango.Rows)
            {
                if (rdoKamokuGokei.Checked == true)
                {
                    // 伝票番号からデータを取得
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append(" SELECT");
                    Sql.Append("     KAISHA_CD                       AS KAISHA_CD,");
                    Sql.Append("     SHISHO_CD                       AS SHISHO_CD,");
                    Sql.Append("     DENPYO_DATE                     AS DENPYO_DATE,");
                    Sql.Append("     DENPYO_BANGO                    AS DENPYO_BANGO,");
                    Sql.Append("     MIN(TANTOSHA_CD)                AS TANTOSHA_CD,");
                    Sql.Append("     MIN(TANTOSHA_NM)                AS TANTOSHA_NM,");
                    Sql.Append("     MAX(KARIKATA_KANJO_KAMOKU_CD)   AS KARIKATA_KANJO_KAMOKU_CD,");
                    Sql.Append("     MAX(KARIKATA_KANJO_KAMOKU_NM)   AS KARIKATA_KANJO_KAMOKU_NM,");
                    Sql.Append("     SUM(KARIKATA_ZEIKOMI_KINGAKU)   AS KARIKATA_ZEIKOMI_KINGAKU,");
                    Sql.Append("     SUM(KARIKATA_ZEINUKI_KINGAKU)   AS KARIKATA_ZEINUKI_KINGAKU,");
                    Sql.Append("     SUM(KARIKATA_SHOHIZEI_KINGAKU)  AS KARIKATA_SHOHIZEI_KINGAKU,");
                    Sql.Append("     MAX(KARIKATA_ZEI_RITSU)         AS KARIKATA_ZEI_RITSU,");
                    Sql.Append("     MAX(KASHIKATA_KANJO_KAMOKU_CD)  AS KASHIKATA_KANJO_KAMOKU_CD,");
                    Sql.Append("     MAX(KASHIKATA_KANJO_KAMOKU_NM)  AS KASHIKATA_KANJO_KAMOKU_NM,");
                    Sql.Append("     SUM(KASHIKATA_ZEIKOMI_KINGAKU)  AS KASHIKATA_ZEIKOMI_KINGAKU,");
                    Sql.Append("     SUM(KASHIKATA_ZEINUKI_KINGAKU)  AS KASHIKATA_ZEINUKI_KINGAKU,");
                    Sql.Append("     SUM(KASHIKATA_SHOHIZEI_KINGAKU) AS KASHIKATA_SHOHIZEI_KINGAKU,");
                    Sql.Append("     MAX(KASHIKATA_ZEI_RITSU)        AS KASHIKATA_ZEI_RITSU,");
                    Sql.Append("     MIN(TEKIYO)                     AS TEKIYO");
                    Sql.Append(" FROM");
                    Sql.Append("     VI_ZM_SHIWAKE_DENPYO_GOKEI");
                    Sql.Append(" WHERE");
                    Sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO AND");
                    Sql.Append("     KAISHA_CD = @KAISHA_CD AND");

                    //if (shishoCd != 0)
                        Sql.Append("     SHISHO_CD = @SHISHO_CD AND");

                    Sql.Append("     DENPYO_DATE BETWEEN CAST(@DENPYO_DATE_FR AS DATETIME) AND CAST(@DENPYO_DATE_TO AS DATETIME) AND");
                    Sql.Append("     DENPYO_BANGO = @DENPYO_BANGO AND");
                    Sql.Append("     TANTOSHA_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO");
                    Sql.Append(" GROUP BY");
                    Sql.Append("     KAISHA_CD,");
                    Sql.Append("     SHISHO_CD,");
                    Sql.Append("     DENPYO_DATE,");
                    Sql.Append("     DENPYO_BANGO,");
                    Sql.Append("     GYO_BANGO");
                    Sql.Append(" ORDER BY");
                    Sql.Append("     KAISHA_CD ASC,");
                    Sql.Append("     SHISHO_CD ASC,");
                    if (rdoDateJun.Checked == true)
                    {
                        Sql.Append("     DENPYO_DATE ASC,");
                        Sql.Append("     DENPYO_BANGO ASC,");
                    }
                    else
                    {
                        Sql.Append("     DENPYO_BANGO ASC,");
                        Sql.Append("     DENPYO_DATE ASC,");
                    }
                    Sql.Append("     GYO_BANGO ASC");
                }
                else
                {
                    // 伝票番号からデータを取得
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append(" SELECT");
                    Sql.Append("     A.KAISHA_CD         AS KAISHA_CD,");
                    Sql.Append("     A.SHISHO_CD         AS SHISHO_CD,");
                    Sql.Append("     A.DENPYO_DATE       AS DENPYO_DATE,");
                    Sql.Append("     A.DENPYO_BANGO      AS DENPYO_BANGO,");
                    Sql.Append("     A.GYO_BANGO         AS GYO_BANGO,");
                    Sql.Append("     A.MEISAI_KUBUN      AS MEISAI_KUBUN,");
                    Sql.Append("     B.TANTOSHA_CD       AS TANTOSHA_CD,");
                    Sql.Append("     MAX(B.SHOHYO_BANGO) AS SHOHYO_BANGO,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1  THEN A.KANJO_KAMOKU_CD ELSE  0 END)  AS KARIKATA_KANJO_KAMOKU_CD,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN C.KANJO_KAMOKU_NM ELSE ' ' END)  AS KARIKATA_KANJO_KAMOKU_NM,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.HOJO_KAMOKU_CD ELSE  0 END)    AS KARIKATA_HOJO_KAMOKU_CD,");

                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN ");
                    Sql.Append("              (CASE WHEN C.HOJO_SHIYO_KUBUN=1 THEN D.HOJO_KAMOKU_NM ");
                    Sql.Append("                    WHEN C.HOJO_SHIYO_KUBUN=2 THEN G.TORIHIKISAKI_NM ELSE '' END) ");
                    Sql.Append("              ELSE ' ' END) AS KARIKATA_HOJO_KAMOKU_NM,");

                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.BUMON_CD ELSE  0 END)          AS KARIKATA_BUMON_CD,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN E.BUMON_NM ELSE ' ' END)         AS KARIKATA_BUMON_NM,");
                    Sql.Append("     SUM(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.ZEIKOMI_KINGAKU ELSE  0 END)   AS KARIKATA_ZEIKOMI_KINGAKU,");
                    Sql.Append("     SUM(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.SHOHIZEI_KINGAKU ELSE  0 END)  AS KARIKATA_SHOHIZEI_KINGAKU,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.ZEI_KUBUN ELSE  0 END)         AS KARIKATA_ZEI_KUBUN,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN F.ZEI_KUBUN_NM ELSE ' ' END)     AS KARIKATA_ZEI_KUBUN_NM,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.KAZEI_KUBUN ELSE  0 END)       AS KARIKATA_KAZEI_KUBUN,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.TORIHIKI_KUBUN ELSE  0 END)    AS KARIKATA_TORIHIKI_KUBUN,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.ZEI_RITSU ELSE 0 END)          AS KARIKATA_ZEI_RITSU,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN=1 THEN A.JIGYO_KUBUN ELSE  0 END)       AS KARIKATA_JIGYO_KUBUN,");
                    Sql.Append("     MAX(A.TEKIYO) AS TEKIYO,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.KANJO_KAMOKU_CD ELSE  0 END)  AS KASHIKATA_KANJO_KAMOKU_CD,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN C.KANJO_KAMOKU_NM ELSE ' ' END) AS KASHIKATA_KANJO_KAMOKU_NM,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.HOJO_KAMOKU_CD ELSE 0 END) AS KASHIKATA_HOJO_KAMOKU_CD,");

                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN ");
                    Sql.Append("              (CASE WHEN C.HOJO_SHIYO_KUBUN=1 THEN D.HOJO_KAMOKU_NM ");
                    Sql.Append("                    WHEN C.HOJO_SHIYO_KUBUN=2 THEN G.TORIHIKISAKI_NM ELSE '' END) ");
                    Sql.Append("              ELSE ' ' END) AS KASHIKATA_HOJO_KAMOKU_NM,");

                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.BUMON_CD ELSE 0 END)          AS KASHIKATA_BUMON_CD,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN E.BUMON_NM ELSE ' ' END)        AS KASHIKATA_BUMON_NM,");
                    //Sql.Append("     MAX(A.KOJI_CD) AS KOJI_CD,");
                    Sql.Append("     SUM(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.ZEIKOMI_KINGAKU ELSE 0 END)   AS KASHIKATA_ZEIKOMI_KINGAKU,");
                    Sql.Append("     SUM(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.SHOHIZEI_KINGAKU ELSE 0 END)  AS KASHIKATA_SHOHIZEI_KINGAKU,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.ZEI_KUBUN ELSE 0 END)         AS KASHIKATA_ZEI_KUBUN,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN F.ZEI_KUBUN_NM ELSE ' ' END)    AS KASHIKATA_ZEI_KUBUN_NM,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.KAZEI_KUBUN ELSE 0 END)       AS KASHIKATA_KAZEI_KUBUN,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.TORIHIKI_KUBUN ELSE 0 END)    AS KASHIKATA_TORIHIKI_KUBUN,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.ZEI_RITSU ELSE 0 END)         AS KASHIKATA_ZEI_RITSU,");
                    Sql.Append("     MAX(CASE WHEN A.TAISHAKU_KUBUN<>1 THEN A.JIGYO_KUBUN ELSE  0 END)      AS KASHIKATA_JIGYO_KUBUN");
                    Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS A ");
                    Sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_DENPYO AS B");

                    //Sql.Append("     ON (A.DENPYO_BANGO = B.DENPYO_BANGO) AND (A.KAISHA_CD = B.KAISHA_CD) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");
                    Sql.Append("     ON (A.KAISHA_CD = B.KAISHA_CD) AND (A.SHISHO_CD = B.SHISHO_CD) AND (A.DENPYO_BANGO = B.DENPYO_BANGO) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO)");

                    Sql.Append(" LEFT OUTER JOIN VI_ZM_KANJO_KAMOKU AS C");
                    Sql.Append("     ON (A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD) AND (A.KAISHA_CD = C.KAISHA_CD) AND (A.KAIKEI_NENDO = C.KAIKEI_NENDO)");

                    Sql.Append(" LEFT OUTER JOIN VI_ZM_HOJO_KAMOKU AS D");
                    //Sql.Append("     ON (A.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD) AND (A.HOJO_KAMOKU_CD = D.HOJO_KAMOKU_CD) AND (C.KAISHA_CD = D.KAISHA_CD) AND (A.KAIKEI_NENDO = D.KAIKEI_NENDO)");
                    Sql.Append("     ON (C.KAISHA_CD = D.KAISHA_CD) AND ((A.SHISHO_CD = D.SHISHO_CD or D.SHISHO_CD is null)) AND (A.KAIKEI_NENDO = D.KAIKEI_NENDO) AND (A.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD) AND (A.HOJO_KAMOKU_CD = D.HOJO_KAMOKU_CD)");

                    Sql.Append(" LEFT OUTER JOIN TB_CM_BUMON AS E");
                    Sql.Append("     ON (A.BUMON_CD = E.BUMON_CD) AND (A.KAISHA_CD = E.KAISHA_CD)");
                    Sql.Append(" LEFT OUTER JOIN VI_ZM_ZEI_KUBUN AS F");
                    Sql.Append("     ON (A.ZEI_KUBUN = F.ZEI_KUBUN)");
                    Sql.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS G");
                    Sql.Append("     ON (A.HOJO_KAMOKU_CD = G.TORIHIKISAKI_CD) AND (A.KAISHA_CD = G.KAISHA_CD)");
                    Sql.Append(" WHERE ");
                    Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND ");

                    //if (shishoCd != 0)
                        Sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");

                    Sql.Append("     A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
                    Sql.Append("     A.DENPYO_BANGO BETWEEN @DENPYO_BANGO_FR AND @DENPYO_BANGO_TO AND ");
                    Sql.Append("     B.TANTOSHA_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO AND");
                    Sql.Append("     A.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO AND");
                    Sql.Append("     A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND");
                    Sql.Append("     A.DENPYO_BANGO = @DENPYO_BANGO");
                    Sql.Append(" GROUP BY");
                    Sql.Append(" 	A.KAISHA_CD,");
                    Sql.Append("    A.SHISHO_CD,");
                    Sql.Append(" 	A.DENPYO_DATE,");
                    Sql.Append(" 	A.DENPYO_BANGO,");
                    Sql.Append(" 	A.GYO_BANGO,");
                    Sql.Append(" 	A.MEISAI_KUBUN,");
                    Sql.Append(" 	B.TANTOSHA_CD");
                    Sql.Append(" ORDER BY");
                    Sql.Append("     A.KAISHA_CD ASC,");
                    Sql.Append("     A.SHISHO_CD ASC,");
                    if (rdoDateJun.Checked == true)
                    {
                        Sql.Append("     A.DENPYO_DATE ASC,");
                        Sql.Append("     A.DENPYO_BANGO ASC,");
                    }
                    else
                    {
                        Sql.Append("     A.DENPYO_BANGO ASC,");
                        Sql.Append("     A.DENPYO_DATE ASC,");
                    }
                    Sql.Append("     A.GYO_BANGO ASC,");
                    Sql.Append("     A.MEISAI_KUBUN DESC");
                }
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, dr["DENPYO_BANGO"]);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, dr["SHISHO_CD"]);
                System.Diagnostics.Debug.Print("SHISHO_CD:" + Util.ToString(dr["SHISHO_CD"]));
                // 検索する日付
                dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
                dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                // 検索する伝票番号
                dpc.SetParam("@DENPYO_BANGO_FR", SqlDbType.Decimal, 6, denpyoFr);
                dpc.SetParam("@DENPYO_BANGO_TO", SqlDbType.Decimal, 6, denpyoTo);
                // 検索する担当者コード
                dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, tantoshaFr);
                dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, tantoshaTo);
                // 検索する勘定科目コード
                dpc.SetParam("@KANJO_KAMOKU_CD_FR", SqlDbType.Decimal, 6, kanjoKamokuFr);
                dpc.SetParam("@KANJO_KAMOKU_CD_TO", SqlDbType.Decimal, 6, kanjoKamokuTo);
                // 検索する部門
                dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonFr);
                dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonTo);

                DataTable dtMainLoop = null;
                try
                {
                    dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return false;
                }

                // ワークテーブルにデータ登録
                DataInsert(dtMainLoop);
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_ZM_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_ZM_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// データを元にワークテーブルに登録します。
        /// </summary>
        /// <param name="table">伝票番号毎のデータテーブル</param>
        private void DataInsert(DataTable table)
        {
            // 伝票名称設定
            string denpyoNm;
            if (rdoNyukinDenpyo.Checked == true)
            {
                denpyoNm = "入　　金　　伝　　票";
            }
            else
                if (rdoShukkinDenpyo.Checked == true)
                {
                    denpyoNm = "出　　金　　伝　　票";
                }
                else
                {
                    denpyoNm = "振　　替　　伝　　票";
                }
                if (rdoKamokuGokei.Checked == true)
                {
                    denpyoNm = "科　　目　　合　　計";
                }
            // SORTの最大値を取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            Sql.Append(" SELECT");
            Sql.Append("     MAX(SORT) AS SORT");
            Sql.Append(" FROM");
            Sql.Append("     PR_ZM_TBL");
            Sql.Append(" WHERE");
            Sql.Append("     GUID = @GUID");

            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

            DataTable dtMaxSort = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            Decimal maxSort; // SORT用変数
            if (dtMaxSort.Rows.Count == 0)
            {
                maxSort = 0;
            }
            else
            {
                maxSort = Util.ToDecimal(dtMaxSort.Rows[0]["SORT"]);
            }
            string dBango = ""; // 伝票番号
            int dBangoCt = 1; // 伝票のカウント
            int i = 1; // ループ用カウント変数
            int gyo = 1; // 行番号
            int kensu = table.Rows.Count; // データ件数
            int zeiKubunIchi = 3; // 税区分表示用変数
            Decimal kariKinShokei = 0; // 借方金額小計
            Decimal kariShoShokei = 0; // 借方消費税小計
            Decimal kariKinGokei = 0; // 借方金額合計
            Decimal kariShoGokei = 0; // 借方消費税合計
            Decimal kashiKinShokei = 0; // 貸方金額小計
            Decimal kashiShoShokei = 0; // 貸方消費税小計
            Decimal kashiKinGokei = 0; // 貸方金額合計
            Decimal kashiShoGokei = 0; // 貸方消費税合計

            ArrayList allData = new ArrayList(); // インサートデータを格納する変数

            foreach (DataRow dr in table.Rows)
            {
                // 明細区分が1の場合は表示しない
                if (rdoNyukinDenpyo.Checked == true && Util.ToString(dr["MEISAI_KUBUN"]) == "1")
                {
                    kensu--;
                    continue;
                }
                if (rdoShukkinDenpyo.Checked == true && Util.ToString(dr["MEISAI_KUBUN"]) == "1")
                {
                    kensu--;
                    continue;
                }
                if (rdoFurikaeDenpyo.Checked == true && Util.ToString(dr["MEISAI_KUBUN"]) == "1")
                {
                    kensu--;
                    continue;
                }

                // データを配列に追加
                allData.Add(Util.ToString(gyo));
                allData.Add(Util.ToString(dr["KARIKATA_KANJO_KAMOKU_CD"]));
                allData.Add(Util.ToString(dr["KARIKATA_KANJO_KAMOKU_NM"]));
                if (rdoKamokuGokei.Checked == true)
                {
                    allData.Add("");
                }
                else
                {
                    if ("0".Equals(Util.ToString(dr["KARIKATA_KANJO_KAMOKU_CD"])))
                    {
                        allData.Add("");
                    }
                    else
                    {
                        allData.Add(Util.ToString(dr["KARIKATA_ZEI_KUBUN"]));
                    }
                }
                allData.Add(Util.ToString(dr["KARIKATA_ZEIKOMI_KINGAKU"]));
                if (rdoKamokuGokei.Checked == true)
                {
                    allData.Add("");
                    allData.Add("");
                }
                else
                {
                    allData.Add(Util.ToString(dr["KARIKATA_HOJO_KAMOKU_CD"]));
                    allData.Add(Util.ToString(dr["KARIKATA_HOJO_KAMOKU_NM"]));
                }
                allData.Add("");
                allData.Add(Util.ToString(dr["KARIKATA_SHOHIZEI_KINGAKU"]));
                allData.Add(Util.ToString(dr["TEKIYO"]));
                allData.Add(Util.ToString(dr["KASHIKATA_KANJO_KAMOKU_CD"]));
                allData.Add(Util.ToString(dr["KASHIKATA_KANJO_KAMOKU_NM"]));
                if (rdoKamokuGokei.Checked == true)
                {
                    allData.Add("");
                }
                else
                {
                    if ("0".Equals(Util.ToString(dr["KASHIKATA_KANJO_KAMOKU_CD"])))
                    {
                        allData.Add("");
                    }
                    else
                    {
                        allData.Add(Util.ToString(dr["KASHIKATA_ZEI_KUBUN"]));
                    }
                }
                allData.Add(Util.ToString(dr["KASHIKATA_ZEIKOMI_KINGAKU"]));
                if (rdoKamokuGokei.Checked == true)
                {
                    allData.Add("");
                    allData.Add("");
                }
                else
                {
                    allData.Add(Util.ToString(dr["KASHIKATA_HOJO_KAMOKU_CD"]));
                    allData.Add(Util.ToString(dr["KASHIKATA_HOJO_KAMOKU_NM"]));
                }
                allData.Add("");
                allData.Add(Util.ToString(dr["KASHIKATA_SHOHIZEI_KINGAKU"]));

                // 小計,合計を加算
                kariKinShokei += Util.ToDecimal(dr["KARIKATA_ZEIKOMI_KINGAKU"]);
                kariShoShokei += Util.ToDecimal(dr["KARIKATA_SHOHIZEI_KINGAKU"]);
                kariKinGokei += Util.ToDecimal(dr["KARIKATA_ZEIKOMI_KINGAKU"]);
                kariShoGokei += Util.ToDecimal(dr["KARIKATA_SHOHIZEI_KINGAKU"]);
                kashiKinShokei += Util.ToDecimal(dr["KASHIKATA_ZEIKOMI_KINGAKU"]);
                kashiShoShokei += Util.ToDecimal(dr["KASHIKATA_SHOHIZEI_KINGAKU"]);
                kashiKinGokei += Util.ToDecimal(dr["KASHIKATA_ZEIKOMI_KINGAKU"]);
                kashiShoGokei += Util.ToDecimal(dr["KASHIKATA_SHOHIZEI_KINGAKU"]);

                // データが5件分貯まるか、最後の場合インサート処理
                if (i == kensu || i % 5 == 0)
                {
                    // 数値が0のものは表示しない
                    for (int l = 0; l < allData.Count; l++)
                    {
                        if (rdoKamokuGokei.Checked == true)
                        {
                            if (Util.ToString(allData[l]) == "0")
                            {
                                allData[l] = "";
                            }
                        }
                        else
                        {
                            // 税区分は0でも表示する
                            if (l.Equals(zeiKubunIchi))
                            {
                                zeiKubunIchi += 9;
                                continue;
                            }
                            if (Util.ToString(allData[l]) == "0")
                            {
                                allData[l] = "";
                            }
                        }
                    }

                    // インサート処理
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_ZM_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(" ,ITEM15");
                    Sql.Append(" ,ITEM16");
                    Sql.Append(" ,ITEM17");
                    Sql.Append(" ,ITEM18");
                    Sql.Append(" ,ITEM19");
                    Sql.Append(" ,ITEM20");
                    Sql.Append(" ,ITEM21");
                    Sql.Append(" ,ITEM22");
                    Sql.Append(" ,ITEM23");
                    Sql.Append(" ,ITEM24");
                    Sql.Append(" ,ITEM25");
                    Sql.Append(" ,ITEM26");
                    Sql.Append(" ,ITEM27");
                    Sql.Append(" ,ITEM28");
                    Sql.Append(" ,ITEM29");
                    Sql.Append(" ,ITEM30");
                    Sql.Append(" ,ITEM31");
                    Sql.Append(" ,ITEM32");
                    Sql.Append(" ,ITEM33");
                    Sql.Append(" ,ITEM34");
                    Sql.Append(" ,ITEM35");
                    Sql.Append(" ,ITEM36");
                    Sql.Append(" ,ITEM37");
                    Sql.Append(" ,ITEM38");
                    Sql.Append(" ,ITEM39");
                    Sql.Append(" ,ITEM40");
                    Sql.Append(" ,ITEM41");
                    Sql.Append(" ,ITEM42");
                    Sql.Append(" ,ITEM43");
                    Sql.Append(" ,ITEM44");
                    Sql.Append(" ,ITEM45");
                    Sql.Append(" ,ITEM46");
                    Sql.Append(" ,ITEM47");
                    Sql.Append(" ,ITEM48");
                    Sql.Append(" ,ITEM49");
                    Sql.Append(" ,ITEM50");
                    Sql.Append(" ,ITEM51");
                    Sql.Append(" ,ITEM52");
                    Sql.Append(" ,ITEM53");
                    Sql.Append(" ,ITEM54");
                    Sql.Append(" ,ITEM55");
                    Sql.Append(" ,ITEM56");
                    Sql.Append(" ,ITEM57");
                    Sql.Append(" ,ITEM58");
                    Sql.Append(" ,ITEM59");
                    Sql.Append(" ,ITEM60");
                    Sql.Append(" ,ITEM61");
                    Sql.Append(" ,ITEM62");
                    Sql.Append(" ,ITEM63");
                    Sql.Append(" ,ITEM64");
                    Sql.Append(" ,ITEM65");
                    Sql.Append(" ,ITEM66");
                    Sql.Append(" ,ITEM67");
                    Sql.Append(" ,ITEM68");
                    Sql.Append(" ,ITEM69");
                    Sql.Append(" ,ITEM70");
                    Sql.Append(" ,ITEM71");
                    Sql.Append(" ,ITEM72");
                    Sql.Append(" ,ITEM73");
                    Sql.Append(" ,ITEM74");
                    Sql.Append(" ,ITEM75");
                    Sql.Append(" ,ITEM76");
                    Sql.Append(" ,ITEM77");
                    Sql.Append(" ,ITEM78");
                    Sql.Append(" ,ITEM79");
                    Sql.Append(" ,ITEM80");
                    Sql.Append(" ,ITEM81");
                    Sql.Append(" ,ITEM82");
                    Sql.Append(" ,ITEM83");
                    Sql.Append(" ,ITEM84");
                    Sql.Append(" ,ITEM85");
                    Sql.Append(" ,ITEM86");
                    Sql.Append(" ,ITEM87");
                    Sql.Append(" ,ITEM88");
                    Sql.Append(" ,ITEM89");
                    Sql.Append(" ,ITEM90");
                    Sql.Append(" ,ITEM91");
                    Sql.Append(" ,ITEM92");
                    Sql.Append(" ,ITEM93");
                    Sql.Append(" ,ITEM94");
                    Sql.Append(" ,ITEM95");
                    Sql.Append(" ,ITEM96");
                    Sql.Append(" ,ITEM97");
                    Sql.Append(" ,ITEM98");
                    Sql.Append(" ,ITEM99");
                    Sql.Append(" ,ITEM100");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(" ,@ITEM15");
                    Sql.Append(" ,@ITEM16");
                    Sql.Append(" ,@ITEM17");
                    Sql.Append(" ,@ITEM18");
                    Sql.Append(" ,@ITEM19");
                    Sql.Append(" ,@ITEM20");
                    Sql.Append(" ,@ITEM21");
                    Sql.Append(" ,@ITEM22");
                    Sql.Append(" ,@ITEM23");
                    Sql.Append(" ,@ITEM24");
                    Sql.Append(" ,@ITEM25");
                    Sql.Append(" ,@ITEM26");
                    Sql.Append(" ,@ITEM27");
                    Sql.Append(" ,@ITEM28");
                    Sql.Append(" ,@ITEM29");
                    Sql.Append(" ,@ITEM30");
                    Sql.Append(" ,@ITEM31");
                    Sql.Append(" ,@ITEM32");
                    Sql.Append(" ,@ITEM33");
                    Sql.Append(" ,@ITEM34");
                    Sql.Append(" ,@ITEM35");
                    Sql.Append(" ,@ITEM36");
                    Sql.Append(" ,@ITEM37");
                    Sql.Append(" ,@ITEM38");
                    Sql.Append(" ,@ITEM39");
                    Sql.Append(" ,@ITEM40");
                    Sql.Append(" ,@ITEM41");
                    Sql.Append(" ,@ITEM42");
                    Sql.Append(" ,@ITEM43");
                    Sql.Append(" ,@ITEM44");
                    Sql.Append(" ,@ITEM45");
                    Sql.Append(" ,@ITEM46");
                    Sql.Append(" ,@ITEM47");
                    Sql.Append(" ,@ITEM48");
                    Sql.Append(" ,@ITEM49");
                    Sql.Append(" ,@ITEM50");
                    Sql.Append(" ,@ITEM51");
                    Sql.Append(" ,@ITEM52");
                    Sql.Append(" ,@ITEM53");
                    Sql.Append(" ,@ITEM54");
                    Sql.Append(" ,@ITEM55");
                    Sql.Append(" ,@ITEM56");
                    Sql.Append(" ,@ITEM57");
                    Sql.Append(" ,@ITEM58");
                    Sql.Append(" ,@ITEM59");
                    Sql.Append(" ,@ITEM60");
                    Sql.Append(" ,@ITEM61");
                    Sql.Append(" ,@ITEM62");
                    Sql.Append(" ,@ITEM63");
                    Sql.Append(" ,@ITEM64");
                    Sql.Append(" ,@ITEM65");
                    Sql.Append(" ,@ITEM66");
                    Sql.Append(" ,@ITEM67");
                    Sql.Append(" ,@ITEM68");
                    Sql.Append(" ,@ITEM69");
                    Sql.Append(" ,@ITEM70");
                    Sql.Append(" ,@ITEM71");
                    Sql.Append(" ,@ITEM72");
                    Sql.Append(" ,@ITEM73");
                    Sql.Append(" ,@ITEM74");
                    Sql.Append(" ,@ITEM75");
                    Sql.Append(" ,@ITEM76");
                    Sql.Append(" ,@ITEM77");
                    Sql.Append(" ,@ITEM78");
                    Sql.Append(" ,@ITEM79");
                    Sql.Append(" ,@ITEM80");
                    Sql.Append(" ,@ITEM81");
                    Sql.Append(" ,@ITEM82");
                    Sql.Append(" ,@ITEM83");
                    Sql.Append(" ,@ITEM84");
                    Sql.Append(" ,@ITEM85");
                    Sql.Append(" ,@ITEM86");
                    Sql.Append(" ,@ITEM87");
                    Sql.Append(" ,@ITEM88");
                    Sql.Append(" ,@ITEM89");
                    Sql.Append(" ,@ITEM90");
                    Sql.Append(" ,@ITEM91");
                    Sql.Append(" ,@ITEM92");
                    Sql.Append(" ,@ITEM93");
                    Sql.Append(" ,@ITEM94");
                    Sql.Append(" ,@ITEM95");
                    Sql.Append(" ,@ITEM96");
                    Sql.Append(" ,@ITEM97");
                    Sql.Append(" ,@ITEM98");
                    Sql.Append(" ,@ITEM99");
                    Sql.Append(" ,@ITEM100");
                    Sql.Append(") ");

                    // 日付の設定
                    string[] date = Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba);

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, maxSort + 1);
                    // データを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, denpyoNm);
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200,
                        string.Format("{0}  {1}  年  {2}  月  {3}  日", date[0], date[2], date[3], date[4]));
                    if (dBango == Util.ToString(table.Rows[0]["DENPYO_BANGO"]))
                    {
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, table.Rows[0]["DENPYO_BANGO"] + " - " + dBangoCt);
                    }
                    else
                    {
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, table.Rows[0]["DENPYO_BANGO"]);
                    }
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, allData[0]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, allData[1]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, allData[2]);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, allData[3]);
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, allData[4]);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, allData[5]);
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, allData[6]);
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, allData[7]);
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, allData[8]);
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, allData[9]);
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, allData[10]);
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, allData[11]);
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, allData[12]);
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, allData[13]);
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, allData[14]);
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, allData[15]);
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, allData[16]);
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, allData[17]);

                    if (kensu >= 2)
                    {
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, allData[18]);
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, allData[19]);
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, allData[20]);
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, allData[21]);
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, allData[22]);
                        dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, allData[23]);
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, allData[24]);
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, allData[25]);
                        dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, allData[26]);
                        dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, allData[27]);
                        dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, allData[28]);
                        dpc.SetParam("@ITEM34", SqlDbType.VarChar, 200, allData[29]);
                        dpc.SetParam("@ITEM35", SqlDbType.VarChar, 200, allData[30]);
                        dpc.SetParam("@ITEM36", SqlDbType.VarChar, 200, allData[31]);
                        dpc.SetParam("@ITEM37", SqlDbType.VarChar, 200, allData[32]);
                        dpc.SetParam("@ITEM38", SqlDbType.VarChar, 200, allData[33]);
                        dpc.SetParam("@ITEM39", SqlDbType.VarChar, 200, allData[34]);
                        dpc.SetParam("@ITEM40", SqlDbType.VarChar, 200, allData[35]);
                    }
                    else
                    {
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM34", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM35", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM36", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM37", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM38", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM39", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM40", SqlDbType.VarChar, 200, "");
                    }

                    if (kensu >= 3)
                    {
                        dpc.SetParam("@ITEM41", SqlDbType.VarChar, 200, allData[36]);
                        dpc.SetParam("@ITEM42", SqlDbType.VarChar, 200, allData[37]);
                        dpc.SetParam("@ITEM43", SqlDbType.VarChar, 200, allData[38]);
                        dpc.SetParam("@ITEM44", SqlDbType.VarChar, 200, allData[39]);
                        dpc.SetParam("@ITEM45", SqlDbType.VarChar, 200, allData[40]);
                        dpc.SetParam("@ITEM46", SqlDbType.VarChar, 200, allData[41]);
                        dpc.SetParam("@ITEM47", SqlDbType.VarChar, 200, allData[42]);
                        dpc.SetParam("@ITEM48", SqlDbType.VarChar, 200, allData[43]);
                        dpc.SetParam("@ITEM49", SqlDbType.VarChar, 200, allData[44]);
                        dpc.SetParam("@ITEM50", SqlDbType.VarChar, 200, allData[45]);
                        dpc.SetParam("@ITEM51", SqlDbType.VarChar, 200, allData[46]);
                        dpc.SetParam("@ITEM52", SqlDbType.VarChar, 200, allData[47]);
                        dpc.SetParam("@ITEM53", SqlDbType.VarChar, 200, allData[48]);
                        dpc.SetParam("@ITEM54", SqlDbType.VarChar, 200, allData[49]);
                        dpc.SetParam("@ITEM55", SqlDbType.VarChar, 200, allData[50]);
                        dpc.SetParam("@ITEM56", SqlDbType.VarChar, 200, allData[51]);
                        dpc.SetParam("@ITEM57", SqlDbType.VarChar, 200, allData[52]);
                        dpc.SetParam("@ITEM58", SqlDbType.VarChar, 200, allData[53]);
                    }
                    else
                    {
                        dpc.SetParam("@ITEM41", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM42", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM43", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM44", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM45", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM46", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM47", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM48", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM49", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM50", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM51", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM52", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM53", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM54", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM55", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM56", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM57", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM58", SqlDbType.VarChar, 200, "");
                    }

                    if (kensu >= 4)
                    {
                        dpc.SetParam("@ITEM59", SqlDbType.VarChar, 200, allData[54]);
                        dpc.SetParam("@ITEM60", SqlDbType.VarChar, 200, allData[55]);
                        dpc.SetParam("@ITEM61", SqlDbType.VarChar, 200, allData[56]);
                        dpc.SetParam("@ITEM62", SqlDbType.VarChar, 200, allData[57]);
                        dpc.SetParam("@ITEM63", SqlDbType.VarChar, 200, allData[58]);
                        dpc.SetParam("@ITEM64", SqlDbType.VarChar, 200, allData[59]);
                        dpc.SetParam("@ITEM65", SqlDbType.VarChar, 200, allData[60]);
                        dpc.SetParam("@ITEM66", SqlDbType.VarChar, 200, allData[61]);
                        dpc.SetParam("@ITEM67", SqlDbType.VarChar, 200, allData[62]);
                        dpc.SetParam("@ITEM68", SqlDbType.VarChar, 200, allData[63]);
                        dpc.SetParam("@ITEM69", SqlDbType.VarChar, 200, allData[64]);
                        dpc.SetParam("@ITEM70", SqlDbType.VarChar, 200, allData[65]);
                        dpc.SetParam("@ITEM71", SqlDbType.VarChar, 200, allData[66]);
                        dpc.SetParam("@ITEM72", SqlDbType.VarChar, 200, allData[67]);
                        dpc.SetParam("@ITEM73", SqlDbType.VarChar, 200, allData[68]);
                        dpc.SetParam("@ITEM74", SqlDbType.VarChar, 200, allData[69]);
                        dpc.SetParam("@ITEM75", SqlDbType.VarChar, 200, allData[70]);
                        dpc.SetParam("@ITEM76", SqlDbType.VarChar, 200, allData[71]);
                    }
                    else
                    {
                        dpc.SetParam("@ITEM59", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM60", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM61", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM62", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM63", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM64", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM65", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM66", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM67", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM68", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM69", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM70", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM71", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM72", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM73", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM74", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM75", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM76", SqlDbType.VarChar, 200, "");
                    }

                    if (kensu >= 5)
                    {
                        dpc.SetParam("@ITEM77", SqlDbType.VarChar, 200, allData[72]);
                        dpc.SetParam("@ITEM78", SqlDbType.VarChar, 200, allData[73]);
                        dpc.SetParam("@ITEM79", SqlDbType.VarChar, 200, allData[74]);
                        dpc.SetParam("@ITEM80", SqlDbType.VarChar, 200, allData[75]);
                        dpc.SetParam("@ITEM81", SqlDbType.VarChar, 200, allData[76]);
                        dpc.SetParam("@ITEM82", SqlDbType.VarChar, 200, allData[77]);
                        dpc.SetParam("@ITEM83", SqlDbType.VarChar, 200, allData[78]);
                        dpc.SetParam("@ITEM84", SqlDbType.VarChar, 200, allData[79]);
                        dpc.SetParam("@ITEM85", SqlDbType.VarChar, 200, allData[80]);
                        dpc.SetParam("@ITEM86", SqlDbType.VarChar, 200, allData[81]);
                        dpc.SetParam("@ITEM87", SqlDbType.VarChar, 200, allData[82]);
                        dpc.SetParam("@ITEM88", SqlDbType.VarChar, 200, allData[83]);
                        dpc.SetParam("@ITEM89", SqlDbType.VarChar, 200, allData[84]);
                        dpc.SetParam("@ITEM90", SqlDbType.VarChar, 200, allData[85]);
                        dpc.SetParam("@ITEM91", SqlDbType.VarChar, 200, allData[86]);
                        dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, allData[87]);
                        dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, allData[88]);
                        dpc.SetParam("@ITEM94", SqlDbType.VarChar, 200, allData[89]);
                    }
                    else
                    {
                        dpc.SetParam("@ITEM77", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM78", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM79", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM80", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM81", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM82", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM83", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM84", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM85", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM86", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM87", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM88", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM89", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM90", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM91", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM94", SqlDbType.VarChar, 200, "");
                    }
                    if (kensu <= 5)
                    {
                        dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM96", SqlDbType.VarChar, 200, "");
                        if (kariKinGokei == 0)
                        {
                            dpc.SetParam("@ITEM97", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM97", SqlDbType.VarChar, 200, kariKinGokei);
                        }
                        if (kariShoGokei == 0)
                        {
                            dpc.SetParam("@ITEM98", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM98", SqlDbType.VarChar, 200, kariShoGokei);
                        }
                        if (kashiKinGokei == 0)
                        {
                            dpc.SetParam("@ITEM99", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM99", SqlDbType.VarChar, 200, kashiKinGokei);
                        }
                        if (kashiShoGokei == 0)
                        {
                            dpc.SetParam("@ITEM100", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM100", SqlDbType.VarChar, 200, kashiShoGokei);
                        }
                    }
                    else
                    {
                        if (kariKinShokei == 0)
                        {
                            dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, kariKinShokei);
                        }
                        if (kashiKinShokei == 0)
                        {
                            dpc.SetParam("@ITEM96", SqlDbType.VarChar, 200, "");
                        }
                        else
                        {
                            dpc.SetParam("@ITEM96", SqlDbType.VarChar, 200, kashiKinShokei);
                        }
                        dpc.SetParam("@ITEM97", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM98", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM99", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM100", SqlDbType.VarChar, 200, "");
                    }
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    // 伝票番号の保持
                    dBango = Util.ToString(table.Rows[0]["DENPYO_BANGO"]);
                    dBangoCt++;
                    // インサート後の処理
                    maxSort++;
                    kensu = kensu - 5;
                    i = 0;
                    allData.Clear();
                }
                i++;
                gyo++;
            }
        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }

        /// <summary>
        /// 会計年度内日付に変換
        /// </summary>
        private DateTime FixNendoDate(DateTime date)
        {
            DateTime dateFr = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            DateTime dateTo = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }
        #endregion
    }
}
