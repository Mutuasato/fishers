﻿namespace jp.co.fsi.zm.zmdr1031
{
    partial class ZMDR1032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGenkin = new System.Windows.Forms.Label();
            this.lblKanjoKamoku = new System.Windows.Forms.Label();
            this.txtKanjoKamoku = new System.Windows.Forms.TextBox();
            this.lblKanjoKamokuResults = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 609);
            // 
            // lblTitle
            // 
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1097, 41);
            this.lblTitle.Text = "";
            // 
            // lblGenkin
            // 
            this.lblGenkin.BackColor = System.Drawing.Color.Silver;
            this.lblGenkin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGenkin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGenkin.Location = new System.Drawing.Point(0, 0);
            this.lblGenkin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGenkin.Name = "lblGenkin";
            this.lblGenkin.Size = new System.Drawing.Size(425, 37);
            this.lblGenkin.TabIndex = 2;
            this.lblGenkin.Tag = "CHANGE";
            this.lblGenkin.Text = "設定コード";
            this.lblGenkin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamoku
            // 
            this.lblKanjoKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamoku.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoKamoku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamoku.Name = "lblKanjoKamoku";
            this.lblKanjoKamoku.Size = new System.Drawing.Size(425, 37);
            this.lblKanjoKamoku.TabIndex = 1;
            this.lblKanjoKamoku.Tag = "CHANGE";
            this.lblKanjoKamoku.Text = "勘　定　科　目";
            this.lblKanjoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamoku
            // 
            this.txtKanjoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamoku.Location = new System.Drawing.Point(88, 7);
            this.txtKanjoKamoku.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKamoku.MaxLength = 6;
            this.txtKanjoKamoku.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtKanjoKamoku.Name = "txtKanjoKamoku";
            this.txtKanjoKamoku.Size = new System.Drawing.Size(69, 23);
            this.txtKanjoKamoku.TabIndex = 3;
            this.txtKanjoKamoku.Text = "123456";
            this.txtKanjoKamoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamoku.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanjoKamoku_KeyDown);
            this.txtKanjoKamoku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamoku_Validating);
            // 
            // lblKanjoKamokuResults
            // 
            this.lblKanjoKamokuResults.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuResults.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamokuResults.Location = new System.Drawing.Point(165, 6);
            this.lblKanjoKamokuResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuResults.Name = "lblKanjoKamokuResults";
            this.lblKanjoKamokuResults.Size = new System.Drawing.Size(251, 24);
            this.lblKanjoKamokuResults.TabIndex = 4;
            this.lblKanjoKamokuResults.Tag = "DISPNAME";
            this.lblKanjoKamokuResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 44);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(433, 89);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtKanjoKamoku);
            this.fsiPanel2.Controls.Add(this.lblKanjoKamokuResults);
            this.fsiPanel2.Controls.Add(this.lblGenkin);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 48);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(425, 37);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.lblKanjoKamoku);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(425, 37);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // ZMDR1032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1097, 745);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMDR1032";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblGenkin;
        private System.Windows.Forms.Label lblKanjoKamoku;
        private System.Windows.Forms.TextBox txtKanjoKamoku;
        private System.Windows.Forms.Label lblKanjoKamokuResults;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}