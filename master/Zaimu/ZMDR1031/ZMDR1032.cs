﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Drawing;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmdr1031
{
    /// <summary>
    /// 科目設定(ZMDR1032)
    /// </summary>
    public partial class ZMDR1032 : BasePgForm
    {
        #region 変数

        // 支所コード
        private int ShishoCode;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDR1032()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            // サイズを縮める
            this.Size = new Size(511, 330);
            // Escapeのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 初期表示
            InitDisp();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			// アセンブリのロード
			//asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
			asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
            // フォーム作成
            //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9011");
            t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = "2";
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] outData = (string[])frm.OutData;
                        this.txtKanjoKamoku.Text = outData[0];
                        this.lblKanjoKamokuResults.Text = outData[1];
                    }
                }
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (Msg.ConfYesNo("更新しますか？") == DialogResult.Yes)
            {
                //入力チェック
                KamokuCdChk();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 勘定科目の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamoku_Validating(object sender, CancelEventArgs e)
        {
            if (ValChk.IsEmpty(this.txtKanjoKamoku.Text))
            {
                this.lblKanjoKamokuResults.Text = "";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamoku.Text))
            {
                Msg.Notice("勘定科目は数値のみで入力してください。");
                this.txtKanjoKamoku.SelectAll();
                e.Cancel = true;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.ShishoCode.ToString(), this.txtKanjoKamoku.Text);

                if (!ValChk.IsEmpty(name))
                {
                    this.lblKanjoKamokuResults.Text = name;
                }
                else
                {
                    this.lblKanjoKamokuResults.Text = "";
                }
            }
        }

        private void txtKanjoKamoku_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!ValChk.IsNumber(this.txtKanjoKamoku.Text))
                {
                    Msg.Notice("勘定科目は数値のみで入力してください。");
                    this.txtKanjoKamoku.SelectAll();
                    return;
                }
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                if (!ValChk.IsEmpty(this.txtKanjoKamoku.Text))
                {
                    string name = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.ShishoCode.ToString(), this.txtKanjoKamoku.Text);
                    this.lblKanjoKamokuResults.Text = name;

                    if (ValChk.IsEmpty(name))
                    {
                        this.lblKanjoKamokuResults.Text = "";

                        Msg.Notice("入力に誤りがあります。");
                        this.txtKanjoKamoku.Focus();
                        this.txtKanjoKamoku.SelectAll();
                        return;
                    }
                }
                else
                {
                    Msg.Notice("コードを入力してください。");
                    this.lblKanjoKamokuResults.Text = "";
                    this.txtKanjoKamoku.Focus();
                    return;
                }

                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 初期情報の表示
        /// </summary>
        private void InitDisp()
        {
            this.txtKanjoKamoku.Text = this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1031", "Setting", "KamokuCd");
            this.lblKanjoKamokuResults.Text = this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDR1031", "Setting", "KamokuNm");
        }

        /// <summary>
        /// 勘定科目コードのチェック
        /// </summary>
        private void KamokuCdChk()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamoku.Text))
            {
                Msg.Notice("勘定科目は数値のみで入力してください。");
                this.txtKanjoKamoku.SelectAll();
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtKanjoKamoku.Text))
            {
                string name = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.ShishoCode.ToString(), this.txtKanjoKamoku.Text);
                this.lblKanjoKamokuResults.Text = name;

                if (!ValChk.IsEmpty(name))
                {
                    this.Config.SetPgConfig(Constants.SubSys.Zai, "ZMDR1031", "Setting", "KamokuCd", this.txtKanjoKamoku.Text);
                    this.Config.SetPgConfig(Constants.SubSys.Zai, "ZMDR1031", "Setting", "KamokuNm", name);

                    // 設定を保存する
                    this.Config.SaveConfig();
                    Msg.Info("更新しました。");
                    this.Close();
                }
                else
                {
                    this.lblKanjoKamokuResults.Text = "";

                    Msg.Notice("入力に誤りがあります。");
                    this.txtKanjoKamoku.Focus();
                    this.txtKanjoKamoku.SelectAll();
                    return;
                }
            }
            else
            {
                Msg.Notice("コードを入力してください。");
                this.lblKanjoKamokuResults.Text = "";
                this.txtKanjoKamoku.Focus();
                return;
            }

        }
        #endregion
    }
}
