﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.zm.zmmr1071
{
    /// <summary>
    /// ZMMR107111R の帳票
    /// </summary>
    public partial class ZMMR107111R : BaseReport
    {

        public ZMMR107111R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            ////西暦から和暦に変換
            //CultureInfo culture = new CultureInfo("ja-JP", true);
            //culture.DateTimeFormat.Calendar = new JapaneseCalendar();
            //txtToday.Text = DateTime.Now.ToString("ggyy年M月d日", culture);
        }

        private void detail_Format(object sender, EventArgs e)
        {
            if (textBox8.Text == "1")
            {
                shape1.Visible = true;
            }
            else
            {
                shape1.Visible = false;
            }
        }
    }
}
