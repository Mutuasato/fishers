﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System;
using System.Collections;

namespace jp.co.fsi.zm.zmyb1011
{
    /// <summary>
    /// 年次繰越(ZMYB1011)
    /// </summary>
    public partial class ZMYB1012 : BasePgForm
    {
        #region 変数

        private bool _overWriteFlag;
        private DataTable _dtTableCopySetting;
        private DataTable _dtTableCopySettingCopy;

        #endregion

        #region プロパティ

        /// <summary>
        /// 上書きフラグ
        /// </summary>
        public bool OverWriteFlag
        {
            get
            {
                return _overWriteFlag;
            }
            set
            {
                _overWriteFlag = value;
            }
        }

        /// <summary>
        /// テーブル設定情報
        /// </summary>
        public DataTable DtTableCopySetting
        {
            get
            {
                return _dtTableCopySetting;
            }
            set
            {
                _dtTableCopySetting = value;
            }
        }

        public DataTable DtTableCopySettingCopy
        {
            get
            {
                return _dtTableCopySettingCopy;
            }
            set
            {
                _dtTableCopySettingCopy = value;
            }
        }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMYB1012()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            //// サイズを縮める
            this.Size = new Size(641, 688);
            // ESC F1 F6のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.ShowFButton = true;

            // 初期表示
            if (this.OverWriteFlag)
            {
                this.rdoOpt1.Checked = true;
            }
            else
            {
                this.rdoOpt2.Checked = true;
            }
            //一時退避
            this.DtTableCopySettingCopy = DtTableCopySetting.Copy();

            DispData();
            this.label1.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            //編集前の値に戻す
            for (int i = 0; i < this.DtTableCopySettingCopy.Rows.Count; i++)
            {
                this.DtTableCopySetting.Rows[i][0] = this.DtTableCopySettingCopy.Rows[i][0];
                this.DtTableCopySetting.Rows[i][1] = this.DtTableCopySettingCopy.Rows[i][1];
                this.DtTableCopySetting.Rows[i][2] = this.DtTableCopySettingCopy.Rows[i][2];
            }
            // ダイアログとしての処理結果を返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            if (Msg.ConfYesNo("更新しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 設定内容をセット
            this.OverWriteFlag = (this.rdoOpt1.Checked ? true : false);
            this.DtTableCopySetting = (DataTable)this.dgvList.DataSource;

            // ダイアログとしての処理結果を返却する
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// 設定データを表示する
        /// </summary>
        private void DispData()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // フォームの配置を上へ移動する
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(12, 30);

            // グリッドへデータソース設定
            this.dgvList.DataSource = this.DtTableCopySetting;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvList.AlternatingRowsDefaultCellStyle.BackColor = Color.Aquamarine;

            // 編集設定
            this.dgvList.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Off; 
            this.dgvList.ReadOnly = false;
            this.dgvList.EditMode = DataGridViewEditMode.EditOnEnter;
            this.dgvList.Columns[0].ReadOnly = true;
            this.dgvList.Columns[2].ReadOnly = true;

            // 列設定
            this.dgvList.Columns[0].HeaderText = "NO";
            this.dgvList.Columns[0].Width = 40;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            this.dgvList.Columns[1].HeaderText = "＊";
            this.dgvList.Columns[1].Width = 40;
            this.dgvList.Columns[2].HeaderText = "テーブル名";
            this.dgvList.Columns[2].Width = 280;
        }

        #endregion

    }
}
