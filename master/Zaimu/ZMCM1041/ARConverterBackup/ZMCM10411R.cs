﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1041
{
    /// <summary>
    /// ZMCM10411R の概要の説明です。
    /// </summary>
    public partial class ZMCM10411R : BaseReport
    {

        public ZMCM10411R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }
    }
}
