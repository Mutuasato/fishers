﻿namespace jp.co.fsi.zm.zmcm1041
{
    partial class ZMCM1045
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanjoCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtKanjoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoCdFr = new System.Windows.Forms.Label();
            this.txtKanjoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 99);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(800, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(789, 31);
            this.lblTitle.Text = "補助科目の印刷";
            this.lblTitle.UseWaitCursor = true;
            this.lblTitle.Visible = false;
            // 
            // lblKanjoCdTo
            // 
            this.lblKanjoCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoCdTo.Location = new System.Drawing.Point(469, 19);
            this.lblKanjoCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoCdTo.Name = "lblKanjoCdTo";
            this.lblKanjoCdTo.Size = new System.Drawing.Size(289, 24);
            this.lblKanjoCdTo.TabIndex = 4;
            this.lblKanjoCdTo.Tag = "DISPNAME";
            this.lblKanjoCdTo.Text = "最　後";
            this.lblKanjoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(373, 20);
            this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(20, 24);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Tag = "CHANGE";
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoCdFr
            // 
            this.txtKanjoCdFr.AutoSizeFromLength = false;
            this.txtKanjoCdFr.DisplayLength = null;
            this.txtKanjoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoCdFr.Location = new System.Drawing.Point(3, 21);
            this.txtKanjoCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoCdFr.MaxLength = 6;
            this.txtKanjoCdFr.Name = "txtKanjoCdFr";
            this.txtKanjoCdFr.Size = new System.Drawing.Size(65, 23);
            this.txtKanjoCdFr.TabIndex = 2;
            this.txtKanjoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoCdFr_Validating);
            // 
            // lblKanjoCdFr
            // 
            this.lblKanjoCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblKanjoCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoCdFr.Name = "lblKanjoCdFr";
            this.lblKanjoCdFr.Size = new System.Drawing.Size(289, 24);
            this.lblKanjoCdFr.TabIndex = 1;
            this.lblKanjoCdFr.Tag = "DISPNAME";
            this.lblKanjoCdFr.Text = "先　頭";
            this.lblKanjoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoCdTo
            // 
            this.txtKanjoCdTo.AutoSizeFromLength = false;
            this.txtKanjoCdTo.DisplayLength = null;
            this.txtKanjoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoCdTo.Location = new System.Drawing.Point(401, 20);
            this.txtKanjoCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoCdTo.MaxLength = 6;
            this.txtKanjoCdTo.Name = "txtKanjoCdTo";
            this.txtKanjoCdTo.Size = new System.Drawing.Size(65, 23);
            this.txtKanjoCdTo.TabIndex = 3;
            this.txtKanjoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanjoCdTo_KeyDown);
            this.txtKanjoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoCdTo_Validating);
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 1;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(777, 59);
            this.fsiTableLayoutPanel1.TabIndex = 905;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblKanjoCdTo);
            this.fsiPanel2.Controls.Add(this.txtKanjoCdFr);
            this.fsiPanel2.Controls.Add(this.lblCodeBet);
            this.fsiPanel2.Controls.Add(this.txtKanjoCdTo);
            this.fsiPanel2.Controls.Add(this.lblKanjoCdFr);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(769, 51);
            this.fsiPanel2.TabIndex = 0;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(769, 51);
            this.label2.TabIndex = 2;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "勘定科目コード範囲";
            // 
            // ZMCM1045
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 237);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1045";
            this.ShowFButton = true;
            this.Text = "補助科目の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblKanjoCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoCdFr;
        private System.Windows.Forms.Label lblKanjoCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoCdTo;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label2;
    }
}