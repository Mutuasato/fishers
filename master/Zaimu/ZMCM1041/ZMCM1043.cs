﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1041
{
    /// <summary>
    /// 取引先の登録(ZMCM1043)
    /// </summary>
    public partial class ZMCM1043 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1043()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public ZMCM1043(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：取引先コード
            
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
                this.btnF1.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
                this.btnF1.Enabled = false;
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 補助科目からの取引先マスタの削除は不可としておく
            this.btnF3.Enabled = false;

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        
        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
                return;

            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;
            Array indata = (Array)InData;
            // 削除する前に再度コードの存在チェック
            if (IsCodeUsing())
            {
                Msg.Error("取引先コードが使用されているため、削除できません。");
                return;
            }
            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }
            // 削除処理
            try
            {
                // データ削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
                dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtToriSkCd.Text);
                this.Dba.Delete("TB_CM_TORIHIKISAKI", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dpc);

                // 補助科目マスタも削除
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
                dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtToriSkCd.Text);
                this.Dba.Delete("TB_ZM_HOJO_KAMOKU",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD ",
                    dpc);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled)
                return;

            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                this.txtFax.Focus();
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmtori = SetCmToriParams();
            ArrayList alParamsZmHojo = SetZmHojoParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    if (IsCodeExists())
                    {
                        Msg.Error("取引先情報が登録済みです。");
                        return;
                    }

                    // データ登録
                    // 取引先
                    this.Dba.Insert("TB_CM_TORIHIKISAKI", (DbParamCollection)alParamsCmtori[0]);

                    // 補助科目マスタ
                    this.Dba.Insert("TB_ZM_HOJO_KAMOKU", (DbParamCollection)alParamsZmHojo[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 取引先
                    this.Dba.Update("TB_CM_TORIHIKISAKI",
                        (DbParamCollection)alParamsCmtori[1],
                        "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                        (DbParamCollection)alParamsCmtori[0]);

                    // 補助科目マスタ
                    // TODO:会計年度だけのデータを更新で本当に良いものか
                    this.Dba.Update("TB_ZM_HOJO_KAMOKU",
                        (DbParamCollection)alParamsZmHojo[1],
                        "KAISHA_CD = @KAISHA_CD"
                        + " AND SHISHO_CD = @SHISHO_CD"
                        + " AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD"
                        + " AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD"
                        + " AND KAIKEI_NENDO = @KAIKEI_NENDO",
                        (DbParamCollection)alParamsZmHojo[0]);
                }
                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 取引先コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtToriSk_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidToriCd())
            {
                e.Cancel = true;
                this.txtToriSkCd.SelectAll();
            }
        }

        /// <summary>
        /// FAX番号Enter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 取引先コードの初期値を取得
            // 取引先の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToString(this.InData));
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");

            DataTable dtMaxTori =
                this.Dba.GetDataTableByConditionWithParams("MAX(TORIHIKISAKI_CD) AS MAX_CD",
                    "TB_CM_TORIHIKISAKI", Util.ToString(where), dpc);
            if (dtMaxTori.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxTori.Rows[0]["MAX_CD"]))
            {
                this.txtToriSkCd.Text = Util.ToString(Util.ToInt(dtMaxTori.Rows[0]["MAX_CD"]) + 1);
                if (this.txtToriSkCd.Text == "10000")
                {
                    StringBuilder where2 = new StringBuilder();
                    where2.Append("KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD < 9999");
                    dtMaxTori =
                        this.Dba.GetDataTableByConditionWithParams("MAX(TORIHIKISAKI_CD) AS MAX_CD",
                            "TB_CM_TORIHIKISAKI", Util.ToString(where2), dpc);
                    if (dtMaxTori.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxTori.Rows[0]["MAX_CD"]))
                    {
                        this.txtToriSkCd.Text = Util.ToString(Util.ToInt(dtMaxTori.Rows[0]["MAX_CD"]) + 1);
                    }
                    else
                    {
                        this.txtToriSkCd.Text = "2";
                    }
                }
            }
            else
            {
                this.txtToriSkCd.Text = "2";
            }
            this.txtKanjoKamokuCd.Text = Util.ToString(this.InData);
            this.lblKanjoK.Text = " 勘定科目 ";
            this.lblKanjoKamokuCd.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKanjoKamokuCd.Text);
            this.txtKanjoKamokuCd.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            Array indata = (Array)InData;
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append(" KAISHA_CD,");
            cols.Append(" TORIHIKISAKI_CD,");
            cols.Append(" TORIHIKISAKI_NM,");
            cols.Append(" TORIHIKISAKI_KANA_NM,");
            cols.Append(" YUBIN_BANGO1,");
            cols.Append(" YUBIN_BANGO2,");
            cols.Append(" JUSHO1,");
            cols.Append(" JUSHO2,");
            cols.Append(" DENWA_BANGO,");
            cols.Append(" FAX_BANGO");

            StringBuilder from = new StringBuilder();
            from.Append(" TB_CM_TORIHIKISAKI ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, Util.ToString(indata.GetValue(1)));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    " KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD ",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtToriSkCd.Text = Util.ToString(drDispData["TORIHIKISAKI_CD"]);
            this.txtToriSkNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);
            this.txtToriSkKanaNm.Text = Util.ToString(drDispData["TORIHIKISAKI_KANA_NM"]);
            this.txtYubin1.Text = Util.ToString(drDispData["YUBIN_BANGO1"]);
            this.txtYubin2.Text = Util.ToString(drDispData["YUBIN_BANGO2"]);
            this.txtJusho1.Text = Util.ToString(drDispData["JUSHO1"]);
            this.txtJusho2.Text = Util.ToString(drDispData["JUSHO2"]);
            this.txtDenwa.Text = Util.ToString(drDispData["DENWA_BANGO"]);
            this.txtFax.Text = Util.ToString(drDispData["FAX_BANGO"]);
            this.lblKanjoK.Text = " 勘定科目 ";
            this.txtKanjoKamokuCd.Text = Util.ToString(indata.GetValue(0));
            this.lblKanjoKamokuCd.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKanjoKamokuCd.Text);
            
            //　取引先コード・勘定科目コード入力不可　
            this.txtToriSkCd.Enabled = false;
            this.txtKanjoKamokuCd.Enabled = false;

            if (IsCodeUsing())
            {
                // 使用されているので削除不可
                this.btnF3.Enabled = false;
            }
            else
            {
                // 使用されていないので削除OK
                this.btnF3.Enabled = true;
            }
        }

        /// <summary>
        /// 取引先コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidToriCd()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtToriSkCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtToriSkCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtToriSkCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = 1");
            where.Append(" AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD");
            DataTable dtkanjo =
                this.Dba.GetDataTableByConditionWithParams("TORIHIKISAKI_CD",
                    "TB_CM_TORIHIKISAKI", Util.ToString(where), dpc);
            if (dtkanjo.Rows.Count > 0)
            {
                Msg.Error("既に存在する取引先コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 取引先名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidToriNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtToriSkNm.Text, this.txtToriSkNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 取引先カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidToriKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtToriSkKanaNm.Text, this.txtToriSkKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便１の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubin()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubin1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 郵便２の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinn()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubin2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 住所１の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho1()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho1.Text, this.txtToriSkKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所２の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho2()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho2.Text, this.txtToriSkKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 電話番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDenwa()
        {
            //// 数字のみの入力を許可
            //if (!ValChk.IsNumber(this.txtDenwa.Text))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}
            if (!string.IsNullOrEmpty(this.txtDenwa.Text))
            {
                if (!IsPhoneNumber(this.txtDenwa.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// FAX番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFax()
        {
            //// 数字のみの入力を許可
            //if (!ValChk.IsNumber(this.txtFax.Text))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}
            if (!string.IsNullOrEmpty(this.txtFax.Text))
            {
                if (!IsPhoneNumber(this.txtFax.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }


        /// <summary>
        /// 番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsPhoneNumber(string number)
        {
            return Regex.IsMatch(number, @"^0\d{1,4}-\d{1,4}-\d{4}$");
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 取引先コードのチェック
                if (!IsValidToriCd())
                {
                    this.txtToriSkCd.Focus();
                    return false;
                }
            }

            // 取引先名のチェック
            if (!IsValidToriNm())
            {
                this.txtToriSkNm.Focus();
                return false;
            }

            // 取引先カナ名のチェック
            if (!IsValidToriKanaNm())
            {
                this.txtToriSkKanaNm.Focus();
                return false;
            }

            // 郵便1のチェック
            if (!IsValidYubin())
            {
                this.txtYubin1.Focus();
                return false;
            }

            // 郵便2のチェック
            if (!IsValidYubinn())
            {
                this.txtYubin2.Focus();
                return false;
            }

            // 住所1のチェック
            if (!IsValidJusho1())
            {
                this.txtJusho1.Focus();
                return false;
            }

            // 住所2のチェック
            if (!IsValidJusho2())
            {
                this.txtJusho2.Focus();
                return false;
            }

            // 電話番号のチェック
            if (!IsValidDenwa())
            {
                this.txtDenwa.Focus();
                return false;
            }

            // FAX番号のチェック
            if (!IsValidFax())
            {
                this.txtFax.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_CM_TORIHIKISAKIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと取引先コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                updParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtToriSkCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと取引先コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtToriSkCd.Text);
                alParams.Add(whereParam);
            }
            // 取引先名
            updParam.SetParam("@TORIHIKISAKI_NM", SqlDbType.VarChar, 40, this.txtToriSkNm.Text);
            // 取引先カナ名
            updParam.SetParam("@TORIHIKISAKI_KANA_NM", SqlDbType.VarChar, 30, this.txtToriSkKanaNm.Text);
            // 郵便1
            updParam.SetParam("@YUBIN_BANGO1", SqlDbType.VarChar, 3, this.txtYubin1.Text);
            // 郵便2
            updParam.SetParam("@YUBIN_BANGO2", SqlDbType.VarChar, 4, this.txtYubin2.Text);
            // 住所1
            updParam.SetParam("@JUSHO1", SqlDbType.VarChar, 30, this.txtJusho1.Text);
            // 住所2
            updParam.SetParam("@JUSHO2", SqlDbType.VarChar, 30, this.txtJusho2.Text);
            // 電話番号
            updParam.SetParam("@DENWA_BANGO", SqlDbType.VarChar, 15, this.txtDenwa.Text);
            // FAX番号
            updParam.SetParam("@FAX_BANGO", SqlDbType.VarChar, 15, this.txtFax.Text);
            // 締日
            updParam.SetParam("@SIMEBI", SqlDbType.Decimal, 4, 99);
            // 請求書発行
            updParam.SetParam("@SEIKYUSHO_HAKKO", SqlDbType.Decimal, 3, 1);
            // 請求書形式
            updParam.SetParam("@SEIKYUSHO_KEISHIKI", SqlDbType.Decimal, 3, 1);
            // 消費税端数処理
            updParam.SetParam("@SHOHIZEI_HASU_SHORI", SqlDbType.Decimal, 3, 2);
             //更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_HOJO_KAMOKUに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmHojoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと勘定科目コードと取引先コードと会計年度を更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
                updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
                updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtToriSkCd.Text);
                updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと勘定科目コードと取引先コードと会計年度をWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
                whereParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
                whereParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtToriSkCd.Text);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                alParams.Add(whereParam);
            }
            // 補助科目名
            updParam.SetParam("@HOJO_KAMOKU_NM", SqlDbType.VarChar, 40, this.txtToriSkNm.Text);
            // 補助科目カナ名
            updParam.SetParam("@HOJO_KAMOKU_KANA_NM", SqlDbType.VarChar, 30, this.txtToriSkKanaNm.Text);
            //更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 対象の取引先コードがマスタ登録されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeExists()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtToriSkCd.Text);
            DataTable dtCmTorihikisaki = this.Dba.GetDataTableByConditionWithParams("*", "TB_CM_TORIHIKISAKI", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dpc);

            if (dtCmTorihikisaki.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 対象の勘定科目コードがトランテーブルで使用されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeUsing()
        {
            Array indata = (Array)InData;
            // 削除可否チェック
            // 指定した取引先コードが使用されているかどうかをチェック
            // 仕訳明細
            StringBuilder from = new StringBuilder();
            from.Append(" TB_ZM_SHIWAKE_MEISAI AS A");
            from.Append(" LEFT OUTER JOIN");
            from.Append(" TB_ZM_KANJO_KAMOKU AS B");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            from.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            from.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");

            StringBuilder where = new StringBuilder();
            where.Append(" A.KAISHA_CD = @KAISHA_CD");
            where.Append(" AND A.SHISHO_CD = @SHISHO_CD");
            where.Append(" AND A.HOJO_KAMOKU_CD = @TORIHIKISAKI_CD");
            where.Append(" AND B.HOJO_SHIYO_KUBUN = 2");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, Util.ToString(indata.GetValue(1)));

            DataTable dtSwkMsi = this.Dba.GetDataTableByConditionWithParams("A.KANJO_KAMOKU_CD", from.ToString(), where.ToString(), dpc);

            if (dtSwkMsi.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
		#endregion

		private void lblToriSkCd_Click(object sender, EventArgs e)
		{

		}

		private void txtToriSkCd_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtKanjoKamokuCd_TextChanged(object sender, EventArgs e)
		{

		}

		private void lblKanjoKamokuCd_Click(object sender, EventArgs e)
		{

		}
	}
}
        