﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;
using jp.co.fsi.common.forms;

namespace jp.co.fsi.zm.zmmr1051
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1051PR
    {
        #region private変数
        /// <summary>
        /// ZAMR1051(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMMR1051 _pForm;

        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// ユニークID
        /// </summary>
        string _unqId;

        /// <summary>
        /// 部門情報管理用
        /// </summary>
        class BumonRecord
        {
            public string KamokuNm { get; set; }
            public int BumonCd { get; set; }
            public string BumonNm { get; set; }
            public decimal MTotak { get; set; }
            public decimal LTotak { get; set; }
            public decimal KHi { get; set; }
            public decimal BHi { get; set; }
        }
        #endregion

        #region プロパティ
        /// <summary>
        /// 会計期間開始日
        /// </summary>
        private DateTime _dtKikanKaishibi;
        public DateTime kikanKaishibi
        {
            get
            {
                return this._dtKikanKaishibi;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMMR1051PR(UserInfo uInfo, DbAccess dba, ConfigLoader config, string unuqId, ZMMR1051 frm)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
            this._unqId = unuqId;
            this._pForm = frm;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        public bool DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            if (!DispSetKikan())
            {
                return false;
            }

            bool dataFlag;
            ZMMR1054 msgFrm = new ZMMR1054();
            try
            {
                // 処理中メッセージ表示
                msgFrm.Show();
                msgFrm.Refresh();

                this._dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    #region public 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.AppendLine("  ITEM01");
                    cols.AppendLine(" ,ITEM02");
                    cols.AppendLine(" ,ITEM03");
                    cols.AppendLine(" ,ITEM04");
                    cols.AppendLine(" ,ITEM05");
                    cols.AppendLine(" ,ITEM06");
                    cols.AppendLine(" ,ITEM07");
                    cols.AppendLine(" ,ITEM08");
                    cols.AppendLine(" ,ITEM09");
                    cols.AppendLine(" ,ITEM10");
                    cols.AppendLine(" ,ITEM11");
                    cols.AppendLine(" ,ITEM12");
                    cols.AppendLine(" ,ITEM13");
                    cols.AppendLine(" ,ITEM14");
                    cols.AppendLine(" ,ITEM15");
                    cols.AppendLine(" ,ITEM16");
                    cols.AppendLine(" ,ITEM17");
                    cols.AppendLine(" ,ITEM18");
                    cols.AppendLine(" ,ITEM19");
                    cols.AppendLine(" ,ITEM20");
                    cols.AppendLine(" ,ITEM21");
                    cols.AppendLine(" ,ITEM22");
                    cols.AppendLine(" ,ITEM23");
                    cols.AppendLine(" ,ITEM24");
                    cols.AppendLine(" ,ITEM25");
                    cols.AppendLine(" ,ITEM26");
                    cols.AppendLine(" ,ITEM27");
                    cols.AppendLine(" ,ITEM28");
                    cols.AppendLine(" ,ITEM29");
                    #endregion

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);

                    // データの取得
                    DataTable dtOutput = this._dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    if (dtOutput.Rows.Count == 0)
                    {
                        Msg.Info("該当データがありません。");
                    }
                    else
                    {
                        // 帳票オブジェクトをインスタンス化
                        ZMMR10511R rpt = new ZMMR10511R(dtOutput);

                        rpt.Document.Printer.DocumentName = Util.ToString(this._pForm.Condition["ReportName"]);
                        rpt.Document.Name = Util.ToString(this._pForm.Condition["ReportName"]);

                        if (isExcel)
                        {
                            GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                            //SetExcelSetting(xlsExport1);
                            rpt.Run();
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                xlsExport1.Export(rpt.Document, saveFileName);
                                Msg.InfoNm("EXCEL出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                        else if (isPdf)
                        {
                            GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                            rpt.Run();
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                p.Export(rpt.Document, saveFileName);
                                Msg.InfoNm("PDF出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                        else if (isPreview)
                        {
                            // プレビュー画面表示
                            PreviewForm pFrm = new PreviewForm(rpt, this._unqId);
                            pFrm.WindowState = FormWindowState.Maximized;
                            pFrm.Show();
                        }
                        else
                        {
                            // 直接印刷
                            rpt.Run(false);
                            rpt.Document.Print(true, true, false);
                        }
                    }
                }
            }
            finally
            {
                this._dba.Rollback();

                msgFrm.Close();
            }
            if (dataFlag)
                return true;
            else
                return false;
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 保持用データをセット
        /// </summary>
        private Boolean DispSetKikan()
        {
            // 該当会計年度の会計期間開始日を取得
            DataTable dt = Util.GetKaikeiKikan(this._uInfo.KaikeiNendo, this._dba);
   
            if (dt.Rows.Count == 0)
            {
                Msg.Info("該当会計年度の情報が取得できませんでした。");
                return false;
            }

            this._dtKikanKaishibi = Util.ToDate(dt.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]);

            return true;
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 期間
            string kikan = "自 ";
            string[] aryJpDateFr = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtFr"]), this._dba);
            kikan += aryJpDateFr[5];
            string[] aryJpDateTo = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtTo"]), this._dba);
            kikan += " 至 " + aryJpDateTo[5];

            // 出力日付
            string outPutDate = "出力日付 " + Util.ToString(this._pForm.Condition["ShurutyokuDt"]);

            // 部門範囲を表示 ※開始と終了が未設定の場合は、【全社】
            string bumonRange;
            string bumonNmFr = Util.ToString(this._pForm.Condition["BumonNmFr"]);
            string bumonNmTo = Util.ToString(this._pForm.Condition["BumonNmTo"]);
            if (bumonNmFr == "先　頭" && bumonNmTo == "最　後")
            {
                bumonRange = "【全社】";
            }
            else
            {
                bumonRange = bumonNmFr + "　～　" + bumonNmTo;
            }

            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            int dbSORT = 0;

            // 部門データの作成
            ZMMR1051DA da = new ZMMR1051DA(this._uInfo, this._dba, this._config);
            DataTable wdt = da.CreateData(this._pForm.Condition, this._unqId);
            // 取得できない場合はエラー
            if (wdt == null)
            {
                return false;
            }

            DataTable dtBumon = wdt.Copy();

            // 金額ゼロ出力しない場合は除外
            if (Util.ToString(this._pForm.Condition["Inji"]) == "no")
            {
                DataView dtv = new DataView(wdt.Copy());
                dtv.RowFilter = "BUMON_KINGAKU_KEI > 0";
                #region 実績無し部門の除外
                DataTable dtw = dtv.ToTable().Copy();
                dtv = new DataView(dtw.Copy());
                dtw = dtv.ToTable(true, new string[] { "BUMON_CD" });
                List<int> bList = new List<int>();
                foreach (DataRow r in dtw.Rows)
                {
                    string expr = "BUMON_CD = " + Util.ToString(r["BUMON_CD"]);
                    decimal dcw = Util.ToDecimal(Util.ToString(wdt.Compute("SUM(RUIKEI_KINGAKU_KEI)", expr)));
                    if (dcw == 0)
                    {
                        bList.Add(Util.ToInt(Util.ToString(r["BUMON_CD"])));
                    }
                }

                string addexpr = "";
                if (bList.Count != 0)
                {
                    foreach (int i in bList)
                    {
                        addexpr += " AND BUMON_CD <> " + i.ToString();
                    }
                }
                dtv = new DataView(wdt.Copy());
                dtv.RowFilter = "BUMON_KINGAKU_KEI > 0" + addexpr;
                #endregion
                dtBumon = dtv.ToTable().Copy();
            }
            try
            {
                // 部門リストの作成（４部門毎リスト）
                List<int> bumonList = new List<int>();
                DataView dv = new DataView(dtBumon.Copy());
                DataTable dt = dv.ToTable(true, new string[] { "BUMON_CD" });

                if (dt.Rows.Count == 0)
                    return true;

                foreach (DataRow dr in dt.Rows)
                {
                    bumonList.Add(Util.ToInt(dr["BUMON_CD"]));
                }
                List<string> bmnList = new List<string>();
                string bmnCode = "";
                int i = 1;
                string w = "";
                foreach (int bmnCd in bumonList)
                {
                    bmnCode = bmnCd.ToString();
                    if (i < 5)
                        w += bmnCd.ToString() + ",";
                    else
                    {
                        bmnList.Add(w);
                        i = 1;
                        w = bmnCd.ToString() + ",";
                    }
                    i++;
                }
                bmnList.Add(w);

                // 印字データ用準備
                // １部門情報
                int addRowCount = 5;
                String itemNo01 = "10";
                String itemNo02 = "11";
                String itemNo03 = "12";
                String itemNo04 = "13";
                String itemNo05 = "14";

                String itemNm = "@ITEM";

                // １列目をベースに横展開部門情報を抽出し印刷テーブルへ登録
                foreach (string bl in bmnList)
                {
                    string[] bmn = bl.Split(',');
                    // 部門リストの先頭分でループ
                    dv = new DataView(dtBumon.Copy());
                    dv.RowFilter = "BUMON_CD = " + bmn[0];
                    dt = dv.ToTable();
                    foreach (DataRow r in dt.Rows)
                    {
                        // 出来たリストを元にテーブルへ登録する
                        dbSORT++;
                        Sql = MakeWkData_insertTable(new StringBuilder(), (((bmn.Length - 1) * 5) - 1) + 10);
                        dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, bl, Util.ToString(r["NAME"]), Util.ToString(r["YOUYAKU_KAMOKU"]));

                        for (i = 0; i < bmn.Length - 1; i++)
                        {
                            string b = bmn[i];
                            if (b != "")
                            {
                                BumonRecord br = GetBumonRecord(Util.ToInt(bmn[i]), r, dtBumon);

                                dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, br.BumonCd == 99999 ? br.BumonNm : br.BumonCd.ToString() + " " + br.BumonNm); // 部門名
                                dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, br.MTotak == 0 ? "" : Util.FormatNum(br.MTotak)); // 月計
                                dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, br.LTotak == 0 ? "" : Util.FormatNum(br.LTotak)); // 累計
                                dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, br.KHi == 0 ? "" : Util.FormatNum(br.KHi, 1)); // 構成比
                                dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, br.BHi == 0 ? "" : Util.FormatNum(br.BHi, 1)); // 部門比

                                // ITEM名称をCOUNTUP
                                itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
                                itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
                                itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
                                itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
                                itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);
                            }
                        }

                        // インサート処理を実行
                        this._dba.ModifyBySql(Util.ToString(Sql), dpc);
                        // 列名の初期化
                        itemNo01 = "10";
                        itemNo02 = "11";
                        itemNo03 = "12";
                        itemNo04 = "13";
                        itemNo05 = "14";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 部門、科目分類情報の取得
        /// </summary>
        /// <param name="bumonCode">部門コード</param>
        /// <param name="r">１列目の部門DataRow</param>
        /// <param name="dt">抽出対象DataTable</param>
        /// <returns>部門情報</returns>
        private BumonRecord GetBumonRecord(int bumonCode, DataRow r, DataTable dt)
        {
            BumonRecord br = new BumonRecord();

            StringBuilder srcCond = new StringBuilder();
            srcCond = new StringBuilder();//
            srcCond.AppendLine("BUMON_CD = " + bumonCode.ToString());
            srcCond.AppendLine(" AND HYOJI_JUNI = " + Util.ToString(r["HYOJI_JUNI"]));
            srcCond.AppendLine(" AND KAMOKU_BUNRUI = " + Util.ToString(r["KAMOKU_BUNRUI"]));
            srcCond.AppendLine(" AND YOUYAKU_KAMOKU = " + Util.ToString(r["YOUYAKU_KAMOKU"]));

            DataRow[] kingkRow = dt.Select(srcCond.ToString());
            for (int i = 0; i < kingkRow.Length; i++)
            {
                br.KamokuNm = Util.ToString(kingkRow[i]["KAMOKU_BUNRUI_NM"]);
                br.BumonCd = Util.ToInt(Util.ToString(kingkRow[i]["BUMON_CD"]));
                br.BumonNm = Util.ToString(kingkRow[i]["BUMON_NM"]);
                // 金額情報
                br.MTotak = Util.ToDecimal(Util.ToString(kingkRow[i]["HASSEI_KINGAKU"]));
                br.LTotak = Util.ToDecimal(Util.ToString(kingkRow[i]["RUIKEI_KINGAKU"]));
                //
                br.KHi = Util.ToDecimal(Util.ToString(kingkRow[i]["KOUSEI_HI"]));
                br.BHi = Util.ToDecimal(Util.ToString(kingkRow[i]["BUMON_HI"]));

                break;
            }
            return br;
        }


        /// <summary>
        /// インサートテーブルを作成します。
        /// </summary>
        private StringBuilder MakeWkData_insertTable(StringBuilder Sql, int maxNo)
        {
            Sql.AppendLine("INSERT INTO PR_ZM_TBL(");
            Sql.AppendLine("  GUID");
            Sql.AppendLine(" ,SORT");
            Sql.AppendLine(" ,ITEM01");
            Sql.AppendLine(" ,ITEM02");
            Sql.AppendLine(" ,ITEM03");
            Sql.AppendLine(" ,ITEM04");
            Sql.AppendLine(" ,ITEM05");
            Sql.AppendLine(" ,ITEM06");
            Sql.AppendLine(" ,ITEM07");
            Sql.AppendLine(" ,ITEM08");
            Sql.AppendLine(" ,ITEM09");
            for (int itemNo = 10; itemNo <= maxNo; itemNo++)
            {
                Sql.AppendLine(" ,ITEM" + itemNo);
            }
            Sql.AppendLine(") ");
            Sql.AppendLine("VALUES(");
            Sql.AppendLine("  @GUID");
            Sql.AppendLine(" ,@SORT");
            Sql.AppendLine(" ,@ITEM01");
            Sql.AppendLine(" ,@ITEM02");
            Sql.AppendLine(" ,@ITEM03");
            Sql.AppendLine(" ,@ITEM04");
            Sql.AppendLine(" ,@ITEM05");
            Sql.AppendLine(" ,@ITEM06");
            Sql.AppendLine(" ,@ITEM07");
            Sql.AppendLine(" ,@ITEM08");
            Sql.AppendLine(" ,@ITEM09");
            for (int itemNo = 10; itemNo <= maxNo; itemNo++)
            {
                Sql.AppendLine(" ,@ITEM" + itemNo);
            }
            Sql.AppendLine(") ");

            return Sql;
        }

        /// <summary>
        /// 共通パラム
        /// </summary>
        private DbParamCollection MakeWkData_setDpc(int dbSORT, String kikan, string bumonRange, string outPutDate, string pageBreak, string kamokuNm, string yoyakukamoku)
        {
            DbParamCollection dpc = new DbParamCollection();
            /* ▼▼▼ 共通 ▼▼▼ */
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, bumonRange); // 部門名
            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, this._uInfo.KaishaNm); // 会社名
            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, kikan); // 期間
            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, Util.ToString(this._pForm.Condition["ShohizeiShori"])); // 税抜きor税込み
            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, outPutDate); // 出力日付
            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dbSORT); // ページ切り替え用
            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, pageBreak); // 実際の改ページ
            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, kamokuNm); // 科目名
            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, yoyakukamoku); // 要約科目（カラー行判定）
            /* ▲▲▲ 共通 ▲▲▲ */

            return dpc;
        }

        #endregion
    }
}
