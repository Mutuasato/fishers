﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1051
{
    /// <summary>
    /// 部門別損益管理表[部門設定](ZMMR1052)
    /// </summary>
    public partial class ZMMR1052 : BasePgForm
    {
        #region 定数

        /// <summary>
        /// 未設定部門名
        /// </summary>
        private const string noBumonName = "★未設定";

        // 列の番号
        const int COL_KAMOKU_CD = 0;
        const int COL_KAMOKU_NM = 1;
        const int COL_BUNRUI_NM = 2;
        const int COL_BUMON_CD = 3;
        const int COL_BUMON_NM = 4;

        #endregion

        #region private変数

        /// <summary>
        /// V部門データ
        /// </summary>
        private DataTable _dtBumon;

        /// <summary>
        /// DataGridView表示有無のFlag
        /// </summary>
        int _viewFlag = 0;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMMR1052()
        {
            InitializeComponent();
            BindGotFocusEvent();

            new MessageForwarder(this.txtGridEdit, 0x20A);
            new MessageForwarder(this.dgvInputList, 0x20A);
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            this.ShowFButton = false;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvInputList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Font = this.txtGridEdit.Font;
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvInputList.DefaultCellStyle.Font = this.txtGridEdit.Font;
            this.dgvInputList.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            this.dgvInputList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvInputList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvInputList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            this.dgvInputList.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            //this.dgvInputList.Columns[0].DefaultCellStyle.BackColor = ColorTranslator.FromHtml("#BBFFFF");
            //this.dgvInputList.Columns[1].DefaultCellStyle.BackColor = ColorTranslator.FromHtml("#BBFFFF");
            //this.dgvInputList.Columns[2].DefaultCellStyle.BackColor = ColorTranslator.FromHtml("#BBFFFF");
            //this.dgvInputList.Columns[4].DefaultCellStyle.BackColor = ColorTranslator.FromHtml("#BBFFFF");

            // ユーザ操作による行追加を無効(禁止)
            this.dgvInputList.AllowUserToAddRows = false;
            // 入力モード設定
            this.dgvInputList.EditMode = DataGridViewEditMode.EditProgrammatically;

            // データ表示
            DataLoad();

            try
            {
                this.dgvInputList.CurrentCell = this.dgvInputList[COL_BUMON_CD, 0];
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            #region 部門データ
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                StringBuilder cols = new StringBuilder();
                cols.AppendLine("KAISHA_CD");
                cols.AppendLine(", BUMON_CD");
                cols.AppendLine(", BUMON_NM");
                cols.AppendLine(", BUMON_KANA_NM");
                cols.AppendLine(", JIGYO_KUBUN");
                cols.AppendLine(", JIGYO_KUBUN_NM");
                cols.AppendLine(", HAIFU_KUBUN");
                StringBuilder where = new StringBuilder();
                where.AppendLine("KAISHA_CD = @KAISHA_CD");
                _dtBumon = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "VI_ZM_BUMON"
                                , Util.ToString(where), dpc);
            }
            #endregion

            // Escape F1 F6のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            this.ShowFButton = true;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            /* 検索用小窓が使える項目にフォーカス時のみF1キーを有効にする
                 ただし、明細は dgvInputList_CellEnter() でF1キーを有効にしている
             */
            switch (this.ActiveCtlNm)
            {
                case "txtGridEdit":
                    switch (this.dgvInputList.CurrentCell.ColumnIndex)
                    {
                        case COL_BUMON_CD:
                            this.btnF1.Enabled = true;
                            break;
                    }
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            switch (this.ActiveCtlNm)
            {
                #region 明細
                case "txtGridEdit":
                    switch (this.dgvInputList.CurrentCell.ColumnIndex)
                    {
                        case COL_BUMON_CD://3
                            // アセンブリのロード
                            asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2041.exe");
                            // フォーム作成
                            t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                            //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2043");
                            if (t != null)
                            {
                                Object obj = Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    // タブの一部として埋め込む
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.Par1 = "1";
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        string[] result = (string[])frm.OutData;
                                        this.txtGridEdit.Text = result[0];
                                    }
                                }
                            }
                            break;
                    }
                    break;

                    #endregion
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 更新処理
            if (!this.btnF6.Enabled)
                return;

            string msg = "保存しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            DbParamCollection updParam;
            DbParamCollection whereParam;
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 事前削除
                whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                this.Dba.Delete("TB_ZM_BMN_SNK_KMK_STI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                    whereParam);

                int i = 0;
                while (this.dgvInputList.RowCount - 1 > i)
                {
                    // 部門コードの入力がある行のみ
                    if (Util.ToDecimal(Util.ToString(this.dgvInputList[COL_BUMON_CD, i].Value)) != 0)
                    {
                        updParam = new DbParamCollection();

                        updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
                        updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_KAMOKU_CD, i].Value)));
                        updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_BUMON_CD, i].Value)));

                        updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now);

                        this.Dba.Insert("TB_ZM_BMN_SNK_KMK_STI", updParam);
                    }

                    i++;
                }

                // トランザクションをコミット
                this.Dba.Commit();

                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();

                if (this.DialogResult == DialogResult.None)
                    this.DialogResult = DialogResult.Cancel;
            }
        }
        #endregion

        #region イベント

        private void ZAMR9012_Shown(object sender, EventArgs e)
        {

            if (_viewFlag == 0)
            {
                // 表示されている情報をクリア
                this.dgvInputList.Rows.Clear();
                Msg.Info("該当するデータがありません。");
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
                return;
            }
            // 自分自身をアクティブに設定
            this.Activate();
        }

        /// <summary>
        /// データグリッドにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Enter(object sender, EventArgs e)
        {
            DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
            if (dgvCell != null && dgvCell.ColumnIndex == 0)
            {
                this.dgvInputList.CurrentCell = this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex];
            }
        }

        /// <summary>
        /// データグリッドのセルにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                switch (e.ColumnIndex)
                {
                    case COL_BUMON_CD:// 3:
                        this.txtGridEdit.BackColor = Color.White;
                        this.txtGridEdit.Visible = true;
                        DataGridView dgv = (DataGridView)sender;
                        Rectangle rctCell = dgv.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);

                        this.txtGridEdit.Size = rctCell.Size;
                        this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
                        this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;
                        this.txtGridEdit.Text = Util.ToString(dgvInputList[e.ColumnIndex, e.RowIndex].Value).Replace(",", "");

                        this.txtGridEdit.SelectAll();

                        this.txtGridEdit.Focus();
                        break;
                    default:
                        this.txtGridEdit.Visible = false;
                        break;

                }
            }
            catch (Exception ex) { Console.WriteLine("CellEnter:" + ex.Message); }
        }

        /// <summary>
        /// データグリッドがスクロールした時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            Rectangle rctCell = dgv.GetCellDisplayRectangle(this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex, false);

            this.txtGridEdit.Size = rctCell.Size;
            this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
            this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;
        }

        /// <summary>
        /// データグリッド用のテキストにキーダウン時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGridEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
            {
                DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;

                switch (dgvCell.ColumnIndex)
                {
                    case COL_BUMON_CD:// 3:
                        if ((ValChk.IsEmpty(txtGridEdit.Text)) || (!ValChk.IsNumber(txtGridEdit.Text)))
                        {
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        // 存在しないコードを入力されたらエラー
                        if (Util.ToInt(this.txtGridEdit.Text) == 0)
                        {
                            this.dgvInputList[COL_BUMON_NM, dgvCell.RowIndex].Value = noBumonName;
                        }
                        else
                        {
                            DataRow dr = GetBumonDataRow(Util.ToString(this.txtGridEdit.Text));
                            if (dr == null)
                            {
                                Msg.Error("入力に誤りがあります。");
                                return;
                            }
                            else
                            {
                                this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex].Value = this.txtGridEdit.Text;
                                this.dgvInputList[COL_BUMON_NM, dgvCell.RowIndex].Value = Util.ToString(dr["BUMON_NM"]);
                            }

                        }

                        if (this.GetGridKeyEnter(e.KeyCode, dgvCell))
                        {
                            if ((dgvInputList.RowCount - 1) != dgvCell.RowIndex)
                            {
                                dgvInputList.CurrentCell = dgvInputList[COL_BUMON_CD, dgvCell.RowIndex + 1];
                            }
                            else if((dgvInputList.RowCount - 1) == dgvCell.RowIndex)
                            {
                                this.PressF6();
                            }
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// データグリッド用のテキストにEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGridEdit_Enter(object sender, EventArgs e)
        {
            BeginInvoke(new EventHandler(TextBoxSelectAll), sender, e);
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// 部門レコードを取得
        /// </summary>
        /// <param name="bumonCd">部門コード</param>
        /// <returns>部門レコード又はnull</returns>
        private DataRow GetBumonDataRow(string bumonCd)
        {
            DataRow ret = null;
            DataRow[] foundRows;

            foundRows = _dtBumon.Select("BUMON_CD = '" + bumonCd + "'");
            if (foundRows.Length > 0)
            {
                ret = foundRows[0];
            }

            return ret;
        }

        /// <summary>
        /// 設定情報の表示
        /// </summary>
        private void DataLoad()
        {
            // グリッドをクリア
            this.dgvInputList.Rows.Clear();

            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" SELECT");
            sql.AppendLine(" A.KANJO_KAMOKU_CD");
            sql.AppendLine(",A.KANJO_KAMOKU_NM");
            sql.AppendLine(",A.KAMOKU_BUNRUI_CD");
            sql.AppendLine(",B.KAMOKU_BUNRUI_NM");
            sql.AppendLine(",ISNULL(C.BUMON_CD, 0) AS BUMON_CD");
            sql.AppendLine(",D.BUMON_NM");
            sql.AppendLine(",C.REGIST_DATE");
            sql.AppendLine(" FROM");
            sql.AppendLine(" TB_ZM_KANJO_KAMOKU AS A");
            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_F_KAMOKU_BUNRUI AS B");
            sql.AppendLine("  ON A.KAMOKU_BUNRUI_CD = B.KAMOKU_BUNRUI_CD");
            sql.AppendLine(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" AND A.KAISHA_CD = @KAISHA_CD");

            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_BMN_SNK_KMK_STI AS C");
            sql.AppendLine("  ON C.KAISHA_CD = A.KAISHA_CD");
            sql.AppendLine(" AND C.SHISHO_CD = 0");
            sql.AppendLine(" AND C.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD");
            sql.AppendLine(" AND C.KAIKEI_NENDO = A.KAIKEI_NENDO");

            sql.AppendLine(" LEFT OUTER JOIN TB_CM_BUMON AS D");
            sql.AppendLine("  ON D.KAISHA_CD = C.KAISHA_CD");
            sql.AppendLine(" AND D.BUMON_CD = C.BUMON_CD");

            sql.AppendLine(" WHERE");
            sql.AppendLine("  A.KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine("  A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.AppendLine("  A.BUMON_UMU = 0 AND");
            sql.AppendLine("  B.BUNRUI_CD1 = 2");
            sql.AppendLine(" ORDER BY");
            sql.AppendLine("  A.KAMOKU_BUNRUI_CD ASC,");
            sql.AppendLine("  A.KANJO_KAMOKU_CD ASC");
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, UInfo.KaikeiNendo);

            try
            {
                DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

                if (dt.Rows.Count > 0)
                {

                    _viewFlag = 1;

                    this.dgvInputList.RowCount = dt.Rows.Count;
                    int i = 0;
                    while (dt.Rows.Count > i)
                    {
                        this.dgvInputList[COL_KAMOKU_CD, i].Value = Util.ToString(dt.Rows[i]["KANJO_KAMOKU_CD"]);
                        this.dgvInputList[COL_KAMOKU_NM, i].Value = Util.ToString(dt.Rows[i]["KANJO_KAMOKU_NM"]);
                        this.dgvInputList[COL_BUNRUI_NM, i].Value = Util.ToString(dt.Rows[i]["KAMOKU_BUNRUI_NM"]);
                        this.dgvInputList[COL_BUMON_CD, i].Value = Util.ToString(dt.Rows[i]["BUMON_CD"]);
                        if (Util.ToInt(dt.Rows[i]["BUMON_CD"]) == 0)
                        {
                            this.dgvInputList[COL_BUMON_NM, i].Value = noBumonName;
                        }
                        else
                        {
                            this.dgvInputList[COL_BUMON_NM, i].Value = Util.ToString(dt.Rows[i]["BUMON_NM"]);
                        }
                        i++;
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        /// <summary>
        /// 明細エンターキー以外の処理
        /// </summary>
        /// <returns>エンターキーを押したかの真偽値(真:押した,偽:押してない)</param>
        private bool GetGridKeyEnter(Keys KeyCode, DataGridViewCell dgvCell)
        {
            if (KeyCode == Keys.Enter)
            {
                return true;
            }
            if (KeyCode == Keys.Down)
            {
                if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
                {
                    this.dgvInputList.CurrentCell = dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex + 1];
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (dgvCell.RowIndex > 0)
                {
                    this.dgvInputList.CurrentCell = dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex - 1];
                }
            }
            return false;
        }

        /// <summary>
        /// データグリッド用のテキストの全選択
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBoxSelectAll(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox == null) return;

            //// 少し長めにクリックされた場合にも一応対処
            //while ((Control.MouseButtons & MouseButtons.Left) != MouseButtons.None)
            //    Application.DoEvents();

            textBox.SelectAll();
        }
        #endregion
    }
}
