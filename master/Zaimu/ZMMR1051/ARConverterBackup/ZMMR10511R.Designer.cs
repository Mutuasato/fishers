﻿namespace jp.co.fsi.zm.zmmr1051
{
    /// <summary>
    /// ZMMR10511R の概要の説明です。
    /// </summary>
    partial class ZMMR10511R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMMR10511R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtKaishaNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1321 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtKamokuNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZeinukiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKamokuNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1321)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKamokuNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeinukiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKamokuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox18,
            this.textBox16,
            this.textBox14,
            this.textBox12,
            this.textBox10,
            this.textBox8,
            this.textBox3,
            this.txtTitle05,
            this.textBox11,
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtTitle,
            this.txtDate,
            this.txtZei,
            this.txtTitle01,
            this.txtTitle03,
            this.txtTitle04,
            this.line23,
            this.line24,
            this.line6,
            this.line3,
            this.textBox1,
            this.line5,
            this.textBox7,
            this.textBox9,
            this.line7,
            this.line8,
            this.line18,
            this.textBox13,
            this.line20,
            this.line21,
            this.line22,
            this.textBox15,
            this.textBox17,
            this.line27,
            this.line28,
            this.line29,
            this.textBox4,
            this.line25,
            this.textBox5,
            this.line19,
            this.textBox6,
            this.line26,
            this.line30,
            this.line16,
            this.line17,
            this.txtKaishaNm,
            this.label1,
            this.textBox1321,
            this.line9});
            this.pageHeader.Height = 1.174016F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // textBox18
            // 
            this.textBox18.Height = 0.2657481F;
            this.textBox18.Left = 10.36614F;
            this.textBox18.MultiLine = false;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 8.25pt; font-weight" +
    ": normal; text-align: center; vertical-align: middle";
            this.textBox18.Text = "部門比";
            this.textBox18.Top = 0.9082677F;
            this.textBox18.Width = 0.4094502F;
            // 
            // textBox16
            // 
            this.textBox16.Height = 0.2657481F;
            this.textBox16.Left = 9.933071F;
            this.textBox16.MultiLine = false;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 8.25pt; font-weight" +
    ": normal; text-align: center; vertical-align: middle";
            this.textBox16.Text = "構成比";
            this.textBox16.Top = 0.9082677F;
            this.textBox16.Width = 0.4496066F;
            // 
            // textBox14
            // 
            this.textBox14.Height = 0.2657481F;
            this.textBox14.Left = 7.968504F;
            this.textBox14.MultiLine = false;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 8.25pt; font-weight" +
    ": normal; text-align: center; vertical-align: middle";
            this.textBox14.Text = "部門比";
            this.textBox14.Top = 0.9070867F;
            this.textBox14.Width = 0.4496066F;
            // 
            // textBox12
            // 
            this.textBox12.Height = 0.2657481F;
            this.textBox12.Left = 7.535433F;
            this.textBox12.MultiLine = false;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 8.25pt; font-weight" +
    ": normal; text-align: center; vertical-align: middle";
            this.textBox12.Text = "構成比";
            this.textBox12.Top = 0.9070867F;
            this.textBox12.Width = 0.4496066F;
            // 
            // textBox10
            // 
            this.textBox10.Height = 0.2657481F;
            this.textBox10.Left = 5.570866F;
            this.textBox10.MultiLine = false;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 8.25pt; font-weight" +
    ": normal; text-align: center; vertical-align: middle";
            this.textBox10.Text = "部門比";
            this.textBox10.Top = 0.9165354F;
            this.textBox10.Width = 0.4496066F;
            // 
            // textBox8
            // 
            this.textBox8.Height = 0.2657481F;
            this.textBox8.Left = 5.137795F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 8.25pt; font-weight" +
    ": normal; text-align: center; vertical-align: middle";
            this.textBox8.Text = "構成比";
            this.textBox8.Top = 0.9165354F;
            this.textBox8.Width = 0.4496066F;
            // 
            // textBox3
            // 
            this.textBox3.Height = 0.2657481F;
            this.textBox3.Left = 3.173229F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 8.25pt; font-weight" +
    ": normal; text-align: center; vertical-align: middle";
            this.textBox3.Text = "部門比";
            this.textBox3.Top = 0.911811F;
            this.textBox3.Width = 0.4496066F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.Height = 0.2657481F;
            this.txtTitle05.Left = 2.740158F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 8.25pt; font-weight" +
    ": normal; text-align: center; vertical-align: middle";
            this.txtTitle05.Text = "構成比";
            this.txtTitle05.Top = 0.911811F;
            this.txtTitle05.Width = 0.4496066F;
            // 
            // textBox11
            // 
            this.textBox11.Height = 0.2657481F;
            this.textBox11.Left = 5.980315F;
            this.textBox11.MultiLine = false;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox11.Text = "月　計";
            this.textBox11.Top = 0.9070867F;
            this.textBox11.Width = 0.7795278F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM05";
            this.txtToday.Height = 0.1968504F;
            this.txtToday.Left = 8.722048F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtToday.Text = "yyyy-MM-dd";
            this.txtToday.Top = 0.04645665F;
            this.txtToday.Width = 1.218897F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 9.999209F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblPage.Text = "Page:";
            this.lblPage.Top = 0.04645665F;
            this.lblPage.Width = 0.4299212F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 10.48307F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "1";
            this.txtPageCount.Top = 0.04645665F;
            this.txtPageCount.Width = 0.1909451F;
            // 
            // txtTitle
            // 
            this.txtTitle.Height = 0.25F;
            this.txtTitle.Left = 3.702756F;
            this.txtTitle.MultiLine = false;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: bold; text-align: center; " +
    "vertical-align: middle";
            this.txtTitle.Text = "部門別損益管理表";
            this.txtTitle.Top = 0.003543307F;
            this.txtTitle.Width = 3.611024F;
            // 
            // txtDate
            // 
            this.txtDate.DataField = "ITEM03";
            this.txtDate.Height = 0.1968504F;
            this.txtDate.Left = 3.734252F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.353937F;
            this.txtDate.Width = 3.579528F;
            // 
            // txtZei
            // 
            this.txtZei.DataField = "ITEM04";
            this.txtZei.Height = 0.1968504F;
            this.txtZei.Left = 8.722048F;
            this.txtZei.MultiLine = false;
            this.txtZei.Name = "txtZei";
            this.txtZei.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: middle";
            this.txtZei.Text = null;
            this.txtZei.Top = 0.353937F;
            this.txtZei.Width = 0.839766F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.5314961F;
            this.txtTitle01.Left = 0F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.txtTitle01.Text = "勘定科目";
            this.txtTitle01.Top = 0.646063F;
            this.txtTitle01.Width = 1.185039F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.2657481F;
            this.txtTitle03.Left = 1.185039F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.txtTitle03.Text = "月　計";
            this.txtTitle03.Top = 0.911811F;
            this.txtTitle03.Width = 0.7795276F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.2657481F;
            this.txtTitle04.Left = 1.968504F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.txtTitle04.Text = "累　計";
            this.txtTitle04.Top = 0.911811F;
            this.txtTitle04.Width = 0.7795276F;
            // 
            // line23
            // 
            this.line23.Height = 0.5314959F;
            this.line23.Left = 1.968504F;
            this.line23.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.6472441F;
            this.line23.Width = 0F;
            this.line23.X1 = 1.968504F;
            this.line23.X2 = 1.968504F;
            this.line23.Y1 = 0.6472441F;
            this.line23.Y2 = 1.17874F;
            // 
            // line24
            // 
            this.line24.Height = 0.5314963F;
            this.line24.Left = 2.740158F;
            this.line24.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.6425197F;
            this.line24.Width = 0F;
            this.line24.X1 = 2.740158F;
            this.line24.X2 = 2.740158F;
            this.line24.Y1 = 0.6425197F;
            this.line24.Y2 = 1.174016F;
            // 
            // line6
            // 
            this.line6.Height = 0.5314963F;
            this.line6.Left = 3.173229F;
            this.line6.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.6425197F;
            this.line6.Width = 0F;
            this.line6.X1 = 3.173229F;
            this.line6.X2 = 3.173229F;
            this.line6.Y1 = 0.6425197F;
            this.line6.Y2 = 1.174016F;
            // 
            // line3
            // 
            this.line3.Height = 0.5314963F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.6425197F;
            this.line3.Width = 0F;
            this.line3.X1 = 0F;
            this.line3.X2 = 0F;
            this.line3.Y1 = 0.6425197F;
            this.line3.Y2 = 1.174016F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM10";
            this.textBox1.Height = 0.2657481F;
            this.textBox1.Left = 1.185039F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox1.Text = "計";
            this.textBox1.Top = 0.646063F;
            this.textBox1.Width = 2.397638F;
            // 
            // line5
            // 
            this.line5.Height = 0.5314963F;
            this.line5.Left = 1.185039F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.6425197F;
            this.line5.Width = 0F;
            this.line5.X1 = 1.185039F;
            this.line5.X2 = 1.185039F;
            this.line5.Y1 = 0.6425197F;
            this.line5.Y2 = 1.174016F;
            // 
            // textBox7
            // 
            this.textBox7.Height = 0.2657481F;
            this.textBox7.Left = 3.582677F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox7.Text = "月　計";
            this.textBox7.Top = 0.9165354F;
            this.textBox7.Width = 0.7795278F;
            // 
            // textBox9
            // 
            this.textBox9.Height = 0.2657481F;
            this.textBox9.Left = 4.366142F;
            this.textBox9.MultiLine = false;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox9.Text = "累　計";
            this.textBox9.Top = 0.9165354F;
            this.textBox9.Width = 0.7795278F;
            // 
            // line7
            // 
            this.line7.Height = 0.5314965F;
            this.line7.Left = 4.366142F;
            this.line7.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.6519685F;
            this.line7.Width = 0F;
            this.line7.X1 = 4.366142F;
            this.line7.X2 = 4.366142F;
            this.line7.Y1 = 0.6519685F;
            this.line7.Y2 = 1.183465F;
            // 
            // line8
            // 
            this.line8.Height = 0.5314959F;
            this.line8.Left = 5.137795F;
            this.line8.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.6472441F;
            this.line8.Width = 0F;
            this.line8.X1 = 5.137795F;
            this.line8.X2 = 5.137795F;
            this.line8.Y1 = 0.6472441F;
            this.line8.Y2 = 1.17874F;
            // 
            // line18
            // 
            this.line18.Height = 0.5314959F;
            this.line18.Left = 5.570866F;
            this.line18.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0.6472441F;
            this.line18.Width = 0F;
            this.line18.X1 = 5.570866F;
            this.line18.X2 = 5.570866F;
            this.line18.Y1 = 0.6472441F;
            this.line18.Y2 = 1.17874F;
            // 
            // textBox13
            // 
            this.textBox13.Height = 0.2657481F;
            this.textBox13.Left = 6.76378F;
            this.textBox13.MultiLine = false;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox13.Text = "累　計";
            this.textBox13.Top = 0.9070867F;
            this.textBox13.Width = 0.7795278F;
            // 
            // line20
            // 
            this.line20.Height = 0.5314964F;
            this.line20.Left = 6.76378F;
            this.line20.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.6425196F;
            this.line20.Width = 0F;
            this.line20.X1 = 6.76378F;
            this.line20.X2 = 6.76378F;
            this.line20.Y1 = 0.6425196F;
            this.line20.Y2 = 1.174016F;
            // 
            // line21
            // 
            this.line21.Height = 0.5267713F;
            this.line21.Left = 7.535433F;
            this.line21.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.6425197F;
            this.line21.Width = 0F;
            this.line21.X1 = 7.535433F;
            this.line21.X2 = 7.535433F;
            this.line21.Y1 = 0.6425197F;
            this.line21.Y2 = 1.169291F;
            // 
            // line22
            // 
            this.line22.Height = 0.5267713F;
            this.line22.Left = 7.968504F;
            this.line22.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.6425197F;
            this.line22.Width = 0F;
            this.line22.X1 = 7.968504F;
            this.line22.X2 = 7.968504F;
            this.line22.Y1 = 0.6425197F;
            this.line22.Y2 = 1.169291F;
            // 
            // textBox15
            // 
            this.textBox15.Height = 0.2657481F;
            this.textBox15.Left = 8.377954F;
            this.textBox15.MultiLine = false;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox15.Text = "月　計";
            this.textBox15.Top = 0.9082677F;
            this.textBox15.Width = 0.7795278F;
            // 
            // textBox17
            // 
            this.textBox17.Height = 0.2657481F;
            this.textBox17.Left = 9.161418F;
            this.textBox17.MultiLine = false;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox17.Text = "累　計";
            this.textBox17.Top = 0.9082677F;
            this.textBox17.Width = 0.7795278F;
            // 
            // line27
            // 
            this.line27.Height = 0.5314963F;
            this.line27.Left = 9.161418F;
            this.line27.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 0.6437007F;
            this.line27.Width = 0F;
            this.line27.X1 = 9.161418F;
            this.line27.X2 = 9.161418F;
            this.line27.Y1 = 0.6437007F;
            this.line27.Y2 = 1.175197F;
            // 
            // line28
            // 
            this.line28.Height = 0.5279523F;
            this.line28.Left = 9.933071F;
            this.line28.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 0.6425197F;
            this.line28.Width = 0F;
            this.line28.X1 = 9.933071F;
            this.line28.X2 = 9.933071F;
            this.line28.Y1 = 0.6425197F;
            this.line28.Y2 = 1.170472F;
            // 
            // line29
            // 
            this.line29.Height = 0.5314957F;
            this.line29.Left = 10.36614F;
            this.line29.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 0.6389763F;
            this.line29.Width = 0F;
            this.line29.X1 = 10.36614F;
            this.line29.X2 = 10.36614F;
            this.line29.Y1 = 0.6389763F;
            this.line29.Y2 = 1.170472F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM15";
            this.textBox4.Height = 0.2657481F;
            this.textBox4.Left = 3.582677F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox4.Text = "計";
            this.textBox4.Top = 0.646063F;
            this.textBox4.Width = 2.397637F;
            // 
            // line25
            // 
            this.line25.Height = 0.5314963F;
            this.line25.Left = 3.582677F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.6425197F;
            this.line25.Width = 0F;
            this.line25.X1 = 3.582677F;
            this.line25.X2 = 3.582677F;
            this.line25.Y1 = 0.6425197F;
            this.line25.Y2 = 1.174016F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM20";
            this.textBox5.Height = 0.2657481F;
            this.textBox5.Left = 5.980315F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox5.Text = "計";
            this.textBox5.Top = 0.646063F;
            this.textBox5.Width = 2.401575F;
            // 
            // line19
            // 
            this.line19.Height = 0.5314963F;
            this.line19.Left = 5.980315F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.6425197F;
            this.line19.Width = 0F;
            this.line19.X1 = 5.980315F;
            this.line19.X2 = 5.980315F;
            this.line19.Y1 = 0.6425197F;
            this.line19.Y2 = 1.174016F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM25";
            this.textBox6.Height = 0.2657481F;
            this.textBox6.Left = 8.377954F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.textBox6.Text = "計";
            this.textBox6.Top = 0.646063F;
            this.textBox6.Width = 2.397637F;
            // 
            // line26
            // 
            this.line26.Height = 0.5314963F;
            this.line26.Left = 8.377954F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 0.6425197F;
            this.line26.Width = 0F;
            this.line26.X1 = 8.377954F;
            this.line26.X2 = 8.377954F;
            this.line26.Y1 = 0.6425197F;
            this.line26.Y2 = 1.174016F;
            // 
            // line30
            // 
            this.line30.Height = 0.5314963F;
            this.line30.Left = 10.77559F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 0.6425197F;
            this.line30.Width = 0F;
            this.line30.X1 = 10.77559F;
            this.line30.X2 = 10.77559F;
            this.line30.Y1 = 0.6425197F;
            this.line30.Y2 = 1.174016F;
            // 
            // line16
            // 
            this.line16.Height = 0F;
            this.line16.Left = 0F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.646063F;
            this.line16.Width = 10.77559F;
            this.line16.X1 = 0F;
            this.line16.X2 = 10.77559F;
            this.line16.Y1 = 0.646063F;
            this.line16.Y2 = 0.646063F;
            // 
            // line17
            // 
            this.line17.Height = 0F;
            this.line17.Left = 0F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 1.177559F;
            this.line17.Width = 10.77559F;
            this.line17.X1 = 0F;
            this.line17.X2 = 10.77559F;
            this.line17.Y1 = 1.177559F;
            this.line17.Y2 = 1.177559F;
            // 
            // txtKaishaNm
            // 
            this.txtKaishaNm.DataField = "ITEM02";
            this.txtKaishaNm.Height = 0.2F;
            this.txtKaishaNm.Left = 0.06259843F;
            this.txtKaishaNm.Name = "txtKaishaNm";
            this.txtKaishaNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtKaishaNm.Text = null;
            this.txtKaishaNm.Top = 0.05000001F;
            this.txtKaishaNm.Width = 2.269685F;
            // 
            // label1
            // 
            this.label1.Height = 0.1968504F;
            this.label1.HyperLink = null;
            this.label1.Left = 9.692126F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.label1.Text = "単位：円";
            this.label1.Top = 0.353937F;
            this.label1.Width = 0.9818897F;
            // 
            // textBox1321
            // 
            this.textBox1321.DataField = "ITEM01";
            this.textBox1321.Height = 0.1968504F;
            this.textBox1321.Left = 0.06259842F;
            this.textBox1321.MultiLine = false;
            this.textBox1321.Name = "textBox1321";
            this.textBox1321.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.textBox1321.Text = null;
            this.textBox1321.Top = 0.353937F;
            this.textBox1321.Width = 2.269685F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 1.185039F;
            this.line9.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0.9165355F;
            this.line9.Width = 9.566931F;
            this.line9.X1 = 1.185039F;
            this.line9.X2 = 10.75197F;
            this.line9.Y1 = 0.9165355F;
            this.line9.Y2 = 0.9165355F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM07";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape1,
            this.txtKamokuNo,
            this.txtZeinukiKingaku,
            this.txtShohizei,
            this.txtKei,
            this.txtKamokuNm,
            this.line2,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.textBox2,
            this.line15,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.line4,
            this.line31,
            this.line32,
            this.textBox22,
            this.line33,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.line34,
            this.line35,
            this.line36,
            this.textBox26,
            this.line37,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.line38,
            this.line39,
            this.line40,
            this.textBox30,
            this.line41});
            this.detail.Height = 0.2027559F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.LightCyan;
            this.shape1.Height = 0.2027559F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0F;
            this.shape1.Visible = false;
            this.shape1.Width = 10.77559F;
            // 
            // txtKamokuNo
            // 
            this.txtKamokuNo.DataField = "ITEM09";
            this.txtKamokuNo.Height = 0.09842519F;
            this.txtKamokuNo.Left = 0.06259843F;
            this.txtKamokuNo.Name = "txtKamokuNo";
            this.txtKamokuNo.Style = "font-family: ＭＳ ゴシック; font-size: 8.25pt; font-weight: normal; text-align: right; " +
    "vertical-align: top";
            this.txtKamokuNo.Text = null;
            this.txtKamokuNo.Top = 0.003937008F;
            this.txtKamokuNo.Visible = false;
            this.txtKamokuNo.Width = 0.3330709F;
            // 
            // txtZeinukiKingaku
            // 
            this.txtZeinukiKingaku.DataField = "ITEM11";
            this.txtZeinukiKingaku.Height = 0.1968504F;
            this.txtZeinukiKingaku.Left = 1.228346F;
            this.txtZeinukiKingaku.Name = "txtZeinukiKingaku";
            this.txtZeinukiKingaku.OutputFormat = resources.GetString("txtZeinukiKingaku.OutputFormat");
            this.txtZeinukiKingaku.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtZeinukiKingaku.Text = "999,999,999";
            this.txtZeinukiKingaku.Top = 0.003937008F;
            this.txtZeinukiKingaku.Width = 0.7165354F;
            // 
            // txtShohizei
            // 
            this.txtShohizei.DataField = "ITEM12";
            this.txtShohizei.Height = 0.1968504F;
            this.txtShohizei.Left = 2.007874F;
            this.txtShohizei.Name = "txtShohizei";
            this.txtShohizei.OutputFormat = resources.GetString("txtShohizei.OutputFormat");
            this.txtShohizei.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtShohizei.Text = "999,999,999";
            this.txtShohizei.Top = 0F;
            this.txtShohizei.Width = 0.7165354F;
            // 
            // txtKei
            // 
            this.txtKei.DataField = "ITEM13";
            this.txtKei.Height = 0.1968504F;
            this.txtKei.Left = 2.795276F;
            this.txtKei.Name = "txtKei";
            this.txtKei.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.txtKei.Text = "100.0";
            this.txtKei.Top = 0F;
            this.txtKei.Width = 0.3425197F;
            // 
            // txtKamokuNm
            // 
            this.txtKamokuNm.DataField = "ITEM08";
            this.txtKamokuNm.Height = 0.1968504F;
            this.txtKamokuNm.Left = 0.06259842F;
            this.txtKamokuNm.Name = "txtKamokuNm";
            this.txtKamokuNm.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: left; vert" +
    "ical-align: bottom; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.txtKamokuNm.Text = null;
            this.txtKamokuNm.Top = 0.003937008F;
            this.txtKamokuNm.Width = 1.103937F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.2027559F;
            this.line2.Width = 10.77559F;
            this.line2.X1 = 0F;
            this.line2.X2 = 10.77559F;
            this.line2.Y1 = 0.2027559F;
            this.line2.Y2 = 0.2027559F;
            // 
            // line10
            // 
            this.line10.Height = 0.2027559F;
            this.line10.Left = 0F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 0F;
            this.line10.X2 = 0F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.2027559F;
            // 
            // line11
            // 
            this.line11.Height = 0.2027559F;
            this.line11.Left = 1.184991F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 4.899502E-05F;
            this.line11.X1 = 1.18504F;
            this.line11.X2 = 1.184991F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.2027559F;
            // 
            // line12
            // 
            this.line12.Height = 0.2027559F;
            this.line12.Left = 1.968504F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 1.968504F;
            this.line12.X2 = 1.968504F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.2027559F;
            // 
            // line13
            // 
            this.line13.Height = 0.2027559F;
            this.line13.Left = 2.740158F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 2.740158F;
            this.line13.X2 = 2.740158F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.2027559F;
            // 
            // line14
            // 
            this.line14.Height = 0.2027559F;
            this.line14.Left = 3.173229F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 3.173229F;
            this.line14.X2 = 3.173229F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.2027559F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM14";
            this.textBox2.Height = 0.1968504F;
            this.textBox2.Left = 3.204725F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.textBox2.Text = "100.0";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 0.3543308F;
            // 
            // line15
            // 
            this.line15.Height = 0.2027559F;
            this.line15.Left = 3.582677F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 3.582677F;
            this.line15.X2 = 3.582677F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.2027559F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM16";
            this.textBox19.Height = 0.1968504F;
            this.textBox19.Left = 3.625984F;
            this.textBox19.Name = "textBox19";
            this.textBox19.OutputFormat = resources.GetString("textBox19.OutputFormat");
            this.textBox19.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.textBox19.Text = "999,999,999";
            this.textBox19.Top = 0.003937006F;
            this.textBox19.Width = 0.7165354F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM17";
            this.textBox20.Height = 0.1968504F;
            this.textBox20.Left = 4.405512F;
            this.textBox20.Name = "textBox20";
            this.textBox20.OutputFormat = resources.GetString("textBox20.OutputFormat");
            this.textBox20.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.textBox20.Text = "999,999,999";
            this.textBox20.Top = 0F;
            this.textBox20.Width = 0.7165354F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM18";
            this.textBox21.Height = 0.1968504F;
            this.textBox21.Left = 5.192914F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.textBox21.Text = "100.0";
            this.textBox21.Top = 0F;
            this.textBox21.Width = 0.3425197F;
            // 
            // line4
            // 
            this.line4.Height = 0.2027559F;
            this.line4.Left = 4.366142F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 4.366142F;
            this.line4.X2 = 4.366142F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0.2027559F;
            // 
            // line31
            // 
            this.line31.Height = 0.2027559F;
            this.line31.Left = 5.137795F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 0F;
            this.line31.Width = 0F;
            this.line31.X1 = 5.137795F;
            this.line31.X2 = 5.137795F;
            this.line31.Y1 = 0F;
            this.line31.Y2 = 0.2027559F;
            // 
            // line32
            // 
            this.line32.Height = 0.2027559F;
            this.line32.Left = 5.570866F;
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Top = 0F;
            this.line32.Width = 0F;
            this.line32.X1 = 5.570866F;
            this.line32.X2 = 5.570866F;
            this.line32.Y1 = 0F;
            this.line32.Y2 = 0.2027559F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM19";
            this.textBox22.Height = 0.1968504F;
            this.textBox22.Left = 5.602362F;
            this.textBox22.Name = "textBox22";
            this.textBox22.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.textBox22.Text = "100.0";
            this.textBox22.Top = 0F;
            this.textBox22.Width = 0.3543307F;
            // 
            // line33
            // 
            this.line33.Height = 0.2027559F;
            this.line33.Left = 5.980315F;
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Top = 0F;
            this.line33.Width = 0F;
            this.line33.X1 = 5.980315F;
            this.line33.X2 = 5.980315F;
            this.line33.Y1 = 0F;
            this.line33.Y2 = 0.2027559F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM21";
            this.textBox23.Height = 0.1968504F;
            this.textBox23.Left = 6.023622F;
            this.textBox23.Name = "textBox23";
            this.textBox23.OutputFormat = resources.GetString("textBox23.OutputFormat");
            this.textBox23.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.textBox23.Text = "999,999,999";
            this.textBox23.Top = 0.003937008F;
            this.textBox23.Width = 0.7165354F;
            // 
            // textBox24
            // 
            this.textBox24.DataField = "ITEM22";
            this.textBox24.Height = 0.1968504F;
            this.textBox24.Left = 6.80315F;
            this.textBox24.Name = "textBox24";
            this.textBox24.OutputFormat = resources.GetString("textBox24.OutputFormat");
            this.textBox24.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.textBox24.Text = "999,999,999";
            this.textBox24.Top = 0F;
            this.textBox24.Width = 0.7165354F;
            // 
            // textBox25
            // 
            this.textBox25.DataField = "ITEM23";
            this.textBox25.Height = 0.1968504F;
            this.textBox25.Left = 7.590551F;
            this.textBox25.Name = "textBox25";
            this.textBox25.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.textBox25.Text = "100.0";
            this.textBox25.Top = 0F;
            this.textBox25.Width = 0.3425197F;
            // 
            // line34
            // 
            this.line34.Height = 0.2027559F;
            this.line34.Left = 6.76378F;
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Top = 0F;
            this.line34.Width = 0F;
            this.line34.X1 = 6.76378F;
            this.line34.X2 = 6.76378F;
            this.line34.Y1 = 0F;
            this.line34.Y2 = 0.2027559F;
            // 
            // line35
            // 
            this.line35.Height = 0.2027559F;
            this.line35.Left = 7.535433F;
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Top = 0F;
            this.line35.Width = 0F;
            this.line35.X1 = 7.535433F;
            this.line35.X2 = 7.535433F;
            this.line35.Y1 = 0F;
            this.line35.Y2 = 0.2027559F;
            // 
            // line36
            // 
            this.line36.Height = 0.2027559F;
            this.line36.Left = 7.968504F;
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Top = 0F;
            this.line36.Width = 0F;
            this.line36.X1 = 7.968504F;
            this.line36.X2 = 7.968504F;
            this.line36.Y1 = 0F;
            this.line36.Y2 = 0.2027559F;
            // 
            // textBox26
            // 
            this.textBox26.DataField = "ITEM24";
            this.textBox26.Height = 0.1968504F;
            this.textBox26.Left = 8F;
            this.textBox26.Name = "textBox26";
            this.textBox26.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.textBox26.Text = "100.0";
            this.textBox26.Top = 0F;
            this.textBox26.Width = 0.3543307F;
            // 
            // line37
            // 
            this.line37.Height = 0.2027559F;
            this.line37.Left = 8.377954F;
            this.line37.LineWeight = 1F;
            this.line37.Name = "line37";
            this.line37.Top = 0F;
            this.line37.Width = 0F;
            this.line37.X1 = 8.377954F;
            this.line37.X2 = 8.377954F;
            this.line37.Y1 = 0F;
            this.line37.Y2 = 0.2027559F;
            // 
            // textBox27
            // 
            this.textBox27.DataField = "ITEM26";
            this.textBox27.Height = 0.1968504F;
            this.textBox27.Left = 8.42126F;
            this.textBox27.Name = "textBox27";
            this.textBox27.OutputFormat = resources.GetString("textBox27.OutputFormat");
            this.textBox27.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.textBox27.Text = "999,999,999";
            this.textBox27.Top = 0.003937008F;
            this.textBox27.Width = 0.7165354F;
            // 
            // textBox28
            // 
            this.textBox28.DataField = "ITEM27";
            this.textBox28.Height = 0.1968504F;
            this.textBox28.Left = 9.200788F;
            this.textBox28.Name = "textBox28";
            this.textBox28.OutputFormat = resources.GetString("textBox28.OutputFormat");
            this.textBox28.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.textBox28.Text = "999,999,999";
            this.textBox28.Top = 0F;
            this.textBox28.Width = 0.7165354F;
            // 
            // textBox29
            // 
            this.textBox29.DataField = "ITEM28";
            this.textBox29.Height = 0.1968504F;
            this.textBox29.Left = 9.98819F;
            this.textBox29.Name = "textBox29";
            this.textBox29.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.textBox29.Text = "100.0";
            this.textBox29.Top = 0F;
            this.textBox29.Width = 0.3425197F;
            // 
            // line38
            // 
            this.line38.Height = 0.2027559F;
            this.line38.Left = 9.161418F;
            this.line38.LineWeight = 1F;
            this.line38.Name = "line38";
            this.line38.Top = 0F;
            this.line38.Width = 0F;
            this.line38.X1 = 9.161418F;
            this.line38.X2 = 9.161418F;
            this.line38.Y1 = 0F;
            this.line38.Y2 = 0.2027559F;
            // 
            // line39
            // 
            this.line39.Height = 0.2027559F;
            this.line39.Left = 9.933071F;
            this.line39.LineWeight = 1F;
            this.line39.Name = "line39";
            this.line39.Top = 0F;
            this.line39.Width = 0F;
            this.line39.X1 = 9.933071F;
            this.line39.X2 = 9.933071F;
            this.line39.Y1 = 0F;
            this.line39.Y2 = 0.2027559F;
            // 
            // line40
            // 
            this.line40.Height = 0.2027559F;
            this.line40.Left = 10.36614F;
            this.line40.LineWeight = 1F;
            this.line40.Name = "line40";
            this.line40.Top = 0F;
            this.line40.Width = 0F;
            this.line40.X1 = 10.36614F;
            this.line40.X2 = 10.36614F;
            this.line40.Y1 = 0F;
            this.line40.Y2 = 0.2027559F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM29";
            this.textBox30.Height = 0.1968504F;
            this.textBox30.Left = 10.39764F;
            this.textBox30.Name = "textBox30";
            this.textBox30.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle; white-space: nowrap; ddo-wrap-mode: nowrap";
            this.textBox30.Text = "100.0";
            this.textBox30.Top = 0F;
            this.textBox30.Width = 0.3543307F;
            // 
            // line41
            // 
            this.line41.Height = 0.2027559F;
            this.line41.Left = 10.77559F;
            this.line41.LineWeight = 1F;
            this.line41.Name = "line41";
            this.line41.Top = 0F;
            this.line41.Width = 0F;
            this.line41.X1 = 10.77559F;
            this.line41.X2 = 10.77559F;
            this.line41.Y1 = 0F;
            this.line41.Y2 = 0.2027559F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            this.reportHeader1.Visible = false;
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line1});
            this.reportFooter1.Height = 0F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2165354F;
            this.line1.Width = 10.90158F;
            this.line1.X1 = 0F;
            this.line1.X2 = 10.90158F;
            this.line1.Y1 = 0.2165354F;
            this.line1.Y2 = 0.2165354F;
            // 
            // ZMMR10511R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1905512F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.1937008F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.78623F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1321)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKamokuNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeinukiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKamokuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKamokuNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKamokuNm;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZeinukiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKei;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKaishaNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1321;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
    }
}
