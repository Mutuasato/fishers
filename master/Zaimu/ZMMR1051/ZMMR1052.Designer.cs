﻿namespace jp.co.fsi.zm.zmmr1051
{
    partial class ZMMR1052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblKaishaSettei = new System.Windows.Forms.Label();
            this.lblJp = new System.Windows.Forms.Label();
            this.lblZei = new System.Windows.Forms.Label();
            this.lblGridViewTitle = new System.Windows.Forms.Label();
            this.dgvInputList = new System.Windows.Forms.DataGridView();
            this.KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Zandaka = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KarikataHassei = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KashikataHassei = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TouZan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtGridEdit = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFunanushiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 652);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1013, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1003, 31);
            this.lblTitle.Text = "";
            // 
            // lblKaishaSettei
            // 
            this.lblKaishaSettei.BackColor = System.Drawing.Color.Silver;
            this.lblKaishaSettei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKaishaSettei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaishaSettei.ForeColor = System.Drawing.Color.Black;
            this.lblKaishaSettei.Location = new System.Drawing.Point(0, 0);
            this.lblKaishaSettei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKaishaSettei.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblKaishaSettei.Name = "lblKaishaSettei";
            this.lblKaishaSettei.Size = new System.Drawing.Size(188, 41);
            this.lblKaishaSettei.TabIndex = 0;
            this.lblKaishaSettei.Tag = "CHANGE";
            this.lblKaishaSettei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJp
            // 
            this.lblJp.BackColor = System.Drawing.Color.Silver;
            this.lblJp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJp.ForeColor = System.Drawing.Color.Black;
            this.lblJp.Location = new System.Drawing.Point(0, 0);
            this.lblJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJp.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJp.Name = "lblJp";
            this.lblJp.Size = new System.Drawing.Size(473, 41);
            this.lblJp.TabIndex = 4;
            this.lblJp.Tag = "CHANGE";
            this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblZei
            // 
            this.lblZei.BackColor = System.Drawing.Color.Silver;
            this.lblZei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZei.ForeColor = System.Drawing.Color.Black;
            this.lblZei.Location = new System.Drawing.Point(0, 0);
            this.lblZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZei.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblZei.Name = "lblZei";
            this.lblZei.Size = new System.Drawing.Size(104, 41);
            this.lblZei.TabIndex = 5;
            this.lblZei.Tag = "CHANGE";
            this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGridViewTitle
            // 
            this.lblGridViewTitle.BackColor = System.Drawing.Color.Silver;
            this.lblGridViewTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGridViewTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGridViewTitle.ForeColor = System.Drawing.Color.Black;
            this.lblGridViewTitle.Location = new System.Drawing.Point(0, 0);
            this.lblGridViewTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGridViewTitle.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblGridViewTitle.Name = "lblGridViewTitle";
            this.lblGridViewTitle.Size = new System.Drawing.Size(192, 41);
            this.lblGridViewTitle.TabIndex = 3;
            this.lblGridViewTitle.Tag = "CHANGE";
            this.lblGridViewTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvInputList
            // 
            this.dgvInputList.AllowUserToAddRows = false;
            this.dgvInputList.AllowUserToDeleteRows = false;
            this.dgvInputList.AllowUserToResizeColumns = false;
            this.dgvInputList.AllowUserToResizeRows = false;
            this.dgvInputList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInputList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.KamokuNm,
            this.Zandaka,
            this.KarikataHassei,
            this.KashikataHassei,
            this.TouZan});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvInputList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInputList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvInputList.EnableHeadersVisualStyles = false;
            this.dgvInputList.Location = new System.Drawing.Point(0, 0);
            this.dgvInputList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvInputList.MultiSelect = false;
            this.dgvInputList.Name = "dgvInputList";
            this.dgvInputList.RowHeadersVisible = false;
            this.dgvInputList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInputList.RowTemplate.Height = 21;
            this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInputList.Size = new System.Drawing.Size(958, 549);
            this.dgvInputList.TabIndex = 902;
            this.dgvInputList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellEnter);
            this.dgvInputList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvInputList_Scroll);
            this.dgvInputList.Enter += new System.EventHandler(this.dgvInputList_Enter);
            // 
            // KamokuNm
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.KamokuNm.DefaultCellStyle = dataGridViewCellStyle2;
            this.KamokuNm.HeaderText = "ｺｰﾄﾞ";
            this.KamokuNm.Name = "KamokuNm";
            this.KamokuNm.ReadOnly = true;
            this.KamokuNm.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.KamokuNm.Width = 50;
            // 
            // Zandaka
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.Zandaka.DefaultCellStyle = dataGridViewCellStyle3;
            this.Zandaka.HeaderText = "勘 定 科 目 名";
            this.Zandaka.Name = "Zandaka";
            this.Zandaka.ReadOnly = true;
            this.Zandaka.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Zandaka.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Zandaka.Width = 190;
            // 
            // KarikataHassei
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.KarikataHassei.DefaultCellStyle = dataGridViewCellStyle4;
            this.KarikataHassei.HeaderText = "科 目 分 類 名";
            this.KarikataHassei.Name = "KarikataHassei";
            this.KarikataHassei.ReadOnly = true;
            this.KarikataHassei.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KarikataHassei.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.KarikataHassei.Width = 190;
            // 
            // KashikataHassei
            // 
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.KashikataHassei.DefaultCellStyle = dataGridViewCellStyle5;
            this.KashikataHassei.HeaderText = "ｺｰﾄﾞ";
            this.KashikataHassei.Name = "KashikataHassei";
            this.KashikataHassei.ReadOnly = true;
            this.KashikataHassei.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KashikataHassei.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.KashikataHassei.Width = 50;
            // 
            // TouZan
            // 
            dataGridViewCellStyle6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.TouZan.DefaultCellStyle = dataGridViewCellStyle6;
            this.TouZan.HeaderText = "部　門　名";
            this.TouZan.Name = "TouZan";
            this.TouZan.ReadOnly = true;
            this.TouZan.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TouZan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TouZan.Width = 190;
            // 
            // txtGridEdit
            // 
            this.txtGridEdit.AutoSizeFromLength = true;
            this.txtGridEdit.BackColor = System.Drawing.Color.White;
            this.txtGridEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGridEdit.DisplayLength = null;
            this.txtGridEdit.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGridEdit.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGridEdit.Location = new System.Drawing.Point(163, 10);
            this.txtGridEdit.Margin = new System.Windows.Forms.Padding(4);
            this.txtGridEdit.MaxLength = 10;
            this.txtGridEdit.Name = "txtGridEdit";
            this.txtGridEdit.Size = new System.Drawing.Size(101, 23);
            this.txtGridEdit.TabIndex = 904;
            this.txtGridEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGridEdit.Enter += new System.EventHandler(this.txtGridEdit_Enter);
            this.txtGridEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGridEdit_KeyDown);
            // 
            // txtFunanushiCd
            // 
            this.txtFunanushiCd.AutoSizeFromLength = true;
            this.txtFunanushiCd.DisplayLength = null;
            this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFunanushiCd.Location = new System.Drawing.Point(479, 17);
            this.txtFunanushiCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtFunanushiCd.MaxLength = 4;
            this.txtFunanushiCd.Name = "txtFunanushiCd";
            this.txtFunanushiCd.Size = new System.Drawing.Size(0, 20);
            this.txtFunanushiCd.TabIndex = 905;
            this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 39);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.114035F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.88596F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(968, 609);
            this.fsiTableLayoutPanel1.TabIndex = 906;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.dgvInputList);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 55);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(958, 549);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel6);
            this.fsiPanel1.Controls.Add(this.fsiPanel5);
            this.fsiPanel1.Controls.Add(this.fsiPanel4);
            this.fsiPanel1.Controls.Add(this.fsiPanel3);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(958, 41);
            this.fsiPanel1.TabIndex = 0;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtGridEdit);
            this.fsiPanel6.Controls.Add(this.lblJp);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel6.Location = new System.Drawing.Point(381, 0);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(473, 41);
            this.fsiPanel6.TabIndex = 4;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblGridViewTitle);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel5.Location = new System.Drawing.Point(188, 0);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(192, 41);
            this.fsiPanel5.TabIndex = 3;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblKaishaSettei);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel4.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(188, 41);
            this.fsiPanel4.TabIndex = 2;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.lblZei);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel3.Location = new System.Drawing.Point(854, 0);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(104, 41);
            this.fsiPanel3.TabIndex = 1;
            // 
            // ZMMR1052
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 789);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.txtFunanushiCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMMR1052";
            this.ShowFButton = true;
            this.Text = "";
            this.Shown += new System.EventHandler(this.ZAMR9012_Shown);
            this.Controls.SetChildIndex(this.txtFunanushiCd, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKaishaSettei;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblZei;
        private System.Windows.Forms.Label lblGridViewTitle;
        private System.Windows.Forms.DataGridView dgvInputList;
        private jp.co.fsi.common.controls.FsiTextBox txtGridEdit;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn Zandaka;
        private System.Windows.Forms.DataGridViewTextBoxColumn KarikataHassei;
        private System.Windows.Forms.DataGridViewTextBoxColumn KashikataHassei;
        private System.Windows.Forms.DataGridViewTextBoxColumn TouZan;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
    }
}