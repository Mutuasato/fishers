﻿using System;
using System.Collections;
using System.Data;
using System.Text;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1061
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1061DA
    {
        #region private変数
        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        public ZMMR1061DA(UserInfo uInfo, DbAccess dba)
        {
            this._uInfo = uInfo;
            this._dba = dba;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 部門集計データを取得
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>部門集計データ</returns>
        public DataTable GetBumonData(Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shiwakeShori = Util.ToInt(condition["ShiwakeShurui"]);
            string zeiKubun;
            if (Util.ToInt(condition["ShohizeiShoriHandan"]) == 1)
            {
                zeiKubun = "ZEIKOMI";
            }
            else
            {
                zeiKubun = "ZEINUKI";
            }
            int shohizeiShoriHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
            // 支所コード
            int shishoCd = Util.ToInt(condition["ShishoCode"]);
            decimal sCd = 0;
            string ssCd = "";

            Decimal zenZandaka;
            Decimal zandaka;
            string kamokuCd = "";
            string zenZandakaSt;
            string karikata;
            string kasikata;
            string zandakaSt;

            string zenZandakaKeiStr;
            string karikataKeiStr;
            string kasikataKeiStr;
            string zandakaKeiStr;

            // 合計金額
            Decimal zenZandakaKei = 0;
            Decimal karikataKei= 0;
            Decimal kashikataKei = 0;
            Decimal zandakaKei = 0;

            sql.AppendLine(" SELECT");
            sql.AppendLine("     A.SHISHO_CD            AS SHISHO_CD,");
            sql.AppendLine("     A.KANJO_KAMOKU_CD      AS KAMOKU_CD,");
            sql.AppendLine("     A.BUMON_CD             AS BUMON_CD,");
            sql.AppendLine("     MAX(B.KANJO_KAMOKU_NM) AS KAMOKU_NM,");
            sql.AppendLine("     MAX(C.BUMON_NM)        AS BUMON_NM,");

            sql.AppendLine("     SUM(CASE WHEN A.DENPYO_DATE < CAST(@DATE_FR AS DATETIME) THEN ");
            sql.AppendLine("              CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN A." + zeiKubun + "_KINGAKU ELSE A." + zeiKubun + "_KINGAKU * -1 END ");
            sql.AppendLine("         ELSE 0 ");
            sql.AppendLine("         END) AS ZEN_ZAN,");

            sql.AppendLine("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DATE_FR AS DATETIME) THEN ");
            sql.AppendLine("              CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A." + zeiKubun + "_KINGAKU ELSE 0 END ");
            sql.AppendLine("         ELSE 0 ");
            sql.AppendLine("         END)  AS KARIKATA,");

            sql.AppendLine("     SUM(CASE WHEN A.DENPYO_DATE >= CAST(@DATE_FR AS DATETIME) THEN ");
            sql.AppendLine("              CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A." + zeiKubun + "_KINGAKU ELSE 0 END ");
            sql.AppendLine("         ELSE 0 ");
            sql.AppendLine("         END)  AS KASHIKATA,");

            sql.AppendLine("     SUM(CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN A." + zeiKubun + "_KINGAKU ELSE A." + zeiKubun + "_KINGAKU * -1 END) ");
            sql.AppendLine("              AS ZANDAKA");

            sql.AppendLine(" FROM");
            sql.AppendLine("     TB_ZM_SHIWAKE_MEISAI AS A ");
            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ");
            sql.AppendLine("     ON (A.KAISHA_CD     = B.KAISHA_CD) AND (A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO) ");
            sql.AppendLine(" LEFT OUTER JOIN TB_CM_BUMON AS C ");
            sql.AppendLine("     ON (A.KAISHA_CD = C.KAISHA_CD) AND (A.BUMON_CD = C.BUMON_CD) ");
            sql.AppendLine(" WHERE");
            sql.AppendLine("     A.KAISHA_CD = @KAISHA_CD AND");

            if (shishoCd != 0)
                sql.AppendLine("     A.SHISHO_CD = @SHISHO_CD AND");

            sql.AppendLine("     A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.AppendLine("     A.KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_FR  AND @KANJO_KAMOKU_TO AND");
            sql.AppendLine("     A.BUMON_CD BETWEEN @BUMON_FR  AND @BUMON_TO AND");
            sql.AppendLine("     A.DENPYO_DATE <= CAST(@DATE_TO AS DATETIME)  AND");
            //sql.AppendLine("     A.MEISAI_KUBUN <= 0 AND");
            if (shohizeiShoriHandan == 1)
            {
                sql.AppendLine(" A.MEISAI_KUBUN <= 0 AND");
            }
            else
            {
                sql.AppendLine(" A.MEISAI_KUBUN >= 0 AND");
            }
            //sql.AppendLine("     A.KESSAN_KUBUN = 0 AND");
            sql.AppendLine("     B.BUMON_UMU = 1");
            if (Util.ToInt(condition["ShiwakeShurui"]) != 9)
            {
                sql.AppendLine(" AND    A.KESSAN_KUBUN = " + condition["ShiwakeShurui"]);
            }
            sql.AppendLine(" GROUP BY");

            sql.AppendLine("     A.SHISHO_CD,");
            sql.AppendLine("     A.KANJO_KAMOKU_CD,");
            sql.AppendLine("     A.BUMON_CD");
            sql.AppendLine(" ORDER BY");
            sql.AppendLine("     A.SHISHO_CD,");
            sql.AppendLine("     A.KANJO_KAMOKU_CD,");
            sql.AppendLine("     A.BUMON_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@SHIWAKE_SHORI", SqlDbType.Decimal, 1, shiwakeShori);
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, Util.ToDate(condition["DtFr"]));
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, Util.ToDate(condition["DtTo"]));
            dpc.SetParam("@KANJO_KAMOKU_FR", SqlDbType.VarChar, 6, Util.ToDecimal(Util.ToString(condition["KanjoKamokuFr"])));
            dpc.SetParam("@KANJO_KAMOKU_TO", SqlDbType.VarChar, 6, Util.ToDecimal(Util.ToString(condition["KanjoKamokuTo"])));
            dpc.SetParam("@HOJO_KAMOKU_FR", SqlDbType.VarChar, 10, Util.ToDecimal(Util.ToString(condition["HojoKamokuFr"])));
            dpc.SetParam("@HOJO_KAMOKU_TO", SqlDbType.VarChar, 10, Util.ToDecimal(Util.ToString(condition["HojoKamokuTo"])));
            dpc.SetParam("@BUMON_FR", SqlDbType.VarChar, 4, Util.ToString(condition["BumonFr"]));
            dpc.SetParam("@BUMON_TO", SqlDbType.VarChar, 4, Util.ToString(condition["BumonTo"]));

            DataTable dtBumonKamokuData = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            //計算結果を格納するテーブル
            DataTable dtShukeiData = new DataTable();
            dtShukeiData.Columns.Add("コード");
            dtShukeiData.Columns.Add("勘定科目名");
            dtShukeiData.Columns.Add("前月残高");
            dtShukeiData.Columns.Add("借方");
            dtShukeiData.Columns.Add("貸方");
            dtShukeiData.Columns.Add("残高");
            dtShukeiData.Columns.Add("FLG");
            dtShukeiData.Columns.Add("SHISHO_CD", typeof(decimal));
            foreach (DataRow dt in dtBumonKamokuData.Rows)
            {
                // 科目合計を挿入
                if (!ValChk.IsEmpty(kamokuCd) && (!kamokuCd.Equals(Util.ToString(dt["KAMOKU_CD"])) || !ssCd.Equals(Util.ToString(dt["SHISHO_CD"]))))
                {
                    // 金額が0の場合、非表示とする
                    zenZandakaKeiStr = Util.FormatNum(zenZandakaKei);
                    karikataKeiStr = Util.FormatNum(karikataKei);
                    kasikataKeiStr = Util.FormatNum(kashikataKei);
                    zandakaKeiStr = Util.FormatNum(zandakaKei);
                    if (zenZandakaKei == 0)
                    {
                        zenZandakaKeiStr = "";
                    }
                    if (karikataKei == 0)
                    {
                        karikataKeiStr = "";
                    }
                    if (kashikataKei == 0)
                    {
                        kasikataKeiStr = "";
                    }
                    if (zandakaKei == 0)
                    {
                        zandakaKeiStr = "";
                    }
                    dtShukeiData.Rows.Add("",
                        "合計",
                        zenZandakaKeiStr,
                        karikataKeiStr,
                        kasikataKeiStr,
                        zandakaKeiStr,
                        "",
                        sCd);
                    zenZandakaKei = 0;
                    karikataKei = 0;
                    kashikataKei = 0;
                    zandakaKei = 0;
                    // 支所コードの保持
                    sCd = Util.ToDecimal(Util.ToString(dt["SHISHO_CD"]));
                }
                // 行の始めに勘定科目名を挿入
                if (ValChk.IsEmpty(kamokuCd) || (!kamokuCd.Equals(Util.ToString(dt["KAMOKU_CD"])) || !ssCd.Equals(Util.ToString(dt["SHISHO_CD"]))))
                {
                    // 支所コードの保持
                    sCd = Util.ToDecimal(Util.ToString(dt["SHISHO_CD"]));
                    dtShukeiData.Rows.Add(dt["KAMOKU_CD"],
                        dt["KAMOKU_NM"],
                        "",
                        "",
                        "",
                        "",
                        "1",
                        sCd);
                }

                // データの設定
                zenZandaka = Util.ToDecimal(dt["ZEN_ZAN"]);
                zenZandakaSt = Util.ToString(zenZandaka);
                if (zenZandakaSt == "0")
                {
                    zenZandakaSt = "";
                }
                if (Util.ToDecimal(dt["KARIKATA"]) == 0)
                {
                    karikata = "";
                }
                else
                {
                    karikata = Util.ToString(dt["KARIKATA"]);
                }
                if (Util.ToDecimal(dt["KASHIKATA"]) == 0)
                {
                    kasikata = "";
                }
                else
                {
                    kasikata = Util.ToString(dt["KASHIKATA"]);
                }
                zandaka = Util.ToDecimal(dt["ZANDAKA"]);
                zandakaSt = Util.ToString(zandaka);
                if (zandakaSt == "0")
                {
                    zandakaSt = "";
                }
                // データの追加
                if (Util.ToString(condition["Inji"]) == "no")
                {
                    if (zenZandakaSt != "" || karikata != "" || kasikata != "" || zandakaSt != "")
                    {
                        dtShukeiData.Rows.Add(dt["BUMON_CD"],
                            dt["BUMON_NM"],
                            Util.FormatNum(zenZandakaSt),
                            Util.FormatNum(karikata),
                            Util.FormatNum(kasikata),
                            Util.FormatNum(zandakaSt),
                            "",
                            sCd);
                    }
                }
                else
                {
                    dtShukeiData.Rows.Add(dt["BUMON_CD"],
                        dt["BUMON_NM"],
                        Util.FormatNum(zenZandakaSt),
                        Util.FormatNum(karikata),
                        Util.FormatNum(kasikata),
                        Util.FormatNum(zandakaSt),
                        "",
                        sCd);
                }

                // 科目コードの保持
                kamokuCd = Util.ToString(dt["KAMOKU_CD"]);
                // 支所コードの保持
                ssCd = Util.ToString(dt["SHISHO_CD"]);

                // 合計金額の計算
                zenZandakaKei += zenZandaka;
                karikataKei += Util.ToDecimal(dt["KARIKATA"]);
                kashikataKei += Util.ToDecimal(dt["KASHIKATA"]);
                zandakaKei += zandaka;
            }
            if (dtShukeiData.Rows.Count > 0)
            {
                // ループ終了後に合計を挿入
                // 金額が0の場合、非表示とする
                zenZandakaKeiStr = Util.FormatNum(zenZandakaKei);
                karikataKeiStr = Util.FormatNum(karikataKei);
                kasikataKeiStr = Util.FormatNum(kashikataKei);
                zandakaKeiStr = Util.FormatNum(zandakaKei);
                if (zenZandakaKei == 0)
                {
                    zenZandakaKeiStr = "";
                }
                if (karikataKei == 0)
                {
                    karikataKeiStr = "";
                }
                if (kashikataKei == 0)
                {
                    kasikataKeiStr = "";
                }
                if (zandakaKei == 0)
                {
                    zandakaKeiStr = "";
                }
                dtShukeiData.Rows.Add("",
                    "合計",
                    zenZandakaKeiStr,
                    karikataKeiStr,
                    kasikataKeiStr,
                    zandakaKeiStr,
                    "",
                    sCd);
            }

            return dtShukeiData;
        }
        #endregion
    }
}
