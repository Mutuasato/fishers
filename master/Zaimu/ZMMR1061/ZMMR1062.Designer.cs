﻿namespace jp.co.fsi.zm.zmmr1061
{
    partial class ZMMR1062
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblCdKei = new System.Windows.Forms.Label();
            this.lblSoGokei = new System.Windows.Forms.Label();
            this.lblZenZanKei = new System.Windows.Forms.Label();
            this.lblKarikataKei = new System.Windows.Forms.Label();
            this.lblKashikataKei = new System.Windows.Forms.Label();
            this.lblZandakaKei = new System.Windows.Forms.Label();
            this.lblJp = new System.Windows.Forms.Label();
            this.lblZei = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel11.SuspendLayout();
            this.fsiPanel10.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 431);
            this.pnlDebug.Size = new System.Drawing.Size(833, 100);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Size = new System.Drawing.Size(825, 23);
            this.lblTitle.Text = "";
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvList.EnableHeadersVisualStyles = false;
            this.dgvList.Location = new System.Drawing.Point(0, 0);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.MenuHighlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(793, 323);
            this.dgvList.TabIndex = 1;
            // 
            // lblCdKei
            // 
            this.lblCdKei.BackColor = System.Drawing.Color.Silver;
            this.lblCdKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCdKei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCdKei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCdKei.Location = new System.Drawing.Point(0, 0);
            this.lblCdKei.Name = "lblCdKei";
            this.lblCdKei.Size = new System.Drawing.Size(157, 30);
            this.lblCdKei.TabIndex = 2;
            this.lblCdKei.Tag = "CHANGE";
            // 
            // lblSoGokei
            // 
            this.lblSoGokei.BackColor = System.Drawing.Color.Silver;
            this.lblSoGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSoGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSoGokei.Location = new System.Drawing.Point(0, 0);
            this.lblSoGokei.Name = "lblSoGokei";
            this.lblSoGokei.Size = new System.Drawing.Size(154, 30);
            this.lblSoGokei.TabIndex = 3;
            this.lblSoGokei.Tag = "CHANGE";
            this.lblSoGokei.Text = "総 合 計";
            this.lblSoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblZenZanKei
            // 
            this.lblZenZanKei.BackColor = System.Drawing.Color.LightCyan;
            this.lblZenZanKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblZenZanKei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZenZanKei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZenZanKei.Location = new System.Drawing.Point(0, 0);
            this.lblZenZanKei.Name = "lblZenZanKei";
            this.lblZenZanKei.Size = new System.Drawing.Size(121, 30);
            this.lblZenZanKei.TabIndex = 4;
            this.lblZenZanKei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKarikataKei
            // 
            this.lblKarikataKei.BackColor = System.Drawing.Color.LightCyan;
            this.lblKarikataKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKarikataKei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKarikataKei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKarikataKei.Location = new System.Drawing.Point(0, 0);
            this.lblKarikataKei.Name = "lblKarikataKei";
            this.lblKarikataKei.Size = new System.Drawing.Size(121, 30);
            this.lblKarikataKei.TabIndex = 5;
            this.lblKarikataKei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashikataKei
            // 
            this.lblKashikataKei.BackColor = System.Drawing.Color.LightCyan;
            this.lblKashikataKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashikataKei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKashikataKei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKashikataKei.Location = new System.Drawing.Point(0, 0);
            this.lblKashikataKei.Name = "lblKashikataKei";
            this.lblKashikataKei.Size = new System.Drawing.Size(121, 30);
            this.lblKashikataKei.TabIndex = 6;
            this.lblKashikataKei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblZandakaKei
            // 
            this.lblZandakaKei.BackColor = System.Drawing.Color.LightCyan;
            this.lblZandakaKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblZandakaKei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZandakaKei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblZandakaKei.Location = new System.Drawing.Point(0, 0);
            this.lblZandakaKei.Name = "lblZandakaKei";
            this.lblZandakaKei.Size = new System.Drawing.Size(121, 30);
            this.lblZandakaKei.TabIndex = 7;
            this.lblZandakaKei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblJp
            // 
            this.lblJp.BackColor = System.Drawing.Color.Silver;
            this.lblJp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblJp.ForeColor = System.Drawing.Color.Black;
            this.lblJp.Location = new System.Drawing.Point(0, 0);
            this.lblJp.Name = "lblJp";
            this.lblJp.Size = new System.Drawing.Size(734, 18);
            this.lblJp.TabIndex = 908;
            this.lblJp.Tag = "CHANGE";
            this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblZei
            // 
            this.lblZei.BackColor = System.Drawing.Color.Silver;
            this.lblZei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblZei.ForeColor = System.Drawing.Color.Black;
            this.lblZei.Location = new System.Drawing.Point(0, 0);
            this.lblZei.Name = "lblZei";
            this.lblZei.Size = new System.Drawing.Size(59, 18);
            this.lblZei.TabIndex = 913;
            this.lblZei.Tag = "CHANGE";
            this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(9, 26);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(801, 356);
            this.fsiTableLayoutPanel1.TabIndex = 914;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.dgvList);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 29);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(793, 323);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel5);
            this.fsiPanel1.Controls.Add(this.fsiPanel4);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(793, 18);
            this.fsiPanel1.TabIndex = 0;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblJp);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(734, 18);
            this.fsiPanel5.TabIndex = 3;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblZei);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel4.Location = new System.Drawing.Point(734, 0);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(59, 18);
            this.fsiPanel4.TabIndex = 2;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel3, 0, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(9, 384);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 1;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(801, 38);
            this.fsiTableLayoutPanel2.TabIndex = 915;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.fsiPanel11);
            this.fsiPanel3.Controls.Add(this.fsiPanel10);
            this.fsiPanel3.Controls.Add(this.fsiPanel9);
            this.fsiPanel3.Controls.Add(this.fsiPanel8);
            this.fsiPanel3.Controls.Add(this.fsiPanel7);
            this.fsiPanel3.Controls.Add(this.fsiPanel6);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(793, 30);
            this.fsiPanel3.TabIndex = 0;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel11
            // 
            this.fsiPanel11.Controls.Add(this.lblZenZanKei);
            this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel11.Location = new System.Drawing.Point(309, 0);
            this.fsiPanel11.Name = "fsiPanel11";
            this.fsiPanel11.Size = new System.Drawing.Size(121, 30);
            this.fsiPanel11.TabIndex = 6;
            this.fsiPanel11.Tag = "CHANGE";
            // 
            // fsiPanel10
            // 
            this.fsiPanel10.Controls.Add(this.lblKarikataKei);
            this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel10.Location = new System.Drawing.Point(430, 0);
            this.fsiPanel10.Name = "fsiPanel10";
            this.fsiPanel10.Size = new System.Drawing.Size(121, 30);
            this.fsiPanel10.TabIndex = 5;
            this.fsiPanel10.Tag = "CHANGE";
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.lblKashikataKei);
            this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel9.Location = new System.Drawing.Point(551, 0);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(121, 30);
            this.fsiPanel9.TabIndex = 4;
            this.fsiPanel9.Tag = "CHANGE";
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.lblSoGokei);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel8.Location = new System.Drawing.Point(157, 0);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(154, 30);
            this.fsiPanel8.TabIndex = 3;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.lblCdKei);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel7.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(157, 30);
            this.fsiPanel7.TabIndex = 2;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.lblZandakaKei);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel6.Location = new System.Drawing.Point(672, 0);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(121, 30);
            this.fsiPanel6.TabIndex = 1;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // ZMMR1062
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 534);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMMR1062";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel11.ResumeLayout(false);
            this.fsiPanel10.ResumeLayout(false);
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblCdKei;
        private System.Windows.Forms.Label lblSoGokei;
        private System.Windows.Forms.Label lblZenZanKei;
        private System.Windows.Forms.Label lblKarikataKei;
        private System.Windows.Forms.Label lblKashikataKei;
        private System.Windows.Forms.Label lblZandakaKei;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblZei;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel10;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
    }
}