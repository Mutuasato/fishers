﻿namespace jp.co.fsi.zm.zmdr1021
{
    /// <summary>
    /// ZAMR1051R の概要の説明です。
    /// </summary>
    partial class ZMDR10211R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMDR10211R));
			this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtTitle11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTitle12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblIn01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIn02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIn03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIn04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIn05 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtValue08Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblKarikataGokei = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblKashikataGokei = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtValue14Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn03)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn04)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn05)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZei)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue08)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue09)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue08Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblKarikataGokei)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblKashikataGokei)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue14Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTitle11,
            this.line2,
            this.txtTitle12,
            this.txtTitle10,
            this.txtTitle09,
            this.txtTitle08,
            this.txtTitle07,
            this.txtTitle06,
            this.txtTitle05,
            this.txtTitle04,
            this.txtTitle01,
            this.txtTitle03,
            this.txtTitle02,
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtCompanyName,
            this.txtTitleName,
            this.line1,
            this.lblIn01,
            this.lblIn02,
            this.lblIn03,
            this.lblIn04,
            this.lblIn05,
            this.txtDate,
            this.line3,
            this.line6,
            this.line4,
            this.line5,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.line28,
            this.txtZei});
			this.pageHeader.Height = 1.682497F;
			this.pageHeader.Name = "pageHeader";
			this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
			this.pageHeader.AfterPrint += new System.EventHandler(this.pageHeader_AfterPrint);
			// 
			// txtTitle11
			// 
			this.txtTitle11.Height = 0.3937008F;
			this.txtTitle11.Left = 4.96063F;
			this.txtTitle11.LineSpacing = 3F;
			this.txtTitle11.Name = "txtTitle11";
			this.txtTitle11.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-we" +
    "ight: bold; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.txtTitle11.Text = "金　額";
			this.txtTitle11.Top = 1.295669F;
			this.txtTitle11.Width = 0.8031502F;
			// 
			// line2
			// 
			this.line2.Height = 0F;
			this.line2.Left = 4.251969F;
			this.line2.LineWeight = 1F;
			this.line2.Name = "line2";
			this.line2.Top = 0.4015748F;
			this.line2.Width = 3.228346F;
			this.line2.X1 = 4.251969F;
			this.line2.X2 = 7.480315F;
			this.line2.Y1 = 0.4015748F;
			this.line2.Y2 = 0.4015748F;
			// 
			// txtTitle12
			// 
			this.txtTitle12.Height = 0.5905511F;
			this.txtTitle12.Left = 5.748032F;
			this.txtTitle12.Name = "txtTitle12";
			this.txtTitle12.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.txtTitle12.Text = "摘　　　要";
			this.txtTitle12.Top = 1.098819F;
			this.txtTitle12.Width = 1.732284F;
			// 
			// txtTitle10
			// 
			this.txtTitle10.Height = 0.3937008F;
			this.txtTitle10.Left = 4.76378F;
			this.txtTitle10.LineSpacing = 3F;
			this.txtTitle10.Name = "txtTitle10";
			this.txtTitle10.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; te" +
    "xt-align: center; vertical-align: middle; ddo-char-set: 128";
			this.txtTitle10.Text = "税";
			this.txtTitle10.Top = 1.295669F;
			this.txtTitle10.Width = 0.1929135F;
			// 
			// txtTitle09
			// 
			this.txtTitle09.Height = 0.3937008F;
			this.txtTitle09.Left = 4.496063F;
			this.txtTitle09.LineSpacing = 3F;
			this.txtTitle09.Name = "txtTitle09";
			this.txtTitle09.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; te" +
    "xt-align: center; vertical-align: middle; ddo-char-set: 128";
			this.txtTitle09.Text = "部門";
			this.txtTitle09.Top = 1.295669F;
			this.txtTitle09.Width = 0.2637796F;
			// 
			// txtTitle08
			// 
			this.txtTitle08.CharacterSpacing = 2F;
			this.txtTitle08.Height = 0.3937007F;
			this.txtTitle08.Left = 3.114173F;
			this.txtTitle08.LineSpacing = 3F;
			this.txtTitle08.Name = "txtTitle08";
			this.txtTitle08.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; te" +
    "xt-align: center; vertical-align: middle; ddo-char-set: 128";
			this.txtTitle08.Text = "勘定科目\r\n補助科目";
			this.txtTitle08.Top = 1.295669F;
			this.txtTitle08.Width = 1.38622F;
			// 
			// txtTitle07
			// 
			this.txtTitle07.Height = 0.1968504F;
			this.txtTitle07.Left = 3.088977F;
			this.txtTitle07.Name = "txtTitle07";
			this.txtTitle07.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: bottom; ddo-char-set: 128";
			this.txtTitle07.Text = "貸　　　方";
			this.txtTitle07.Top = 1.098819F;
			this.txtTitle07.Width = 2.674803F;
			// 
			// txtTitle06
			// 
			this.txtTitle06.Height = 0.3937008F;
			this.txtTitle06.Left = 2.330709F;
			this.txtTitle06.LineSpacing = 3F;
			this.txtTitle06.Name = "txtTitle06";
			this.txtTitle06.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; te" +
    "xt-align: center; vertical-align: middle; ddo-char-set: 128";
			this.txtTitle06.Text = "金　額";
			this.txtTitle06.Top = 1.295669F;
			this.txtTitle06.Width = 0.7874016F;
			// 
			// txtTitle05
			// 
			this.txtTitle05.Height = 0.3937008F;
			this.txtTitle05.Left = 2.127559F;
			this.txtTitle05.LineSpacing = 3F;
			this.txtTitle05.Name = "txtTitle05";
			this.txtTitle05.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; te" +
    "xt-align: center; vertical-align: middle; ddo-char-set: 128";
			this.txtTitle05.Text = "税";
			this.txtTitle05.Top = 1.295669F;
			this.txtTitle05.Width = 0.1968504F;
			// 
			// txtTitle04
			// 
			this.txtTitle04.Height = 0.3937008F;
			this.txtTitle04.Left = 1.863779F;
			this.txtTitle04.LineSpacing = 3F;
			this.txtTitle04.Name = "txtTitle04";
			this.txtTitle04.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; te" +
    "xt-align: center; vertical-align: middle; ddo-char-set: 128";
			this.txtTitle04.Text = "部門";
			this.txtTitle04.Top = 1.295669F;
			this.txtTitle04.Width = 0.2637796F;
			// 
			// txtTitle01
			// 
			this.txtTitle01.Height = 0.5948818F;
			this.txtTitle01.Left = 0F;
			this.txtTitle01.LineSpacing = 5F;
			this.txtTitle01.Name = "txtTitle01";
			this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
			this.txtTitle01.Text = "伝票日付\r\n伝票番号";
			this.txtTitle01.Top = 1.094488F;
			this.txtTitle01.Width = 0.5389764F;
			// 
			// txtTitle03
			// 
			this.txtTitle03.CharacterSpacing = 2F;
			this.txtTitle03.Height = 0.3937008F;
			this.txtTitle03.Left = 0.5389764F;
			this.txtTitle03.LineSpacing = 3F;
			this.txtTitle03.Name = "txtTitle03";
			this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; te" +
    "xt-align: center; vertical-align: middle; ddo-char-set: 128";
			this.txtTitle03.Text = "勘定科目\r\n補助科目";
			this.txtTitle03.Top = 1.295669F;
			this.txtTitle03.Width = 1.324803F;
			// 
			// txtTitle02
			// 
			this.txtTitle02.Height = 0.1968504F;
			this.txtTitle02.Left = 0.5389764F;
			this.txtTitle02.Name = "txtTitle02";
			this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: bottom";
			this.txtTitle02.Text = "借　　　方";
			this.txtTitle02.Top = 1.098819F;
			this.txtTitle02.Width = 2.575197F;
			// 
			// txtToday
			// 
			this.txtToday.DataField = "ITEM19";
			this.txtToday.Height = 0.1968504F;
			this.txtToday.Left = 5.796851F;
			this.txtToday.MultiLine = false;
			this.txtToday.Name = "txtToday";
			this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
			this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.txtToday.Text = "yyyy/MM/dd";
			this.txtToday.Top = 0F;
			this.txtToday.Width = 1.181102F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1968504F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 7.325198F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.lblPage.Text = "頁";
			this.lblPage.Top = 0F;
			this.lblPage.Width = 0.1590552F;
			// 
			// txtPageCount
			// 
			this.txtPageCount.Height = 0.1968504F;
			this.txtPageCount.Left = 7.00945F;
			this.txtPageCount.MultiLine = false;
			this.txtPageCount.Name = "txtPageCount";
			this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.txtPageCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
			this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPageCount.Text = "999";
			this.txtPageCount.Top = 0F;
			this.txtPageCount.Width = 0.2952756F;
			// 
			// txtCompanyName
			// 
			this.txtCompanyName.DataField = "ITEM01";
			this.txtCompanyName.Height = 0.1968504F;
			this.txtCompanyName.Left = 0F;
			this.txtCompanyName.MultiLine = false;
			this.txtCompanyName.Name = "txtCompanyName";
			this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 1";
			this.txtCompanyName.Text = null;
			this.txtCompanyName.Top = 0.7291339F;
			this.txtCompanyName.Width = 2.800788F;
			// 
			// txtTitleName
			// 
			this.txtTitleName.Height = 0.2874016F;
			this.txtTitleName.Left = 1.551575F;
			this.txtTitleName.MultiLine = false;
			this.txtTitleName.Name = "txtTitleName";
			this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle; ddo-char-set: 1";
			this.txtTitleName.Text = "仕　訳　帳";
			this.txtTitleName.Top = 0.1968504F;
			this.txtTitleName.Width = 1.941732F;
			// 
			// line1
			// 
			this.line1.Height = 0F;
			this.line1.Left = 1.551575F;
			this.line1.LineWeight = 1F;
			this.line1.Name = "line1";
			this.line1.Top = 0.484252F;
			this.line1.Width = 1.941732F;
			this.line1.X1 = 1.551575F;
			this.line1.X2 = 3.493307F;
			this.line1.Y1 = 0.484252F;
			this.line1.Y2 = 0.484252F;
			// 
			// lblIn01
			// 
			this.lblIn01.Height = 0.472441F;
			this.lblIn01.HyperLink = null;
			this.lblIn01.Left = 4.251969F;
			this.lblIn01.Name = "lblIn01";
			this.lblIn01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; t" +
    "ext-justify: auto; vertical-align: bottom; ddo-char-set: 128; ddo-font-vertical:" +
    " true";
			this.lblIn01.Text = "組合長";
			this.lblIn01.Top = 0.3937008F;
			this.lblIn01.Width = 0.1968504F;
			// 
			// lblIn02
			// 
			this.lblIn02.Height = 0.472441F;
			this.lblIn02.HyperLink = null;
			this.lblIn02.Left = 4.889764F;
			this.lblIn02.LineSpacing = 10F;
			this.lblIn02.Name = "lblIn02";
			this.lblIn02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; t" +
    "ext-justify: auto; vertical-align: bottom; ddo-char-set: 128; ddo-font-vertical:" +
    " true";
			this.lblIn02.Text = "課長";
			this.lblIn02.Top = 0.3937008F;
			this.lblIn02.Width = 0.1968504F;
			// 
			// lblIn03
			// 
			this.lblIn03.Height = 0.472441F;
			this.lblIn03.HyperLink = null;
			this.lblIn03.Left = 5.511811F;
			this.lblIn03.LineSpacing = 10F;
			this.lblIn03.Name = "lblIn03";
			this.lblIn03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; text-justify: auto; ve" +
    "rtical-align: bottom; white-space: inherit; ddo-char-set: 1; ddo-font-vertical: " +
    "true; ddo-wrap-mode: inherit";
			this.lblIn03.Text = "係長";
			this.lblIn03.Top = 0.3937008F;
			this.lblIn03.Width = 0.1968504F;
			// 
			// lblIn04
			// 
			this.lblIn04.Height = 0.472441F;
			this.lblIn04.HyperLink = null;
			this.lblIn04.Left = 6.141733F;
			this.lblIn04.LineSpacing = 10F;
			this.lblIn04.Name = "lblIn04";
			this.lblIn04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; text-justify: auto; ve" +
    "rtical-align: bottom; white-space: inherit; ddo-char-set: 1; ddo-font-vertical: " +
    "true; ddo-wrap-mode: inherit";
			this.lblIn04.Text = "照査";
			this.lblIn04.Top = 0.3937008F;
			this.lblIn04.Width = 0.1968504F;
			// 
			// lblIn05
			// 
			this.lblIn05.Height = 0.472441F;
			this.lblIn05.HyperLink = null;
			this.lblIn05.Left = 6.811023F;
			this.lblIn05.LineSpacing = 10F;
			this.lblIn05.Name = "lblIn05";
			this.lblIn05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; text-justify: auto; ve" +
    "rtical-align: bottom; white-space: inherit; ddo-char-set: 1; ddo-font-vertical: " +
    "true; ddo-wrap-mode: inherit";
			this.lblIn05.Text = "起票";
			this.lblIn05.Top = 0.3937008F;
			this.lblIn05.Width = 0.1968504F;
			// 
			// txtDate
			// 
			this.txtDate.DataField = "ITEM02";
			this.txtDate.Height = 0.1968504F;
			this.txtDate.Left = 1.227559F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.484252F;
			this.txtDate.Width = 2.624803F;
			// 
			// line3
			// 
			this.line3.Height = 0F;
			this.line3.Left = 4.251969F;
			this.line3.LineWeight = 1F;
			this.line3.Name = "line3";
			this.line3.Top = 0.8740158F;
			this.line3.Width = 3.232281F;
			this.line3.X1 = 4.251969F;
			this.line3.X2 = 7.48425F;
			this.line3.Y1 = 0.8740158F;
			this.line3.Y2 = 0.8740158F;
			// 
			// line6
			// 
			this.line6.Height = 0.4724409F;
			this.line6.Left = 4.251969F;
			this.line6.LineWeight = 1F;
			this.line6.Name = "line6";
			this.line6.Top = 0.4015748F;
			this.line6.Width = 0F;
			this.line6.X1 = 4.251969F;
			this.line6.X2 = 4.251969F;
			this.line6.Y1 = 0.4015748F;
			this.line6.Y2 = 0.8740157F;
			// 
			// line4
			// 
			this.line4.Height = 0.4724409F;
			this.line4.Left = 4.448819F;
			this.line4.LineWeight = 1F;
			this.line4.Name = "line4";
			this.line4.Top = 0.4015748F;
			this.line4.Width = 0F;
			this.line4.X1 = 4.448819F;
			this.line4.X2 = 4.448819F;
			this.line4.Y1 = 0.4015748F;
			this.line4.Y2 = 0.8740157F;
			// 
			// line5
			// 
			this.line5.Height = 0.472441F;
			this.line5.Left = 4.889764F;
			this.line5.LineWeight = 1F;
			this.line5.Name = "line5";
			this.line5.Top = 0.4015748F;
			this.line5.Width = 0F;
			this.line5.X1 = 4.889764F;
			this.line5.X2 = 4.889764F;
			this.line5.Y1 = 0.4015748F;
			this.line5.Y2 = 0.8740158F;
			// 
			// line7
			// 
			this.line7.Height = 0.472441F;
			this.line7.Left = 5.086615F;
			this.line7.LineWeight = 1F;
			this.line7.Name = "line7";
			this.line7.Top = 0.4015748F;
			this.line7.Width = 0F;
			this.line7.X1 = 5.086615F;
			this.line7.X2 = 5.086615F;
			this.line7.Y1 = 0.4015748F;
			this.line7.Y2 = 0.8740158F;
			// 
			// line8
			// 
			this.line8.Height = 0.4724409F;
			this.line8.Left = 5.517323F;
			this.line8.LineWeight = 1F;
			this.line8.Name = "line8";
			this.line8.Top = 0.4015748F;
			this.line8.Width = 0F;
			this.line8.X1 = 5.517323F;
			this.line8.X2 = 5.517323F;
			this.line8.Y1 = 0.4015748F;
			this.line8.Y2 = 0.8740157F;
			// 
			// line9
			// 
			this.line9.Height = 0.4724409F;
			this.line9.Left = 5.714173F;
			this.line9.LineWeight = 1F;
			this.line9.Name = "line9";
			this.line9.Top = 0.4015748F;
			this.line9.Width = 0F;
			this.line9.X1 = 5.714173F;
			this.line9.X2 = 5.714173F;
			this.line9.Y1 = 0.4015748F;
			this.line9.Y2 = 0.8740157F;
			// 
			// line10
			// 
			this.line10.Height = 0.4724409F;
			this.line10.Left = 6.141734F;
			this.line10.LineWeight = 1F;
			this.line10.Name = "line10";
			this.line10.Top = 0.4015748F;
			this.line10.Width = 0F;
			this.line10.X1 = 6.141734F;
			this.line10.X2 = 6.141734F;
			this.line10.Y1 = 0.4015748F;
			this.line10.Y2 = 0.8740157F;
			// 
			// line11
			// 
			this.line11.Height = 0.4724409F;
			this.line11.Left = 6.338583F;
			this.line11.LineWeight = 1F;
			this.line11.Name = "line11";
			this.line11.Top = 0.4015748F;
			this.line11.Width = 0F;
			this.line11.X1 = 6.338583F;
			this.line11.X2 = 6.338583F;
			this.line11.Y1 = 0.4015748F;
			this.line11.Y2 = 0.8740157F;
			// 
			// line12
			// 
			this.line12.Height = 0.472441F;
			this.line12.Left = 6.811024F;
			this.line12.LineWeight = 1F;
			this.line12.Name = "line12";
			this.line12.Top = 0.4015749F;
			this.line12.Width = 0F;
			this.line12.X1 = 6.811024F;
			this.line12.X2 = 6.811024F;
			this.line12.Y1 = 0.4015749F;
			this.line12.Y2 = 0.8740159F;
			// 
			// line13
			// 
			this.line13.Height = 0.472441F;
			this.line13.Left = 7.007874F;
			this.line13.LineWeight = 1F;
			this.line13.Name = "line13";
			this.line13.Top = 0.4015749F;
			this.line13.Width = 0F;
			this.line13.X1 = 7.007874F;
			this.line13.X2 = 7.007874F;
			this.line13.Y1 = 0.4015749F;
			this.line13.Y2 = 0.8740159F;
			// 
			// line14
			// 
			this.line14.Height = 0.4724409F;
			this.line14.Left = 7.480316F;
			this.line14.LineWeight = 1F;
			this.line14.Name = "line14";
			this.line14.Top = 0.4015748F;
			this.line14.Width = 0F;
			this.line14.X1 = 7.480316F;
			this.line14.X2 = 7.480316F;
			this.line14.Y1 = 0.4015748F;
			this.line14.Y2 = 0.8740157F;
			// 
			// line15
			// 
			this.line15.Height = 0F;
			this.line15.Left = 0F;
			this.line15.LineWeight = 1F;
			this.line15.Name = "line15";
			this.line15.Top = 1.098819F;
			this.line15.Width = 7.484252F;
			this.line15.X1 = 0F;
			this.line15.X2 = 7.484252F;
			this.line15.Y1 = 1.098819F;
			this.line15.Y2 = 1.098819F;
			// 
			// line16
			// 
			this.line16.Height = 0F;
			this.line16.Left = 0F;
			this.line16.LineWeight = 1F;
			this.line16.Name = "line16";
			this.line16.Top = 1.68937F;
			this.line16.Width = 7.48425F;
			this.line16.X1 = 0F;
			this.line16.X2 = 7.48425F;
			this.line16.Y1 = 1.68937F;
			this.line16.Y2 = 1.68937F;
			// 
			// line17
			// 
			this.line17.Height = 0F;
			this.line17.Left = 0.5389764F;
			this.line17.LineWeight = 1F;
			this.line17.Name = "line17";
			this.line17.Top = 1.295669F;
			this.line17.Width = 5.209056F;
			this.line17.X1 = 0.5389764F;
			this.line17.X2 = 5.748032F;
			this.line17.Y1 = 1.295669F;
			this.line17.Y2 = 1.295669F;
			// 
			// line18
			// 
			this.line18.Height = 0.590157F;
			this.line18.Left = 0.5389764F;
			this.line18.LineWeight = 1F;
			this.line18.Name = "line18";
			this.line18.Top = 1.098819F;
			this.line18.Width = 0F;
			this.line18.X1 = 0.5389764F;
			this.line18.X2 = 0.5389764F;
			this.line18.Y1 = 1.098819F;
			this.line18.Y2 = 1.688976F;
			// 
			// line19
			// 
			this.line19.Height = 0.590552F;
			this.line19.Left = 0F;
			this.line19.LineWeight = 1F;
			this.line19.Name = "line19";
			this.line19.Top = 1.098819F;
			this.line19.Width = 0F;
			this.line19.X1 = 0F;
			this.line19.X2 = 0F;
			this.line19.Y1 = 1.098819F;
			this.line19.Y2 = 1.689371F;
			// 
			// line20
			// 
			this.line20.Height = 0.590552F;
			this.line20.Left = 5.748032F;
			this.line20.LineWeight = 1F;
			this.line20.Name = "line20";
			this.line20.Top = 1.098819F;
			this.line20.Width = 0F;
			this.line20.X1 = 5.748032F;
			this.line20.X2 = 5.748032F;
			this.line20.Y1 = 1.098819F;
			this.line20.Y2 = 1.689371F;
			// 
			// line21
			// 
			this.line21.Height = 0.590552F;
			this.line21.Left = 7.484252F;
			this.line21.LineWeight = 1F;
			this.line21.Name = "line21";
			this.line21.Top = 1.098819F;
			this.line21.Width = 0F;
			this.line21.X1 = 7.484252F;
			this.line21.X2 = 7.484252F;
			this.line21.Y1 = 1.098819F;
			this.line21.Y2 = 1.689371F;
			// 
			// line22
			// 
			this.line22.Height = 0.393702F;
			this.line22.Left = 2.32441F;
			this.line22.LineWeight = 1F;
			this.line22.Name = "line22";
			this.line22.Top = 1.295669F;
			this.line22.Width = 0F;
			this.line22.X1 = 2.32441F;
			this.line22.X2 = 2.32441F;
			this.line22.Y1 = 1.295669F;
			this.line22.Y2 = 1.689371F;
			// 
			// line23
			// 
			this.line23.Height = 0.393702F;
			this.line23.Left = 1.86378F;
			this.line23.LineWeight = 1F;
			this.line23.Name = "line23";
			this.line23.Top = 1.295669F;
			this.line23.Width = 0F;
			this.line23.X1 = 1.86378F;
			this.line23.X2 = 1.86378F;
			this.line23.Y1 = 1.295669F;
			this.line23.Y2 = 1.689371F;
			// 
			// line24
			// 
			this.line24.Height = 0.393702F;
			this.line24.Left = 2.127559F;
			this.line24.LineWeight = 1F;
			this.line24.Name = "line24";
			this.line24.Top = 1.295669F;
			this.line24.Width = 0F;
			this.line24.X1 = 2.127559F;
			this.line24.X2 = 2.127559F;
			this.line24.Y1 = 1.295669F;
			this.line24.Y2 = 1.689371F;
			// 
			// line25
			// 
			this.line25.Height = 0.393702F;
			this.line25.Left = 4.49567F;
			this.line25.LineWeight = 1F;
			this.line25.Name = "line25";
			this.line25.Top = 1.295669F;
			this.line25.Width = 0F;
			this.line25.X1 = 4.49567F;
			this.line25.X2 = 4.49567F;
			this.line25.Y1 = 1.295669F;
			this.line25.Y2 = 1.689371F;
			// 
			// line26
			// 
			this.line26.Height = 0.393702F;
			this.line26.Left = 4.755906F;
			this.line26.LineWeight = 1F;
			this.line26.Name = "line26";
			this.line26.Top = 1.291732F;
			this.line26.Width = 0F;
			this.line26.X1 = 4.755906F;
			this.line26.X2 = 4.755906F;
			this.line26.Y1 = 1.291732F;
			this.line26.Y2 = 1.685434F;
			// 
			// line27
			// 
			this.line27.Height = 0.393702F;
			this.line27.Left = 4.96063F;
			this.line27.LineWeight = 1F;
			this.line27.Name = "line27";
			this.line27.Top = 1.295669F;
			this.line27.Width = 0F;
			this.line27.X1 = 4.96063F;
			this.line27.X2 = 4.96063F;
			this.line27.Y1 = 1.295669F;
			this.line27.Y2 = 1.689371F;
			// 
			// line28
			// 
			this.line28.Height = 0.590552F;
			this.line28.Left = 3.120079F;
			this.line28.LineWeight = 1F;
			this.line28.Name = "line28";
			this.line28.Top = 1.094488F;
			this.line28.Width = 0F;
			this.line28.X1 = 3.120079F;
			this.line28.X2 = 3.120079F;
			this.line28.Y1 = 1.094488F;
			this.line28.Y2 = 1.68504F;
			// 
			// txtZei
			// 
			this.txtZei.DataField = "ITEM03";
			this.txtZei.Height = 0.1968504F;
			this.txtZei.Left = 6.315749F;
			this.txtZei.MultiLine = false;
			this.txtZei.Name = "txtZei";
			this.txtZei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.txtZei.Text = null;
			this.txtZei.Top = 0.1968504F;
			this.txtZei.Width = 1.074015F;
			// 
			// detail
			// 
			this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue01,
            this.txtValue02,
            this.txtValue05,
            this.txtValue04,
            this.txtValue06,
            this.txtValue07,
            this.txtValue08,
            this.txtValue11,
            this.txtValue12,
            this.txtValue13,
            this.txtValue14,
            this.txtValue15,
            this.line29,
            this.line30,
            this.line31,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line36,
            this.line37,
            this.line38,
            this.line39,
            this.line40,
            this.txtValue03,
            this.txtValue10,
            this.txtValue09,
            this.textBox1,
            this.textBox2});
			this.detail.Height = 0.2325296F;
			this.detail.Name = "detail";
			this.detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
			// 
			// txtValue01
			// 
			this.txtValue01.DataField = "ITEM04";
			this.txtValue01.Height = 0.1181102F;
			this.txtValue01.Left = 0.02755906F;
			this.txtValue01.MultiLine = false;
			this.txtValue01.Name = "txtValue01";
			this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: left; vertical-align: top; ddo" +
    "-char-set: 1";
			this.txtValue01.Text = "27/99/99";
			this.txtValue01.Top = 0F;
			this.txtValue01.Width = 0.5082678F;
			// 
			// txtValue02
			// 
			this.txtValue02.CanGrow = false;
			this.txtValue02.DataField = "ITEM05";
			this.txtValue02.Height = 0.1377953F;
			this.txtValue02.Left = 0.02755906F;
			this.txtValue02.MultiLine = false;
			this.txtValue02.Name = "txtValue02";
			this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.txtValue02.Text = null;
			this.txtValue02.Top = 0.1181102F;
			this.txtValue02.Width = 0.4838583F;
			// 
			// txtValue05
			// 
			this.txtValue05.CanGrow = false;
			this.txtValue05.DataField = "ITEM08";
			this.txtValue05.Height = 0.1377953F;
			this.txtValue05.Left = 0.5507874F;
			this.txtValue05.MultiLine = false;
			this.txtValue05.Name = "txtValue05";
			this.txtValue05.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: left; vertical-align: top; ddo-ch" +
    "ar-set: 1";
			this.txtValue05.Text = null;
			this.txtValue05.Top = 0.1181102F;
			this.txtValue05.Width = 1.309449F;
			// 
			// txtValue04
			// 
			this.txtValue04.DataField = "ITEM07";
			this.txtValue04.Height = 0.1181102F;
			this.txtValue04.Left = 0.9287402F;
			this.txtValue04.MultiLine = false;
			this.txtValue04.Name = "txtValue04";
			this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: left; vertical-align: top; ddo" +
    "-char-set: 1";
			this.txtValue04.Text = null;
			this.txtValue04.Top = 0F;
			this.txtValue04.Width = 0.9350393F;
			// 
			// txtValue06
			// 
			this.txtValue06.DataField = "ITEM09";
			this.txtValue06.Height = 0.1181102F;
			this.txtValue06.Left = 1.869685F;
			this.txtValue06.MultiLine = false;
			this.txtValue06.Name = "txtValue06";
			this.txtValue06.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.txtValue06.Text = null;
			this.txtValue06.Top = 0F;
			this.txtValue06.Width = 0.2559055F;
			// 
			// txtValue07
			// 
			this.txtValue07.DataField = "ITEM10";
			this.txtValue07.Height = 0.1181102F;
			this.txtValue07.Left = 2.149213F;
			this.txtValue07.MultiLine = false;
			this.txtValue07.Name = "txtValue07";
			this.txtValue07.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.txtValue07.Text = null;
			this.txtValue07.Top = 0F;
			this.txtValue07.Width = 0.1531495F;
			// 
			// txtValue08
			// 
			this.txtValue08.DataField = "ITEM11";
			this.txtValue08.Height = 0.1299213F;
			this.txtValue08.Left = 2.338583F;
			this.txtValue08.MultiLine = false;
			this.txtValue08.Name = "txtValue08";
			this.txtValue08.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.txtValue08.Text = null;
			this.txtValue08.Top = 0F;
			this.txtValue08.Width = 0.7755906F;
			// 
			// txtValue11
			// 
			this.txtValue11.CanGrow = false;
			this.txtValue11.DataField = "ITEM14";
			this.txtValue11.Height = 0.1377953F;
			this.txtValue11.Left = 3.13504F;
			this.txtValue11.MultiLine = false;
			this.txtValue11.Name = "txtValue11";
			this.txtValue11.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: left; vertical-align: top; ddo-ch" +
    "ar-set: 1";
			this.txtValue11.Text = null;
			this.txtValue11.Top = 0.1181102F;
			this.txtValue11.Width = 1.309449F;
			// 
			// txtValue12
			// 
			this.txtValue12.DataField = "ITEM15";
			this.txtValue12.Height = 0.1181102F;
			this.txtValue12.Left = 4.507874F;
			this.txtValue12.MultiLine = false;
			this.txtValue12.Name = "txtValue12";
			this.txtValue12.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.txtValue12.Text = null;
			this.txtValue12.Top = 0F;
			this.txtValue12.Width = 0.240157F;
			// 
			// txtValue13
			// 
			this.txtValue13.DataField = "ITEM16";
			this.txtValue13.Height = 0.1181102F;
			this.txtValue13.Left = 4.772F;
			this.txtValue13.MultiLine = false;
			this.txtValue13.Name = "txtValue13";
			this.txtValue13.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.txtValue13.Text = null;
			this.txtValue13.Top = 0F;
			this.txtValue13.Width = 0.1728349F;
			// 
			// txtValue14
			// 
			this.txtValue14.DataField = "ITEM17";
			this.txtValue14.Height = 0.1299213F;
			this.txtValue14.Left = 4.972441F;
			this.txtValue14.MultiLine = false;
			this.txtValue14.Name = "txtValue14";
			this.txtValue14.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.txtValue14.Text = null;
			this.txtValue14.Top = 0F;
			this.txtValue14.Width = 0.7755906F;
			// 
			// txtValue15
			// 
			this.txtValue15.CanGrow = false;
			this.txtValue15.DataField = "ITEM18";
			this.txtValue15.Height = 0.2244095F;
			this.txtValue15.Left = 5.76378F;
			this.txtValue15.Name = "txtValue15";
			this.txtValue15.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: left; vertical-align: top; ddo" +
    "-char-set: 1";
			this.txtValue15.Text = null;
			this.txtValue15.Top = 0.03149607F;
			this.txtValue15.Width = 1.703937F;
			// 
			// line29
			// 
			this.line29.Height = 0F;
			this.line29.Left = 0F;
			this.line29.LineWeight = 1F;
			this.line29.Name = "line29";
			this.line29.Top = 0.2559055F;
			this.line29.Width = 7.48425F;
			this.line29.X1 = 0F;
			this.line29.X2 = 7.48425F;
			this.line29.Y1 = 0.2559055F;
			this.line29.Y2 = 0.2559055F;
			// 
			// line30
			// 
			this.line30.Height = 0.2559055F;
			this.line30.Left = 0F;
			this.line30.LineWeight = 1F;
			this.line30.Name = "line30";
			this.line30.Top = 0F;
			this.line30.Width = 0F;
			this.line30.X1 = 0F;
			this.line30.X2 = 0F;
			this.line30.Y1 = 0F;
			this.line30.Y2 = 0.2559055F;
			// 
			// line31
			// 
			this.line31.Height = 0.2559055F;
			this.line31.Left = 0.5389764F;
			this.line31.LineWeight = 1F;
			this.line31.Name = "line31";
			this.line31.Top = 0F;
			this.line31.Width = 0F;
			this.line31.X1 = 0.5389764F;
			this.line31.X2 = 0.5389764F;
			this.line31.Y1 = 0F;
			this.line31.Y2 = 0.2559055F;
			// 
			// line32
			// 
			this.line32.Height = 0.2559055F;
			this.line32.Left = 1.86378F;
			this.line32.LineWeight = 1F;
			this.line32.Name = "line32";
			this.line32.Top = 0F;
			this.line32.Width = 0F;
			this.line32.X1 = 1.86378F;
			this.line32.X2 = 1.86378F;
			this.line32.Y1 = 0F;
			this.line32.Y2 = 0.2559055F;
			// 
			// line33
			// 
			this.line33.Height = 0.2559055F;
			this.line33.Left = 2.127559F;
			this.line33.LineWeight = 1F;
			this.line33.Name = "line33";
			this.line33.Top = 0F;
			this.line33.Width = 0F;
			this.line33.X1 = 2.127559F;
			this.line33.X2 = 2.127559F;
			this.line33.Y1 = 0F;
			this.line33.Y2 = 0.2559055F;
			// 
			// line34
			// 
			this.line34.Height = 0.2559055F;
			this.line34.Left = 2.32441F;
			this.line34.LineWeight = 1F;
			this.line34.Name = "line34";
			this.line34.Top = 0F;
			this.line34.Width = 0F;
			this.line34.X1 = 2.32441F;
			this.line34.X2 = 2.32441F;
			this.line34.Y1 = 0F;
			this.line34.Y2 = 0.2559055F;
			// 
			// line35
			// 
			this.line35.Height = 0.2559055F;
			this.line35.Left = 3.120079F;
			this.line35.LineWeight = 1F;
			this.line35.Name = "line35";
			this.line35.Top = 0F;
			this.line35.Width = 0F;
			this.line35.X1 = 3.120079F;
			this.line35.X2 = 3.120079F;
			this.line35.Y1 = 0F;
			this.line35.Y2 = 0.2559055F;
			// 
			// line36
			// 
			this.line36.Height = 0.2559055F;
			this.line36.Left = 4.495669F;
			this.line36.LineWeight = 1F;
			this.line36.Name = "line36";
			this.line36.Top = 0F;
			this.line36.Width = 0F;
			this.line36.X1 = 4.495669F;
			this.line36.X2 = 4.495669F;
			this.line36.Y1 = 0F;
			this.line36.Y2 = 0.2559055F;
			// 
			// line37
			// 
			this.line37.Height = 0.2559055F;
			this.line37.Left = 4.755906F;
			this.line37.LineWeight = 1F;
			this.line37.Name = "line37";
			this.line37.Top = 0F;
			this.line37.Width = 0F;
			this.line37.X1 = 4.755906F;
			this.line37.X2 = 4.755906F;
			this.line37.Y1 = 0F;
			this.line37.Y2 = 0.2559055F;
			// 
			// line38
			// 
			this.line38.Height = 0.2559055F;
			this.line38.Left = 4.96063F;
			this.line38.LineWeight = 1F;
			this.line38.Name = "line38";
			this.line38.Top = 0F;
			this.line38.Width = 0F;
			this.line38.X1 = 4.96063F;
			this.line38.X2 = 4.96063F;
			this.line38.Y1 = 0F;
			this.line38.Y2 = 0.2559055F;
			// 
			// line39
			// 
			this.line39.Height = 0.2559055F;
			this.line39.Left = 5.748032F;
			this.line39.LineWeight = 1F;
			this.line39.Name = "line39";
			this.line39.Top = 0F;
			this.line39.Width = 0F;
			this.line39.X1 = 5.748032F;
			this.line39.X2 = 5.748032F;
			this.line39.Y1 = 0F;
			this.line39.Y2 = 0.2559055F;
			// 
			// line40
			// 
			this.line40.Height = 0.2559055F;
			this.line40.Left = 7.484252F;
			this.line40.LineWeight = 1F;
			this.line40.Name = "line40";
			this.line40.Top = 0F;
			this.line40.Width = 0F;
			this.line40.X1 = 7.484252F;
			this.line40.X2 = 7.484252F;
			this.line40.Y1 = 0F;
			this.line40.Y2 = 0.2559055F;
			// 
			// txtValue03
			// 
			this.txtValue03.DataField = "ITEM06";
			this.txtValue03.Height = 0.1181102F;
			this.txtValue03.Left = 0.5547245F;
			this.txtValue03.MultiLine = false;
			this.txtValue03.Name = "txtValue03";
			this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.txtValue03.Text = null;
			this.txtValue03.Top = 0F;
			this.txtValue03.Width = 0.3740157F;
			// 
			// txtValue10
			// 
			this.txtValue10.DataField = "ITEM13";
			this.txtValue10.Height = 0.1181102F;
			this.txtValue10.Left = 3.511811F;
			this.txtValue10.MultiLine = false;
			this.txtValue10.Name = "txtValue10";
			this.txtValue10.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: left; vertical-align: top; ddo" +
    "-char-set: 1";
			this.txtValue10.Text = null;
			this.txtValue10.Top = 0F;
			this.txtValue10.Width = 0.9342518F;
			// 
			// txtValue09
			// 
			this.txtValue09.DataField = "ITEM12";
			this.txtValue09.Height = 0.1181102F;
			this.txtValue09.Left = 3.137795F;
			this.txtValue09.MultiLine = false;
			this.txtValue09.Name = "txtValue09";
			this.txtValue09.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.txtValue09.Text = null;
			this.txtValue09.Top = 0F;
			this.txtValue09.Width = 0.3740157F;
			// 
			// textBox1
			// 
			this.textBox1.DataField = "ITEM22";
			this.textBox1.Height = 0.1181102F;
			this.textBox1.Left = 4.772F;
			this.textBox1.MultiLine = false;
			this.textBox1.Name = "textBox1";
			this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
			this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.textBox1.Text = null;
			this.textBox1.Top = 0.13F;
			this.textBox1.Width = 0.1728349F;
			// 
			// textBox2
			// 
			this.textBox2.DataField = "ITEM21";
			this.textBox2.Height = 0.1181102F;
			this.textBox2.Left = 2.149F;
			this.textBox2.MultiLine = false;
			this.textBox2.Name = "textBox2";
			this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
			this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: top; dd" +
    "o-char-set: 1";
			this.textBox2.Text = null;
			this.textBox2.Top = 0.13F;
			this.textBox2.Width = 0.1531495F;
			// 
			// pageFooter
			// 
			this.pageFooter.Height = 0F;
			this.pageFooter.Name = "pageFooter";
			this.pageFooter.Visible = false;
			// 
			// groupHeader1
			// 
			this.groupHeader1.CanGrow = false;
			this.groupHeader1.DataField = "ITEM20";
			this.groupHeader1.Height = 0F;
			this.groupHeader1.Name = "groupHeader1";
			this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.groupHeader1.UnderlayNext = true;
			// 
			// groupFooter1
			// 
			this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue08Total,
            this.lblKarikataGokei,
            this.lblKashikataGokei,
            this.line49,
            this.line50,
            this.line51,
            this.line52,
            this.line53,
            this.line54,
            this.line55,
            this.line56,
            this.txtValue14Total});
			this.groupFooter1.Height = 0.1574803F;
			this.groupFooter1.Name = "groupFooter1";
			this.groupFooter1.Format += new System.EventHandler(this.groupFooter_Format);
			// 
			// txtValue08Total
			// 
			this.txtValue08Total.DataField = "ITEM11";
			this.txtValue08Total.Height = 0.1299213F;
			this.txtValue08Total.Left = 2.338583F;
			this.txtValue08Total.MultiLine = false;
			this.txtValue08Total.Name = "txtValue08Total";
			this.txtValue08Total.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.txtValue08Total.SummaryGroup = "groupHeader1";
			this.txtValue08Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtValue08Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtValue08Total.Text = null;
			this.txtValue08Total.Top = 0.01181102F;
			this.txtValue08Total.Width = 0.7755906F;
			// 
			// lblKarikataGokei
			// 
			this.lblKarikataGokei.Height = 0.1574803F;
			this.lblKarikataGokei.HyperLink = null;
			this.lblKarikataGokei.Left = 0.5547245F;
			this.lblKarikataGokei.Name = "lblKarikataGokei";
			this.lblKarikataGokei.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: bottom; ddo-char-set: 1";
			this.lblKarikataGokei.Text = "【借 方 合 計】";
			this.lblKarikataGokei.Top = 0F;
			this.lblKarikataGokei.Width = 1.747638F;
			// 
			// lblKashikataGokei
			// 
			this.lblKashikataGokei.Height = 0.1574803F;
			this.lblKashikataGokei.HyperLink = null;
			this.lblKashikataGokei.Left = 3.149606F;
			this.lblKashikataGokei.Name = "lblKashikataGokei";
			this.lblKashikataGokei.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: center; vertic" +
    "al-align: bottom; ddo-char-set: 1";
			this.lblKashikataGokei.Text = "【貸 方 合 計】";
			this.lblKashikataGokei.Top = 0F;
			this.lblKashikataGokei.Width = 1.799213F;
			// 
			// line49
			// 
			this.line49.Height = 0F;
			this.line49.Left = 0F;
			this.line49.LineWeight = 1F;
			this.line49.Name = "line49";
			this.line49.Top = 0.1574803F;
			this.line49.Width = 7.48425F;
			this.line49.X1 = 0F;
			this.line49.X2 = 7.48425F;
			this.line49.Y1 = 0.1574803F;
			this.line49.Y2 = 0.1574803F;
			// 
			// line50
			// 
			this.line50.Height = 0.1574803F;
			this.line50.Left = 0F;
			this.line50.LineWeight = 1F;
			this.line50.Name = "line50";
			this.line50.Top = 0F;
			this.line50.Width = 0F;
			this.line50.X1 = 0F;
			this.line50.X2 = 0F;
			this.line50.Y1 = 0F;
			this.line50.Y2 = 0.1574803F;
			// 
			// line51
			// 
			this.line51.Height = 0.1574804F;
			this.line51.Left = 0.5389764F;
			this.line51.LineWeight = 1F;
			this.line51.Name = "line51";
			this.line51.Top = 0F;
			this.line51.Width = 0F;
			this.line51.X1 = 0.5389764F;
			this.line51.X2 = 0.5389764F;
			this.line51.Y1 = 0F;
			this.line51.Y2 = 0.1574804F;
			// 
			// line52
			// 
			this.line52.Height = 0.1574804F;
			this.line52.Left = 3.120079F;
			this.line52.LineWeight = 1F;
			this.line52.Name = "line52";
			this.line52.Top = 0F;
			this.line52.Width = 0F;
			this.line52.X1 = 3.120079F;
			this.line52.X2 = 3.120079F;
			this.line52.Y1 = 0F;
			this.line52.Y2 = 0.1574804F;
			// 
			// line53
			// 
			this.line53.Height = 0.1574804F;
			this.line53.Left = 5.748032F;
			this.line53.LineWeight = 1F;
			this.line53.Name = "line53";
			this.line53.Top = 0F;
			this.line53.Width = 0F;
			this.line53.X1 = 5.748032F;
			this.line53.X2 = 5.748032F;
			this.line53.Y1 = 0F;
			this.line53.Y2 = 0.1574804F;
			// 
			// line54
			// 
			this.line54.Height = 0.1574804F;
			this.line54.Left = 2.32441F;
			this.line54.LineWeight = 1F;
			this.line54.Name = "line54";
			this.line54.Top = 0F;
			this.line54.Width = 0F;
			this.line54.X1 = 2.32441F;
			this.line54.X2 = 2.32441F;
			this.line54.Y1 = 0F;
			this.line54.Y2 = 0.1574804F;
			// 
			// line55
			// 
			this.line55.Height = 0.1574804F;
			this.line55.Left = 4.96063F;
			this.line55.LineWeight = 1F;
			this.line55.Name = "line55";
			this.line55.Top = 0F;
			this.line55.Width = 0F;
			this.line55.X1 = 4.96063F;
			this.line55.X2 = 4.96063F;
			this.line55.Y1 = 0F;
			this.line55.Y2 = 0.1574804F;
			// 
			// line56
			// 
			this.line56.Height = 0.1574804F;
			this.line56.Left = 7.48425F;
			this.line56.LineWeight = 1F;
			this.line56.Name = "line56";
			this.line56.Top = 0F;
			this.line56.Width = 0F;
			this.line56.X1 = 7.48425F;
			this.line56.X2 = 7.48425F;
			this.line56.Y1 = 0F;
			this.line56.Y2 = 0.1574804F;
			// 
			// txtValue14Total
			// 
			this.txtValue14Total.DataField = "ITEM17";
			this.txtValue14Total.Height = 0.1299212F;
			this.txtValue14Total.Left = 4.972441F;
			this.txtValue14Total.MultiLine = false;
			this.txtValue14Total.Name = "txtValue14Total";
			this.txtValue14Total.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.txtValue14Total.SummaryGroup = "groupHeader1";
			this.txtValue14Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtValue14Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtValue14Total.Text = null;
			this.txtValue14Total.Top = 0.007874017F;
			this.txtValue14Total.Width = 0.7755904F;
			// 
			// ZMDR10211R
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.3937008F;
			this.PageSettings.Margins.Left = 0.3937008F;
			this.PageSettings.Margins.Right = 0.3937008F;
			this.PageSettings.Margins.Top = 0.3937008F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.492126F;
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.groupHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.groupFooter1);
			this.Sections.Add(this.pageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn03)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn04)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIn05)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZei)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue08)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue09)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue08Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblKarikataGokei)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblKashikataGokei)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue14Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblIn01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblIn02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblIn03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblIn04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblIn05;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue09;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKarikataGokei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKashikataGokei;
        private GrapeCity.ActiveReports.SectionReportModel.Line line49;
        private GrapeCity.ActiveReports.SectionReportModel.Line line50;
        private GrapeCity.ActiveReports.SectionReportModel.Line line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line line52;
        private GrapeCity.ActiveReports.SectionReportModel.Line line53;
        private GrapeCity.ActiveReports.SectionReportModel.Line line54;
        private GrapeCity.ActiveReports.SectionReportModel.Line line55;
        private GrapeCity.ActiveReports.SectionReportModel.Line line56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue14Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
	}
}
