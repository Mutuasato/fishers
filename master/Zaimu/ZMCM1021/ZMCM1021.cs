﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1021
{
    /// <summary>
    /// 消費税情報の設定仲買人の登録(ZMCM1021)
    /// </summary>
    public partial class ZMCM1021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";

        /// <summary>
        /// SQL関連
        /// </summary>
        private const int KAISHA_CDDE = 1; // 会社コード
        private const int TORIHIKISAKI_KUBUN2 = 2; // 取引先区分２
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            this.ShowFButton = false;

            // 編集モードの初期表示
            InitDispOnEdit();

            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 精算番号、船主コードに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYearFr":        // 適用開始日の年
                case "txtKazeiHoho":         // 課税方法
                case "txtKojoHoho":          // 控除方法
                case "txtJigyoKubun":        // 主たる事業区分
                case "txtShohizeiHasuShori": // 消費税端数処理
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            String[] result;
			// System.Reflection.Assembly asm;
			// Type t;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			switch (this.ActiveCtlNm)
            {
                case "txtDateYearFr":
                    // アセンブリのロード
                    System.Reflection.Assembly asmFr = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "COMC9011.exe");
                    // フォーム作成
                    Type tFr = asmFr.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (tFr != null)
                    {
                        Object obj = System.Activator.CreateInstance(tFr);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
                case "txtKazeiHoho": // 課税方法
                    //result = this.openSearchWindow("COMC8111", "TB_ZM_F_KAZEI_HOHO", this.txtKazeiHoho.Text);
                    //result = this.openSearchWindow(stCurrentDir + @"\EXE\" + "COMC8111", "1", this.txtKazeiHoho.Text);
                    //if (!ValChk.IsEmpty(result[0]))
                    //{
                    //    this.txtKazeiHoho.Text = result[0];
                    //    this.lblKazeiHohoNm.Text = result[1];
                    //}
                    //break;
                    
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_KAZEI_HOHO";
                            frm.InData = this.txtKazeiHoho.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (String[])frm.OutData;
                                this.txtKazeiHoho.Text = outData[0];
                                this.lblKazeiHohoNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                case "txtKojoHoho": // 控除方法
                    //result = this.openSearchWindow("COMC8011", "TB_ZM_F_KOJO_HOHO", this.txtKojoHoho.Text);
                    //if (!ValChk.IsEmpty(result[0]))
                    //{
                    //    this.txtKojoHoho.Text = result[0];
                    //    this.lblKojoHohoNm.Text = result[1];
                    //}

                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_KOJO_HOHO";
                            frm.InData = this.txtKazeiHoho.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (String[])frm.OutData;
                                this.txtKojoHoho.Text = outData[0];
                                this.lblKojoHohoNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                case "txtJigyoKubun": // 主たる事業区分
                    //result = this.openSearchWindow("COMC8011", "TB_ZM_F_JIGYO_KUBUN", this.txtJigyoKubun.Text);
                    //if (!ValChk.IsEmpty(result[0]))
                    //{
                    //    this.txtJigyoKubun.Text = result[0];
                    //    this.lblJigyoKubunNm.Text = result[1];
                    //}

                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_JIGYO_KUBUN";
                            frm.InData = this.txtKazeiHoho.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (String[])frm.OutData;
                                this.txtJigyoKubun.Text = outData[0];
                                this.lblJigyoKubunNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                case "txtShohizeiHasuShori": // 消費税端数処理
                    //result = this.openSearchWindow("COMC8011", "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", this.txtShohizeiHasuShori.Text);
                    //if (!ValChk.IsEmpty(result[0]))
                    //{
                    //    this.txtShohizeiHasuShori.Text = result[0];
                    //    this.lblShohizeiHasuShoriNm.Text = result[1];
                    //}

                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_HASU_SHORI";
                            frm.InData = this.txtKazeiHoho.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (String[])frm.OutData;
                                this.txtShohizeiHasuShori.Text = outData[0];
                                this.lblShohizeiHasuShoriNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理（登録）
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 確認メッセージを表示
            string msg = "保存しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsZmShohizei = SetZmShohizeiParams();

            try
            {
                this.Dba.BeginTransaction();

                // データ更新
                // 消費税情報テーブル
                this.Dba.Update("TB_ZM_SHOHIZEI_JOHO",
                    (DbParamCollection)alParamsZmShohizei[1],
                    "KAISHA_CD = @KAISHA_CD AND KESSANKI = @KESSANKI AND KAIKEI_NENDO = @KAIKEI_NENDO",
                    (DbParamCollection)alParamsZmShohizei[0]);

                // トランザクションをコミット
                this.Dba.Commit();

                // DialogResultに「OK」をセットし結果を返却
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception e)
            {
                // ロールバック
                this.Dba.Rollback();

                // 失敗通知ダイアログ
                Msg.Error("保存が失敗しました。" + e.Message);
            }
            finally
            {
                ;
            }

        }
        #endregion

        #region イベント
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidYearFrom())
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
                return;
            }
        }

        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidMonthFrom())
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
                return;
            }
        }

        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidDayFrom())
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
                return;
            }
        }

        /// <summary>
        /// 課税方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKazeiHoho_Validating(object sender, CancelEventArgs e)
        {
            // 存在チェック
            string name = this.getKazeiHohoNm(this.txtKazeiHoho.Text);
            this.lblKazeiHohoNm.Text = name;
        }

        /// <summary>
        /// 控除方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojoHoho_Validating(object sender, CancelEventArgs e)
        {
            // 存在チェック
            string name = this.getKojoHohoNm(this.txtKojoHoho.Text);
            this.lblKojoHohoNm.Text = name;
        }

        /// <summary>
        /// 主たる事業区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJigyoKubun_Validating(object sender, CancelEventArgs e)
        {
            // 存在チェック
            string name = this.getJigyoKubunNm(this.txtJigyoKubun.Text);
            this.lblJigyoKubunNm.Text = name;
        }

        /// <summary>
        /// 消費税端数処理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiHasuShori_Validating(object sender, CancelEventArgs e)
        {
            // 存在チェック
            string name = this.getHasuShoriNm(this.txtShohizeiHasuShori.Text);
            this.lblShohizeiHasuShoriNm.Text = name;
        }

        /// <summary>
        /// 旧消費税率の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuShohizeiRitsu_Validating(object sender, CancelEventArgs e)
        {
            decimal wk = Util.ToDecimal(this.txtKyuShohizeiRitsu.Text);
            this.txtKyuShohizeiRitsu.Text = wk.ToString("#0.0");
        }

        /// <summary>
        /// 新消費税率の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShinShohizeiRitsu_Validating(object sender, CancelEventArgs e)
        {
            decimal wk = Util.ToDecimal(this.txtShinShohizeiRitsu.Text);
            this.txtShinShohizeiRitsu.Text = wk.ToString("#0.0");
        }

        /// <summary>
        /// 地方消費税率
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChihoShohizeiritsu(object sender, CancelEventArgs e)
        {
            decimal wk = Util.ToDecimal(this.txtChihoShohizeiRitsu.Text);
            this.txtChihoShohizeiRitsu.Text = wk.ToString("#0.0");
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 適用日付範囲fm（開始年）入力チェック
        /// </summary>
        /// <returns></returns>
        private bool IsValidYearFrom()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateYearFr.SelectAll();
                return false;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearFr.Text))
            {
                this.txtDateYearFr.Text = "0";
            }
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));

            return true;
        }

        /// <summary>
        /// 適用日付範囲fm（開始月）入力チェック
        /// </summary>
        /// <returns></returns>
        private bool IsValidMonthFrom()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateMonthFr.Text))
            {
                // 空の場合、1月として処理
                this.txtDateMonthFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthFr.Text) > 12)
                {
                    this.txtDateMonthFr.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthFr.Text) < 1)
                {
                    this.txtDateMonthFr.Text = "1";
                }

                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                {
                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));

            return true;
        }

        /// <summary>
        /// 適用日付範囲fm（開始日）入力チェック
        /// </summary>
        /// <returns></returns>
        private bool IsValidDayFrom()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDayFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateDayFr.SelectAll();
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDayFr.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDayFr.Text = "1";
            }
            else
            {
                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                {
                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                }
                // 1より小さい日が入力された場合、1日として処理
                else if (Util.ToInt(this.txtDateDayFr.Text) < 1)
                {
                    this.txtDateDayFr.Text = "1";
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));

            return true;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 会社情報を読み込む（TB_CM_KAISHA_JOHO）
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");                      // 会社コード
            cols.Append(" ,A.KESSANKI");                     // 決算期
            cols.Append(" ,A.KAIKEI_NENDO");                 // 会計年度
            cols.Append(" ,A.KAZEI_HOHO");                   // 課税方法
            cols.Append(" ,A.KOJO_HOHO");                    // 控除方法
            cols.Append(" ,A.JIGYO_KUBUN");                  // 事業区分
            cols.Append(" ,A.SHOHIZEI_NYURYOKU_HOHO");       // 消費税入力方法
            cols.Append(" ,A.SHOHIZEI_HASU_SHORI");          // 消費税端数処理
            cols.Append(" ,A.KYU_SHOHIZEI_RITSU");           // 旧消費税率
            cols.Append(" ,A.SHIN_SHOHIZEI_RITSU");          // 新消費税率
            cols.Append(" ,A.KARIUKE_SHOHIZEI_KAMOKU_CD");   // 仮受消費税科目コード
            cols.Append(" ,A.KARIBARAI_SHOHIZEI_KAMOKU_CD"); // 仮払消費税科目コード
            cols.Append(" ,A.CHIHO_SHOHIZEI_RITSU");         // 地方消費税率
            cols.Append(" ,A.TEKIYO_KAISHIBI");              // 適用開始日

            StringBuilder from = new StringBuilder();
            from.Append("TB_ZM_SHOHIZEI_JOHO AS A");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 5, this.UInfo.KessanKi);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            DataTable dtZmShohizeiJoho =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.KESSANKI = @KESSANKI AND A.KAIKEI_NENDO = @KAIKEI_NENDO",
                    dpc);

            // 取得した内容を表示
            DataRow drDispData = dtZmShohizeiJoho.Rows[0];
            this.txtKazeiHoho.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["KAZEI_HOHO"]); // 課税方法
            this.txtKojoHoho.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["KOJO_HOHO"]); // 控除方法
            this.txtJigyoKubun.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["JIGYO_KUBUN"]); // 事業区分
            this.txtShohizeiNyuryokuHoho.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"]); // 消費税入力方法
            this.txtShohizeiHasuShori.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["SHOHIZEI_HASU_SHORI"]); // 消費税端数処理
            this.txtKyuShohizeiRitsu.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["KYU_SHOHIZEI_RITSU"]); // 旧消費税率
            this.txtShinShohizeiRitsu.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["SHIN_SHOHIZEI_RITSU"]); // 新消費税率
            this.txtChihoShohizeiRitsu.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["CHIHO_SHOHIZEI_RITSU"]); // 地方消費税率
            this.txtKaribaraiShohizeiKamokuCd.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["KARIBARAI_SHOHIZEI_KAMOKU_CD"]); // 仮払消費税科目コード
            this.txtKariukeShohizeiKamokuCd.Text = Util.ToString(dtZmShohizeiJoho.Rows[0]["KARIUKE_SHOHIZEI_KAMOKU_CD"]); // 仮受消費税科目コード

            int karibaraiShohizeiKamokuCd = Util.ToInt(dtZmShohizeiJoho.Rows[0]["KARIBARAI_SHOHIZEI_KAMOKU_CD"]); // 仮払消費税科目コード
            int kariukeShohizeiKamokuCd = Util.ToInt(dtZmShohizeiJoho.Rows[0]["KARIUKE_SHOHIZEI_KAMOKU_CD"]); // 仮受消費税科目コード

            // 適用開始日
            string[] jpDate = Util.ConvJpDate(Util.ToDate(dtZmShohizeiJoho.Rows[0]["TEKIYO_KAISHIBI"]), this.Dba);
            this.lblDateGengoFr.Text = jpDate[0]; // 元号
            this.txtDateYearFr.Text = jpDate[2];  // 年
            this.txtDateMonthFr.Text = jpDate[3]; // 月
            this.txtDateDayFr.Text = jpDate[4];   // 日

            // 課税方法名の表示
            this.lblKazeiHohoNm.Text = this.getKazeiHohoNm(Util.ToString(dtZmShohizeiJoho.Rows[0]["KAZEI_HOHO"]));

            // 控除方法名の表示
            this.lblKojoHohoNm.Text = this.getKojoHohoNm(Util.ToString(dtZmShohizeiJoho.Rows[0]["KOJO_HOHO"]));

            // 事業区分名の表示
            this.lblJigyoKubunNm.Text = this.getJigyoKubunNm(Util.ToString(dtZmShohizeiJoho.Rows[0]["JIGYO_KUBUN"]));

            // 消費税入力情報名の表示
            this.lblShohizeiNyuryokuHohoNm.Text = this.getNyuryokuHohoNm(Util.ToString(dtZmShohizeiJoho.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"]));

            // 消費税端数処理名を表示
            this.lblShohizeiHasuShoriNm.Text = this.getHasuShoriNm(Util.ToString(dtZmShohizeiJoho.Rows[0]["SHOHIZEI_HASU_SHORI"]));

            // +---------------------------------------------------------------------------+
            // | 科目名取得（TB_ZM_KANJO_KAMOKU）                                          |
            // +---------------------------------------------------------------------------+
            cols = new StringBuilder();
            cols.Append("G.KANJO_KAMOKU_CD,"); // 勘定科目コード
            cols.Append("G.KANJO_KAMOKU_NM"); // 勘定科目名

            from = new StringBuilder();
            from.Append("TB_ZM_KANJO_KAMOKU AS G"); // 勘定科目テーブル

            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KARIBARAI", SqlDbType.Decimal, 6, karibaraiShohizeiKamokuCd);
            dpc.SetParam("@KARIUKE", SqlDbType.Decimal, 6, kariukeShohizeiKamokuCd);

            DataTable dtZmKanjoKamoku =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND G.KANJO_KAMOKU_CD IN (@KARIBARAI, @KARIUKE)",
                    "G.KANJO_KAMOKU_CD",
                    dpc);

            // 科目名を表示
            int rows = dtZmKanjoKamoku.Rows.Count;
            if (rows <= 0)
            {
                // 仮払消費税科目名を消去
                this.lblKaribaraiShohizeiKamokuNm.Text = "";

                // 仮受消費税科目名を消去
                this.lblKariukeShohizeiKamokuNm.Text = "";

                return;
            }
            for (int i = 0; i < rows; i++)
            {
                if (Util.ToInt(dtZmKanjoKamoku.Rows[i]["KANJO_KAMOKU_CD"]) == karibaraiShohizeiKamokuCd)
                {
                    // 仮払消費税科目名を表示
                    this.lblKaribaraiShohizeiKamokuNm.Text = Util.ToString(dtZmKanjoKamoku.Rows[i]["KANJO_KAMOKU_NM"]);
                    continue;
                }

                if (Util.ToInt(dtZmKanjoKamoku.Rows[i]["KANJO_KAMOKU_CD"]) == kariukeShohizeiKamokuCd)
                {
                    // 仮受消費税科目名を表示
                    this.lblKariukeShohizeiKamokuNm.Text = Util.ToString(dtZmKanjoKamoku.Rows[i]["KANJO_KAMOKU_NM"]);
                    continue;
                }
            }

        }

        /// <summary>
        /// 課税方法の名称取得（TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO）
        /// </summary>
        /// <param name="kazeiHohoCd"></param>
        /// <returns></returns>
        private string getKazeiHohoNm(string kazeiHohoCd)
        {
            return this.Dba.GetName(this.UInfo, "TB_ZM_F_KAZEI_HOHO", "", kazeiHohoCd);
        }

        /// <summary>
        /// 控除方法の名称取得（TB_ZM_F_KOJO_HOHO）
        /// </summary>
        /// <param name="kojoHohoCd"></param>
        /// <returns></returns>
        private string getKojoHohoNm(string kojoHohoCd)
        {
            return this.Dba.GetName(this.UInfo, "TB_ZM_F_KOJO_HOHO", "", kojoHohoCd);
        }

        /// <summary>
        /// 事業区分の名称取得
        /// </summary>
        /// <param name="jigyoKubunCd"></param>
        /// <returns></returns>
        private string getJigyoKubunNm(string jigyoKubunCd)
        {
            return this.Dba.GetName(this.UInfo, "TB_ZM_F_JIGYO_KUBUN", "", jigyoKubunCd);
        }

        /// <summary>
        /// 消費税入力情報の名称取得
        /// </summary>
        /// <param name="shohizeiNyuryokuHohoCd"></param>
        /// <returns></returns>
        private string getNyuryokuHohoNm(string shohizeiNyuryokuHohoCd)
        {
            return this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", shohizeiNyuryokuHohoCd);
        }

        /// <summary>
        /// 消費税端数処理名取得
        /// </summary>
        /// <param name="shohizeiHasuShoriCd"></param>
        /// <returns></returns>
        private string getHasuShoriNm(string shohizeiHasuShoriCd)
        {
            StringBuilder cols = new StringBuilder();
            cols.Append("F.HASU_SHORI_NM"); // 端数処理方法名

            StringBuilder from = new StringBuilder();
            //from.Append("TB_ZM_HASU_SHORI AS F"); // 消費税入力方法テーブル
            from.Append("TB_ZM_F_HASU_SHORI AS F"); // 消費税入力方法テーブル
            
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@HASU_SHORI", SqlDbType.Decimal, 3, shohizeiHasuShoriCd);

            DataTable dtZmHasuShori =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "F.HASU_SHORI = @HASU_SHORI",
                    dpc);

            // 消費税入力情報を表示
            if (dtZmHasuShori != null && dtZmHasuShori.Rows.Count == 1)
            {
                return Util.ToString(dtZmHasuShori.Rows[0]["HASU_SHORI_NM"]);
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 区分入力チェック（共通）
        /// </summary>
        /// <param name="inputForm"></param>
        /// <returns></returns>
        private bool IsValidInputKubun(common.controls.FsiTextBox inputForm)
        {
            // 数字のみの入力を許可
            if (ValChk.IsEmpty(inputForm.Text))
            {
                return false;
            }

            // 1バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(inputForm.Text, inputForm.MaxLength))
            {
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(inputForm.Text))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 課税方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKazeiHoho()
        {
            // 区分の入力チェック
            if (!this.IsValidInputKubun(this.txtKazeiHoho))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在チェック
            string name = this.getKazeiHohoNm(this.txtKazeiHoho.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                this.lblKazeiHohoNm.Text = "";
                return false;
            }

            this.lblKazeiHohoNm.Text = name;

            return true;
        }

        /// <summary>
        /// 控除方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKojoHoho()
        {
            // 区分の入力チェック
            if (!this.IsValidInputKubun(this.txtKojoHoho))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在チェック
            string name = this.getKojoHohoNm(this.txtKojoHoho.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                this.lblKojoHohoNm.Text = "";
                return false;
            }

            this.lblKojoHohoNm.Text = name;

            return true;
        }

        /// <summary>
        /// 主たる事業区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJigyoKubun()
        {
            // 区分の入力チェック
            if (!this.IsValidInputKubun(this.txtJigyoKubun))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在チェック
            string name = this.getJigyoKubunNm(this.txtJigyoKubun.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                this.lblJigyoKubunNm.Text = "";
                return false;
            }

            this.lblJigyoKubunNm.Text = name;

            return true;
        }

        /// <summary>
        /// 消費税端数処理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHasuShori()
        {
            // 区分の入力チェック
            if (!this.IsValidInputKubun(this.txtShohizeiHasuShori))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在チェック
            string name = this.getHasuShoriNm(this.txtShohizeiHasuShori.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                this.lblShohizeiHasuShoriNm.Text = "";
                return false;
            }

            this.lblShohizeiHasuShoriNm.Text = name;

            return true;
        }

        /// <summary>
        /// 消費税率の検証の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiRitsu(common.controls.FsiTextBox inputData)
        {
            // 未入力はエラー
            if (ValChk.IsEmpty(inputData.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 最大桁数オーバーしたらエラー
            if (!ValChk.IsWithinLength(inputData.Text, inputData.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // ピリオドが複数あったらエラー
            string removedPiriod = inputData.Text.ToString().Replace(".", "");
            if (1 < inputData.Text.Length - removedPiriod.Length)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 小数部桁数チェック
            char[] splitCahrs = { '.' };
            string[] suuji = inputData.Text.Split(splitCahrs, 2);
            if (suuji.Length == 2 && 1 < suuji[1].Length)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(removedPiriod))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 範囲チェック
            decimal tesuryouRitsu = Util.ToDecimal(this.txtKyuShohizeiRitsu.Text);
            if (tesuryouRitsu < (decimal)0.01 || (decimal)99.99 < tesuryouRitsu)
            {
                // ０入力可能にする
                if (0 != tesuryouRitsu)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 課税方法のチェック
            if (!this.IsValidKazeiHoho())
            {
                this.txtKazeiHoho.Focus();
                return false;
            }

            // 控除方法のチェック
            if (!this.IsValidKojoHoho())
            {
                this.txtKojoHoho.Focus();
                return false;
            }

            // 主たる事業区分の入力チェック
            if (!this.IsValidJigyoKubun())
            {
                this.txtJigyoKubun.Focus();
                return false;
            }

            // 消費税端数処理の入力チェック
            if (!this.IsValidHasuShori())
            {
                this.txtShohizeiHasuShori.Focus();
                return false;
            }

            // 旧消費税率の検証の入力チェック
            if (!this.IsValidShohizeiRitsu(this.txtKyuShohizeiRitsu))
            {
                this.txtKyuShohizeiRitsu.Focus();
                return false;
            }

            // 新消費税率の検証の入力チェック
            if (!this.IsValidShohizeiRitsu(this.txtShinShohizeiRitsu))
            {
                this.txtShinShohizeiRitsu.Focus();
                return false;
            }

            // 地方消費税率の検証の入力チェック
            if (!this.IsValidShohizeiRitsu(this.txtChihoShohizeiRitsu))
            {
                this.txtChihoShohizeiRitsu.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_ZM_SHOHIZEI_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmShohizeiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 会社コードと取引先コードをWhere句のパラメータに設定
            DbParamCollection whereParam = new DbParamCollection();
            // 会社コードと決算期と会計年度を更新パラメータに設定
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            whereParam.SetParam("@KESSANKI", SqlDbType.Decimal, 4, this.UInfo.KessanKi);
            whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            alParams.Add(whereParam);

            // 課税情報
            updParam.SetParam("@KAZEI_HOHO", SqlDbType.Decimal, 3, this.txtKazeiHoho.Text);
            // 控除方法
            updParam.SetParam("@KOJO_HOHO", SqlDbType.Decimal, 3, this.txtKojoHoho.Text);
            // 事業区分
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 3, this.txtJigyoKubun.Text);
            // 消費税端数処理
            updParam.SetParam("@SHOHIZEI_HASU_SHORI", SqlDbType.Decimal, 3, this.txtShohizeiHasuShori.Text);
            // 旧消費税率
            updParam.SetParam("@KYU_SHOHIZEI_RITSU", SqlDbType.Decimal, 4, this.txtKyuShohizeiRitsu.Text);
            // 新消費税率
            updParam.SetParam("@SHIN_SHOHIZEI_RITSU", SqlDbType.Decimal, 4, this.txtShinShohizeiRitsu.Text);
            // 地方消費税率
            updParam.SetParam("@CHIHO_SHOHIZEI_RITSU", SqlDbType.Decimal, 4, this.txtChihoShohizeiRitsu.Text);
            // 更新日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            // 適用開始日を西暦にして設定する
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            // 更新日付
            updParam.SetParam("@TEKIYO_KAISHIBI", SqlDbType.DateTime, tmpDateFr);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private string[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 2);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = System.Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }

            }
            return result;

        }

        #endregion
    }
}
