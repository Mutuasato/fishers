﻿namespace jp.co.fsi.zm.zmyr1031
{
    /// <summary>
    /// ZAMR3062R の概要の説明です。
    /// </summary>
    partial class ZMYR1032R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZMYR1032R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtKamokuNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZeiRitsu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZeinukiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKamokuNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZeinukiKingakuTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohizeiTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKeiTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue01Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue05Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue08Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue09Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKamokuNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeiRitsu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeinukiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKamokuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeinukiKingakuTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizeiTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeiTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtTitle,
            this.txtDate,
            this.txtZei,
            this.txtTitle01,
            this.txtTitle03,
            this.txtTitle02,
            this.line5,
            this.line4,
            this.txtTitle12,
            this.txtTitle11,
            this.txtTitle10,
            this.txtTitle09,
            this.txtTitle08,
            this.txtTitle07,
            this.txtTitle06,
            this.txtTitle05,
            this.txtTitle04,
            this.line23,
            this.line24,
            this.line25,
            this.txtTitle13,
            this.txtTitle14,
            this.line16,
            this.line17,
            this.line6,
            this.line7,
            this.line8,
            this.line26,
            this.line27,
            this.line28,
            this.line29,
            this.line30,
            this.line9,
            this.line3});
            this.pageHeader.Height = 0.8244919F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.1968504F;
            this.txtToday.Left = 9.36378F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtToday.Text = "yyyy-MM-dd";
            this.txtToday.Top = 0.05314961F;
            this.txtToday.Width = 0.816535F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.23858F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblPage.Text = "Page:";
            this.lblPage.Top = 0.05314961F;
            this.lblPage.Width = 0.4299212F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 10.72244F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "1";
            this.txtPageCount.Top = 0.05314961F;
            this.txtPageCount.Width = 0.1909451F;
            // 
            // txtTitle
            // 
            this.txtTitle.DataField = "ITEM01";
            this.txtTitle.Height = 0.25F;
            this.txtTitle.Left = 3.702756F;
            this.txtTitle.MultiLine = false;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text-align: center" +
    "; vertical-align: middle";
            this.txtTitle.Text = null;
            this.txtTitle.Top = 0.003543307F;
            this.txtTitle.Width = 3.611024F;
            // 
            // txtDate
            // 
            this.txtDate.DataField = "ITEM02";
            this.txtDate.Height = 0.1968504F;
            this.txtDate.Left = 0F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.05314961F;
            this.txtDate.Width = 3.579528F;
            // 
            // txtZei
            // 
            this.txtZei.DataField = "ITEM03";
            this.txtZei.Height = 0.1968504F;
            this.txtZei.Left = 8.524016F;
            this.txtZei.MultiLine = false;
            this.txtZei.Name = "txtZei";
            this.txtZei.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: middle";
            this.txtZei.Text = null;
            this.txtZei.Top = 0.05669296F;
            this.txtZei.Width = 0.839766F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.5314961F;
            this.txtTitle01.Left = 0F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.txtTitle01.Text = "勘定科目";
            this.txtTitle01.Top = 0.2917323F;
            this.txtTitle01.Width = 1.185039F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.5314961F;
            this.txtTitle03.Left = 1.432677F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.txtTitle03.Text = "税抜";
            this.txtTitle03.Top = 0.288189F;
            this.txtTitle03.Width = 0.7874016F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.5314961F;
            this.txtTitle02.Left = 1.185039F;
            this.txtTitle02.LineSpacing = 0.1F;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = resources.GetString("txtTitle02.Style");
            this.txtTitle02.Text = "率\r\n(%)";
            this.txtTitle02.Top = 0.2917323F;
            this.txtTitle02.Width = 0.2476378F;
            // 
            // line5
            // 
            this.line5.Height = 0.531496F;
            this.line5.Left = 1.185039F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.288189F;
            this.line5.Width = 0F;
            this.line5.X1 = 1.185039F;
            this.line5.X2 = 1.185039F;
            this.line5.Y1 = 0.288189F;
            this.line5.Y2 = 0.819685F;
            // 
            // line4
            // 
            this.line4.Height = 0.531496F;
            this.line4.Left = 1.432677F;
            this.line4.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.288189F;
            this.line4.Width = 0F;
            this.line4.X1 = 1.432677F;
            this.line4.X2 = 1.432677F;
            this.line4.Y1 = 0.288189F;
            this.line4.Y2 = 0.819685F;
            // 
            // txtTitle12
            // 
            this.txtTitle12.DataField = "ITEM10";
            this.txtTitle12.Height = 0.5314961F;
            this.txtTitle12.Left = 8.527953F;
            this.txtTitle12.Name = "txtTitle12";
            this.txtTitle12.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 1, 0, 0);
            this.txtTitle12.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: top";
            this.txtTitle12.Text = null;
            this.txtTitle12.Top = 0.288189F;
            this.txtTitle12.Width = 0.7874016F;
            // 
            // txtTitle11
            // 
            this.txtTitle11.DataField = "ITEM09";
            this.txtTitle11.Height = 0.5314961F;
            this.txtTitle11.Left = 7.736615F;
            this.txtTitle11.Name = "txtTitle11";
            this.txtTitle11.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 1, 0, 0);
            this.txtTitle11.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: top";
            this.txtTitle11.Text = null;
            this.txtTitle11.Top = 0.288189F;
            this.txtTitle11.Width = 0.7874016F;
            // 
            // txtTitle10
            // 
            this.txtTitle10.DataField = "ITEM08";
            this.txtTitle10.Height = 0.5314961F;
            this.txtTitle10.Left = 6.949213F;
            this.txtTitle10.Name = "txtTitle10";
            this.txtTitle10.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 1, 0, 0);
            this.txtTitle10.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: top";
            this.txtTitle10.Text = null;
            this.txtTitle10.Top = 0.288189F;
            this.txtTitle10.Width = 0.7874016F;
            // 
            // txtTitle09
            // 
            this.txtTitle09.DataField = "ITEM07";
            this.txtTitle09.Height = 0.5314961F;
            this.txtTitle09.Left = 6.161811F;
            this.txtTitle09.Name = "txtTitle09";
            this.txtTitle09.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 1, 0, 0);
            this.txtTitle09.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: top";
            this.txtTitle09.Text = null;
            this.txtTitle09.Top = 0.288189F;
            this.txtTitle09.Width = 0.7874016F;
            // 
            // txtTitle08
            // 
            this.txtTitle08.DataField = "ITEM06";
            this.txtTitle08.Height = 0.5314961F;
            this.txtTitle08.Left = 5.37441F;
            this.txtTitle08.Name = "txtTitle08";
            this.txtTitle08.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 1, 0, 0);
            this.txtTitle08.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: top";
            this.txtTitle08.Text = null;
            this.txtTitle08.Top = 0.288189F;
            this.txtTitle08.Width = 0.7874016F;
            // 
            // txtTitle07
            // 
            this.txtTitle07.DataField = "ITEM05";
            this.txtTitle07.Height = 0.5314961F;
            this.txtTitle07.Left = 4.587008F;
            this.txtTitle07.Name = "txtTitle07";
            this.txtTitle07.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 1, 0, 0);
            this.txtTitle07.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: top";
            this.txtTitle07.Text = null;
            this.txtTitle07.Top = 0.288189F;
            this.txtTitle07.Width = 0.7874016F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.DataField = "ITEM04";
            this.txtTitle06.Height = 0.5314961F;
            this.txtTitle06.Left = 3.799607F;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 1, 0, 0);
            this.txtTitle06.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: top";
            this.txtTitle06.Text = null;
            this.txtTitle06.Top = 0.288189F;
            this.txtTitle06.Width = 0.7874016F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.Height = 0.5314961F;
            this.txtTitle05.Left = 3.019292F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.txtTitle05.Text = "計";
            this.txtTitle05.Top = 0.2917323F;
            this.txtTitle05.Width = 0.7874016F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.5314961F;
            this.txtTitle04.Left = 2.23189F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: middle";
            this.txtTitle04.Text = "消費税";
            this.txtTitle04.Top = 0.2917323F;
            this.txtTitle04.Width = 0.7874016F;
            // 
            // line23
            // 
            this.line23.Height = 0.531496F;
            this.line23.Left = 2.23189F;
            this.line23.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.288189F;
            this.line23.Width = 0F;
            this.line23.X1 = 2.23189F;
            this.line23.X2 = 2.23189F;
            this.line23.Y1 = 0.288189F;
            this.line23.Y2 = 0.819685F;
            // 
            // line24
            // 
            this.line24.Height = 0.531496F;
            this.line24.Left = 3.019292F;
            this.line24.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.288189F;
            this.line24.Width = 0F;
            this.line24.X1 = 3.019292F;
            this.line24.X2 = 3.019292F;
            this.line24.Y1 = 0.288189F;
            this.line24.Y2 = 0.819685F;
            // 
            // line25
            // 
            this.line25.Height = 0.531496F;
            this.line25.Left = 3.806693F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.288189F;
            this.line25.Width = 0F;
            this.line25.X1 = 3.806693F;
            this.line25.X2 = 3.806693F;
            this.line25.Y1 = 0.288189F;
            this.line25.Y2 = 0.819685F;
            // 
            // txtTitle13
            // 
            this.txtTitle13.DataField = "ITEM11";
            this.txtTitle13.Height = 0.5314961F;
            this.txtTitle13.Left = 9.322835F;
            this.txtTitle13.Name = "txtTitle13";
            this.txtTitle13.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 1, 0, 0);
            this.txtTitle13.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: top";
            this.txtTitle13.Text = null;
            this.txtTitle13.Top = 0.288189F;
            this.txtTitle13.Width = 0.7874016F;
            // 
            // txtTitle14
            // 
            this.txtTitle14.DataField = "ITEM12";
            this.txtTitle14.Height = 0.5314961F;
            this.txtTitle14.Left = 10.11024F;
            this.txtTitle14.Name = "txtTitle14";
            this.txtTitle14.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 1, 0, 0);
            this.txtTitle14.Style = "background-color: Cyan; font-family: MS UI Gothic; font-size: 9pt; font-weight: n" +
    "ormal; text-align: center; vertical-align: top";
            this.txtTitle14.Text = null;
            this.txtTitle14.Top = 0.2917323F;
            this.txtTitle14.Width = 0.7874016F;
            // 
            // line16
            // 
            this.line16.Height = 0F;
            this.line16.Left = 0F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.2917323F;
            this.line16.Width = 10.90158F;
            this.line16.X1 = 0F;
            this.line16.X2 = 10.90158F;
            this.line16.Y1 = 0.2917323F;
            this.line16.Y2 = 0.2917323F;
            // 
            // line17
            // 
            this.line17.Height = 0F;
            this.line17.Left = 0F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0.8232284F;
            this.line17.Width = 10.90158F;
            this.line17.X1 = 0F;
            this.line17.X2 = 10.90158F;
            this.line17.Y1 = 0.8232284F;
            this.line17.Y2 = 0.8232284F;
            // 
            // line6
            // 
            this.line6.Height = 0.531496F;
            this.line6.Left = 4.587008F;
            this.line6.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.288189F;
            this.line6.Width = 0F;
            this.line6.X1 = 4.587008F;
            this.line6.X2 = 4.587008F;
            this.line6.Y1 = 0.288189F;
            this.line6.Y2 = 0.819685F;
            // 
            // line7
            // 
            this.line7.Height = 0.531496F;
            this.line7.Left = 5.37441F;
            this.line7.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.288189F;
            this.line7.Width = 0F;
            this.line7.X1 = 5.37441F;
            this.line7.X2 = 5.37441F;
            this.line7.Y1 = 0.288189F;
            this.line7.Y2 = 0.819685F;
            // 
            // line8
            // 
            this.line8.Height = 0.531496F;
            this.line8.Left = 6.161811F;
            this.line8.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.288189F;
            this.line8.Width = 0F;
            this.line8.X1 = 6.161811F;
            this.line8.X2 = 6.161811F;
            this.line8.Y1 = 0.288189F;
            this.line8.Y2 = 0.819685F;
            // 
            // line26
            // 
            this.line26.Height = 0.531496F;
            this.line26.Left = 6.949213F;
            this.line26.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 0.288189F;
            this.line26.Width = 0F;
            this.line26.X1 = 6.949213F;
            this.line26.X2 = 6.949213F;
            this.line26.Y1 = 0.288189F;
            this.line26.Y2 = 0.819685F;
            // 
            // line27
            // 
            this.line27.Height = 0.531496F;
            this.line27.Left = 7.736615F;
            this.line27.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 0.288189F;
            this.line27.Width = 0F;
            this.line27.X1 = 7.736615F;
            this.line27.X2 = 7.736615F;
            this.line27.Y1 = 0.288189F;
            this.line27.Y2 = 0.819685F;
            // 
            // line28
            // 
            this.line28.Height = 0.531496F;
            this.line28.Left = 8.535434F;
            this.line28.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 0.288189F;
            this.line28.Width = 0F;
            this.line28.X1 = 8.535434F;
            this.line28.X2 = 8.535434F;
            this.line28.Y1 = 0.288189F;
            this.line28.Y2 = 0.819685F;
            // 
            // line29
            // 
            this.line29.Height = 0.531496F;
            this.line29.Left = 9.322835F;
            this.line29.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 0.288189F;
            this.line29.Width = 0F;
            this.line29.X1 = 9.322835F;
            this.line29.X2 = 9.322835F;
            this.line29.Y1 = 0.288189F;
            this.line29.Y2 = 0.819685F;
            // 
            // line30
            // 
            this.line30.Height = 0.531496F;
            this.line30.Left = 10.11024F;
            this.line30.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 0.288189F;
            this.line30.Width = 0F;
            this.line30.X1 = 10.11024F;
            this.line30.X2 = 10.11024F;
            this.line30.Y1 = 0.288189F;
            this.line30.Y2 = 0.819685F;
            // 
            // line9
            // 
            this.line9.Height = 0.531496F;
            this.line9.Left = 10.90158F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0.288189F;
            this.line9.Width = 0F;
            this.line9.X1 = 10.90158F;
            this.line9.X2 = 10.90158F;
            this.line9.Y1 = 0.288189F;
            this.line9.Y2 = 0.819685F;
            // 
            // line3
            // 
            this.line3.Height = 0.531496F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.288189F;
            this.line3.Width = 0F;
            this.line3.X1 = 0F;
            this.line3.X2 = 0F;
            this.line3.Y1 = 0.288189F;
            this.line3.Y2 = 0.819685F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtKamokuNo,
            this.txtZeiRitsu,
            this.txtZeinukiKingaku,
            this.txtShohizei,
            this.txtKei,
            this.txtValue01,
            this.txtValue02,
            this.txtKamokuNm,
            this.line2,
            this.txtValue03,
            this.txtValue04,
            this.txtValue05,
            this.txtValue06,
            this.txtValue07,
            this.txtValue08,
            this.txtValue09});
            this.detail.Height = 0.2236221F;
            this.detail.Name = "detail";
            // 
            // txtKamokuNo
            // 
            this.txtKamokuNo.DataField = "ITEM13";
            this.txtKamokuNo.Height = 0.09842519F;
            this.txtKamokuNo.Left = 0.06259843F;
            this.txtKamokuNo.Name = "txtKamokuNo";
            this.txtKamokuNo.Style = "font-family: ＭＳ ゴシック; font-size: 8.25pt; font-weight: normal; text-align: right; " +
    "vertical-align: top";
            this.txtKamokuNo.Text = null;
            this.txtKamokuNo.Top = 0.003937008F;
            this.txtKamokuNo.Width = 0.3330709F;
            // 
            // txtZeiRitsu
            // 
            this.txtZeiRitsu.DataField = "ITEM15";
            this.txtZeiRitsu.Height = 0.1968504F;
            this.txtZeiRitsu.Left = 1.204725F;
            this.txtZeiRitsu.Name = "txtZeiRitsu";
            this.txtZeiRitsu.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtZeiRitsu.Text = null;
            this.txtZeiRitsu.Top = 0F;
            this.txtZeiRitsu.Width = 0.2165354F;
            // 
            // txtZeinukiKingaku
            // 
            this.txtZeinukiKingaku.DataField = "ITEM16";
            this.txtZeinukiKingaku.Height = 0.1968504F;
            this.txtZeinukiKingaku.Left = 1.460236F;
            this.txtZeinukiKingaku.Name = "txtZeinukiKingaku";
            this.txtZeinukiKingaku.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtZeinukiKingaku.Text = null;
            this.txtZeinukiKingaku.Top = 0.003937008F;
            this.txtZeinukiKingaku.Width = 0.7582677F;
            // 
            // txtShohizei
            // 
            this.txtShohizei.DataField = "ITEM17";
            this.txtShohizei.Height = 0.1968504F;
            this.txtShohizei.Left = 2.250787F;
            this.txtShohizei.Name = "txtShohizei";
            this.txtShohizei.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtShohizei.Text = null;
            this.txtShohizei.Top = 0F;
            this.txtShohizei.Width = 0.7582677F;
            // 
            // txtKei
            // 
            this.txtKei.DataField = "ITEM18";
            this.txtKei.Height = 0.1968504F;
            this.txtKei.Left = 3.035827F;
            this.txtKei.Name = "txtKei";
            this.txtKei.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtKei.Text = null;
            this.txtKei.Top = 0F;
            this.txtKei.Width = 0.7582677F;
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM19";
            this.txtValue01.Height = 0.1968504F;
            this.txtValue01.Left = 3.817323F;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0F;
            this.txtValue01.Width = 0.7582677F;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM20";
            this.txtValue02.Height = 0.1968504F;
            this.txtValue02.Left = 4.608662F;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0F;
            this.txtValue02.Width = 0.7582677F;
            // 
            // txtKamokuNm
            // 
            this.txtKamokuNm.DataField = "ITEM14";
            this.txtKamokuNm.Height = 0.1023622F;
            this.txtKamokuNm.Left = 0.06259843F;
            this.txtKamokuNm.Name = "txtKamokuNm";
            this.txtKamokuNm.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: left; vert" +
    "ical-align: bottom";
            this.txtKamokuNm.Text = null;
            this.txtKamokuNm.Top = 0.1062992F;
            this.txtKamokuNm.Width = 1.103937F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.2165354F;
            this.line2.Width = 10.90158F;
            this.line2.X1 = 0F;
            this.line2.X2 = 10.90158F;
            this.line2.Y1 = 0.2165354F;
            this.line2.Y2 = 0.2165354F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM21";
            this.txtValue03.Height = 0.1968504F;
            this.txtValue03.Left = 5.387402F;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue03.Text = null;
            this.txtValue03.Top = 0F;
            this.txtValue03.Width = 0.7582681F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM22";
            this.txtValue04.Height = 0.1968504F;
            this.txtValue04.Left = 6.161811F;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0F;
            this.txtValue04.Width = 0.7582681F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM23";
            this.txtValue05.Height = 0.1968504F;
            this.txtValue05.Left = 6.970473F;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0.003937008F;
            this.txtValue05.Width = 0.7582681F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM24";
            this.txtValue06.Height = 0.1968504F;
            this.txtValue06.Left = 7.757874F;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue06.Text = null;
            this.txtValue06.Top = 0F;
            this.txtValue06.Width = 0.7582681F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM25";
            this.txtValue07.Height = 0.1968504F;
            this.txtValue07.Left = 8.549213F;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0F;
            this.txtValue07.Width = 0.7582681F;
            // 
            // txtValue08
            // 
            this.txtValue08.DataField = "ITEM26";
            this.txtValue08.Height = 0.1968504F;
            this.txtValue08.Left = 9.340158F;
            this.txtValue08.Name = "txtValue08";
            this.txtValue08.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue08.Text = null;
            this.txtValue08.Top = 0F;
            this.txtValue08.Width = 0.7582681F;
            // 
            // txtValue09
            // 
            this.txtValue09.DataField = "ITEM27";
            this.txtValue09.Height = 0.1968504F;
            this.txtValue09.Left = 10.13937F;
            this.txtValue09.Name = "txtValue09";
            this.txtValue09.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue09.Text = null;
            this.txtValue09.Top = 0F;
            this.txtValue09.Width = 0.7582681F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Name = "reportHeader1";
            this.reportHeader1.Visible = false;
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line1,
            this.textBox1,
            this.txtZeinukiKingakuTotal,
            this.txtShohizeiTotal,
            this.txtKeiTotal,
            this.txtValue01Total,
            this.txtValue02Total,
            this.txtValue03Total,
            this.txtValue04Total,
            this.txtValue05Total,
            this.txtValue06Total,
            this.txtValue07Total,
            this.txtValue08Total,
            this.txtValue09Total});
            this.reportFooter1.Height = 0.21875F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2165354F;
            this.line1.Width = 10.90158F;
            this.line1.X1 = 0F;
            this.line1.X2 = 10.90158F;
            this.line1.Y1 = 0.2165354F;
            this.line1.Y2 = 0.2165354F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.1968504F;
            this.textBox1.Left = 0.01574803F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; ve" +
    "rtical-align: bottom";
            this.textBox1.Text = "【合計】";
            this.textBox1.Top = 0.007874017F;
            this.textBox1.Width = 1.150787F;
            // 
            // txtZeinukiKingakuTotal
            // 
            this.txtZeinukiKingakuTotal.DataField = "ITEM16";
            this.txtZeinukiKingakuTotal.Height = 0.1968504F;
            this.txtZeinukiKingakuTotal.Left = 1.460236F;
            this.txtZeinukiKingakuTotal.Name = "txtZeinukiKingakuTotal";
            this.txtZeinukiKingakuTotal.OutputFormat = resources.GetString("txtZeinukiKingakuTotal.OutputFormat");
            this.txtZeinukiKingakuTotal.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtZeinukiKingakuTotal.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtZeinukiKingakuTotal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtZeinukiKingakuTotal.Text = null;
            this.txtZeinukiKingakuTotal.Top = 0F;
            this.txtZeinukiKingakuTotal.Width = 0.7582681F;
            // 
            // txtShohizeiTotal
            // 
            this.txtShohizeiTotal.DataField = "ITEM17";
            this.txtShohizeiTotal.Height = 0.1968504F;
            this.txtShohizeiTotal.Left = 2.250787F;
            this.txtShohizeiTotal.Name = "txtShohizeiTotal";
            this.txtShohizeiTotal.OutputFormat = resources.GetString("txtShohizeiTotal.OutputFormat");
            this.txtShohizeiTotal.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtShohizeiTotal.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtShohizeiTotal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtShohizeiTotal.Text = null;
            this.txtShohizeiTotal.Top = 0F;
            this.txtShohizeiTotal.Width = 0.7582681F;
            // 
            // txtKeiTotal
            // 
            this.txtKeiTotal.DataField = "ITEM18";
            this.txtKeiTotal.Height = 0.1968504F;
            this.txtKeiTotal.Left = 3.035827F;
            this.txtKeiTotal.Name = "txtKeiTotal";
            this.txtKeiTotal.OutputFormat = resources.GetString("txtKeiTotal.OutputFormat");
            this.txtKeiTotal.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtKeiTotal.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtKeiTotal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtKeiTotal.Text = null;
            this.txtKeiTotal.Top = 0F;
            this.txtKeiTotal.Width = 0.7582681F;
            // 
            // txtValue01Total
            // 
            this.txtValue01Total.DataField = "ITEM19";
            this.txtValue01Total.Height = 0.1968504F;
            this.txtValue01Total.Left = 3.817323F;
            this.txtValue01Total.Name = "txtValue01Total";
            this.txtValue01Total.OutputFormat = resources.GetString("txtValue01Total.OutputFormat");
            this.txtValue01Total.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue01Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtValue01Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue01Total.Text = null;
            this.txtValue01Total.Top = 0F;
            this.txtValue01Total.Width = 0.7582681F;
            // 
            // txtValue02Total
            // 
            this.txtValue02Total.DataField = "ITEM20";
            this.txtValue02Total.Height = 0.1968504F;
            this.txtValue02Total.Left = 4.608662F;
            this.txtValue02Total.Name = "txtValue02Total";
            this.txtValue02Total.OutputFormat = resources.GetString("txtValue02Total.OutputFormat");
            this.txtValue02Total.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue02Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtValue02Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue02Total.Text = null;
            this.txtValue02Total.Top = 0F;
            this.txtValue02Total.Width = 0.7582681F;
            // 
            // txtValue03Total
            // 
            this.txtValue03Total.DataField = "ITEM21";
            this.txtValue03Total.Height = 0.1968504F;
            this.txtValue03Total.Left = 5.387402F;
            this.txtValue03Total.Name = "txtValue03Total";
            this.txtValue03Total.OutputFormat = resources.GetString("txtValue03Total.OutputFormat");
            this.txtValue03Total.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue03Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtValue03Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue03Total.Text = null;
            this.txtValue03Total.Top = 0F;
            this.txtValue03Total.Width = 0.7582681F;
            // 
            // txtValue04Total
            // 
            this.txtValue04Total.DataField = "ITEM22";
            this.txtValue04Total.Height = 0.1968504F;
            this.txtValue04Total.Left = 6.161811F;
            this.txtValue04Total.Name = "txtValue04Total";
            this.txtValue04Total.OutputFormat = resources.GetString("txtValue04Total.OutputFormat");
            this.txtValue04Total.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue04Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtValue04Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue04Total.Text = null;
            this.txtValue04Total.Top = 0F;
            this.txtValue04Total.Width = 0.7582681F;
            // 
            // txtValue05Total
            // 
            this.txtValue05Total.DataField = "ITEM23";
            this.txtValue05Total.Height = 0.1968504F;
            this.txtValue05Total.Left = 6.970473F;
            this.txtValue05Total.Name = "txtValue05Total";
            this.txtValue05Total.OutputFormat = resources.GetString("txtValue05Total.OutputFormat");
            this.txtValue05Total.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue05Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtValue05Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue05Total.Text = null;
            this.txtValue05Total.Top = 0F;
            this.txtValue05Total.Width = 0.7582681F;
            // 
            // txtValue06Total
            // 
            this.txtValue06Total.DataField = "ITEM24";
            this.txtValue06Total.Height = 0.1968504F;
            this.txtValue06Total.Left = 7.757874F;
            this.txtValue06Total.Name = "txtValue06Total";
            this.txtValue06Total.OutputFormat = resources.GetString("txtValue06Total.OutputFormat");
            this.txtValue06Total.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue06Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtValue06Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue06Total.Text = null;
            this.txtValue06Total.Top = 0F;
            this.txtValue06Total.Width = 0.7582681F;
            // 
            // txtValue07Total
            // 
            this.txtValue07Total.DataField = "ITEM25";
            this.txtValue07Total.Height = 0.1968504F;
            this.txtValue07Total.Left = 8.549213F;
            this.txtValue07Total.Name = "txtValue07Total";
            this.txtValue07Total.OutputFormat = resources.GetString("txtValue07Total.OutputFormat");
            this.txtValue07Total.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue07Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtValue07Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue07Total.Text = null;
            this.txtValue07Total.Top = 0F;
            this.txtValue07Total.Width = 0.7582681F;
            // 
            // txtValue08Total
            // 
            this.txtValue08Total.DataField = "ITEM26";
            this.txtValue08Total.Height = 0.1968504F;
            this.txtValue08Total.Left = 9.340158F;
            this.txtValue08Total.Name = "txtValue08Total";
            this.txtValue08Total.OutputFormat = resources.GetString("txtValue08Total.OutputFormat");
            this.txtValue08Total.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue08Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtValue08Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue08Total.Text = null;
            this.txtValue08Total.Top = 0F;
            this.txtValue08Total.Width = 0.7582681F;
            // 
            // txtValue09Total
            // 
            this.txtValue09Total.DataField = "ITEM27";
            this.txtValue09Total.Height = 0.1968504F;
            this.txtValue09Total.Left = 10.13937F;
            this.txtValue09Total.Name = "txtValue09Total";
            this.txtValue09Total.OutputFormat = resources.GetString("txtValue09Total.OutputFormat");
            this.txtValue09Total.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ver" +
    "tical-align: middle";
            this.txtValue09Total.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtValue09Total.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtValue09Total.Text = null;
            this.txtValue09Total.Top = 0F;
            this.txtValue09Total.Width = 0.7582681F;
            // 
            // ZMYR1032R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5905512F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.90158F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKamokuNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeiRitsu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeinukiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKamokuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeinukiKingakuTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizeiTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeiTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKamokuNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKamokuNm;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZeiRitsu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZeinukiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue09;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZeinukiKingakuTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohizeiTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKeiTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue09Total;
    }
}
