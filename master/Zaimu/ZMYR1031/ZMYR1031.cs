﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using GrapeCity.ActiveReports;

//using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.zam.zame1031;

namespace jp.co.fsi.zm.zmyr1031
{
    /// <summary>
    /// 新 消費税区分集計表(ZMYR1031)
    /// </summary>
    public partial class ZMYR1031 : BasePgForm
    {
        #region 変数
        //int _sort;              // ワークテーブルレコードカウンタ

        //DataTable _dtZeiKubun;                  // 税区分データ
        //DataTable _dtZeiKubunShukei;            // 税区分別税額集計データ
        //DataTable _dtKamokuShukei;              // 科目別税額集計データ 
        #endregion

        #region 定数
        /// <summary>
        /// レポートデータ列数
        /// </summary>
        private const int rptCols1 = 24;
        private const int rptCols2 = 27;
        private const int rptCols3 = 27;
        #endregion

        #region プロパティ
        private string _unqId01;
        private string _unqId02;
        private string _unqId03;
        /// <summary>
        /// ユニークID
        /// </summary>
        private string UnqId01
        {
            get
            {
                return this._unqId01;
            }
        }
        private string UnqId02
        {
            get
            {
                return this._unqId02;
            }
        }
        private string UnqId03
        {
            get
            {
                return this._unqId03;
            }
        }
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMYR1031()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }

            // 帳票穂毎に一意に識別する目的でGUIDを発行する
            Guid myGuid;
            myGuid = Guid.NewGuid();
            this._unqId01 = myGuid.ToString();
            myGuid = Guid.NewGuid();
            this._unqId02 = myGuid.ToString();
            myGuid = Guid.NewGuid();
            this._unqId03 = myGuid.ToString();

            // 会計期間を取得
            string[] jpDateFr = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
            string[] jpDateTo = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);

            // 期間（自）
            lblDateGengoFr.Text = jpDateFr[0];
            txtDateYearFr.Text = jpDateFr[2];
            txtDateMonthFr.Text = jpDateFr[3];
            txtDateDayFr.Text = jpDateFr[4];
            // 期間（至）
            lblDateGengoTo.Text = jpDateTo[0];
            txtDateYearTo.Text = jpDateTo[2];
            txtDateMonthTo.Text = jpDateTo[3];
            txtDateDayTo.Text = jpDateTo[4];

            rdoZeiNuki.Checked = true;

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付にフォーカス時のみF1を有効にする
            switch (this.ActiveControl.Name)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveControl.Name)
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearFr":
                case "txtDateYearTo":
                    #region 元号検索
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveControl.Name == "txtDateYearFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblDateGengoFr.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblDateGengoFr.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        ZaUtil.FixJpDate(this.lblDateGengoFr.Text,
                                            this.txtDateYearFr.Text,
                                            this.txtDateMonthFr.Text,
                                            this.txtDateDayFr.Text,
                                            this.Dba);
                                    SetJpDateFr(arrJpDate);
                                }
                            }
                            else if (this.ActiveControl.Name == "txtDateYearTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblDateGengoTo.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblDateGengoTo.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        ZaUtil.FixJpDate(this.lblDateGengoTo.Text,
                                            this.txtDateYearTo.Text,
                                            this.txtDateMonthTo.Text,
                                            this.txtDateDayTo.Text,
                                            this.Dba);
                                    SetJpDateTo(arrJpDate);
                                }
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            //PrintSettingForm psForm = new PrintSettingForm(new string[2] { "ZMYR1031R", "ZMYR1032R" });
            PrintSettingForm psForm = new PrintSettingForm(new string[3] { "ZMYR1031R", "ZMYR1032RU", "ZMYR1032RS" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
            }
        }

        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidGengoYearFr())
            {
                this.txtDateYearFr.SelectAll();
                this.txtDateYearFr.Focus();
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidMonthFr())
            {
                this.txtDateMonthFr.SelectAll();
                this.txtDateMonthFr.Focus();
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// 日(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidDayFr())
            {
                this.txtDateDayFr.SelectAll();
                this.txtDateDayFr.Focus();
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidGengoYearTo())
            {
                this.txtDateYearTo.SelectAll();
                this.txtDateYearTo.Focus();
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidMonthTo())
            {
                this.txtDateMonthTo.SelectAll();
                this.txtDateMonthTo.Focus();
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// 日(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidDayTo())
            {
                this.txtDateDayTo.SelectAll();
                this.txtDateDayTo.Focus();
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// ラジオボタンでEnterキーが押された場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Enter)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 印刷処理
                this.PressF4();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年(自)の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidGengoYearFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateYearFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(ZaUtil.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));

            // 画面日付を取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            // 会計期間の開始日の日付を取得
            string[] jpDateFr = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
            DateTime tmpKaikeiDate = Util.ConvAdDate(jpDateFr[0], jpDateFr[2], jpDateFr[3], jpDateFr[4], this.Dba);

            // 現在設定している会計期間の開始日と比較する
            if (tmpDate < tmpKaikeiDate)
            {
                // 開始日より過去が入力された場合、開始日を表示する
                lblDateGengoFr.Text = jpDateFr[0];
                txtDateYearFr.Text = jpDateFr[2];
                txtDateMonthFr.Text = jpDateFr[3];
                txtDateDayFr.Text = jpDateFr[4];
            }

            return true;
        }
        
        /// <summary>
        /// 月(自)の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidMonthFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(ZaUtil.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));

            // 画面日付を取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            // 会計期間の開始日の日付を取得
            string[] jpDateFr = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
            DateTime tmpKaikeiDate = Util.ConvAdDate(jpDateFr[0], jpDateFr[2], jpDateFr[3], jpDateFr[4], this.Dba);

            // 現在設定している会計期間の開始日と比較する
            if (tmpDate < tmpKaikeiDate)
            {
                // 開始日より過去が入力された場合、開始日を表示する
                lblDateGengoFr.Text = jpDateFr[0];
                txtDateYearFr.Text = jpDateFr[2];
                txtDateMonthFr.Text = jpDateFr[3];
                txtDateDayFr.Text = jpDateFr[4];
            }

            return true;
        }
        
        /// <summary>
        /// 日(自)の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidDayFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateDayFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(ZaUtil.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));

            // 画面日付を取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            // 会計期間の開始日の日付を取得
            string[] jpDateFr = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
            DateTime tmpKaikeiDate = Util.ConvAdDate(jpDateFr[0], jpDateFr[2], jpDateFr[3], jpDateFr[4], this.Dba);

            // 現在設定している会計期間の開始日と比較する
            if (tmpDate < tmpKaikeiDate)
            {
                // 開始日より過去が入力された場合、開始日を表示する
                lblDateGengoFr.Text = jpDateFr[0];
                txtDateYearFr.Text = jpDateFr[2];
                txtDateMonthFr.Text = jpDateFr[3];
                txtDateDayFr.Text = jpDateFr[4];
            }
            
            return true;
        }

        /// <summary>
        /// 年(至)の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidGengoYearTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateYearTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(ZaUtil.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));

            // 画面日付を取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 会計期間の終了日の日付を取得
            string[] jpDateTo = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);
            DateTime tmpKaikeiDate = Util.ConvAdDate(jpDateTo[0], jpDateTo[2], jpDateTo[3], jpDateTo[4], this.Dba);

            // 現在設定している会計期間の終了日と比較する
            if (tmpDate > tmpKaikeiDate)
            {
                // 終了日より未来が入力された場合、終了日を表示する
                lblDateGengoTo.Text = jpDateTo[0];
                txtDateYearTo.Text = jpDateTo[2];
                txtDateMonthTo.Text = jpDateTo[3];
                txtDateDayTo.Text = jpDateTo[4];
            }

            return true;
        }
        
        /// <summary>
        /// 月(至)の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidMonthTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateMonthTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(ZaUtil.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));

            // 画面日付を取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 会計期間の終了日の日付を取得
            string[] jpDateTo = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);
            DateTime tmpKaikeiDate = Util.ConvAdDate(jpDateTo[0], jpDateTo[2], jpDateTo[3], jpDateTo[4], this.Dba);

            // 現在設定している会計期間の終了日と比較する
            if (tmpDate > tmpKaikeiDate)
            {
                // 終了日より未来が入力された場合、終了日を表示する
                lblDateGengoTo.Text = jpDateTo[0];
                txtDateYearTo.Text = jpDateTo[2];
                txtDateMonthTo.Text = jpDateTo[3];
                txtDateDayTo.Text = jpDateTo[4];
            }

            return true;
        }
        
        /// <summary>
        /// 日(至)の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidDayTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDateDayTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(ZaUtil.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));

            // 画面日付を取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 会計期間の終了日の日付を取得
            string[] jpDateTo = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);
            DateTime tmpKaikeiDate = Util.ConvAdDate(jpDateTo[0], jpDateTo[2], jpDateTo[3], jpDateTo[4], this.Dba);

            // 現在設定している会計期間の終了日と比較する
            if (tmpDate > tmpKaikeiDate)
            {
                // 終了日より未来が入力された場合、終了日を表示する
                lblDateGengoTo.Text = jpDateTo[0];
                txtDateYearTo.Text = jpDateTo[2];
                txtDateMonthTo.Text = jpDateTo[3];
                txtDateDayTo.Text = jpDateTo[4];
            }

            return true;
        }

        /// <summary>
        /// 出力帳票選択値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidChohyo()
        {
            if (!this.chkChohyo1.Checked && !this.chkChohyo2.Checked && !this.chkChohyo3.Checked)
            {
                Msg.Notice("出力帳票は1項目以上選択してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)の入力チェック
            if (!this.isValidGengoYearFr())
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }

            // 月(自)の入力チェック
            if (!this.isValidMonthFr())
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }

            // 日(自)の入力チェック
            if (!this.isValidDayFr())
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }

            // 年(至)の入力チェック
            if (!this.isValidGengoYearTo())
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }

            // 月(至)の入力チェック
            if (!this.isValidMonthTo())
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }

            // 日(至)の入力チェック
            if (!this.isValidDayTo())
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }

            // 出力帳票の選択チェック
            if (!this.isValidChohyo())
            {
                this.fsiPanel3.Focus();
                this.fsiPanel3.Select();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            string[] fixJpDate =
                Util.ConvJpDate(
                ZaUtil.FixNendoDate(
                Util.ConvAdDate(arrJpDate[0], arrJpDate[2], arrJpDate[3], arrJpDate[4], this.Dba), this.UInfo)
                , this.Dba);

            this.lblDateGengoFr.Text = fixJpDate[0];
            this.txtDateYearFr.Text = fixJpDate[2];
            this.txtDateMonthFr.Text = fixJpDate[3];
            this.txtDateDayFr.Text = fixJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            string[] fixJpDate =
                Util.ConvJpDate(
                ZaUtil.FixNendoDate(
                Util.ConvAdDate(arrJpDate[0], arrJpDate[2], arrJpDate[3], arrJpDate[4], this.Dba), this.UInfo)
                , this.Dba);

            this.lblDateGengoTo.Text = fixJpDate[0];
            this.txtDateYearTo.Text = fixJpDate[2];
            this.txtDateMonthTo.Text = fixJpDate[3];
            this.txtDateDayTo.Text = fixJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する。
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag01 = MakeWkData01();
                bool dataFlag02 = MakeWkData02();
                bool dataFlag03 = MakeWkData03();

                // 帳票出力
                if (dataFlag01 || dataFlag02 || dataFlag03)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    DbParamCollection dpc = new DbParamCollection();

                    #region 区分別消費税額集計表
                    DataTable dtOutput01 = new DataTable();
                    if (dataFlag01)
                    {
                        cols.Append(Util.ColsArray(rptCols1, ""));
                        // バインドパラメータの設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId01);
                        // データの取得
                        dtOutput01 = this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);
                    }
                    #endregion

                    cols = new StringBuilder();

                    #region 売上科目別消費税額集計表
                    DataTable dtOutput02 = new DataTable(); ;
                    if (dataFlag02)
                    {
                        cols.Append(Util.ColsArray(rptCols2, ""));
                        // バインドパラメータの設定
                        dpc = new DbParamCollection();
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId02);
                        // データの取得
                        dtOutput02 = this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);
                    }
                    #endregion

                    cols = new StringBuilder();

                    #region 仕入科目別消費税額集計表
                    DataTable dtOutput03 = new DataTable(); ;
                    if (dataFlag03)
                    {
                        cols.Append(Util.ColsArray(rptCols3, ""));
                        // バインドパラメータの設定
                        dpc = new DbParamCollection();
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId03);
                        // データの取得
                        dtOutput03 = this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);
                    }
                    #endregion

                    // 帳票オブジェクトをインスタンス化
                    ZMYR1031R rpt01 = new ZMYR1031R(dtOutput01);
                    //ZMYR1032R rpt02 = new ZMYR1032R(dtOutput02);
                    //ZMYR1032R rpt03 = new ZMYR1032R(dtOutput03);
                    ZMYR1032R rpt02 = new ZMYR1032R(dtOutput02, "ZMYR1032RU");
                    ZMYR1032R rpt03 = new ZMYR1032R(dtOutput03, "ZMYR1032RS");

                    rpt01.Run(false);
                    rpt02.Run(false);
                    rpt03.Run(false);

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        if (dataFlag01)
                        {
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt01.Document.Name, 2);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                xlsExport1.Export(rpt01.Document, saveFileName);
                                Msg.InfoNm("EXCEL出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                        if (dataFlag02)
                        {
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt02.Document.Name, 2);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                xlsExport1.Export(rpt02.Document, saveFileName);
                                Msg.InfoNm("EXCEL出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                        if (dataFlag03)
                        {
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt03.Document.Name, 2);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                xlsExport1.Export(rpt03.Document, saveFileName);
                                Msg.InfoNm("EXCEL出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        if (dataFlag01)
                        {
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt01.Document.Name, 1);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                p.Export(rpt01.Document, saveFileName);
                                Msg.InfoNm("PDF出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                        if (dataFlag02)
                        {
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt02.Document.Name, 1);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                p.Export(rpt02.Document, saveFileName);
                                Msg.InfoNm("PDF出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                        if (dataFlag03)
                        {
                            string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt03.Document.Name, 1);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                p.Export(rpt03.Document, saveFileName);
                                Msg.InfoNm("PDF出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        if (dataFlag01)
                        {
                            PreviewForm pFrm01 = new PreviewForm(rpt01, this.UnqId01);
                            pFrm01.Show();
                            pFrm01.WindowState = FormWindowState.Maximized;
                        }
                        if (dataFlag02)
                        {
                            PreviewForm pFrm02 = new PreviewForm(rpt02, this.UnqId02);
                            pFrm02.Show();
                            pFrm02.WindowState = FormWindowState.Maximized;
                        }
                        if (dataFlag03)
                        {
                            PreviewForm pFrm03 = new PreviewForm(rpt03, this.UnqId03);
                            pFrm03.Show();
                            pFrm03.WindowState = FormWindowState.Maximized;
                        }
                    }
                    else
                    {
                        // 直接印刷
                        if (dataFlag01)
                        {
                            rpt01.Document.Print(true, true, false);
                        }
                        if (dataFlag02)
                        {
                            rpt02.Document.Print(true, true, false);
                        }
                        if (dataFlag03)
                        {
                            rpt03.Document.Print(true, true, false);
                        }
                    }
                }
                else
                {
                    Msg.Notice("該当するデータがありません。");
                }
            }
            catch(Exception ex)
            {
               //Console.WriteLine(ex.Message);
               MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData01()
        {
            bool dataFlag = false;

            // 区分別消費税額集計表が選択されている場合に実行
            if (this.chkChohyo1.Checked)
            {
                MakeWkDataCheck01();

                // 印刷ワークテーブルのデータ件数を取得
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId01);
                DataTable tmpDt = this.Dba.GetDataTableByConditionWithParams(
                    "SORT", "PR_ZM_TBL", "GUID = @GUID", dpc);
                if (tmpDt.Rows.Count > 0)
                {
                    dataFlag = true;
                }
            }

            return dataFlag;
        }
        private bool MakeWkData02()
        {
            bool dataFlag = false;

            // 売上科目別消費税額集計表が選択されている場合に実行
            if (this.chkChohyo2.Checked)
            {
                MakeWkDataCheck02();

                // 印刷ワークテーブルのデータ件数を取得
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId02);
                DataTable tmpDt = this.Dba.GetDataTableByConditionWithParams(
                    "SORT", "PR_ZM_TBL", "GUID = @GUID", dpc);
                if (tmpDt.Rows.Count > 0)
                {
                    dataFlag = true;
                }
            }

            return dataFlag;
        }
        private bool MakeWkData03()
        {
            bool dataFlag = false;

            // 区分別消費税額集計表が選択されている場合に実行
            if (this.chkChohyo3.Checked)
            {
                MakeWkDataCheck03();

                // 印刷ワークテーブルのデータ件数を取得
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId03);
                DataTable tmpDt = this.Dba.GetDataTableByConditionWithParams(
                    "SORT", "PR_ZM_TBL", "GUID = @GUID", dpc);
                if (tmpDt.Rows.Count > 0)
                {
                    dataFlag = true;
                }
            }

            return dataFlag;
        }

        /// <summary>
        /// ワークテーブルの区分別消費税額集計表データを作成します。
        /// </summary>
        private void MakeWkDataCheck01()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            #region 税区分を取得
            // 日付を西暦にして取得
            DateTime tmpJpFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpJpTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 日付を和暦にして取得
            string[] jpMontFr = Util.ConvJpDate(tmpJpFr, this.Dba);
            string[] jpMontTo = Util.ConvJpDate(tmpJpTo, this.Dba);

            Sql.AppendLine("SELECT");
            Sql.AppendLine(" Z.ZEI_KUBUN,");
            Sql.AppendLine(" MIN(Z.ZEI_KUBUN_NM) AS ZEI_KUBUN_NM,");
            Sql.AppendLine(" Z.ZEI_RITSU,");
            Sql.AppendLine(" SUM(CASE WHEN S.TAISHAKU_KUBUN = Z.TAISHAKU_KUBUN THEN S.ZEINUKI_KINGAKU ELSE S.ZEINUKI_KINGAKU * -1 END) AS ZEINUKI,");
            Sql.AppendLine(" SUM(CASE WHEN S.TAISHAKU_KUBUN = Z.TAISHAKU_KUBUN THEN S.SHOHIZEI_KINGAKU ELSE S.SHOHIZEI_KINGAKU * -1 END) AS SHOHIZEI,");
            Sql.AppendLine(" SUM(CASE WHEN S.TAISHAKU_KUBUN = Z.TAISHAKU_KUBUN THEN S.ZEINUKI_KINGAKU ELSE S.ZEINUKI_KINGAKU * -1 END) +");
            Sql.AppendLine(" SUM(CASE WHEN S.TAISHAKU_KUBUN = Z.TAISHAKU_KUBUN THEN S.SHOHIZEI_KINGAKU ELSE S.SHOHIZEI_KINGAKU * -1 END) AS KEI ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" (SELECT AA.ZEI_KUBUN, AA.ZEI_KUBUN_NM, AA.TAISHAKU_KUBUN, BB.ZEI_RITSU FROM TB_ZM_F_ZEI_KUBUN AS AA");
            Sql.AppendLine(" CROSS JOIN (SELECT 0 AS ZEI_RITSU UNION ALL SELECT 3 AS ZEI_RITSU UNION ALL SELECT 5 AS ZEI_RITSU");
            Sql.AppendLine(" UNION ALL SELECT 8 AS ZEI_RITSU UNION ALL SELECT 10 AS ZEI_RITSU ) AS BB");
            Sql.AppendLine(" WHERE AA.ZEI_KUBUN > 0 AND AA.ZEI_KUBUN < 30) AS Z ");
            Sql.AppendLine("LEFT JOIN");
            Sql.AppendLine(" (SELECT CASE ZEI_KUBUN WHEN 30 THEN 10 WHEN 31 THEN 11 WHEN 34 THEN 14 WHEN 35 THEN 15 WHEN 40 THEN 20 WHEN 50 THEN 10 WHEN 51 THEN 20 WHEN 52 THEN 10 WHEN 53 THEN 20 ELSE ZEI_KUBUN END AS ZEI_KUBUN,");
            Sql.AppendLine(" ZEI_RITSU, TAISHAKU_KUBUN, ZEINUKI_KINGAKU, SHOHIZEI_KINGAKU");
            Sql.AppendLine(" FROM TB_ZM_SHIWAKE_MEISAI");
            Sql.AppendLine(" WHERE KAIKEI_NENDO = @KAIKEI_NENDO AND KAISHA_CD = @KAISHA_CD AND ");
            if (this.txtMizuageShishoCd.Text != "" && this.txtMizuageShishoCd.Text != "0")
            {
                Sql.AppendLine(" SHISHO_CD = @SHISHO_CD AND ");
            }
            Sql.AppendLine(" MEISAI_KUBUN = 0 AND DENPYO_KUBUN = 1 AND ");
            Sql.AppendLine(" DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND ZEI_KUBUN > 0) AS S");
            Sql.AppendLine(" ON Z.ZEI_KUBUN = S.ZEI_KUBUN AND Z.ZEI_RITSU = S.ZEI_RITSU ");
            Sql.AppendLine("GROUP BY");
            Sql.AppendLine(" Z.ZEI_KUBUN, Z.ZEI_RITSU ");
            Sql.AppendLine("ORDER BY");
            Sql.AppendLine(" Z.ZEI_KUBUN, Z.ZEI_RITSU");
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo); // 会計年度
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd); // 会社コード
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpJpFr.Date.ToString("yyyy/MM/dd")); // 日付Fr
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpJpTo.Date.ToString("yyyy/MM/dd")); // 日付To
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text); // 支所コード

            DataTable dtKubunBetsu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            bool flg = false;
            int i = 0;
            int j = 0;
            string beforeKubunBango = "";
            int ritsu01 = 1;
            int ritsu02 = 2;
            int ritsu03 = 3;
            int ritsu04 = 4;
            foreach (DataRow dr in dtKubunBetsu.Rows)
            {
                #region 税区分番号が異なる場合に実行
                if (flg && beforeKubunBango != dr["ZEI_KUBUN"].ToString())
                {
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                #endregion

                #region 税区分番号が異なる場合に実行
                if (beforeKubunBango != dr["ZEI_KUBUN"].ToString())
                {
                    // ITEMXXを初期値に戻す
                    ritsu01 = 1;
                    ritsu02 = 2;
                    ritsu03 = 3;
                    ritsu04 = 4;

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.AppendLine("INSERT INTO PR_ZM_TBL(");
                    Sql.AppendLine(" GUID");
                    Sql.AppendLine(" ,SORT");
                    Sql.AppendLine(" ,ITEM01");
                    Sql.AppendLine(" ,ITEM02");
                    Sql.AppendLine(" ,ITEM03");
                    Sql.AppendLine(" ,ITEM04");
                    Sql.AppendLine(" ,ITEM05");
                    Sql.AppendLine(" ,ITEM06");
                    Sql.AppendLine(" ,ITEM07");
                    Sql.AppendLine(" ,ITEM08");
                    Sql.AppendLine(" ,ITEM09");
                    Sql.AppendLine(" ,ITEM10");
                    Sql.AppendLine(" ,ITEM11");
                    Sql.AppendLine(" ,ITEM12");
                    Sql.AppendLine(" ,ITEM13");
                    Sql.AppendLine(" ,ITEM14");
                    Sql.AppendLine(" ,ITEM15");
                    Sql.AppendLine(" ,ITEM16");
                    Sql.AppendLine(" ,ITEM17");
                    Sql.AppendLine(" ,ITEM18");
                    Sql.AppendLine(" ,ITEM19");
                    Sql.AppendLine(" ,ITEM20");
                    Sql.AppendLine(" ,ITEM21");
                    Sql.AppendLine(" ,ITEM22");
                    Sql.AppendLine(" ,ITEM23");
                    Sql.AppendLine(" ,ITEM24");
                    Sql.AppendLine(") ");
                    Sql.AppendLine("VALUES(");
                    Sql.AppendLine("  @GUID");
                    Sql.AppendLine(" ,@SORT");
                    Sql.AppendLine(" ,@ITEM01");
                    Sql.AppendLine(" ,@ITEM02");
                    Sql.AppendLine(" ,@ITEM03");
                    Sql.AppendLine(" ,@ITEM04");
                    Sql.AppendLine(" ,@ITEM05");
                    Sql.AppendLine(" ,@ITEM06");
                    Sql.AppendLine(" ,@ITEM07");
                    Sql.AppendLine(" ,@ITEM08");
                    Sql.AppendLine(" ,@ITEM09");
                    Sql.AppendLine(" ,@ITEM10");
                    Sql.AppendLine(" ,@ITEM11");
                    Sql.AppendLine(" ,@ITEM12");
                    Sql.AppendLine(" ,@ITEM13");
                    Sql.AppendLine(" ,@ITEM14");
                    Sql.AppendLine(" ,@ITEM15");
                    Sql.AppendLine(" ,@ITEM16");
                    Sql.AppendLine(" ,@ITEM17");
                    Sql.AppendLine(" ,@ITEM18");
                    Sql.AppendLine(" ,@ITEM19");
                    Sql.AppendLine(" ,@ITEM20");
                    Sql.AppendLine(" ,@ITEM21");
                    Sql.AppendLine(" ,@ITEM22");
                    Sql.AppendLine(" ,@ITEM23");
                    Sql.AppendLine(" ,@ITEM24");
                    Sql.AppendLine(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId01);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, "区分別 消費税額集計表"); // タイトル名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, jpMontFr[5] + " ～ " + jpMontTo[5]); // 期間
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["ZEI_KUBUN"].ToString()); // 税区分番号
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["ZEI_KUBUN_NM"].ToString()); // 税区分名
                }
                #endregion

                // ITEMXXを設定
                ritsu01 += 4;
                ritsu02 += 4;
                ritsu03 += 4;
                ritsu04 += 4;

                dpc.SetParam("@ITEM" + Util.PadZero(ritsu01, 2), SqlDbType.VarChar, 200, dr["ZEI_RITSU"].ToString()); // 税率
                // 税抜き
                if (dr["ZEINUKI"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM" + Util.PadZero(ritsu02, 2), SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEINUKI"]));
                }
                else
                {
                    dpc.SetParam("@ITEM" + Util.PadZero(ritsu02, 2), SqlDbType.VarChar, 200, "");
                }
                // 消費税
                if (dr["SHOHIZEI"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM" + Util.PadZero(ritsu03, 2), SqlDbType.VarChar, 200, Util.FormatNum(dr["SHOHIZEI"]));
                }
                else
                {
                    dpc.SetParam("@ITEM" + Util.PadZero(ritsu03, 2), SqlDbType.VarChar, 200, "");
                }
                // 計
                if (dr["KEI"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM" + Util.PadZero(ritsu04, 2), SqlDbType.VarChar, 200, Util.FormatNum(dr["KEI"]));
                }
                else
                {
                    dpc.SetParam("@ITEM" + Util.PadZero(ritsu04, 2), SqlDbType.VarChar, 200, "");
                }

                // 現在の税区分番号を保持
                beforeKubunBango = dr["ZEI_KUBUN"].ToString();
                flg = true;
                j++;

                #region ループの最後に実行
                if (j == dtKubunBetsu.Rows.Count)
                {
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                }
                #endregion
            }
        }

        /// <summary>
        /// ワークテーブルの売上科目別消費税額集計表データを作成します。
        /// </summary>
        private void MakeWkDataCheck02()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            // 日付を西暦にして取得
            DateTime tmpJpFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpJpTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 日付を和暦にして取得
            string[] jpMontFr = Util.ConvJpDate(tmpJpFr, this.Dba);
            string[] jpMontTo = Util.ConvJpDate(tmpJpTo, this.Dba);

            string zei = "";
            if (this.rdoZeiNuki.Checked)
            {
                //zei = "【税抜き】";
                zei = "【" + this.rdoZeiNuki.Text + "】";
            }
            else
            {
                //zei = "【税込み】";
                zei = "【" + this.rdoZeiKomi.Text + "】";
            }

            #region 税区分名を取得
            int zeiKbn0 = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Zai, this.ProductName, "Setting", "Uriage_kbn0"));
            int zeiKbnFr = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Zai, this.ProductName, "Setting", "Uriage_kbnFr"));
            int zeiKbnTo = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Zai, this.ProductName, "Setting", "Uriage_kbnTo"));
            Sql.AppendLine("SELECT");
            Sql.AppendLine(" CASE WHEN ZEI_KUBUN = 0 THEN 1 ELSE 0 END AS stkbn,");
            Sql.AppendLine(" ZEI_KUBUN,");
            Sql.AppendLine(" ZEI_KUBUN_NM ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" TB_ZM_F_ZEI_KUBUN ");
            Sql.AppendLine("WHERE");
            //Sql.AppendLine(" (ZEI_KUBUN = 0) OR (ZEI_KUBUN BETWEEN 10 AND 19) ");
            Sql.AppendLine(" (ZEI_KUBUN = @KUBUN0) OR (ZEI_KUBUN BETWEEN @KUBUN_FR AND @KUBUN_TO) ");
            Sql.AppendLine("ORDER BY");
            Sql.AppendLine(" stkbn, ZEI_KUBUN");
            dpc.SetParam("@KUBUN0", SqlDbType.Decimal, 4, zeiKbn0);
            dpc.SetParam("@KUBUN_FR", SqlDbType.Decimal, 4, zeiKbnFr);
            dpc.SetParam("@KUBUN_TO", SqlDbType.Decimal, 4, zeiKbnTo);

            DataTable dtZeiKubunNm = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 表題を設定
            Hashtable kubunNm = new Hashtable();
            foreach (DataRow dr in dtZeiKubunNm.Rows)
            {
                kubunNm.Add(dr["ZEI_KUBUN"].ToString(), dr["ZEI_KUBUN"].ToString() + "\r\n" + dr["ZEI_KUBUN_NM"].ToString());
            }
            #endregion

            #region 勘定科目毎の明細を取得
            Sql = new StringBuilder();
            dpc = new DbParamCollection();

            Sql.AppendLine("SELECT");
            Sql.AppendLine(" KANJO_KAMOKU_CD,");
            Sql.AppendLine(" MIN(KANJO_KAMOKU_NM) AS KANJO_KAMOKU_NM,");
            Sql.AppendLine(" ZEI_RITSU,");
            Sql.AppendLine(" SUM(ZEINUKI) AS ZEINUKI,");
            Sql.AppendLine(" SUM(SHOHIZEI) AS SHOHIZEI,");
            // 税抜・税込で集計対象を変える
            string sumString = (this.rdoZeiNuki.Checked) ? "ZEINUKI" : "ZEINUKI + SHOHIZEI";
            // Configで指定された税区分の集計SQLを作成する
            int cnt = 1;
            int i;
            string sql;
            for (i = zeiKbnFr; i <= zeiKbnTo; i++)
            {
                string fieldName = Util.ToString(kubunNm[i.ToString()]);
                // マスタに存在する税区分だけ対象とする
                if (fieldName != "")
                {
                    sql = " SUM(CASE WHEN ZEI_KUBUN = " + i.ToString() + " THEN " + sumString + " ELSE 0 END) AS gak" + cnt.ToString() + ",";
                    Sql.AppendLine(sql);
                    cnt++;
                }
            }
            sql = " SUM(CASE WHEN ZEI_KUBUN = " + zeiKbn0.ToString() + " THEN " + sumString + " ELSE 0 END) AS gak" + cnt.ToString() + ",";
            Sql.AppendLine(sql);
            Sql.AppendLine(" SUM(ZEINUKI) + SUM(SHOHIZEI) AS KEI ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" (SELECT");
            Sql.AppendLine(" M.KANJO_KAMOKU_CD,");
            Sql.AppendLine(" K.KANJO_KAMOKU_NM,");
            Sql.AppendLine(" M.ZEI_RITSU,");
            Sql.AppendLine(" CASE WHEN M.TAISHAKU_KUBUN =");
            Sql.AppendLine(" (CASE WHEN Z.TAISHAKU_KUBUN = 0 THEN K.TAISHAKU_KUBUN ELSE Z.TAISHAKU_KUBUN END) THEN M.ZEINUKI_KINGAKU ELSE M.ZEINUKI_KINGAKU * -1 END AS ZEINUKI,");
            Sql.AppendLine(" CASE WHEN M.TAISHAKU_KUBUN =");
            Sql.AppendLine(" (CASE WHEN Z.TAISHAKU_KUBUN = 0 THEN K.TAISHAKU_KUBUN ELSE Z.TAISHAKU_KUBUN END) THEN M.SHOHIZEI_KINGAKU ELSE M.SHOHIZEI_KINGAKU * -1 END AS SHOHIZEI,");
            Sql.AppendLine(" CASE M.ZEI_KUBUN WHEN 30 THEN 10 WHEN 31 THEN 11 WHEN 34 THEN 14 WHEN 35 THEN 15 WHEN 40 THEN 20 WHEN 50 THEN 10 WHEN 51 THEN 20 WHEN 52 THEN 10 WHEN 53 THEN 20  ELSE M.ZEI_KUBUN END AS ZEI_KUBUN");
            Sql.AppendLine(" FROM TB_ZM_SHIWAKE_MEISAI AS M");
            Sql.AppendLine(" LEFT JOIN TB_ZM_KANJO_KAMOKU AS K ON M.KAISHA_CD = K.KAISHA_CD AND M.KANJO_KAMOKU_CD = K.KANJO_KAMOKU_CD");
            Sql.AppendLine(" LEFT JOIN TB_ZM_F_ZEI_KUBUN AS Z ON M.ZEI_KUBUN = Z.ZEI_KUBUN WHERE M.KAISHA_CD = 1 AND M.MEISAI_KUBUN = 0 AND");
            Sql.AppendLine(" M.DENPYO_KUBUN = 1 AND K.KAIKEI_NENDO = @KAIKEI_NENDO AND M.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND M.KANJO_KAMOKU_CD");
            Sql.AppendLine(" IN (SELECT DISTINCT KANJO_KAMOKU_CD FROM TB_ZM_SHIWAKE_MEISAI");
            Sql.AppendLine(" WHERE KAISHA_CD = @KAISHA_CD AND ");
            if (this.txtMizuageShishoCd.Text != "" && this.txtMizuageShishoCd.Text != "0")
            {
                Sql.AppendLine(" SHISHO_CD = @SHISHO_CD AND ");
            }
            //Sql.AppendLine(" MEISAI_KUBUN = 0 AND DENPYO_KUBUN = 1 AND DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND ZEI_KUBUN BETWEEN 10 AND 19)"); 
            Sql.AppendLine(" MEISAI_KUBUN = 0 AND DENPYO_KUBUN = 1 AND DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND ");
            Sql.AppendLine(" ZEI_KUBUN BETWEEN @KUBUN_FR AND @KUBUN_TO) ");
            Sql.AppendLine(") AS tbl ");
            Sql.AppendLine("GROUP BY");
            Sql.AppendLine(" KANJO_KAMOKU_CD, ZEI_RITSU HAVING SUM(ZEINUKI) <> 0 ");
            Sql.AppendLine("ORDER BY");
            Sql.AppendLine(" KANJO_KAMOKU_CD, ZEI_RITSU");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo); // 会計年度
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd); // 会社コード
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpJpFr.Date.ToString("yyyy/MM/dd")); // 日付Fr
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpJpTo.Date.ToString("yyyy/MM/dd")); // 日付To
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text); // 支所コード
            dpc.SetParam("@KUBUN_FR", SqlDbType.Decimal, 4, zeiKbnFr);
            dpc.SetParam("@KUBUN_TO", SqlDbType.Decimal, 4, zeiKbnTo);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region ワークにデータをセット
            //int i = 0;
            int iSort = 0;
            foreach (DataRow dr in dtMainLoop.Rows)
            {
                #region 準備
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.AppendLine("INSERT INTO PR_ZM_TBL(");
                Sql.AppendLine(" GUID");
                Sql.AppendLine(" ,SORT");
                Sql.AppendLine(" ," + Util.ColsArray(rptCols3, ""));
                Sql.AppendLine(") ");
                Sql.AppendLine("VALUES(");
                Sql.AppendLine("  @GUID");
                Sql.AppendLine(" ,@SORT");
                Sql.AppendLine(" ," + Util.ColsArray(rptCols3, "@"));
                Sql.AppendLine(") ");
                #endregion

                #region セット
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId02);
                //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, iSort);
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, "売上科目別 消費税額集計表"); // 税区分番号
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, jpMontFr[5] + " ～ " + jpMontTo[5]); // 期間
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, zei); // 税抜きor税込み
                // 税区分名の設定
                int itemNo = 4;
                // Configで設定された範囲
                for (i = zeiKbnFr; i <= zeiKbnTo; i++)
                {
                    string fieldName = Util.ToString(kubunNm[i.ToString()]);
                    // マスタに存在する税区分だけ対象とする
                    if (fieldName !="")
                    {
                        dpc.SetParam("@ITEM" + itemNo.ToString("00"), SqlDbType.VarChar, 200, fieldName);
                        itemNo++;
                    }
                }
                // 不課税項目を最後に追加
                dpc.SetParam("@ITEM" + itemNo.ToString("00"), SqlDbType.VarChar, 200, (string)kubunNm["0"]);

                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_CD"].ToString()); // 勘定科目コード
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_NM"].ToString()); // 勘定科目名
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEI_RITSU"].ToString())); // 率
                // 税抜き金額
                if (dr["ZEINUKI"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEINUKI"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "");
                }
                // 消費税
                if (dr["SHOHIZEI"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dr["SHOHIZEI"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "");
                }
                // 計
                if (dr["KEI"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dr["KEI"].ToString())); 
                }
                else
                {
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, ""); 
                }
                // 税区分10の税抜き金額
                if (dr["gak1"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak1"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, "");
                }
                // 税区分11の税抜き金額
                if (dr["gak2"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak2"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, "");
                }
                // 税区分12の税抜き金額
                if (dr["gak3"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak3"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, "");
                }
                // 税区分13の税抜き金額
                if (dr["gak4"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak4"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, "");
                }
                // 税区分14の税抜き金額
                if (dr["gak5"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak5"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, "");
                }
                // 税区分15の税抜き金額
                if (dr["gak6"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak6"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, "");
                }
                // 税区分16の税抜き金額
                if (dr["gak7"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak7"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, "");
                }
                // 税区分19の税抜き金額
                if (dr["gak8"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak8"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, "");
                }
                // 税区分0の税抜き金額
                if (dr["gak9"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak9"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, "");
                }

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
                iSort++;
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// ワークテーブルの仕入科目別消費税額集計表データを作成します。
        /// </summary>
        private void MakeWkDataCheck03()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            // 日付を西暦にして取得
            DateTime tmpJpFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpJpTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 日付を和暦にして取得
            string[] jpMontFr = Util.ConvJpDate(tmpJpFr, this.Dba);
            string[] jpMontTo = Util.ConvJpDate(tmpJpTo, this.Dba);

            string zei = "";
            if (this.rdoZeiNuki.Checked)
            {
                //zei = "【税抜き】";
                zei = "【" + this.rdoZeiNuki.Text + "】";
            }
            else
            {
                //zei = "【税込み】";
                zei = "【" + this.rdoZeiKomi.Text + "】";
            }

            #region 税区分名を取得
            int zeiKbn0 = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Zai, this.ProductName, "Setting", "Shiire_kbn0"));
            int zeiKbnFr = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Zai, this.ProductName, "Setting", "Shiire_kbnFr"));
            int zeiKbnTo = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Zai, this.ProductName, "Setting", "Shiire_kbnTo"));
            Sql.AppendLine("SELECT");
            Sql.AppendLine(" CASE WHEN ZEI_KUBUN = 0 THEN 1 ELSE 0 END AS stkbn,");
            Sql.AppendLine(" ZEI_KUBUN,");
            Sql.AppendLine(" ZEI_KUBUN_NM ");
            Sql.AppendLine("FROM");
            Sql.AppendLine(" TB_ZM_F_ZEI_KUBUN ");
            Sql.AppendLine("WHERE");
            //Sql.AppendLine(" (ZEI_KUBUN = 0) OR (ZEI_KUBUN BETWEEN 20 AND 23) ");
            Sql.AppendLine(" (ZEI_KUBUN = @KUBUN0) OR (ZEI_KUBUN BETWEEN @KUBUN_FR AND @KUBUN_TO) ");
            Sql.AppendLine("ORDER BY");
            Sql.AppendLine(" stkbn, ZEI_KUBUN");
            dpc.SetParam("@KUBUN0", SqlDbType.Decimal, 4, zeiKbn0);
            dpc.SetParam("@KUBUN_FR", SqlDbType.Decimal, 4, zeiKbnFr);
            dpc.SetParam("@KUBUN_TO", SqlDbType.Decimal, 4, zeiKbnTo);

            DataTable dtZeiKubunNm = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 表題を設定
            Hashtable kubunNm = new Hashtable();
            foreach (DataRow dr in dtZeiKubunNm.Rows)
            {
                kubunNm.Add(dr["ZEI_KUBUN"].ToString(), dr["ZEI_KUBUN"].ToString() + "\r\n" + dr["ZEI_KUBUN_NM"].ToString());
            }
            #endregion

            #region 勘定科目毎の明細を取得
            Sql = new StringBuilder();
            dpc = new DbParamCollection();

            Sql.AppendLine("SELECT");
            Sql.AppendLine(" KANJO_KAMOKU_CD,");
            Sql.AppendLine(" MIN(KANJO_KAMOKU_NM) AS KANJO_KAMOKU_NM,");
            Sql.AppendLine(" ZEI_RITSU,");
            Sql.AppendLine(" SUM(ZEINUKI) AS ZEINUKI,");
            Sql.AppendLine(" SUM(SHOHIZEI) AS SHOHIZEI,");
            //Sql.AppendLine(" SUM(ZEINUKI) + SUM(SHOHIZEI) AS KEI,");
            string sumString = (this.rdoZeiNuki.Checked) ? "ZEINUKI" : "ZEINUKI + SHOHIZEI";
            int cnt = 1;
            int i;
            string sql;
            for (i = zeiKbnFr; i <= zeiKbnTo; i++)
            {
                sql = " SUM(CASE WHEN ZEI_KUBUN = " + i.ToString() + " THEN " + sumString  + " ELSE 0 END) AS gak" + cnt.ToString() + ",";
                Sql.AppendLine(sql);
                cnt++;
            }
            sql = " SUM(CASE WHEN ZEI_KUBUN = " + zeiKbn0.ToString() + " THEN " + sumString + " ELSE 0 END) AS gak" + cnt.ToString() + ",";
            Sql.AppendLine(sql);
            for (i = cnt + 1; i < 10; i++)
            {
                sql = " 0 AS gak" + i.ToString() + ",";
                Sql.AppendLine(sql);
            }
            Sql.AppendLine(" SUM(ZEINUKI) + SUM(SHOHIZEI) AS KEI ");

            Sql.AppendLine("FROM");
            Sql.AppendLine(" (SELECT");
            Sql.AppendLine(" M.KANJO_KAMOKU_CD,");
            Sql.AppendLine(" K.KANJO_KAMOKU_NM,");
            Sql.AppendLine(" M.ZEI_RITSU,");
            Sql.AppendLine(" CASE WHEN M.TAISHAKU_KUBUN =");
            Sql.AppendLine(" (CASE WHEN Z.TAISHAKU_KUBUN = 0 THEN K.TAISHAKU_KUBUN ELSE Z.TAISHAKU_KUBUN END) THEN M.ZEINUKI_KINGAKU ELSE M.ZEINUKI_KINGAKU * -1 END AS ZEINUKI,");
            Sql.AppendLine(" CASE WHEN M.TAISHAKU_KUBUN =");
            Sql.AppendLine(" (CASE WHEN Z.TAISHAKU_KUBUN = 0 THEN K.TAISHAKU_KUBUN ELSE Z.TAISHAKU_KUBUN END) THEN M.SHOHIZEI_KINGAKU ELSE M.SHOHIZEI_KINGAKU * -1 END AS SHOHIZEI,");
            Sql.AppendLine(" CASE M.ZEI_KUBUN WHEN 30 THEN 10 WHEN 31 THEN 11 WHEN 34 THEN 14 WHEN 35 THEN 15 WHEN 40 THEN 20 WHEN 50 THEN 10 WHEN 51 THEN 20 WHEN 52 THEN 10 WHEN 53 THEN 20  ELSE M.ZEI_KUBUN END AS ZEI_KUBUN");
            Sql.AppendLine(" FROM TB_ZM_SHIWAKE_MEISAI AS M");
            Sql.AppendLine(" LEFT JOIN TB_ZM_KANJO_KAMOKU AS K ON M.KAISHA_CD = K.KAISHA_CD AND M.KANJO_KAMOKU_CD = K.KANJO_KAMOKU_CD");
            Sql.AppendLine(" LEFT JOIN TB_ZM_F_ZEI_KUBUN AS Z ON M.ZEI_KUBUN = Z.ZEI_KUBUN WHERE M.KAISHA_CD = 1 AND M.MEISAI_KUBUN = 0 AND");
            Sql.AppendLine(" M.DENPYO_KUBUN = 1 AND K.KAIKEI_NENDO = @KAIKEI_NENDO AND M.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND M.KANJO_KAMOKU_CD");
            Sql.AppendLine(" IN (SELECT DISTINCT KANJO_KAMOKU_CD FROM TB_ZM_SHIWAKE_MEISAI");
            Sql.AppendLine(" WHERE KAISHA_CD = @KAISHA_CD AND ");
            if (this.txtMizuageShishoCd.Text != "" && this.txtMizuageShishoCd.Text != "0")
            {
                Sql.AppendLine(" SHISHO_CD = @SHISHO_CD AND ");
            }
            //Sql.AppendLine(" MEISAI_KUBUN = 0 AND DENPYO_KUBUN = 1 AND DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND ZEI_KUBUN BETWEEN 20 AND 23)");
            Sql.AppendLine(" MEISAI_KUBUN = 0 AND DENPYO_KUBUN = 1 AND DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND ");
            Sql.AppendLine(" ZEI_KUBUN BETWEEN @KUBUN_FR AND @KUBUN_TO) ");
            Sql.AppendLine(") AS tbl ");
            Sql.AppendLine("GROUP BY");
            Sql.AppendLine(" KANJO_KAMOKU_CD, ZEI_RITSU HAVING SUM(ZEINUKI) <> 0 ");
            Sql.AppendLine("ORDER BY");
            Sql.AppendLine(" KANJO_KAMOKU_CD, ZEI_RITSU");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo); // 会計年度
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd); // 会社コード
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpJpFr.Date.ToString("yyyy/MM/dd")); // 日付Fr
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpJpTo.Date.ToString("yyyy/MM/dd")); // 日付To
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text); // 支所コード
            dpc.SetParam("@KUBUN_FR", SqlDbType.Decimal, 4, zeiKbnFr);
            dpc.SetParam("@KUBUN_TO", SqlDbType.Decimal, 4, zeiKbnTo);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region ワークにデータをセット
            //int i = 0;
            int iSort = 0;
            foreach (DataRow dr in dtMainLoop.Rows)
            {
                #region 準備
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.AppendLine("INSERT INTO PR_ZM_TBL(");
                Sql.AppendLine(" GUID");
                Sql.AppendLine(" ,SORT");
                Sql.AppendLine(" ," + Util.ColsArray(rptCols3, ""));
                Sql.AppendLine(") ");
                Sql.AppendLine("VALUES(");
                Sql.AppendLine("  @GUID");
                Sql.AppendLine(" ,@SORT");
                Sql.AppendLine(" ," + Util.ColsArray(rptCols3, "@"));
                Sql.AppendLine(") ");
                #endregion

                #region セット
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId03);
                //dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, iSort);
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, "仕入科目別 消費税額集計表"); // 税区分番号
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, jpMontFr[5] + " ～ " + jpMontTo[5]); // 期間
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, zei); // 税抜きor税込み
                // 税区分名の設定
                int itemNo = 4;
                // Configで設定された範囲
                for (i=zeiKbnFr; i<=zeiKbnTo; i++)
                {
                    string fieldName = Util.ToString(kubunNm[i.ToString()]);
                    dpc.SetParam("@ITEM" + itemNo.ToString("00"), SqlDbType.VarChar, 200, fieldName);
                    itemNo++;
                }
                // 不課税項目を最後に追加
                dpc.SetParam("@ITEM" + itemNo.ToString("00"), SqlDbType.VarChar, 200, (string)kubunNm["0"]); 
                // 残りを空白で埋める
                for (i = itemNo +1; i < 13; i++)
                {
                    dpc.SetParam("@ITEM" + i.ToString("00"), SqlDbType.VarChar, 200, "");
                }
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_CD"].ToString()); // 勘定科目コード
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_NM"].ToString()); // 勘定科目名
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEI_RITSU"].ToString())); // 率
                // 税抜き金額
                if (dr["ZEINUKI"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEINUKI"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "");
                }
                // 消費税
                if (dr["SHOHIZEI"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dr["SHOHIZEI"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "");
                }
                // 計
                if (dr["KEI"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dr["KEI"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "");
                }
                // 税区分10の税抜き金額
                if (dr["gak1"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak1"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, "");
                }
                // 税区分11の税抜き金額
                if (dr["gak2"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak2"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, "");
                }
                // 税区分12の税抜き金額
                if (dr["gak3"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak3"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, "");
                }
                // 税区分13の税抜き金額
                if (dr["gak4"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak4"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, "");
                }
                // 税区分14の税抜き金額
                if (dr["gak5"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak5"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, "");
                }
                // 税区分15の税抜き金額
                if (dr["gak6"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak6"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, "");
                }
                // 税区分16の税抜き金額
                if (dr["gak7"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak7"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, "");
                }
                // 税区分19の税抜き金額
                if (dr["gak8"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak8"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, "");
                }
                // 税区分0の税抜き金額
                if (dr["gak9"].ToString() != "0")
                {
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Util.FormatNum(dr["gak9"].ToString()));
                }
                else
                {
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, "");
                }

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
                iSort++;
                #endregion
            }
            #endregion

        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }
        #endregion
    }
}
