﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Security.Cryptography;
using jp.co.fsi.common.mynumber;

namespace jp.co.fsi.zm.zmcm1011
{
    /// <summary>
    /// 会社情報の登録(ZMCM1011)
    /// </summary>
    public partial class ZMCM1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";

        /// <summary>
        /// SQL関連
        /// </summary>
        private const int KAISHA_CDDE = 1; // 会社コード
        private const int TORIHIKISAKI_KUBUN2 = 2; // 取引先区分２
        //パスワードに使用する文字
        //private static readonly string passwordChars = "0123456789abcdefghijklmnopqrstuvwxyz";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        /*
        public ZMCM1011(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        */
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            this.ShowFButton = false;

            // 編集モードの初期表示
            InitDispOnEdit();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F6キー押下時処理（登録）
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 画面上の決算期と選択決算期が一致するかチェック
            if (this.txtKessankiCd.Text != Util.ToString(this.UInfo.KessanKi))
            {
                Msg.Error("決算期が選択会計年度の決算期と異なります。");
                return;
            }

            // 確認メッセージを表示
            string msg = "保存しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmKaisha = SetCmKaishaParams();
            ArrayList alParamsZnKaisha = SetZmKaishaParams();

            try
            {
                this.Dba.BeginTransaction();

                // 削除用パラメータ
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SEALA_NO", SqlDbType.Decimal, 6, this.txtKaishaCd.Text);
                dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 2);// 法人の区分は2

                // マイナンバー削除
                this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SEALA_NO AND KUBUN = @KUBUN", dpc);
                this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SEALA_NO AND KUBUN = @KUBUN", dpc);
                // マイナンバー登録
                Mynumber.InsertNumber(Util.ToString(txtMyNumber.Text), Util.ToInt(txtKaishaCd.Text), this.Dba);
                // データ更新
                // 共通.取引先マスタ
                this.Dba.Update("TB_CM_KAISHA_JOHO",
                    (DbParamCollection)alParamsCmKaisha[1],
                    "KAISHA_CD = @KAISHA_CD",
                    (DbParamCollection)alParamsCmKaisha[0]);
                // 販売.取引先情報
                this.Dba.Update("TB_ZM_KAISHA_JOHO",
                    (DbParamCollection)alParamsZnKaisha[1],
                    "KAISHA_CD = @KAISHA_CD AND KESSANKI = @KESSANKI AND KAIKEI_NENDO = @KAIKEI_NENDO",
                    (DbParamCollection)alParamsZnKaisha[0]);

                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("保存しました。");

                // DialogResultに「OK」をセットし結果を返却
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception e)
            {
                // 失敗通知ダイアログ
                Msg.Error("保存に失敗しました。" + e.Message);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }
        #endregion

        #region イベント

        #endregion

        #region privateメソッド
        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 会社情報を読み込む（TB_CM_KAISHA_JOHO）
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");      // 会社コード
            cols.Append(" ,A.KAISHA_NM");    // 会社名
            cols.Append(" ,A.YUBIN_BANGO1"); // 郵便番号頭３桁
            cols.Append(" ,A.YUBIN_BANGO2"); // 郵便番号下４桁
            cols.Append(" ,A.JUSHO1");       //　住所１
            cols.Append(" ,A.JUSHO2");       //　住所２
            cols.Append(" ,A.DENWA_BANGO");  // 電話番号
            cols.Append(" ,A.FAX_BANGO");    // FAX番号
            cols.Append(" ,A.DAIHYOSHA_NM"); // 代表者名
            cols.Append(" ,A.PAYAO_KOZA_NO");    // パヤオ口座番号 TODO 20181206 SJ
            cols.Append(" ,A.KOZA_NM"); // パヤオ口座名 TODO 20181206 SJ


            StringBuilder from = new StringBuilder();
            from.Append("TB_CM_KAISHA_JOHO AS A");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            DataTable dtCmKaishaJoho =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD",
                    dpc);

            // 取得した内容を表示
            DataRow drDispData = dtCmKaishaJoho.Rows[0];
            this.txtKaishaCd.Text = Util.ToString(this.UInfo.KaishaCd); // 会社コード
            this.txtKaishaNm.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["KAISHA_NM"]); // 会社名
            this.txtDaihyoshaNm.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["DAIHYOSHA_NM"]); // 代表者名
            this.txtYubinBango1.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["YUBIN_BANGO1"]); // 郵便番号1
            this.txtYubinBango2.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["YUBIN_BANGO2"]); // 郵便番号2
            this.txtJusho1.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["JUSHO1"]); // 住所1
            this.txtJusho2.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["JUSHO2"]); // 住所2
            this.txtTel.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["DENWA_BANGO"]); // 電話番号
            this.txtFax.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["FAX_BANGO"]); // FAX番号
            this.txtPAYAO_NO.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["PAYAO_KOZA_NO"]); // パヤオ口座番号 TODO 20181206 SJ
            this.txtPAYAO_NM.Text = Util.ToString(dtCmKaishaJoho.Rows[0]["KOZA_NM"]); // パヤオ口座名 TODO 20181206 SJ

            // ----------------------------------------------------------------------------
            // 財務の会社情報を読み込む（TB_ZM_KAISHA_JOHO）
            cols = new StringBuilder();
            cols.Append("B.KAISHA_CD");             // 会社コード
            cols.Append(" ,B.KAIKEI_NENDO");          // 会計年度
            cols.Append(" ,B.KESSANKI");              // 決算期
            cols.Append(" ,B.KAIKEI_KIKAN_KAISHIBI"); // 会計期間開始日
            cols.Append(" ,B.KAIKEI_KIKAN_SHURYOBI"); // 会計期間終了日

            from = new StringBuilder();
            from.Append("TB_ZM_KAISHA_JOHO AS B");

            // dpc = new DbParamCollection();
            // dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 4, this.UInfo.KessanKi);

            DataTable dtZmKaishaJoho =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "B.KAISHA_CD = @KAISHA_CD AND B.KAIKEI_NENDO = @KAIKEI_NENDO AND B.KESSANKI = @KESSANKI",
                    dpc);

            // 取得した内容を表示
            this.txtKessankiCd.Text = Util.ToString(this.UInfo.KessanKi); // 決算期

            string[] jpDate = Util.ConvJpDate(Util.ToDate(dtZmKaishaJoho.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
            // 日付範囲前
            this.lblDateGengoFr.Text = jpDate[0]; // 元号
            this.txtDateYearFr.Text = jpDate[2];  // 年
            this.txtDateMonthFr.Text = jpDate[3]; // 月
            this.txtDateDayFr.Text = jpDate[4];   // 日

            // 日付範囲後
            jpDate = Util.ConvJpDate(Util.ToDate(dtZmKaishaJoho.Rows[0]["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);
            this.lblDateGengoTo.Text = jpDate[0];
            this.txtDateYearTo.Text = jpDate[2];
            this.txtDateMonthTo.Text = jpDate[3];
            this.txtDateDayTo.Text = jpDate[4];
            
            // マイナンバーを表示（法人13桁）
            this.txtMyNumber.Text = Mynumber.GetmyNumber(Util.ToInt(txtKaishaCd.Text),this.Dba);
        }

        /// <summary>
        /// 会社名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKaishaNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtKaishaNm.Text, this.txtKaishaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 代表者名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDaihyosha()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtDaihyoshaNm.Text, this.txtDaihyoshaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(上3桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango1()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtYubinBango1.Text, this.txtYubinBango1.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(下4桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango2()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtYubinBango2.Text, this.txtYubinBango2.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所１の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho1()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho1.Text, this.txtJusho1.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所２の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho2()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho2.Text, this.txtJusho2.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 電話番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTel()
        {
            // 15文字を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTel.Text, this.txtTel.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 電話番号利用可能文字チェック
            if (!this.chkTelAvailableCharacters(this.txtTel.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// FAX番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFax()
        {
            // 15文字を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtFax.Text, this.txtFax.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 電話番号利用可能文字チェック
            if (!this.chkTelAvailableCharacters(this.txtFax.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// マイナンバー（法人番号）の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMyNumber()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMyNumber.Text, this.txtMyNumber.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtMyNumber.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 電話番号利用可能文字チェック @"^[\s-9]+$"
        /// </summary>
        /// <param name="telNumber"電話番号></param>
        /// <returns>true = 書式OK,  false = 書式NG</returns>
        private bool chkTelAvailableCharacters(string telNumber)
        {
            bool result = false;
            // 半角スペースから半角"9"までの入力を認める（アスキーコード表参照）
            result = System.Text.RegularExpressions.Regex.IsMatch(
                 txtTel.Text,
                 @"^[ -9]*$");

            // 全部半角スペースならエラーにする
            if (result)
            {
                if (0 < telNumber.Replace(" ", "").Length)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 会社名のチェック
            if (!IsValidKaishaNm())
            {
                this.txtKaishaNm.Focus();
                return false;
            }

            // 代表者名のチェック
            if (!IsValidDaihyosha())
            {
                this.txtDaihyoshaNm.Focus();
                return false;
            }

            // 郵便番号１のチェック
            if (!IsValidYubinBango1())
            {
                this.txtYubinBango1.Focus();
                return false;
            }

            // 郵便番号２のチェック
            if (!IsValidYubinBango2())
            {
                this.txtYubinBango2.Focus();
                return false;
            }

            // 郵便番号２だけの入力はエラー
            if (ValChk.IsEmpty(this.txtYubinBango1.Text) &&
                !ValChk.IsEmpty(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtYubinBango1.Focus();
                return false;
            }

            // 住所１のチェック
            if (!IsValidJusho1())
            {
                this.txtJusho1.Focus();
                return false;
            }

            // 住所２のチェック
            if (!IsValidJusho2())
            {
                this.txtJusho2.Focus();
                return false;
            }

            // 電話番号のチェック
            if (!IsValidTel())
            {
                this.txtTel.Focus();
                return false;
            }

            // FAX号のチェック
            if (!IsValidTel())
            {
                this.txtFax.Focus();
                return false;
            }

            // マイナンバーのチェック
            if (!IsValidMyNumber())
            {
                this.txtMyNumber.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_CM_KAISHA_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmKaishaParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 会社コードと取引先コードをWhere句のパラメータに設定
            DbParamCollection whereParam = new DbParamCollection();
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            alParams.Add(whereParam);

            // 会社名
            updParam.SetParam("@KAISHA_NM", SqlDbType.VarChar, 40, this.txtKaishaNm.Text);
            // 代表者名
            updParam.SetParam("@DAIHYOSHA_NM", SqlDbType.VarChar, 30, this.txtDaihyoshaNm.Text);
            // 郵便番号1
            updParam.SetParam("@YUBIN_BANGO1", SqlDbType.VarChar, 3, this.txtYubinBango1.Text);
            // 郵便番号2
            updParam.SetParam("@YUBIN_BANGO2", SqlDbType.VarChar, 4, this.txtYubinBango2.Text);
            // 住所1
            updParam.SetParam("@JUSHO1", SqlDbType.VarChar, 30, this.txtJusho1.Text);
            // 住所2
            updParam.SetParam("@JUSHO2", SqlDbType.VarChar, 30, this.txtJusho2.Text);
            // 住所3
            updParam.SetParam("@JUSHO3", SqlDbType.VarChar, 30, "");
            // 電話番号
            updParam.SetParam("@DENWA_BANGO", SqlDbType.VarChar, 15, this.txtTel.Text);
            // FAX番号
            updParam.SetParam("@FAX_BANGO", SqlDbType.VarChar, 15, this.txtFax.Text);
            // 登録日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            // パヤオ口座番号
            updParam.SetParam("@PAYAO_KOZA_NO", SqlDbType.VarChar, 15, this.txtPAYAO_NO.Text); //TODO 20181206 SJ
            // パヤオ口座名
            updParam.SetParam("@KOZA_NM", SqlDbType.VarChar, 30, this.txtPAYAO_NM.Text); //TODO 20181206 SJ



            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_KAISHA_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmKaishaParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 会社コードと取引先コードをWhere句のパラメータに設定
            DbParamCollection whereParam = new DbParamCollection();
            // 会社コードと決算期と会計年度を更新パラメータに設定
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            whereParam.SetParam("@KESSANKI", SqlDbType.Decimal, 4, this.UInfo.KessanKi);
            whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            alParams.Add(whereParam);

            // 会計期間開始日
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            updParam.SetParam("@KAIKEI_KIKAN_KAISHIBI", SqlDbType.VarChar, tmpDateFr.Date.ToString("yyyy/MM/dd"));

            // 会計期間終了日
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            updParam.SetParam("@KAIKEI_KIKAN_SHURYOBI", SqlDbType.VarChar, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }
        #endregion

        /// <summary>
        /// 全角、アルファベットの入力不可にする処理
        /// </summary>
        private void txtPAYAO_NO_KeyPress(object sender, KeyPressEventArgs e)
        {
            // 数字のみの入力を許可
            // TODO 2018-12-07 バックスペースは無視
            if (e.KeyChar == '\b') return;

            if (!ValChk.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Returnキー押下時に、保存される処理
        /// </summary>
        private void txtPAYAO_NO_KeyDown(object sender, KeyEventArgs e)
        {
            //TODO 20181207 SJ
            if (e.KeyCode == Keys.Return)
            {
                this.PressF6();
            }

        }
    }
}
