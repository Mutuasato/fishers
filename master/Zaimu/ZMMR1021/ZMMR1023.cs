﻿
using jp.co.fsi.common.constants;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1021
{
    /// <summary>
    /// 総勘定元帳[印字設定](ZMMR1023)
    /// </summary>
    public partial class ZMMR1023 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMMR1023()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトル非表示
            this.lblTitle.Visible = false;

            // 画面の初期表示
            InitDisp();

            // 初期フォーカス
            this.chk01.Focus();

        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            if (Msg.ConfYesNo("保存しますか？") == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            // 保存処理
            // 内部で保持してるっぽい
            SaveSettings();

            // 画面を閉じる
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 初期情報の表示
        /// </summary>
        private void InitDisp()
        {
            this.chk01.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk01")) ? true : false;
            this.chk02.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk02")) ? true : false;
            this.chk03.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk03")) ? true : false;
            this.chk04.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk04")) ? true : false;
            this.chk05.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk05")) ? true : false;
            this.chk06.Checked = "1".Equals(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk06")) ? true : false;
        }

        /// <summary>
        /// 設定を保存する
        /// </summary>
        private void SaveSettings()
        {
            this.Config.SetPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk01",
                this.chk01.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk02",
                this.chk02.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk03",
                this.chk03.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk04",
                this.chk04.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk05",
                this.chk05.Checked ? "1" : "0");
            this.Config.SetPgConfig(Constants.SubSys.Zai, "ZMMR1021", "Setting", "Chk06",
                this.chk06.Checked ? "1" : "0");

            // 設定を保存する
            this.Config.SaveConfig();
        }
        #endregion

    }
}
