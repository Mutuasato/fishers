﻿using System;
using System.Collections;
using System.Data;
using System.Text;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1021
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1021DA
    {
        #region private変数
        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMMR1021DA(UserInfo uInfo, DbAccess dba, ConfigLoader config)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 補助科目有無データ、部門有無データ、工事有無データを取得
        /// </summary>
        /// <param name="kanjoKamoku">勘定科目の入力値</param>
        /// <returns>補助科目有無データ、部門有無データ、工事有無データ</returns>
        public DataTable GetCdUmu(int kanjoKamoku)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" HOJO_KAMOKU_UMU,");
            sql.Append(" BUMON_UMU,");
            sql.Append(" KOJI_UMU");
            sql.Append(" FROM");
            sql.Append(" VI_ZM_KANJO_KAMOKU");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU", SqlDbType.Decimal, 6, kanjoKamoku);

            DataTable dtCdUmu = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtCdUmu;
        }

        /// <summary>
        /// 補助科目コードの有無を取得
        /// </summary>
        /// <param name="kanjoKamokuCd">勘定科目コード</param>
        /// <param name="shishoCd">支所コード</param>
        /// <returns>補助科目コード数</returns>
        public int GetHojoKamokuCd(string kanjoKamokuCd, int shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" HOJO_KAMOKU_CD");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHIWAKE_MEISAI");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKamokuCd);

            DataTable dtHojoKamokuCd = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtHojoKamokuCd.Rows.Count;
        }

        /// <summary>
        /// 補助科目コードを取得
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>補助科目コード</returns>
        /// 
        public DataTable GetHojoKamokuCd(Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shishoCd = Util.ToInt(condition["ShishoCode"]);
            int kanjoKamokuCd = Util.ToInt(condition["KanjoKamokuCd"]);
            int hojoKamokuCdFr = Util.ToInt(condition["HojoKamokuFr"]);
            int hojoKamokuCdTo = Util.ToInt(condition["HojoKamokuTo"]);
            int bumonCdFr = Util.ToInt(condition["BumonFr"]);
            int bumonCdTo = Util.ToInt(condition["BumonTo"]);
            //int kojiCdFr = Util.ToInt(condition["KojiFr"]);
            //int kojiCdTo = Util.ToInt(condition["KojiTo"]);
            DateTime dtFr = (DateTime)condition["DtFr"];
            DateTime dtTo = (DateTime)condition["DtTo"];
            string inji = Util.ToString(condition["Inji"]);

            int shiwakeShori = Util.ToInt(condition["ShiwakeShurui"]);
            int shohizeiShoriHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);

            if (inji == "yes")
            {
                sql.Append(" SELECT");
                sql.Append(" SHISHO_CD,");
                sql.Append(" HOJO_KAMOKU_CD");
                sql.Append(" FROM");
                sql.Append(" TB_ZM_SHIWAKE_MEISAI");
                sql.Append(" WHERE");
                sql.Append(" KAISHA_CD = @KAISHA_CD AND");

                if (shishoCd != 0)
                    sql.Append(" SHISHO_CD = @SHISHO_CD AND");

                sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND");
                sql.Append(" HOJO_KAMOKU_CD BETWEEN @HOJO_KAMOKU_CD_FR AND @HOJO_KAMOKU_CD_TO AND");
                sql.Append(" BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO AND");
                //sql.Append(" KOJI_CD BETWEEN @KOJI_CD_FR AND @KOJI_CD_TO AND");
                //sql.Append(" DENPYO_DATE <= CAST(@KIKAN_TO AS DATETIME) AND");
                sql.Append(" DENPYO_DATE <= CAST(@KIKAN_TO AS DATETIME) ");

                //sql.Append(" MEISAI_KUBUN <= 0");
                if (shohizeiShoriHandan == 1)
                {
                    sql.Append(" AND MEISAI_KUBUN <= 0 ");
                }
                else
                {
                    sql.Append(" AND MEISAI_KUBUN >= 0 ");
                }
                if (shiwakeShori != 9)
                {
                    sql.Append(" AND KESSAN_KUBUN = @SHIWAKE_SHORI");
                }

                sql.Append(" GROUP BY SHISHO_CD,KANJO_KAMOKU_CD, HOJO_KAMOKU_CD");
                sql.Append(" ORDER BY SHISHO_CD,HOJO_KAMOKU_CD");
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                dpc.SetParam("@SHIWAKE_SHORI", SqlDbType.Decimal, 1, shiwakeShori);
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKamokuCd);
                dpc.SetParam("@HOJO_KAMOKU_CD_FR", SqlDbType.Decimal, 10, hojoKamokuCdFr);
                dpc.SetParam("@HOJO_KAMOKU_CD_TO", SqlDbType.Decimal, 10, hojoKamokuCdTo);
                dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
                dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
                //dpc.SetParam("@KOJI_CD_FR", SqlDbType.Decimal, 4, kojiCdFr);
                //dpc.SetParam("@KOJI_CD_TO", SqlDbType.Decimal, 4, kojiCdTo);
                dpc.SetParam("@KIKAN_TO", SqlDbType.VarChar, dtTo.Date.ToString("yyyy/MM/dd"));
            }
            else
            {
                sql.Append(" SELECT");
                sql.Append(" SHISHO_CD,");
                sql.Append(" HOJO_KAMOKU_CD");
                sql.Append(" FROM");
                sql.Append(" VI_ZM_SHIWAKE_MEISAI_AITE");
                sql.Append(" WHERE");
                sql.Append(" KAISHA_CD = @KAISHA_CD AND");

                if (shishoCd != 0)
                    sql.Append(" SHISHO_CD = @SHISHO_CD AND");

                sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND");
                sql.Append(" HOJO_KAMOKU_CD BETWEEN @HOJO_KAMOKU_CD_FR AND @HOJO_KAMOKU_CD_TO AND");
                sql.Append(" BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO AND");
                //sql.Append(" KOJI_CD BETWEEN @KOJI_CD_FR AND @KOJI_CD_TO AND");
                sql.Append(" DENPYO_DATE BETWEEN CAST(@KIKAN_FR AS DATETIME) AND");
                //sql.Append(" CAST(@KIKAN_TO AS DATETIME) AND");
                sql.Append(" CAST(@KIKAN_TO AS DATETIME) ");

                //sql.Append(" MEISAI_KUBUN <= 0 AND");
                //sql.Append(" DENPYO_KUBUN = 1");
                if (shohizeiShoriHandan == 1)
                {
                    sql.Append(" AND MEISAI_KUBUN <= 0 ");
                }
                else
                {
                    sql.Append(" AND MEISAI_KUBUN >= 0 ");
                }
                if (shiwakeShori != 9)
                {
                    sql.Append(" AND KESSAN_KUBUN = @SHIWAKE_SHORI");
                }

                sql.Append(" GROUP BY SHISHO_CD,HOJO_KAMOKU_CD");
                sql.Append(" ORDER BY SHISHO_CD,HOJO_KAMOKU_CD");
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                dpc.SetParam("@SHIWAKE_SHORI", SqlDbType.Decimal, 1, shiwakeShori);
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKamokuCd);
                dpc.SetParam("@HOJO_KAMOKU_CD_FR", SqlDbType.Decimal, 10, hojoKamokuCdFr);
                dpc.SetParam("@HOJO_KAMOKU_CD_TO", SqlDbType.Decimal, 10, hojoKamokuCdTo);
                dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, bumonCdFr);
                dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, bumonCdTo);
                //dpc.SetParam("@KOJI_CD_FR", SqlDbType.Decimal, 4, kojiCdFr);
                //dpc.SetParam("@KOJI_CD_TO", SqlDbType.Decimal, 4, kojiCdTo);
                dpc.SetParam("@KIKAN_FR", SqlDbType.VarChar, dtFr.Date.ToString("yyyy/MM/dd"));
                dpc.SetParam("@KIKAN_TO", SqlDbType.VarChar, dtTo.Date.ToString("yyyy/MM/dd"));
            }

            DataTable dtHojoKamokuCd = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtHojoKamokuCd;
        }

        /// <summary>
        /// 補助科目名を取得
        /// </summary>
        /// <param name="kanjoKamoku">勘定科目コード</param>
        /// <param name="hojoKamoku">補助科目コード</param>
        /// <param name="shishoCd">支所コード</param>
        /// <returns>補助科目名</returns>
        public string GetHojoKamokuNm(int kanjoKamoku, int hojoKamoku, int shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            string hojoKamokuNm = "";

            if (hojoKamoku > 0)
            {
                sql.Append(" SELECT");
                sql.Append(" HOJO_KAMOKU_NM");
                sql.Append(" FROM");
                sql.Append(" VI_ZM_HOJO_KAMOKU");
                sql.Append(" WHERE");
                sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                sql.Append(" (SHISHO_CD = @SHISHO_CD or SHISHO_CD is null) AND");
                sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
                sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
                sql.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKamoku);
                dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, hojoKamoku);

                DataTable dtHojoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
                if (dtHojoKamoku.Rows.Count != 0)
                {
                    hojoKamokuNm = Util.ToString(dtHojoKamoku.Rows[0]["HOJO_KAMOKU_NM"]);
                }
            }

            return hojoKamokuNm;
        }

        /// <summary>
        /// 貸借区分、繰越残高を取得
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <param name="hojoKamoku">補助科目コード</param>
        /// <param name="shishoCd">支所コード</param>
        /// <returns>貸借区分、繰越残高</returns>
        public DataTable GetKamokuJoho(Hashtable condition, int hojoKamokuCd, int shishoCd)
        {
            // SetSubjectScript1

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            int kanjoKamoku = Util.ToInt(condition["KanjoKamokuCd"]);

            int shiwakeShori = Util.ToInt(condition["ShiwakeShurui"]);
            int shohizeiShoriHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
            string colName = "ZEIKOMI_KINGAKU";
            if (shohizeiShoriHandan == 1)
            {
                colName = "ZEIKOMI_KINGAKU";
            }
            else
            {
                colName = "ZEINUKI_KINGAKU";
            }

            sql.Append(" SELECT");
            sql.Append(" A.HOJO_KAMOKU_CD,");
            sql.Append(" B.TAISHAKU_KUBUN,");
            //sql.Append(" SUM(");
            //sql.Append("   CASE WHEN A.DENPYO_DATE < CAST(@DATE_FR AS DATETIME) THEN");
            //sql.Append("     CASE WHEN B.TAISHAKU_KUBUN = 1 THEN");
            //sql.Append("       (CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEIKOMI_KINGAKU ELSE 0 END) -");
            //sql.Append("         (CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEIKOMI_KINGAKU ELSE 0 END)");
            //sql.Append("     ELSE");
            //sql.Append("       (CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A.ZEIKOMI_KINGAKU ELSE 0 END) -");
            //sql.Append("         (CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A.ZEIKOMI_KINGAKU ELSE 0 END)");
            //sql.Append("     END");
            //sql.Append("   ELSE 0");
            //sql.Append(" END) AS KURIKOSHI_ZANDAKA");

            sql.Append(" SUM(");
            sql.Append("   CASE WHEN A.DENPYO_DATE < CAST(@DATE_FR AS DATETIME) THEN");
            sql.Append("     CASE WHEN B.TAISHAKU_KUBUN = 1 THEN");
            sql.Append("       (CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A." + colName + " ELSE 0 END) -");
            sql.Append("         (CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A." + colName + " ELSE 0 END)");
            sql.Append("     ELSE");
            sql.Append("       (CASE WHEN A.TAISHAKU_KUBUN = 2 THEN A." + colName + " ELSE 0 END) -");
            sql.Append("         (CASE WHEN A.TAISHAKU_KUBUN = 1 THEN A." + colName + " ELSE 0 END)");
            sql.Append("     END");
            sql.Append("   ELSE 0");
            sql.Append(" END) AS KURIKOSHI_ZANDAKA");

            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO");
//            sql.Append(" LEFT OUTER JOIN TB_ZM_HOJO_KAMOKU AS C ON");
//          
//            sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
//            sql.Append(" A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD AND");

            //sql.Append(" A.HOJO_KAMOKU_CD = C.HOJO_KAMOKU_CD AND");
//            sql.Append(" (A.SHISHO_CD = C.SHISHO_CD or C.SHISHO_CD is null) AND");

//            sql.Append(" AND C.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS D ON");
            sql.Append(" A.KAISHA_CD = D.KAISHA_CD AND");
            sql.Append(" A.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.BUMON_CD BETWEEN @BUMON_FR AND @BUMON_TO AND");
            sql.Append(" A.KANJO_KAMOKU_CD = @KANJO_KAMOKU AND");
            sql.Append(" A.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND");
            //sql.Append(" A.KOJI_CD BETWEEN @KOJI_FR AND @KOJI_TO AND");
            sql.Append(" A.DENPYO_DATE <= CAST(@DATE_TO AS DATETIME) AND");

            //sql.Append(" A.MEISAI_KUBUN <= 0 AND");
            if (shohizeiShoriHandan == 1)
            {
                sql.Append(" A.MEISAI_KUBUN <= 0 AND");
            }
            else
            {
                sql.Append(" A.MEISAI_KUBUN >= 0 AND");
            }
            if (shiwakeShori != 9)
            {
                sql.Append(" A.KESSAN_KUBUN = @SHIWAKE_SHORI AND");
            }

            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" GROUP BY");
            sql.Append(" A.KANJO_KAMOKU_CD,");
            sql.Append(" B.KANJO_KAMOKU_NM,");
            sql.Append(" A.HOJO_KAMOKU_CD,");
            sql.Append(" B.TAISHAKU_KUBUN");
            sql.Append(" ORDER BY");
            sql.Append(" A.HOJO_KAMOKU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@SHIWAKE_SHORI", SqlDbType.Decimal, 1, shiwakeShori);
            DateTime dtFr = (DateTime)condition["DtFr"];
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, dtFr.Date.ToString("yyyy/MM/dd"));
            DateTime dtTo = (DateTime)condition["DtTo"];
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, dtTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KANJO_KAMOKU", SqlDbType.VarChar, 6, kanjoKamoku);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.VarChar, 10, hojoKamokuCd);
            dpc.SetParam("@BUMON_FR", SqlDbType.VarChar, 4, Util.ToString(condition["BumonFr"]));
            dpc.SetParam("@BUMON_TO", SqlDbType.VarChar, 4, Util.ToString(condition["BumonTo"]));
            //dpc.SetParam("@KOJI_FR", SqlDbType.VarChar, 4, Util.ToString(condition["KojiFr"]));
            //dpc.SetParam("@KOJI_TO", SqlDbType.VarChar, 4, Util.ToString(condition["KojiTo"]));

            DataTable dtKurikoshiZandaka = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtKurikoshiZandaka;
        }

        /// <summary>
        /// 相手科目コードを取得
        /// </summary>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="denpyoData">伝票日付</param>
        /// <param name="gyoBango">行番号</param>
        /// <param name="taishakuKubun">貸借区分</param>
        /// <param name="kaikeiNendo">会計年度</param>
        /// <returns>相手科目名コード</returns>
        /// 
        public DataTable GetAinteKamokuCd(int denpyoBango, string denpyoData, int gyoBango, int taishakuKubun, int kaikeiNendo, int shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            if (taishakuKubun == 1)
            {
                taishakuKubun = 2;
            }
            else
            {
                taishakuKubun = 1;
            }

            sql.Append(" SELECT TOP 1 ");
            sql.Append(" KANJO_KAMOKU_CD,");
            sql.Append(" HOJO_KAMOKU_CD");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHIWAKE_MEISAI");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" DENPYO_BANGO = @DENPYO_BANGO AND");
            sql.Append(" GYO_BANGO <= @GYO_BANGO AND");
            sql.Append(" TAISHAKU_KUBUN = @TAISHAKU_KUBUN AND");
            sql.Append(" MEISAI_KUBUN = 0 AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append(" DENPYO_DATE = CAST(@DENPYO_DATE AS DATETIME) ");
            sql.Append(" ORDER BY GYO_BANGO DESC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 4, denpyoBango);
            dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 4, gyoBango);
            dpc.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 4, taishakuKubun);
            dpc.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, 4, Util.ToDate(denpyoData));

            DataTable dtAiteKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtAiteKamoku;
        }

        /// <summary>
        /// 相手科目名を取得
        /// </summary>
        /// <param name="kanjoKamoku">勘定科目コード</param>
        /// <returns>相手科目名</returns>
        /// 
        public string GetKanjoKamokuNm(int kanjoKamokuCd, int flag, int kaikei_nendo, int shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" MAX(B.KANJO_KAMOKU_NM) AS KAMOKU_NM");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" LEFT OUTER JOIN");
            sql.Append(" TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON (A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append("     A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            sql.Append("     A.KAIKEI_NENDO = B.KAIKEI_NENDO) ");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = @KANJO_KAMOKU AND ");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" GROUP BY");
            sql.Append(" A.KANJO_KAMOKU_CD");
            sql.Append(" ORDER BY");
            sql.Append(" A.KANJO_KAMOKU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KANJO_KAMOKU", SqlDbType.Decimal, 6, kanjoKamokuCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikei_nendo);

            DataTable dtKanjoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            string kanjoKamoku;
            if (flag == 0)
            {
                kanjoKamoku = " " + kanjoKamokuCd + " " + Util.ToString(dtKanjoKamoku.Rows[0]["KAMOKU_NM"]);
            }
            else
            {
                kanjoKamoku = Util.ToString(dtKanjoKamoku.Rows[0]["KAMOKU_NM"]);
            }

            return kanjoKamoku;
        }

        /// <summary>
        /// 相手補助科目名を取得
        /// </summary>
        /// <param name="kanjoKamoku">勘定科目コード</param>
        /// <param name="hojoKamoku">相手補助科目コード</param>
        /// <param name="kaikeiNendo">会計年度</param>
        /// <returns>相手補助科目名</returns>
        public string GetAiteHojoKamokuNm(int kanjoKamoku, int hojoKamoku, int kaikeiNendo, int shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            string hojoKamokuNm = "";

            if (hojoKamoku > 0)
            {
                sql.Append(" SELECT");
                sql.Append(" HOJO_KAMOKU_NM");
                sql.Append(" FROM");
                sql.Append(" VI_ZM_HOJO_KAMOKU");
                sql.Append(" WHERE");
                sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                sql.Append(" (SHISHO_CD = @SHISHO_CD or SHISHO_CD is null) AND");
                sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND");
                sql.Append(" HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND ");
                sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKamoku);
                dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, hojoKamoku);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);

                DataTable dtHojoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

                hojoKamokuNm = Util.ToString(dtHojoKamoku.Rows[0]["HOJO_KAMOKU_NM"]);
            }

            return hojoKamokuNm;
        }

        ///// <summary>
        ///// 工事名を取得
        ///// </summary>
        ///// <param name="kojiCd">工事コード</param>
        ///// <returns>工事名</returns>
        //public string GetKojiNm(int kojiCd)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder sql = new StringBuilder();
        //    string kojiNm;
        //    sql.Append(" SELECT");
        //    sql.Append(" KOJI_NM");
        //    sql.Append(" FROM");
        //    sql.Append(" TB_ZM_KOJI");
        //    sql.Append(" WHERE");
        //    sql.Append(" KAISHA_CD = @KAISHA_CD AND");
        //    sql.Append(" KOJI_CD = " + kojiCd);
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);

        //    DataTable dtKoji = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

        //    kojiNm = Util.ToString(dtKoji.Rows[0]["KOJI_NM"]);

        //    return kojiNm;
        //}

        /// <summary>
        /// 勘定科目を指定した表示データを取得
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <param name="kanjoKamoku">勘定科目データ</param>
        /// <returns>勘定科目を指定した表示データ</returns>
        public DataTable GetTaishakuData(Hashtable condition, int hojoKamoku, int shishoCd)
        {
            // SetSubject2

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shiwakeShori = Util.ToInt(condition["ShiwakeShurui"]);
            int shohizeiShoriHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
            int kanjoKamoku = Util.ToInt(condition["KanjoKamokuCd"]);

            sql.Append(" SELECT ");
            sql.Append("   A2.SHISHO_CD");
            sql.Append("   , A2.DENPYO_BANGO");
            sql.Append("   , A2.GYO_BANGO");
            sql.Append("   , A2.TAISHAKU_KUBUN");
            sql.Append("   , A2.DENPYO_DATE");
            sql.Append("   , A2.KANJO_KAMOKU_CD");
            sql.Append("   , A2.KANJO_KAMOKU_NM");
            sql.Append("   , A2.HOJO_KAMOKU_CD");
            sql.Append("   , A2.HOJO_KAMOKU_NM");
            sql.Append("   , ISNULL(G.KANJO_KAMOKU_CD, 0) AS AITE_KANJO_KAMOKU_CD");
            sql.Append("   , ISNULL(G.HOJO_KAMOKU_CD, 0) AS AITE_HOJO_KAMOKU_CD");
            sql.Append("   , A2.HOJO_SHIYO_KUBUN");
            //sql.Append("   , G.KOJI_CD AS AITE_KOJI_CD");
            sql.Append("   , A2.BUMON_CD");
            sql.Append("   , A2.BUMON_NM");
            //sql.Append("   , A2.KOJI_CD");
            //sql.Append("   , A2.KOJI_NM");
            sql.Append("   , A2.TEKIYO");
            sql.Append("   , A2.SHOHYO_BANGO");
            sql.Append("   , A2.ZEIKOMI_KINGAKU");
            sql.Append("   , A2.ZEINUKI_KINGAKU");
            sql.Append("   , A2.SHOHIZEI_KINGAKU");
            sql.Append("   , A2.ZEI_RITSU");
            sql.Append("   , A2.KAIKEI_NENDO");
            //sql.Append("   , ISNULL(H.KOJI_NM, '') AS AITE_KOJI_NM");
            sql.Append("   , ( ");
            sql.Append("     SELECT");
            sql.Append("       ISNULL(HOJO_KAMOKU_NM, '') ");                 // 元ネタ：GetHojoKamokuNm() ※JOINすると遅くなる
            sql.Append("     FROM");
            sql.Append("       VI_ZM_HOJO_KAMOKU ");
            sql.Append("     WHERE");
            sql.Append("       KANJO_KAMOKU_CD = G.KANJO_KAMOKU_CD");         // "G"と結合するのは間違いではありません
            sql.Append("       AND HOJO_KAMOKU_CD = G.HOJO_KAMOKU_CD");
            sql.Append("       AND KAIKEI_NENDO = G.KAIKEI_NENDO");
            sql.Append("       AND (SHISHO_CD = G.SHISHO_CD or SHISHO_CD is null) ");
            sql.Append("       AND KAISHA_CD = G.KAISHA_CD ");
            sql.Append("   ) AS AITE_HOJO_KAMOKU_NM");
            sql.Append("   , ISNULL(I.KANJO_KAMOKU_NM, '') AS AITE_KANJO_KAMOKU_NM ");
            sql.Append(" FROM ");
            sql.Append("   ( ");
            sql.Append("     SELECT ");                                       // 元ネタ：VI_ZM_SHIWAKE_MEISAI_AITE
            sql.Append("       A.KAISHA_CD");
            sql.Append("       , A.KAIKEI_NENDO");
            sql.Append("       , A.SHISHO_CD");
            sql.Append("       , A.DENPYO_BANGO");
            sql.Append("       , A.GYO_BANGO");
            sql.Append("       , A.TAISHAKU_KUBUN");
            sql.Append("       , A.DENPYO_DATE");
            sql.Append("       , A.KANJO_KAMOKU_CD");
            sql.Append("       , C.KANJO_KAMOKU_NM");
            sql.Append("       , A.HOJO_KAMOKU_CD");
            sql.Append("       , D.HOJO_KAMOKU_NM");
            sql.Append("       , C.HOJO_SHIYO_KUBUN");
            sql.Append("       , A.BUMON_CD");
            sql.Append("       , E.BUMON_NM");
            //sql.Append("       , A.KOJI_CD");
            //sql.Append("       , F.KOJI_NM");
            sql.Append("       , A.TEKIYO");
            sql.Append("       , B.SHOHYO_BANGO");
            sql.Append("       , A.ZEIKOMI_KINGAKU");
            sql.Append("       , A.ZEINUKI_KINGAKU");
            sql.Append("       , A.SHOHIZEI_KINGAKU");
            sql.Append("       , A.ZEI_RITSU");
            sql.Append("       , ( ");
            sql.Append("         CASE ");
            sql.Append("           WHEN A.TAISHAKU_KUBUN = 1 ");
            sql.Append("           THEN 2 ");
            sql.Append("           ELSE 1 ");
            sql.Append("           END ");
            sql.Append("       ) AS REVERSE_TAISHAKU_KUBUN ");
            sql.Append("       , 0 AS MEISAI_KUBUN ");
            sql.Append("     FROM ");
            sql.Append("       TB_ZM_SHIWAKE_MEISAI AS A ");
            sql.Append("       LEFT OUTER JOIN TB_ZM_SHIWAKE_DENPYO AS B ");
            sql.Append("         ON ( ");
            sql.Append("           A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("           AND A.SHISHO_CD = B.SHISHO_CD ");
            sql.Append("           AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            sql.Append("           AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append("         ) ");
            sql.Append("       LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C ");
            sql.Append("         ON ( ");
            sql.Append("           A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("           AND A.KAIKEI_NENDO = C.KAIKEI_NENDO ");
            sql.Append("           AND A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            sql.Append("         ) ");
            sql.Append("       LEFT OUTER JOIN TB_ZM_HOJO_KAMOKU AS D ");
            sql.Append("         ON ( ");
            sql.Append("           A.KAISHA_CD = D.KAISHA_CD ");
            sql.Append("           AND A.SHISHO_CD = D.SHISHO_CD ");
            sql.Append("           AND A.KAIKEI_NENDO = D.KAIKEI_NENDO ");
            sql.Append("           AND A.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD ");
            sql.Append("           AND A.HOJO_KAMOKU_CD = D.HOJO_KAMOKU_CD ");
            sql.Append("         ) ");
            sql.Append("       LEFT OUTER JOIN TB_CM_BUMON AS E ");
            sql.Append("         ON ( ");
            sql.Append("           A.KAISHA_CD = E.KAISHA_CD ");
            sql.Append("           AND A.BUMON_CD = E.BUMON_CD ");
            sql.Append("         ) ");
            //sql.Append("       LEFT OUTER JOIN TB_ZM_KOJI AS F ");
            //sql.Append("         ON ( ");
            //sql.Append("           A.KAISHA_CD = F.KAISHA_CD ");
            //sql.Append("           AND A.KAIKEI_NENDO = F.KAIKEI_NENDO ");
            //sql.Append("           AND A.KOJI_CD = F.KOJI_CD ");
            //sql.Append("         ) ");
            sql.Append("     WHERE ");
            sql.Append("       A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("       AND A.SHISHO_CD = @SHISHO_CD ");
            //if (shohizeiShoriHandan == 1)
            //{
            sql.Append("       AND A.DENPYO_KUBUN = 1 ");
            //}
            //else
            //{
            //    sql.Append("       AND A.DENPYO_KUBUN >= 0 "); // INDEXを効かせたい
            //}
            sql.Append("       AND A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD ");
            sql.Append("       AND A.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD ");
            if (shohizeiShoriHandan == 1)
            {
                sql.Append("       AND A.MEISAI_KUBUN <= 0 ");
            }
            else
            {
                sql.Append("       AND A.MEISAI_KUBUN >= 0 "); // INDEXを効かせたい
            }
            sql.Append("       AND A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO ");
            sql.Append("       AND A.BUMON_CD BETWEEN @BUMON_FR AND @BUMON_TO ");
            //sql.Append("       AND A.KOJI_CD BETWEEN @KOJI_FR AND @KOJI_TO ");
            if (shiwakeShori != 9)
            {
                sql.Append("       AND A.KESSAN_KUBUN = " + shiwakeShori);
            }
            sql.Append("   ) AS A2 ");
            sql.Append("   LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS G ");      // 元ネタ：GetAinteKamokuCd()
            sql.Append("     ON ( ");
            sql.Append("       A2.KAISHA_CD = G.KAISHA_CD ");
            sql.Append("       AND A2.SHISHO_CD = G.SHISHO_CD ");
            sql.Append("       AND A2.DENPYO_BANGO = G.DENPYO_BANGO ");
            sql.Append("       AND A2.GYO_BANGO = G.GYO_BANGO ");
            sql.Append("       AND A2.REVERSE_TAISHAKU_KUBUN = G.TAISHAKU_KUBUN ");
            sql.Append("       AND A2.MEISAI_KUBUN = G.MEISAI_KUBUN ");
            sql.Append("       AND A2.KAIKEI_NENDO = G.KAIKEI_NENDO ");
            sql.Append("       AND A2.DENPYO_DATE = G.DENPYO_DATE ");
            sql.Append("     ) ");
            //sql.Append("   LEFT OUTER JOIN TB_ZM_KOJI AS H ");                // 元ネタ：GetKojiNm()
            //sql.Append("     ON ( ");
            //sql.Append("       G.KAISHA_CD = H.KAISHA_CD ");
            //sql.Append("       AND G.KOJI_CD = H.KOJI_CD ");
            //sql.Append("       AND G.KAIKEI_NENDO = H.KAIKEI_NENDO ");
            //sql.Append("     ) ");
            sql.Append("   LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS I ");        // 元ネタ：GetKanjoKamokuNm()
            sql.Append("     ON ( ");
            sql.Append("       G.KAISHA_CD = I.KAISHA_CD ");
            sql.Append("       AND G.KANJO_KAMOKU_CD = I.KANJO_KAMOKU_CD ");
            sql.Append("       AND G.KAIKEI_NENDO = I.KAIKEI_NENDO ");
            sql.Append("     ) ");
            sql.Append(" ORDER BY A2.SHISHO_CD, A2.HOJO_KAMOKU_CD, A2.DENPYO_DATE, A2.DENPYO_BANGO, A2.GYO_BANGO ");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DateTime dtFr = (DateTime)condition["DtFr"];
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 23, dtFr.Date.ToString("yyyy/MM/dd 00:00:00.000"));
            DateTime dtTo = (DateTime)condition["DtTo"];
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 23, dtTo.Date.ToString("yyyy/MM/dd 23:59:59.000")); // ミリ秒を999にすると、翌日のデータも取得する
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.VarChar, 6, kanjoKamoku);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.VarChar, 10, hojoKamoku);
            dpc.SetParam("@BUMON_FR", SqlDbType.VarChar, 4, Util.ToString(condition["BumonFr"]));
            dpc.SetParam("@BUMON_TO", SqlDbType.VarChar, 4, Util.ToString(condition["BumonTo"]));
            //dpc.SetParam("@KOJI_FR", SqlDbType.VarChar, 4, Util.ToString(condition["KojiFr"]));
            //dpc.SetParam("@KOJI_TO", SqlDbType.VarChar, 4, Util.ToString(condition["KojiTo"]));

            DataTable dsTaishakuData = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dsTaishakuData;
        }
        #endregion
    }
}
