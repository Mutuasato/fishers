﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

using systembase.table;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.zm.zmde1011
{
    /// <summary>
    /// 出納帳入力(ZMDE1011)
    /// </summary>
    public partial class ZMDE1011 : BasePgForm
    {
        #region 定数

        private const decimal DENPYO_KUBUN = 1;         // 伝票区分：本処理定義値
        private enum JurnalType                         // 伝票種類
        {
            Single, Compound
        }
        private enum EditMode                           // 明細レコード編集状態
        {
            None, Add, Edit, Delete
        }

        #endregion

        #region 変数

        private UTable.CField _activeCField;        // UTableアクティブフィールド参照用
        private UTable.CRecord _activeCRecord;      // UTableアクティブレコード参照用       
        private DateTime _denpyoDateFr;             // 伝票日付(自)
        private DateTime _denpyoDateTo;             // 伝票日付(至)

        private DataTable _dtKanjoKamoku;           // V勘定科目データ
        private DataTable _dtZeiKubun;              // T税区分データ
        private DataTable _dtBumon;                 // V部門データ
        private DataRow _drSuitoKanjo;              // 出納対象勘定レコード

        private int ShishoCode;                     // 支所コード
        private int _autoDataSetting = 0;           // 自動仕訳データの操作許可設定
        private string DENPYO_DATE = string.Empty;  // 
        private Dictionary<string, DataTable> _fd;  //
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDE1011()
        {

            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            //this.Par1 = "11131";
            //this.Par1 = "11132";

            // 処理情報初期値
            _denpyoDateFr = DateTime.Today;
            _denpyoDateTo = DateTime.Today;

            // 共通データを読込
            InitFormDataLoad();

            // 明細部を初期化
            InitDetailArea();

            // 自動仕訳作成の伝票操作許可判定用（設定から取得）
            this._autoDataSetting = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDE1011", "Setting", "autoDataSetting")));
            // 複合仕訳データ保持用コレクションの初期化
            this._fd = new Dictionary<string, DataTable>();

            // フォーカス移動
            this.mtbList.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            SetFunctionKey();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {

            // もともとは範囲指定画面を開く設定になっているが
            // 共通的な画面遷移処理に反するため
            // 範囲指定
            //            SpecifyRange(false);

            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            if (!this.btnF1.Enabled)
                return;

            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            //アクティブコントロールごとの処理
            //switch (this.ActiveControl.Name)
            switch (GetActiveObjectName())
            {
                case "DENPYO_DATE_E":
                    #region 元号検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                        t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                        if (t != null)
                        {
                            //Object obj = System.Activator.CreateInstance(t);
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                if (GetActiveObjectName() == "DENPYO_DATE_E")
                                {
                                    // タブの一部として埋め込む
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.InData = Util.ToString(_activeCField.Value);
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        string[] result = (string[])frm.OutData;
                                        _activeCField.Value = result[1];

                                        // 存在しない日付の場合、補正して存在する日付に戻す
                                        SetJp(_activeCRecord,
                                            ZaUtil.FixJpDate(
                                            Util.ToString(_activeCRecord.Fields["DENPYO_DATE_G"].Value),
                                            Util.ToString(_activeCRecord.Fields["DENPYO_DATE_E"].Value),
                                            Util.ToString(_activeCRecord.Fields["DENPYO_DATE_M"].Value),
                                            Util.ToString(_activeCRecord.Fields["DENPYO_DATE_D"].Value),
                                            this.Dba));
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "TEKIYO_CD":
                case "TEKIYO":
                    #region 摘要検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("ZAMC9031.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1051.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.zam.zamc9031.ZAMC9031");
                        t = asm.GetType("jp.co.fsi.zm.zmcm1051.ZMCM1051");
                        if (t != null)
                        {
                            //Object obj = System.Activator.CreateInstance(t);
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCRecord.Fields["TEKIYO_CD"].Value = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "KANJO_KAMOKU_CD":
                    #region 勘定科目検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9013");
                        t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                        if (t != null)
                        {
                            //Object obj = System.Activator.CreateInstance(t);
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "2";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCField.Value = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "HOJO_KAMOKU_CD":
                    #region 補助科目検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("ZAMC9021.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1041.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.zam.zamc9021.ZAMC9024");
                        t = asm.GetType("jp.co.fsi.zm.zmcm1041.ZMCM1044");
                        if (t != null)
                        {
                            //Object obj = System.Activator.CreateInstance(t);
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = Util.ToString(_activeCRecord.Fields["KANJO_KAMOKU_CD"].Value);
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCRecord.Fields["HOJO_KAMOKU_CD"].Value = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "BUMON_CD":
                    #region 部門検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");  // -> ZAMC9011
                        //asm = Assembly.LoadFrom("CMCM2041.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                        //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                        t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                        if (t != null)
                        {
                            //Object obj = System.Activator.CreateInstance(t);
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                //frm.Par1 = "1";
                                frm.Par1 = "TB_CM_BUMON";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCField.Value = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "DrDENPYO_KINGAKU":
                    #region 借方税入力
                    if (_activeCRecord != null)
                    {
                        // 自動仕訳作成の場合は変更不可
                        if (_activeCRecord != null &&
                            Util.ToInt(Util.ToString(_activeCRecord.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
                        {
                            break;
                        }
                        // 課税区分１の時に編集可能
                        if (Util.ToString(_activeCRecord.Fields["KAZEI_KUBUN"].Value) == "1")
                        {
                            _activeCRecord.Fields["SHOHIZEI_HENKO"].Value = 1;

                            CFieldEnabled(_activeCRecord.Fields["DrSHOHIZEI_KINGAKU"], true);
                            _activeCRecord.Fields["DrSHOHIZEI_KINGAKU"].Focus();
                        }
                    }
                    #endregion  
                    break;
                case "CrDENPYO_KINGAKU":
                    #region 貸方税入力
                    if (_activeCRecord != null)
                    {
                        // 自動仕訳作成の場合は変更不可
                        if (_activeCRecord != null &&
                            Util.ToInt(Util.ToString(_activeCRecord.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
                        {
                            break;
                        }
                        // 課税区分１の時に編集可能
                        if (Util.ToString(_activeCRecord.Fields["KAZEI_KUBUN"].Value) == "1")
                        {
                            _activeCRecord.Fields["SHOHIZEI_HENKO"].Value = 1;

                            CFieldEnabled(_activeCRecord.Fields["CrSHOHIZEI_KINGAKU"], true);
                            _activeCRecord.Fields["CrSHOHIZEI_KINGAKU"].Focus();
                        }
                    }
                    #endregion
                    break;
                case "KOJI_CD":
                    // NONE
                    break;
                case "KOSHU_CD":
                    // NONE;
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
                return;

            // 自動仕訳作成の場合は操作不可
            if (_activeCRecord != null &&
                Util.ToInt(Util.ToString(_activeCRecord.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
            {
                return;
            }

            if (_activeCRecord != null)
            {
                // 伝票日付
                DateTime denpyoDate = Util.ConvAdDate(
                    Util.ToString(_activeCRecord.Fields["DENPYO_DATE_G"].Value),
                    Util.ToString(_activeCRecord.Fields["DENPYO_DATE_E"].Value),
                    Util.ToString(_activeCRecord.Fields["DENPYO_DATE_M"].Value),
                    Util.ToString(_activeCRecord.Fields["DENPYO_DATE_D"].Value), this.Dba);

                // 複合仕訳入力画面

                Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMDE1031.exe");
                // フォーム作成
                Type t = asm.GetType("jp.co.fsi.zm.zmde1031.ZMDE1031");
                if (t != null)
                {
                    Object obj = Activator.CreateInstance(t);
                    if (obj != null)
                    {
                        // タブの一部として埋め込む
                        BasePgForm frm = (BasePgForm)obj;

                        if (Util.ToInt(Util.ToString(_activeCRecord.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
                        {
                            return;
                        }
                        else
                        {
                            int i = this.mtbList.Content.Records.IndexOf(_activeCRecord);
                            object[] inData = new object[12];
                            inData[0] = Util.ToString(_activeCRecord.Fields["DENPYO_BANGO"].Value);
                            inData[1] = denpyoDate.ToString("yyyy/MM/dd");
                            inData[2] = Util.ToString(_activeCRecord.Fields["SHOHYO_BANGO"].Value);
                            inData[3] = this.txtTantoshaCd.Text;
                            inData[4] = Util.ToString(_activeCRecord.Fields["KESSAN_KUBUN"].Value);
                            //
                            inData[5] = this.txtKanjoKamokuCd.Text;
                            inData[6] = this.txtHojoKamokuCd.Text;
                            inData[7] = this.txtBumonCd.Text;
                            inData[8] = this._denpyoDateFr.ToString("yyyy/MM/dd");
                            inData[9] = this._denpyoDateTo.ToString("yyyy/MM/dd");

                            if ((JurnalType)_activeCRecord.Fields["JURNAL_TYPE"].Value == JurnalType.Compound)
                            {
                                // 複合の場合は保持内容から
                                // 表示対象は消費税レコードを除外
                                DataView dv = new DataView(this._fd[i.ToString()].Copy());
                                dv.RowFilter = "MEISAI_KUBUN = 0";
                                dv.Sort = "GYO_BANGO ASC, TAISHAKU_KUBUN ASC, MEISAI_KUBUN ASC";
                                inData[10] = dv.ToTable();
                            }
                            else
                            {
                                // その他は単一仕訳を作成して渡す
                                inData[10] = GetDtSuito();
                            }

                            // タブの一部として埋め込む
                            frm.InData = inData;
                            frm.ShowDialog(this);

                            // 変更で戻って来た場合は受け取り内容で表示の書き換えを行い受け取り内容で差し替える
                            if (frm.DialogResult == DialogResult.OK)
                            {
                                // 戻りは振替入力で作成された仕訳データ
                                DataTable result = (DataTable)frm.OutData;

                                // 保存済みの物は差し替え
                                if (this._fd.ContainsKey(i.ToString()))
                                {
                                    if (IsCompound(result.Copy()))
                                    {
                                        // 諸口になる場合はコレクションへ差し替え
                                        this._fd[i.ToString()] = result.Copy();
                                    }
                                    else
                                    {
                                        // 単一仕訳で戻って来た場合はコレクションから削除
                                        this._fd.Remove(i.ToString());
                                    }
                                }
                                else
                                {
                                    // 諸口になる場合はコレクションへ追加
                                    if (IsCompound(result.Copy()))
                                    {
                                        this._fd.Add(i.ToString(), result.Copy());
                                    }
                                }

                                // 表示対象は消費税レコードを除外
                                DataView dv = new DataView(result.Copy());
                                dv.RowFilter = "MEISAI_KUBUN = 0";

                                // １行分の表示
                                DataLoadFukugo((i + 1), dv.ToTable());
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (!this.btnF4.Enabled)
                return;

            // 自動仕訳作成の場合は変更不可
            if (_activeCRecord != null &&
                Util.ToInt(Util.ToString(_activeCRecord.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
            {
                return;
            }

            if (_activeCRecord != null
            && !ValChk.IsEmpty(_activeCRecord.Fields["KANJO_KAMOKU_CD"].Value))
            {
                // 伝票日付
                DateTime denpyoDate = Util.ConvAdDate(
                    Util.ToString(_activeCRecord.Fields["DENPYO_DATE_G"].Value),
                    Util.ToString(_activeCRecord.Fields["DENPYO_DATE_E"].Value),
                    Util.ToString(_activeCRecord.Fields["DENPYO_DATE_M"].Value),
                    Util.ToString(_activeCRecord.Fields["DENPYO_DATE_D"].Value), this.Dba);

                // 税区分を選択(非選択も可)->メソッド側で検索及び設定へ
                //string zeiKubun = SelectZeiKubun(denpyoDate, false);
                //if (!ValChk.IsEmpty(zeiKubun))
                //{
                //    _activeCRecord.Fields["ZEI_KUBUN"].Value = zeiKubun;

                //    // 借方伝票金額が入力されている時
                //    if (Util.ToDecimal(Util.ToString(_activeCRecord.Fields["DrDENPYO_KINGAKU"].Value)) > 0)
                //    {
                //        SetZeiKubunInfo(_activeCRecord, 1);
                //    }
                //    // 貸方伝票金額が入力されている時
                //    if (Util.ToDecimal(Util.ToString(_activeCRecord.Fields["CrDENPYO_KINGAKU"].Value)) > 0)
                //    {
                //        SetZeiKubunInfo(_activeCRecord, 2);
                //    }
                //}
                SelectZeiKubun(denpyoDate, false, _activeCField);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (!this.btnF5.Enabled)
                return;

            // 自動仕訳作成の場合は変更不可
            if (_activeCRecord != null &&
                Util.ToInt(Util.ToString(_activeCRecord.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
            {
                return;
            }

            if (_activeCRecord != null)
            {
                if (!ValChk.IsEmpty(_activeCRecord.Fields["KANJO_KAMOKU_CD"].Value))
                {
                    // アセンブリのロード
                    //Assembly asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    //Type t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    Type t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        //Object obj = System.Activator.CreateInstance(t);
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_JIGYO_KUBUN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                _activeCRecord.Fields["JIGYO_KUBUN"].Value = result[0];
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled)
                return;

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 確認メッセージを表示
            if (Msg.ConfYesNo("更新しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            try
            {
                DateTime upd = System.DateTime.Now;         // 更新日時

                // 伝票更新ロック取得
                ZaUtil.UpdateLockOn(this.UInfo, this.Dba);

                // トランザクション開始
                this.Dba.BeginTransaction();

                // 編集結果を保存
                for (int i = 0; i < mtbList.Content.Records.Count; i++)
                {
                    UTable.CRecord rec = mtbList.Content.Records[i];

                    // 自動仕訳作成の場合は対象外
                    if (Util.ToInt(Util.ToString(rec.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
                    {
                        // DEBUG
                        System.Diagnostics.Debug.WriteLine("AUTO NONE " + Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                        // DEBUG
                    }
                    else
                    {
                        // 伝票抽出用WHERE句
                        string where = "KAISHA_CD = @KAISHA_CD";
                        where += " AND SHISHO_CD = @SHISHO_CD";
                        where += " AND DENPYO_BANGO = @DENPYO_BANGO";
                        where += " AND KAIKEI_NENDO = @KAIKEI_NENDO";

                        #region 削除行の処理
                        if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Delete)
                        {
                            // DEBUG
                            System.Diagnostics.Debug.WriteLine("DELETE " + Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                            // DEBUG

                            if (!ValChk.IsEmpty(rec.Fields["DENPYO_BANGO"].Value))
                            {
                                // 仕訳伝票データ・仕訳明細データ削除
                                DbParamCollection dpc = new DbParamCollection();
                                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 2, this.ShishoCode);
                                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6,
                                                        Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                                this.Dba.Delete("TB_ZM_SHIWAKE_DENPYO", where, dpc);
                                this.Dba.Delete("TB_ZM_SHIWAKE_MEISAI", where, dpc);
                            }
                        }
                        #endregion

                        #region 単一仕訳行の処理
                        if ((JurnalType)rec.Fields["JURNAL_TYPE"].Value == JurnalType.Single)
                        {
                            decimal denpyoBango = 0;

                            // 仕訳伝票データ登録
                            if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Add)
                            {
                                // 伝票番号採番
                                denpyoBango = ZaUtil.GetNewDenpyoBango(this.UInfo, this.Dba);

                                // DEBUG
                                System.Diagnostics.Debug.WriteLine("INSERT-SINGLE " + Util.ToString(denpyoBango));
                                // DEBUG

                                // データ追加
                                ArrayList alParamsZmDenpyo =
                                    SetZmShiwakeDenpyoParams(
                                    (EditMode)rec.Fields["EDIT_MODE"].Value,
                                    denpyoBango,
                                    Util.ConvAdDate(
                                        Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), Dba),
                                    Util.ToString(rec.Fields["SHOHYO_BANGO"].Value),
                                    upd);
                                this.Dba.Insert("TB_ZM_SHIWAKE_DENPYO", (DbParamCollection)alParamsZmDenpyo[0]);
                            }
                            else if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Edit)
                            {
                                // DEBUG
                                System.Diagnostics.Debug.WriteLine("UPDATE-SINGLE " + Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                                // DEBUG

                                // 伝票番号
                                denpyoBango = ZaUtil.ToDecimal(rec.Fields["DENPYO_BANGO"].Value);

                                // データ更新
                                ArrayList alParamsZmDenpyo =
                                    SetZmShiwakeDenpyoParams(
                                    (EditMode)rec.Fields["EDIT_MODE"].Value,
                                    denpyoBango,
                                    Util.ConvAdDate(
                                        Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), Dba),
                                    Util.ToString(rec.Fields["SHOHYO_BANGO"].Value),
                                    upd);
                                this.Dba.Update("TB_ZM_SHIWAKE_DENPYO", (DbParamCollection)alParamsZmDenpyo[1],
                                                    where, (DbParamCollection)alParamsZmDenpyo[0]);
                            }


                            // 仕訳明細データ登録
                            if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Add
                                || (EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Edit)
                            {
                                // 仕訳明細データ登録(常に保存済レコード削除＆編集レコード挿入)
                                // (明細データ削除)
                                DbParamCollection dpc = new DbParamCollection();
                                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 2, this.ShishoCode);
                                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);

                                this.Dba.Delete("TB_ZM_SHIWAKE_MEISAI", where, dpc);


                                // (明細データ追加)
                                // 出納勘定レコード
                                {
                                    ArrayList alParamsZmMeisai = SetZmShiwakeMeisaiParams(
                                        true, denpyoBango, 0, upd, DENPYO_KUBUN, rec);
                                    this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParamsZmMeisai[0]);
                                }

                                // 出納勘定消費税レコード
                                decimal shohizeiKingaku;
                                //if (ZaUtil.ToDecimal(rec.Fields["DrDENPYO_KINGAKU"].Value) > 0)
                                if (ZaUtil.ToDecimal(rec.Fields["DrDENPYO_KINGAKU"].Value) != 0)
                                {
                                    // 保持された税率で計算
                                    //shohizeiKingaku = ZaUtil.CalcConsumptionTax(
                                    //    Util.ToInt(_drSuitoKanjo["KARIKATA_ZEI_KUBUN"]),
                                    //    ZaUtil.ToDecimal(rec.Fields["DrDENPYO_KINGAKU"].Value), this.UInfo, this.Dba);
                                    shohizeiKingaku = ZaUtil.CalcConsumptionTax(
                                        Util.ToDecimal(rec.Fields["DrZEI_RITSU"].Value),
                                        ZaUtil.ToDecimal(rec.Fields["DrDENPYO_KINGAKU"].Value), this.UInfo, this.Dba);
                                }
                                else
                                {
                                    // 保持された税率で計算
                                    //shohizeiKingaku = ZaUtil.CalcConsumptionTax(
                                    //    Util.ToInt(_drSuitoKanjo["KASHIKATA_ZEI_KUBUN"]),
                                    //    ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value), this.UInfo, this.Dba);
                                    shohizeiKingaku = ZaUtil.CalcConsumptionTax(
                                        Util.ToDecimal(rec.Fields["CrZEI_RITSU"].Value),
                                        ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value), this.UInfo, this.Dba);
                                }
                                if (shohizeiKingaku != 0)
                                {
                                    ArrayList alParamsZmShohizei = SetZmShiwakeMeisaiParams(
                                        true, denpyoBango, 1, upd, DENPYO_KUBUN, rec);
                                    this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParamsZmShohizei[0]);
                                }

                                // 相手勘定レコード
                                {
                                    ArrayList alParamsZmMeisai = SetZmShiwakeMeisaiParams(
                                    false, denpyoBango, 0, upd, DENPYO_KUBUN, rec);
                                    this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParamsZmMeisai[0]);
                                }

                                // 相手勘定消費税レコード
                                if (ZaUtil.ToDecimal(rec.Fields["DrSHOHIZEI_KINGAKU"].Value) != 0
                                    || ZaUtil.ToDecimal(rec.Fields["CrSHOHIZEI_KINGAKU"].Value) != 0)
                                {
                                    ArrayList alParamsZmShohizei = SetZmShiwakeMeisaiParams(
                                    false, denpyoBango, 1, upd, DENPYO_KUBUN, rec);
                                    this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParamsZmShohizei[0]);
                                }
                            }
                        }
                        #endregion

                        #region 複式仕訳行の処理

                        #region 複合仕訳での内訳変更無し（既存処理）
                        if ((JurnalType)rec.Fields["JURNAL_TYPE"].Value == JurnalType.Compound &&
                            Util.ToInt(Util.ToString(rec.Fields["FUKUGO_SHIWAKE_KUBUN"].Value)) == 0)
                        {
                            if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Edit)
                            {
                                // DEBUG
                                System.Diagnostics.Debug.WriteLine("UPDATE-COMPOUND " + Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                                // DEBUG

                                DbParamCollection setDpc;
                                DbParamCollection whereDpc;

                                // 仕訳伝票データ
                                whereDpc = new DbParamCollection();
                                whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                                whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 2, this.ShishoCode);
                                whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                                whereDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6,
                                                        Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                                setDpc = new DbParamCollection();
                                setDpc.SetParam("@DENPYO_DATE", SqlDbType.Decimal,
                                    Util.ConvAdDate(
                                        Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), Dba));
                                setDpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, this.txtTantoshaCd.Text);
                                setDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Now);
                                this.Dba.Update("TB_ZM_SHIWAKE_DENPYO", setDpc, where, whereDpc);

                                // 仕訳明細データ
                                whereDpc = new DbParamCollection();
                                whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                                whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 2, this.ShishoCode);
                                whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                                whereDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6,
                                                        Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                                setDpc = new DbParamCollection();
                                setDpc.SetParam("@DENPYO_DATE", SqlDbType.Decimal,
                                    Util.ConvAdDate(
                                        Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), Dba));
                                setDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Now);
                                this.Dba.Update("TB_ZM_SHIWAKE_MEISAI", setDpc, where, whereDpc);
                            }
                        }
                        #endregion

                        #region 複合仕訳での内訳変更あり（複合仕訳入力）
                        if ((JurnalType)rec.Fields["JURNAL_TYPE"].Value == JurnalType.Compound &&
                            Util.ToInt(Util.ToString(rec.Fields["FUKUGO_SHIWAKE_KUBUN"].Value)) == 1)
                        {
                            // DEBUG
                            System.Diagnostics.Debug.WriteLine("UPDATE-COMPOUND-FUKUGO " + Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                            // DEBUG

                            decimal denpyoBango = 0;
                            DateTime denpyoDate = Util.ConvAdDate(
                                        Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                                        Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), Dba);

                            // 保持されているDataTableより更新
                            DataView dv = new DataView(this._fd[(i).ToString()].Copy());
                            dv.Sort = "GYO_BANGO ASC, TAISHAKU_KUBUN ASC, MEISAI_KUBUN ASC";
                            DataTable dt = dv.ToTable();

                            // 仕訳伝票データ登録
                            if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Add)
                            {
                                // 伝票番号採番
                                denpyoBango = ZaUtil.GetNewDenpyoBango(this.UInfo, this.Dba);

                                // DEBUG
                                System.Diagnostics.Debug.WriteLine("INSERT-COMPOUND-FUKUGO " + Util.ToString(denpyoBango));
                                // DEBUG

                                // データ追加
                                ArrayList alParamsZmDenpyo =
                                    SetZmShiwakeDenpyoParamsFukugo(
                                    (EditMode)rec.Fields["EDIT_MODE"].Value,
                                    denpyoBango,
                                    denpyoDate,
                                    Util.ToString(rec.Fields["SHOHYO_BANGO"].Value),
                                    Util.ToInt(Util.ToString(dt.Rows[0]["SHIWAKE_GYOSU"])),
                                    upd);
                                this.Dba.Insert("TB_ZM_SHIWAKE_DENPYO", (DbParamCollection)alParamsZmDenpyo[0]);
                            }
                            else if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Edit)
                            {
                                // DEBUG
                                System.Diagnostics.Debug.WriteLine("UPDATE-COMPOUND-FUKUGO " + Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                                // DEBUG

                                // 伝票番号
                                denpyoBango = ZaUtil.ToDecimal(rec.Fields["DENPYO_BANGO"].Value);

                                // データ更新
                                ArrayList alParamsZmDenpyo =
                                    SetZmShiwakeDenpyoParamsFukugo(
                                    (EditMode)rec.Fields["EDIT_MODE"].Value,
                                    denpyoBango,
                                    denpyoDate,
                                    Util.ToString(rec.Fields["SHOHYO_BANGO"].Value),
                                    Util.ToInt(Util.ToString(dt.Rows[0]["SHIWAKE_GYOSU"])),
                                    upd);
                                this.Dba.Update("TB_ZM_SHIWAKE_DENPYO", (DbParamCollection)alParamsZmDenpyo[1],
                                                    where, (DbParamCollection)alParamsZmDenpyo[0]);
                            }
                            // 仕訳明細データ登録
                            if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Add
                                || (EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Edit)
                            {
                                // 仕訳明細データ登録(常に保存済レコード削除＆編集レコード挿入)
                                // (明細データ削除)
                                DbParamCollection dpc = new DbParamCollection();
                                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 2, this.ShishoCode);
                                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);

                                this.Dba.Delete("TB_ZM_SHIWAKE_MEISAI", where, dpc);

                                // (明細データ追加)
                                // 明細自体は複合入力の方で作成した物
                                foreach (DataRow dr in dt.Rows)
                                {
                                    ArrayList alParamsZmMeisai = SetZmShiwakeMeisaiParamsFukugo(dr,
                                        denpyoBango, denpyoDate, upd);
                                    this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParamsZmMeisai[0]);
                                }
                            }
                        }
                        #endregion

                        #endregion

                        #region 更新しない行
                        if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.None)
                        {
                            // DEBUG
                            System.Diagnostics.Debug.WriteLine("NONE " + Util.ToString(rec.Fields["DENPYO_BANGO"].Value));
                            // DEBUG
                        }
                        #endregion
                    }
                }

                // トランザクションをコミット
                this.Dba.Commit();

                // 登録データを再ロード
                DataLoad();

                // 伝票更新ロック開放
                ZaUtil.UpdateLockOff(this.UInfo, this.Dba);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // 編集内容クリア：ESC取消
            PressEsc();
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        public override void PressF7()
        {
            if (!this.btnF7.Enabled)
                return;

            // 行挿入
            if (_activeCRecord != null)
            {
                int i = this.mtbList.Content.Records.IndexOf(_activeCRecord);
                this.mtbList.Content.InsertRecord(i);
                UTable.CRecord rec = mtbList.Content.LastAddedRecord;
                rec.Fields["NO"].Value = i + 1;
                rec.Fields["JURNAL_TYPE"].Value = JurnalType.Single;
                rec.Fields["EDIT_MODE"].Value = EditMode.Add;

                SetJp(rec, Util.ConvJpDate(Util.ToString(_denpyoDateTo), this.Dba));

                this.UTableNoRefresh();
            }
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            if (!this.btnF8.Enabled)
                return;

            // 自動仕訳作成の場合は変更不可
            if (_activeCRecord != null &&
                Util.ToInt(Util.ToString(_activeCRecord.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
            {
                return;
            }

            // 行削除
            if (_activeCRecord != null)
            {
                UTable.CRecord rec = _activeCRecord;
                if (!ValChk.IsEmpty(rec.Fields["NO"].Value))
                {
                    rec.Fields["NO"].Value = Util.ToString(rec.Fields["NO"].Value) + "D";
                    rec.Fields["EDIT_MODE"].Value = EditMode.Delete;
                    rec.Fields["ZANDAKA"].Value = "";
                    rec.Visible = false;

                    //合計計算
                    CalcTotal();

                    this.UTableNoRefresh();
                }
            }
        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            if (!this.btnF9.Enabled)
                return;

            // 自動仕訳作成の場合は変更不可
            if (_activeCRecord != null)
            {
                int i = this.mtbList.Content.Records.IndexOf(_activeCRecord);
                if (i > 0)
                {
                    UTable.CRecord rec = this.mtbList.Content.Records[i];
                    if (Util.ToInt(Util.ToString(rec.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
                    {
                        return;
                    }
                    rec = this.mtbList.Content.Records[i - 1];
                    if (Util.ToInt(Util.ToString(rec.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
                    {
                        return;
                    }
                }
            }

            // 前行複写
            if (_activeCRecord != null &&
                (JurnalType)_activeCRecord.Fields["JURNAL_TYPE"].Value == JurnalType.Single)
            {
                UTable.CRecord srcRec = null;

                // 複写元レコード取得
                int curIndex = this.mtbList.Content.Records.IndexOf(_activeCRecord);
                for (int i = curIndex - 1; i >= 0; i--)
                {
                    UTable.CRecord rec = this.mtbList.Content.Records[i];
                    if (rec.Visible && (JurnalType)rec.Fields["JURNAL_TYPE"].Value == JurnalType.Single)
                    {
                        srcRec = rec;
                        break;
                    }
                }
                if (srcRec != null)
                {
                    using (mtbList.RenderBlock())
                    {
                        //_activeCRecord.Fields["NO"].Value = srcRec.Fields["NO"].Value;
                        //_activeCRecord.Fields["DENPYO_BANGO"].Value = "";
                        _activeCRecord.Fields["DENPYO_DATE_G"].Value = srcRec.Fields["DENPYO_DATE_G"].Value;
                        _activeCRecord.Fields["DENPYO_DATE_E"].Value = srcRec.Fields["DENPYO_DATE_E"].Value;
                        _activeCRecord.Fields["DENPYO_DATE_M"].Value = srcRec.Fields["DENPYO_DATE_M"].Value;
                        _activeCRecord.Fields["DENPYO_DATE_D"].Value = srcRec.Fields["DENPYO_DATE_D"].Value;
                        _activeCRecord.Fields["TEKIYO_CD"].Value = srcRec.Fields["TEKIYO_CD"].Value;
                        _activeCRecord.Fields["SHOHYO_BANGO"].Value = srcRec.Fields["SHOHYO_BANGO"].Value;
                        _activeCRecord.Fields["TEKIYO"].Value = srcRec.Fields["TEKIYO"].Value;
                        _activeCRecord.Fields["KANJO_KAMOKU_CD"].Value = srcRec.Fields["KANJO_KAMOKU_CD"].Value;
                        _activeCRecord.Fields["KANJO_KAMOKU_NM"].Value = srcRec.Fields["KANJO_KAMOKU_NM"].Value;
                        _activeCRecord.Fields["HOJO_KAMOKU_CD"].Value = srcRec.Fields["HOJO_KAMOKU_CD"].Value;
                        _activeCRecord.Fields["HOJO_KAMOKU_NM"].Value = srcRec.Fields["HOJO_KAMOKU_NM"].Value;
                        _activeCRecord.Fields["BUMON_CD"].Value = srcRec.Fields["BUMON_CD"].Value;
                        _activeCRecord.Fields["JIGYO_KUBUN"].Value = srcRec.Fields["JIGYO_KUBUN"].Value;
                        _activeCRecord.Fields["ZEI_KUBUN"].Value = srcRec.Fields["ZEI_KUBUN"].Value;
                        _activeCRecord.Fields["DrDENPYO_KINGAKU"].Value = srcRec.Fields["DrDENPYO_KINGAKU"].Value;
                        _activeCRecord.Fields["DrSHOHIZEI_KINGAKU"].Value = srcRec.Fields["DrSHOHIZEI_KINGAKU"].Value;
                        _activeCRecord.Fields["CrDENPYO_KINGAKU"].Value = srcRec.Fields["CrDENPYO_KINGAKU"].Value;
                        _activeCRecord.Fields["CrSHOHIZEI_KINGAKU"].Value = srcRec.Fields["CrSHOHIZEI_KINGAKU"].Value;
                        _activeCRecord.Fields["ZANDAKA"].Value = srcRec.Fields["ZANDAKA"].Value;
                        _activeCRecord.Fields["ZANDAKA_SHITA"].Value = srcRec.Fields["ZANDAKA_SHITA"].Value;

                        //_activeCRecord.Fields["KOJI_CD"].Value = srcRec.Fields["KOJI_CD"].Value;
                        //_activeCRecord.Fields["KOJI_NM"].Value = srcRec.Fields["KOJI_NM"].Value;
                        //_activeCRecord.Fields["KOSHU_CD"].Value = srcRec.Fields["KOSHU_CD"].Value;
                        //_activeCRecord.Fields["KOSHU_NM"].Value = srcRec.Fields["KOSHU_NM"].Value;

                        //_activeCRecord.Fields["JURNAL_TYPE"].Value = srcRec.Fields["JURNAL_TYPE"].Value;
                        //_activeCRecord.Fields["EDIT_MODE"].Value = srcRec.Fields["EDIT_MODE"].Value;
                        //_activeCRecord.Fields["KOJI_UMU"].Value = srcRec.Fields["KOJI_UMU"].Value;
                        _activeCRecord.Fields["KAZEI_KUBUN"].Value = srcRec.Fields["KAZEI_KUBUN"].Value;
                        _activeCRecord.Fields["SHOHIZEI_HENKO"].Value = srcRec.Fields["SHOHIZEI_HENKO"].Value;
                        _activeCRecord.Fields["KESSAN_KUBUN"].Value = srcRec.Fields["KESSAN_KUBUN"].Value;
                        // 編集可否
                        CFieldEnabled(_activeCRecord.Fields["HOJO_KAMOKU_CD"], CFieldEnabled(srcRec.Fields["HOJO_KAMOKU_CD"]));
                        CFieldEnabled(_activeCRecord.Fields["BUMON_CD"], CFieldEnabled(srcRec.Fields["BUMON_CD"]));
                        CFieldEnabled(_activeCRecord.Fields["DrDENPYO_KINGAKU"], CFieldEnabled(srcRec.Fields["DrDENPYO_KINGAKU"]));
                        CFieldEnabled(_activeCRecord.Fields["CrDENPYO_KINGAKU"], CFieldEnabled(srcRec.Fields["CrDENPYO_KINGAKU"]));
                        CFieldEnabled(_activeCRecord.Fields["DrSHOHIZEI_KINGAKU"], CFieldEnabled(srcRec.Fields["DrSHOHIZEI_KINGAKU"]));
                        CFieldEnabled(_activeCRecord.Fields["CrSHOHIZEI_KINGAKU"], CFieldEnabled(srcRec.Fields["CrSHOHIZEI_KINGAKU"]));

                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (Util.ToDecimal(Util.ToString(_activeCRecord.Fields["DrDENPYO_KINGAKU"].Value)) >= 0)
                        {
                            _activeCRecord.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                            _activeCRecord.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            _activeCRecord.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                            _activeCRecord.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                        if (Util.ToDecimal(Util.ToString(_activeCRecord.Fields["CrDENPYO_KINGAKU"].Value)) >= 0)
                        {
                            _activeCRecord.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                            _activeCRecord.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            _activeCRecord.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                            _activeCRecord.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                    }
                    if (_activeCRecord.Fields["NO"].Value == null)
                    {
                        _activeCRecord.Fields["NO"].Value = mtbList.Content.Records.Count;
                        _activeCRecord.Fields["EDIT_MODE"].Value = EditMode.Add;            // レコード追加あり
                        AddBlankRecord();                                                   // ブランク行を追加
                    }
                    else
                    {
                        _activeCRecord.Fields["EDIT_MODE"].Value = EditMode.Edit;           // レコード編集あり

                    }

                    //合計計算
                    CalcTotal();

                    this.UTableNoRefresh();
                }
            }
        }

        /// <summary>
        /// F10キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            SpecifyRange(false);
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            if (!this.btnF11.Enabled)
                return;

            // アセンブリのロード
            //System.Reflection.Assembly asm = System.Reflection.Assembly.LoadFrom("ZAMC9511.exe");
            Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMDE1031.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.zm.zmde1031.ZMDE1033");
            if (t != null)
            {
                //Object obj = System.Activator.CreateInstance(t);
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    // タブの一部として埋め込む
                    BasePgForm frm = (BasePgForm)obj;
                    frm.ShowDialog(this);
                }
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            if (!this.btnF12.Enabled)
                return;

            // UTableカレント行が有効な単一仕訳伝票の場合に実行
            string tekiyoCd = "";
            string tekiyoNm = "";
            if (_activeCRecord != null && (JurnalType)_activeCRecord.Fields["JURNAL_TYPE"].Value == JurnalType.Single)
            {
                tekiyoCd = Util.ToString(_activeCRecord.Fields["TEKIYO_CD"].Value);
                tekiyoNm = Util.ToString(_activeCRecord.Fields["TEKIYO"].Value);

                // 編集中仕訳事例データ
                DataTable dt = GetDtJirei();

                // 保存可能な仕訳事例が存在すれば実行
                if (dt.Rows.Count > 0)
                {
                    //// (デバッグ用)
                    //dt.WriteXml("C:\\WORK\\JIREI.xml");
                    //// (デバッグ用)

                    //// アセンブリのロード
                    //System.Reflection.Assembly asm = System.Reflection.Assembly.LoadFrom("ZAMC9521.exe");
                    //// フォーム作成
                    //Type t = asm.GetType("jp.co.fsi.zam.zamc9521.ZAMC9521");
                    //if (t != null)
                    //{
                    //    Object obj = System.Activator.CreateInstance(t);
                    //    if (obj != null)
                    //    {
                    //        // タブの一部として埋め込む
                    //        zamc9521.ZAMC9521 frm = (zamc9521.ZAMC9521)obj;
                    //        frm.TekiyoCd = tekiyoCd;
                    //        frm.DtJirei = GetDtJirei();
                    //        frm.ShowDialog(this);
                    //    }
                    //}

                    // アセンブリのロード
                    Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMDE1031.exe");
                    // フォーム作成
                    Type t = asm.GetType("jp.co.fsi.zm.zmde1031.ZMDE1035");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;

                            object[] inData = new object[3];
                            inData[0] = tekiyoCd;
                            inData[1] = tekiyoNm;
                            inData[2] = dt;
                            frm.InData = inData;

                            frm.ShowDialog(this);
                        }
                    }
                }
            }
        }

        #endregion

        #region イベント
        private void ZMDE1011_Shown(object sender, EventArgs e)
        {
            // 範囲指定(初回)
            SpecifyRange(true);
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 共通データ読込
        /// </summary>
        private void InitFormDataLoad()
        {
            #region 勘定科目データ
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                StringBuilder cols = new StringBuilder();
                cols.Append("KAISHA_CD");
                cols.Append(", KAIKEI_NENDO");
                cols.Append(", KANJO_KAMOKU_CD");
                cols.Append(", KANJO_KAMOKU_NM");
                cols.Append(", KANJO_KAMOKU_KANA_NM");
                cols.Append(", KAMOKU_BUNRUI_CD");
                cols.Append(", KAMOKU_BUNRUI_NM");
                cols.Append(", KAMOKU_KUBUN");
                cols.Append(", KAMOKU_KUBUN_NM");
                cols.Append(", TAISHAKU_KUBUN");
                cols.Append(", TAISHAKU_KUBUN_NM");
                cols.Append(", KARIKATA_ZEI_KUBUN");
                cols.Append(", KARIKATA_ZEI_KUBUN_NM");
                cols.Append(", KARI_KAZEI_KUBUN");
                cols.Append(", KARI_TORIHIKI_KUBUN");
                cols.Append(", KARI_ZEI_RITSU");
                cols.Append(", KARI_TAISHAKU_KUBUN");
                cols.Append(", KARI_HENKAN_KUBUN");
                cols.Append(", KASHIKATA_ZEI_KUBUN");
                cols.Append(", KASHIKATA_ZEI_KUBUN_NM");
                cols.Append(", KASHI_KAZEI_KUBUN");
                cols.Append(", KASHI_TORIHIKI_KUBUN");
                cols.Append(", KASHI_ZEI_RITSU");
                cols.Append(", KASHI_TAISHAKU_KUBUN");
                cols.Append(", KASHI_HENKAN_KUBUN");
                cols.Append(", BUMON_UMU");
                cols.Append(", BUMON_UMU_NM");
                cols.Append(", HOJO_KAMOKU_UMU");
                cols.Append(", ASHOJO_KAMOKU_UMU_NM");
                cols.Append(", HOJO_SHIYO_KUBUN");
                cols.Append(", HOJO_SHIYO_KUBUN_NM");
                //cols.Append(", KOJI_UMU");
                //cols.Append(", KOJI_UMU_NM");
                cols.Append(", SHIYO");
                cols.Append(", SHIYO_NM");
                cols.Append(", SHUKEI_CD");
                cols.Append(", SHUKEI_KEISAN_KUBUN");
                StringBuilder where = new StringBuilder();
                where.Append("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
                _dtKanjoKamoku = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "VI_ZM_KANJO_KAMOKU"
                                , Util.ToString(where), dpc);
            }
            #endregion

            #region 税区分データ
            {
                StringBuilder sql = new StringBuilder();
                sql.Append("SELECT ZEI_KUBUN");
                sql.Append(", ZEI_KUBUN_NM");
                sql.Append(", KAZEI_KUBUN");
                sql.Append(", TORIHIKI_KUBUN");
                sql.Append(", ZEI_RITSU");
                sql.Append(", TAISHAKU_KUBUN");
                sql.Append(", HENKAN_KUBUN");
                sql.Append(" FROM TB_ZM_F_ZEI_KUBUN");
                _dtZeiKubun = this.Dba.GetDataTableFromSql(Util.ToString(sql));
            }
            #endregion

            #region 部門データ
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                StringBuilder cols = new StringBuilder();
                cols.Append("KAISHA_CD");
                cols.Append(", BUMON_CD");
                cols.Append(", BUMON_NM");
                cols.Append(", BUMON_KANA_NM");
                cols.Append(", JIGYO_KUBUN");
                cols.Append(", JIGYO_KUBUN_NM");
                cols.Append(", HAIFU_KUBUN");
                StringBuilder where = new StringBuilder();
                where.Append("KAISHA_CD = @KAISHA_CD");
                _dtBumon = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "VI_ZM_BUMON"
                                , Util.ToString(where), dpc);
            }
            #endregion
        }

        /// <summary>
        /// 明細部の初期化
        /// </summary>
        private void InitDetailArea()
        {
            UTable.CRecordProvider rp = new UTable.CRecordProvider();
            CLayoutBuilder lb = new CLayoutBuilder(CLayoutBuilder.EOrientation.ROW);
            UTable.CFieldDesc fd;

            // 工事情報を外し

            #region UTableフィールド設定・配置
            //****************************************************************************************************
            // 次の行・列位置へ各フィールドを配置
            //
            // 　　　 (　　　　　　　借方　　　　　　　　 )　　　　     (　　　　　　　貸方　　　　　　　　 )
            //0 行番号 伝票番号      摘要CD ダミ 証憑番号 科目CD 科目名 部門CD　 　　　 出金　   入金     残高 f工事CD 工事名
            //1 　　　 元号 年 月 日 摘要   　　          補助CD 補助名 事業区分 税区分 % 消費税 % 消費税 ダミ  工種CD 工種名 
            //  0      1   2  3  4  5      6   7        8     9      10       11    12 13    14 15    16   17     18
            //
            //(サイズ)
            //  30,    30, 20,20,20,30,    120,    30,    95,   20,      20,    25,75,  25, 75,   80,   30,   30
            //****************************************************************************************************
            // 明細連番フィールド
            fd = rp.AddField("NO", new CSjTextFieldProvider("NO."), lb.Set(0, 0).Next(2, 1));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 伝票番号フィールド
            fd = rp.AddField("DENPYO_BANGO", new CSjTextFieldProvider("伝票番号"), lb.Set(0, 1).Next(1, 4));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 伝票日付元号フィールド
            fd = rp.AddField("DENPYO_DATE_G", new CSjTextFieldProvider("伝票日付"), lb.Set(1, 1).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.MergeCaption = "DENPYO_DATE_D";
            // 伝票日付和暦年フィールド
            fd = rp.AddField("DENPYO_DATE_E", new CSjTextFieldProvider(), lb.Set(1, 2).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 伝票日付月フィールド
            fd = rp.AddField("DENPYO_DATE_M", new CSjTextFieldProvider(), lb.Set(1, 3).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 伝票日付日フィールド
            fd = rp.AddField("DENPYO_DATE_D", new CSjTextFieldProvider(), lb.Set(1, 4).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 摘要CDフィールド
            fd = rp.AddField("TEKIYO_CD", new CSjTextFieldProvider("摘要コード"), lb.Set(0, 5).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.MergeCaption = "TEKIYO_CD_MIGI";
            // 摘要コード右ダミーフィールド
            fd = rp.AddField("TEKIYO_CD_MIGI", new CSjTextFieldProvider(""), lb.Set(0, 6).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.CreateCaption = false;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 摘要フィールド
            fd = rp.AddField("TEKIYO", new CSjTextFieldProvider("摘　　　要"), lb.Set(1, 5).Next(1, 3));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 証憑番号
            fd = rp.AddField("SHOHYO_BANGO", new CSjTextFieldProvider("証憑番号"), lb.Set(0, 7).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 勘定科目CDフィールド
            fd = rp.AddField("KANJO_KAMOKU_CD", new CSjTextFieldProvider("勘定科目"), lb.Set(0, 8).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.MergeCaption = "KANJO_KAMOKU_NM";
            // 勘定科目名フィールド
            fd = rp.AddField("KANJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(0, 9).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 補助科目CDフィールド
            fd = rp.AddField("HOJO_KAMOKU_CD", new CSjTextFieldProvider("補助科目"), lb.Set(1, 8).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.MergeCaption = "HOJO_KAMOKU_NM";
            // 補助科目名フィールド
            fd = rp.AddField("HOJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(1, 9).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 部門CDフィールド
            fd = rp.AddField("BUMON_CD", new CSjTextFieldProvider("部門"), lb.Set(0, 10).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 事業区分フィールド
            fd = rp.AddField("JIGYO_KUBUN", new CSjTextFieldProvider("事"), lb.Set(1, 10).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 税区分フィールド
            fd = rp.AddField("ZEI_KUBUN", new CSjTextFieldProvider("税"), lb.Set(1, 11).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 借方伝票金額フィールド
            fd = rp.AddField("DrDENPYO_KINGAKU", new CSjTextFieldProvider("金　　　額"), lb.Set(0, 12).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.MergeCaption = "ZANDAKA";
            // 借方消費税率フィールド
            fd = rp.AddField("DrZEI_RITSU", new CSjTextFieldProvider("入　金"), lb.Set(1, 12).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.Setting.ForeColor = Color.Blue;
            fd.MergeCaption = "DrSHOHIZEI_KINGAKU";
            // 借方消費税金額フィールド
            fd = rp.AddField("DrSHOHIZEI_KINGAKU", new CSjTextFieldProvider(), lb.Set(1, 13).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 貸方伝票金額フィールド
            fd = rp.AddField("CrDENPYO_KINGAKU", new CSjTextFieldProvider(), lb.Set(0, 14).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 貸方消費税率フィールド
            fd = rp.AddField("CrZEI_RITSU", new CSjTextFieldProvider("出　金"), lb.Set(1, 14).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.Setting.ForeColor = Color.Blue;
            fd.MergeCaption = "CrSHOHIZEI_KINGAKU";
            // 貸方消費税金額フィールド
            fd = rp.AddField("CrSHOHIZEI_KINGAKU", new CSjTextFieldProvider(), lb.Set(1, 15).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 残高フィールド
            fd = rp.AddField("ZANDAKA", new CSjTextFieldProvider(), lb.Set(0, 16).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 残高下のダミーフィールド
            fd = rp.AddField("ZANDAKA_SHITA", new CSjTextFieldProvider("残　高"), lb.Set(1, 16).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            //// 工事CDフィールド
            //fd = rp.AddField("KOJI_CD", new CSjTextFieldProvider("工　事"), lb.Set(0, 17).Next());
            //fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            //fd.MergeCaption = "KOJI_NM";
            //// 工事名フィールド
            //fd = rp.AddField("KOJI_NM", new CSjTextFieldProvider(), lb.Set(0, 18).Next());
            //fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            //fd.CreateCaption = false;
            //// 工種CDフィールド
            //fd = rp.AddField("KOSHU_CD", new CSjTextFieldProvider("工　種"), lb.Set(1, 17).Next());
            //fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            //fd.MergeCaption = "KOSHU_NM";
            //// 工種名フィールド
            //fd = rp.AddField("KOSHU_NM", new CSjTextFieldProvider(), lb.Set(1, 18).Next());
            //fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            //fd.CreateCaption = false;

            // 工事削除で番号ズラシ
            //// (非表示)仕訳種類フィールド
            //fd = rp.AddField("JURNAL_TYPE", new CSjTextFieldProvider(), lb.Set(0, 19).Next());
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //// (非表示)レコード編集状態フィールド
            //fd = rp.AddField("EDIT_MODE", new CSjTextFieldProvider(), lb.Set(1, 19).Next());
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //// (非表示)工事有無フィールド
            //fd = rp.AddField("KOJI_UMU", new CSjTextFieldProvider(), lb.Set(0, 20).Next());
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //// (非表示)課税区分フィールド
            //fd = rp.AddField("KAZEI_KUBUN", new CSjTextFieldProvider(), lb.Set(1, 20).Next());
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //// (非表示)消費税変更フィールド
            //fd = rp.AddField("SHOHIZEI_HENKO", new CSjTextFieldProvider(), lb.Set(0, 21).Next());
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //// (非表示)決算区分フィールド
            //fd = rp.AddField("KESSAN_KUBUN", new CSjTextFieldProvider(), lb.Set(1, 21).Next());
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;

            //// (非表示)仕訳作成区分
            //fd = rp.AddField("SHIWAKE_SAKUSEI_KUBUN", new CSjTextFieldProvider(), lb.Set(0, 22).Next());
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //// (非表示)複合仕訳区分（振替伝票入力経由）
            //fd = rp.AddField("FUKUGO_SHIWAKE_KUBUN", new CSjTextFieldProvider(), lb.Set(1, 22).Next());
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)仕訳種類フィールド
            fd = rp.AddField("JURNAL_TYPE", new CSjTextFieldProvider(), lb.Set(0, 17).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)レコード編集状態フィールド
            fd = rp.AddField("EDIT_MODE", new CSjTextFieldProvider(), lb.Set(1, 17).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)工事有無フィールド
            fd = rp.AddField("KOJI_UMU", new CSjTextFieldProvider(), lb.Set(0, 18).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)課税区分フィールド
            fd = rp.AddField("KAZEI_KUBUN", new CSjTextFieldProvider(), lb.Set(1, 18).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)消費税変更フィールド
            fd = rp.AddField("SHOHIZEI_HENKO", new CSjTextFieldProvider(), lb.Set(0, 19).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)決算区分フィールド
            fd = rp.AddField("KESSAN_KUBUN", new CSjTextFieldProvider(), lb.Set(1, 19).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;

            // (非表示)仕訳作成区分
            fd = rp.AddField("SHIWAKE_SAKUSEI_KUBUN", new CSjTextFieldProvider(), lb.Set(0, 20).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)複合仕訳区分（振替伝票入力経由）
            fd = rp.AddField("FUKUGO_SHIWAKE_KUBUN", new CSjTextFieldProvider(), lb.Set(1, 20).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;

            #endregion

            this.mtbList.Content.SetRecordProvider(rp);
            this.mtbList.Setting.CaptionBackColor = Color.LightSkyBlue;
            this.mtbList.Setting.CaptionForeColor = Color.Navy;
            this.mtbList.Setting.BackColor = SystemColors.HighlightText;
            this.mtbList.Setting.UserColResizable = UTable.EAllow.DISABLE;
            this.mtbList.CreateCaption(UTable.EHAlign.MIDDLE);
            this.mtbList.Setting.ContentBackColor = SystemColors.AppWorkspace;
            this.mtbList.KeyboardOperation.EnterBehavior = CKeyboardOperation.EBehavior.NEXT_EDITABLE_FIELD;
            this.mtbList.Cols.SetSize(25, 25, 30, 30, 30, 30, 40, 280, 50, 220, 20, 20, 25, 75, 30, 80, 90, 1, 1, 1, 1);
        }

        /// <summary>
        /// データの読込
        /// </summary>
        private void DataLoad()
        {
            DbParamCollection dpc;
            StringBuilder sql;

            // 検索中メッセージ表示
            ZMMR1013 msgFrm = new ZMMR1013();
            msgFrm.Show();
            msgFrm.Refresh();

            // 伝票情報データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, DENPYO_KUBUN);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtKanjoKamokuCd.Text));
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, Util.ToDecimal(this.txtHojoKamokuCd.Text));
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtBumonCd.Text));
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, this._denpyoDateFr);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, this._denpyoDateTo);
            #region sql = SQL文
            sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append("  A.KAISHA_CD");
            sql.Append(", A.SHISHO_CD");
            sql.Append(", A.DENPYO_DATE");
            sql.Append(", A.DENPYO_BANGO");
            sql.Append(", A.KAIKEI_NENDO");
            sql.Append(", A.SHOHIZEI_NYURYOKU_HOHO");
            sql.Append(", (SELECT MAX(S.GYO_BANGO) FROM VI_ZM_SHIWAKE_MEISAI AS S ");
            sql.Append("   WHERE A.KAISHA_CD = S.KAISHA_CD ");
            sql.Append("     AND A.SHISHO_CD = S.SHISHO_CD ");
            sql.Append("     AND A.DENPYO_BANGO = S.DENPYO_BANGO ");
            sql.Append("     AND A.KAIKEI_NENDO = S.KAIKEI_NENDO ");
            sql.Append("  ) AS MAXGYO_BANGO");
            //// 貸借共に課税区分がある場合も諸口の対象（出納科目に課税区分がある場合）
            //sql.Append(", (SELECT SUM(CASE WHEN DR.TAISHAKU_KUBUN = 1 THEN ");
            //sql.Append("                  DR.KAZEI_KUBUN ELSE 0 ");
            //sql.Append("              END) AS DrKAZEI_KUBUN ");
            //sql.Append("   FROM VI_ZM_SHIWAKE_MEISAI AS DR ");
            //sql.Append("   WHERE A.KAISHA_CD = DR.KAISHA_CD ");
            //sql.Append("     AND A.SHISHO_CD = DR.SHISHO_CD ");
            //sql.Append("     AND A.DENPYO_BANGO = DR.DENPYO_BANGO ");
            //sql.Append("     AND A.KAIKEI_NENDO = DR.KAIKEI_NENDO ");
            //sql.Append("     AND DR.MEISAI_KUBUN = 0");
            //sql.Append("  ) AS DrKAZEI_KUBUN ");
            //sql.Append(", (SELECT SUM(CASE WHEN CR.TAISHAKU_KUBUN = 2 THEN ");
            //sql.Append("                  CR.KAZEI_KUBUN ELSE 0  ");
            //sql.Append("              END) AS CrKAZEI_KUBUN ");
            //sql.Append("   FROM VI_ZM_SHIWAKE_MEISAI AS CR ");
            //sql.Append("   WHERE A.KAISHA_CD = CR.KAISHA_CD ");
            //sql.Append("     AND A.SHISHO_CD = CR.SHISHO_CD ");
            //sql.Append("     AND A.DENPYO_BANGO = CR.DENPYO_BANGO ");
            //sql.Append("     AND A.KAIKEI_NENDO = CR.KAIKEI_NENDO ");
            //sql.Append("     AND CR.MEISAI_KUBUN = 0");
            //sql.Append("  ) AS CrKAZEI_KUBUN ");
            // 貸借毎の軒数
            sql.Append(", (SELECT COUNT(*) AS DrCOUNT ");
            sql.Append("   FROM VI_ZM_SHIWAKE_MEISAI AS DR ");
            sql.Append("   WHERE A.KAISHA_CD = DR.KAISHA_CD ");
            sql.Append("     AND A.SHISHO_CD = DR.SHISHO_CD ");
            sql.Append("     AND A.DENPYO_BANGO = DR.DENPYO_BANGO ");
            sql.Append("     AND A.KAIKEI_NENDO = DR.KAIKEI_NENDO ");
            sql.Append("     AND DR.TAISHAKU_KUBUN = 1 ");
            sql.Append("     AND DR.MEISAI_KUBUN = 0");
            sql.Append("  ) AS DrCOUNT ");
            sql.Append(", (SELECT COUNT(*) AS CrCOUNT ");
            sql.Append("   FROM VI_ZM_SHIWAKE_MEISAI AS CR ");
            sql.Append("   WHERE A.KAISHA_CD = CR.KAISHA_CD ");
            sql.Append("     AND A.SHISHO_CD = CR.SHISHO_CD ");
            sql.Append("     AND A.DENPYO_BANGO = CR.DENPYO_BANGO ");
            sql.Append("     AND A.KAIKEI_NENDO = CR.KAIKEI_NENDO ");
            sql.Append("     AND CR.TAISHAKU_KUBUN = 2");
            sql.Append("     AND CR.MEISAI_KUBUN = 0");
            sql.Append("  ) AS CrCOUNT ");

            sql.Append(" FROM");
            sql.Append("  VI_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" WHERE");
            sql.Append("  A.KAISHA_CD = @KAISHA_CD");
            sql.Append("  AND A.SHISHO_CD = @SHISHO_CD");
            sql.Append("  AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append("  AND A.MEISAI_KUBUN = 0");
            sql.Append("  AND A.DENPYO_KUBUN = @DENPYO_KUBUN");
            sql.Append("  AND A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            sql.Append("  AND A.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
            sql.Append("  AND A.BUMON_CD = @BUMON_CD");
            sql.Append("  AND A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO");
            sql.Append(" GROUP BY");
            sql.Append("  A.KAISHA_CD");
            sql.Append(", A.SHISHO_CD");
            sql.Append(", A.DENPYO_DATE");
            sql.Append(", A.DENPYO_BANGO");
            sql.Append(", A.KAIKEI_NENDO");
            sql.Append(", A.SHOHIZEI_NYURYOKU_HOHO");
            sql.Append(" ORDER BY");
            sql.Append("  A.KAISHA_CD");
            sql.Append(", A.DENPYO_DATE");
            sql.Append(", A.DENPYO_BANGO");
            #endregion
            DataTable dtDenpyo = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            /// **************************************************
            /// UTableレンダリングブロック開始
            /// **************************************************
            using (mtbList.RenderBlock())
            {
                this.mtbList.Content.ClearRecord();         // UTableレコードクリア
                decimal no = 0;                             // 伝票明細行の連番
                int autoDataFlg = 0;                        // 自動仕訳作成区分
                // 複合伝票保持のクリア
                this._fd = new Dictionary<string, DataTable>();

                foreach (DataRow drDenpyo in dtDenpyo.Rows)
                {
                    // UTableレコード
                    UTable.CRecord rec;
                    no++;
                    rec = this.mtbList.Content.AddRecord();

                    // VI仕訳明細データ
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(drDenpyo["DENPYO_BANGO"])));
                    #region sql = SQL文
                    sql = new StringBuilder();
                    sql.Append("SELECT");
                    sql.Append("  KAISHA_CD");
                    sql.Append(", SHISHO_CD");
                    sql.Append(", DENPYO_DATE");
                    sql.Append(", DENPYO_BANGO");
                    sql.Append(", GYO_BANGO");
                    sql.Append(", TAISHAKU_KUBUN");
                    sql.Append(", MEISAI_KUBUN");
                    sql.Append(", DENPYO_KUBUN");
                    sql.Append(", SHOHYO_BANGO");
                    sql.Append(", TANTOSHA_CD");
                    sql.Append(", TANTOSHA_NM");
                    sql.Append(", KANJO_KAMOKU_CD");
                    sql.Append(", KANJO_KAMOKU_NM");
                    sql.Append(", AITE_KANJO_KAMOKU_CD");
                    sql.Append(", AITE_KANJO_KAMOKU_NM");
                    sql.Append(", BUMON_UMU");
                    sql.Append(", HOJO_UMU");
                    //sql.Append(", KOJI_UMU");
                    sql.Append(", HOJO_KAMOKU_CD");
                    sql.Append(", HOJO_KAMOKU_NM");
                    sql.Append(", BUMON_CD");
                    sql.Append(", BUMON_NM");
                    //sql.Append(", KOJI_CD");
                    //sql.Append(", KOJI_NM");
                    //sql.Append(", KOSHU_CD");
                    //sql.Append(", KOSHU_NM");
                    sql.Append(", TEKIYO_CD");
                    sql.Append(", TEKIYO");
                    sql.Append(", ZEI_KUBUN");
                    sql.Append(", ZEI_KUBUN_NM");
                    sql.Append(", KAZEI_KUBUN");
                    sql.Append(", TORIHIKI_KUBUN");
                    sql.Append(", ZEI_RITSU");
                    sql.Append(", JIGYO_KUBUN");
                    sql.Append(", SHOHIZEI_NYURYOKU_HOHO");
                    sql.Append(", SHOHIZEI_HENKO");
                    sql.Append(", KESSAN_KUBUN");
                    sql.Append(", ZEIKOMI_KINGAKU");
                    sql.Append(", ZEINUKI_KINGAKU");
                    sql.Append(", SHOHIZEI_KINGAKU");
                    sql.Append(", SHIWAKE_SAKUSEI_KUBUN");
                    sql.Append(" FROM");
                    sql.Append("  VI_ZM_SHIWAKE_MEISAI");
                    sql.Append(" WHERE");
                    sql.Append("  KAISHA_CD = @KAISHA_CD");
                    sql.Append("  AND SHISHO_CD = @SHISHO_CD");
                    sql.Append("  AND KAIKEI_NENDO = @KAIKEI_NENDO");
                    sql.Append("  AND DENPYO_BANGO = @DENPYO_BANGO");
                    sql.Append("  AND MEISAI_KUBUN = 0");
                    sql.Append(" ORDER BY");
                    sql.Append("  GYO_BANGO ASC");
                    sql.Append(", TAISHAKU_KUBUN ASC");
                    sql.Append(", MEISAI_KUBUN ASC");
                    #endregion
                    DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

                    // 仕訳内容により編集明細をセット
                    decimal maxGyo = 0;                     // 保存済行数
                    decimal drDenpyoKingaku = 0;            // 借方伝票金額
                    decimal drShohizeiRitsu = 0;            // 借方消費税率
                    decimal drShohizeiKingaku = 0;          // 借方消費税額
                    decimal crDenpyoKingaku = 0;            // 貸方伝票金額
                    decimal crShohizeiKingaku = 0;          // 貸方消費税額
                    decimal crShohizeiRitsu = 0;            // 貸方消費税率

                    decimal drKzeiCount = 0;                // 借方課税区分
                    decimal crKzeiCount = 0;                // 貸方課税区分

                    foreach (DataRow dr in dt.Rows)
                    {
                        maxGyo = Util.ToDecimal(Util.ToString(dr["GYO_BANGO"]));

                        // 貸借レコード共通
                        if (rec.Fields["NO"].Value == null)
                        {
                            rec.Fields["NO"].Value = Util.ToString(no);
                            rec.Fields["DENPYO_BANGO"].Value = Util.ToString(dr["DENPYO_BANGO"]);

                            SetJp(rec, Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba));

                            rec.Fields["TEKIYO_CD"].Value = ZaUtil.FormatNum(dr["TEKIYO_CD"]);
                            rec.Fields["TEKIYO"].Value = Util.ToString(dr["TEKIYO"]);
                            rec.Fields["SHOHYO_BANGO"].Value = Util.ToString(dr["SHOHYO_BANGO"]);
                            rec.Fields["KESSAN_KUBUN"].Value = Util.ToString(dr["KESSAN_KUBUN"]);
                            rec.Fields["JURNAL_TYPE"].Value = JurnalType.Single;
                            rec.Fields["EDIT_MODE"].Value = EditMode.None;

                            // 諸口伝票の場合はコレクションへ保持（複合仕訳入力）
                            //if (Util.ToDecimal(Util.ToString(drDenpyo["MAXGYO_BANGO"])) > 1 ||
                            //    (Util.ToDecimal(Util.ToString(drDenpyo["DrKAZEI_KUBUN"])) >= 1 &&
                            //     Util.ToDecimal(Util.ToString(drDenpyo["CrKAZEI_KUBUN"])) >= 1))
                            if (Util.ToDecimal(Util.ToString(drDenpyo["MAXGYO_BANGO"])) > 1 ||
                                ((Util.ToDecimal(Util.ToString(drDenpyo["DrCOUNT"])) +
                                 Util.ToDecimal(Util.ToString(drDenpyo["CrCOUNT"])) > 2)))
                            {
                                this._fd.Add(Util.ToString(no - 1), dt.Copy());
                                drKzeiCount = Util.ToDecimal(Util.ToString(drDenpyo["DrCOUNT"]));
                                crKzeiCount = Util.ToDecimal(Util.ToString(drDenpyo["CrCOUNT"]));
                            }
                        }

                        // 貸借レコード別
                        if (Util.ToDecimal(Util.ToString(dr["KANJO_KAMOKU_CD"])) == Util.ToDecimal(this.txtKanjoKamokuCd.Text)
                             && Util.ToDecimal(Util.ToString(dr["HOJO_KAMOKU_CD"])) == Util.ToDecimal(this.txtHojoKamokuCd.Text))
                        {
                            // 出納勘定レコード情報
                            if (Util.ToInt(Util.ToString(dr["TAISHAKU_KUBUN"])) == 1)
                            {
                                // 借方金額集計
                                drDenpyoKingaku += ZaUtil.GetDenpyoKingaku(
                                                        Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                        Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])), this.UInfo);
                            }
                            else
                            {
                                // 貸方金額集計
                                crDenpyoKingaku += ZaUtil.GetDenpyoKingaku(
                                                        Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                        Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])), this.UInfo);
                            }
                            //// 工事・工種情報
                            //if (Util.ToInt(dr["KOJI_UMU"]) == 1)
                            //{
                            //    rec.Fields["KOJI_CD"].Value = ZaUtil.FormatNum(dr["KOJI_CD"]);
                            //    rec.Fields["KOJI_NM"].Value = Util.ToString(dr["KOJI_NM"]);
                            //    rec.Fields["KOSHU_CD"].Value = ZaUtil.FormatNum(dr["KOSHU_CD"]);
                            //    rec.Fields["KOSHU_NM"].Value = Util.ToString(dr["KOSHU_NM"]);
                            //}
                        }
                        else
                        {
                            // 相手科目レコードから明細情報をセット
                            rec.Fields["KANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KANJO_KAMOKU_CD"]);
                            rec.Fields["KANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                            rec.Fields["HOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["HOJO_KAMOKU_CD"]);
                            rec.Fields["HOJO_KAMOKU_NM"].Value = Util.ToString(dr["HOJO_KAMOKU_NM"]);
                            rec.Fields["BUMON_CD"].Value = ZaUtil.FormatNum(dr["BUMON_CD"]);
                            rec.Fields["JIGYO_KUBUN"].Value = ZaUtil.FormatNum(dr["JIGYO_KUBUN"]);
                            rec.Fields["ZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["ZEI_KUBUN"]);
                            // 消費税額は相手レコードから取得
                            // (するので保存された自レコード貸借区分とは反対へセットする)
                            if (Util.ToInt(Util.ToString(dr["TAISHAKU_KUBUN"])) == 1)
                            {
                                crShohizeiRitsu = Util.ToDecimal(Util.ToString(dr["ZEI_RITSU"]));
                                //crShohizeiKingaku = Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"]));
                                crShohizeiKingaku += Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"]));
                            }
                            else
                            {
                                drShohizeiRitsu = Util.ToDecimal(Util.ToString(dr["ZEI_RITSU"]));
                                //drShohizeiKingaku = Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"]));
                                drShohizeiKingaku += Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"]));
                            }
                            // 非表示フィールド
                            rec.Fields["EDIT_MODE"].Value = EditMode.None;
                            //rec.Fields["KOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                            rec.Fields["KAZEI_KUBUN"].Value = Util.ToString(dr["KAZEI_KUBUN"]);
                            rec.Fields["SHOHIZEI_HENKO"].Value = Util.ToString(dr["SHOHIZEI_HENKO"]);
                            //// 工事・工種情報
                            //if (Util.ToInt(dr["KOJI_UMU"]) == 1)
                            //{
                            //    rec.Fields["KOJI_CD"].Value = ZaUtil.FormatNum(dr["KOJI_CD"]);
                            //    rec.Fields["KOJI_NM"].Value = Util.ToString(dr["KOJI_NM"]);
                            //    rec.Fields["KOSHU_CD"].Value = ZaUtil.FormatNum(dr["KOSHU_CD"]);
                            //    rec.Fields["KOSHU_NM"].Value = Util.ToString(dr["KOSHU_NM"]);
                            //}
                            // 編集可否
                            CFieldEnabled(rec.Fields["HOJO_KAMOKU_CD"], Util.ToDecimal(Util.ToString(dr["HOJO_UMU"])) == 1);
                            CFieldEnabled(rec.Fields["BUMON_CD"], Util.ToDecimal(Util.ToString(dr["BUMON_UMU"])) == 1);
                        }
                        autoDataFlg = Util.ToInt(Util.ToString(dr["SHIWAKE_SAKUSEI_KUBUN"]));
                    }
                    // 伝票金額・消費税
                    rec.Fields["DrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(drDenpyoKingaku);
                    rec.Fields["CrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(crDenpyoKingaku);
                    rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(drShohizeiRitsu);
                    rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(crShohizeiRitsu);
                    rec.Fields["DrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(drShohizeiKingaku);
                    rec.Fields["CrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(crShohizeiKingaku);

                    // 伝票金額、消費税がゼロ以下は文字色を赤にする
                    if (drDenpyoKingaku >= 0)
                    {
                        rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                        rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                    }
                    else
                    {
                        rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                        rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                    }
                    if (crDenpyoKingaku >= 0)
                    {
                        rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                        rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                    }
                    else
                    {
                        rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                        rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                    }

                    // 諸口仕訳判定
                    //if (maxGyo > 1 || drDenpyoKingaku == crDenpyoKingaku ||
                    //    (drKzeiCount >=1 && crKzeiCount >= 1))
                    if (maxGyo > 1 || drDenpyoKingaku == crDenpyoKingaku ||
                        ((drKzeiCount + crKzeiCount) > 2))
                    {
                        // 諸口レコードをセット
                        rec.Fields["TEKIYO_CD"].Value = "";
                        rec.Fields["TEKIYO"].Value = "＜諸口＞";
                        rec.Fields["KANJO_KAMOKU_CD"].Value = "";
                        rec.Fields["KANJO_KAMOKU_NM"].Value = "";
                        rec.Fields["HOJO_KAMOKU_CD"].Value = "";
                        rec.Fields["HOJO_KAMOKU_NM"].Value = "";
                        rec.Fields["BUMON_CD"].Value = "";
                        rec.Fields["JIGYO_KUBUN"].Value = "";
                        rec.Fields["DrZEI_RITSU"].Value = "";
                        rec.Fields["CrZEI_RITSU"].Value = "";
                        // 非表示フィールド
                        rec.Fields["JURNAL_TYPE"].Value = JurnalType.Compound;
                        rec.Fields["EDIT_MODE"].Value = EditMode.None;
                        //rec.Fields["KOJI_UMU"].Value = "";
                        rec.Fields["KAZEI_KUBUN"].Value = "";
                        rec.Fields["SHOHIZEI_HENKO"].Value = "";
                        rec.Fields["ZEI_KUBUN"].Value = "";
                        rec.Fields["KESSAN_KUBUN"].Value = "";
                        // 編集可否
                        CFieldEnabled(rec.Fields["TEKIYO_CD"], false);
                        CFieldEnabled(rec.Fields["TEKIYO"], false);
                        CFieldEnabled(rec.Fields["KANJO_KAMOKU_CD"], false);
                        CFieldEnabled(rec.Fields["HOJO_KAMOKU_CD"], false);
                        CFieldEnabled(rec.Fields["BUMON_CD"], false);
                        CFieldEnabled(rec.Fields["JIGYO_KUBUN"], false);
                        CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], false);
                        CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], false);
                        CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], false);
                        CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], false);
                    }
                    else
                    {
                        // 伝票金額・消費税編集可否
                        if (drDenpyoKingaku > 0)
                        {
                            CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], true);
                            CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], true);
                            CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"],
                                Util.ToInt(rec.Fields["SHOHIZEI_HENKO"].Value) == 1);
                            CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], false);
                        }
                        else
                        {
                            CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], true);
                            CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], true);
                            CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], false);
                            CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"],
                                Util.ToInt(rec.Fields["SHOHIZEI_HENKO"].Value) == 1);
                        }
                        //// 工事・工種編集可否
                        //if (Util.ToInt(_drSuitoKanjo["KOJI_UMU"]) == 1
                        //    || Util.ToInt(rec.Fields["KOJI_UMU"].Value) == 1)
                        //{
                        //    CFieldEnabled(rec.Fields["KOJI_CD"], true);
                        //    CFieldEnabled(rec.Fields["KOSHU_CD"], true);
                        //}
                        //else
                        //{
                        //    CFieldEnabled(rec.Fields["KOJI_CD"], false);
                        //    CFieldEnabled(rec.Fields["KOSHU_CD"], false);
                        //}
                    }

                    // 自動仕訳作成区分
                    rec.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value = autoDataFlg;

                    // 設定がチェックしない場合は無条件にチェックしないへ
                    if (_autoDataSetting == 0)
                        rec.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value = 0;

                    if (Util.ToInt(Util.ToString(rec.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
                    {
                        rec.Fields["EDIT_MODE"].Value = EditMode.None;

                        if (Util.ToString(rec.Fields["TEKIYO"].Value) == "＜諸口＞")
                            rec.Fields["TEKIYO"].Value = "＜諸口＞＊＊自動仕訳 ";
                        else
                            rec.Fields["TEKIYO"].Value = "＊＊自動仕訳 ";

                        CFieldEnabled(rec.Fields["DENPYO_DATE_G"], false);
                        CFieldEnabled(rec.Fields["DENPYO_DATE_E"], false);
                        CFieldEnabled(rec.Fields["DENPYO_DATE_M"], false);
                        CFieldEnabled(rec.Fields["DENPYO_DATE_D"], false);
                        CFieldEnabled(rec.Fields["TEKIYO_CD"], false);
                        CFieldEnabled(rec.Fields["TEKIYO"], false);
                        CFieldEnabled(rec.Fields["SHOHYO_BANGO"], false);
                        CFieldEnabled(rec.Fields["KANJO_KAMOKU_CD"], false);
                        CFieldEnabled(rec.Fields["HOJO_KAMOKU_CD"], false);
                        CFieldEnabled(rec.Fields["BUMON_CD"], false);
                        CFieldEnabled(rec.Fields["JIGYO_KUBUN"], false);
                        CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], false);
                        CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], false);
                        CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], false);
                        CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], false);
                    }
                }
            }
            /// **************************************************
            /// UTableレンダリングブロック終了
            /// **************************************************
            mtbList.Render();

            AddBlankRecord();                       // 新規行を追加

            msgFrm.Close();

            mtbList.Content.LastAddedRecord.Fields["DENPYO_DATE_D"].Focus();    // ブランク行へ移動

            CalcTotal();                            // 合計表示
        }

        /// <summary>
        /// データの読込（複合仕訳入力からの戻り）
        /// </summary>
        /// <param name="no">行番号</param>
        /// <param name="dt">表示用DataTable</param>
        private void DataLoadFukugo(decimal no, DataTable dt)
        {

            using (mtbList.RenderBlock())
            {
                int autoDataFlg = 0;                        // 自動仕訳作成区分
                                                            // UTableレコード
                UTable.CRecord rec;
                rec = this._activeCRecord;
                // 仕訳内容により編集明細をセット
                decimal maxGyo = 0;                     // 保存済行数
                decimal drDenpyoKingaku = 0;            // 借方伝票金額
                decimal drShohizeiRitsu = 0;            // 借方消費税率
                decimal drShohizeiKingaku = 0;          // 借方消費税額
                decimal crDenpyoKingaku = 0;            // 貸方伝票金額
                decimal crShohizeiKingaku = 0;          // 貸方消費税額
                decimal crShohizeiRitsu = 0;            // 貸方消費税率

                decimal lineCount = 0;                  // 保存済行数
                decimal denpyoBango = 0;                // 伝票番号
                // 以下を貸借の判断で設定して判断
                decimal drKzeiCount = 0;                // 借方課税区分
                decimal crKzeiCount = 0;                // 貸方課税区分

                #region 表示
                foreach (DataRow dr in dt.Rows)
                {
                    maxGyo = Util.ToDecimal(Util.ToString(dr["GYO_BANGO"]));

                    // 貸借レコード共通
                    if (rec.Fields["NO"].Value == null || lineCount == 0)
                    {
                        rec.Fields["NO"].Value = Util.ToString(no);
                        rec.Fields["DENPYO_BANGO"].Value = Util.ToString(dr["DENPYO_BANGO"]);
                        denpyoBango = Util.ToDecimal(Util.ToString(dr["DENPYO_BANGO"]));

                        SetJp(rec, Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba));

                        rec.Fields["TEKIYO_CD"].Value = ZaUtil.FormatNum(dr["TEKIYO_CD"]);
                        rec.Fields["TEKIYO"].Value = Util.ToString(dr["TEKIYO"]);
                        rec.Fields["SHOHYO_BANGO"].Value = Util.ToString(dr["SHOHYO_BANGO"]);
                        rec.Fields["KESSAN_KUBUN"].Value = Util.ToString(dr["KESSAN_KUBUN"]);
                        rec.Fields["JURNAL_TYPE"].Value = JurnalType.Single;

                        // 伝票番号があればEdit、無ければAddを設定
                        if (denpyoBango == 0)
                            rec.Fields["EDIT_MODE"].Value = EditMode.Add;  // 新規で振替入力でデータ作成時
                        else
                            rec.Fields["EDIT_MODE"].Value = EditMode.Edit; // 既存読み込みを振替入力でデータ作成時

                        // 複合仕訳での作成区分（更新処理用）
                        rec.Fields["FUKUGO_SHIWAKE_KUBUN"].Value = 1;
                    }
                    lineCount++;

                    // 貸借レコード別
                    if (Util.ToDecimal(Util.ToString(dr["KANJO_KAMOKU_CD"])) == Util.ToDecimal(this.txtKanjoKamokuCd.Text)
                         && Util.ToDecimal(Util.ToString(dr["HOJO_KAMOKU_CD"])) == Util.ToDecimal(this.txtHojoKamokuCd.Text)
                         && Util.ToDecimal(Util.ToString(dr["BUMON_CD"])) == Util.ToDecimal(this.txtBumonCd.Text))
                    {
                        // 出納勘定レコード情報
                        if (Util.ToInt(Util.ToString(dr["TAISHAKU_KUBUN"])) == 1)
                        {
                            // 借方金額集計
                            drDenpyoKingaku += ZaUtil.GetDenpyoKingaku(
                                                    Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                    Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])), this.UInfo);
                        }
                        else
                        {
                            // 貸方金額集計
                            crDenpyoKingaku += ZaUtil.GetDenpyoKingaku(
                                                    Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                    Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])), this.UInfo);
                        }
                        //if (Util.ToDecimal(Util.ToString(dr["KAZEI_KUBUN"])) != 0)
                        //{
                        //    drKzeiCount = 1;
                        //    crKzeiCount = 1;
                        //}
                        //// 工事・工種情報
                        //if (Util.ToInt(dr["KOJI_UMU"]) == 1)
                        //{
                        //    rec.Fields["KOJI_CD"].Value = ZaUtil.FormatNum(dr["KOJI_CD"]);
                        //    rec.Fields["KOJI_NM"].Value = Util.ToString(dr["KOJI_NM"]);
                        //    rec.Fields["KOSHU_CD"].Value = ZaUtil.FormatNum(dr["KOSHU_CD"]);
                        //    rec.Fields["KOSHU_NM"].Value = Util.ToString(dr["KOSHU_NM"]);
                        //}
                    }
                    else
                    {
                        // 相手科目レコードから明細情報をセット
                        rec.Fields["KANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KANJO_KAMOKU_CD"]);
                        rec.Fields["KANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                        rec.Fields["HOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["HOJO_KAMOKU_CD"]);
                        rec.Fields["HOJO_KAMOKU_NM"].Value = Util.ToString(dr["HOJO_KAMOKU_NM"]);
                        rec.Fields["BUMON_CD"].Value = ZaUtil.FormatNum(dr["BUMON_CD"]);
                        rec.Fields["JIGYO_KUBUN"].Value = ZaUtil.FormatNum(dr["JIGYO_KUBUN"]);
                        rec.Fields["ZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["ZEI_KUBUN"]);
                        // 消費税額は相手レコードから取得
                        // (するので保存された自レコード貸借区分とは反対へセットする)
                        if (Util.ToInt(Util.ToString(dr["TAISHAKU_KUBUN"])) == 1)
                        {
                            crShohizeiRitsu = Util.ToDecimal(Util.ToString(dr["ZEI_RITSU"]));
                            //crShohizeiKingaku = Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"]));
                            crShohizeiKingaku += Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"]));
                        }
                        else
                        {
                            drShohizeiRitsu = Util.ToDecimal(Util.ToString(dr["ZEI_RITSU"]));
                            //drShohizeiKingaku = Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"]));
                            drShohizeiKingaku += Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"]));
                        }
                        // 非表示フィールド
                        //rec.Fields["EDIT_MODE"].Value = EditMode.None; // ココは行の状態を設定
                        // 伝票番号があればEdit、無ければAddを設定
                        if (denpyoBango == 0)
                            rec.Fields["EDIT_MODE"].Value = EditMode.Add;  // 新規で振替入力でデータ作成時
                        else
                            rec.Fields["EDIT_MODE"].Value = EditMode.Edit; // 既存読み込みを振替入力でデータ作成時

                        //rec.Fields["KOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                        rec.Fields["KAZEI_KUBUN"].Value = Util.ToString(dr["KAZEI_KUBUN"]);
                        rec.Fields["SHOHIZEI_HENKO"].Value = Util.ToString(dr["SHOHIZEI_HENKO"]);
                        //// 工事・工種情報
                        //if (Util.ToInt(dr["KOJI_UMU"]) == 1)
                        //{
                        //    rec.Fields["KOJI_CD"].Value = ZaUtil.FormatNum(dr["KOJI_CD"]);
                        //    rec.Fields["KOJI_NM"].Value = Util.ToString(dr["KOJI_NM"]);
                        //    rec.Fields["KOSHU_CD"].Value = ZaUtil.FormatNum(dr["KOSHU_CD"]);
                        //    rec.Fields["KOSHU_NM"].Value = Util.ToString(dr["KOSHU_NM"]);
                        //}
                        // 編集可否
                        CFieldEnabled(rec.Fields["HOJO_KAMOKU_CD"], Util.ToDecimal(Util.ToString(dr["HOJO_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["BUMON_CD"], Util.ToDecimal(Util.ToString(dr["BUMON_UMU"])) == 1);
                    }

                    // 明細の件数
                    if (Util.ToInt(Util.ToString(dr["TAISHAKU_KUBUN"])) == 1)
                        drKzeiCount += 1;
                    else
                        crKzeiCount += 1;

                    autoDataFlg = Util.ToInt(Util.ToString(dr["SHIWAKE_SAKUSEI_KUBUN"]));
                }
                // 伝票金額・消費税
                rec.Fields["DrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(drDenpyoKingaku);
                rec.Fields["CrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(crDenpyoKingaku);
                rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(drShohizeiRitsu);
                rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(crShohizeiRitsu);
                rec.Fields["DrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(drShohizeiKingaku);
                rec.Fields["CrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(crShohizeiKingaku);

                // 伝票金額、消費税がゼロ以下は文字色を赤にする
                if (drDenpyoKingaku >= 0)
                {
                    rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                    rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                }
                else
                {
                    rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                    rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                }
                if (crDenpyoKingaku >= 0)
                {
                    rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                    rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                }
                else
                {
                    rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                    rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                }

                // 諸口仕訳判定
                //if (maxGyo > 1 || drDenpyoKingaku == crDenpyoKingaku ||
                //    (drKzeiCount >= 1 && crKzeiCount >= 1))
                if (maxGyo > 1 || drDenpyoKingaku == crDenpyoKingaku ||
                    ((drKzeiCount + crKzeiCount) > 2))
                {
                    // 諸口レコードをセット
                    rec.Fields["TEKIYO_CD"].Value = "";
                    rec.Fields["TEKIYO"].Value = "＜諸口＞";
                    rec.Fields["KANJO_KAMOKU_CD"].Value = "";
                    rec.Fields["KANJO_KAMOKU_NM"].Value = "";
                    rec.Fields["HOJO_KAMOKU_CD"].Value = "";
                    rec.Fields["HOJO_KAMOKU_NM"].Value = "";
                    rec.Fields["BUMON_CD"].Value = "";
                    rec.Fields["JIGYO_KUBUN"].Value = "";
                    rec.Fields["DrZEI_RITSU"].Value = "";
                    rec.Fields["CrZEI_RITSU"].Value = "";
                    // 非表示フィールド
                    rec.Fields["JURNAL_TYPE"].Value = JurnalType.Compound;

                    if (denpyoBango == 0)
                        rec.Fields["EDIT_MODE"].Value = EditMode.Add;  // 新規で振替入力でデータ作成時
                    else
                        rec.Fields["EDIT_MODE"].Value = EditMode.Edit; // 既存読み込みを振替入力でデータ作成時

                    // 複合仕訳での作成区分（更新処理用）
                    rec.Fields["FUKUGO_SHIWAKE_KUBUN"].Value = 1;

                    //rec.Fields["KOJI_UMU"].Value = "";
                    rec.Fields["KAZEI_KUBUN"].Value = "";
                    rec.Fields["SHOHIZEI_HENKO"].Value = "";
                    rec.Fields["ZEI_KUBUN"].Value = "";
                    rec.Fields["KESSAN_KUBUN"].Value = "";
                    // 編集可否
                    CFieldEnabled(rec.Fields["TEKIYO_CD"], false);
                    CFieldEnabled(rec.Fields["TEKIYO"], false);
                    CFieldEnabled(rec.Fields["KANJO_KAMOKU_CD"], false);
                    CFieldEnabled(rec.Fields["HOJO_KAMOKU_CD"], false);
                    CFieldEnabled(rec.Fields["BUMON_CD"], false);
                    CFieldEnabled(rec.Fields["JIGYO_KUBUN"], false);
                    CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], false);
                    CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], false);
                    CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], false);
                    CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], false);
                }
                else
                {
                    // 伝票金額・消費税編集可否
                    if (drDenpyoKingaku > 0)
                    {
                        CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], true);
                        CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], true);
                        CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"],
                            Util.ToInt(rec.Fields["SHOHIZEI_HENKO"].Value) == 1);
                        CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], false);
                    }
                    else
                    {
                        CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], true);
                        CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], true);
                        CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], false);
                        CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"],
                            Util.ToInt(rec.Fields["SHOHIZEI_HENKO"].Value) == 1);
                    }
                    //// 工事・工種編集可否
                    //if (Util.ToInt(_drSuitoKanjo["KOJI_UMU"]) == 1
                    //    || Util.ToInt(rec.Fields["KOJI_UMU"].Value) == 1)
                    //{
                    //    CFieldEnabled(rec.Fields["KOJI_CD"], true);
                    //    CFieldEnabled(rec.Fields["KOSHU_CD"], true);
                    //}
                    //else
                    //{
                    //    CFieldEnabled(rec.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(rec.Fields["KOSHU_CD"], false);
                    //}
                }

                // 自動仕訳作成区分
                rec.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value = autoDataFlg;
                // makishi（設定がチェックしない場合は無条件にチェックしないへ）
                if (_autoDataSetting == 0)
                    rec.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value = 0;
                if (Util.ToInt(Util.ToString(rec.Fields["SHIWAKE_SAKUSEI_KUBUN"].Value)) != 0)
                {
                    rec.Fields["EDIT_MODE"].Value = EditMode.None;
                    if (Util.ToString(rec.Fields["TEKIYO"].Value) == "＜諸口＞")
                        rec.Fields["TEKIYO"].Value = "＜諸口＞＊＊自動仕訳 ";
                    else
                        rec.Fields["TEKIYO"].Value = "＊＊自動仕訳 ";
                    CFieldEnabled(rec.Fields["DENPYO_DATE_G"], false);
                    CFieldEnabled(rec.Fields["DENPYO_DATE_E"], false);
                    CFieldEnabled(rec.Fields["DENPYO_DATE_M"], false);
                    CFieldEnabled(rec.Fields["DENPYO_DATE_D"], false);
                    CFieldEnabled(rec.Fields["TEKIYO_CD"], false);
                    CFieldEnabled(rec.Fields["TEKIYO"], false);
                    CFieldEnabled(rec.Fields["SHOHYO_BANGO"], false);
                    CFieldEnabled(rec.Fields["KANJO_KAMOKU_CD"], false);
                    CFieldEnabled(rec.Fields["HOJO_KAMOKU_CD"], false);
                    CFieldEnabled(rec.Fields["BUMON_CD"], false);
                    CFieldEnabled(rec.Fields["JIGYO_KUBUN"], false);
                    CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], false);
                    CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], false);
                    CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], false);
                    CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], false);
                }
                #endregion

                /// **************************************************
                /// UTableレンダリングブロック終了
                /// **************************************************
                mtbList.Render();

                if (no == mtbList.Content.Records.Count)
                {
                    // 最終行の場合に実施
                    AddBlankRecord();                       // 新規行を追加
                    mtbList.Content.LastAddedRecord.Fields["DENPYO_DATE_D"].Focus();    // ブランク行へ移動
                }

                CalcTotal();                            // 合計表示
            }
        }

        /// <summary>
        /// 諸口伝票の判定
        /// </summary>
        /// <param name="dt">判断を行うDataTable</param>
        /// <returns></returns>
        private bool IsCompound(DataTable dt)
        {
            decimal maxGyo = 0;                     // 保存済行数
            decimal drDenpyoKingaku = 0;            // 借方伝票金額
            decimal crDenpyoKingaku = 0;            // 貸方伝票金額
            decimal drKzeiCount = 0;                // 借方課税区分
            decimal crKzeiCount = 0;                // 貸方課税区分
            foreach (DataRow dr in dt.Rows)
            {
                maxGyo = Util.ToDecimal(Util.ToString(dr["GYO_BANGO"]));
                // 貸借レコード別
                if (Util.ToDecimal(Util.ToString(dr["KANJO_KAMOKU_CD"])) == Util.ToDecimal(this.txtKanjoKamokuCd.Text)
                     && Util.ToDecimal(Util.ToString(dr["HOJO_KAMOKU_CD"])) == Util.ToDecimal(this.txtHojoKamokuCd.Text)
                     && Util.ToDecimal(Util.ToString(dr["BUMON_CD"])) == Util.ToDecimal(this.txtBumonCd.Text))
                {
                    // 出納勘定レコード情報
                    if (Util.ToInt(Util.ToString(dr["TAISHAKU_KUBUN"])) == 1)
                    {
                        // 借方金額集計
                        drDenpyoKingaku += ZaUtil.GetDenpyoKingaku(
                                                Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])), this.UInfo);
                    }
                    else
                    {
                        // 貸方金額集計
                        crDenpyoKingaku += ZaUtil.GetDenpyoKingaku(
                                                Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])), this.UInfo);
                        //crKzeiCount += Util.ToDecimal(Util.ToString(dr["KAZEI_KUBUN"]));
                    }
                    //if (Util.ToDecimal(Util.ToString(dr["KAZEI_KUBUN"])) != 0)
                    //{
                    //    drKzeiCount = 1;
                    //    crKzeiCount = 1;
                    //}
                }
                // 明細の件数
                if (Util.ToInt(Util.ToString(dr["TAISHAKU_KUBUN"])) == 1)
                    drKzeiCount += 1;
                else
                    crKzeiCount += 1;
            }

            // 諸口仕訳判定
            //if (maxGyo > 1 || drDenpyoKingaku == crDenpyoKingaku ||
            //    (drKzeiCount >= 1 && crKzeiCount >= 1))
            if (maxGyo > 1 || drDenpyoKingaku == crDenpyoKingaku ||
                ((drKzeiCount + crKzeiCount) > 2))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 合計エリアの計算
        /// </summary>
        private void CalcTotal()
        {
            decimal DrGokei = 0;
            decimal CrGokei = 0;
            decimal zandaka = Util.ToDecimal(Util.ToString(this.txtZandaka.Text));

            using (mtbList.RenderBlock())
            {
                // 貸借金額集計
                for (int i = 0; i < mtbList.Content.Records.Count; i++)
                {
                    UTable.CRecord rec = mtbList.Content.Records[i];

                    if (rec.Visible)        // 明細削除レコードは対象外
                    {
                        // 各行残高
                        if (Util.ToInt(Util.ToString(_drSuitoKanjo["TAISHAKU_KUBUN"])) == 1)
                        {
                            zandaka = zandaka
                                + Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value))
                                - Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value));
                        }
                        else
                        {
                            zandaka = zandaka
                                + Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value))
                                - Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value));
                        }

                        // 合計
                        DrGokei += Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value));
                        CrGokei += Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value));

                        // UTable残高フィールド更新
                        rec.Fields["ZANDAKA"].Value = ZaUtil.FormatCur(zandaka);
                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (Util.ToDecimal(Util.ToString(rec.Fields["ZANDAKA"].Value)) >= 0)
                        {
                            rec.Fields["ZANDAKA"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            rec.Fields["ZANDAKA"].Setting.ForeColor = Color.Red;
                        }
                    }
                }
            }

            // 合計情報
            lblNyukin.Text = ZaUtil.FormatCur(DrGokei);
            lblShukkin.Text = ZaUtil.FormatCur(CrGokei);
            lblGokeiZandaka.Text = ZaUtil.FormatCur(zandaka);
            // 伝票金額、消費税がゼロ以下は文字色を赤にする
            if (DrGokei >= 0)
                lblNyukin.ForeColor = Color.Black;
            else
                lblNyukin.ForeColor = Color.Red;
            if (CrGokei >= 0)
                lblShukkin.ForeColor = Color.Black;
            else
                lblShukkin.ForeColor = Color.Red;
            if (zandaka >= 0)
                lblGokeiZandaka.ForeColor = Color.Black;
            else
                lblGokeiZandaka.ForeColor = Color.Red;
        }

        /// <summary>
        /// 仕訳事例登録用データテーブル作成
        /// </summary>
        private DataTable GetDtJirei()
        {
            DataTable dt = new DataTable();

            // データテーブル名
            dt.TableName = "TABLE1";
            // フィールド定義
            dt.Columns.Add("GYO_BANGO");
            dt.Columns.Add("TAISHAKU_KUBUN");
            dt.Columns.Add("MEISAI_KUBUN");
            dt.Columns.Add("KANJO_KAMOKU_CD");
            dt.Columns.Add("HOJO_KAMOKU_CD");
            dt.Columns.Add("BUMON_CD");
            dt.Columns.Add("TEKIYO");
            dt.Columns.Add("ZEIKOMI_KINGAKU");
            dt.Columns.Add("ZEINUKI_KINGAKU");
            dt.Columns.Add("SHOHIZEI_KINGAKU");
            dt.Columns.Add("ZEI_KUBUN");
            dt.Columns.Add("KAZEI_KUBUN");
            dt.Columns.Add("TORIHIKI_KUBUN");
            dt.Columns.Add("ZEI_RITSU");
            dt.Columns.Add("JIGYO_KUBUN");
            dt.Columns.Add("SHOHIZEI_NYURYOKU_HOHO");
            dt.Columns.Add("SHOHIZEI_HENKO");
            dt.Columns.Add("KESSAN_KUBUN");

            DataRow drZei;              // 税区分レコード
            decimal denpyoKingaku;      // 伝票金額セット用
            decimal shohizeiKingaku;    // 消費税額セット用

            UTable.CRecord rec = _activeCRecord;

            // 出納勘定レコード
            if (!ValChk.IsEmpty(this.txtKanjoKamokuCd.Text))
            {
                DataRow dr = dt.NewRow();

                dr["GYO_BANGO"] = 1;
                dr["TAISHAKU_KUBUN"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)) > 0 ? 2 : 1;
                dr["MEISAI_KUBUN"] = "0";
                dr["KANJO_KAMOKU_CD"] = this.txtKanjoKamokuCd.Text;
                dr["HOJO_KAMOKU_CD"] = ZaUtil.ToDecimal(this.txtHojoKamokuCd.Text);
                dr["BUMON_CD"] = ZaUtil.ToDecimal(this.txtBumonCd.Text);

                // 伝票金額・税区分情報
                //if (ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value) > 0)
                if (ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value) != 0)
                {
                    denpyoKingaku = ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value);
                    drZei = GetZeiKubunDataRow(Util.ToString(_drSuitoKanjo["KASHIKATA_ZEI_KUBUN"]));
                }
                else
                {
                    denpyoKingaku = ZaUtil.ToDecimal(rec.Fields["DrDENPYO_KINGAKU"].Value);
                    drZei = GetZeiKubunDataRow(Util.ToString(_drSuitoKanjo["KARIKATA_ZEI_KUBUN"]));
                }
                // 出納勘定消費税額(税区分から計算する)
                // 保持された税率で計算
                //shohizeiKingaku =
                //    ZaUtil.CalcConsumptionTax(Util.ToInt(drZei["ZEI_KUBUN"]), denpyoKingaku, this.UInfo, this.Dba);
                shohizeiKingaku =
                    ZaUtil.CalcConsumptionTax(Util.ToDecimal(Util.ToString(rec.Fields["DrZEI_RITSU"].Value).Replace("%", "")), denpyoKingaku, this.UInfo, this.Dba);

                dr["ZEI_KUBUN"] = drZei["ZEI_KUBUN"];
                dr["JIGYO_KUBUN"] = this.UInfo.KaikeiSettings["JIGYO_KUBUN"];
                dr["TEKIYO"] = rec.Fields["TEKIYO"].Value;
                dr["ZEIKOMI_KINGAKU"] = ZaUtil.GetZeikomiKingaku(denpyoKingaku, shohizeiKingaku, this.UInfo);
                dr["ZEINUKI_KINGAKU"] = ZaUtil.GetZeinukiKingaku(denpyoKingaku, shohizeiKingaku, this.UInfo);
                dr["SHOHIZEI_KINGAKU"] = shohizeiKingaku;
                dr["KAZEI_KUBUN"] = drZei["KAZEI_KUBUN"];
                dr["TORIHIKI_KUBUN"] = drZei["TORIHIKI_KUBUN"];

                // 保持された税率
                //dr["ZEI_RITSU"] = drZei["ZEI_RITSU"];
                dr["ZEI_RITSU"] = Util.ToString(rec.Fields["DrZEI_RITSU"].Value).Replace("%", "");

                dr["SHOHIZEI_NYURYOKU_HOHO"] = this.UInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
                dr["SHOHIZEI_HENKO"] = "0";
                dr["KESSAN_KUBUN"] = 0;
                dt.Rows.Add(dr);
            }

            // 相手勘定レコード
            if (!ValChk.IsEmpty(rec.Fields["KANJO_KAMOKU_CD"].Value))
            {
                DataRow dr = dt.NewRow();

                dr["GYO_BANGO"] = 1;
                dr["TAISHAKU_KUBUN"] = ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value) > 0 ? 1 : 2;
                dr["MEISAI_KUBUN"] = "0";
                dr["KANJO_KAMOKU_CD"] = rec.Fields["KANJO_KAMOKU_CD"].Value;
                dr["HOJO_KAMOKU_CD"] = ZaUtil.ToDecimal(rec.Fields["HOJO_KAMOKU_CD"].Value);
                dr["BUMON_CD"] = ZaUtil.ToDecimal(rec.Fields["BUMON_CD"].Value);

                // 伝票金額・消費税額
                //if (ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value) > 0)
                if (ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value) != 0)
                {
                    denpyoKingaku = ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value);
                    shohizeiKingaku = ZaUtil.ToDecimal(rec.Fields["CrSHOHIZEI_KINGAKU"].Value);
                }
                else
                {
                    denpyoKingaku = ZaUtil.ToDecimal(rec.Fields["DrDENPYO_KINGAKU"].Value);
                    shohizeiKingaku = ZaUtil.ToDecimal(rec.Fields["DrSHOHIZEI_KINGAKU"].Value);
                }
                // 税区分情報
                drZei = GetZeiKubunDataRow(Util.ToString(rec.Fields["ZEI_KUBUN"].Value));

                // 借方レコードをセット
                dr["ZEI_KUBUN"] = rec.Fields["ZEI_KUBUN"];
                dr["JIGYO_KUBUN"] = rec.Fields["JIGYO_KUBUN"];
                dr["TEKIYO"] = rec.Fields["TEKIYO"].Value;
                dr["ZEIKOMI_KINGAKU"] = ZaUtil.GetZeikomiKingaku(denpyoKingaku, shohizeiKingaku, this.UInfo);
                dr["ZEINUKI_KINGAKU"] = ZaUtil.GetZeinukiKingaku(denpyoKingaku, shohizeiKingaku, this.UInfo);
                dr["SHOHIZEI_KINGAKU"] = shohizeiKingaku;
                dr["KAZEI_KUBUN"] = drZei["KAZEI_KUBUN"];
                dr["TORIHIKI_KUBUN"] = drZei["TORIHIKI_KUBUN"];

                // 保持された税率
                //dr["ZEI_RITSU"] = drZei["ZEI_RITSU"];
                dr["ZEI_RITSU"] = Util.ToString(rec.Fields["CrZEI_RITSU"]).Replace("%", "");

                dr["SHOHIZEI_NYURYOKU_HOHO"] = this.UInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
                dr["SHOHIZEI_HENKO"] = Util.ToString(rec.Fields["SHOHIZEI_HENKO"].Value);
                dr["KESSAN_KUBUN"] = 0;
                dt.Rows.Add(dr);
            }

            return dt;
        }

        /// <summary>
        /// 勘定科目レコードを取得
        /// </summary>
        /// <param name="kamokuCd">科目コード</param>
        /// <returns>科目レコード又はnull</returns>
        private DataRow GetKanjoKamokuDataRow(string kamokuCd)
        {
            DataRow ret = null;
            DataRow[] foundRows;

            foundRows = _dtKanjoKamoku.Select("KANJO_KAMOKU_CD = '" + kamokuCd + "'");
            if (foundRows.Length > 0)
            {
                ret = foundRows[0];
            }

            return ret;
        }

        /// <summary>
        /// 税区分レコードを取得
        /// </summary>
        /// <param name="zeiKubun">税区分</param>
        /// <returns>税区分レコード又はnull</returns>
        private DataRow GetZeiKubunDataRow(string zeiKubun)
        {
            DataRow ret = null;
            DataRow[] foundRows;

            foundRows = _dtZeiKubun.Select("ZEI_KUBUN = '" + zeiKubun + "'");
            if (foundRows.Length > 0)
            {
                ret = foundRows[0];
            }

            return ret;
        }

        /// <summary>
        /// 部門レコードを取得
        /// </summary>
        /// <param name="bumonCd">部門コード</param>
        /// <returns>部門レコード又はnull</returns>
        private DataRow GetBumonDataRow(string bumonCd)
        {
            DataRow ret = null;
            DataRow[] foundRows;

            foundRows = _dtBumon.Select("BUMON_CD = '" + bumonCd + "'");
            if (foundRows.Length > 0)
            {
                ret = foundRows[0];
            }

            return ret;
        }

        /// <summary>
        /// 指定科目・日付の繰越残高計算
        /// </summary>
        private decimal GetZandaka(string kanjoKamokuCd, string hojoKamokuCd, DateTime date)
        {
            decimal ret = 0;

            // 仕訳明細の金額集計
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, Util.ToDecimal(this.txtHojoKamokuCd.Text));
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtBumonCd.Text));
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, date);
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" SUM(");
            sql.Append("  CASE WHEN TAISHAKU_KUBUN = 1 THEN ZEIKOMI_KINGAKU");
            sql.Append("  ELSE 0 END");
            sql.Append(" ) AS DrKINGAKU");
            sql.Append(",SUM(");
            sql.Append("  CASE WHEN TAISHAKU_KUBUN <> 1 THEN ZEIKOMI_KINGAKU");
            sql.Append("  ELSE 0 END");
            sql.Append(" ) AS CrKINGAKU");
            sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHISHO_CD = @SHISHO_CD");
            sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" AND MEISAI_KUBUN = 0");
            sql.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            sql.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
            sql.Append(" AND BUMON_CD = @BUMON_CD");
            sql.Append(" AND DENPYO_DATE < @DENPYO_DATE");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dt.Rows.Count > 0)
            {
                // 出納勘定の貸借区分別に残高計算
                if (Util.ToInt(Util.ToString(_drSuitoKanjo["TAISHAKU_KUBUN"])) == 1)
                {
                    ret = Util.ToDecimal(Util.ToString(dt.Rows[0]["DrKINGAKU"])) - Util.ToDecimal(Util.ToString(dt.Rows[0]["CrKINGAKU"]));
                }
                else
                {
                    ret = Util.ToDecimal(Util.ToString(dt.Rows[0]["CrKINGAKU"])) - Util.ToDecimal(Util.ToString(dt.Rows[0]["DrKINGAKU"]));
                }
            }

            return ret;
        }

        /// <summary>
        /// 税区分の選択
        /// </summary>
        /// <param name="denpyoDate">伝票日付</param>
        /// <param name="required">必須判定</param>
        /// <param name="field"></param>
        /// <returns></returns>
        private string SelectZeiKubun(DateTime denpyoDate, bool required, UTable.CField field)
        {
            string ret = "";
            bool selected = false;

            string pre = string.Empty;
            if (Util.ToString(field.Key).Substring(0, 2) == "Dr") pre = "Dr";
            if (Util.ToString(field.Key).Substring(0, 2) == "Cr") pre = "Cr";

            // アセンブリのロード
            //Assembly asm = Assembly.LoadFrom("ZMCM1061.exe");
            Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1061.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.zm.zmcm1061.ZMCM1065");

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    // タブの一部として埋め込む
                    BasePgForm frm = (BasePgForm)obj;
                    frm.InData = Util.ToString(field.Record.Fields["ZEI_KUBUN"].Value);

                    // 検索ダイアログを表示する
                    while (!selected)
                    {
                        frm.ShowDialog(this);

                        if (frm.DialogResult == DialogResult.OK)
                        {
                            string[] result = (string[])frm.OutData;

                            // 選択結果を基準日適合チェック
                            decimal taxRate = 0;
                            DialogResult ans = ConfPastZeiKbn(denpyoDate, result[0], ref taxRate, Util.ToDecimal(Util.ToString(result[2])));

                            if (ans == DialogResult.OK)
                            {
                                // 税区分を選択して終了
                                ret = result[0];
                                selected = true;
                                if (pre.Length != 0)
                                {
                                    // 税情報をセット
                                    field.Record.Fields["ZEI_KUBUN"].Value = result[0];
                                    field.Record.Fields[pre + "ZEI_RITSU"].Value = ZaUtil.FormatPercentage(Util.ToDecimal(Util.ToString(result[2])));
                                    field.Record.Fields["KAZEI_KUBUN"].Value = Util.ToString(Util.ToString(result[3]));
                                    // 消費税額の算出
                                    decimal zei = 0;
                                    if (!ValChk.IsEmpty(field.Record.Fields[pre + "DENPYO_KINGAKU"].Value))
                                    {
                                        zei = ZaUtil.CalcConsumptionTax(Util.ToDecimal(result[2]),
                                                Util.ToDecimal(Util.ToString(field.Record.Fields[pre + "DENPYO_KINGAKU"].Value)), this.UInfo, this.Dba);
                                    }
                                    if (zei != 0)
                                    {
                                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Value = Util.FormatNum(zei);
                                    }
                                    else
                                    {
                                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Value = "";
                                    }
                                    // 伝票金額、消費税がゼロ以下は文字色を赤にする
                                    if (zei >= 0)
                                    {
                                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                                    }
                                    else
                                    {
                                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                                    }
                                    //合計計算
                                    CalcTotal();
                                }
                            }
                        }
                        else
                        {
                            // 税区分を選択しないで終了
                            if (!required) selected = true;
                        }
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// 税率変更時の確認処理
        /// </summary>
        /// <param name="baseDate"></param>
        /// <param name="zeiKbn"></param>
        /// <param name="zeiritsu"></param>
        /// <param name="taxRate"></param>
        /// <returns></returns>
        private DialogResult ConfPastZeiKbn(DateTime baseDate, string zeiKbn, ref decimal zeiritsu, decimal taxRate)
        {
            zeiritsu = 0;

            // 概要：日付を元に、現在の税率と異なる税率を持つ税区分が選択されていれば確認メッセージを出す

            // 税区分を元にTB_ZM_F_ZEI_KUBUNから設定値を取得
            // ※万が一取得できなければNGを返す
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, zeiKbn);
            DataTable dtZeiKbn = this.Dba.GetDataTableByConditionWithParams(
                "*", "TB_ZM_F_ZEI_KUBUN", "ZEI_KUBUN = @ZEI_KUBUN", dpc);

            if (dtZeiKbn.Rows.Count == 0) return DialogResult.No;

            // 非課税であればダイアログを出さない
            //if (Util.ToInt(dtZeiKbn.Rows[0]["KAZEI_KUBUN"]) == 0) return DialogResult.OK;
            if (Util.ToInt(dtZeiKbn.Rows[0]["KAZEI_KUBUN"]) != 1) return DialogResult.OK;

            // 適用開始日が基準日より小さいレコードを摘要開始日の降順で取得し、
            // その1件目の新消費税を保持
            // 税区分から取得した税率と消費税情報テーブルから取得した税率を比較し、
            // 異なっていれば確認メッセージを表示
            decimal taxRateBase = GetZeiritsu(baseDate);
            //decimal taxRateJudge = Util.ToDecimal(dtZeiKbn.Rows[0]["ZEI_RITSU"]);
            decimal taxRateJudge = TaxUtil.GetTaxRate(baseDate, Util.ToInt(zeiKbn), this.Dba); // 新税率から取得
            // 税率指定時はそちらを優先
            if (taxRate != -1) taxRateJudge = taxRate;

            if (taxRateBase != taxRateJudge)
            {
                DialogResult result = Msg.ConfOKCancel("基準日時点の税率と異なる税率を持つ税区分が選択されています。"
                    + Environment.NewLine + "処理を続行してよろしいですか？");

                if (result == DialogResult.OK) zeiritsu = taxRateJudge;

                return result;
            }
            else
            {
                zeiritsu = taxRateJudge;
                return DialogResult.OK;
            }
        }
        private new DialogResult ConfPastZeiKbn(DateTime baseDate, string zeiKbn, ref decimal zeiritsu)
        {
            return ConfPastZeiKbn(baseDate, zeiKbn, ref zeiritsu, -1);
        }

        /// <summary>
        /// 複合仕訳画面へ渡すDataTable作成
        /// </summary>
        /// <returns>DataTable</returns>
        private DataTable GetDtSuito()
        {
            DataTable dt = new DataTable();

            // データテーブル名
            dt.TableName = "TABLE1";
            // フィールド定義
            dt.Columns.Add("KAISHA_CD");
            dt.Columns.Add("SHISHO_CD");
            dt.Columns.Add("DENPYO_DATE");
            dt.Columns.Add("DENPYO_BANGO");
            dt.Columns.Add("GYO_BANGO", typeof(decimal));
            dt.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
            dt.Columns.Add("MEISAI_KUBUN", typeof(decimal));
            dt.Columns.Add("DENPYO_KUBUN");
            dt.Columns.Add("SHOHYO_BANGO");
            dt.Columns.Add("TANTOSHA_CD");
            dt.Columns.Add("TANTOSHA_NM");
            dt.Columns.Add("KANJO_KAMOKU_CD");
            dt.Columns.Add("KANJO_KAMOKU_NM");
            dt.Columns.Add("HOJO_KAMOKU_CD");
            dt.Columns.Add("HOJO_KAMOKU_NM");
            dt.Columns.Add("BUMON_UMU");
            dt.Columns.Add("HOJO_UMU");
            dt.Columns.Add("BUMON_CD");
            dt.Columns.Add("BUMON_NM");
            dt.Columns.Add("TEKIYO_CD");
            dt.Columns.Add("TEKIYO");
            dt.Columns.Add("ZEIKOMI_KINGAKU");
            dt.Columns.Add("ZEINUKI_KINGAKU");
            dt.Columns.Add("SHOHIZEI_KINGAKU");
            dt.Columns.Add("ZEI_KUBUN");
            dt.Columns.Add("KAZEI_KUBUN");
            dt.Columns.Add("TORIHIKI_KUBUN");
            dt.Columns.Add("ZEI_RITSU");
            dt.Columns.Add("JIGYO_KUBUN");
            dt.Columns.Add("SHOHIZEI_NYURYOKU_HOHO");
            dt.Columns.Add("SHOHIZEI_HENKO");
            dt.Columns.Add("KESSAN_KUBUN");
            dt.Columns.Add("SHIWAKE_SAKUSEI_KUBUN");

            DataRow dr;

            UTable.CRecord rec = _activeCRecord;

            // 伝票番号
            decimal denpyoBango = ZaUtil.ToDecimal(rec.Fields["DENPYO_BANGO"].Value);

            // 表示用なので明細区分：０のみ

            // 出納勘定レコード
            dr = GetDtSuitoRecord(dt, true, denpyoBango, 0, DENPYO_KUBUN, rec);
            dt.Rows.Add(dr);

            //// 出納勘定消費税レコード
            //if (ZaUtil.ToDecimal(rec.Fields["DrDENPYO_KINGAKU"].Value) != 0)
            //{
            //    shohizeiKingaku = ZaUtil.CalcConsumptionTax(
            //        Util.ToDecimal(rec.Fields["DrZEI_RITSU"].Value),
            //        ZaUtil.ToDecimal(rec.Fields["DrDENPYO_KINGAKU"].Value), this.UInfo, this.Dba);
            //}
            //else
            //{
            //    shohizeiKingaku = ZaUtil.CalcConsumptionTax(
            //        Util.ToDecimal(rec.Fields["CrZEI_RITSU"].Value),
            //        ZaUtil.ToDecimal(rec.Fields["CrDENPYO_KINGAKU"].Value), this.UInfo, this.Dba);
            //}
            //if (shohizeiKingaku != 0)
            //{
            //    dr = GetDtSuitoRecord(dt,true, denpyoBango, 1, DENPYO_KUBUN, rec);
            //    dt.Rows.Add(dr);
            //}

            // 相手勘定レコード
            dr = GetDtSuitoRecord(dt, false, denpyoBango, 0, DENPYO_KUBUN, rec);
            dt.Rows.Add(dr);

            //// 相手勘定消費税レコード
            //if (ZaUtil.ToDecimal(rec.Fields["DrSHOHIZEI_KINGAKU"].Value) != 0
            //    || ZaUtil.ToDecimal(rec.Fields["CrSHOHIZEI_KINGAKU"].Value) != 0)
            //{
            //    dr = GetDtSuitoRecord(dt, false, denpyoBango, 1, DENPYO_KUBUN, rec);
            //    dt.Rows.Add(dr);
            //}

            DataView dv = new DataView(dt);
            dv.Sort = "GYO_BANGO ASC, TAISHAKU_KUBUN ASC, MEISAI_KUBUN ASC";

            return dv.ToTable();
        }

        /// <summary>
        /// 複合仕訳画面へ渡すDataRow作成
        /// </summary>
        /// <param name="dt">作成対象のDataTable</param>
        /// <param name="suitoFlag">出納勘定レコード/相手勘定レコード</param>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="meisaiKubun">明細区分</param>
        /// <param name="denpyoKubun">伝票区分</param>
        /// <param name="rec">UTableレコード</param>
        /// <returns>設定されたDataRow</returns>
        private DataRow GetDtSuitoRecord(DataTable dt, bool suitoFlag, decimal denpyoBango,
            decimal meisaiKubun, decimal denpyoKubun, UTable.CRecord rec)
        {
            // 処理内容はSetZmShiwakeMeisaiParams

            DataRow dr = dt.NewRow();

            // 貸借区分・伝票金額
            int taishakuKubun;
            decimal denpyoKingaku;
            if (Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)) != 0)
            {
                // 借方伝票金額が入力されている時
                taishakuKubun = suitoFlag ? 1 : 2;
                denpyoKingaku = Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value));
            }
            else
            {
                if (Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)) != 0)
                {
                    taishakuKubun = suitoFlag ? 2 : 1;
                    denpyoKingaku = Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value));
                }
                else
                {
                    // 両方に金額が入って居ない場合（新規行で複合仕訳へ遷移）
                    taishakuKubun = suitoFlag ? 1 : 2;
                    denpyoKingaku = Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value));
                }
            }
            // 出納勘定税区分情報
            DataRow drSuitoZei = GetZeiKubunDataRow(taishakuKubun == 1 ?
                Util.ToString(_drSuitoKanjo["KARIKATA_ZEI_KUBUN"]) : Util.ToString(_drSuitoKanjo["KASHIKATA_ZEI_KUBUN"]));
            // 出納勘定消費税額(税区分から計算する)

            DateTime denpyoDate = Util.ConvAdDate(
                Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), this.Dba);

            decimal suitoZeiRt =
                taishakuKubun == 1 ?
                TaxUtil.GetTaxRate(denpyoDate, Util.ToDecimal(Util.ToString(_drSuitoKanjo["KARIKATA_ZEI_KUBUN"])), this.Dba) : TaxUtil.GetTaxRate(denpyoDate, Util.ToDecimal(Util.ToString(_drSuitoKanjo["KASHIKATA_ZEI_KUBUN"])), this.Dba);
            decimal aiteZeiRt =
                taishakuKubun == 1 ?
                ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_RITSU"].Value).Replace("%", "")) : ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_RITSU"].Value).Replace("%", ""));
            decimal suitoShohizeiKingaku =
                ZaUtil.CalcConsumptionTax(suitoZeiRt, denpyoKingaku, this.UInfo, this.Dba);

            // 相手勘定税区分情報
            DataRow drAiteZei = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(rec.Fields["ZEI_KUBUN"].Value)));
            // 相手勘定消費税額
            decimal aiteShohizeiKingaku =
                taishakuKubun == 1 ?
                ZaUtil.ToDecimal(rec.Fields["CrSHOHIZEI_KINGAKU"].Value) : ZaUtil.ToDecimal(rec.Fields["DrSHOHIZEI_KINGAKU"].Value);

            dr["KAISHA_CD"] = this.UInfo.KaishaCd;
            dr["SHISHO_CD"] = this.ShishoCode;
            dr["DENPYO_DATE"] = denpyoDate;
            dr["DENPYO_BANGO"] = denpyoBango;
            dr["GYO_BANGO"] = 1;
            dr["TAISHAKU_KUBUN"] = taishakuKubun;
            dr["MEISAI_KUBUN"] = meisaiKubun;
            dr["DENPYO_KUBUN"] = denpyoKubun;
            dr["SHOHYO_BANGO"] = Util.ToString(rec.Fields["SHOHYO_BANGO"].Value);//

            dr["TANTOSHA_CD"] = this.txtTantoshaCd.Text;
            dr["TANTOSHA_NM"] = this.lblTantoshaNm.Text;

            DataRow r = null;

            /// 明細区分・出納勘定相手勘定レコード別処理
            if (meisaiKubun == 0 && suitoFlag)
            {
                /// 明細区分０・出納勘定レコード
                dr["KANJO_KAMOKU_CD"] = ZaUtil.ToDecimal(this.txtKanjoKamokuCd.Text);
                dr["KANJO_KAMOKU_NM"] = Util.ToString(this.lblKanjoKamokuNm.Text);
                dr["HOJO_KAMOKU_CD"] = ZaUtil.ToDecimal(this.txtHojoKamokuCd.Text);
                dr["HOJO_KAMOKU_NM"] = Util.ToString(this.lblHojoKamokuNm.Text);
                dr["BUMON_CD"] = this.txtBumonCd.Text;
                dr["BUMON_NM"] = this.lblBumonNm.Text;
                r = GetKanjoKamokuDataRow(Util.ToString(dr["KANJO_KAMOKU_CD"]));
                if (r != null)
                {
                    dr["BUMON_UMU"] = Util.ToString(r["BUMON_UMU"]);
                    dr["HOJO_UMU"] = Util.ToString(r["HOJO_KAMOKU_UMU"]);
                }
                dr["JIGYO_KUBUN"] = ZaUtil.ToDecimal(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);
                dr["ZEI_KUBUN"] = ZaUtil.ToDecimal(drSuitoZei["ZEI_KUBUN"]);
                dr["KAZEI_KUBUN"] = drSuitoZei["KAZEI_KUBUN"];
                dr["ZEI_RITSU"] = suitoZeiRt;
                dr["TORIHIKI_KUBUN"] = drSuitoZei["TORIHIKI_KUBUN"];
                dr["SHOHIZEI_HENKO"] = 0;
                // 税込・税抜金額・消費税額
                dr["ZEIKOMI_KINGAKU"] = ZaUtil.GetZeikomiKingaku(denpyoKingaku, suitoShohizeiKingaku, this.UInfo);
                dr["ZEINUKI_KINGAKU"] = ZaUtil.GetZeinukiKingaku(denpyoKingaku, suitoShohizeiKingaku, this.UInfo);
                dr["SHOHIZEI_KINGAKU"] = suitoShohizeiKingaku;
            }
            else if (meisaiKubun == 0 && !suitoFlag)
            {
                /// 明細区分０・相手勘定レコード
                dr["KANJO_KAMOKU_CD"] = ZaUtil.ToDecimal(rec.Fields["KANJO_KAMOKU_CD"].Value);
                dr["KANJO_KAMOKU_NM"] = Util.ToString(rec.Fields["KANJO_KAMOKU_NM"].Value);
                dr["HOJO_KAMOKU_CD"] = ZaUtil.ToDecimal(rec.Fields["HOJO_KAMOKU_CD"].Value);
                dr["HOJO_KAMOKU_NM"] = Util.ToString(rec.Fields["HOJO_KAMOKU_NM"].Value);
                dr["BUMON_CD"] = ZaUtil.ToDecimal(rec.Fields["BUMON_CD"].Value);
                r = GetBumonDataRow(Util.ToString(dr["BUMON_CD"]));
                if (r != null)
                    dr["BUMON_NM"] = Util.ToString(r["BUMON_NM"]);
                r = GetKanjoKamokuDataRow(Util.ToString(dr["KANJO_KAMOKU_CD"]));
                if (r != null)
                {
                    dr["BUMON_UMU"] = Util.ToString(r["BUMON_UMU"]);
                    dr["HOJO_UMU"] = Util.ToString(r["HOJO_KAMOKU_UMU"]);
                }
                dr["JIGYO_KUBUN"] = ZaUtil.ToDecimal(rec.Fields["JIGYO_KUBUN"].Value);
                dr["ZEI_KUBUN"] = ZaUtil.ToDecimal(rec.Fields["ZEI_KUBUN"].Value);
                dr["KAZEI_KUBUN"] = drAiteZei["KAZEI_KUBUN"];
                dr["ZEI_RITSU"] = aiteZeiRt;
                dr["TORIHIKI_KUBUN"] = drAiteZei["TORIHIKI_KUBUN"];
                dr["SHOHIZEI_HENKO"] = ZaUtil.ToDecimal(rec.Fields["SHOHIZEI_HENKO"].Value);
                /// 税込・税抜金額・消費税額
                dr["ZEIKOMI_KINGAKU"] = ZaUtil.GetZeikomiKingaku(denpyoKingaku, aiteShohizeiKingaku, this.UInfo);
                dr["ZEINUKI_KINGAKU"] = ZaUtil.GetZeinukiKingaku(denpyoKingaku, aiteShohizeiKingaku, this.UInfo);
                dr["SHOHIZEI_KINGAKU"] = aiteShohizeiKingaku;
            }
            if (meisaiKubun == 1 && suitoFlag)
            {
                /// 明細区分１・出納勘定レコード
                if (drSuitoZei != null)
                {
                    int triKb = Util.ToInt(drSuitoZei["TORIHIKI_KUBUN"]);
                    if (triKb >= 10 && triKb <= 19)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));
                    }
                    else if (triKb >= 20 && triKb <= 29)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
                    }
                    if (r != null)
                    {
                        dr["KANJO_KAMOKU_CD"] = Util.ToDecimal(Util.ToString(r["KANJO_KAMOKU_CD"]));
                        dr["KANJO_KAMOKU_NM"] = Util.ToString(r["KANJO_KAMOKU_NM"]);
                        dr["HOJO_KAMOKU_CD"] = 0;
                        dr["HOJO_KAMOKU_NM"] = "";
                        if (ZaUtil.ToDecimal(r["BUMON_UMU"]) == 0)
                            dr["BUMON_CD"] = ZaUtil.ToDecimal("0");
                        else
                            dr["BUMON_CD"] = ZaUtil.ToDecimal(this.txtBumonCd.Text);
                        dr["BUMON_NM"] = "";
                        if (r != null)
                        {
                            dr["BUMON_UMU"] = Util.ToString(r["BUMON_UMU"]);
                            dr["HOJO_UMU"] = Util.ToString(r["HOJO_KAMOKU_UMU"]);
                        }

                        if (taishakuKubun == 1)
                        {
                            dr["ZEI_KUBUN"] = r["KARIKATA_ZEI_KUBUN"];
                            dr["KAZEI_KUBUN"] = r["KARI_KAZEI_KUBUN"];
                            dr["TORIHIKI_KUBUN"] = r["KARI_TORIHIKI_KUBUN"];
                            dr["ZEI_RITSU"] = r["KARI_ZEI_RITSU"];
                        }
                        else
                        {
                            dr["ZEI_KUBUN"] = r["KASHIKATA_ZEI_KUBUN"];
                            dr["KAZEI_KUBUN"] = r["KASHI_KAZEI_KUBUN"];
                            dr["TORIHIKI_KUBUN"] = r["KASHI_TORIHIKI_KUBUN"];
                            dr["ZEI_RITSU"] = r["KASHI_ZEI_RITSU"];
                        }
                    }
                }
                dr["JIGYO_KUBUN"] = ZaUtil.ToDecimal(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);
                dr["SHOHIZEI_HENKO"] = 0;
                /// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                dr["ZEIKOMI_KINGAKU"] = 0;
                dr["ZEINUKI_KINGAKU"] = Util.ToDecimal(suitoShohizeiKingaku);
                dr["SHOHIZEI_KINGAKU"] = 0;
            }
            if (meisaiKubun == 1 && !suitoFlag)
            {
                /// 明細区分１・相手勘定レコード
                if (drAiteZei != null)
                {
                    int triKb = Util.ToInt(drAiteZei["TORIHIKI_KUBUN"]);
                    if (triKb >= 10 && triKb <= 19)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));
                    }
                    else if (triKb >= 20 && triKb <= 29)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
                    }
                    if (r != null)
                    {
                        dr["KANJO_KAMOKU_CD"] = Util.ToDecimal(Util.ToString(r["KANJO_KAMOKU_CD"]));
                        dr["KANJO_KAMOKU_NM"] = Util.ToString(r["KANJO_KAMOKU_NM"]);
                        dr["HOJO_KAMOKU_CD"] = 0;
                        dr["HOJO_KAMOKU_NM"] = "";
                        if (ZaUtil.ToDecimal(r["BUMON_UMU"]) == 0)
                            dr["BUMON_CD"] = ZaUtil.ToDecimal("0");
                        else
                            dr["BUMON_CD"] = ZaUtil.ToDecimal(rec.Fields["BUMON_CD"].Value);
                        dr["BUMON_NM"] = "";
                        if (r != null)
                        {
                            dr["BUMON_UMU"] = Util.ToString(r["BUMON_UMU"]);
                            dr["HOJO_UMU"] = Util.ToString(r["HOJO_KAMOKU_UMU"]);
                        }

                        if (taishakuKubun == 1)
                        {
                            dr["ZEI_KUBUN"] = r["KARIKATA_ZEI_KUBUN"];
                            dr["KAZEI_KUBUN"] = r["KARI_KAZEI_KUBUN"];
                            dr["TORIHIKI_KUBUN"] = r["KARI_TORIHIKI_KUBUN"];
                            dr["ZEI_RITSU"] = r["KARI_ZEI_RITSU"];
                        }
                        else
                        {
                            dr["ZEI_KUBUN"] = r["KASHIKATA_ZEI_KUBUN"];
                            dr["KAZEI_KUBUN"] = r["KASHI_KAZEI_KUBUN"];
                            dr["TORIHIKI_KUBUN"] = r["KASHI_TORIHIKI_KUBUN"];
                            dr["ZEI_RITSU"] = r["KASHI_ZEI_RITSU"];
                        }
                    }
                }
                dr["JIGYO_KUBUN"] = ZaUtil.ToDecimal(rec.Fields["JIGYO_KUBUN"].Value);
                dr["SHOHIZEI_HENKO"] = ZaUtil.ToDecimal(rec.Fields["SHOHIZEI_HENKO"].Value);
                /// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                dr["ZEIKOMI_KINGAKU"] = 0;
                dr["ZEINUKI_KINGAKU"] = Util.ToDecimal(aiteShohizeiKingaku);
                dr["SHOHIZEI_KINGAKU"] = 0;
            }

            // 消費税入力方法
            dr["SHOHIZEI_NYURYOKU_HOHO"] = this.UInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
            // 摘要コード
            dr["TEKIYO_CD"] = ZaUtil.ToDecimal(rec.Fields["TEKIYO_CD"].Value);
            // 摘要
            dr["TEKIYO"] = Util.ToString(rec.Fields["TEKIYO"].Value);
            // 決算区分
            dr["KESSAN_KUBUN"] = ZaUtil.ToDecimal(rec.Fields["KESSAN_KUBUN"].Value);
            // 仕訳作成区分
            dr["SHIWAKE_SAKUSEI_KUBUN"] = null;

            return dr;
        }

        /// <summary>
        /// ファンクションキーの制御
        /// </summary>
        private void SetFunctionKey()
        {
            // コントロール別ファンクションキー表示内容
            switch (GetActiveObjectName())
            {
                //case "DENPYO_DATE_E":
                //case "DENPYO_DATE_M":
                //case "DENPYO_DATE_D":
                //case "TEKIYO_CD":
                //case "TEKIYO":
                //case "SHOHYO_BANGO":
                //case "KANJO_KAMOKU_CD":
                //case "HOJO_KAMOKU_CD":
                //case "BUMON_CD":
                case "DrDENPYO_KINGAKU":
                case "CrDENPYO_KINGAKU":
                    //case "DrSHOHIZEI_KINGAKU":
                    //case "CrSHOHIZEI_KINGAKU":
                    //case "KOJI_CD":
                    //case "KOSHU_CD":
                    btnF1.Text = "F1" + "\n\r" + "\n\r" + "税額入力";
                    break;
                default:
                    btnF1.Text = "F1" + "\n\r" + "\n\r" + "検索";
                    break;
            }
            // コントロール別ファンクションキー利用可否制御
            // (F1)
            switch (GetActiveObjectName())
            {
                case "DENPYO_DATE_E":
                //case "DENPYO_DATE_M":
                //case "DENPYO_DATE_D":
                case "TEKIYO_CD":
                //case "TEKIYO":
                //case "SHOHYO_BANGO":
                case "KANJO_KAMOKU_CD":
                    this.btnF1.Enabled = true;
                    //if (this.MenuFrm != null)
                    //{
                    //    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = this.btnF1.Enabled;
                    //}
                    break;
                case "HOJO_KAMOKU_CD":
                case "BUMON_CD":
                    this.btnF1.Enabled = CFieldEnabled(_activeCRecord.Fields[GetActiveObjectName()]);
                    //if (this.MenuFrm != null)
                    //{
                    //    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = this.btnF1.Enabled;
                    //}
                    break;
                case "DrDENPYO_KINGAKU":
                case "CrDENPYO_KINGAKU":
                    if (_activeCRecord != null)
                    {
                        // 課税区分＝1の時のみ使用可能
                        object kazeiKubun = _activeCRecord.Fields["KAZEI_KUBUN"].Value;
                        this.btnF1.Enabled = ((kazeiKubun != null) && Util.ToInt(kazeiKubun) == 1);
                        //if (this.MenuFrm != null)
                        //{
                        //    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = this.btnF1.Enabled;
                        //}
                    }
                    break;
                //case "DrSHOHIZEI_KINGAKU":
                //case "CrSHOHIZEI_KINGAKU":

                //case "KOJI_CD":
                //case "KOSHU_CD":
                //    this.btnF1.Enabled = true;
                //    break;

                default:
                    this.btnF1.Enabled = false;
                    //if (this.MenuFrm != null)
                    //{
                    //    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = this.btnF1.Enabled;
                    //}
                    break;
            }

            // コントロール別ファンクションキー利用可否制御
            // (F4 F5)
            switch (GetActiveObjectName())
            {
                //case "DENPYO_DATE_E":
                //case "DENPYO_DATE_M":
                //case "DENPYO_DATE_D":
                //case "TEKIYO_CD":
                //case "TEKIYO":
                //case "SHOHYO_BANGO":
                case "KANJO_KAMOKU_CD":
                case "HOJO_KAMOKU_CD":
                case "BUMON_CD":
                case "DrDENPYO_KINGAKU":
                case "CrDENPYO_KINGAKU":
                    //case "DrSHOHIZEI_KINGAKU":
                    //case "CrSHOHIZEI_KINGAKU":
                    //case "KOJI_CD":
                    //case "KOSHU_CD":
                    if (_activeCRecord != null &&
                        !ValChk.IsEmpty(_activeCRecord.Fields["KANJO_KAMOKU_CD"].Value))
                    {
                        this.btnF4.Enabled = true;
                        this.btnF5.Enabled = true;
                    }
                    else
                    {
                        this.btnF4.Enabled = false;
                        this.btnF5.Enabled = false;
                    }
                    break;
                default:
                    this.btnF4.Enabled = false;
                    this.btnF5.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// アクティブコントロール名・アクティブフィールド名を取得
        /// </summary>
        private string GetActiveObjectName()
        {
            string ret = "";
            if (this.ActiveControl != null) ret = this.ActiveControl.Name;

            // アクティブなCFieldが存在する場合はフィールド名を返す
            if (_activeCField != null) ret = Util.ToString(_activeCField.Key);

            return ret;
        }

        /// <summary>
        /// 入力範囲指定画面を表示
        /// </summary>
        /// <param name="isInitial">初期化フラグ</param>
        private void SpecifyRange(bool isInitial)
        {
            ZMDE1012 frm = new ZMDE1012();
            frm.IsInitial = isInitial;
            frm.TantoshaCd = isInitial ? "1" : this.txtTantoshaCd.Text;
            frm.KanjoKamokuCd = this.txtKanjoKamokuCd.Text;
            frm.HojoKamokuCd = "";
            frm.BumonCd = "";
            frm.DenpyoDateFr = _denpyoDateFr;
            frm.DenpyoDateTo = _denpyoDateTo;
            frm.Par1 = this.Par1;
            this.Show();
            DialogResult diagResult = frm.ShowDialog(this);
            if (diagResult == DialogResult.OK)
            {
                // 入力範囲情報を表示
                this.ShishoCode = frm.ShishoCode;
                this.txtTantoshaCd.Text = frm.TantoshaCd;
                this.txtKanjoKamokuCd.Text = frm.KanjoKamokuCd;
                this.txtHojoKamokuCd.Text = frm.HojoKamokuCd;
                this.txtBumonCd.Text = frm.BumonCd;
                _denpyoDateFr = frm.DenpyoDateFr;
                _denpyoDateTo = frm.DenpyoDateTo;
                this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.ShishoCode.ToString(), this.txtTantoshaCd.Text);
                this.lblKanjoKamokuNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.ShishoCode.ToString(), this.txtKanjoKamokuCd.Text);

                this.lblHojoKamokuNm.Text = ValChk.IsEmpty(this.txtHojoKamokuCd.Text)
                    ? "" : this.GetHojoKmkNm(this.ShishoCode.ToString(), this.txtKanjoKamokuCd.Text, this.txtHojoKamokuCd.Text);

                this.lblBumonNm.Text = ValChk.IsEmpty(this.txtBumonCd.Text)
                    ? "" : this.Dba.GetName(this.UInfo, "TB_CM_BUMON", this.ShishoCode.ToString(), this.txtBumonCd.Text);

                // 出納対象勘定レコードをセット
                _drSuitoKanjo = GetKanjoKamokuDataRow(this.txtKanjoKamokuCd.Text);

                // 繰越残高
                this.txtZandaka.Text =
                    Util.FormatNum(GetZandaka(this.txtKanjoKamokuCd.Text, this.txtHojoKamokuCd.Text, _denpyoDateFr));
                // 日付範囲
                string[] jpDateFr = Util.ConvJpDate(_denpyoDateFr, this.Dba);
                string[] jpDateTo = Util.ConvJpDate(_denpyoDateTo, this.Dba);
                this.lblDate.Text = jpDateFr[0] + String.Format("{0, 2}", jpDateFr[2]) + "年"
                                                + String.Format("{0, 2}", jpDateFr[3]) + "月"
                                                + String.Format("{0, 2}", jpDateFr[4]) + "日"
                                                + "～" + jpDateTo[0]
                                                + String.Format("{0, 2}", jpDateTo[2]) + "年"
                                                + String.Format("{0, 2}", jpDateTo[3]) + "月"
                                                + String.Format("{0, 2}", jpDateTo[4]) + "日";

                this.Refresh();     // 画面ちらつき防止

                // 対象データ読込
                DataLoad();
            }
            else
            {

                this.OrgnDialogResult = false;

                if (null != this.MenuFrm)
                {
                    this.MenuFrm.PressEsc();
                }

                base.PressEsc();
            }
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_DENPYOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="mode">EditMode.(Add/Edit)</param>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="denpyoDate">伝票日付</param>
        /// <param name="shohyoBango">証憑番号</param>
        /// <param name="upd">更新日時</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmShiwakeDenpyoParams(EditMode mode, decimal denpyoBango,
            DateTime denpyoDate, string shohyoBango, DateTime upd)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (mode == EditMode.Add)
            {
                // 更新パラメータ設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
                updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                //// 証憑番号
                //updParam.SetParam("@SHOHYO_BANGO", SqlDbType.VarChar, 10, shohyoBango);
                // 仕訳行数
                updParam.SetParam("@SHIWAKE_GYOSU", SqlDbType.Decimal, 6, 1);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, upd);
            }
            else if (mode == EditMode.Edit)
            {
                // Where句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                alParams.Add(whereParam);
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }

            // 伝票日付
            updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, denpyoDate);
            // 担当者コード
            updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.txtTantoshaCd.Text);
            // 証憑番号
            updParam.SetParam("@SHOHYO_BANGO", SqlDbType.VarChar, 10, shohyoBango);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, upd);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_MEISAIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="suitoFlag">出納勘定レコード/相手勘定レコード</param>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="meisaiKubun">明細区分</param>
        /// <param name="upd">更新日時</param>
        /// <param name="denpyoKubun">伝票区分</param>
        /// <param name="rec">UTableレコード</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmShiwakeMeisaiParams(bool suitoFlag, decimal denpyoBango,
            decimal meisaiKubun, DateTime upd, decimal denpyoKubun, UTable.CRecord rec)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 貸借区分・伝票金額
            int taishakuKubun;
            decimal denpyoKingaku;
            if (Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)) != 0)
            {
                // 借方伝票金額が入力されている時
                taishakuKubun = suitoFlag ? 1 : 2;
                denpyoKingaku = Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value));
            }
            else
            {
                taishakuKubun = suitoFlag ? 2 : 1;
                denpyoKingaku = Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value));
            }

            // 伝票日付
            DateTime denpyoDate = Util.ConvAdDate(
                Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), this.Dba);

            // 出納勘定税区分情報
            DataRow drSuitoZei = GetZeiKubunDataRow(taishakuKubun == 1 ?
                Util.ToString(_drSuitoKanjo["KARIKATA_ZEI_KUBUN"]) : Util.ToString(_drSuitoKanjo["KASHIKATA_ZEI_KUBUN"]));
            // 出納勘定消費税額(税区分から計算する)

            //decimal suitoShohizeiKingaku =
            //    ZaUtil.CalcConsumptionTax(Util.ToInt(drSuitoZei["ZEI_KUBUN"]), denpyoKingaku, this.UInfo, this.Dba);

            decimal suitoZeiRt =
                taishakuKubun == 1 ?
                TaxUtil.GetTaxRate(denpyoDate, Util.ToDecimal(Util.ToString(_drSuitoKanjo["KARIKATA_ZEI_KUBUN"])), this.Dba) : TaxUtil.GetTaxRate(denpyoDate, Util.ToDecimal(Util.ToString(_drSuitoKanjo["KASHIKATA_ZEI_KUBUN"])), this.Dba);
            decimal aiteZeiRt =
                taishakuKubun == 1 ?
                ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_RITSU"].Value).Replace("%", "")) : ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_RITSU"].Value).Replace("%", ""));
            decimal suitoShohizeiKingaku =
                ZaUtil.CalcConsumptionTax(suitoZeiRt, denpyoKingaku, this.UInfo, this.Dba);

            // 相手勘定税区分情報
            DataRow drAiteZei = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(rec.Fields["ZEI_KUBUN"].Value)));
            // 相手勘定消費税額
            decimal aiteShohizeiKingaku =
                taishakuKubun == 1 ?
                ZaUtil.ToDecimal(rec.Fields["CrSHOHIZEI_KINGAKU"].Value) : ZaUtil.ToDecimal(rec.Fields["DrSHOHIZEI_KINGAKU"].Value);

            // 会社コード
            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 支所コード
            updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            // 伝票番号
            updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
            // 行番号
            updParam.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, 1);      // 行番号＝1
            // 貸借区分
            updParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, taishakuKubun);
            // 明細区分
            updParam.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, meisaiKubun);
            // 会計年度
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            // 登録日
            updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, upd);
            // 伝票区分
            updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, denpyoKubun);
            // 伝票日付
            updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime,
                Util.ConvAdDate(
                Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), Dba));
            /// 明細区分・出納勘定相手勘定レコード別処理
            if (meisaiKubun == 0 && suitoFlag)
            {
                /// 明細区分０・出納勘定レコード
                updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, ZaUtil.ToDecimal(this.txtKanjoKamokuCd.Text));
                updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, ZaUtil.ToDecimal(this.txtHojoKamokuCd.Text));
                updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(this.txtBumonCd.Text));
                updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]));
                updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, ZaUtil.ToDecimal(drSuitoZei["ZEI_KUBUN"]));
                updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, drSuitoZei["KAZEI_KUBUN"]);

                // 税率取得より
                //updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, drSuitoZei["ZEI_RITSU"]);
                updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, suitoZeiRt);

                updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, drSuitoZei["TORIHIKI_KUBUN"]);
                updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, 0);
                // 税込・税抜金額・消費税額
                updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15,
                    ZaUtil.GetZeikomiKingaku(denpyoKingaku, suitoShohizeiKingaku, this.UInfo));
                updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15,
                    ZaUtil.GetZeinukiKingaku(denpyoKingaku, suitoShohizeiKingaku, this.UInfo));
                updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, suitoShohizeiKingaku);
            }
            else if (meisaiKubun == 0 && !suitoFlag)
            {
                /// 明細区分０・相手勘定レコード
                updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, ZaUtil.ToDecimal(rec.Fields["KANJO_KAMOKU_CD"].Value));
                updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, ZaUtil.ToDecimal(rec.Fields["HOJO_KAMOKU_CD"].Value));
                updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(rec.Fields["BUMON_CD"].Value));
                updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(rec.Fields["JIGYO_KUBUN"].Value));
                updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, ZaUtil.ToDecimal(rec.Fields["ZEI_KUBUN"].Value));
                updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, drAiteZei["KAZEI_KUBUN"]);

                // 税率取得より
                //updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, drAiteZei["ZEI_RITSU"]);
                updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, aiteZeiRt);

                updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, drAiteZei["TORIHIKI_KUBUN"]);
                updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(rec.Fields["SHOHIZEI_HENKO"].Value));
                /// 税込・税抜金額・消費税額
                updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15,
                    ZaUtil.GetZeikomiKingaku(denpyoKingaku, aiteShohizeiKingaku, this.UInfo));
                updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15,
                    ZaUtil.GetZeinukiKingaku(denpyoKingaku, aiteShohizeiKingaku, this.UInfo));
                updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, aiteShohizeiKingaku);
            }
            if (meisaiKubun == 1 && suitoFlag)
            {
                ///// 明細区分１・出納勘定レコード
                //updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4,
                //    taishakuKubun == 1 ? this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]
                //                                            : this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]);
                //updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 4, 0);
                //updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, 0);
                //updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, 0);

                ////updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]));

                //updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, 0);
                ///// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                //updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, 0);
                //updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, Util.ToDecimal(Util.ToString(suitoShohizeiKingaku)));
                //updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);
                ///// 税区分情報
                //updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, 0);
                //updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, 0);

                /// 明細区分１・出納勘定レコード
                DataRow r = null;
                if (drSuitoZei != null)
                {
                    int triKb = Util.ToInt(drSuitoZei["TORIHIKI_KUBUN"]);
                    if (triKb >= 10 && triKb <= 19)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));
                    }
                    else if (triKb >= 20 && triKb <= 29)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
                    }
                    if (r != null)
                    {
                        updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(r["KANJO_KAMOKU_CD"])));
                        updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, 0);
                        if (ZaUtil.ToDecimal(r["BUMON_UMU"]) == 0)
                            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal("0"));
                        else
                            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(this.txtBumonCd.Text));

                        if (taishakuKubun == 1)
                        {
                            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, r["KARIKATA_ZEI_KUBUN"]);
                            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, r["KARI_KAZEI_KUBUN"]);
                            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, r["KARI_TORIHIKI_KUBUN"]);
                            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, r["KARI_ZEI_RITSU"]);
                        }
                        else
                        {
                            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, r["KASHIKATA_ZEI_KUBUN"]);
                            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, r["KASHI_KAZEI_KUBUN"]);
                            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, r["KASHI_TORIHIKI_KUBUN"]);
                            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, r["KASHI_ZEI_RITSU"]);
                        }
                    }
                }
                updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrJIGYO_KUBUN"].Value)));
                updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, 0);
                /// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, 0);
                updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, Util.ToDecimal(Util.ToString(suitoShohizeiKingaku)));
                updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);
            }
            if (meisaiKubun == 1 && !suitoFlag)
            {
                ///// 明細区分１・相手勘定レコード
                //updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4,
                //    taishakuKubun == 1 ? this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]
                //                                        : this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]);
                //updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 4, 0);
                //updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, 0);
                //updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, 0);

                ////updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(rec.Fields["JIGYO_KUBUN"].Value));
                ////updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(rec.Fields["SHOHIZEI_HENKO"].Value));

                ///// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                //updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, 0);
                //updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, Util.ToDecimal(Util.ToString(aiteShohizeiKingaku)));
                //updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);
                ///// 税区分情報
                //updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, 0);
                //updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, 0);

                /// 明細区分１・相手勘定レコード
                DataRow r = null;
                if (drAiteZei != null)
                {
                    int triKb = Util.ToInt(drAiteZei["TORIHIKI_KUBUN"]);
                    if (triKb >= 10 && triKb <= 19)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));
                    }
                    else if (triKb >= 20 && triKb <= 29)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
                    }
                    if (r != null)
                    {
                        updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(r["KANJO_KAMOKU_CD"])));
                        updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, 0);
                        if (ZaUtil.ToDecimal(r["BUMON_UMU"]) == 0)
                            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal("0"));
                        else
                            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(rec.Fields["BUMON_CD"].Value));

                        if (taishakuKubun == 1)
                        {
                            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, r["KARIKATA_ZEI_KUBUN"]);
                            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, r["KARI_KAZEI_KUBUN"]);
                            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, r["KARI_TORIHIKI_KUBUN"]);
                            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, r["KARI_ZEI_RITSU"]);
                        }
                        else
                        {
                            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, r["KASHIKATA_ZEI_KUBUN"]);
                            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, r["KASHI_KAZEI_KUBUN"]);
                            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, r["KASHI_TORIHIKI_KUBUN"]);
                            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, r["KASHI_ZEI_RITSU"]);
                        }
                    }
                }
                updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(rec.Fields["JIGYO_KUBUN"].Value));
                updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(rec.Fields["SHOHIZEI_HENKO"].Value));
                /// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, 0);
                updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, Util.ToDecimal(Util.ToString(aiteShohizeiKingaku)));
                updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);
            }

            // 消費税入力方法
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, this.UInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"]);
            //// 工事コード 
            //updParam.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(rec.Fields["KOJI_CD"].Value));
            //// 工種コード
            //updParam.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(rec.Fields["KOSHU_CD"].Value));
            // 摘要コード
            updParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(rec.Fields["TEKIYO_CD"].Value));
            // 摘要
            updParam.SetParam("@TEKIYO", SqlDbType.VarChar, 40, Util.ToString(rec.Fields["TEKIYO"].Value));
            // 決算区分
            updParam.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, ZaUtil.ToDecimal(rec.Fields["KESSAN_KUBUN"].Value));
            // 仕訳作成区分
            updParam.SetParam("@SHIWAKE_SAKUSEI_KUBUN", SqlDbType.Decimal, 1, null);
            // 備考
            updParam.SetParam("@BIKO", SqlDbType.VarChar, 40, null);
            // 更新日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, upd);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 補助科目名取得
        /// </summary>
        /// <param name="shishoCd">支所コード</param>
        /// <param name="kanjoKmkCd">勘定科目コード</param>
        /// <param name="code">補助科目コード</param>
        /// <returns>補助科目名</returns>
        private string GetHojoKmkNm(string shishoCd, string kanjoKmkCd, string code)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKmkCd);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, code);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND (SHISHO_CD = @SHISHO_CD or SHISHO_CD is null)");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");

            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_ZM_HOJO_KAMOKU",
                where.ToString(),
                dpc);
            if (dt.Rows.Count != 0)
            {
                name = Util.ToString(dt.Rows[0]["HOJO_KAMOKU_NM"]);
            }
            return name;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_DENPYOに更新するためのパラメータ設定をします。
        /// （複合仕訳入力で作成された仕訳伝票）
        /// </summary>
        /// <param name="mode">EditMode.(Add/Edit)</param>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="denpyoDate">伝票日付</param>
        /// <param name="shohyoBango">証憑番号</param>
        /// <param name="gyoSu">仕訳行数</param>
        /// <param name="upd">更新日時</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmShiwakeDenpyoParamsFukugo(EditMode mode,
            decimal denpyoBango, DateTime denpyoDate, string shohyoBango, int gyoSu, DateTime upd)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (mode == EditMode.Add)
            {
                // 更新パラメータ設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
                updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, upd);
            }
            else if (mode == EditMode.Edit)
            {
                // Where句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                alParams.Add(whereParam);
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }

            // 伝票日付
            updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, denpyoDate);
            // 担当者コード
            updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.txtTantoshaCd.Text);
            // 証憑番号
            updParam.SetParam("@SHOHYO_BANGO", SqlDbType.VarChar, 10, shohyoBango);
            // 仕訳行数
            updParam.SetParam("@SHIWAKE_GYOSU", SqlDbType.Decimal, 6, gyoSu);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, upd);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_MEISAIに更新するためのパラメータ設定をします。
        /// （複合仕訳入力で作成された仕訳明細）
        /// </summary>
        /// <param name="dr">保存対象DataRow</param>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="denpyoDate">伝票日付</param>
        /// <param name="upd">更新日時</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmShiwakeMeisaiParamsFukugo(DataRow dr,
            decimal denpyoBango, DateTime denpyoDate, DateTime upd)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 会社コード
            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 支所コード
            updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            // 伝票番号
            updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
            // 行番号
            updParam.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, ZaUtil.ToDecimal(dr["GYO_BANGO"]));
            // 貸借区分
            updParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(dr["TAISHAKU_KUBUN"]));
            // 明細区分
            updParam.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(dr["MEISAI_KUBUN"]));
            // 会計年度
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            // 伝票区分
            updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(dr["DENPYO_KUBUN"]));
            // 伝票日付
            updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, denpyoDate);

            // 勘定科目コード
            updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, ZaUtil.ToDecimal(dr["KANJO_KAMOKU_CD"]));
            // 補助科目コード
            updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, ZaUtil.ToDecimal(dr["HOJO_KAMOKU_CD"]));
            // 部門コード
            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(dr["BUMON_CD"]));
            // 摘要コード
            updParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(dr["TEKIYO_CD"]));
            // 摘要
            updParam.SetParam("@TEKIYO", SqlDbType.VarChar, 40, Util.ToString(dr["TEKIYO"]));
            // 税込・税抜金額・消費税額
            updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, ZaUtil.ToDecimal(dr["ZEIKOMI_KINGAKU"]));
            updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, ZaUtil.ToDecimal(dr["ZEINUKI_KINGAKU"]));
            updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, ZaUtil.ToDecimal(dr["SHOHIZEI_KINGAKU"]));
            // 税区分
            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, ZaUtil.ToDecimal(dr["ZEI_KUBUN"]));
            // 課税区分
            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(dr["KAZEI_KUBUN"]));
            // 取引区分
            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, ZaUtil.ToDecimal(dr["TORIHIKI_KUBUN"]));
            // 税率
            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, ZaUtil.ToDecimal(dr["ZEI_RITSU"]));
            // 事業区分
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(dr["JIGYO_KUBUN"]));
            // 消費税入力方法
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(dr["SHOHIZEI_NYURYOKU_HOHO"]));
            // 消費税変更
            updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(dr["SHOHIZEI_HENKO"]));
            // 決算区分
            updParam.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, ZaUtil.ToDecimal(dr["KESSAN_KUBUN"]));
            // 仕訳作成区分
            updParam.SetParam("@SHIWAKE_SAKUSEI_KUBUN", SqlDbType.Decimal, 1, null);
            // 備考
            updParam.SetParam("@BIKO", SqlDbType.VarChar, 40, null);
            // 更新日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, upd);

            alParams.Add(updParam);

            return alParams;
        }

        #endregion

        #region privateメソッド(入力検査)

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // UTable項目のチェック
            if (!IsValidateUTableAll())
            {
                return false;
            }

            return true;
        }

        #endregion

        #region UTableイベント

        /// <summary>
        /// レコード移動時
        /// </summary>
        /// <param name="record"></param>
        private void mtbList_RecordEnter(UTable.CRecord record)
        {
            // クラス変数にアクティブレコードをセット
            _activeCRecord = record;
        }

        /// <summary>
        /// フォーカス移動時
        /// </summary>
        /// <param name="field"></param>
        private void mtbList_FieldEnter(UTable.CField field)
        {
            // 編集モード開始
            mtbList.StartEdit();

            // クラス変数にアクティブフィールドをセット
            _activeCField = field;

            // ファンクションキー制御
            SetFunctionKey();
        }

        /// <summary>
        /// カスタムエディタの初期化 
        /// </summary>
        private void mtbList_InitializeEditor(UTable.CField field, IEditor editor)
        {
            // 入力制限・IMEモード
            FsiTextBox ed = (FsiTextBox)editor;
            switch (Util.ToString(field.Key))
            {
                case "DENPYO_DATE_E":
                case "DENPYO_DATE_M":
                case "DENPYO_DATE_D":
                    ed.MaxLength = 2;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
                //case "KANJO_KAMOKU_CD":
                case "HOJO_KAMOKU_CD":
                case "BUMON_CD":
                case "TEKIYO_CD":
                case "KOJI_CD":
                case "KOSHU_CD":
                    ed.MaxLength = 4;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
                case "KANJO_KAMOKU_CD":
                    ed.MaxLength = 6;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
                case "DrDENPYO_KINGAKU":
                case "DrSHOHIZEI_KINGAKU":
                case "CrDENPYO_KINGAKU":
                case "CrSHOHIZEI_KINGAKU":
                    ed.MaxLength = 10;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
                case "SHOHYO_BANGO":
                    ed.Width = 10;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
                case "TEKIYO":
                    ed.Width = 40;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        ///  編集モードの開始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mtbList_EditStart(UTable.CField field, IEditor editor)
        {
            // 編集値フォーマット
            FsiTextBox ed = (FsiTextBox)editor;
            switch (Util.ToString(field.Key))
            {
                case "DENPYO_DATE_E":
                case "DENPYO_DATE_M":
                case "DENPYO_DATE_D":
                case "KANJO_KAMOKU_CD":
                case "HOJO_KAMOKU_CD":
                case "BUMON_CD":
                case "TEKIYO_CD":
                case "KOJI_CD":
                case "KOSHU_CD":
                    // NONE
                    break;
                case "DrDENPYO_KINGAKU":
                case "DrSHOHIZEI_KINGAKU":
                case "CrDENPYO_KINGAKU":
                case "CrSHOHIZEI_KINGAKU":
                    ed.Text = Util.ToString(Util.ToDecimal(Util.ToString(field.Value)));
                    break;
                case "TEKIYO":
                    // NONE
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// フォーカス喪失時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mtbList_Leave(object sender, EventArgs e)
        {
            mtbList.FocusField = null;      // フォーカス位置をクリア

            _activeCRecord = null;          // アクティブレコード情報をクリア
            _activeCField = null;           // アクティブフィールド情報をクリア
        }

        #endregion

        #region UTableイベント(入力検査)

        /// <summary>
        ///  入力値検証
        /// </summary>
        /// <param name="field"></param>
        /// <param name="e"></param>
        private void mtbList_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            // フィールド別処理
            switch (Util.ToString(field.Key))
            {
                case "DENPYO_DATE_E":
                    DENPYO_DATE_E_FieldValidating(field, e);
                    break;
                case "DENPYO_DATE_M":
                    DENPYO_DATE_M_FieldValidating(field, e);
                    break;
                case "DENPYO_DATE_D":
                    DENPYO_DATE_D_FieldValidating(field, e);
                    break;
                case "TEKIYO_CD":
                    TEKIYO_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "TEKIYO":
                    TEKIYO_FieldValidating(field, e);
                    break;
                case "SHOHYO_BANGO":
                    SHOHYO_BANGO_FieldValidating(field, e);
                    break;
                case "KANJO_KAMOKU_CD":
                    KANJO_KAMOKU_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "HOJO_KAMOKU_CD":
                    HOJO_KAMOKU_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "BUMON_CD":
                    BUMON_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "KOJI_CD":
                    KOJI_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "KOSHU_CD":
                    KOSHU_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "DrDENPYO_KINGAKU":
                case "CrDENPYO_KINGAKU":
                    DENPYO_KINGAKU_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatCur(field.Value);           // 金額フォーマット
                    break;
                case "DrSHOHIZEI_KINGAKU":
                case "CrSHOHIZEI_KINGAKU":
                    SHOHIZEI_KINGAKU_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatCur(field.Value);           // 金額フォーマット
                    break;
                default:
                    break;
            }

            // レコード編集状態
            if (!e.Cancel)
            {
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value))
                {
                    if ((EditMode)field.Record.Fields["EDIT_MODE"].Value == EditMode.None)
                    {
                        // 新規行へのレコード番号付番号判定
                        if (field.Record.Fields["NO"].Value == null)
                        {
                            field.Record.Fields["NO"].Value = mtbList.Content.Records.Count;
                            field.Record.Fields["EDIT_MODE"].Value = EditMode.Add;          // レコード追加あり
                            AddBlankRecord();                                               // ブランク行を追加
                        }
                        else
                        {
                            field.Record.Fields["EDIT_MODE"].Value = EditMode.Edit;         // レコード編集あり
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 伝票日付(年)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DENPYO_DATE_E_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValid.IsYear(field.Value, 2))
            {
                e.Cancel = true;
                return;
            }
            else
            {
                field.Value = Util.ToString(IsValid.SetYear(field.Value));
                UTable.CRecord rec = field.Record;
                SetJp(rec, ZaUtil.FixJpDate(
                        Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), this.Dba));
            }
        }

        /// <summary>
        /// 伝票日付(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DENPYO_DATE_M_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(field.Value, 2))
            {
                e.Cancel = true;
                return;
            }
            else
            {
                field.Value = Util.ToString(IsValid.SetMonth(field.Value));
                UTable.CRecord rec = field.Record;
                SetJp(rec, ZaUtil.FixJpDate(
                        Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), this.Dba));
            }
        }

        /// <summary>
        /// 伝票日付(日)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DENPYO_DATE_D_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValid.IsDay(field.Value, 2))
            {
                e.Cancel = true;
                return;
            }
            else
            {
                field.Value = Util.ToString(IsValid.SetDay(field.Value));
                UTable.CRecord rec = field.Record;
                SetJp(rec, ZaUtil.FixJpDate(
                        Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), this.Dba));
            }
        }

        /// <summary>
        /// 入力値検証(摘要コード)
        /// </summary>
        private void TEKIYO_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidTekiyoCd(field))
            {
                e.Cancel = true;
            }
            else
            {
                // コードが変わった場合かコード入力があり名称が消えている場合
                if ((Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
                    && !ValChk.IsEmpty(field.Value)) || (!ValChk.IsEmpty(field.Value) && Util.ToString(field.Record.Fields["TEKIYO"].Value).Length == 0))
                {
                    string name = this.Dba.GetName(this.UInfo, "TB_ZM_TEKIYO", this.ShishoCode.ToString(), Util.ToString(field.Value));
                    field.Record.Fields["TEKIYO"].Value = name;

                    /* このタイミングでフォーム呼ぶ必要ないので不要 2020/02/19 吉村
                    #region 仕訳事例検索
                    // アセンブリのロード
                    //Assembly asm = System.Reflection.Assembly.LoadFrom("ZAMC9531.exe");
                    string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
                    Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMDE1031.exe");

                    string NameSpace = "zm.zmde1031";

                    //フォーム作成
                    string moduleNameSpace = "jp.co.fsi." + NameSpace + "." + "ZMDE1031";
                    Type t = asm.GetType(moduleNameSpace);

                    // フォーム作成
                    //Type t = asm.GetType("jp.co.fsi.zam.zamc9531.ZAMC9531");
                    //Type t = asm.GetType("jp.co.fsi.zm.zmde1031.ZMDE1036");
                    if (t != null)
                    {
                        //Object obj = System.Activator.CreateInstance(t);
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            string[] inData = new string[2];
                            inData[0] = Util.ToString(field.Value);         // 摘要コード
                            inData[1] = this.txtKanjoKamokuCd.Text;         // 出納勘定科目コード
                            frm.InData = inData;
                            frm.Par1 = "2";                                 // 自動検索モード
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;

                                // 仕訳事例データのロード
                                ShiwakeJireiLoad(Util.ToString(field.Value), result[0]);
                            }
                        }
                    }
                    #endregion
                    */

                    //合計計算
                    CalcTotal();
                }
            }
        }

        /// <summary>
        /// 入力値検証(摘要)
        /// </summary>
        private void TEKIYO_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidTekiyo(field))
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// 入力値検証(証憑番号)
        /// </summary>
        private void SHOHYO_BANGO_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidShohyoBango(field))
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// 入力値検証(勘定科目コード)
        /// </summary>
        private void KANJO_KAMOKU_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidKANJO_KAMOKU_CD(field))
            {
                e.Cancel = true;
            }
            else
            {
                // 値変更の場合に従属情報をセット
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value))
                {
                    SetKanjoKamokuInfo(field);
                }
            }
        }

        /// <summary>
        /// 入力値検証(補助科目コード)
        /// </summary>
        private void HOJO_KAMOKU_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidHOJO_KAMOKU_CD(Util.ToString(field.Record.Fields["KANJO_KAMOKU_CD"].Value), field))
            {
                e.Cancel = true;
            }
            else
            {
                // 値変更の場合に補助科目名をセット
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
                    && !ValChk.IsEmpty(field.Value))
                {
                    // 補助科目名をセット
                    field.Record.Fields["HOJO_KAMOKU_NM"].Value =
                        this.GetHojoKmkNm(this.ShishoCode.ToString(),
                            Util.ToString(field.Record.Fields["KANJO_KAMOKU_CD"].Value),
                            Util.ToString(field.Value));
                }
            }
        }

        /// <summary>
        /// 入力値検証(部門コード)
        /// </summary>
        private void BUMON_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidBUMON_CD(field))
            {
                e.Cancel = true;
            }
            else
            {
                // 値変更の場合に従属情報をセット
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
                    && !ValChk.IsEmpty(field.Value))
                {
                    // 部門情報を取得
                    DataRow dr = GetBumonDataRow(Util.ToString(field.Value));
                    // 従属フィールド情報セット
                    if (Util.ToInt(Util.ToString(dr["JIGYO_KUBUN"])) != 0)
                        field.Record.Fields["JIGYO_KUBUN"].Value = Util.ToString(dr["JIGYO_KUBUN"]);
                    else
                        field.Record.Fields["JIGYO_KUBUN"].Value = ZaUtil.FormatNum(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);
                }
            }
        }

        /// <summary>
        /// 入力値検証(伝票金額)
        /// </summary>
        private void DENPYO_KINGAKU_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidDENPYO_KINGAKU(field))
            {
                e.Cancel = true;
                return;
            }

            string pre = string.Empty;
            if (Util.ToString(field.Key).Substring(0, 2) == "Dr") pre = "Dr";
            if (Util.ToString(field.Key).Substring(0, 2) == "Cr") pre = "Cr";

            // 表示書式
            if (Util.ToDecimal(field.Value) != 0)
            {
                field.Value = Util.FormatNum(field.Value);
            }
            else
            {
                field.Value = "";
            }
            // 伝票金額、消費税がゼロ以下は文字色を赤にする
            if (Util.ToDecimal(Util.ToString(field.Value)) >= 0)
            {
                field.Setting.ForeColor = Color.Black;
            }
            else
            {
                field.Setting.ForeColor = Color.Red;
            }
            // 金額変更時
            if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value))
            {
                // 税区分情報のセット
                // 消費税額の算出
                decimal zei = 0;
                if (!ValChk.IsEmpty(field.Value))
                {
                    // 保持された税率で計算
                    DateTime denpyoDate = Util.ConvAdDate(
                        Util.ToString(field.Record.Fields["DENPYO_DATE_G"].Value),
                        Util.ToString(field.Record.Fields["DENPYO_DATE_E"].Value),
                        Util.ToString(field.Record.Fields["DENPYO_DATE_M"].Value),
                        Util.ToString(field.Record.Fields["DENPYO_DATE_D"].Value), this.Dba);
                    field.Record.Fields[pre + "ZEI_RITSU"].Value = ZaUtil.FormatPercentage(TaxUtil.GetTaxRate(denpyoDate, Util.ToDecimal(Util.ToString(field.Record.Fields["ZEI_KUBUN"].Value)), this.Dba));
                    zei = ZaUtil.CalcConsumptionTax(Util.ToDecimal(Util.ToString(field.Record.Fields[pre + "ZEI_RITSU"].Value).Replace("%", "")),
                            Util.ToDecimal(field.Value), this.UInfo, this.Dba);
                    if (zei != 0)
                    {
                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Value = Util.FormatNum(zei);
                    }
                    else
                    {
                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Value = "";
                        field.Record.Fields[pre + "ZEI_RITSU"].Value = "";
                    }
                    // 伝票金額、消費税がゼロ以下は文字色を赤にする
                    if (zei >= 0)
                    {
                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                    }
                    else
                    {
                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                    }
                }
                else
                {
                    field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Value = "";
                    field.Record.Fields[pre + "ZEI_RITSU"].Value = "";
                }

                //合計計算
                CalcTotal();
            }

            if (Util.ToDecimal(Util.ToString(field.Record.Fields["DrDENPYO_KINGAKU"].Value)) != 0 &&
                pre == "Dr")
            {
                CFieldEnabled(field.Record.Fields["CrDENPYO_KINGAKU"], false);
            }
            else
            {
                CFieldEnabled(field.Record.Fields["CrDENPYO_KINGAKU"], true);
            }
        }

        /// <summary>
        /// 入力値検証(消費税)
        /// </summary>
        private void SHOHIZEI_KINGAKU_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidSHOHIZEI_KINGAKU(field))
            {
                e.Cancel = true;
                return;
            }

            // 表示書式
            if (Util.ToDecimal(field.Value) != 0)
            {
                field.Value = Util.FormatNum(field.Value);
            }
            else
            {
                field.Value = "";
            }
            // 伝票金額、消費税がゼロ以下は文字色を赤にする
            if (Util.ToDecimal(Util.ToString(field.Value)) >= 0)
            {
                field.Setting.ForeColor = Color.Black;
            }
            else
            {
                field.Setting.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// 入力値検証(工事コード)
        /// </summary>
        private void KOJI_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            //if (!IsValidKojiCd(field))
            //{
            //    e.Cancel = true;
            //}
            //else
            //{
            //    // 値変更の場合に従属情報をセット
            //    if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
            //        && !ValChk.IsEmpty(field.Value))
            //    {
            //        field.Record.Fields["KOJI_NM"].Value =
            //        this.Dba.GetName(this.UInfo, "TB_ZM_KOJI", Util.ToString(field.Value)); 
            //    }
            //}
        }

        /// <summary>
        /// 入力値検証(工種コード)
        /// </summary>
        private void KOSHU_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            //if (!IsValidKoshuCd(field))
            //{
            //    e.Cancel = true;
            //}
            //else
            //{
            //    // 値変更の場合に従属情報をセット
            //    if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
            //        && !ValChk.IsEmpty(field.Value))
            //    {
            //        field.Record.Fields["KOSHU_NM"].Value =
            //        this.Dba.GetName(this.UInfo, "TB_ZM_KOSHU", Util.ToString(field.Value));
            //    }
            //}
        }

        #endregion

        #region UTable privateメソッド

        /// <summary>
        /// CField使用可否設定
        /// </summary>
        private void CFieldEnabled(UTable.CField field, bool flag)
        {
            if (field != null)
            {
                field.Setting.TabStop = flag ? UTable.ETabStop.STOP : UTable.ETabStop.NOTSTOP;
                field.Setting.Editable = flag ? UTable.EAllow.ALLOW : UTable.EAllow.DISABLE;
            }
        }

        /// <summary>
        /// CField使用可否取得
        /// </summary>
        private bool CFieldEnabled(UTable.CField field)
        {
            bool ret = false;

            if (field != null)
            {
                ret = (field.Setting.Editable == UTable.EAllow.ALLOW);
            }
            return ret;
        }

        /// <summary>
        /// ブランク行追加
        /// </summary>
        private void AddBlankRecord()
        {
            mtbList.Content.AddRecord();

            UTable.CRecord rec = mtbList.Content.LastAddedRecord;

            rec.Fields["JURNAL_TYPE"].Value = JurnalType.Single;
            rec.Fields["EDIT_MODE"].Value = EditMode.None;

            SetJp(rec, Util.ConvJpDate(Util.ToString(_denpyoDateTo), this.Dba));  // 初期値：指定範囲最終日
        }

        /// <summary>
        /// 年月日の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(Util.ToString(this._activeCRecord.Fields["DENPYO_DATE_G"].Value), Util.ToString(this._activeCRecord.Fields["DENPYO_DATE_E"].Value),
                Util.ToString(this._activeCRecord.Fields["DENPYO_DATE_M"].Value), "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(Util.ToString(this._activeCRecord.Fields["DENPYO_DATE_D"].Value)) > lastDayInMonth)
            {
                this._activeCRecord.Fields["DENPYO_DATE_D"].Value = Util.ToString(lastDayInMonth);
            }
        }

        ///// <summary>
        ///// 年月日の正しい和暦への変換処理
        ///// </summary>
        ///// 
        //private void SetJp(UTable.CRecord rec)
        //{
        //    // 本来その元号に存在しない日付である可能性があるので、
        //    // 一度西暦変換→和暦変換して、正しい和暦に戻す
        //    SetJp(this._activeCRecord,
        //          Util.ConvJpDate(
        //          this.FixFromToDate(
        //          ZaUtil.FixNendoDate(
        //          Util.ConvAdDate(Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
        //                          Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
        //                          Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
        //                          Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), this.Dba), this.UInfo)), this.Dba));
        //}

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(UTable.CRecord rec, string[] arrJpDate)
        {
            string[] jpDate =
                  Util.ConvJpDate(
                  this.FixFromToDate(
                  ZaUtil.FixNendoDate(
                  Util.ConvAdDate(arrJpDate[0],
                                  arrJpDate[2],
                                  arrJpDate[3],
                                  arrJpDate[4], this.Dba), this.UInfo)), this.Dba);

            rec.Fields["DENPYO_DATE_G"].Value = jpDate[0];
            rec.Fields["DENPYO_DATE_E"].Value = jpDate[2];
            rec.Fields["DENPYO_DATE_M"].Value = jpDate[3];
            rec.Fields["DENPYO_DATE_D"].Value = jpDate[4];

            // 伝票日付の変更に伴う税率変更処理
            DateTime denpyoDate = Util.ConvAdDate(
                Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                Util.ToString(rec.Fields["DENPYO_DATE_D"].Value),
                this.Dba);

            // 新規は無条件、修正時は日付が変わったら行う
            if (DENPYO_DATE != denpyoDate.ToString("yyyy/MM/dd"))
            {
                DENPYO_DATE = denpyoDate.ToString("yyyy/MM/dd");
                // 日付が変わった場合には無条件に税率を再設定して金額も再計算
                string pre = string.Empty;
                if (Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)) != 0)
                {
                    pre = "Dr";
                }
                else if (Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)) != 0)
                {
                    pre = "Cr";
                }
                if (pre.Length != 0)
                {
                    decimal zeiRt = TaxUtil.GetTaxRate(denpyoDate, Util.ToInt(Util.ToString(rec.Fields["ZEI_KUBUN"].Value)), this.Dba);
                    if (zeiRt != Util.ToDecimal(Util.ToString(rec.Fields[pre + "ZEI_RITSU"].Value).Replace("%", "")))
                    {
                        rec.Fields[pre + "ZEI_RITSU"].Value = ZaUtil.FormatPercentage(zeiRt);
                        decimal zei = ZaUtil.CalcConsumptionTax(Util.ToDecimal(Util.ToString(rec.Fields[pre + "ZEI_RITSU"].Value).Replace("%", "")),
                            Util.ToDecimal(Util.ToString(rec.Fields[pre + "DENPYO_KINGAKU"].Value)), this.UInfo, this.Dba);
                        if (zei != 0)
                        {
                            rec.Fields[pre + "SHOHIZEI_KINGAKU"].Value = Util.FormatNum(zei);
                        }
                        else
                        {
                            rec.Fields[pre + "SHOHIZEI_KINGAKU"].Value = "";
                        }
                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (zei >= 0)
                        {
                            rec.Fields[pre + "SHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            rec.Fields[pre + "SHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 日付範囲（条件入力）内日付に変換
        /// </summary>
        /// <param name="date">調整日付</param>
        /// <returns></returns>
        private DateTime FixFromToDate(DateTime date)
        {
            DateTime dateFr = _denpyoDateFr;
            DateTime dateTo = _denpyoDateTo;
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }

        /// <summary>
        /// 勘定科目情報をセット
        /// </summary>
        /// <param name="field">UTableフィールド</param>
        private void SetKanjoKamokuInfo(UTable.CField field)
        {
            if (Util.ToString(field.Key) == "KANJO_KAMOKU_CD")
            {
                if (ValChk.IsEmpty(field.Value))
                {
                    // 従属フィールドをクリア
                    field.Record.Fields["KANJO_KAMOKU_NM"].Value = "";
                    field.Record.Fields["HOJO_KAMOKU_CD"].Value = "";
                    field.Record.Fields["HOJO_KAMOKU_NM"].Value = "";
                    field.Record.Fields["BUMON_CD"].Value = "";
                    field.Record.Fields["JIGYO_KUBUN"].Value = "";
                    field.Record.Fields["ZEI_KUBUN"].Value = "";
                    field.Record.Fields["DrZEI_RITSU"].Value = "";
                    field.Record.Fields["CrZEI_RITSU"].Value = "";
                    field.Record.Fields["DrSHOHIZEI_KINGAKU"].Value = "";
                    field.Record.Fields["CrSHOHIZEI_KINGAKU"].Value = "";
                    //field.Record.Fields["KOJI_UMU"].Value = "";
                    field.Record.Fields["KAZEI_KUBUN"].Value = "";
                    field.Record.Fields["SHOHIZEI_HENKO"].Value = "";
                    // 編集可否をクリア
                    CFieldEnabled(field.Record.Fields["HOJO_KAMOKU_CD"], false);
                    CFieldEnabled(field.Record.Fields["BUMON_CD"], false);
                    CFieldEnabled(field.Record.Fields["DrSHOHIZEI_KINGAKU"], false);
                    CFieldEnabled(field.Record.Fields["CrSHOHIZEI_KINGAKU"], false);
                    //// 工事情報(出納勘定も工事なしの場合クリア)
                    //if (Util.ToInt(_drSuitoKanjo["KOJI_UMU"]) != 1)
                    //{
                    //    field.Record.Fields["KOJI_CD"].Value = "";
                    //    field.Record.Fields["KOJI_NM"].Value = "";
                    //    field.Record.Fields["KOSHU_CD"].Value = "";
                    //    field.Record.Fields["KOSHU_NM"].Value = "";
                    //    CFieldEnabled(field.Record.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(field.Record.Fields["KOSHU_CD"], false);
                    //}
                }
                else
                {
                    // 科目情報を取得
                    DataRow drKmk = GetKanjoKamokuDataRow(Util.ToString(field.Value));
                    // 従属フィールド情報セット
                    field.Record.Fields["KANJO_KAMOKU_NM"].Value = Util.ToString(drKmk["KANJO_KAMOKU_NM"]);
                    field.Record.Fields["HOJO_KAMOKU_CD"].Value = "";
                    field.Record.Fields["HOJO_KAMOKU_NM"].Value = "";
                    field.Record.Fields["BUMON_CD"].Value = "";
                    field.Record.Fields["JIGYO_KUBUN"].Value =
                        ZaUtil.FormatNum(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);     // 会社情報の事業区分
                    //field.Record.Fields["KOJI_UMU"].Value = Util.ToString(drKmk["KOJI_UMU"]);

                    /// ***<NOTE>*** 
                    /// 画面上では伝票金額が入力された貸借に税額表示されるが、
                    /// 税情報は画面上の逆の貸借レコードに関する入力である。
                    /// ***<NOTE>***

                    // 指定科目の貸方税区分を仮セット
                    field.Record.Fields["ZEI_KUBUN"].Value = Util.ToString(drKmk["KASHIKATA_ZEI_KUBUN"]);

                    // 伝票日付
                    DateTime denpyoDate = Util.ConvAdDate(
                        Util.ToString(field.Record.Fields["DENPYO_DATE_G"].Value),
                        Util.ToString(field.Record.Fields["DENPYO_DATE_E"].Value),
                        Util.ToString(field.Record.Fields["DENPYO_DATE_M"].Value),
                        Util.ToString(field.Record.Fields["DENPYO_DATE_D"].Value), this.Dba);

                    // makishi
                    field.Record.Fields["KAZEI_KUBUN"].Value = Util.ToString(drKmk["KASHI_KAZEI_KUBUN"]);

                    // 借方伝票金額が入力済のとき
                    if (Util.ToDecimal(Util.ToString(field.Record.Fields["DrDENPYO_KINGAKU"].Value)) != 0)
                    {
                        // 伝票借方(勘定科目貸方)税区分を基準日チェック
                        string zeiKubun = Util.ToString(drKmk["KASHIKATA_ZEI_KUBUN"]);
                        decimal taxRate = 0;
                        DialogResult ans = ConfPastZeiKbn(denpyoDate, zeiKubun, ref taxRate);
                        if (ans != DialogResult.OK)
                        {
                            // 税区分を選択->メソッド側で検索及び設定へ
                            //string ret = SelectZeiKubun(denpyoDate, false);
                            //if (!ValChk.IsEmpty(ret))
                            //{
                            //    zeiKubun = ret;
                            //}
                            SelectZeiKubun(denpyoDate, false, field);
                        }
                        // makishi
                        //field.Record.Fields["ZEI_KUBUN"].Value = zeiKubun;
                        //SetZeiKubunInfo(field.Record, 1);
                    }
                    // 貸方伝票金額が入力済のとき
                    if (Util.ToDecimal(Util.ToString(field.Record.Fields["CrDENPYO_KINGAKU"].Value)) != 0)
                    {
                        // 伝票貸方(勘定科目借方)税区分を基準日チェック
                        string zeiKubun = Util.ToString(drKmk["KARIKATA_ZEI_KUBUN"]);
                        decimal taxRate = 0;
                        DialogResult ans = ConfPastZeiKbn(denpyoDate, zeiKubun, ref taxRate);
                        if (ans != DialogResult.OK)
                        {
                            // 税区分を選択->メソッド側で検索及び設定へ
                            //string ret = SelectZeiKubun(denpyoDate, false);
                            //if (!ValChk.IsEmpty(ret))
                            //{
                            //    zeiKubun = ret;
                            //}
                            SelectZeiKubun(denpyoDate, false, field);
                        }
                        // makishi
                        //field.Record.Fields["ZEI_KUBUN"].Value = zeiKubun;
                        //SetZeiKubunInfo(field.Record, 2);
                    }

                    // 編集可否
                    CFieldEnabled(field.Record.Fields["HOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(drKmk["HOJO_SHIYO_KUBUN"])) > 0);
                    CFieldEnabled(field.Record.Fields["BUMON_CD"], Util.ToInt(Util.ToString(drKmk["BUMON_UMU"])) == 1);
                    CFieldEnabled(field.Record.Fields["DrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(drKmk["KANJO_KAMOKU_CD"])) > 0);
                    CFieldEnabled(field.Record.Fields["CrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(drKmk["KANJO_KAMOKU_CD"])) > 0);
                    CFieldEnabled(field.Record.Fields["DrSHOHIZEI_KINGAKU"],
                        Util.ToInt(Util.ToString(field.Record.Fields["SHOHIZEI_HENKO"])) == 1);
                    CFieldEnabled(field.Record.Fields["CrSHOHIZEI_KINGAKU"],
                        Util.ToInt(Util.ToString(field.Record.Fields["SHOHIZEI_HENKO"])) == 1);
                    //// 工事・工種編集可否
                    //if (Util.ToInt(field.Record.Fields["KOJI_UMU"].Value) == 1
                    //    || Util.ToInt(_drSuitoKanjo["KOJI_UMU"]) == 1)
                    //{
                    //    CFieldEnabled(field.Record.Fields["KOJI_CD"], true);
                    //    CFieldEnabled(field.Record.Fields["KOSHU_CD"], true);
                    //}
                    //else
                    //{
                    //    CFieldEnabled(field.Record.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(field.Record.Fields["KOSHU_CD"], false);
                    //}
                }

            }
        }

        /// <summary>
        /// 仕訳事例データの読込
        /// </summary>
        /// <param name="tekiyoCd">摘要コード</param>
        /// <param name="shiwakeCd">仕訳コード</param>
        private void ShiwakeJireiLoad(string tekiyoCd, string shiwakeCd)
        {
            #region TB仕訳事例データ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, tekiyoCd);
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 4, shiwakeCd);
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append(", TEKIYO_CD");
            cols.Append(", TEKIYO_NM");
            cols.Append(", SHIWAKE_CD");
            cols.Append(", GYO_BANGO");
            cols.Append(", TAISHAKU_KUBUN");
            cols.Append(", MEISAI_KUBUN");
            cols.Append(", KANJO_KAMOKU_CD");
            cols.Append(", KANJO_KAMOKU_NM");
            cols.Append(", BUMON_UMU");
            cols.Append(", HOJO_KAMOKU_UMU");
            //cols.Append(", KOJI_UMU");
            cols.Append(", HOJO_KAMOKU_CD");
            cols.Append(", HOJO_KAMOKU_NM");
            cols.Append(", BUMON_CD");
            cols.Append(", BUMON_NM");
            //cols.Append(", KOJI_CD");
            //cols.Append(", KOJI_NM");
            //cols.Append(", KOSHU_CD");
            //cols.Append(", KOSHU_NM");
            cols.Append(", TEKIYO");
            cols.Append(", ZEI_KUBUN");
            cols.Append(", ZEI_KUBUN_NM");
            cols.Append(", KAZEI_KUBUN");
            cols.Append(", TORIHIKI_KUBUN");
            cols.Append(", ZEI_RITSU");
            cols.Append(", JIGYO_KUBUN");
            cols.Append(", SHOHIZEI_NYURYOKU_HOHO");
            cols.Append(", SHOHIZEI_HENKO");
            cols.Append(", KESSAN_KUBUN");
            cols.Append(", ZEIKOMI_KINGAKU");
            cols.Append(", ZEINUKI_KINGAKU");
            cols.Append(", SHOHIZEI_KINGAKU");
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND TEKIYO_CD = @TEKIYO_CD");
            where.Append(" AND SHIWAKE_CD = @SHIWAKE_CD");
            where.Append(" AND MEISAI_KUBUN = 0");
            StringBuilder order = new StringBuilder();
            order.Append("GYO_BANGO ASC");
            order.Append(", TAISHAKU_KUBUN ASC");
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                            , "VI_ZM_SHIWAKE_JIREI"
                            , Util.ToString(where), dpc);

            #endregion

            // 利用可能な仕訳事例データかを判定
            if (dt.Rows.Count > 1)
            {
                DataRow[] drs;

                // 複数行仕訳は処理しない
                drs = dt.Select("GYO_BANGO = MAX(GYO_BANGO) AND MEISAI_KUBUN = 0");
                if (drs.Length > 0 && Util.ToInt(Util.ToString(drs[0]["GYO_BANGO"])) > 1) return;

                // 指定出納勘定≠1レコード は処理しない
                drs = dt.Select("KANJO_KAMOKU_CD = '" + this.txtKanjoKamokuCd.Text + "'");
                if (drs.Length != 1) return;

            }

            #region 事例データをカレント行位置へ上書き
            /// **************************************************
            /// UTableレンダリングブロック開始
            /// **************************************************
            using (mtbList.RenderBlock())
            {
                // 伝票日付
                DateTime denpyoDate;

                // 仕訳事例データを参照
                foreach (DataRow dr in dt.Rows)
                {
                    UTable.CRecord rec = _activeCRecord;
                    //bool suitoKojiUmu = false;

                    // 伝票日付
                    denpyoDate = Util.ConvAdDate(
                        Util.ToString(rec.Fields["DENPYO_DATE_G"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_E"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_M"].Value),
                        Util.ToString(rec.Fields["DENPYO_DATE_D"].Value), this.Dba);

                    if (Util.ToString(dr["KANJO_KAMOKU_CD"]) == txtKanjoKamokuCd.Text)
                    {
                        // 貸借レコード共通情報
                        rec.Fields["TEKIYO"].Value = Util.ToString(dr["TEKIYO"]);
                        //rec.Fields["KOJI_CD"].Value = "";                           // 事例データに保存されない
                        //rec.Fields["KOJI_NM"].Value = "";
                        //rec.Fields["KOSHU_CD"].Value = "";
                        //rec.Fields["KOSHU_NM"].Value = "";

                        // 貸借の別
                        if (Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])) != 0)
                        {
                            if (Util.ToInt(Util.ToString(dr["TAISHAKU_KUBUN"])) == 1)
                            {
                                rec.Fields["DrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                                    ZaUtil.GetDenpyoKingaku(
                                        Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                        Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])), this.UInfo));
                                rec.Fields["CrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(0);
                            }
                            else
                            {
                                rec.Fields["DrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(0);
                                rec.Fields["CrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                                    ZaUtil.GetDenpyoKingaku(
                                        Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                        Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])), this.UInfo));
                            }
                        }

                        //// 出納勘定工事有無の別
                        //suitoKojiUmu = Util.ToInt(Util.ToString(dr["KOJI_UMU"])) == 1 ? true : false;
                    }
                    else
                    {
                        // 相手勘定情報
                        rec.Fields["KANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KANJO_KAMOKU_CD"]);
                        rec.Fields["KANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                        if (Util.ToInt(Util.ToString(dr["HOJO_KAMOKU_CD"])) > 0)
                        {
                            rec.Fields["HOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["HOJO_KAMOKU_CD"]);
                            rec.Fields["HOJO_KAMOKU_NM"].Value = Util.ToString(dr["HOJO_KAMOKU_NM"]);
                        }
                        if (Util.ToInt(Util.ToString(dr["BUMON_CD"])) > 0)
                        {
                            rec.Fields["BUMON_CD"].Value = ZaUtil.FormatNum(dr["BUMON_CD"]);
                        }
                        if (Util.ToInt(Util.ToString(dr["JIGYO_KUBUN"])) > 0)
                        {
                            rec.Fields["JIGYO_KUBUN"].Value = ZaUtil.FormatNum(dr["JIGYO_KUBUN"]);
                        }
                        else
                        {
                            rec.Fields["JIGYO_KUBUN"].Value =
                                ZaUtil.FormatNum(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);
                        }
                        rec.Fields["ZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["ZEI_KUBUN"]);

                        if (Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"])) != 0)
                        {
                            if (Util.ToInt(Util.ToString(dr["TAISHAKU_KUBUN"])) == 2)  // 画面上への表示は貸借逆位置
                            {
                                // 税率
                                //rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                                rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(TaxUtil.GetTaxRate(denpyoDate, Util.ToDecimal(Util.ToString(dr["ZEI_KUBUN"])), this.Dba));

                                rec.Fields["CrZEI_RITSU"].Value = "";
                                rec.Fields["DrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["SHOHIZEI_KINGAKU"]);
                                rec.Fields["CrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(0);
                            }
                            else
                            {
                                rec.Fields["DrZEI_RITSU"].Value = "";
                                // 税率
                                //rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                                rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(TaxUtil.GetTaxRate(denpyoDate, Util.ToDecimal(Util.ToString(dr["ZEI_KUBUN"])), this.Dba));

                                rec.Fields["DrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(0);
                                rec.Fields["CrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["SHOHIZEI_KINGAKU"]);
                            }
                        }
                        // 非表示フィールド
                        //rec.Fields["KOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                        rec.Fields["KAZEI_KUBUN"].Value = Util.ToString(dr["KAZEI_KUBUN"]);
                        rec.Fields["SHOHIZEI_HENKO"].Value = Util.ToString(dr["SHOHIZEI_HENKO"]);
                        rec.Fields["KESSAN_KUBUN"].Value = Util.ToString(dr["KESSAN_KUBUN"]);
                        // 使用可否
                        CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], true);
                        CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], true);
                        CFieldEnabled(rec.Fields["HOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(dr["HOJO_KAMOKU_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["BUMON_CD"], Util.ToInt(Util.ToString(dr["BUMON_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], Util.ToInt(Util.ToString(dr["SHOHIZEI_HENKO"])) == 1);
                        CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], Util.ToInt(Util.ToString(dr["SHOHIZEI_HENKO"])) == 1);
                    }

                    //// 工事・工種編集可否
                    //if (suitoKojiUmu || Util.ToInt(rec.Fields["KOJI_UMU"]) == 1)
                    //{
                    //    CFieldEnabled(rec.Fields["KOJI_CD"], true);
                    //    CFieldEnabled(rec.Fields["KOSHU_CD"], true);
                    //}
                    //else
                    //{
                    //    CFieldEnabled(rec.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(rec.Fields["KOSHU_CD"], false);
                    //}
                }
                this.UTableNoRefresh();
            }
            /// **************************************************
            /// UTableレンダリングブロック終了
            /// **************************************************
            mtbList.Render();

            #endregion
        }

        /// <summary>
        /// UTableレコード連番更新
        /// </summary>
        private void UTableNoRefresh()
        {
            using (mtbList.RenderBlock())
            {
                int no = 1;
                foreach (UTable.CRecord rec in this.mtbList.Content.Records)
                {
                    if (rec.Visible)        // 明細削除レコードは対象外
                    {
                        if (!ValChk.IsEmpty(rec.Fields["NO"].Value))
                        {
                            rec.Fields["NO"].Value = Util.ToString(no);
                            no++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }

        #endregion

        #region UTable privateメソッド(入力検査)

        /// <summary>
        /// 摘要コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTekiyoCd(UTable.CField field)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(field.Value)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はOK判定
            if (ValChk.IsEmpty(field.Value))
            {
                return true;
            }

            // 存在しないコードを入力されたらエラー
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_TEKIYO", this.ShishoCode.ToString(), Util.ToString(field.Value));
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 摘要の入力チェック
        /// </summary>
        private bool IsValidTekiyo(UTable.CField field)
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(field.Value, 40))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 証憑番号の入力チェック
        /// </summary>
        private bool IsValidShohyoBango(UTable.CField field)
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(field.Value, 40))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidKANJO_KAMOKU_CD(UTable.CField field)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(field.Value)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はOK判定
            if (ValChk.IsEmpty(Util.ToString(field.Value)))
            {
                return true;
            }

            // 存在しないコードを入力されたらエラー
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.ShishoCode.ToString(), Util.ToString(field.Value));
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 補助科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidHOJO_KAMOKU_CD(string kamokuCd, UTable.CField field)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(field.Value)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はOK判定
            if (ValChk.IsEmpty(field.Value))
            {
                return true;
            }

            // 存在しないコードを入力されたらエラー
            string name = this.GetHojoKmkNm(this.ShishoCode.ToString(), kamokuCd, Util.ToString(field.Value));
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidBUMON_CD(UTable.CField field)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(field.Value)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はOK判定
            if (ValChk.IsEmpty(field.Value))
            {
                return true;
            }

            // 存在しないコードを入力されたらエラー
            DataRow dr = GetBumonDataRow(Util.ToString(field.Value));
            if (dr == null)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 伝票金額の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidDENPYO_KINGAKU(UTable.CField field)
        {
            // 10桁数値
            if (!ValChk.IsDecNumWithinLength(
                Util.ToDecimal(Util.ToString(field.Value)), 10, 0, true))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税額の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidSHOHIZEI_KINGAKU(UTable.CField field)
        {
            // 10桁数値
            if (!ValChk.IsDecNumWithinLength(
                Util.ToDecimal(Util.ToString(field.Value)), 10, 0, true))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        ///// <summary>
        ///// 工事コードの入力チェック
        ///// </summary>
        ///// <returns>true:OK/false:NG</returns>
        //private bool IsValidKojiCd(UTable.CField field)
        //{
        //    // 数字のみの入力を許可
        //    if (!ValChk.IsNumber(Util.ToString(field.Value)))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    // 未入力はOK判定
        //    if (ValChk.IsEmpty(field.Value))
        //    {
        //        return true;
        //    }

        //    // 存在しないコードを入力されたらエラー
        //    string name = this.Dba.GetName(this.UInfo, "TB_ZM_KOJI", Util.ToString(field.Value));
        //    if (ValChk.IsEmpty(name))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    return true;
        //}

        ///// <summary>
        ///// 工種コードの入力チェック
        ///// </summary>
        ///// <returns>true:OK/false:NG</returns>
        //private bool IsValidKoshuCd(UTable.CField field)
        //{
        //    // 数字のみの入力を許可・未入力はNG
        //    if (!ValChk.IsNumber(Util.ToString(field.Value)))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    // 未入力はOK判定
        //    if (ValChk.IsEmpty(field.Value))
        //    {
        //        return true;
        //    }

        //    // 存在しないコードを入力されたらエラー
        //    string name = this.Dba.GetName(this.UInfo, "TB_ZM_KOSHU", Util.ToString(field.Value));  
        //    if (ValChk.IsEmpty(name))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    return true;
        //}

        /// <summary>
        /// UTable入力項目をチェック
        /// </summary>
        /// <returns></returns>
        private bool IsValidateUTableAll()
        {
            for (int i = 0; i < mtbList.Content.Records.Count; i++)
            {
                UTable.CRecord rec = mtbList.Content.Records[i];

                // 編集/追加レコードのチェック
                if ((EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Edit
                    || (EditMode)rec.Fields["EDIT_MODE"].Value == EditMode.Add)
                {
                    if (!IsValidateUTableRecord(rec))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// UTable.CRecord入力項目をチェック
        /// </summary>
        /// <param name="rec"></param>
        /// <returns></returns>
        private bool IsValidateUTableRecord(UTable.CRecord rec)
        {
            // 摘要コードの入力チェック
            if (!IsValidTekiyoCd(rec.Fields["TEKIYO_CD"]))
            {
                rec.Fields["TEKIYO_CD"].Focus();
                return false;
            }

            // 摘要の入力チェック
            if (!IsValidTekiyo(rec.Fields["TEKIYO"]))
            {
                rec.Fields["TEKIYO"].Focus();
                return false;
            }

            // 証憑番号の入力チェック
            if (!IsValidShohyoBango(rec.Fields["SHOHYO_BANGO"]))
            {
                rec.Fields["SHOHYO_BANGO"].Focus();
                return false;
            }

            // 単一仕訳の場合に以下をチェック（複合の場合は表示が無いし複合側でチェックを行う為）
            if ((JurnalType)rec.Fields["JURNAL_TYPE"].Value == JurnalType.Single)
            {
                // 勘定科目コードの入力チェック
                if (!IsValidKANJO_KAMOKU_CD(rec.Fields["KANJO_KAMOKU_CD"]))
                {
                    rec.Fields["KANJO_KAMOKU_CD"].Focus();
                    return false;
                }
                // 勘定科目コード省略不可チェック
                if (ValChk.IsEmpty(rec.Fields["KANJO_KAMOKU_CD"].Value))
                {
                    Msg.Info("省略できません。");
                    rec.Fields["KANJO_KAMOKU_CD"].Focus();
                    return false;
                }

                /// 勘定科目従属フィールド
                DataRow dr = GetKanjoKamokuDataRow(Util.ToString(rec.Fields["KANJO_KAMOKU_CD"].Value));
                // 補助科目コードの入力チェック
                if (!IsValidHOJO_KAMOKU_CD(Util.ToString(dr["KANJO_KAMOKU_CD"]), rec.Fields["HOJO_KAMOKU_CD"]))
                {
                    rec.Fields["HOJO_KAMOKU_CD"].Focus();
                    return false;
                }
                // 借方部門コードの入力チェック
                if (!IsValidBUMON_CD(rec.Fields["BUMON_CD"]))
                {
                    rec.Fields["BUMON_CD"].Focus();
                    return false;
                }
                // 補助科目省略不可チェック
                if (Util.ToString(dr["HOJO_KAMOKU_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["HOJO_KAMOKU_CD"].Value))
                {
                    Msg.Info("省略できません。");
                    rec.Fields["HOJO_KAMOKU_CD"].Focus();
                    return false;
                }
                // 部門コード省略不可チェック
                if (Util.ToString(dr["BUMON_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["BUMON_CD"].Value))
                {
                    Msg.Info("省略できません。");
                    rec.Fields["BUMON_CD"].Focus();
                    return false;
                }
            }


            //// 工事コード省略不可チェック
            //if (Util.ToString(dr["KOJI_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["KOJI_CD"].Value))
            //{
            //    Msg.Info("工事コードが存在しません。");
            //    rec.Fields["KOJI_CD"].Focus();
            //    return false;
            //}
            //// 工種コード省略不可チェック
            //if (Util.ToString(dr["KOJI_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["KOSHU_CD"].Value))
            //{
            //    Msg.Info("工種コードが存在しません。");
            //    rec.Fields["KOSHU_CD"].Focus();
            //    return false;
            //}

            /// 伝票金額
            // 借方伝票金額の入力チェック
            if (!IsValidDENPYO_KINGAKU(rec.Fields["DrDENPYO_KINGAKU"]))
            {
                rec.Fields["DrDENPYO_KINGAKU"].Focus();
                return false;
            }
            // 貸方伝票金額の入力チェック
            if (!IsValidDENPYO_KINGAKU(rec.Fields["CrDENPYO_KINGAKU"]))
            {
                rec.Fields["CrDENPYO_KINGAKU"].Focus();
                return false;
            }
            // 貸借とも伝票金額の未入力チェック
            if (Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)) == 0
                && Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)) == 0)
            {
                Msg.Info("省略できません。");
                rec.Fields["DrDENPYO_KINGAKU"].Focus();
                return false;
            }
            // 貸借とも伝票金額の入力済チェック
            if (Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)) != 0
                    && Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)) != 0)
            {
                Msg.Info("金額入力の誤りです。");
                rec.Fields["DrDENPYO_KINGAKU"].Focus();
                return false;
            }

            // 借方消費税金額の入力チェック
            if (!IsValidSHOHIZEI_KINGAKU(rec.Fields["DrSHOHIZEI_KINGAKU"]))
            {
                rec.Fields["DrSHOHIZEI_KINGAKU"].Focus();
                return false;
            }
            // 貸方消費税金額の入力チェック
            if (!IsValidSHOHIZEI_KINGAKU(rec.Fields["CrSHOHIZEI_KINGAKU"]))
            {
                rec.Fields["CrSHOHIZEI_KINGAKU"].Focus();
                return false;
            }

            //// 工事コードの入力チェック(入力値データ存在チェック：未入力はOK)
            //if (!IsValidKojiCd(rec.Fields["KOJI_CD"]))
            //{
            //    rec.Fields["KOJI_CD"].Focus();
            //    return false;
            //}
            //// 工種コードの入力チェック(入力値データ存在チェック：未入力はOK)
            //if (!IsValidKoshuCd(rec.Fields["KOSHU_CD"]))
            //{
            //    rec.Fields["KOSHU_CD"].Focus();
            //    return false;
            //}
            return true;
        }

        #endregion

    }
}
