﻿namespace jp.co.fsi.zm.zmde1011
{
    partial class ZMDE1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblDayDenpyoDateTo = new System.Windows.Forms.Label();
			this.txtDayDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoDateTo = new System.Windows.Forms.Label();
			this.lblMonthDenpyoDateTo = new System.Windows.Forms.Label();
			this.lblYearDenpyoDateTo = new System.Windows.Forms.Label();
			this.txtMonthDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoDenpyoDateTo = new System.Windows.Forms.Label();
			this.txtGengoYearDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDayDenpyoDateFr = new System.Windows.Forms.Label();
			this.txtDayDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMonthDenpyoDateFr = new System.Windows.Forms.Label();
			this.lblYearDenpyoDateFr = new System.Windows.Forms.Label();
			this.txtMonthDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoDenpyoDateFr = new System.Windows.Forms.Label();
			this.txtGengoYearDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblHojoKamokuNm = new System.Windows.Forms.Label();
			this.txtHojoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKanjoKamokuNm = new System.Windows.Forms.Label();
			this.txtKanjoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblBumonNm = new System.Windows.Forms.Label();
			this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTantoshaNm = new System.Windows.Forms.Label();
			this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblTantosha = new System.Windows.Forms.Label();
			this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
			this.lblHojoKamokuCd = new System.Windows.Forms.Label();
			this.lblBumonCd = new System.Windows.Forms.Label();
			this.lblDenpyoDate = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 609);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 32);
			this.lblTitle.Text = "入力範囲指定";
			// 
			// lblDayDenpyoDateTo
			// 
			this.lblDayDenpyoDateTo.AutoSize = true;
			this.lblDayDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
			this.lblDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDayDenpyoDateTo.Location = new System.Drawing.Point(594, 4);
			this.lblDayDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDayDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblDayDenpyoDateTo.Name = "lblDayDenpyoDateTo";
			this.lblDayDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
			this.lblDayDenpyoDateTo.TabIndex = 36;
			this.lblDayDenpyoDateTo.Tag = "CHANGE";
			this.lblDayDenpyoDateTo.Text = "日";
			this.lblDayDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDayDenpyoDateTo
			// 
			this.txtDayDenpyoDateTo.AutoSizeFromLength = false;
			this.txtDayDenpyoDateTo.DisplayLength = null;
			this.txtDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDayDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtDayDenpyoDateTo.Location = new System.Drawing.Point(559, 5);
			this.txtDayDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDayDenpyoDateTo.MaxLength = 2;
			this.txtDayDenpyoDateTo.Name = "txtDayDenpyoDateTo";
			this.txtDayDenpyoDateTo.Size = new System.Drawing.Size(28, 23);
			this.txtDayDenpyoDateTo.TabIndex = 35;
			this.txtDayDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayDenpyoDateTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDayDenpyoDateTo_KeyDown);
			this.txtDayDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateTo_Validating);
			// 
			// lblDenpyoDateTo
			// 
			this.lblDenpyoDateTo.AutoSize = true;
			this.lblDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDenpyoDateTo.Location = new System.Drawing.Point(335, 4);
			this.lblDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblDenpyoDateTo.Name = "lblDenpyoDateTo";
			this.lblDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
			this.lblDenpyoDateTo.TabIndex = 29;
			this.lblDenpyoDateTo.Tag = "CHANGE";
			this.lblDenpyoDateTo.Text = "～";
			this.lblDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMonthDenpyoDateTo
			// 
			this.lblMonthDenpyoDateTo.AutoSize = true;
			this.lblMonthDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
			this.lblMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMonthDenpyoDateTo.Location = new System.Drawing.Point(529, 4);
			this.lblMonthDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonthDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblMonthDenpyoDateTo.Name = "lblMonthDenpyoDateTo";
			this.lblMonthDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
			this.lblMonthDenpyoDateTo.TabIndex = 34;
			this.lblMonthDenpyoDateTo.Tag = "CHANGE";
			this.lblMonthDenpyoDateTo.Text = "月";
			this.lblMonthDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearDenpyoDateTo
			// 
			this.lblYearDenpyoDateTo.AutoSize = true;
			this.lblYearDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
			this.lblYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblYearDenpyoDateTo.Location = new System.Drawing.Point(465, 4);
			this.lblYearDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYearDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblYearDenpyoDateTo.Name = "lblYearDenpyoDateTo";
			this.lblYearDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
			this.lblYearDenpyoDateTo.TabIndex = 32;
			this.lblYearDenpyoDateTo.Tag = "CHANGE";
			this.lblYearDenpyoDateTo.Text = "年";
			this.lblYearDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMonthDenpyoDateTo
			// 
			this.txtMonthDenpyoDateTo.AutoSizeFromLength = false;
			this.txtMonthDenpyoDateTo.DisplayLength = null;
			this.txtMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMonthDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtMonthDenpyoDateTo.Location = new System.Drawing.Point(495, 5);
			this.txtMonthDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonthDenpyoDateTo.MaxLength = 2;
			this.txtMonthDenpyoDateTo.Name = "txtMonthDenpyoDateTo";
			this.txtMonthDenpyoDateTo.Size = new System.Drawing.Size(28, 23);
			this.txtMonthDenpyoDateTo.TabIndex = 33;
			this.txtMonthDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateTo_Validating);
			// 
			// lblGengoDenpyoDateTo
			// 
			this.lblGengoDenpyoDateTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoDenpyoDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblGengoDenpyoDateTo.Location = new System.Drawing.Point(379, 4);
			this.lblGengoDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengoDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblGengoDenpyoDateTo.Name = "lblGengoDenpyoDateTo";
			this.lblGengoDenpyoDateTo.Size = new System.Drawing.Size(53, 24);
			this.lblGengoDenpyoDateTo.TabIndex = 30;
			this.lblGengoDenpyoDateTo.Text = "平成";
			this.lblGengoDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtGengoYearDenpyoDateTo
			// 
			this.txtGengoYearDenpyoDateTo.AutoSizeFromLength = false;
			this.txtGengoYearDenpyoDateTo.DisplayLength = null;
			this.txtGengoYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtGengoYearDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtGengoYearDenpyoDateTo.Location = new System.Drawing.Point(435, 5);
			this.txtGengoYearDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtGengoYearDenpyoDateTo.MaxLength = 2;
			this.txtGengoYearDenpyoDateTo.Name = "txtGengoYearDenpyoDateTo";
			this.txtGengoYearDenpyoDateTo.Size = new System.Drawing.Size(28, 23);
			this.txtGengoYearDenpyoDateTo.TabIndex = 31;
			this.txtGengoYearDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYearDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateTo_Validating);
			// 
			// lblDayDenpyoDateFr
			// 
			this.lblDayDenpyoDateFr.AutoSize = true;
			this.lblDayDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
			this.lblDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDayDenpyoDateFr.Location = new System.Drawing.Point(299, 4);
			this.lblDayDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDayDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblDayDenpyoDateFr.Name = "lblDayDenpyoDateFr";
			this.lblDayDenpyoDateFr.Size = new System.Drawing.Size(24, 24);
			this.lblDayDenpyoDateFr.TabIndex = 28;
			this.lblDayDenpyoDateFr.Tag = "CHANGE";
			this.lblDayDenpyoDateFr.Text = "日";
			this.lblDayDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDayDenpyoDateFr
			// 
			this.txtDayDenpyoDateFr.AutoSizeFromLength = false;
			this.txtDayDenpyoDateFr.DisplayLength = null;
			this.txtDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDayDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtDayDenpyoDateFr.Location = new System.Drawing.Point(265, 5);
			this.txtDayDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDayDenpyoDateFr.MaxLength = 2;
			this.txtDayDenpyoDateFr.Name = "txtDayDenpyoDateFr";
			this.txtDayDenpyoDateFr.Size = new System.Drawing.Size(28, 23);
			this.txtDayDenpyoDateFr.TabIndex = 27;
			this.txtDayDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateFr_Validating);
			// 
			// lblMonthDenpyoDateFr
			// 
			this.lblMonthDenpyoDateFr.AutoSize = true;
			this.lblMonthDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
			this.lblMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMonthDenpyoDateFr.Location = new System.Drawing.Point(234, 4);
			this.lblMonthDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonthDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblMonthDenpyoDateFr.Name = "lblMonthDenpyoDateFr";
			this.lblMonthDenpyoDateFr.Size = new System.Drawing.Size(24, 24);
			this.lblMonthDenpyoDateFr.TabIndex = 26;
			this.lblMonthDenpyoDateFr.Tag = "CHANGE";
			this.lblMonthDenpyoDateFr.Text = "月";
			this.lblMonthDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearDenpyoDateFr
			// 
			this.lblYearDenpyoDateFr.AutoSize = true;
			this.lblYearDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
			this.lblYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblYearDenpyoDateFr.Location = new System.Drawing.Point(170, 4);
			this.lblYearDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYearDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblYearDenpyoDateFr.Name = "lblYearDenpyoDateFr";
			this.lblYearDenpyoDateFr.Size = new System.Drawing.Size(24, 24);
			this.lblYearDenpyoDateFr.TabIndex = 24;
			this.lblYearDenpyoDateFr.Tag = "CHANGE";
			this.lblYearDenpyoDateFr.Text = "年";
			this.lblYearDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMonthDenpyoDateFr
			// 
			this.txtMonthDenpyoDateFr.AutoSizeFromLength = false;
			this.txtMonthDenpyoDateFr.DisplayLength = null;
			this.txtMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMonthDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtMonthDenpyoDateFr.Location = new System.Drawing.Point(201, 5);
			this.txtMonthDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonthDenpyoDateFr.MaxLength = 2;
			this.txtMonthDenpyoDateFr.Name = "txtMonthDenpyoDateFr";
			this.txtMonthDenpyoDateFr.Size = new System.Drawing.Size(28, 23);
			this.txtMonthDenpyoDateFr.TabIndex = 25;
			this.txtMonthDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateFr_Validating);
			// 
			// lblGengoDenpyoDateFr
			// 
			this.lblGengoDenpyoDateFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoDenpyoDateFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblGengoDenpyoDateFr.Location = new System.Drawing.Point(85, 4);
			this.lblGengoDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengoDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblGengoDenpyoDateFr.Name = "lblGengoDenpyoDateFr";
			this.lblGengoDenpyoDateFr.Size = new System.Drawing.Size(53, 24);
			this.lblGengoDenpyoDateFr.TabIndex = 22;
			this.lblGengoDenpyoDateFr.Text = "平成";
			this.lblGengoDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtGengoYearDenpyoDateFr
			// 
			this.txtGengoYearDenpyoDateFr.AutoSizeFromLength = false;
			this.txtGengoYearDenpyoDateFr.DisplayLength = null;
			this.txtGengoYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtGengoYearDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtGengoYearDenpyoDateFr.Location = new System.Drawing.Point(141, 5);
			this.txtGengoYearDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtGengoYearDenpyoDateFr.MaxLength = 2;
			this.txtGengoYearDenpyoDateFr.Name = "txtGengoYearDenpyoDateFr";
			this.txtGengoYearDenpyoDateFr.Size = new System.Drawing.Size(28, 23);
			this.txtGengoYearDenpyoDateFr.TabIndex = 23;
			this.txtGengoYearDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYearDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateFr_Validating);
			// 
			// lblHojoKamokuNm
			// 
			this.lblHojoKamokuNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblHojoKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblHojoKamokuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblHojoKamokuNm.ForeColor = System.Drawing.Color.Black;
			this.lblHojoKamokuNm.Location = new System.Drawing.Point(157, 2);
			this.lblHojoKamokuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblHojoKamokuNm.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblHojoKamokuNm.Name = "lblHojoKamokuNm";
			this.lblHojoKamokuNm.Size = new System.Drawing.Size(475, 24);
			this.lblHojoKamokuNm.TabIndex = 16;
			this.lblHojoKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtHojoKamokuCd
			// 
			this.txtHojoKamokuCd.AutoSizeFromLength = false;
			this.txtHojoKamokuCd.BackColor = System.Drawing.SystemColors.Window;
			this.txtHojoKamokuCd.DisplayLength = null;
			this.txtHojoKamokuCd.Enabled = false;
			this.txtHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtHojoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtHojoKamokuCd.Location = new System.Drawing.Point(85, 3);
			this.txtHojoKamokuCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtHojoKamokuCd.MaxLength = 4;
			this.txtHojoKamokuCd.Name = "txtHojoKamokuCd";
			this.txtHojoKamokuCd.Size = new System.Drawing.Size(69, 23);
			this.txtHojoKamokuCd.TabIndex = 15;
			this.txtHojoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtHojoKamokuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoKamokuCd_Validating);
			// 
			// lblKanjoKamokuNm
			// 
			this.lblKanjoKamokuNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblKanjoKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanjoKamokuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKanjoKamokuNm.ForeColor = System.Drawing.Color.Black;
			this.lblKanjoKamokuNm.Location = new System.Drawing.Point(157, 2);
			this.lblKanjoKamokuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanjoKamokuNm.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblKanjoKamokuNm.Name = "lblKanjoKamokuNm";
			this.lblKanjoKamokuNm.Size = new System.Drawing.Size(475, 24);
			this.lblKanjoKamokuNm.TabIndex = 12;
			this.lblKanjoKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKanjoKamokuCd
			// 
			this.txtKanjoKamokuCd.AutoSizeFromLength = false;
			this.txtKanjoKamokuCd.BackColor = System.Drawing.SystemColors.Window;
			this.txtKanjoKamokuCd.DisplayLength = null;
			this.txtKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKanjoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKanjoKamokuCd.Location = new System.Drawing.Point(85, 3);
			this.txtKanjoKamokuCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuCd.MaxLength = 6;
			this.txtKanjoKamokuCd.Name = "txtKanjoKamokuCd";
			this.txtKanjoKamokuCd.Size = new System.Drawing.Size(69, 23);
			this.txtKanjoKamokuCd.TabIndex = 11;
			this.txtKanjoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKanjoKamokuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCd_Validating);
			// 
			// lblBumonNm
			// 
			this.lblBumonNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblBumonNm.ForeColor = System.Drawing.Color.Black;
			this.lblBumonNm.Location = new System.Drawing.Point(158, 2);
			this.lblBumonNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBumonNm.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblBumonNm.Name = "lblBumonNm";
			this.lblBumonNm.Size = new System.Drawing.Size(475, 24);
			this.lblBumonNm.TabIndex = 19;
			this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBumonCd
			// 
			this.txtBumonCd.AutoSizeFromLength = false;
			this.txtBumonCd.BackColor = System.Drawing.SystemColors.Window;
			this.txtBumonCd.DisplayLength = null;
			this.txtBumonCd.Enabled = false;
			this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtBumonCd.Location = new System.Drawing.Point(85, 3);
			this.txtBumonCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtBumonCd.MaxLength = 4;
			this.txtBumonCd.Name = "txtBumonCd";
			this.txtBumonCd.Size = new System.Drawing.Size(69, 23);
			this.txtBumonCd.TabIndex = 18;
			this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
			// 
			// lblTantoshaNm
			// 
			this.lblTantoshaNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTantoshaNm.ForeColor = System.Drawing.Color.Black;
			this.lblTantoshaNm.Location = new System.Drawing.Point(157, 3);
			this.lblTantoshaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaNm.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblTantoshaNm.Name = "lblTantoshaNm";
			this.lblTantoshaNm.Size = new System.Drawing.Size(475, 24);
			this.lblTantoshaNm.TabIndex = 8;
			this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTantoshaCd
			// 
			this.txtTantoshaCd.AutoSizeFromLength = false;
			this.txtTantoshaCd.BackColor = System.Drawing.Color.WhiteSmoke;
			this.txtTantoshaCd.DisplayLength = null;
			this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtTantoshaCd.Location = new System.Drawing.Point(85, 4);
			this.txtTantoshaCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtTantoshaCd.MaxLength = 4;
			this.txtTantoshaCd.Name = "txtTantoshaCd";
			this.txtTantoshaCd.Size = new System.Drawing.Size(69, 23);
			this.txtTantoshaCd.TabIndex = 7;
			this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(652, 31);
			this.lblMizuageShisho.TabIndex = 2;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = false;
			this.txtMizuageShishoCd.BackColor = System.Drawing.SystemColors.Window;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(85, 3);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(69, 23);
			this.txtMizuageShishoCd.TabIndex = 3;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMizuageShishoNm.ForeColor = System.Drawing.Color.Black;
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(157, 2);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(475, 24);
			this.lblMizuageShishoNm.TabIndex = 4;
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTantosha
			// 
			this.lblTantosha.BackColor = System.Drawing.Color.Silver;
			this.lblTantosha.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantosha.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTantosha.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantosha.Location = new System.Drawing.Point(0, 0);
			this.lblTantosha.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantosha.Name = "lblTantosha";
			this.lblTantosha.Size = new System.Drawing.Size(652, 31);
			this.lblTantosha.TabIndex = 5;
			this.lblTantosha.Tag = "CHANGE";
			this.lblTantosha.Text = "担当者";
			this.lblTantosha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKanjoKamokuCd
			// 
			this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.Silver;
			this.lblKanjoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanjoKamokuCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKanjoKamokuCd.Location = new System.Drawing.Point(0, 0);
			this.lblKanjoKamokuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
			this.lblKanjoKamokuCd.Size = new System.Drawing.Size(652, 31);
			this.lblKanjoKamokuCd.TabIndex = 10;
			this.lblKanjoKamokuCd.Tag = "CHANGE";
			this.lblKanjoKamokuCd.Text = "勘定科目";
			this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblHojoKamokuCd
			// 
			this.lblHojoKamokuCd.BackColor = System.Drawing.Color.Silver;
			this.lblHojoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblHojoKamokuCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblHojoKamokuCd.Location = new System.Drawing.Point(0, 0);
			this.lblHojoKamokuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblHojoKamokuCd.Name = "lblHojoKamokuCd";
			this.lblHojoKamokuCd.Size = new System.Drawing.Size(652, 31);
			this.lblHojoKamokuCd.TabIndex = 14;
			this.lblHojoKamokuCd.Tag = "CHANGE";
			this.lblHojoKamokuCd.Text = "補助科目";
			this.lblHojoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblBumonCd
			// 
			this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
			this.lblBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBumonCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBumonCd.Location = new System.Drawing.Point(0, 0);
			this.lblBumonCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBumonCd.Name = "lblBumonCd";
			this.lblBumonCd.Size = new System.Drawing.Size(652, 31);
			this.lblBumonCd.TabIndex = 17;
			this.lblBumonCd.Tag = "CHANGE";
			this.lblBumonCd.Text = "部門";
			this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDenpyoDate
			// 
			this.lblDenpyoDate.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDenpyoDate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoDate.Location = new System.Drawing.Point(0, 0);
			this.lblDenpyoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoDate.Name = "lblDenpyoDate";
			this.lblDenpyoDate.Size = new System.Drawing.Size(652, 36);
			this.lblDenpyoDate.TabIndex = 20;
			this.lblDenpyoDate.Tag = "CHANGE";
			this.lblDenpyoDate.Text = "伝票日付";
			this.lblDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 44);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 6;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(660, 234);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.lblGengoDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.lblDayDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.txtGengoYearDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.txtDayDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.txtMonthDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.lblDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblYearDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.lblMonthDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblMonthDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.lblYearDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.txtDayDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.txtMonthDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblDayDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.lblGengoDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.txtGengoYearDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblDenpyoDate);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel6.Location = new System.Drawing.Point(4, 194);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(652, 36);
			this.fsiPanel6.TabIndex = 5;
			this.fsiPanel6.Tag = "CHANGE";
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.txtBumonCd);
			this.fsiPanel5.Controls.Add(this.lblBumonNm);
			this.fsiPanel5.Controls.Add(this.lblBumonCd);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 156);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(652, 31);
			this.fsiPanel5.TabIndex = 4;
			this.fsiPanel5.Tag = "CHANGE";
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtHojoKamokuCd);
			this.fsiPanel4.Controls.Add(this.lblHojoKamokuNm);
			this.fsiPanel4.Controls.Add(this.lblHojoKamokuCd);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 118);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(652, 31);
			this.fsiPanel4.TabIndex = 3;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtKanjoKamokuCd);
			this.fsiPanel3.Controls.Add(this.lblKanjoKamokuNm);
			this.fsiPanel3.Controls.Add(this.lblKanjoKamokuCd);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 80);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(652, 31);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtTantoshaCd);
			this.fsiPanel2.Controls.Add(this.lblTantoshaNm);
			this.fsiPanel2.Controls.Add(this.lblTantosha);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 42);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(652, 31);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(652, 31);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// ZMDE1012
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "ZMDE1012";
			this.Text = "入力範囲指定";
			this.Shown += new System.EventHandler(this.ZMDE1012_Shown);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblDayDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateTo;
        private System.Windows.Forms.Label lblDenpyoDateTo;
        private System.Windows.Forms.Label lblMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblYearDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblGengoDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateTo;
        private System.Windows.Forms.Label lblDayDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateFr;
        private System.Windows.Forms.Label lblMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblYearDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblGengoDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateFr;
        private System.Windows.Forms.Label lblHojoKamokuNm;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuCd;
        private System.Windows.Forms.Label lblKanjoKamokuNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCd;
        private System.Windows.Forms.Label lblBumonNm;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblTantosha;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private System.Windows.Forms.Label lblHojoKamokuCd;
        private System.Windows.Forms.Label lblBumonCd;
        private System.Windows.Forms.Label lblDenpyoDate;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}