﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1033
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblTorihikisakiCd = new System.Windows.Forms.Label();
            this.txtTorihikisakiCd = new System.Windows.Forms.TextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblTorihikisakiNm = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblSaikenTotal = new System.Windows.Forms.Label();
            this.lblSaimuTotal = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 609);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "元帳照会";
            // 
            // lblTorihikisakiCd
            // 
            this.lblTorihikisakiCd.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikisakiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikisakiCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTorihikisakiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikisakiCd.Location = new System.Drawing.Point(0, 0);
            this.lblTorihikisakiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTorihikisakiCd.Name = "lblTorihikisakiCd";
            this.lblTorihikisakiCd.Size = new System.Drawing.Size(262, 36);
            this.lblTorihikisakiCd.TabIndex = 1;
            this.lblTorihikisakiCd.Tag = "CHANGE";
            this.lblTorihikisakiCd.Text = "取引先";
            this.lblTorihikisakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTorihikisakiCd
            // 
            this.txtTorihikisakiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTorihikisakiCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTorihikisakiCd.Location = new System.Drawing.Point(115, 7);
            this.txtTorihikisakiCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTorihikisakiCd.MaxLength = 4;
            this.txtTorihikisakiCd.Name = "txtTorihikisakiCd";
            this.txtTorihikisakiCd.Size = new System.Drawing.Size(60, 23);
            this.txtTorihikisakiCd.TabIndex = 2;
            this.txtTorihikisakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikisakiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikisakiCd_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvList.EnableHeadersVisualStyles = false;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(0, 0);
            this.dgvList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(889, 330);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.Enter += new System.EventHandler(this.dgvList_Enter);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // lblTorihikisakiNm
            // 
            this.lblTorihikisakiNm.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikisakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikisakiNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTorihikisakiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikisakiNm.Location = new System.Drawing.Point(0, 0);
            this.lblTorihikisakiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTorihikisakiNm.Name = "lblTorihikisakiNm";
            this.lblTorihikisakiNm.Size = new System.Drawing.Size(627, 36);
            this.lblTorihikisakiNm.TabIndex = 902;
            this.lblTorihikisakiNm.Tag = "CHANGE";
            this.lblTorihikisakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.White;
            this.lblTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotal.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTotal.Location = new System.Drawing.Point(0, 0);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(304, 36);
            this.lblTotal.TabIndex = 903;
            this.lblTotal.Text = "合計";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSaikenTotal
            // 
            this.lblSaikenTotal.BackColor = System.Drawing.Color.LightCyan;
            this.lblSaikenTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSaikenTotal.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSaikenTotal.Location = new System.Drawing.Point(0, 0);
            this.lblSaikenTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSaikenTotal.Name = "lblSaikenTotal";
            this.lblSaikenTotal.Size = new System.Drawing.Size(350, 36);
            this.lblSaikenTotal.TabIndex = 904;
            this.lblSaikenTotal.Text = "1,234,567,890,123";
            this.lblSaikenTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSaimuTotal
            // 
            this.lblSaimuTotal.BackColor = System.Drawing.Color.LightCyan;
            this.lblSaimuTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSaimuTotal.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSaimuTotal.Location = new System.Drawing.Point(0, 0);
            this.lblSaimuTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSaimuTotal.Name = "lblSaimuTotal";
            this.lblSaimuTotal.Size = new System.Drawing.Size(235, 36);
            this.lblSaimuTotal.TabIndex = 905;
            this.lblSaimuTotal.Text = "1,234,567,890,123";
            this.lblSaimuTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 2);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 53);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(897, 424);
            this.fsiTableLayoutPanel1.TabIndex = 906;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.dgvList);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 47);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(889, 330);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel4);
            this.fsiPanel1.Controls.Add(this.fsiPanel3);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(889, 36);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblTorihikisakiNm);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(262, 0);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(627, 36);
            this.fsiPanel4.TabIndex = 1;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtTorihikisakiCd);
            this.fsiPanel3.Controls.Add(this.lblTorihikisakiCd);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel3.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(262, 36);
            this.fsiPanel3.TabIndex = 1;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.fsiPanel8);
            this.fsiPanel5.Controls.Add(this.fsiPanel7);
            this.fsiPanel5.Controls.Add(this.fsiPanel6);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 384);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(889, 36);
            this.fsiPanel5.TabIndex = 2;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.lblSaikenTotal);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(304, 0);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(350, 36);
            this.fsiPanel8.TabIndex = 5;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.lblTotal);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel7.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(304, 36);
            this.fsiPanel7.TabIndex = 4;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.lblSaimuTotal);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel6.Location = new System.Drawing.Point(654, 0);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(235, 36);
            this.fsiPanel6.TabIndex = 3;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // ZMDE1033
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 745);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.MaximizeBox = true;
            this.Name = "ZMDE1033";
            this.ShowFButton = true;
            this.Text = "元帳照会";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTorihikisakiCd;
        private System.Windows.Forms.TextBox txtTorihikisakiCd;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblTorihikisakiNm;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblSaikenTotal;
        private System.Windows.Forms.Label lblSaimuTotal;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
    }
}