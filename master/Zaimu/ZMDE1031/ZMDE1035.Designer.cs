﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1035
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTekiyoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShiwakeNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.chkOpt3 = new System.Windows.Forms.CheckBox();
            this.chkOpt2 = new System.Windows.Forms.CheckBox();
            this.chkOpt1 = new System.Windows.Forms.CheckBox();
            this.txtTekiyoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTekiyoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 313);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(578, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(568, 41);
            this.lblTitle.Text = "仕訳事例の登録";
            // 
            // txtTekiyoCd
            // 
            this.txtTekiyoCd.AutoSizeFromLength = false;
            this.txtTekiyoCd.DisplayLength = null;
            this.txtTekiyoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtTekiyoCd.ForeColor = System.Drawing.Color.Black;
            this.txtTekiyoCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTekiyoCd.Location = new System.Drawing.Point(89, 7);
            this.txtTekiyoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTekiyoCd.MaxLength = 4;
            this.txtTekiyoCd.Name = "txtTekiyoCd";
            this.txtTekiyoCd.Size = new System.Drawing.Size(47, 23);
            this.txtTekiyoCd.TabIndex = 4;
            this.txtTekiyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
            // 
            // txtShiwakeNm
            // 
            this.txtShiwakeNm.AutoSizeFromLength = false;
            this.txtShiwakeNm.DisplayLength = null;
            this.txtShiwakeNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtShiwakeNm.ForeColor = System.Drawing.Color.Black;
            this.txtShiwakeNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtShiwakeNm.Location = new System.Drawing.Point(89, 6);
            this.txtShiwakeNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiwakeNm.MaxLength = 40;
            this.txtShiwakeNm.Name = "txtShiwakeNm";
            this.txtShiwakeNm.Size = new System.Drawing.Size(393, 23);
            this.txtShiwakeNm.TabIndex = 6;
            this.txtShiwakeNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiwakeNm_Validating);
            // 
            // txtShiwakeCd
            // 
            this.txtShiwakeCd.AutoSizeFromLength = false;
            this.txtShiwakeCd.DisplayLength = null;
            this.txtShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtShiwakeCd.ForeColor = System.Drawing.Color.Black;
            this.txtShiwakeCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShiwakeCd.Location = new System.Drawing.Point(89, 6);
            this.txtShiwakeCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiwakeCd.MaxLength = 4;
            this.txtShiwakeCd.Name = "txtShiwakeCd";
            this.txtShiwakeCd.Size = new System.Drawing.Size(47, 23);
            this.txtShiwakeCd.TabIndex = 4;
            this.txtShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiwakeCd_Validating);
            // 
            // chkOpt3
            // 
            this.chkOpt3.AutoSize = true;
            this.chkOpt3.BackColor = System.Drawing.Color.Silver;
            this.chkOpt3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.chkOpt3.Location = new System.Drawing.Point(383, 8);
            this.chkOpt3.Margin = new System.Windows.Forms.Padding(4);
            this.chkOpt3.MinimumSize = new System.Drawing.Size(0, 24);
            this.chkOpt3.Name = "chkOpt3";
            this.chkOpt3.Size = new System.Drawing.Size(139, 24);
            this.chkOpt3.TabIndex = 2;
            this.chkOpt3.Tag = "CHANGE";
            this.chkOpt3.Text = "金額を保存する";
            this.chkOpt3.UseVisualStyleBackColor = false;
            // 
            // chkOpt2
            // 
            this.chkOpt2.AutoSize = true;
            this.chkOpt2.BackColor = System.Drawing.Color.Silver;
            this.chkOpt2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.chkOpt2.Location = new System.Drawing.Point(236, 8);
            this.chkOpt2.Margin = new System.Windows.Forms.Padding(4);
            this.chkOpt2.MinimumSize = new System.Drawing.Size(0, 24);
            this.chkOpt2.Name = "chkOpt2";
            this.chkOpt2.Size = new System.Drawing.Size(139, 24);
            this.chkOpt2.TabIndex = 1;
            this.chkOpt2.Tag = "CHANGE";
            this.chkOpt2.Text = "補助を保存する";
            this.chkOpt2.UseVisualStyleBackColor = false;
            // 
            // chkOpt1
            // 
            this.chkOpt1.AutoSize = true;
            this.chkOpt1.BackColor = System.Drawing.Color.Silver;
            this.chkOpt1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.chkOpt1.Location = new System.Drawing.Point(89, 8);
            this.chkOpt1.Margin = new System.Windows.Forms.Padding(4);
            this.chkOpt1.MinimumSize = new System.Drawing.Size(0, 24);
            this.chkOpt1.Name = "chkOpt1";
            this.chkOpt1.Size = new System.Drawing.Size(139, 24);
            this.chkOpt1.TabIndex = 0;
            this.chkOpt1.Tag = "CHANGE";
            this.chkOpt1.Text = "部門を保存する";
            this.chkOpt1.UseVisualStyleBackColor = false;
            // 
            // txtTekiyoNm
            // 
            this.txtTekiyoNm.AutoSizeFromLength = false;
            this.txtTekiyoNm.DisplayLength = null;
            this.txtTekiyoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtTekiyoNm.ForeColor = System.Drawing.Color.Black;
            this.txtTekiyoNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTekiyoNm.Location = new System.Drawing.Point(89, 6);
            this.txtTekiyoNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtTekiyoNm.MaxLength = 40;
            this.txtTekiyoNm.Name = "txtTekiyoNm";
            this.txtTekiyoNm.Size = new System.Drawing.Size(393, 23);
            this.txtTekiyoNm.TabIndex = 6;
            // 
            // txtTekiyoKanaNm
            // 
            this.txtTekiyoKanaNm.AutoSizeFromLength = false;
            this.txtTekiyoKanaNm.DisplayLength = null;
            this.txtTekiyoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtTekiyoKanaNm.ForeColor = System.Drawing.Color.Black;
            this.txtTekiyoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtTekiyoKanaNm.Location = new System.Drawing.Point(89, 6);
            this.txtTekiyoKanaNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtTekiyoKanaNm.MaxLength = 30;
            this.txtTekiyoKanaNm.Name = "txtTekiyoKanaNm";
            this.txtTekiyoKanaNm.Size = new System.Drawing.Size(393, 23);
            this.txtTekiyoKanaNm.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(524, 36);
            this.label3.TabIndex = 3;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "摘要コード";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel9, 0, 5);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel8, 0, 4);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel7, 0, 3);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel6, 0, 2);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel5, 0, 1);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel4, 0, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(4, 44);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 6;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(532, 262);
            this.fsiTableLayoutPanel2.TabIndex = 903;
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.chkOpt1);
            this.fsiPanel9.Controls.Add(this.chkOpt2);
            this.fsiPanel9.Controls.Add(this.chkOpt3);
            this.fsiPanel9.Controls.Add(this.label14);
            this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel9.Location = new System.Drawing.Point(4, 219);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(524, 39);
            this.fsiPanel9.TabIndex = 909;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Silver;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(524, 39);
            this.label14.TabIndex = 4;
            this.label14.Tag = "CHANGE";
            this.label14.Text = "保存指定";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.txtShiwakeNm);
            this.fsiPanel8.Controls.Add(this.label13);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(4, 176);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(524, 36);
            this.fsiPanel8.TabIndex = 908;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Silver;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(524, 36);
            this.label13.TabIndex = 4;
            this.label13.Tag = "CHANGE";
            this.label13.Text = "仕訳名称";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.txtShiwakeCd);
            this.fsiPanel7.Controls.Add(this.label12);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(4, 133);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(524, 36);
            this.fsiPanel7.TabIndex = 907;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Silver;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(524, 36);
            this.label12.TabIndex = 4;
            this.label12.Tag = "CHANGE";
            this.label12.Text = "仕訳コード";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtTekiyoKanaNm);
            this.fsiPanel6.Controls.Add(this.label11);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 90);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(524, 36);
            this.fsiPanel6.TabIndex = 906;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Silver;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label11.Location = new System.Drawing.Point(0, 0);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(524, 36);
            this.label11.TabIndex = 4;
            this.label11.Tag = "CHANGE";
            this.label11.Text = "摘要カナ名";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtTekiyoNm);
            this.fsiPanel5.Controls.Add(this.label10);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 47);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(524, 36);
            this.fsiPanel5.TabIndex = 905;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Silver;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(524, 36);
            this.label10.TabIndex = 4;
            this.label10.Tag = "CHANGE";
            this.label10.Text = "摘要名";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtTekiyoCd);
            this.fsiPanel4.Controls.Add(this.label3);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(524, 36);
            this.fsiPanel4.TabIndex = 904;
            // 
            // ZMDE1035
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 449);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMDE1035";
            this.ShowFButton = true;
            this.Text = "仕訳事例の登録";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel9.PerformLayout();
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShiwakeNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShiwakeCd;
        private System.Windows.Forms.CheckBox chkOpt3;
        private System.Windows.Forms.CheckBox chkOpt2;
        private System.Windows.Forms.CheckBox chkOpt1;
        private System.Windows.Forms.Label label3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.controls.FsiTextBox txtTekiyoNm;
        private common.controls.FsiTextBox txtTekiyoKanaNm;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
    }
}