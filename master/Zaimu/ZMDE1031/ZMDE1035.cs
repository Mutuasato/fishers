﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmde1031
{
    /// <summary>
    /// 仕訳事例の登録(ZMDE1035)
    /// </summary>
    public partial class ZMDE1035 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region 変数
        private string _tekiyoCd;       // 摘要コード
        private string _tekiyoNm;       // 摘要名称
        private DataTable _dtJirei;     // 仕訳事例データテーブル

        // クラス変数
        private string _mode;           // 新規・編集モード(起動時は常に新規モード)

        // 支所コード
        private int ShishoCode;
        #endregion

        #region プロパティ

        /// <summary>
        /// 摘要コード
        /// </summary>
        public string TekiyoCd
        {
            get
            {
                return _tekiyoCd;
            }
            set
            {
                _tekiyoCd = Util.ToString(value);
            }
        }

        /// <summary>
        /// 摘要名称
        /// </summary>
        public string TekiyoNm
        {
            get
            {
                return _tekiyoNm;
            }
            set
            {
                _tekiyoNm = Util.ToString(value);
            }
        }

        /// <summary>
        /// 仕訳事例データテーブル
        /// </summary>
        public DataTable DtJirei
        {
            get
            {
                return _dtJirei;
            }
            set
            {
                _dtJirei = value;
            }
        }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDE1035()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            // サイズを縮める
            this.Size = new Size(588, 492);
            // ESC F1 F3 F6 のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Location = this.btnF3.Location;
            this.btnF3.Enabled = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.ShowFButton = true;

            // 初期表示
            InitDisp();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtTekiyoCd":
                case "txtShiwakeCd":
                    this.btnF1.Enabled = true;          // 検索
                    break;
                default:
                    this.btnF1.Enabled = false;         // 検索
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // ダイアログとしての処理結果を返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			//アクティブコントロールごとの処理
			switch (this.ActiveCtlNm)
            {
                case "txtTekiyoCd":
                    #region 摘要検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9031.exe");
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1051.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9031.ZAMC9031");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1051.ZMCM1051");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtTekiyoCd.Text = result[0];

                                TekiyoDataLoad(this.txtTekiyoCd.Text);
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtShiwakeCd":
                    #region 仕訳検索
                    //// アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9531.exe");
                    //// フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9531.ZAMC9531");
                    //if (t != null)
                    //{
                    //    Object obj = System.Activator.CreateInstance(t);
                    //    if (obj != null)
                    //    {
                    //        // タブの一部として埋め込む
                    //        BasePgForm frm = (BasePgForm)obj;
                    //        frm.Par1 = "1";
                    //        frm.InData = this.txtTekiyoCd.Text;
                    //        frm.ShowDialog(this);

                    //        if (frm.DialogResult == DialogResult.OK)
                    //        {
                    //            string[] result = (string[])frm.OutData;
                    //            this.txtShiwakeCd.Text = result[0];
                    //            ShiwakeJireiDataLoad(this.txtTekiyoCd.Text, this.txtShiwakeCd.Text);
                    //        }
                    //    }
                    //}
                    using (ZMDE1036 frm1036 = new ZMDE1036())
                    {
                        string[] args = new string[2];
                        args[0] = this.txtTekiyoCd.Text;
                        frm1036.InData = args;
                        //frm1036.Par1 = "1";
                        frm1036.Par1 = "2"; // 自動検索モード
                        frm1036.ShowDialog(this);
                        if (frm1036.DialogResult == DialogResult.OK)
                        {
                            string[] result = (string[])frm1036.OutData;
                            this.txtShiwakeCd.Text = result[0];
                            ShiwakeJireiDataLoad(this.txtTekiyoCd.Text, this.txtShiwakeCd.Text);
                        }
                    }
                    #endregion
                    break;
                default:
                    // NONE
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void  PressF3()
        {
            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCd.Text);
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 4, this.txtShiwakeCd.Text);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, this.UInfo.KaikeiNendo);
            string where = "KAISHA_CD = @KAISHA_CD"
                        + " AND SHISHO_CD = @SHISHO_CD"
                        + " AND TEKIYO_CD = @TEKIYO_CD"
                        + " AND SHIWAKE_CD = @SHIWAKE_CD"
                        + " AND KAIKEI_NENDO = @KAIKEI_NENDO";
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // データ削除
                this.Dba.Delete("TB_ZM_SHIWAKE_JIREI", where, dpc);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this._mode) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 摘要データの更新準備
            string modeTekiyo = this.Dba.GetName(this.UInfo, "TB_ZM_TEKIYO", this.ShishoCode.ToString(), this.txtTekiyoCd.Text) == null ?
                                        MODE_NEW : MODE_EDIT;
            ArrayList alParamsZmTekiyo = SetZmTekiyoParams(modeTekiyo);

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 摘要データのINSERT/UPDATE
                if (MODE_NEW.Equals(modeTekiyo))
                {
                    this.Dba.Insert("TB_ZM_TEKIYO", (DbParamCollection)alParamsZmTekiyo[0]);
                }
                else
                {
                    this.Dba.Update("TB_ZM_TEKIYO", 
                        (DbParamCollection)alParamsZmTekiyo[1],
                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND TEKIYO_CD = @TEKIYO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                        (DbParamCollection)alParamsZmTekiyo[0]);
                }

                /// (UPDATE⇒DELETE＆INSERT)
                // 事例データのDELETE
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCd.Text);
                dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 4, this.txtShiwakeCd.Text);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, this.UInfo.KaikeiNendo);
                string where = "KAISHA_CD = @KAISHA_CD"
                            + " AND SHISHO_CD = @SHISHO_CD"
                            + " AND TEKIYO_CD = @TEKIYO_CD"
                            + " AND SHIWAKE_CD = @SHIWAKE_CD"
                            + " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                this.Dba.Delete("TB_ZM_SHIWAKE_JIREI", where, dpc);
                // 事例データのINSERT
                if (this.DtJirei != null)
                {
                    foreach (DataRow dr in this.DtJirei.Rows)
                    {
                        ArrayList alParamsZmJire = SetZmJireiParams(dr);
                        this.Dba.Insert("TB_ZM_SHIWAKE_JIREI", (DbParamCollection)alParamsZmJire[0]);
                    }
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #endregion

        #region イベント

        /// <summary>
        /// 摘要コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyoCd())
            {
                e.Cancel = true;
                this.txtTekiyoCd.SelectAll();
            }
            else
            {
                if (this.txtTekiyoCd.Modified)
                {
                    if (ValChk.IsEmpty(this.txtTekiyoCd.Text))
                    {
                        SetNewTekiyoCd();       // 摘要データ新規モード
                    }
                    else
                    {
                        TekiyoDataLoad(this.txtTekiyoCd.Text);
                    }
                    SetNewShiwakeCd();          // 事例データ新規モード


                    _mode = MODE_NEW;                               // 処理状態は新規モード
                    this.btnF3.Enabled = _mode.Equals(MODE_EDIT);   

                    this.txtTekiyoCd.Modified = false;
                }
            }
        }

        /// <summary>
        /// 摘要名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyoNm())
            {
                e.Cancel = true;
                this.txtTekiyoNm.SelectAll();
            }
        }

        /// <summary>
        /// 摘要カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyoKanaNm())
            {
                e.Cancel = true;
                this.txtTekiyoKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 仕訳コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiwakeCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiwakeCd())
            {
                e.Cancel = true;
                this.txtShiwakeCd.SelectAll();
            }
            else
            {
                if (this.txtShiwakeCd.Modified)
                {
                    if (ValChk.IsEmpty(this.txtShiwakeCd.Text))
                    {
                        SetNewShiwakeCd();
                        _mode = MODE_NEW;                           // 処理状態は追加モード
                        this.btnF3.Enabled = false;
                    }
                    else
                    {
                        ShiwakeJireiDataLoad(this.txtTekiyoCd.Text, this.txtShiwakeCd.Text);
                    }

                    this.txtShiwakeCd.Modified = false;
                }
            }
        }

        /// <summary>
        /// 仕訳名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiwakeNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiwakeNm())
            {
                e.Cancel = true;
                this.txtShiwakeNm.SelectAll();
            }
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// 初期表示
        /// </summary>
        private void InitDisp()
        {
            //// (デバッグ用)
            //if (ValChk.IsEmpty(this.TekiyoCd)) this.TekiyoCd = "2";
            //DataSet ds = new DataSet();
            //System.IO.FileStream fsReadXml = new System.IO.FileStream("C:\\WORK\\JIREI.xml", System.IO.FileMode.Open);
            //ds.ReadXml(fsReadXml);
            //this.DtJirei = ds.Tables["TABLE1"];
            //// (デバッグ用)

            // InDataより受け取り
            object[] inData = (object[])this.InData;
            this.TekiyoCd = (string)inData[0];
            this.TekiyoNm = (string)inData[1];
            this.DtJirei = (DataTable)inData[2];

            // 新規・編集モード
            _mode = MODE_NEW;

            // 摘要コード初期値
            if (ValChk.IsEmpty(this.TekiyoCd))
            {
                SetNewTekiyoCd();
            }
            else
            {
                this.txtTekiyoCd.Text = this.TekiyoCd;
                TekiyoDataLoad(this.TekiyoCd);
            }

            // 仕訳コード初期値
            SetNewShiwakeCd();
            this.txtShiwakeNm.Text = this.txtTekiyoNm.Text;

            // 摘要コードにフォーカス
            this.txtTekiyoCd.Focus();
        }
       
        /// <summary>
        /// 摘要コードの新番号セット
        /// </summary>
        private void SetNewTekiyoCd()
        {
            // 摘要コードが未入力なら新規コードをセット
            if (ValChk.IsEmpty(this.txtTekiyoCd.Text))
            {
                // レコードアクセス用パラメータ
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, this.UInfo.KaikeiNendo);
                StringBuilder sql = new StringBuilder();
                sql.Append("SELECT");
                sql.Append(" MAX(TEKIYO_CD) AS MAXCD");
                sql.Append(" FROM TB_ZM_TEKIYO");
                sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                sql.Append(" AND SHISHO_CD = @SHISHO_CD");
                sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
                DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
                if (dt.Rows.Count == 0)
                {
                    this.txtTekiyoCd.Text = "1";
                }
                else
                {
                    DataRow dr = dt.Rows[0];
                    this.txtTekiyoCd.Text = Util.ToString(Util.ToDecimal(Util.ToString(dr["MAXCD"])) + 1);
                }
                this.txtTekiyoNm.Text = this.TekiyoNm;
            }
        }

        /// <summary>
        /// 仕訳コードの新番号セット
        /// </summary>
        private void SetNewShiwakeCd()
        {
            // 仕訳コードが未入力なら新規コードをセット
            if (ValChk.IsEmpty(this.txtShiwakeCd.Text))
            {
                // レコードアクセス用パラメータ
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCd.Text);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, this.UInfo.KaikeiNendo);
                StringBuilder sql = new StringBuilder();
                sql.Append("SELECT ");
                sql.Append(" MAX(SHIWAKE_CD) AS MAXCD");
                sql.Append(" FROM");
                sql.Append(" TB_ZM_SHIWAKE_JIREI");
                sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                sql.Append(" AND SHISHO_CD = @SHISHO_CD");
                sql.Append(" AND TEKIYO_CD = @TEKIYO_CD");
                sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
                DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
                if (dt.Rows.Count == 0)
                {
                    this.txtShiwakeCd.Text = "1";
                }
                else
                {
                    DataRow dr = dt.Rows[0];
                    this.txtShiwakeCd.Text = Util.ToString(Util.ToDecimal(Util.ToString(dr["MAXCD"])) + 1);
                }
            }
        }

        /// <summary>
        ///  摘要データ読込
        /// </summary>
        /// <param name="tekiyoCd">摘要コード</param>
        private void TekiyoDataLoad(string tekiyoCd)
        {
            // 登録データの取得
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, Util.ToString(this.UInfo.KaishaCd));
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToString(this.ShishoCode));
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, Util.ToString(tekiyoCd));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, Util.ToString(this.UInfo.KaikeiNendo));
            string cols = "TEKIYO_NM, TEKIYO_KANA_NM";
            string from = "TB_ZM_TEKIYO";
            string where = "KAISHA_CD = @KAISHA_CD"
                        + " AND SHISHO_CD = @SHISHO_CD"
                        + " AND TEKIYO_CD = @TEKIYO_CD"
                        + " AND KAIKEI_NENDO = @KAIKEI_NENDO";
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where,dpc);
            if (dt.Rows.Count == 0)
            {
                // 登録データが存在しない時
                this.txtTekiyoNm.Text = "";
                this.txtTekiyoKanaNm.Text = "";

                _mode = MODE_NEW;                       // 処理状態は追加モード
            }
            else
            {
                // 登録データが存在する時
                DataRow dr = dt.Rows[0];
                this.txtTekiyoNm.Text = Util.ToString(dr["TEKIYO_NM"]);
                this.txtTekiyoKanaNm.Text = Util.ToString(dr["TEKIYO_KANA_NM"]);

                _mode = MODE_EDIT;                      // 処理状態は編集モード
            }
            this.btnF3.Enabled = _mode.Equals(MODE_EDIT);
        }

        /// <summary>
        ///  仕訳事例データ読込
        /// </summary>
        /// <param name="tekiyoCd">摘要コード</param>
        private void ShiwakeJireiDataLoad(string tekiyoCd, string shiwakeCd)
        {
            // 登録データの取得
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, Util.ToString(this.UInfo.KaishaCd));
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToString(this.ShishoCode));
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, Util.ToString(tekiyoCd));
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 4, Util.ToString(shiwakeCd));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, Util.ToString(this.UInfo.KaikeiNendo));
            string cols = "SHIWAKE_NM";
            string from = "TB_ZM_SHIWAKE_JIREI";
            string where = "KAISHA_CD = @KAISHA_CD"
                        + " AND SHISHO_CD = @SHISHO_CD"
                        + " AND TEKIYO_CD = @TEKIYO_CD"
                        + " AND SHIWAKE_CD = @SHIWAKE_CD"
                        + " AND KAIKEI_NENDO = @KAIKEI_NENDO";
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            if (dt.Rows.Count == 0)
            {
                // 登録データが存在しない時
                this.txtShiwakeNm.Text = "";
            }
            else
            {
                // 登録データが存在する時
                DataRow dr = dt.Rows[0];
                this.txtShiwakeNm.Text = Util.ToString(dr["SHIWAKE_NM"]);
            }
        }

        /// <summary>
        /// 摘要コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTekiyoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
         
            return true;
        }

        /// <summary>
        /// 摘要名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoNm()
        {
            // 指定文字数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTekiyoNm.Text, this.txtTekiyoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 摘要カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoKanaNm()
        {
            // 指定文字数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTekiyoKanaNm.Text, this.txtTekiyoKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仕訳コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiwakeCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仕訳名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiwakeNm()
        {
            // 指定文字数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShiwakeNm.Text, this.txtShiwakeNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 摘要コードのチェック
            if (!IsValidTekiyoCd())
            {
                this.txtTekiyoCd.Focus();
                return false;
            }

            // 摘要名のチェック
            if (!IsValidTekiyoNm())
            {
                this.txtTekiyoNm.Focus();
                return false;
            }
            // 摘要カナ名のチェック
            if (!IsValidTekiyoKanaNm())
            {
                this.txtTekiyoKanaNm.Focus();
                return false;
            }
            // 仕訳コードのチェック
            if (!IsValidShiwakeCd())
            {
                this.txtShiwakeCd.Focus();
                return false;
            }
            // 仕訳名称のチェック
            if (!IsValidShiwakeNm())
            {
                this.txtShiwakeNm.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_ZM_TEKIYOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="mode">1登録/2更新モード</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmTekiyoParams(string mode)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(mode))
            {
                // 更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                updParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCd.Text);
                updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(mode))
            {
                // Where句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 6, this.ShishoCode);
                whereParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCd.Text);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                alParams.Add(whereParam);
            }

            // 摘要名
            updParam.SetParam("@TEKIYO_NM", SqlDbType.VarChar, 40, this.txtTekiyoNm.Text);
            // 摘要カナ名
            updParam.SetParam("@TEKIYO_KANA_NM", SqlDbType.VarChar, 30, this.txtTekiyoKanaNm.Text);
            //// 摘要分類コード
            //updParam.SetParam("@KANYUSHA_NM", SqlDbType.Decimal, 4, ?);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_JIREIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="dr">仕訳事例明細データ</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmJireiParams(DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 会社コード
            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 支所コード
            updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            // 摘要コード
            updParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCd.Text);
            // 仕訳コード
            updParam.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 4, this.txtShiwakeCd.Text);
            // 行番号
            updParam.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(dr["GYO_BANGO"])));
            // 貸借区分
            updParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(Util.ToString(dr["TAISHAKU_KUBUN"])));
            // 明細区分
            updParam.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(Util.ToString(dr["MEISAI_KUBUN"])));
            // 会計年度
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, Util.ToDecimal(this.UInfo.KaikeiNendo));
            // 仕訳名称
            updParam.SetParam("@SHIWAKE_NM", SqlDbType.VarChar, 40, Util.ToString(this.txtShiwakeNm.Text));
            // 勘定科目コード
            updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(dr["KANJO_KAMOKU_CD"])));
            // 補助科目コード(保存オプション2で判定)
            updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, 
                chkOpt2.Checked ? Util.ToDecimal(Util.ToString(dr["HOJO_KAMOKU_CD"])) : 0);
            // 部門コード(保存オプション1で判定)
            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, 
                chkOpt1.Checked ? Util.ToDecimal(Util.ToString(dr["BUMON_CD"])) : 0);
            //// 保存しない
            ////// 工事コード 
            ////updParam.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, dr["KOJI_CD"]);
            ////// 工種コード
            ////updParam.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, dr["KOSHU_CD"]);
            
            // 摘要
            updParam.SetParam("@TEKIYO", SqlDbType.VarChar, 40, Util.ToString(dr["TEKIYO"]));
            // 税込金額(保存オプション3で判定)
            updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, 
                chkOpt3.Checked ? Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])) : 0);
            // 税抜金額(保存オプション3で判定)
            updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, 
                chkOpt3.Checked ? Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])) : 0);
            // 消費税金額(保存オプション3で判定)
            updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 
                chkOpt3.Checked ? Util.ToDecimal(Util.ToString(dr["SHOHIZEI_KINGAKU"])) : 0);
            // 税区分
            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(Util.ToString(dr["ZEI_KUBUN"])));
            // 課税区分
            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(Util.ToString(dr["KAZEI_KUBUN"])));
            // 取引区分
            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, Util.ToDecimal(Util.ToString(dr["TORIHIKI_KUBUN"])));
            // 税率
            //updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, Util.ToDecimal(dr["ZEI_KUBUN"]));
            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, Util.ToDecimal(Util.ToString(dr["ZEI_RITSU"])));
            // 事業区分
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(Util.ToString(dr["JIGYO_KUBUN"])));
            // 消費税入力方法
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, Util.ToDecimal(Util.ToString(dr["SHOHIZEI_NYURYOKU_HOHO"])));
            // 消費税変更
            updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, Util.ToDecimal(Util.ToString(dr["SHOHIZEI_HENKO"])));
            // 決算区分
            updParam.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, Util.ToDecimal(Util.ToString(dr["KESSAN_KUBUN"])));
            // 登録日
            updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            // 更新日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);
            return alParams;
        }

        #endregion
    }
}
