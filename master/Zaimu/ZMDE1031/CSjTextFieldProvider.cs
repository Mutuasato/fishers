﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using systembase.table;

namespace jp.co.fsi.zm.zmde1031
{
    class CSjTextFieldProvider: CFieldProvider
    {

        private ImeMode _imeMode;

        public CSjTextFieldProvider() : this(null)
        {
        }

        public CSjTextFieldProvider(string caption) : this(caption, System.Windows.Forms.ImeMode.NoControl)
        {
        }

        public CSjTextFieldProvider(string caption, ImeMode imeMode): base(caption)
        {
	        this._imeMode = imeMode;
        }

        public override IEditor CreateEditor()
        {
	        return new CFsiTextBoxEditor();
        }

        public override System.Windows.Forms.ImeMode ImeMode()
        {
	        return this._imeMode;
        }

    }
}
