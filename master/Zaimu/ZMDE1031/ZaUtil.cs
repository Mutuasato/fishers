﻿using System;
using System.Text;
using System.Data;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.util;
using jp.co.fsi.common.userinfo;

namespace jp.co.fsi.zm.zmde1031
{
    public static class ZaUtil
    {
        /// <summary>
        /// 伝票更新ロック開始
        /// </summary>
        /// <param name="uinfo">UInfo</param>
        /// <param name="dba">Dba</param>
        public static void UpdateLockOn(UserInfo uinfo, DbAccess dba)
        {
            // 制御レコード追加用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uinfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, uinfo.ShishoCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, "0");                   // 伝票区分＝0
            dpc.SetParam("@DENPYO_KOSHIN_FLG", SqlDbType.Decimal, "1");
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, 10, DateTime.Now);
            // 制御レコードを追加できるまで繰り返し
            while (true)
            {
                try
                {
                    if (dba.Insert("TB_CM_DENPYO_KOSHIN", dpc) == 1)
                    {
                        break;
                    }
                    System.Threading.Thread.Sleep(10);                                  // とりあえず10ミリ秒待機
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());                   
                }
            }
        }

        /// <summary>
        /// 伝票更新ロック開放
        /// </summary>
        /// <param name="uinfo">UInfo</param>
        /// <param name="dba">Dba</param>
        public static void UpdateLockOff(UserInfo uinfo, DbAccess dba)
        {
            // 制御レコード削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uinfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, uinfo.ShishoCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, "0");                   // 伝票区分＝0
            string where = "KAISHA_CD = @KAISHA_CD";
            where += " AND SHISHO_CD = @SHISHO_CD";
            where += " AND DENPYO_KUBUN = @DENPYO_KUBUN";

            // 制御レコード削除
            dba.Delete("TB_CM_DENPYO_KOSHIN", where, dpc);
        }

        /// <summary>
        /// 伝票番号の取得
        /// </summary>
        /// <param name="uinfo">UInfo</param>
        /// <param name="dba">Dba</param>
        /// <returns>伝票番号</returns>
        public static decimal GetNewDenpyoBango(UserInfo uinfo, DbAccess dba)
        {
            decimal newBango;

            // レコードアクセス用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uinfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, uinfo.ShishoCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, "0");                   // 伝票区分＝0
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uinfo.KaikeiNendo);
            // 年度レコードを取得
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append(" DENPYO_BANGO");
            sql.Append(" FROM ");
            sql.Append(" TB_ZM_DENPYO_BANGO");
            sql.Append(" WHERE ");
            sql.Append(" KAISHA_CD = @KAISHA_CD ");
            sql.Append(" AND SHISHO_CD = @SHISHO_CD ");
            sql.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
            sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            DataTable dtResult = dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            if (dtResult.Rows.Count > 0)
            {
                newBango = Util.ToDecimal(dtResult.Rows[0]["DENPYO_BANGO"]) + 1;

                // 更新用パラメータ
                DbParamCollection udpc = new DbParamCollection();
                udpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, newBango);
                udpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, 10, DateTime.Now);
                // 伝票抽出用WHERE句
                string where = "KAISHA_CD = @KAISHA_CD";
                where += " AND SHISHO_CD = @SHISHO_CD";
                where += " AND DENPYO_KUBUN = @DENPYO_KUBUN";
                where += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                // 更新実行
                dba.Update("TB_ZM_DENPYO_BANGO", udpc, where, dpc);
            }
            else
            {
                newBango = 1;

                // 過年度レコードを取得
                sql.Clear();
                sql.Append("SELECT ");
                sql.Append(" DENPYO_BANGO");
                sql.Append(" FROM ");
                sql.Append(" TB_ZM_DENPYO_BANGO");
                sql.Append(" WHERE ");
                sql.Append(" KAISHA_CD = @KAISHA_CD ");
                sql.Append(" AND SHISHO_CD = @SHISHO_CD ");
                sql.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
                sql.Append(" AND KAIKEI_NENDO < @KAIKEI_NENDO ");                       // 過年度
                sql.Append(" ORDER BY KAIKEI_NENDO DESC");                              // 降順
                DataTable dt = dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
                DataRow dr = dt.Rows[0];                                                // 過年度レコードは存在する前提
                // 採番レコード追加用パラメータ
                DbParamCollection idpc = new DbParamCollection();
                idpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uinfo.KaishaCd);
                idpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, uinfo.ShishoCd);
                idpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, "0");                   
                idpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uinfo.KaikeiNendo);
                idpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, newBango);
                idpc.SetParam("@BANGO_ZOBUN", SqlDbType.Decimal, 4, dr["BANGO_ZOBUN"]);
                idpc.SetParam("@BANGO_SAISHOCHI", SqlDbType.Decimal, 9, dr["BANGO_SAISHOCHI"]);
                idpc.SetParam("@BANGO_SAIDAICHI", SqlDbType.Decimal, 9, dr["BANGO_SAIDAICHI"]);
                idpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, 10, DateTime.Now);
                idpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, 10, DateTime.Now);
                // 追加実行
                dba.Insert("TB_ZM_DENPYO_BANGO", idpc); 
            }

            return newBango;
        }

        /// <summary>
        /// 消費税額の計算
        /// ※会社情報の税込/税抜方法別の税額算出を行う。
        /// CASE 税抜入力・計算なし⇒常に０円
        /// CASE 税抜入力・計算あり⇒対象金額×税率
        /// CASE 税込入力・計算あり⇒対象金額×税率÷(100＋税率)
        /// </summary>
        /// <param name="taxCategory">税区分</param>
        /// <param name="amount">対象金額</param>
        /// <param name="uinfo">UInfo</param>
        /// <param name="dba">Dba</param>
        //public static decimal CalcConsumptionTax(int taxCategory, decimal amount, UserInfo uinfo, DbAccess dba)
        //{
        //    decimal ret = 0;
        //    int nyuryokuHoho = 0;
        //    int hasuShori = 0;
        //    decimal ritsu = 0;

        //    // 会社情報参照
        //    {
        //        DbParamCollection dpc = new DbParamCollection();
        //        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uinfo.KaishaCd);
        //        dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 3, uinfo.KessanKi);
        //        StringBuilder cols = new StringBuilder();
        //        cols.Append("SHOHIZEI_NYURYOKU_HOHO");
        //        cols.Append(", SHOHIZEI_HASU_SHORI");
        //        StringBuilder where = new StringBuilder();
        //        where.Append("KAISHA_CD = @KAISHA_CD");
        //        where.Append(" AND KESSANKI = @KESSANKI");
        //        DataTable dt = dba.GetDataTableByConditionWithParams(Util.ToString(cols)
        //                        , "VI_ZM_KAISHA_JOHO"
        //                        , Util.ToString(where), dpc);
        //        if (dt.Rows.Count == 1)
        //        {
        //            nyuryokuHoho = Util.ToInt(dt.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"]);
        //            hasuShori = Util.ToInt(dt.Rows[0]["SHOHIZEI_HASU_SHORI"]);
        //        }
        //    }
        //    // 税区分情報参照
        //    {
        //        DbParamCollection dpc = new DbParamCollection();
        //        dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, taxCategory);
        //        StringBuilder cols = new StringBuilder();
        //        cols.Append("ZEI_RITSU");
        //        StringBuilder where = new StringBuilder();
        //        where.Append("ZEI_KUBUN = @ZEI_KUBUN");
        //        DataTable dt = dba.GetDataTableByConditionWithParams(Util.ToString(cols)
        //                        , "TB_ZM_F_ZEI_KUBUN"
        //                        , Util.ToString(where), dpc);
        //        if (dt.Rows.Count == 1)
        //        {
        //            ritsu = Util.ToDecimal(dt.Rows[0]["ZEI_RITSU"]);
        //        }
        //    }
        //    // 税入力方法別
        //    switch (nyuryokuHoho)
        //    {
        //        case 1:     // 税抜入力：消費税計算なし＝税額０円で返す
        //            ret = 0;
        //            break;
        //        case 2:     // 税抜入力：消費税計算あり＝税抜額(100%)に対する税率計算結果を返す
        //            ret = RoundByType(hasuShori, amount * ritsu / 100);
        //            break;
        //        case 3:     // 税込入力：消費税計算あり＝税込額(100%＋税額)に対する税率計算結果を返す
        //            ret = RoundByType(hasuShori, amount * ritsu / (100 + ritsu));
        //            break;
        //    }

        //    return ret;
        //}
        public static decimal CalcConsumptionTax(decimal taxRate, decimal amount, UserInfo uinfo, DbAccess dba)
        {
            decimal ret = 0;
            int nyuryokuHoho = 0;
            int hasuShori = 0;
            // 会社情報参照
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uinfo.KaishaCd);
                dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 3, uinfo.KessanKi);
                StringBuilder cols = new StringBuilder();
                cols.Append("SHOHIZEI_NYURYOKU_HOHO");
                cols.Append(", SHOHIZEI_HASU_SHORI");
                StringBuilder where = new StringBuilder();
                where.Append("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND KESSANKI = @KESSANKI");
                DataTable dt = dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "VI_ZM_KAISHA_JOHO"
                                , Util.ToString(where), dpc);
                if (dt.Rows.Count == 1)
                {
                    nyuryokuHoho = Util.ToInt(dt.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"]);
                    hasuShori = Util.ToInt(dt.Rows[0]["SHOHIZEI_HASU_SHORI"]);
                }
            }
            // 税入力方法別
            switch (nyuryokuHoho)
            {
                case 1:     // 税抜入力：消費税計算なし＝税額０円で返す
                    ret = 0;
                    break;
                case 2:     // 税抜入力：消費税計算あり＝税抜額(100%)に対する税率計算結果を返す
                    ret = RoundByType(hasuShori, amount * taxRate / 100);
                    break;
                case 3:     // 税込入力：消費税計算あり＝税込額(100%＋税額)に対する税率計算結果を返す
                    ret = RoundByType(hasuShori, amount * taxRate / (100 + taxRate));
                    break;
            }

            return ret;
        }

        /// <summary>
        /// 伝票金額の取得
        /// </summary>
        /// <param name="zeikomiKingaku">税込金額</param>
        /// <param name="zeinukiKingaku">税抜金額</param>
        /// <param name="uinfo">UInfo</param>
        public static decimal GetDenpyoKingaku(decimal zeikomiKingaku, decimal zeinukiKingaku, UserInfo uinfo)
        {
            decimal ret = 0;
            switch (Util.ToInt(uinfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"]))
            {
                case 1:     // 税抜入力・消費税計算なし
                    ret = zeinukiKingaku;
                    break;
                case 2:     // 税抜入力・消費税計算あり
                    ret = zeinukiKingaku;
                    break;
                case 3:     // 税込入力・消費税計算あり
                    ret = zeikomiKingaku;
                    break;
            }
            return ret;
        }

        /// <summary>
        /// 税込金額の取得
        /// </summary>
        /// <param name="denpyoKingaku">伝票金額</param>
        /// <param name="shohizeiKingaku">消費税額</param>
        /// <param name="uinfo">UInfo</param>
        public static decimal GetZeikomiKingaku(decimal denpyoKingaku, decimal shohizeiKingaku, UserInfo uinfo)
        {
            decimal ret = 0;
            switch (Util.ToInt(uinfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"]))
            {
                case 1:     // 税抜入力・消費税計算なし
                    ret = denpyoKingaku;
                    break;
                case 2:     // 税抜入力・消費税計算あり
                    ret = denpyoKingaku + shohizeiKingaku;
                    break;
                case 3:     // 税込入力・消費税計算あり
                    ret = denpyoKingaku;
                    break;
            }
            return ret;
        }

        /// <summary>
        /// 税抜金額の取得
        /// </summary>
        /// <param name="denpyoKingaku">伝票金額</param>
        /// <param name="shohizeiKingaku">消費税額</param>
        /// <param name="uinfo">UInfo</param>
        public static decimal GetZeinukiKingaku(decimal denpyoKingaku, decimal shohizeiKingaku, UserInfo uinfo)
        {
            decimal ret = 0;
            switch (Util.ToInt(uinfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"]))
            {
                case 1:     // 税抜入力・消費税計算なし
                    ret = denpyoKingaku;
                    break;
                case 2:     // 税抜入力・消費税計算あり
                    ret = denpyoKingaku;
                    break;
                case 3:     // 税込入力・消費税計算あり
                    ret = denpyoKingaku - shohizeiKingaku;
                    break;
            }
            return ret;
        }

        /// <summary>
        /// 会計年度内日付に変換
        /// </summary>
        public static DateTime FixNendoDate(DateTime date, UserInfo uinfo)
        {
            DateTime dateFr = Util.ToDate(uinfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            DateTime dateTo = Util.ToDate(uinfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }

        /// <summary>
        /// 和暦日付を正しい日付に補正します。(FixJpDate へ　和暦年・月・日の各値基本補正)
        /// </summary>
        /// <param name="gengo">元号(漢字名称。「平成」「昭和」など)</param>
        /// <param name="jpYear">年(和暦)</param>
        /// <param name="month">月</param>
        /// <param name="day">日</param>
        /// <param name="dba">データアクセスオブジェクト</param>
        /// <returns>
        /// 和暦にフォーマットされた日付
        /// 0:元号
        /// 1:元号を示す記号
        /// 2:年
        /// 3:月
        /// 4:日
        /// 5:全部一纏めにした日付
        /// </returns>
        public static string[] FixJpDate(string gengo, string jpYear, string month, string day, DbAccess dba)
        {
            string fixJpYear = jpYear;
            string fixMonth = month;
            string fixDay = day;
            
            // 年値基本補正
            if (Util.ToInt(jpYear) < 1) fixJpYear = "0"; 
            // 月値基本補正
            if (Util.ToInt(month) < 1) fixMonth = "1";
            if (Util.ToInt(month) > 12) fixMonth = "12";
            // 日値基本補正
            if (Util.ToInt(day) < 1) fixDay = "1";
            DateTime getsumatsu = Util.ConvAdDate(gengo, fixJpYear, fixMonth, "1", dba);
            getsumatsu = getsumatsu.AddMonths(1).AddDays(-1);                           // 月末日
            if (Util.ToInt(fixDay) > getsumatsu.Day) fixDay = Util.ToString(getsumatsu.Day);

            // まず西暦に変換
            DateTime datAdDate = Util.ConvAdDate(gengo, fixJpYear, fixMonth, fixDay, dba);

            // それを和暦に変換して返却
            return Util.ConvJpDate(datAdDate, dba);
        }

        /// <summary>
        /// 端数丸め処理
        /// </summary>
        /// <param name="type">1切り捨て　2四捨五入　3切り上げ</param>
        public static decimal RoundByType(int type, decimal amount)
        {
            decimal ret = 0;
            switch (type)
            {
                case 1:     // 切り捨て
                    ret = Math.Floor(amount);
                    break;
                case 2:     // 四捨五入
                    ret = Util.Round(amount, 0);
                    break;
                case 3:     // 切り上げ
                    ret = Math.Ceiling(amount);
                    break;
            }
            return ret;
        }

        /// <summary>
        /// 数値フォーマット(０値を表示しない)
        /// </summary>
        public static string FormatNum(object num)
        {
            string ret = "";
            if (num != null) ret = Util.ToString(num);
            if (ret == "0") ret = "";
            return ret;
        }

        /// <summary>
        /// 金額値フォーマット(０値を表示しないカンマ区切り)
        /// </summary>
        public static string FormatCur(object num)
        {
            string ret = "";
            if (num != null) ret = Util.FormatNum(num);
            if (ret == "0") ret = "";
            return ret;
        }

        /// <summary>
        /// 百分率値フォーマット(０値以外を%つき表示)
        /// </summary>
        public static string FormatPercentage(object num)
        {
            string ret = "";
            if (num != null) ret = Util.FormatNum(num);
            if (ret == "0" || ret == null) ret = "";
            if (Util.ToDecimal(ret) > 0) ret = ret + '%';
            return ret;
        }

        /// <summary>
        /// 数値コンバート(空文字とnullを０値変換)
        /// </summary>
        public static Decimal ToDecimal(object txt)
        {
            decimal ret = 0;
            if (txt != null) ret = Util.ToDecimal(txt);
            return ret;
        }

    }
}
