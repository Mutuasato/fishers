﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using systembase.table;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.userinfo;
using System.Text;
using System.Collections;
using System.Reflection;

namespace jp.co.fsi.zm.zmde1031
{
    /// <summary>
    /// 振替伝票入力(ZAME1031)
    /// </summary>
    public partial class ZMDE1031 : BasePgForm
    {
        #region 定数

        private const decimal DENPYO_KUBUN = 1;                     // 伝票区分　値定義1

        private const string MODE_NEW = "《登録》";
        private const string MODE_EDIT = "《修正》";

        #endregion

        #region クラス
        // 出納帳関連
        class BaseInfo
        {
            public string DenpyoBango { get; set; }
            public string DenpyoDate { get; set; }
            public string ShohyoBango { get; set; }
            public string TantoshaCd { get; set; }
            public string KessanKubun { get; set; }

            public string KanjoKamokuCd { get; set; }
            public string HojoKamokuCd { get; set; }
            public string BumonCd { get; set; }
            public DateTime DenpyoDateFr { get; set; }
            public DateTime DenpyoDateTo { get; set; }
            
            public BaseInfo(UserInfo uinfo)
            {
                DenpyoDateFr = Util.ToDate(uinfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
                DenpyoDateTo = Util.ToDate(uinfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            }
        }
        #endregion

        #region 変数
        private UTable.CField _activeCField;     // UTableアクティブフィールド参照用
        private UTable.CRecord _activeCRecord;   // UTableアクティブレコード参照用       
        private DataTable _dtKanjoKamoku;        // V勘定科目データ
        private DataTable _dtZeiKubun;           // T税区分データ
        private DataTable _dtBumon;              // V部門データ
        private int _cpFlg;                      // 行複写フラグ
        private decimal _prvKanjoKamoku;         // 前行の勘定科目

        private string _denpyoCondition = "";    // 伝票検索条件
        private int _autoDataSetting = 0;        // 自動仕訳データの操作許可設定
        private int _autoDataFlg = 0;            // 自動仕訳作成区分（上記が行う設定の場合）
        private int _externalCall = 0;           // 外部から呼ばれた
        private string DENPYO_DATE = string.Empty;
        private BaseInfo _bInfo;                 // 出納帳引き継ぎ情報

        // データ入力フラグ(フォームを閉じる時に確認画面を出力時使用)
        private int IS_INPUT = 1;                // 1:未入力,2:入力済
        private int _fromF6 = 0;                 // 0:クリア,1:F6より
        #endregion

        #region イレギュラー関連
        // ログ関連
        class LogTbl
        {
            public int Denno { get; set; }
            public int Denkb { get; set; }
            public int IkkatuDenno { get; set; }
            public DateTime Dendate { get; set; }
            public decimal Kingk { get; set; }
            public DateTime DendateOld { get; set; }
            public decimal KingkOld { get; set; }
            public string Memo { get; set; }
        }
        private LogTbl _irgLog;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDE1031()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();

            BindTextChanged();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }
            // 取り合えず入力、更新系は触れない様に
            txtMizuageShishoCd.Enabled = false;
            // 伝票複写
            //this.btnShiftF4.Text = "Shift+F4" + "\n\r" + "伝票複写";
            this.lblBiko.Text = "";

            // 共通データを読込
            InitFormDataLoad();

            // 明細部を初期化
            InitDetailArea();

            this.btnEsc.Enabled = true;
            this.btnF1.Enabled = false;
            this.btnF2.Enabled = false;
            this.btnF3.Enabled = false;
            this.btnF4.Enabled = false;
            this.btnF5.Enabled = false;
            this.btnF6.Enabled = false;
            this.btnF7.Enabled = false;
            this.btnF8.Enabled = false;
            this.btnF9.Enabled = false;
            this.btnF10.Enabled = false;
            this.btnF11.Enabled = true;
            this.btnF12.Enabled = true;

            // 自動仕訳作成の伝票操作許可判定用（設定から取得）
            this._autoDataSetting = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDE1031", "Setting", "autoDataSetting")));

            // 伝票番号指定の場合は伝票データロード
            if (this.InData != null)
            {
                string arg = string.Empty;
                if (this.InData.GetType().IsArray)
                {
                    // 出納帳からの呼び出し
                    object[] inData = (object[])this.InData;
                    arg = (string)inData[0];
                    this._externalCall = 1;

                    this._bInfo = new BaseInfo(this.UInfo);
                    this._bInfo.DenpyoBango = Util.ToString(inData[0]);
                    this._bInfo.DenpyoDate = Util.ToString(inData[1]);
                    this._bInfo.ShohyoBango = Util.ToString(inData[2]);
                    this._bInfo.TantoshaCd = Util.ToString(inData[3]);
                    this._bInfo.KessanKubun = Util.ToString(inData[4]);

                    this._bInfo.KanjoKamokuCd = Util.ToString(inData[5]);
                    this._bInfo.HojoKamokuCd = Util.ToString(inData[6]);
                    this._bInfo.BumonCd = Util.ToString(inData[7]);
                    this._bInfo.DenpyoDateFr = Util.ToDate(inData[8]);
                    this._bInfo.DenpyoDateTo = Util.ToDate(inData[9]);

                    //btnF6.Text = "F6" + "\n\r" + "確定";

                    this.Text = "複合仕訳入力";
                    this.lblTitle.Text = this.Text;

                    this.btnF2.Visible = false;
                    this.btnEsc.Location = this.btnF1.Location;
                    this.btnF1.Location = this.btnF2.Location;

                    DataLoadFukugo((DataTable)inData[10]);
                    if (arg == "0" || arg == "")
                    {
                        lblMode.Text = MODE_NEW;
                    }
                }
                else
                {
                    // 元帳系からの呼び出し
                    arg = (string)this.InData;
                    this._externalCall = 2;

                    string[] cnd = arg.Split(',');
                    if (cnd.Length != 0)
                    {
                        this.txtMizuageShishoCd.Text = cnd[0];
                        this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
                        arg = cnd[1];
                    }

                    this.txtDenpyoBango.Text = arg;
                    this.DataLoad(this.UInfo.KaishaCd, this.UInfo.KaikeiNendo, this.txtDenpyoBango.Text);
                }
                // 外部呼出しは複写不可
                this.btnShiftF4.Visible = false;
                this.btnShiftF4.Enabled = false;
                //btnF10.Text = "F10" + "\n\r" + "戻る";

                // 自動仕訳作成分のロックメッセージ
                if (_autoDataFlg != 0)
                {
                    Msg.Info("自動仕訳作成より作成された伝票の為、変更できません");
                }

                // 外部呼出し時は戻る
                //this.btnEsc.Text = "Esc" + "\n\r" + "戻る";
                if (this.MenuFrm != null)
                {
                    //((Button)this.MenuFrm.Controls["btnEsc"]).Text = "Esc" + "\n\r" + "\n\r" + "戻る";
                }
            }
            else
            {
                // データクリア
                DataClear();
                this._externalCall = 0;

                // 担当者情報初期値
                //this.txtTantoshaCd.Text = "1";
                //this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "1");
                this.txtTantoshaCd.Text = "";

                // 伝票日付初期値
                SetJp(Util.ConvJpDate(DateTime.Now, this.Dba));

                // 登録モード時は終了
                //this.btnEsc.Text = "Esc" + "\n\r" + "終了";
                if (this.MenuFrm != null)
                {
                    //((Button)this.MenuFrm.Controls["btnEsc"]).Text = "Esc" + "\n\r" + "\n\r" + "終了";
                }
            }

            // 保存削除メッセージクリア
            this.lblMessage.Text = "";
            this._fromF6 = 0;

            // フォーカス移動
            //this.txtDenpyoBango.Focus();
            this.txtTantoshaCd.Focus();

            if (MODE_NEW.Equals(this.lblMode.Text))
            {
                // 外部呼出しで無い場合は元々の処理、外部呼出し時は表示へ
                if (this._externalCall == 0)
                    this.ShowFButton = false;
                else
                    this.ShowFButton = true;
            }
            else if (MODE_EDIT.Equals(this.lblMode.Text))
            {
                this.ShowFButton = true;
            }

            // 外部呼出し時は担当者、伝票番号ロック
            if (this.InData != null && !ValChk.IsEmpty(this.InData))
            {
                this.txtDenpyoBango.Enabled = false;
                this.txtTantoshaCd.Enabled = false;

                this.txtGengoYearDenpyoDate.Focus();
            }

            // 
            this.IS_INPUT = 1;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            SetFunctionKey();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // 外部呼出し時は閉じる
            if (this._externalCall != 0)
            {
                this.DialogResult = DialogResult.Cancel;
                base.PressEsc();
                return;
            }

            // 新規の場合は閉じる
            if (MODE_NEW.Equals(this.lblMode.Text) && this._fromF6 == 0)
            {
                this.DialogResult = DialogResult.Cancel;
                base.PressEsc();
                return;
            }
            // 新規の場合は閉じる
            if (MODE_EDIT.Equals(this.lblMode.Text) && this._fromF6 == 0)
            {
                this.DialogResult = DialogResult.Cancel;
                base.PressEsc();
                return;
            }


            DataClear();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            if (!this.btnF1.Enabled)
                return;

            Assembly asm;
            Type t;
            DialogResult diagResult;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			//アクティブコントロールごとの処理
			//switch (this.ActiveCtlNm)
			switch (GetActiveObjectName())
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtTantoshaCd":
                    #region 担当者検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9041.KOBC9041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtTantoshaCd.Text = result[0];
                                this.lblTantoshaNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtGengoYearDenpyoDate":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoDenpyoDate.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoDenpyoDate.Text = result[1];

                                if (this.lblGengoDenpyoDate.Text != result[1])
                                    this.IS_INPUT = 2;

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJp();
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtDenpyoBango":
                    #region 伝票検索
                    using (ZMDE1032 frm1032 = new ZMDE1032())
                    {
                        // 伝票検索条件
                        frm1032.InData = this._denpyoCondition;

                        diagResult = frm1032.ShowDialog(this);
                        if (diagResult == DialogResult.OK)
                        {
                            string[] ret = (string[])frm1032.OutData;

                            // 伝票検索条件
                            this._denpyoCondition = ret[1];

                            this.txtDenpyoBango.Text = ret[0];
                            DataLoad(this.UInfo.KaishaCd, this.UInfo.KaikeiNendo, this.txtDenpyoBango.Text);

                            // 自動仕訳作成で無い場合、伝票複写可に（伝票が読み込まれた時）
                            if (_autoDataFlg == 0 && lblMode.Text != MODE_NEW) this.btnShiftF4.Enabled = true;

                            // 自動仕訳作成分のロックメッセージ
                            if (_autoDataFlg != 0)
                            {
                                Msg.Info("自動仕訳作成より作成された伝票の為、変更できません");
                            }
                        }
                        else
                        {
                            // 伝票検索条件クリア
                            this._denpyoCondition = string.Empty;
                        }
                    }
                    #endregion
                    break;
                case "TEKIYO_CD":
                    //case "TEKIYO":
                    #region 摘要検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("ZAMC9031.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1051.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.zam.zamc9031.ZAMC9031");
                        t = asm.GetType("jp.co.fsi.zm.zmcm1051.ZMCM1051");
                        if (t != null)
                        {
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCRecord.Fields["TEKIYO_CD"].Value = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "KOJI_CD":
                    // NONE
                    break;
                case "KOSHU_CD":
                    // NONE;
                    break;
                case "DrKANJO_KAMOKU_CD":
                case "CrKANJO_KAMOKU_CD":
                    #region 勘定科目検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9013");
                        t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                        if (t != null)
                        {
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "2";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCField.Value = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "DrBUMON_CD":
                case "CrBUMON_CD":
                    #region 部門検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");  // -> ZAMC9011
                        //asm = Assembly.LoadFrom("CMCM2041.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                        //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                        t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                        if (t != null)
                        {
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                //frm.Par1 = "1";
                                frm.Par1 = "TB_CM_BUMON";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCField.Value = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "DrHOJO_KAMOKU_CD":
                    #region 補助科目検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("ZAMC9021.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1041.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.zam.zamc9021.ZAMC9024");
                        t = asm.GetType("jp.co.fsi.zm.zmcm1041.ZMCM1044");
                        if (t != null)
                        {
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = _activeCRecord.Fields["DrKANJO_KAMOKU_CD"].Value;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCRecord.Fields["DrHOJO_KAMOKU_CD"].Value = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "DrDENPYO_KINGAKU":
                    #region 消費税額編集
                    if (_activeCRecord != null)
                    {
                        // 課税区分１の時に編集可能
                        if (Util.ToString(_activeCRecord.Fields["DrKAZEI_KUBUN"].Value) == "1")
                        {
                            CFieldEnabled(_activeCRecord.Fields["DrSHOHIZEI_KINGAKU"], true);
                        }
                    }
                    #endregion
                    break;
                case "CrHOJO_KAMOKU_CD":
                    #region 補助科目検索
                    if (_activeCRecord != null)
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("ZAMC9021.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1041.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.zam.zamc9021.ZAMC9024");
                        t = asm.GetType("jp.co.fsi.zm.zmcm1041.ZMCM1044");
                        if (t != null)
                        {
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = _activeCRecord.Fields["CrKANJO_KAMOKU_CD"].Value;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCRecord.Fields["CrHOJO_KAMOKU_CD"].Value = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "CrDENPYO_KINGAKU":
                    #region 消費税額編集
                    if (_activeCRecord != null)
                    {
                        // 課税区分１の時に編集可能
                        if (Util.ToString(_activeCRecord.Fields["CrKAZEI_KUBUN"].Value) == "1")
                        {
                            CFieldEnabled(_activeCRecord.Fields["CrSHOHIZEI_KINGAKU"], true);
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
                return;

            if (IsValidDenpyoBango())
            {
                if (txtDenpyoBango.Modified)
                {
                    if (ValChk.IsEmpty(txtDenpyoBango.Text) || Util.ToDecimal(txtDenpyoBango.Text) <= 0)
                    {
                        DataClear();
                    }
                    else
                    {
                        // 仕訳データの読込
                        mtbList.Visible = false;
                        DataLoad(UInfo.KaishaCd, UInfo.KaikeiNendo, txtDenpyoBango.Text);
                        mtbList.Visible = true;
                    }
                    txtDenpyoBango.Modified = false;    // プロパティクリア
                }
            }

            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            //// 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }


            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, this.txtDenpyoBango.Text);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            string where = "KAISHA_CD = @KAISHA_CD";
            where += " AND SHISHO_CD = @SHISHO_CD";
            where += " AND DENPYO_BANGO = @DENPYO_BANGO";
            where += " AND KAIKEI_NENDO = @KAIKEI_NENDO";

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 伝票更新ロック取得
                ZaUtil.UpdateLockOn(this.UInfo, this.Dba);

                if (MODE_EDIT.Equals(this.lblMode.Text))
                {
                    // 仕訳伝票・仕訳明細データ削除
                    this.Dba.Delete("TB_ZM_SHIWAKE_DENPYO", where, dpc);
                    this.Dba.Delete("TB_ZM_SHIWAKE_MEISAI", where, dpc);
                }

                // 伝票更新ロック開放
                ZaUtil.UpdateLockOff(this.UInfo, this.Dba);

                #region イレギュラー関連
                IrgLogInsert(3);
                #endregion

                // トランザクションをコミット
                this.Dba.Commit();

            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // 削除後処理
            this.lblMessage.Text = "前回削除伝票番号：" + this.txtDenpyoBango.Text;

            // 更新処理からのクリア時のメッセージ抑止
            this._fromF6 = 1;

            // 編集内容クリア：ESC取消
            PressEsc();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (!this.btnF4.Enabled)
                return;

            if (_activeCRecord != null)
            {
                //アクティブオブジェクトごとの処理
                if (Util.ToString(GetActiveObjectName()).Substring(0, 2) == "Dr")
                {
                    // 借方フィールド上での処理
                    if (!ValChk.IsEmpty(_activeCRecord.Fields["DrKANJO_KAMOKU_CD"].Value))
                    {
                        // 税区分を選択(非選択も可)->メソッド側で検索及び設定へ
                        //string zeiKubun = SelectZeiKubun(false);
                        //if (!ValChk.IsEmpty(zeiKubun))
                        //{
                        //    _activeCRecord.Fields["DrZEI_KUBUN"].Value = zeiKubun;
                        //    SetZeiKubunInfo(_activeCField);
                        //}
                        SelectZeiKubun(false, _activeCField);
                    }
                }
                if (Util.ToString(GetActiveObjectName()).Substring(0, 2) == "Cr")
                {
                    // 貸方フィールド上での処理
                    if (!ValChk.IsEmpty(_activeCRecord.Fields["CrKANJO_KAMOKU_CD"].Value))
                    {
                        // 税区分を選択(非選択も可)->メソッド側で検索及び設定へ
                        //string zeiKubun = SelectZeiKubun(false);
                        //if (!ValChk.IsEmpty(zeiKubun))
                        //{
                        //    // 税区分を選択(非選択も可)
                        //    _activeCRecord.Fields["CrZEI_KUBUN"].Value = zeiKubun;
                        //    SetZeiKubunInfo(_activeCField);
                        //}
                        SelectZeiKubun(false, _activeCField);
                    }
                }
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (!this.btnF5.Enabled)
                return;

            if (_activeCRecord != null)
            {
                Assembly asm;
                Type t;

                //アクティブオブジェクトごとの処理
                if (Util.ToString(GetActiveObjectName()).Substring(0, 2) == "Dr")
                {
                    // 借方フィールド上での処理
                    if (!ValChk.IsEmpty(_activeCRecord.Fields["DrKANJO_KAMOKU_CD"].Value))
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                        t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                        if (t != null)
                        {
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "TB_ZM_F_JIGYO_KUBUN";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCRecord.Fields["DrJIGYO_KUBUN"].Value = result[0];
                                }
                            }
                        }
                    }
                }
                if (Util.ToString(GetActiveObjectName()).Substring(0, 2) == "Cr")
                {
                    // 貸方フィールド上での処理
                    if (!ValChk.IsEmpty(_activeCRecord.Fields["CrKANJO_KAMOKU_CD"].Value))
                    {
                        // アセンブリのロード
                        //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                        // フォーム作成
                        //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                        t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                        if (t != null)
                        {
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "TB_ZM_F_JIGYO_KUBUN";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    _activeCRecord.Fields["CrJIGYO_KUBUN"].Value = result[0];
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled)
                return;

            if (IsValidDenpyoBango())
            {
                if (txtDenpyoBango.Modified)
                {
                    if (ValChk.IsEmpty(txtDenpyoBango.Text) || Util.ToDecimal(txtDenpyoBango.Text) <= 0)
                    {
                        DataClear();
                    }
                    else
                    {
                        // 仕訳データの読込
                        mtbList.Visible = false;
                        DataLoad(UInfo.KaishaCd, UInfo.KaikeiNendo, txtDenpyoBango.Text);
                        mtbList.Visible = true;

                        // 自動仕訳作成で無い場合、伝票複写可に（伝票が読み込まれた時）
                        if (_autoDataFlg == 0 && lblMode.Text != MODE_NEW) this.btnShiftF4.Enabled = true;
                    }
                    txtDenpyoBango.Modified = false;    // プロパティクリア
                }
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(lblMode.Text) ? "登録" : "更新") + "しますか？";
            if (this._externalCall == 1) msg = "確定しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 出納帳からの呼び出し
            if (this._externalCall == 1)
            {
                // 入力内容のDataTableを返し終了
                this.OutData = GetDtSuito();

                this._externalCall = 0;
                this.DialogResult = DialogResult.OK;
                // 更新処理からのクリア時のメッセージ抑止
                this._fromF6 = 1;
                this.IS_INPUT = 1;
                base.PressEsc();
                return;
            }

            decimal denpyoBango = 0;

            try
            {
                DateTime upd = System.DateTime.Now;

                // 伝票抽出用WHERE句
                string where = "KAISHA_CD = @KAISHA_CD";
                where += " AND SHISHO_CD = @SHISHO_CD";
                where += " AND DENPYO_BANGO = @DENPYO_BANGO";
                where += " AND KAIKEI_NENDO = @KAIKEI_NENDO";

                // 伝票更新ロック取得
                ZaUtil.UpdateLockOn(this.UInfo, this.Dba);

                // トランザクション開始
                this.Dba.BeginTransaction();

                // 仕訳伝票データ登録
                if (MODE_NEW.Equals(this.lblMode.Text))
                {
                    // 伝票番号
                    denpyoBango = ZaUtil.GetNewDenpyoBango(this.UInfo, this.Dba);
                    // データ追加
                    ArrayList alParamsZmDenpyo = SetZmShiwakeDenpyoParams(denpyoBango, upd);
                    this.Dba.Insert("TB_ZM_SHIWAKE_DENPYO", (DbParamCollection)alParamsZmDenpyo[0]);
                }
                else if (MODE_EDIT.Equals(this.lblMode.Text))
                {
                    // 伝票番号
                    denpyoBango = Util.ToDecimal(this.txtDenpyoBango.Text);
                    // データ更新
                    ArrayList alParamsZmDenpyo = SetZmShiwakeDenpyoParams(denpyoBango, upd);
                    this.Dba.Update("TB_ZM_SHIWAKE_DENPYO", (DbParamCollection)alParamsZmDenpyo[1],
                                        where, (DbParamCollection)alParamsZmDenpyo[0]);
                }

                // 仕訳明細データ登録(常に保存済レコード削除＆編集レコード挿入)
                // (明細データ削除)
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                this.Dba.Delete("TB_ZM_SHIWAKE_MEISAI", where, dpc);
                // (明細データ追加)
                for (int i = 0; i < mtbList.Content.Records.Count; i++)
                {
                    UTable.CRecord rec = mtbList.Content.Records[i];

                    // 借方レコード
                    if (!ValChk.IsEmpty(rec.Fields["DrKANJO_KAMOKU_CD"].Value))
                    {
                        ArrayList alParamsZmMeisai = SetZmShiwakeMeisaiParams(
                                                        denpyoBango, 1, 0, upd, DENPYO_KUBUN, rec);
                        this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParamsZmMeisai[0]);

                        // 消費税レコード
                        if (!ValChk.IsEmpty(rec.Fields["DrSHOHIZEI_KINGAKU"].Value))
                        {
                            ArrayList alParamsZmShohizei = SetZmShiwakeMeisaiParams(
                                                            denpyoBango, 1, 1, upd, DENPYO_KUBUN, rec);
                            this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParamsZmShohizei[0]);
                        }
                    }
                    // 貸方レコード
                    if (!ValChk.IsEmpty(rec.Fields["CrKANJO_KAMOKU_CD"].Value))
                    {
                        ArrayList alParamsZmMeisai = SetZmShiwakeMeisaiParams(
                                                        denpyoBango, 2, 0, upd, DENPYO_KUBUN, rec);
                        this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParamsZmMeisai[0]);

                        // 消費税レコード
                        if (!ValChk.IsEmpty(rec.Fields["CrSHOHIZEI_KINGAKU"].Value))
                        {
                            ArrayList alParamsZmShohizei = SetZmShiwakeMeisaiParams(
                                                            denpyoBango, 2, 1, upd, DENPYO_KUBUN, rec);
                            this.Dba.Insert("TB_ZM_SHIWAKE_MEISAI", (DbParamCollection)alParamsZmShohizei[0]);
                        }
                    }
                }

                #region イレギュラー関連
                IrgLogInsert((MODE_NEW.Equals(this.lblMode.Text) ? 1 : 2));
                #endregion

                // トランザクションをコミット
                this.Dba.Commit();

                // 伝票更新ロック開放
                ZaUtil.UpdateLockOff(this.UInfo, this.Dba);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // 更新後処理
            this.lblMessage.Text = "前回登録伝票番号：" + Util.ToString(denpyoBango);

            // 外部呼出し時は閉じる
            if (this._externalCall != 0)
            {
                this._externalCall = 0;
                this.DialogResult = DialogResult.OK;
                // 更新処理からのクリア時のメッセージ抑止
                this._fromF6 = 1;
                this.IS_INPUT = 1;
                base.PressEsc();
                return;
            }

            // 更新処理からのクリア時のメッセージ抑止
            this._fromF6 = 1;

            // 編集内容クリア：ESC取消
            PressEsc();
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        public override void PressF7()
        {
            if (!this.btnF7.Enabled)
                return;

            // 行挿入
            if (_activeCRecord != null)
            {
                this.mtbList.FinishEdit();
                int i = this.mtbList.Content.Records.IndexOf(_activeCRecord);
                this.mtbList.Content.InsertRecord(i);
                this.mtbList.Content.LastAddedRecord.Fields["GYO_BANGO"].Value = i + 1;
                this.UTableGyoBangoRefresh();
            }
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            if (!this.btnF8.Enabled)
                return;

            // 行削除
            if (_activeCRecord != null)
            {
                if (_activeCRecord.Fields["GYO_BANGO"].Value != null)
                {
                    this.mtbList.Content.RemoveRecord(_activeCRecord);

                    // 合計エリア計算
                    CalcTotal();

                    this.UTableGyoBangoRefresh();
                }
            }
        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            if (!this.btnF9.Enabled)
                return;

            // 前行複写
            if (_activeCRecord != null)
            {
                int curIndex = this.mtbList.Content.Records.IndexOf(_activeCRecord);
                if (curIndex >= 0)
                {
                    // 複写元レコード取得
                    UTable.CRecord srcRec = this.mtbList.Content.Records[curIndex];

                    using (mtbList.RenderBlock())
                    {
                        // 貸借共通情報
                        _activeCRecord.Fields["GYO_BANGO"].Value = null;
                        _activeCRecord.Fields["TEKIYO_CD"].Value = srcRec.Fields["TEKIYO_CD"].Value;
                        _activeCRecord.Fields["TEKIYO"].Value = srcRec.Fields["TEKIYO"].Value;
                        // 借方情報
                        _activeCRecord.Fields["DrKANJO_KAMOKU_CD"].Value = srcRec.Fields["DrKANJO_KAMOKU_CD"].Value;
                        _activeCRecord.Fields["DrKANJO_KAMOKU_NM"].Value = srcRec.Fields["DrKANJO_KAMOKU_NM"].Value;
                        _activeCRecord.Fields["DrHOJO_KAMOKU_CD"].Value = srcRec.Fields["DrHOJO_KAMOKU_CD"].Value;
                        _activeCRecord.Fields["DrHOJO_KAMOKU_NM"].Value = srcRec.Fields["DrHOJO_KAMOKU_NM"].Value;
                        _activeCRecord.Fields["DrBUMON_CD"].Value = srcRec.Fields["DrBUMON_CD"].Value;
                        _activeCRecord.Fields["DrJIGYO_KUBUN"].Value = srcRec.Fields["DrJIGYO_KUBUN"].Value;
                        _activeCRecord.Fields["DrZEI_KUBUN"].Value = srcRec.Fields["DrZEI_KUBUN"].Value;
                        _activeCRecord.Fields["DrDENPYO_KINGAKU"].Value = srcRec.Fields["DrDENPYO_KINGAKU"].Value;
                        _activeCRecord.Fields["DrSHOHIZEI_KINGAKU"].Value = srcRec.Fields["DrSHOHIZEI_KINGAKU"].Value;
                        // 貸方情報
                        _activeCRecord.Fields["CrKANJO_KAMOKU_CD"].Value = srcRec.Fields["CrKANJO_KAMOKU_CD"].Value;
                        _activeCRecord.Fields["CrKANJO_KAMOKU_NM"].Value = srcRec.Fields["CrKANJO_KAMOKU_NM"].Value;
                        _activeCRecord.Fields["CrHOJO_KAMOKU_CD"].Value = srcRec.Fields["CrHOJO_KAMOKU_CD"].Value;
                        _activeCRecord.Fields["CrHOJO_KAMOKU_NM"].Value = srcRec.Fields["CrHOJO_KAMOKU_NM"].Value;
                        _activeCRecord.Fields["CrBUMON_CD"].Value = srcRec.Fields["CrBUMON_CD"].Value;
                        _activeCRecord.Fields["CrJIGYO_KUBUN"].Value = srcRec.Fields["CrJIGYO_KUBUN"].Value;
                        _activeCRecord.Fields["CrZEI_KUBUN"].Value = srcRec.Fields["CrZEI_KUBUN"].Value;
                        _activeCRecord.Fields["CrDENPYO_KINGAKU"].Value = srcRec.Fields["CrDENPYO_KINGAKU"].Value;
                        _activeCRecord.Fields["CrSHOHIZEI_KINGAKU"].Value = srcRec.Fields["CrSHOHIZEI_KINGAKU"].Value;
                        // 非表示フィールド
                        //_activeCRecord.Fields["DrKOJI_UMU"].Value = srcRec.Fields["DrKOJI_UMU"].Value;
                        _activeCRecord.Fields["DrKAZEI_KUBUN"].Value = srcRec.Fields["DrKAZEI_KUBUN"].Value;
                        //_activeCRecord.Fields["CrKOJI_UMU"].Value = srcRec.Fields["CrKOJI_UMU"].Value;
                        _activeCRecord.Fields["CrKAZEI_KUBUN"].Value = srcRec.Fields["CrKAZEI_KUBUN"].Value;

                        //// 工事・工種情報
                        //_activeCRecord.Fields["KOJI_CD"].Value = srcRec.Fields["KOJI_CD"].Value;
                        //_activeCRecord.Fields["KOJI_NM"].Value = srcRec.Fields["KOJI_NM"].Value;
                        //_activeCRecord.Fields["KOSHU_CD"].Value = srcRec.Fields["KOSHU_CD"].Value;
                        //_activeCRecord.Fields["KOSHU_NM"].Value = srcRec.Fields["KOSHU_NM"].Value;
                        // 編集可否
                        CFieldEnabled(_activeCRecord.Fields["DrHOJO_KAMOKU_CD"], CFieldEnabled(srcRec.Fields["DrHOJO_KAMOKU_CD"]));
                        CFieldEnabled(_activeCRecord.Fields["DrBUMON_CD"], CFieldEnabled(srcRec.Fields["DrBUMON_CD"]));
                        CFieldEnabled(_activeCRecord.Fields["DrDENPYO_KINGAKU"], CFieldEnabled(srcRec.Fields["DrDENPYO_KINGAKU"]));
                        CFieldEnabled(_activeCRecord.Fields["DrSHOHIZEI_KINGAKU"], CFieldEnabled(srcRec.Fields["DrSHOHIZEI_KINGAKU"]));
                        CFieldEnabled(_activeCRecord.Fields["CrHOJO_KAMOKU_CD"], CFieldEnabled(srcRec.Fields["CrHOJO_KAMOKU_CD"]));
                        CFieldEnabled(_activeCRecord.Fields["CrBUMON_CD"], CFieldEnabled(srcRec.Fields["CrBUMON_CD"]));
                        CFieldEnabled(_activeCRecord.Fields["CrDENPYO_KINGAKU"], CFieldEnabled(srcRec.Fields["CrDENPYO_KINGAKU"]));
                        CFieldEnabled(_activeCRecord.Fields["CrSHOHIZEI_KINGAKU"], CFieldEnabled(srcRec.Fields["CrSHOHIZEI_KINGAKU"]));

                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (Util.ToDecimal(Util.ToString(_activeCRecord.Fields["DrDENPYO_KINGAKU"].Value)) >= 0)
                        {
                            _activeCRecord.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                            _activeCRecord.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            _activeCRecord.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                            _activeCRecord.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                        if (Util.ToDecimal(Util.ToString(_activeCRecord.Fields["CrDENPYO_KINGAKU"].Value)) >= 0)
                        {
                            _activeCRecord.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                            _activeCRecord.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            _activeCRecord.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                            _activeCRecord.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                    }
                    // 新規行へのレコード番号付番号判定
                    if (_activeCRecord.Fields["GYO_BANGO"].Value == null)
                    {
                        // 貸借いずれかの科目コードが入力済なら行更新
                        if (!ValChk.IsEmpty(_activeCRecord.Fields["DrKANJO_KAMOKU_CD"].Value)
                            || !ValChk.IsEmpty(_activeCRecord.Fields["CrKANJO_KAMOKU_CD"].Value))
                        {
                            _activeCRecord.Fields["GYO_BANGO"].Value = mtbList.Content.Records.Count;
                            mtbList.Content.AddRecord();            // 新規行を追加

                            // 最新行までスクロールする
                            mtbList.SetVScrollValue(mtbList.Content.Records.Count * 40);
                        }
                    }
                    this._cpFlg = 1;
                    if (srcRec.Fields["DrKANJO_KAMOKU_CD"].Value != null)
                    {
                        this._prvKanjoKamoku = Util.ToDecimal(Util.ToString(srcRec.Fields["DrKANJO_KAMOKU_CD"].Value));
                    }
                    // 合計エリア計算
                    CalcTotal();

                    this.UTableGyoBangoRefresh();
                }
            }
        }

        /// <summary>
        /// F10キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            base.PressEsc();
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            if (!this.btnF11.Enabled)
                return;

            // 元帳照会はプロジェクトへ取り込み

            //// アセンブリのロード
            //System.Reflection.Assembly asm = System.Reflection.Assembly.LoadFrom("ZAMC9511.exe");
            //// フォーム作成
            //Type t = asm.GetType("jp.co.fsi.zam.zamc9511.ZAMC9511");
            //if (t != null)
            //{
            //    Object obj = Activator.CreateInstance(t);
            //    if (obj != null)
            //    {
            //        // タブの一部として埋め込む
            //        BasePgForm frm = (BasePgForm)obj;
            //        frm.ShowDialog(this);
            //    }
            //}
            using (ZMDE1033 frm1033 = new ZMDE1033())
            {
                frm1033.ShowDialog(this);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            if (!this.btnF12.Enabled)
                return;

            // UTableカレント行摘要コード
            string tekiyoCd = "";
            string tekiyoNm = "";
            if (_activeCRecord != null)
            {
                tekiyoCd = Util.ToString(_activeCRecord.Fields["TEKIYO_CD"].Value);
                tekiyoNm = Util.ToString(_activeCRecord.Fields["TEKIYO"].Value);
            }
            // 編集中仕訳事例データ
            DataTable dt = GetDtJirei();

            // 保存可能な仕訳事例が存在すれば実行
            if (dt.Rows.Count > 0)
            {
                // (デバッグ用)
                //dt.WriteXml("C:\\WORK\\JIREI.xml");
                // (デバッグ用)

                // 仕訳事例の登録はプロジェクトへ取り込み

                //// アセンブリのロード
                //Assembly asm = Assembly.LoadFrom("ZAMC9521.exe");
                //// フォーム作成
                //Type t = asm.GetType("jp.co.fsi.zam.zamc9521.ZAMC9521");
                //if (t != null)
                //{
                //    Object obj = Activator.CreateInstance(t);
                //    if (obj != null)
                //    {
                //        // タブの一部として埋め込む
                //        zamc9521.ZAMC9521 frm = (zamc9521.ZAMC9521)obj;
                //        frm.TekiyoCd = tekiyoCd;
                //        frm.DtJirei = GetDtJirei();
                //        frm.ShowDialog(this);
                //    }
                //}

                using (ZMDE1035 frm1035 = new ZMDE1035())
                {
                    // 出納帳画面からも使用できる様にパラメータ渡し
                    object[] inData = new object[3];
                    inData[0] = tekiyoCd;
                    inData[1] = tekiyoNm;
                    inData[2] = dt;
                    frm1035.InData = inData;

                    frm1035.ShowDialog(this);
                }
            }
        }
        #endregion

        #region イベント

        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
        }

        /// <summary>
        /// 担当者コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoCd_Validating(object sender, CancelEventArgs e)
        {
            // 担当者名を表示
            string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);
            this.lblTantoshaNm.Text = name;
        }

        /// <summary>
        /// 伝票番号の指定時処理
        /// </summary>
        private void txtDenpyoBango_Validating(object sender, CancelEventArgs e)
        {
            if (IsValidDenpyoBango())
            {
                if (txtDenpyoBango.Modified)
                {
                    if (ValChk.IsEmpty(txtDenpyoBango.Text) || Util.ToDecimal(txtDenpyoBango.Text) <= 0)
                    {
                        DataClear();
                    }
                    else
                    {
                        // 仕訳データの読込
                        mtbList.Visible = false;
                        DataLoad(UInfo.KaishaCd, UInfo.KaikeiNendo, txtDenpyoBango.Text);
                        mtbList.Visible = true;

                        // 自動仕訳作成で無い場合、伝票複写可に（伝票が読み込まれた時）
                        if (_autoDataFlg == 0 && lblMode.Text != MODE_NEW) this.btnShiftF4.Enabled = true;
                    }
                    txtDenpyoBango.Modified = false;    // プロパティクリア

                    // 更新モード時は取消
                    //this.btnEsc.Text = "Esc" + "\n\r" + "取消";
                    if (this.MenuFrm != null)
                    {
                        //((Button)this.MenuFrm.Controls["btnEsc"]).Text = "Esc" + "\n\r" + "\n\r" + "取消";
                    }
                    this.IS_INPUT = 1;

                    // 自動仕訳作成分のロックメッセージ
                    if (_autoDataFlg != 0)
                    {
                        Msg.Info("自動仕訳作成から作成された伝票の為、変更できません");
                    }
                }
            }
            else
            {
                e.Cancel = true;
                this.txtDenpyoBango.SelectAll();
            }
        }

        /// <summary>
        /// 伝票日付(年)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearDenpyoDate_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtGengoYearDenpyoDate.Text, this.txtGengoYearDenpyoDate.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYearDenpyoDate.SelectAll();
            }
            else
            {
                this.txtGengoYearDenpyoDate.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearDenpyoDate.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 伝票日付(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthDenpyoDate_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthDenpyoDate.Text, this.txtMonthDenpyoDate.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthDenpyoDate.SelectAll();
            }
            else
            {
                this.txtMonthDenpyoDate.Text = Util.ToString(IsValid.SetMonth(this.txtMonthDenpyoDate.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 伝票日付(日)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayDenpyoDate_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayDenpyoDate.Text, this.txtDayDenpyoDate.MaxLength))
            {
                e.Cancel = true;
                this.txtDayDenpyoDate.SelectAll();
            }
            else
            {
                this.txtDayDenpyoDate.Text = Util.ToString(IsValid.SetDay(this.txtDayDenpyoDate.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// KeyDown時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_KeyDown(object sender, KeyEventArgs e)
        {
            // メニューからのKeyDownは受け付けない
            if (this.ActiveControl != null &&
                this.ActiveControl.GetType().BaseType != null &&
                (this.ActiveControl.GetType().BaseType == typeof(BaseForm)
                || this.ActiveControl.GetType().BaseType == typeof(BasePgForm)))
            {
                return;
            }
            switch (e.KeyCode)
            {
                case Keys.F4:
                    if (e.Shift)
                    {
                        this.PressShiftF4();
                        return;
                    }
                    break;
            }
        }

        /// <summary>
        /// ボタンクリック時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShiftF4_Click(object sender, EventArgs e)
        {
            this.PressShiftF4();
        }

        /// <summary>
        /// Shift + F4キー押下時処理
        /// </summary>
        private void PressShiftF4()
        {
            if (!this.btnShiftF4.Enabled)
                return;

            if (_activeCRecord != null)
            {
                return;
            }
            // 表示中の伝票内容を読み込みモードを新規として伝票の複写とする

            // 仕訳データの読込
            mtbList.Visible = false;
            DataLoad(UInfo.KaishaCd, UInfo.KaikeiNendo, txtDenpyoBango.Text);
            mtbList.Visible = true;

            // 伝票情報クリア
            txtDenpyoBango.Text = "";
            // 編集モード
            lblMode.Text = MODE_NEW;
            // 仕訳作成区分
            _autoDataFlg = 0;
            this.btnShiftF4.Enabled = false;

            txtGengoYearDenpyoDate.Focus();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 共通データ読込
        /// </summary>
        private void InitFormDataLoad()
        {
            #region 勘定科目データ
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                StringBuilder cols = new StringBuilder();
                cols.Append("KAISHA_CD");
                cols.Append(", KAIKEI_NENDO");
                cols.Append(", KANJO_KAMOKU_CD");
                cols.Append(", KANJO_KAMOKU_NM");
                cols.Append(", KANJO_KAMOKU_KANA_NM");
                cols.Append(", KAMOKU_BUNRUI_CD");
                cols.Append(", KAMOKU_BUNRUI_NM");
                cols.Append(", KAMOKU_KUBUN");
                cols.Append(", KAMOKU_KUBUN_NM");
                cols.Append(", TAISHAKU_KUBUN");
                cols.Append(", TAISHAKU_KUBUN_NM");
                cols.Append(", KARIKATA_ZEI_KUBUN");
                cols.Append(", KARIKATA_ZEI_KUBUN_NM");
                cols.Append(", KARI_KAZEI_KUBUN");
                cols.Append(", KARI_TORIHIKI_KUBUN");
                cols.Append(", KARI_ZEI_RITSU");
                cols.Append(", KARI_TAISHAKU_KUBUN");
                cols.Append(", KARI_HENKAN_KUBUN");
                cols.Append(", KASHIKATA_ZEI_KUBUN");
                cols.Append(", KASHIKATA_ZEI_KUBUN_NM");
                cols.Append(", KASHI_KAZEI_KUBUN");
                cols.Append(", KASHI_TORIHIKI_KUBUN");
                cols.Append(", KASHI_ZEI_RITSU");
                cols.Append(", KASHI_TAISHAKU_KUBUN");
                cols.Append(", KASHI_HENKAN_KUBUN");
                cols.Append(", BUMON_UMU");
                cols.Append(", BUMON_UMU_NM");
                cols.Append(", HOJO_KAMOKU_UMU");
                cols.Append(", ASHOJO_KAMOKU_UMU_NM");
                cols.Append(", HOJO_SHIYO_KUBUN");
                cols.Append(", HOJO_SHIYO_KUBUN_NM");
                cols.Append(", KOJI_UMU");
                cols.Append(", KOJI_UMU_NM");
                cols.Append(", SHIYO");
                cols.Append(", SHIYO_NM");
                cols.Append(", SHUKEI_CD");
                cols.Append(", SHUKEI_KEISAN_KUBUN");
                StringBuilder where = new StringBuilder();
                where.Append("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
                _dtKanjoKamoku = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "VI_ZM_KANJO_KAMOKU"
                                , Util.ToString(where), dpc);
            }
            #endregion

            #region 税区分データ
            {
                StringBuilder sql = new StringBuilder();
                sql.Append("SELECT ZEI_KUBUN");
                sql.Append(", ZEI_KUBUN_NM");
                sql.Append(", KAZEI_KUBUN");
                sql.Append(", TORIHIKI_KUBUN");
                sql.Append(", ZEI_RITSU");
                sql.Append(", TAISHAKU_KUBUN");
                sql.Append(", HENKAN_KUBUN");
                sql.Append(" FROM TB_ZM_F_ZEI_KUBUN");
                _dtZeiKubun = this.Dba.GetDataTableFromSql(Util.ToString(sql));
            }
            #endregion

            #region 部門データ
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                StringBuilder cols = new StringBuilder();
                cols.Append("KAISHA_CD");
                cols.Append(", BUMON_CD");
                cols.Append(", BUMON_NM");
                cols.Append(", BUMON_KANA_NM");
                cols.Append(", JIGYO_KUBUN");
                cols.Append(", JIGYO_KUBUN_NM");
                cols.Append(", HAIFU_KUBUN");
                StringBuilder where = new StringBuilder();
                where.Append("KAISHA_CD = @KAISHA_CD");
                _dtBumon = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "VI_ZM_BUMON"
                                , Util.ToString(where), dpc);
            }
            #endregion
        }

        /// <summary>
        /// 明細部の初期化
        /// </summary>
        private void InitDetailArea()
        {
            UTable.CRecordProvider rp = new UTable.CRecordProvider();
            CLayoutBuilder lb = new CLayoutBuilder(CLayoutBuilder.EOrientation.ROW);
            UTable.CFieldDesc fd;

            // 工事情報を外し

            #region UTableフィールド設定・配置
            //****************************************************************************************************
            // 次の行・列位置へ各フィールドを配置
            //
            // 　　　 (　　　　　　　借方　　　　　　　　 )　　　　     (　　　　　　　貸方　　　　　　　　 )
            //0 行番号 科目CD 科目名 部門CD　 　　　 金額　   摘要CD ダミ 科目CD 科目名 部門CD　 　　   金額　   工事CD 工事名
            //1 　　　 補助CD 補助名 事業区分 税区分 % 消費税 摘要     　 補助CD 補助名 事業区分 税区分 % 消費税 工種CD 工種名 
            //  0      1     2      3        4     5 6     7      8    9      10    11       12    13 14    15    16  
            //
            //(サイズ)
            //  30,    30,   90,    20,      20,   25,75,  30,    130, 30,    90,   20,      20,   25,75    30,   35
            //****************************************************************************************************
            // TabOrderのため摘要エリアからフィールド作成
            // 行番号フィールド
            fd = rp.AddField("GYO_BANGO", new CSjTextFieldProvider("NO."), lb.Set(0, 0).Next(2, 1));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.MIDDLE;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ  明朝", 12F, FontStyle.Regular);
            // 借方勘定科目CDフィールド
            fd = rp.AddField("DrKANJO_KAMOKU_CD", new CSjTextFieldProvider("勘定科目"), lb.Set(0, 1).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.MergeCaption = "DrKANJO_KAMOKU_NM";
            // 借方勘定科目名フィールド
            fd = rp.AddField("DrKANJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(0, 2).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 借方補助科目CDフィールド
            fd = rp.AddField("DrHOJO_KAMOKU_CD", new CSjTextFieldProvider("補助科目"), lb.Set(1, 1).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.MergeCaption = "DrHOJO_KAMOKU_NM";
            // 借方補助科目名フィールド
            fd = rp.AddField("DrHOJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(1, 2).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 借方部門CDフィールド
            fd = rp.AddField("DrBUMON_CD", new CSjTextFieldProvider("部門"), lb.Set(0, 3).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 借方事業区分フィールド
            fd = rp.AddField("DrJIGYO_KUBUN", new CSjTextFieldProvider("事"), lb.Set(1, 3).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 借方税区分フィールド
            fd = rp.AddField("DrZEI_KUBUN", new CSjTextFieldProvider("税"), lb.Set(1, 4).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 借方伝票金額フィールド
            fd = rp.AddField("DrDENPYO_KINGAKU", new CSjTextFieldProvider("金　額"), lb.Set(0, 5).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 借方消費税率フィールド
            fd = rp.AddField("DrZEI_RITSU", new CSjTextFieldProvider("消費税"), lb.Set(1, 5).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.Setting.ForeColor = Color.Blue;
            fd.MergeCaption = "DrSHOHIZEI_KINGAKU";
            // 借方消費税金額フィールド
            fd = rp.AddField("DrSHOHIZEI_KINGAKU", new CSjTextFieldProvider(), lb.Set(1, 6).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 貸方勘定科目CDフィールド
            fd = rp.AddField("CrKANJO_KAMOKU_CD", new CSjTextFieldProvider("勘定科目"), lb.Set(0, 9).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.MergeCaption = "CrKANJO_KAMOKU_NM";
            // 貸方勘定科目名フィールド
            fd = rp.AddField("CrKANJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(0, 10).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 貸方補助科目CDフィールド
            fd = rp.AddField("CrHOJO_KAMOKU_CD", new CSjTextFieldProvider("補助科目"), lb.Set(1, 9).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.MergeCaption = "CrHOJO_KAMOKU_NM";
            // 貸方補助科目名フィールド
            fd = rp.AddField("CrHOJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(1, 10).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 貸方部門CDフィールド
            fd = rp.AddField("CrBUMON_CD", new CSjTextFieldProvider("部門"), lb.Set(0, 11).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 貸方事業区分フィールド
            fd = rp.AddField("CrJIGYO_KUBUN", new CSjTextFieldProvider("事"), lb.Set(1, 11).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 貸方税区分フィールド
            fd = rp.AddField("CrZEI_KUBUN", new CSjTextFieldProvider("税"), lb.Set(1, 12).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 貸方伝票金額フィールド
            fd = rp.AddField("CrDENPYO_KINGAKU", new CSjTextFieldProvider("金　額"), lb.Set(0, 13).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            // 貸方消費税率フィールド
            fd = rp.AddField("CrZEI_RITSU", new CSjTextFieldProvider("消費税"), lb.Set(1, 13).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.Setting.ForeColor = Color.Blue;
            fd.MergeCaption = "CrSHOHIZEI_KINGAKU";
            // 貸方消費税金額フィールド
            fd = rp.AddField("CrSHOHIZEI_KINGAKU", new CSjTextFieldProvider(), lb.Set(1, 14).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            fd.CreateCaption = false;
            //// 工事CDフィールド
            //fd = rp.AddField("KOJI_CD", new CSjTextFieldProvider("工　事"), lb.Set(0, 15).Next());
            //fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            //fd.MergeCaption = "KOJI_NM";
            //// 工事名フィールド
            //fd = rp.AddField("KOJI_NM", new CSjTextFieldProvider(), lb.Set(0, 16).Next());
            //fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            //fd.CreateCaption = false;
            //// 工種CDフィールド
            //fd = rp.AddField("KOSHU_CD", new CSjTextFieldProvider("工　種"), lb.Set(1, 15).Next());
            //fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            //fd.MergeCaption = "KOSHU_NM";
            //// 工種名フィールド
            //fd = rp.AddField("KOSHU_NM", new CSjTextFieldProvider(), lb.Set(1, 16).Next());
            //fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            //fd.Setting.Editable = UTable.EAllow.DISABLE;
            //fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            //fd.CreateCaption = false;
            // 摘要CDフィールド
            fd = rp.AddField("TEKIYO_CD", new CSjTextFieldProvider("摘　　　　要"), lb.Set(0, 7).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.MergeCaption = "TEKIYO";
            // 摘要CD右ダミーフィールド
            fd = rp.AddField("TEKIYO_CD_MIGI", new CSjTextFieldProvider(), lb.Set(0, 8).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 摘要フィールド
            fd = rp.AddField("TEKIYO", new CSjTextFieldProvider(), lb.Set(1, 7).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.ALLOW;
            //fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.CreateCaption = false;
            // (非表示)借方工事有無フィールド
            //fd = rp.AddField("DrKOJI_UMU", new CSjTextFieldProvider(), lb.Set(0, 17).Next());
            fd = rp.AddField("DrKOJI_UMU", new CSjTextFieldProvider(), lb.Set(0, 15).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)貸方工事有無フィールド
            //fd = rp.AddField("CrKOJI_UMU", new CSjTextFieldProvider(), lb.Set(1, 17).Next());
            fd = rp.AddField("CrKOJI_UMU", new CSjTextFieldProvider(), lb.Set(1, 15).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)借方課税区分フィールド
            //fd = rp.AddField("DrKAZEI_KUBUN", new CSjTextFieldProvider(), lb.Set(0, 18).Next());
            fd = rp.AddField("DrKAZEI_KUBUN", new CSjTextFieldProvider(), lb.Set(0, 16).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            // (非表示)貸方課税区分フィールド
            //fd = rp.AddField("CrKAZEI_KUBUN", new CSjTextFieldProvider(), lb.Set(1, 18).Next());
            fd = rp.AddField("CrKAZEI_KUBUN", new CSjTextFieldProvider(), lb.Set(1, 16).Next());
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;

            #endregion

            this.mtbList.Content.SetRecordProvider(rp);
            this.mtbList.Setting.CaptionBackColor = Color.LightSkyBlue;
            this.mtbList.Setting.CaptionForeColor = Color.Navy;
            this.mtbList.Setting.BackColor = SystemColors.HighlightText;
            this.mtbList.Setting.UserColResizable = UTable.EAllow.DISABLE;
            this.mtbList.CreateCaption(UTable.EHAlign.MIDDLE);
            this.mtbList.Setting.ContentBackColor = SystemColors.AppWorkspace;
            this.mtbList.KeyboardOperation.EnterBehavior = CKeyboardOperation.EBehavior.NEXT_EDITABLE_FIELD;
            this.mtbList.Cols.SetSize(30, 50, 150, 20, 20, 25, 100, 30, 280, 50, 150, 20, 20, 25, 100, 1, 1);
        }

        /// <summary>
        /// データの読込
        /// </summary>
        /// <param name="kaishaCd">会社コード</param>
        /// <param name="kaikeiNendo">会計年度</param>
        /// <param name="denpyoBango">伝票番号</param>
        private void DataLoad(string kaishaCd, decimal kaikeiNendo, string denpyoBango)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, UInfo.KaishaCd);//kaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, UInfo.KaikeiNendo);//kaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);

            StringBuilder cols;
            StringBuilder where;
            StringBuilder order;

            #region TB仕訳伝票データ
            cols = new StringBuilder("KAISHA_CD");
            cols.Append(" ,SHISHO_CD");
            cols.Append(" ,DENPYO_BANGO");
            cols.Append(" ,KAIKEI_NENDO");
            cols.Append(" ,DENPYO_DATE");
            cols.Append(" ,SHOHYO_BANGO");
            cols.Append(" ,TANTOSHA_CD");
            cols.Append(" ,SHIWAKE_GYOSU");
            cols.Append(" ,REGIST_DATE");
            cols.Append(" ,UPDATE_DATE");
            where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND DENPYO_BANGO = @DENPYO_BANGO");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            DataTable dtDenpyo = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                            , "TB_ZM_SHIWAKE_DENPYO"
                            , Util.ToString(where), dpc);
            if (dtDenpyo.Rows.Count == 1)
            {
                // 伝票情報を画面表示
                DataRow dr = dtDenpyo.Rows[0];
                txtTantoshaCd.Text = Util.ToString(dr["TANTOSHA_CD"]);
                txtDenpyoBango.Text = Util.ToString(Util.ToDecimal(txtDenpyoBango.Text));

                this.DENPYO_DATE = Util.ToDate(dr["DENPYO_DATE"]).ToString("yyyy/MM/dd");
                //SetJpDenpyoDate(Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba));
                string[] fixJpDate = Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba);
                //this.txtGengoYearDenpyoDate.Text = fixJpDate[0];
                this.lblGengoDenpyoDate.Text = fixJpDate[0];
                this.txtGengoYearDenpyoDate.Text = fixJpDate[2];
                this.txtMonthDenpyoDate.Text = fixJpDate[3];
                this.txtDayDenpyoDate.Text = fixJpDate[4];
                // 水揚支所
                this.txtMizuageShishoCd.Text = Util.ToString(dr["SHISHO_CD"]);
                this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

                txtShohyoBango.Text = Util.ToString(dr["SHOHYO_BANGO"]);
                lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);

                // 決算区分を取得
                cols = new StringBuilder("TOP 1 KESSAN_KUBUN");
                where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND DENPYO_BANGO = @DENPYO_BANGO");
                where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
                DataTable dtKessanKbn = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "TB_ZM_SHIWAKE_MEISAI"
                                , Util.ToString(where), dpc);

                if (dtKessanKbn.Rows.Count == 1)
                {
                    // 決算区分によりチェックを変える
                    if (Util.ToInt(Util.ToString(dtKessanKbn.Rows[0]["KESSAN_KUBUN"])) == 1)
                    {
                        rdoKessanKubun1.Checked = true;
                    }
                    else
                    {
                        rdoKessanKubun0.Checked = true;
                    }
                }

                // 編集モード
                lblMode.Text = MODE_EDIT;
            }
            else
            {
                // 伝票情報をクリア
                txtDenpyoBango.Text = "";
                txtShohyoBango.Text = "";

                // 編集モード
                lblMode.Text = MODE_NEW;
            }
            #endregion

            // 仕訳作成区分
            _autoDataFlg = 0;
            int autoDataFlgBackup = _autoDataFlg;

            #region VI仕訳明細データ
            #region cols.Append(照会列)
            cols = new StringBuilder("KAISHA_CD");
            cols.Append(", DENPYO_DATE");
            cols.Append(", DENPYO_BANGO");
            cols.Append(", GYO_BANGO");
            cols.Append(", TAISHAKU_KUBUN");
            cols.Append(", MEISAI_KUBUN");
            cols.Append(", DENPYO_KUBUN");
            cols.Append(", SHOHYO_BANGO");
            cols.Append(", TANTOSHA_CD");
            cols.Append(", TANTOSHA_NM");
            cols.Append(", KANJO_KAMOKU_CD");
            cols.Append(", KANJO_KAMOKU_NM");
            // 他に使っていないようなので、時間短縮のためコメントアウト。
            // cols.Append(", AITE_KANJO_KAMOKU_CD");
            // cols.Append(", AITE_KANJO_KAMOKU_NM");
            cols.Append(", BUMON_UMU");
            cols.Append(", HOJO_UMU");
            //cols.Append(", KOJI_UMU");
            cols.Append(", HOJO_KAMOKU_CD");
            cols.Append(", HOJO_KAMOKU_NM");
            cols.Append(", BUMON_CD");
            cols.Append(", BUMON_NM");
            //cols.Append(", KOJI_CD");
            //cols.Append(", KOJI_NM");
            //cols.Append(", KOSHU_CD");
            //cols.Append(", KOSHU_NM");
            cols.Append(", TEKIYO_CD");
            cols.Append(", TEKIYO");
            cols.Append(", ZEI_KUBUN");
            cols.Append(", ZEI_KUBUN_NM");
            cols.Append(", KAZEI_KUBUN");
            cols.Append(", TORIHIKI_KUBUN");
            cols.Append(", ZEI_RITSU");
            cols.Append(", JIGYO_KUBUN");
            cols.Append(", SHOHIZEI_NYURYOKU_HOHO");
            cols.Append(", SHOHIZEI_HENKO");
            cols.Append(", KESSAN_KUBUN");
            cols.Append(", ZEIKOMI_KINGAKU");
            cols.Append(", ZEINUKI_KINGAKU");
            cols.Append(", SHOHIZEI_KINGAKU");
            cols.Append(", SHIWAKE_SAKUSEI_KUBUN");
            #endregion
            where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND DENPYO_BANGO = @DENPYO_BANGO");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND MEISAI_KUBUN = 0");
            order = new StringBuilder("GYO_BANGO ASC");
            order.Append(", TAISHAKU_KUBUN ASC");
            order.Append(", MEISAI_KUBUN ASC");
            DataTable dtMeisai = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                            , "VI_ZM_SHIWAKE_MEISAI"
                            , Util.ToString(where), Util.ToString(order), dpc);
            /// **************************************************
            /// UTableレンダリングブロック開始
            /// **************************************************
            using (mtbList.RenderBlock())
            {
                this.mtbList.Content.ClearRecord();         // UTableレコードクリア
                decimal gyo = 0;                            // 明細行番号
                foreach (DataRow dr in dtMeisai.Rows)
                {
                    // 仕訳明細をUTableレコードへセット
                    UTable.CRecord rec;

                    // 行番号ブレイクでレコード追加 
                    if (Util.ToDecimal(Util.ToString(dr["GYO_BANGO"])) > gyo)
                    {
                        gyo = Util.ToDecimal(Util.ToString(dr["GYO_BANGO"]));      // ブレイク判定値

                        rec = this.mtbList.Content.AddRecord();
                        // 貸借レコード共通情報
                        rec.Fields["GYO_BANGO"].Value = Util.ToString(dr["GYO_BANGO"]);
                        rec.Fields["TEKIYO_CD"].Value = ZaUtil.FormatNum(dr["TEKIYO_CD"]);
                        rec.Fields["TEKIYO"].Value = Util.ToString(dr["TEKIYO"]);
                    }

                    // 仕訳情報をセット
                    rec = this.mtbList.Content.LastAddedRecord;
                    if (Util.ToString(dr["TAISHAKU_KUBUN"]).Equals("1"))            // 借方レコード
                    {
                        rec.Fields["DrKANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KANJO_KAMOKU_CD"]);
                        rec.Fields["DrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                        rec.Fields["DrHOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["HOJO_KAMOKU_CD"]);
                        rec.Fields["DrHOJO_KAMOKU_NM"].Value = Util.ToString(dr["HOJO_KAMOKU_NM"]);
                        rec.Fields["DrBUMON_CD"].Value = ZaUtil.FormatNum(dr["BUMON_CD"]);
                        rec.Fields["DrJIGYO_KUBUN"].Value = ZaUtil.FormatNum(dr["JIGYO_KUBUN"]);
                        rec.Fields["DrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["ZEI_KUBUN"]);
                        rec.Fields["DrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                            ZaUtil.GetDenpyoKingaku(Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                    Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])),
                                                    this.UInfo));
                        rec.Fields["DrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["SHOHIZEI_KINGAKU"]);
                        //rec.Fields["DrKOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                        rec.Fields["DrKAZEI_KUBUN"].Value = Util.ToString(dr["KAZEI_KUBUN"]);
                        rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                        // 編集可否
                        CFieldEnabled(rec.Fields["DrHOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(dr["HOJO_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["DrBUMON_CD"], Util.ToInt(Util.ToString(dr["BUMON_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(dr["KANJO_KAMOKU_CD"])) > 0);
                        CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], Util.ToInt(Util.ToString(dr["SHOHIZEI_HENKO"])) == 1);
                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])) >= 0)
                        {
                            rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                    }
                    else if (Util.ToString(dr["TAISHAKU_KUBUN"]).Equals("2"))       // 貸方レコード
                    {
                        rec.Fields["CrKANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KANJO_KAMOKU_CD"]);
                        rec.Fields["CrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                        rec.Fields["CrHOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["HOJO_KAMOKU_CD"]);
                        rec.Fields["CrHOJO_KAMOKU_NM"].Value = Util.ToString(dr["HOJO_KAMOKU_NM"]);
                        rec.Fields["CrBUMON_CD"].Value = ZaUtil.FormatNum(dr["BUMON_CD"]);
                        rec.Fields["CrJIGYO_KUBUN"].Value = Util.ToString(dr["JIGYO_KUBUN"]);
                        rec.Fields["CrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["ZEI_KUBUN"]);
                        rec.Fields["CrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                            ZaUtil.GetDenpyoKingaku(Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                    Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])),
                                                    this.UInfo));
                        rec.Fields["CrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["SHOHIZEI_KINGAKU"]);
                        //rec.Fields["CrKOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                        rec.Fields["CrKAZEI_KUBUN"].Value = Util.ToString(dr["KAZEI_KUBUN"]);
                        rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                        // 編集可否
                        CFieldEnabled(rec.Fields["CrHOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(dr["HOJO_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["CrBUMON_CD"], Util.ToInt(Util.ToString(dr["BUMON_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(dr["KANJO_KAMOKU_CD"])) > 0);
                        CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], Util.ToInt(Util.ToString(dr["SHOHIZEI_HENKO"])) == 1);
                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (Util.ToDecimal(Util.ToString(Util.ToString(dr["ZEINUKI_KINGAKU"]))) >= 0)
                        {
                            rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                    }
                    //// 工事・工種情報
                    //if (Util.ToInt(dr["KOJI_UMU"]) == 1)
                    //{
                    //    rec.Fields["KOJI_CD"].Value = ZaUtil.FormatNum(dr["KOJI_CD"]);
                    //    rec.Fields["KOJI_NM"].Value = Util.ToString(dr["KOJI_NM"]);
                    //    rec.Fields["KOSHU_CD"].Value = ZaUtil.FormatNum(dr["KOSHU_CD"]);
                    //    rec.Fields["KOSHU_NM"].Value = Util.ToString(dr["KOSHU_NM"]);
                    //}
                    //// 工事・工種編集可否
                    //if ((rec.Fields["DrKOJI_UMU"].Value != null && Util.ToInt(rec.Fields["DrKOJI_UMU"].Value) == 1)
                    //    || (rec.Fields["CrKOJI_UMU"].Value != null && Util.ToInt(rec.Fields["CrKOJI_UMU"].Value) == 1))
                    //{
                    //    CFieldEnabled(rec.Fields["KOJI_CD"], true);
                    //    CFieldEnabled(rec.Fields["KOSHU_CD"], true);
                    //}
                    //else
                    //{
                    //    CFieldEnabled(rec.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(rec.Fields["KOSHU_CD"], false);
                    //}

                    // 仕訳作成区分
                    _autoDataFlg = Util.ToInt(Util.ToString(dr["SHIWAKE_SAKUSEI_KUBUN"]));
                }
                autoDataFlgBackup = _autoDataFlg;

                // 設定がチェックしない場合は無条件にチェックしないへ
                if (_autoDataSetting == 0)
                    _autoDataFlg = 0;
            }
            /// **************************************************
            /// UTableレンダリングブロック終了
            /// **************************************************
            mtbList.Render();

            #endregion

            mtbList.Content.AddRecord();            // 新規行を追加

            CalcTotal();                            // 合計表示

            #region イレギュラー関連
            IrgLogSet(autoDataFlgBackup);
            #endregion

            // 
            this.IS_INPUT = 1;
        }

        /// <summary>
        /// データの読込（出納帳呼び出し）
        /// </summary>
        /// <param name="dt">表示用DataTable</param>
        private void DataLoadFukugo(DataTable dt)
        {
            #region TB仕訳伝票データ
            {
                // 伝票情報を画面表示
                DataRow dr = dt.Rows[0];
                txtTantoshaCd.Text = Util.ToString(dr["TANTOSHA_CD"]);
                //txtDenpyoBango.Text = Util.ToString(Util.ToDecimal(txtDenpyoBango.Text));
                txtDenpyoBango.Text = Util.ToString(dr["DENPYO_BANGO"]);

                this.DENPYO_DATE = Util.ToDate(dr["DENPYO_DATE"]).ToString("yyyy/MM/dd");
                //SetJpDenpyoDate(Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba));
                string[] fixJpDate = Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba);
                //this.txtGengoYearDenpyoDate.Text = fixJpDate[0];
                this.lblGengoDenpyoDate.Text = fixJpDate[0];
                this.txtGengoYearDenpyoDate.Text = fixJpDate[2];
                this.txtMonthDenpyoDate.Text = fixJpDate[3];
                this.txtDayDenpyoDate.Text = fixJpDate[4];
                // 水揚支所
                this.txtMizuageShishoCd.Text = Util.ToString(dr["SHISHO_CD"]);
                this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

                txtShohyoBango.Text = Util.ToString(dr["SHOHYO_BANGO"]);
                lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);

                // 決算区分によりチェックを変える
                if (Util.ToInt(this._bInfo.KessanKubun) == 1)
                {
                    rdoKessanKubun1.Checked = true;
                }
                else
                {
                    rdoKessanKubun0.Checked = true;
                }

                // 編集モード
                lblMode.Text = MODE_EDIT;
            }
            #endregion

            #region VI仕訳明細データ
            /// **************************************************
            /// UTableレンダリングブロック開始
            /// **************************************************
            using (mtbList.RenderBlock())
            {
                this.mtbList.Content.ClearRecord();         // UTableレコードクリア
                decimal gyo = 0;                            // 明細行番号
                foreach (DataRow dr in dt.Rows)
                {
                    // 仕訳明細をUTableレコードへセット
                    UTable.CRecord rec;

                    // 行番号ブレイクでレコード追加 
                    if (Util.ToDecimal(Util.ToString(dr["GYO_BANGO"])) > gyo)
                    {
                        gyo = Util.ToDecimal(Util.ToString(dr["GYO_BANGO"]));      // ブレイク判定値

                        rec = this.mtbList.Content.AddRecord();
                        // 貸借レコード共通情報
                        rec.Fields["GYO_BANGO"].Value = Util.ToString(Util.ToString(dr["GYO_BANGO"]));
                        rec.Fields["TEKIYO_CD"].Value = ZaUtil.FormatNum(dr["TEKIYO_CD"]);
                        rec.Fields["TEKIYO"].Value = Util.ToString(dr["TEKIYO"]);
                    }

                    // 仕訳情報をセット
                    rec = this.mtbList.Content.LastAddedRecord;
                    if (Util.ToString(dr["TAISHAKU_KUBUN"]).Equals("1"))            // 借方レコード
                    {
                        rec.Fields["DrKANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KANJO_KAMOKU_CD"]);
                        rec.Fields["DrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                        rec.Fields["DrHOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["HOJO_KAMOKU_CD"]);
                        rec.Fields["DrHOJO_KAMOKU_NM"].Value = Util.ToString(dr["HOJO_KAMOKU_NM"]);
                        rec.Fields["DrBUMON_CD"].Value = ZaUtil.FormatNum(dr["BUMON_CD"]);
                        rec.Fields["DrJIGYO_KUBUN"].Value = ZaUtil.FormatNum(dr["JIGYO_KUBUN"]);
                        rec.Fields["DrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["ZEI_KUBUN"]);
                        rec.Fields["DrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                            ZaUtil.GetDenpyoKingaku(Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                    Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])),
                                                    this.UInfo));
                        rec.Fields["DrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["SHOHIZEI_KINGAKU"]);
                        //rec.Fields["DrKOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                        rec.Fields["DrKAZEI_KUBUN"].Value = Util.ToString(dr["KAZEI_KUBUN"]);
                        rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                        // 編集可否
                        CFieldEnabled(rec.Fields["DrHOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(dr["HOJO_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["DrBUMON_CD"], Util.ToInt(Util.ToString(dr["BUMON_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(dr["KANJO_KAMOKU_CD"])) > 0);
                        CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], Util.ToInt(Util.ToString(dr["SHOHIZEI_HENKO"])) == 1);
                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])) >= 0)
                        {
                            rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                    }
                    else if (Util.ToString(dr["TAISHAKU_KUBUN"]).Equals("2"))       // 貸方レコード
                    {
                        rec.Fields["CrKANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KANJO_KAMOKU_CD"]);
                        rec.Fields["CrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                        rec.Fields["CrHOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["HOJO_KAMOKU_CD"]);
                        rec.Fields["CrHOJO_KAMOKU_NM"].Value = Util.ToString(dr["HOJO_KAMOKU_NM"]);
                        rec.Fields["CrBUMON_CD"].Value = ZaUtil.FormatNum(dr["BUMON_CD"]);
                        rec.Fields["CrJIGYO_KUBUN"].Value = Util.ToString(dr["JIGYO_KUBUN"]);
                        rec.Fields["CrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["ZEI_KUBUN"]);
                        rec.Fields["CrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                            ZaUtil.GetDenpyoKingaku(Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                                    Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])),
                                                    this.UInfo));
                        rec.Fields["CrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["SHOHIZEI_KINGAKU"]);
                        //rec.Fields["CrKOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                        rec.Fields["CrKAZEI_KUBUN"].Value = Util.ToString(dr["KAZEI_KUBUN"]);
                        rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                        // 編集可否
                        CFieldEnabled(rec.Fields["CrHOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(dr["HOJO_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["CrBUMON_CD"], Util.ToInt(Util.ToString(dr["BUMON_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(dr["KANJO_KAMOKU_CD"])) > 0);
                        CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], Util.ToInt(Util.ToString(dr["SHOHIZEI_HENKO"])) == 1);
                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])) >= 0)
                        {
                            rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                    }

                    // 仕訳作成区分
                    _autoDataFlg = Util.ToInt(Util.ToString(dr["SHIWAKE_SAKUSEI_KUBUN"]));
                }

                // 設定がチェックしない場合は無条件にチェックしないへ
                if (_autoDataSetting == 0)
                    _autoDataFlg = 0;
            }
            /// **************************************************
            /// UTableレンダリングブロック終了
            /// **************************************************
            mtbList.Render();

            #endregion

            mtbList.Content.AddRecord();            // 新規行を追加

            CalcTotal();                            // 合計表示

            // 
            this.IS_INPUT = 1;
        }

        /// <summary>
        /// データのクリア
        /// </summary>
        private void DataClear()
        {
            // 伝票情報クリア
            txtDenpyoBango.Text = "";
            txtShohyoBango.Text = "";

            // 明細情報クリア
            mtbList.Content.ClearRecord();
            mtbList.Content.AddRecord();            // 新規行を追加

            // 合計実行
            CalcTotal();

            // 編集モード
            lblMode.Text = MODE_NEW;

            // 仕訳作成区分
            _autoDataFlg = 0;
            this.btnShiftF4.Enabled = false;
            this.lblBiko.Text = "";
            this._fromF6 = 0;

            // 
            this.IS_INPUT = 1;
            // 登録モード時は終了
            //this.btnEsc.Text = "Esc" + "\n\r" + "終了";
            if (this.MenuFrm != null)
            {
                //((Button)this.MenuFrm.Controls["btnEsc"]).Text = "Esc" + "\n\r" + "\n\r" + "終了";
            }

            #region イレギュラー関連
            this._irgLog = new LogTbl();
            #endregion

            // フォーカス初期値
            //this.txtDenpyoBango.Focus();
            this.txtTantoshaCd.Focus();
        }

        /// <summary>
        /// 合計エリアの計算
        /// </summary>
        private void CalcTotal()
        {
            decimal DrGokei = 0;
            decimal CrGokei = 0;
            decimal DrZei = 0;
            decimal CrZei = 0;
            for (int i = 0; i < mtbList.Content.Records.Count; i++)
            {
                UTable.CRecord rec = mtbList.Content.Records[i];
                DrGokei += Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value));
                CrGokei += Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value));
                DrZei += Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value));
                CrZei += Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value));
            }
            // 合計情報
            lblDrDenpyoKingaku.Text = ZaUtil.FormatCur(DrGokei);
            lblDrShohizeiKingaku.Text = ZaUtil.FormatCur(DrZei);
            lblCrDenpyoKingaku.Text = ZaUtil.FormatCur(CrGokei);
            lblCrShohizeiKingaku.Text = ZaUtil.FormatCur(CrZei);
            // 伝票金額、消費税がゼロ以下は文字色を赤にする
            if (DrGokei >= 0)
            {
                lblDrDenpyoKingaku.ForeColor=Color.Black;
                lblDrShohizeiKingaku.ForeColor = Color.Black;
            }
            else
            {
                lblDrDenpyoKingaku.ForeColor = Color.Red;
                lblDrShohizeiKingaku.ForeColor = Color.Red;
            }
            if (CrGokei >= 0)
            {
                lblCrDenpyoKingaku.ForeColor = Color.Black;
                lblCrShohizeiKingaku.ForeColor = Color.Black;
            }
            else
            {
                lblCrDenpyoKingaku.ForeColor = Color.Red;
                lblCrShohizeiKingaku.ForeColor = Color.Red;
            }

            if (DrGokei == CrGokei)
            {
                lblInfo.Text = "";
            }
            else
            {
                lblInfo.Text = "(差額：" + ZaUtil.FormatCur(Math.Abs(DrGokei - CrGokei)) + ")";
            }
        }

        /// <summary>
        /// 仕訳事例登録用データテーブル作成
        /// </summary>
        private DataTable GetDtJirei()
        {
            DataTable dt = new DataTable();

            // データテーブル名
            dt.TableName = "TABLE1";
            // フィールド定義
            dt.Columns.Add("GYO_BANGO");
            dt.Columns.Add("TAISHAKU_KUBUN");
            dt.Columns.Add("MEISAI_KUBUN");
            dt.Columns.Add("KANJO_KAMOKU_CD");
            dt.Columns.Add("HOJO_KAMOKU_CD");
            dt.Columns.Add("BUMON_CD");
            dt.Columns.Add("TEKIYO");
            dt.Columns.Add("ZEIKOMI_KINGAKU");
            dt.Columns.Add("ZEINUKI_KINGAKU");
            dt.Columns.Add("SHOHIZEI_KINGAKU");
            dt.Columns.Add("ZEI_KUBUN");
            dt.Columns.Add("KAZEI_KUBUN");
            dt.Columns.Add("TORIHIKI_KUBUN");
            dt.Columns.Add("ZEI_RITSU");
            dt.Columns.Add("JIGYO_KUBUN");
            dt.Columns.Add("SHOHIZEI_NYURYOKU_HOHO");
            dt.Columns.Add("SHOHIZEI_HENKO");
            dt.Columns.Add("KESSAN_KUBUN");

            // 税率取得用
            DateTime DenpyoDate =
                Util.ConvAdDate(this.lblGengoDenpyoDate.Text, this.txtGengoYearDenpyoDate.Text,
                                this.txtMonthDenpyoDate.Text, this.txtDayDenpyoDate.Text, Dba);

            // レコード追加(消費税レコードは追加しない)
            for (int i = 0; i < mtbList.Content.Records.Count; i++)
            {
                UTable.CRecord rec = mtbList.Content.Records[i];

                // 借方レコード
                if (!ValChk.IsEmpty(rec.Fields["DrKANJO_KAMOKU_CD"].Value))
                {
                    DataRow dr = dt.NewRow();
                    dr["GYO_BANGO"] = rec.Fields["GYO_BANGO"].Value;
                    dr["TAISHAKU_KUBUN"] = "1";
                    dr["MEISAI_KUBUN"] = "0";
                    dr["KANJO_KAMOKU_CD"] = rec.Fields["DrKANJO_KAMOKU_CD"].Value;
                    dr["HOJO_KAMOKU_CD"] = rec.Fields["DrHOJO_KAMOKU_CD"].Value;
                    dr["BUMON_CD"] = rec.Fields["DrBUMON_CD"].Value;
                    dr["ZEI_KUBUN"] = rec.Fields["DrZEI_KUBUN"].Value;
                    dr["JIGYO_KUBUN"] = rec.Fields["DrJIGYO_KUBUN"].Value;
                    dr["TEKIYO"] = rec.Fields["TEKIYO"].Value;

                    dr["ZEIKOMI_KINGAKU"] = ZaUtil.GetZeikomiKingaku(
                                                Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)),
                                                Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value)),
                                                this.UInfo);
                    dr["ZEINUKI_KINGAKU"] = ZaUtil.GetZeinukiKingaku(
                                                Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)),
                                                Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value)),
                                                this.UInfo);
                    dr["SHOHIZEI_KINGAKU"] = rec.Fields["DrSHOHIZEI_KINGAKU"].Value;
                    DataRow drZei = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(rec.Fields["DrZEI_KUBUN"].Value)));
                    dr["KAZEI_KUBUN"] = drZei["KAZEI_KUBUN"];
                    dr["TORIHIKI_KUBUN"] = drZei["TORIHIKI_KUBUN"];

                    // 税率
                    //dr["ZEI_RITSU"] = drZei["ZEI_RITSU"];
                    dr["ZEI_RITSU"] = TaxUtil.GetTaxRate(DenpyoDate, Util.ToInt(Util.ToString(rec.Fields["DrZEI_KUBUN"].Value)), this.Dba);

                    dr["SHOHIZEI_NYURYOKU_HOHO"] = this.UInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
                    dr["SHOHIZEI_HENKO"] = rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0";
                    dr["KESSAN_KUBUN"] = rdoKessanKubun0.Checked ? 0 : 1;
                    dt.Rows.Add(dr);
                }
                // 貸方レコード
                if (!ValChk.IsEmpty(rec.Fields["CrKANJO_KAMOKU_CD"].Value))
                {
                    DataRow dr = dt.NewRow();
                    dr["GYO_BANGO"] = rec.Fields["GYO_BANGO"].Value;
                    dr["TAISHAKU_KUBUN"] = "2";
                    dr["MEISAI_KUBUN"] = "0";
                    dr["KANJO_KAMOKU_CD"] = rec.Fields["CrKANJO_KAMOKU_CD"].Value;
                    dr["HOJO_KAMOKU_CD"] = rec.Fields["CrHOJO_KAMOKU_CD"].Value;
                    dr["BUMON_CD"] = rec.Fields["CrBUMON_CD"].Value;
                    dr["ZEI_KUBUN"] = rec.Fields["CrZEI_KUBUN"].Value;
                    dr["JIGYO_KUBUN"] = rec.Fields["CrJIGYO_KUBUN"].Value;
                    dr["TEKIYO"] = rec.Fields["TEKIYO"].Value;

                    dr["ZEIKOMI_KINGAKU"] = ZaUtil.GetZeikomiKingaku(
                                                Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)),
                                                Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value)),
                                                this.UInfo);
                    dr["ZEINUKI_KINGAKU"] = ZaUtil.GetZeinukiKingaku(
                                                Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)),
                                                Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value)),
                                                this.UInfo);
                    dr["SHOHIZEI_KINGAKU"] = rec.Fields["CrSHOHIZEI_KINGAKU"].Value;
                    DataRow drZei = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_KUBUN"].Value))));
                    dr["KAZEI_KUBUN"] = drZei["KAZEI_KUBUN"];
                    dr["TORIHIKI_KUBUN"] = drZei["TORIHIKI_KUBUN"];

                    // 税率
                    //dr["ZEI_RITSU"] = drZei["ZEI_RITSU"];
                    dr["ZEI_RITSU"] = TaxUtil.GetTaxRate(DenpyoDate, Util.ToInt(Util.ToString(rec.Fields["CrZEI_KUBUN"].Value)), this.Dba);

                    dr["SHOHIZEI_NYURYOKU_HOHO"] = this.UInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
                    dr["SHOHIZEI_HENKO"] = rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0";
                    dr["KESSAN_KUBUN"] = rdoKessanKubun0.Checked ? 0 : 1;
                    dt.Rows.Add(dr);
                }
            }

            return dt;
        }

        /// <summary>
        /// 勘定科目レコードを取得
        /// </summary>
        /// <param name="kamokuCd">科目コード</param>
        /// <returns>科目レコード又はnull</returns>
        private DataRow GetKanjoKamokuDataRow(string kamokuCd)
        {
            DataRow ret = null;
            DataRow[] foundRows;

            foundRows = _dtKanjoKamoku.Select("KANJO_KAMOKU_CD = '" + kamokuCd + "'");
            if (foundRows.Length > 0)
            {
                ret = foundRows[0];
            }

            return ret;
        }

        /// <summary>
        /// 税区分レコードを取得
        /// </summary>
        /// <param name="zeiKubun">税区分</param>
        /// <returns>税区分レコード又はnull</returns>
        private DataRow GetZeiKubunDataRow(string zeiKubun)
        {
            DataRow ret = null;
            DataRow[] foundRows;

            foundRows = _dtZeiKubun.Select("ZEI_KUBUN = '" + zeiKubun + "'");
            if (foundRows.Length > 0)
            {
                ret = foundRows[0];
            }

            return ret;
        }

        /// <summary>
        /// 部門レコードを取得
        /// </summary>
        /// <param name="bumonCd">部門コード</param>
        /// <returns>部門レコード又はnull</returns>
        private DataRow GetBumonDataRow(string bumonCd)
        {
            DataRow ret = null;
            DataRow[] foundRows;

            foundRows = _dtBumon.Select("BUMON_CD = '" + bumonCd + "'");
            if (foundRows.Length > 0)
            {
                ret = foundRows[0];
            }

            return ret;
        }

        /// <summary>
        /// 年月日の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoDenpyoDate.Text, this.txtGengoYearDenpyoDate.Text,
                this.txtMonthDenpyoDate.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayDenpyoDate.Text) > lastDayInMonth)
            {
                this.txtDayDenpyoDate.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            if (this._externalCall == 1)
            {
                // 出納帳呼び出し時は日付範囲
                SetJp(Util.ConvJpDate(
                      this.FixFromToDate(
                      Util.ConvAdDate(this.lblGengoDenpyoDate.Text,
                                      this.txtGengoYearDenpyoDate.Text,
                                      this.txtMonthDenpyoDate.Text,
                                      this.txtDayDenpyoDate.Text, this.Dba)), this.Dba));
            }
            else
            {
                SetJp(Util.ConvJpDate(
                      ZaUtil.FixNendoDate(
                      Util.ConvAdDate(this.lblGengoDenpyoDate.Text,
                                      this.txtGengoYearDenpyoDate.Text,
                                      this.txtMonthDenpyoDate.Text,
                                      this.txtDayDenpyoDate.Text, this.Dba), this.UInfo), this.Dba));
            }
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblGengoDenpyoDate.Text = arrJpDate[0];
            this.txtGengoYearDenpyoDate.Text = arrJpDate[2];
            this.txtMonthDenpyoDate.Text = arrJpDate[3];
            this.txtDayDenpyoDate.Text = arrJpDate[4];


            // 伝票日付の変更に伴う税率変更処理
            DateTime denpyoDate = Util.ConvAdDate(
                this.lblGengoDenpyoDate.Text,
                this.txtGengoYearDenpyoDate.Text,
                this.txtMonthDenpyoDate.Text,
                this.txtDayDenpyoDate.Text,
                this.Dba);

            // 新規は無条件、修正時は日付が変わったら行う
            if ((MODE_EDIT.Equals(lblMode.Text) && this.DENPYO_DATE != denpyoDate.ToString("yyyy/MM/dd")) ||
                 MODE_NEW.Equals(lblMode.Text))
            {
                decimal zeiRt;
                // 日付が変わった場合には無条件に税率を再設定して金額も再計算
                for (int i = 0; i < mtbList.Content.Records.Count; i++)
                {
                    UTable.CRecord rec = mtbList.Content.Records[i];
                    // 必要な部分の税率を見て差異が出ている所は書き直して金額も再計算
                    if (!ValChk.IsEmpty(rec.Fields["DrZEI_KUBUN"].Value))
                    {
                        zeiRt = TaxUtil.GetTaxRate(denpyoDate, Util.ToInt(Util.ToString(rec.Fields["DrZEI_KUBUN"].Value)), this.Dba);
                        rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(zeiRt);
                        decimal zei = ZaUtil.CalcConsumptionTax(Util.ToDecimal(Util.ToString(rec.Fields["DrZEI_RITSU"].Value).Replace("%", "")),
                            Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)), this.UInfo, this.Dba);
                        if (zei != 0)
                        {
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Value = Util.FormatNum(zei);
                        }
                        else
                        {
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Value = "";
                        }
                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (zei >= 0)
                        {
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                    }
                    if (!ValChk.IsEmpty(rec.Fields["CrZEI_KUBUN"].Value))
                    {
                        zeiRt = TaxUtil.GetTaxRate(denpyoDate, Util.ToInt(Util.ToString(rec.Fields["CrZEI_KUBUN"].Value)), this.Dba);
                        rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(zeiRt);
                        decimal zei = ZaUtil.CalcConsumptionTax(Util.ToDecimal(Util.ToString(rec.Fields["CrZEI_RITSU"].Value).Replace("%", "")),
                            Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)), this.UInfo, this.Dba);
                        if (zei != 0)
                        {
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Value = Util.FormatNum(zei);
                        }
                        else
                        {
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Value = "";
                        }
                        // 伝票金額、消費税がゼロ以下は文字色を赤にする
                        if (zei >= 0)
                        {
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                        }
                        else
                        {
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                        }
                    }
                }
                //合計計算
                CalcTotal();

                this.DENPYO_DATE = denpyoDate.ToString("yyyy/MM/dd");
            }
        }

        /// <summary>
        /// 日付範囲内日付に変換（出納帳呼び出し時の日付範囲）
        /// </summary>
        /// <param name="date">対象日付</param>
        /// <returns>補正後の日付</returns>
        private DateTime FixFromToDate(DateTime date)
        {
            DateTime dateFr = this._bInfo.DenpyoDateFr;
            DateTime dateTo = this._bInfo.DenpyoDateTo;
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }

        /// <summary>
        /// 税区分の選択
        /// </summary>
        /// <param name="required">必須判定</param>
        /// <param name="field"></param>
        /// <returns></returns>
        private string SelectZeiKubun(bool required, UTable.CField field)
        {
            string ret = "";
            bool selected = false;

            // 伝票日付
            DateTime denpyoDate = Util.ConvAdDate(
                this.lblGengoDenpyoDate.Text,
                this.txtGengoYearDenpyoDate.Text,
                this.txtMonthDenpyoDate.Text,
                this.txtDayDenpyoDate.Text,
                this.Dba);

            string pre = string.Empty;
            if (Util.ToString(field.Key).Substring(0, 2) == "Dr") pre = "Dr";
            if (Util.ToString(field.Key).Substring(0, 2) == "Cr") pre = "Cr";

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1061.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.zm.zmcm1061.ZMCM1065");
            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    // タブの一部として埋め込む
                    BasePgForm frm = (BasePgForm)obj;
                    frm.InData = Util.ToString(field.Record.Fields[pre + "ZEI_KUBUN"].Value);

                    // 検索ダイアログを表示する
                    while (!selected)
                    {
                        frm.ShowDialog(this);

                        if (frm.DialogResult == DialogResult.OK)
                        {
                            string[] result = (string[])frm.OutData;

                            // 選択結果を基準日適合チェック
                            decimal taxRate = 0;
                            DialogResult ans = ConfPastZeiKbn(denpyoDate, result[0], ref taxRate, Util.ToDecimal(Util.ToString(result[2])));

                            if (ans == DialogResult.OK)
                            {
                                // 税区分を選択して終了
                                ret = result[0];
                                selected = true;
                                if (pre.Length != 0)
                                {
                                    // 税情報をセット
                                    field.Record.Fields[pre + "ZEI_KUBUN"].Value = result[0];
                                    field.Record.Fields[pre + "ZEI_RITSU"].Value = ZaUtil.FormatPercentage(Util.ToDecimal(Util.ToString(result[2])));
                                    field.Record.Fields[pre + "KAZEI_KUBUN"].Value = Util.ToString(result[3]);
                                    // 消費税額の算出
                                    decimal zei = 0;
                                    if (!ValChk.IsEmpty(field.Record.Fields[pre + "DENPYO_KINGAKU"].Value))
                                    {
                                        zei = ZaUtil.CalcConsumptionTax(Util.ToDecimal(result[2]),
                                                Util.ToDecimal(Util.ToString(field.Record.Fields[pre + "DENPYO_KINGAKU"].Value)), this.UInfo, this.Dba);
                                    }
                                    if (zei != 0)
                                    {
                                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Value = Util.FormatNum(zei);
                                    }
                                    else
                                    {
                                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Value = "";
                                    }
                                    // 伝票金額、消費税がゼロ以下は文字色を赤にする
                                    if (zei >= 0)
                                    {
                                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                                    }
                                    else
                                    {
                                        field.Record.Fields[pre + "SHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                                    }
                                    //合計計算
                                    CalcTotal();
                                }
                            }
                        }
                        else
                        {
                            // 税区分を選択しないで終了
                            if (!required) selected = true;
                        }
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// 税率変更時の確認処理
        /// </summary>
        /// <param name="baseDate"></param>
        /// <param name="zeiKbn"></param>
        /// <param name="zeiritsu"></param>
        /// <param name="taxRate"></param>
        /// <returns></returns>
        private DialogResult ConfPastZeiKbn(DateTime baseDate, string zeiKbn, ref decimal zeiritsu, decimal taxRate)
        {
            zeiritsu = 0;

            // 概要：日付を元に、現在の税率と異なる税率を持つ税区分が選択されていれば確認メッセージを出す

            // 税区分を元にTB_ZM_F_ZEI_KUBUNから設定値を取得
            // ※万が一取得できなければNGを返す
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, zeiKbn);
            DataTable dtZeiKbn = this.Dba.GetDataTableByConditionWithParams(
                "*", "TB_ZM_F_ZEI_KUBUN", "ZEI_KUBUN = @ZEI_KUBUN", dpc);

            if (dtZeiKbn.Rows.Count == 0) return DialogResult.No;

            // 非課税であればダイアログを出さない
            //if (Util.ToInt(dtZeiKbn.Rows[0]["KAZEI_KUBUN"]) == 0) return DialogResult.OK;
            if (Util.ToInt(dtZeiKbn.Rows[0]["KAZEI_KUBUN"]) != 1) return DialogResult.OK;

            // 適用開始日が基準日より小さいレコードを摘要開始日の降順で取得し、
            // その1件目の新消費税を保持
            // 税区分から取得した税率と消費税情報テーブルから取得した税率を比較し、
            // 異なっていれば確認メッセージを表示
            decimal taxRateBase = GetZeiritsu(baseDate);
            //decimal taxRateJudge = Util.ToDecimal(dtZeiKbn.Rows[0]["ZEI_RITSU"]);
            decimal taxRateJudge = TaxUtil.GetTaxRate(baseDate, Util.ToInt(zeiKbn), this.Dba); // 新税率から取得
            // 税率指定時はそちらを優先
            if (taxRate != -1) taxRateJudge = taxRate;

            if (taxRateBase != taxRateJudge)
            {
                DialogResult result = Msg.ConfOKCancel("基準日時点の税率と異なる税率を持つ税区分が選択されています。"
                    + Environment.NewLine + "処理を続行してよろしいですか？");

                if (result == DialogResult.OK) zeiritsu = taxRateJudge;

                return result;
            }
            else
            {
                zeiritsu = taxRateJudge;
                return DialogResult.OK;
            }
        }
        private new DialogResult ConfPastZeiKbn(DateTime baseDate, string zeiKbn, ref decimal zeiritsu)
        {
            return ConfPastZeiKbn(baseDate, zeiKbn, ref zeiritsu, -1);
        }

        /// <summary>
        /// ファンクションキーの制御
        /// </summary>
        private void SetFunctionKey()
        {
            // コントロール別ファンクションキー表示内容
            switch (GetActiveObjectName())
            {
                case "DrDENPYO_KINGAKU":
                case "CrDENPYO_KINGAKU":
                    //btnF1.Text = "F1" + "\n\r" + "税額入力";
                    break;
                default:
                    //btnF1.Text = "F1" + "\n\r" + "検索";
                    break;
            }
            // コントロール別ファンクションキー利用可否制御
            switch (GetActiveObjectName())
            {
                // F1○　F3×　F4×　F5×　F6×　F7×　F8×　F9×
                case "txtMizuageShishoCd":
                case "txtTantoshaCd":
                case "txtDenpyoBango":
                    this.btnF1.Enabled = true;
                    this.btnF3.Enabled = false;
                    this.btnF4.Enabled = false;
                    this.btnF5.Enabled = false;
                    this.btnF6.Enabled = false;
                    this.btnF7.Enabled = false;
                    this.btnF8.Enabled = false;
                    this.btnF9.Enabled = false;
                    /*
                    if (this.MenuFrm != null)
                    {
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF04"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF05"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = false;
                    }
                    */
                    break;

                // F1○　F3○　F4×　F5×　F6○　F7×　F8×　F9×
                case "txtGengoYearDenpyoDate":
                    this.btnF1.Enabled = true;
                    this.btnF3.Enabled = true;
                    this.btnF4.Enabled = false;
                    this.btnF5.Enabled = false;
                    this.btnF6.Enabled = true;
                    this.btnF7.Enabled = false;
                    this.btnF8.Enabled = false;
                    this.btnF9.Enabled = false;
                    /*
                    if (this.MenuFrm != null)
                    {
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF04"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF05"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = false;
                    }
                    */
                    break;

                // F1×　F3○　F4×　F5×　F6○　F7×　F8×　F9×
                case "txtMonthDenpyoDate":
                case "txtDayDenpyoDate":
                case "txtShohyoBango":
                case "rdoKessanKubun0":
                case "rdoKessanKubun1":
                    this.btnF1.Enabled = false;
                    this.btnF3.Enabled = true;
                    this.btnF4.Enabled = false;
                    this.btnF5.Enabled = false;
                    this.btnF6.Enabled = true;
                    this.btnF7.Enabled = false;
                    this.btnF8.Enabled = false;
                    this.btnF9.Enabled = false;
                    /*
                    if (this.MenuFrm != null)
                    {
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF04"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF05"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = false;
                    }
                    */
                    break;

                // UTable.CField別制御
                // F1○　F3○　F4○　F5○　F6○　F7○　F8○
                case "DrKANJO_KAMOKU_CD":
                case "CrKANJO_KAMOKU_CD":
                //case "DrHOJO_KAMOKU_CD":
                //case "CrHOJO_KAMOKU_CD":
                //case "DrBUMON_CD":
                //case "CrBUMON_CD":
                    this.btnF1.Enabled = true;
                    this.btnF3.Enabled = true;
                    this.btnF4.Enabled = true;
                    this.btnF5.Enabled = true;
                    this.btnF6.Enabled = true;
                    this.btnF7.Enabled = true;
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = true;
                    /*
                    if (this.MenuFrm != null)
                    {
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF04"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF05"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = true;
                    }
                    */
                    break;

                case "DrHOJO_KAMOKU_CD":
                case "CrHOJO_KAMOKU_CD":
                case "DrBUMON_CD":
                case "CrBUMON_CD":
                    this.btnF1.Enabled = CFieldEnabled(_activeCRecord.Fields[GetActiveObjectName()]);
                    this.btnF3.Enabled = true;
                    this.btnF4.Enabled = true;
                    this.btnF5.Enabled = true;
                    this.btnF6.Enabled = true;
                    this.btnF7.Enabled = true;
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = true;
                    /*
                    if (this.MenuFrm != null)
                    {
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = this.btnF1.Enabled;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF04"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF05"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = true;
                    }
                    */
                    break;

                // F1○税入力　F3○　F4○　F5○　F6○　F7○　F8○
                case "DrDENPYO_KINGAKU":
                case "CrDENPYO_KINGAKU":
                    this.btnF1.Enabled = true;
                    this.btnF3.Enabled = true;
                    this.btnF4.Enabled = true;
                    this.btnF5.Enabled = true;
                    this.btnF6.Enabled = true;
                    this.btnF7.Enabled = true;
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = true;
                    /*
                    if (this.MenuFrm != null)
                    {
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF04"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF05"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = true;
                    }
                    */
                    break;

                // F1×　F3○　F4○　F5○　F6○　F7○　F8○
                case "DrSHOHIZEI_KINGAKU":
                case "CrSHOHIZEI_KINGAKU":
                    this.btnF1.Enabled = false;
                    this.btnF3.Enabled = true;
                    this.btnF4.Enabled = true;
                    this.btnF5.Enabled = true;
                    this.btnF6.Enabled = true;
                    this.btnF7.Enabled = true;
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = true;
                    /*
                    if (this.MenuFrm != null)
                    {
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF04"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF05"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = true;
                    }
                    */
                    break;

                // F1○　F3○　F4×　F5×　F6○　F7○　F8○
                case "TEKIYO_CD":
                    this.btnF1.Enabled = true;
                    this.btnF3.Enabled = true;
                    this.btnF4.Enabled = false;
                    this.btnF5.Enabled = false;
                    this.btnF6.Enabled = true;
                    this.btnF7.Enabled = true;
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = true;
                    /*
                    if (this.MenuFrm != null)
                    {
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF04"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF05"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = true;
                    }
                    */
                    break;

                // F1×　F3○　F4×　F5×　F6○　F7○　F8○
                case "TEKIYO":
                case "KOJI_CD":
                case "KOSHU_CD":
                    this.btnF1.Enabled = false;
                    this.btnF3.Enabled = true;
                    this.btnF4.Enabled = false;
                    this.btnF5.Enabled = false;
                    this.btnF6.Enabled = true;
                    this.btnF7.Enabled = true;
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = true;
                    /*
                    if (this.MenuFrm != null)
                    {
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF01"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF04"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF05"].Enabled = false;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = true;
                        ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = true;
                    }
                    */
                    break;

                default:
                    // NONE
                    break;
            }

            // 自動仕訳作成分の変更不可の場合（更新ボタンの操作不可）
            if (_autoDataFlg != 0)
            {
                this.btnF3.Enabled = false;
                this.btnF6.Enabled = false;
                this.btnF7.Enabled = false;
                this.btnF8.Enabled = false;
                this.btnF9.Enabled = false;
                /*
                if (this.MenuFrm != null)
                {
                    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = false;
                    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF06"].Enabled = false;
                    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF07"].Enabled = false;
                    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF08"].Enabled = false;
                    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF09"].Enabled = false;
                }
                */
            }
            // 出納帳呼出しの削除不可（出納帳側で削除）
            if (this._externalCall == 1)
            {
                this.btnF3.Enabled = false;
                /*
                if (this.MenuFrm != null)
                {
                    ((ToolStrip)((ToolStripContainer)this.MenuFrm.Controls["toolStripContainer1"]).BottomToolStripPanel.Controls["bottmTool"]).Items["tsBtnF03"].Enabled = false;
                }
                */
            }
        }

        /// <summary>
        /// アクティブコントロール名・アクティブフィールド名を取得
        /// </summary>
        private string GetActiveObjectName()
        {
            string ret;
            ret = this.ActiveCtlNm;

            // アクティブなCFieldが存在する場合はフィールド名を返す
            if (_activeCField != null) ret = Util.ToString(_activeCField.Key);

            return ret;
        }

        /// <summary>
        /// 仕訳行数
        /// </summary>
        private int GetShiwakeGyosu()
        {
            int ret = 0;

            // 行番号がセット済行数を数える
            for (int i = 0; i < mtbList.Content.Records.Count; i++)
            {
                UTable.CRecord rec = mtbList.Content.Records[i];
                if (!ValChk.IsEmpty(rec.Fields["GYO_BANGO"].Value))
                {
                    ret++;
                }
            }


            return ret;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_DENPYOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="upd">更新日時</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmShiwakeDenpyoParams(decimal denpyoBango, DateTime upd)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(lblMode.Text))
            {
                // 更新パラメータ設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
                updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, upd);
            }
            else if (MODE_EDIT.Equals(lblMode.Text))
            {
                // Where句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                alParams.Add(whereParam);
            }

            // 伝票日付
            updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime,
                Util.ConvAdDate(this.lblGengoDenpyoDate.Text, this.txtGengoYearDenpyoDate.Text,
                                this.txtMonthDenpyoDate.Text, this.txtDayDenpyoDate.Text, Dba));
            // 証憑番号
            updParam.SetParam("@SHOHYO_BANGO", SqlDbType.VarChar, 10, this.txtShohyoBango.Text);
            // 担当者コード
            updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.txtTantoshaCd.Text);
            // 仕訳行数
            updParam.SetParam("@SHIWAKE_GYOSU", SqlDbType.Decimal, 6, GetShiwakeGyosu());
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, upd);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_MEISAIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="taishakuKubun">貸借区分</param>
        /// <param name="meisaiKubun">明細区分</param>
        /// <param name="upd">更新日時</param>
        /// <param name="denpyoKubun">伝票区分</param>
        /// <param name="rec">UTableレコード</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetZmShiwakeMeisaiParams(decimal denpyoBango, decimal taishakuKubun,
            decimal meisaiKubun, DateTime upd, decimal denpyoKubun, UTable.CRecord rec)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 会社コード
            updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 支所コード
            updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            // 伝票番号
            updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, denpyoBango);
            // 行番号
            updParam.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(rec.Fields["GYO_BANGO"].Value)));
            // 貸借区分
            updParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, taishakuKubun);
            // 明細区分
            updParam.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, meisaiKubun);
            // 会計年度
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            // 登録日
            updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, upd);
            // 伝票区分
            updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, denpyoKubun);
            // 伝票日付
            updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime,
                Util.ConvAdDate(this.lblGengoDenpyoDate.Text, this.txtGengoYearDenpyoDate.Text,
                                this.txtMonthDenpyoDate.Text, this.txtDayDenpyoDate.Text, Dba));
            /// 明細区分・貸借レコード別処理
            if (meisaiKubun == 0 && taishakuKubun == 1)
            {
                /// 明細区分０・借方レコード
                updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrKANJO_KAMOKU_CD"].Value)));
                updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrHOJO_KAMOKU_CD"].Value)));
                updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrBUMON_CD"].Value)));
                updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_KUBUN"].Value)));
                updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrJIGYO_KUBUN"].Value)));
                updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1,
                    rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0");
                // 税込・税抜金額・消費税額
                updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15,
                    ZaUtil.GetZeikomiKingaku(
                        Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)),
                        Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value)),
                        this.UInfo));
                updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15,
                    ZaUtil.GetZeinukiKingaku(
                        Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)),
                        Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value)),
                        this.UInfo));
                updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value)));
                // 税区分情報
                DataRow dr = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_KUBUN"].Value))));
                updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, dr["KAZEI_KUBUN"]);

                // 税率の手修正可にする為、入力をそのまま保存
                //updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, dr["ZEI_RITSU"]);
                updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_RITSU"].Value).Replace("%", "")));

                updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, dr["TORIHIKI_KUBUN"]);
                //// 工事・工種情報
                //if (Util.ToInt(rec.Fields["DrKOJI_UMU"].Value) == 1)
                //{
                //    updParam.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(rec.Fields["KOJI_CD"].Value));
                //    updParam.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(rec.Fields["KOSHU_CD"].Value));
                //}
                //else
                //{
                //    updParam.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, 0);
                //    updParam.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, 0);
                //}
            }
            else if (meisaiKubun == 0 && taishakuKubun == 2)
            {
                /// 明細区分０・貸方レコード
                updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrKANJO_KAMOKU_CD"].Value)));
                updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrHOJO_KAMOKU_CD"].Value)));
                updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrBUMON_CD"].Value)));
                updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_KUBUN"].Value)));
                updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrJIGYO_KUBUN"].Value)));
                updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1,
                    rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0");
                /// 税込・税抜金額・消費税額
                updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15,
                    ZaUtil.GetZeikomiKingaku(
                        Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)),
                        Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value)),
                        this.UInfo));
                updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15,
                    ZaUtil.GetZeinukiKingaku(
                        Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)),
                        Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value)),
                        this.UInfo));
                updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value)));
                // 税区分情報
                DataRow dr = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_KUBUN"].Value))));
                updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, dr["KAZEI_KUBUN"]);

                // 税率の手修正可にする為、入力をそのまま保存
                //updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, dr["ZEI_RITSU"]);
                updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_RITSU"].Value).Replace("%", "")));

                updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, dr["TORIHIKI_KUBUN"]);
                //// 工事・工種情報
                //if (Util.ToInt(rec.Fields["CrKOJI_UMU"].Value) == 1)
                //{
                //    updParam.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(rec.Fields["KOJI_CD"].Value));
                //    updParam.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(rec.Fields["KOSHU_CD"].Value));
                //}
                //else
                //{
                //    updParam.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, 0);
                //    updParam.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, 0);
                //}
            }
            if (meisaiKubun == 1 && taishakuKubun == 1)
            {
                ///// 借方勘定科目レコード取得
                //DataRow dr = GetKanjoKamokuDataRow(Util.ToString(rec.Fields["DrKANJO_KAMOKU_CD"].Value));
                ///// 明細区分１・借方レコード
                //updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4,
                //    Util.ToInt(dr["TAISHAKU_KUBUN"]) == 1 ? this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]
                //                                            : this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]);
                //updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 4, 0);
                //updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, 0);
                //updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, 0);

                //// 事業区分
                ////updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrJIGYO_KUBUN"].Value)));
                //// 消費税変更
                ////updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1,
                //    rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0");

                ///// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                //updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, 0);
                //updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15,
                //        Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value)));
                //updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);
                ///// 税区分情報
                //updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, 0);
                //updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, 0);
                /////// 工事・工種情報
                ////updParam.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, 0);
                ////updParam.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, 0);

                /// 明細区分１・借方レコード
                DataRow r = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_KUBUN"].Value))));
                if (r != null)
                {
                    int triKb = Util.ToInt(r["TORIHIKI_KUBUN"]);
                    if (triKb >= 10 && triKb <= 19)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));
                    }
                    else if (triKb >= 20 && triKb <= 29)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
                    }
                    if (r != null)
                    {
                        updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(r["KANJO_KAMOKU_CD"])));
                        updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, 0);
                        updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, 0);
                        if (taishakuKubun == 1)
                        {
                            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, r["KARIKATA_ZEI_KUBUN"]);
                            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, r["KARI_KAZEI_KUBUN"]);
                            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, r["KARI_TORIHIKI_KUBUN"]);
                            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, r["KARI_ZEI_RITSU"]);
                        }
                        else
                        {
                            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, r["KASHIKATA_ZEI_KUBUN"]);
                            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, r["KASHI_KAZEI_KUBUN"]);
                            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, r["KASHI_TORIHIKI_KUBUN"]);
                            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, r["KASHI_ZEI_RITSU"]);
                        }
                    }
                }
                updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrJIGYO_KUBUN"].Value)));
                updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1,
                    rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0");
                /// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, 0);
                updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15,
                        Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value)));
                updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);
            }
            if (meisaiKubun == 1 && taishakuKubun == 2)
            {
                ///// 貸方勘定科目レコード取得
                //DataRow dr = GetKanjoKamokuDataRow(Util.ToString(rec.Fields["CrKANJO_KAMOKU_CD"].Value));
                ///// 明細区分１・貸方レコード
                //updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4,
                //    Util.ToInt(dr["TAISHAKU_KUBUN"]) == 1 ? this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]
                //                                            : this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]);
                //updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 4, 0);
                //updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, 0);
                //updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, 0);

                //// 事業区分
                ////updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrJIGYO_KUBUN"].Value)));
                //// 消費税変更
                ////updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1,
                //    rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0");

                ///// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                //updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, 0);
                //updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15,
                //        Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value)));
                //updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);
                ///// 税区分情報
                //updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, 0);
                //updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, 0);
                //updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, 0);
                /////// 工事・工種情報
                ////updParam.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, 0);
                ////updParam.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, 0);

                /// 明細区分１・貸方レコード
                DataRow r = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_KUBUN"].Value))));
                if (r != null)
                {
                    int triKb = Util.ToInt(r["TORIHIKI_KUBUN"]);
                    if (triKb >= 10 && triKb <= 19)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));
                    }
                    else if (triKb >= 20 && triKb <= 29)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
                    }
                    if (r != null)
                    {
                        updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(r["KANJO_KAMOKU_CD"])));
                        updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, 0);
                        updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, 0);
                        if (taishakuKubun == 1)
                        {
                            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, r["KARIKATA_ZEI_KUBUN"]);
                            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, r["KARI_KAZEI_KUBUN"]);
                            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, r["KARI_TORIHIKI_KUBUN"]);
                            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, r["KARI_ZEI_RITSU"]);
                        }
                        else
                        {
                            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, r["KASHIKATA_ZEI_KUBUN"]);
                            updParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, r["KASHI_KAZEI_KUBUN"]);
                            updParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, r["KASHI_TORIHIKI_KUBUN"]);
                            updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, r["KASHI_ZEI_RITSU"]);
                        }
                    }
                }
                updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrJIGYO_KUBUN"].Value)));
                updParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1,
                    rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0");
                /// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, 0);
                updParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15,
                        Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value)));
                updParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);
            }

            // 消費税入力方法
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, this.UInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"]);
            // 摘要コード
            updParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, ZaUtil.ToDecimal(Util.ToString(rec.Fields["TEKIYO_CD"].Value)));
            // 摘要
            updParam.SetParam("@TEKIYO", SqlDbType.VarChar, 40, Util.ToString(rec.Fields["TEKIYO"].Value));
            // 決算区分
            updParam.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, rdoKessanKubun0.Checked ? 0 : 1);
            // 仕訳作成区分
            updParam.SetParam("@SHIWAKE_SAKUSEI_KUBUN", SqlDbType.Decimal, 1, null);
            // 備考
            updParam.SetParam("@BIKO", SqlDbType.VarChar, 40, null);
            // 更新日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, upd);

            alParams.Add(updParam);

            return alParams;
        }

        #endregion

        #region privateメソッド(入力値検査)

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 伝票入力時は必須
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 支所の切り替え
            if (Util.ToInt(this.UInfo.ShishoCd) != Util.ToInt(this.txtMizuageShishoCd.Text))
            {
                this.UInfo.ShishoCd = this.txtMizuageShishoCd.Text;
                this.UInfo.ShishoNm = this.lblMizuageShishoNm.Text;
            }

            return true;
        }

        /// <summary>
        /// 担当者コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTantoshaCd()
        {
            // 数字のみの入力を許可・未入力はNG
            if (!ValChk.IsNumber(this.txtTantoshaCd.Text) || ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtTantoshaCd.Focus();
                return false;
            }

            // 存在しないコードを入力されたらエラー
            string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtTantoshaCd.Focus();
                return false;
            }
            this.lblTantoshaNm.Text = name;

            return true;
        }

        /// <summary>
        /// 伝票番号の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidDenpyoBango()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTantoshaCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 証憑番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohyoBango()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShohyoBango.Text, this.txtShohyoBango.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            //txtDenpyoBango.Focus();
            txtTantoshaCd.Focus();

            // 担当者コードのチェック
            if (!IsValidTantoshaCd())
            {
                this.txtTantoshaCd.Focus();
                return false;
            }

            // 証憑番号のチェック
            if (!IsValidShohyoBango())
            {
                this.txtShohyoBango.Focus();
                return false;
            }

            // UTable項目のチェック
            if (!IsValidateUTableAll())
            {
                return false;
            }

            // 出納帳からの呼び出し
            if (this._externalCall == 1)
            {
                // 対象勘定科目の設定有無
                if (!IsValidateUTableFukugo())
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region UTableイベント

        /// <summary>
        /// レコード移動時
        /// </summary>
        /// <param name="record"></param>
        private void mtbList_RecordEnter(UTable.CRecord record)
        {
            // クラス変数にアクティブレコードをセット
            _activeCRecord = record;
        }

        /// <summary>
        /// フォーカス移動時
        /// </summary>
        /// <param name="field"></param>
        private void mtbList_FieldEnter(UTable.CField field)
        {
            // フォーカスをあてる
            this.mtbList.Focus();

            // 編集モード開始
            mtbList.StartEdit();

            // クラス変数にアクティブフィールドをセット
            _activeCField = field;

            // ファンクションキー制御
            SetFunctionKey();
        }

        /// <summary>
        /// カスタムエディタの初期化 
        /// </summary>
        private void mtbList_InitializeEditor(UTable.CField field, IEditor editor)
        {
            // 入力制限・IMEモード
            FsiTextBox ed = (FsiTextBox)editor;
            switch (Util.ToString(field.Key))
            {
                //case "DrKANJO_KAMOKU_CD":
                case "DrHOJO_KAMOKU_CD":
                case "DrBUMON_CD":
                //case "CrKANJO_KAMOKU_CD":
                case "CrHOJO_KAMOKU_CD":
                case "CrBUMON_CD":
                case "TEKIYO_CD":
                case "KOJI_CD":
                case "KOSHU_CD":
                    ed.MaxLength = 4;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
                case "DrKANJO_KAMOKU_CD":
                case "CrKANJO_KAMOKU_CD":
                    ed.MaxLength = 6;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
                case "DrDENPYO_KINGAKU":
                case "DrSHOHIZEI_KINGAKU":
                case "CrDENPYO_KINGAKU":
                case "CrSHOHIZEI_KINGAKU":
                    ed.MaxLength = 10;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
                case "TEKIYO":
                    ed.Width = 40;
                    ed.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        ///  編集モードの開始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mtbList_EditStart(UTable.CField field, IEditor editor)
        {
            // 編集値フォーマット
            FsiTextBox ed = (FsiTextBox)editor;
            switch (Util.ToString(field.Key))
            {
                case "DrKANJO_KAMOKU_CD":
                case "DrHOJO_KAMOKU_CD":
                case "DrBUMON_CD":
                case "CrKANJO_KAMOKU_CD":
                case "CrHOJO_KAMOKU_CD":
                case "CrBUMON_CD":
                case "TEKIYO_CD":
                case "KOJI_CD":
                case "KOSHU_CD":
                    // NONE
                    break;
                case "DrDENPYO_KINGAKU":
                case "DrSHOHIZEI_KINGAKU":
                case "CrDENPYO_KINGAKU":
                case "CrSHOHIZEI_KINGAKU":
                    ed.Text = Util.ToString(Util.ToDecimal(field.Value));
                    break;
                case "TEKIYO":
                    // NONE
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// フォーカス喪失時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mtbList_Leave(object sender, EventArgs e)
        {
            mtbList.FocusField = null;      // フォーカス位置をクリア

            _activeCField = null;
            //_activeCRecord = null;          // アクティブレコード情報をクリア
            //_activeCField = null;           // アクティブフィールド情報をクリア
        }

        #endregion

        #region UTableイベント(入力検査)

        /// <summary>
        ///  入力値検証
        /// </summary>
        /// <param name="field"></param>
        /// <param name="e"></param>
        private void mtbList_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            // フィールド別処理
            switch (Util.ToString(field.Key))
            {
                case "DrKANJO_KAMOKU_CD":
                    DrKANJO_KAMOKU_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "DrHOJO_KAMOKU_CD":
                    DrHOJO_KAMOKU_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "CrKANJO_KAMOKU_CD":
                    CrKANJO_KAMOKU_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "CrHOJO_KAMOKU_CD":
                    CrHOJO_KAMOKU_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "DrBUMON_CD":
                    DrBUMON_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "CrBUMON_CD":
                    CrBUMON_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                case "TEKIYO_CD":
                    TEKIYO_CD_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                    break;
                //case "KOJI_CD":
                //    KOJI_CD_FieldValidating(field, e);
                //    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                //    break;
                //case "KOSHU_CD":
                //    KOSHU_CD_FieldValidating(field, e);
                //    field.Value = ZaUtil.FormatNum(field.Value);           // 数値フォーマット
                //    break;
                case "DrDENPYO_KINGAKU":
                    DrDENPYO_KINGAKU_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatCur(field.Value);           // 通貨フォーマット
                    break;
                case "CrDENPYO_KINGAKU":
                    CrDENPYO_KINGAKU_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatCur(field.Value);           // 通貨フォーマット
                    break;
                case "DrSHOHIZEI_KINGAKU":
                case "CrSHOHIZEI_KINGAKU":
                    SHOHIZEI_KINGAKU_FieldValidating(field, e);
                    field.Value = ZaUtil.FormatCur(field.Value);           // 通貨フォーマット
                    break;
                case "TEKIYO":
                    TEKIYO_FieldValidating(field, e);
                    break;
                default:
                    break;
            }
            // レコード編集状態
            if (!e.Cancel)
            {
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value))
                {
                    // 新規行へのレコード番号付番号判定
                    if (field.Record.Fields["GYO_BANGO"].Value == null)
                    {
                        // 貸借いずれかの科目コードが入力済なら行更新
                        if (!ValChk.IsEmpty(field.Record.Fields["DrKANJO_KAMOKU_CD"].Value)
                            || !ValChk.IsEmpty(field.Record.Fields["CrKANJO_KAMOKU_CD"].Value))
                        {
                            field.Record.Fields["GYO_BANGO"].Value = mtbList.Content.Records.Count;
                            mtbList.Content.AddRecord();            // 新規行を追加
                        }
                    }
                    // 新規行へのレコード番号付番号判定
                    if (field.Record.Fields["GYO_BANGO"].Value == null)
                    {
                        // 貸借いずれかの科目コードが入力済なら行更新
                        if (!ValChk.IsEmpty(field.Record.Fields["DrKANJO_KAMOKU_CD"].Value)
                            || !ValChk.IsEmpty(field.Record.Fields["CrKANJO_KAMOKU_CD"].Value))
                        {
                            field.Record.Fields["GYO_BANGO"].Value = mtbList.Content.Records.Count;
                            mtbList.Content.AddRecord();            // 新規行を追加
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 入力値検証(摘要コード)
        /// </summary>
        private void TEKIYO_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidTekiyoCd(field))
            {
                e.Cancel = true;
            }
            else
            {
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
                    && !ValChk.IsEmpty(field.Value))
                {
                    string name = this.Dba.GetName(this.UInfo, "TB_ZM_TEKIYO", this.txtMizuageShishoCd.Text, Util.ToString(field.Value));
                    field.Record.Fields["TEKIYO"].Value = name;

                    #region 仕訳事例検索（仕訳事例の検索はプロジェクトへ取り込み）
                    // アセンブリのロード
                    //Assembly asm = Assembly.LoadFrom("ZAMC9531.exe");
                    //// フォーム作成
                    //Type t = asm.GetType("jp.co.fsi.zam.zamc9531.ZAMC9531");
                    //if (t != null)
                    //{
                    //    Object obj = Activator.CreateInstance(t);
                    //    if (obj != null)
                    //    {
                    //        // タブの一部として埋め込む
                    //        BasePgForm frm = (BasePgForm)obj;
                    //        string[] args = new string[2];
                    //        args[0] = Util.ToString(field.Value);
                    //        frm.InData = args;
                    //        frm.Par1 = "2";     // 自動検索モード
                    //        frm.ShowDialog(this);

                    //        if (frm.DialogResult == DialogResult.OK)
                    //        {
                    //            string[] result = (string[])frm.OutData;

                    //            // 仕訳事例データのロード
                    //            ShiwakeJireiLoad(Util.ToString(field.Value), result[0]);
                    //        }
                    //    }
                    //}
                    //using (ZMDE1036 frm1036 = new ZMDE1036())
                    //{
                    //    string[] args = new string[2];
                    //    args[0] = Util.ToString(field.Value);
                    //    // 出納帳からの呼出し時は基本の科目コードを設定
                    //    if (this._externalCall == 1)
                    //    {
                    //        args[1] = this._bInfo.KanjoKamokuCd;
                    //    }
                    //    frm1036.InData = args;
                    //    frm1036.Par1 = "2";     // 自動検索モード
                    //    frm1036.ShowDialog(this);
                    //    if (frm1036.DialogResult == DialogResult.OK)
                    //    {
                    //        string[] result = (string[])frm1036.OutData;
                    //        // 仕訳事例データのロード
                    //        ShiwakeJireiLoad(Util.ToString(field.Value), result[0]);
                    //    }
                    //}
                    #endregion
                }
            }
        }

        /// <summary>
        /// 入力値検証(摘要)
        /// </summary>
        private void TEKIYO_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidTekiyo(field))
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// 入力値検証(借方科目コード)
        /// </summary>
        private void DrKANJO_KAMOKU_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidKANJO_KAMOKU_CD(field))
            {
                e.Cancel = true;
            }
            else
            {
                // 値変更の場合
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value))
                {
                    if (this._cpFlg == 0)
                    {
                        // 勘定科目情報をセット
                        SetKanjoKamokuInfo(field);
                    }
                    else
                    {
                        if (this._prvKanjoKamoku != Util.ToDecimal(field.Value))
                        {
                            // 勘定科目情報をセット
                            SetKanjoKamokuInfo(field);
                        }
                        else
                        {
                            this._cpFlg = 0;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 入力値検証(借方補助コード)
        /// </summary>
        private void DrHOJO_KAMOKU_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidHOJO_KAMOKU_CD(Util.ToString(field.Record.Fields["DrKANJO_KAMOKU_CD"].Value), field))
            {
                e.Cancel = true;
            }
            else
            {
                // 値変更の場合に補助科目名をセット
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
                    && !ValChk.IsEmpty(field.Value))
                {
                    // 補助科目名をセット
                    field.Record.Fields["DrHOJO_KAMOKU_NM"].Value =
                        this.GetHojoKmkNm(
                            this.txtMizuageShishoCd.Text,
                            Util.ToString(field.Record.Fields["DrKANJO_KAMOKU_CD"].Value),
                            Util.ToString(field.Value));
                }
            }
        }

        /// <summary>
        /// 入力値検証(借方部門コード)
        /// </summary>
        private void DrBUMON_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidBUMON_CD(field))
            {
                e.Cancel = true;
            }
            else
            {
                // 値変更の場合に従属情報をセット
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
                    && !ValChk.IsEmpty(field.Value))
                {
                    SetBumonInfo(field);
                }
            }
        }

        /// <summary>
        /// 入力値検証(借方伝票金額)
        /// </summary>
        private void DrDENPYO_KINGAKU_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidDENPYO_KINGAKU(field))
            {
                e.Cancel = true;
            }

            // 表示書式
            if (Util.ToDecimal(Util.ToString(field.Value)) != 0)
            {
                field.Value = Util.FormatNum(Util.ToDecimal(Util.ToString(field.Value)));
            }
            else
            {
                field.Value = "";
            }
            // 伝票金額、消費税がゼロ以下は文字色を赤にする
            if (Util.ToDecimal(Util.ToString(field.Value)) >= 0)
            {
                field.Setting.ForeColor = Color.Black;
            }
            else
            {
                field.Setting.ForeColor = Color.Red;
            }

            // 金額が変わった場合のみ消費税の再計算とする（自動仕訳で送られた物の場合、計算とは一致しない為）
            if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value))
            {
                // 消費税額の算出
                decimal zei = 0;
                if (!ValChk.IsEmpty(field.Value))
                {
                    //zei = ZaUtil.CalcConsumptionTax(Util.ToInt(field.Record.Fields["DrZEI_KUBUN"].Value),
                    //        Util.ToDecimal(field.Value), this.UInfo, this.Dba);
                    // 保持された税率で計算
                    zei = ZaUtil.CalcConsumptionTax(Util.ToDecimal(Util.ToString(field.Record.Fields["DrZEI_RITSU"].Value).Replace("%", "")),
                            Util.ToDecimal(Util.ToString(field.Value)), this.UInfo, this.Dba);
                }
                //if (zei > 0)
                if (zei != 0)
                {
                    field.Record.Fields["DrSHOHIZEI_KINGAKU"].Value = Util.FormatNum(zei);
                }
                else
                {
                    field.Record.Fields["DrSHOHIZEI_KINGAKU"].Value = "";
                }
                // 伝票金額、消費税がゼロ以下は文字色を赤にする
                if (zei >= 0)
                {
                    field.Record.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                }
                else
                {
                    field.Record.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                }
                //合計計算
                CalcTotal();
            }
        }

        /// <summary>
        /// 入力値検証(貸方科目コード)
        /// </summary>
        private void CrKANJO_KAMOKU_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidKANJO_KAMOKU_CD(field))
            {
                e.Cancel = true;
            }
            else
            {
                // 値変更の場合
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value))
                {
                    // 勘定科目情報をセット
                    SetKanjoKamokuInfo(field);
                }
            }
        }

        /// <summary>
        /// 入力値検証(貸方補助コード)
        /// </summary>
        private void CrHOJO_KAMOKU_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidHOJO_KAMOKU_CD(Util.ToString(field.Record.Fields["CrKANJO_KAMOKU_CD"].Value), field))
            {
                e.Cancel = true;
            }
            else
            {
                // 値変更の場合に補助科目名をセット
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
                    && !ValChk.IsEmpty(field.Value))
                {
                    // 補助科目名をセット
                    field.Record.Fields["CrHOJO_KAMOKU_NM"].Value =
                        this.GetHojoKmkNm(
                            this.txtMizuageShishoCd.Text,
                            Util.ToString(field.Record.Fields["CrKANJO_KAMOKU_CD"].Value),
                            Util.ToString(field.Value));
                }
            }
        }

        /// <summary>
        /// 入力値検証(貸方部門コード)
        /// </summary>
        private void CrBUMON_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidBUMON_CD(field))
            {
                e.Cancel = true;
            }
            else
            {
                // 値変更の場合に従属情報をセット
                if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
                    && !ValChk.IsEmpty(field.Value))
                {
                    SetBumonInfo(field);
                }
            }
        }

        /// <summary>
        /// 入力値検証(貸方伝票金額)
        /// </summary>
        private void CrDENPYO_KINGAKU_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidDENPYO_KINGAKU(field))
            {
                e.Cancel = true;
            }

            // 表示書式
            if (Util.ToDecimal(Util.ToString(field.Value)) != 0)
            {
                field.Value = Util.FormatNum(Util.ToDecimal(Util.ToString(field.Value)));
            }
            else
            {
                field.Value = "";
            }
            // 伝票金額、消費税がゼロ以下は文字色を赤にする
            if (Util.ToDecimal(Util.ToString(field.Value)) >= 0)
            {
                field.Setting.ForeColor = Color.Black;
            }
            else
            {
                field.Setting.ForeColor = Color.Red;
            }

            // 金額が変わった場合のみ消費税の再計算とする（自動仕訳で送られた物の場合、計算とは一致しない為）
            if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value))
            {
                // 消費税額の算出
                decimal zei = 0;
                if (!ValChk.IsEmpty(field.Value))
                {
                    //zei = ZaUtil.CalcConsumptionTax(Util.ToInt(field.Record.Fields["CrZEI_KUBUN"].Value),
                    //        Util.ToDecimal(field.Value), this.UInfo, this.Dba);
                    // 保持された税率で計算
                    zei = ZaUtil.CalcConsumptionTax(Util.ToDecimal(Util.ToString(field.Record.Fields["CrZEI_RITSU"].Value).Replace("%", "")),
                            Util.ToDecimal(Util.ToString(field.Value)), this.UInfo, this.Dba);
                }
                if (zei != 0)
                {
                    field.Record.Fields["CrSHOHIZEI_KINGAKU"].Value = Util.FormatNum(zei);
                }
                else
                {
                    field.Record.Fields["CrSHOHIZEI_KINGAKU"].Value = "";
                }
                // 伝票金額、消費税がゼロ以下は文字色を赤にする
                if (zei >= 0)
                {
                    field.Record.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                }
                else
                {
                    field.Record.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                }
                //合計計算
                CalcTotal();
            }
        }

        /// <summary>
        /// 入力値検証(消費税)
        /// </summary>
        private void SHOHIZEI_KINGAKU_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            if (!IsValidSHOHIZEI_KINGAKU(field))
            {
                e.Cancel = true;
            }

            // 表示書式
            if (Util.ToDecimal(Util.ToString(field.Value)) != 0)
            {
                field.Value = Util.FormatNum(field.Value);
            }
            else
            {
                field.Value = "";
            }
            // 伝票金額、消費税がゼロ以下は文字色を赤にする
            if (Util.ToDecimal(Util.ToString(field.Value)) >= 0)
            {
                field.Setting.ForeColor = Color.Black;
            }
            else
            {
                field.Setting.ForeColor = Color.Red;
            }
            //合計計算
            CalcTotal();
        }

        /// <summary>
        /// 入力値検証(工事コード)
        /// </summary>
        private void KOJI_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            //if (!IsValidKojiCd(field))
            //{
            //    e.Cancel = true;
            //}
            //else
            //{
            //    // 値変更の場合に従属情報をセット
            //    if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
            //        && !ValChk.IsEmpty(field.Value))
            //    {
            //        field.Record.Fields["KOJI_NM"].Value =
            //        this.Dba.GetName(this.UInfo, "TB_ZM_KOJI", this.txtMizuageShishoCd.Text, Util.ToString(field.Value));
            //    }
            //}
        }

        /// <summary>
        /// 入力値検証(工種コード)
        /// </summary>
        private void KOSHU_CD_FieldValidating(UTable.CField field, CancelEventArgs e)
        {
            //if (!IsValidKoshuCd(field))
            //{
            //    e.Cancel = true;
            //}
            //else
            //{
            //    // 値変更の場合に従属情報をセット
            //    if (Util.ToString(field.CommittedValue()) != Util.ToString(field.Value)
            //        && !ValChk.IsEmpty(field.Value))
            //    {
            //        field.Record.Fields["KOSHU_NM"].Value =
            //        this.Dba.GetName(this.UInfo, "TB_ZM_KOSHU", this.txtMizuageShishoCd.Text, Util.ToString(field.Value));
            //    }
            //}
        }

        #endregion

        #region UTable privateメソッド

        /// <summary>
        /// 勘定科目情報をセット
        /// </summary>
        /// <param name="field">UTableフィールド</param>
        private void SetKanjoKamokuInfo(UTable.CField field)
        {
            // 税率取得用
            DateTime DenpyoDate =
                Util.ConvAdDate(this.lblGengoDenpyoDate.Text, this.txtGengoYearDenpyoDate.Text,
                                this.txtMonthDenpyoDate.Text, this.txtDayDenpyoDate.Text, Dba);

            if (Util.ToString(field.Key) == "DrKANJO_KAMOKU_CD")
            {
                #region 借方勘定科目情報をセット
                if (ValChk.IsEmpty(field.Value))
                {
                    // 従属フィールドをクリア
                    field.Record.Fields["DrKANJO_KAMOKU_NM"].Value = "";
                    field.Record.Fields["DrHOJO_KAMOKU_CD"].Value = "";
                    field.Record.Fields["DrHOJO_KAMOKU_NM"].Value = "";
                    field.Record.Fields["DrBUMON_CD"].Value = "";
                    field.Record.Fields["DrJIGYO_KUBUN"].Value = "";
                    field.Record.Fields["DrZEI_KUBUN"].Value = "";
                    field.Record.Fields["DrZEI_RITSU"].Value = "";
                    field.Record.Fields["DrDENPYO_KINGAKU"].Value = "";
                    field.Record.Fields["DrSHOHIZEI_KINGAKU"].Value = "";
                    //field.Record.Fields["DrKOJI_UMU"].Value = "";
                    field.Record.Fields["DrKAZEI_KUBUN"].Value = "";
                    // 編集可否をクリア
                    CFieldEnabled(field.Record.Fields["DrHOJO_KAMOKU_CD"], false);
                    CFieldEnabled(field.Record.Fields["DrBUMON_CD"], false);
                    CFieldEnabled(field.Record.Fields["DrDENPYO_KINGAKU"], false);
                    CFieldEnabled(field.Record.Fields["DrSHOHIZEI_KINGAKU"], false);
                    //// 工事情報(貸方も工事なしの場合クリア)
                    //if (Util.ToString(field.Record.Fields["CrKOJI_UMU"].Value) != "1")
                    //{
                    //    field.Record.Fields["KOJI_CD"].Value = "";
                    //    field.Record.Fields["KOJI_NM"].Value = "";
                    //    field.Record.Fields["KOSHU_CD"].Value = "";
                    //    field.Record.Fields["KOSHU_NM"].Value = "";
                    //    CFieldEnabled(field.Record.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(field.Record.Fields["KOSHU_CD"], false);
                    //}
                }
                else
                {
                    // 科目情報を取得
                    DataRow dr = GetKanjoKamokuDataRow(Util.ToString(field.Value));
                    // 従属フィールド情報セット
                    field.Record.Fields["DrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                    field.Record.Fields["DrHOJO_KAMOKU_CD"].Value = "";
                    field.Record.Fields["DrHOJO_KAMOKU_NM"].Value = "";
                    field.Record.Fields["DrBUMON_CD"].Value = "";
                    field.Record.Fields["DrJIGYO_KUBUN"].Value =
                        ZaUtil.FormatNum(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);
                    field.Record.Fields["DrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["KARIKATA_ZEI_KUBUN"]);

                    // 税率
                    //field.Record.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["KARI_ZEI_RITSU"]);
                    field.Record.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(TaxUtil.GetTaxRate(DenpyoDate, Util.ToInt(Util.ToString(dr["KARIKATA_ZEI_KUBUN"])),this.Dba));

                    field.Record.Fields["DrDENPYO_KINGAKU"].Value = "";
                    field.Record.Fields["DrSHOHIZEI_KINGAKU"].Value = "";
                    //field.Record.Fields["DrKOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                    field.Record.Fields["DrKAZEI_KUBUN"].Value = Util.ToString(dr["KARI_KAZEI_KUBUN"]);
                    // 編集可否
                    CFieldEnabled(field.Record.Fields["DrHOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(dr["HOJO_SHIYO_KUBUN"])) > 0);
                    CFieldEnabled(field.Record.Fields["DrBUMON_CD"], Util.ToInt(Util.ToString(dr["BUMON_UMU"])) == 1);
                    CFieldEnabled(field.Record.Fields["DrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(dr["KANJO_KAMOKU_CD"])) > 0);
                    CFieldEnabled(field.Record.Fields["DrSHOHIZEI_KINGAKU"], false);
                    //// 工事・工種編集可否
                    //if (Util.ToString(field.Record.Fields["DrKOJI_UMU"].Value) == "1"
                    //    || Util.ToString(field.Record.Fields["CrKOJI_UMU"].Value) == "1")
                    //{
                    //    CFieldEnabled(field.Record.Fields["KOJI_CD"], true);
                    //    CFieldEnabled(field.Record.Fields["KOSHU_CD"], true);
                    //}
                    //else
                    //{
                    //    CFieldEnabled(field.Record.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(field.Record.Fields["KOSHU_CD"], false);
                    //}

                    //// 税区分を基準日チェック
                    //DateTime denpyoDate = Util.ConvAdDate(
                    //    this.lblGengoDenpyoDate.Text,
                    //    this.txtGengoYearDenpyoDate.Text,
                    //    this.txtMonthDenpyoDate.Text,
                    //    this.txtDayDenpyoDate.Text,
                    //    this.Dba);
                    string zeiKubun = Util.ToString(ZaUtil.ToDecimal(Util.ToString(field.Record.Fields["DrZEI_KUBUN"].Value)));
                    decimal taxRate = 0;
                    DialogResult ans = ConfPastZeiKbn(DenpyoDate, zeiKubun, ref taxRate);
                    if (ans != DialogResult.OK)
                    {
                        // 税区分を選択(非選択も可)->メソッド側で検索及び設定へ
                        //// 税区分を選択(選択取消可能)
                        //string ret = SelectZeiKubun(false);
                        //if (!ValChk.IsEmpty(ret))
                        //{
                        //    field.Record.Fields["DrZEI_KUBUN"].Value = ret;
                        //    SetZeiKubunInfo(field);
                        //}
                        SelectZeiKubun(false, field);
                    }
                }

                //合計計算
                CalcTotal();

                #endregion
            }
            if (Util.ToString(field.Key) == "CrKANJO_KAMOKU_CD")
            {
                #region 貸方勘定科目情報をセット
                if (ValChk.IsEmpty(field.Value))
                {
                    // 従属フィールドをクリア
                    field.Record.Fields["CrKANJO_KAMOKU_NM"].Value = "";
                    field.Record.Fields["CrHOJO_KAMOKU_CD"].Value = "";
                    field.Record.Fields["CrHOJO_KAMOKU_NM"].Value = "";
                    field.Record.Fields["CrBUMON_CD"].Value = "";
                    field.Record.Fields["CrJIGYO_KUBUN"].Value = "";
                    field.Record.Fields["CrZEI_KUBUN"].Value = "";
                    field.Record.Fields["CrZEI_RITSU"].Value = "";
                    field.Record.Fields["CrDENPYO_KINGAKU"].Value = "";
                    field.Record.Fields["CrSHOHIZEI_KINGAKU"].Value = "";
                    //field.Record.Fields["CrKOJI_UMU"].Value = "";
                    field.Record.Fields["CrKAZEI_KUBUN"].Value = "";
                    // 編集可否をクリア
                    CFieldEnabled(field.Record.Fields["CrHOJO_KAMOKU_CD"], false);
                    CFieldEnabled(field.Record.Fields["CrBUMON_CD"], false);
                    CFieldEnabled(field.Record.Fields["CrDENPYO_KINGAKU"], false);
                    CFieldEnabled(field.Record.Fields["CrSHOHIZEI_KINGAKU"], false);
                    //// 工事情報(借方も工事なしの場合クリア)
                    //if (Util.ToString(field.Record.Fields["DrKOJI_UMU"].Value) != "1")
                    //{
                    //    field.Record.Fields["KOJI_CD"].Value = "";
                    //    field.Record.Fields["KOJI_NM"].Value = "";
                    //    field.Record.Fields["KOSHU_CD"].Value = "";
                    //    field.Record.Fields["KOSHU_NM"].Value = "";
                    //    CFieldEnabled(field.Record.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(field.Record.Fields["KOSHU_CD"], false);
                    //}
                }
                else
                {
                    // 科目情報を取得
                    DataRow dr = GetKanjoKamokuDataRow(Util.ToString(field.Value));
                    // 従属フィールド情報セット
                    field.Record.Fields["CrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                    field.Record.Fields["CrHOJO_KAMOKU_CD"].Value = "";
                    field.Record.Fields["CrHOJO_KAMOKU_NM"].Value = "";
                    field.Record.Fields["CrBUMON_CD"].Value = "";
                    field.Record.Fields["CrJIGYO_KUBUN"].Value =
                        ZaUtil.FormatNum(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);
                    field.Record.Fields["CrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["KASHIKATA_ZEI_KUBUN"]);

                    // 税率
                    //field.Record.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["KASHI_ZEI_RITSU"]);
                    field.Record.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(TaxUtil.GetTaxRate(DenpyoDate, Util.ToInt(Util.ToString(dr["KASHIKATA_ZEI_KUBUN"])),this.Dba));

                    field.Record.Fields["CrDENPYO_KINGAKU"].Value = field.Record.Fields["DrDENPYO_KINGAKU"].Value;

                    // 金額を転記している為、消費税の算出も実施
                    //field.Record.Fields["CrSHOHIZEI_KINGAKU"].Value = "";
                    decimal zei = ZaUtil.CalcConsumptionTax(Util.ToDecimal(Util.ToString(field.Record.Fields["CrZEI_RITSU"].Value).Replace("%", "")),
                            Util.ToDecimal(Util.ToString(field.Record.Fields["CrDENPYO_KINGAKU"].Value)), this.UInfo, this.Dba);
                    if (zei != 0)
                    {
                        field.Record.Fields["CrSHOHIZEI_KINGAKU"].Value = Util.FormatNum(zei);
                    }
                    else
                    {
                        field.Record.Fields["CrSHOHIZEI_KINGAKU"].Value = "";
                    }
                    // 伝票金額、消費税がゼロ以下は文字色を赤にする
                    if (Util.ToDecimal(Util.ToString(field.Record.Fields["CrDENPYO_KINGAKU"].Value)) >= 0)
                    {
                        field.Record.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                        field.Record.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                    }
                    else
                    {
                        field.Record.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                        field.Record.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                    }

                    //合計計算
                    CalcTotal();

                    //field.Record.Fields["CrKOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                    field.Record.Fields["CrKAZEI_KUBUN"].Value = Util.ToString(dr["KASHI_KAZEI_KUBUN"]);
                    // 編集可否
                    CFieldEnabled(field.Record.Fields["CrHOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(dr["HOJO_SHIYO_KUBUN"])) > 0);
                    CFieldEnabled(field.Record.Fields["CrBUMON_CD"], Util.ToInt(Util.ToString(dr["BUMON_UMU"])) == 1);
                    CFieldEnabled(field.Record.Fields["CrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(dr["KANJO_KAMOKU_CD"])) > 0);
                    CFieldEnabled(field.Record.Fields["CrSHOHIZEI_KINGAKU"], false);
                    //// 工事・工種編集可否
                    //if (Util.ToString(field.Record.Fields["DrKOJI_UMU"].Value) == "1"
                    //    || Util.ToString(field.Record.Fields["CrKOJI_UMU"].Value) == "1")
                    //{
                    //    CFieldEnabled(field.Record.Fields["KOJI_CD"], true);
                    //    CFieldEnabled(field.Record.Fields["KOSHU_CD"], true);
                    //}
                    //else
                    //{
                    //    CFieldEnabled(field.Record.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(field.Record.Fields["KOSHU_CD"], false);
                    //}

                    //// 税区分を基準日チェック
                    //DateTime denpyoDate = Util.ConvAdDate(
                    //    this.lblGengoDenpyoDate.Text,
                    //    this.txtGengoYearDenpyoDate.Text,
                    //    this.txtMonthDenpyoDate.Text,
                    //    this.txtDayDenpyoDate.Text,
                    //    this.Dba);
                    string zeiKubun = Util.ToString(ZaUtil.ToDecimal(Util.ToString(field.Record.Fields["CrZEI_KUBUN"].Value)));
                    decimal taxRate = 0;
                    DialogResult ans = ConfPastZeiKbn(DenpyoDate, zeiKubun, ref taxRate);
                    if (ans != DialogResult.OK)
                    {
                        // 税区分を選択(非選択も可)->メソッド側で検索及び設定へ
                        //// 税区分を選択(選択取消可能)
                        //string ret = SelectZeiKubun(false);
                        //if (!ValChk.IsEmpty(ret))
                        //{
                        //    field.Record.Fields["CrZEI_KUBUN"].Value = ret;
                        //    SetZeiKubunInfo(field);
                        //}
                        SelectZeiKubun(false, field);
                    }
                }
                #endregion
            }
        }

        /// <summary>
        /// 部門情報をセット
        /// </summary>
        /// <param name="field">UTableフィールド</param>
        private void SetBumonInfo(UTable.CField field)
        {
            if (Util.ToString(field.Key) == "DrBUMON_CD")
            {
                // 部門情報を取得
                DataRow dr = GetBumonDataRow(Util.ToString(field.Value));
                // 従属フィールド情報セット
                // 事業区分が存在は設定、無い場合は基本情報
                //field.Record.Fields["DrJIGYO_KUBUN"].Value = Util.ToString(dr["JIGYO_KUBUN"]);
                if (Util.ToInt(Util.ToString(dr["JIGYO_KUBUN"])) != 0)
                    field.Record.Fields["DrJIGYO_KUBUN"].Value = Util.ToString(dr["JIGYO_KUBUN"]);
                else
                    field.Record.Fields["DrJIGYO_KUBUN"].Value = ZaUtil.FormatNum(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);
            }
            if (Util.ToString(field.Key) == "CrBUMON_CD")
            {
                // 部門情報を取得
                DataRow dr = GetBumonDataRow(Util.ToString(field.Value));
                // 従属フィールド情報セット
                // 事業区分が存在は設定、無い場合は基本情報
                //field.Record.Fields["CrJIGYO_KUBUN"].Value = Util.ToString(dr["JIGYO_KUBUN"]);
                if (Util.ToInt(Util.ToString(dr["JIGYO_KUBUN"])) != 0)
                    field.Record.Fields["CrJIGYO_KUBUN"].Value = Util.ToString(dr["JIGYO_KUBUN"]);
                else
                    field.Record.Fields["CrJIGYO_KUBUN"].Value = ZaUtil.FormatNum(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);
            }
        }

        /// <summary>
        /// CField使用可否設定
        /// </summary>
        private void CFieldEnabled(UTable.CField field, bool flag)
        {
            if (field != null)
            {
                field.Setting.TabStop = flag ? UTable.ETabStop.STOP : UTable.ETabStop.NOTSTOP;
                field.Setting.Editable = flag ? UTable.EAllow.ALLOW : UTable.EAllow.DISABLE;
            }
        }

        /// <summary>
        /// CField使用可否取得
        /// </summary>
        private bool CFieldEnabled(UTable.CField field)
        {
            bool ret = false;

            if (field != null)
            {
                ret = (field.Setting.Editable == UTable.EAllow.ALLOW);
            }
            return ret;
        }

        /// <summary>
        /// 仕訳事例データの読込
        /// </summary>
        /// 
        private void ShiwakeJireiLoad(string tekiyoCd, string shiwakeCd)
        {

            #region TB仕訳事例データ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, tekiyoCd);
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 4, shiwakeCd);
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append(", TEKIYO_CD");
            cols.Append(", TEKIYO_NM");
            cols.Append(", SHIWAKE_CD");
            cols.Append(", GYO_BANGO");
            cols.Append(", TAISHAKU_KUBUN");
            cols.Append(", MEISAI_KUBUN");
            cols.Append(", KANJO_KAMOKU_CD");
            cols.Append(", KANJO_KAMOKU_NM");
            cols.Append(", BUMON_UMU");
            cols.Append(", HOJO_KAMOKU_UMU");
            //cols.Append(", KOJI_UMU");
            cols.Append(", HOJO_KAMOKU_CD");
            cols.Append(", HOJO_KAMOKU_NM");
            cols.Append(", BUMON_CD");
            cols.Append(", BUMON_NM");
            //cols.Append(", KOJI_CD");
            //cols.Append(", KOJI_NM");
            //cols.Append(", KOSHU_CD");
            //cols.Append(", KOSHU_NM");
            cols.Append(", TEKIYO");
            cols.Append(", ZEI_KUBUN");
            cols.Append(", ZEI_KUBUN_NM");
            cols.Append(", KAZEI_KUBUN");
            cols.Append(", TORIHIKI_KUBUN");
            cols.Append(", ZEI_RITSU");
            cols.Append(", JIGYO_KUBUN");
            cols.Append(", SHOHIZEI_NYURYOKU_HOHO");
            cols.Append(", SHOHIZEI_HENKO");
            cols.Append(", KESSAN_KUBUN");
            cols.Append(", ZEIKOMI_KINGAKU");
            cols.Append(", ZEINUKI_KINGAKU");
            cols.Append(", SHOHIZEI_KINGAKU");
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND TEKIYO_CD = @TEKIYO_CD");
            where.Append(" AND SHIWAKE_CD = @SHIWAKE_CD");
            where.Append(" AND MEISAI_KUBUN = 0");
            StringBuilder order = new StringBuilder();
            order.Append("GYO_BANGO ASC");
            order.Append(", TAISHAKU_KUBUN ASC");
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                            , "VI_ZM_SHIWAKE_JIREI"
                            , Util.ToString(where), dpc);
            #endregion

            #region 明細データ追加
            /// **************************************************
            /// UTableレンダリングブロック開始
            /// **************************************************
            using (mtbList.RenderBlock())
            {
                // カレントレコード位置を取得して削除
                int recIndex = 0;
                if (_activeCRecord != null)
                {
                    // 追加用ブランク行の場合はブランク行を追加
                    if (_activeCRecord.Fields["GYO_BANGO"].Value == null)
                    {
                        this.mtbList.Content.AddRecord();
                    }
                    // カレント行を削除
                    recIndex = this.mtbList.Content.Records.IndexOf(_activeCRecord);
                    this.mtbList.Content.RemoveRecord(_activeCRecord);
                }

                // 税率取得用
                DateTime DenpyoDate =
                    Util.ConvAdDate(this.lblGengoDenpyoDate.Text, this.txtGengoYearDenpyoDate.Text,
                                    this.txtMonthDenpyoDate.Text, this.txtDayDenpyoDate.Text, Dba);

                // 仕訳事例データを追加
                int gyo = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    // 仕訳明細をUTableレコードへセット
                    UTable.CRecord rec;

                    // 行番号ブレイクでレコード追加 
                    if (Util.ToDecimal(dr["GYO_BANGO"]) > gyo)
                    {
                        gyo = Util.ToInt(dr["GYO_BANGO"]);      // ブレイク判定値

                        rec = this.mtbList.Content.InsertRecord(recIndex + gyo - 1);
                        // 貸借レコード共通情報
                        rec.Fields["GYO_BANGO"].Value = Util.ToString(recIndex + gyo);
                        rec.Fields["TEKIYO_CD"].Value = ZaUtil.FormatNum(dr["TEKIYO_CD"]);
                        rec.Fields["TEKIYO"].Value = Util.ToString(dr["TEKIYO"]);
                        //rec.Fields["KOJI_CD"].Value = "";                           // 事例データに保存されない
                        //rec.Fields["KOJI_NM"].Value = "";
                        //rec.Fields["KOSHU_CD"].Value = "";
                        //rec.Fields["KOSHU_NM"].Value = "";
                    }

                    // 仕訳情報をセット
                    rec = this.mtbList.Content.LastAddedRecord;
                    if (Util.ToString(dr["TAISHAKU_KUBUN"]).Equals("1"))            // 借方レコード
                    {
                        rec.Fields["DrKANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KANJO_KAMOKU_CD"]);
                        rec.Fields["DrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                        rec.Fields["DrHOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["HOJO_KAMOKU_CD"]);
                        rec.Fields["DrHOJO_KAMOKU_NM"].Value = Util.ToString(dr["HOJO_KAMOKU_NM"]);
                        rec.Fields["DrBUMON_CD"].Value = ZaUtil.FormatNum(dr["BUMON_CD"]);
                        if (Util.ToInt(Util.ToString(dr["JIGYO_KUBUN"])) > 0)
                        {
                            rec.Fields["DrJIGYO_KUBUN"].Value = ZaUtil.FormatNum(dr["JIGYO_KUBUN"]);
                        }
                        else
                        {
                            rec.Fields["DrJIGYO_KUBUN"].Value =
                                ZaUtil.FormatNum(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);

                        }
                        rec.Fields["DrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["ZEI_KUBUN"]);

                        // 税率が事例内と一致する場合は金額を設定し相違する場合は金額の設定は行わず、事例に登録し直し
                        //rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                        decimal taxRate = TaxUtil.GetTaxRate(DenpyoDate, Util.ToInt(Util.ToString(dr["ZEI_KUBUN"])), this.Dba);
                        if (taxRate == Util.ToDecimal(Util.ToString(dr["ZEI_RITSU"])))
                        {
                            rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                            rec.Fields["DrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                                ZaUtil.GetDenpyoKingaku(
                                    Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                    Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])),
                                    this.UInfo));
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["SHOHIZEI_KINGAKU"]);
                            // 伝票金額、消費税がゼロ以下は文字色を赤にする
                            if (Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)) >= 0)
                            {
                                rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                                rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                            }
                            else
                            {
                                rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                                rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                            }
                        }
                        else
                        {
                            rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(taxRate);
                            rec.Fields["DrDENPYO_KINGAKU"].Value = "";
                            rec.Fields["DrSHOHIZEI_KINGAKU"].Value = "";
                        }
                        //rec.Fields["DrKOJI_UMU"].Value = Util.ToString(dr["KOJI_UMU"]);
                        rec.Fields["DrKAZEI_KUBUN"].Value = Util.ToString(dr["KAZEI_KUBUN"]);
                        // 編集可否
                        CFieldEnabled(rec.Fields["DrHOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(dr["HOJO_KAMOKU_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["DrBUMON_CD"], Util.ToInt(Util.ToString(dr["BUMON_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["DrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(dr["KANJO_KAMOKU_CD"])) > 0);
                        CFieldEnabled(rec.Fields["DrSHOHIZEI_KINGAKU"], Util.ToInt(Util.ToString(dr["SHOHIZEI_HENKO"])) == 1);
                    }
                    else if (Util.ToString(dr["TAISHAKU_KUBUN"]).Equals("2"))       // 貸方レコード
                    {
                        rec.Fields["CrKANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KANJO_KAMOKU_CD"]);
                        rec.Fields["CrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                        rec.Fields["CrHOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["HOJO_KAMOKU_CD"]);
                        rec.Fields["CrHOJO_KAMOKU_NM"].Value = Util.ToString(dr["HOJO_KAMOKU_NM"]);
                        rec.Fields["CrBUMON_CD"].Value = ZaUtil.FormatNum(dr["BUMON_CD"]);
                        if (Util.ToInt(Util.ToString(dr["JIGYO_KUBUN"])) > 0)
                        {
                            rec.Fields["CrJIGYO_KUBUN"].Value = Util.ToString(dr["JIGYO_KUBUN"]);
                        }
                        else
                        {
                            rec.Fields["CrJIGYO_KUBUN"].Value =
                                ZaUtil.FormatNum(this.UInfo.KaikeiSettings["JIGYO_KUBUN"]);

                        }
                        rec.Fields["CrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["ZEI_KUBUN"]);

                        // 税率が事例内と一致する場合は金額を設定し相違する場合は金額の設定は行わず、事例に登録し直し
                        //rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                        decimal taxRate = TaxUtil.GetTaxRate(DenpyoDate, Util.ToInt(Util.ToString(dr["ZEI_KUBUN"])), this.Dba);
                        if (taxRate == Util.ToDecimal(Util.ToString(dr["ZEI_RITSU"])))
                        {
                            rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["ZEI_RITSU"]);
                            rec.Fields["CrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                                ZaUtil.GetDenpyoKingaku(
                                    Util.ToDecimal(Util.ToString(dr["ZEIKOMI_KINGAKU"])),
                                    Util.ToDecimal(Util.ToString(dr["ZEINUKI_KINGAKU"])),
                                    this.UInfo));
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["SHOHIZEI_KINGAKU"]);
                            // 伝票金額、消費税がゼロ以下は文字色を赤にする
                            if (Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)) >= 0)
                            {
                                rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                                rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                            }
                            else
                            {
                                rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                                rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                            }
                        }
                        else
                        {
                            rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(taxRate);
                            rec.Fields["CrDENPYO_KINGAKU"].Value = "";
                            rec.Fields["CrSHOHIZEI_KINGAKU"].Value = "";
                        }
                        //rec.Fields["CrKOJI_UMU"].Value = Util.FormatNum(dr["KOJI_UMU"]);
                        rec.Fields["CrKAZEI_KUBUN"].Value = Util.ToString(dr["KAZEI_KUBUN"]);
                        // 編集可否
                        CFieldEnabled(rec.Fields["CrHOJO_KAMOKU_CD"], Util.ToInt(Util.ToString(dr["HOJO_KAMOKU_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["CrBUMON_CD"], Util.ToInt(Util.ToString(dr["BUMON_UMU"])) == 1);
                        CFieldEnabled(rec.Fields["CrDENPYO_KINGAKU"], Util.ToInt(Util.ToString(dr["KANJO_KAMOKU_CD"])) > 0);
                        CFieldEnabled(rec.Fields["CrSHOHIZEI_KINGAKU"], Util.ToInt(Util.ToString(dr["SHOHIZEI_HENKO"])) == 1);
                    }
                    //// 工事・工種編集可否
                    //if (Util.ToInt(rec.Fields["DrKOJI_UMU"]) == 1 || Util.ToInt(rec.Fields["CrKOJI_UMU"]) == 1)
                    //{
                    //    CFieldEnabled(rec.Fields["KOJI_CD"], true);
                    //    CFieldEnabled(rec.Fields["KOSHU_CD"], true);
                    //}
                    //else
                    //{
                    //    CFieldEnabled(rec.Fields["KOJI_CD"], false);
                    //    CFieldEnabled(rec.Fields["KOSHU_CD"], false);
                    //}
                }
                this.UTableGyoBangoRefresh();
            }
            /// **************************************************
            /// UTableレンダリングブロック終了
            /// **************************************************
            mtbList.Render();

            #endregion
        }

        /// <summary>
        /// UTableレコード連番更新
        /// </summary>
        private void UTableGyoBangoRefresh()
        {
            using (mtbList.RenderBlock())
            {
                int gyoBango = 1;
                foreach (UTable.CRecord rec in this.mtbList.Content.Records)
                {
                    if (!ValChk.IsEmpty(rec.Fields["GYO_BANGO"].Value))
                    {
                        rec.Fields["GYO_BANGO"].Value = Util.ToString(gyoBango);
                        gyoBango++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// 補助科目名取得
        /// </summary>
        /// <param name="shishoCd">支所コード</param>
        /// <param name="kanjoKmkCd">勘定科目コード</param>
        /// <param name="code">補助科目コード</param>
        /// <returns>補助科目名</returns>
        private string GetHojoKmkNm(string shishoCd, string kanjoKmkCd, string code)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKmkCd);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, code);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND (SHISHO_CD = @SHISHO_CD or SHISHO_CD is null)");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");

            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_ZM_HOJO_KAMOKU",
                where.ToString(),
                dpc);
            if (dt.Rows.Count != 0)
            {
                name = Util.ToString(dt.Rows[0]["HOJO_KAMOKU_NM"]);
            }
            return name;
        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }

        /// <summary>
        /// 出納帳画面へ返すDataTable作成
        /// </summary>
        /// <returns>DataTable</returns>
        private DataTable GetDtSuito()
        {
            DataTable dt = new DataTable();

            // データテーブル名
            dt.TableName = "TABLE1";
            // フィールド定義
            dt.Columns.Add("KAISHA_CD");//
            dt.Columns.Add("SHISHO_CD");//
            dt.Columns.Add("DENPYO_DATE");//
            dt.Columns.Add("DENPYO_BANGO");//
            dt.Columns.Add("SHIWAKE_GYOSU");//
            dt.Columns.Add("GYO_BANGO", typeof(decimal));
            dt.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
            dt.Columns.Add("MEISAI_KUBUN", typeof(decimal));
            dt.Columns.Add("DENPYO_KUBUN");//
            dt.Columns.Add("SHOHYO_BANGO");//
            dt.Columns.Add("TANTOSHA_CD");//
            dt.Columns.Add("TANTOSHA_NM");//
            dt.Columns.Add("KANJO_KAMOKU_CD");
            dt.Columns.Add("KANJO_KAMOKU_NM");//
            dt.Columns.Add("HOJO_KAMOKU_CD");
            dt.Columns.Add("HOJO_KAMOKU_NM");//
            dt.Columns.Add("BUMON_UMU");//
            dt.Columns.Add("HOJO_UMU");//
            dt.Columns.Add("BUMON_CD");
            dt.Columns.Add("BUMON_NM");//
            dt.Columns.Add("TEKIYO_CD");//
            dt.Columns.Add("TEKIYO");
            dt.Columns.Add("ZEIKOMI_KINGAKU");
            dt.Columns.Add("ZEINUKI_KINGAKU");
            dt.Columns.Add("SHOHIZEI_KINGAKU");
            dt.Columns.Add("ZEI_KUBUN");
            dt.Columns.Add("KAZEI_KUBUN");
            dt.Columns.Add("TORIHIKI_KUBUN");
            dt.Columns.Add("ZEI_RITSU");
            dt.Columns.Add("JIGYO_KUBUN");
            dt.Columns.Add("SHOHIZEI_NYURYOKU_HOHO");
            dt.Columns.Add("SHOHIZEI_HENKO");
            dt.Columns.Add("KESSAN_KUBUN");
            dt.Columns.Add("SHIWAKE_SAKUSEI_KUBUN");//

            DataRow dr;              

            // 伝票番号
            decimal denpyoBango = Util.ToDecimal(this.txtDenpyoBango.Text);

            // (明細データ追加)
            for (int i = 0; i < mtbList.Content.Records.Count; i++)
            {
                UTable.CRecord rec = mtbList.Content.Records[i];

                // 借方レコード
                if (!ValChk.IsEmpty(rec.Fields["DrKANJO_KAMOKU_CD"].Value))
                {
                    dr = GetDtSuitoRecord(dt, denpyoBango, 1, 0, DENPYO_KUBUN, rec);
                    dt.Rows.Add(dr);

                    // 消費税レコード
                    if (!ValChk.IsEmpty(rec.Fields["DrSHOHIZEI_KINGAKU"].Value))
                    {
                        dr = GetDtSuitoRecord(dt, denpyoBango, 1, 1, DENPYO_KUBUN, rec);
                        dt.Rows.Add(dr);
                    }
                }

                // 貸方レコード
                if (!ValChk.IsEmpty(rec.Fields["CrKANJO_KAMOKU_CD"].Value))
                {
                    dr = GetDtSuitoRecord(dt, denpyoBango, 2, 0, DENPYO_KUBUN, rec);
                    dt.Rows.Add(dr);

                    // 消費税レコード
                    if (!ValChk.IsEmpty(rec.Fields["CrSHOHIZEI_KINGAKU"].Value))
                    {
                        dr = GetDtSuitoRecord(dt, denpyoBango, 2, 1, DENPYO_KUBUN, rec);
                        dt.Rows.Add(dr);
                    }
                }
            }

            DataView dv = new DataView(dt);
            dv.Sort = "GYO_BANGO ASC, TAISHAKU_KUBUN ASC, MEISAI_KUBUN ASC";

            return dv.ToTable();
        }

        /// <summary>
        /// 出納帳画面へ返すDataRow作成
        /// </summary>
        /// <param name="dt">作成対象のDataTable</param>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="taishakuKubun">貸借区分</param>
        /// <param name="meisaiKubun">明細区分</param>
        /// <param name="denpyoKubun">伝票区分</param>
        /// <param name="rec">UTableレコード</param>
        /// <returns>設定されたDataRow</returns>
        private DataRow GetDtSuitoRecord(DataTable dt, decimal denpyoBango, decimal taishakuKubun,
            decimal meisaiKubun, decimal denpyoKubun, UTable.CRecord rec)
        {
            // 処理内容はSetZmShiwakeMeisaiParamsと同じイメージ

            DataRow dr = dt.NewRow();

            DateTime denpyoDate = Util.ConvAdDate(
                Util.ToString(this.lblGengoDenpyoDate.Text),
                Util.ToString(this.txtGengoYearDenpyoDate.Text),
                Util.ToString(this.txtMonthDenpyoDate.Text),
                Util.ToString(this.txtDayDenpyoDate.Text), this.Dba);

            dr["KAISHA_CD"] = this.UInfo.KaishaCd;
            dr["SHISHO_CD"] = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));
            dr["DENPYO_DATE"] = denpyoDate;
            dr["DENPYO_BANGO"] = denpyoBango;
            dr["SHIWAKE_GYOSU"] = GetShiwakeGyosu();
            dr["GYO_BANGO"] = Util.ToDecimal(Util.ToString(rec.Fields["GYO_BANGO"].Value));
            dr["TAISHAKU_KUBUN"] = taishakuKubun;
            dr["MEISAI_KUBUN"] = meisaiKubun;
            dr["DENPYO_KUBUN"] = denpyoKubun;
            dr["SHOHYO_BANGO"] = Util.ToString(this.txtShohyoBango.Text);//

            dr["TANTOSHA_CD"] = this.txtTantoshaCd.Text;
            dr["TANTOSHA_NM"] = this.lblTantoshaNm.Text;

            DataRow r = null;

            /// 明細区分・貸借レコード別処理
            if (meisaiKubun == 0 && taishakuKubun == 1)
            {
                /// 明細区分０・借方レコード
                dr["KANJO_KAMOKU_CD"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrKANJO_KAMOKU_CD"].Value));
                dr["KANJO_KAMOKU_NM"] = Util.ToString(rec.Fields["DrKANJO_KAMOKU_NM"].Value);
                dr["HOJO_KAMOKU_CD"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrHOJO_KAMOKU_CD"].Value));
                dr["HOJO_KAMOKU_NM"] = Util.ToString(rec.Fields["DrHOJO_KAMOKU_NM"].Value);
                dr["BUMON_CD"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrBUMON_CD"].Value));
                r = GetBumonDataRow(Util.ToString(dr["BUMON_CD"]));
                if (r != null)
                    dr["BUMON_NM"] = Util.ToString(r["BUMON_NM"]);
                r = GetKanjoKamokuDataRow(Util.ToString(dr["KANJO_KAMOKU_CD"]));
                if (r != null)
                {
                    dr["BUMON_UMU"] = Util.ToString(r["BUMON_UMU"]);
                    dr["HOJO_UMU"] = Util.ToString(r["HOJO_KAMOKU_UMU"]);
                }
                dr["JIGYO_KUBUN"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrJIGYO_KUBUN"].Value));

                dr["ZEI_KUBUN"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_KUBUN"].Value));
                r = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_KUBUN"].Value))));
                if (r != null)
                {
                    dr["KAZEI_KUBUN"] = r["KAZEI_KUBUN"];
                    dr["TORIHIKI_KUBUN"] = r["TORIHIKI_KUBUN"];
                }
                dr["ZEI_RITSU"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_RITSU"].Value).Replace("%", ""));
                dr["SHOHIZEI_HENKO"] = rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0";
                // 税込・税抜金額・消費税額
                dr["ZEIKOMI_KINGAKU"] = ZaUtil.GetZeikomiKingaku(
                        Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)),
                        Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value)),
                        this.UInfo);
                dr["ZEINUKI_KINGAKU"] = ZaUtil.GetZeinukiKingaku(
                        Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)),
                        Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value)),
                        this.UInfo);
                dr["SHOHIZEI_KINGAKU"] = Util.ToDecimal(Util.ToString(rec.Fields["DrSHOHIZEI_KINGAKU"].Value));
            }
            else if (meisaiKubun == 0 && taishakuKubun == 2)
            {
                /// 明細区分０・貸方レコード
                dr["KANJO_KAMOKU_CD"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrKANJO_KAMOKU_CD"].Value));
                dr["KANJO_KAMOKU_NM"] = Util.ToString(rec.Fields["CrKANJO_KAMOKU_NM"].Value);
                dr["HOJO_KAMOKU_CD"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrHOJO_KAMOKU_CD"].Value));
                dr["HOJO_KAMOKU_NM"] = Util.ToString(rec.Fields["CrHOJO_KAMOKU_NM"].Value);
                dr["BUMON_CD"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrBUMON_CD"].Value));
                r = GetBumonDataRow(Util.ToString(dr["BUMON_CD"]));
                if (r != null)
                    dr["BUMON_NM"] = Util.ToString(r["BUMON_NM"]);
                r = GetKanjoKamokuDataRow(Util.ToString(dr["KANJO_KAMOKU_CD"]));
                if (r != null)
                {
                    dr["BUMON_UMU"] = Util.ToString(r["BUMON_UMU"]);
                    dr["HOJO_UMU"] = Util.ToString(r["HOJO_KAMOKU_UMU"]);
                }
                dr["JIGYO_KUBUN"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrJIGYO_KUBUN"].Value));

                dr["ZEI_KUBUN"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_KUBUN"].Value));
                r = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_KUBUN"].Value))));
                if (r != null)
                {
                    dr["KAZEI_KUBUN"] = r["KAZEI_KUBUN"];
                    dr["TORIHIKI_KUBUN"] = r["TORIHIKI_KUBUN"];
                }
                dr["ZEI_RITSU"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_RITSU"].Value).Replace("%", ""));
                dr["SHOHIZEI_HENKO"] = rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0";
                // 税込・税抜金額・消費税額
                dr["ZEIKOMI_KINGAKU"] = ZaUtil.GetZeikomiKingaku(
                        Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)),
                        Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value)),
                        this.UInfo);
                dr["ZEINUKI_KINGAKU"] = ZaUtil.GetZeinukiKingaku(
                        Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)),
                        Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value)),
                        this.UInfo);
                dr["SHOHIZEI_KINGAKU"] = Util.ToDecimal(Util.ToString(rec.Fields["CrSHOHIZEI_KINGAKU"].Value));
            }
            if (meisaiKubun == 1 && taishakuKubun == 1)
            {
                /// 明細区分１・借方レコード
                r = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrZEI_KUBUN"].Value))));
                if (r != null)
                {
                    int triKb = Util.ToInt(r["TORIHIKI_KUBUN"]);
                    if (triKb >= 10 && triKb <= 19)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));

                    }
                    else if (triKb >= 20 && triKb <= 29)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
                    }
                    if (r != null)
                    {
                        dr["KANJO_KAMOKU_CD"] = Util.ToDecimal(Util.ToString(r["KANJO_KAMOKU_CD"]));
                        dr["KANJO_KAMOKU_NM"] = Util.ToString(r["KANJO_KAMOKU_NM"]);
                        dr["HOJO_KAMOKU_CD"] = 0;
                        dr["HOJO_KAMOKU_NM"] = "";
                        dr["BUMON_CD"] = 0;
                        dr["BUMON_NM"] = "";
                        dr["BUMON_UMU"] = Util.ToString(r["BUMON_UMU"]);
                        dr["HOJO_UMU"] = Util.ToString(r["HOJO_KAMOKU_UMU"]);
                        if (taishakuKubun == 1)
                        {
                            dr["ZEI_KUBUN"] = r["KARIKATA_ZEI_KUBUN"];
                            dr["KAZEI_KUBUN"] = r["KARI_KAZEI_KUBUN"];
                            dr["TORIHIKI_KUBUN"] = r["KARI_TORIHIKI_KUBUN"];
                            dr["ZEI_RITSU"] = r["KARI_ZEI_RITSU"];
                        }
                        else
                        {
                            dr["ZEI_KUBUN"] = r["KASHIKATA_ZEI_KUBUN"];
                            dr["KAZEI_KUBUN"] = r["KASHI_KAZEI_KUBUN"];
                            dr["TORIHIKI_KUBUN"] = r["KASHI_TORIHIKI_KUBUN"];
                            dr["ZEI_RITSU"] = r["KASHI_ZEI_RITSU"];
                        }
                    }
                }
                dr["JIGYO_KUBUN"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["DrJIGYO_KUBUN"].Value));
                dr["SHOHIZEI_HENKO"] = rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0";
                /// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                dr["ZEIKOMI_KINGAKU"] = 0;
                dr["ZEINUKI_KINGAKU"] = Util.ToDecimal(rec.Fields["DrSHOHIZEI_KINGAKU"].Value);
                dr["SHOHIZEI_KINGAKU"] = 0;
            }
            if (meisaiKubun == 1 && taishakuKubun == 2)
            {
                /// 明細区分１・貸方レコード
                r = GetZeiKubunDataRow(Util.ToString(ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrZEI_KUBUN"].Value))));
                if (r != null)
                {
                    int triKb = Util.ToInt(r["TORIHIKI_KUBUN"]);
                    if (triKb >= 10 && triKb <= 19)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));

                    }
                    else if (triKb >= 20 && triKb <= 29)
                    {
                        r = GetKanjoKamokuDataRow(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));
                    }
                    if (r != null)
                    {
                        dr["KANJO_KAMOKU_CD"] = Util.ToDecimal(Util.ToString(r["KANJO_KAMOKU_CD"]));
                        dr["KANJO_KAMOKU_NM"] = Util.ToString(r["KANJO_KAMOKU_NM"]);
                        dr["HOJO_KAMOKU_CD"] = 0;
                        dr["HOJO_KAMOKU_NM"] = "";
                        dr["BUMON_CD"] = 0;
                        dr["BUMON_NM"] = "";
                        dr["BUMON_UMU"] = Util.ToString(r["BUMON_UMU"]);
                        dr["HOJO_UMU"] = Util.ToString(r["HOJO_KAMOKU_UMU"]);
                        if (taishakuKubun == 1)
                        {
                            dr["ZEI_KUBUN"] = r["KARIKATA_ZEI_KUBUN"];
                            dr["KAZEI_KUBUN"] = r["KARI_KAZEI_KUBUN"];
                            dr["TORIHIKI_KUBUN"] = r["KARI_TORIHIKI_KUBUN"];
                            dr["ZEI_RITSU"] = r["KARI_ZEI_RITSU"];
                        }
                        else
                        {
                            dr["ZEI_KUBUN"] = r["KASHIKATA_ZEI_KUBUN"];
                            dr["KAZEI_KUBUN"] = r["KASHI_KAZEI_KUBUN"];
                            dr["TORIHIKI_KUBUN"] = r["KASHI_TORIHIKI_KUBUN"];
                            dr["ZEI_RITSU"] = r["KASHI_ZEI_RITSU"];
                        }
                    }
                }
                dr["JIGYO_KUBUN"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["CrJIGYO_KUBUN"].Value));
                dr["SHOHIZEI_HENKO"] = rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.Editable.Equals(UTable.EAllow.ALLOW) ? "1" : "0";
                /// 税込・税抜金額・消費税額(入力方法＝1税抜入力は処理されない)
                dr["ZEIKOMI_KINGAKU"] = 0;
                dr["ZEINUKI_KINGAKU"] = Util.ToDecimal(rec.Fields["CrSHOHIZEI_KINGAKU"].Value);
                dr["SHOHIZEI_KINGAKU"] = 0;

            }
            // 消費税入力方法
            dr["SHOHIZEI_NYURYOKU_HOHO"] = this.UInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
            // 摘要コード
            dr["TEKIYO_CD"] = ZaUtil.ToDecimal(Util.ToString(rec.Fields["TEKIYO_CD"].Value));
            // 摘要
            dr["TEKIYO"] = Util.ToString(rec.Fields["TEKIYO"].Value);
            // 決算区分
            dr["KESSAN_KUBUN"] = rdoKessanKubun0.Checked ? 0 : 1;
            // 仕訳作成区分
            dr["SHIWAKE_SAKUSEI_KUBUN"] = null;

            return dr;
        }
        #endregion

        #region UTable privateメソッド(入力検査)

        /// <summary>
        /// 摘要コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTekiyoCd(UTable.CField field)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(field.Value)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はOK判定
            if (ValChk.IsEmpty(field.Value))
            {
                return true;
            }

            // 存在しないコードを入力されたらエラー
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_TEKIYO", this.txtMizuageShishoCd.Text, Util.ToString(field.Value));
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        ///<summary>
        ///摘要の入力チェック
        /// </summary>
        private bool IsValidTekiyo(UTable.CField field)
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(field.Value, 40))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 貸借共通勘定科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidKANJO_KAMOKU_CD(UTable.CField field)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(field.Value)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はOK判定
            if (ValChk.IsEmpty(Util.ToString(field.Value)))
            {
                return true;
            }

            // 存在しないコードを入力されたらエラー
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.txtMizuageShishoCd.Text, Util.ToString(field.Value));
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 貸借共通補助科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidHOJO_KAMOKU_CD(string kamokuCd, UTable.CField field)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(field.Value)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はOK判定
            if (ValChk.IsEmpty(field.Value))
            {
                return true;
            }

            // 存在しないコードを入力されたらエラー 
            //string name = this.Dba.GetHojoKmkNm(this.UInfo, Util.ToDecimal(this.txtMizuageShishoCd.Text), kamokuCd, Util.ToString(field.Value));
            string name = this.GetHojoKmkNm(this.txtMizuageShishoCd.Text, kamokuCd, Util.ToString(field.Value));
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 貸借共通部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidBUMON_CD(UTable.CField field)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(field.Value)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はOK判定
            if (ValChk.IsEmpty(field.Value))
            {
                return true;
            }

            // 存在しないコードを入力されたらエラー
            DataRow dr = GetBumonDataRow(Util.ToString(field.Value));
            if (dr == null)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 貸借共通伝票金額の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidDENPYO_KINGAKU(UTable.CField field)
        {
            // 10桁数値
            if (!ValChk.IsDecNumWithinLength(
                Util.ToDecimal(Util.ToString(field.Value)), 10, 0, true))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 貸借共通消費税額の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidSHOHIZEI_KINGAKU(UTable.CField field)
        {
            // 10桁数値
            if (!ValChk.IsDecNumWithinLength(
                Util.ToDecimal(Util.ToString(field.Value)), 10, 0, true))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        ///// <summary>
        ///// 工事コードの入力チェック
        ///// </summary>
        ///// <returns>true:OK/false:NG</returns>
        //private bool IsValidKojiCd(UTable.CField field)
        //{
        //    // 数字のみの入力を許可
        //    if (!ValChk.IsNumber(Util.ToString(field.Value)))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    // 未入力はOK判定
        //    if (ValChk.IsEmpty(field.Value))
        //    {
        //        return true;
        //    }

        //    // 存在しないコードを入力されたらエラー
        //    string name = this.Dba.GetName(this.UInfo, "TB_ZM_KOJI", this.txtMizuageShishoCd.Text, Util.ToString(field.Value));
        //    if (ValChk.IsEmpty(name))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    return true;
        //}

        ///// <summary>
        ///// 工種コードの入力チェック
        ///// </summary>
        ///// <returns>true:OK/false:NG</returns>
        //private bool IsValidKoshuCd(UTable.CField field)
        //{
        //    // 数字のみの入力を許可・未入力はNG
        //    if (!ValChk.IsNumber(Util.ToString(field.Value)))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    // 未入力はOK判定
        //    if (ValChk.IsEmpty(field.Value))
        //    {
        //        return true;
        //    }

        //    // 存在しないコードを入力されたらエラー
        //    string name = this.Dba.GetName(this.UInfo, "TB_ZM_KOSHU", this.txtMizuageShishoCd.Text, Util.ToString(field.Value));
        //    if (ValChk.IsEmpty(name))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    return true;
        //}

        /// <summary>
        /// UTable入力項目をチェック
        /// </summary>
        /// <returns></returns>
        private bool IsValidateUTableAll()
        {
            decimal DrGokei = 0;
            decimal CrGokei = 0;
            decimal DrGokeiAbs = 0;
            decimal CrGokeiAbs = 0;

            // 仕訳行数チェック
            if (GetShiwakeGyosu() == 0)
            {
                Msg.Error("省略できません。");
                mtbList.Focus();
                return false;
            }

            // 各明細の編集状態チェック
            for (int i = 0; i < mtbList.Content.Records.Count; i++)
            {
                UTable.CRecord rec = mtbList.Content.Records[i];
                DrGokei += Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value));
                CrGokei += Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value));
                // コード振り変え等で相殺でゼロの入力時の対応
                DrGokeiAbs += Math.Abs(Util.ToDecimal(Util.ToString(rec.Fields["DrDENPYO_KINGAKU"].Value)));
                CrGokeiAbs += Math.Abs(Util.ToDecimal(Util.ToString(rec.Fields["CrDENPYO_KINGAKU"].Value)));
                // 付番済レコードのチェック
                if (!IsValidateUTableRecord(rec))
                {
                    return false;
                }
            }

            // 伝票金額のチェック
            // コード振替等
            //if (DrGokei == 0 || CrGokei == 0)
            if (DrGokeiAbs ==0 && CrGokeiAbs == 0)
            {
                // 行追加でブランク行のみが存在する場合
                Msg.Error("入力内容が無効です。");
                return false;
            }

            // 貸借金額の一致チェック
            if (DrGokei != CrGokei)
            {
                Msg.Error("貸借の金額が一致しません。" + "\n\r" + "金額を確認して下さい。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// UTable.CRecord入力項目をチェック
        /// </summary>
        /// <param name="rec"></param>
        /// <returns></returns>
        private bool IsValidateUTableRecord(UTable.CRecord rec)
        {
            // 摘要コードの入力チェック
            if (!IsValidTekiyoCd(rec.Fields["TEKIYO_CD"]))
            {
                rec.Fields["TEKIYO_CD"].Focus();
                return false;
            }

            // 摘要の入力チェック
            if (!IsValidTekiyo(rec.Fields["TEKIYO"]))
            {
                rec.Fields["TEKIYO"].Focus();
                return false;
            }

            #region 借方情報のチェック
            // 借方勘定科目コードの入力チェック
            if (!IsValidKANJO_KAMOKU_CD(rec.Fields["DrKANJO_KAMOKU_CD"]))
            {
                rec.Fields["DrKANJO_KAMOKU_CD"].Focus();
                return false;
            }
            // ほか項目の入力チェック・省略不可チェック
            if (!ValChk.IsEmpty(rec.Fields["DrKANJO_KAMOKU_CD"].Value))
            {
                DataRow dr = GetKanjoKamokuDataRow(Util.ToString(rec.Fields["DrKANJO_KAMOKU_CD"].Value));
                // 借方補助科目コードの入力チェック
                if (!IsValidHOJO_KAMOKU_CD(Util.ToString(dr["KANJO_KAMOKU_CD"]), rec.Fields["DrHOJO_KAMOKU_CD"]))
                {
                    rec.Fields["DrHOJO_KAMOKU_CD"].Focus();
                    return false;
                }
                // 借方部門コードの入力チェック
                if (!IsValidBUMON_CD(rec.Fields["DrBUMON_CD"]))
                {
                    rec.Fields["DrBUMON_CD"].Focus();
                    return false;
                }
                // 借方伝票金額の入力チェック
                if (!IsValidDENPYO_KINGAKU(rec.Fields["DrDENPYO_KINGAKU"]))
                {
                    rec.Fields["DrDENPYO_KINGAKU"].Focus();
                    return false;
                }
                // 借方消費税金額の入力チェック
                if (!IsValidSHOHIZEI_KINGAKU(rec.Fields["DrSHOHIZEI_KINGAKU"]))
                {
                    rec.Fields["DrSHOHIZEI_KINGAKU"].Focus();
                    return false;
                }
                // 補助科目省略不可チェック
                if (Util.ToString(dr["HOJO_KAMOKU_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["DrHOJO_KAMOKU_CD"].Value))
                {
                    Msg.Info("省略できません。");
                    rec.Fields["DrHOJO_KAMOKU_CD"].Focus();
                    return false;
                }
                // 部門コード省略不可チェック
                if (Util.ToString(dr["BUMON_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["DrBUMON_CD"].Value))
                {
                    Msg.Info("省略できません。");
                    rec.Fields["DrBUMON_CD"].Focus();
                    return false;
                }
                // 借方伝票金額省略不可チェック
                if (ValChk.IsEmpty(rec.Fields["DrDENPYO_KINGAKU"].Value))
                {
                    Msg.Info("省略できません。");
                    rec.Fields["DrDENPYO_KINGAKU"].Focus();
                    return false;
                }
                //// 工事コード省略不可チェック
                //if (Util.ToString(dr["KOJI_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["KOJI_CD"].Value))
                //{
                //    Msg.Info("工事コードが存在しません。");
                //    rec.Fields["KOJI_CD"].Focus();
                //    return false;
                //}
                //// 工種コード省略不可チェック
                //if (Util.ToString(dr["KOJI_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["KOSHU_CD"].Value))
                //{
                //    Msg.Info("工種コードが存在しません。");
                //    rec.Fields["KOSHU_CD"].Focus();
                //    return false;
                //}
            }
            #endregion

            #region 貸方情報のチェック
            // 貸方勘定科目コードの入力チェック
            if (!IsValidKANJO_KAMOKU_CD(rec.Fields["CrKANJO_KAMOKU_CD"]))
            {
                rec.Fields["CrKANJO_KAMOKU_CD"].Focus();
                return false;
            }
            // ほか項目の入力チェック・省略不可チェック
            if (!ValChk.IsEmpty(rec.Fields["CrKANJO_KAMOKU_CD"].Value))
            {
                DataRow dr = GetKanjoKamokuDataRow(Util.ToString(rec.Fields["CrKANJO_KAMOKU_CD"].Value));
                // 貸方補助科目コードの入力チェック
                if (!IsValidHOJO_KAMOKU_CD(Util.ToString(dr["KANJO_KAMOKU_CD"]), rec.Fields["CrHOJO_KAMOKU_CD"]))
                {
                    rec.Fields["CrHOJO_KAMOKU_CD"].Focus();
                    return false;
                }
                // 貸方部門コードの入力チェック
                if (!IsValidBUMON_CD(rec.Fields["CrBUMON_CD"]))
                {
                    rec.Fields["CrBUMON_CD"].Focus();
                    return false;
                }
                // 貸方伝票金額の入力チェック
                if (!IsValidDENPYO_KINGAKU(rec.Fields["CrDENPYO_KINGAKU"]))
                {
                    rec.Fields["CrDENPYO_KINGAKU"].Focus();
                    return false;
                }
                // 貸方消費税金額の入力チェック
                if (!IsValidSHOHIZEI_KINGAKU(rec.Fields["CrSHOHIZEI_KINGAKU"]))
                {
                    rec.Fields["CrSHOHIZEI_KINGAKU"].Focus();
                    return false;
                }
                // 貸方科目省略不可チェック
                if (Util.ToString(dr["HOJO_KAMOKU_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["CrHOJO_KAMOKU_CD"].Value))
                {
                    Msg.Info("省略できません。");
                    rec.Fields["CrHOJO_KAMOKU_CD"].Focus();
                    return false;
                }
                // 貸方コード省略不可チェック
                if (Util.ToString(dr["BUMON_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["CrBUMON_CD"].Value))
                {
                    Msg.Info("省略できません。");
                    rec.Fields["CrBUMON_CD"].Focus();
                    return false;
                }
                // 貸方伝票金額省略不可チェック
                if (ValChk.IsEmpty(rec.Fields["CrDENPYO_KINGAKU"].Value))
                {
                    Msg.Info("省略できません。");
                    rec.Fields["CrDENPYO_KINGAKU"].Focus();
                    return false;
                }
                //// 工事コード省略不可チェック
                //if (Util.ToString(dr["KOJI_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["KOJI_CD"].Value))
                //{
                //    Msg.Info("省略できません。");
                //    rec.Fields["KOJI_CD"].Focus();
                //    return false;
                //}
                //// 工種コード省略不可チェック
                //if (Util.ToString(dr["KOJI_UMU"]) == "1" && ValChk.IsEmpty(rec.Fields["KOSHU_CD"].Value))
                //{
                //    Msg.Info("省略できません。");
                //    rec.Fields["KOSHU_CD"].Focus();
                //    return false;
                //}
            }
            #endregion

            //// 工事コードの入力チェック
            //if (!IsValidKojiCd(rec.Fields["KOJI_CD"]))
            //{
            //    rec.Fields["KOJI_CD"].Focus();
            //    return false;
            //}
            //// 工種コードの入力チェック
            //if (!IsValidKoshuCd(rec.Fields["KOSHU_CD"]))
            //{
            //    rec.Fields["KOSHU_CD"].Focus();
            //    return false;
            //}
            return true;
        }

        /// <summary>
        /// 出納帳呼び出し時の対象科目の存在チェック
        /// </summary>
        /// <returns></returns>
        private bool IsValidateUTableFukugo()
        {
            bool f = false;
            for (int i = 0; i < mtbList.Content.Records.Count; i++)
            {
                UTable.CRecord rec = mtbList.Content.Records[i];
                if (Util.ToDecimal(Util.ToString(rec.Fields["DrKANJO_KAMOKU_CD"].Value)) == Util.ToDecimal(this._bInfo.KanjoKamokuCd) &&
                    Util.ToDecimal(Util.ToString(rec.Fields["DrHOJO_KAMOKU_CD"].Value)) == Util.ToDecimal(this._bInfo.HojoKamokuCd) &&
                    Util.ToDecimal(Util.ToString(rec.Fields["DrBUMON_CD"].Value)) == Util.ToDecimal(this._bInfo.BumonCd))
                {
                    f = true;
                    break;
                }
                else if (Util.ToDecimal(Util.ToString(rec.Fields["CrKANJO_KAMOKU_CD"].Value)) == Util.ToDecimal(this._bInfo.KanjoKamokuCd) &&
                         Util.ToDecimal(Util.ToString(rec.Fields["CrHOJO_KAMOKU_CD"].Value)) == Util.ToDecimal(this._bInfo.HojoKamokuCd) &&
                         Util.ToDecimal(Util.ToString(rec.Fields["CrBUMON_CD"].Value)) == Util.ToDecimal(this._bInfo.BumonCd))
                {
                    f = true;
                    break;
                }
            }
            if (!f)
            {
                // 対象科目が未入力
                Msg.Error("該当する勘定科目コードが入力されていません。、明細を確認して下さい。");
                mtbList.Focus();
                return false;
            }

            return true;
        }
        #endregion

        private void mtbList_FieldValueChanged(UTable.CField field)
        {
            this.IS_INPUT = 2;
        }

        protected void BindTextChanged()
        {
            Control[] list = { txtMizuageShishoCd, txtDenpyoBango, txtGengoYearDenpyoDate, txtMonthDenpyoDate, txtDayDenpyoDate, txtTantoshaCd, txtShohyoBango };
            //foreach (Control control in this.Controls)
            foreach (Control control in list)
            {
                BindTextChanged(control);
            }
        }

        private void BindTextChanged(Control control)
        {
            if (control.HasChildren)
            {
                foreach (Control childControl in control.Controls)
                {
                    BindTextChanged(childControl);
                }
            }
            else
            {
                if (control is common.controls.FsiTextBox)
                {
                    control.TextChanged += new EventHandler(TxtBox_TextChanged);
                }
            }
        }

        private void TxtBox_TextChanged(object sender, EventArgs e)
        {
            this.IS_INPUT = 2;
        }

        private void rdo_CheckedChanged(object sender, EventArgs e)
        {
            this.IS_INPUT = 2;
        }

        private void ZMDE1031_FormClosing(object sender, FormClosingEventArgs e)
        {
            // データ入力済時に確認します
            if (this.IS_INPUT == 2)
            {
                string msg = "未更新データが存在します！　処理を終了しますか？";
                if (Msg.ConfYesNo(msg) == DialogResult.No)
                {
                    // 「いいえ」を押されたらフォームを閉じない
                    // コントロールの検証処理(Validata)を有効にする
                    base.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    e.Cancel = true;
                    return;
                }
            }
        }
        #region イレギュラー関連
        private void IrgLogSet(int autoDataKbn)
        {
            try
            {
                this._irgLog = new LogTbl();
                this._irgLog.Denno = Util.ToInt(this.txtDenpyoBango.Text);
                this._irgLog.Denkb = 0; // 財務は取り合えずゼロ
                DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengoDenpyoDate.Text, this.txtGengoYearDenpyoDate.Text,
                        this.txtMonthDenpyoDate.Text, this.txtDayDenpyoDate.Text, this.Dba);
                this._irgLog.DendateOld = DENPYO_DATE;
                this._irgLog.KingkOld = Util.ToDecimal(this.lblDrDenpyoKingaku.Text);
                this._irgLog.IkkatuDenno = autoDataKbn;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        private void IrgLogInsert(int mode_edit)
        {
            try
            {
                DbParamCollection dpc;
                DataTable dtCheck;
                Decimal DENPYO_BANGO = 0;
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo + 1); // 更新対象会計期の翌期
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                dtCheck = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "TB_ZM_SHIWAKE_DENPYO",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_BANGO = @DENPYO_BANGO",
                    dpc);
                if (dtCheck.Rows.Count > 0)
                {
                    // 翌会計期の期首残が存在する場合に伝票の更新を行われた場合
                    DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoBango.Text);
                    DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengoDenpyoDate.Text, this.txtGengoYearDenpyoDate.Text,
                            this.txtMonthDenpyoDate.Text, this.txtDayDenpyoDate.Text, this.Dba);
                    DbParamCollection updParam;
                    updParam = new DbParamCollection();
                    updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
                    updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.UInfo.UserCd));
                    updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 0);
                    updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                    updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DENPYO_DATE);
                    updParam.SetParam("@DENPYO_DATE_OLD", SqlDbType.DateTime, this._irgLog.DendateOld);
                    updParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, Util.ToDecimal(this.lblDrDenpyoKingaku.Text));
                    updParam.SetParam("@ZEIKOMI_KINGAKU_OLD", SqlDbType.Decimal, 15, this._irgLog.KingkOld);
                    string memo = string.Format("翌期の期首残がある状況で伝票が{0}されました", (mode_edit == 1 ? "登録" : (mode_edit == 2 ? "更新" : "削除")));
                    updParam.SetParam("@MEMO", SqlDbType.VarChar, 40, memo);
                    this.Dba.Insert("TB_CM_IRREGULAR_LOG", updParam);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        #endregion

    }
}
