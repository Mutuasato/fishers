﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.rdoKessanKubunAll = new System.Windows.Forms.RadioButton();
			this.rdoKessanKubun1 = new System.Windows.Forms.RadioButton();
			this.rdoKessanKubun0 = new System.Windows.Forms.RadioButton();
			this.lblDayDenpyoDateTo = new System.Windows.Forms.Label();
			this.txtDayDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoDateTo = new System.Windows.Forms.Label();
			this.lblMonthDenpyoDateTo = new System.Windows.Forms.Label();
			this.lblYearDenpyoDateTo = new System.Windows.Forms.Label();
			this.txtMonthDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoDenpyoDateTo = new System.Windows.Forms.Label();
			this.txtGengoYearDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDayDenpyoDateFr = new System.Windows.Forms.Label();
			this.txtDayDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMonthDenpyoDateFr = new System.Windows.Forms.Label();
			this.lblYearDenpyoDateFr = new System.Windows.Forms.Label();
			this.txtMonthDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoDenpyoDateFr = new System.Windows.Forms.Label();
			this.txtGengoYearDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
			this.txtShohyoBango = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKingaku = new System.Windows.Forms.Label();
			this.txtKingaku = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTekiyo = new System.Windows.Forms.Label();
			this.txtTekiyo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblBumonCdTo = new System.Windows.Forms.Label();
			this.txtBumonCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtBumonCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKojiCdTo = new System.Windows.Forms.Label();
			this.txtKojiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKojiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKanjoKamokuCdTo = new System.Windows.Forms.Label();
			this.txtKanjoKamokuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblHojoKamokuCdTo = new System.Windows.Forms.Label();
			this.txtHojoKamokuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtHojoKamokuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblZeiKubunTo = new System.Windows.Forms.Label();
			this.txtZeiKubunTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtZeiKubunFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtZeiRt = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblZeiRtPer = new System.Windows.Forms.Label();
			this.lblZeiRt = new System.Windows.Forms.Label();
			this.lblBakDenpyoDate = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel17 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel16 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel15 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel14 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel13 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel18 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel19 = new jp.co.fsi.common.FsiPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.fsiPanel20 = new jp.co.fsi.common.FsiPanel();
			this.label10 = new System.Windows.Forms.Label();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel3 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.lblCondition = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel7.SuspendLayout();
			this.fsiPanel17.SuspendLayout();
			this.fsiPanel16.SuspendLayout();
			this.fsiPanel15.SuspendLayout();
			this.fsiPanel8.SuspendLayout();
			this.fsiPanel14.SuspendLayout();
			this.fsiPanel13.SuspendLayout();
			this.fsiPanel12.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel11.SuspendLayout();
			this.fsiPanel10.SuspendLayout();
			this.fsiPanel9.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiTableLayoutPanel2.SuspendLayout();
			this.fsiPanel18.SuspendLayout();
			this.fsiPanel19.SuspendLayout();
			this.fsiPanel20.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiTableLayoutPanel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 609);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.Text = "伝票検索";
			// 
			// rdoKessanKubunAll
			// 
			this.rdoKessanKubunAll.AutoSize = true;
			this.rdoKessanKubunAll.BackColor = System.Drawing.Color.Silver;
			this.rdoKessanKubunAll.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.rdoKessanKubunAll.Location = new System.Drawing.Point(272, 10);
			this.rdoKessanKubunAll.Margin = new System.Windows.Forms.Padding(4);
			this.rdoKessanKubunAll.MinimumSize = new System.Drawing.Size(0, 24);
			this.rdoKessanKubunAll.Name = "rdoKessanKubunAll";
			this.rdoKessanKubunAll.Size = new System.Drawing.Size(74, 24);
			this.rdoKessanKubunAll.TabIndex = 58;
			this.rdoKessanKubunAll.Tag = "CHANGE";
			this.rdoKessanKubunAll.Text = "全仕訳";
			this.rdoKessanKubunAll.UseVisualStyleBackColor = false;
			// 
			// rdoKessanKubun1
			// 
			this.rdoKessanKubun1.AutoSize = true;
			this.rdoKessanKubun1.BackColor = System.Drawing.Color.Silver;
			this.rdoKessanKubun1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.rdoKessanKubun1.Location = new System.Drawing.Point(143, 10);
			this.rdoKessanKubun1.Margin = new System.Windows.Forms.Padding(4);
			this.rdoKessanKubun1.MinimumSize = new System.Drawing.Size(0, 24);
			this.rdoKessanKubun1.Name = "rdoKessanKubun1";
			this.rdoKessanKubun1.Size = new System.Drawing.Size(90, 24);
			this.rdoKessanKubun1.TabIndex = 57;
			this.rdoKessanKubun1.Tag = "CHANGE";
			this.rdoKessanKubun1.Text = "決算仕訳";
			this.rdoKessanKubun1.UseVisualStyleBackColor = false;
			// 
			// rdoKessanKubun0
			// 
			this.rdoKessanKubun0.AutoSize = true;
			this.rdoKessanKubun0.BackColor = System.Drawing.Color.Silver;
			this.rdoKessanKubun0.Checked = true;
			this.rdoKessanKubun0.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.rdoKessanKubun0.Location = new System.Drawing.Point(15, 10);
			this.rdoKessanKubun0.Margin = new System.Windows.Forms.Padding(4);
			this.rdoKessanKubun0.MinimumSize = new System.Drawing.Size(0, 24);
			this.rdoKessanKubun0.Name = "rdoKessanKubun0";
			this.rdoKessanKubun0.Size = new System.Drawing.Size(90, 24);
			this.rdoKessanKubun0.TabIndex = 56;
			this.rdoKessanKubun0.TabStop = true;
			this.rdoKessanKubun0.Tag = "CHANGE";
			this.rdoKessanKubun0.Text = "通常仕訳";
			this.rdoKessanKubun0.UseVisualStyleBackColor = false;
			// 
			// lblDayDenpyoDateTo
			// 
			this.lblDayDenpyoDateTo.AutoSize = true;
			this.lblDayDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
			this.lblDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayDenpyoDateTo.Location = new System.Drawing.Point(584, 10);
			this.lblDayDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDayDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblDayDenpyoDateTo.Name = "lblDayDenpyoDateTo";
			this.lblDayDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
			this.lblDayDenpyoDateTo.TabIndex = 17;
			this.lblDayDenpyoDateTo.Tag = "CHANGE";
			this.lblDayDenpyoDateTo.Text = "日";
			this.lblDayDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDayDenpyoDateTo
			// 
			this.txtDayDenpyoDateTo.AutoSizeFromLength = false;
			this.txtDayDenpyoDateTo.DisplayLength = null;
			this.txtDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDayDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtDayDenpyoDateTo.Location = new System.Drawing.Point(549, 11);
			this.txtDayDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDayDenpyoDateTo.MaxLength = 2;
			this.txtDayDenpyoDateTo.Name = "txtDayDenpyoDateTo";
			this.txtDayDenpyoDateTo.Size = new System.Drawing.Size(28, 23);
			this.txtDayDenpyoDateTo.TabIndex = 16;
			this.txtDayDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateTo_Validating);
			// 
			// lblDenpyoDateTo
			// 
			this.lblDenpyoDateTo.AutoSize = true;
			this.lblDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoDateTo.Location = new System.Drawing.Point(325, 10);
			this.lblDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblDenpyoDateTo.Name = "lblDenpyoDateTo";
			this.lblDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
			this.lblDenpyoDateTo.TabIndex = 10;
			this.lblDenpyoDateTo.Tag = "CHANGE";
			this.lblDenpyoDateTo.Text = "～";
			// 
			// lblMonthDenpyoDateTo
			// 
			this.lblMonthDenpyoDateTo.AutoSize = true;
			this.lblMonthDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
			this.lblMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthDenpyoDateTo.Location = new System.Drawing.Point(518, 10);
			this.lblMonthDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonthDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblMonthDenpyoDateTo.Name = "lblMonthDenpyoDateTo";
			this.lblMonthDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
			this.lblMonthDenpyoDateTo.TabIndex = 15;
			this.lblMonthDenpyoDateTo.Tag = "CHANGE";
			this.lblMonthDenpyoDateTo.Text = "月";
			this.lblMonthDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearDenpyoDateTo
			// 
			this.lblYearDenpyoDateTo.AutoSize = true;
			this.lblYearDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
			this.lblYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearDenpyoDateTo.Location = new System.Drawing.Point(454, 10);
			this.lblYearDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYearDenpyoDateTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblYearDenpyoDateTo.Name = "lblYearDenpyoDateTo";
			this.lblYearDenpyoDateTo.Size = new System.Drawing.Size(24, 24);
			this.lblYearDenpyoDateTo.TabIndex = 13;
			this.lblYearDenpyoDateTo.Tag = "CHANGE";
			this.lblYearDenpyoDateTo.Text = "年";
			this.lblYearDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMonthDenpyoDateTo
			// 
			this.txtMonthDenpyoDateTo.AutoSizeFromLength = false;
			this.txtMonthDenpyoDateTo.DisplayLength = null;
			this.txtMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonthDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtMonthDenpyoDateTo.Location = new System.Drawing.Point(485, 11);
			this.txtMonthDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonthDenpyoDateTo.MaxLength = 2;
			this.txtMonthDenpyoDateTo.Name = "txtMonthDenpyoDateTo";
			this.txtMonthDenpyoDateTo.Size = new System.Drawing.Size(28, 23);
			this.txtMonthDenpyoDateTo.TabIndex = 14;
			this.txtMonthDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateTo_Validating);
			// 
			// lblGengoDenpyoDateTo
			// 
			this.lblGengoDenpyoDateTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoDenpyoDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengoDenpyoDateTo.Location = new System.Drawing.Point(369, 10);
			this.lblGengoDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengoDenpyoDateTo.Name = "lblGengoDenpyoDateTo";
			this.lblGengoDenpyoDateTo.Size = new System.Drawing.Size(53, 24);
			this.lblGengoDenpyoDateTo.TabIndex = 11;
			this.lblGengoDenpyoDateTo.Tag = "DISPNAME";
			this.lblGengoDenpyoDateTo.Text = "平成";
			this.lblGengoDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtGengoYearDenpyoDateTo
			// 
			this.txtGengoYearDenpyoDateTo.AutoSizeFromLength = false;
			this.txtGengoYearDenpyoDateTo.DisplayLength = null;
			this.txtGengoYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGengoYearDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtGengoYearDenpyoDateTo.Location = new System.Drawing.Point(425, 11);
			this.txtGengoYearDenpyoDateTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtGengoYearDenpyoDateTo.MaxLength = 2;
			this.txtGengoYearDenpyoDateTo.Name = "txtGengoYearDenpyoDateTo";
			this.txtGengoYearDenpyoDateTo.Size = new System.Drawing.Size(28, 23);
			this.txtGengoYearDenpyoDateTo.TabIndex = 12;
			this.txtGengoYearDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYearDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateTo_Validating);
			// 
			// lblDayDenpyoDateFr
			// 
			this.lblDayDenpyoDateFr.AutoSize = true;
			this.lblDayDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
			this.lblDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayDenpyoDateFr.Location = new System.Drawing.Point(289, 10);
			this.lblDayDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDayDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblDayDenpyoDateFr.Name = "lblDayDenpyoDateFr";
			this.lblDayDenpyoDateFr.Size = new System.Drawing.Size(24, 24);
			this.lblDayDenpyoDateFr.TabIndex = 9;
			this.lblDayDenpyoDateFr.Tag = "CHANGE";
			this.lblDayDenpyoDateFr.Text = "日";
			this.lblDayDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDayDenpyoDateFr
			// 
			this.txtDayDenpyoDateFr.AutoSizeFromLength = false;
			this.txtDayDenpyoDateFr.DisplayLength = null;
			this.txtDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDayDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtDayDenpyoDateFr.Location = new System.Drawing.Point(254, 11);
			this.txtDayDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDayDenpyoDateFr.MaxLength = 2;
			this.txtDayDenpyoDateFr.Name = "txtDayDenpyoDateFr";
			this.txtDayDenpyoDateFr.Size = new System.Drawing.Size(28, 23);
			this.txtDayDenpyoDateFr.TabIndex = 8;
			this.txtDayDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateFr_Validating);
			// 
			// lblMonthDenpyoDateFr
			// 
			this.lblMonthDenpyoDateFr.AutoSize = true;
			this.lblMonthDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
			this.lblMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthDenpyoDateFr.Location = new System.Drawing.Point(224, 10);
			this.lblMonthDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonthDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblMonthDenpyoDateFr.Name = "lblMonthDenpyoDateFr";
			this.lblMonthDenpyoDateFr.Size = new System.Drawing.Size(24, 24);
			this.lblMonthDenpyoDateFr.TabIndex = 7;
			this.lblMonthDenpyoDateFr.Tag = "CHANGE";
			this.lblMonthDenpyoDateFr.Text = "月";
			this.lblMonthDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearDenpyoDateFr
			// 
			this.lblYearDenpyoDateFr.AutoSize = true;
			this.lblYearDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
			this.lblYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearDenpyoDateFr.Location = new System.Drawing.Point(160, 10);
			this.lblYearDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYearDenpyoDateFr.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblYearDenpyoDateFr.Name = "lblYearDenpyoDateFr";
			this.lblYearDenpyoDateFr.Size = new System.Drawing.Size(24, 24);
			this.lblYearDenpyoDateFr.TabIndex = 5;
			this.lblYearDenpyoDateFr.Tag = "CHANGE";
			this.lblYearDenpyoDateFr.Text = "年";
			this.lblYearDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMonthDenpyoDateFr
			// 
			this.txtMonthDenpyoDateFr.AutoSizeFromLength = false;
			this.txtMonthDenpyoDateFr.DisplayLength = null;
			this.txtMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonthDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtMonthDenpyoDateFr.Location = new System.Drawing.Point(190, 11);
			this.txtMonthDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonthDenpyoDateFr.MaxLength = 2;
			this.txtMonthDenpyoDateFr.Name = "txtMonthDenpyoDateFr";
			this.txtMonthDenpyoDateFr.Size = new System.Drawing.Size(28, 23);
			this.txtMonthDenpyoDateFr.TabIndex = 6;
			this.txtMonthDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateFr_Validating);
			// 
			// lblGengoDenpyoDateFr
			// 
			this.lblGengoDenpyoDateFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoDenpyoDateFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengoDenpyoDateFr.Location = new System.Drawing.Point(74, 10);
			this.lblGengoDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengoDenpyoDateFr.Name = "lblGengoDenpyoDateFr";
			this.lblGengoDenpyoDateFr.Size = new System.Drawing.Size(53, 24);
			this.lblGengoDenpyoDateFr.TabIndex = 3;
			this.lblGengoDenpyoDateFr.Tag = "DISPNAME";
			this.lblGengoDenpyoDateFr.Text = "平成";
			this.lblGengoDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtGengoYearDenpyoDateFr
			// 
			this.txtGengoYearDenpyoDateFr.AutoSizeFromLength = false;
			this.txtGengoYearDenpyoDateFr.DisplayLength = null;
			this.txtGengoYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGengoYearDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtGengoYearDenpyoDateFr.Location = new System.Drawing.Point(130, 11);
			this.txtGengoYearDenpyoDateFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtGengoYearDenpyoDateFr.MaxLength = 2;
			this.txtGengoYearDenpyoDateFr.Name = "txtGengoYearDenpyoDateFr";
			this.txtGengoYearDenpyoDateFr.Size = new System.Drawing.Size(28, 23);
			this.txtGengoYearDenpyoDateFr.TabIndex = 4;
			this.txtGengoYearDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYearDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateFr_Validating);
			// 
			// mtbList
			// 
			this.mtbList.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.mtbList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mtbList.FixedCols = 0;
			this.mtbList.FocusField = null;
			this.mtbList.ForeColor = System.Drawing.Color.Navy;
			this.mtbList.Location = new System.Drawing.Point(0, 0);
			this.mtbList.Margin = new System.Windows.Forms.Padding(4);
			this.mtbList.Name = "mtbList";
			this.mtbList.NotSelectableCols = 0;
			this.mtbList.SelectRange = null;
			this.mtbList.Size = new System.Drawing.Size(1058, 316);
			this.mtbList.TabIndex = 59;
			this.mtbList.Text = "sjMultiTable1";
			this.mtbList.UndoBufferEnabled = false;
			this.mtbList.RecordEnter += new systembase.table.UTable.RecordEnterEventHandler(this.mtbList_RecordEnter);
			this.mtbList.DoubleClick += new System.EventHandler(this.mtbList_DoubleClick);
			this.mtbList.Leave += new System.EventHandler(this.mtbList_Leave);
			this.mtbList.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.mtbList_PreviewKeyDown);
			// 
			// txtShohyoBango
			// 
			this.txtShohyoBango.AutoSizeFromLength = false;
			this.txtShohyoBango.DisplayLength = null;
			this.txtShohyoBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShohyoBango.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtShohyoBango.Location = new System.Drawing.Point(103, 11);
			this.txtShohyoBango.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohyoBango.MaxLength = 10;
			this.txtShohyoBango.Name = "txtShohyoBango";
			this.txtShohyoBango.Size = new System.Drawing.Size(157, 23);
			this.txtShohyoBango.TabIndex = 49;
			// 
			// lblKingaku
			// 
			this.lblKingaku.AutoSize = true;
			this.lblKingaku.BackColor = System.Drawing.Color.Silver;
			this.lblKingaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKingaku.Location = new System.Drawing.Point(727, 149);
			this.lblKingaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKingaku.Name = "lblKingaku";
			this.lblKingaku.Size = new System.Drawing.Size(0, 16);
			this.lblKingaku.TabIndex = 51;
			// 
			// txtKingaku
			// 
			this.txtKingaku.AutoSizeFromLength = false;
			this.txtKingaku.DisplayLength = null;
			this.txtKingaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKingaku.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKingaku.Location = new System.Drawing.Point(118, 10);
			this.txtKingaku.Margin = new System.Windows.Forms.Padding(4);
			this.txtKingaku.MaxLength = 10;
			this.txtKingaku.Name = "txtKingaku";
			this.txtKingaku.Size = new System.Drawing.Size(157, 23);
			this.txtKingaku.TabIndex = 52;
			this.txtKingaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblTekiyo
			// 
			this.lblTekiyo.AutoSize = true;
			this.lblTekiyo.BackColor = System.Drawing.Color.Silver;
			this.lblTekiyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTekiyo.Location = new System.Drawing.Point(727, 193);
			this.lblTekiyo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTekiyo.Name = "lblTekiyo";
			this.lblTekiyo.Size = new System.Drawing.Size(0, 16);
			this.lblTekiyo.TabIndex = 54;
			// 
			// txtTekiyo
			// 
			this.txtTekiyo.AutoSizeFromLength = false;
			this.txtTekiyo.DisplayLength = null;
			this.txtTekiyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTekiyo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtTekiyo.Location = new System.Drawing.Point(80, 9);
			this.txtTekiyo.Margin = new System.Windows.Forms.Padding(4);
			this.txtTekiyo.MaxLength = 40;
			this.txtTekiyo.Name = "txtTekiyo";
			this.txtTekiyo.Size = new System.Drawing.Size(244, 23);
			this.txtTekiyo.TabIndex = 55;
			this.txtTekiyo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTekiyo_KeyDown);
			// 
			// lblBumonCdTo
			// 
			this.lblBumonCdTo.AutoSize = true;
			this.lblBumonCdTo.BackColor = System.Drawing.Color.Silver;
			this.lblBumonCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblBumonCdTo.Location = new System.Drawing.Point(174, 11);
			this.lblBumonCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBumonCdTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblBumonCdTo.Name = "lblBumonCdTo";
			this.lblBumonCdTo.Size = new System.Drawing.Size(24, 24);
			this.lblBumonCdTo.TabIndex = 33;
			this.lblBumonCdTo.Tag = "CHANGE";
			this.lblBumonCdTo.Text = "～";
			this.lblBumonCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBumonCdTo
			// 
			this.txtBumonCdTo.AutoSizeFromLength = false;
			this.txtBumonCdTo.DisplayLength = null;
			this.txtBumonCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtBumonCdTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtBumonCdTo.Location = new System.Drawing.Point(213, 12);
			this.txtBumonCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtBumonCdTo.MaxLength = 4;
			this.txtBumonCdTo.Name = "txtBumonCdTo";
			this.txtBumonCdTo.Size = new System.Drawing.Size(67, 23);
			this.txtBumonCdTo.TabIndex = 34;
			this.txtBumonCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtBumonCdFr
			// 
			this.txtBumonCdFr.AutoSizeFromLength = false;
			this.txtBumonCdFr.DisplayLength = null;
			this.txtBumonCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtBumonCdFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtBumonCdFr.Location = new System.Drawing.Point(95, 12);
			this.txtBumonCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtBumonCdFr.MaxLength = 4;
			this.txtBumonCdFr.Name = "txtBumonCdFr";
			this.txtBumonCdFr.Size = new System.Drawing.Size(67, 23);
			this.txtBumonCdFr.TabIndex = 32;
			this.txtBumonCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblKojiCdTo
			// 
			this.lblKojiCdTo.AutoSize = true;
			this.lblKojiCdTo.BackColor = System.Drawing.Color.Silver;
			this.lblKojiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKojiCdTo.Location = new System.Drawing.Point(164, 10);
			this.lblKojiCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKojiCdTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblKojiCdTo.Name = "lblKojiCdTo";
			this.lblKojiCdTo.Size = new System.Drawing.Size(24, 24);
			this.lblKojiCdTo.TabIndex = 38;
			this.lblKojiCdTo.Tag = "CHANGE";
			this.lblKojiCdTo.Text = "～";
			this.lblKojiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKojiCdTo
			// 
			this.txtKojiCdTo.AutoSizeFromLength = false;
			this.txtKojiCdTo.DisplayLength = null;
			this.txtKojiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKojiCdTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKojiCdTo.Location = new System.Drawing.Point(203, 11);
			this.txtKojiCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtKojiCdTo.MaxLength = 4;
			this.txtKojiCdTo.Name = "txtKojiCdTo";
			this.txtKojiCdTo.Size = new System.Drawing.Size(67, 23);
			this.txtKojiCdTo.TabIndex = 39;
			this.txtKojiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtKojiCdFr
			// 
			this.txtKojiCdFr.AutoSizeFromLength = false;
			this.txtKojiCdFr.DisplayLength = null;
			this.txtKojiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKojiCdFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKojiCdFr.Location = new System.Drawing.Point(85, 11);
			this.txtKojiCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtKojiCdFr.MaxLength = 4;
			this.txtKojiCdFr.Name = "txtKojiCdFr";
			this.txtKojiCdFr.Size = new System.Drawing.Size(67, 23);
			this.txtKojiCdFr.TabIndex = 37;
			this.txtKojiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblKanjoKamokuCdTo
			// 
			this.lblKanjoKamokuCdTo.AutoSize = true;
			this.lblKanjoKamokuCdTo.BackColor = System.Drawing.Color.Silver;
			this.lblKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKanjoKamokuCdTo.Location = new System.Drawing.Point(209, 11);
			this.lblKanjoKamokuCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanjoKamokuCdTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblKanjoKamokuCdTo.Name = "lblKanjoKamokuCdTo";
			this.lblKanjoKamokuCdTo.Size = new System.Drawing.Size(24, 24);
			this.lblKanjoKamokuCdTo.TabIndex = 23;
			this.lblKanjoKamokuCdTo.Tag = "CHANGE";
			this.lblKanjoKamokuCdTo.Text = "～";
			this.lblKanjoKamokuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKanjoKamokuCdTo
			// 
			this.txtKanjoKamokuCdTo.AutoSizeFromLength = false;
			this.txtKanjoKamokuCdTo.DisplayLength = null;
			this.txtKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKanjoKamokuCdTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKanjoKamokuCdTo.Location = new System.Drawing.Point(248, 12);
			this.txtKanjoKamokuCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuCdTo.MaxLength = 6;
			this.txtKanjoKamokuCdTo.Name = "txtKanjoKamokuCdTo";
			this.txtKanjoKamokuCdTo.Size = new System.Drawing.Size(69, 23);
			this.txtKanjoKamokuCdTo.TabIndex = 24;
			this.txtKanjoKamokuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKanjoKamokuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdTo_Validating);
			// 
			// txtKanjoKamokuCdFr
			// 
			this.txtKanjoKamokuCdFr.AutoSizeFromLength = false;
			this.txtKanjoKamokuCdFr.DisplayLength = null;
			this.txtKanjoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKanjoKamokuCdFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKanjoKamokuCdFr.Location = new System.Drawing.Point(130, 12);
			this.txtKanjoKamokuCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuCdFr.MaxLength = 6;
			this.txtKanjoKamokuCdFr.Name = "txtKanjoKamokuCdFr";
			this.txtKanjoKamokuCdFr.Size = new System.Drawing.Size(69, 23);
			this.txtKanjoKamokuCdFr.TabIndex = 22;
			this.txtKanjoKamokuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKanjoKamokuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdFr_Validating);
			// 
			// lblHojoKamokuCdTo
			// 
			this.lblHojoKamokuCdTo.AutoSize = true;
			this.lblHojoKamokuCdTo.BackColor = System.Drawing.Color.Silver;
			this.lblHojoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblHojoKamokuCdTo.Location = new System.Drawing.Point(209, 11);
			this.lblHojoKamokuCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblHojoKamokuCdTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblHojoKamokuCdTo.Name = "lblHojoKamokuCdTo";
			this.lblHojoKamokuCdTo.Size = new System.Drawing.Size(24, 24);
			this.lblHojoKamokuCdTo.TabIndex = 28;
			this.lblHojoKamokuCdTo.Tag = "CHANGE";
			this.lblHojoKamokuCdTo.Text = "～";
			this.lblHojoKamokuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtHojoKamokuCdTo
			// 
			this.txtHojoKamokuCdTo.AutoSizeFromLength = false;
			this.txtHojoKamokuCdTo.DisplayLength = null;
			this.txtHojoKamokuCdTo.Enabled = false;
			this.txtHojoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtHojoKamokuCdTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtHojoKamokuCdTo.Location = new System.Drawing.Point(248, 12);
			this.txtHojoKamokuCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtHojoKamokuCdTo.MaxLength = 4;
			this.txtHojoKamokuCdTo.Name = "txtHojoKamokuCdTo";
			this.txtHojoKamokuCdTo.Size = new System.Drawing.Size(67, 23);
			this.txtHojoKamokuCdTo.TabIndex = 29;
			this.txtHojoKamokuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtHojoKamokuCdFr
			// 
			this.txtHojoKamokuCdFr.AutoSizeFromLength = false;
			this.txtHojoKamokuCdFr.DisplayLength = null;
			this.txtHojoKamokuCdFr.Enabled = false;
			this.txtHojoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtHojoKamokuCdFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtHojoKamokuCdFr.Location = new System.Drawing.Point(130, 12);
			this.txtHojoKamokuCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtHojoKamokuCdFr.MaxLength = 4;
			this.txtHojoKamokuCdFr.Name = "txtHojoKamokuCdFr";
			this.txtHojoKamokuCdFr.Size = new System.Drawing.Size(67, 23);
			this.txtHojoKamokuCdFr.TabIndex = 27;
			this.txtHojoKamokuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblZeiKubunTo
			// 
			this.lblZeiKubunTo.AutoSize = true;
			this.lblZeiKubunTo.BackColor = System.Drawing.Color.Silver;
			this.lblZeiKubunTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblZeiKubunTo.Location = new System.Drawing.Point(209, 9);
			this.lblZeiKubunTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZeiKubunTo.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblZeiKubunTo.Name = "lblZeiKubunTo";
			this.lblZeiKubunTo.Size = new System.Drawing.Size(24, 24);
			this.lblZeiKubunTo.TabIndex = 42;
			this.lblZeiKubunTo.Tag = "CHANGE";
			this.lblZeiKubunTo.Text = "～";
			this.lblZeiKubunTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtZeiKubunTo
			// 
			this.txtZeiKubunTo.AutoSizeFromLength = false;
			this.txtZeiKubunTo.DisplayLength = null;
			this.txtZeiKubunTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtZeiKubunTo.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtZeiKubunTo.Location = new System.Drawing.Point(248, 10);
			this.txtZeiKubunTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtZeiKubunTo.MaxLength = 2;
			this.txtZeiKubunTo.Name = "txtZeiKubunTo";
			this.txtZeiKubunTo.Size = new System.Drawing.Size(67, 23);
			this.txtZeiKubunTo.TabIndex = 43;
			this.txtZeiKubunTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtZeiKubunFr
			// 
			this.txtZeiKubunFr.AutoSizeFromLength = false;
			this.txtZeiKubunFr.DisplayLength = null;
			this.txtZeiKubunFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtZeiKubunFr.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtZeiKubunFr.Location = new System.Drawing.Point(130, 10);
			this.txtZeiKubunFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtZeiKubunFr.MaxLength = 2;
			this.txtZeiKubunFr.Name = "txtZeiKubunFr";
			this.txtZeiKubunFr.Size = new System.Drawing.Size(67, 23);
			this.txtZeiKubunFr.TabIndex = 41;
			this.txtZeiKubunFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtZeiRt
			// 
			this.txtZeiRt.AutoSizeFromLength = false;
			this.txtZeiRt.DisplayLength = null;
			this.txtZeiRt.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtZeiRt.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtZeiRt.Location = new System.Drawing.Point(96, 9);
			this.txtZeiRt.Margin = new System.Windows.Forms.Padding(4);
			this.txtZeiRt.MaxLength = 2;
			this.txtZeiRt.Name = "txtZeiRt";
			this.txtZeiRt.Size = new System.Drawing.Size(67, 23);
			this.txtZeiRt.TabIndex = 46;
			this.txtZeiRt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtZeiRt.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiRt_Validating);
			// 
			// lblZeiRtPer
			// 
			this.lblZeiRtPer.AutoSize = true;
			this.lblZeiRtPer.BackColor = System.Drawing.Color.Silver;
			this.lblZeiRtPer.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblZeiRtPer.Location = new System.Drawing.Point(175, 8);
			this.lblZeiRtPer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZeiRtPer.MinimumSize = new System.Drawing.Size(0, 24);
			this.lblZeiRtPer.Name = "lblZeiRtPer";
			this.lblZeiRtPer.Size = new System.Drawing.Size(24, 24);
			this.lblZeiRtPer.TabIndex = 2;
			this.lblZeiRtPer.Tag = "CHANGE";
			this.lblZeiRtPer.Text = "％";
			this.lblZeiRtPer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblZeiRt
			// 
			this.lblZeiRt.AutoSize = true;
			this.lblZeiRt.BackColor = System.Drawing.Color.Silver;
			this.lblZeiRt.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblZeiRt.Location = new System.Drawing.Point(415, 195);
			this.lblZeiRt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZeiRt.Name = "lblZeiRt";
			this.lblZeiRt.Size = new System.Drawing.Size(0, 16);
			this.lblZeiRt.TabIndex = 45;
			// 
			// lblBakDenpyoDate
			// 
			this.lblBakDenpyoDate.BackColor = System.Drawing.Color.Silver;
			this.lblBakDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBakDenpyoDate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblBakDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBakDenpyoDate.Location = new System.Drawing.Point(0, 0);
			this.lblBakDenpyoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBakDenpyoDate.Name = "lblBakDenpyoDate";
			this.lblBakDenpyoDate.Size = new System.Drawing.Size(647, 42);
			this.lblBakDenpyoDate.TabIndex = 1;
			this.lblBakDenpyoDate.Tag = "CHANGE";
			this.lblBakDenpyoDate.Text = "伝票日付";
			this.lblBakDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(330, 42);
			this.label3.TabIndex = 21;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "勘定科目コード";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(340, 42);
			this.label4.TabIndex = 25;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "補助科目コード";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Silver;
			this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label5.Location = new System.Drawing.Point(0, 0);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(350, 42);
			this.label5.TabIndex = 29;
			this.label5.Tag = "CHANGE";
			this.label5.Text = "税区分コード";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.Silver;
			this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label6.Location = new System.Drawing.Point(0, 0);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(330, 42);
			this.label6.TabIndex = 30;
			this.label6.Tag = "CHANGE";
			this.label6.Text = "部門コード";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label7
			// 
			this.label7.BackColor = System.Drawing.Color.Silver;
			this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label7.Location = new System.Drawing.Point(0, 0);
			this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(310, 42);
			this.label7.TabIndex = 35;
			this.label7.Tag = "CHANGE";
			this.label7.Text = "工事コード";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label8
			// 
			this.label8.BackColor = System.Drawing.Color.Silver;
			this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label8.Location = new System.Drawing.Point(0, 0);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(310, 42);
			this.label8.TabIndex = 44;
			this.label8.Tag = "CHANGE";
			this.label8.Text = "税      率";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label9
			// 
			this.label9.BackColor = System.Drawing.Color.Silver;
			this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
			this.label9.Location = new System.Drawing.Point(0, 0);
			this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(372, 42);
			this.label9.TabIndex = 923;
			this.label9.Tag = "CHANGE";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label11
			// 
			this.label11.BackColor = System.Drawing.Color.Silver;
			this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label11.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label11.Location = new System.Drawing.Point(0, 0);
			this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(354, 42);
			this.label11.TabIndex = 47;
			this.label11.Tag = "CHANGE";
			this.label11.Text = "証憑番号";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label12
			// 
			this.label12.BackColor = System.Drawing.Color.Silver;
			this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label12.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label12.Location = new System.Drawing.Point(0, 0);
			this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(364, 42);
			this.label12.TabIndex = 50;
			this.label12.Tag = "CHANGE";
			this.label12.Text = "金　　額";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label13
			// 
			this.label13.BackColor = System.Drawing.Color.Silver;
			this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label13.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label13.Location = new System.Drawing.Point(0, 0);
			this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(357, 42);
			this.label13.TabIndex = 53;
			this.label13.Tag = "CHANGE";
			this.label13.Text = "摘　　要";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(49, 47);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 4;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1025, 197);
			this.fsiTableLayoutPanel1.TabIndex = 924;
			// 
			// fsiPanel7
			// 
			this.fsiPanel7.Controls.Add(this.fsiPanel17);
			this.fsiPanel7.Controls.Add(this.fsiPanel16);
			this.fsiPanel7.Controls.Add(this.fsiPanel15);
			this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel7.Location = new System.Drawing.Point(4, 151);
			this.fsiPanel7.Name = "fsiPanel7";
			this.fsiPanel7.Size = new System.Drawing.Size(1017, 42);
			this.fsiPanel7.TabIndex = 2;
			this.fsiPanel7.Tag = "CHANGE";
			// 
			// fsiPanel17
			// 
			this.fsiPanel17.Controls.Add(this.txtTekiyo);
			this.fsiPanel17.Controls.Add(this.label13);
			this.fsiPanel17.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel17.Location = new System.Drawing.Point(660, 0);
			this.fsiPanel17.Name = "fsiPanel17";
			this.fsiPanel17.Size = new System.Drawing.Size(357, 42);
			this.fsiPanel17.TabIndex = 7;
			this.fsiPanel17.Tag = "CHANGE";
			// 
			// fsiPanel16
			// 
			this.fsiPanel16.Controls.Add(this.txtZeiRt);
			this.fsiPanel16.Controls.Add(this.lblZeiRtPer);
			this.fsiPanel16.Controls.Add(this.label8);
			this.fsiPanel16.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel16.Location = new System.Drawing.Point(350, 0);
			this.fsiPanel16.Name = "fsiPanel16";
			this.fsiPanel16.Size = new System.Drawing.Size(310, 42);
			this.fsiPanel16.TabIndex = 6;
			this.fsiPanel16.Tag = "CHANGE";
			// 
			// fsiPanel15
			// 
			this.fsiPanel15.Controls.Add(this.txtZeiKubunFr);
			this.fsiPanel15.Controls.Add(this.txtZeiKubunTo);
			this.fsiPanel15.Controls.Add(this.lblZeiKubunTo);
			this.fsiPanel15.Controls.Add(this.label5);
			this.fsiPanel15.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel15.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel15.Name = "fsiPanel15";
			this.fsiPanel15.Size = new System.Drawing.Size(350, 42);
			this.fsiPanel15.TabIndex = 5;
			this.fsiPanel15.Tag = "CHANGE";
			// 
			// fsiPanel8
			// 
			this.fsiPanel8.Controls.Add(this.fsiPanel14);
			this.fsiPanel8.Controls.Add(this.fsiPanel13);
			this.fsiPanel8.Controls.Add(this.fsiPanel12);
			this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel8.Location = new System.Drawing.Point(4, 102);
			this.fsiPanel8.Name = "fsiPanel8";
			this.fsiPanel8.Size = new System.Drawing.Size(1017, 42);
			this.fsiPanel8.TabIndex = 3;
			this.fsiPanel8.Tag = "CHANGE";
			// 
			// fsiPanel14
			// 
			this.fsiPanel14.Controls.Add(this.txtKingaku);
			this.fsiPanel14.Controls.Add(this.label12);
			this.fsiPanel14.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel14.Location = new System.Drawing.Point(650, 0);
			this.fsiPanel14.Name = "fsiPanel14";
			this.fsiPanel14.Size = new System.Drawing.Size(364, 42);
			this.fsiPanel14.TabIndex = 5;
			this.fsiPanel14.Tag = "CHANGE";
			// 
			// fsiPanel13
			// 
			this.fsiPanel13.Controls.Add(this.txtKojiCdFr);
			this.fsiPanel13.Controls.Add(this.txtKojiCdTo);
			this.fsiPanel13.Controls.Add(this.lblKojiCdTo);
			this.fsiPanel13.Controls.Add(this.label7);
			this.fsiPanel13.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel13.Location = new System.Drawing.Point(340, 0);
			this.fsiPanel13.Name = "fsiPanel13";
			this.fsiPanel13.Size = new System.Drawing.Size(310, 42);
			this.fsiPanel13.TabIndex = 4;
			this.fsiPanel13.Tag = "CHANGE";
			// 
			// fsiPanel12
			// 
			this.fsiPanel12.Controls.Add(this.txtHojoKamokuCdFr);
			this.fsiPanel12.Controls.Add(this.txtHojoKamokuCdTo);
			this.fsiPanel12.Controls.Add(this.lblHojoKamokuCdTo);
			this.fsiPanel12.Controls.Add(this.label4);
			this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel12.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel12.Name = "fsiPanel12";
			this.fsiPanel12.Size = new System.Drawing.Size(340, 42);
			this.fsiPanel12.TabIndex = 4;
			this.fsiPanel12.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.fsiPanel11);
			this.fsiPanel2.Controls.Add(this.fsiPanel10);
			this.fsiPanel2.Controls.Add(this.fsiPanel9);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 53);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(1017, 42);
			this.fsiPanel2.TabIndex = 3;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel11
			// 
			this.fsiPanel11.Controls.Add(this.txtShohyoBango);
			this.fsiPanel11.Controls.Add(this.label11);
			this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel11.Location = new System.Drawing.Point(660, 0);
			this.fsiPanel11.Name = "fsiPanel11";
			this.fsiPanel11.Size = new System.Drawing.Size(354, 42);
			this.fsiPanel11.TabIndex = 6;
			this.fsiPanel11.Tag = "CHANGE";
			// 
			// fsiPanel10
			// 
			this.fsiPanel10.Controls.Add(this.txtBumonCdFr);
			this.fsiPanel10.Controls.Add(this.txtBumonCdTo);
			this.fsiPanel10.Controls.Add(this.lblBumonCdTo);
			this.fsiPanel10.Controls.Add(this.label6);
			this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel10.Location = new System.Drawing.Point(330, 0);
			this.fsiPanel10.Name = "fsiPanel10";
			this.fsiPanel10.Size = new System.Drawing.Size(330, 42);
			this.fsiPanel10.TabIndex = 5;
			this.fsiPanel10.Tag = "CHANGE";
			// 
			// fsiPanel9
			// 
			this.fsiPanel9.Controls.Add(this.txtKanjoKamokuCdFr);
			this.fsiPanel9.Controls.Add(this.txtKanjoKamokuCdTo);
			this.fsiPanel9.Controls.Add(this.lblKanjoKamokuCdTo);
			this.fsiPanel9.Controls.Add(this.label3);
			this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel9.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel9.Name = "fsiPanel9";
			this.fsiPanel9.Size = new System.Drawing.Size(330, 42);
			this.fsiPanel9.TabIndex = 4;
			this.fsiPanel9.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.fsiPanel5);
			this.fsiPanel1.Controls.Add(this.fsiPanel6);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(1017, 42);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.rdoKessanKubun0);
			this.fsiPanel5.Controls.Add(this.rdoKessanKubun1);
			this.fsiPanel5.Controls.Add(this.rdoKessanKubunAll);
			this.fsiPanel5.Controls.Add(this.label9);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel5.Location = new System.Drawing.Point(645, 0);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(372, 42);
			this.fsiPanel5.TabIndex = 3;
			this.fsiPanel5.Tag = "CHANGE";
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.lblGengoDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.lblYearDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.txtMonthDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.lblMonthDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.txtGengoYearDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.txtDayDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.lblDayDenpyoDateFr);
			this.fsiPanel6.Controls.Add(this.txtGengoYearDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblGengoDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.txtMonthDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblYearDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblMonthDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.txtDayDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblDayDenpyoDateTo);
			this.fsiPanel6.Controls.Add(this.lblBakDenpyoDate);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel6.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(647, 42);
			this.fsiPanel6.TabIndex = 2;
			this.fsiPanel6.Tag = "CHANGE";
			// 
			// fsiTableLayoutPanel2
			// 
			this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel2.ColumnCount = 1;
			this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel18, 0, 0);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel3, 0, 1);
			this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(8, 247);
			this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
			this.fsiTableLayoutPanel2.RowCount = 2;
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.487085F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.51292F));
			this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(1066, 354);
			this.fsiTableLayoutPanel2.TabIndex = 924;
			// 
			// fsiPanel18
			// 
			this.fsiPanel18.Controls.Add(this.fsiPanel19);
			this.fsiPanel18.Controls.Add(this.fsiPanel20);
			this.fsiPanel18.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel18.Name = "fsiPanel18";
			this.fsiPanel18.Size = new System.Drawing.Size(1058, 23);
			this.fsiPanel18.TabIndex = 926;
			this.fsiPanel18.Tag = "CHANGE";
			// 
			// fsiPanel19
			// 
			this.fsiPanel19.BackColor = System.Drawing.Color.Silver;
			this.fsiPanel19.Controls.Add(this.label2);
			this.fsiPanel19.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel19.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel19.Name = "fsiPanel19";
			this.fsiPanel19.Size = new System.Drawing.Size(403, 27);
			this.fsiPanel19.TabIndex = 908;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(403, 27);
			this.label2.TabIndex = 9;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "借　　　方";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// fsiPanel20
			// 
			this.fsiPanel20.BackColor = System.Drawing.Color.Silver;
			this.fsiPanel20.Controls.Add(this.label10);
			this.fsiPanel20.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel20.Location = new System.Drawing.Point(721, 0);
			this.fsiPanel20.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel20.Name = "fsiPanel20";
			this.fsiPanel20.Size = new System.Drawing.Size(337, 23);
			this.fsiPanel20.TabIndex = 909;
			this.fsiPanel20.Tag = "CHANGE";
			// 
			// label10
			// 
			this.label10.BackColor = System.Drawing.Color.Silver;
			this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.label10.Location = new System.Drawing.Point(0, 0);
			this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(337, 23);
			this.label10.TabIndex = 10;
			this.label10.Tag = "CHANGE";
			this.label10.Text = "貸　　　方";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.mtbList);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 34);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(1058, 316);
			this.fsiPanel3.TabIndex = 1;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiTableLayoutPanel3
			// 
			this.fsiTableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel3.ColumnCount = 1;
			this.fsiTableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel3.Controls.Add(this.lblCondition, 0, 0);
			this.fsiTableLayoutPanel3.Location = new System.Drawing.Point(8, 100);
			this.fsiTableLayoutPanel3.Name = "fsiTableLayoutPanel3";
			this.fsiTableLayoutPanel3.RowCount = 1;
			this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel3.Size = new System.Drawing.Size(39, 140);
			this.fsiTableLayoutPanel3.TabIndex = 925;
			// 
			// lblCondition
			// 
			this.lblCondition.BackColor = System.Drawing.Color.Silver;
			this.lblCondition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblCondition.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCondition.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblCondition.Location = new System.Drawing.Point(5, 1);
			this.lblCondition.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCondition.Name = "lblCondition";
			this.lblCondition.Size = new System.Drawing.Size(29, 138);
			this.lblCondition.TabIndex = 31;
			this.lblCondition.Tag = "CHANGE";
			this.lblCondition.Text = "検索条件";
			this.lblCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ZMDE1032
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel3);
			this.Controls.Add(this.fsiTableLayoutPanel2);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.lblTekiyo);
			this.Controls.Add(this.lblKingaku);
			this.Controls.Add(this.lblZeiRt);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "ZMDE1032";
			this.Text = "伝票検索";
			this.Controls.SetChildIndex(this.lblZeiRt, 0);
			this.Controls.SetChildIndex(this.lblKingaku, 0);
			this.Controls.SetChildIndex(this.lblTekiyo, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel3, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel7.ResumeLayout(false);
			this.fsiPanel17.ResumeLayout(false);
			this.fsiPanel17.PerformLayout();
			this.fsiPanel16.ResumeLayout(false);
			this.fsiPanel16.PerformLayout();
			this.fsiPanel15.ResumeLayout(false);
			this.fsiPanel15.PerformLayout();
			this.fsiPanel8.ResumeLayout(false);
			this.fsiPanel14.ResumeLayout(false);
			this.fsiPanel14.PerformLayout();
			this.fsiPanel13.ResumeLayout(false);
			this.fsiPanel13.PerformLayout();
			this.fsiPanel12.ResumeLayout(false);
			this.fsiPanel12.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel11.ResumeLayout(false);
			this.fsiPanel11.PerformLayout();
			this.fsiPanel10.ResumeLayout(false);
			this.fsiPanel10.PerformLayout();
			this.fsiPanel9.ResumeLayout(false);
			this.fsiPanel9.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiTableLayoutPanel2.ResumeLayout(false);
			this.fsiPanel18.ResumeLayout(false);
			this.fsiPanel19.ResumeLayout(false);
			this.fsiPanel20.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiTableLayoutPanel3.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton rdoKessanKubun1;
        private System.Windows.Forms.RadioButton rdoKessanKubun0;
        private System.Windows.Forms.Label lblDayDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateTo;
        private System.Windows.Forms.Label lblDenpyoDateTo;
        private System.Windows.Forms.Label lblMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblYearDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblGengoDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateTo;
        private System.Windows.Forms.Label lblDayDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateFr;
        private System.Windows.Forms.Label lblMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblYearDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblGengoDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateFr;
        private jp.co.fsi.common.controls.SjMultiTable mtbList;
        private System.Windows.Forms.Label lblKanjoKamokuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCdFr;
        private System.Windows.Forms.RadioButton rdoKessanKubunAll;
        private jp.co.fsi.common.controls.FsiTextBox txtShohyoBango;
        private System.Windows.Forms.Label lblKingaku;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku;
        private System.Windows.Forms.Label lblTekiyo;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyo;
        private System.Windows.Forms.Label lblBumonCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCdFr;
        private System.Windows.Forms.Label lblKojiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKojiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKojiCdFr;
        private System.Windows.Forms.Label lblHojoKamokuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuCdFr;
        private System.Windows.Forms.Label lblZeiKubunTo;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunTo;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunFr;
        private common.controls.FsiTextBox txtZeiRt;
        private System.Windows.Forms.Label lblZeiRtPer;
        private System.Windows.Forms.Label lblZeiRt;
        private System.Windows.Forms.Label lblBakDenpyoDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel17;
        private common.FsiPanel fsiPanel16;
        private common.FsiPanel fsiPanel15;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel14;
        private common.FsiPanel fsiPanel13;
        private common.FsiPanel fsiPanel12;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel10;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel6;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiPanel fsiPanel3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel3;
        private System.Windows.Forms.Label lblCondition;
        private common.FsiPanel fsiPanel18;
        private common.FsiPanel fsiPanel19;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel20;
        private System.Windows.Forms.Label label10;
    }
}