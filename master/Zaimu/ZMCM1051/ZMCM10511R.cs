﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.zm.zmcm1051
{
    /// <summary>
    /// ZMCM10511R の帳票
    /// </summary>
    public partial class ZMCM10511R : BaseReport
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        public ZMCM10511R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {           
            //西暦から和暦に変換
            CultureInfo culture = new CultureInfo("ja-JP", true);
            culture.DateTimeFormat.Calendar = new JapaneseCalendar();
            txtToday.Text = DateTime.Now.ToString("ggyy年M月d日", culture);
        }
    }
}
