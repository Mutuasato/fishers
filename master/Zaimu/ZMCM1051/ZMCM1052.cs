﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1051
{
    /// <summary>
    /// 摘要の登録(ZMCM1052)
    /// </summary>
    public partial class ZMCM1052 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1052()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public ZMCM1052(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：摘要コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
                this.btnF1.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
                this.btnF1.Enabled = false;
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();

            try
            {
                this.Dba.BeginTransaction();

                // データ削除
                // 摘要
                this.Dba.Delete("VI_ZM_TEKIYO",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND TEKIYO_CD = @TEKIYO_CD",
                    (DbParamCollection)delParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally 
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                this.txtTekiyoKana.Focus();
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamshnTekiyo = SetHnTekiyoParams();

            try
            {
                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.摘要
                    this.Dba.Insert("TB_ZM_TEKIYO", (DbParamCollection)alParamshnTekiyo[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.摘要
                    this.Dba.Update("TB_ZM_TEKIYO",
                        (DbParamCollection)alParamshnTekiyo[1],
                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND TEKIYO_CD = @TEKIYO_CD",
                        (DbParamCollection)alParamshnTekiyo[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 摘要コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoCd_Validating
            (object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyoCd())
            {
                e.Cancel = true;
                this.txtTekiyoCd.SelectAll();
            }
        }

        /// <summary>
        /// 摘要名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyoNm())
            {
                e.Cancel = true;
                this.txtTekiyoNm.SelectAll();
            }
        }

        /// <summary>
        /// 摘要カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoKana_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyoKana())
            {
                e.Cancel = true;
                this.txtTekiyoKana.SelectAll();
            }
        }

        /// <summary>
        /// 摘要カナ名Enter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoKana_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 摘要コードの初期値を取得
            // 摘要コードの中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            StringBuilder where = new StringBuilder();
            where.Append(" KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            DataTable dtMaxGyoshubn =
                this.Dba.GetDataTableByConditionWithParams("MAX(TEKIYO_CD) AS MAX_CD",
                    "VI_ZM_TEKIYO", Util.ToString(where), dpc);
            if (dtMaxGyoshubn.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxGyoshubn.Rows[0]["MAX_CD"]))
            {
                this.txtTekiyoCd.Text = Util.ToString(Util.ToInt(dtMaxGyoshubn.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtTekiyoCd.Text = "1";
            }

            // 摘要名に初期フォーカス
            this.ActiveControl = this.txtTekiyoNm;
            this.txtTekiyoNm.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.KAIKEI_NENDO");
            cols.Append(" ,A.TEKIYO_CD");
            cols.Append(" ,A.TEKIYO_NM");
            cols.Append(" ,A.TEKIYO_KANA_NM");

            StringBuilder from = new StringBuilder();
            from.Append("VI_ZM_TEKIYO AS A");


            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 6, Util.ToString(this.InData));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.SHISHO_CD = @SHISHO_CD AND A.KAIKEI_NENDO = @KAIKEI_NENDO AND A.TEKIYO_CD = @TEKIYO_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtTekiyoCd.Text = Util.ToString(drDispData["TEKIYO_CD"]);
            this.txtTekiyoNm.Text = Util.ToString(drDispData["TEKIYO_NM"]);
            this.txtTekiyoKana.Text = Util.ToString(drDispData["TEKIYO_KANA_NM"]);

            // 摘要コードは入力不可
            this.lbTekiyoCd.Enabled = false;
            this.txtTekiyoCd.Enabled = false;
        }

        /// <summary>
        /// 摘要コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTekiyoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 7, this.txtTekiyoCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND TEKIYO_CD = @TEKIYO_CD");
            DataTable dtGyoshubn =
                this.Dba.GetDataTableByConditionWithParams("TEKIYO_CD",
                    "VI_ZM_TEKIYO", Util.ToString(where), dpc);
            if (dtGyoshubn.Rows.Count > 0)
            {
                Msg.Error("既に存在する摘要コードと重複しています。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 摘要名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoNm()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTekiyoNm.Text, this.txtTekiyoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 摘要カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoKana()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTekiyoKana.Text, this.txtTekiyoKana.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 摘要コードのチェック
                if (!IsValidTekiyoCd())
                {
                    this.txtTekiyoCd.Focus();
                    return false;
                }
            }

            // 摘要名のチェック
            if (!IsValidTekiyoNm())
            {
                this.txtTekiyoNm.Focus();
                return false;
            }
            
            // 摘要カナ名のチェック
            if (!IsValidTekiyoKana())
            {
                this.txtTekiyoKana.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// VI_ZM_TEKIYOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetHnTekiyoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと支所コードと摘要コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
                updParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 6, this.txtTekiyoCd.Text);
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと支所コードと摘要コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
                whereParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 6, this.txtTekiyoCd.Text);
                alParams.Add(whereParam);
            }

            // 摘要名
            updParam.SetParam("@TEKIYO_NM", SqlDbType.VarChar, 40, this.txtTekiyoNm.Text);
            // 摘要カナ名
            updParam.SetParam("@TEKIYO_KANA_NM", SqlDbType.VarChar, 30, this.txtTekiyoKana.Text);
            // 会計年度
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// VI_ZM_TEKIYOからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 会社コードと支所コードと摘要コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcDenpyo = new DbParamCollection();
            dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpcDenpyo.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpcDenpyo.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpcDenpyo.SetParam("@TEKIYO_CD", SqlDbType.VarChar, 6, this.txtTekiyoCd.Text);

            alParams.Add(dpcDenpyo);

            return alParams;
        }
		#endregion
	}
}



