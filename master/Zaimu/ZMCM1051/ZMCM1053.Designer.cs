﻿namespace jp.co.fsi.zm.zmcm1051
{
    partial class ZMCM1053
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblTekiyoCdTo = new System.Windows.Forms.Label();
			this.txtTekiyoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblCodeBet1 = new System.Windows.Forms.Label();
			this.lblTekiyoCdFr = new System.Windows.Forms.Label();
			this.txtTekiyoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 96);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(696, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(686, 31);
			this.lblTitle.Text = "";
			// 
			// lblTekiyoCdTo
			// 
			this.lblTekiyoCdTo.BackColor = System.Drawing.Color.Silver;
			this.lblTekiyoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTekiyoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTekiyoCdTo.Location = new System.Drawing.Point(417, 17);
			this.lblTekiyoCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTekiyoCdTo.Name = "lblTekiyoCdTo";
			this.lblTekiyoCdTo.Size = new System.Drawing.Size(240, 24);
			this.lblTekiyoCdTo.TabIndex = 4;
			this.lblTekiyoCdTo.Tag = "DISPNAME";
			this.lblTekiyoCdTo.Text = "最　後";
			this.lblTekiyoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTekiyoCdTo
			// 
			this.txtTekiyoCdTo.AutoSizeFromLength = true;
			this.txtTekiyoCdTo.DisplayLength = null;
			this.txtTekiyoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTekiyoCdTo.Location = new System.Drawing.Point(349, 18);
			this.txtTekiyoCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtTekiyoCdTo.MaxLength = 4;
			this.txtTekiyoCdTo.Name = "txtTekiyoCdTo";
			this.txtTekiyoCdTo.Size = new System.Drawing.Size(63, 23);
			this.txtTekiyoCdTo.TabIndex = 3;
			this.txtTekiyoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTekiyoCdTo.TextChanged += new System.EventHandler(this.txtTekiyoCdTo_TextChanged);
			this.txtTekiyoCdTo.Enter += new System.EventHandler(this.txtTekiyoCdTo_Enter);
			this.txtTekiyoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCdTo_Validating);
			// 
			// lblCodeBet1
			// 
			this.lblCodeBet1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBet1.Location = new System.Drawing.Point(321, 18);
			this.lblCodeBet1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBet1.Name = "lblCodeBet1";
			this.lblCodeBet1.Size = new System.Drawing.Size(20, 24);
			this.lblCodeBet1.TabIndex = 2;
			this.lblCodeBet1.Tag = "CHANGE";
			this.lblCodeBet1.Text = "～";
			this.lblCodeBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTekiyoCdFr
			// 
			this.lblTekiyoCdFr.BackColor = System.Drawing.Color.Silver;
			this.lblTekiyoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTekiyoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTekiyoCdFr.Location = new System.Drawing.Point(73, 18);
			this.lblTekiyoCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTekiyoCdFr.Name = "lblTekiyoCdFr";
			this.lblTekiyoCdFr.Size = new System.Drawing.Size(240, 24);
			this.lblTekiyoCdFr.TabIndex = 1;
			this.lblTekiyoCdFr.Tag = "DISPNAME";
			this.lblTekiyoCdFr.Text = "先　頭";
			this.lblTekiyoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTekiyoCdFr
			// 
			this.txtTekiyoCdFr.AutoSizeFromLength = true;
			this.txtTekiyoCdFr.DisplayLength = null;
			this.txtTekiyoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTekiyoCdFr.Location = new System.Drawing.Point(5, 19);
			this.txtTekiyoCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtTekiyoCdFr.MaxLength = 4;
			this.txtTekiyoCdFr.Name = "txtTekiyoCdFr";
			this.txtTekiyoCdFr.Size = new System.Drawing.Size(63, 23);
			this.txtTekiyoCdFr.TabIndex = 0;
			this.txtTekiyoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTekiyoCdFr.TextChanged += new System.EventHandler(this.txtTekiyoCdFr_TextChanged);
			this.txtTekiyoCdFr.Enter += new System.EventHandler(this.txtTekiyoCdFr_Enter);
			this.txtTekiyoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCdFr_Validating);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 1;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(678, 54);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblTekiyoCdTo);
			this.fsiPanel2.Controls.Add(this.txtTekiyoCdTo);
			this.fsiPanel2.Controls.Add(this.txtTekiyoCdFr);
			this.fsiPanel2.Controls.Add(this.lblCodeBet1);
			this.fsiPanel2.Controls.Add(this.lblTekiyoCdFr);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(670, 46);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(670, 46);
			this.label2.TabIndex = 1;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "摘要コード範囲";
			// 
			// ZMCM1053
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(686, 233);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "ZMCM1053";
			this.ShowFButton = true;
			this.Text = "摘要の印刷";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblTekiyoCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCdTo;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.Label lblTekiyoCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCdFr;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label2;
    };
}