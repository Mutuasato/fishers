﻿namespace jp.co.fsi.zm.zmcm1051
{
    partial class ZMCM1052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTekiyoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbTekiyoCd = new System.Windows.Forms.Label();
            this.txtTekiyoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyoshubnKana = new System.Windows.Forms.Label();
            this.txtTekiyoKana = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyoshubnNM = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 83);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(499, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(488, 31);
            this.lblTitle.Text = "摘要の登録";
            // 
            // txtTekiyoCd
            // 
            this.txtTekiyoCd.AutoSizeFromLength = true;
            this.txtTekiyoCd.DisplayLength = null;
            this.txtTekiyoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTekiyoCd.Location = new System.Drawing.Point(95, 2);
            this.txtTekiyoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTekiyoCd.MaxLength = 4;
            this.txtTekiyoCd.Name = "txtTekiyoCd";
            this.txtTekiyoCd.Size = new System.Drawing.Size(72, 23);
            this.txtTekiyoCd.TabIndex = 2;
            this.txtTekiyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
            // 
            // lbTekiyoCd
            // 
            this.lbTekiyoCd.BackColor = System.Drawing.Color.Silver;
            this.lbTekiyoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbTekiyoCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTekiyoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbTekiyoCd.Location = new System.Drawing.Point(0, 0);
            this.lbTekiyoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTekiyoCd.Name = "lbTekiyoCd";
            this.lbTekiyoCd.Size = new System.Drawing.Size(399, 28);
            this.lbTekiyoCd.TabIndex = 1;
            this.lbTekiyoCd.Tag = "CHANGE";
            this.lbTekiyoCd.Text = "摘要コード";
            this.lbTekiyoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoNm
            // 
            this.txtTekiyoNm.AutoSizeFromLength = false;
            this.txtTekiyoNm.DisplayLength = null;
            this.txtTekiyoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTekiyoNm.Location = new System.Drawing.Point(95, 1);
            this.txtTekiyoNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtTekiyoNm.MaxLength = 20;
            this.txtTekiyoNm.Name = "txtTekiyoNm";
            this.txtTekiyoNm.Size = new System.Drawing.Size(292, 23);
            this.txtTekiyoNm.TabIndex = 1;
            this.txtTekiyoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoNm_Validating);
            // 
            // lbGyoshubnKana
            // 
            this.lbGyoshubnKana.BackColor = System.Drawing.Color.Silver;
            this.lbGyoshubnKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyoshubnKana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbGyoshubnKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbGyoshubnKana.Location = new System.Drawing.Point(0, 0);
            this.lbGyoshubnKana.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbGyoshubnKana.Name = "lbGyoshubnKana";
            this.lbGyoshubnKana.Size = new System.Drawing.Size(399, 29);
            this.lbGyoshubnKana.TabIndex = 0;
            this.lbGyoshubnKana.Tag = "CHANGE";
            this.lbGyoshubnKana.Text = "摘要カナ名";
            this.lbGyoshubnKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoKana
            // 
            this.txtTekiyoKana.AutoSizeFromLength = false;
            this.txtTekiyoKana.DisplayLength = null;
            this.txtTekiyoKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtTekiyoKana.Location = new System.Drawing.Point(95, 2);
            this.txtTekiyoKana.Margin = new System.Windows.Forms.Padding(4);
            this.txtTekiyoKana.MaxLength = 20;
            this.txtTekiyoKana.Name = "txtTekiyoKana";
            this.txtTekiyoKana.Size = new System.Drawing.Size(292, 23);
            this.txtTekiyoKana.TabIndex = 2;
            this.txtTekiyoKana.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTekiyoKana_KeyDown);
            // 
            // lbGyoshubnNM
            // 
            this.lbGyoshubnNM.BackColor = System.Drawing.Color.Silver;
            this.lbGyoshubnNM.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyoshubnNM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbGyoshubnNM.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbGyoshubnNM.Location = new System.Drawing.Point(0, 0);
            this.lbGyoshubnNM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbGyoshubnNM.Name = "lbGyoshubnNM";
            this.lbGyoshubnNM.Size = new System.Drawing.Size(399, 28);
            this.lbGyoshubnNM.TabIndex = 3;
            this.lbGyoshubnNM.Tag = "CHANGE";
            this.lbGyoshubnNM.Text = "摘要名";
            this.lbGyoshubnNM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(407, 107);
            this.fsiTableLayoutPanel1.TabIndex = 903;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtTekiyoCd);
            this.fsiPanel1.Controls.Add(this.lbTekiyoCd);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(399, 28);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtTekiyoNm);
            this.fsiPanel2.Controls.Add(this.lbGyoshubnNM);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 39);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(399, 28);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtTekiyoKana);
            this.fsiPanel3.Controls.Add(this.lbGyoshubnKana);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 74);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(399, 29);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // ZMCM1052
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 220);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1052";
            this.ShowFButton = true;
            this.Text = "摘要の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;
        private System.Windows.Forms.Label lbTekiyoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoNm;
        private System.Windows.Forms.Label lbGyoshubnKana;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoKana;
        private System.Windows.Forms.Label lbGyoshubnNM;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
	};
}