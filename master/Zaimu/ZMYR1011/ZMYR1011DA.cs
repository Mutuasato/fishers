﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMYR1011DA
    {
        #region private変数
        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMYR1011DA(UserInfo uInfo, DbAccess dba, ConfigLoader config)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 貸借対照表設定データ 又は 損益計算書設定データ 又は 製造原価設定データ を取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <returns>貸借対照表設定データ 又は 損益計算書設定データ 又は 製造原価設定データ</returns>
        public DataTable GetKamokuDaiKomoku(int chohyoBunrui)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" SELECT");
            sql.AppendLine(" KAMOKU_BUNRUI,");
            sql.AppendLine(" HYOJI_JUNI,");
            sql.AppendLine(" KAMOKU_BUNRUI_NM,");
            sql.AppendLine(" TAISHAKU_KUBUN,");
            sql.AppendLine(" MEISAI_KOMOKUSU,");
            sql.AppendLine(" MEISAI_KUBUN,");
            sql.AppendLine(" KAKKO_KUBUN,");
            sql.AppendLine(" KAKKO_HYOJI,");
            sql.AppendLine(" MOJI_SHUBETSU,");
            sql.AppendLine(" SHUKEI_KUBUN,");
            sql.AppendLine(" SHUKEI_KEISANSHIKI");
            sql.AppendLine(" FROM");
            sql.AppendLine(" TB_ZM_KESSANSHO_KAMOKU_BUNRUI");
            sql.AppendLine(" WHERE");
            sql.AppendLine(" KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" SHISHO_CD = @SHISHO_CD AND");
            sql.AppendLine(" CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.AppendLine(" SHIYO_KUBUN = 1 AND");
            sql.AppendLine(" KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" ORDER BY");
            sql.AppendLine(" HYOJI_JUNI ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 1, chohyoBunrui);

            DataTable dtYoyakuSettei = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtYoyakuSettei;
        }

        /// <summary>
        /// 貸借勘定科目データを取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <param name="kamokuBunrui">科目分類番号</param>
        /// <param name="gyoBango">行番号</param>
        /// <returns>貸借勘定科目データ</returns>
        public DataTable GetKanjoKamokuIchiran(int kamokuBunrui)
        {
            //////DbParamCollection dpc = new DbParamCollection();
            //////StringBuilder sql = new StringBuilder();

            //////sql.Append(" SELECT");
            //////sql.Append(" KANJO_KAMOKU_CD,");
            //////sql.Append(" KANJO_KAMOKU_NM,");
            //////sql.Append(" TAISHAKU_KUBUN");
            //////sql.Append(" FROM");
            //////sql.Append(" TB_ZM_KANJO_KAMOKU");
            //////sql.Append(" WHERE");
            //////sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            //////sql.Append(" KAMOKU_BUNRUI_CD = @KAMOKU_BUNRUI AND");
            //////sql.Append(" SHIYO_MISHIYO = 1 AND");
            //////sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");
            //////sql.Append(" ORDER BY");
            //////sql.Append(" KANJO_KAMOKU_CD ASC");
            //////dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            //////dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            //////dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 6, kamokuBunrui);

            // 引数の科目分類での抽出ではなく、
            // 決算書科目設定に未登録の科目一覧を返す
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT DISTINCT A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD");
            sql.AppendLine(", A.KANJO_KAMOKU_NM AS KANJO_KAMOKU_NM");
            sql.AppendLine(", A.TAISHAKU_KUBUN AS TAISHAKU_KUBUN");
            sql.AppendLine(" FROM TB_ZM_KANJO_KAMOKU AS A");
            sql.AppendLine(" LEFT OUTER JOIN");
            sql.AppendLine(" TB_ZM_KESSANSHO_KAMOKU_SETTEI AS B");
            sql.AppendLine(" ON A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.AppendLine(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            sql.AppendLine(" WHERE A.KAISHA_CD = @KAISHA_CD");
            sql.AppendLine(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" AND B.KANJO_KAMOKU_CD IS NULL");
            sql.AppendLine(" ORDER BY KANJO_KAMOKU_CD ASC");

            DataTable dtKanjoKamokuIchiran = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtKanjoKamokuIchiran;
        }

        /// <summary>
        /// 貸借対照表データ 又は 損益計算書データ 又は 製造原価データ を取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <param name="kamokuBunrui">科目分類番号</param>
        /// <returns>貸借対照表データ 又は 損益計算書データ 又は 製造原価データ</returns>
        public DataTable GetKamokuShoKomoku(int chohyoBunrui, int kamokuBunrui)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" SELECT");
            sql.AppendLine(" A.KAMOKU_BUNRUI,");
            sql.AppendLine(" A.GYO_BANGO,");
            sql.AppendLine(" A.KANJO_KAMOKU_CD,");
            sql.AppendLine(" A.KANJO_KAMOKU_NM,");
            sql.AppendLine(" A.TAISHAKU_KUBUN,");
            sql.AppendLine(" B.KANJO_KAMOKU_NM AS M_KANJO_KAMOKU_NM,");
            sql.AppendLine(" B.TAISHAKU_KUBUN AS M_TAISHAKU_KUBUN");
            sql.AppendLine(" FROM");
            sql.AppendLine(" TB_ZM_KESSANSHO_KAMOKU_SETTEI AS A");
            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.AppendLine(" ON A.KAISHA_CD = B.KAISHA_CD AND");
            sql.AppendLine(" B.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD AND");
            sql.AppendLine(" B.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" WHERE");
            sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.AppendLine(" A.CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.AppendLine(" A.KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND");
            sql.AppendLine(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" ORDER BY");
            sql.AppendLine(" A.GYO_BANGO ASC,");
            sql.AppendLine(" A.KANJO_KAMOKU_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 1, chohyoBunrui);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 6, kamokuBunrui);

            DataTable dtYoyakuSetteiData = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtYoyakuSetteiData;
        }

        /// <summary>
        /// 貸借勘定科目データを取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <param name="kamokuBunrui">科目分類番号</param>
        /// <param name="gyoBango">行番号</param>
        /// <returns>貸借勘定科目</returns>
        public DataTable GetYoyakuSetteiData(int chohyoBunrui, int kamokuBunrui, int gyoBango)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" SELECT");
            sql.AppendLine(" A.KAMOKU_BUNRUI,");
            sql.AppendLine(" A.GYO_BANGO,");
            sql.AppendLine(" A.KANJO_KAMOKU_CD,");
            sql.AppendLine(" A.KANJO_KAMOKU_NM,");
            sql.AppendLine(" A.TAISHAKU_KUBUN,");
            sql.AppendLine(" B.KANJO_KAMOKU_NM AS M_KANJO_KAMOKU_NM,");
            sql.AppendLine(" B.TAISHAKU_KUBUN AS M_TAISHAKU_KUBUN");
            sql.AppendLine(" FROM");
            sql.AppendLine(" TB_ZM_KESSANSHO_KAMOKU_SETTEI AS A");
            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.AppendLine(" ON A.KAISHA_CD = B.KAISHA_CD AND");
            sql.AppendLine(" B.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD AND");
            sql.AppendLine(" B.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" WHERE");
            sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.AppendLine(" A.CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.AppendLine(" A.KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND");
            sql.AppendLine(" A.GYO_BANGO = @GYO_BANGO AND");
            sql.AppendLine(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.AppendLine(" ORDER BY");
            sql.AppendLine(" A.GYO_BANGO ASC,");
            sql.AppendLine(" A.KANJO_KAMOKU_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 1, chohyoBunrui);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 6, kamokuBunrui);
            dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 2, gyoBango);

            DataTable dtYoyakuSetteiData = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtYoyakuSetteiData;
        }

        /// <summary>
        /// 指定した科目分類のデータを取得
        /// </summary>
        /// <param name="chohyoBunrui">帳票分類番号</param>
        /// <param name="hyojiJuni">表示順位番号</param>
        /// <param name="kamokuBunrui">科目分類番号</param>
        /// <returns>指定した科目分類のデータ</returns>
        public DataTable GetUpDateMotoJoho(int chohyoBunrui, int hyojiJuni, int kamokuBunrui)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" SELECT");
            sql.AppendLine(" *");
            sql.AppendLine(" FROM");
            sql.AppendLine(" TB_ZM_KESSANSHO_KAMOKU_BUNRUI");
            sql.AppendLine(" WHERE");
            sql.AppendLine(" KAISHA_CD = @KAISHA_CD AND");
            sql.AppendLine(" SHISHO_CD = @SHISHO_CD AND");
            sql.AppendLine(" CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND");
            sql.AppendLine(" HYOJI_JUNI = @HYOJI_JUNI AND");
            sql.AppendLine(" KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND");
            sql.AppendLine(" KAIKEI_NENDO = @KAIKEI_NENDO");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 1, chohyoBunrui);
            dpc.SetParam("@HYOJI_JUNI", SqlDbType.Decimal, 4, hyojiJuni);
            dpc.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 6, kamokuBunrui);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);

            DataTable dtUpDateMotoJoho = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtUpDateMotoJoho;
        }
        #endregion
    }
}
