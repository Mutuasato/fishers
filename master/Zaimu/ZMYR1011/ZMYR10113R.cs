﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;
using GrapeCity.ActiveReports.SectionReportModel;
using System.Windows.Forms;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10113R の帳票
    /// </summary>
    public partial class ZMYR10113R : BaseReport
    {

        public ZMYR10113R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void pageFooter_Format(object sender, EventArgs e)
        {
            if (this.textBox7.Text == "12")
            {
                this.pageFooter.Visible = false;
            }
            else
            {
                this.pageFooter.Visible = true;
            }

        }

        private void detail_BeforePrint(object sender, EventArgs e)
        {
            if (this.textBox8.Text == "1")
            {
                detail.NewPage = NewPage.After;
                this.pageFooter.Visible = true;
            }
            else
            {
                detail.NewPage = NewPage.None;
                this.pageFooter.Visible = false;
            }
        }
    }
}
