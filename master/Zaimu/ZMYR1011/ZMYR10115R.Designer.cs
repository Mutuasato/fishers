﻿namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10115R の概要の説明です。
    /// </summary>
    partial class ZMYR10115R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMYR10115R));
			this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.textBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox69 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.txtSouko = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTanaban = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.textBox71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHyojiDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox65)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSouko)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.Height = 0F;
			this.pageHeader.Name = "pageHeader";
			// 
			// detail
			// 
			this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox4,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.shape1,
            this.textBox51,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.line1,
            this.line2,
            this.line3,
            this.line4});
			this.detail.Height = 8.367963F;
			this.detail.Name = "detail";
			// 
			// textBox4
			// 
			this.textBox4.DataField = "ITEM22";
			this.textBox4.Height = 0.1968504F;
			this.textBox4.Left = 0.134252F;
			this.textBox4.MultiLine = false;
			this.textBox4.Name = "textBox4";
			this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: left; verti" +
    "cal-align: middle; ddo-char-set: 1";
			this.textBox4.Text = "22";
			this.textBox4.Top = 0.1968504F;
			this.textBox4.Width = 3.020472F;
			// 
			// textBox1
			// 
			this.textBox1.DataField = "ITEM21";
			this.textBox1.Height = 0.1968504F;
			this.textBox1.Left = 0.134252F;
			this.textBox1.Name = "textBox1";
			this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: left; verti" +
    "cal-align: middle; ddo-char-set: 1";
			this.textBox1.Text = "21";
			this.textBox1.Top = 0F;
			this.textBox1.Width = 3.020472F;
			// 
			// textBox2
			// 
			this.textBox2.DataField = "ITEM23";
			this.textBox2.Height = 0.1968504F;
			this.textBox2.Left = 0.134252F;
			this.textBox2.MultiLine = false;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox2.Text = "23";
			this.textBox2.Top = 2.362205F;
			this.textBox2.Width = 3.020472F;
			// 
			// textBox3
			// 
			this.textBox3.DataField = "ITEM24";
			this.textBox3.Height = 0.1968504F;
			this.textBox3.Left = 0.134252F;
			this.textBox3.Name = "textBox3";
			this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: left; verti" +
    "cal-align: middle; ddo-char-set: 1";
			this.textBox3.Text = "24";
			this.textBox3.Top = 2.637795F;
			this.textBox3.Width = 3.020472F;
			// 
			// textBox5
			// 
			this.textBox5.DataField = "ITEM25";
			this.textBox5.Height = 0.1968504F;
			this.textBox5.Left = 0.1968504F;
			this.textBox5.MultiLine = false;
			this.textBox5.Name = "textBox5";
			this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox5.Text = "25";
			this.textBox5.Top = 4.829134F;
			this.textBox5.Width = 2.957874F;
			// 
			// textBox6
			// 
			this.textBox6.DataField = "ITEM26";
			this.textBox6.Height = 0.1968504F;
			this.textBox6.Left = 5.108662F;
			this.textBox6.Name = "textBox6";
			this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox6.Text = "26";
			this.textBox6.Top = 0F;
			this.textBox6.Width = 1.919291F;
			// 
			// textBox7
			// 
			this.textBox7.DataField = "ITEM27";
			this.textBox7.Height = 0.1968504F;
			this.textBox7.Left = 5.108662F;
			this.textBox7.MultiLine = false;
			this.textBox7.Name = "textBox7";
			this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox7.Text = "27";
			this.textBox7.Top = 2.165354F;
			this.textBox7.Width = 1.919291F;
			// 
			// textBox8
			// 
			this.textBox8.DataField = "ITEM28";
			this.textBox8.Height = 0.1968504F;
			this.textBox8.Left = 5.108662F;
			this.textBox8.Name = "textBox8";
			this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox8.Text = "28";
			this.textBox8.Top = 2.362205F;
			this.textBox8.Width = 1.919291F;
			// 
			// textBox9
			// 
			this.textBox9.DataField = "ITEM29";
			this.textBox9.Height = 0.1968504F;
			this.textBox9.Left = 5.108662F;
			this.textBox9.MultiLine = false;
			this.textBox9.Name = "textBox9";
			this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox9.Text = "29";
			this.textBox9.Top = 4.632284F;
			this.textBox9.Width = 1.919291F;
			// 
			// textBox10
			// 
			this.textBox10.DataField = "ITEM31";
			this.textBox10.Height = 0.1968504F;
			this.textBox10.Left = 0.3956693F;
			this.textBox10.Name = "textBox10";
			this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox10.Text = "31";
			this.textBox10.Top = 0.3937007F;
			this.textBox10.Width = 2.759055F;
			// 
			// textBox11
			// 
			this.textBox11.DataField = "ITEM33";
			this.textBox11.Height = 0.1968504F;
			this.textBox11.Left = 0.3956693F;
			this.textBox11.MultiLine = false;
			this.textBox11.Name = "textBox11";
			this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox11.Text = "33";
			this.textBox11.Top = 0.7874016F;
			this.textBox11.Width = 2.759055F;
			// 
			// textBox12
			// 
			this.textBox12.DataField = "ITEM30";
			this.textBox12.Height = 0.1968504F;
			this.textBox12.Left = 5.108662F;
			this.textBox12.Name = "textBox12";
			this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox12.Text = "30";
			this.textBox12.Top = 4.829134F;
			this.textBox12.Width = 1.919291F;
			// 
			// textBox13
			// 
			this.textBox13.DataField = "ITEM32";
			this.textBox13.Height = 0.1968504F;
			this.textBox13.Left = 0.3956693F;
			this.textBox13.MultiLine = false;
			this.textBox13.Name = "textBox13";
			this.textBox13.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox13.Text = "32";
			this.textBox13.Top = 0.5905511F;
			this.textBox13.Width = 2.759055F;
			// 
			// textBox14
			// 
			this.textBox14.DataField = "ITEM34";
			this.textBox14.Height = 0.1968504F;
			this.textBox14.Left = 0.3956693F;
			this.textBox14.Name = "textBox14";
			this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox14.Text = "34";
			this.textBox14.Top = 0.984252F;
			this.textBox14.Width = 2.759055F;
			// 
			// textBox15
			// 
			this.textBox15.DataField = "ITEM35";
			this.textBox15.Height = 0.1968504F;
			this.textBox15.Left = 0.3956693F;
			this.textBox15.MultiLine = false;
			this.textBox15.Name = "textBox15";
			this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox15.Text = "35";
			this.textBox15.Top = 1.181102F;
			this.textBox15.Width = 2.759055F;
			// 
			// textBox16
			// 
			this.textBox16.DataField = "ITEM36";
			this.textBox16.Height = 0.1968504F;
			this.textBox16.Left = 0.3956693F;
			this.textBox16.Name = "textBox16";
			this.textBox16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox16.Text = "36";
			this.textBox16.Top = 1.377953F;
			this.textBox16.Width = 2.759055F;
			// 
			// textBox17
			// 
			this.textBox17.DataField = "ITEM37";
			this.textBox17.Height = 0.1968504F;
			this.textBox17.Left = 0.3956693F;
			this.textBox17.MultiLine = false;
			this.textBox17.Name = "textBox17";
			this.textBox17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox17.Text = "37";
			this.textBox17.Top = 1.574803F;
			this.textBox17.Width = 2.759055F;
			// 
			// textBox18
			// 
			this.textBox18.DataField = "ITEM38";
			this.textBox18.Height = 0.1968504F;
			this.textBox18.Left = 0.3956693F;
			this.textBox18.Name = "textBox18";
			this.textBox18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox18.Text = "38";
			this.textBox18.Top = 1.771654F;
			this.textBox18.Width = 2.759055F;
			// 
			// textBox19
			// 
			this.textBox19.DataField = "ITEM39";
			this.textBox19.Height = 0.1968504F;
			this.textBox19.Left = 0.3956693F;
			this.textBox19.MultiLine = false;
			this.textBox19.Name = "textBox19";
			this.textBox19.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox19.Text = "39";
			this.textBox19.Top = 1.968504F;
			this.textBox19.Width = 2.759055F;
			// 
			// textBox20
			// 
			this.textBox20.DataField = "ITEM40";
			this.textBox20.Height = 0.1968504F;
			this.textBox20.Left = 0.3956693F;
			this.textBox20.Name = "textBox20";
			this.textBox20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox20.Text = "40";
			this.textBox20.Top = 2.165354F;
			this.textBox20.Width = 2.759055F;
			// 
			// textBox21
			// 
			this.textBox21.DataField = "ITEM42";
			this.textBox21.Height = 0.1968504F;
			this.textBox21.Left = 0.3956693F;
			this.textBox21.MultiLine = false;
			this.textBox21.Name = "textBox21";
			this.textBox21.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox21.Text = "42";
			this.textBox21.Top = 3.057481F;
			this.textBox21.Width = 2.759055F;
			// 
			// textBox22
			// 
			this.textBox22.DataField = "ITEM41";
			this.textBox22.Height = 0.1968504F;
			this.textBox22.Left = 0.3956693F;
			this.textBox22.Name = "textBox22";
			this.textBox22.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox22.Text = "41";
			this.textBox22.Top = 2.86063F;
			this.textBox22.Width = 2.759055F;
			// 
			// textBox23
			// 
			this.textBox23.DataField = "ITEM43";
			this.textBox23.Height = 0.1968504F;
			this.textBox23.Left = 0.3956693F;
			this.textBox23.MultiLine = false;
			this.textBox23.Name = "textBox23";
			this.textBox23.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox23.Text = "43";
			this.textBox23.Top = 3.254331F;
			this.textBox23.Width = 2.759055F;
			// 
			// textBox24
			// 
			this.textBox24.DataField = "ITEM44";
			this.textBox24.Height = 0.1968504F;
			this.textBox24.Left = 0.3956693F;
			this.textBox24.Name = "textBox24";
			this.textBox24.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox24.Text = "44";
			this.textBox24.Top = 3.451182F;
			this.textBox24.Width = 2.759055F;
			// 
			// textBox25
			// 
			this.textBox25.DataField = "ITEM45";
			this.textBox25.Height = 0.1968504F;
			this.textBox25.Left = 0.3956693F;
			this.textBox25.MultiLine = false;
			this.textBox25.Name = "textBox25";
			this.textBox25.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox25.Text = "45";
			this.textBox25.Top = 3.648032F;
			this.textBox25.Width = 2.759055F;
			// 
			// textBox26
			// 
			this.textBox26.DataField = "ITEM46";
			this.textBox26.Height = 0.1968504F;
			this.textBox26.Left = 0.3956693F;
			this.textBox26.Name = "textBox26";
			this.textBox26.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox26.Text = "46";
			this.textBox26.Top = 3.844882F;
			this.textBox26.Width = 2.759055F;
			// 
			// textBox27
			// 
			this.textBox27.DataField = "ITEM47";
			this.textBox27.Height = 0.1968504F;
			this.textBox27.Left = 0.3956693F;
			this.textBox27.MultiLine = false;
			this.textBox27.Name = "textBox27";
			this.textBox27.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox27.Text = "47";
			this.textBox27.Top = 4.041733F;
			this.textBox27.Width = 2.759055F;
			// 
			// textBox28
			// 
			this.textBox28.DataField = "ITEM48";
			this.textBox28.Height = 0.1968504F;
			this.textBox28.Left = 0.3956693F;
			this.textBox28.Name = "textBox28";
			this.textBox28.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox28.Text = "48";
			this.textBox28.Top = 4.238584F;
			this.textBox28.Width = 2.759055F;
			// 
			// textBox29
			// 
			this.textBox29.DataField = "ITEM49";
			this.textBox29.Height = 0.1968504F;
			this.textBox29.Left = 0.3956693F;
			this.textBox29.MultiLine = false;
			this.textBox29.Name = "textBox29";
			this.textBox29.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox29.Text = "49";
			this.textBox29.Top = 4.435433F;
			this.textBox29.Width = 2.759055F;
			// 
			// textBox30
			// 
			this.textBox30.DataField = "ITEM50";
			this.textBox30.Height = 0.1968504F;
			this.textBox30.Left = 0.3956693F;
			this.textBox30.Name = "textBox30";
			this.textBox30.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
			this.textBox30.Text = "50";
			this.textBox30.Top = 4.632284F;
			this.textBox30.Width = 2.759055F;
			// 
			// textBox31
			// 
			this.textBox31.DataField = "ITEM52";
			this.textBox31.Height = 0.1968504F;
			this.textBox31.Left = 3.154725F;
			this.textBox31.MultiLine = false;
			this.textBox31.Name = "textBox31";
			this.textBox31.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox31.Text = "52";
			this.textBox31.Top = 0.5905512F;
			this.textBox31.Width = 1.872835F;
			// 
			// textBox32
			// 
			this.textBox32.DataField = "ITEM51";
			this.textBox32.Height = 0.1968504F;
			this.textBox32.Left = 3.154725F;
			this.textBox32.Name = "textBox32";
			this.textBox32.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox32.Text = "51";
			this.textBox32.Top = 0.3937008F;
			this.textBox32.Width = 1.872835F;
			// 
			// textBox33
			// 
			this.textBox33.DataField = "ITEM53";
			this.textBox33.Height = 0.1968504F;
			this.textBox33.Left = 3.154725F;
			this.textBox33.MultiLine = false;
			this.textBox33.Name = "textBox33";
			this.textBox33.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox33.Text = "53";
			this.textBox33.Top = 0.7874016F;
			this.textBox33.Width = 1.872835F;
			// 
			// textBox34
			// 
			this.textBox34.DataField = "ITEM54";
			this.textBox34.Height = 0.1968504F;
			this.textBox34.Left = 3.154725F;
			this.textBox34.Name = "textBox34";
			this.textBox34.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox34.Text = "54";
			this.textBox34.Top = 0.984252F;
			this.textBox34.Width = 1.872835F;
			// 
			// textBox35
			// 
			this.textBox35.DataField = "ITEM55";
			this.textBox35.Height = 0.1968504F;
			this.textBox35.Left = 3.154725F;
			this.textBox35.MultiLine = false;
			this.textBox35.Name = "textBox35";
			this.textBox35.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox35.Text = "55";
			this.textBox35.Top = 1.181103F;
			this.textBox35.Width = 1.872835F;
			// 
			// textBox36
			// 
			this.textBox36.DataField = "ITEM56";
			this.textBox36.Height = 0.1968504F;
			this.textBox36.Left = 3.154725F;
			this.textBox36.Name = "textBox36";
			this.textBox36.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox36.Text = "56";
			this.textBox36.Top = 1.377953F;
			this.textBox36.Width = 1.872835F;
			// 
			// textBox37
			// 
			this.textBox37.DataField = "ITEM57";
			this.textBox37.Height = 0.1968504F;
			this.textBox37.Left = 3.154725F;
			this.textBox37.MultiLine = false;
			this.textBox37.Name = "textBox37";
			this.textBox37.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox37.Text = "57";
			this.textBox37.Top = 1.574803F;
			this.textBox37.Width = 1.872835F;
			// 
			// textBox38
			// 
			this.textBox38.DataField = "ITEM58";
			this.textBox38.Height = 0.1968504F;
			this.textBox38.Left = 3.154725F;
			this.textBox38.Name = "textBox38";
			this.textBox38.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox38.Text = "58";
			this.textBox38.Top = 1.771654F;
			this.textBox38.Width = 1.872835F;
			// 
			// textBox39
			// 
			this.textBox39.DataField = "ITEM59";
			this.textBox39.Height = 0.1968504F;
			this.textBox39.Left = 3.154725F;
			this.textBox39.MultiLine = false;
			this.textBox39.Name = "textBox39";
			this.textBox39.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox39.Text = "59";
			this.textBox39.Top = 1.968504F;
			this.textBox39.Width = 1.872835F;
			// 
			// textBox40
			// 
			this.textBox40.DataField = "ITEM60";
			this.textBox40.Height = 0.1968504F;
			this.textBox40.Left = 3.154725F;
			this.textBox40.Name = "textBox40";
			this.textBox40.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox40.Text = "60";
			this.textBox40.Top = 2.165354F;
			this.textBox40.Width = 1.872835F;
			// 
			// textBox41
			// 
			this.textBox41.DataField = "ITEM62";
			this.textBox41.Height = 0.1968504F;
			this.textBox41.Left = 3.154725F;
			this.textBox41.MultiLine = false;
			this.textBox41.Name = "textBox41";
			this.textBox41.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox41.Text = "62";
			this.textBox41.Top = 3.057481F;
			this.textBox41.Width = 1.872835F;
			// 
			// textBox42
			// 
			this.textBox42.DataField = "ITEM61";
			this.textBox42.Height = 0.1968504F;
			this.textBox42.Left = 3.154725F;
			this.textBox42.Name = "textBox42";
			this.textBox42.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox42.Text = "61";
			this.textBox42.Top = 2.86063F;
			this.textBox42.Width = 1.872835F;
			// 
			// textBox43
			// 
			this.textBox43.DataField = "ITEM63";
			this.textBox43.Height = 0.1968504F;
			this.textBox43.Left = 3.154725F;
			this.textBox43.MultiLine = false;
			this.textBox43.Name = "textBox43";
			this.textBox43.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox43.Text = "63";
			this.textBox43.Top = 3.254331F;
			this.textBox43.Width = 1.872835F;
			// 
			// textBox44
			// 
			this.textBox44.DataField = "ITEM64";
			this.textBox44.Height = 0.1968504F;
			this.textBox44.Left = 3.154725F;
			this.textBox44.Name = "textBox44";
			this.textBox44.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox44.Text = "64";
			this.textBox44.Top = 3.451181F;
			this.textBox44.Width = 1.872835F;
			// 
			// textBox45
			// 
			this.textBox45.DataField = "ITEM65";
			this.textBox45.Height = 0.1968504F;
			this.textBox45.Left = 3.154725F;
			this.textBox45.MultiLine = false;
			this.textBox45.Name = "textBox45";
			this.textBox45.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox45.Text = "65";
			this.textBox45.Top = 3.648032F;
			this.textBox45.Width = 1.872835F;
			// 
			// textBox46
			// 
			this.textBox46.DataField = "ITEM66";
			this.textBox46.Height = 0.1968504F;
			this.textBox46.Left = 3.154725F;
			this.textBox46.Name = "textBox46";
			this.textBox46.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox46.Text = "66";
			this.textBox46.Top = 3.844882F;
			this.textBox46.Width = 1.872835F;
			// 
			// textBox47
			// 
			this.textBox47.DataField = "ITEM67";
			this.textBox47.Height = 0.1968504F;
			this.textBox47.Left = 3.154725F;
			this.textBox47.MultiLine = false;
			this.textBox47.Name = "textBox47";
			this.textBox47.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox47.Text = "67";
			this.textBox47.Top = 4.041733F;
			this.textBox47.Width = 1.872835F;
			// 
			// textBox48
			// 
			this.textBox48.DataField = "ITEM68";
			this.textBox48.Height = 0.1968504F;
			this.textBox48.Left = 3.154725F;
			this.textBox48.Name = "textBox48";
			this.textBox48.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox48.Text = "68";
			this.textBox48.Top = 4.238583F;
			this.textBox48.Width = 1.872835F;
			// 
			// textBox49
			// 
			this.textBox49.DataField = "ITEM69";
			this.textBox49.Height = 0.1968504F;
			this.textBox49.Left = 3.154725F;
			this.textBox49.MultiLine = false;
			this.textBox49.Name = "textBox49";
			this.textBox49.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox49.Text = "69";
			this.textBox49.Top = 4.435433F;
			this.textBox49.Width = 1.872835F;
			// 
			// textBox50
			// 
			this.textBox50.DataField = "ITEM70";
			this.textBox50.Height = 0.1968504F;
			this.textBox50.Left = 3.154725F;
			this.textBox50.MultiLine = false;
			this.textBox50.Name = "textBox50";
			this.textBox50.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
			this.textBox50.Text = "70";
			this.textBox50.Top = 4.632284F;
			this.textBox50.Width = 1.872835F;
			// 
			// shape1
			// 
			this.shape1.Height = 5.025984F;
			this.shape1.Left = 7.450581E-09F;
			this.shape1.LineWeight = 2F;
			this.shape1.Name = "shape1";
			this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
			this.shape1.Top = 0F;
			this.shape1.Width = 7.090551F;
			// 
			// textBox51
			// 
			this.textBox51.DataField = "ITEM71";
			this.textBox51.Height = 0.1574803F;
			this.textBox51.Left = 0.009055119F;
			this.textBox51.Name = "textBox51";
			this.textBox51.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox51.Text = "71";
			this.textBox51.Top = 5.120079F;
			this.textBox51.Width = 7.018898F;
			// 
			// textBox52
			// 
			this.textBox52.DataField = "ITEM72";
			this.textBox52.Height = 0.1574803F;
			this.textBox52.Left = 0.009055119F;
			this.textBox52.Name = "textBox52";
			this.textBox52.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox52.Text = "72";
			this.textBox52.Top = 5.277559F;
			this.textBox52.Width = 7.018898F;
			// 
			// textBox53
			// 
			this.textBox53.DataField = "ITEM73";
			this.textBox53.Height = 0.1574803F;
			this.textBox53.Left = 0.009055119F;
			this.textBox53.Name = "textBox53";
			this.textBox53.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox53.Text = "73";
			this.textBox53.Top = 5.43504F;
			this.textBox53.Width = 7.018898F;
			// 
			// textBox54
			// 
			this.textBox54.DataField = "ITEM74";
			this.textBox54.Height = 0.1574803F;
			this.textBox54.Left = 0.009055119F;
			this.textBox54.Name = "textBox54";
			this.textBox54.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox54.Text = "74";
			this.textBox54.Top = 5.59252F;
			this.textBox54.Width = 7.018898F;
			// 
			// textBox55
			// 
			this.textBox55.DataField = "ITEM75";
			this.textBox55.Height = 0.1574803F;
			this.textBox55.Left = 0.009055119F;
			this.textBox55.Name = "textBox55";
			this.textBox55.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox55.Text = "75";
			this.textBox55.Top = 5.75F;
			this.textBox55.Width = 7.018898F;
			// 
			// textBox56
			// 
			this.textBox56.DataField = "ITEM76";
			this.textBox56.Height = 0.1574803F;
			this.textBox56.Left = 0.009055119F;
			this.textBox56.Name = "textBox56";
			this.textBox56.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox56.Text = "76";
			this.textBox56.Top = 5.907481F;
			this.textBox56.Width = 7.018898F;
			// 
			// textBox57
			// 
			this.textBox57.DataField = "ITEM77";
			this.textBox57.Height = 0.1574803F;
			this.textBox57.Left = 0.009055119F;
			this.textBox57.Name = "textBox57";
			this.textBox57.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox57.Text = "77";
			this.textBox57.Top = 6.064962F;
			this.textBox57.Width = 7.018898F;
			// 
			// textBox58
			// 
			this.textBox58.DataField = "ITEM78";
			this.textBox58.Height = 0.1574803F;
			this.textBox58.Left = 0.009055119F;
			this.textBox58.Name = "textBox58";
			this.textBox58.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox58.Text = "78";
			this.textBox58.Top = 6.222442F;
			this.textBox58.Width = 7.018898F;
			// 
			// textBox59
			// 
			this.textBox59.DataField = "ITEM79";
			this.textBox59.Height = 0.1574803F;
			this.textBox59.Left = 0.009055119F;
			this.textBox59.Name = "textBox59";
			this.textBox59.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox59.Text = "79";
			this.textBox59.Top = 6.379923F;
			this.textBox59.Width = 7.018898F;
			// 
			// textBox60
			// 
			this.textBox60.DataField = "ITEM80";
			this.textBox60.Height = 0.1574803F;
			this.textBox60.Left = 0.009055119F;
			this.textBox60.Name = "textBox60";
			this.textBox60.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox60.Text = "80";
			this.textBox60.Top = 6.537403F;
			this.textBox60.Width = 7.018898F;
			// 
			// textBox61
			// 
			this.textBox61.DataField = "ITEM81";
			this.textBox61.Height = 0.1574803F;
			this.textBox61.Left = 0.009055119F;
			this.textBox61.Name = "textBox61";
			this.textBox61.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox61.Text = "81";
			this.textBox61.Top = 6.694883F;
			this.textBox61.Width = 7.018898F;
			// 
			// textBox62
			// 
			this.textBox62.DataField = "ITEM82";
			this.textBox62.Height = 0.1574803F;
			this.textBox62.Left = 0.009055119F;
			this.textBox62.Name = "textBox62";
			this.textBox62.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox62.Text = "82";
			this.textBox62.Top = 6.852363F;
			this.textBox62.Width = 7.018898F;
			// 
			// textBox63
			// 
			this.textBox63.DataField = "ITEM83";
			this.textBox63.Height = 0.1574803F;
			this.textBox63.Left = 0.009055119F;
			this.textBox63.Name = "textBox63";
			this.textBox63.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox63.Text = "83";
			this.textBox63.Top = 7.009844F;
			this.textBox63.Width = 7.018898F;
			// 
			// textBox64
			// 
			this.textBox64.DataField = "ITEM84";
			this.textBox64.Height = 0.1574803F;
			this.textBox64.Left = 0.009055119F;
			this.textBox64.Name = "textBox64";
			this.textBox64.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox64.Text = "84";
			this.textBox64.Top = 7.167324F;
			this.textBox64.Width = 7.018898F;
			// 
			// textBox65
			// 
			this.textBox65.DataField = "ITEM85";
			this.textBox65.Height = 0.1574803F;
			this.textBox65.Left = 0.009055119F;
			this.textBox65.Name = "textBox65";
			this.textBox65.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox65.Text = "85";
			this.textBox65.Top = 7.324804F;
			this.textBox65.Width = 7.018898F;
			// 
			// textBox66
			// 
			this.textBox66.DataField = "ITEM86";
			this.textBox66.Height = 0.1574803F;
			this.textBox66.Left = 0.009055119F;
			this.textBox66.Name = "textBox66";
			this.textBox66.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox66.Text = "86";
			this.textBox66.Top = 7.482285F;
			this.textBox66.Width = 7.018898F;
			// 
			// textBox67
			// 
			this.textBox67.DataField = "ITEM87";
			this.textBox67.Height = 0.1574803F;
			this.textBox67.Left = 0.009055119F;
			this.textBox67.Name = "textBox67";
			this.textBox67.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox67.Text = "87";
			this.textBox67.Top = 7.639764F;
			this.textBox67.Width = 7.018898F;
			// 
			// textBox68
			// 
			this.textBox68.DataField = "ITEM88";
			this.textBox68.Height = 0.1574803F;
			this.textBox68.Left = 0.009055119F;
			this.textBox68.Name = "textBox68";
			this.textBox68.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox68.Text = "88";
			this.textBox68.Top = 7.797245F;
			this.textBox68.Width = 7.018898F;
			// 
			// textBox69
			// 
			this.textBox69.DataField = "ITEM89";
			this.textBox69.Height = 0.1574803F;
			this.textBox69.Left = 0.009055119F;
			this.textBox69.Name = "textBox69";
			this.textBox69.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox69.Text = "89";
			this.textBox69.Top = 7.954725F;
			this.textBox69.Width = 7.018898F;
			// 
			// textBox70
			// 
			this.textBox70.DataField = "ITEM90";
			this.textBox70.Height = 0.1574803F;
			this.textBox70.Left = 0.009055119F;
			this.textBox70.Name = "textBox70";
			this.textBox70.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.textBox70.Text = "90";
			this.textBox70.Top = 8.112206F;
			this.textBox70.Width = 7.018898F;
			// 
			// line1
			// 
			this.line1.Height = 5.025985F;
			this.line1.Left = 5.099607F;
			this.line1.LineWeight = 2F;
			this.line1.Name = "line1";
			this.line1.Top = 0F;
			this.line1.Width = 0.009055138F;
			this.line1.X1 = 5.099607F;
			this.line1.X2 = 5.108662F;
			this.line1.Y1 = 5.025985F;
			this.line1.Y2 = 0F;
			// 
			// line2
			// 
			this.line2.Height = 0F;
			this.line2.Left = 3.154725F;
			this.line2.LineWeight = 2F;
			this.line2.Name = "line2";
			this.line2.Top = 2.362205F;
			this.line2.Width = 3.935827F;
			this.line2.X1 = 7.090552F;
			this.line2.X2 = 3.154725F;
			this.line2.Y1 = 2.362205F;
			this.line2.Y2 = 2.362205F;
			// 
			// line3
			// 
			this.line3.Height = 0F;
			this.line3.Left = 3.154725F;
			this.line3.LineWeight = 2F;
			this.line3.Name = "line3";
			this.line3.Top = 4.829134F;
			this.line3.Width = 3.935827F;
			this.line3.X1 = 7.090552F;
			this.line3.X2 = 3.154725F;
			this.line3.Y1 = 4.829134F;
			this.line3.Y2 = 4.829134F;
			// 
			// line4
			// 
			this.line4.Height = 5.025985F;
			this.line4.Left = 3.154725F;
			this.line4.LineWeight = 2F;
			this.line4.Name = "line4";
			this.line4.Top = 0F;
			this.line4.Width = 0F;
			this.line4.X1 = 3.154725F;
			this.line4.X2 = 3.154725F;
			this.line4.Y1 = 5.025985F;
			this.line4.Y2 = 0F;
			// 
			// pageFooter
			// 
			this.pageFooter.Height = 0F;
			this.pageFooter.Name = "pageFooter";
			// 
			// groupHeader1
			// 
			this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSouko,
            this.txtTanaban,
            this.label3});
			this.groupHeader1.Height = 0.5937499F;
			this.groupHeader1.Name = "groupHeader1";
			// 
			// txtSouko
			// 
			this.txtSouko.DataField = "ITEM10";
			this.txtSouko.Height = 0.2283465F;
			this.txtSouko.Left = 0.3956693F;
			this.txtSouko.Name = "txtSouko";
			this.txtSouko.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: middle";
			this.txtSouko.Text = "10 利益処分案";
			this.txtSouko.Top = 0F;
			this.txtSouko.Width = 6.299212F;
			// 
			// txtTanaban
			// 
			this.txtTanaban.DataField = "ITEM05";
			this.txtTanaban.Height = 0.1574803F;
			this.txtTanaban.Left = 0.06102363F;
			this.txtTanaban.MultiLine = false;
			this.txtTanaban.Name = "txtTanaban";
			this.txtTanaban.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
			this.txtTanaban.Text = "5";
			this.txtTanaban.Top = 0.2283465F;
			this.txtTanaban.Width = 6.694882F;
			// 
			// label3
			// 
			this.label3.Height = 0.1555118F;
			this.label3.HyperLink = null;
			this.label3.Left = 6.08819F;
			this.label3.Name = "label3";
			this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: bottom";
			this.label3.Text = "(単位：円)";
			this.label3.Top = 0.438189F;
			this.label3.Width = 1.002362F;
			// 
			// groupFooter1
			// 
			this.groupFooter1.Height = 0F;
			this.groupFooter1.Name = "groupFooter1";
			// 
			// reportHeader1
			// 
			this.reportHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.textBox71,
            this.txtShohinNm,
            this.txtShohinCd,
            this.txtCompanyName,
            this.txtHyojiDate,
            this.textBox72,
            this.line5,
            this.shape2,
            this.shape3});
			this.reportHeader1.Height = 10.31102F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// label1
			// 
			this.label1.Height = 0.3937007F;
			this.label1.HyperLink = null;
			this.label1.Left = 1.577165F;
			this.label1.Name = "label1";
			this.label1.Style = "font-family: ＭＳ 明朝; font-size: 20.25pt; font-style: normal; font-weight: bold; te" +
    "xt-align: center; text-decoration: none; vertical-align: middle";
			this.label1.Text = "決 算 報 告 書";
			this.label1.Top = 2.734252F;
			this.label1.Width = 3.937008F;
			// 
			// textBox71
			// 
			this.textBox71.DataField = "ITEM05";
			this.textBox71.Height = 0.1968504F;
			this.textBox71.Left = 1.858662F;
			this.textBox71.MultiLine = false;
			this.textBox71.Name = "textBox71";
			this.textBox71.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: center";
			this.textBox71.Text = "5";
			this.textBox71.Top = 7.208268F;
			this.textBox71.Width = 3.346457F;
			// 
			// txtShohinNm
			// 
			this.txtShohinNm.DataField = "ITEM07";
			this.txtShohinNm.Height = 0.1968504F;
			this.txtShohinNm.Left = 1.701575F;
			this.txtShohinNm.Name = "txtShohinNm";
			this.txtShohinNm.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center";
			this.txtShohinNm.Text = "7";
			this.txtShohinNm.Top = 7.665748F;
			this.txtShohinNm.Width = 3.502757F;
			// 
			// txtShohinCd
			// 
			this.txtShohinCd.DataField = "ITEM06";
			this.txtShohinCd.Height = 0.1968504F;
			this.txtShohinCd.Left = 1.701575F;
			this.txtShohinCd.MultiLine = false;
			this.txtShohinCd.Name = "txtShohinCd";
			this.txtShohinCd.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center";
			this.txtShohinCd.Text = "6";
			this.txtShohinCd.Top = 7.468504F;
			this.txtShohinCd.Width = 3.503544F;
			// 
			// txtCompanyName
			// 
			this.txtCompanyName.DataField = "ITEM01";
			this.txtCompanyName.Height = 0.2700787F;
			this.txtCompanyName.Left = 2.442913F;
			this.txtCompanyName.MultiLine = false;
			this.txtCompanyName.Name = "txtCompanyName";
			this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 18pt; text-align: center; vertical-align: middle";
			this.txtCompanyName.Text = "第  26  期";
			this.txtCompanyName.Top = 3.775984F;
			this.txtCompanyName.Width = 2.399606F;
			// 
			// txtHyojiDate
			// 
			this.txtHyojiDate.DataField = "ITEM03";
			this.txtHyojiDate.Height = 0.1811024F;
			this.txtHyojiDate.Left = 3.652756F;
			this.txtHyojiDate.Name = "txtHyojiDate";
			this.txtHyojiDate.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom";
			this.txtHyojiDate.Text = "3";
			this.txtHyojiDate.Top = 4.385433F;
			this.txtHyojiDate.Width = 2.050787F;
			// 
			// textBox72
			// 
			this.textBox72.DataField = "ITEM02";
			this.textBox72.Height = 0.1811024F;
			this.textBox72.Left = 1.701575F;
			this.textBox72.MultiLine = false;
			this.textBox72.Name = "textBox72";
			this.textBox72.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.textBox72.Text = "02";
			this.textBox72.Top = 4.385433F;
			this.textBox72.Width = 1.951181F;
			// 
			// line5
			// 
			this.line5.Height = 0F;
			this.line5.Left = 1.850394F;
			this.line5.LineWeight = 1F;
			this.line5.Name = "line5";
			this.line5.Top = 7.405513F;
			this.line5.Width = 3.353938F;
			this.line5.X1 = 1.850394F;
			this.line5.X2 = 5.204332F;
			this.line5.Y1 = 7.405513F;
			this.line5.Y2 = 7.405513F;
			// 
			// shape2
			// 
			this.shape2.Height = 0.708268F;
			this.shape2.Left = 1.472441F;
			this.shape2.Name = "shape2";
			this.shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
			this.shape2.Top = 2.55748F;
			this.shape2.Width = 4.161811F;
			// 
			// shape3
			// 
			this.shape3.Height = 0.768504F;
			this.shape3.Left = 1.441339F;
			this.shape3.Name = "shape3";
			this.shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
			this.shape3.Top = 2.528347F;
			this.shape3.Width = 4.233465F;
			// 
			// reportFooter1
			// 
			this.reportFooter1.Height = 0F;
			this.reportFooter1.Name = "reportFooter1";
			// 
			// ZMYR10115R
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.7874016F;
			this.PageSettings.Margins.Left = 0.5905512F;
			this.PageSettings.Margins.Right = 0.5905512F;
			this.PageSettings.Margins.Top = 0.5905512F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.090552F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.groupHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.groupFooter1);
			this.Sections.Add(this.pageFooter);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox65)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSouko)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSouko;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanaban;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox69;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox70;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox72;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape3;
        public GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
    }
}
