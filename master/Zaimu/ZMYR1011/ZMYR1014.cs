﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// 注記(ZMYR1014)
    /// </summary>
    public partial class ZMYR1014 : BasePgForm
    {

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMYR1014(ZMYR1011 frm)
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // サイズ変更
            //this.Size = new Size(710, 400);
            // Escape F6 のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            //this.ShowFButton = true;

            // 画面の初期表示
            DispData();

            // フォーカス設定
            this.tabPages.Focus();
            this.txtTaishakuTaishohyoChuki1.Focus();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            this.tabPages.SelectedTab = this.tabPage1;
            this.pnlPage1.SelectNextControl(null, true, true, true, true);
            this.txtTaishakuTaishohyoChuki1.Focus();
        }

        /// <summary>
        /// F2キー押下時処理
        /// </summary>
        public override void PressF2()
        {
            this.tabPages.SelectedTab = this.tabPage2;
            this.pnlPage2.SelectNextControl(null, true, true, true, true);
            this.txtSonekiKeisanshoChuki1.Focus();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            this.tabPages.SelectedTab = this.tabPage3;
            this.pnlPage3.SelectNextControl(null, true, true, true, true);
            this.txtSeizoGenkaChuki1.Focus();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = ("保存しますか？");
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 編集データ保存
                string tableNm = "TB_ZM_KESSANSHO_CHUKI";
                string where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = 0 AND KAIKEI_NENDO = @KAIKEI_NENDO";
                ArrayList alParams = SetZmKessanshoChukiParams();
                this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #endregion

        #region イベント

        /// <summary>
        /// 注記テキストボックスValidating時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChuki_Validating(object sender, CancelEventArgs e)
        {
            FsiTextBox txtBox = (FsiTextBox)sender;

            if (!IsValidChuki(txtBox))
            {
                e.Cancel = true;
                txtBox.SelectAll();
            }
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// データを表示
        /// </summary>
        private void DispData()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            StringBuilder sql = new StringBuilder();
            #region sql.Append(SQL文)        // TB_ZM_KESSANSHO_CHUKIの参照
            sql.AppendLine("SELECT KAISHA_CD");
            sql.AppendLine(",KAIKEI_NENDO");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI1");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI2");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI3");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI4");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI5");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI6");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI7");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI8");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI9");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_CHUKI10");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI1");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI2");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI3");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI4");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI5");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI6");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI7");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI8");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI9");
            sql.AppendLine(",SONEKI_KEISANSHO_CHUKI10");
            sql.AppendLine(",SEIZO_GENKA_CHUKI1");
            sql.AppendLine(",SEIZO_GENKA_CHUKI2");
            sql.AppendLine(",SEIZO_GENKA_CHUKI3");
            sql.AppendLine(",SEIZO_GENKA_CHUKI4");
            sql.AppendLine(",SEIZO_GENKA_CHUKI5");
            sql.AppendLine(",SEIZO_GENKA_CHUKI6");
            sql.AppendLine(",SEIZO_GENKA_CHUKI7");
            sql.AppendLine(",SEIZO_GENKA_CHUKI8");
            sql.AppendLine(",SEIZO_GENKA_CHUKI9");
            sql.AppendLine(",SEIZO_GENKA_CHUKI10");
            sql.AppendLine(" FROM TB_ZM_KESSANSHO_CHUKI");
            sql.AppendLine(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.AppendLine(" AND SHISHO_CD = @SHISHO_CD");
            sql.AppendLine(" AND KAIKEI_NENDO <= @KAIKEI_NENDO");
            sql.AppendLine(" ORDER BY KAIKEI_NENDO DESC");
            #endregion
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                this.txtTaishakuTaishohyoChuki1.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI1"]);
                this.txtTaishakuTaishohyoChuki2.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI2"]);
                this.txtTaishakuTaishohyoChuki3.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI3"]);
                this.txtTaishakuTaishohyoChuki4.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI4"]);
                this.txtTaishakuTaishohyoChuki5.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI5"]);
                this.txtTaishakuTaishohyoChuki6.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI6"]);
                this.txtTaishakuTaishohyoChuki7.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI7"]);
                this.txtTaishakuTaishohyoChuki8.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI8"]);
                this.txtTaishakuTaishohyoChuki9.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI9"]);
                this.txtTaishakuTaishohyoChuki10.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_CHUKI10"]);
                this.txtSonekiKeisanshoChuki1.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI1"]);
                this.txtSonekiKeisanshoChuki2.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI2"]);
                this.txtSonekiKeisanshoChuki3.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI3"]);
                this.txtSonekiKeisanshoChuki4.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI4"]);
                this.txtSonekiKeisanshoChuki5.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI5"]);
                this.txtSonekiKeisanshoChuki6.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI6"]);
                this.txtSonekiKeisanshoChuki7.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI7"]);
                this.txtSonekiKeisanshoChuki8.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI8"]);
                this.txtSonekiKeisanshoChuki9.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI9"]);
                this.txtSonekiKeisanshoChuki10.Text = Util.ToString(dr["SONEKI_KEISANSHO_CHUKI10"]);
                this.txtSeizoGenkaChuki1.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI1"]);
                this.txtSeizoGenkaChuki2.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI2"]);
                this.txtSeizoGenkaChuki3.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI3"]);
                this.txtSeizoGenkaChuki4.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI4"]);
                this.txtSeizoGenkaChuki5.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI5"]);
                this.txtSeizoGenkaChuki6.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI6"]);
                this.txtSeizoGenkaChuki7.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI7"]);
                this.txtSeizoGenkaChuki8.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI8"]);
                this.txtSeizoGenkaChuki9.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI9"]);
                this.txtSeizoGenkaChuki10.Text = Util.ToString(dr["SEIZO_GENKA_CHUKI10"]);
            }
        }

        /// <summary>
        /// 注記項目の入力チェック
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool IsValidChuki(FsiTextBox txt)
        {
            // 入力サイズが指定バイト数以上はNG
            if (!ValChk.IsWithinLength(txt.Text, txt.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                switch (txt.Name.Substring(3, 5))       // 先頭５文字でタブページ判定
                {
                    case "TAISH":
                        this.tabPages.SelectedTab = this.tabPage1;
                        break;
                    case "SONEK":
                        this.tabPages.SelectedTab = this.tabPage2;
                        break;
                    case "SEIZO":
                        this.tabPages.SelectedTab = this.tabPage3;
                        break;
                    default:
                        break;
                }
                txt.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            Control[] ctrl;
            FsiTextBox txtBox;

            for (int i = 1; i <= 10; i++)
            {
                // 貸借対照表注記コントロール
                ctrl = this.Controls.Find("txtTaishakuTaishohyoChuki" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    txtBox = (FsiTextBox)ctrl[0];
                    return IsValidChuki(txtBox);
                }

                // 損益計算書注記コントロール
                ctrl = this.Controls.Find("txtSonekiKeisanshoChuki" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    txtBox = (FsiTextBox)ctrl[0];
                    return IsValidChuki(txtBox);
                }

                // 製造原価計算書注記コントロール
                ctrl = this.Controls.Find("txtSeizoGenkaChuki" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    txtBox = (FsiTextBox)ctrl[0];
                    return IsValidChuki(txtBox);
                }
            }

            return true;
        }

        /// <summary>
        /// TB_ZM_KESSANSHO_CHUKIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 更新処理：DbParamCollection*2(WHERE句・SET句)
        /// </returns>
        private ArrayList SetZmKessanshoChukiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection whereParam = new DbParamCollection();
            DbParamCollection setParam = new DbParamCollection();

            // WHERE句パラメータ
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            alParams.Add(whereParam);
            // SET句パラメータ
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI1", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki1.Text);
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI2", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki2.Text);
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI3", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki3.Text);
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI4", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki4.Text);
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI5", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki5.Text);
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI6", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki6.Text);
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI7", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki7.Text);
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI8", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki8.Text);
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI9", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki9.Text);
            setParam.SetParam("@TAISHAKU_TAISHOHYO_CHUKI10", SqlDbType.VarChar, 128, this.txtTaishakuTaishohyoChuki10.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI1", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki1.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI2", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki2.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI3", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki3.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI4", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki4.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI5", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki5.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI6", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki6.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI7", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki7.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI8", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki8.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI9", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki9.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_CHUKI10", SqlDbType.VarChar, 128, this.txtSonekiKeisanshoChuki10.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI1", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki1.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI2", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki2.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI3", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki3.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI4", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki4.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI5", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki5.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI6", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki6.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI7", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki7.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI8", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki8.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI9", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki9.Text);
            setParam.SetParam("@SEIZO_GENKA_CHUKI10", SqlDbType.VarChar, 128, this.txtSeizoGenkaChuki10.Text);
            setParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(setParam);

            return alParams;
        }

        #endregion

    }
}
