﻿namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10114R の概要の説明です。
    /// </summary>
    partial class ZMYR10114R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMYR10114R));
			this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtKingaku01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKamoku01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKingaku03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCompanyName02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHyojiDateFr02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTani = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHyojiDateTo02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.crossSectionLine1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
			this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKingaku02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCompanyName001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtJusho02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtJusho01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHyojiDateTo01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHyojiDateFr01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtKingaku01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKamoku01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKingaku03)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompanyName02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateFr02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTani)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateTo02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKingaku02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompanyName001)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJusho02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJusho01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateTo01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateFr01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.Height = 0F;
			this.pageHeader.Name = "pageHeader";
			// 
			// detail
			// 
			this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line4,
            this.line5,
            this.txtKingaku01,
            this.txtKamoku01});
			this.detail.Height = 0.1666667F;
			this.detail.Name = "detail";
			// 
			// line4
			// 
			this.line4.Height = 0.1653543F;
			this.line4.Left = 0.00393701F;
			this.line4.LineWeight = 2F;
			this.line4.Name = "line4";
			this.line4.Top = 0F;
			this.line4.Width = 0F;
			this.line4.X1 = 0.00393701F;
			this.line4.X2 = 0.00393701F;
			this.line4.Y1 = 0F;
			this.line4.Y2 = 0.1653543F;
			// 
			// line5
			// 
			this.line5.Height = 0.1653543F;
			this.line5.Left = 7.090551F;
			this.line5.LineWeight = 2F;
			this.line5.Name = "line5";
			this.line5.Top = 0F;
			this.line5.Width = 0F;
			this.line5.X1 = 7.090551F;
			this.line5.X2 = 7.090551F;
			this.line5.Y1 = 0F;
			this.line5.Y2 = 0.1653543F;
			// 
			// txtKingaku01
			// 
			this.txtKingaku01.DataField = "ITEM22";
			this.txtKingaku01.Height = 0.1653543F;
			this.txtKingaku01.Left = 2.840158F;
			this.txtKingaku01.Name = "txtKingaku01";
			this.txtKingaku01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 128";
			this.txtKingaku01.Text = "22";
			this.txtKingaku01.Top = 0F;
			this.txtKingaku01.Width = 2.085433F;
			// 
			// txtKamoku01
			// 
			this.txtKamoku01.DataField = "ITEM21";
			this.txtKamoku01.Height = 0.1653543F;
			this.txtKamoku01.Left = 0.02047244F;
			this.txtKamoku01.Name = "txtKamoku01";
			this.txtKamoku01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 128";
			this.txtKamoku01.Text = "21";
			this.txtKamoku01.Top = 0F;
			this.txtKamoku01.Width = 2.836221F;
			// 
			// line2
			// 
			this.line2.Height = 0F;
			this.line2.Left = 2.980232E-08F;
			this.line2.LineWeight = 2F;
			this.line2.Name = "line2";
			this.line2.Top = 0.7511811F;
			this.line2.Width = 7.090549F;
			this.line2.X1 = 2.980232E-08F;
			this.line2.X2 = 7.090549F;
			this.line2.Y1 = 0.7511811F;
			this.line2.Y2 = 0.7511811F;
			// 
			// pageFooter
			// 
			this.pageFooter.Height = 0F;
			this.pageFooter.Name = "pageFooter";
			this.pageFooter.Visible = false;
			// 
			// txtTitle06
			// 
			this.txtTitle06.Height = 0.1795276F;
			this.txtTitle06.Left = 0.003937008F;
			this.txtTitle06.MultiLine = false;
			this.txtTitle06.Name = "txtTitle06";
			this.txtTitle06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom";
			this.txtTitle06.Text = "合          計";
			this.txtTitle06.Top = 0F;
			this.txtTitle06.Width = 2.836221F;
			// 
			// txtKingaku03
			// 
			this.txtKingaku03.DataField = "ITEM23";
			this.txtKingaku03.Height = 0.1795276F;
			this.txtKingaku03.Left = 2.840158F;
			this.txtKingaku03.MultiLine = false;
			this.txtKingaku03.Name = "txtKingaku03";
			this.txtKingaku03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
			this.txtKingaku03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtKingaku03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.txtKingaku03.Text = "23";
			this.txtKingaku03.Top = 0F;
			this.txtKingaku03.Width = 4.230315F;
			// 
			// line9
			// 
			this.line9.Height = 0.1795276F;
			this.line9.Left = 2.840158F;
			this.line9.LineWeight = 2F;
			this.line9.Name = "line9";
			this.line9.Top = 0F;
			this.line9.Width = 0F;
			this.line9.X1 = 2.840158F;
			this.line9.X2 = 2.840158F;
			this.line9.Y1 = 0F;
			this.line9.Y2 = 0.1795276F;
			// 
			// line11
			// 
			this.line11.Height = 0.1795276F;
			this.line11.Left = 7.090551F;
			this.line11.LineWeight = 2F;
			this.line11.Name = "line11";
			this.line11.Top = 0F;
			this.line11.Width = 0F;
			this.line11.X1 = 7.090551F;
			this.line11.X2 = 7.090551F;
			this.line11.Y1 = 0F;
			this.line11.Y2 = 0.1795276F;
			// 
			// line12
			// 
			this.line12.Height = 0F;
			this.line12.Left = 0.02047244F;
			this.line12.LineWeight = 2F;
			this.line12.Name = "line12";
			this.line12.Top = 0F;
			this.line12.Width = 7.090549F;
			this.line12.X1 = 0.02047244F;
			this.line12.X2 = 7.111022F;
			this.line12.Y1 = 0F;
			this.line12.Y2 = 0F;
			// 
			// line14
			// 
			this.line14.Height = 0F;
			this.line14.Left = 0.003937008F;
			this.line14.LineWeight = 2F;
			this.line14.Name = "line14";
			this.line14.Top = 0.1795276F;
			this.line14.Width = 7.090549F;
			this.line14.X1 = 0.003937008F;
			this.line14.X2 = 7.094486F;
			this.line14.Y1 = 0.1795276F;
			this.line14.Y2 = 0.1795276F;
			// 
			// groupHeader1
			// 
			this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line3,
            this.txtTitle04,
            this.txtCompanyName02,
            this.txtHyojiDateFr02,
            this.lblTani,
            this.txtHyojiDateTo02,
            this.txtTitle02,
            this.txtTitle03,
            this.crossSectionLine1,
            this.line1,
            this.line6,
            this.line2});
			this.groupHeader1.Height = 0.75F;
			this.groupHeader1.Name = "groupHeader1";
			// 
			// line3
			// 
			this.line3.Height = 0.1886482F;
			this.line3.Left = 0.003937008F;
			this.line3.LineWeight = 2F;
			this.line3.Name = "line3";
			this.line3.Top = 0.5716536F;
			this.line3.Width = 0F;
			this.line3.X1 = 0.003937008F;
			this.line3.X2 = 0.003937008F;
			this.line3.Y1 = 0.5716536F;
			this.line3.Y2 = 0.7603018F;
			// 
			// txtTitle04
			// 
			this.txtTitle04.DataField = "ITEM10";
			this.txtTitle04.Height = 0.2291339F;
			this.txtTitle04.Left = 0.3956693F;
			this.txtTitle04.Name = "txtTitle04";
			this.txtTitle04.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: middle";
			this.txtTitle04.Text = "10 販売費及び一般管理費";
			this.txtTitle04.Top = 0F;
			this.txtTitle04.Width = 6.299212F;
			// 
			// txtCompanyName02
			// 
			this.txtCompanyName02.DataField = "ITEM05";
			this.txtCompanyName02.Height = 0.1555118F;
			this.txtCompanyName02.Left = 0F;
			this.txtCompanyName02.MultiLine = false;
			this.txtCompanyName02.Name = "txtCompanyName02";
			this.txtCompanyName02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
			this.txtCompanyName02.Text = "5";
			this.txtCompanyName02.Top = 0.2283465F;
			this.txtCompanyName02.Width = 6.694881F;
			// 
			// txtHyojiDateFr02
			// 
			this.txtHyojiDateFr02.DataField = "ITEM02";
			this.txtHyojiDateFr02.Height = 0.1555118F;
			this.txtHyojiDateFr02.Left = 2.010236F;
			this.txtHyojiDateFr02.MultiLine = false;
			this.txtHyojiDateFr02.Name = "txtHyojiDateFr02";
			this.txtHyojiDateFr02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle; ddo-char-set: 1";
			this.txtHyojiDateFr02.Text = "02";
			this.txtHyojiDateFr02.Top = 0.4027559F;
			this.txtHyojiDateFr02.Width = 1.487402F;
			// 
			// lblTani
			// 
			this.lblTani.Height = 0.1555118F;
			this.lblTani.HyperLink = null;
			this.lblTani.Left = 6.092126F;
			this.lblTani.Name = "lblTani";
			this.lblTani.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
			this.lblTani.Text = "(単位：円)";
			this.lblTani.Top = 0.3838583F;
			this.lblTani.Width = 1.002362F;
			// 
			// txtHyojiDateTo02
			// 
			this.txtHyojiDateTo02.DataField = "ITEM03";
			this.txtHyojiDateTo02.Height = 0.1555118F;
			this.txtHyojiDateTo02.Left = 3.585827F;
			this.txtHyojiDateTo02.MultiLine = false;
			this.txtHyojiDateTo02.Name = "txtHyojiDateTo02";
			this.txtHyojiDateTo02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle; ddo-char-set: 1";
			this.txtHyojiDateTo02.Text = "03";
			this.txtHyojiDateTo02.Top = 0.4027559F;
			this.txtHyojiDateTo02.Width = 1.487402F;
			// 
			// txtTitle02
			// 
			this.txtTitle02.Height = 0.1795276F;
			this.txtTitle02.Left = 0F;
			this.txtTitle02.MultiLine = false;
			this.txtTitle02.Name = "txtTitle02";
			this.txtTitle02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
			this.txtTitle02.Text = "科           目";
			this.txtTitle02.Top = 0.5716536F;
			this.txtTitle02.Width = 2.836221F;
			// 
			// txtTitle03
			// 
			this.txtTitle03.Height = 0.1795277F;
			this.txtTitle03.Left = 2.836221F;
			this.txtTitle03.MultiLine = false;
			this.txtTitle03.Name = "txtTitle03";
			this.txtTitle03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
			this.txtTitle03.Text = "金                     額";
			this.txtTitle03.Top = 0.5716536F;
			this.txtTitle03.Width = 4.254332F;
			// 
			// crossSectionLine1
			// 
			this.crossSectionLine1.Bottom = 0.002444342F;
			this.crossSectionLine1.Left = 2.840158F;
			this.crossSectionLine1.LineWeight = 2F;
			this.crossSectionLine1.Name = "crossSectionLine1";
			this.crossSectionLine1.Top = 0.5716536F;
			// 
			// line1
			// 
			this.line1.Height = 0F;
			this.line1.Left = 0F;
			this.line1.LineWeight = 2F;
			this.line1.Name = "line1";
			this.line1.Top = 0.5582678F;
			this.line1.Width = 7.090552F;
			this.line1.X1 = 0F;
			this.line1.X2 = 7.090552F;
			this.line1.Y1 = 0.5582678F;
			this.line1.Y2 = 0.5582678F;
			// 
			// line6
			// 
			this.line6.Height = 0.1818897F;
			this.line6.Left = 7.090551F;
			this.line6.LineWeight = 2F;
			this.line6.Name = "line6";
			this.line6.Top = 0.5716536F;
			this.line6.Width = 0F;
			this.line6.X1 = 7.090551F;
			this.line6.X2 = 7.090551F;
			this.line6.Y1 = 0.5716536F;
			this.line6.Y2 = 0.7535433F;
			// 
			// groupFooter1
			// 
			this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line7,
            this.line8,
            this.txtTitle06,
            this.txtKingaku03,
            this.line9,
            this.line11,
            this.line12,
            this.line14});
			this.groupFooter1.Height = 0.1899443F;
			this.groupFooter1.Name = "groupFooter1";
			this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
			// 
			// line7
			// 
			this.line7.Height = 0.1653543F;
			this.line7.Left = 7.090551F;
			this.line7.LineWeight = 2F;
			this.line7.Name = "line7";
			this.line7.Top = 0F;
			this.line7.Width = 0F;
			this.line7.X1 = 7.090551F;
			this.line7.X2 = 7.090551F;
			this.line7.Y1 = 0F;
			this.line7.Y2 = 0.1653543F;
			// 
			// line8
			// 
			this.line8.Height = 0.1795276F;
			this.line8.Left = 0.003937008F;
			this.line8.LineWeight = 2F;
			this.line8.Name = "line8";
			this.line8.Top = 0F;
			this.line8.Width = 0F;
			this.line8.X1 = 0.003937008F;
			this.line8.X2 = 0.003937008F;
			this.line8.Y1 = 0F;
			this.line8.Y2 = 0.1795276F;
			// 
			// txtTitle05
			// 
			this.txtTitle05.Height = 0.1795276F;
			this.txtTitle05.Left = 0F;
			this.txtTitle05.MultiLine = false;
			this.txtTitle05.Name = "txtTitle05";
			this.txtTitle05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: bottom";
			this.txtTitle05.Text = "費  目  分  類  計";
			this.txtTitle05.Top = 0F;
			this.txtTitle05.Visible = false;
			this.txtTitle05.Width = 2.836221F;
			// 
			// txtKingaku02
			// 
			this.txtKingaku02.DataField = "ITEM23";
			this.txtKingaku02.Height = 0.1795276F;
			this.txtKingaku02.Left = 4.986221F;
			this.txtKingaku02.Name = "txtKingaku02";
			this.txtKingaku02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
			this.txtKingaku02.SummaryGroup = "groupHeader2";
			this.txtKingaku02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtKingaku02.Text = "23";
			this.txtKingaku02.Top = 0F;
			this.txtKingaku02.Width = 2.084252F;
			// 
			// groupHeader2
			// 
			this.groupHeader2.DataField = "ITEM14";
			this.groupHeader2.Height = 0F;
			this.groupHeader2.Name = "groupHeader2";
			// 
			// groupFooter2
			// 
			this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTitle05,
            this.txtKingaku02,
            this.line10,
            this.line13,
            this.line15});
			this.groupFooter2.Height = 0.1748032F;
			this.groupFooter2.Name = "groupFooter2";
			this.groupFooter2.Format += new System.EventHandler(this.groupFooter2_Format);
			// 
			// line10
			// 
			this.line10.Height = 0.1818895F;
			this.line10.Left = 7.090551F;
			this.line10.LineWeight = 2F;
			this.line10.Name = "line10";
			this.line10.Top = 0F;
			this.line10.Width = 0F;
			this.line10.X1 = 7.090551F;
			this.line10.X2 = 7.090551F;
			this.line10.Y1 = 0F;
			this.line10.Y2 = 0.1818895F;
			// 
			// line13
			// 
			this.line13.Height = 0.1818895F;
			this.line13.Left = 0.003937008F;
			this.line13.LineWeight = 2F;
			this.line13.Name = "line13";
			this.line13.Top = 0F;
			this.line13.Width = 0F;
			this.line13.X1 = 0.003937008F;
			this.line13.X2 = 0.003937008F;
			this.line13.Y1 = 0F;
			this.line13.Y2 = 0.1818895F;
			// 
			// line15
			// 
			this.line15.Height = 0F;
			this.line15.Left = 2.840158F;
			this.line15.LineWeight = 2F;
			this.line15.Name = "line15";
			this.line15.Top = 0.1795276F;
			this.line15.Width = 4.250391F;
			this.line15.X1 = 2.840158F;
			this.line15.X2 = 7.090549F;
			this.line15.Y1 = 0.1795276F;
			this.line15.Y2 = 0.1795276F;
			// 
			// reportHeader1
			// 
			this.reportHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.txtCompanyName001,
            this.txtJusho02,
            this.txtJusho01,
            this.lblTitle01,
            this.txtHyojiDateTo01,
            this.txtHyojiDateFr01,
            this.line16,
            this.shape1,
            this.shape2});
			this.reportHeader1.Height = 10.31102F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// label1
			// 
			this.label1.Height = 0.3937007F;
			this.label1.HyperLink = null;
			this.label1.Left = 1.577165F;
			this.label1.Name = "label1";
			this.label1.Style = "font-family: ＭＳ 明朝; font-size: 20.25pt; font-style: normal; font-weight: bold; te" +
    "xt-align: center; text-decoration: none; vertical-align: middle";
			this.label1.Text = "決 算 報 告 書";
			this.label1.Top = 2.734253F;
			this.label1.Width = 3.937008F;
			// 
			// txtCompanyName001
			// 
			this.txtCompanyName001.DataField = "ITEM05";
			this.txtCompanyName001.Height = 0.1968504F;
			this.txtCompanyName001.Left = 1.858662F;
			this.txtCompanyName001.MultiLine = false;
			this.txtCompanyName001.Name = "txtCompanyName001";
			this.txtCompanyName001.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtCompanyName001.Text = "5";
			this.txtCompanyName001.Top = 7.208265F;
			this.txtCompanyName001.Width = 3.346457F;
			// 
			// txtJusho02
			// 
			this.txtJusho02.DataField = "ITEM07";
			this.txtJusho02.Height = 0.1968504F;
			this.txtJusho02.Left = 1.701575F;
			this.txtJusho02.Name = "txtJusho02";
			this.txtJusho02.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center";
			this.txtJusho02.Text = "7";
			this.txtJusho02.Top = 7.665355F;
			this.txtJusho02.Width = 3.503544F;
			// 
			// txtJusho01
			// 
			this.txtJusho01.DataField = "ITEM06";
			this.txtJusho01.Height = 0.1968504F;
			this.txtJusho01.Left = 1.701575F;
			this.txtJusho01.MultiLine = false;
			this.txtJusho01.Name = "txtJusho01";
			this.txtJusho01.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center";
			this.txtJusho01.Text = "6";
			this.txtJusho01.Top = 7.468508F;
			this.txtJusho01.Width = 3.503544F;
			// 
			// lblTitle01
			// 
			this.lblTitle01.DataField = "ITEM01";
			this.lblTitle01.Height = 0.2700787F;
			this.lblTitle01.Left = 2.442914F;
			this.lblTitle01.MultiLine = false;
			this.lblTitle01.Name = "lblTitle01";
			this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 18pt; text-align: center; vertical-align: middle";
			this.lblTitle01.Text = "第  26  期";
			this.lblTitle01.Top = 3.775985F;
			this.lblTitle01.Width = 2.399606F;
			// 
			// txtHyojiDateTo01
			// 
			this.txtHyojiDateTo01.DataField = "ITEM03";
			this.txtHyojiDateTo01.Height = 0.1811024F;
			this.txtHyojiDateTo01.Left = 3.652755F;
			this.txtHyojiDateTo01.Name = "txtHyojiDateTo01";
			this.txtHyojiDateTo01.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-align: center; ver" +
    "tical-align: bottom";
			this.txtHyojiDateTo01.Text = "3";
			this.txtHyojiDateTo01.Top = 4.385434F;
			this.txtHyojiDateTo01.Width = 2.050787F;
			// 
			// txtHyojiDateFr01
			// 
			this.txtHyojiDateFr01.DataField = "ITEM02";
			this.txtHyojiDateFr01.Height = 0.1811024F;
			this.txtHyojiDateFr01.Left = 1.701575F;
			this.txtHyojiDateFr01.MultiLine = false;
			this.txtHyojiDateFr01.Name = "txtHyojiDateFr01";
			this.txtHyojiDateFr01.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center; vertical-align: middle";
			this.txtHyojiDateFr01.Text = "02";
			this.txtHyojiDateFr01.Top = 4.385433F;
			this.txtHyojiDateFr01.Width = 1.951181F;
			// 
			// line16
			// 
			this.line16.Height = 0F;
			this.line16.Left = 1.85118F;
			this.line16.LineWeight = 1F;
			this.line16.Name = "line16";
			this.line16.Top = 7.40512F;
			this.line16.Width = 3.353939F;
			this.line16.X1 = 1.85118F;
			this.line16.X2 = 5.205119F;
			this.line16.Y1 = 7.40512F;
			this.line16.Y2 = 7.40512F;
			// 
			// shape1
			// 
			this.shape1.Height = 0.708268F;
			this.shape1.Left = 1.472441F;
			this.shape1.Name = "shape1";
			this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
			this.shape1.Top = 2.55748F;
			this.shape1.Width = 4.161811F;
			// 
			// shape2
			// 
			this.shape2.Height = 0.768504F;
			this.shape2.Left = 1.441339F;
			this.shape2.Name = "shape2";
			this.shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
			this.shape2.Top = 2.528347F;
			this.shape2.Width = 4.233465F;
			// 
			// reportFooter1
			// 
			this.reportFooter1.Height = 0F;
			this.reportFooter1.Name = "reportFooter1";
			// 
			// ZMYR10114R
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.7874016F;
			this.PageSettings.Margins.Left = 0.5905512F;
			this.PageSettings.Margins.Right = 0.5905512F;
			this.PageSettings.Margins.Top = 0.5905512F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.090551F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.groupHeader1);
			this.Sections.Add(this.groupHeader2);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.groupFooter2);
			this.Sections.Add(this.groupFooter1);
			this.Sections.Add(this.pageFooter);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtKingaku01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKamoku01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKingaku03)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompanyName02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateFr02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTani)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateTo02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKingaku02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCompanyName001)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJusho02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJusho01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateTo01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateFr01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDateFr02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTani;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDateTo02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKamoku01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKingaku01;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKingaku03;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKingaku02;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName001;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJusho02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJusho01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDateTo01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDateFr01;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        public GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
    }
}
