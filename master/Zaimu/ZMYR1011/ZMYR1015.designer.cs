﻿namespace jp.co.fsi.zm.zmyr1011
{
    partial class ZMYR1015
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txtKingaku3 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKingaku2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku10 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku9 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku8 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku7 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku6 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku5 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku4 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku3 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNiniTmttknTorikuzushigaku1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKamokuBunruiNm30 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKamokuBunruiNm20 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_10 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_9 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_8 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_7 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_6 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_5 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_4 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_3 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90020_1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKingaku1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKamokuBunruiNm10 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKingaku5 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKingaku4 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku10 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku9 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku8 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku7 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku6 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku5 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku4 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku3 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtRiekikinShobungaku1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKamokuBunruiNm50 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKamokuBunruiNm40 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_10 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_9 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_8 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_7 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_6 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_5 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_4 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_3 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKanjoKamokuNm90040_1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel16 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel36 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel26 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel15 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel35 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel25 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel14 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel34 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel24 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel13 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel33 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel23 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel32 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel22 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel31 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel21 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel30 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel20 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel29 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel19 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel28 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel18 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel17 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel3 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel37 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel39 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel40 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel41 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel38 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel43 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel42 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel4 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel52 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel61 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel70 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel51 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel60 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel69 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel50 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel59 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel68 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel49 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel58 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel67 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel48 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel57 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel66 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel47 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel56 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel65 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel46 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel55 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel64 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel45 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel54 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel63 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel44 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel53 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel62 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel27 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel5 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel72 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel74 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel77 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel71 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel73 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel76 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel75 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiTableLayoutPanel2.SuspendLayout();
			this.fsiPanel16.SuspendLayout();
			this.fsiPanel36.SuspendLayout();
			this.fsiPanel26.SuspendLayout();
			this.fsiPanel15.SuspendLayout();
			this.fsiPanel35.SuspendLayout();
			this.fsiPanel25.SuspendLayout();
			this.fsiPanel14.SuspendLayout();
			this.fsiPanel34.SuspendLayout();
			this.fsiPanel24.SuspendLayout();
			this.fsiPanel13.SuspendLayout();
			this.fsiPanel33.SuspendLayout();
			this.fsiPanel23.SuspendLayout();
			this.fsiPanel12.SuspendLayout();
			this.fsiPanel32.SuspendLayout();
			this.fsiPanel22.SuspendLayout();
			this.fsiPanel11.SuspendLayout();
			this.fsiPanel31.SuspendLayout();
			this.fsiPanel21.SuspendLayout();
			this.fsiPanel10.SuspendLayout();
			this.fsiPanel30.SuspendLayout();
			this.fsiPanel20.SuspendLayout();
			this.fsiPanel9.SuspendLayout();
			this.fsiPanel29.SuspendLayout();
			this.fsiPanel19.SuspendLayout();
			this.fsiPanel8.SuspendLayout();
			this.fsiPanel28.SuspendLayout();
			this.fsiPanel18.SuspendLayout();
			this.fsiPanel7.SuspendLayout();
			this.fsiPanel17.SuspendLayout();
			this.fsiTableLayoutPanel3.SuspendLayout();
			this.fsiPanel37.SuspendLayout();
			this.fsiPanel39.SuspendLayout();
			this.fsiPanel40.SuspendLayout();
			this.fsiPanel41.SuspendLayout();
			this.fsiPanel38.SuspendLayout();
			this.fsiPanel43.SuspendLayout();
			this.fsiPanel42.SuspendLayout();
			this.fsiTableLayoutPanel4.SuspendLayout();
			this.fsiPanel52.SuspendLayout();
			this.fsiPanel61.SuspendLayout();
			this.fsiPanel70.SuspendLayout();
			this.fsiPanel51.SuspendLayout();
			this.fsiPanel60.SuspendLayout();
			this.fsiPanel69.SuspendLayout();
			this.fsiPanel50.SuspendLayout();
			this.fsiPanel59.SuspendLayout();
			this.fsiPanel68.SuspendLayout();
			this.fsiPanel49.SuspendLayout();
			this.fsiPanel58.SuspendLayout();
			this.fsiPanel67.SuspendLayout();
			this.fsiPanel48.SuspendLayout();
			this.fsiPanel57.SuspendLayout();
			this.fsiPanel66.SuspendLayout();
			this.fsiPanel47.SuspendLayout();
			this.fsiPanel56.SuspendLayout();
			this.fsiPanel65.SuspendLayout();
			this.fsiPanel46.SuspendLayout();
			this.fsiPanel55.SuspendLayout();
			this.fsiPanel64.SuspendLayout();
			this.fsiPanel45.SuspendLayout();
			this.fsiPanel54.SuspendLayout();
			this.fsiPanel63.SuspendLayout();
			this.fsiPanel44.SuspendLayout();
			this.fsiPanel53.SuspendLayout();
			this.fsiPanel62.SuspendLayout();
			this.fsiPanel27.SuspendLayout();
			this.fsiTableLayoutPanel5.SuspendLayout();
			this.fsiPanel72.SuspendLayout();
			this.fsiPanel74.SuspendLayout();
			this.fsiPanel77.SuspendLayout();
			this.fsiPanel71.SuspendLayout();
			this.fsiPanel73.SuspendLayout();
			this.fsiPanel76.SuspendLayout();
			this.fsiPanel75.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 827);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1147, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.BackColor = System.Drawing.Color.White;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1136, 31);
			this.lblTitle.Text = "";
			// 
			// txtKingaku3
			// 
			this.txtKingaku3.AutoSizeFromLength = false;
			this.txtKingaku3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.txtKingaku3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtKingaku3.DisplayLength = null;
			this.txtKingaku3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKingaku3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKingaku3.ForeColor = System.Drawing.Color.Black;
			this.txtKingaku3.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKingaku3.Location = new System.Drawing.Point(0, 0);
			this.txtKingaku3.Margin = new System.Windows.Forms.Padding(4);
			this.txtKingaku3.MaxLength = 128;
			this.txtKingaku3.Name = "txtKingaku3";
			this.txtKingaku3.ReadOnly = true;
			this.txtKingaku3.Size = new System.Drawing.Size(469, 23);
			this.txtKingaku3.TabIndex = 23;
			this.txtKingaku3.TabStop = false;
			this.txtKingaku3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtKingaku2
			// 
			this.txtKingaku2.AutoSizeFromLength = false;
			this.txtKingaku2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.txtKingaku2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtKingaku2.DisplayLength = null;
			this.txtKingaku2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKingaku2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKingaku2.ForeColor = System.Drawing.Color.Black;
			this.txtKingaku2.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKingaku2.Location = new System.Drawing.Point(0, 0);
			this.txtKingaku2.Margin = new System.Windows.Forms.Padding(4);
			this.txtKingaku2.MaxLength = 128;
			this.txtKingaku2.Name = "txtKingaku2";
			this.txtKingaku2.ReadOnly = true;
			this.txtKingaku2.Size = new System.Drawing.Size(145, 23);
			this.txtKingaku2.TabIndex = 22;
			this.txtKingaku2.TabStop = false;
			this.txtKingaku2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtNiniTmttknTorikuzushigaku10
			// 
			this.txtNiniTmttknTorikuzushigaku10.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku10.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku10.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku10.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku10.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku10.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku10.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku10.Name = "txtNiniTmttknTorikuzushigaku10";
			this.txtNiniTmttknTorikuzushigaku10.Size = new System.Drawing.Size(329, 23);
			this.txtNiniTmttknTorikuzushigaku10.TabIndex = 20;
			this.txtNiniTmttknTorikuzushigaku10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku10.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku10.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtNiniTmttknTorikuzushigaku9
			// 
			this.txtNiniTmttknTorikuzushigaku9.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku9.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku9.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku9.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku9.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku9.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku9.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku9.Name = "txtNiniTmttknTorikuzushigaku9";
			this.txtNiniTmttknTorikuzushigaku9.Size = new System.Drawing.Size(474, 23);
			this.txtNiniTmttknTorikuzushigaku9.TabIndex = 18;
			this.txtNiniTmttknTorikuzushigaku9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku9.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku9.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtNiniTmttknTorikuzushigaku8
			// 
			this.txtNiniTmttknTorikuzushigaku8.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku8.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku8.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku8.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku8.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku8.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku8.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku8.Name = "txtNiniTmttknTorikuzushigaku8";
			this.txtNiniTmttknTorikuzushigaku8.Size = new System.Drawing.Size(474, 23);
			this.txtNiniTmttknTorikuzushigaku8.TabIndex = 16;
			this.txtNiniTmttknTorikuzushigaku8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku8.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku8.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtNiniTmttknTorikuzushigaku7
			// 
			this.txtNiniTmttknTorikuzushigaku7.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku7.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku7.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku7.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku7.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku7.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku7.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku7.Name = "txtNiniTmttknTorikuzushigaku7";
			this.txtNiniTmttknTorikuzushigaku7.Size = new System.Drawing.Size(474, 23);
			this.txtNiniTmttknTorikuzushigaku7.TabIndex = 14;
			this.txtNiniTmttknTorikuzushigaku7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku7.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku7.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtNiniTmttknTorikuzushigaku6
			// 
			this.txtNiniTmttknTorikuzushigaku6.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku6.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku6.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku6.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku6.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku6.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku6.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku6.Name = "txtNiniTmttknTorikuzushigaku6";
			this.txtNiniTmttknTorikuzushigaku6.Size = new System.Drawing.Size(474, 23);
			this.txtNiniTmttknTorikuzushigaku6.TabIndex = 12;
			this.txtNiniTmttknTorikuzushigaku6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku6.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku6.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtNiniTmttknTorikuzushigaku5
			// 
			this.txtNiniTmttknTorikuzushigaku5.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku5.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku5.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku5.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku5.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku5.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku5.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku5.Name = "txtNiniTmttknTorikuzushigaku5";
			this.txtNiniTmttknTorikuzushigaku5.Size = new System.Drawing.Size(474, 23);
			this.txtNiniTmttknTorikuzushigaku5.TabIndex = 10;
			this.txtNiniTmttknTorikuzushigaku5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku5.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku5.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtNiniTmttknTorikuzushigaku4
			// 
			this.txtNiniTmttknTorikuzushigaku4.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku4.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku4.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku4.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku4.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku4.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku4.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku4.Name = "txtNiniTmttknTorikuzushigaku4";
			this.txtNiniTmttknTorikuzushigaku4.Size = new System.Drawing.Size(474, 23);
			this.txtNiniTmttknTorikuzushigaku4.TabIndex = 8;
			this.txtNiniTmttknTorikuzushigaku4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku4.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku4.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtNiniTmttknTorikuzushigaku3
			// 
			this.txtNiniTmttknTorikuzushigaku3.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku3.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku3.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku3.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku3.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku3.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku3.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku3.Name = "txtNiniTmttknTorikuzushigaku3";
			this.txtNiniTmttknTorikuzushigaku3.Size = new System.Drawing.Size(474, 23);
			this.txtNiniTmttknTorikuzushigaku3.TabIndex = 6;
			this.txtNiniTmttknTorikuzushigaku3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku3.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku3.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtNiniTmttknTorikuzushigaku2
			// 
			this.txtNiniTmttknTorikuzushigaku2.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku2.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku2.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku2.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku2.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku2.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku2.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku2.Name = "txtNiniTmttknTorikuzushigaku2";
			this.txtNiniTmttknTorikuzushigaku2.Size = new System.Drawing.Size(474, 23);
			this.txtNiniTmttknTorikuzushigaku2.TabIndex = 4;
			this.txtNiniTmttknTorikuzushigaku2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku2.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtNiniTmttknTorikuzushigaku1
			// 
			this.txtNiniTmttknTorikuzushigaku1.AutoSizeFromLength = false;
			this.txtNiniTmttknTorikuzushigaku1.DisplayLength = null;
			this.txtNiniTmttknTorikuzushigaku1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtNiniTmttknTorikuzushigaku1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiniTmttknTorikuzushigaku1.ForeColor = System.Drawing.Color.Black;
			this.txtNiniTmttknTorikuzushigaku1.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtNiniTmttknTorikuzushigaku1.Location = new System.Drawing.Point(0, 0);
			this.txtNiniTmttknTorikuzushigaku1.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiniTmttknTorikuzushigaku1.MaxLength = 15;
			this.txtNiniTmttknTorikuzushigaku1.Name = "txtNiniTmttknTorikuzushigaku1";
			this.txtNiniTmttknTorikuzushigaku1.Size = new System.Drawing.Size(474, 23);
			this.txtNiniTmttknTorikuzushigaku1.TabIndex = 2;
			this.txtNiniTmttknTorikuzushigaku1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiniTmttknTorikuzushigaku1.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtNiniTmttknTorikuzushigaku1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtKamokuBunruiNm30
			// 
			this.txtKamokuBunruiNm30.AutoSizeFromLength = false;
			this.txtKamokuBunruiNm30.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.txtKamokuBunruiNm30.DisplayLength = null;
			this.txtKamokuBunruiNm30.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKamokuBunruiNm30.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKamokuBunruiNm30.ForeColor = System.Drawing.Color.Black;
			this.txtKamokuBunruiNm30.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKamokuBunruiNm30.Location = new System.Drawing.Point(0, 0);
			this.txtKamokuBunruiNm30.Margin = new System.Windows.Forms.Padding(4);
			this.txtKamokuBunruiNm30.MaxLength = 30;
			this.txtKamokuBunruiNm30.Name = "txtKamokuBunruiNm30";
			this.txtKamokuBunruiNm30.Size = new System.Drawing.Size(315, 23);
			this.txtKamokuBunruiNm30.TabIndex = 21;
			this.txtKamokuBunruiNm30.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKamokuBunruiNm20
			// 
			this.txtKamokuBunruiNm20.AutoSizeFromLength = false;
			this.txtKamokuBunruiNm20.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.txtKamokuBunruiNm20.DisplayLength = null;
			this.txtKamokuBunruiNm20.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKamokuBunruiNm20.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKamokuBunruiNm20.ForeColor = System.Drawing.Color.Black;
			this.txtKamokuBunruiNm20.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKamokuBunruiNm20.Location = new System.Drawing.Point(0, 0);
			this.txtKamokuBunruiNm20.Margin = new System.Windows.Forms.Padding(4);
			this.txtKamokuBunruiNm20.MaxLength = 30;
			this.txtKamokuBunruiNm20.Name = "txtKamokuBunruiNm20";
			this.txtKamokuBunruiNm20.Size = new System.Drawing.Size(785, 23);
			this.txtKamokuBunruiNm20.TabIndex = 0;
			this.txtKamokuBunruiNm20.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_10
			// 
			this.txtKanjoKamokuNm90020_10.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_10.DisplayLength = null;
			this.txtKanjoKamokuNm90020_10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_10.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_10.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_10.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_10.MaxLength = 30;
			this.txtKanjoKamokuNm90020_10.Name = "txtKanjoKamokuNm90020_10";
			this.txtKanjoKamokuNm90020_10.Size = new System.Drawing.Size(315, 23);
			this.txtKanjoKamokuNm90020_10.TabIndex = 19;
			this.txtKanjoKamokuNm90020_10.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_9
			// 
			this.txtKanjoKamokuNm90020_9.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_9.DisplayLength = null;
			this.txtKanjoKamokuNm90020_9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_9.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_9.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_9.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_9.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_9.MaxLength = 30;
			this.txtKanjoKamokuNm90020_9.Name = "txtKanjoKamokuNm90020_9";
			this.txtKanjoKamokuNm90020_9.Size = new System.Drawing.Size(311, 23);
			this.txtKanjoKamokuNm90020_9.TabIndex = 17;
			this.txtKanjoKamokuNm90020_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_8
			// 
			this.txtKanjoKamokuNm90020_8.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_8.DisplayLength = null;
			this.txtKanjoKamokuNm90020_8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_8.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_8.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_8.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_8.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_8.MaxLength = 30;
			this.txtKanjoKamokuNm90020_8.Name = "txtKanjoKamokuNm90020_8";
			this.txtKanjoKamokuNm90020_8.Size = new System.Drawing.Size(311, 23);
			this.txtKanjoKamokuNm90020_8.TabIndex = 15;
			this.txtKanjoKamokuNm90020_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_7
			// 
			this.txtKanjoKamokuNm90020_7.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_7.DisplayLength = null;
			this.txtKanjoKamokuNm90020_7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_7.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_7.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_7.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_7.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_7.MaxLength = 30;
			this.txtKanjoKamokuNm90020_7.Name = "txtKanjoKamokuNm90020_7";
			this.txtKanjoKamokuNm90020_7.Size = new System.Drawing.Size(311, 23);
			this.txtKanjoKamokuNm90020_7.TabIndex = 13;
			this.txtKanjoKamokuNm90020_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_6
			// 
			this.txtKanjoKamokuNm90020_6.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_6.DisplayLength = null;
			this.txtKanjoKamokuNm90020_6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_6.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_6.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_6.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_6.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_6.MaxLength = 30;
			this.txtKanjoKamokuNm90020_6.Name = "txtKanjoKamokuNm90020_6";
			this.txtKanjoKamokuNm90020_6.Size = new System.Drawing.Size(311, 23);
			this.txtKanjoKamokuNm90020_6.TabIndex = 11;
			this.txtKanjoKamokuNm90020_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_5
			// 
			this.txtKanjoKamokuNm90020_5.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_5.DisplayLength = null;
			this.txtKanjoKamokuNm90020_5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_5.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_5.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_5.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_5.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_5.MaxLength = 30;
			this.txtKanjoKamokuNm90020_5.Name = "txtKanjoKamokuNm90020_5";
			this.txtKanjoKamokuNm90020_5.Size = new System.Drawing.Size(311, 23);
			this.txtKanjoKamokuNm90020_5.TabIndex = 9;
			this.txtKanjoKamokuNm90020_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_4
			// 
			this.txtKanjoKamokuNm90020_4.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_4.DisplayLength = null;
			this.txtKanjoKamokuNm90020_4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_4.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_4.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_4.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_4.MaxLength = 30;
			this.txtKanjoKamokuNm90020_4.Name = "txtKanjoKamokuNm90020_4";
			this.txtKanjoKamokuNm90020_4.Size = new System.Drawing.Size(311, 23);
			this.txtKanjoKamokuNm90020_4.TabIndex = 7;
			this.txtKanjoKamokuNm90020_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_3
			// 
			this.txtKanjoKamokuNm90020_3.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_3.DisplayLength = null;
			this.txtKanjoKamokuNm90020_3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_3.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_3.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_3.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_3.MaxLength = 30;
			this.txtKanjoKamokuNm90020_3.Name = "txtKanjoKamokuNm90020_3";
			this.txtKanjoKamokuNm90020_3.Size = new System.Drawing.Size(311, 23);
			this.txtKanjoKamokuNm90020_3.TabIndex = 5;
			this.txtKanjoKamokuNm90020_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_2
			// 
			this.txtKanjoKamokuNm90020_2.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_2.DisplayLength = null;
			this.txtKanjoKamokuNm90020_2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_2.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_2.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_2.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_2.MaxLength = 30;
			this.txtKanjoKamokuNm90020_2.Name = "txtKanjoKamokuNm90020_2";
			this.txtKanjoKamokuNm90020_2.Size = new System.Drawing.Size(311, 23);
			this.txtKanjoKamokuNm90020_2.TabIndex = 3;
			this.txtKanjoKamokuNm90020_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90020_1
			// 
			this.txtKanjoKamokuNm90020_1.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90020_1.DisplayLength = null;
			this.txtKanjoKamokuNm90020_1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90020_1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90020_1.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90020_1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90020_1.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90020_1.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90020_1.MaxLength = 30;
			this.txtKanjoKamokuNm90020_1.Name = "txtKanjoKamokuNm90020_1";
			this.txtKanjoKamokuNm90020_1.Size = new System.Drawing.Size(311, 23);
			this.txtKanjoKamokuNm90020_1.TabIndex = 1;
			this.txtKanjoKamokuNm90020_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKingaku1
			// 
			this.txtKingaku1.AutoSizeFromLength = false;
			this.txtKingaku1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.txtKingaku1.DisplayLength = null;
			this.txtKingaku1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKingaku1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKingaku1.ForeColor = System.Drawing.Color.Black;
			this.txtKingaku1.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKingaku1.Location = new System.Drawing.Point(0, 0);
			this.txtKingaku1.Margin = new System.Windows.Forms.Padding(4);
			this.txtKingaku1.MaxLength = 128;
			this.txtKingaku1.Name = "txtKingaku1";
			this.txtKingaku1.Size = new System.Drawing.Size(455, 23);
			this.txtKingaku1.TabIndex = 1;
			this.txtKingaku1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKingaku1.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtKingaku1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtKamokuBunruiNm10
			// 
			this.txtKamokuBunruiNm10.AutoSizeFromLength = false;
			this.txtKamokuBunruiNm10.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.txtKamokuBunruiNm10.DisplayLength = null;
			this.txtKamokuBunruiNm10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKamokuBunruiNm10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKamokuBunruiNm10.ForeColor = System.Drawing.Color.Black;
			this.txtKamokuBunruiNm10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKamokuBunruiNm10.Location = new System.Drawing.Point(0, 0);
			this.txtKamokuBunruiNm10.Margin = new System.Windows.Forms.Padding(4);
			this.txtKamokuBunruiNm10.MaxLength = 30;
			this.txtKamokuBunruiNm10.Name = "txtKamokuBunruiNm10";
			this.txtKamokuBunruiNm10.Size = new System.Drawing.Size(330, 23);
			this.txtKamokuBunruiNm10.TabIndex = 0;
			this.txtKamokuBunruiNm10.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKingaku5
			// 
			this.txtKingaku5.AutoSizeFromLength = false;
			this.txtKingaku5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.txtKingaku5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtKingaku5.DisplayLength = null;
			this.txtKingaku5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKingaku5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKingaku5.ForeColor = System.Drawing.Color.Black;
			this.txtKingaku5.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKingaku5.Location = new System.Drawing.Point(0, 0);
			this.txtKingaku5.Margin = new System.Windows.Forms.Padding(4);
			this.txtKingaku5.MaxLength = 128;
			this.txtKingaku5.Name = "txtKingaku5";
			this.txtKingaku5.ReadOnly = true;
			this.txtKingaku5.Size = new System.Drawing.Size(223, 23);
			this.txtKingaku5.TabIndex = 23;
			this.txtKingaku5.TabStop = false;
			this.txtKingaku5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtKingaku4
			// 
			this.txtKingaku4.AutoSizeFromLength = false;
			this.txtKingaku4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.txtKingaku4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtKingaku4.DisplayLength = null;
			this.txtKingaku4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKingaku4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKingaku4.ForeColor = System.Drawing.Color.Black;
			this.txtKingaku4.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKingaku4.Location = new System.Drawing.Point(0, 0);
			this.txtKingaku4.Margin = new System.Windows.Forms.Padding(4);
			this.txtKingaku4.MaxLength = 128;
			this.txtKingaku4.Name = "txtKingaku4";
			this.txtKingaku4.ReadOnly = true;
			this.txtKingaku4.Size = new System.Drawing.Size(223, 23);
			this.txtKingaku4.TabIndex = 22;
			this.txtKingaku4.TabStop = false;
			this.txtKingaku4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtRiekikinShobungaku10
			// 
			this.txtRiekikinShobungaku10.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku10.DisplayLength = null;
			this.txtRiekikinShobungaku10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku10.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku10.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku10.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku10.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku10.MaxLength = 15;
			this.txtRiekikinShobungaku10.Name = "txtRiekikinShobungaku10";
			this.txtRiekikinShobungaku10.Size = new System.Drawing.Size(269, 23);
			this.txtRiekikinShobungaku10.TabIndex = 20;
			this.txtRiekikinShobungaku10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku10.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku10.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtRiekikinShobungaku9
			// 
			this.txtRiekikinShobungaku9.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku9.DisplayLength = null;
			this.txtRiekikinShobungaku9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku9.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku9.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku9.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku9.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku9.MaxLength = 15;
			this.txtRiekikinShobungaku9.Name = "txtRiekikinShobungaku9";
			this.txtRiekikinShobungaku9.Size = new System.Drawing.Size(137, 23);
			this.txtRiekikinShobungaku9.TabIndex = 18;
			this.txtRiekikinShobungaku9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku9.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku9.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtRiekikinShobungaku8
			// 
			this.txtRiekikinShobungaku8.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku8.DisplayLength = null;
			this.txtRiekikinShobungaku8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku8.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku8.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku8.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku8.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku8.MaxLength = 15;
			this.txtRiekikinShobungaku8.Name = "txtRiekikinShobungaku8";
			this.txtRiekikinShobungaku8.Size = new System.Drawing.Size(137, 23);
			this.txtRiekikinShobungaku8.TabIndex = 16;
			this.txtRiekikinShobungaku8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku8.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku8.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtRiekikinShobungaku7
			// 
			this.txtRiekikinShobungaku7.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku7.DisplayLength = null;
			this.txtRiekikinShobungaku7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku7.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku7.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku7.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku7.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku7.MaxLength = 15;
			this.txtRiekikinShobungaku7.Name = "txtRiekikinShobungaku7";
			this.txtRiekikinShobungaku7.Size = new System.Drawing.Size(137, 23);
			this.txtRiekikinShobungaku7.TabIndex = 14;
			this.txtRiekikinShobungaku7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku7.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku7.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtRiekikinShobungaku6
			// 
			this.txtRiekikinShobungaku6.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku6.DisplayLength = null;
			this.txtRiekikinShobungaku6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku6.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku6.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku6.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku6.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku6.MaxLength = 15;
			this.txtRiekikinShobungaku6.Name = "txtRiekikinShobungaku6";
			this.txtRiekikinShobungaku6.Size = new System.Drawing.Size(137, 23);
			this.txtRiekikinShobungaku6.TabIndex = 12;
			this.txtRiekikinShobungaku6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku6.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku6.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtRiekikinShobungaku5
			// 
			this.txtRiekikinShobungaku5.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku5.DisplayLength = null;
			this.txtRiekikinShobungaku5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku5.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku5.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku5.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku5.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku5.MaxLength = 15;
			this.txtRiekikinShobungaku5.Name = "txtRiekikinShobungaku5";
			this.txtRiekikinShobungaku5.Size = new System.Drawing.Size(137, 23);
			this.txtRiekikinShobungaku5.TabIndex = 10;
			this.txtRiekikinShobungaku5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku5.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku5.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtRiekikinShobungaku4
			// 
			this.txtRiekikinShobungaku4.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku4.DisplayLength = null;
			this.txtRiekikinShobungaku4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku4.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku4.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku4.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku4.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku4.MaxLength = 15;
			this.txtRiekikinShobungaku4.Name = "txtRiekikinShobungaku4";
			this.txtRiekikinShobungaku4.Size = new System.Drawing.Size(137, 23);
			this.txtRiekikinShobungaku4.TabIndex = 8;
			this.txtRiekikinShobungaku4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku4.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku4.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtRiekikinShobungaku3
			// 
			this.txtRiekikinShobungaku3.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku3.DisplayLength = null;
			this.txtRiekikinShobungaku3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku3.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku3.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku3.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku3.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku3.MaxLength = 15;
			this.txtRiekikinShobungaku3.Name = "txtRiekikinShobungaku3";
			this.txtRiekikinShobungaku3.Size = new System.Drawing.Size(137, 23);
			this.txtRiekikinShobungaku3.TabIndex = 6;
			this.txtRiekikinShobungaku3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku3.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku3.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtRiekikinShobungaku2
			// 
			this.txtRiekikinShobungaku2.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku2.DisplayLength = null;
			this.txtRiekikinShobungaku2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku2.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku2.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku2.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku2.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku2.MaxLength = 15;
			this.txtRiekikinShobungaku2.Name = "txtRiekikinShobungaku2";
			this.txtRiekikinShobungaku2.Size = new System.Drawing.Size(137, 23);
			this.txtRiekikinShobungaku2.TabIndex = 4;
			this.txtRiekikinShobungaku2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku2.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtRiekikinShobungaku1
			// 
			this.txtRiekikinShobungaku1.AutoSizeFromLength = false;
			this.txtRiekikinShobungaku1.DisplayLength = null;
			this.txtRiekikinShobungaku1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtRiekikinShobungaku1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtRiekikinShobungaku1.ForeColor = System.Drawing.Color.Black;
			this.txtRiekikinShobungaku1.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtRiekikinShobungaku1.Location = new System.Drawing.Point(0, 0);
			this.txtRiekikinShobungaku1.Margin = new System.Windows.Forms.Padding(4);
			this.txtRiekikinShobungaku1.MaxLength = 15;
			this.txtRiekikinShobungaku1.Name = "txtRiekikinShobungaku1";
			this.txtRiekikinShobungaku1.Size = new System.Drawing.Size(137, 23);
			this.txtRiekikinShobungaku1.TabIndex = 2;
			this.txtRiekikinShobungaku1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtRiekikinShobungaku1.Enter += new System.EventHandler(this.txtKingaku_Enter);
			this.txtRiekikinShobungaku1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
			// 
			// txtKamokuBunruiNm50
			// 
			this.txtKamokuBunruiNm50.AutoSizeFromLength = false;
			this.txtKamokuBunruiNm50.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.txtKamokuBunruiNm50.DisplayLength = null;
			this.txtKamokuBunruiNm50.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKamokuBunruiNm50.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKamokuBunruiNm50.ForeColor = System.Drawing.Color.Black;
			this.txtKamokuBunruiNm50.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKamokuBunruiNm50.Location = new System.Drawing.Point(0, 0);
			this.txtKamokuBunruiNm50.Margin = new System.Windows.Forms.Padding(4);
			this.txtKamokuBunruiNm50.MaxLength = 30;
			this.txtKamokuBunruiNm50.Name = "txtKamokuBunruiNm50";
			this.txtKamokuBunruiNm50.Size = new System.Drawing.Size(561, 23);
			this.txtKamokuBunruiNm50.TabIndex = 21;
			this.txtKamokuBunruiNm50.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKamokuBunruiNm40
			// 
			this.txtKamokuBunruiNm40.AutoSizeFromLength = false;
			this.txtKamokuBunruiNm40.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.txtKamokuBunruiNm40.DisplayLength = null;
			this.txtKamokuBunruiNm40.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKamokuBunruiNm40.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKamokuBunruiNm40.ForeColor = System.Drawing.Color.Black;
			this.txtKamokuBunruiNm40.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKamokuBunruiNm40.Location = new System.Drawing.Point(0, 0);
			this.txtKamokuBunruiNm40.Margin = new System.Windows.Forms.Padding(4);
			this.txtKamokuBunruiNm40.MaxLength = 30;
			this.txtKamokuBunruiNm40.Name = "txtKamokuBunruiNm40";
			this.txtKamokuBunruiNm40.Size = new System.Drawing.Size(784, 23);
			this.txtKamokuBunruiNm40.TabIndex = 0;
			this.txtKamokuBunruiNm40.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_10
			// 
			this.txtKanjoKamokuNm90040_10.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_10.DisplayLength = null;
			this.txtKanjoKamokuNm90040_10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_10.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_10.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_10.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_10.MaxLength = 30;
			this.txtKanjoKamokuNm90040_10.Name = "txtKanjoKamokuNm90040_10";
			this.txtKanjoKamokuNm90040_10.Size = new System.Drawing.Size(292, 23);
			this.txtKanjoKamokuNm90040_10.TabIndex = 19;
			this.txtKanjoKamokuNm90040_10.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_9
			// 
			this.txtKanjoKamokuNm90040_9.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_9.DisplayLength = null;
			this.txtKanjoKamokuNm90040_9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_9.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_9.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_9.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_9.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_9.MaxLength = 30;
			this.txtKanjoKamokuNm90040_9.Name = "txtKanjoKamokuNm90040_9";
			this.txtKanjoKamokuNm90040_9.Size = new System.Drawing.Size(647, 23);
			this.txtKanjoKamokuNm90040_9.TabIndex = 17;
			this.txtKanjoKamokuNm90040_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_8
			// 
			this.txtKanjoKamokuNm90040_8.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_8.DisplayLength = null;
			this.txtKanjoKamokuNm90040_8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_8.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_8.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_8.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_8.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_8.MaxLength = 30;
			this.txtKanjoKamokuNm90040_8.Name = "txtKanjoKamokuNm90040_8";
			this.txtKanjoKamokuNm90040_8.Size = new System.Drawing.Size(647, 23);
			this.txtKanjoKamokuNm90040_8.TabIndex = 15;
			this.txtKanjoKamokuNm90040_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_7
			// 
			this.txtKanjoKamokuNm90040_7.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_7.DisplayLength = null;
			this.txtKanjoKamokuNm90040_7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_7.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_7.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_7.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_7.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_7.MaxLength = 30;
			this.txtKanjoKamokuNm90040_7.Name = "txtKanjoKamokuNm90040_7";
			this.txtKanjoKamokuNm90040_7.Size = new System.Drawing.Size(647, 23);
			this.txtKanjoKamokuNm90040_7.TabIndex = 13;
			this.txtKanjoKamokuNm90040_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_6
			// 
			this.txtKanjoKamokuNm90040_6.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_6.DisplayLength = null;
			this.txtKanjoKamokuNm90040_6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_6.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_6.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_6.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_6.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_6.MaxLength = 30;
			this.txtKanjoKamokuNm90040_6.Name = "txtKanjoKamokuNm90040_6";
			this.txtKanjoKamokuNm90040_6.Size = new System.Drawing.Size(647, 23);
			this.txtKanjoKamokuNm90040_6.TabIndex = 11;
			this.txtKanjoKamokuNm90040_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_5
			// 
			this.txtKanjoKamokuNm90040_5.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_5.DisplayLength = null;
			this.txtKanjoKamokuNm90040_5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_5.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_5.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_5.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_5.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_5.MaxLength = 30;
			this.txtKanjoKamokuNm90040_5.Name = "txtKanjoKamokuNm90040_5";
			this.txtKanjoKamokuNm90040_5.Size = new System.Drawing.Size(647, 23);
			this.txtKanjoKamokuNm90040_5.TabIndex = 9;
			this.txtKanjoKamokuNm90040_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_4
			// 
			this.txtKanjoKamokuNm90040_4.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_4.DisplayLength = null;
			this.txtKanjoKamokuNm90040_4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_4.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_4.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_4.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_4.MaxLength = 30;
			this.txtKanjoKamokuNm90040_4.Name = "txtKanjoKamokuNm90040_4";
			this.txtKanjoKamokuNm90040_4.Size = new System.Drawing.Size(647, 23);
			this.txtKanjoKamokuNm90040_4.TabIndex = 7;
			this.txtKanjoKamokuNm90040_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_3
			// 
			this.txtKanjoKamokuNm90040_3.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_3.DisplayLength = null;
			this.txtKanjoKamokuNm90040_3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_3.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_3.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_3.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_3.MaxLength = 30;
			this.txtKanjoKamokuNm90040_3.Name = "txtKanjoKamokuNm90040_3";
			this.txtKanjoKamokuNm90040_3.Size = new System.Drawing.Size(647, 23);
			this.txtKanjoKamokuNm90040_3.TabIndex = 5;
			this.txtKanjoKamokuNm90040_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_2
			// 
			this.txtKanjoKamokuNm90040_2.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_2.DisplayLength = null;
			this.txtKanjoKamokuNm90040_2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_2.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_2.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_2.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_2.MaxLength = 30;
			this.txtKanjoKamokuNm90040_2.Name = "txtKanjoKamokuNm90040_2";
			this.txtKanjoKamokuNm90040_2.Size = new System.Drawing.Size(647, 23);
			this.txtKanjoKamokuNm90040_2.TabIndex = 3;
			this.txtKanjoKamokuNm90040_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// txtKanjoKamokuNm90040_1
			// 
			this.txtKanjoKamokuNm90040_1.AutoSizeFromLength = false;
			this.txtKanjoKamokuNm90040_1.DisplayLength = null;
			this.txtKanjoKamokuNm90040_1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtKanjoKamokuNm90040_1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokuNm90040_1.ForeColor = System.Drawing.Color.Black;
			this.txtKanjoKamokuNm90040_1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
			this.txtKanjoKamokuNm90040_1.Location = new System.Drawing.Point(0, 0);
			this.txtKanjoKamokuNm90040_1.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokuNm90040_1.MaxLength = 30;
			this.txtKanjoKamokuNm90040_1.Name = "txtKanjoKamokuNm90040_1";
			this.txtKanjoKamokuNm90040_1.Size = new System.Drawing.Size(647, 20);
			this.txtKanjoKamokuNm90040_1.TabIndex = 1;
			this.txtKanjoKamokuNm90040_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(330, 19);
			this.label1.TabIndex = 902;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "科　　　　　目";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(455, 19);
			this.label2.TabIndex = 903;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "金　　　　　額";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(20, 38);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(793, 53);
			this.fsiTableLayoutPanel1.TabIndex = 904;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.fsiPanel5);
			this.fsiPanel1.Controls.Add(this.fsiPanel3);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(785, 19);
			this.fsiPanel1.TabIndex = 906;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.label2);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(330, 0);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(455, 19);
			this.fsiPanel5.TabIndex = 906;
			this.fsiPanel5.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.label1);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel3.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(330, 19);
			this.fsiPanel3.TabIndex = 906;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.fsiPanel6);
			this.fsiPanel2.Controls.Add(this.fsiPanel4);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 30);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(785, 19);
			this.fsiPanel2.TabIndex = 906;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.txtKingaku1);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel6.Location = new System.Drawing.Point(330, 0);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(455, 19);
			this.fsiPanel6.TabIndex = 906;
			this.fsiPanel6.Tag = "CHANGE";
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtKamokuBunruiNm10);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel4.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(330, 19);
			this.fsiPanel4.TabIndex = 906;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiTableLayoutPanel2
			// 
			this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel2.ColumnCount = 1;
			this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel16, 0, 9);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel15, 0, 8);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel14, 0, 7);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel13, 0, 6);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel12, 0, 5);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel11, 0, 4);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel10, 0, 3);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel9, 0, 2);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel8, 0, 1);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel7, 0, 0);
			this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(20, 100);
			this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
			this.fsiTableLayoutPanel2.RowCount = 10;
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(793, 248);
			this.fsiTableLayoutPanel2.TabIndex = 905;
			// 
			// fsiPanel16
			// 
			this.fsiPanel16.Controls.Add(this.fsiPanel36);
			this.fsiPanel16.Controls.Add(this.fsiPanel26);
			this.fsiPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel16.Location = new System.Drawing.Point(4, 220);
			this.fsiPanel16.Name = "fsiPanel16";
			this.fsiPanel16.Size = new System.Drawing.Size(785, 24);
			this.fsiPanel16.TabIndex = 914;
			this.fsiPanel16.Tag = "CHANGE";
			// 
			// fsiPanel36
			// 
			this.fsiPanel36.Controls.Add(this.txtNiniTmttknTorikuzushigaku9);
			this.fsiPanel36.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel36.Location = new System.Drawing.Point(311, 0);
			this.fsiPanel36.Name = "fsiPanel36";
			this.fsiPanel36.Size = new System.Drawing.Size(474, 24);
			this.fsiPanel36.TabIndex = 918;
			this.fsiPanel36.Tag = "CHANGE";
			// 
			// fsiPanel26
			// 
			this.fsiPanel26.Controls.Add(this.txtKanjoKamokuNm90020_9);
			this.fsiPanel26.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel26.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel26.Name = "fsiPanel26";
			this.fsiPanel26.Size = new System.Drawing.Size(311, 24);
			this.fsiPanel26.TabIndex = 916;
			this.fsiPanel26.Tag = "CHANGE";
			// 
			// fsiPanel15
			// 
			this.fsiPanel15.Controls.Add(this.fsiPanel35);
			this.fsiPanel15.Controls.Add(this.fsiPanel25);
			this.fsiPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel15.Location = new System.Drawing.Point(4, 196);
			this.fsiPanel15.Name = "fsiPanel15";
			this.fsiPanel15.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel15.TabIndex = 913;
			this.fsiPanel15.Tag = "CHANGE";
			// 
			// fsiPanel35
			// 
			this.fsiPanel35.Controls.Add(this.txtNiniTmttknTorikuzushigaku8);
			this.fsiPanel35.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel35.Location = new System.Drawing.Point(311, 0);
			this.fsiPanel35.Name = "fsiPanel35";
			this.fsiPanel35.Size = new System.Drawing.Size(474, 17);
			this.fsiPanel35.TabIndex = 918;
			this.fsiPanel35.Tag = "CHANGE";
			// 
			// fsiPanel25
			// 
			this.fsiPanel25.Controls.Add(this.txtKanjoKamokuNm90020_8);
			this.fsiPanel25.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel25.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel25.Name = "fsiPanel25";
			this.fsiPanel25.Size = new System.Drawing.Size(311, 17);
			this.fsiPanel25.TabIndex = 916;
			this.fsiPanel25.Tag = "CHANGE";
			// 
			// fsiPanel14
			// 
			this.fsiPanel14.Controls.Add(this.fsiPanel34);
			this.fsiPanel14.Controls.Add(this.fsiPanel24);
			this.fsiPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel14.Location = new System.Drawing.Point(4, 172);
			this.fsiPanel14.Name = "fsiPanel14";
			this.fsiPanel14.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel14.TabIndex = 912;
			this.fsiPanel14.Tag = "CHANGE";
			// 
			// fsiPanel34
			// 
			this.fsiPanel34.Controls.Add(this.txtNiniTmttknTorikuzushigaku7);
			this.fsiPanel34.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel34.Location = new System.Drawing.Point(311, 0);
			this.fsiPanel34.Name = "fsiPanel34";
			this.fsiPanel34.Size = new System.Drawing.Size(474, 17);
			this.fsiPanel34.TabIndex = 918;
			this.fsiPanel34.Tag = "CHANGE";
			// 
			// fsiPanel24
			// 
			this.fsiPanel24.Controls.Add(this.txtKanjoKamokuNm90020_7);
			this.fsiPanel24.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel24.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel24.Name = "fsiPanel24";
			this.fsiPanel24.Size = new System.Drawing.Size(311, 17);
			this.fsiPanel24.TabIndex = 916;
			this.fsiPanel24.Tag = "CHANGE";
			// 
			// fsiPanel13
			// 
			this.fsiPanel13.Controls.Add(this.fsiPanel33);
			this.fsiPanel13.Controls.Add(this.fsiPanel23);
			this.fsiPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel13.Location = new System.Drawing.Point(4, 148);
			this.fsiPanel13.Name = "fsiPanel13";
			this.fsiPanel13.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel13.TabIndex = 911;
			this.fsiPanel13.Tag = "CHANGE";
			// 
			// fsiPanel33
			// 
			this.fsiPanel33.Controls.Add(this.txtNiniTmttknTorikuzushigaku6);
			this.fsiPanel33.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel33.Location = new System.Drawing.Point(311, 0);
			this.fsiPanel33.Name = "fsiPanel33";
			this.fsiPanel33.Size = new System.Drawing.Size(474, 17);
			this.fsiPanel33.TabIndex = 918;
			this.fsiPanel33.Tag = "CHANGE";
			// 
			// fsiPanel23
			// 
			this.fsiPanel23.Controls.Add(this.txtKanjoKamokuNm90020_6);
			this.fsiPanel23.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel23.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel23.Name = "fsiPanel23";
			this.fsiPanel23.Size = new System.Drawing.Size(311, 17);
			this.fsiPanel23.TabIndex = 916;
			this.fsiPanel23.Tag = "CHANGE";
			// 
			// fsiPanel12
			// 
			this.fsiPanel12.Controls.Add(this.fsiPanel32);
			this.fsiPanel12.Controls.Add(this.fsiPanel22);
			this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel12.Location = new System.Drawing.Point(4, 124);
			this.fsiPanel12.Name = "fsiPanel12";
			this.fsiPanel12.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel12.TabIndex = 910;
			this.fsiPanel12.Tag = "CHANGE";
			// 
			// fsiPanel32
			// 
			this.fsiPanel32.Controls.Add(this.txtNiniTmttknTorikuzushigaku5);
			this.fsiPanel32.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel32.Location = new System.Drawing.Point(311, 0);
			this.fsiPanel32.Name = "fsiPanel32";
			this.fsiPanel32.Size = new System.Drawing.Size(474, 17);
			this.fsiPanel32.TabIndex = 918;
			this.fsiPanel32.Tag = "CHANGE";
			// 
			// fsiPanel22
			// 
			this.fsiPanel22.Controls.Add(this.txtKanjoKamokuNm90020_5);
			this.fsiPanel22.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel22.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel22.Name = "fsiPanel22";
			this.fsiPanel22.Size = new System.Drawing.Size(311, 17);
			this.fsiPanel22.TabIndex = 916;
			this.fsiPanel22.Tag = "CHANGE";
			// 
			// fsiPanel11
			// 
			this.fsiPanel11.Controls.Add(this.fsiPanel31);
			this.fsiPanel11.Controls.Add(this.fsiPanel21);
			this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel11.Location = new System.Drawing.Point(4, 100);
			this.fsiPanel11.Name = "fsiPanel11";
			this.fsiPanel11.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel11.TabIndex = 909;
			this.fsiPanel11.Tag = "CHANGE";
			// 
			// fsiPanel31
			// 
			this.fsiPanel31.Controls.Add(this.txtNiniTmttknTorikuzushigaku4);
			this.fsiPanel31.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel31.Location = new System.Drawing.Point(311, 0);
			this.fsiPanel31.Name = "fsiPanel31";
			this.fsiPanel31.Size = new System.Drawing.Size(474, 17);
			this.fsiPanel31.TabIndex = 918;
			this.fsiPanel31.Tag = "CHANGE";
			// 
			// fsiPanel21
			// 
			this.fsiPanel21.Controls.Add(this.txtKanjoKamokuNm90020_4);
			this.fsiPanel21.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel21.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel21.Name = "fsiPanel21";
			this.fsiPanel21.Size = new System.Drawing.Size(311, 17);
			this.fsiPanel21.TabIndex = 916;
			this.fsiPanel21.Tag = "CHANGE";
			// 
			// fsiPanel10
			// 
			this.fsiPanel10.Controls.Add(this.fsiPanel30);
			this.fsiPanel10.Controls.Add(this.fsiPanel20);
			this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel10.Location = new System.Drawing.Point(4, 76);
			this.fsiPanel10.Name = "fsiPanel10";
			this.fsiPanel10.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel10.TabIndex = 908;
			this.fsiPanel10.Tag = "CHANGE";
			// 
			// fsiPanel30
			// 
			this.fsiPanel30.Controls.Add(this.txtNiniTmttknTorikuzushigaku3);
			this.fsiPanel30.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel30.Location = new System.Drawing.Point(311, 0);
			this.fsiPanel30.Name = "fsiPanel30";
			this.fsiPanel30.Size = new System.Drawing.Size(474, 17);
			this.fsiPanel30.TabIndex = 918;
			this.fsiPanel30.Tag = "CHANGE";
			// 
			// fsiPanel20
			// 
			this.fsiPanel20.Controls.Add(this.txtKanjoKamokuNm90020_3);
			this.fsiPanel20.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel20.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel20.Name = "fsiPanel20";
			this.fsiPanel20.Size = new System.Drawing.Size(311, 17);
			this.fsiPanel20.TabIndex = 916;
			this.fsiPanel20.Tag = "CHANGE";
			// 
			// fsiPanel9
			// 
			this.fsiPanel9.Controls.Add(this.fsiPanel29);
			this.fsiPanel9.Controls.Add(this.fsiPanel19);
			this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel9.Location = new System.Drawing.Point(4, 52);
			this.fsiPanel9.Name = "fsiPanel9";
			this.fsiPanel9.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel9.TabIndex = 907;
			this.fsiPanel9.Tag = "CHANGE";
			// 
			// fsiPanel29
			// 
			this.fsiPanel29.Controls.Add(this.txtNiniTmttknTorikuzushigaku2);
			this.fsiPanel29.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel29.Location = new System.Drawing.Point(311, 0);
			this.fsiPanel29.Name = "fsiPanel29";
			this.fsiPanel29.Size = new System.Drawing.Size(474, 17);
			this.fsiPanel29.TabIndex = 918;
			this.fsiPanel29.Tag = "CHANGE";
			// 
			// fsiPanel19
			// 
			this.fsiPanel19.Controls.Add(this.txtKanjoKamokuNm90020_2);
			this.fsiPanel19.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel19.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel19.Name = "fsiPanel19";
			this.fsiPanel19.Size = new System.Drawing.Size(311, 17);
			this.fsiPanel19.TabIndex = 916;
			this.fsiPanel19.Tag = "CHANGE";
			// 
			// fsiPanel8
			// 
			this.fsiPanel8.Controls.Add(this.fsiPanel28);
			this.fsiPanel8.Controls.Add(this.fsiPanel18);
			this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel8.Location = new System.Drawing.Point(4, 28);
			this.fsiPanel8.Name = "fsiPanel8";
			this.fsiPanel8.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel8.TabIndex = 906;
			this.fsiPanel8.Tag = "CHANGE";
			// 
			// fsiPanel28
			// 
			this.fsiPanel28.Controls.Add(this.txtNiniTmttknTorikuzushigaku1);
			this.fsiPanel28.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel28.Location = new System.Drawing.Point(311, 0);
			this.fsiPanel28.Name = "fsiPanel28";
			this.fsiPanel28.Size = new System.Drawing.Size(474, 17);
			this.fsiPanel28.TabIndex = 918;
			this.fsiPanel28.Tag = "CHANGE";
			// 
			// fsiPanel18
			// 
			this.fsiPanel18.Controls.Add(this.txtKanjoKamokuNm90020_1);
			this.fsiPanel18.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel18.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel18.Name = "fsiPanel18";
			this.fsiPanel18.Size = new System.Drawing.Size(311, 17);
			this.fsiPanel18.TabIndex = 916;
			this.fsiPanel18.Tag = "CHANGE";
			// 
			// fsiPanel7
			// 
			this.fsiPanel7.Controls.Add(this.fsiPanel17);
			this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel7.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel7.Name = "fsiPanel7";
			this.fsiPanel7.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel7.TabIndex = 906;
			this.fsiPanel7.Tag = "CHANGE";
			// 
			// fsiPanel17
			// 
			this.fsiPanel17.Controls.Add(this.txtKamokuBunruiNm20);
			this.fsiPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel17.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel17.Name = "fsiPanel17";
			this.fsiPanel17.Size = new System.Drawing.Size(785, 17);
			this.fsiPanel17.TabIndex = 915;
			this.fsiPanel17.Tag = "CHANGE";
			// 
			// fsiTableLayoutPanel3
			// 
			this.fsiTableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel3.ColumnCount = 1;
			this.fsiTableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel37, 0, 0);
			this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel38, 0, 1);
			this.fsiTableLayoutPanel3.Location = new System.Drawing.Point(21, 354);
			this.fsiTableLayoutPanel3.Name = "fsiTableLayoutPanel3";
			this.fsiTableLayoutPanel3.RowCount = 2;
			this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel3.Size = new System.Drawing.Size(792, 67);
			this.fsiTableLayoutPanel3.TabIndex = 905;
			// 
			// fsiPanel37
			// 
			this.fsiPanel37.Controls.Add(this.fsiPanel39);
			this.fsiPanel37.Controls.Add(this.fsiPanel40);
			this.fsiPanel37.Controls.Add(this.fsiPanel41);
			this.fsiPanel37.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel37.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel37.Name = "fsiPanel37";
			this.fsiPanel37.Size = new System.Drawing.Size(784, 26);
			this.fsiPanel37.TabIndex = 919;
			this.fsiPanel37.Tag = "CHANGE";
			// 
			// fsiPanel39
			// 
			this.fsiPanel39.Controls.Add(this.txtNiniTmttknTorikuzushigaku10);
			this.fsiPanel39.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel39.Location = new System.Drawing.Point(310, 0);
			this.fsiPanel39.Name = "fsiPanel39";
			this.fsiPanel39.Size = new System.Drawing.Size(329, 26);
			this.fsiPanel39.TabIndex = 919;
			this.fsiPanel39.Tag = "CHANGE";
			// 
			// fsiPanel40
			// 
			this.fsiPanel40.Controls.Add(this.txtKingaku2);
			this.fsiPanel40.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel40.Location = new System.Drawing.Point(639, 0);
			this.fsiPanel40.Name = "fsiPanel40";
			this.fsiPanel40.Size = new System.Drawing.Size(145, 26);
			this.fsiPanel40.TabIndex = 919;
			this.fsiPanel40.Tag = "CHANGE";
			// 
			// fsiPanel41
			// 
			this.fsiPanel41.Controls.Add(this.txtKanjoKamokuNm90020_10);
			this.fsiPanel41.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel41.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel41.Name = "fsiPanel41";
			this.fsiPanel41.Size = new System.Drawing.Size(315, 26);
			this.fsiPanel41.TabIndex = 919;
			this.fsiPanel41.Tag = "CHANGE";
			// 
			// fsiPanel38
			// 
			this.fsiPanel38.Controls.Add(this.fsiPanel43);
			this.fsiPanel38.Controls.Add(this.fsiPanel42);
			this.fsiPanel38.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel38.Location = new System.Drawing.Point(4, 37);
			this.fsiPanel38.Name = "fsiPanel38";
			this.fsiPanel38.Size = new System.Drawing.Size(784, 26);
			this.fsiPanel38.TabIndex = 919;
			this.fsiPanel38.Tag = "CHANGE";
			// 
			// fsiPanel43
			// 
			this.fsiPanel43.Controls.Add(this.txtKingaku3);
			this.fsiPanel43.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel43.Location = new System.Drawing.Point(315, 0);
			this.fsiPanel43.Name = "fsiPanel43";
			this.fsiPanel43.Size = new System.Drawing.Size(469, 26);
			this.fsiPanel43.TabIndex = 919;
			this.fsiPanel43.Tag = "CHANGE";
			// 
			// fsiPanel42
			// 
			this.fsiPanel42.Controls.Add(this.txtKamokuBunruiNm30);
			this.fsiPanel42.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel42.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel42.Name = "fsiPanel42";
			this.fsiPanel42.Size = new System.Drawing.Size(315, 26);
			this.fsiPanel42.TabIndex = 919;
			this.fsiPanel42.Tag = "CHANGE";
			// 
			// fsiTableLayoutPanel4
			// 
			this.fsiTableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel4.ColumnCount = 1;
			this.fsiTableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel52, 0, 9);
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel51, 0, 8);
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel50, 0, 7);
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel49, 0, 6);
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel48, 0, 5);
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel47, 0, 4);
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel46, 0, 3);
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel45, 0, 2);
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel44, 0, 1);
			this.fsiTableLayoutPanel4.Controls.Add(this.fsiPanel27, 0, 0);
			this.fsiTableLayoutPanel4.Location = new System.Drawing.Point(21, 434);
			this.fsiTableLayoutPanel4.Name = "fsiTableLayoutPanel4";
			this.fsiTableLayoutPanel4.RowCount = 10;
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel4.Size = new System.Drawing.Size(792, 268);
			this.fsiTableLayoutPanel4.TabIndex = 905;
			// 
			// fsiPanel52
			// 
			this.fsiPanel52.Controls.Add(this.fsiPanel61);
			this.fsiPanel52.Controls.Add(this.fsiPanel70);
			this.fsiPanel52.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel52.Location = new System.Drawing.Point(4, 238);
			this.fsiPanel52.Name = "fsiPanel52";
			this.fsiPanel52.Size = new System.Drawing.Size(784, 26);
			this.fsiPanel52.TabIndex = 911;
			this.fsiPanel52.Tag = "CHANGE";
			// 
			// fsiPanel61
			// 
			this.fsiPanel61.Controls.Add(this.txtKanjoKamokuNm90040_9);
			this.fsiPanel61.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel61.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel61.Name = "fsiPanel61";
			this.fsiPanel61.Size = new System.Drawing.Size(647, 26);
			this.fsiPanel61.TabIndex = 912;
			this.fsiPanel61.Tag = "CHANGE";
			// 
			// fsiPanel70
			// 
			this.fsiPanel70.Controls.Add(this.txtRiekikinShobungaku9);
			this.fsiPanel70.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel70.Location = new System.Drawing.Point(647, 0);
			this.fsiPanel70.Name = "fsiPanel70";
			this.fsiPanel70.Size = new System.Drawing.Size(137, 26);
			this.fsiPanel70.TabIndex = 916;
			this.fsiPanel70.Tag = "CHANGE";
			// 
			// fsiPanel51
			// 
			this.fsiPanel51.Controls.Add(this.fsiPanel60);
			this.fsiPanel51.Controls.Add(this.fsiPanel69);
			this.fsiPanel51.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel51.Location = new System.Drawing.Point(4, 212);
			this.fsiPanel51.Name = "fsiPanel51";
			this.fsiPanel51.Size = new System.Drawing.Size(784, 19);
			this.fsiPanel51.TabIndex = 910;
			this.fsiPanel51.Tag = "CHANGE";
			// 
			// fsiPanel60
			// 
			this.fsiPanel60.Controls.Add(this.txtKanjoKamokuNm90040_8);
			this.fsiPanel60.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel60.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel60.Name = "fsiPanel60";
			this.fsiPanel60.Size = new System.Drawing.Size(647, 19);
			this.fsiPanel60.TabIndex = 912;
			this.fsiPanel60.Tag = "CHANGE";
			// 
			// fsiPanel69
			// 
			this.fsiPanel69.Controls.Add(this.txtRiekikinShobungaku8);
			this.fsiPanel69.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel69.Location = new System.Drawing.Point(647, 0);
			this.fsiPanel69.Name = "fsiPanel69";
			this.fsiPanel69.Size = new System.Drawing.Size(137, 19);
			this.fsiPanel69.TabIndex = 915;
			this.fsiPanel69.Tag = "CHANGE";
			// 
			// fsiPanel50
			// 
			this.fsiPanel50.Controls.Add(this.fsiPanel59);
			this.fsiPanel50.Controls.Add(this.fsiPanel68);
			this.fsiPanel50.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel50.Location = new System.Drawing.Point(4, 186);
			this.fsiPanel50.Name = "fsiPanel50";
			this.fsiPanel50.Size = new System.Drawing.Size(784, 19);
			this.fsiPanel50.TabIndex = 910;
			this.fsiPanel50.Tag = "CHANGE";
			// 
			// fsiPanel59
			// 
			this.fsiPanel59.Controls.Add(this.txtKanjoKamokuNm90040_7);
			this.fsiPanel59.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel59.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel59.Name = "fsiPanel59";
			this.fsiPanel59.Size = new System.Drawing.Size(647, 19);
			this.fsiPanel59.TabIndex = 912;
			this.fsiPanel59.Tag = "CHANGE";
			// 
			// fsiPanel68
			// 
			this.fsiPanel68.Controls.Add(this.txtRiekikinShobungaku7);
			this.fsiPanel68.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel68.Location = new System.Drawing.Point(647, 0);
			this.fsiPanel68.Name = "fsiPanel68";
			this.fsiPanel68.Size = new System.Drawing.Size(137, 19);
			this.fsiPanel68.TabIndex = 914;
			this.fsiPanel68.Tag = "CHANGE";
			// 
			// fsiPanel49
			// 
			this.fsiPanel49.Controls.Add(this.fsiPanel58);
			this.fsiPanel49.Controls.Add(this.fsiPanel67);
			this.fsiPanel49.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel49.Location = new System.Drawing.Point(4, 160);
			this.fsiPanel49.Name = "fsiPanel49";
			this.fsiPanel49.Size = new System.Drawing.Size(784, 19);
			this.fsiPanel49.TabIndex = 910;
			this.fsiPanel49.Tag = "CHANGE";
			// 
			// fsiPanel58
			// 
			this.fsiPanel58.Controls.Add(this.txtKanjoKamokuNm90040_6);
			this.fsiPanel58.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel58.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel58.Name = "fsiPanel58";
			this.fsiPanel58.Size = new System.Drawing.Size(647, 19);
			this.fsiPanel58.TabIndex = 912;
			this.fsiPanel58.Tag = "CHANGE";
			// 
			// fsiPanel67
			// 
			this.fsiPanel67.Controls.Add(this.txtRiekikinShobungaku6);
			this.fsiPanel67.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel67.Location = new System.Drawing.Point(647, 0);
			this.fsiPanel67.Name = "fsiPanel67";
			this.fsiPanel67.Size = new System.Drawing.Size(137, 19);
			this.fsiPanel67.TabIndex = 913;
			this.fsiPanel67.Tag = "CHANGE";
			// 
			// fsiPanel48
			// 
			this.fsiPanel48.Controls.Add(this.fsiPanel57);
			this.fsiPanel48.Controls.Add(this.fsiPanel66);
			this.fsiPanel48.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel48.Location = new System.Drawing.Point(4, 134);
			this.fsiPanel48.Name = "fsiPanel48";
			this.fsiPanel48.Size = new System.Drawing.Size(784, 19);
			this.fsiPanel48.TabIndex = 910;
			this.fsiPanel48.Tag = "CHANGE";
			// 
			// fsiPanel57
			// 
			this.fsiPanel57.Controls.Add(this.txtKanjoKamokuNm90040_5);
			this.fsiPanel57.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel57.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel57.Name = "fsiPanel57";
			this.fsiPanel57.Size = new System.Drawing.Size(647, 19);
			this.fsiPanel57.TabIndex = 912;
			this.fsiPanel57.Tag = "CHANGE";
			// 
			// fsiPanel66
			// 
			this.fsiPanel66.Controls.Add(this.txtRiekikinShobungaku5);
			this.fsiPanel66.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel66.Location = new System.Drawing.Point(647, 0);
			this.fsiPanel66.Name = "fsiPanel66";
			this.fsiPanel66.Size = new System.Drawing.Size(137, 19);
			this.fsiPanel66.TabIndex = 912;
			this.fsiPanel66.Tag = "CHANGE";
			// 
			// fsiPanel47
			// 
			this.fsiPanel47.Controls.Add(this.fsiPanel56);
			this.fsiPanel47.Controls.Add(this.fsiPanel65);
			this.fsiPanel47.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel47.Location = new System.Drawing.Point(4, 108);
			this.fsiPanel47.Name = "fsiPanel47";
			this.fsiPanel47.Size = new System.Drawing.Size(784, 19);
			this.fsiPanel47.TabIndex = 909;
			this.fsiPanel47.Tag = "CHANGE";
			// 
			// fsiPanel56
			// 
			this.fsiPanel56.Controls.Add(this.txtKanjoKamokuNm90040_4);
			this.fsiPanel56.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel56.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel56.Name = "fsiPanel56";
			this.fsiPanel56.Size = new System.Drawing.Size(647, 19);
			this.fsiPanel56.TabIndex = 912;
			this.fsiPanel56.Tag = "CHANGE";
			// 
			// fsiPanel65
			// 
			this.fsiPanel65.Controls.Add(this.txtRiekikinShobungaku4);
			this.fsiPanel65.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel65.Location = new System.Drawing.Point(647, 0);
			this.fsiPanel65.Name = "fsiPanel65";
			this.fsiPanel65.Size = new System.Drawing.Size(137, 19);
			this.fsiPanel65.TabIndex = 912;
			this.fsiPanel65.Tag = "CHANGE";
			// 
			// fsiPanel46
			// 
			this.fsiPanel46.Controls.Add(this.fsiPanel55);
			this.fsiPanel46.Controls.Add(this.fsiPanel64);
			this.fsiPanel46.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel46.Location = new System.Drawing.Point(4, 82);
			this.fsiPanel46.Name = "fsiPanel46";
			this.fsiPanel46.Size = new System.Drawing.Size(784, 19);
			this.fsiPanel46.TabIndex = 908;
			this.fsiPanel46.Tag = "CHANGE";
			// 
			// fsiPanel55
			// 
			this.fsiPanel55.Controls.Add(this.txtKanjoKamokuNm90040_3);
			this.fsiPanel55.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel55.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel55.Name = "fsiPanel55";
			this.fsiPanel55.Size = new System.Drawing.Size(647, 19);
			this.fsiPanel55.TabIndex = 912;
			this.fsiPanel55.Tag = "CHANGE";
			// 
			// fsiPanel64
			// 
			this.fsiPanel64.Controls.Add(this.txtRiekikinShobungaku3);
			this.fsiPanel64.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel64.Location = new System.Drawing.Point(647, 0);
			this.fsiPanel64.Name = "fsiPanel64";
			this.fsiPanel64.Size = new System.Drawing.Size(137, 19);
			this.fsiPanel64.TabIndex = 912;
			this.fsiPanel64.Tag = "CHANGE";
			// 
			// fsiPanel45
			// 
			this.fsiPanel45.Controls.Add(this.fsiPanel54);
			this.fsiPanel45.Controls.Add(this.fsiPanel63);
			this.fsiPanel45.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel45.Location = new System.Drawing.Point(4, 56);
			this.fsiPanel45.Name = "fsiPanel45";
			this.fsiPanel45.Size = new System.Drawing.Size(784, 19);
			this.fsiPanel45.TabIndex = 907;
			this.fsiPanel45.Tag = "CHANGE";
			// 
			// fsiPanel54
			// 
			this.fsiPanel54.Controls.Add(this.txtKanjoKamokuNm90040_2);
			this.fsiPanel54.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel54.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel54.Name = "fsiPanel54";
			this.fsiPanel54.Size = new System.Drawing.Size(647, 19);
			this.fsiPanel54.TabIndex = 912;
			this.fsiPanel54.Tag = "CHANGE";
			// 
			// fsiPanel63
			// 
			this.fsiPanel63.Controls.Add(this.txtRiekikinShobungaku2);
			this.fsiPanel63.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel63.Location = new System.Drawing.Point(647, 0);
			this.fsiPanel63.Name = "fsiPanel63";
			this.fsiPanel63.Size = new System.Drawing.Size(137, 19);
			this.fsiPanel63.TabIndex = 912;
			this.fsiPanel63.Tag = "CHANGE";
			// 
			// fsiPanel44
			// 
			this.fsiPanel44.Controls.Add(this.fsiPanel53);
			this.fsiPanel44.Controls.Add(this.fsiPanel62);
			this.fsiPanel44.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel44.Location = new System.Drawing.Point(4, 30);
			this.fsiPanel44.Name = "fsiPanel44";
			this.fsiPanel44.Size = new System.Drawing.Size(784, 19);
			this.fsiPanel44.TabIndex = 906;
			this.fsiPanel44.Tag = "CHANGE";
			// 
			// fsiPanel53
			// 
			this.fsiPanel53.Controls.Add(this.txtKanjoKamokuNm90040_1);
			this.fsiPanel53.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel53.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel53.Name = "fsiPanel53";
			this.fsiPanel53.Size = new System.Drawing.Size(647, 19);
			this.fsiPanel53.TabIndex = 912;
			this.fsiPanel53.Tag = "CHANGE";
			// 
			// fsiPanel62
			// 
			this.fsiPanel62.Controls.Add(this.txtRiekikinShobungaku1);
			this.fsiPanel62.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel62.Location = new System.Drawing.Point(647, 0);
			this.fsiPanel62.Name = "fsiPanel62";
			this.fsiPanel62.Size = new System.Drawing.Size(137, 19);
			this.fsiPanel62.TabIndex = 912;
			this.fsiPanel62.Tag = "CHANGE";
			// 
			// fsiPanel27
			// 
			this.fsiPanel27.Controls.Add(this.txtKamokuBunruiNm40);
			this.fsiPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel27.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel27.Name = "fsiPanel27";
			this.fsiPanel27.Size = new System.Drawing.Size(784, 19);
			this.fsiPanel27.TabIndex = 908;
			this.fsiPanel27.Tag = "CHANGE";
			// 
			// fsiTableLayoutPanel5
			// 
			this.fsiTableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel5.ColumnCount = 1;
			this.fsiTableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel5.Controls.Add(this.fsiPanel72, 0, 1);
			this.fsiTableLayoutPanel5.Controls.Add(this.fsiPanel71, 0, 0);
			this.fsiTableLayoutPanel5.Location = new System.Drawing.Point(21, 712);
			this.fsiTableLayoutPanel5.Name = "fsiTableLayoutPanel5";
			this.fsiTableLayoutPanel5.RowCount = 2;
			this.fsiTableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel5.Size = new System.Drawing.Size(792, 70);
			this.fsiTableLayoutPanel5.TabIndex = 905;
			// 
			// fsiPanel72
			// 
			this.fsiPanel72.Controls.Add(this.fsiPanel74);
			this.fsiPanel72.Controls.Add(this.fsiPanel77);
			this.fsiPanel72.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel72.Location = new System.Drawing.Point(4, 38);
			this.fsiPanel72.Name = "fsiPanel72";
			this.fsiPanel72.Size = new System.Drawing.Size(784, 28);
			this.fsiPanel72.TabIndex = 918;
			this.fsiPanel72.Tag = "CHANGE";
			// 
			// fsiPanel74
			// 
			this.fsiPanel74.Controls.Add(this.txtKamokuBunruiNm50);
			this.fsiPanel74.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel74.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel74.Name = "fsiPanel74";
			this.fsiPanel74.Size = new System.Drawing.Size(561, 28);
			this.fsiPanel74.TabIndex = 917;
			this.fsiPanel74.Tag = "CHANGE";
			// 
			// fsiPanel77
			// 
			this.fsiPanel77.Controls.Add(this.txtKingaku5);
			this.fsiPanel77.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel77.Location = new System.Drawing.Point(561, 0);
			this.fsiPanel77.Name = "fsiPanel77";
			this.fsiPanel77.Size = new System.Drawing.Size(223, 28);
			this.fsiPanel77.TabIndex = 917;
			this.fsiPanel77.Tag = "CHANGE";
			// 
			// fsiPanel71
			// 
			this.fsiPanel71.Controls.Add(this.fsiPanel73);
			this.fsiPanel71.Controls.Add(this.fsiPanel76);
			this.fsiPanel71.Controls.Add(this.fsiPanel75);
			this.fsiPanel71.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel71.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel71.Name = "fsiPanel71";
			this.fsiPanel71.Size = new System.Drawing.Size(784, 27);
			this.fsiPanel71.TabIndex = 917;
			this.fsiPanel71.Tag = "CHANGE";
			// 
			// fsiPanel73
			// 
			this.fsiPanel73.Controls.Add(this.txtKanjoKamokuNm90040_10);
			this.fsiPanel73.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel73.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel73.Name = "fsiPanel73";
			this.fsiPanel73.Size = new System.Drawing.Size(292, 27);
			this.fsiPanel73.TabIndex = 917;
			this.fsiPanel73.Tag = "CHANGE";
			// 
			// fsiPanel76
			// 
			this.fsiPanel76.Controls.Add(this.txtRiekikinShobungaku10);
			this.fsiPanel76.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel76.Location = new System.Drawing.Point(292, 0);
			this.fsiPanel76.Name = "fsiPanel76";
			this.fsiPanel76.Size = new System.Drawing.Size(269, 27);
			this.fsiPanel76.TabIndex = 917;
			this.fsiPanel76.Tag = "CHANGE";
			// 
			// fsiPanel75
			// 
			this.fsiPanel75.Controls.Add(this.txtKingaku4);
			this.fsiPanel75.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel75.Location = new System.Drawing.Point(561, 0);
			this.fsiPanel75.Name = "fsiPanel75";
			this.fsiPanel75.Size = new System.Drawing.Size(223, 27);
			this.fsiPanel75.TabIndex = 917;
			this.fsiPanel75.Tag = "CHANGE";
			// 
			// ZMYR1015
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1136, 964);
			this.Controls.Add(this.fsiTableLayoutPanel5);
			this.Controls.Add(this.fsiTableLayoutPanel4);
			this.Controls.Add(this.fsiTableLayoutPanel3);
			this.Controls.Add(this.fsiTableLayoutPanel2);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "ZMYR1015";
			this.ShowFButton = true;
			this.Text = "";
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel3, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel4, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel5, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiTableLayoutPanel2.ResumeLayout(false);
			this.fsiPanel16.ResumeLayout(false);
			this.fsiPanel36.ResumeLayout(false);
			this.fsiPanel36.PerformLayout();
			this.fsiPanel26.ResumeLayout(false);
			this.fsiPanel26.PerformLayout();
			this.fsiPanel15.ResumeLayout(false);
			this.fsiPanel35.ResumeLayout(false);
			this.fsiPanel35.PerformLayout();
			this.fsiPanel25.ResumeLayout(false);
			this.fsiPanel25.PerformLayout();
			this.fsiPanel14.ResumeLayout(false);
			this.fsiPanel34.ResumeLayout(false);
			this.fsiPanel34.PerformLayout();
			this.fsiPanel24.ResumeLayout(false);
			this.fsiPanel24.PerformLayout();
			this.fsiPanel13.ResumeLayout(false);
			this.fsiPanel33.ResumeLayout(false);
			this.fsiPanel33.PerformLayout();
			this.fsiPanel23.ResumeLayout(false);
			this.fsiPanel23.PerformLayout();
			this.fsiPanel12.ResumeLayout(false);
			this.fsiPanel32.ResumeLayout(false);
			this.fsiPanel32.PerformLayout();
			this.fsiPanel22.ResumeLayout(false);
			this.fsiPanel22.PerformLayout();
			this.fsiPanel11.ResumeLayout(false);
			this.fsiPanel31.ResumeLayout(false);
			this.fsiPanel31.PerformLayout();
			this.fsiPanel21.ResumeLayout(false);
			this.fsiPanel21.PerformLayout();
			this.fsiPanel10.ResumeLayout(false);
			this.fsiPanel30.ResumeLayout(false);
			this.fsiPanel30.PerformLayout();
			this.fsiPanel20.ResumeLayout(false);
			this.fsiPanel20.PerformLayout();
			this.fsiPanel9.ResumeLayout(false);
			this.fsiPanel29.ResumeLayout(false);
			this.fsiPanel29.PerformLayout();
			this.fsiPanel19.ResumeLayout(false);
			this.fsiPanel19.PerformLayout();
			this.fsiPanel8.ResumeLayout(false);
			this.fsiPanel28.ResumeLayout(false);
			this.fsiPanel28.PerformLayout();
			this.fsiPanel18.ResumeLayout(false);
			this.fsiPanel18.PerformLayout();
			this.fsiPanel7.ResumeLayout(false);
			this.fsiPanel17.ResumeLayout(false);
			this.fsiPanel17.PerformLayout();
			this.fsiTableLayoutPanel3.ResumeLayout(false);
			this.fsiPanel37.ResumeLayout(false);
			this.fsiPanel39.ResumeLayout(false);
			this.fsiPanel39.PerformLayout();
			this.fsiPanel40.ResumeLayout(false);
			this.fsiPanel40.PerformLayout();
			this.fsiPanel41.ResumeLayout(false);
			this.fsiPanel41.PerformLayout();
			this.fsiPanel38.ResumeLayout(false);
			this.fsiPanel43.ResumeLayout(false);
			this.fsiPanel43.PerformLayout();
			this.fsiPanel42.ResumeLayout(false);
			this.fsiPanel42.PerformLayout();
			this.fsiTableLayoutPanel4.ResumeLayout(false);
			this.fsiPanel52.ResumeLayout(false);
			this.fsiPanel61.ResumeLayout(false);
			this.fsiPanel61.PerformLayout();
			this.fsiPanel70.ResumeLayout(false);
			this.fsiPanel70.PerformLayout();
			this.fsiPanel51.ResumeLayout(false);
			this.fsiPanel60.ResumeLayout(false);
			this.fsiPanel60.PerformLayout();
			this.fsiPanel69.ResumeLayout(false);
			this.fsiPanel69.PerformLayout();
			this.fsiPanel50.ResumeLayout(false);
			this.fsiPanel59.ResumeLayout(false);
			this.fsiPanel59.PerformLayout();
			this.fsiPanel68.ResumeLayout(false);
			this.fsiPanel68.PerformLayout();
			this.fsiPanel49.ResumeLayout(false);
			this.fsiPanel58.ResumeLayout(false);
			this.fsiPanel58.PerformLayout();
			this.fsiPanel67.ResumeLayout(false);
			this.fsiPanel67.PerformLayout();
			this.fsiPanel48.ResumeLayout(false);
			this.fsiPanel57.ResumeLayout(false);
			this.fsiPanel57.PerformLayout();
			this.fsiPanel66.ResumeLayout(false);
			this.fsiPanel66.PerformLayout();
			this.fsiPanel47.ResumeLayout(false);
			this.fsiPanel56.ResumeLayout(false);
			this.fsiPanel56.PerformLayout();
			this.fsiPanel65.ResumeLayout(false);
			this.fsiPanel65.PerformLayout();
			this.fsiPanel46.ResumeLayout(false);
			this.fsiPanel55.ResumeLayout(false);
			this.fsiPanel55.PerformLayout();
			this.fsiPanel64.ResumeLayout(false);
			this.fsiPanel64.PerformLayout();
			this.fsiPanel45.ResumeLayout(false);
			this.fsiPanel54.ResumeLayout(false);
			this.fsiPanel54.PerformLayout();
			this.fsiPanel63.ResumeLayout(false);
			this.fsiPanel63.PerformLayout();
			this.fsiPanel44.ResumeLayout(false);
			this.fsiPanel53.ResumeLayout(false);
			this.fsiPanel53.PerformLayout();
			this.fsiPanel62.ResumeLayout(false);
			this.fsiPanel62.PerformLayout();
			this.fsiPanel27.ResumeLayout(false);
			this.fsiPanel27.PerformLayout();
			this.fsiTableLayoutPanel5.ResumeLayout(false);
			this.fsiPanel72.ResumeLayout(false);
			this.fsiPanel74.ResumeLayout(false);
			this.fsiPanel74.PerformLayout();
			this.fsiPanel77.ResumeLayout(false);
			this.fsiPanel77.PerformLayout();
			this.fsiPanel71.ResumeLayout(false);
			this.fsiPanel73.ResumeLayout(false);
			this.fsiPanel73.PerformLayout();
			this.fsiPanel76.ResumeLayout(false);
			this.fsiPanel76.PerformLayout();
			this.fsiPanel75.ResumeLayout(false);
			this.fsiPanel75.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_10;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_9;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_8;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_7;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_6;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_5;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_4;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_3;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_2;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_1;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku3;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku2;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku10;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku9;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku8;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku7;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku6;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku5;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku4;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku3;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku2;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku1;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm30;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm20;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku1;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm10;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku5;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku4;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku10;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku9;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku8;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku7;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku6;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku5;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku4;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku3;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku2;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku1;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm50;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm40;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_10;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_9;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_8;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_7;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_6;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_5;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_4;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_3;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_2;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel4;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel5;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel16;
        private common.FsiPanel fsiPanel36;
        private common.FsiPanel fsiPanel26;
        private common.FsiPanel fsiPanel15;
        private common.FsiPanel fsiPanel35;
        private common.FsiPanel fsiPanel25;
        private common.FsiPanel fsiPanel14;
        private common.FsiPanel fsiPanel34;
        private common.FsiPanel fsiPanel24;
        private common.FsiPanel fsiPanel13;
        private common.FsiPanel fsiPanel33;
        private common.FsiPanel fsiPanel23;
        private common.FsiPanel fsiPanel12;
        private common.FsiPanel fsiPanel32;
        private common.FsiPanel fsiPanel22;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel31;
        private common.FsiPanel fsiPanel21;
        private common.FsiPanel fsiPanel10;
        private common.FsiPanel fsiPanel30;
        private common.FsiPanel fsiPanel20;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel29;
        private common.FsiPanel fsiPanel19;
        private common.FsiPanel fsiPanel28;
        private common.FsiPanel fsiPanel18;
        private common.FsiPanel fsiPanel17;
        private common.FsiPanel fsiPanel37;
        private common.FsiPanel fsiPanel39;
        private common.FsiPanel fsiPanel40;
        private common.FsiPanel fsiPanel41;
        private common.FsiPanel fsiPanel38;
        private common.FsiPanel fsiPanel43;
        private common.FsiPanel fsiPanel42;
        private common.FsiPanel fsiPanel52;
        private common.FsiPanel fsiPanel61;
        private common.FsiPanel fsiPanel51;
        private common.FsiPanel fsiPanel60;
        private common.FsiPanel fsiPanel50;
        private common.FsiPanel fsiPanel59;
        private common.FsiPanel fsiPanel49;
        private common.FsiPanel fsiPanel58;
        private common.FsiPanel fsiPanel48;
        private common.FsiPanel fsiPanel57;
        private common.FsiPanel fsiPanel47;
        private common.FsiPanel fsiPanel56;
        private common.FsiPanel fsiPanel46;
        private common.FsiPanel fsiPanel55;
        private common.FsiPanel fsiPanel45;
        private common.FsiPanel fsiPanel54;
        private common.FsiPanel fsiPanel44;
        private common.FsiPanel fsiPanel53;
        private common.FsiPanel fsiPanel27;
        private common.FsiPanel fsiPanel62;
        private common.FsiPanel fsiPanel70;
        private common.FsiPanel fsiPanel69;
        private common.FsiPanel fsiPanel68;
        private common.FsiPanel fsiPanel67;
        private common.FsiPanel fsiPanel66;
        private common.FsiPanel fsiPanel65;
        private common.FsiPanel fsiPanel64;
        private common.FsiPanel fsiPanel63;
        private common.FsiPanel fsiPanel72;
        private common.FsiPanel fsiPanel74;
        private common.FsiPanel fsiPanel71;
        private common.FsiPanel fsiPanel73;
        private common.FsiPanel fsiPanel76;
        private common.FsiPanel fsiPanel75;
        private common.FsiPanel fsiPanel77;
    }
}