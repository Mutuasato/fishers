﻿namespace jp.co.fsi.zm.zmyr1011
{
    partial class ZMYR1013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.F1 = new System.Windows.Forms.TabPage();
            this.mtbListF1 = new System.Windows.Forms.DataGridView();
            this.F2 = new System.Windows.Forms.TabPage();
            this.mtbListF2 = new System.Windows.Forms.DataGridView();
            this.F3 = new System.Windows.Forms.TabPage();
            this.mtbListF3 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKakkoKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblkakkoShubetsu = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKakkoHyoji = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKakkoHyoji = new System.Windows.Forms.Label();
            this.lblMemo = new System.Windows.Forms.Label();
            this.txtMojiShubetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMoji = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.lbxKanjoKamokuIchiran = new System.Windows.Forms.ListBox();
            this.lbxTaishoKanjoKamoku = new System.Windows.Forms.ListBox();
            this.rdoKashi = new System.Windows.Forms.RadioButton();
            this.rdoKari = new System.Windows.Forms.RadioButton();
            this.txtKamokuNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.F3VisibleHandan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3kamokuCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3gyoBango = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3Taishaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3Kakko = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3deleteAndAddNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3HyojiJuni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3KAKKO_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3KAKKO_HYOJI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3MOJI_SHUBETSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2VisibleHandan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2kamokuCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2gyoBango = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2Taishaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2Kakko = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2deleteAndAddNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2HyojiJuni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2KAKKO_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2KAKKO_HYOJI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2MOJI_SHUBETSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1VisibleHandan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1kamokuCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1gyoBango = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1Taishaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1Kakko = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1deleteAndAddNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1HyojiJuni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1KAKKO_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1KAKKO_HYOJI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1MOJI_SHUBETSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDebug.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.F1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF1)).BeginInit();
            this.F2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF2)).BeginInit();
            this.F3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF3)).BeginInit();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(4, 65);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF1
            // 
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(89, 65);
            this.btnF6.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 647);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1107, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1096, 31);
            this.lblTitle.Text = "総勘定元帳 [印字設定]";
            this.lblTitle.Visible = false;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.F1);
            this.tabControl.Controls.Add(this.F2);
            this.tabControl.Controls.Add(this.F3);
            this.tabControl.Location = new System.Drawing.Point(8, 16);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(512, 689);
            this.tabControl.TabIndex = 1;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // F1
            // 
            this.F1.Controls.Add(this.mtbListF1);
            this.F1.Location = new System.Drawing.Point(4, 26);
            this.F1.Margin = new System.Windows.Forms.Padding(4);
            this.F1.Name = "F1";
            this.F1.Padding = new System.Windows.Forms.Padding(4);
            this.F1.Size = new System.Drawing.Size(504, 659);
            this.F1.TabIndex = 0;
            this.F1.Text = "F1：貸借対照表";
            this.F1.UseVisualStyleBackColor = true;
            // 
            // mtbListF1
            // 
            this.mtbListF1.AllowUserToAddRows = false;
            this.mtbListF1.AllowUserToDeleteRows = false;
            this.mtbListF1.AllowUserToResizeRows = false;
            this.mtbListF1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbListF1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.mtbListF1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbListF1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F1VisibleHandan,
            this.F1kamokuCd,
            this.F1KamokuNm,
            this.F1gyoBango,
            this.F1Taishaku,
            this.F1Kakko,
            this.F1deleteAndAddNo,
            this.F1HyojiJuni,
            this.F1KAKKO_KUBUN,
            this.F1KAKKO_HYOJI,
            this.F1MOJI_SHUBETSU});
            this.mtbListF1.EnableHeadersVisualStyles = false;
            this.mtbListF1.Location = new System.Drawing.Point(8, 8);
            this.mtbListF1.Margin = new System.Windows.Forms.Padding(5);
            this.mtbListF1.MultiSelect = false;
            this.mtbListF1.Name = "mtbListF1";
            this.mtbListF1.ReadOnly = true;
            this.mtbListF1.RowHeadersVisible = false;
            this.mtbListF1.RowTemplate.Height = 21;
            this.mtbListF1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbListF1.Size = new System.Drawing.Size(485, 640);
            this.mtbListF1.TabIndex = 3;
            this.mtbListF1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellClick);
            // 
            // F2
            // 
            this.F2.Controls.Add(this.mtbListF2);
            this.F2.Location = new System.Drawing.Point(4, 26);
            this.F2.Margin = new System.Windows.Forms.Padding(4);
            this.F2.Name = "F2";
            this.F2.Padding = new System.Windows.Forms.Padding(4);
            this.F2.Size = new System.Drawing.Size(504, 659);
            this.F2.TabIndex = 1;
            this.F2.Text = "F2：損益計算書";
            this.F2.UseVisualStyleBackColor = true;
            // 
            // mtbListF2
            // 
            this.mtbListF2.AllowUserToAddRows = false;
            this.mtbListF2.AllowUserToDeleteRows = false;
            this.mtbListF2.AllowUserToResizeRows = false;
            this.mtbListF2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbListF2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.mtbListF2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbListF2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F2VisibleHandan,
            this.F2kamokuCd,
            this.F2KamokuNm,
            this.F2gyoBango,
            this.F2Taishaku,
            this.F2Kakko,
            this.F2deleteAndAddNo,
            this.F2HyojiJuni,
            this.F2KAKKO_KUBUN,
            this.F2KAKKO_HYOJI,
            this.F2MOJI_SHUBETSU});
            this.mtbListF2.EnableHeadersVisualStyles = false;
            this.mtbListF2.Location = new System.Drawing.Point(8, 8);
            this.mtbListF2.Margin = new System.Windows.Forms.Padding(4);
            this.mtbListF2.MultiSelect = false;
            this.mtbListF2.Name = "mtbListF2";
            this.mtbListF2.ReadOnly = true;
            this.mtbListF2.RowHeadersVisible = false;
            this.mtbListF2.RowTemplate.Height = 21;
            this.mtbListF2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbListF2.Size = new System.Drawing.Size(485, 640);
            this.mtbListF2.TabIndex = 4;
            this.mtbListF2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellClick);
            // 
            // F3
            // 
            this.F3.Controls.Add(this.mtbListF3);
            this.F3.Location = new System.Drawing.Point(4, 26);
            this.F3.Margin = new System.Windows.Forms.Padding(4);
            this.F3.Name = "F3";
            this.F3.Size = new System.Drawing.Size(504, 659);
            this.F3.TabIndex = 2;
            this.F3.Text = "F3：製造原価";
            this.F3.UseVisualStyleBackColor = true;
            // 
            // mtbListF3
            // 
            this.mtbListF3.AllowUserToAddRows = false;
            this.mtbListF3.AllowUserToDeleteRows = false;
            this.mtbListF3.AllowUserToResizeRows = false;
            this.mtbListF3.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbListF3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.mtbListF3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbListF3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F3VisibleHandan,
            this.F3kamokuCd,
            this.F3KamokuNm,
            this.F3gyoBango,
            this.F3Taishaku,
            this.F3Kakko,
            this.F3deleteAndAddNo,
            this.F3HyojiJuni,
            this.F3KAKKO_KUBUN,
            this.F3KAKKO_HYOJI,
            this.F3MOJI_SHUBETSU});
            this.mtbListF3.EnableHeadersVisualStyles = false;
            this.mtbListF3.Location = new System.Drawing.Point(8, 8);
            this.mtbListF3.Margin = new System.Windows.Forms.Padding(4);
            this.mtbListF3.MultiSelect = false;
            this.mtbListF3.Name = "mtbListF3";
            this.mtbListF3.ReadOnly = true;
            this.mtbListF3.RowHeadersVisible = false;
            this.mtbListF3.RowTemplate.Height = 21;
            this.mtbListF3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbListF3.Size = new System.Drawing.Size(485, 640);
            this.mtbListF3.TabIndex = 5;
            this.mtbListF3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellClick);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.LightCyan;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(176, 4);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(242, 24);
            this.label3.TabIndex = 8;
            this.label3.Tag = "DISPNAME";
            this.label3.Text = "0：無　1：()　2：【】　3：[]";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKakkoKubun
            // 
            this.txtKakkoKubun.AutoSizeFromLength = false;
            this.txtKakkoKubun.DisplayLength = null;
            this.txtKakkoKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKakkoKubun.ForeColor = System.Drawing.Color.Black;
            this.txtKakkoKubun.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKakkoKubun.Location = new System.Drawing.Point(129, 5);
            this.txtKakkoKubun.Margin = new System.Windows.Forms.Padding(5);
            this.txtKakkoKubun.MaxLength = 1;
            this.txtKakkoKubun.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtKakkoKubun.Name = "txtKakkoKubun";
            this.txtKakkoKubun.Size = new System.Drawing.Size(39, 23);
            this.txtKakkoKubun.TabIndex = 0;
            this.txtKakkoKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKakkoKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtKakkoKubun_Validating);
            // 
            // lblkakkoShubetsu
            // 
            this.lblkakkoShubetsu.BackColor = System.Drawing.Color.Silver;
            this.lblkakkoShubetsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblkakkoShubetsu.ForeColor = System.Drawing.Color.Black;
            this.lblkakkoShubetsu.Location = new System.Drawing.Point(0, 0);
            this.lblkakkoShubetsu.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblkakkoShubetsu.Name = "lblkakkoShubetsu";
            this.lblkakkoShubetsu.Size = new System.Drawing.Size(550, 36);
            this.lblkakkoShubetsu.TabIndex = 6;
            this.lblkakkoShubetsu.Tag = "CHANGE";
            this.lblkakkoShubetsu.Text = "括弧種別";
            this.lblkakkoShubetsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightCyan;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(177, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 24);
            this.label1.TabIndex = 5;
            this.label1.Tag = "DISPNAME";
            this.label1.Text = "0：常時　1：ゼロ時表示無";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKakkoHyoji
            // 
            this.txtKakkoHyoji.AutoSizeFromLength = false;
            this.txtKakkoHyoji.DisplayLength = null;
            this.txtKakkoHyoji.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKakkoHyoji.ForeColor = System.Drawing.Color.Black;
            this.txtKakkoHyoji.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKakkoHyoji.Location = new System.Drawing.Point(129, 9);
            this.txtKakkoHyoji.Margin = new System.Windows.Forms.Padding(5);
            this.txtKakkoHyoji.MaxLength = 1;
            this.txtKakkoHyoji.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtKakkoHyoji.Name = "txtKakkoHyoji";
            this.txtKakkoHyoji.Size = new System.Drawing.Size(39, 23);
            this.txtKakkoHyoji.TabIndex = 1;
            this.txtKakkoHyoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKakkoHyoji.Validating += new System.ComponentModel.CancelEventHandler(this.txtKakkoHyoji_Validating);
            // 
            // lblKakkoHyoji
            // 
            this.lblKakkoHyoji.BackColor = System.Drawing.Color.Silver;
            this.lblKakkoHyoji.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKakkoHyoji.ForeColor = System.Drawing.Color.Black;
            this.lblKakkoHyoji.Location = new System.Drawing.Point(0, 0);
            this.lblKakkoHyoji.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblKakkoHyoji.Name = "lblKakkoHyoji";
            this.lblKakkoHyoji.Size = new System.Drawing.Size(550, 40);
            this.lblKakkoHyoji.TabIndex = 3;
            this.lblKakkoHyoji.Tag = "CHANGE";
            this.lblKakkoHyoji.Text = "括弧表示";
            this.lblKakkoHyoji.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMemo
            // 
            this.lblMemo.BackColor = System.Drawing.Color.LightCyan;
            this.lblMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMemo.ForeColor = System.Drawing.Color.Black;
            this.lblMemo.Location = new System.Drawing.Point(176, 7);
            this.lblMemo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblMemo.Name = "lblMemo";
            this.lblMemo.Size = new System.Drawing.Size(241, 24);
            this.lblMemo.TabIndex = 2;
            this.lblMemo.Tag = "DISPNAME";
            this.lblMemo.Text = "0：標準　1：太字";
            this.lblMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMojiShubetsu
            // 
            this.txtMojiShubetsu.AutoSizeFromLength = false;
            this.txtMojiShubetsu.DisplayLength = null;
            this.txtMojiShubetsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMojiShubetsu.ForeColor = System.Drawing.Color.Black;
            this.txtMojiShubetsu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMojiShubetsu.Location = new System.Drawing.Point(129, 7);
            this.txtMojiShubetsu.Margin = new System.Windows.Forms.Padding(5);
            this.txtMojiShubetsu.MaxLength = 1;
            this.txtMojiShubetsu.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMojiShubetsu.Name = "txtMojiShubetsu";
            this.txtMojiShubetsu.Size = new System.Drawing.Size(39, 23);
            this.txtMojiShubetsu.TabIndex = 2;
            this.txtMojiShubetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMojiShubetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtMojiShubetsu_Validating);
            // 
            // lblMoji
            // 
            this.lblMoji.BackColor = System.Drawing.Color.Silver;
            this.lblMoji.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMoji.ForeColor = System.Drawing.Color.Black;
            this.lblMoji.Location = new System.Drawing.Point(0, 0);
            this.lblMoji.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblMoji.Name = "lblMoji";
            this.lblMoji.Size = new System.Drawing.Size(550, 37);
            this.lblMoji.TabIndex = 0;
            this.lblMoji.Tag = "CHANGE";
            this.lblMoji.Text = "文字スタイル";
            this.lblMoji.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAdd.Location = new System.Drawing.Point(324, 0);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(53, 53);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "▲";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnUMove_Click);
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnDel.Location = new System.Drawing.Point(181, 0);
            this.btnDel.Margin = new System.Windows.Forms.Padding(4);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(53, 53);
            this.btnDel.TabIndex = 2;
            this.btnDel.Text = "▼";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDMove_Click);
            // 
            // lbxKanjoKamokuIchiran
            // 
            this.lbxKanjoKamokuIchiran.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxKanjoKamokuIchiran.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lbxKanjoKamokuIchiran.FormattingEnabled = true;
            this.lbxKanjoKamokuIchiran.ItemHeight = 16;
            this.lbxKanjoKamokuIchiran.Location = new System.Drawing.Point(0, 35);
            this.lbxKanjoKamokuIchiran.Margin = new System.Windows.Forms.Padding(5);
            this.lbxKanjoKamokuIchiran.Name = "lbxKanjoKamokuIchiran";
            this.lbxKanjoKamokuIchiran.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxKanjoKamokuIchiran.Size = new System.Drawing.Size(553, 155);
            this.lbxKanjoKamokuIchiran.TabIndex = 0;
            // 
            // lbxTaishoKanjoKamoku
            // 
            this.lbxTaishoKanjoKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxTaishoKanjoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lbxTaishoKanjoKamoku.FormattingEnabled = true;
            this.lbxTaishoKanjoKamoku.ItemHeight = 16;
            this.lbxTaishoKanjoKamoku.Location = new System.Drawing.Point(0, 35);
            this.lbxTaishoKanjoKamoku.Margin = new System.Windows.Forms.Padding(5);
            this.lbxTaishoKanjoKamoku.Name = "lbxTaishoKanjoKamoku";
            this.lbxTaishoKanjoKamoku.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxTaishoKanjoKamoku.Size = new System.Drawing.Size(553, 116);
            this.lbxTaishoKanjoKamoku.TabIndex = 0;
            // 
            // rdoKashi
            // 
            this.rdoKashi.AutoSize = true;
            this.rdoKashi.BackColor = System.Drawing.Color.Silver;
            this.rdoKashi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoKashi.ForeColor = System.Drawing.Color.Black;
            this.rdoKashi.Location = new System.Drawing.Point(459, 7);
            this.rdoKashi.Margin = new System.Windows.Forms.Padding(5);
            this.rdoKashi.Name = "rdoKashi";
            this.rdoKashi.Size = new System.Drawing.Size(58, 20);
            this.rdoKashi.TabIndex = 1;
            this.rdoKashi.TabStop = true;
            this.rdoKashi.Text = "貸方";
            this.rdoKashi.UseVisualStyleBackColor = false;
            this.rdoKashi.Click += new System.EventHandler(this.rdoKashi_Click);
            // 
            // rdoKari
            // 
            this.rdoKari.AutoSize = true;
            this.rdoKari.BackColor = System.Drawing.Color.Silver;
            this.rdoKari.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdoKari.ForeColor = System.Drawing.Color.Black;
            this.rdoKari.Location = new System.Drawing.Point(380, 7);
            this.rdoKari.Margin = new System.Windows.Forms.Padding(5);
            this.rdoKari.Name = "rdoKari";
            this.rdoKari.Size = new System.Drawing.Size(58, 20);
            this.rdoKari.TabIndex = 0;
            this.rdoKari.TabStop = true;
            this.rdoKari.Text = "借方";
            this.rdoKari.UseVisualStyleBackColor = false;
            this.rdoKari.Click += new System.EventHandler(this.rdoKari_Click);
            // 
            // txtKamokuNm
            // 
            this.txtKamokuNm.AutoSizeFromLength = false;
            this.txtKamokuNm.DisplayLength = null;
            this.txtKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuNm.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKamokuNm.Location = new System.Drawing.Point(101, 3);
            this.txtKamokuNm.Margin = new System.Windows.Forms.Padding(5);
            this.txtKamokuNm.MaxLength = 30;
            this.txtKamokuNm.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtKamokuNm.Name = "txtKamokuNm";
            this.txtKamokuNm.Size = new System.Drawing.Size(269, 20);
            this.txtKamokuNm.TabIndex = 0;
            this.txtKamokuNm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.kamokuNm_change);
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(528, 500);
            this.fsiTableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 3;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.35294F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.29412F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.35294F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(560, 141);
            this.fsiTableLayoutPanel2.TabIndex = 4;
            this.fsiTableLayoutPanel2.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtMojiShubetsu);
            this.fsiPanel3.Controls.Add(this.lblMemo);
            this.fsiPanel3.Controls.Add(this.lblMoji);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 99);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(550, 37);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtKakkoHyoji);
            this.fsiPanel2.Controls.Add(this.label1);
            this.fsiPanel2.Controls.Add(this.lblKakkoHyoji);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 50);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(550, 40);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.label3);
            this.fsiPanel1.Controls.Add(this.txtKakkoKubun);
            this.fsiPanel1.Controls.Add(this.lblkakkoShubetsu);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(550, 36);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 1);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(528, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(563, 447);
            this.fsiTableLayoutPanel1.TabIndex = 5;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.rdoKashi);
            this.fsiPanel4.Controls.Add(this.txtKamokuNm);
            this.fsiPanel4.Controls.Add(this.rdoKari);
            this.fsiPanel4.Controls.Add(this.label2);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(553, 36);
            this.fsiPanel4.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(553, 36);
            this.label2.TabIndex = 7;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "要約設定";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.fsiPanel8);
            this.fsiPanel5.Controls.Add(this.fsiPanel7);
            this.fsiPanel5.Controls.Add(this.fsiPanel6);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(5, 50);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(553, 392);
            this.fsiPanel5.TabIndex = 9;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.lbxKanjoKamokuIchiran);
            this.fsiPanel8.Controls.Add(this.label5);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(0, 202);
            this.fsiPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(553, 190);
            this.fsiPanel8.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(553, 35);
            this.label5.TabIndex = 7;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "勘定科目一覧";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.btnAdd);
            this.fsiPanel7.Controls.Add(this.btnDel);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.fsiPanel7.Location = new System.Drawing.Point(0, 151);
            this.fsiPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(553, 51);
            this.fsiPanel7.TabIndex = 9;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.lbxTaishoKanjoKamoku);
            this.fsiPanel6.Controls.Add(this.label4);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.fsiPanel6.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(553, 151);
            this.fsiPanel6.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(553, 35);
            this.label4.TabIndex = 8;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "対象勘定科目";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // F3VisibleHandan
            // 
            this.F3VisibleHandan.HeaderText = "Visible判断";
            this.F3VisibleHandan.Name = "F3VisibleHandan";
            this.F3VisibleHandan.ReadOnly = true;
            this.F3VisibleHandan.Visible = false;
            // 
            // F3kamokuCd
            // 
            this.F3kamokuCd.HeaderText = "科目コード";
            this.F3kamokuCd.Name = "F3kamokuCd";
            this.F3kamokuCd.ReadOnly = true;
            this.F3kamokuCd.Visible = false;
            // 
            // F3KamokuNm
            // 
            this.F3KamokuNm.HeaderText = "科　目　名";
            this.F3KamokuNm.Name = "F3KamokuNm";
            this.F3KamokuNm.ReadOnly = true;
            this.F3KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F3KamokuNm.Width = 250;
            // 
            // F3gyoBango
            // 
            this.F3gyoBango.HeaderText = "行番号";
            this.F3gyoBango.Name = "F3gyoBango";
            this.F3gyoBango.ReadOnly = true;
            this.F3gyoBango.Visible = false;
            // 
            // F3Taishaku
            // 
            this.F3Taishaku.HeaderText = "貸借";
            this.F3Taishaku.Name = "F3Taishaku";
            this.F3Taishaku.ReadOnly = true;
            this.F3Taishaku.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F3Taishaku.Width = 50;
            // 
            // F3Kakko
            // 
            this.F3Kakko.HeaderText = "括弧";
            this.F3Kakko.Name = "F3Kakko";
            this.F3Kakko.ReadOnly = true;
            this.F3Kakko.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F3Kakko.Width = 50;
            // 
            // F3deleteAndAddNo
            // 
            this.F3deleteAndAddNo.HeaderText = "削除と追加用行番号";
            this.F3deleteAndAddNo.Name = "F3deleteAndAddNo";
            this.F3deleteAndAddNo.ReadOnly = true;
            this.F3deleteAndAddNo.Visible = false;
            // 
            // F3HyojiJuni
            // 
            this.F3HyojiJuni.HeaderText = "表示順位";
            this.F3HyojiJuni.Name = "F3HyojiJuni";
            this.F3HyojiJuni.ReadOnly = true;
            this.F3HyojiJuni.Visible = false;
            // 
            // F3KAKKO_KUBUN
            // 
            this.F3KAKKO_KUBUN.HeaderText = "括弧区分";
            this.F3KAKKO_KUBUN.Name = "F3KAKKO_KUBUN";
            this.F3KAKKO_KUBUN.ReadOnly = true;
            this.F3KAKKO_KUBUN.Visible = false;
            // 
            // F3KAKKO_HYOJI
            // 
            this.F3KAKKO_HYOJI.HeaderText = "括弧表示";
            this.F3KAKKO_HYOJI.Name = "F3KAKKO_HYOJI";
            this.F3KAKKO_HYOJI.ReadOnly = true;
            this.F3KAKKO_HYOJI.Visible = false;
            // 
            // F3MOJI_SHUBETSU
            // 
            this.F3MOJI_SHUBETSU.HeaderText = "文字種別";
            this.F3MOJI_SHUBETSU.Name = "F3MOJI_SHUBETSU";
            this.F3MOJI_SHUBETSU.ReadOnly = true;
            this.F3MOJI_SHUBETSU.Visible = false;
            // 
            // F2VisibleHandan
            // 
            this.F2VisibleHandan.HeaderText = "Visible判断";
            this.F2VisibleHandan.Name = "F2VisibleHandan";
            this.F2VisibleHandan.ReadOnly = true;
            this.F2VisibleHandan.Visible = false;
            // 
            // F2kamokuCd
            // 
            this.F2kamokuCd.HeaderText = "科目コード";
            this.F2kamokuCd.Name = "F2kamokuCd";
            this.F2kamokuCd.ReadOnly = true;
            this.F2kamokuCd.Visible = false;
            // 
            // F2KamokuNm
            // 
            this.F2KamokuNm.HeaderText = "科　目　名";
            this.F2KamokuNm.Name = "F2KamokuNm";
            this.F2KamokuNm.ReadOnly = true;
            this.F2KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F2KamokuNm.Width = 250;
            // 
            // F2gyoBango
            // 
            this.F2gyoBango.HeaderText = "行番号";
            this.F2gyoBango.Name = "F2gyoBango";
            this.F2gyoBango.ReadOnly = true;
            this.F2gyoBango.Visible = false;
            // 
            // F2Taishaku
            // 
            this.F2Taishaku.HeaderText = "貸借";
            this.F2Taishaku.Name = "F2Taishaku";
            this.F2Taishaku.ReadOnly = true;
            this.F2Taishaku.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F2Taishaku.Width = 50;
            // 
            // F2Kakko
            // 
            this.F2Kakko.HeaderText = "括弧";
            this.F2Kakko.Name = "F2Kakko";
            this.F2Kakko.ReadOnly = true;
            this.F2Kakko.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F2Kakko.Width = 50;
            // 
            // F2deleteAndAddNo
            // 
            this.F2deleteAndAddNo.HeaderText = "削除と追加用行番号";
            this.F2deleteAndAddNo.Name = "F2deleteAndAddNo";
            this.F2deleteAndAddNo.ReadOnly = true;
            this.F2deleteAndAddNo.Visible = false;
            // 
            // F2HyojiJuni
            // 
            this.F2HyojiJuni.HeaderText = "表示順位";
            this.F2HyojiJuni.Name = "F2HyojiJuni";
            this.F2HyojiJuni.ReadOnly = true;
            this.F2HyojiJuni.Visible = false;
            // 
            // F2KAKKO_KUBUN
            // 
            this.F2KAKKO_KUBUN.HeaderText = "括弧区分";
            this.F2KAKKO_KUBUN.Name = "F2KAKKO_KUBUN";
            this.F2KAKKO_KUBUN.ReadOnly = true;
            this.F2KAKKO_KUBUN.Visible = false;
            // 
            // F2KAKKO_HYOJI
            // 
            this.F2KAKKO_HYOJI.HeaderText = "括弧表示";
            this.F2KAKKO_HYOJI.Name = "F2KAKKO_HYOJI";
            this.F2KAKKO_HYOJI.ReadOnly = true;
            this.F2KAKKO_HYOJI.Visible = false;
            // 
            // F2MOJI_SHUBETSU
            // 
            this.F2MOJI_SHUBETSU.HeaderText = "文字種別";
            this.F2MOJI_SHUBETSU.Name = "F2MOJI_SHUBETSU";
            this.F2MOJI_SHUBETSU.ReadOnly = true;
            this.F2MOJI_SHUBETSU.Visible = false;
            // 
            // F1VisibleHandan
            // 
            this.F1VisibleHandan.HeaderText = "Visible判断";
            this.F1VisibleHandan.Name = "F1VisibleHandan";
            this.F1VisibleHandan.ReadOnly = true;
            this.F1VisibleHandan.Visible = false;
            // 
            // F1kamokuCd
            // 
            this.F1kamokuCd.HeaderText = "科目コード";
            this.F1kamokuCd.Name = "F1kamokuCd";
            this.F1kamokuCd.ReadOnly = true;
            this.F1kamokuCd.Visible = false;
            // 
            // F1KamokuNm
            // 
            this.F1KamokuNm.HeaderText = "科　目　名";
            this.F1KamokuNm.Name = "F1KamokuNm";
            this.F1KamokuNm.ReadOnly = true;
            this.F1KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F1KamokuNm.Width = 250;
            // 
            // F1gyoBango
            // 
            this.F1gyoBango.HeaderText = "行番号";
            this.F1gyoBango.Name = "F1gyoBango";
            this.F1gyoBango.ReadOnly = true;
            this.F1gyoBango.Visible = false;
            // 
            // F1Taishaku
            // 
            this.F1Taishaku.HeaderText = "貸借";
            this.F1Taishaku.Name = "F1Taishaku";
            this.F1Taishaku.ReadOnly = true;
            this.F1Taishaku.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F1Taishaku.Width = 50;
            // 
            // F1Kakko
            // 
            this.F1Kakko.HeaderText = "括弧";
            this.F1Kakko.Name = "F1Kakko";
            this.F1Kakko.ReadOnly = true;
            this.F1Kakko.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F1Kakko.Width = 50;
            // 
            // F1deleteAndAddNo
            // 
            this.F1deleteAndAddNo.HeaderText = "削除と追加用行番号";
            this.F1deleteAndAddNo.Name = "F1deleteAndAddNo";
            this.F1deleteAndAddNo.ReadOnly = true;
            this.F1deleteAndAddNo.Visible = false;
            // 
            // F1HyojiJuni
            // 
            this.F1HyojiJuni.HeaderText = "表示順位";
            this.F1HyojiJuni.Name = "F1HyojiJuni";
            this.F1HyojiJuni.ReadOnly = true;
            this.F1HyojiJuni.Visible = false;
            // 
            // F1KAKKO_KUBUN
            // 
            this.F1KAKKO_KUBUN.HeaderText = "括弧区分";
            this.F1KAKKO_KUBUN.Name = "F1KAKKO_KUBUN";
            this.F1KAKKO_KUBUN.ReadOnly = true;
            this.F1KAKKO_KUBUN.Visible = false;
            // 
            // F1KAKKO_HYOJI
            // 
            this.F1KAKKO_HYOJI.HeaderText = "括弧表示";
            this.F1KAKKO_HYOJI.Name = "F1KAKKO_HYOJI";
            this.F1KAKKO_HYOJI.ReadOnly = true;
            this.F1KAKKO_HYOJI.Visible = false;
            // 
            // F1MOJI_SHUBETSU
            // 
            this.F1MOJI_SHUBETSU.HeaderText = "文字種別";
            this.F1MOJI_SHUBETSU.Name = "F1MOJI_SHUBETSU";
            this.F1MOJI_SHUBETSU.ReadOnly = true;
            this.F1MOJI_SHUBETSU.Visible = false;
            // 
            // ZMYR1013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1096, 784);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.tabControl);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMYR1013";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.tabControl, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.pnlDebug.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.F1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF1)).EndInit();
            this.F2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF2)).EndInit();
            this.F3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF3)).EndInit();
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage F1;
        private System.Windows.Forms.TabPage F2;
        private System.Windows.Forms.TabPage F3;
        private System.Windows.Forms.RadioButton rdoKashi;
        private System.Windows.Forms.RadioButton rdoKari;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuNm;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Label lblMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtMojiShubetsu;
        private System.Windows.Forms.Label lblMoji;
        private System.Windows.Forms.ListBox lbxKanjoKamokuIchiran;
        private System.Windows.Forms.ListBox lbxTaishoKanjoKamoku;
        private System.Windows.Forms.DataGridView mtbListF1;
        private System.Windows.Forms.DataGridView mtbListF2;
        private System.Windows.Forms.DataGridView mtbListF3;
        private System.Windows.Forms.Label label3;
        private jp.co.fsi.common.controls.FsiTextBox txtKakkoKubun;
        private System.Windows.Forms.Label lblkakkoShubetsu;
        private System.Windows.Forms.Label label1;
        private jp.co.fsi.common.controls.FsiTextBox txtKakkoHyoji;
        private System.Windows.Forms.Label lblKakkoHyoji;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel4;
		private System.Windows.Forms.Label label2;
		private common.FsiPanel fsiPanel5;
		private common.FsiPanel fsiPanel8;
		private System.Windows.Forms.Label label5;
		private common.FsiPanel fsiPanel7;
		private common.FsiPanel fsiPanel6;
		private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2VisibleHandan;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2kamokuCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2gyoBango;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2Taishaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2Kakko;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2deleteAndAddNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2HyojiJuni;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2KAKKO_KUBUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2KAKKO_HYOJI;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2MOJI_SHUBETSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3VisibleHandan;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3kamokuCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3gyoBango;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3Taishaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3Kakko;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3deleteAndAddNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3HyojiJuni;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3KAKKO_KUBUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3KAKKO_HYOJI;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3MOJI_SHUBETSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1VisibleHandan;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1kamokuCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1gyoBango;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1Taishaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1Kakko;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1deleteAndAddNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1HyojiJuni;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1KAKKO_KUBUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1KAKKO_HYOJI;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1MOJI_SHUBETSU;
    }
}