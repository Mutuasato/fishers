﻿namespace jp.co.fsi.zm.zmyr1011
{
    partial class ZMYR1016
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRiekiShobunanChuki20 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki19 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki18 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki17 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki16 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki15 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki13 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki12 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki11 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel20 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel19 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel18 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel17 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel16 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel15 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel14 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel13 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel20.SuspendLayout();
            this.fsiPanel19.SuspendLayout();
            this.fsiPanel18.SuspendLayout();
            this.fsiPanel17.SuspendLayout();
            this.fsiPanel16.SuspendLayout();
            this.fsiPanel15.SuspendLayout();
            this.fsiPanel14.SuspendLayout();
            this.fsiPanel13.SuspendLayout();
            this.fsiPanel12.SuspendLayout();
            this.fsiPanel11.SuspendLayout();
            this.fsiPanel10.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 647);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1173, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1162, 31);
            this.lblTitle.Text = "";
            // 
            // txtRiekiShobunanChuki20
            // 
            this.txtRiekiShobunanChuki20.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki20.DisplayLength = null;
            this.txtRiekiShobunanChuki20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki20.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki20.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki20.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki20.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki20.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki20.MaxLength = 128;
            this.txtRiekiShobunanChuki20.Name = "txtRiekiShobunanChuki20";
            this.txtRiekiShobunanChuki20.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki20.TabIndex = 20;
            this.txtRiekiShobunanChuki20.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki19
            // 
            this.txtRiekiShobunanChuki19.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki19.DisplayLength = null;
            this.txtRiekiShobunanChuki19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki19.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki19.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki19.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki19.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki19.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki19.MaxLength = 128;
            this.txtRiekiShobunanChuki19.Name = "txtRiekiShobunanChuki19";
            this.txtRiekiShobunanChuki19.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki19.TabIndex = 19;
            this.txtRiekiShobunanChuki19.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki18
            // 
            this.txtRiekiShobunanChuki18.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki18.DisplayLength = null;
            this.txtRiekiShobunanChuki18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki18.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki18.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki18.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki18.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki18.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki18.MaxLength = 128;
            this.txtRiekiShobunanChuki18.Name = "txtRiekiShobunanChuki18";
            this.txtRiekiShobunanChuki18.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki18.TabIndex = 18;
            this.txtRiekiShobunanChuki18.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki17
            // 
            this.txtRiekiShobunanChuki17.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki17.DisplayLength = null;
            this.txtRiekiShobunanChuki17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki17.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki17.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki17.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki17.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki17.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki17.MaxLength = 128;
            this.txtRiekiShobunanChuki17.Name = "txtRiekiShobunanChuki17";
            this.txtRiekiShobunanChuki17.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki17.TabIndex = 17;
            this.txtRiekiShobunanChuki17.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki16
            // 
            this.txtRiekiShobunanChuki16.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki16.DisplayLength = null;
            this.txtRiekiShobunanChuki16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki16.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki16.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki16.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki16.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki16.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki16.MaxLength = 128;
            this.txtRiekiShobunanChuki16.Name = "txtRiekiShobunanChuki16";
            this.txtRiekiShobunanChuki16.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki16.TabIndex = 16;
            this.txtRiekiShobunanChuki16.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki15
            // 
            this.txtRiekiShobunanChuki15.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki15.DisplayLength = null;
            this.txtRiekiShobunanChuki15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki15.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki15.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki15.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki15.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki15.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki15.MaxLength = 128;
            this.txtRiekiShobunanChuki15.Name = "txtRiekiShobunanChuki15";
            this.txtRiekiShobunanChuki15.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki15.TabIndex = 15;
            this.txtRiekiShobunanChuki15.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki14
            // 
            this.txtRiekiShobunanChuki14.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki14.DisplayLength = null;
            this.txtRiekiShobunanChuki14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki14.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki14.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki14.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki14.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki14.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki14.MaxLength = 128;
            this.txtRiekiShobunanChuki14.Name = "txtRiekiShobunanChuki14";
            this.txtRiekiShobunanChuki14.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki14.TabIndex = 14;
            this.txtRiekiShobunanChuki14.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki13
            // 
            this.txtRiekiShobunanChuki13.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki13.DisplayLength = null;
            this.txtRiekiShobunanChuki13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki13.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki13.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki13.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki13.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki13.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki13.MaxLength = 128;
            this.txtRiekiShobunanChuki13.Name = "txtRiekiShobunanChuki13";
            this.txtRiekiShobunanChuki13.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki13.TabIndex = 13;
            this.txtRiekiShobunanChuki13.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki12
            // 
            this.txtRiekiShobunanChuki12.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki12.DisplayLength = null;
            this.txtRiekiShobunanChuki12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki12.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki12.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki12.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki12.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki12.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki12.MaxLength = 128;
            this.txtRiekiShobunanChuki12.Name = "txtRiekiShobunanChuki12";
            this.txtRiekiShobunanChuki12.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki12.TabIndex = 12;
            this.txtRiekiShobunanChuki12.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki11
            // 
            this.txtRiekiShobunanChuki11.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki11.DisplayLength = null;
            this.txtRiekiShobunanChuki11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki11.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki11.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki11.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki11.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki11.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki11.MaxLength = 128;
            this.txtRiekiShobunanChuki11.Name = "txtRiekiShobunanChuki11";
            this.txtRiekiShobunanChuki11.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki11.TabIndex = 11;
            this.txtRiekiShobunanChuki11.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki10
            // 
            this.txtRiekiShobunanChuki10.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki10.DisplayLength = null;
            this.txtRiekiShobunanChuki10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki10.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki10.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki10.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki10.MaxLength = 128;
            this.txtRiekiShobunanChuki10.Name = "txtRiekiShobunanChuki10";
            this.txtRiekiShobunanChuki10.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki10.TabIndex = 10;
            this.txtRiekiShobunanChuki10.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki9
            // 
            this.txtRiekiShobunanChuki9.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki9.DisplayLength = null;
            this.txtRiekiShobunanChuki9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki9.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki9.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki9.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki9.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki9.MaxLength = 128;
            this.txtRiekiShobunanChuki9.Name = "txtRiekiShobunanChuki9";
            this.txtRiekiShobunanChuki9.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki9.TabIndex = 9;
            this.txtRiekiShobunanChuki9.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki8
            // 
            this.txtRiekiShobunanChuki8.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki8.DisplayLength = null;
            this.txtRiekiShobunanChuki8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki8.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki8.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki8.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki8.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki8.MaxLength = 128;
            this.txtRiekiShobunanChuki8.Name = "txtRiekiShobunanChuki8";
            this.txtRiekiShobunanChuki8.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki8.TabIndex = 8;
            this.txtRiekiShobunanChuki8.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki7
            // 
            this.txtRiekiShobunanChuki7.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki7.DisplayLength = null;
            this.txtRiekiShobunanChuki7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki7.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki7.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki7.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki7.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki7.MaxLength = 128;
            this.txtRiekiShobunanChuki7.Name = "txtRiekiShobunanChuki7";
            this.txtRiekiShobunanChuki7.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki7.TabIndex = 7;
            this.txtRiekiShobunanChuki7.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki6
            // 
            this.txtRiekiShobunanChuki6.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki6.DisplayLength = null;
            this.txtRiekiShobunanChuki6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki6.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki6.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki6.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki6.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki6.MaxLength = 128;
            this.txtRiekiShobunanChuki6.Name = "txtRiekiShobunanChuki6";
            this.txtRiekiShobunanChuki6.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki6.TabIndex = 6;
            this.txtRiekiShobunanChuki6.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki5
            // 
            this.txtRiekiShobunanChuki5.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki5.DisplayLength = null;
            this.txtRiekiShobunanChuki5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki5.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki5.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki5.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki5.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki5.MaxLength = 128;
            this.txtRiekiShobunanChuki5.Name = "txtRiekiShobunanChuki5";
            this.txtRiekiShobunanChuki5.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki5.TabIndex = 5;
            this.txtRiekiShobunanChuki5.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki4
            // 
            this.txtRiekiShobunanChuki4.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki4.DisplayLength = null;
            this.txtRiekiShobunanChuki4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki4.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki4.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki4.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki4.MaxLength = 128;
            this.txtRiekiShobunanChuki4.Name = "txtRiekiShobunanChuki4";
            this.txtRiekiShobunanChuki4.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki4.TabIndex = 4;
            this.txtRiekiShobunanChuki4.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki3
            // 
            this.txtRiekiShobunanChuki3.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki3.DisplayLength = null;
            this.txtRiekiShobunanChuki3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki3.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki3.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki3.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki3.MaxLength = 128;
            this.txtRiekiShobunanChuki3.Name = "txtRiekiShobunanChuki3";
            this.txtRiekiShobunanChuki3.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki3.TabIndex = 3;
            this.txtRiekiShobunanChuki3.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki2
            // 
            this.txtRiekiShobunanChuki2.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki2.DisplayLength = null;
            this.txtRiekiShobunanChuki2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki2.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki2.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki2.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki2.MaxLength = 128;
            this.txtRiekiShobunanChuki2.Name = "txtRiekiShobunanChuki2";
            this.txtRiekiShobunanChuki2.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki2.TabIndex = 2;
            this.txtRiekiShobunanChuki2.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki1
            // 
            this.txtRiekiShobunanChuki1.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki1.DisplayLength = null;
            this.txtRiekiShobunanChuki1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRiekiShobunanChuki1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki1.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki1.Location = new System.Drawing.Point(0, 0);
            this.txtRiekiShobunanChuki1.Margin = new System.Windows.Forms.Padding(4);
            this.txtRiekiShobunanChuki1.MaxLength = 128;
            this.txtRiekiShobunanChuki1.Name = "txtRiekiShobunanChuki1";
            this.txtRiekiShobunanChuki1.Size = new System.Drawing.Size(823, 23);
            this.txtRiekiShobunanChuki1.TabIndex = 1;
            this.txtRiekiShobunanChuki1.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel20, 0, 19);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel19, 0, 18);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel18, 0, 17);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel17, 0, 16);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel16, 0, 15);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel15, 0, 14);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel14, 0, 13);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel13, 0, 12);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel12, 0, 11);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel11, 0, 10);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel10, 0, 9);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel9, 0, 8);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 7);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(12, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 20;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(831, 548);
            this.fsiTableLayoutPanel1.TabIndex = 904;
            // 
            // fsiPanel20
            // 
            this.fsiPanel20.Controls.Add(this.txtRiekiShobunanChuki20);
            this.fsiPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel20.Location = new System.Drawing.Point(4, 517);
            this.fsiPanel20.Name = "fsiPanel20";
            this.fsiPanel20.Size = new System.Drawing.Size(823, 27);
            this.fsiPanel20.TabIndex = 19;
            // 
            // fsiPanel19
            // 
            this.fsiPanel19.Controls.Add(this.txtRiekiShobunanChuki19);
            this.fsiPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel19.Location = new System.Drawing.Point(4, 490);
            this.fsiPanel19.Name = "fsiPanel19";
            this.fsiPanel19.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel19.TabIndex = 18;
            // 
            // fsiPanel18
            // 
            this.fsiPanel18.Controls.Add(this.txtRiekiShobunanChuki18);
            this.fsiPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel18.Location = new System.Drawing.Point(4, 463);
            this.fsiPanel18.Name = "fsiPanel18";
            this.fsiPanel18.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel18.TabIndex = 17;
            // 
            // fsiPanel17
            // 
            this.fsiPanel17.Controls.Add(this.txtRiekiShobunanChuki17);
            this.fsiPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel17.Location = new System.Drawing.Point(4, 436);
            this.fsiPanel17.Name = "fsiPanel17";
            this.fsiPanel17.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel17.TabIndex = 16;
            // 
            // fsiPanel16
            // 
            this.fsiPanel16.Controls.Add(this.txtRiekiShobunanChuki16);
            this.fsiPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel16.Location = new System.Drawing.Point(4, 409);
            this.fsiPanel16.Name = "fsiPanel16";
            this.fsiPanel16.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel16.TabIndex = 15;
            // 
            // fsiPanel15
            // 
            this.fsiPanel15.Controls.Add(this.txtRiekiShobunanChuki15);
            this.fsiPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel15.Location = new System.Drawing.Point(4, 382);
            this.fsiPanel15.Name = "fsiPanel15";
            this.fsiPanel15.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel15.TabIndex = 14;
            // 
            // fsiPanel14
            // 
            this.fsiPanel14.Controls.Add(this.txtRiekiShobunanChuki14);
            this.fsiPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel14.Location = new System.Drawing.Point(4, 355);
            this.fsiPanel14.Name = "fsiPanel14";
            this.fsiPanel14.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel14.TabIndex = 13;
            // 
            // fsiPanel13
            // 
            this.fsiPanel13.Controls.Add(this.txtRiekiShobunanChuki13);
            this.fsiPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel13.Location = new System.Drawing.Point(4, 328);
            this.fsiPanel13.Name = "fsiPanel13";
            this.fsiPanel13.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel13.TabIndex = 12;
            // 
            // fsiPanel12
            // 
            this.fsiPanel12.Controls.Add(this.txtRiekiShobunanChuki12);
            this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel12.Location = new System.Drawing.Point(4, 301);
            this.fsiPanel12.Name = "fsiPanel12";
            this.fsiPanel12.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel12.TabIndex = 11;
            // 
            // fsiPanel11
            // 
            this.fsiPanel11.Controls.Add(this.txtRiekiShobunanChuki11);
            this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel11.Location = new System.Drawing.Point(4, 274);
            this.fsiPanel11.Name = "fsiPanel11";
            this.fsiPanel11.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel11.TabIndex = 10;
            // 
            // fsiPanel10
            // 
            this.fsiPanel10.Controls.Add(this.txtRiekiShobunanChuki10);
            this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel10.Location = new System.Drawing.Point(4, 247);
            this.fsiPanel10.Name = "fsiPanel10";
            this.fsiPanel10.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel10.TabIndex = 9;
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.txtRiekiShobunanChuki9);
            this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel9.Location = new System.Drawing.Point(4, 220);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel9.TabIndex = 8;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.txtRiekiShobunanChuki8);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(4, 193);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel8.TabIndex = 7;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.txtRiekiShobunanChuki7);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(4, 166);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel7.TabIndex = 6;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtRiekiShobunanChuki6);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 139);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel6.TabIndex = 5;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtRiekiShobunanChuki5);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 112);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel5.TabIndex = 4;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtRiekiShobunanChuki4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 85);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel4.TabIndex = 3;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtRiekiShobunanChuki3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 58);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel3.TabIndex = 2;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtRiekiShobunanChuki2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 31);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtRiekiShobunanChuki1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(823, 20);
            this.fsiPanel1.TabIndex = 0;
            // 
            // ZMYR1016
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1162, 785);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMYR1016";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel20.ResumeLayout(false);
            this.fsiPanel20.PerformLayout();
            this.fsiPanel19.ResumeLayout(false);
            this.fsiPanel19.PerformLayout();
            this.fsiPanel18.ResumeLayout(false);
            this.fsiPanel18.PerformLayout();
            this.fsiPanel17.ResumeLayout(false);
            this.fsiPanel17.PerformLayout();
            this.fsiPanel16.ResumeLayout(false);
            this.fsiPanel16.PerformLayout();
            this.fsiPanel15.ResumeLayout(false);
            this.fsiPanel15.PerformLayout();
            this.fsiPanel14.ResumeLayout(false);
            this.fsiPanel14.PerformLayout();
            this.fsiPanel13.ResumeLayout(false);
            this.fsiPanel13.PerformLayout();
            this.fsiPanel12.ResumeLayout(false);
            this.fsiPanel12.PerformLayout();
            this.fsiPanel11.ResumeLayout(false);
            this.fsiPanel11.PerformLayout();
            this.fsiPanel10.ResumeLayout(false);
            this.fsiPanel10.PerformLayout();
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel9.PerformLayout();
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel7.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki20;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki19;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki18;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki17;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki16;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki15;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki14;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki13;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki12;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki11;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki10;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki9;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki8;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki7;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki6;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki5;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki4;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki3;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki2;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel20;
        private common.FsiPanel fsiPanel19;
        private common.FsiPanel fsiPanel18;
        private common.FsiPanel fsiPanel17;
        private common.FsiPanel fsiPanel16;
        private common.FsiPanel fsiPanel15;
        private common.FsiPanel fsiPanel14;
        private common.FsiPanel fsiPanel13;
        private common.FsiPanel fsiPanel12;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel10;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}