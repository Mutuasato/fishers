﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// 帳票タイトル(ZMYR1017)
    /// </summary>
    public partial class ZMYR1017 : BasePgForm
    {

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMYR1017(ZMYR1011 frm)
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // サイズ変更
            this.Size = new Size(530, 557);
            // Escape F6のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            //this.ShowFButton = true;

            // 画面の初期表示
            DispData();

            // フォーカス設定
            this.txtTaishakuTaishohyoTitle.Focus();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = ("保存しますか？");
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 編集データ保存
                string tableNm = "TB_ZM_KESSANSHO_SETTEI";
                string where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = 0 AND KAIKEI_NENDO = @KAIKEI_NENDO";
                ArrayList alParams = SetZmKessanshoChukiParams();
                this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// ESCキー押下時処理
        /// </summary>
        public override void PressEsc()
        {
            base.PressEsc();
        }
        #endregion

        #region イベント

        /// <summary>
        /// 注記テキストボックスValidating時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTitle_Validating(object sender, CancelEventArgs e)
        {
            FsiTextBox txtBox = (FsiTextBox)sender;

            if (!IsValidTitle(txtBox))
            {
                e.Cancel = true;
                txtBox.SelectAll();
            }
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// データを表示
        /// </summary>
        private void DispData()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            StringBuilder sql = new StringBuilder();
            #region sql文
            sql.AppendLine("SELECT KAISHA_CD");
            sql.AppendLine(",KAIKEI_NENDO");
            sql.AppendLine(",TAISHAKU_TAISHOHYO_TITLE");
            sql.AppendLine(",SONEKI_KEISANSHO_TITLE");
            sql.AppendLine(",SEIZO_GENKA_TITLE");
            sql.AppendLine(",HANBAIHI_MEISAIHYO_TITLE");
            sql.AppendLine(",RIEKI_SHOBUNAN_TITLE");
            sql.AppendLine(",MINUS_KINGAKU");
            sql.AppendLine(" FROM TB_ZM_KESSANSHO_SETTEI");
            sql.AppendLine(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.AppendLine(" AND SHISHO_CD = @SHISHO_CD");
            sql.AppendLine(" AND KAIKEI_NENDO <= @KAIKEI_NENDO");
            sql.AppendLine(" ORDER BY KAIKEI_NENDO DESC");
            #endregion
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                this.txtTaishakuTaishohyoTitle.Text = Util.ToString(dr["TAISHAKU_TAISHOHYO_TITLE"]);
                this.txtSonekiKeisanshoTitle.Text = Util.ToString(dr["SONEKI_KEISANSHO_TITLE"]);
                this.txtSeizoGenkaTitle.Text = Util.ToString(dr["SEIZO_GENKA_TITLE"]);
                this.txtHanbaihiMeisaihyoTitle.Text = Util.ToString(dr["HANBAIHI_MEISAIHYO_TITLE"]);
                this.txtRiekiShobunanTitle.Text = Util.ToString(dr["RIEKI_SHOBUNAN_TITLE"]);
                if (Util.ToInt(dr["MINUS_KINGAKU"]) == 0)
                {
                    this.rdoMinusKingaku0.Checked = true;
                }
                else
                {
                    this.rdoMinusKingaku1.Checked = true;
                }
            }
        }

        /// <summary>
        /// 注記項目の入力チェック
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool IsValidTitle(FsiTextBox txt)
        {
            // 入力サイズが指定バイト数以上はNG
            if (!ValChk.IsWithinLength(txt.Text, txt.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                txt.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (!IsValidTitle(this.txtTaishakuTaishohyoTitle)) return false;
            if (!IsValidTitle(this.txtSonekiKeisanshoTitle)) return false;
            if (!IsValidTitle(this.txtSeizoGenkaTitle)) return false;
            if (!IsValidTitle(this.txtHanbaihiMeisaihyoTitle)) return false;
            if (!IsValidTitle(this.txtRiekiShobunanTitle)) return false;

            return true;
        }

        /// <summary>
        /// TB_ZM_KESSANSHO_SETTEIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 更新処理：DbParamCollection*2(WHERE句・SET句)
        /// </returns>
        private ArrayList SetZmKessanshoChukiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection whereParam = new DbParamCollection();
            DbParamCollection setParam = new DbParamCollection();

            // WHERE句パラメータ
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            alParams.Add(whereParam);
            // SET句パラメータ
            setParam.SetParam("@TAISHAKU_TAISHOHYO_TITLE", SqlDbType.VarChar, 64, this.txtTaishakuTaishohyoTitle.Text);
            setParam.SetParam("@SONEKI_KEISANSHO_TITLE", SqlDbType.VarChar, 64, this.txtSonekiKeisanshoTitle.Text);
            setParam.SetParam("@SEIZO_GENKA_TITLE", SqlDbType.VarChar, 64, this.txtSeizoGenkaTitle.Text);
            setParam.SetParam("@HANBAIHI_MEISAIHYO_TITLE", SqlDbType.VarChar, 64, this.txtHanbaihiMeisaihyoTitle.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_TITLE", SqlDbType.VarChar, 64, this.txtRiekiShobunanTitle.Text);
            setParam.SetParam("@MINUS_KINGAKU", SqlDbType.Decimal, 1, this.rdoMinusKingaku0.Checked ? 0 : 1);
            setParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(setParam);

            return alParams;
        }

        #endregion
    }
}
