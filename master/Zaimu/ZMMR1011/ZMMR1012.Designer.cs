﻿namespace jp.co.fsi.zm.zmmr1011
{
    partial class ZMMR1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblKanjoBango = new System.Windows.Forms.Label();
			this.lblKanjoKamoku = new System.Windows.Forms.Label();
			this.lblJp = new System.Windows.Forms.Label();
			this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
			this.lblZei = new System.Windows.Forms.Label();
			this.lblGokei = new System.Windows.Forms.Label();
			this.lblKariGokei = new System.Windows.Forms.Label();
			this.lblKariZeiGokei = new System.Windows.Forms.Label();
			this.lblKashiZeiGokei = new System.Windows.Forms.Label();
			this.lblKashiGokei = new System.Windows.Forms.Label();
			this.lblZan = new System.Windows.Forms.Label();
			this.lblWaku = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF8
			// 
			this.btnF8.Text = "F8\r\n\r\n次へ";
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 587);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(880, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(869, 31);
			this.lblTitle.Text = "";
			// 
			// lblKanjoBango
			// 
			this.lblKanjoBango.BackColor = System.Drawing.Color.Silver;
			this.lblKanjoBango.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKanjoBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKanjoBango.ForeColor = System.Drawing.Color.Black;
			this.lblKanjoBango.Location = new System.Drawing.Point(0, 0);
			this.lblKanjoBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanjoBango.Name = "lblKanjoBango";
			this.lblKanjoBango.Size = new System.Drawing.Size(113, 33);
			this.lblKanjoBango.TabIndex = 906;
			this.lblKanjoBango.Tag = "CHANGE";
			this.lblKanjoBango.Text = "123456";
			this.lblKanjoBango.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblKanjoKamoku
			// 
			this.lblKanjoKamoku.BackColor = System.Drawing.Color.Silver;
			this.lblKanjoKamoku.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKanjoKamoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKanjoKamoku.ForeColor = System.Drawing.Color.Black;
			this.lblKanjoKamoku.Location = new System.Drawing.Point(0, 0);
			this.lblKanjoKamoku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanjoKamoku.Name = "lblKanjoKamoku";
			this.lblKanjoKamoku.Size = new System.Drawing.Size(407, 33);
			this.lblKanjoKamoku.TabIndex = 907;
			this.lblKanjoKamoku.Tag = "CHANGE";
			this.lblKanjoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJp
			// 
			this.lblJp.BackColor = System.Drawing.Color.Silver;
			this.lblJp.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJp.ForeColor = System.Drawing.Color.Black;
			this.lblJp.Location = new System.Drawing.Point(0, 0);
			this.lblJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJp.Name = "lblJp";
			this.lblJp.Size = new System.Drawing.Size(341, 33);
			this.lblJp.TabIndex = 908;
			this.lblJp.Tag = "CHANGE";
			this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// mtbList
			// 
			this.mtbList.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.mtbList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mtbList.FixedCols = 0;
			this.mtbList.FocusField = null;
			this.mtbList.Location = new System.Drawing.Point(0, 0);
			this.mtbList.Margin = new System.Windows.Forms.Padding(4);
			this.mtbList.Name = "mtbList";
			this.mtbList.NotSelectableCols = 0;
			this.mtbList.SelectRange = null;
			this.mtbList.Size = new System.Drawing.Size(851, 540);
			this.mtbList.TabIndex = 912;
			this.mtbList.Text = "SjMultiTable1";
			this.mtbList.UndoBufferEnabled = false;
			// 
			// lblZei
			// 
			this.lblZei.BackColor = System.Drawing.Color.Silver;
			this.lblZei.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblZei.ForeColor = System.Drawing.Color.Black;
			this.lblZei.Location = new System.Drawing.Point(0, 0);
			this.lblZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZei.Name = "lblZei";
			this.lblZei.Size = new System.Drawing.Size(139, 33);
			this.lblZei.TabIndex = 913;
			this.lblZei.Tag = "CHANGE";
			this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblGokei
			// 
			this.lblGokei.BackColor = System.Drawing.SystemColors.Control;
			this.lblGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGokei.ForeColor = System.Drawing.Color.Black;
			this.lblGokei.Location = new System.Drawing.Point(17, 636);
			this.lblGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGokei.Name = "lblGokei";
			this.lblGokei.Size = new System.Drawing.Size(625, 55);
			this.lblGokei.TabIndex = 914;
			this.lblGokei.Text = "合　　　計";
			this.lblGokei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.lblGokei.Visible = false;
			// 
			// lblKariGokei
			// 
			this.lblKariGokei.BackColor = System.Drawing.Color.Transparent;
			this.lblKariGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKariGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKariGokei.ForeColor = System.Drawing.Color.Black;
			this.lblKariGokei.Location = new System.Drawing.Point(641, 663);
			this.lblKariGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKariGokei.Name = "lblKariGokei";
			this.lblKariGokei.Size = new System.Drawing.Size(109, 29);
			this.lblKariGokei.TabIndex = 915;
			this.lblKariGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblKariGokei.Visible = false;
			// 
			// lblKariZeiGokei
			// 
			this.lblKariZeiGokei.BackColor = System.Drawing.Color.Transparent;
			this.lblKariZeiGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKariZeiGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKariZeiGokei.ForeColor = System.Drawing.Color.Black;
			this.lblKariZeiGokei.Location = new System.Drawing.Point(641, 636);
			this.lblKariZeiGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKariZeiGokei.Name = "lblKariZeiGokei";
			this.lblKariZeiGokei.Size = new System.Drawing.Size(109, 27);
			this.lblKariZeiGokei.TabIndex = 916;
			this.lblKariZeiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblKariZeiGokei.Visible = false;
			// 
			// lblKashiZeiGokei
			// 
			this.lblKashiZeiGokei.BackColor = System.Drawing.Color.Transparent;
			this.lblKashiZeiGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKashiZeiGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKashiZeiGokei.ForeColor = System.Drawing.Color.Black;
			this.lblKashiZeiGokei.Location = new System.Drawing.Point(748, 636);
			this.lblKashiZeiGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKashiZeiGokei.Name = "lblKashiZeiGokei";
			this.lblKashiZeiGokei.Size = new System.Drawing.Size(109, 27);
			this.lblKashiZeiGokei.TabIndex = 918;
			this.lblKashiZeiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblKashiZeiGokei.Visible = false;
			// 
			// lblKashiGokei
			// 
			this.lblKashiGokei.BackColor = System.Drawing.Color.Transparent;
			this.lblKashiGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKashiGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKashiGokei.ForeColor = System.Drawing.Color.Black;
			this.lblKashiGokei.Location = new System.Drawing.Point(748, 663);
			this.lblKashiGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKashiGokei.Name = "lblKashiGokei";
			this.lblKashiGokei.Size = new System.Drawing.Size(109, 29);
			this.lblKashiGokei.TabIndex = 917;
			this.lblKashiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblKashiGokei.Visible = false;
			// 
			// lblZan
			// 
			this.lblZan.BackColor = System.Drawing.Color.Transparent;
			this.lblZan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblZan.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblZan.ForeColor = System.Drawing.Color.Black;
			this.lblZan.Location = new System.Drawing.Point(856, 663);
			this.lblZan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZan.Name = "lblZan";
			this.lblZan.Size = new System.Drawing.Size(107, 29);
			this.lblZan.TabIndex = 919;
			this.lblZan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblZan.Visible = false;
			// 
			// lblWaku
			// 
			this.lblWaku.BackColor = System.Drawing.Color.Transparent;
			this.lblWaku.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblWaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblWaku.ForeColor = System.Drawing.Color.Black;
			this.lblWaku.Location = new System.Drawing.Point(856, 636);
			this.lblWaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblWaku.Name = "lblWaku";
			this.lblWaku.Size = new System.Drawing.Size(107, 29);
			this.lblWaku.TabIndex = 920;
			this.lblWaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblWaku.Visible = false;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 35);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(861, 592);
			this.fsiTableLayoutPanel1.TabIndex = 921;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.mtbList);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 47);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(851, 540);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.fsiPanel6);
			this.fsiPanel1.Controls.Add(this.fsiPanel5);
			this.fsiPanel1.Controls.Add(this.fsiPanel4);
			this.fsiPanel1.Controls.Add(this.fsiPanel3);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(851, 33);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.lblJp);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel6.Location = new System.Drawing.Point(371, 0);
			this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(341, 33);
			this.fsiPanel6.TabIndex = 2;
			this.fsiPanel6.Tag = "CHANGE";
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.lblZei);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel5.Location = new System.Drawing.Point(712, 0);
			this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(139, 33);
			this.fsiPanel5.TabIndex = 2;
			this.fsiPanel5.Tag = "CHANGE";
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lblKanjoKamoku);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel4.Location = new System.Drawing.Point(113, 0);
			this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(407, 33);
			this.fsiPanel4.TabIndex = 2;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblKanjoBango);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.fsiPanel3.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(113, 33);
			this.fsiPanel3.TabIndex = 1;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// ZMMR1012
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(869, 724);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.lblZan);
			this.Controls.Add(this.lblKariZeiGokei);
			this.Controls.Add(this.lblKariGokei);
			this.Controls.Add(this.lblGokei);
			this.Controls.Add(this.lblWaku);
			this.Controls.Add(this.lblKashiZeiGokei);
			this.Controls.Add(this.lblKashiGokei);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "ZMMR1012";
			this.ShowFButton = true;
			this.Text = "";
			this.Shown += new System.EventHandler(this.ZMMR1012_Shown);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblKashiGokei, 0);
			this.Controls.SetChildIndex(this.lblKashiZeiGokei, 0);
			this.Controls.SetChildIndex(this.lblWaku, 0);
			this.Controls.SetChildIndex(this.lblGokei, 0);
			this.Controls.SetChildIndex(this.lblKariGokei, 0);
			this.Controls.SetChildIndex(this.lblKariZeiGokei, 0);
			this.Controls.SetChildIndex(this.lblZan, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKanjoBango;
        private System.Windows.Forms.Label lblKanjoKamoku;
        private System.Windows.Forms.Label lblJp;
        private jp.co.fsi.common.controls.SjMultiTable mtbList;
        private System.Windows.Forms.Label lblZei;
        private System.Windows.Forms.Label lblGokei;
        private System.Windows.Forms.Label lblKariGokei;
        private System.Windows.Forms.Label lblKariZeiGokei;
        private System.Windows.Forms.Label lblKashiZeiGokei;
        private System.Windows.Forms.Label lblKashiGokei;
        private System.Windows.Forms.Label lblZan;
        private System.Windows.Forms.Label lblWaku;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
    }
}