﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1011
{
    /// <summary>
    /// 総勘定元帳(ZMMR1011)
    /// </summary>
    public partial class ZMMR1011 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面入力値(他の画面との連携に用いる)
        /// </summary>
        public Hashtable Condition
        {
            get
            {
                return GetCondition();
            }
        }

        private DataTable _dtSwkTgtData = new DataTable();
        /// <summary>
        /// 仕訳対象データ
        /// </summary>
        public DataTable SwkTgtData
        {
            get
            {
                return this._dtSwkTgtData;
            }
            set
            {
                this._dtSwkTgtData = value;
            }
        }

        private DataTable _dsTaishakuData = new DataTable();
        /// <summary>
        /// 貸借データ
        /// </summary>
        /// <remarks>
        /// 貸借を仕訳したデータ(1伝票あたり1DataTable)
        /// </remarks>
        public DataTable TaishakuData
        {
            get
            {
                return this._dsTaishakuData;
            }
        }

        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        private int _externalCall = 0;           // 外部から呼ばれた
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMMR1011()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            #region 試算表呼び出し時は一覧表示
            if (this.InData != null)
            {
                this.Visible = false;
                string[] cnd = (string[])this.InData;
                if (cnd.Length != 0)
                {
                    this.txtMizuageShishoCd.Text = cnd[0];
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
                    SetJpFr(Util.ConvJpDate(Util.ToDate(cnd[1]), this.Dba));
                    SetJpTo(Util.ConvJpDate(Util.ToDate(cnd[2]), this.Dba));
                    SetJp(Util.ConvJpDate(DateTime.Today, this.Dba));

                    this.txtKanjoKamokuFr.Text = cnd[3];
                    this.lblKanjoKamokuFr.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", Util.ToString(txtMizuageShishoCd.Text), this.txtKanjoKamokuFr.Text);
                    this.txtKanjoKamokuTo.Text = cnd[4];
                    this.lblKanjoKamokuTo.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", Util.ToString(txtMizuageShishoCd.Text), this.txtKanjoKamokuTo.Text);

                    this._externalCall = 1;

                    // 仕訳種類の全仕訳にチェック
                    rdoZenbu.Checked = true;
                    // 消費税処理の税込みにチェック
                    rdoZeikomi.Checked = true;
                    // 金額がｾﾞﾛの科目を印字のしないにチェック
                    rdoNo.Checked = true;

                    // 仕訳対象データを抽出
                    ZMMR1011DA da = new ZMMR1011DA(this.UInfo, this.Dba, this.Config, cnd[5]);
                    this._dtSwkTgtData = da.GetSwkTgtData(this.Condition);

                    // 総勘定元帳画面(ZAMR2012)を起動
                    using (ZMMR1012 frm = new ZMMR1012(this))
                    {
                        this.DialogResult = frm.ShowDialog(this);
                    }
                }
                else
                {
                    this.PressEsc();
                }
            }
            #endregion

            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }

            string[] jpMontFr;
            string[] jpMontTo;

            // 設定会計年度の開始日と終了日を取得
            DateTime kaikeiNendoKaishiBi = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            DateTime kaikeiNendoShuryoBi = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            // 取得したデータを和暦に変換
            string[] warekiKaikeiNendoKaishiBiDate = Util.ConvJpDate(kaikeiNendoKaishiBi, this.Dba);
            string[] warekiKaikeiNendoShuryoBiDate = Util.ConvJpDate(kaikeiNendoShuryoBi, this.Dba);

            // 今日の日付を取得
            DateTime dtToday = DateTime.Today;
            string[] jpDate = Util.ConvJpDate(dtToday, this.Dba);

            // 現在が1月～3月の場合　設定会計年度が現在の会計年度以降であれば、今月の初日と末日を取得設定
            // 又は
            // 現在が4月～12月の場合　設定会計年度が現在の会計年度以降であれば、今月の初日と末日を取得設定
            if ((Util.ToInt(jpDate[3]) < 4 && Util.ToInt(warekiKaikeiNendoShuryoBiDate[2]) == Util.ToInt(jpDate[2])) ||
                (Util.ToInt(jpDate[3]) > 3 && Util.ToInt(warekiKaikeiNendoShuryoBiDate[2]) > Util.ToInt(jpDate[2])))
            {
                //今月の最初の日
                DateTime dtMonthFr = new DateTime(dtToday.Year, dtToday.Month, 1);
                jpMontFr = Util.ConvJpDate(dtMonthFr, this.Dba);
                //今月の最後の日
                DateTime dtMonthTo = new DateTime(dtToday.Year, dtToday.Month, DateTime.DaysInMonth(dtToday.Year, dtToday.Month));
                jpMontTo = Util.ConvJpDate(dtMonthTo, this.Dba);
            }
            // 設定会計年度が過去の会計年度であれば、設定会計年度の終了日を設定
            else
            {
                jpMontFr = warekiKaikeiNendoShuryoBiDate;
                jpMontTo = warekiKaikeiNendoShuryoBiDate;
            }

            // 日付範囲(期間)前
            lblGengoFr.Text = jpMontFr[0];
            txtYearFr.Text = jpMontFr[2];
            txtMonthFr.Text = jpMontFr[3];
            txtDayFr.Text = jpMontFr[4];
            // 日付範囲(期間)後
            lblGengoTo.Text = jpMontTo[0];
            txtYearTo.Text = jpMontTo[2];
            txtMonthTo.Text = jpMontTo[3];
            txtDayTo.Text = jpMontTo[4];
            // 日付(出力日付)
            lblGengo.Text = jpDate[0];
            txtYear.Text = jpDate[2];
            txtMonth.Text = jpDate[3];
            txtDay.Text = jpDate[4];

            // 仕訳種類の全仕訳にチェック
            rdoZenbu.Checked = true;
            // 消費税処理の税込みにチェック
            rdoZeikomi.Checked = true;
            // 金額がｾﾞﾛの科目を印字のしないにチェック
            rdoNo.Checked = true;

            // フォーカス設定
            this.rdoZenbu.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)、勘定科目範囲、部門範囲、工事範囲にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYearFr":
                case "txtYearTo":
                case "txtYear":
                case "txtKanjoKamokuFr":
                case "txtKanjoKamokuTo":
                case "txtBumonFr":
                case "txtBumonTo":
                //case "txtKojiFr":
                //case "txtKojiTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYearFr":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpFr();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYearTo":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpTo();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYear":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJp();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKanjoKamokuFr":
                    #region 勘定科目検索
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("ZAMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9011");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoKamokuFr.Text = outData[0];
                                this.lblKanjoKamokuFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKanjoKamokuTo":
                    #region 勘定科目検索
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("ZAMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9011");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoKamokuTo.Text = outData[0];
                                this.lblKanjoKamokuTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtBumonFr":
                    #region 部門検索
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("ZAMC9051.exe");
                    //asm = Assembly.LoadFrom("CMCM2041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9051.ZAMC9051");
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2043");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "TB_CM_BUMON";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtBumonFr.Text = outData[0];
                                this.lblBumonFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtBumonTo":
                    #region 部門検索
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("ZAMC9051.exe");
                    //asm = Assembly.LoadFrom("CMCM2041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9051.ZAMC9051");
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2043");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "TB_CM_BUMON";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtBumonTo.Text = outData[0];
                                this.lblBumonTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                // 工事コードを使用しないため、一旦コメントアウト
                /*case "txtKojiFr":
                case "txtKojiTo":
                    Msg.Notice("TODO:工事検索画面を起動しないといけない");
                    break;*/
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {

            // 印刷処理
            this.PrintingProcess(true);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {

            // 印刷処理
            this.PrintingProcess(false);
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 印刷処理
            this.PrintingProcess(false, true);
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 印刷処理
            this.PrintingProcess(false, false, true);
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            // 設定処理
            using (ZMMR1013 frm = new ZMMR1013())
            {
                if (frm.ShowDialog(this) == DialogResult.OK)
                {
                    this.Config.ReloadConfig();
                }
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            using (PrintSettingForm psForm = new PrintSettingForm(new string[1] { "ZMMR10111R" }))
            {
                psForm.ShowDialog();
            }
        }
        #endregion

        #region イベント

        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
            }
        }

        /// <summary>
        /// 期間(年)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtYearFr.SelectAll();
            }
            else
            {
                this.txtYearFr.Text = Util.ToString(IsValid.SetYear(this.txtYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 期間(月)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthFr.SelectAll();
            }
            else
            {
                this.txtMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 期間(日)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayFr.SelectAll();
            }
            else
            {
                this.txtDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDayFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 期間(年)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtYearTo.SelectAll();
            }
            else
            {
                this.txtYearTo.Text = Util.ToString(IsValid.SetYear(this.txtYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 期間(月)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthTo.SelectAll();
            }
            else
            {
                this.txtMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 期間(日)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayTo.SelectAll();
            }
            else
            {
                this.txtDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDayTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 出力日付(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYear.Text, this.txtYear.MaxLength))
            {
                e.Cancel = true;
                this.txtYear.SelectAll();
            }
            else
            {
                this.txtYear.Text = Util.ToString(IsValid.SetYear(this.txtYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 出力日付(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtMonth.SelectAll();
            }
            else
            {
                this.txtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtMonth.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 出力日付(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDay.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                this.txtDay.Text = Util.ToString(IsValid.SetDay(this.txtDay.Text));
                CheckJp();
                SetJp();

                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 出力日のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 印刷処理
                this.PrintingProcess(true);
            }
        }

        /// <summary>
        /// 勘定科目範囲(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKanjoKamokuFr.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", Util.ToString(txtMizuageShishoCd.Text), this.txtKanjoKamokuFr.Text);
            if (ValChk.IsEmpty(this.lblKanjoKamokuFr.Text))
            {
                this.lblKanjoKamokuFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 勘定科目範囲(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKanjoKamokuTo.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", Util.ToString(txtMizuageShishoCd.Text), this.txtKanjoKamokuTo.Text);
            if (ValChk.IsEmpty(this.lblKanjoKamokuTo.Text))
            {
                this.lblKanjoKamokuTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門範囲(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblBumonFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", Util.ToString(txtMizuageShishoCd.Text), this.txtBumonFr.Text);
            if (ValChk.IsEmpty(this.lblBumonFr.Text))
            {
                this.lblBumonFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門範囲(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblBumonTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", Util.ToString(txtMizuageShishoCd.Text), this.txtBumonTo.Text);
            if (ValChk.IsEmpty(this.lblBumonTo.Text))
            {
                this.lblBumonTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 工事(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojiFr_Validating(object sender, CancelEventArgs e)
        {
            //if (ValChk.IsEmpty(this.txtKojiFr.Text))
            //{
            //    this.lblKojiFr.Text = "先　頭";
            //}
            //else
            //// 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            //if (!ValChk.IsNumber(this.txtKojiFr.Text))
            //{
            //    Msg.Notice("コード(自)は数値のみで入力してください。");
            //    this.txtKojiFr.SelectAll();
            //    e.Cancel = true;
            //    return;
            //}
            //else
            //{
            //    // コードを元に名称を取得する
            //    this.lblKojiFr.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KOJI", this.txtKojiFr.Text);
            //}
        }

        /// <summary>
        /// 工事(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojiTo_Validating(object sender, CancelEventArgs e)
        {
            //if (ValChk.IsEmpty(this.txtKojiTo.Text))
            //{
            //    this.lblKojiTo.Text = "最　後";
            //}
            //else
            //// 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            //if (!ValChk.IsNumber(this.txtKojiTo.Text))
            //{
            //    Msg.Notice("コード(至)は数値のみで入力してください。");
            //    this.txtKojiTo.SelectAll();
            //    e.Cancel = true;
            //}
            //else
            //{
            //    // コードを元に名称を取得する
            //    this.lblKojiTo.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KOJI", this.txtKojiTo.Text);
            //}
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コード(自)の入力チェック
        /// </summary>
        private bool IsValidKanjoKamokuFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamokuFr.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コード(至)の入力チェック
        /// </summary>
        private bool IsValidKanjoKamokuTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamokuTo.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 部門コード(自)の入力チェック
        /// </summary>
        private bool IsValidBumonFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonFr.Text))
            {
                Msg.Error("部門コードは数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 部門コード(至)の入力チェック
        /// </summary>
        private bool IsValidBumonTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonTo.Text))
            {
                Msg.Error("部門コードは数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayFr.Text) > lastDayInMonth)
            {
                this.txtDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoFr.Text,
                                  this.txtYearFr.Text,
                                  this.txtMonthFr.Text,
                                  this.txtDayFr.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayTo.Text) > lastDayInMonth)
            {
                this.txtDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.ConvJpDate(
                  FixNendoDate(
                  Util.ConvAdDate(this.lblGengoTo.Text,
                                  this.txtYearTo.Text,
                                  this.txtMonthTo.Text,
                                  this.txtDayTo.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 年月日(出力日付)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDay.Text) > lastDayInMonth)
            {
                this.txtDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(出力日付)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            //SetJp(Util.ConvJpDate(
            //      FixNendoDate(
            //      Util.ConvAdDate(this.lblGengo.Text,
            //                      this.txtYear.Text,
            //                      this.txtMonth.Text,
            //                      this.txtDay.Text, this.Dba)), this.Dba));
            SetJp(Util.ConvJpDate(
                  Util.ConvAdDate(this.lblGengo.Text,
                                  this.txtYear.Text,
                                  this.txtMonth.Text,
                                  this.txtDay.Text, this.Dba), this.Dba));
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }
        private void SetJp(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                this.txtYearFr.Focus();
                this.txtYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                this.txtMonthFr.Focus();
                this.txtMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                this.txtDayFr.Focus();
                this.txtDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                this.txtYearTo.Focus();
                this.txtYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                this.txtMonthTo.Focus();
                this.txtMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                this.txtDayTo.Focus();
                this.txtDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            // 勘定科目コード(自)の入力チェック
            if (!IsValidKanjoKamokuFr())
            {
                this.txtKanjoKamokuFr.Focus();
                this.txtKanjoKamokuFr.SelectAll();
                return false;
            }
            // 勘定科目コード(至)の入力チェック
            if (!IsValidKanjoKamokuTo())
            {
                this.txtKanjoKamokuTo.Focus();
                this.txtKanjoKamokuTo.SelectAll();
                return false;
            }

            // 部門コード(自)の入力チェック
            if (!IsValidBumonFr())
            {
                this.txtBumonFr.Focus();
                this.txtBumonFr.SelectAll();
                return false;
            }
            // 部門コード(至)の入力チェック
            if (!IsValidBumonTo())
            {
                this.txtBumonTo.Focus();
                this.txtBumonTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 画面値をHashTableに格納する
        /// </summary>
        /// <returns>画面値を格納したHashTable</returns>
        private Hashtable GetCondition()
        {
            Hashtable htCondition = new Hashtable();

            // 支所コード
            htCondition["ShishoCode"] = Util.ToString(txtMizuageShishoCd.Text);
            htCondition["ExternalCall"] = Util.ToString(this._externalCall);

            // 出力ファイル
            htCondition["ReportName"] = this.lblTitle.Text;

            // 仕訳種類
            if (rdoTsujo.Checked)
            {
                htCondition["ShiwakeShurui"] = 0;
            }
            else if (rdoKessan.Checked)
            {
                htCondition["ShiwakeShurui"] = 1;
            }
            else if (rdoZenbu.Checked)
            {
                htCondition["ShiwakeShurui"] = 9;
            }

            // 消費税処理
            if (rdoZeikomi.Checked)
            {
                htCondition["ShohizeiShoriHandan"] = 1;
                //htCondition["ShohizeiShori"] = "【税込み】";
                htCondition["ShohizeiShori"] = "【" + this.rdoZeikomi.Text + "】";
            }
            else if (rdoZeinuki.Checked)
            {
                htCondition["ShohizeiShoriHandan"] = 2;
                //htCondition["ShohizeiShori"] = "【税抜き】";
                htCondition["ShohizeiShori"] = "【" + this.rdoZeinuki.Text + "】";
            }

            // 期間Fr
            htCondition["DtFr"] = Util.ConvAdDate(this.lblGengoFr.Text,
                    Util.ToInt(this.txtYearFr.Text),
                    Util.ToInt(this.txtMonthFr.Text),
                    Util.ToInt(this.txtDayFr.Text), this.Dba);
            // 期間To
            htCondition["DtTo"] = Util.ConvAdDate(this.lblGengoTo.Text,
                    Util.ToInt(this.txtYearTo.Text),
                    Util.ToInt(this.txtMonthTo.Text),
                    Util.ToInt(this.txtDayTo.Text), this.Dba);

            // 勘定科目範囲Fr
            if (ValChk.IsEmpty(this.txtKanjoKamokuFr.Text))
            {
                htCondition["KnjoKamokuFr"] = "0";
            }
            else
            {
                htCondition["KnjoKamokuFr"] = this.txtKanjoKamokuFr.Text;
            }

            // 勘定科目範囲To
            if (ValChk.IsEmpty(this.txtKanjoKamokuTo.Text))
            {
                htCondition["KnjoKamokuTo"] = "999999";
            }
            else
            {
                htCondition["KnjoKamokuTo"] = this.txtKanjoKamokuTo.Text;
            }

            // 部門範囲Fr
            if (ValChk.IsEmpty(this.txtBumonFr.Text))
            {
                htCondition["BumonFr"] = "0";
            }
            else
            {
                htCondition["BumonFr"] = this.txtBumonFr.Text;
            }

            // 部門範囲To
            if (ValChk.IsEmpty(this.txtBumonTo.Text))
            {
                htCondition["BumonTo"] = "9999";
            }
            else
            {
                htCondition["BumonTo"] = this.txtBumonTo.Text;
            }

            // 工事範囲Fr
            if (ValChk.IsEmpty(this.txtKojiFr.Text))
            {
                htCondition["KojiFr"] = "0";
            }
            else
            {
                htCondition["KojiFr"] = this.txtKojiFr.Text;
            }

            // 工事範囲To
            if (ValChk.IsEmpty(this.txtKojiTo.Text))
            {
                htCondition["KojiTo"] = "9999";
            }
            else
            {
                htCondition["KojiTo"] = this.txtKojiTo.Text;
            }

            // 金額がｾﾞﾛの科目を印字するかしないか
            if (rdoNo.Checked)
            {
                htCondition["Inji"] = "no";
            }
            else
            {
                htCondition["Inji"] = "yes";
            }

            // 出力日付
            htCondition["ShurutyokuDt"] = Util.ConvAdDate(this.lblGengo.Text,
                    Util.ToInt(this.txtYear.Text),
                    Util.ToInt(this.txtMonth.Text),
                    Util.ToInt(this.txtDay.Text), this.Dba);

            // 設定ファイル情報
            htCondition["check01"] = this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1011", "Setting", "Chk01");
            htCondition["check02"] = this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1011", "Setting", "Chk02");
            htCondition["check03"] = this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1011", "Setting", "Chk03");
            htCondition["check04"] = this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1011", "Setting", "Chk04");
            htCondition["check05"] = this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1011", "Setting", "Chk05");
            htCondition["check06"] = this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1011", "Setting", "Chk06");
            htCondition["check07"] = this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1011", "Setting", "Chk07");
            return htCondition;
        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }

        /// <summary>
        /// 会計年度内日付に変換
        /// </summary>
        private DateTime FixNendoDate(DateTime date)
        {
            DateTime dateFr = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            DateTime dateTo = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }

        /// <summary>
        /// 印刷処理
        /// </summary>
        /// <param name="isPreview"></param>
        /// <returns></returns>
        private bool PrintingProcess(bool isPreview, bool isPdf = false, bool isExcel = false)
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return false;
            }

            //if (Msg.ConfNmYesNo((isPreview ? "プレビュー" : "印刷"), "実行しますか？") == DialogResult.Yes)
            string dlgTitle = isPreview ? "プレビュー" : "印刷";
            dlgTitle = isExcel ? "EXCEL出力" : dlgTitle;
            dlgTitle = isPdf ? "PDF出力" : dlgTitle;
            if (Msg.ConfNmYesNo(dlgTitle, "実行しますか？") == DialogResult.Yes)
            {
                // 仕訳対象データを抽出
                ZMMR1011DA da = new ZMMR1011DA(this.UInfo, this.Dba, this.Config);
                this._dtSwkTgtData = da.GetSwkTgtData(this.Condition);

                if (this._dtSwkTgtData.Rows.Count == 0)
                {
                    Msg.Info("該当するデータがありません。");
                    return false;
                }

                if (isPreview)
                {
                    // 総勘定元帳画面(ZAMR2012)を起動
                    using (ZMMR1012 frm = new ZMMR1012(this))
                    {
                        frm.ShowDialog(this);
                    }
                }
                else
                {
                    // 印刷処理
                    ZMMR1011PR pr = new ZMMR1011PR(this.UInfo, this.Dba, this.Config, this.UnqId, this);
                    //pr.DoPrint(false);
                    pr.DoPrint(false, isPdf, isExcel);
                }
            }
            else
            {
                return false;
            }

            return true;
        }
        #endregion
    }
}
