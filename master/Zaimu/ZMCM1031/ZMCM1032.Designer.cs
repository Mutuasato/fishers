﻿namespace jp.co.fsi.zm.zmcm1031
{
    partial class ZMCM1032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtkanjoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoCd = new System.Windows.Forms.Label();
            this.txtKamokuBnNm = new System.Windows.Forms.Label();
            this.lblkashi = new System.Windows.Forms.Label();
            this.lblkari = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKamokuKubnNm = new System.Windows.Forms.Label();
            this.txtShukeihoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShukeihoho = new System.Windows.Forms.Label();
            this.txtGokeiKeisanKubn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGokeiKeisanKubn = new System.Windows.Forms.Label();
            this.txtShukeiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSkshKskMemo = new System.Windows.Forms.Label();
            this.txtKojiKnri = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSkshHkMemo = new System.Windows.Forms.Label();
            this.txtHojoShiyoKubn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoShiyoKubn = new System.Windows.Forms.Label();
            this.lblShimebiMemo = new System.Windows.Forms.Label();
            this.txtHojoKamokuKnri = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoKamokuKnri = new System.Windows.Forms.Label();
            this.lblSeikyusakiNm = new System.Windows.Forms.Label();
            this.txtBumonKnri = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonKnri = new System.Windows.Forms.Label();
            this.txtKashikataKubn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKashikataKubn = new System.Windows.Forms.Label();
            this.txtKarikataKubn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKarikataKubn = new System.Windows.Forms.Label();
            this.txtTaishakuKubn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTaishakuKubn = new System.Windows.Forms.Label();
            this.txtKamokuKubn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKamokuKubn = new System.Windows.Forms.Label();
            this.txtKamokuBnCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKamokuBnCd = new System.Windows.Forms.Label();
            this.txtkanjoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblkanjoKanaNm = new System.Windows.Forms.Label();
            this.txtkanjoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoNm = new System.Windows.Forms.Label();
            this.lblShukeiCd = new System.Windows.Forms.Label();
            this.lblKojiKnri = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel21 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel20 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel19 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel18 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel17 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel16 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel15 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel14 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel13 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel22 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel21.SuspendLayout();
            this.fsiPanel20.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel19.SuspendLayout();
            this.fsiPanel18.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel17.SuspendLayout();
            this.fsiPanel16.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel15.SuspendLayout();
            this.fsiPanel14.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel13.SuspendLayout();
            this.fsiPanel12.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel11.SuspendLayout();
            this.fsiPanel10.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.fsiPanel22.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 369);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1083, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1072, 31);
            this.lblTitle.Text = "勘定科目の登録";
            // 
            // txtkanjoCd
            // 
            this.txtkanjoCd.AutoSizeFromLength = true;
            this.txtkanjoCd.DisplayLength = null;
            this.txtkanjoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtkanjoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtkanjoCd.Location = new System.Drawing.Point(119, 5);
            this.txtkanjoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtkanjoCd.MaxLength = 6;
            this.txtkanjoCd.Name = "txtkanjoCd";
            this.txtkanjoCd.Size = new System.Drawing.Size(63, 23);
            this.txtkanjoCd.TabIndex = 2;
            this.txtkanjoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblKanjoCd
            // 
            this.lblKanjoCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoCd.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoCd.Name = "lblKanjoCd";
            this.lblKanjoCd.Size = new System.Drawing.Size(383, 32);
            this.lblKanjoCd.TabIndex = 1;
            this.lblKanjoCd.Tag = "CHANGE";
            this.lblKanjoCd.Text = "勘定科目コード";
            this.lblKanjoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKamokuBnNm
            // 
            this.txtKamokuBnNm.BackColor = System.Drawing.Color.LightCyan;
            this.txtKamokuBnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtKamokuBnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuBnNm.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuBnNm.Location = new System.Drawing.Point(191, 4);
            this.txtKamokuBnNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtKamokuBnNm.Name = "txtKamokuBnNm";
            this.txtKamokuBnNm.Size = new System.Drawing.Size(273, 24);
            this.txtKamokuBnNm.TabIndex = 55;
            this.txtKamokuBnNm.Tag = "DISPNAME";
            this.txtKamokuBnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblkashi
            // 
            this.lblkashi.BackColor = System.Drawing.Color.LightCyan;
            this.lblkashi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblkashi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblkashi.ForeColor = System.Drawing.Color.Black;
            this.lblkashi.Location = new System.Drawing.Point(191, 5);
            this.lblkashi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblkashi.Name = "lblkashi";
            this.lblkashi.Size = new System.Drawing.Size(273, 24);
            this.lblkashi.TabIndex = 54;
            this.lblkashi.Tag = "DISPNAME";
            this.lblkashi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblkari
            // 
            this.lblkari.BackColor = System.Drawing.Color.LightCyan;
            this.lblkari.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblkari.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblkari.ForeColor = System.Drawing.Color.Black;
            this.lblkari.Location = new System.Drawing.Point(191, 4);
            this.lblkari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblkari.Name = "lblkari";
            this.lblkari.Size = new System.Drawing.Size(273, 24);
            this.lblkari.TabIndex = 53;
            this.lblkari.Tag = "DISPNAME";
            this.lblkari.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.LightCyan;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(191, 5);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(273, 24);
            this.label4.TabIndex = 52;
            this.label4.Tag = "DISPNAME";
            this.label4.Text = "１：借方　　２：貸方";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKamokuKubnNm
            // 
            this.txtKamokuKubnNm.BackColor = System.Drawing.Color.LightCyan;
            this.txtKamokuKubnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtKamokuKubnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuKubnNm.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuKubnNm.Location = new System.Drawing.Point(191, 3);
            this.txtKamokuKubnNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtKamokuKubnNm.Name = "txtKamokuKubnNm";
            this.txtKamokuKubnNm.Size = new System.Drawing.Size(273, 24);
            this.txtKamokuKubnNm.TabIndex = 51;
            this.txtKamokuKubnNm.Tag = "DISPNAME";
            this.txtKamokuKubnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShukeihoho
            // 
            this.txtShukeihoho.AutoSizeFromLength = true;
            this.txtShukeihoho.DisplayLength = null;
            this.txtShukeihoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShukeihoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShukeihoho.Location = new System.Drawing.Point(112, 6);
            this.txtShukeihoho.Margin = new System.Windows.Forms.Padding(4);
            this.txtShukeihoho.MaxLength = 1;
            this.txtShukeihoho.Name = "txtShukeihoho";
            this.txtShukeihoho.Size = new System.Drawing.Size(25, 23);
            this.txtShukeihoho.TabIndex = 47;
            this.txtShukeihoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShukeihoho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShukeihoho_KeyDown);
            // 
            // lblShukeihoho
            // 
            this.lblShukeihoho.BackColor = System.Drawing.Color.Silver;
            this.lblShukeihoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShukeihoho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShukeihoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukeihoho.Location = new System.Drawing.Point(0, 0);
            this.lblShukeihoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShukeihoho.Name = "lblShukeihoho";
            this.lblShukeihoho.Size = new System.Drawing.Size(484, 38);
            this.lblShukeihoho.TabIndex = 46;
            this.lblShukeihoho.Tag = "CHANGE";
            this.lblShukeihoho.Text = "集計方法";
            this.lblShukeihoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGokeiKeisanKubn
            // 
            this.txtGokeiKeisanKubn.AutoSizeFromLength = true;
            this.txtGokeiKeisanKubn.DisplayLength = 2;
            this.txtGokeiKeisanKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokeiKeisanKubn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGokeiKeisanKubn.Location = new System.Drawing.Point(112, 5);
            this.txtGokeiKeisanKubn.Margin = new System.Windows.Forms.Padding(4);
            this.txtGokeiKeisanKubn.MaxLength = 1;
            this.txtGokeiKeisanKubn.Name = "txtGokeiKeisanKubn";
            this.txtGokeiKeisanKubn.Size = new System.Drawing.Size(25, 23);
            this.txtGokeiKeisanKubn.TabIndex = 44;
            this.txtGokeiKeisanKubn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblGokeiKeisanKubn
            // 
            this.lblGokeiKeisanKubn.BackColor = System.Drawing.Color.Silver;
            this.lblGokeiKeisanKubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokeiKeisanKubn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGokeiKeisanKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokeiKeisanKubn.Location = new System.Drawing.Point(0, 0);
            this.lblGokeiKeisanKubn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGokeiKeisanKubn.Name = "lblGokeiKeisanKubn";
            this.lblGokeiKeisanKubn.Size = new System.Drawing.Size(484, 31);
            this.lblGokeiKeisanKubn.TabIndex = 43;
            this.lblGokeiKeisanKubn.Tag = "CHANGE";
            this.lblGokeiKeisanKubn.Text = "合計計算区分";
            this.lblGokeiKeisanKubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShukeiCd
            // 
            this.txtShukeiCd.AutoSizeFromLength = true;
            this.txtShukeiCd.DisplayLength = 2;
            this.txtShukeiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShukeiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShukeiCd.Location = new System.Drawing.Point(112, 3);
            this.txtShukeiCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtShukeiCd.MaxLength = 2;
            this.txtShukeiCd.Name = "txtShukeiCd";
            this.txtShukeiCd.Size = new System.Drawing.Size(25, 23);
            this.txtShukeiCd.TabIndex = 41;
            this.txtShukeiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShukeiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShukeiCd_Validating);
            // 
            // lblSkshKskMemo
            // 
            this.lblSkshKskMemo.BackColor = System.Drawing.Color.LightCyan;
            this.lblSkshKskMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSkshKskMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSkshKskMemo.ForeColor = System.Drawing.Color.Black;
            this.lblSkshKskMemo.Location = new System.Drawing.Point(145, 3);
            this.lblSkshKskMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSkshKskMemo.Name = "lblSkshKskMemo";
            this.lblSkshKskMemo.Size = new System.Drawing.Size(331, 24);
            this.lblSkshKskMemo.TabIndex = 39;
            this.lblSkshKskMemo.Tag = "DISPNAME";
            this.lblSkshKskMemo.Text = "０：しない　１：する";
            this.lblSkshKskMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblSkshKskMemo.Visible = false;
            // 
            // txtKojiKnri
            // 
            this.txtKojiKnri.AutoSizeFromLength = true;
            this.txtKojiKnri.DisplayLength = 2;
            this.txtKojiKnri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojiKnri.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKojiKnri.Location = new System.Drawing.Point(112, 4);
            this.txtKojiKnri.Margin = new System.Windows.Forms.Padding(4);
            this.txtKojiKnri.MaxLength = 1;
            this.txtKojiKnri.Name = "txtKojiKnri";
            this.txtKojiKnri.Size = new System.Drawing.Size(25, 23);
            this.txtKojiKnri.TabIndex = 38;
            this.txtKojiKnri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojiKnri.Visible = false;
            // 
            // lblSkshHkMemo
            // 
            this.lblSkshHkMemo.BackColor = System.Drawing.Color.LightCyan;
            this.lblSkshHkMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSkshHkMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSkshHkMemo.ForeColor = System.Drawing.Color.Black;
            this.lblSkshHkMemo.Location = new System.Drawing.Point(145, 2);
            this.lblSkshHkMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSkshHkMemo.Name = "lblSkshHkMemo";
            this.lblSkshHkMemo.Size = new System.Drawing.Size(331, 24);
            this.lblSkshHkMemo.TabIndex = 36;
            this.lblSkshHkMemo.Tag = "DISPNAME";
            this.lblSkshHkMemo.Text = "１：補助　　２：取引先";
            this.lblSkshHkMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoShiyoKubn
            // 
            this.txtHojoShiyoKubn.AutoSizeFromLength = true;
            this.txtHojoShiyoKubn.DisplayLength = 2;
            this.txtHojoShiyoKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoShiyoKubn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtHojoShiyoKubn.Location = new System.Drawing.Point(112, 3);
            this.txtHojoShiyoKubn.Margin = new System.Windows.Forms.Padding(4);
            this.txtHojoShiyoKubn.MaxLength = 1;
            this.txtHojoShiyoKubn.Name = "txtHojoShiyoKubn";
            this.txtHojoShiyoKubn.Size = new System.Drawing.Size(25, 23);
            this.txtHojoShiyoKubn.TabIndex = 35;
            this.txtHojoShiyoKubn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoShiyoKubn.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoShiyoKubn_Validating);
            // 
            // lblHojoShiyoKubn
            // 
            this.lblHojoShiyoKubn.BackColor = System.Drawing.Color.Silver;
            this.lblHojoShiyoKubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoShiyoKubn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHojoShiyoKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoShiyoKubn.Location = new System.Drawing.Point(0, 0);
            this.lblHojoShiyoKubn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoShiyoKubn.Name = "lblHojoShiyoKubn";
            this.lblHojoShiyoKubn.Size = new System.Drawing.Size(484, 31);
            this.lblHojoShiyoKubn.TabIndex = 34;
            this.lblHojoShiyoKubn.Tag = "CHANGE";
            this.lblHojoShiyoKubn.Text = "補助使用区分";
            this.lblHojoShiyoKubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShimebiMemo
            // 
            this.lblShimebiMemo.BackColor = System.Drawing.Color.LightCyan;
            this.lblShimebiMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebiMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShimebiMemo.ForeColor = System.Drawing.Color.Black;
            this.lblShimebiMemo.Location = new System.Drawing.Point(145, 4);
            this.lblShimebiMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShimebiMemo.Name = "lblShimebiMemo";
            this.lblShimebiMemo.Size = new System.Drawing.Size(331, 24);
            this.lblShimebiMemo.TabIndex = 33;
            this.lblShimebiMemo.Tag = "DISPNAME";
            this.lblShimebiMemo.Text = "０：しない　１：する";
            this.lblShimebiMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoKamokuKnri
            // 
            this.txtHojoKamokuKnri.AutoSizeFromLength = true;
            this.txtHojoKamokuKnri.DisplayLength = null;
            this.txtHojoKamokuKnri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoKamokuKnri.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtHojoKamokuKnri.Location = new System.Drawing.Point(112, 5);
            this.txtHojoKamokuKnri.Margin = new System.Windows.Forms.Padding(4);
            this.txtHojoKamokuKnri.MaxLength = 1;
            this.txtHojoKamokuKnri.Name = "txtHojoKamokuKnri";
            this.txtHojoKamokuKnri.Size = new System.Drawing.Size(25, 23);
            this.txtHojoKamokuKnri.TabIndex = 32;
            this.txtHojoKamokuKnri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblHojoKamokuKnri
            // 
            this.lblHojoKamokuKnri.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuKnri.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokuKnri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHojoKamokuKnri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuKnri.Location = new System.Drawing.Point(0, 0);
            this.lblHojoKamokuKnri.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoKamokuKnri.Name = "lblHojoKamokuKnri";
            this.lblHojoKamokuKnri.Size = new System.Drawing.Size(484, 31);
            this.lblHojoKamokuKnri.TabIndex = 31;
            this.lblHojoKamokuKnri.Tag = "CHANGE";
            this.lblHojoKamokuKnri.Text = "補助科目管理";
            this.lblHojoKamokuKnri.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyusakiNm
            // 
            this.lblSeikyusakiNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblSeikyusakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyusakiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeikyusakiNm.ForeColor = System.Drawing.Color.Black;
            this.lblSeikyusakiNm.Location = new System.Drawing.Point(145, 4);
            this.lblSeikyusakiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSeikyusakiNm.Name = "lblSeikyusakiNm";
            this.lblSeikyusakiNm.Size = new System.Drawing.Size(331, 24);
            this.lblSeikyusakiNm.TabIndex = 30;
            this.lblSeikyusakiNm.Tag = "DISPNAME";
            this.lblSeikyusakiNm.Text = "０：しない　１：する";
            this.lblSeikyusakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonKnri
            // 
            this.txtBumonKnri.AutoSizeFromLength = true;
            this.txtBumonKnri.DisplayLength = null;
            this.txtBumonKnri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonKnri.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBumonKnri.Location = new System.Drawing.Point(112, 5);
            this.txtBumonKnri.Margin = new System.Windows.Forms.Padding(4);
            this.txtBumonKnri.MaxLength = 1;
            this.txtBumonKnri.Name = "txtBumonKnri";
            this.txtBumonKnri.Size = new System.Drawing.Size(25, 23);
            this.txtBumonKnri.TabIndex = 29;
            this.txtBumonKnri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBumonKnri
            // 
            this.lblBumonKnri.BackColor = System.Drawing.Color.Silver;
            this.lblBumonKnri.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonKnri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBumonKnri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonKnri.Location = new System.Drawing.Point(0, 0);
            this.lblBumonKnri.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonKnri.Name = "lblBumonKnri";
            this.lblBumonKnri.Size = new System.Drawing.Size(484, 31);
            this.lblBumonKnri.TabIndex = 28;
            this.lblBumonKnri.Tag = "CHANGE";
            this.lblBumonKnri.Text = "部門管理";
            this.lblBumonKnri.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKashikataKubn
            // 
            this.txtKashikataKubn.AutoSizeFromLength = true;
            this.txtKashikataKubn.DisplayLength = null;
            this.txtKashikataKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKashikataKubn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKashikataKubn.Location = new System.Drawing.Point(119, 6);
            this.txtKashikataKubn.Margin = new System.Windows.Forms.Padding(4);
            this.txtKashikataKubn.MaxLength = 2;
            this.txtKashikataKubn.Name = "txtKashikataKubn";
            this.txtKashikataKubn.Size = new System.Drawing.Size(64, 23);
            this.txtKashikataKubn.TabIndex = 15;
            this.txtKashikataKubn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKashikataKubn.Validating += new System.ComponentModel.CancelEventHandler(this.txtKashikataKubn_Validating);
            // 
            // lblKashikataKubn
            // 
            this.lblKashikataKubn.BackColor = System.Drawing.Color.Silver;
            this.lblKashikataKubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKashikataKubn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKashikataKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKashikataKubn.Location = new System.Drawing.Point(0, 0);
            this.lblKashikataKubn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKashikataKubn.Name = "lblKashikataKubn";
            this.lblKashikataKubn.Size = new System.Drawing.Size(505, 38);
            this.lblKashikataKubn.TabIndex = 14;
            this.lblKashikataKubn.Tag = "CHANGE";
            this.lblKashikataKubn.Text = "貸方税区分";
            this.lblKashikataKubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKarikataKubn
            // 
            this.txtKarikataKubn.AutoSizeFromLength = true;
            this.txtKarikataKubn.DisplayLength = null;
            this.txtKarikataKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKarikataKubn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKarikataKubn.Location = new System.Drawing.Point(119, 5);
            this.txtKarikataKubn.Margin = new System.Windows.Forms.Padding(4);
            this.txtKarikataKubn.MaxLength = 2;
            this.txtKarikataKubn.Name = "txtKarikataKubn";
            this.txtKarikataKubn.Size = new System.Drawing.Size(64, 23);
            this.txtKarikataKubn.TabIndex = 13;
            this.txtKarikataKubn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKarikataKubn.Validating += new System.ComponentModel.CancelEventHandler(this.txtKarikataKubn_Validating);
            // 
            // lblKarikataKubn
            // 
            this.lblKarikataKubn.BackColor = System.Drawing.Color.Silver;
            this.lblKarikataKubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKarikataKubn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKarikataKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKarikataKubn.Location = new System.Drawing.Point(0, 0);
            this.lblKarikataKubn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKarikataKubn.Name = "lblKarikataKubn";
            this.lblKarikataKubn.Size = new System.Drawing.Size(505, 31);
            this.lblKarikataKubn.TabIndex = 12;
            this.lblKarikataKubn.Tag = "CHANGE";
            this.lblKarikataKubn.Text = "借方税区分";
            this.lblKarikataKubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTaishakuKubn
            // 
            this.txtTaishakuKubn.AutoSizeFromLength = false;
            this.txtTaishakuKubn.DisplayLength = null;
            this.txtTaishakuKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuKubn.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTaishakuKubn.Location = new System.Drawing.Point(119, 5);
            this.txtTaishakuKubn.Margin = new System.Windows.Forms.Padding(4);
            this.txtTaishakuKubn.MaxLength = 1;
            this.txtTaishakuKubn.Name = "txtTaishakuKubn";
            this.txtTaishakuKubn.Size = new System.Drawing.Size(64, 23);
            this.txtTaishakuKubn.TabIndex = 11;
            this.txtTaishakuKubn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTaishakuKubn
            // 
            this.lblTaishakuKubn.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakuKubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishakuKubn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTaishakuKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTaishakuKubn.Location = new System.Drawing.Point(0, 0);
            this.lblTaishakuKubn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTaishakuKubn.Name = "lblTaishakuKubn";
            this.lblTaishakuKubn.Size = new System.Drawing.Size(505, 31);
            this.lblTaishakuKubn.TabIndex = 10;
            this.lblTaishakuKubn.Tag = "CHANGE";
            this.lblTaishakuKubn.Text = "貸借区分";
            this.lblTaishakuKubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKamokuKubn
            // 
            this.txtKamokuKubn.AutoSizeFromLength = false;
            this.txtKamokuKubn.DisplayLength = null;
            this.txtKamokuKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuKubn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKamokuKubn.Location = new System.Drawing.Point(119, 4);
            this.txtKamokuKubn.Margin = new System.Windows.Forms.Padding(4);
            this.txtKamokuKubn.MaxLength = 4;
            this.txtKamokuKubn.Name = "txtKamokuKubn";
            this.txtKamokuKubn.Size = new System.Drawing.Size(64, 23);
            this.txtKamokuKubn.TabIndex = 9;
            this.txtKamokuKubn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKamokuKubn.Validating += new System.ComponentModel.CancelEventHandler(this.txtKamokuKubn_Validating);
            // 
            // lblKamokuKubn
            // 
            this.lblKamokuKubn.BackColor = System.Drawing.Color.Silver;
            this.lblKamokuKubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokuKubn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKamokuKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKamokuKubn.Location = new System.Drawing.Point(0, 0);
            this.lblKamokuKubn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKamokuKubn.Name = "lblKamokuKubn";
            this.lblKamokuKubn.Size = new System.Drawing.Size(505, 31);
            this.lblKamokuKubn.TabIndex = 8;
            this.lblKamokuKubn.Tag = "CHANGE";
            this.lblKamokuKubn.Text = "科目区分";
            this.lblKamokuKubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKamokuBnCd
            // 
            this.txtKamokuBnCd.AutoSizeFromLength = true;
            this.txtKamokuBnCd.DisplayLength = null;
            this.txtKamokuBnCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuBnCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKamokuBnCd.Location = new System.Drawing.Point(119, 5);
            this.txtKamokuBnCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtKamokuBnCd.MaxLength = 5;
            this.txtKamokuBnCd.Name = "txtKamokuBnCd";
            this.txtKamokuBnCd.Size = new System.Drawing.Size(64, 23);
            this.txtKamokuBnCd.TabIndex = 5;
            this.txtKamokuBnCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKamokuBnCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKamokuBnCd_Validating);
            // 
            // lblKamokuBnCd
            // 
            this.lblKamokuBnCd.BackColor = System.Drawing.Color.Silver;
            this.lblKamokuBnCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokuBnCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKamokuBnCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKamokuBnCd.Location = new System.Drawing.Point(0, 0);
            this.lblKamokuBnCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKamokuBnCd.Name = "lblKamokuBnCd";
            this.lblKamokuBnCd.Size = new System.Drawing.Size(505, 31);
            this.lblKamokuBnCd.TabIndex = 4;
            this.lblKamokuBnCd.Tag = "CHANGE";
            this.lblKamokuBnCd.Text = "科目分類コード";
            this.lblKamokuBnCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtkanjoKanaNm
            // 
            this.txtkanjoKanaNm.AutoSizeFromLength = false;
            this.txtkanjoKanaNm.DisplayLength = null;
            this.txtkanjoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtkanjoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtkanjoKanaNm.Location = new System.Drawing.Point(118, 5);
            this.txtkanjoKanaNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtkanjoKanaNm.MaxLength = 30;
            this.txtkanjoKanaNm.Name = "txtkanjoKanaNm";
            this.txtkanjoKanaNm.Size = new System.Drawing.Size(355, 23);
            this.txtkanjoKanaNm.TabIndex = 3;
            // 
            // lblkanjoKanaNm
            // 
            this.lblkanjoKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblkanjoKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblkanjoKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblkanjoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblkanjoKanaNm.Location = new System.Drawing.Point(0, 0);
            this.lblkanjoKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblkanjoKanaNm.Name = "lblkanjoKanaNm";
            this.lblkanjoKanaNm.Size = new System.Drawing.Size(505, 31);
            this.lblkanjoKanaNm.TabIndex = 2;
            this.lblkanjoKanaNm.Tag = "CHANGE";
            this.lblkanjoKanaNm.Text = "勘定科目カナ名";
            this.lblkanjoKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtkanjoNm
            // 
            this.txtkanjoNm.AutoSizeFromLength = false;
            this.txtkanjoNm.DisplayLength = null;
            this.txtkanjoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtkanjoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtkanjoNm.Location = new System.Drawing.Point(118, 5);
            this.txtkanjoNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtkanjoNm.MaxLength = 40;
            this.txtkanjoNm.Name = "txtkanjoNm";
            this.txtkanjoNm.Size = new System.Drawing.Size(355, 23);
            this.txtkanjoNm.TabIndex = 1;
            // 
            // lblKanjoNm
            // 
            this.lblKanjoNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoNm.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoNm.Name = "lblKanjoNm";
            this.lblKanjoNm.Size = new System.Drawing.Size(505, 31);
            this.lblKanjoNm.TabIndex = 0;
            this.lblKanjoNm.Tag = "CHANGE";
            this.lblKanjoNm.Text = "勘定科目名";
            this.lblKanjoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShukeiCd
            // 
            this.lblShukeiCd.BackColor = System.Drawing.Color.Silver;
            this.lblShukeiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShukeiCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShukeiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukeiCd.Location = new System.Drawing.Point(0, 0);
            this.lblShukeiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShukeiCd.Name = "lblShukeiCd";
            this.lblShukeiCd.Size = new System.Drawing.Size(484, 31);
            this.lblShukeiCd.TabIndex = 40;
            this.lblShukeiCd.Tag = "CHANGE";
            this.lblShukeiCd.Text = "集計コード";
            this.lblShukeiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKojiKnri
            // 
            this.lblKojiKnri.BackColor = System.Drawing.Color.Silver;
            this.lblKojiKnri.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojiKnri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKojiKnri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojiKnri.Location = new System.Drawing.Point(0, 0);
            this.lblKojiKnri.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKojiKnri.Name = "lblKojiKnri";
            this.lblKojiKnri.Size = new System.Drawing.Size(484, 31);
            this.lblKojiKnri.TabIndex = 37;
            this.lblKojiKnri.Tag = "CHANGE";
            this.lblKojiKnri.Text = "工事管理";
            this.lblKojiKnri.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblKojiKnri.Visible = false;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(16, 80);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 7;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(997, 274);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.fsiPanel21);
            this.fsiPanel7.Controls.Add(this.fsiPanel20);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(4, 232);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(989, 38);
            this.fsiPanel7.TabIndex = 6;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // fsiPanel21
            // 
            this.fsiPanel21.Controls.Add(this.txtShukeihoho);
            this.fsiPanel21.Controls.Add(this.lblShukeihoho);
            this.fsiPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel21.Location = new System.Drawing.Point(505, 0);
            this.fsiPanel21.Name = "fsiPanel21";
            this.fsiPanel21.Size = new System.Drawing.Size(484, 38);
            this.fsiPanel21.TabIndex = 908;
            this.fsiPanel21.Tag = "CHANGE";
            // 
            // fsiPanel20
            // 
            this.fsiPanel20.Controls.Add(this.lblkashi);
            this.fsiPanel20.Controls.Add(this.txtKashikataKubn);
            this.fsiPanel20.Controls.Add(this.lblKashikataKubn);
            this.fsiPanel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel20.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel20.Name = "fsiPanel20";
            this.fsiPanel20.Size = new System.Drawing.Size(505, 38);
            this.fsiPanel20.TabIndex = 908;
            this.fsiPanel20.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.fsiPanel19);
            this.fsiPanel6.Controls.Add(this.fsiPanel18);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 194);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(989, 31);
            this.fsiPanel6.TabIndex = 5;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // fsiPanel19
            // 
            this.fsiPanel19.Controls.Add(this.txtGokeiKeisanKubn);
            this.fsiPanel19.Controls.Add(this.lblGokeiKeisanKubn);
            this.fsiPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel19.Location = new System.Drawing.Point(505, 0);
            this.fsiPanel19.Name = "fsiPanel19";
            this.fsiPanel19.Size = new System.Drawing.Size(484, 31);
            this.fsiPanel19.TabIndex = 910;
            this.fsiPanel19.Tag = "CHANGE";
            // 
            // fsiPanel18
            // 
            this.fsiPanel18.Controls.Add(this.lblkari);
            this.fsiPanel18.Controls.Add(this.txtKarikataKubn);
            this.fsiPanel18.Controls.Add(this.lblKarikataKubn);
            this.fsiPanel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel18.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel18.Name = "fsiPanel18";
            this.fsiPanel18.Size = new System.Drawing.Size(505, 31);
            this.fsiPanel18.TabIndex = 909;
            this.fsiPanel18.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.fsiPanel17);
            this.fsiPanel5.Controls.Add(this.fsiPanel16);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 156);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(989, 31);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel17
            // 
            this.fsiPanel17.Controls.Add(this.txtShukeiCd);
            this.fsiPanel17.Controls.Add(this.lblSkshKskMemo);
            this.fsiPanel17.Controls.Add(this.lblShukeiCd);
            this.fsiPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel17.Location = new System.Drawing.Point(505, 0);
            this.fsiPanel17.Name = "fsiPanel17";
            this.fsiPanel17.Size = new System.Drawing.Size(484, 31);
            this.fsiPanel17.TabIndex = 908;
            this.fsiPanel17.Tag = "CHANGE";
            // 
            // fsiPanel16
            // 
            this.fsiPanel16.Controls.Add(this.txtTaishakuKubn);
            this.fsiPanel16.Controls.Add(this.label4);
            this.fsiPanel16.Controls.Add(this.lblTaishakuKubn);
            this.fsiPanel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel16.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel16.Name = "fsiPanel16";
            this.fsiPanel16.Size = new System.Drawing.Size(505, 31);
            this.fsiPanel16.TabIndex = 908;
            this.fsiPanel16.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.fsiPanel15);
            this.fsiPanel4.Controls.Add(this.fsiPanel14);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 118);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(989, 31);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel15
            // 
            this.fsiPanel15.Controls.Add(this.txtKojiKnri);
            this.fsiPanel15.Controls.Add(this.lblKojiKnri);
            this.fsiPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel15.Location = new System.Drawing.Point(505, 0);
            this.fsiPanel15.Name = "fsiPanel15";
            this.fsiPanel15.Size = new System.Drawing.Size(484, 31);
            this.fsiPanel15.TabIndex = 908;
            this.fsiPanel15.Tag = "CHANGE";
            // 
            // fsiPanel14
            // 
            this.fsiPanel14.Controls.Add(this.txtKamokuKubn);
            this.fsiPanel14.Controls.Add(this.txtKamokuKubnNm);
            this.fsiPanel14.Controls.Add(this.lblKamokuKubn);
            this.fsiPanel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel14.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel14.Name = "fsiPanel14";
            this.fsiPanel14.Size = new System.Drawing.Size(505, 31);
            this.fsiPanel14.TabIndex = 907;
            this.fsiPanel14.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.fsiPanel13);
            this.fsiPanel3.Controls.Add(this.fsiPanel12);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 80);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(989, 31);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel13
            // 
            this.fsiPanel13.Controls.Add(this.txtHojoShiyoKubn);
            this.fsiPanel13.Controls.Add(this.lblSkshHkMemo);
            this.fsiPanel13.Controls.Add(this.lblHojoShiyoKubn);
            this.fsiPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel13.Location = new System.Drawing.Point(505, 0);
            this.fsiPanel13.Name = "fsiPanel13";
            this.fsiPanel13.Size = new System.Drawing.Size(484, 31);
            this.fsiPanel13.TabIndex = 907;
            this.fsiPanel13.Tag = "CHANGE";
            // 
            // fsiPanel12
            // 
            this.fsiPanel12.Controls.Add(this.txtKamokuBnCd);
            this.fsiPanel12.Controls.Add(this.txtKamokuBnNm);
            this.fsiPanel12.Controls.Add(this.lblKamokuBnCd);
            this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel12.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel12.Name = "fsiPanel12";
            this.fsiPanel12.Size = new System.Drawing.Size(505, 31);
            this.fsiPanel12.TabIndex = 906;
            this.fsiPanel12.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.fsiPanel11);
            this.fsiPanel2.Controls.Add(this.fsiPanel10);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 42);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(989, 31);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel11
            // 
            this.fsiPanel11.Controls.Add(this.txtHojoKamokuKnri);
            this.fsiPanel11.Controls.Add(this.lblShimebiMemo);
            this.fsiPanel11.Controls.Add(this.lblHojoKamokuKnri);
            this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel11.Location = new System.Drawing.Point(505, 0);
            this.fsiPanel11.Name = "fsiPanel11";
            this.fsiPanel11.Size = new System.Drawing.Size(484, 31);
            this.fsiPanel11.TabIndex = 905;
            this.fsiPanel11.Tag = "CHANGE";
            // 
            // fsiPanel10
            // 
            this.fsiPanel10.Controls.Add(this.txtkanjoKanaNm);
            this.fsiPanel10.Controls.Add(this.lblkanjoKanaNm);
            this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel10.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel10.Name = "fsiPanel10";
            this.fsiPanel10.Size = new System.Drawing.Size(505, 31);
            this.fsiPanel10.TabIndex = 906;
            this.fsiPanel10.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel9);
            this.fsiPanel1.Controls.Add(this.fsiPanel8);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(989, 31);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.txtBumonKnri);
            this.fsiPanel9.Controls.Add(this.lblSeikyusakiNm);
            this.fsiPanel9.Controls.Add(this.lblBumonKnri);
            this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel9.Location = new System.Drawing.Point(505, 0);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(484, 31);
            this.fsiPanel9.TabIndex = 904;
            this.fsiPanel9.Tag = "CHANGE";
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.txtkanjoNm);
            this.fsiPanel8.Controls.Add(this.lblKanjoNm);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel8.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(505, 31);
            this.fsiPanel8.TabIndex = 903;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel22, 0, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(16, 34);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 1;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(391, 40);
            this.fsiTableLayoutPanel2.TabIndex = 902;
            // 
            // fsiPanel22
            // 
            this.fsiPanel22.Controls.Add(this.txtkanjoCd);
            this.fsiPanel22.Controls.Add(this.lblKanjoCd);
            this.fsiPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel22.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel22.Name = "fsiPanel22";
            this.fsiPanel22.Size = new System.Drawing.Size(383, 32);
            this.fsiPanel22.TabIndex = 0;
            // 
            // ZMCM1032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 507);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCM1032";
            this.ShowFButton = true;
            this.Text = "勘定科目の登録";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel21.ResumeLayout(false);
            this.fsiPanel21.PerformLayout();
            this.fsiPanel20.ResumeLayout(false);
            this.fsiPanel20.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel19.ResumeLayout(false);
            this.fsiPanel19.PerformLayout();
            this.fsiPanel18.ResumeLayout(false);
            this.fsiPanel18.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel17.ResumeLayout(false);
            this.fsiPanel17.PerformLayout();
            this.fsiPanel16.ResumeLayout(false);
            this.fsiPanel16.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel15.ResumeLayout(false);
            this.fsiPanel15.PerformLayout();
            this.fsiPanel14.ResumeLayout(false);
            this.fsiPanel14.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel13.ResumeLayout(false);
            this.fsiPanel13.PerformLayout();
            this.fsiPanel12.ResumeLayout(false);
            this.fsiPanel12.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel11.ResumeLayout(false);
            this.fsiPanel11.PerformLayout();
            this.fsiPanel10.ResumeLayout(false);
            this.fsiPanel10.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel9.PerformLayout();
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.fsiPanel22.ResumeLayout(false);
            this.fsiPanel22.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtkanjoCd;
        private System.Windows.Forms.Label lblKanjoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtkanjoNm;
        private System.Windows.Forms.Label lblKanjoNm;
        private System.Windows.Forms.Label lblkanjoKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtkanjoKanaNm;
        private System.Windows.Forms.Label lblKamokuBnCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBnCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuKubn;
        private System.Windows.Forms.Label lblKamokuKubn;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuKubn;
        private System.Windows.Forms.Label lblTaishakuKubn;
        private System.Windows.Forms.Label lblKarikataKubn;
        private jp.co.fsi.common.controls.FsiTextBox txtKarikataKubn;
        private jp.co.fsi.common.controls.FsiTextBox txtKashikataKubn;
        private System.Windows.Forms.Label lblKashikataKubn;
        private System.Windows.Forms.Label lblSeikyusakiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonKnri;
        private System.Windows.Forms.Label lblBumonKnri;
        private System.Windows.Forms.Label lblHojoKamokuKnri;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuKnri;
        private System.Windows.Forms.Label lblShimebiMemo;
        private System.Windows.Forms.Label lblSkshHkMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoShiyoKubn;
        private System.Windows.Forms.Label lblHojoShiyoKubn;
        private System.Windows.Forms.Label lblSkshKskMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtKojiKnri;
        private System.Windows.Forms.Label lblKojiKnri;
        private jp.co.fsi.common.controls.FsiTextBox txtShukeiCd;
        private System.Windows.Forms.Label lblShukeiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGokeiKeisanKubn;
        private System.Windows.Forms.Label lblGokeiKeisanKubn;
        private jp.co.fsi.common.controls.FsiTextBox txtShukeihoho;
        private System.Windows.Forms.Label lblShukeihoho;
        private System.Windows.Forms.Label lblkashi;
        private System.Windows.Forms.Label lblkari;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label txtKamokuKubnNm;
        private System.Windows.Forms.Label txtKamokuBnNm;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel21;
        private common.FsiPanel fsiPanel20;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel19;
        private common.FsiPanel fsiPanel18;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel17;
        private common.FsiPanel fsiPanel16;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel15;
        private common.FsiPanel fsiPanel14;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel13;
        private common.FsiPanel fsiPanel12;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel10;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel8;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiPanel fsiPanel22;
    }
}