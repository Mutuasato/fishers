﻿namespace jp.co.fsi.zm.zmcm1031
{
    /// <summary>
    /// ZAMC9011R の概要の説明です。
    /// </summary>
    partial class ZMCM10311R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMCM10311R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday_tate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtToday_tate,
            this.lblPage,
            this.txtPageCount,
            this.txtTitleName,
            this.line1,
            this.txtCompanyName,
            this.txtTitle02,
            this.txtTitle01,
            this.txtTitle03,
            this.txtTitle04,
            this.txtTitle05,
            this.txtTitle06,
            this.txtTitle07,
            this.txtTitle08,
            this.line2,
            this.line3,
            this.line6,
            this.line5,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line4});
            this.pageHeader.Height = 0.9755909F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtToday_tate
            // 
            this.txtToday_tate.Height = 0.1968504F;
            this.txtToday_tate.Left = 5.796851F;
            this.txtToday_tate.MultiLine = false;
            this.txtToday_tate.Name = "txtToday_tate";
            this.txtToday_tate.OutputFormat = resources.GetString("txtToday_tate.OutputFormat");
            this.txtToday_tate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtToday_tate.Text = null;
            this.txtToday_tate.Top = 0F;
            this.txtToday_tate.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.325198F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 7.009448F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 2.779134F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle";
            this.txtTitleName.Text = "勘定科目リスト";
            this.txtTitleName.Top = 0F;
            this.txtTitleName.Width = 1.941733F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.779134F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2559055F;
            this.line1.Width = 1.941731F;
            this.line1.X1 = 2.779134F;
            this.line1.X2 = 4.720865F;
            this.line1.Y1 = 0.2559055F;
            this.line1.Y2 = 0.2559055F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 1";
            this.txtCompanyName.Text = null;
            this.txtCompanyName.Top = 0.3531496F;
            this.txtCompanyName.Width = 2.708268F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.3744094F;
            this.txtTitle02.Left = 0.476378F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle02.Text = "勘定科目名";
            this.txtTitle02.Top = 0.5972441F;
            this.txtTitle02.Width = 1.740158F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.3744093F;
            this.txtTitle01.Left = 0F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle01.Text = "ｺｰﾄﾞ";
            this.txtTitle01.Top = 0.5972441F;
            this.txtTitle01.Width = 0.476378F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.3744094F;
            this.txtTitle03.Left = 2.216536F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle03.Text = "貸借";
            this.txtTitle03.Top = 0.5944882F;
            this.txtTitle03.Width = 0.3937008F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.3744094F;
            this.txtTitle04.Left = 2.610236F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle04.Text = "借方税区分";
            this.txtTitle04.Top = 0.5944882F;
            this.txtTitle04.Width = 1.929134F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.Height = 0.3744094F;
            this.txtTitle05.Left = 4.53937F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle05.Text = "貸方税区分";
            this.txtTitle05.Top = 0.5972441F;
            this.txtTitle05.Width = 1.929134F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.Height = 0.3744094F;
            this.txtTitle06.Left = 6.468504F;
            this.txtTitle06.MultiLine = false;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle06.Text = "部門";
            this.txtTitle06.Top = 0.5972441F;
            this.txtTitle06.Width = 0.3346457F;
            // 
            // txtTitle07
            // 
            this.txtTitle07.Height = 0.3744094F;
            this.txtTitle07.Left = 6.814961F;
            this.txtTitle07.MultiLine = false;
            this.txtTitle07.Name = "txtTitle07";
            this.txtTitle07.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle07.Text = "補助";
            this.txtTitle07.Top = 0.5972441F;
            this.txtTitle07.Width = 0.3346457F;
            // 
            // txtTitle08
            // 
            this.txtTitle08.Height = 0.3744094F;
            this.txtTitle08.Left = 7.149607F;
            this.txtTitle08.MultiLine = false;
            this.txtTitle08.Name = "txtTitle08";
            this.txtTitle08.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.txtTitle08.Text = "集計";
            this.txtTitle08.Top = 0.5944882F;
            this.txtTitle08.Width = 0.3346457F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.5972443F;
            this.line2.Width = 7.484252F;
            this.line2.X1 = 0F;
            this.line2.X2 = 7.484252F;
            this.line2.Y1 = 0.5972443F;
            this.line2.Y2 = 0.5972443F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.9755906F;
            this.line3.Width = 7.48425F;
            this.line3.X1 = 0F;
            this.line3.X2 = 7.48425F;
            this.line3.Y1 = 0.9755906F;
            this.line3.Y2 = 0.9755906F;
            // 
            // line6
            // 
            this.line6.Height = 0.3744095F;
            this.line6.Left = 0.4763779F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.6011811F;
            this.line6.Width = 8.940697E-08F;
            this.line6.X1 = 0.476378F;
            this.line6.X2 = 0.4763779F;
            this.line6.Y1 = 0.6011811F;
            this.line6.Y2 = 0.9755906F;
            // 
            // line5
            // 
            this.line5.Height = 0.3744098F;
            this.line5.Left = 2.216536F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.5972441F;
            this.line5.Width = 0F;
            this.line5.X1 = 2.216536F;
            this.line5.X2 = 2.216536F;
            this.line5.Y1 = 0.5972441F;
            this.line5.Y2 = 0.9716539F;
            // 
            // line7
            // 
            this.line7.Height = 0.3744097F;
            this.line7.Left = 2.610236F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.5944882F;
            this.line7.Width = 0F;
            this.line7.X1 = 2.610236F;
            this.line7.X2 = 2.610236F;
            this.line7.Y1 = 0.5944882F;
            this.line7.Y2 = 0.9688979F;
            // 
            // line8
            // 
            this.line8.Height = 0.3744098F;
            this.line8.Left = 4.539371F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.6011811F;
            this.line8.Width = 0F;
            this.line8.X1 = 4.539371F;
            this.line8.X2 = 4.539371F;
            this.line8.Y1 = 0.6011811F;
            this.line8.Y2 = 0.9755909F;
            // 
            // line9
            // 
            this.line9.Height = 0.3744097F;
            this.line9.Left = 0F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0.5972441F;
            this.line9.Width = 0F;
            this.line9.X1 = 0F;
            this.line9.X2 = 0F;
            this.line9.Y1 = 0.5972441F;
            this.line9.Y2 = 0.9716538F;
            // 
            // line10
            // 
            this.line10.Height = 0.3744097F;
            this.line10.Left = 6.468504F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0.5944882F;
            this.line10.Width = 0F;
            this.line10.X1 = 6.468504F;
            this.line10.X2 = 6.468504F;
            this.line10.Y1 = 0.5944882F;
            this.line10.Y2 = 0.9688979F;
            // 
            // line11
            // 
            this.line11.Height = 0.3744098F;
            this.line11.Left = 6.80315F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0.5972441F;
            this.line11.Width = 0F;
            this.line11.X1 = 6.80315F;
            this.line11.X2 = 6.80315F;
            this.line11.Y1 = 0.5972441F;
            this.line11.Y2 = 0.9716539F;
            // 
            // line12
            // 
            this.line12.Height = 0.3744097F;
            this.line12.Left = 7.149607F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.5944882F;
            this.line12.Width = 0F;
            this.line12.X1 = 7.149607F;
            this.line12.X2 = 7.149607F;
            this.line12.Y1 = 0.5944882F;
            this.line12.Y2 = 0.9688979F;
            // 
            // line13
            // 
            this.line13.Height = 0.3744097F;
            this.line13.Left = 7.484252F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0.5972441F;
            this.line13.Width = 0F;
            this.line13.X1 = 7.484252F;
            this.line13.X2 = 7.484252F;
            this.line13.Y1 = 0.5972441F;
            this.line13.Y2 = 0.9716538F;
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 0F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.9716536F;
            this.line4.Width = 7.484251F;
            this.line4.X1 = 0F;
            this.line4.X2 = 7.484251F;
            this.line4.Y1 = 0.9716536F;
            this.line4.Y2 = 0.9716536F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.txtValue01,
            this.txtValue02,
            this.txtValue05,
            this.txtValue03,
            this.txtValue04,
            this.txtValue07,
            this.txtValue06,
            this.txtValue08,
            this.txtValue09,
            this.txtValue10,
            this.line14});
            this.detail.Height = 0.2480315F;
            this.detail.Name = "detail";
            // 
            // line15
            // 
            this.line15.Height = 0.2480315F;
            this.line15.Left = 0.476378F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 0.476378F;
            this.line15.X2 = 0.476378F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.2480315F;
            // 
            // line16
            // 
            this.line16.Height = 0.2480315F;
            this.line16.Left = 2.216536F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 2.216536F;
            this.line16.X2 = 2.216536F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.2480315F;
            // 
            // line17
            // 
            this.line17.Height = 0.2480315F;
            this.line17.Left = 2.610236F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0F;
            this.line17.Width = 0F;
            this.line17.X1 = 2.610236F;
            this.line17.X2 = 2.610236F;
            this.line17.Y1 = 0F;
            this.line17.Y2 = 0.2480315F;
            // 
            // line18
            // 
            this.line18.Height = 0.2480315F;
            this.line18.Left = 4.539371F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 4.539371F;
            this.line18.X2 = 4.539371F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.2480315F;
            // 
            // line19
            // 
            this.line19.Height = 0.2480315F;
            this.line19.Left = 6.468504F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0F;
            this.line19.Width = 0F;
            this.line19.X1 = 6.468504F;
            this.line19.X2 = 6.468504F;
            this.line19.Y1 = 0F;
            this.line19.Y2 = 0.2480315F;
            // 
            // line20
            // 
            this.line20.Height = 0.2480315F;
            this.line20.Left = 6.80315F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0F;
            this.line20.Width = 0F;
            this.line20.X1 = 6.80315F;
            this.line20.X2 = 6.80315F;
            this.line20.Y1 = 0F;
            this.line20.Y2 = 0.2480315F;
            // 
            // line21
            // 
            this.line21.Height = 0.2480315F;
            this.line21.Left = 7.149607F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0F;
            this.line21.Width = 0F;
            this.line21.X1 = 7.149607F;
            this.line21.X2 = 7.149607F;
            this.line21.Y1 = 0F;
            this.line21.Y2 = 0.2480315F;
            // 
            // line22
            // 
            this.line22.Height = 0.2480315F;
            this.line22.Left = 7.484252F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0F;
            this.line22.Width = 0F;
            this.line22.X1 = 7.484252F;
            this.line22.X2 = 7.484252F;
            this.line22.Y1 = 0F;
            this.line22.Y2 = 0.2480315F;
            // 
            // line23
            // 
            this.line23.Height = 0.2480315F;
            this.line23.Left = 0F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0F;
            this.line23.Width = 0F;
            this.line23.X1 = 0F;
            this.line23.X2 = 0F;
            this.line23.Y1 = 0F;
            this.line23.Y2 = 0.2480315F;
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM02";
            this.txtValue01.Height = 0.2362205F;
            this.txtValue01.Left = 0.01968504F;
            this.txtValue01.MultiLine = false;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0F;
            this.txtValue01.Width = 0.4448819F;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM03";
            this.txtValue02.Height = 0.2362205F;
            this.txtValue02.Left = 0.5275591F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0F;
            this.txtValue02.Width = 1.681102F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM06";
            this.txtValue05.Height = 0.2362205F;
            this.txtValue05.Left = 2.850394F;
            this.txtValue05.MultiLine = false;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0F;
            this.txtValue05.Width = 1.673228F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM04";
            this.txtValue03.Height = 0.2362205F;
            this.txtValue03.Left = 2.248032F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.txtValue03.Text = null;
            this.txtValue03.Top = 0F;
            this.txtValue03.Width = 0.3543307F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM05";
            this.txtValue04.Height = 0.2362205F;
            this.txtValue04.Left = 2.634252F;
            this.txtValue04.MultiLine = false;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0F;
            this.txtValue04.Width = 0.1968504F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM08";
            this.txtValue07.Height = 0.2362205F;
            this.txtValue07.Left = 4.775197F;
            this.txtValue07.MultiLine = false;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0F;
            this.txtValue07.Width = 1.673228F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM07";
            this.txtValue06.Height = 0.2362205F;
            this.txtValue06.Left = 4.562993F;
            this.txtValue06.MultiLine = false;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue06.Text = null;
            this.txtValue06.Top = 0F;
            this.txtValue06.Width = 0.1968504F;
            // 
            // txtValue08
            // 
            this.txtValue08.DataField = "ITEM09";
            this.txtValue08.Height = 0.2362205F;
            this.txtValue08.Left = 6.488189F;
            this.txtValue08.MultiLine = false;
            this.txtValue08.Name = "txtValue08";
            this.txtValue08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue08.Text = null;
            this.txtValue08.Top = 0F;
            this.txtValue08.Width = 0.2952756F;
            // 
            // txtValue09
            // 
            this.txtValue09.DataField = "ITEM10";
            this.txtValue09.Height = 0.2362205F;
            this.txtValue09.Left = 6.826772F;
            this.txtValue09.MultiLine = false;
            this.txtValue09.Name = "txtValue09";
            this.txtValue09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue09.Text = null;
            this.txtValue09.Top = 0F;
            this.txtValue09.Width = 0.3110236F;
            // 
            // txtValue10
            // 
            this.txtValue10.DataField = "ITEM11";
            this.txtValue10.Height = 0.2362205F;
            this.txtValue10.Left = 7.165355F;
            this.txtValue10.MultiLine = false;
            this.txtValue10.Name = "txtValue10";
            this.txtValue10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue10.Text = null;
            this.txtValue10.Top = 0F;
            this.txtValue10.Width = 0.3070866F;
            // 
            // line14
            // 
            this.line14.Height = 0F;
            this.line14.Left = -1.110223E-16F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0.2480315F;
            this.line14.Width = 7.484252F;
            this.line14.X1 = -1.110223E-16F;
            this.line14.X2 = 7.484252F;
            this.line14.Y1 = 0.2480315F;
            this.line14.Y2 = 0.2480315F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // ZAMC9011R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.1968504F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.484252F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday_tate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue10;
    }
}
