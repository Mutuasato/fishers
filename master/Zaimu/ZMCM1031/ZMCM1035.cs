﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1031
{
    /// <summary>
    /// 勘定科目の印刷(ZMCM1035)
    /// </summary>
    public partial class ZMCM1035 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// データ取得用
        /// </summary>
        private const int MAX_LINE_NO = 40;
        #endregion

        #region 変数
        private bool _dtFlg = new bool();
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1035()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // EscapeとF1のみ表示
            //this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 科目分類コードにフォーカスを当てる
            this.txtKamokuBunruiCdFr.SelectAll();
            this.txtKamokuBunruiCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 科目分類コード、科目区分、借方税区分、貸方税区分に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtKamokuBunruiCdFr":
                case "txtKamokuBunruiCdTo":
                case "txtKanjoKamokuCdFr":
                case "txtKanjoKamokuCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtKamokuBunruiCdFr":
                case "txtKamokuBunruiCdTo":
                    #region 科目分類
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_KAMOKU_BUNRUI";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                if (this.ActiveCtlNm.Equals("txtKamokuBunruiCdFr"))
                                {
                                    this.txtKamokuBunruiCdFr.Text = result[0];
                                    this.lblKamokuBunruiCdFr.Text = result[1];
                                }
                                else
                                {
                                    this.txtKamokuBunruiCdTo.Text = result[0];
                                    this.lblKamokuBunruiCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKanjoKamokuCdFr":
                case "txtKanjoKamokuCdTo":
                    #region 勘定科目
                    //アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9013"); ;
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            if (this.ActiveCtlNm.Equals("txtKanjoKamokuCdFr"))
                            {
                                frm.InData = this.txtKanjoKamokuCdFr.Text;
                            }
                            else
                            {
                                frm.InData = this.txtKanjoKamokuCdTo.Text;
                            }
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                if (this.ActiveCtlNm.Equals("txtKanjoKamokuCdFr"))
                                {
                                    this.txtKanjoKamokuCdFr.Text = result[0];
                                    this.lblKanjoKamokuCdFr.Text = result[1];
                                }
                                else
                                {
                                    this.txtKanjoKamokuCdTo.Text = result[0];
                                    this.lblKanjoKamokuCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF5();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 科目分類コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKamokuBunruiCdFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKamokuBunruiCdFr.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_KAMOKU_BUNRUI", this.UInfo.ShishoCd, this.txtKamokuBunruiCdFr.Text);
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtKamokuBunruiCdFr.Text))
            {
                this.lblKamokuBunruiCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 科目分類コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKamokuBunruiCdTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKamokuBunruiCdTo.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_KAMOKU_BUNRUI", this.UInfo.ShishoCd, this.txtKamokuBunruiCdTo.Text);
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtKamokuBunruiCdTo.Text))
            {
                this.lblKamokuBunruiCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 勘定科目コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCdFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKanjoKamokuCdFr.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKanjoKamokuCdFr.Text);
            if (ValChk.IsEmpty(this.txtKanjoKamokuCdFr.Text))
            {
                this.lblKanjoKamokuCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 勘定科目コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCdTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblKanjoKamokuCdTo.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKanjoKamokuCdTo.Text);
            if (ValChk.IsEmpty(this.txtKanjoKamokuCdTo.Text))
            {
                this.lblKanjoKamokuCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 勘定科目のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this._dtFlg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // ﾌﾟﾚﾋﾞｭｰ処理の呼び出し
                this.btnF4.PerformClick();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 科目分類コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidtxtKamokuBunruiCdFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKamokuBunruiCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 科目分類コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidtxtKamokuBunruiCdTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKamokuBunruiCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コード(自)の入力チェック
        /// </summary>
        private bool IsValidtxtKanjoKamokuCdFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamokuCdFr.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コード(至)の入力チェック
        /// </summary>
        private bool IsValidtxtKanjoKamokuCdTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKanjoKamokuCdTo.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 単項目チェック
            /// 科目分類コード(自)
            if (!IsValidtxtKamokuBunruiCdFr())
            {
                this.txtKamokuBunruiCdFr.Focus();
                this.txtKamokuBunruiCdFr.SelectAll();
                return false;
            }

            /// 科目分類コード(至)
            if (!IsValidtxtKamokuBunruiCdTo())
            {
                this.txtKamokuBunruiCdTo.Focus();
                this.txtKamokuBunruiCdTo.SelectAll();
                return false;
            }

            /// 勘定科目コード(自)
            if (!IsValidtxtKanjoKamokuCdFr())
            {
                this.txtKanjoKamokuCdFr.Focus();
                this.txtKanjoKamokuCdFr.SelectAll();
                return false;
            }

            /// 勘定科目コード(至)
            if (!IsValidtxtKanjoKamokuCdTo())
            {
                this.txtKanjoKamokuCdTo.Focus();
                this.txtKanjoKamokuCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMCM10311R rpt = new ZMCM10311R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region 設定値をセットする
            /* コード範囲を取得 */
            // 科目分類コードを取得
            string KamokuBunruiCdFr;
            string KamokuBunruiCdTo;
            if (Util.ToString(this.txtKamokuBunruiCdFr.Text) != "")
            {
                KamokuBunruiCdFr = this.txtKamokuBunruiCdFr.Text;
            }
            else
            {
                KamokuBunruiCdFr = "0";
            }
            if (Util.ToString(this.txtKamokuBunruiCdTo.Text) != "")
            {
                KamokuBunruiCdTo = this.txtKamokuBunruiCdTo.Text;
            }
            else
            {
                KamokuBunruiCdTo = "99999";
            }
            // 勘定科目コードを取得
            string KanjoKamokuCdFr;
            string KanjoKamokuCdTo;
            if (Util.ToString(this.txtKanjoKamokuCdFr.Text) != "")
            {
                KanjoKamokuCdFr = this.txtKanjoKamokuCdFr.Text;
            }
            else
            {
                KanjoKamokuCdFr = "0";
            }
            if (Util.ToString(this.txtKanjoKamokuCdTo.Text) != "")
            {
                KanjoKamokuCdTo = this.txtKanjoKamokuCdTo.Text;
            }
            else
            {
                KanjoKamokuCdTo = "999999";
            }
            #endregion

            #region データを取得する
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" VI_ZM_KANJO_KAMOKU ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND @KANJO_KAMOKU_CD_TO AND ");
            Sql.Append(" KAMOKU_BUNRUI_CD BETWEEN @KAMOKU_BUNRUI_CD_FR AND @KAMOKU_BUNRUI_CD_TO ");
            Sql.Append("ORDER BY");
            Sql.Append(" KAISHA_CD,");
            Sql.Append(" KAMOKU_BUNRUI_CD,");
            Sql.Append(" KANJO_KAMOKU_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd); // 会社コード
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo); // 会計年度
            dpc.SetParam("@KANJO_KAMOKU_CD_FR", SqlDbType.Decimal, 6, KanjoKamokuCdFr); // 勘定科目コードFr
            dpc.SetParam("@KANJO_KAMOKU_CD_TO", SqlDbType.Decimal, 6, KanjoKamokuCdTo); // 勘定科目コードTo
            dpc.SetParam("@KAMOKU_BUNRUI_CD_FR", SqlDbType.Decimal, 5, KamokuBunruiCdFr); // 科目分類コードFr
            dpc.SetParam("@KAMOKU_BUNRUI_CD_TO", SqlDbType.Decimal, 5, KamokuBunruiCdTo); // 科目分類コードTo

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                #region 印刷ワークテーブルに登録
                int i = 0; // ソート用変数
                decimal beforeKamokuBunruiCd = -1; // 科目分類コード保持用変数
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    // 科目分類を表示する
                    if (beforeKamokuBunruiCd != (decimal)dr["KAMOKU_BUNRUI_CD"] || i % MAX_LINE_NO == 0)
                    {
                        #region 空行を表示する
                        if ((i + 1) % MAX_LINE_NO == 0)
                        {
                            Sql = new StringBuilder();
                            Sql.Append("INSERT INTO PR_ZM_TBL(");
                            Sql.Append(" GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(") ");

                            dpc = new DbParamCollection();
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            i++;
                        }
                        #endregion

                        #region
                        Sql = new StringBuilder();
                        Sql.Append("INSERT INTO PR_ZM_TBL(");
                        Sql.Append(" GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(") ");
                        #endregion

                        #region データをセットする
                        dpc = new DbParamCollection();
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dr["KAMOKU_BUNRUI_CD"].ToString()); // 科目分類コード
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "【" + dr["KAMOKU_BUNRUI_NM"].ToString() + "】"); // 科目分類名
                        #endregion

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        i++;

                        // 科目分類コードを保持する
                        beforeKamokuBunruiCd = (decimal)dr["KAMOKU_BUNRUI_CD"];
                    }

                    #region
                    Sql = new StringBuilder();
                    Sql.Append("INSERT INTO PR_ZM_TBL(");
                    Sql.Append(" GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(") ");
                    #endregion

                    #region データをセットする
                    dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_CD"].ToString()); // コード
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_NM"].ToString()); // 勘定科目名
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["TAISHAKU_KUBUN_NM"].ToString()); // 貸借
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["KARIKATA_ZEI_KUBUN"].ToString()); // 借方税区分コード
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["KARIKATA_ZEI_KUBUN_NM"].ToString()); // 貸方税区分
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["KASHIKATA_ZEI_KUBUN"].ToString()); // 貸方税区分コード
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["KASHIKATA_ZEI_KUBUN_NM"].ToString()); // 貸方税区分
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["BUMON_UMU_NM"].ToString()); // // 部門
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["HOJO_SHIYO_KUBUN_NM"].ToString()); // 補助
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["SHUKEI_CD"].ToString()); // 集計
                    #endregion

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_ZM_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_ZM_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
