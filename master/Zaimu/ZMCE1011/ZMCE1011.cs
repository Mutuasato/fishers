﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Reflection;
using System.ComponentModel;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmce1011
{
    /// <summary>
    /// 期首残高の登録(ZMCE1011)
    /// </summary>
    public partial class ZMCE1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 期首残高テーブルインデックス
        /// </summary>
        private const int KANJO_KAMOKU_CD = 0;      // 勘定科目コード
        private const int KANJO_KAMOKU_NM = 1;      // 勘定科目名称
        private const int TAISHAKU_KUBUN_NM = 2;    // 貸借区分名
        private const int KISHU_ZANDAKA = 3;        // 期首残高
        private const int TAISHAKU_KUBUN = 4;       // 貸借区分
        private const int HOJO_KAMOKU_UMU = 5;      // 補助科目有無
        private const int HOJO_SHIYO_KUBUN = 6;          // 補助科目使用区分（財務か取引先か）
        private const int BUMON_UMU = 7;           // 部門有無
        private const int HOJO_KAMOKU_CD = 8;       // 補助科目コード
        private const int HOJO_KAMOKU_NM = 9;       // 補助科目名
        //private const int TORIHIKISAKI_NM = 10;      // 取引先名（必要か？）
        private const int BUMON_CD = 10;            // 部門コード
        private const int BUMON_NM = 11;            // 部門名
        private const int HOJO_KISHU_ZANDAKA = 12;   // 補助期首残高
        #endregion

        private ZMCE1011M com = new ZMCE1011M();
        private ZMCE1011M.KISHUZAN_HEAD gHead;

        #region プロパティ
        private DataTable _kinList;
        /// <summary>
        /// 金額一覧
        /// <summary>
        public DataTable KinList
        {
            get
            {
                return this._kinList;
            }
            set
            {
                this._kinList = value;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCE1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 手当項目グリッド初期化
            //InitGridKishuZan(true);

            // 初期フォーカス
            //this.dgvKishuZan.Focus();
            //this.dgvKishuZan.CurrentCell = this.dgvKishuZan[3, 0];
            this.txtMizuageShishoCd.Focus();
        }

        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
			string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
			//MEMO:現状アクティブなコントロールごとに処理を実装してください。
			switch (this.ActiveCtlNm)
            {
            case "txtMizuageShishoCd": // 水揚支所
                // アセンブリのロード
                asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                // フォーム作成
                t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                if (t != null)
                {
                    Object obj = Activator.CreateInstance(t);
                    if (obj != null)
                    {
                        BasePgForm frm = (BasePgForm)obj;
                        frm.Par1 = "1";
                        frm.InData = this.txtMizuageShishoCd.Text;
                        frm.ShowDialog(this);

                        if (frm.DialogResult == DialogResult.OK)
                        {
                            result = (String[])frm.OutData;
                            this.txtMizuageShishoCd.Text = result[0];
                            this.lblMizuageShishoNm.Text = result[1];
                        }
                    }
                }
                break;
            }

        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("期首残高（印刷）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            ZMCE1013 frmZMCE1013 = new ZMCE1013();
            frmZMCE1013.Par2 = this.txtMizuageShishoCd.Text;
            frmZMCE1013.ShowDialog(this);
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("データ連携（更新）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全項目を再度入力値チェック
            if (!ValidateDataGridViewAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 確認メッセージを表示
            string msg;
            // 貸借の値が不一致の場合
            if (this.txtKariGokei.Text != this.txtKashiGokei.Text)
            {
                msg = ("貸借の金額が一致しません。よろしいですか？");
                if (Msg.ConfYesNo(msg) == DialogResult.No)
                {
                    // 「いいえ」を押されたら処理終了
                    return;
                }
            }
            msg = ("保存しますか？");
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                ArrayList alParams;
                string tableNm = "TB_ZM_SHIWAKE_MEISAI";
                string where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_BANGO = 0 AND GYO_BANGO BETWEEN 0 AND 999999";

                // 仕訳明細テーブルのデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                this.Dba.Delete(tableNm, where, dpc);

                // 変数宣言
                int gyoNo = 1;
                decimal hojoKmoku = 0;

                // 決算期の開始
                DateTime dateFr = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
                dateFr = dateFr.AddDays(-1);
                // 入力内容を保存
                foreach (DataRow ro in _kinList.Rows)
                {
                    // 期首残高が0で無い場合に登録 ※正の数と負の数を許可
                    if (Util.ToDecimal(ro["KISHU_ZANDAKA"]) != 0m)
                    {
                        if (Util.ToDecimal(ro["HOJO_KAMOKU_UMU"]) == Const.HOJO_UMU_ARI)
                        {
                            if (Util.ToDecimal(ro["HOJO_SHIYO_KUBUN"]) != Const.HOJO_SHIYO_KUBUN_ZAM)
                            {
                                if (Util.ToDecimal(ro["HOJO_KISHU_ZANDAKA"]) != 0m)
                                {
                                    //if (hojoKmoku == (Util.ToDecimal(ro["HOJO_KAMOKU_CD"])))
                                    //{
                                    //    continue;
                                    //}
                                    alParams = SetShiwakeMeisaiStiParams(Util.ToInt(ro["KANJO_KAMOKU_CD"]), Util.ToInt(ro["HOJO_KAMOKU_CD"]), Util.ToInt(ro["BUMON_CD"]),
                                            Util.ToInt(ro["TAISHAKU_KUBUN"]), Util.ToDecimal(ro["HOJO_KISHU_ZANDAKA"]), Util.ToString(dateFr), gyoNo);
                                    this.Dba.Insert(tableNm, (DbParamCollection)alParams[0]);
                                    gyoNo++;
                                    hojoKmoku = Util.ToDecimal(ro["HOJO_KAMOKU_CD"]);
                                }
                            }
                            else
                            {
                                if (Util.ToDecimal(ro["HOJO_KISHU_ZANDAKA"]) != 0m)
                                {
                                    alParams = SetShiwakeMeisaiStiParams(Util.ToInt(ro["KANJO_KAMOKU_CD"]), Util.ToInt(ro["HOJO_KAMOKU_CD"]), Util.ToInt(ro["BUMON_CD"]),
                                        Util.ToInt(ro["TAISHAKU_KUBUN"]), Util.ToDecimal(ro["HOJO_KISHU_ZANDAKA"]), Util.ToString(dateFr), gyoNo);
                                    this.Dba.Insert(tableNm, (DbParamCollection)alParams[0]);
                                    gyoNo++;
                                }
                            }
                        }
                        else
                        {
                            alParams = SetShiwakeMeisaiStiParams(Util.ToInt(ro["KANJO_KAMOKU_CD"]), 0, Util.ToInt(ro["BUMON_CD"]),
                                         Util.ToInt(ro["TAISHAKU_KUBUN"]), Util.ToDecimal(ro["KISHU_ZANDAKA"]), Util.ToString(dateFr), gyoNo);
                            this.Dba.Insert(tableNm, (DbParamCollection)alParams[0]);
                            gyoNo++;
                        }
                    }
                }

                // トランザクションをコミット
                this.Dba.Commit();

                // 処理終了メッセージ
                Msg.Info("更新処理が完了しました。");
            }
            catch
            {
                // 処理終了メッセージ
                Msg.Info("更新処理に失敗しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }
        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (this.IsValidMizuageShishoCd())
            {
                try
                {
                    this.SuspendLayout();
                    this.dgvKishuZan.SuspendLayout();

                    // 手当項目グリッド初期化
                    InitGridKishuZan(true);
                }
                finally
                {
                    this.dgvKishuZan.ResumeLayout();
                    this.ResumeLayout();
                }

                // 初期フォーカス
                this.dgvKishuZan.Focus();
                this.dgvKishuZan.CurrentCell = this.dgvKishuZan[3, 0];
            }
        }

        /// <summary>
        /// グリッド共通EditingControlShowingイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //表示されているコントロールがDataGridViewTextBoxEditingControlか調べる
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                DataGridView dgv = (DataGridView)sender;

                //編集のために表示されているコントロールを取得
                DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;

                //該当する列か調べる
                switch (dgv.CurrentCell.OwningColumn.Name)
                {
                    case "KISHU_ZANDAKA":
                        // 入力文字サイズ
                        tb.MaxLength = 15;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// グリッド共通Enterイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Enter(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            dgv.Focus();
            if (dgv.CurrentRow != null)
            {
                if (!dgv.IsCurrentCellDirty)
                    dgv.CurrentCell = dgv[1, dgv.CurrentRow.Index];
            }
        }

        /// <summary>
        /// グリッドCellEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            switch (dgv.Columns[e.ColumnIndex].Name)
            {
                case "KISHU_ZANDAKA":
                    dgv.ImeMode = System.Windows.Forms.ImeMode.Alpha;
                    break;
                default:
                    dgv.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
            }
        }

        /// <summary>
        /// グリッドCellValueChanged時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataRow[] arrKishu;
            arrKishu = _kinList.Select("KANJO_KAMOKU_CD = " + Util.ToInt(dgv.CurrentRow.Cells[KANJO_KAMOKU_CD].Value)
                    + " AND HOJO_KAMOKU_CD  =" + Util.ToInt(dgv.CurrentRow.Cells[HOJO_KAMOKU_CD].Value));
            foreach (DataRow rows in arrKishu)
            {
                rows["KISHU_ZANDAKA"] = Util.ToInt(dgvKishuZan.CurrentRow.Cells[KISHU_ZANDAKA].Value);
            }
            // 借・貸合計を表示
            this.txtKariGokei.Text = Util.ToString(Util.ToDecimal(this._kinList.Compute("Sum(KISHU_ZANDAKA)", "TAISHAKU_KUBUN = 1 AND HOJO_KAMOKU_UMU = 0")) + 
                                                   Util.ToDecimal(this._kinList.Compute("Sum(HOJO_KISHU_ZANDAKA)", "TAISHAKU_KUBUN = 1 AND HOJO_KAMOKU_UMU = 1")));

            this.txtKashiGokei.Text = Util.ToString(Util.ToDecimal(this._kinList.Compute("Sum(KISHU_ZANDAKA)", "TAISHAKU_KUBUN = 2 AND HOJO_KAMOKU_UMU = 0")) + 
                                                    Util.ToDecimal(this._kinList.Compute("Sum(HOJO_KISHU_ZANDAKA)", "TAISHAKU_KUBUN = 2 AND HOJO_KAMOKU_UMU = 1")));

            this.txtKashiGokei.Text = Util.FormatNum(Util.ToDecimal(this.txtKashiGokei.Text), 0);
            this.txtKariGokei.Text = Util.FormatNum(Util.ToDecimal(this.txtKariGokei.Text), 0);
        }

        /// <summary>
        /// グリッドCellDoubleClick時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            switch (dgv.Columns[e.ColumnIndex].Name)
            {
                case "KISHU_ZANDAKA":
                    if (Util.ToInt(dgv.CurrentRow.Cells[HOJO_KAMOKU_UMU].Value) == Const.HOJO_UMU_ARI || Util.ToInt(dgv.CurrentRow.Cells[BUMON_UMU].Value) == Const.BUMON_UMU_ARI)
                    {
                        // 部門管理有り、補助科目あり
                        if (Util.ToInt(dgv.CurrentRow.Cells[HOJO_KAMOKU_UMU].Value) == Const.HOJO_UMU_ARI && Util.ToInt(dgv.CurrentRow.Cells[BUMON_UMU].Value) == Const.BUMON_UMU_ARI)
                        {
                            // 
                            EditHojoBmnKamokuZan(Util.ToInt(dgv.CurrentRow.Cells[KANJO_KAMOKU_CD].Value), Util.ToInt(dgv.CurrentRow.Cells[HOJO_SHIYO_KUBUN].Value));
                            break;
                        }
                        else
                        {
                            //既存の画面表示
                            EditHojoKamokuZan(Util.ToInt(dgv.CurrentRow.Cells[KANJO_KAMOKU_CD].Value), Util.ToInt(dgv.CurrentRow.Cells[HOJO_SHIYO_KUBUN].Value));
                            break;
                        }
                    }
                    break;
                default:
                    dgv.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
            }
        }

        /// <summary>
        /// グリッドセル値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (this.dgvKishuZan.IsCurrentCellDirty)
            {
                switch (this.dgvKishuZan.Columns[e.ColumnIndex].Name)
                {
                    case "KISHU_ZANDAKA":                            // 期首残高
                        e.Cancel = !IsValidKishuZan(e.FormattedValue);
                        break;
                    default:
                        // NONE
                        break;
                }
            }
        }

        /// <summary>
        /// データグリッドのマウスダウン時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvKishuZan_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            this.btnF1.Enabled = false;
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        // フォームが最初に呼ばれた時の処理
        /// </summary>
        private void ZMCE1011_Shown(object sender, EventArgs e)
        {
            //try
            //{
            //    this.SuspendLayout();
            //    this.dgvKishuZan.SuspendLayout();

            //    // 手当項目グリッド初期化
            //    InitGridKishuZan(true);
            //}
            //finally
            //{
            //    this.dgvKishuZan.ResumeLayout();
            //    this.ResumeLayout();
            //}

            //// 初期フォーカス
            //this.dgvKishuZan.Focus();
            //this.dgvKishuZan.CurrentCell = this.dgvKishuZan[3, 0];
        }


        /// <summary>
        /// 手当項目グリッド初期化
        /// </summary>
        /// <param name="isInitial">初期判定</param>
        private void InitGridKishuZan(bool isInitial)
        {
            // 初期処理の場合SQLでデータを取得
            DataTable dgvMain;
            // グリッドデータ定義
            dgvMain = new DataTable();
            dgvMain.Columns.Add("KANJO_KAMOKU_CD", Type.GetType("System.Int32"));
            dgvMain.Columns.Add("KANJO_KAMOKU_NM", Type.GetType("System.String"));
            dgvMain.Columns.Add("TAISHAKU_KUBUN_NM", Type.GetType("System.String"));
            dgvMain.Columns.Add("KISHU_ZANDAKA", Type.GetType("System.Int64"));
            dgvMain.Columns.Add("TAISHAKU_KUBUN", Type.GetType("System.Int32"));
            dgvMain.Columns.Add("HOJO_KAMOKU_UMU", Type.GetType("System.Int32"));
            dgvMain.Columns.Add("HOJO_SHIYO_KUBUN", Type.GetType("System.Int32"));
            dgvMain.Columns.Add("BUMON_UMU", Type.GetType("System.Int32"));

            dgvMain.Columns.Add("HOJO_KAMOKU_CD", Type.GetType("System.Int32"));
            dgvMain.Columns.Add("HOJO_KAMOKU_NM", Type.GetType("System.String"));
            dgvMain.Columns.Add("BUMON_CD", Type.GetType("System.Int32"));
            dgvMain.Columns.Add("BUMON_NM", Type.GetType("System.String"));
            dgvMain.Columns.Add("HOJO_KISHU_ZANDAKA", Type.GetType("System.Int64"));

            DataTable kinMain;
            // グリッドデータ定義
            kinMain = new DataTable();
            kinMain.Columns.Add("KANJO_KAMOKU_CD", Type.GetType("System.Int32"));
            kinMain.Columns.Add("KANJO_KAMOKU_NM", Type.GetType("System.String"));
            kinMain.Columns.Add("TAISHAKU_KUBUN_NM", Type.GetType("System.String"));
            kinMain.Columns.Add("KISHU_ZANDAKA", Type.GetType("System.Int64"));
            kinMain.Columns.Add("TAISHAKU_KUBUN", Type.GetType("System.Int32"));
            kinMain.Columns.Add("HOJO_KAMOKU_UMU", Type.GetType("System.Int32"));
            kinMain.Columns.Add("HOJO_SHIYO_KUBUN", Type.GetType("System.Int32"));
            kinMain.Columns.Add("BUMON_UMU", Type.GetType("System.Int32"));

            kinMain.Columns.Add("HOJO_KAMOKU_CD", Type.GetType("System.Int32"));
            kinMain.Columns.Add("HOJO_KAMOKU_NM", Type.GetType("System.String"));
            kinMain.Columns.Add("BUMON_CD", Type.GetType("System.Int32"));
            kinMain.Columns.Add("BUMON_NM", Type.GetType("System.String"));
            kinMain.Columns.Add("HOJO_KISHU_ZANDAKA", Type.GetType("System.Int64"));

            // 初期処理の場合SQLでデータを取得
            if (isInitial)
            {
                // データ読込
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 2, this.txtMizuageShishoCd.Text);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                DataTable dtSti = this.Dba.GetDataTableFromSqlWithParams(com.GetKomokuStiSql(), dpc);
                _kinList = dtSti;
            }
            // 変数宣言
            decimal kanjoKmkCd = 0;
            decimal hojoKmkCd = 0;

            // 行を追加
            foreach (DataRow row in _kinList.Rows)
            {

                gHead.KANJO_KAMOKU_CD = Util.ToInt(row["KANJO_KAMOKU_CD"]);
                gHead.KANJO_KAMOKU_NM = Util.ToString(row["KANJO_KAMOKU_NM"]);
                gHead.TAISHAKU_KUBUN = Util.ToInt(row["TAISHAKU_KUBUN"]);
                if (gHead.TAISHAKU_KUBUN == 1)
                {
                    gHead.TAISHAKU_KUBUN_NM = "借方";
                }
                else
                {
                    gHead.TAISHAKU_KUBUN_NM = "貸方";
                }
                gHead.KISHU_ZANDAKA = Util.ToDecimal(row["KISHU_ZANDAKA"]);

                gHead.HOJO_SHIYO_KUBUN = Util.ToInt(row["HOJO_SHIYO_KUBUN"]);
                gHead.HOJO_KAMOKU_UMU = Util.ToInt(row["HOJO_KAMOKU_UMU"]);
                gHead.BUMON_UMU = Util.ToInt(row["BUMON_UMU"]);

                // 初期処理の場合データを取得
                if (isInitial)
                {
                    DataTable hojoTable = new DataTable();
                    StringBuilder Sql = new StringBuilder();
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                    dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 10, gHead.KANJO_KAMOKU_CD);

                    // 補助科目有り、部門有り
                    if (gHead.BUMON_UMU == Const.BUMON_UMU_ARI && gHead.HOJO_KAMOKU_UMU == Const.HOJO_UMU_ARI)
                    {
                        Sql.Append(com.GetHjkBmnZan(gHead.KANJO_KAMOKU_CD, gHead.HOJO_SHIYO_KUBUN));

                        hojoTable = this.Dba.GetDataTableFromSqlWithParams(Sql.ToString(), dpc);
                    }
                    else if (gHead.BUMON_UMU == Const.BUMON_UMU_ARI || gHead.HOJO_KAMOKU_UMU == Const.HOJO_UMU_ARI)
                    {
                        if (gHead.HOJO_KAMOKU_UMU == Const.HOJO_UMU_ARI)
                        {
                            // 補助科目あり
                            Sql.Append(com.GetHjkZan(gHead.KANJO_KAMOKU_CD, gHead.HOJO_SHIYO_KUBUN));

                            hojoTable = this.Dba.GetDataTableFromSqlWithParams(Sql.ToString(), dpc);
                        }
                        else
                        {
                            // 部門有り
                            Sql.Append(com.GetBmnZan(gHead.KANJO_KAMOKU_CD));

                            hojoTable = this.Dba.GetDataTableFromSqlWithParams(Sql.ToString(), dpc);
                        }
                    }

                    if (hojoTable.Rows.Count != 0)
                    {
                        foreach (DataRow hDr in hojoTable.Rows)
                        {
                            DataRow dr = kinMain.NewRow();

                            dr["HOJO_KAMOKU_CD"] = Util.ToDecimal(hDr["HOJO_KAMOKU_CD"]);
                            dr["HOJO_KAMOKU_NM"] = Util.ToString(hDr["HOJO_KAMOKU_NM"]);
                            dr["BUMON_CD"] = Util.ToDecimal(hDr["BUMON_CD"]);
                            dr["BUMON_NM"] = Util.ToString(hDr["BUMON_NM"]);
                            dr["HOJO_KISHU_ZANDAKA"] = Util.ToDecimal(hDr["KISHU_ZANDAKA"]);

                            dr["KANJO_KAMOKU_CD"] = gHead.KANJO_KAMOKU_CD;
                            dr["KANJO_KAMOKU_NM"] = gHead.KANJO_KAMOKU_NM;
                            dr["TAISHAKU_KUBUN"] = gHead.TAISHAKU_KUBUN;
                            dr["TAISHAKU_KUBUN_NM"] = gHead.TAISHAKU_KUBUN_NM;
                            dr["KISHU_ZANDAKA"] = gHead.KISHU_ZANDAKA;
                            dr["HOJO_KAMOKU_UMU"] = gHead.HOJO_KAMOKU_UMU;
                            dr["HOJO_SHIYO_KUBUN"] = gHead.HOJO_SHIYO_KUBUN;
                            dr["BUMON_UMU"] = gHead.BUMON_UMU;

                            kinMain.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        DataRow dr = kinMain.NewRow();

                        dr["KANJO_KAMOKU_CD"] = gHead.KANJO_KAMOKU_CD;
                        dr["KANJO_KAMOKU_NM"] = gHead.KANJO_KAMOKU_NM;
                        dr["TAISHAKU_KUBUN"] = gHead.TAISHAKU_KUBUN;
                        dr["TAISHAKU_KUBUN_NM"] = gHead.TAISHAKU_KUBUN_NM;
                        dr["KISHU_ZANDAKA"] = gHead.KISHU_ZANDAKA;
                        dr["HOJO_KAMOKU_UMU"] = gHead.HOJO_KAMOKU_UMU;
                        dr["HOJO_SHIYO_KUBUN"] = gHead.HOJO_SHIYO_KUBUN;
                        dr["BUMON_UMU"] = gHead.BUMON_UMU;

                        dr["HOJO_KAMOKU_CD"] = "0";
                        dr["HOJO_KAMOKU_NM"] = "";
                        dr["BUMON_CD"] = "0";
                        dr["BUMON_NM"] = "";
                        dr["HOJO_KISHU_ZANDAKA"] = "0";

                        kinMain.Rows.Add(dr);
                    }
                }
                else
                {
                    DataRow dr = kinMain.NewRow();
                    dr["KANJO_KAMOKU_CD"] = gHead.KANJO_KAMOKU_CD;
                    dr["KANJO_KAMOKU_NM"] = gHead.KANJO_KAMOKU_NM;
                    dr["TAISHAKU_KUBUN"] = gHead.TAISHAKU_KUBUN;
                    dr["TAISHAKU_KUBUN_NM"] = gHead.TAISHAKU_KUBUN_NM;
                    dr["KISHU_ZANDAKA"] = gHead.KISHU_ZANDAKA;
                    dr["HOJO_KAMOKU_UMU"] = gHead.HOJO_KAMOKU_UMU;
                    dr["HOJO_SHIYO_KUBUN"] = gHead.HOJO_SHIYO_KUBUN;
                    dr["BUMON_UMU"] = gHead.BUMON_UMU;
                    dr["HOJO_KAMOKU_CD"] = Util.ToDecimal(row["HOJO_KAMOKU_CD"]);
                    dr["HOJO_KAMOKU_NM"] = Util.ToString(row["HOJO_KAMOKU_NM"]);
                    dr["BUMON_CD"] = Util.ToDecimal(row["BUMON_CD"]);
                    dr["BUMON_NM"] = Util.ToString(row["BUMON_NM"]);
                    dr["HOJO_KISHU_ZANDAKA"] = Util.ToDecimal(row["HOJO_KISHU_ZANDAKA"]);
                    kinMain.Rows.Add(dr);
                    hojoKmkCd = Util.ToDecimal(row["HOJO_KAMOKU_CD"]);
                }

                if (kanjoKmkCd == gHead.KANJO_KAMOKU_CD)
                {
                    continue;
                }
                DataRow dgvr = dgvMain.NewRow();
                dgvr["KANJO_KAMOKU_CD"] = gHead.KANJO_KAMOKU_CD;
                dgvr["KANJO_KAMOKU_NM"] = gHead.KANJO_KAMOKU_NM;
                dgvr["TAISHAKU_KUBUN_NM"] = gHead.TAISHAKU_KUBUN_NM;
                dgvr["KISHU_ZANDAKA"] = gHead.KISHU_ZANDAKA;
                dgvr["TAISHAKU_KUBUN"] = gHead.TAISHAKU_KUBUN;
                dgvr["HOJO_KAMOKU_UMU"] = gHead.HOJO_KAMOKU_UMU;
                dgvr["HOJO_SHIYO_KUBUN"] = gHead.HOJO_SHIYO_KUBUN;
                dgvr["BUMON_UMU"] = gHead.BUMON_UMU;

                dgvr["HOJO_KAMOKU_CD"] = "0";
                dgvr["HOJO_KAMOKU_NM"] = "";
                dgvr["BUMON_CD"] = "0";
                dgvr["BUMON_NM"] = "";
                dgvr["HOJO_KISHU_ZANDAKA"] = "0";

                dgvMain.Rows.Add(dgvr);

                kanjoKmkCd = Util.ToDecimal(row["KANJO_KAMOKU_CD"]);
                gHead.Clear();
            }

            // グリッドへデータソース設定
            _kinList = kinMain;
            this.dgvKishuZan.DataSource = dgvMain;
            this.txtKariGokei.Enabled = false;
            this.txtKashiGokei.Enabled = false;

            // 借・貸合計を表示
            this.txtKariGokei.Text = Util.ToString(Util.ToDecimal(this._kinList.Compute("Sum(KISHU_ZANDAKA)", "TAISHAKU_KUBUN = 1 AND HOJO_KAMOKU_UMU = 0 AND BUMON_UMU = 0")) + 
                                                   Util.ToDecimal(this._kinList.Compute("Sum(HOJO_KISHU_ZANDAKA)", "TAISHAKU_KUBUN = 1 AND (HOJO_KAMOKU_UMU = 1 OR BUMON_UMU = 1)"))
                                                   );

            this.txtKashiGokei.Text = Util.ToString(Util.ToDecimal(this._kinList.Compute("Sum(KISHU_ZANDAKA)", "TAISHAKU_KUBUN = 2 AND HOJO_KAMOKU_UMU = 0 AND BUMON_UMU = 0")) + 
                                                    Util.ToDecimal(this._kinList.Compute("Sum(HOJO_KISHU_ZANDAKA)", "TAISHAKU_KUBUN = 2 AND (HOJO_KAMOKU_UMU = 1 OR BUMON_UMU = 1)")));

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvKishuZan.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvKishuZan.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvKishuZan.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvKishuZan.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 12F);

            // 編集設定
            this.dgvKishuZan.SelectionMode = DataGridViewSelectionMode.CellSelect;
            this.dgvKishuZan.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.dgvKishuZan.ReadOnly = false;
            this.dgvKishuZan.EditMode = DataGridViewEditMode.EditOnEnter;

            // 各列設定
            this.dgvKishuZan.AllowUserToResizeRows = false;
            DataGridViewColumn col = this.dgvKishuZan.Columns[KANJO_KAMOKU_CD];
            col.HeaderText = "コード";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            col.Width = 70;
            col.ReadOnly = true;
            col.Resizable = DataGridViewTriState.False;

            col = this.dgvKishuZan.Columns[KANJO_KAMOKU_NM];
            col.HeaderText = "勘定科目名";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 280; 
            col.ReadOnly = true;
            col.Resizable = DataGridViewTriState.False;

            col = this.dgvKishuZan.Columns[TAISHAKU_KUBUN_NM];
            col.HeaderText = "貸借";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Width = 50;
            col.ReadOnly = true;
            col.Resizable = DataGridViewTriState.False;

            col = this.dgvKishuZan.Columns[KISHU_ZANDAKA];
            col.HeaderText = "期首残高";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            col.Width = 178;
            col.Resizable = DataGridViewTriState.False;
            col.DefaultCellStyle.Format = "#,0";

            // 各テキストボックスの設定
            this.txtKariGokei.TextAlign = HorizontalAlignment.Right;
            this.txtKashiGokei.TextAlign = HorizontalAlignment.Right;
            this.txtKashiGokei.Text = Util.FormatNum(Util.ToDecimal(this.txtKashiGokei.Text), 0);
            this.txtKariGokei.Text = Util.FormatNum(Util.ToDecimal(this.txtKariGokei.Text), 0);
            // 列を非表示にする
            this.dgvKishuZan.Columns[TAISHAKU_KUBUN].Visible = false;
            this.dgvKishuZan.Columns[HOJO_KAMOKU_UMU].Visible = false;
            this.dgvKishuZan.Columns[HOJO_SHIYO_KUBUN].Visible = false;
            this.dgvKishuZan.Columns[BUMON_UMU].Visible = false;
            this.dgvKishuZan.Columns[HOJO_KAMOKU_CD].Visible = false;
            this.dgvKishuZan.Columns[HOJO_KAMOKU_NM].Visible = false;
            //this.dgvKishuZan.Columns[TORIHIKISAKI_NM].Visible = false;
            this.dgvKishuZan.Columns[BUMON_CD].Visible = false;
            this.dgvKishuZan.Columns[BUMON_NM].Visible = false;
            this.dgvKishuZan.Columns[HOJO_KISHU_ZANDAKA].Visible = false;
            // 補助科目有無が１の時・工事有無が１の時セルの色を変更
            for (int i = 0; i < dgvKishuZan.Rows.Count; i++)
            {
                if (Util.ToInt(dgvKishuZan[HOJO_KAMOKU_UMU, i].Value) == 1)
                {
                    Color hojoCellCol = ColorTranslator.FromHtml(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMCE1011", "Setting", "HojoCellCol"));
                    dgvKishuZan[KISHU_ZANDAKA, i].Style.BackColor = hojoCellCol;
                    dgvKishuZan[KISHU_ZANDAKA, i].ReadOnly = true;
                }
                if (Util.ToInt(dgvKishuZan[BUMON_UMU, i].Value) == 1)
                {
                    Color hojoCellCol = ColorTranslator.FromHtml(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMCE1011", "Setting", "KojiCellCol"));
                    dgvKishuZan[KISHU_ZANDAKA, i].Style.BackColor = hojoCellCol;
                    dgvKishuZan[KISHU_ZANDAKA, i].ReadOnly = true;
                }
            }
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_MEISAIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="kanjoCd">勘定科目コード</param>
        /// <param name="hojoCd">補助科目コード</param>
        /// <param name="bumonCd">部門コード</param>
        /// <param name="taishakKbn">貸借区分</param>
        /// <param name="kishuZan">期首残高</param>
        /// <param name="denpyoDate">伝票番号</param>
        /// <param name="gyoNo">行番号</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetShiwakeMeisaiStiParams(int kanjoCd, int hojoCd, int bumonCd, int taishakKbn, decimal kishuZan, string denpyoDate, int gyoNo)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection insParam = new DbParamCollection();
            // 登録パラメータ
            insParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);             // 会社コード
            insParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);    // 支所コード
            insParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, 0);                            // 伝票番号
            insParam.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, gyoNo);                          // 行番号
            insParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, taishakKbn);                 // 貸借区分
            insParam.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, 0);                            // 明細区分
            insParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);       // 会計年度
            insParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, 0);                            // 伝票区分
            insParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, denpyoDate);                      // 伝票日付
            insParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoCd);                   // 勘定科目コード
            insParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal,10, hojoCd);                     // 補助科目コード
            insParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, bumonCd);                          // 部門コード
            insParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, 0);                               // 摘要コード
            insParam.SetParam("@TEKIYO", SqlDbType.VarChar, 40, "期首残高");                        // 摘要名
            insParam.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, kishuZan);                 // 税込金額
            insParam.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, kishuZan);                 // 税抜金額
            insParam.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, 0);                       // 消費税金額
            insParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, 0);                               // 税区分
            insParam.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, 0);                             // 課税区分
            insParam.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, 0);                          // 取引区分
            insParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 0);                               // 税率
            insParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, 0);                             // 事業区分
            insParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, 0);                  // 消費税入力方法
            insParam.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, 0);                          // 消費税変更
            insParam.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, 0);                            // 決算区分
            insParam.SetParam("@SHIWAKE_SAKUSEI_KUBUN", SqlDbType.Decimal, 1, 0);                   // 仕訳作成区分
            insParam.SetParam("@BIKO", SqlDbType.VarChar, 40, "");                                  // 備考
            insParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");                      // 登録日時
            insParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");                      // 更新日時
            alParams.Add(insParam);

            return alParams;
        }

        /// <summary>
        /// 補助科目別の画面起動
        /// </summary>
        /// <param name="code">勘定科目コード</param>
        /// <param name="hojoKbn">補助使用区分</param>
        private void EditHojoKamokuZan(int code, int hojoKbn)
        {

            if (hojoKbn == Const.HOJO_SHIYO_KUBUN_ZAM)
            {
                // 財務補助科目マスタで登録画面を起動
                ZMCE1012 frmZMCE1012 = new ZMCE1012(this, "1", code);
                frmZMCE1012.Par2 = this.txtMizuageShishoCd.Text;

                DialogResult result = frmZMCE1012.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    // データを再表示
                    InitGridKishuZan(false);
                    // Gridに再度フォーカスをセット
                    this.ActiveControl = this.dgvKishuZan;
                    this.dgvKishuZan.Focus();
                }
            }
            else if( hojoKbn == Const.HOJO_SHIYO_KUBUN_TRI)
            {
                // 取引先で登録画面を起動
                ZMCE1012 frmZMCE1012 = new ZMCE1012(this, "2", code);
                frmZMCE1012.Par2 = this.txtMizuageShishoCd.Text;

                DialogResult result = frmZMCE1012.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    // データを再表示
                    InitGridKishuZan(false);
                    // Gridに再度フォーカスをセット
                    this.ActiveControl = this.dgvKishuZan;
                    this.dgvKishuZan.Focus();
                }
            }
            else
            {
                // 部門で登録画面を起動
                ZMCE1012 frmZMCE1012 = new ZMCE1012(this, "3", code);
                frmZMCE1012.Par2 = this.txtMizuageShishoCd.Text;

                DialogResult result = frmZMCE1012.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    // データを再表示
                    InitGridKishuZan(false);
                    // Gridに再度フォーカスをセット
                    this.ActiveControl = this.dgvKishuZan;
                    this.dgvKishuZan.Focus();
                }
            }
        }

        /// <summary>
        /// 補助科目別の画面起動
        /// </summary>
        /// <param name="code">勘定科目コード</param>
        /// <param name="hojoKbn">補助使用区分</param>
        private void EditHojoBmnKamokuZan(int code, int hojoKbn)
        {

            if (hojoKbn == Const.HOJO_SHIYO_KUBUN_ZAM)
            {
                // 財務補助科目マスタで登録画面を起動
                ZMCE1014 frmZMCE1012 = new ZMCE1014(this, "1", code);
                frmZMCE1012.Par2 = this.txtMizuageShishoCd.Text;

                DialogResult result = frmZMCE1012.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    // データを再表示
                    InitGridKishuZan(false);
                    // Gridに再度フォーカスをセット
                    this.ActiveControl = this.dgvKishuZan;
                    this.dgvKishuZan.Focus();
                }
            }
            else if (hojoKbn == Const.HOJO_SHIYO_KUBUN_TRI)
            {
                // 取引先で登録画面を起動
                ZMCE1014 frmZMCE1012 = new ZMCE1014(this, "2", code);
                frmZMCE1012.Par2 = this.txtMizuageShishoCd.Text;

                DialogResult result = frmZMCE1012.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    // データを再表示
                    InitGridKishuZan(false);
                    // Gridに再度フォーカスをセット
                    this.ActiveControl = this.dgvKishuZan;
                    this.dgvKishuZan.Focus();
                }
            }
        }

        /// <summary>
        /// グリッド項目名フィールドの入力チェック
        /// </summary>
        /// <param name="value"></param>
        /// <returns>true:OK,false:NG</returns>
        private bool IsValidKishuZan(object value)
        {
            // 入力サイズが15バイト以上はNG
            if (!ValChk.IsWithinLength(Util.ToString(value), 15))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値以外はNG
            // 空はOKとする
            else if (!ValChk.IsEmpty(value) && !Regex.IsMatch(value.ToString(), "^-?[0-9\\,]+$"))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// グリッド全編集行を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateDataGridViewAll()
        {
            foreach (DataGridViewRow row in dgvKishuZan.Rows)
            {
                // 期首残高
                if (!IsValidKishuZan(row.Cells["KISHU_ZANDAKA"].Value))
                {
                    dgvKishuZan.Focus();
                    row.Selected = true;
                    row.Cells["KISHU_ZANDAKA"].Selected = true;
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}
