﻿namespace jp.co.fsi.zm.zmce1011
{
    partial class ZMCE1014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblKanjoKmk = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblHojoGokei = new System.Windows.Forms.Label();
            this.lblKanjoGokei = new System.Windows.Forms.Label();
            this.dgvKishuZan = new jp.co.fsi.zm.zmce1011.DataGridViewEx();
            this.dgvHjkBmnKei = new System.Windows.Forms.DataGridView();
            this.dgvBmnKei = new System.Windows.Forms.DataGridView();
            this.dgvKmkKei = new System.Windows.Forms.DataGridView();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.txtKanjoKmk = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiTableLayoutPanel3 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiTableLayoutPanel4 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiTableLayoutPanel5 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHjkBmnKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBmnKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKmkKei)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.fsiTableLayoutPanel3.SuspendLayout();
            this.fsiTableLayoutPanel4.SuspendLayout();
            this.fsiTableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.lblHojoGokei);
            this.pnlDebug.Controls.Add(this.label4);
            this.pnlDebug.Controls.Add(this.label3);
            this.pnlDebug.Location = new System.Drawing.Point(7, 692);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1196, 133);
            this.pnlDebug.Controls.SetChildIndex(this.label3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.label4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.lblHojoGokei, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1185, 31);
            this.lblTitle.Text = "補助科目残高";
            // 
            // lblKanjoKmk
            // 
            this.lblKanjoKmk.BackColor = System.Drawing.SystemColors.Window;
            this.lblKanjoKmk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoKmk.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoKmk.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoKmk.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKmk.Name = "lblKanjoKmk";
            this.lblKanjoKmk.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.lblKanjoKmk.Size = new System.Drawing.Size(267, 23);
            this.lblKanjoKmk.TabIndex = 3;
            this.lblKanjoKmk.Tag = "DISPNAME";
            this.lblKanjoKmk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 23);
            this.label1.TabIndex = 1;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "勘定科目：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label3.Location = new System.Drawing.Point(239, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(335, 31);
            this.label3.TabIndex = 7;
            this.label3.Text = "合　計";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(159, 1);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 31);
            this.label4.TabIndex = 6;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label4.Visible = false;
            // 
            // lblHojoGokei
            // 
            this.lblHojoGokei.BackColor = System.Drawing.Color.PaleTurquoise;
            this.lblHojoGokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblHojoGokei.Location = new System.Drawing.Point(582, 0);
            this.lblHojoGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoGokei.Name = "lblHojoGokei";
            this.lblHojoGokei.Size = new System.Drawing.Size(241, 31);
            this.lblHojoGokei.TabIndex = 8;
            this.lblHojoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblHojoGokei.Visible = false;
            // 
            // lblKanjoGokei
            // 
            this.lblKanjoGokei.BackColor = System.Drawing.SystemColors.Window;
            this.lblKanjoGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKanjoGokei.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoGokei.Name = "lblKanjoGokei";
            this.lblKanjoGokei.Size = new System.Drawing.Size(513, 23);
            this.lblKanjoGokei.TabIndex = 4;
            this.lblKanjoGokei.Tag = "CHANGE";
            this.lblKanjoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvKishuZan
            // 
            this.dgvKishuZan.AllowUserToAddRows = false;
            this.dgvKishuZan.AllowUserToDeleteRows = false;
            this.dgvKishuZan.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKishuZan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvKishuZan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvKishuZan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvKishuZan.EnableHeadersVisualStyles = false;
            this.dgvKishuZan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvKishuZan.Location = new System.Drawing.Point(5, 5);
            this.dgvKishuZan.Margin = new System.Windows.Forms.Padding(4);
            this.dgvKishuZan.MultiSelect = false;
            this.dgvKishuZan.Name = "dgvKishuZan";
            this.dgvKishuZan.ReadOnly = true;
            this.dgvKishuZan.RowHeadersVisible = false;
            this.dgvKishuZan.RowTemplate.Height = 21;
            this.dgvKishuZan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvKishuZan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvKishuZan.Size = new System.Drawing.Size(923, 541);
            this.dgvKishuZan.TabIndex = 0;
            this.dgvKishuZan.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEnter);
            this.dgvKishuZan.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgv_CellValidating);
            this.dgvKishuZan.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvKishuZan_Scroll);
            this.dgvKishuZan.Enter += new System.EventHandler(this.dgv_Enter);
            // 
            // dgvHjkBmnKei
            // 
            this.dgvHjkBmnKei.AllowUserToAddRows = false;
            this.dgvHjkBmnKei.AllowUserToDeleteRows = false;
            this.dgvHjkBmnKei.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHjkBmnKei.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvHjkBmnKei.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvHjkBmnKei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvHjkBmnKei.EnableHeadersVisualStyles = false;
            this.dgvHjkBmnKei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvHjkBmnKei.Location = new System.Drawing.Point(5, 5);
            this.dgvHjkBmnKei.Margin = new System.Windows.Forms.Padding(4);
            this.dgvHjkBmnKei.MultiSelect = false;
            this.dgvHjkBmnKei.Name = "dgvHjkBmnKei";
            this.dgvHjkBmnKei.ReadOnly = true;
            this.dgvHjkBmnKei.RowHeadersVisible = false;
            this.dgvHjkBmnKei.RowTemplate.Height = 21;
            this.dgvHjkBmnKei.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvHjkBmnKei.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHjkBmnKei.Size = new System.Drawing.Size(215, 541);
            this.dgvHjkBmnKei.TabIndex = 904;
            this.dgvHjkBmnKei.TabStop = false;
            // 
            // dgvBmnKei
            // 
            this.dgvBmnKei.AllowUserToAddRows = false;
            this.dgvBmnKei.AllowUserToDeleteRows = false;
            this.dgvBmnKei.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBmnKei.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBmnKei.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBmnKei.ColumnHeadersVisible = false;
            this.dgvBmnKei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBmnKei.EnableHeadersVisualStyles = false;
            this.dgvBmnKei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvBmnKei.Location = new System.Drawing.Point(5, 5);
            this.dgvBmnKei.Margin = new System.Windows.Forms.Padding(4);
            this.dgvBmnKei.MultiSelect = false;
            this.dgvBmnKei.Name = "dgvBmnKei";
            this.dgvBmnKei.ReadOnly = true;
            this.dgvBmnKei.RowHeadersVisible = false;
            this.dgvBmnKei.RowTemplate.Height = 21;
            this.dgvBmnKei.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvBmnKei.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvBmnKei.Size = new System.Drawing.Size(923, 36);
            this.dgvBmnKei.TabIndex = 907;
            this.dgvBmnKei.TabStop = false;
            this.dgvBmnKei.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvBmnKei_CellPainting);
            // 
            // dgvKmkKei
            // 
            this.dgvKmkKei.AllowUserToAddRows = false;
            this.dgvKmkKei.AllowUserToDeleteRows = false;
            this.dgvKmkKei.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKmkKei.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvKmkKei.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKmkKei.ColumnHeadersVisible = false;
            this.dgvKmkKei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvKmkKei.EnableHeadersVisualStyles = false;
            this.dgvKmkKei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvKmkKei.Location = new System.Drawing.Point(5, 5);
            this.dgvKmkKei.Margin = new System.Windows.Forms.Padding(4);
            this.dgvKmkKei.MultiSelect = false;
            this.dgvKmkKei.Name = "dgvKmkKei";
            this.dgvKmkKei.ReadOnly = true;
            this.dgvKmkKei.RowHeadersVisible = false;
            this.dgvKmkKei.RowTemplate.Height = 21;
            this.dgvKmkKei.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvKmkKei.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvKmkKei.Size = new System.Drawing.Size(215, 35);
            this.dgvKmkKei.TabIndex = 908;
            this.dgvKmkKei.TabStop = false;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(11, 35);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 1;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1164, 31);
            this.fsiTableLayoutPanel1.TabIndex = 904;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel5);
            this.fsiPanel1.Controls.Add(this.fsiPanel4);
            this.fsiPanel1.Controls.Add(this.fsiPanel3);
            this.fsiPanel1.Controls.Add(this.fsiPanel2);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(1156, 23);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblKanjoGokei);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(643, 0);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(513, 23);
            this.fsiPanel5.TabIndex = 2;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblKanjoKmk);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel4.Location = new System.Drawing.Point(376, 0);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(267, 23);
            this.fsiPanel4.TabIndex = 1;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtKanjoKmk);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel3.Location = new System.Drawing.Point(109, 0);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(267, 23);
            this.fsiPanel3.TabIndex = 0;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // txtKanjoKmk
            // 
            this.txtKanjoKmk.AutoSizeFromLength = false;
            this.txtKanjoKmk.DisplayLength = null;
            this.txtKanjoKmk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKanjoKmk.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKanjoKmk.Location = new System.Drawing.Point(0, 0);
            this.txtKanjoKmk.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKmk.MaxLength = 6;
            this.txtKanjoKmk.MinimumSize = new System.Drawing.Size(4, 30);
            this.txtKanjoKmk.Name = "txtKanjoKmk";
            this.txtKanjoKmk.Size = new System.Drawing.Size(267, 23);
            this.txtKanjoKmk.TabIndex = 2;
            this.txtKanjoKmk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.label1);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel2.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(109, 23);
            this.fsiPanel2.TabIndex = 0;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Controls.Add(this.dgvKishuZan, 0, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(11, 69);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 1;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(933, 551);
            this.fsiTableLayoutPanel2.TabIndex = 905;
            // 
            // fsiTableLayoutPanel3
            // 
            this.fsiTableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel3.ColumnCount = 1;
            this.fsiTableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel3.Controls.Add(this.dgvBmnKei, 0, 0);
            this.fsiTableLayoutPanel3.Location = new System.Drawing.Point(11, 632);
            this.fsiTableLayoutPanel3.Name = "fsiTableLayoutPanel3";
            this.fsiTableLayoutPanel3.RowCount = 1;
            this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel3.Size = new System.Drawing.Size(933, 46);
            this.fsiTableLayoutPanel3.TabIndex = 905;
            // 
            // fsiTableLayoutPanel4
            // 
            this.fsiTableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel4.ColumnCount = 1;
            this.fsiTableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel4.Controls.Add(this.dgvHjkBmnKei, 0, 0);
            this.fsiTableLayoutPanel4.Location = new System.Drawing.Point(950, 69);
            this.fsiTableLayoutPanel4.Name = "fsiTableLayoutPanel4";
            this.fsiTableLayoutPanel4.RowCount = 1;
            this.fsiTableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel4.Size = new System.Drawing.Size(225, 551);
            this.fsiTableLayoutPanel4.TabIndex = 905;
            // 
            // fsiTableLayoutPanel5
            // 
            this.fsiTableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel5.ColumnCount = 1;
            this.fsiTableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel5.Controls.Add(this.dgvKmkKei, 0, 0);
            this.fsiTableLayoutPanel5.Location = new System.Drawing.Point(950, 632);
            this.fsiTableLayoutPanel5.Name = "fsiTableLayoutPanel5";
            this.fsiTableLayoutPanel5.RowCount = 1;
            this.fsiTableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel5.Size = new System.Drawing.Size(225, 45);
            this.fsiTableLayoutPanel5.TabIndex = 905;
            // 
            // ZMCE1014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 830);
            this.Controls.Add(this.fsiTableLayoutPanel5);
            this.Controls.Add(this.fsiTableLayoutPanel4);
            this.Controls.Add(this.fsiTableLayoutPanel3);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCE1014";
            this.Text = "補助科目残高";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel3, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel4, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel5, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHjkBmnKei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBmnKei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKmkKei)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.fsiTableLayoutPanel3.ResumeLayout(false);
            this.fsiTableLayoutPanel4.ResumeLayout(false);
            this.fsiTableLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblKanjoKmk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblHojoGokei;
        private System.Windows.Forms.Label lblKanjoGokei;
        //private System.Windows.Forms.DataGridView dgvKishuZan;
        //private jp.co.fsi.common.controls.SjDataGridViewEx dgvKishuZan;
        private DataGridViewEx dgvKishuZan;
        private System.Windows.Forms.DataGridView dgvHjkBmnKei;
        private System.Windows.Forms.DataGridView dgvBmnKei;
        private System.Windows.Forms.DataGridView dgvKmkKei;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel4;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel5;
        private common.controls.FsiTextBox txtKanjoKmk;
    }
}