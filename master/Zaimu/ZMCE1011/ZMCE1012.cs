﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmce1011
{
    /// <summary>
    /// 補助科目残高(ZAMM4012)
    /// </summary>
    public partial class ZMCE1012 : BasePgForm
    {
        #region 定数

        private const string HOJO_KAMOKU_CD = "HOJO_KAMOKU_CD";
        private const string HOJO_KAMOKU_NM = "HOJO_KAMOKU_NM";
        private const string BUMON_CD = "BUMON_CD";
        private const string BUMON_NM = "BUMON_NM";
        #endregion


        #region プロパティ
        /// <summary>
        /// 金額一覧
        /// </summary>
        private DataTable _dispList;
        #endregion

        #region private変数
        /// <summary>
        /// ZMCE1011(期首残高登録画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMCE1011 _pForm;
        int _kanjo;
        string _hojoKbn;


        string SHISHO_CD;
        string KAMOKU_CD;
        string KAMOKU_NM;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCE1012(ZMCE1011 frm, string par1, int kanjoCd)
        {
            this._pForm = frm;
            this._kanjo = kanjoCd;
            this._hojoKbn = par1;

            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // EscapeとF6のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.lblTitle.Visible = false;

            SHISHO_CD = this.Par2;
            if (this._hojoKbn == "3")
            {
                KAMOKU_CD = BUMON_CD;
                KAMOKU_NM = BUMON_NM;
            }
            else
            {
                KAMOKU_CD = HOJO_KAMOKU_CD;
                KAMOKU_NM = HOJO_KAMOKU_NM;
            }

            // 手当項目グリッド初期化
            InitGridKishuZan();

            // 初期フォーカス
            this.dgvKishuZan.Focus();

            //if (this.dgvKishuZan.Rows.Count == 0)
            //{
            //    //Msg.Error("補助科目が登録されていません。");
            //    Msg.Info("補助科目が登録されていません。");
            //    // DialogResultに「OK」をセットし結果を返却
            //    this.DialogResult = DialogResult.OK;
            //    this.Close();
            //}
            //else
            //{
            //    //this.dgvKishuZan.CurrentCell = this.dgvKishuZan[1, 0];
            //    ActiveControl = this.dgvKishuZan;
            //    this.dgvKishuZan.CurrentCell = this.dgvKishuZan[2, 0];
            //    this.dgvKishuZan.BeginEdit(true);
            //}
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("補助科目残高（保存）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 確認メッセージを表示
            string msg = ("保存しますか？");
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateDataGridViewAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            DataRow[] arrAppend;
            // 入力内容を保存
            if (_hojoKbn == "3")
            {
               foreach (DataRow row in _dispList.Rows)
               {
                   arrAppend = this._pForm.KinList.Select("KANJO_KAMOKU_CD = " + this._kanjo
                   + " AND BUMON_CD  =" + Util.ToDecimal(row["BUMON_CD"]));
                   foreach (DataRow rows in arrAppend)
                    {
                       rows["HOJO_KISHU_ZANDAKA"] = Util.ToDecimal(row["HOJO_KISHU_ZANDAKA"]);
                       rows["KISHU_ZANDAKA"] = Util.ToDecimal(this.lblHojoGokei.Text);
                    }
               }
            }
            else
            { 
                foreach (DataRow row in _dispList.Rows)
                {
                    arrAppend = this._pForm.KinList.Select("KANJO_KAMOKU_CD = " + this._kanjo
                    + " AND HOJO_KAMOKU_CD  =" + Util.ToDecimal(row["HOJO_KAMOKU_CD"]));
                    foreach (DataRow rows in arrAppend)
                    {
                        if (Util.ToDecimal(rows["HOJO_KISHU_ZANDAKA"]) != Util.ToDecimal((row["HOJO_KISHU_ZANDAKA"]))) //変更時のみ
                        {
                            rows["HOJO_KISHU_ZANDAKA"] = Util.ToDecimal(row["HOJO_KISHU_ZANDAKA"]);
                        }
                        if (Util.ToDecimal(rows["HOJO_KAMOKU_CD"]) == Util.ToDecimal("1"))//テーブルの１行目は必ず更新
                        //if (Util.ToDecimal(rows["KISHU_ZANDAKA"]) != Util.ToDecimal((this.lblHojoGokei.Text)))
                        {
                            rows["KISHU_ZANDAKA"] = Util.ToDecimal(this.lblHojoGokei.Text);
                        }
                    }
                }
            }
            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッド共通EditingControlShowingイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //表示されているコントロールがDataGridViewTextBoxEditingControlか調べる
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                DataGridView dgv = (DataGridView)sender;

                //編集のために表示されているコントロールを取得
                DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;

                //該当する列か調べる
                switch (dgv.CurrentCell.OwningColumn.Name)
                {
                    case "HOJO_KISHU_ZANDAKA":
                        tb.MaxLength = 15;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// グリッド共通Enterイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Enter(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            dgv.Focus();
            if (dgv.CurrentRow != null)
            {
                if (!dgv.IsCurrentCellDirty)
                    dgv.CurrentCell = dgv[1, dgv.CurrentRow.Index];
            }
        }

        /// <summary>
        /// グリッドCellEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            switch (dgv.Columns[e.ColumnIndex].Name)
            {
                case "HOJO_KISHU_ZANDAKA":
                    //dgv.ImeMode = System.Windows.Forms.ImeMode.Alpha;
                    dgv.ImeMode = System.Windows.Forms.ImeMode.Off;
                    // 合計値を表示
                    this.lblHojoGokei.Text = Util.FormatNum(this._dispList.Compute("Sum(HOJO_KISHU_ZANDAKA)", ""),0);
                    break;
                default:
                    dgv.ImeMode = System.Windows.Forms.ImeMode.Off;
                    break;
            }
        }

        /// <summary>
        /// グリッドセル値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (this.dgvKishuZan.IsCurrentCellDirty)
            {
                switch (this.dgvKishuZan.Columns[e.ColumnIndex].Name)
                {
                    case "HOJO_KISHU_ZANDAKA":                                   // 支給項目名
                        e.Cancel = !IsValidHojoKishuZan(e.FormattedValue);
                        break;
                    default:
                        // NONE
                        break;
                }
            }
        }

        private void frm_Shown(object sender, EventArgs e)
        {
            if (this.dgvKishuZan.Rows.Count == 0)
            {
                //Msg.Error("補助科目が登録されていません。");
                Msg.Info("補助科目が登録されていません。");
                // DialogResultに「OK」をセットし結果を返却
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                //this.dgvKishuZan.CurrentCell = this.dgvKishuZan[1, 0];
                ActiveControl = this.dgvKishuZan;
                this.dgvKishuZan.CurrentCell = this.dgvKishuZan[2, 0];
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 手当項目グリッド初期化
        /// </summary>
        private void InitGridKishuZan()
        {
            // グリッドデータ定義
            this._dispList = new DataTable();
            _dispList.Columns.Add(KAMOKU_CD, Type.GetType("System.Int32"));
            _dispList.Columns.Add(KAMOKU_NM, Type.GetType("System.String"));
            _dispList.Columns.Add("HOJO_KISHU_ZANDAKA", Type.GetType("System.Int64"));
            _dispList.Columns.Add("KANJO_KAMOKU_CD", Type.GetType("System.Int32"));

            // 変数宣言
            decimal hojoKmkCd = 0;

            //データ絞込み
            DataRow[] aarDisp =
             this._pForm.KinList.Select("KANJO_KAMOKU_CD = " + this._kanjo);

            //行を追加
            foreach (DataRow row in aarDisp)
            {
                if (hojoKmkCd == (Util.ToDecimal(row[KAMOKU_CD])))
                {
                    continue;
                }
                DataRow dr = this._dispList.NewRow();
                dr[KAMOKU_CD] = Util.ToDecimal(row[KAMOKU_CD]);
                if (this._hojoKbn != "3")
                {
                    dr[KAMOKU_NM] = Util.ToString(row["HOJO_KAMOKU_NM"]);
                }
                else
                {
                    dr[KAMOKU_NM] = Util.ToString(row[KAMOKU_NM]);
                }

                dr["HOJO_KISHU_ZANDAKA"] = Util.ToDecimal(row["HOJO_KISHU_ZANDAKA"]);
                dr["KANJO_KAMOKU_CD"] = Util.ToDecimal(row["KANJO_KAMOKU_CD"]);
                _dispList.Rows.Add(dr);
                hojoKmkCd = Util.ToDecimal(row[KAMOKU_CD]);
            }

            // 補助科目が未登録時
            if (_dispList.Rows.Count == 0)
                return;

            // グリッドへデータソース設定
            this.dgvKishuZan.DataSource = _dispList;

            // 勘定科目コード・勘定科目名を表示
            this.txtKanjoKmk.Text = Util.ToString(this._kanjo);
            this.lblKanjoKmk.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", SHISHO_CD, this.txtKanjoKmk.Text);
            this.txtKanjoKmk.Enabled = false;

            // 合計値を表示
            this.lblHojoGokei.Text = Util.FormatNum(this._dispList.Compute("Sum(HOJO_KISHU_ZANDAKA)", ""),0);
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvKishuZan.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvKishuZan.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvKishuZan.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvKishuZan.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 12F);
            this.dgvKishuZan.AlternatingRowsDefaultCellStyle.BackColor = Color.Aquamarine;

            // 編集設定
            this.dgvKishuZan.SelectionMode = DataGridViewSelectionMode.CellSelect;
            this.dgvKishuZan.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.dgvKishuZan.ReadOnly = false;
            this.dgvKishuZan.EditMode = DataGridViewEditMode.EditOnEnter;

            // 各列設定
            this.dgvKishuZan.Columns[0].HeaderText = "コード";
            this.dgvKishuZan.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvKishuZan.Columns[0].Width = 80;
            this.dgvKishuZan.Columns[0].Resizable = DataGridViewTriState.False;
            this.dgvKishuZan.Columns[0].ReadOnly = true;
            this.dgvKishuZan.AllowUserToResizeRows = false;
            this.dgvKishuZan.Columns[1].HeaderText = "補助科目名";
            this.dgvKishuZan.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvKishuZan.Columns[1].Width = 300;
            this.dgvKishuZan.Columns[1].ReadOnly = true;
            this.dgvKishuZan.Columns[1].Resizable = DataGridViewTriState.False;
            this.dgvKishuZan.Columns[2].HeaderText = "期首残高";
            this.dgvKishuZan.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvKishuZan.Columns[2].Width = 300;
            this.dgvKishuZan.Columns[2].Resizable = DataGridViewTriState.False;
            this.dgvKishuZan.Columns[2].DefaultCellStyle.Format = "#,0";
            this.dgvKishuZan.Columns[3].Visible = false;

            // ラベルの設定
            this.lblHojoGokei.Text = Util.FormatNum(Util.ToDecimal(this.lblHojoGokei.Text), 0);
            this.lblKanjoGokei.Text = this.lblHojoGokei.Text;
        }

        /// <summary>
        /// グリッド項目名フィールドの入力チェック
        /// </summary>
        /// <param name="value"></param>
        /// <returns>true:OK,false:NG</returns>
        private bool IsValidHojoKishuZan(object value)
        {
            // 入力サイズが15バイト以上はNG
            if (!ValChk.IsWithinLength(Util.ToString(value), 15))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値以外はNG
            // 空はOKとする
            else if (!ValChk.IsEmpty(value) && !Regex.IsMatch(value.ToString(), "^-?[0-9\\,]+$"))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            this.dgvKishuZan.EndEdit();
            this.lblHojoGokei.Text = Util.FormatNum(Util.ToDecimal(this._dispList.Compute("Sum(HOJO_KISHU_ZANDAKA)", "")),0);
            return true;
        }

        /// <summary>
        /// グリッド全編集行を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateDataGridViewAll()
        {
            foreach (DataGridViewRow row in dgvKishuZan.Rows)
            {
                // 期首残高
                if (!IsValidHojoKishuZan(row.Cells["HOJO_KISHU_ZANDAKA"].Value))
                {
                    dgvKishuZan.Focus();
                    row.Selected = true;
                    row.Cells["HOJO_KISHU_ZANDAKA"].Selected = true;
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}
