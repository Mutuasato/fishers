﻿namespace jp.co.fsi.zm.zmce1011
{
    partial class ZMCE1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblKanjoKmk = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvKishuZan = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.lblHojoGokei = new System.Windows.Forms.Label();
            this.lblKanjoGokei = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.txtKanjoKmk = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).BeginInit();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiTableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 483);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(827, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(817, 31);
            this.lblTitle.Text = "補助科目残高";
            // 
            // lblKanjoKmk
            // 
            this.lblKanjoKmk.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKmk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoKmk.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKmk.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoKmk.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKmk.Name = "lblKanjoKmk";
            this.lblKanjoKmk.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.lblKanjoKmk.Size = new System.Drawing.Size(318, 35);
            this.lblKanjoKmk.TabIndex = 3;
            this.lblKanjoKmk.Tag = "DISPNAME";
            this.lblKanjoKmk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 35);
            this.label1.TabIndex = 1;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "勘定科目：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(198, 36);
            this.label3.TabIndex = 7;
            this.label3.Tag = "DISPNAME";
            this.label3.Text = "合　計";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvKishuZan
            // 
            this.dgvKishuZan.AllowUserToAddRows = false;
            this.dgvKishuZan.AllowUserToDeleteRows = false;
            this.dgvKishuZan.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKishuZan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvKishuZan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKishuZan.EnableHeadersVisualStyles = false;
            this.dgvKishuZan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvKishuZan.Location = new System.Drawing.Point(7, 82);
            this.dgvKishuZan.Margin = new System.Windows.Forms.Padding(4);
            this.dgvKishuZan.MultiSelect = false;
            this.dgvKishuZan.Name = "dgvKishuZan";
            this.dgvKishuZan.ReadOnly = true;
            this.dgvKishuZan.RowHeadersVisible = false;
            this.dgvKishuZan.RowTemplate.Height = 21;
            this.dgvKishuZan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvKishuZan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKishuZan.Size = new System.Drawing.Size(800, 334);
            this.dgvKishuZan.TabIndex = 5;
            this.dgvKishuZan.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEnter);
            this.dgvKishuZan.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgv_CellValidating);
            this.dgvKishuZan.Enter += new System.EventHandler(this.dgv_Enter);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(229, 36);
            this.label4.TabIndex = 6;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblHojoGokei
            // 
            this.lblHojoGokei.BackColor = System.Drawing.Color.PaleTurquoise;
            this.lblHojoGokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHojoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoGokei.Location = new System.Drawing.Point(0, 0);
            this.lblHojoGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoGokei.Name = "lblHojoGokei";
            this.lblHojoGokei.Size = new System.Drawing.Size(364, 36);
            this.lblHojoGokei.TabIndex = 8;
            this.lblHojoGokei.Tag = "CHANGE";
            this.lblHojoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKanjoGokei
            // 
            this.lblKanjoGokei.BackColor = System.Drawing.Color.PaleTurquoise;
            this.lblKanjoGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanjoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoGokei.Location = new System.Drawing.Point(0, 0);
            this.lblKanjoGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoGokei.Name = "lblKanjoGokei";
            this.lblKanjoGokei.Size = new System.Drawing.Size(302, 35);
            this.lblKanjoGokei.TabIndex = 4;
            this.lblKanjoGokei.Tag = "CHANGE";
            this.lblKanjoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 1;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(803, 43);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel5);
            this.fsiPanel1.Controls.Add(this.fsiPanel4);
            this.fsiPanel1.Controls.Add(this.fsiPanel3);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(795, 35);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblKanjoGokei);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(493, 0);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(302, 35);
            this.fsiPanel5.TabIndex = 2;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblKanjoKmk);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel4.Location = new System.Drawing.Point(175, 0);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(318, 35);
            this.fsiPanel4.TabIndex = 1;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtKanjoKmk);
            this.fsiPanel3.Controls.Add(this.label1);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel3.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(175, 35);
            this.fsiPanel3.TabIndex = 0;
            // 
            // txtKanjoKmk
            // 
            this.txtKanjoKmk.AutoSizeFromLength = false;
            this.txtKanjoKmk.DisplayLength = null;
            this.txtKanjoKmk.Location = new System.Drawing.Point(82, 6);
            this.txtKanjoKmk.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanjoKmk.MaxLength = 6;
            this.txtKanjoKmk.Name = "txtKanjoKmk";
            this.txtKanjoKmk.Size = new System.Drawing.Size(85, 23);
            this.txtKanjoKmk.TabIndex = 2;
            this.txtKanjoKmk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.label3);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel7.Location = new System.Drawing.Point(229, 0);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(198, 36);
            this.fsiPanel7.TabIndex = 3;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.lblHojoGokei);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(427, 0);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(364, 36);
            this.fsiPanel8.TabIndex = 2;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.label4);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel6.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(229, 36);
            this.fsiPanel6.TabIndex = 1;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.fsiPanel8);
            this.fsiPanel2.Controls.Add(this.fsiPanel7);
            this.fsiPanel2.Controls.Add(this.fsiPanel6);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(791, 36);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiTableLayoutPanel2
            // 
            this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel2.ColumnCount = 1;
            this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel2, 0, 0);
            this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(8, 421);
            this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
            this.fsiTableLayoutPanel2.RowCount = 1;
            this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(799, 44);
            this.fsiTableLayoutPanel2.TabIndex = 903;
            // 
            // ZMCE1012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 620);
            this.Controls.Add(this.dgvKishuZan);
            this.Controls.Add(this.fsiTableLayoutPanel2);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCE1012";
            this.Text = "補助科目残高";
            this.Shown += new System.EventHandler(this.frm_Shown);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.dgvKishuZan, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).EndInit();
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel2.ResumeLayout(false);
            this.fsiTableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblKanjoKmk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvKishuZan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblHojoGokei;
        private System.Windows.Forms.Label lblKanjoGokei;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
        private common.controls.FsiTextBox txtKanjoKmk;
    }
}