﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace jp.co.fsi.zm.zmce1011
{
    public class DataGridViewEx : DataGridView
    {
        /// <summary>
        /// Enterキー、Tabキー、左右カーソルキーが押された時に、
        /// リードオンリーのセルをスキップする
        /// </summary>
        /// <param name="keyData">キーコード</param>
        /// <returns>true：スキップあり、false：スキップなし</returns>
        private bool skipReadonlyCell(Keys keyData)
        {
            // Enterキー、Tabキー、左右カーソルキー以外はスキップしない
            switch (keyData & Keys.KeyCode)
            {
                case Keys.Enter:
                case Keys.Tab:
                case Keys.Left:
                case Keys.Right:
                    break;
                default:
                    return false;
            }

            int fromIndex = this.CurrentCell.ColumnIndex;
            int toIndex = fromIndex;
            int addIndex = 1;
            int maxCellIndex = this.CurrentRow.Cells.Count - 3;
            int MaxRowIndex = this.Rows.Count - 1;
            DataGridViewCell nextCell = this.CurrentRow.Cells[toIndex];

            // Shiftキー又は左カーソルキーが押されている場合は左方向に移動させる
            if ((keyData & Keys.Shift) == Keys.Shift
             || (keyData & Keys.KeyCode) == Keys.Left)
            {
                addIndex = -1;
            }

            // 左端で左方向の移動の場合は移動しない
            if (toIndex <= 0 && -1 == addIndex)
            {
                nextCell = this.CurrentRow.Cells[0];
                this.CurrentCell = nextCell;
                return true;
            }

            // 右端で右方向の移動の場合は改行する
            if (maxCellIndex <= toIndex && 1 == addIndex)
            {
                if (this.CurrentRow.Index < MaxRowIndex)
                {
                    nextCell = this.Rows.SharedRow(this.CurrentRow.Index + 1).Cells[2];
                    this.CurrentCell = nextCell;
                    this.FirstDisplayedScrollingColumnIndex = 2;
                    this.BeginEdit(true);
                    return true;
                }
                nextCell = this.Rows.SharedRow(this.CurrentRow.Index).Cells[0];
                toIndex = 0;
            }

            do
            {
                // 列（セル）を１つ移動する
                toIndex += addIndex;

                // 右端を超えた場合は改行する
                if (maxCellIndex < toIndex)
                {
                    // 最終行の右端なら処理終了
                    if (this.Rows.Count <= this.CurrentRow.Index + 1)
                    {
                        break;
                    }

                    // 改行する
                    // 下の方法ではうまくいかないので、下キーのイベントを投げるか？
                    nextCell = this.Rows.SharedRow(this.CurrentRow.Index + 1).Cells[0];
                    this.CurrentCell = nextCell;
                    toIndex = 0;
                }
                else
                {
                    // 判定対象のセルを取得する
                    nextCell = this.CurrentRow.Cells[toIndex];
                }

                // 入力可能なセル 又は 左端の場合は、そのセルに遷移する
                if (!nextCell.ReadOnly || !nextCell.Visible || (toIndex + addIndex) <= 0)
                {
                    break;
                }
            } while (0 <= toIndex && toIndex <= maxCellIndex);

            // 次の入力可能セルにフォーカスする
            this.CurrentCell = nextCell;

            return true;

        }

        [System.Security.Permissions.UIPermission(
            System.Security.Permissions.SecurityAction.Demand,
            Window = System.Security.Permissions.UIPermissionWindow.AllWindows)]
        protected override bool ProcessDialogKey(Keys keyData)
        {
            try
            {
                // Enterキー、Tabキー、左右カーソルキー押下時にリードオンリーのセルをスキップする
                if (this.skipReadonlyCell(keyData))
                {
                    return true;
                }

                //Enterキーが押された時は、Tabキーが押されたようにする
                if ((keyData & Keys.KeyCode) == Keys.Enter)
                {
                    return this.ProcessTabKey(keyData);
                }
                return base.ProcessDialogKey(keyData);

            }
            catch (InvalidOperationException /*exp*/)
            {
                /* 入力エラー時に'System.InvalidOperationException'が
                 * 発生した場合は処理せずに終了する。
                 * 'System.InvalidOperationException'が出る理由は、
                 * 「セル値の変更をコミットまたは中止できないため、操作は成功しませんでした。」
                 * らしい。
                */
                return true;
            }

        }

        [System.Security.Permissions.SecurityPermission(
            System.Security.Permissions.SecurityAction.Demand,
            Flags = System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)]
        protected override bool ProcessDataGridViewKey(KeyEventArgs e)
        {
            try
            {
                // Enterキー、Tabキー、左右カーソルキー押下時にリードオンリーのセルをスキップする
                if (this.skipReadonlyCell(e.KeyCode))
                {
                    return true;
                }

                //Enterキーが押された時は、Tabキーが押されたようにする
                if (e.KeyCode == Keys.Enter)
                {
                    return this.ProcessTabKey(e.KeyCode);
                }
                return base.ProcessDataGridViewKey(e);

            }
            catch (InvalidOperationException /*exp*/)
            {
                /* 入力エラー時に'System.InvalidOperationException'が
                 * 発生した場合は処理せずに終了する。
                 * 'System.InvalidOperationException'が出る理由は、
                 * 「セル値の変更をコミットまたは中止できないため、操作は成功しませんでした。」
                 * らしい。
                */
                return true;
            }

        }

    }
}
