﻿namespace jp.co.fsi.zm.zmce1011
{
    partial class ZMCE1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtKariGokei = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKariGokei = new System.Windows.Forms.Label();
            this.txtKashiGokei = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKashiGokei = new System.Windows.Forms.Label();
            this.lblZei = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiTableLayoutPanel3 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.dgvKishuZan = new System.Windows.Forms.DataGridView();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiTableLayoutPanel3.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 708);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1124, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1113, 31);
            this.lblTitle.Text = "期首残高の登録";
            // 
            // txtKariGokei
            // 
            this.txtKariGokei.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtKariGokei.AutoSizeFromLength = false;
            this.txtKariGokei.DisplayLength = null;
            this.txtKariGokei.Enabled = false;
            this.txtKariGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKariGokei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKariGokei.Location = new System.Drawing.Point(73, 6);
            this.txtKariGokei.Margin = new System.Windows.Forms.Padding(4);
            this.txtKariGokei.MaxLength = 6;
            this.txtKariGokei.Name = "txtKariGokei";
            this.txtKariGokei.Size = new System.Drawing.Size(251, 23);
            this.txtKariGokei.TabIndex = 4;
            // 
            // lblKariGokei
            // 
            this.lblKariGokei.BackColor = System.Drawing.Color.Silver;
            this.lblKariGokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKariGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKariGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKariGokei.Location = new System.Drawing.Point(0, 0);
            this.lblKariGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKariGokei.Name = "lblKariGokei";
            this.lblKariGokei.Size = new System.Drawing.Size(446, 34);
            this.lblKariGokei.TabIndex = 3;
            this.lblKariGokei.Tag = "CHANGE";
            this.lblKariGokei.Text = "借方合計";
            this.lblKariGokei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKashiGokei
            // 
            this.txtKashiGokei.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtKashiGokei.AutoSizeFromLength = false;
            this.txtKashiGokei.DisplayLength = null;
            this.txtKashiGokei.Enabled = false;
            this.txtKashiGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtKashiGokei.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKashiGokei.Location = new System.Drawing.Point(100, 6);
            this.txtKashiGokei.Margin = new System.Windows.Forms.Padding(4);
            this.txtKashiGokei.MaxLength = 30;
            this.txtKashiGokei.Name = "txtKashiGokei";
            this.txtKashiGokei.Size = new System.Drawing.Size(251, 23);
            this.txtKashiGokei.TabIndex = 6;
            // 
            // lblKashiGokei
            // 
            this.lblKashiGokei.BackColor = System.Drawing.Color.Silver;
            this.lblKashiGokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKashiGokei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKashiGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblKashiGokei.Location = new System.Drawing.Point(0, 0);
            this.lblKashiGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKashiGokei.Name = "lblKashiGokei";
            this.lblKashiGokei.Size = new System.Drawing.Size(446, 34);
            this.lblKashiGokei.TabIndex = 5;
            this.lblKashiGokei.Tag = "CHANGE";
            this.lblKashiGokei.Text = "貸方合計";
            this.lblKashiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZei
            // 
            this.lblZei.BackColor = System.Drawing.Color.Silver;
            this.lblZei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZei.Location = new System.Drawing.Point(0, 0);
            this.lblZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZei.Name = "lblZei";
            this.lblZei.Size = new System.Drawing.Size(460, 35);
            this.lblZei.TabIndex = 1;
            this.lblZei.Tag = "CHANGE";
            this.lblZei.Text = "【税込み入力】";
            this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(43, 6);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(59, 23);
            this.txtMizuageShishoCd.TabIndex = 0;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(106, 5);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 910;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
            this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(432, 35);
            this.lblMizuageShisho.TabIndex = 909;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 1;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(900, 43);
            this.fsiTableLayoutPanel1.TabIndex = 911;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel4);
            this.fsiPanel1.Controls.Add(this.fsiPanel3);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(892, 35);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblZei);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(432, 0);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(460, 35);
            this.fsiPanel4.TabIndex = 2;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel3.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel3.Controls.Add(this.lblMizuageShisho);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel3.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(432, 35);
            this.fsiPanel3.TabIndex = 1;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtKariGokei);
            this.fsiPanel6.Controls.Add(this.lblKariGokei);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel6.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(446, 34);
            this.fsiPanel6.TabIndex = 1;
            this.fsiPanel6.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtKashiGokei);
            this.fsiPanel5.Controls.Add(this.lblKashiGokei);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(446, 0);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(446, 34);
            this.fsiPanel5.TabIndex = 0;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiTableLayoutPanel3
            // 
            this.fsiTableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel3.ColumnCount = 1;
            this.fsiTableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel3.Controls.Add(this.fsiPanel7, 0, 0);
            this.fsiTableLayoutPanel3.Location = new System.Drawing.Point(7, 521);
            this.fsiTableLayoutPanel3.Name = "fsiTableLayoutPanel3";
            this.fsiTableLayoutPanel3.RowCount = 1;
            this.fsiTableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel3.Size = new System.Drawing.Size(900, 42);
            this.fsiTableLayoutPanel3.TabIndex = 912;
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.fsiPanel5);
            this.fsiPanel7.Controls.Add(this.fsiPanel6);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(892, 34);
            this.fsiPanel7.TabIndex = 1;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // dgvKishuZan
            // 
            this.dgvKishuZan.AllowUserToAddRows = false;
            this.dgvKishuZan.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
            this.dgvKishuZan.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvKishuZan.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKishuZan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvKishuZan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKishuZan.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dgvKishuZan.EnableHeadersVisualStyles = false;
            this.dgvKishuZan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvKishuZan.Location = new System.Drawing.Point(7, 82);
            this.dgvKishuZan.Margin = new System.Windows.Forms.Padding(4);
            this.dgvKishuZan.MultiSelect = false;
            this.dgvKishuZan.Name = "dgvKishuZan";
            this.dgvKishuZan.ReadOnly = true;
            this.dgvKishuZan.RowHeadersVisible = false;
            this.dgvKishuZan.RowTemplate.Height = 21;
            this.dgvKishuZan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvKishuZan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKishuZan.Size = new System.Drawing.Size(900, 432);
            this.dgvKishuZan.TabIndex = 0;
            this.dgvKishuZan.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            this.dgvKishuZan.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEnter);
            this.dgvKishuZan.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvKishuZan_CellMouseDown);
            this.dgvKishuZan.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgv_CellValidating);
            this.dgvKishuZan.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellValueChanged);
            this.dgvKishuZan.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_EditingControlShowing);
            this.dgvKishuZan.Enter += new System.EventHandler(this.dgv_Enter);
            // 
            // ZMCE1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 745);
            this.Controls.Add(this.dgvKishuZan);
            this.Controls.Add(this.fsiTableLayoutPanel3);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "ZMCE1011";
            this.Text = "期首残高の登録";
            this.Shown += new System.EventHandler(this.ZMCE1011_Shown);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel3, 0);
            this.Controls.SetChildIndex(this.dgvKishuZan, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiTableLayoutPanel3.ResumeLayout(false);
            this.fsiPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private common.controls.FsiTextBox txtKariGokei;
        private common.controls.FsiTextBox txtKashiGokei;
        private System.Windows.Forms.Label lblKashiGokei;
        private System.Windows.Forms.Label lblKariGokei;
        private System.Windows.Forms.Label lblZei;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel3;
        private common.FsiPanel fsiPanel7;
        private System.Windows.Forms.DataGridView dgvKishuZan;
    }
}