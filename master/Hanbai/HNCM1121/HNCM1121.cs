﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1121
{
    /// <summary>
    /// 仲買人変換マスタメンテ(HNCM1121)
    /// </summary>
    public partial class HNCM1121 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1121()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(600, 500);
                // フォームの配置を上へ移動する
                this.lblNakagaiNm.Location = new System.Drawing.Point(13, 11);
                this.txtNakagaiNm.Location = new System.Drawing.Point(90, 13);
                this.dgvList.Location = new System.Drawing.Point(13, 40);
                // EscapeとF1のみ表示
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
                this.ShowFButton = true;
            }
            else
            {
                // メニューから起動の場合はファンクションボタンを表示しない
                this.ShowFButton = false;
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            //// IMEmodeをひらがなに設定
            this.txtNakagaiNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            // カナ名にフォーカス
            this.txtNakagaiNm.Focus();
            this.txtNakagaiNm.SelectAll();
        }

        /// <summary>
        /// Escボタン押下時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtNakagaiNm.Focus();
            this.txtNakagaiNm.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ担当者登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 仲買人CD変換マスタ登録(新規)画面の起動
                EditNakagai(string.Empty,string.Empty);
            }
        }
        
        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfNmYesNo("プレビュー","実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANC9161R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaNm_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない

            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditNakagai(Util.ToString(this.dgvList.SelectedRows[0].Cells["名護仲買人CD"].Value),
                                 Util.ToString(this.dgvList.SelectedRows[0].Cells["仲買人CD"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditNakagai(Util.ToString(this.dgvList.SelectedRows[0].Cells["名護仲買人CD"].Value),
                           Util.ToString(this.dgvList.SelectedRows[0].Cells["仲買人CD"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 仲買人CD変換マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            if (isInitial)
            {
                // 初期処理の場合、検索結果がヒットしないようにあり得ない検索条件を設定する
                //where.Append(" AND 1 = 0");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtNakagaiNm.Text))
                {
                    where.Append(" AND NIUKENIN_NM LIKE @NIUKENIN_NM");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@NIUKENIN_NM", SqlDbType.VarChar, 42, "%" + this.txtNakagaiNm.Text + "%");
                }
            }

            string cols = "KEN_GYOREN_NIUKENIN_CD AS 名護仲買人CD";
            cols += ", KEN_GYOREN_NIUKENIN_NM AS 名護仲買人名";
            cols += ", NIUKENIN_CD AS 仲買人CD";
            cols += ", NIUKENIN_NM AS 仲買人名";
            string from = "TB_HN_NIUKENIN_CD_HENKAN_MST";

            DataTable dtNakagai =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "KEN_GYOREN_NIUKENIN_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtNakagai.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("データがありません。");
                    this.txtNakagaiNm.Focus();
                }

                dtNakagai.Rows.Add(dtNakagai.NewRow());
            }

            this.dgvList.DataSource = dtNakagai;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 120;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 168;
            this.dgvList.Columns[2].Width = 82;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[3].Width = 168;
        }

        /// <summary>
        /// 仲買人を新規・編集する
        /// </summary>
        /// <param name="code">名護仲買人コード(空：新規登録、以外：編集)</param>
        private void EditNakagai(string code , string code2)
        {
            HNCM1122 frmHANC9162;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHANC9162 = new HNCM1122("1");
            }
            else
            {
                string[] NakagaiCd = new string[2];

                NakagaiCd[0] = code;
                NakagaiCd[1] = code2;

                // 編集モードで登録画面を起動
                frmHANC9162 = new HNCM1122("2");
                frmHANC9162.InData = NakagaiCd;
            }

            DialogResult result = frmHANC9162.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["名護仲買人CD"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                   
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNCM1121R rpt = new HNCM1121R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        //rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            //TB_HN_NIUKENIN_CD_HENKAN_MSTから全データ取得
            sql.Append("SELECT");
            sql.Append(" KEN_GYOREN_NIUKENIN_CD,");
            sql.Append(" KEN_GYOREN_NIUKENIN_NM,");
            sql.Append(" NIUKENIN_CD,");
            sql.Append(" NIUKENIN_NM ");
            sql.Append("FROM ");
            sql.Append(" TB_HN_NIUKENIN_CD_HENKAN_MST ");
            sql.Append(" ORDER BY ");
            sql.Append(" KEN_GYOREN_NIUKENIN_CD");
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("データがありません。");
                return false;
            }
            else
            {
                    while (dtMainLoop.Rows.Count > i)
                    {
                        #region インサートテーブル
                        sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        sql.Append("INSERT INTO PR_HN_TBL(");
                        sql.Append("  GUID");
                        sql.Append(" ,SORT");
                        sql.Append(" ,ITEM01");
                        sql.Append(" ,ITEM02");
                        sql.Append(" ,ITEM03");
                        sql.Append(" ,ITEM04");
                        sql.Append(") ");
                        sql.Append("VALUES(");
                        sql.Append("  @GUID");
                        sql.Append(" ,@SORT");
                        sql.Append(" ,@ITEM01");
                        sql.Append(" ,@ITEM02");
                        sql.Append(" ,@ITEM03");
                        sql.Append(" ,@ITEM04");
                        sql.Append(") ");
                        #endregion

                        #region データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KEN_GYOREN_NIUKENIN_CD"]); // 仲買人コード（名護）
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KEN_GYOREN_NIUKENIN_NM"]); // 仲買人名（名護）
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NIUKENIN_CD"]); // 仲買人コード（宜野座）
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NIUKENIN_NM"]); // 仲買人名（宜野座）
                        this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                        #endregion
                        i++;
                    }
                }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[4] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["名護仲買人CD"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["名護仲買人名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仲買人CD"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仲買人名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
