﻿using System;
using System.Data;
using System.Drawing;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnrm1101
{
    /// <summary>
    /// NMR1102R 地区別個人別業態別漁獲高月報  ※実は累計出力！
    /// </summary>
    public partial class HNMR1102R : BaseReport
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        public HNMR1102R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

    }
}
