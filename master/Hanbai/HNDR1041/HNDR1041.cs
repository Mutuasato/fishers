﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hndr1041
{
    /// <summary>
    /// 個人鮮魚代金明細書兼請求書出力(HNDR1041)
    /// </summary>
    public partial class HNDR1041 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷テーブル使用列数
        /// </summary>
        public const int prtCols = 11;
        //public const int prtCols = 10;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNDR1041()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = this.Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲前
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            txtDateMonth.Text = jpDate[3];
            txtDateDay.Text = jpDate[4];

            // フォーカス設定
            this.txtDateYear.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年),仲買人コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtNakagaininCdFr":
                case "txtNakagaininCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                #endregion

                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                                    this.txtDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengo.Text,
                                        this.txtDateYear.Text,
                                        this.txtDateMonth.Text,
                                        this.txtDateDay.Text,
                                        this.Dba);
                                this.lblDateGengo.Text = arrJpDate[0];
                                this.txtDateYear.Text = arrJpDate[2];
                                this.txtDateMonth.Text = arrJpDate[3];
                                this.txtDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdFr.Text = outData[0];
                                this.lblNakagaininCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdTo.Text = outData[0];
                                this.lblNakagaininCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNDR1041R" });
            psForm.ShowDialog();
        }
#endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            //if (!this.IsValidMizuageShishoCd())
            //{
            //    e.Cancel = true;

            //    this.lblMizuageShishoNm.Text = "";
            //    this.txtMizuageShishoCd.SelectAll();
            //    this.txtMizuageShishoCd.Focus();
            //}
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                this.txtDateYear.Text = Util.ToString(IsValid.SetYear(this.txtDateYear.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonth.SelectAll();
            }
            else
            {
                this.txtDateMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonth.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDay.SelectAll();
            }
            else
            {
                this.txtDateDay.Text = Util.ToString(IsValid.SetDay(this.txtDateDay.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 仲買人コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtNakagaininCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtNakagaininCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 仲買人コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtNakagaininCdTo.Focus();
                }
            }
        }
#endregion

#region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年月日(請求書発行)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
            {
                this.txtDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(請求書発行)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 仲買人コード(自)の入力チェック
        /// </summary>
        /// 
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdFr.Text))
            {
                this.lblNakagaininCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtNakagaininCdFr.Text))
            {
                Msg.Error("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblNakagaininCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtMizuageShishoCd.Text, this.txtNakagaininCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 仲買人コード(至)の入力チェック
        /// </summary>
        /// 
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdTo.Text))
            {
                this.lblNakagaininCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtNakagaininCdTo.Text))
            {
                Msg.Error("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblNakagaininCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtMizuageShishoCd.Text, this.txtNakagaininCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                this.txtDateDay.Focus();
                this.txtDateDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpDate();
            // 年月日(自)の正しい和暦への変換処理
            SetJpDate();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
            this.txtDateDay.Text = arrJpDate[4];
        }

        ///// <summary>
        ///// 検索サブウィンドウオープン
        ///// </summary>
        ///// <param name="moduleName">例："COMC8111"</param>
        ///// <param name="para1">例："TB_CM_SHISHO"</param>
        ///// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        ///// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        ///// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        //private String[] openSearchWindow(String moduleName, String para1, String indata)
        //{
        //    string[] result = { "", "" };

        //    // ネームスペースに使うモジュール名の小文字
        //    string lowerModuleName = moduleName.ToLower();

        //    // ネームスペースの末尾
        //    string nameSpace = lowerModuleName.Substring(0, 3);

        //    // アセンブリのロード
        //    Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

        //    // フォーム作成
        //    string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
        //    Type t = asm.GetType(moduleNameSpace);

        //    if (t != null)
        //    {
        //        Object obj = Activator.CreateInstance(t);
        //        if (obj != null)
        //        {
        //            BasePgForm frm = (BasePgForm)obj;
        //            frm.Par1 = para1;
        //            frm.InData = indata;
        //            frm.ShowDialog(this);

        //            if (frm.DialogResult == DialogResult.OK)
        //            {
        //                string[] ret = (string[])frm.OutData;
        //                result[0] = ret[0];
        //                result[1] = ret[1];
        //                return result;
        //            }
        //        }
        //    }

        //    return result;
        //}

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
                //
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);

                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNDR1041R rpt = new HNDR1041R(dtOutput);
                    rpt.Document.Name = this.lblTitle.Text;
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 日付を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);
            string tmpSeribi = Util.ToDateStr(tmpDate);
            // 日付を和暦で保持
            string[] jpDate = Util.ConvJpDate(tmpDate, this.Dba);
            // 仲買人コード設定
            string NAKAGAININ_CD_FR;
            string NAKAGAININ_CD_TO;
            if (Util.ToDecimal(txtNakagaininCdFr.Text) > 0)
            {
                NAKAGAININ_CD_FR = txtNakagaininCdFr.Text;
            }
            else
            {
                NAKAGAININ_CD_FR = "0";
            }
            if (Util.ToDecimal(txtNakagaininCdTo.Text) > 0)
            {
                NAKAGAININ_CD_TO = txtNakagaininCdTo.Text;
            }
            else
            {
                NAKAGAININ_CD_TO = "99999";
            }
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;

            int dbSORT = 1;
            int i = 0; // メインループ用カウント変数
            #endregion

            #region メインデータ取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // han.VI_鮮魚代金明細兼請求書(VI_HN_SNGDIKN_MISI_KEN_SIKS)からデータを取得
            Sql.Append(" SELECT ");
            if (shishoCd != "0")
            {
                Sql.Append(" A.SHISHO_CD,");
                Sql.Append(" MAX(A.SHISHO_NM) AS SHISHO_NM,");
            }
            else
            {
                Sql.Append(" 0      AS SHISHO_CD,");
                Sql.Append(" ''     AS SHISHO_NM,");
            }
            Sql.Append(" A.SERIBI,");
            Sql.Append(" MIN(A.NAKAGAININ_CD) AS NAKAGAININ_CD,");
            Sql.Append(" A.NAKAGAININ_NM AS NAKAGAININ_NM,");
            Sql.Append(" COUNT(A.NAKAGAININ_CD) AS KENSU,");
            Sql.Append(" SUM(A.SURYO) AS SURYO,");
            Sql.Append(" SUM(A.KINGAKU) AS KINGAKU,");
            Sql.Append(" SUM(A.ZEINUKI_KINGAKU) AS ZEINUKI_KINGAKU,");
            Sql.Append(" dbo.FNC_MarumeS(SUM(A.ZEINUKI_KINGAKU) * (A.ZEI_RITSU / 100), 0, A.NAKAGAI_SHOHIZEI_HASU_SHORI) AS SHOHIZEI,");
            Sql.Append(" B.ZANDAKA ");
            Sql.Append(" FROM VI_HN_SNGDIKN_MISI_KEN_SIKS AS A ");
            Sql.Append(" LEFT OUTER JOIN ");
            Sql.Append(" (");
            Sql.Append(" 	SELECT ");
            Sql.Append(" 		A.KAISHA_CD,");
            Sql.Append(" 		A.HOJO_KAMOKU_CD AS KAIIN_BANGO,");
            Sql.Append(" 		SUM(CASE WHEN A.TAISHAKU_KUBUN = C.TAISHAKU_KUBUN THEN A.ZEIKOMI_KINGAKU ELSE (A.ZEIKOMI_KINGAKU * - 1) END) AS ZANDAKA ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI AS A");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B ");
            Sql.Append(" 		ON	A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append(" 		AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            Sql.Append(" 		AND B.MOTOCHO_KUBUN = 1");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C");
            Sql.Append(" 		ON	A.KAISHA_CD = C.KAISHA_CD ");
            Sql.Append(" 		AND A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
            Sql.Append(" 		AND A.KAIKEI_NENDO = C.KAIKEI_NENDO");
            Sql.Append(" 	LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS D");
            Sql.Append(" 		ON	A.KAISHA_CD = D.KAISHA_CD ");
            Sql.Append(" 		AND A.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD");
            Sql.Append(" 	WHERE ");
            Sql.Append("	    A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
            {
                Sql.Append("	    A.SHISHO_CD = @SHISHO_CD AND ");
            }
            Sql.Append("	    A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" 		A.HOJO_KAMOKU_CD BETWEEN 0 AND 9999 AND ");
            Sql.Append(" 		D.TORIHIKISAKI_KUBUN2 = 2 AND ");
            Sql.Append(" 		A.DENPYO_DATE < @DATE ");
            Sql.Append(" 	GROUP BY ");
            Sql.Append(" 		A.KAISHA_CD,");
            Sql.Append(" 		A.HOJO_KAMOKU_CD");
            Sql.Append(" ) AS B ");
            Sql.Append(" 	ON	A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append(" 	AND A.NAKAGAININ_CD = B.KAIIN_BANGO ");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_SHOHIZEI_JOHO AS D ");
            Sql.Append(" 	ON	A.KAISHA_CD = D.KAISHA_CD ");
            Sql.Append(" 	AND D.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append(" 	AND D.KESSANKI = @KESSANKI ");
            Sql.Append(" WHERE ");
            Sql.Append(" 	A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
            {
                Sql.Append(" 	A.SHISHO_CD = @SHISHO_CD AND ");
            }
            Sql.Append(" 	A.SERIBI = @DATE AND ");
            Sql.Append(" 	NAKAGAININ_CD BETWEEN @NAKAGAININ_CD_FR AND @NAKAGAININ_CD_TO ");
            Sql.Append(" GROUP BY ");
            if (shishoCd != "0")
            {
                Sql.Append(" 	A.SHISHO_CD,");
            }
            Sql.Append(" 	A.SEISAN_KUBUN,");
            Sql.Append(" 	A.SERIBI,");
            Sql.Append(" 	A.NAKAGAININ_NM,");
            Sql.Append(" 	B.ZANDAKA,");
            Sql.Append(" 	A.ZEI_RITSU,");
            //Sql.Append("    A.NAKAGAI_SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append(" 	A.NAKAGAI_SHOHIZEI_HASU_SHORI");
            Sql.Append(" ORDER BY ");
            Sql.Append(" NAKAGAININ_CD");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 4, this.UInfo.KessanKi);
            dpc.SetParam("@DATE", SqlDbType.VarChar, 10, tmpDate.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@NAKAGAININ_CD_FR", SqlDbType.VarChar, 6, NAKAGAININ_CD_FR);
            dpc.SetParam("@NAKAGAININ_CD_TO", SqlDbType.VarChar, 6, NAKAGAININ_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    dpc = new DbParamCollection();
                    Sql = new StringBuilder();

                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                    Sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpSeribi); // セリ日
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_CD"]); // 仲買人CD
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NAKAGAININ_NM"]); // 仲買人名称
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KENSU"]); // 件数
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 買上数量
                    //dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU"])); // 買上金額
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEINUKI_KINGAKU"])); // 買上金額
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI"], 0)); // 消費税
                    if (Util.ToString(dtMainLoop.Rows[i]["ZANDAKA"]) == "")
                    {
                        dtMainLoop.Rows[i]["ZANDAKA"] = 0;
                    }
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZANDAKA"])); // 前回請求額
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.ToString(dtMainLoop.Rows[i]["SHISHO_CD"])); // 支所コード
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.ToString(dtMainLoop.Rows[i]["SHISHO_NM"])); // 支所名
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.ToString(dtMainLoop.Rows[i]["KINGAKU"])); // 税抜金額

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion

                    dbSORT++;
                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;

        }
        #endregion
    }
}
