﻿namespace jp.co.fsi.hn.hnmr1131
{
    partial class HNMR1133
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnEnter = new System.Windows.Forms.Button();
			this.txtShukeihyoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShukeihyoCd = new System.Windows.Forms.Label();
			this.lblInsatsuHokoNm = new System.Windows.Forms.Label();
			this.txtInsatsuHoko = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblInsatsuHoko = new System.Windows.Forms.Label();
			this.txtShukeihyoTitle = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShukeihyoTitle = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(4, 65);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(176, 65);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			this.btnF1.Text = "F1";
			this.btnF1.Visible = false;
			// 
			// btnF2
			// 
			this.btnF2.Location = new System.Drawing.Point(176, 65);
			this.btnF2.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF3
			// 
			this.btnF3.Location = new System.Drawing.Point(261, 65);
			this.btnF3.Margin = new System.Windows.Forms.Padding(5);
			this.btnF3.Text = "F3\r\n\r\n削除";
			this.btnF3.Visible = false;
			// 
			// btnF4
			// 
			this.btnF4.Location = new System.Drawing.Point(261, 65);
			this.btnF4.Margin = new System.Windows.Forms.Padding(5);
			this.btnF4.Visible = false;
			// 
			// btnF5
			// 
			this.btnF5.Location = new System.Drawing.Point(429, 65);
			this.btnF5.Margin = new System.Windows.Forms.Padding(5);
			this.btnF5.Text = "F5";
			this.btnF5.Visible = false;
			// 
			// btnF7
			// 
			this.btnF7.Location = new System.Drawing.Point(603, 65);
			this.btnF7.Margin = new System.Windows.Forms.Padding(5);
			this.btnF7.Visible = false;
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(516, 65);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			this.btnF6.Text = "F6\r\n\r\n登録";
			this.btnF6.Visible = false;
			// 
			// btnF8
			// 
			this.btnF8.Location = new System.Drawing.Point(688, 65);
			this.btnF8.Margin = new System.Windows.Forms.Padding(5);
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Location = new System.Drawing.Point(773, 65);
			this.btnF9.Margin = new System.Windows.Forms.Padding(5);
			this.btnF9.Visible = false;
			// 
			// btnF12
			// 
			this.btnF12.Location = new System.Drawing.Point(1029, 65);
			this.btnF12.Margin = new System.Windows.Forms.Padding(5);
			this.btnF12.Text = "F12";
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Location = new System.Drawing.Point(944, 65);
			this.btnF11.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF10
			// 
			this.btnF10.Location = new System.Drawing.Point(859, 65);
			this.btnF10.Margin = new System.Windows.Forms.Padding(5);
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Location = new System.Drawing.Point(8, 107);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(600, 133);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(623, 31);
			this.lblTitle.Text = "売上集計表検索";
			// 
			// btnEnter
			// 
			this.btnEnter.BackColor = System.Drawing.Color.SkyBlue;
			this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEnter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.btnEnter.ForeColor = System.Drawing.Color.Navy;
			this.btnEnter.Location = new System.Drawing.Point(90, 65);
			this.btnEnter.Margin = new System.Windows.Forms.Padding(4);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new System.Drawing.Size(87, 60);
			this.btnEnter.TabIndex = 905;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n決定";
			this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseCompatibleTextRendering = true;
			this.btnEnter.UseVisualStyleBackColor = false;
			// 
			// txtShukeihyoCd
			// 
			this.txtShukeihyoCd.AutoSizeFromLength = true;
			this.txtShukeihyoCd.DisplayLength = null;
			this.txtShukeihyoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShukeihyoCd.Location = new System.Drawing.Point(108, 2);
			this.txtShukeihyoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtShukeihyoCd.MaxLength = 4;
			this.txtShukeihyoCd.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtShukeihyoCd.Name = "txtShukeihyoCd";
			this.txtShukeihyoCd.Size = new System.Drawing.Size(63, 23);
			this.txtShukeihyoCd.TabIndex = 1;
			this.txtShukeihyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShukeihyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShukeihyo_Validating);
			// 
			// lblShukeihyoCd
			// 
			this.lblShukeihyoCd.BackColor = System.Drawing.Color.Silver;
			this.lblShukeihyoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShukeihyoCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShukeihyoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblShukeihyoCd.Location = new System.Drawing.Point(0, 0);
			this.lblShukeihyoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShukeihyoCd.Name = "lblShukeihyoCd";
			this.lblShukeihyoCd.Size = new System.Drawing.Size(409, 33);
			this.lblShukeihyoCd.TabIndex = 0;
			this.lblShukeihyoCd.Tag = "CHANGE";
			this.lblShukeihyoCd.Text = "集計表コード";
			this.lblShukeihyoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblInsatsuHokoNm
			// 
			this.lblInsatsuHokoNm.BackColor = System.Drawing.Color.Silver;
			this.lblInsatsuHokoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblInsatsuHokoNm.Location = new System.Drawing.Point(132, 3);
			this.lblInsatsuHokoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblInsatsuHokoNm.Name = "lblInsatsuHokoNm";
			this.lblInsatsuHokoNm.Size = new System.Drawing.Size(111, 24);
			this.lblInsatsuHokoNm.TabIndex = 4;
			this.lblInsatsuHokoNm.Tag = "CHANGE";
			this.lblInsatsuHokoNm.Text = "0:縦　1:横";
			this.lblInsatsuHokoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtInsatsuHoko
			// 
			this.txtInsatsuHoko.AutoSizeFromLength = true;
			this.txtInsatsuHoko.DisplayLength = null;
			this.txtInsatsuHoko.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtInsatsuHoko.Location = new System.Drawing.Point(108, 3);
			this.txtInsatsuHoko.Margin = new System.Windows.Forms.Padding(4);
			this.txtInsatsuHoko.MaxLength = 1;
			this.txtInsatsuHoko.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtInsatsuHoko.Name = "txtInsatsuHoko";
			this.txtInsatsuHoko.Size = new System.Drawing.Size(16, 23);
			this.txtInsatsuHoko.TabIndex = 3;
			this.txtInsatsuHoko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtInsatsuHoko.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInsatsuHoko_KeyDown);
			this.txtInsatsuHoko.Validating += new System.ComponentModel.CancelEventHandler(this.txtInsatsuHoko_Validating);
			// 
			// lblInsatsuHoko
			// 
			this.lblInsatsuHoko.BackColor = System.Drawing.Color.Silver;
			this.lblInsatsuHoko.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblInsatsuHoko.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblInsatsuHoko.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblInsatsuHoko.Location = new System.Drawing.Point(0, 0);
			this.lblInsatsuHoko.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblInsatsuHoko.Name = "lblInsatsuHoko";
			this.lblInsatsuHoko.Size = new System.Drawing.Size(409, 34);
			this.lblInsatsuHoko.TabIndex = 2;
			this.lblInsatsuHoko.Tag = "CHANGE";
			this.lblInsatsuHoko.Text = "印 刷 方 向";
			this.lblInsatsuHoko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShukeihyoTitle
			// 
			this.txtShukeihyoTitle.AutoSizeFromLength = true;
			this.txtShukeihyoTitle.DisplayLength = null;
			this.txtShukeihyoTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShukeihyoTitle.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtShukeihyoTitle.Location = new System.Drawing.Point(108, 3);
			this.txtShukeihyoTitle.Margin = new System.Windows.Forms.Padding(4);
			this.txtShukeihyoTitle.MaxLength = 20;
			this.txtShukeihyoTitle.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtShukeihyoTitle.Name = "txtShukeihyoTitle";
			this.txtShukeihyoTitle.Size = new System.Drawing.Size(193, 23);
			this.txtShukeihyoTitle.TabIndex = 1;
			this.txtShukeihyoTitle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShukeihyoTitle_KeyDown);
			this.txtShukeihyoTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtShukeihyoTitle_Validating);
			// 
			// lblShukeihyoTitle
			// 
			this.lblShukeihyoTitle.BackColor = System.Drawing.Color.Silver;
			this.lblShukeihyoTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShukeihyoTitle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShukeihyoTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblShukeihyoTitle.Location = new System.Drawing.Point(0, 0);
			this.lblShukeihyoTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShukeihyoTitle.Name = "lblShukeihyoTitle";
			this.lblShukeihyoTitle.Size = new System.Drawing.Size(409, 33);
			this.lblShukeihyoTitle.TabIndex = 0;
			this.lblShukeihyoTitle.Tag = "CHANGE";
			this.lblShukeihyoTitle.Text = "タ イ ト ル";
			this.lblShukeihyoTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.4F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(417, 122);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtInsatsuHoko);
			this.fsiPanel3.Controls.Add(this.lblInsatsuHokoNm);
			this.fsiPanel3.Controls.Add(this.lblInsatsuHoko);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 84);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(409, 34);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtShukeihyoTitle);
			this.fsiPanel2.Controls.Add(this.lblShukeihyoTitle);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 44);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(409, 33);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtShukeihyoCd);
			this.fsiPanel1.Controls.Add(this.lblShukeihyoCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(409, 33);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNMR1133
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(623, 242);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNMR1133";
			this.ShowFButton = true;
			this.Text = "集計表の設定";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button btnEnter;
        private jp.co.fsi.common.controls.FsiTextBox txtShukeihyoCd;
        private System.Windows.Forms.Label lblShukeihyoCd;
        private System.Windows.Forms.Label lblInsatsuHokoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtInsatsuHoko;
        private System.Windows.Forms.Label lblInsatsuHoko;
        private jp.co.fsi.common.controls.FsiTextBox txtShukeihyoTitle;
        private System.Windows.Forms.Label lblShukeihyoTitle;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}