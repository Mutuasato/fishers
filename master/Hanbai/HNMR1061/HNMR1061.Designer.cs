﻿namespace jp.co.fsi.hn.hnmr1061
{
    partial class HNMR1061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblCodeBetDate = new System.Windows.Forms.Label();
			this.lblDateMonthTo = new System.Windows.Forms.Label();
			this.lblDateYearTo = new System.Windows.Forms.Label();
			this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoTo = new System.Windows.Forms.Label();
			this.lblDateMonthFr = new System.Windows.Forms.Label();
			this.lblDateYearFr = new System.Windows.Forms.Label();
			this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoFr = new System.Windows.Forms.Label();
			this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanKbnNm = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 708);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1124, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1113, 41);
			this.lblTitle.Text = "";
			// 
			// lblCodeBetDate
			// 
			this.lblCodeBetDate.AutoSize = true;
			this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
			this.lblCodeBetDate.Location = new System.Drawing.Point(320, 5);
			this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBetDate.Name = "lblCodeBetDate";
			this.lblCodeBetDate.Size = new System.Drawing.Size(24, 16);
			this.lblCodeBetDate.TabIndex = 902;
			this.lblCodeBetDate.Tag = "CHANGE";
			this.lblCodeBetDate.Text = "～";
			this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblDateMonthTo
			// 
			this.lblDateMonthTo.AutoSize = true;
			this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateMonthTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateMonthTo.Location = new System.Drawing.Point(541, 5);
			this.lblDateMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateMonthTo.Name = "lblDateMonthTo";
			this.lblDateMonthTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthTo.TabIndex = 13;
			this.lblDateMonthTo.Tag = "CHANGE";
			this.lblDateMonthTo.Text = "月";
			this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateYearTo
			// 
			this.lblDateYearTo.AutoSize = true;
			this.lblDateYearTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateYearTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateYearTo.Location = new System.Drawing.Point(462, 5);
			this.lblDateYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateYearTo.Name = "lblDateYearTo";
			this.lblDateYearTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateYearTo.TabIndex = 11;
			this.lblDateYearTo.Tag = "CHANGE";
			this.lblDateYearTo.Text = "年";
			this.lblDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateYearTo
			// 
			this.txtDateYearTo.AutoSizeFromLength = false;
			this.txtDateYearTo.DisplayLength = null;
			this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDateYearTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateYearTo.Location = new System.Drawing.Point(420, 2);
			this.txtDateYearTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateYearTo.MaxLength = 2;
			this.txtDateYearTo.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtDateYearTo.Name = "txtDateYearTo";
			this.txtDateYearTo.Size = new System.Drawing.Size(39, 24);
			this.txtDateYearTo.TabIndex = 10;
			this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
			// 
			// txtDateMonthTo
			// 
			this.txtDateMonthTo.AutoSizeFromLength = false;
			this.txtDateMonthTo.DisplayLength = null;
			this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDateMonthTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateMonthTo.Location = new System.Drawing.Point(498, 2);
			this.txtDateMonthTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateMonthTo.MaxLength = 2;
			this.txtDateMonthTo.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtDateMonthTo.Name = "txtDateMonthTo";
			this.txtDateMonthTo.Size = new System.Drawing.Size(39, 24);
			this.txtDateMonthTo.TabIndex = 12;
			this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
			// 
			// lblDateGengoTo
			// 
			this.lblDateGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateGengoTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengoTo.Location = new System.Drawing.Point(360, 1);
			this.lblDateGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateGengoTo.Name = "lblDateGengoTo";
			this.lblDateGengoTo.Size = new System.Drawing.Size(55, 32);
			this.lblDateGengoTo.TabIndex = 8;
			this.lblDateGengoTo.Tag = "DISPNAME";
			this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateMonthFr
			// 
			this.lblDateMonthFr.AutoSize = true;
			this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateMonthFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateMonthFr.Location = new System.Drawing.Point(279, 5);
			this.lblDateMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateMonthFr.Name = "lblDateMonthFr";
			this.lblDateMonthFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthFr.TabIndex = 5;
			this.lblDateMonthFr.Tag = "CHANGE";
			this.lblDateMonthFr.Text = "月";
			this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateYearFr
			// 
			this.lblDateYearFr.AutoSize = true;
			this.lblDateYearFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateYearFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateYearFr.Location = new System.Drawing.Point(200, 5);
			this.lblDateYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateYearFr.Name = "lblDateYearFr";
			this.lblDateYearFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateYearFr.TabIndex = 3;
			this.lblDateYearFr.Tag = "CHANGE";
			this.lblDateYearFr.Text = "年";
			this.lblDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateYearFr
			// 
			this.txtDateYearFr.AutoSizeFromLength = false;
			this.txtDateYearFr.DisplayLength = null;
			this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateYearFr.Location = new System.Drawing.Point(157, 2);
			this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateYearFr.MaxLength = 2;
			this.txtDateYearFr.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtDateYearFr.Name = "txtDateYearFr";
			this.txtDateYearFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearFr.TabIndex = 2;
			this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
			// 
			// txtDateMonthFr
			// 
			this.txtDateMonthFr.AutoSizeFromLength = false;
			this.txtDateMonthFr.DisplayLength = null;
			this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateMonthFr.Location = new System.Drawing.Point(236, 2);
			this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateMonthFr.MaxLength = 2;
			this.txtDateMonthFr.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtDateMonthFr.Name = "txtDateMonthFr";
			this.txtDateMonthFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthFr.TabIndex = 4;
			this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
			// 
			// lblDateGengoFr
			// 
			this.lblDateGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengoFr.Location = new System.Drawing.Point(97, 1);
			this.lblDateGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateGengoFr.Name = "lblDateGengoFr";
			this.lblDateGengoFr.Size = new System.Drawing.Size(55, 32);
			this.lblDateGengoFr.TabIndex = 1;
			this.lblDateGengoFr.Tag = "DISPNAME";
			this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanKbn
			// 
			this.txtSeisanKbn.AutoSizeFromLength = true;
			this.txtSeisanKbn.DisplayLength = null;
			this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSeisanKbn.Location = new System.Drawing.Point(97, 4);
			this.txtSeisanKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanKbn.MaxLength = 5;
			this.txtSeisanKbn.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtSeisanKbn.Name = "txtSeisanKbn";
			this.txtSeisanKbn.Size = new System.Drawing.Size(67, 23);
			this.txtSeisanKbn.TabIndex = 7;
			this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSeisanKbn_KeyDown);
			this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
			// 
			// lblSeisanKbnNm
			// 
			this.lblSeisanKbnNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblSeisanKbnNm.Location = new System.Drawing.Point(168, 2);
			this.lblSeisanKbnNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
			this.lblSeisanKbnNm.Size = new System.Drawing.Size(283, 32);
			this.lblSeisanKbnNm.TabIndex = 8;
			this.lblSeisanKbnNm.Tag = "DISPNAME";
			this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(97, 3);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(144, 1);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 32);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 47);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.4F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(585, 115);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtSeisanKbn);
			this.fsiPanel3.Controls.Add(this.lblSeisanKbnNm);
			this.fsiPanel3.Controls.Add(this.label3);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 80);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(575, 30);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.MinimumSize = new System.Drawing.Size(0, 32);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(575, 32);
			this.label3.TabIndex = 2;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "精算区分";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblDateGengoFr);
			this.fsiPanel2.Controls.Add(this.lblCodeBetDate);
			this.fsiPanel2.Controls.Add(this.txtDateYearFr);
			this.fsiPanel2.Controls.Add(this.lblDateYearFr);
			this.fsiPanel2.Controls.Add(this.lblDateMonthTo);
			this.fsiPanel2.Controls.Add(this.txtDateMonthFr);
			this.fsiPanel2.Controls.Add(this.lblDateMonthFr);
			this.fsiPanel2.Controls.Add(this.lblDateYearTo);
			this.fsiPanel2.Controls.Add(this.lblDateGengoTo);
			this.fsiPanel2.Controls.Add(this.txtDateMonthTo);
			this.fsiPanel2.Controls.Add(this.txtDateYearTo);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 42);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(575, 29);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.MinimumSize = new System.Drawing.Size(0, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(575, 32);
			this.label2.TabIndex = 2;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "日付範囲";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(575, 28);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.MinimumSize = new System.Drawing.Size(0, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(575, 32);
			this.label1.TabIndex = 2;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "水揚支所";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// HNMR1061
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1113, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNMR1061";
			this.Text = "";
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label lblDateYearTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label lblSeisanKbnNm;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label3;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label1;
    }
}