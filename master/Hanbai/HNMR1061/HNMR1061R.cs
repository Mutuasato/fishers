﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.hn.hnmr1061
{
    /// <summary>
    /// HNMR1061R の概要の説明です。
    /// </summary>
    public partial class HNMR1061R : BaseReport
    {

        public HNMR1061R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }

        private void reportFooter1_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            this.txtTotal01.Text = Util.FormatNum(this.txtTotal01.Text);
            this.txtTotal02.Text = Util.FormatNum(Util.ToDecimal(this.txtTotal02.Text),2);
            this.txtTotal03.Text = Util.FormatNum(this.txtTotal03.Text);
            this.txtTotal04.Text = Util.FormatNum(this.txtTotal04.Text);
            this.txtTotal05.Text = Util.FormatNum(this.txtTotal05.Text);
            this.txtTotal06.Text = Util.FormatNum(this.txtTotal06.Text);
            // 合計金額が0の場合、空表示とする
            if (this.txtTotal01.Text == "0")
            {
                this.txtTotal01.Text = "";
            }
            if (this.txtTotal02.Text == "0")
            {
                this.txtTotal02.Text = "";
            }
            if (this.txtTotal03.Text == "0")
            {
                this.txtTotal03.Text = "";
            }
            if (this.txtTotal04.Text == "0")
            {
                this.txtTotal04.Text = "";
            }
            if (this.txtTotal05.Text == "0")
            {
                this.txtTotal05.Text = "";
            }
            if (this.txtTotal06.Text == "0")
            {
                this.txtTotal06.Text = "";
            }
        }
    }
}
