﻿namespace jp.co.fsi.hn.hnmr1091
{
    partial class HNMR1092
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnLMove = new System.Windows.Forms.Button();
			this.btnRMove = new System.Windows.Forms.Button();
			this.lblTitleCd = new System.Windows.Forms.Label();
			this.txtShukeiCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShukeiNm = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.lbxSentakuKomoku = new System.Windows.Forms.ListBox();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.lbxShukeiKubun = new System.Windows.Forms.ListBox();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF2
			// 
			this.btnF2.Text = "F2";
			// 
			// btnF3
			// 
			this.btnF3.Text = "F3";
			// 
			// btnF4
			// 
			this.btnF4.Text = "F4";
			// 
			// btnF5
			// 
			this.btnF5.Text = "F5";
			// 
			// btnF6
			// 
			this.btnF6.Text = "F6";
			// 
			// btnF12
			// 
			this.btnF12.Text = "F12";
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 527);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(743, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(732, 31);
			this.lblTitle.Text = "集計区分";
			// 
			// btnLMove
			// 
			this.btnLMove.BackColor = System.Drawing.Color.Silver;
			this.btnLMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnLMove.Location = new System.Drawing.Point(315, 229);
			this.btnLMove.Margin = new System.Windows.Forms.Padding(4);
			this.btnLMove.Name = "btnLMove";
			this.btnLMove.Size = new System.Drawing.Size(59, 52);
			this.btnLMove.TabIndex = 3;
			this.btnLMove.Text = "＜";
			this.btnLMove.UseVisualStyleBackColor = false;
			this.btnLMove.Click += new System.EventHandler(this.btnLMove_Click);
			// 
			// btnRMove
			// 
			this.btnRMove.BackColor = System.Drawing.Color.Silver;
			this.btnRMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnRMove.Location = new System.Drawing.Point(315, 153);
			this.btnRMove.Margin = new System.Windows.Forms.Padding(4);
			this.btnRMove.Name = "btnRMove";
			this.btnRMove.Size = new System.Drawing.Size(59, 52);
			this.btnRMove.TabIndex = 2;
			this.btnRMove.Text = "＞";
			this.btnRMove.UseVisualStyleBackColor = false;
			this.btnRMove.Click += new System.EventHandler(this.btnRMove_Click);
			// 
			// lblTitleCd
			// 
			this.lblTitleCd.BackColor = System.Drawing.Color.Silver;
			this.lblTitleCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTitleCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTitleCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTitleCd.ForeColor = System.Drawing.Color.Black;
			this.lblTitleCd.Location = new System.Drawing.Point(0, 0);
			this.lblTitleCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTitleCd.Name = "lblTitleCd";
			this.lblTitleCd.Size = new System.Drawing.Size(695, 40);
			this.lblTitleCd.TabIndex = 3;
			this.lblTitleCd.Tag = "CHANGE";
			this.lblTitleCd.Text = "タイトル";
			this.lblTitleCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShukeiCd
			// 
			this.txtShukeiCd.AutoSizeFromLength = true;
			this.txtShukeiCd.DisplayLength = null;
			this.txtShukeiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShukeiCd.Location = new System.Drawing.Point(74, 8);
			this.txtShukeiCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtShukeiCd.MaxLength = 4;
			this.txtShukeiCd.Name = "txtShukeiCd";
			this.txtShukeiCd.Size = new System.Drawing.Size(63, 23);
			this.txtShukeiCd.TabIndex = 0;
			this.txtShukeiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShukeiCd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtShukeiCd_KeyUp);
			this.txtShukeiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShukeiCd_Validating);
			// 
			// lblShukeiNm
			// 
			this.lblShukeiNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblShukeiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShukeiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShukeiNm.ForeColor = System.Drawing.Color.Black;
			this.lblShukeiNm.Location = new System.Drawing.Point(137, 7);
			this.lblShukeiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShukeiNm.Name = "lblShukeiNm";
			this.lblShukeiNm.Size = new System.Drawing.Size(264, 24);
			this.lblShukeiNm.TabIndex = 903;
			this.lblShukeiNm.Tag = "DISPNAME";
			this.lblShukeiNm.Text = "タイトル";
			this.lblShukeiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 1);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(12, 34);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(703, 469);
			this.fsiTableLayoutPanel1.TabIndex = 904;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtShukeiCd);
			this.fsiPanel1.Controls.Add(this.lblShukeiNm);
			this.fsiPanel1.Controls.Add(this.lblTitleCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(695, 40);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.btnLMove);
			this.fsiPanel3.Controls.Add(this.fsiPanel4);
			this.fsiPanel3.Controls.Add(this.btnRMove);
			this.fsiPanel3.Controls.Add(this.fsiPanel2);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 51);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(695, 414);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lbxSentakuKomoku);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Right;
			this.fsiPanel4.Location = new System.Drawing.Point(442, 0);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(253, 414);
			this.fsiPanel4.TabIndex = 2;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// lbxSentakuKomoku
			// 
			this.lbxSentakuKomoku.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbxSentakuKomoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lbxSentakuKomoku.FormattingEnabled = true;
			this.lbxSentakuKomoku.ItemHeight = 16;
			this.lbxSentakuKomoku.Location = new System.Drawing.Point(0, 0);
			this.lbxSentakuKomoku.Margin = new System.Windows.Forms.Padding(4);
			this.lbxSentakuKomoku.Name = "lbxSentakuKomoku";
			this.lbxSentakuKomoku.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
			this.lbxSentakuKomoku.Size = new System.Drawing.Size(253, 414);
			this.lbxSentakuKomoku.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lbxShukeiKubun);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Left;
			this.fsiPanel2.Location = new System.Drawing.Point(0, 0);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(253, 414);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// lbxShukeiKubun
			// 
			this.lbxShukeiKubun.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbxShukeiKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lbxShukeiKubun.FormattingEnabled = true;
			this.lbxShukeiKubun.ItemHeight = 16;
			this.lbxShukeiKubun.Location = new System.Drawing.Point(0, 0);
			this.lbxShukeiKubun.Margin = new System.Windows.Forms.Padding(4);
			this.lbxShukeiKubun.Name = "lbxShukeiKubun";
			this.lbxShukeiKubun.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
			this.lbxShukeiKubun.Size = new System.Drawing.Size(253, 414);
			this.lbxShukeiKubun.TabIndex = 1;
			// 
			// HNMR1092
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(732, 664);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNMR1092";
			this.ShowFButton = true;
			this.Text = "項目設定";
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnLMove;
        private System.Windows.Forms.Button btnRMove;
        private System.Windows.Forms.Label lblTitleCd;
        private common.controls.FsiTextBox txtShukeiCd;
        private System.Windows.Forms.Label lblShukeiNm;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel4;
        private System.Windows.Forms.ListBox lbxSentakuKomoku;
        private System.Windows.Forms.ListBox lbxShukeiKubun;
    }
}