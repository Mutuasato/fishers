﻿using System;
using System.Data;
using System.Drawing;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1091
{
    /// <summary>
    /// HNMR1091R 地区別個人別魚類別漁獲高月報の帳票  ※実は累計出力！
    /// </summary>
    public partial class HNMR1091R : BaseReport
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        public HNMR1091R(DataTable tgtData)
            : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

    }
}
