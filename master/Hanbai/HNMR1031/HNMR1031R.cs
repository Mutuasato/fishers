﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1031
{
    /// <summary>
    /// HNMR1031R の概要の説明です。
    /// </summary>
    public partial class HNMR1031R : BaseReport
    {

        public HNMR1031R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            ////和暦でDataTimeを文字列に変換する
            //System.Globalization.CultureInfo ci =
            //    new System.Globalization.CultureInfo("ja-JP", false);
            //ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            //this.txtToday.Text = DateTime.Now.ToString("gy年MM月dd日", ci);
        }

        /// <summary>
        /// ページフッターの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportFooter1_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            this.txtTotalMizuageSr.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalMizuageSr.Value),2);

        }
    }
}
