﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hnsb1011
{

    /// <summary>
    /// 精算処理(HNSB1011)
    /// </summary>
    public partial class HNSB1011 : BasePgForm
    {
        #region 構造体
        /// <summary>
        /// 数量・金額情報
        /// </summary>
        private struct Summary
        {
            public decimal KingakuKei;
            public decimal PayaoKei;
            public decimal[] KojoKoumoku;

            /// <summary>
            /// 金額をクリア
            /// </summary>
            public void Clear()
            {
                KingakuKei = 0;
                PayaoKei = 0;
                KojoKoumoku = new decimal[0];
            }
        }
        private Dictionary<decimal, decimal> PayaoKei;
        #endregion

        #region 定数
        /// <summary>
        /// 精算区分(地区内ｾﾘ)
        /// </summary>
        private const string CHIKUNAI = "3";

        /// <summary>
        /// 精算区分(浜売り)
        /// </summary>
        private const string HAMA_URI = "2";

        /// <summary>
        /// 精算区分(地区外ｾﾘ)
        /// </summary>
        private const string CHIKUGAI = "1";

        ///// <summary>
        ///// 保管料率
        ///// </summary>
        //private const string HOKAN_RYO_RITSU = "1.5";
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSB1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            //ログイン担当者が本所の登録でないなら変更不可
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1");

            // 精算日
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblGengo.Text = jpDate[0];
            txtYear.Text = jpDate[2];
            txtMonth.Text = jpDate[3];
            txtDay.Text = jpDate[4];

            // 精算区分初期値を取得
            string seisanKbn = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "Setting", "SeisanKbn"));
            this.txtSeisanKbn.Text = seisanKbn;
            IsValidSeisanKbn();
            // 初期フォーカス
            txtYear.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付,船主CDに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                case "txtSeisanKbn":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                                    this.txtMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDay.Text) > lastDayInMonth)
                                {
                                    this.txtDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblGengo.Text,
                                        this.txtYear.Text,
                                        this.txtMonth.Text,
                                        this.txtDay.Text,
                                        this.Dba);
                                this.lblGengo.Text = arrJpDate[0];
                                this.txtYear.Text = arrJpDate[2];
                                this.txtMonth.Text = arrJpDate[3];
                                this.txtDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
                case "txtSeisanKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtSeisanKbn.Text = outData[0];
                                this.lblSeisanKbnNm.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 会計年度のチェック
            if (!CheckKaikeiNendo())
            {
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 更新処理
            SearchData();
        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF9();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF9()
        {
            // 精算未処理一覧画面を起動
            HNSB1012 frmHNSB1012 = new HNSB1012();
            frmHNSB1012.InData = txtMizuageShishoCd.Text;

            DialogResult result = frmHNSB1012.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // 船主コード,船主名をセット
                string[] outData = (string[])frmHNSB1012.OutData;
                this.txtFunanushiCdFr.Text = outData[0];
                this.lblFunanushiCdFr.Text = outData[1];
                this.txtFunanushiCdTo.Text = outData[0];
                this.lblFunanushiCdTo.Text = outData[1];

                // 精算日をセット
                DateTime seribi = Util.ToDate(outData[2]);
                string[] hyoziSeribi = Util.ConvJpDate(seribi, this.Dba);
                this.lblGengo.Text = hyoziSeribi[0];
                this.txtYear.Text = hyoziSeribi[2];
                this.txtMonth.Text = hyoziSeribi[3];
                this.txtDay.Text = hyoziSeribi[4];

                // 精算区分をセット
                this.txtSeisanKbn.Text = outData[3];
                IsValidSeisanKbn();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYear.Text, this.txtYear.MaxLength))
            {
                e.Cancel = true;
                this.txtYear.SelectAll();
            }
            else
            {
                this.txtYear.Text = Util.ToString(IsValid.SetYear(this.txtYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtMonth.SelectAll();
            }
            else
            {
                this.txtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtMonth.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDay.SelectAll();
            }
            else
            {
                this.txtDay.Text = Util.ToString(IsValid.SetDay(this.txtDay.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 精算区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", this.txtMizuageShishoCd.Text, this.txtSeisanKbn.Text);
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            string shishoCd = this.txtMizuageShishoCd.Text;
            this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", shishoCd, this.txtFunanushiCdFr.Text);

            if (ValChk.IsEmpty(this.lblFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先  頭";
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            string shishoCd = this.txtMizuageShishoCd.Text;
            this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", shishoCd, this.txtFunanushiCdTo.Text);

            if (ValChk.IsEmpty(this.lblFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最  後";
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 会計年度のチェック
                if (!CheckKaikeiNendo())
                {
                    return;
                }

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                // 更新処理
                SearchData();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合、精算処理は支所指定を必須とする
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 会計年度のチェック
        /// </summary>
        private bool CheckKaikeiNendo()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return false;
            }

            // 入力日付に該当する会計年度が選択会計年度と一するかチェック
            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return false;
            }

            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("データ連携（更新）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDay.Text) > lastDayInMonth)
            {
                this.txtDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先  頭";
            }
            else
            {
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    string shishoCd = this.txtMizuageShishoCd.Text;
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", shishoCd, this.txtFunanushiCdFr.Text);
                }
            }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最  後";
            }
            else
            {
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    string shishoCd = this.txtMizuageShishoCd.Text;
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", shishoCd, this.txtFunanushiCdTo.Text);
                }
            }

            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        private bool IsValidSeisanKbn()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtSeisanKbn.Text, this.txtSeisanKbn.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text) || this.txtSeisanKbn.Text == "0")
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                Msg.Notice("精算区分は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", this.txtMizuageShishoCd.Text, this.txtSeisanKbn.Text);
                if (ValChk.IsEmpty(this.lblSeisanKbnNm.Text))
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 日付範囲(年)の入力チェック
            if (!IsValid.IsYear(this.txtYear.Text, this.txtYear.MaxLength))
            {
                this.txtYear.Focus();
                this.txtYear.SelectAll();
                return false;
            }
            // 日付範囲(月)の入力チェック
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                this.txtMonth.Focus();
                this.txtMonth.SelectAll();
                return false;
            }
            // 日付範囲(日)の入力チェック
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                this.txtDay.Focus();
                this.txtDay.SelectAll();
                return false;
            }
            // 日付範囲月末入力チェック処理
            CheckJp();
            // 日付範囲正しい和暦への変換処理
            SetJp();

            // 精算区分の入力チェック
            if (!IsValidSeisanKbn())
            {
                this.txtSeisanKbn.Focus();
                this.txtSeisanKbn.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.InData = this.txtMizuageShishoCd.Text;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        //private String[] openSearchWindow(String nameSpace, String moduleName, String para1, String indata)
        //{
        //    string[] result = { "", "" };

        //    // ネームスペースに使うモジュール名の小文字
        //    string lowerModuleName = moduleName.ToLower();

        //    // アセンブリのロード
        //    Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

        //    // フォーム作成
        //    string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
        //    Type t = asm.GetType(moduleNameSpace);

        //    if (t != null)
        //    {
        //        Object obj = Activator.CreateInstance(t);
        //        if (obj != null)
        //        {
        //            BasePgForm frm = (BasePgForm)obj;
        //            frm.Par1 = para1;
        //            frm.InData = indata;
        //            frm.ShowDialog(this);

        //            if (frm.DialogResult == DialogResult.OK)
        //            {
        //                string[] ret = (string[])frm.OutData;
        //                result[0] = ret[0];
        //                result[1] = ret[1];
        //                return result;
        //            }
        //        }
        //    }

        //    return result;
        //}

        /// <summary>
        /// データを検索し、更新する
        /// </summary>
        private void SearchData()
        {
            #region データ取得準備
            // 日付を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            // 精算区分設定
            string seisanKubunSettei = this.txtSeisanKbn.Text;
            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "9999";
            }
            //支所コード取得
            string shishoCd = this.txtMizuageShishoCd.Text;
            //漁業手数料、パヤオ手数料、荷受人の金額端数処理をコンフィグから取得
            decimal GyogyoTesuryo = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "Hasusyori", "GyogyoTesuryo"));
            decimal PayaoTesuryo = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "Hasusyori", "PayaoTesuryo"));
            decimal NiukeTesuryo = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "Hasusyori", "NiukeTesuryo"));
            decimal Hasusyori = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Han, this.ProductName, "Hasusyori", "Hasusyori"));
            #endregion

            #region データを取得
            // データ件数を取得
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 検索する日付をセット
            dpc.SetParam("@SERIBI", SqlDbType.DateTime, tmpDate);
            // 検索する船主コードをセット
            dpc.SetParam("@SENSHU_CD_FR", SqlDbType.VarChar, 6, funanushiCdFr);
            dpc.SetParam("@SENSHU_CD_TO", SqlDbType.VarChar, 6, funanushiCdTo);
            // 支所コードをセット
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(shishoCd));    //Y.TAKADA ADD 2017/10/03
            // 検索する精算区分をセット
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKubunSettei);

            string cols = "COUNT(*) AS KENSU";

            string from = "TB_HN_SHIKIRI_DATA AS A";
            from += " LEFT OUTER JOIN";
            from += "  VI_HN_TORIHIKISAKI_JOHO AS B";
            from += "  ON A.KAISHA_CD = B.KAISHA_CD";

            string where = "A.KAISHA_CD = @KAISHA_CD AND";
            where += "  B.TORIHIKISAKI_KUBUN1 = 1 AND";
            where += "  A.SENSHU_CD = B.TORIHIKISAKI_CD AND";
            where += "  A.SERIBI = @SERIBI AND";
            where += "  A.SEISAN_KUBUN = @SEISAN_KUBUN  AND";
            where += "  A.SENSHU_CD >= @SENSHU_CD_FR  AND";
            where += "  A.SENSHU_CD <= @SENSHU_CD_TO　AND ";
            where += "  A.SHISHO_CD = @SHISHO_CD ";

            DataTable dtKensu = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            #endregion

            if (Util.ToInt(dtKensu.Rows[0]["KENSU"]) == 0)
            {
                Msg.Info("該当データがありません。");
                return;
            }
            if (Msg.ConfYesNo("登録しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                cols = "* ";
                from = "TB_HN_KOJO_DATA ";
                where = "KAISHA_CD = @KAISHA_CD AND ";
                where += "SEISAN_BI = @SERIBI  AND ";
                where += "SEISAN_KUBUN = @SEISAN_KUBUN  AND ";
                where += "SENSHU_CD >= @SENSHU_CD_FR  AND ";
                where += "SENSHU_CD <= @SENSHU_CD_TO ";
                where += " AND SHISHO_CD = @SHISHO_CD ";
                DataTable dtKojo = this.Dba.GetDataTableByConditionWithParams(cols, from, where, "SEISAN_KUBUN", dpc);

                StringBuilder sql;

                #region "精算済みの場合の処理"
                if (dtKojo.Rows.Count > 0)
                {
                    Msg.Notice("既に精算済みです。");
                    if (Msg.ConfYesNo("指定された精算日の控除データを削除します。よろしいですか？") == DialogResult.No)
                    {
                        return;
                    }

                    #region 控除データ削除処理
                    dpc = new DbParamCollection();
                    sql = new StringBuilder();
                    sql.Append("DELETE ");
                    sql.Append("  FROM TB_HN_KOJO_DATA");
                    sql.Append("  WHERE");
                    sql.Append("   KAISHA_CD = @KAISHA_CD AND");
                    sql.Append("   SHISHO_CD = @SHISHO_CD AND");  //Y.TAKADA ADD 2017/10/03
                    sql.Append("   SEISAN_BI = @SERIBI AND");
                    sql.Append("   SEISAN_KUBUN = @SEISAN_KUBUN AND");
                    sql.Append("   SENSHU_CD >= @SENSHU_CD_FR AND");
                    sql.Append("   SENSHU_CD <= @SENSHU_CD_TO");
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    // 検索する日付をセット
                    dpc.SetParam("@SERIBI", SqlDbType.DateTime, tmpDate);
                    // 検索する船主コードをセット
                    dpc.SetParam("@SENSHU_CD_FR", SqlDbType.VarChar, 6, funanushiCdFr);
                    dpc.SetParam("@SENSHU_CD_TO", SqlDbType.VarChar, 6, funanushiCdTo);
                    // 検索する支所コードをセット
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, shishoCd);   //Y.TAKADA ADD 2017/10/03
                    // 検索する精算区分をセット
                    dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKubunSettei);

                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion
                }
                #endregion

                // アップデート,インサート処理
                #region メインデータを取得   
                dpc = new DbParamCollection();
                sql = new StringBuilder();
                sql.Append(" SELECT ");
                sql.Append(" A.*, ");
                sql.Append("     B.KINGAKU_HASU_SHORI,");
                sql.Append("     B.SHOHIZEI_TENKA_HOHO,");
                sql.Append("     B.SHOHIZEI_HASU_SHORI,");
                sql.Append("     B.TSUMITATE_RITSU,");
                sql.Append("     B.AZUKARIKIN_TSUMITATE_RITSU,");
                sql.Append("     B.HONNIN_TSUMITATE_RITSU");
                sql.Append(" FROM");
                sql.Append("     VI_HN_SHIKIRI_MEISAI AS A ");
                sql.Append(" LEFT OUTER JOIN VI_HN_FUNANUSHI_JOHO AS B   ");
                sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD  AND ");
                sql.Append("     A.SENSHU_CD = B.TORIHIKISAKI_CD");
                sql.Append(" WHERE");
                sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");    //Y.TAKADA ADD 2017/10/03
                sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");    //Y.TAKADA ADD 2017/10/03
                sql.Append("     A.SERIBI = @SERIBI  AND");
                sql.Append("     A.SENSHU_CD <> 0  AND");
                sql.Append("     A.SEISAN_KUBUN = @SEISAN_KUBUN  AND");
                sql.Append("     A.SENSHU_CD >= @SENSHU_CD_FR  AND");
                sql.Append("     A.SENSHU_CD <= @SENSHU_CD_TO");
                sql.Append(" ORDER BY");
                sql.Append("     A.KAISHA_CD,");
                sql.Append("     A.SHISHO_CD,");
                sql.Append("     A.SENSHU_CD,");
                sql.Append("     A.SEISAN_KUBUN,");
                sql.Append("     A.GYOGYO_TESURYO,");
                sql.Append("     A.SERIBI,");
                sql.Append("     A.SEISAN_NO");
                // 検索する日付をセット
                dpc.SetParam("@SERIBI", SqlDbType.DateTime, tmpDate);
                // 検索する船主コードをセット
                dpc.SetParam("@SENSHU_CD_FR", SqlDbType.VarChar, 6, funanushiCdFr);
                dpc.SetParam("@SENSHU_CD_TO", SqlDbType.VarChar, 6, funanushiCdTo);
                // 検索する支所コードをセット
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);   //Y.TAKADA ADD 2017/10/03
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(shishoCd));   //Y.TAKADA ADD 2017/10/03
                // 検索する精算区分をセット
                dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKubunSettei);

                DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                #endregion

                #region 準備
                // 仕切データ更新のための文字列
                string denpyoBango = "";
                int count = 0;
                int i = 1;
                // 精算番号
                string seisanNo = "";
                decimal torihikiKubun1 = 0;
                decimal torihikiKubun2 = 0;
                DateTime seribi = DateTime.Today;
                decimal seisanKubun = 0;
                string senshuCd = "0";
                string senshuNm = "";
                decimal gyogyoTesuryo = 0;    // 漁業手数料
                decimal tsumitateRitsu = 0;    // 積立率
                decimal azukarikinTsumitateRitsu = 0;    // 預り金積立率
                decimal honninTsumitateRitsu = 0; // 本人積立率
                decimal kingakuHasuShori = 0; // 金額端数処理
                decimal shohizeiHasuShori = 0; // 消費税端数処理
                decimal shohizeiTenkaHoho = 0; // 消費税転嫁方法
                decimal MeisaiKingaku = 0;
                decimal KojoGokei = 0;
                decimal SashihikiSeisan = 0;

                // 合計金額,控除金額
                Summary sum = new Summary();
                sum.Clear();
                //パヤオ手数料の計算辞書
                PayaoKei = new Dictionary<decimal, decimal>();

                int cnt = 0;
                #endregion
                this.progressBar1.Maximum = dtMainLoop.Rows.Count;
                this.progressBar1.Value = 0;
                this.progressBar1.Visible = true;

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    #region インサートするための情報の保持
                    seisanNo = Util.ToString(dr["SEISAN_NO"]);
                    torihikiKubun1 = Util.ToDecimal(dr["TORIHIKI_KUBUN1"]);
                    torihikiKubun2 = Util.ToDecimal(dr["TORIHIKI_KUBUN2"]);
                    seribi = Util.ToDate(dr["SERIBI"]);
                    seisanKubun = Util.ToDecimal(dr["SEISAN_KUBUN"]);
                    senshuNm = Util.ToString(dr["SENSHU_NM"]);
                    // 漁業手数料
                    gyogyoTesuryo = Util.ToDecimal(dr["GYOGYO_TESURYO"]);
                    // 積立率
                    tsumitateRitsu = Util.ToDecimal(dr["TSUMITATE_RITSU"]);
                    // 預り金積立率
                    azukarikinTsumitateRitsu = Util.ToDecimal(dr["AZUKARIKIN_TSUMITATE_RITSU"]);
                    // 本人積立率
                    honninTsumitateRitsu = Util.ToDecimal(dr["HONNIN_TSUMITATE_RITSU"]);
                    // 金額端数処理
                    kingakuHasuShori = Util.ToDecimal(dr["KINGAKU_HASU_SHORI"]);
                    // 消費税転嫁方法
                    shohizeiTenkaHoho = Util.ToDecimal(dr["SHOHIZEI_TENKA_HOHO"]);
                    // 消費税端数処理
                    shohizeiHasuShori = Util.ToDecimal(dr["SHOHIZEI_HASU_SHORI"]);
                    // 金額の計算
                    if (Util.ToDecimal(dr["SHOHIZEI"]) == 0)
                    {
                        MeisaiKingaku = Util.ToDecimal(dr["KINGAKU"]) + Util.ToDecimal(dr["GOKEI_SHOHIZEI"]);
                    }
                    else
                    {
                        MeisaiKingaku = Util.ToDecimal(dr["KINGAKU"]) + Util.ToDecimal(dr["SHOHIZEI"]);
                    }
                    //パヤオ手数料計算金額
                    if (Util.ToDecimal(dr["PAYAO_RITSU"]) != 0)
                    {
                        decimal PayaoRitu = Util.ToDecimal(dr["PAYAO_RITSU"]);
                        if (PayaoKei.ContainsKey(PayaoRitu) == true)
                        {
                            PayaoKei[PayaoRitu] += MeisaiKingaku;
                        }
                        else
                        {
                            PayaoKei.Add(PayaoRitu, MeisaiKingaku);
                        }
                    }
                    sum.KingakuKei += MeisaiKingaku;

                    #region "消費税関連 コメントアウト"
                    ////消費税率の取得
                    //decimal Zeiritu = TaxUtil.GetTaxRate(seribi, hakozeikbn, this.Dba);
                    //Zeiritu = 1 + Zeiritu / 100;
                    //箱代の計算
                    #region "箱代計算変更前"
                    //// 消費税転嫁方法が明細の場合
                    //if (shohizeiTenkaHoho == 1)
                    //{
                    //    // 箱代
                    //    // 消費税端数処理が切り捨ての場合
                    //    if (shohizeiHasuShori == 1)
                    //    {
                    //        //sum.Hakodai += Math.Truncate(Util.ToDecimal(dr["HAKOSU"]) * hakodai * hakozei);
                    //        sum.Hakodai += Math.Truncate(Util.ToDecimal(dr["HAKOSU"]) * hakodai * Zeiritu);
                    //    }
                    //    // 消費税端数処理が四捨五入の場合
                    //    else if (shohizeiHasuShori == 2)
                    //    {
                    //        //sum.Hakodai += Util.Round(Util.ToDecimal(dr["HAKOSU"]) * hakodai * hakozei, 0);
                    //        sum.Hakodai += Util.Round(Util.ToDecimal(dr["HAKOSU"]) * hakodai * Zeiritu, 0);
                    //    }
                    //    // 消費税端数処理が切り上げの場合
                    //    else
                    //    {
                    //        //sum.Hakodai += Math.Ceiling(Util.ToDecimal(dr["HAKOSU"]) * hakodai * hakozei);
                    //        sum.Hakodai += Math.Ceiling(Util.ToDecimal(dr["HAKOSU"]) * hakodai * Zeiritu);
                    //    }
                    //}
                    //else
                    //{
                    //    sum.KojoKomoku2 += Math.Ceiling(Util.ToDecimal(dr["HAKOSU"]) * hakodai);
                    //}
                    #endregion
                    // 箱代
                    ////消費税が外税の場合には税額を求めて足しこむ
                    //if (hakozeiinpkbn == 2)
                    //{
                    //    // 消費税端数処理が切り捨ての場合
                    //    if (shohizeiHasuShori == 1)
                    //    {
                    //        sum.Hakodai += Math.Truncate(Util.ToDecimal(dr["HAKOSU"]) * hakodai * Zeiritu);
                    //    }
                    //    // 消費税端数処理が四捨五入の場合
                    //    else if (shohizeiHasuShori == 2)
                    //    {
                    //        sum.Hakodai += Util.Round(Util.ToDecimal(dr["HAKOSU"]) * hakodai * Zeiritu, 0);
                    //    }
                    //    // 消費税端数処理が切り上げの場合
                    //    else
                    //    {
                    //        sum.Hakodai += Math.Ceiling(Util.ToDecimal(dr["HAKOSU"]) * hakodai * Zeiritu);
                    //    }
                    //}
                    //else
                    //{
                    //    //外税以外はそのまま計算
                    //    sum.Hakodai += Util.ToDecimal(dr["HAKOSU"]) * hakodai;
                    //}
                    #endregion

                    // 現在の船主コードを保持
                    senshuCd = Util.ToString(dr["SENSHU_CD"]);
                    cnt++;

                    #region ループの最後 又は 次回の船主が今回の船主と一致しない 又は　精算区分が変わった場合
                    if (cnt == dtMainLoop.Rows.Count || !senshuCd.Equals(Util.ToString(dtMainLoop.Rows[cnt]["SENSHU_CD"])) ||
                        !seisanKubun.Equals(Util.ToDecimal(Util.ToString(dtMainLoop.Rows[cnt]["SEISAN_KUBUN"]))))
                    {
                        #region 伝票番号の取得
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append(" SELECT");
                        sql.Append("     KAIKEI_NENDO,");
                        sql.Append("     DENPYO_KUBUN1,");
                        sql.Append("     DENPYO_KUBUN2,");
                        sql.Append("     DENPYO_BANGO,");
                        sql.Append("     BANGO_ZOBUN,");
                        sql.Append("     BANGO_SAISHOCHI,");
                        sql.Append("     BANGO_SAIDAICHI");
                        sql.Append(" FROM");
                        sql.Append("     TB_HN_F_DENPYO_BANGO");
                        sql.Append(" WHERE");
                        sql.Append("     KAISHA_CD = @KAISHA_CD AND");
                        sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO AND");
                        sql.Append("     DENPYO_KUBUN1 = 4 AND");
                        sql.Append("     DENPYO_KUBUN2 = 0");
                        sql.Append("     AND SHISHO_CD = @SHISHO_CD");

                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(shishoCd));

                        DataTable dtDenpyoBango = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        #endregion

                        #region 伝票番号の更新
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append(" UPDATE");
                        sql.Append("     TB_HN_F_DENPYO_BANGO");
                        sql.Append(" SET");
                        sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO,");
                        sql.Append("     DENPYO_BANGO = @DENPYO_BANGO,");
                        sql.Append("     UPDATE_DATE = @UPDATE_DATE");
                        sql.Append(" WHERE");
                        sql.Append("     KAISHA_CD = @KAISHA_CD AND");
                        sql.Append("     DENPYO_KUBUN1 = 4 AND");
                        sql.Append("     DENPYO_KUBUN2 = 0 AND");
                        sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO");
                        sql.Append("     AND SHISHO_CD = @SHISHO_CD");

                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(dtDenpyoBango.Rows[0]["DENPYO_BANGO"]) + 1);
                        dpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Today);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(shishoCd));

                        this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                        #endregion

                        #region "控除項目設定の取得"
                        DbParamCollection dpk = new DbParamCollection();
                        StringBuilder sqlk = new StringBuilder();
                        sqlk.Append(" SELECT * ");
                        sqlk.Append(" FROM");
                        sqlk.Append("     VI_HN_KOJO_KAMOKU_SETTEI");
                        sqlk.Append(" WHERE");
                        sqlk.Append("     KAISHA_CD = @KAISHA_CD AND");
                        sqlk.Append("     SHISHO_CD = @SHISHO_CD AND");
                        sqlk.Append("     KAIKEI_NENDO = @KAIKEI_NENDO");
                        sqlk.Append(" ORDER BY");
                        sqlk.Append("     SETTEI_CD");
                        dpk.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, UInfo.KaishaCd);
                        dpk.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, UInfo.KaikeiNendo);
                        dpk.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                        DataTable KojoKoumokuDt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sqlk), dpk);
                        Array.Resize(ref sum.KojoKoumoku, KojoKoumokuDt.Rows.Count);
                        #endregion

                        #region TB_HN_KOJO_DATAテーブルへのインサート
                        #region INSERTSQL
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append(" INSERT INTO TB_HN_KOJO_DATA (");
                        sql.Append(" KAISHA_CD,");
                        sql.Append(" SHISHO_CD,");             //Y.TAKADA ADD 2017/10/03
                        sql.Append(" TORIHIKI_KUBUN1,");
                        sql.Append(" TORIHIKI_KUBUN2,");
                        sql.Append(" SEISAN_BI,");
                        sql.Append(" SEISAN_SERI_KAISHI_KIKAN,");
                        sql.Append(" SEISAN_SERI_SHURYO_KIKAN,");
                        sql.Append(" SEISAN_KUBUN,");
                        sql.Append(" SEISAN_BANGO,");
                        sql.Append(" KAIKEI_NENDO,");
                        sql.Append(" SENSHU_CD,");
                        sql.Append(" SENSHU_NM,");
                        sql.Append(" ZEIKOMI_MIZUAGE_KINGAKU,");
                        int idx = 1;
                        foreach (decimal kj in sum.KojoKoumoku)
                        {
                            string KomokuNm = "KOJO_KOMOKU" + idx.ToString();
                            sql.Append(KomokuNm + ",");
                            idx++;
                        }
                        sql.Append(" KOJO_GOKEIGAKU,");
                        sql.Append(" SASHIHIKI_SEISANGAKU,");
                        sql.Append(" REGIST_DATE,");
                        sql.Append(" UPDATE_DATE");
                        sql.Append(" )");
                        sql.Append(" VALUES (");
                        sql.Append(" @KAISHA_CD,");
                        sql.Append(" @SHISHO_CD,");         //Y.TAKADA ADD 2017/10/03
                        sql.Append(" @TORIHIKI_KUBUN1,");
                        sql.Append(" @TORIHIKI_KUBUN2,");
                        sql.Append(" @SEISAN_BI,");
                        sql.Append(" @SEISAN_BI,");
                        sql.Append(" @SEISAN_BI,");
                        sql.Append(" @SEISAN_KUBUN,");
                        sql.Append(" @SEISAN_BANGO,");
                        sql.Append(" @KAIKEI_NENDO,");
                        sql.Append(" @SENSHU_CD,");
                        sql.Append(" @SENSHU_NM,");
                        sql.Append(" @ZEIKOMI_MIZUAGE_KINGAKU,");
                        idx = 1;
                        foreach (decimal kj in sum.KojoKoumoku)
                        {
                            string KomokuNm = "@KOJO_KOMOKU" + idx.ToString();
                            sql.Append(KomokuNm + ",");
                            idx++;
                        }
                        sql.Append(" @KOJO_GOKEIGAKU,");
                        sql.Append(" @SASHIHIKI_SEISANGAKU,");
                        sql.Append(" @REGIST_DATE,");
                        sql.Append(" @UPDATE_DATE");
                        sql.Append(" )");
                        #endregion

                        #region 計算
                        #region "控除項目毎に計算を行う"
                        decimal[] kojoKomokuAry = new decimal[Util.ToInt(KojoKoumokuDt.Rows.Count)];
                        foreach (DataRow dk in KojoKoumokuDt.Rows)
                        {
                            //控除計算符号の退避
                            int fugo = Util.ToInt(dk["KEISAN_FUGO"]) == 1 ? -1 : 1;
                            //計算を行う控除科目なら計算する
                            if (Util.ToInt(dk["KEISAN_KBN"]) == 1)
                            {
                                decimal kingaku = 0;
                                decimal ritu = 0;
                                int zeiNyuryokuHoho = 0;
                                int ri = Util.ToInt(dk["SETTEI_CD"]) - 1;
                                decimal tank = 0;
                                // コンフィグ設定がゼロの場合は、船主の金額端数処理を使う
                                Hasusyori = (Hasusyori == 0) ? kingakuHasuShori : Hasusyori;
                                //対象精算区分の取得
                                string Kbn = Util.ToString(dk["SEISAN_KBN"]);
                                string[] seisan_kbn = Kbn.Split(',');
                                bool goCalc = false;
                                foreach (string seikbn in seisan_kbn)
                                {
                                    if (seikbn.Equals(Util.ToString(dr["SEISAN_KUBUN"])) || seikbn == "0")
                                    {
                                        goCalc = true;
                                        break;
                                    }
                                }
                                if (goCalc)
                                {
                                    string[] siki = Util.ToString(dk["Script"]).Split('.');
                                    foreach (string str in siki)
                                    {
                                        string para = str.Trim();
                                        switch (para)
                                        {
                                            case "MIZUAGE_ZEIKOMI_KINGAKU":
                                                //元の金額
                                                kingaku = sum.KingakuKei;//Util.ToDecimal(dr["MIZUAGE_ZEIKOMI_KINGAKU"]);
                                                break;

                                            case "HAKO":
                                                tank = Util.ToDecimal(dk["TANKA"]);
                                                zeiNyuryokuHoho = Util.ToInt(dk["SHOHIZEI_NYURYOKU_HOHO"]);
                                                kingaku = Util.ToDecimal(dr["HAKOSU"]) * tank;
                                                break;

                                            case "PAYAO_KINGAKU":
                                                //kingaku = sum.PayaoKei;
                                                foreach (KeyValuePair<decimal, decimal> dic in PayaoKei)
                                                {
                                                    //sum.PayaoKei += dic.Value * dic.Key / 100;
                                                    sum.PayaoKei += TaxUtil.CalcFraction(dic.Value * dic.Key / 100, 0, TaxUtil.ROUND_CATEGORY.ADJUST);
                                                }
                                                kingaku = sum.PayaoKei;
                                                Hasusyori = (PayaoTesuryo == 0) ? kingakuHasuShori : PayaoTesuryo;
                                                ritu = 1;
                                                break;

                                            case "ZEI_RITSU":
                                                ritu = TaxUtil.GetTaxRate(seribi, Util.ToDecimal(dk["ZEI_KUBUN"]), this.Dba);
                                                ritu = ritu / 100;
                                                break;

                                            case "GYOGYO_TESURYO":
                                                ritu = Util.ToDecimal(dr["GYOGYO_TESURYO"]) / 100;
                                                //Hasusyori = (GyogyoTesuryo == 0) ? kingakuHasuShori : GyogyoTesuryo;
                                                break;

                                            case "AZUKARIKIN_TSUMITATE_RITSU":
                                                ritu = azukarikinTsumitateRitsu / 100;
                                                break;

                                            case "HONNIN_TSUMITATE_RITSU":
                                                ritu = Util.ToDecimal(dr["HONNIN_TSUMITATE_RITSU"]) / 100;
                                                break;

                                            case "NIUKENIN_RITSU":
                                                ritu = Util.ToDecimal(dr["NIUKENIN_RITSU"]) / 100;
                                                //Hasusyori = (NiukeTesuryo == 0) ? kingakuHasuShori : NiukeTesuryo;
                                                break;

                                            case "PAYAO_RITSU":
                                                ritu = Util.ToDecimal(dr["PAYAO_RITSU"]) / 100;
                                                //Hasusyori = (PayaoTesuryo == 0) ? kingakuHasuShori : PayaoTesuryo;
                                                break;

                                            case "HOKAN_RYO_RITSU":
                                                ritu = Util.ToDecimal(dk["TANKA"]) / 100;
                                                break;

                                            case "TSUMITATE_RITSU":
                                                //ritu = Util.ToDecimal(dr["TSUMITATE_RITSU"]) / 100;
                                                ritu = tsumitateRitsu / 100;
                                                break;

                                            default:
                                                break;
                                        }
                                    }

                                    int hasu = 0;
                                    //消費税が外税の場合には税額を求めて足しこむ
                                    if (zeiNyuryokuHoho == 2 && tank != 0)
                                    {
                                        switch (int.Parse(shohizeiHasuShori.ToString()))
                                        {
                                            case 1:
                                                //kojoKomokuAry[ri] = fugo * (kingaku + Math.Truncate(kingaku * ritu));
                                                hasu = (int)TaxUtil.ROUND_CATEGORY.DOWN;
                                                break;
                                            case 2:
                                                //kojoKomokuAry[ri] = fugo * (kingaku + Math.Round(kingaku * ritu));
                                                hasu = (int)TaxUtil.ROUND_CATEGORY.ADJUST;
                                                break;
                                            case 3:
                                                //kojoKomokuAry[ri] = fugo * (kingaku + Math.Ceiling(kingaku * ritu));
                                                hasu = (int)TaxUtil.ROUND_CATEGORY.UP;
                                                break;
                                        }
                                        kojoKomokuAry[ri] = fugo * (kingaku + TaxUtil.CalcFraction(kingaku * ritu, 0, hasu));
                                    }
                                    else
                                    {

                                        //switch (kingakuHasuShori)
                                        switch (int.Parse(Hasusyori.ToString()))
                                        {
                                            case 1:
                                                //kojoKomokuAry[ri] = fugo * Math.Truncate(kingaku * ritu);
                                                hasu = (int)TaxUtil.ROUND_CATEGORY.DOWN;
                                                break;
                                            case 2:
                                                //kojoKomokuAry[ri] = fugo * Math.Round(kingaku * ritu);
                                                hasu = (int)TaxUtil.ROUND_CATEGORY.ADJUST;
                                                break;
                                            case 3:
                                                //kojoKomokuAry[ri] = fugo * Math.Ceiling(kingaku * ritu);
                                                hasu = (int)TaxUtil.ROUND_CATEGORY.UP;
                                                break;
                                        }
                                        kojoKomokuAry[ri] = fugo * TaxUtil.CalcFraction(kingaku * ritu, 0, hasu);

                                    }
                                    sum.KojoKoumoku[ri] += kojoKomokuAry[ri];
                                }
                            }

                        }
                        #endregion
                        #endregion
                        // 控除合計
                        foreach (decimal kj in sum.KojoKoumoku)
                        {
                            KojoGokei += kj;
                        }
                        // 差引支払額
                        SashihikiSeisan = sum.KingakuKei - KojoGokei;
                        #endregion

                        #region 登録パラメータの設定
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 5, torihikiKubun1);
                        dpc.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 5, torihikiKubun2);
                        dpc.SetParam("@SEISAN_BI", SqlDbType.DateTime, seribi);
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKubun);
                        dpc.SetParam("@SEISAN_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(dtDenpyoBango.Rows[0]["DENPYO_BANGO"]) + 1);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 5, senshuCd);
                        dpc.SetParam("@SENSHU_NM", SqlDbType.VarChar, 32, senshuNm);
                        dpc.SetParam("@ZEIKOMI_MIZUAGE_KINGAKU", SqlDbType.Decimal, 12, sum.KingakuKei);
                        idx = 1;
                        foreach (decimal kj in sum.KojoKoumoku)
                        {
                            string KomokuNm = "@KOJO_KOMOKU" + idx.ToString();
                            dpc.SetParam(KomokuNm, SqlDbType.Decimal, 12, kj);
                            idx++;
                        }
                        dpc.SetParam("@KOJO_GOKEIGAKU", SqlDbType.Decimal, 12, KojoGokei);
                        dpc.SetParam("@SASHIHIKI_SEISANGAKU", SqlDbType.Decimal, 12, SashihikiSeisan);
                        dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Today);
                        dpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Today);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd); //Y.TAKADA ADD
                        #endregion
                        this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                        #endregion

                        #region 仕切データを更新するための伝票番号取得
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append(" SELECT A.DENPYO_BANGO ");
                        sql.Append(" FROM VI_HN_SHIKIRI_MEISAI A ");
                        sql.Append(" WHERE");
                        sql.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
                        sql.Append(" A.SHISHO_CD = @SHISHO_CD AND ");
                        sql.Append(" A.SEISAN_KUBUN = @SEISAN_KUBUN AND ");
                        sql.Append(" A.SENSHU_CD = @SENSHU_CD AND ");
                        sql.Append(" A.SERIBI = @SERIBI AND ");
                        sql.Append(" A.GYO_NO = 1 ");

                        dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 5, senshuCd);
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKubun);
                        dpc.SetParam("@SERIBI", SqlDbType.DateTime, seribi);
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                        DataTable dtDenpyoBangoJunbi = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
                        #endregion

                        count = dtDenpyoBangoJunbi.Rows.Count;

                        foreach (DataRow dt in dtDenpyoBangoJunbi.Rows)
                        {
                            denpyoBango += Util.ToString(dt["DENPYO_BANGO"]);
                            if (count != i)
                            {
                                denpyoBango += ",";
                            }
                            i++;
                        }
                        i = 1;

                        #region 仕切データの更新
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append(" UPDATE");
                        sql.Append("     TB_HN_SHIKIRI_DATA");
                        sql.Append(" SET");
                        sql.Append("     SEISAN_NO = @SEISAN_NO,");
                        sql.Append("     KEIJO_NENGAPPI = @KEIJO_NENGAPPI,");
                        sql.Append("     UPDATE_DATE = @UPDATE_DATE");
                        sql.Append(" WHERE");
                        sql.Append("     KAISHA_CD = @KAISHA_CD AND");
                        sql.Append("     SENSHU_CD = @SENSHU_CD AND");
                        sql.Append("     SEISAN_KUBUN = @SEISAN_KUBUN AND");
                        sql.Append("     DENPYO_BANGO IN (" + denpyoBango + " ) AND");
                        sql.Append("     SERIBI = @SERIBI");

                        dpc.SetParam("@SEISAN_NO", SqlDbType.Decimal, 6, Util.ToDecimal(dtDenpyoBango.Rows[0]["DENPYO_BANGO"]) + 1);
                        dpc.SetParam("@KEIJO_NENGAPPI", SqlDbType.DateTime, seribi);
                        dpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Today);
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 5, senshuCd);
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKubun);
                        dpc.SetParam("@SERIBI", SqlDbType.DateTime, seribi);

                        this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                        #endregion

                        denpyoBango = "";

                        // 金額情報をクリア
                        KojoGokei = 0;
                        sum.Clear();
                        PayaoKei.Clear();

                    }
                    #endregion
                    this.progressBar1.Value += 1;
                }

                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("精算処理が正常に終了しました。");
                this.progressBar1.Visible = false;
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        private void getKojoKamokuSettei(DataTable KojoKoumokuDt, string shishoCd)
        {
            //控除項目設定の取得
            DbParamCollection dpk = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT * ");
            sql.Append(" FROM");
            sql.Append("     VI_HN_KOJO_KAMOKU_SETTEI");
            sql.Append(" WHERE");
            sql.Append("     KAISHA_CD = @KAISHA_CD AND");
            sql.Append("     SHISHO_CD = @SHISHO_CD AND");
            sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO");
            sql.Append(" ORDER BY");
            sql.Append("     SETTEI_CD");
            dpk.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, UInfo.KaishaCd);
            dpk.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, UInfo.KaikeiNendo);
            dpk.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            //DataTable KojoKoumokuDt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpk);
            KojoKoumokuDt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpk);

        }

        #endregion
    }
}
