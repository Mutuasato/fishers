﻿namespace jp.co.fsi.hn.hncr1011
{
    partial class HNCR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblFunanushiCdTo = new System.Windows.Forms.Label();
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiCdFr = new System.Windows.Forms.Label();
			this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeijunKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeijunKubunNm = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.lblKbn = new System.Windows.Forms.Label();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF2
			// 
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Visible = false;
			// 
			// btnF8
			// 
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Visible = false;
			// 
			// btnF12
			// 
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(9, 812);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1155, 132);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "取引先マスタ一覧";
			// 
			// lblFunanushiCdTo
			// 
			this.lblFunanushiCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
			this.lblFunanushiCdTo.Location = new System.Drawing.Point(567, 1);
			this.lblFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
			this.lblFunanushiCdTo.Size = new System.Drawing.Size(289, 24);
			this.lblFunanushiCdTo.TabIndex = 4;
			this.lblFunanushiCdTo.Tag = "DISPNAME";
			this.lblFunanushiCdTo.Text = "最　後";
			this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBet.ForeColor = System.Drawing.Color.Black;
			this.lblCodeBet.Location = new System.Drawing.Point(476, 3);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 24);
			this.lblCodeBet.TabIndex = 2;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdFr
			// 
			this.txtFunanushiCdFr.AutoSizeFromLength = false;
			this.txtFunanushiCdFr.DisplayLength = null;
			this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
			this.txtFunanushiCdFr.Location = new System.Drawing.Point(117, 2);
			this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtFunanushiCdFr.MaxLength = 4;
			this.txtFunanushiCdFr.MinimumSize = new System.Drawing.Size(55, 0);
			this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
			this.txtFunanushiCdFr.Size = new System.Drawing.Size(55, 23);
			this.txtFunanushiCdFr.TabIndex = 0;
			this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
			// 
			// lblFunanushiCdFr
			// 
			this.lblFunanushiCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
			this.lblFunanushiCdFr.Location = new System.Drawing.Point(179, 2);
			this.lblFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
			this.lblFunanushiCdFr.Size = new System.Drawing.Size(289, 24);
			this.lblFunanushiCdFr.TabIndex = 1;
			this.lblFunanushiCdFr.Tag = "DISPNAME";
			this.lblFunanushiCdFr.Text = "先　頭";
			this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdTo
			// 
			this.txtFunanushiCdTo.AutoSizeFromLength = false;
			this.txtFunanushiCdTo.DisplayLength = null;
			this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
			this.txtFunanushiCdTo.Location = new System.Drawing.Point(509, 2);
			this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtFunanushiCdTo.MaxLength = 4;
			this.txtFunanushiCdTo.MinimumSize = new System.Drawing.Size(55, 0);
			this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
			this.txtFunanushiCdTo.Size = new System.Drawing.Size(55, 23);
			this.txtFunanushiCdTo.TabIndex = 3;
			this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
			this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
			// 
			// txtSeijunKubunCd
			// 
			this.txtSeijunKubunCd.AutoSizeFromLength = false;
			this.txtSeijunKubunCd.DisplayLength = null;
			this.txtSeijunKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeijunKubunCd.ForeColor = System.Drawing.Color.Black;
			this.txtSeijunKubunCd.Location = new System.Drawing.Point(120, 3);
			this.txtSeijunKubunCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtSeijunKubunCd.MaxLength = 4;
			this.txtSeijunKubunCd.MinimumSize = new System.Drawing.Size(55, 23);
			this.txtSeijunKubunCd.Name = "txtSeijunKubunCd";
			this.txtSeijunKubunCd.Size = new System.Drawing.Size(55, 23);
			this.txtSeijunKubunCd.TabIndex = 0;
			this.txtSeijunKubunCd.Text = "0";
			this.txtSeijunKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeijunKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeijunKubunCd_Validating);
			// 
			// lblSeijunKubunNm
			// 
			this.lblSeijunKubunNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeijunKubunNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeijunKubunNm.ForeColor = System.Drawing.Color.Black;
			this.lblSeijunKubunNm.Location = new System.Drawing.Point(179, 2);
			this.lblSeijunKubunNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeijunKubunNm.Name = "lblSeijunKubunNm";
			this.lblSeijunKubunNm.Size = new System.Drawing.Size(412, 24);
			this.lblSeijunKubunNm.TabIndex = 1;
			this.lblSeijunKubunNm.Tag = "CHANGE";
			this.lblSeijunKubunNm.Text = "1:正組合員　  2:準組合員　  3:組合員以外";
			this.lblSeijunKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 1);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(921, 79);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtSeijunKubunCd);
			this.fsiPanel1.Controls.Add(this.lblSeijunKubunNm);
			this.fsiPanel1.Controls.Add(this.lblKbn);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(911, 30);
			this.fsiPanel1.TabIndex = 1;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// lblKbn
			// 
			this.lblKbn.BackColor = System.Drawing.Color.Silver;
			this.lblKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKbn.Location = new System.Drawing.Point(0, 0);
			this.lblKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKbn.Name = "lblKbn";
			this.lblKbn.Size = new System.Drawing.Size(911, 30);
			this.lblKbn.TabIndex = 0;
			this.lblKbn.Tag = "CHANGE";
			this.lblKbn.Text = "正準区分";
			this.lblKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblFunanushiCdFr);
			this.fsiPanel3.Controls.Add(this.lblCodeBet);
			this.fsiPanel3.Controls.Add(this.txtFunanushiCdFr);
			this.fsiPanel3.Controls.Add(this.lblFunanushiCdTo);
			this.fsiPanel3.Controls.Add(this.txtFunanushiCdTo);
			this.fsiPanel3.Controls.Add(this.label1);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 44);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(911, 30);
			this.fsiPanel3.TabIndex = 0;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(911, 30);
			this.label1.TabIndex = 0;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "船主CD範囲";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// HNCR1011
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCR1011";
			this.ShowFButton = true;
			this.Text = "ReportSample";
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private common.controls.FsiTextBox txtSeijunKubunCd;
        private System.Windows.Forms.Label lblSeijunKubunNm;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label lblKbn;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label1;
    }
}