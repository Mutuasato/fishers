﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncr1011
{
    /// <summary>
    /// HANC9101R の帳票
    /// </summary>
    public partial class HNCR1011R : BaseReport
    {
        public HNCR1011R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    
        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            //和暦でDataTimeを文字列に変換する
            System.Globalization.CultureInfo ci =
                new System.Globalization.CultureInfo("ja-JP", false);
            ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            this.txtToday_tate.Text = DateTime.Now.ToString("gy年MM月dd日(dddd)", ci);
        }
    }
}
