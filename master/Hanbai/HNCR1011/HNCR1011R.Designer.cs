﻿namespace jp.co.fsi.hn.hncr1011
{
    /// <summary>
    /// HANC9101R の概要の説明です。
    /// </summary>
    partial class HNCR1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNCR1011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblBet01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToday_tate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblBet02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBet03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFunanushiNmFr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFunanushiCdTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFunanushiCdFr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFunanushiNmTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtValue19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiNmFr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiCdTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiCdFr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiNmTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblBet01,
            this.txtToday_tate,
            this.txtTitleName,
            this.lblBet02,
            this.lblBet03,
            this.txtTitle02,
            this.txtTitle01,
            this.txtTitle03,
            this.txtTitle04,
            this.txtTitle05,
            this.txtTitle06,
            this.txtTitle07,
            this.txtTitle08,
            this.txtTitle09,
            this.txtTitle10,
            this.txtTitle13,
            this.txtTitle11,
            this.txtTitle15,
            this.txtTitle17,
            this.txtTitle19,
            this.txtTitle12,
            this.txtTitle14,
            this.txtTitle16,
            this.txtTitle20,
            this.line1,
            this.line2,
            this.txtTitle18,
            this.txtFunanushiNmFr,
            this.txtFunanushiCdTo,
            this.txtFunanushiCdFr,
            this.txtFunanushiNmTo});
            this.pageHeader.Height = 1.358054F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // lblBet01
            // 
            this.lblBet01.Height = 0.1968504F;
            this.lblBet01.HyperLink = null;
            this.lblBet01.Left = 1.952756F;
            this.lblBet01.Name = "lblBet01";
            this.lblBet01.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: center; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.lblBet01.Text = "～";
            this.lblBet01.Top = 0.2952756F;
            this.lblBet01.Width = 0.2622047F;
            // 
            // txtToday_tate
            // 
            this.txtToday_tate.Height = 0.1968504F;
            this.txtToday_tate.Left = 9.718505F;
            this.txtToday_tate.MultiLine = false;
            this.txtToday_tate.Name = "txtToday_tate";
            this.txtToday_tate.OutputFormat = resources.GetString("txtToday_tate.OutputFormat");
            this.txtToday_tate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtToday_tate.Text = "yyyy/MM/dd";
            this.txtToday_tate.Top = 0.1909449F;
            this.txtToday_tate.Width = 1.181102F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 4.166929F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle";
            this.txtTitleName.Text = "船主マスタ一覧";
            this.txtTitleName.Top = 0.05984253F;
            this.txtTitleName.Width = 2.952756F;
            // 
            // lblBet02
            // 
            this.lblBet02.Height = 0.1968504F;
            this.lblBet02.HyperLink = null;
            this.lblBet02.Left = 0.595276F;
            this.lblBet02.Name = "lblBet02";
            this.lblBet02.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: center; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.lblBet02.Text = ":";
            this.lblBet02.Top = 0.2952756F;
            this.lblBet02.Width = 0.09212597F;
            // 
            // lblBet03
            // 
            this.lblBet03.Height = 0.1968504F;
            this.lblBet03.HyperLink = null;
            this.lblBet03.Left = 2.62126F;
            this.lblBet03.Name = "lblBet03";
            this.lblBet03.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: center; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.lblBet03.Text = ":";
            this.lblBet03.Top = 0.2952756F;
            this.lblBet03.Width = 0.09212604F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.CharacterSpacing = 1F;
            this.txtTitle02.Height = 0.1968504F;
            this.txtTitle02.Left = 0.5952756F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle02.Text = "正式船主名";
            this.txtTitle02.Top = 0.7141733F;
            this.txtTitle02.Width = 1.467323F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.1968504F;
            this.txtTitle01.Left = 0.1192914F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle01.Text = "船主CD";
            this.txtTitle01.Top = 0.7141733F;
            this.txtTitle01.Width = 0.498819F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.CharacterSpacing = 1F;
            this.txtTitle03.Height = 0.1968504F;
            this.txtTitle03.Left = 2.062598F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTitle03.Text = "略称船主名";
            this.txtTitle03.Top = 0.7141733F;
            this.txtTitle03.Width = 0.9271655F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.CharacterSpacing = 1F;
            this.txtTitle04.Height = 0.1968504F;
            this.txtTitle04.Left = 2.998425F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTitle04.Text = "郵便番号";
            this.txtTitle04.Top = 0.7141733F;
            this.txtTitle04.Width = 0.7704728F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.CharacterSpacing = 1F;
            this.txtTitle05.Height = 0.1968504F;
            this.txtTitle05.Left = 3.87126F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle05.Text = "住所";
            this.txtTitle05.Top = 0.7141733F;
            this.txtTitle05.Width = 3.022048F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.CharacterSpacing = 1F;
            this.txtTitle06.Height = 0.1968504F;
            this.txtTitle06.Left = 6.893308F;
            this.txtTitle06.MultiLine = false;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle06.Text = "船主CD";
            this.txtTitle06.Top = 0.7141733F;
            this.txtTitle06.Width = 0.6578732F;
            // 
            // txtTitle07
            // 
            this.txtTitle07.CharacterSpacing = 1F;
            this.txtTitle07.Height = 0.1968504F;
            this.txtTitle07.Left = 7.876772F;
            this.txtTitle07.MultiLine = false;
            this.txtTitle07.Name = "txtTitle07";
            this.txtTitle07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle07.Text = "船名\r\n";
            this.txtTitle07.Top = 0.7141733F;
            this.txtTitle07.Width = 0.9271655F;
            // 
            // txtTitle08
            // 
            this.txtTitle08.CharacterSpacing = 1F;
            this.txtTitle08.Height = 0.1968504F;
            this.txtTitle08.Left = 9.069292F;
            this.txtTitle08.MultiLine = false;
            this.txtTitle08.Name = "txtTitle08";
            this.txtTitle08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTitle08.Text = "漁法CD";
            this.txtTitle08.Top = 0.7141733F;
            this.txtTitle08.Width = 0.5496058F;
            // 
            // txtTitle09
            // 
            this.txtTitle09.Height = 0.1968504F;
            this.txtTitle09.Left = 9.844489F;
            this.txtTitle09.LineSpacing = 1F;
            this.txtTitle09.MultiLine = false;
            this.txtTitle09.Name = "txtTitle09";
            this.txtTitle09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle09.Text = "漁法名";
            this.txtTitle09.Top = 0.7141733F;
            this.txtTitle09.Width = 1.396457F;
            // 
            // txtTitle10
            // 
            this.txtTitle10.Height = 0.1968504F;
            this.txtTitle10.Left = 2.016929F;
            this.txtTitle10.LineSpacing = 1F;
            this.txtTitle10.MultiLine = false;
            this.txtTitle10.Name = "txtTitle10";
            this.txtTitle10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle10.Text = "組合手数料率\r\n";
            this.txtTitle10.Top = 1.02441F;
            this.txtTitle10.Width = 0.9271653F;
            // 
            // txtTitle13
            // 
            this.txtTitle13.Height = 0.1968504F;
            this.txtTitle13.Left = 4.74252F;
            this.txtTitle13.LineSpacing = 1F;
            this.txtTitle13.MultiLine = false;
            this.txtTitle13.Name = "txtTitle13";
            this.txtTitle13.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle13.Text = "積立区分";
            this.txtTitle13.Top = 1.02441F;
            this.txtTitle13.Width = 0.6925195F;
            // 
            // txtTitle11
            // 
            this.txtTitle11.Height = 0.1968504F;
            this.txtTitle11.Left = 2.989764F;
            this.txtTitle11.LineSpacing = 1F;
            this.txtTitle11.MultiLine = false;
            this.txtTitle11.Name = "txtTitle11";
            this.txtTitle11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTitle11.Text = "普通口座番号";
            this.txtTitle11.Top = 1.02441F;
            this.txtTitle11.Width = 0.8814961F;
            // 
            // txtTitle15
            // 
            this.txtTitle15.Height = 0.1968504F;
            this.txtTitle15.Left = 5.98189F;
            this.txtTitle15.LineSpacing = 1F;
            this.txtTitle15.MultiLine = false;
            this.txtTitle15.Name = "txtTitle15";
            this.txtTitle15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle15.Text = "積立口座番号\r\n";
            this.txtTitle15.Top = 1.02441F;
            this.txtTitle15.Width = 0.9114169F;
            // 
            // txtTitle17
            // 
            this.txtTitle17.Height = 0.1968504F;
            this.txtTitle17.Left = 7.876772F;
            this.txtTitle17.LineSpacing = 1F;
            this.txtTitle17.MultiLine = false;
            this.txtTitle17.Name = "txtTitle17";
            this.txtTitle17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle17.Text = "電話番号";
            this.txtTitle17.Top = 1.02441F;
            this.txtTitle17.Width = 0.9665354F;
            // 
            // txtTitle19
            // 
            this.txtTitle19.Height = 0.1968504F;
            this.txtTitle19.Left = 9.809843F;
            this.txtTitle19.LineSpacing = 1F;
            this.txtTitle19.MultiLine = false;
            this.txtTitle19.Name = "txtTitle19";
            this.txtTitle19.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle19.Text = "支所区分";
            this.txtTitle19.Top = 1.02441F;
            this.txtTitle19.Width = 0.620078F;
            // 
            // txtTitle12
            // 
            this.txtTitle12.Height = 0.1968504F;
            this.txtTitle12.Left = 3.87126F;
            this.txtTitle12.MultiLine = false;
            this.txtTitle12.Name = "txtTitle12";
            this.txtTitle12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle12.Text = "普通口座区分";
            this.txtTitle12.Top = 1.02441F;
            this.txtTitle12.Width = 0.8712596F;
            // 
            // txtTitle14
            // 
            this.txtTitle14.Height = 0.1968504F;
            this.txtTitle14.Left = 5.43504F;
            this.txtTitle14.LineSpacing = 1F;
            this.txtTitle14.MultiLine = false;
            this.txtTitle14.Name = "txtTitle14";
            this.txtTitle14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle14.Text = "積立率\r\n";
            this.txtTitle14.Top = 1.02441F;
            this.txtTitle14.Width = 0.5468501F;
            // 
            // txtTitle16
            // 
            this.txtTitle16.Height = 0.1968504F;
            this.txtTitle16.Left = 6.965355F;
            this.txtTitle16.LineSpacing = 1F;
            this.txtTitle16.MultiLine = false;
            this.txtTitle16.Name = "txtTitle16";
            this.txtTitle16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle16.Text = "積立口座区分";
            this.txtTitle16.Top = 1.02441F;
            this.txtTitle16.Width = 0.9114169F;
            // 
            // txtTitle20
            // 
            this.txtTitle20.Height = 0.1968504F;
            this.txtTitle20.Left = 10.42992F;
            this.txtTitle20.LineSpacing = 1F;
            this.txtTitle20.MultiLine = false;
            this.txtTitle20.Name = "txtTitle20";
            this.txtTitle20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle20.Text = "正準区分\r\n";
            this.txtTitle20.Top = 1.02441F;
            this.txtTitle20.Width = 0.7763777F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.05905512F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Top = 1.285827F;
            this.line1.Width = 11.14724F;
            this.line1.X1 = 0.05905512F;
            this.line1.X2 = 11.2063F;
            this.line1.Y1 = 1.285827F;
            this.line1.Y2 = 1.285827F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 2.016929F;
            this.line2.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.9629922F;
            this.line2.Width = 9.189371F;
            this.line2.X1 = 2.016929F;
            this.line2.X2 = 11.2063F;
            this.line2.Y1 = 0.9629922F;
            this.line2.Y2 = 0.9629922F;
            // 
            // txtTitle18
            // 
            this.txtTitle18.Height = 0.1968504F;
            this.txtTitle18.Left = 8.843307F;
            this.txtTitle18.LineSpacing = 1F;
            this.txtTitle18.MultiLine = false;
            this.txtTitle18.Name = "txtTitle18";
            this.txtTitle18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle18.Text = "FAX番号";
            this.txtTitle18.Top = 1.02441F;
            this.txtTitle18.Width = 0.9665354F;
            // 
            // txtFunanushiNmFr
            // 
            this.txtFunanushiNmFr.DataField = "ITEM04";
            this.txtFunanushiNmFr.Height = 0.1968504F;
            this.txtFunanushiNmFr.Left = 2.865748F;
            this.txtFunanushiNmFr.MultiLine = false;
            this.txtFunanushiNmFr.Name = "txtFunanushiNmFr";
            this.txtFunanushiNmFr.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtFunanushiNmFr.Text = null;
            this.txtFunanushiNmFr.Top = 0.2952757F;
            this.txtFunanushiNmFr.Width = 1.237008F;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.DataField = "ITEM01";
            this.txtFunanushiCdTo.Height = 0.1968504F;
            this.txtFunanushiCdTo.Left = 0.1728347F;
            this.txtFunanushiCdTo.MultiLine = false;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: right; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.txtFunanushiCdTo.Text = null;
            this.txtFunanushiCdTo.Top = 0.2952756F;
            this.txtFunanushiCdTo.Width = 0.4224411F;
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.DataField = "ITEM02";
            this.txtFunanushiCdFr.Height = 0.1968504F;
            this.txtFunanushiCdFr.Left = 2.214961F;
            this.txtFunanushiCdFr.MultiLine = false;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: right; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.txtFunanushiCdFr.Text = null;
            this.txtFunanushiCdFr.Top = 0.2952756F;
            this.txtFunanushiCdFr.Width = 0.4062995F;
            // 
            // txtFunanushiNmTo
            // 
            this.txtFunanushiNmTo.DataField = "ITEM03";
            this.txtFunanushiNmTo.Height = 0.1968504F;
            this.txtFunanushiNmTo.Left = 0.7850394F;
            this.txtFunanushiNmTo.MultiLine = false;
            this.txtFunanushiNmTo.Name = "txtFunanushiNmTo";
            this.txtFunanushiNmTo.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtFunanushiNmTo.Text = null;
            this.txtFunanushiNmTo.Top = 0.2952757F;
            this.txtFunanushiNmTo.Width = 1.167717F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue01,
            this.txtValue02,
            this.txtValue03,
            this.txtValue06,
            this.txtValue07,
            this.txtValue08,
            this.label1,
            this.txtValue04,
            this.textBox09,
            this.txtValue10,
            this.txtValue11,
            this.txtValue12,
            this.txtValue13,
            this.txtValue14,
            this.txtValue16,
            this.txtValue17,
            this.txtValue15,
            this.txtValue18,
            this.line3,
            this.txtValue19,
            this.txtValue20,
            this.txtValue21,
            this.line4,
            this.txtValue05});
            this.detail.Height = 0.5729167F;
            this.detail.Name = "detail";
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM05";
            this.txtValue01.Height = 0.1574803F;
            this.txtValue01.Left = 0.1192914F;
            this.txtValue01.LineSpacing = 1F;
            this.txtValue01.MultiLine = false;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0F;
            this.txtValue01.Width = 0.4846457F;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM06";
            this.txtValue02.Height = 0.1574803F;
            this.txtValue02.Left = 0.603937F;
            this.txtValue02.LineSpacing = 1F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0F;
            this.txtValue02.Width = 1.467323F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM07";
            this.txtValue03.Height = 0.1574803F;
            this.txtValue03.Left = 2.07126F;
            this.txtValue03.LineSpacing = 1F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue03.Text = "\r\n";
            this.txtValue03.Top = 0F;
            this.txtValue03.Width = 0.9271652F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM09";
            this.txtValue06.Height = 0.1574803F;
            this.txtValue06.Left = 3.387795F;
            this.txtValue06.LineSpacing = 1F;
            this.txtValue06.MultiLine = false;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.txtValue06.Text = null;
            this.txtValue06.Top = 0F;
            this.txtValue06.Width = 0.3811024F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM11";
            this.txtValue07.Height = 0.1574803F;
            this.txtValue07.Left = 6.893308F;
            this.txtValue07.LineSpacing = 1F;
            this.txtValue07.MultiLine = false;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0F;
            this.txtValue07.Width = 0.6578741F;
            // 
            // txtValue08
            // 
            this.txtValue08.DataField = "ITEM12";
            this.txtValue08.Height = 0.1574803F;
            this.txtValue08.Left = 7.876772F;
            this.txtValue08.LineSpacing = 1F;
            this.txtValue08.MultiLine = false;
            this.txtValue08.Name = "txtValue08";
            this.txtValue08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue08.Text = "\r\n";
            this.txtValue08.Top = 0F;
            this.txtValue08.Width = 0.9271657F;
            // 
            // label1
            // 
            this.label1.Height = 0.1574803F;
            this.label1.HyperLink = null;
            this.label1.Left = 3.23937F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 1";
            this.label1.Text = "-";
            this.label1.Top = 0F;
            this.label1.Width = 0.1484252F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM10";
            this.txtValue04.Height = 0.1574803F;
            this.txtValue04.Left = 3.87126F;
            this.txtValue04.LineSpacing = 1F;
            this.txtValue04.MultiLine = false;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0F;
            this.txtValue04.Width = 3.022047F;
            // 
            // textBox09
            // 
            this.textBox09.DataField = "ITEM13";
            this.textBox09.Height = 0.1574803F;
            this.textBox09.Left = 9.069292F;
            this.textBox09.LineSpacing = 1F;
            this.textBox09.MultiLine = false;
            this.textBox09.Name = "textBox09";
            this.textBox09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.textBox09.Text = null;
            this.textBox09.Top = 0F;
            this.textBox09.Width = 0.5496064F;
            // 
            // txtValue10
            // 
            this.txtValue10.DataField = "ITEM14";
            this.txtValue10.Height = 0.1574803F;
            this.txtValue10.Left = 9.809843F;
            this.txtValue10.LineSpacing = 1F;
            this.txtValue10.MultiLine = false;
            this.txtValue10.Name = "txtValue10";
            this.txtValue10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue10.Text = null;
            this.txtValue10.Top = 0F;
            this.txtValue10.Width = 1.396456F;
            // 
            // txtValue11
            // 
            this.txtValue11.DataField = "ITEM15";
            this.txtValue11.Height = 0.1574803F;
            this.txtValue11.Left = 2.07126F;
            this.txtValue11.LineSpacing = 1F;
            this.txtValue11.MultiLine = false;
            this.txtValue11.Name = "txtValue11";
            this.txtValue11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtValue11.Text = null;
            this.txtValue11.Top = 0.2858268F;
            this.txtValue11.Width = 0.8728347F;
            // 
            // txtValue12
            // 
            this.txtValue12.DataField = "ITEM16";
            this.txtValue12.Height = 0.1574803F;
            this.txtValue12.Left = 2.989764F;
            this.txtValue12.LineSpacing = 1F;
            this.txtValue12.MultiLine = false;
            this.txtValue12.Name = "txtValue12";
            this.txtValue12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue12.Text = null;
            this.txtValue12.Top = 0.2858268F;
            this.txtValue12.Width = 0.8814958F;
            // 
            // txtValue13
            // 
            this.txtValue13.DataField = "ITEM17";
            this.txtValue13.Height = 0.1574803F;
            this.txtValue13.Left = 3.87126F;
            this.txtValue13.LineSpacing = 1F;
            this.txtValue13.MultiLine = false;
            this.txtValue13.Name = "txtValue13";
            this.txtValue13.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue13.Text = null;
            this.txtValue13.Top = 0.2858268F;
            this.txtValue13.Width = 0.8712596F;
            // 
            // txtValue14
            // 
            this.txtValue14.DataField = "ITEM18";
            this.txtValue14.Height = 0.1574803F;
            this.txtValue14.Left = 4.74252F;
            this.txtValue14.LineSpacing = 1F;
            this.txtValue14.MultiLine = false;
            this.txtValue14.Name = "txtValue14";
            this.txtValue14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.txtValue14.Text = null;
            this.txtValue14.Top = 0.2858268F;
            this.txtValue14.Width = 0.6925197F;
            // 
            // txtValue16
            // 
            this.txtValue16.DataField = "ITEM20";
            this.txtValue16.Height = 0.1574803F;
            this.txtValue16.Left = 5.98189F;
            this.txtValue16.LineSpacing = 1F;
            this.txtValue16.MultiLine = false;
            this.txtValue16.Name = "txtValue16";
            this.txtValue16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue16.Text = null;
            this.txtValue16.Top = 0.2858268F;
            this.txtValue16.Width = 0.9114173F;
            // 
            // txtValue17
            // 
            this.txtValue17.DataField = "ITEM21";
            this.txtValue17.Height = 0.1574803F;
            this.txtValue17.Left = 6.965355F;
            this.txtValue17.LineSpacing = 1F;
            this.txtValue17.MultiLine = false;
            this.txtValue17.Name = "txtValue17";
            this.txtValue17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.txtValue17.Text = null;
            this.txtValue17.Top = 0.2858268F;
            this.txtValue17.Width = 0.9114172F;
            // 
            // txtValue15
            // 
            this.txtValue15.DataField = "ITEM19";
            this.txtValue15.Height = 0.1574803F;
            this.txtValue15.Left = 5.43504F;
            this.txtValue15.LineSpacing = 1F;
            this.txtValue15.MultiLine = false;
            this.txtValue15.Name = "txtValue15";
            this.txtValue15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue15.Text = null;
            this.txtValue15.Top = 0.2858268F;
            this.txtValue15.Width = 0.5468509F;
            // 
            // txtValue18
            // 
            this.txtValue18.DataField = "ITEM22";
            this.txtValue18.Height = 0.1574803F;
            this.txtValue18.Left = 7.876772F;
            this.txtValue18.LineSpacing = 1F;
            this.txtValue18.MultiLine = false;
            this.txtValue18.Name = "txtValue18";
            this.txtValue18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue18.Text = null;
            this.txtValue18.Top = 0.2858268F;
            this.txtValue18.Width = 0.9665347F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 2.07126F;
            this.line3.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.2291339F;
            this.line3.Width = 9.13504F;
            this.line3.X1 = 2.07126F;
            this.line3.X2 = 11.2063F;
            this.line3.Y1 = 0.2291339F;
            this.line3.Y2 = 0.2291339F;
            // 
            // txtValue19
            // 
            this.txtValue19.DataField = "ITEM23";
            this.txtValue19.Height = 0.1574803F;
            this.txtValue19.Left = 8.843307F;
            this.txtValue19.LineSpacing = 1F;
            this.txtValue19.MultiLine = false;
            this.txtValue19.Name = "txtValue19";
            this.txtValue19.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtValue19.Text = null;
            this.txtValue19.Top = 0.2858268F;
            this.txtValue19.Width = 0.9665356F;
            // 
            // txtValue20
            // 
            this.txtValue20.DataField = "ITEM24";
            this.txtValue20.Height = 0.1574803F;
            this.txtValue20.Left = 9.809843F;
            this.txtValue20.LineSpacing = 1F;
            this.txtValue20.MultiLine = false;
            this.txtValue20.Name = "txtValue20";
            this.txtValue20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.txtValue20.Text = null;
            this.txtValue20.Top = 0.2858268F;
            this.txtValue20.Width = 0.6200791F;
            // 
            // txtValue21
            // 
            this.txtValue21.DataField = "ITEM25";
            this.txtValue21.Height = 0.1574803F;
            this.txtValue21.Left = 10.42992F;
            this.txtValue21.LineSpacing = 1F;
            this.txtValue21.MultiLine = false;
            this.txtValue21.Name = "txtValue21";
            this.txtValue21.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.txtValue21.Text = null;
            this.txtValue21.Top = 0.2858268F;
            this.txtValue21.Width = 0.8110237F;
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 0.05905512F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.4952756F;
            this.line4.Width = 11.14724F;
            this.line4.X1 = 0.05905512F;
            this.line4.X2 = 11.2063F;
            this.line4.Y1 = 0.4952756F;
            this.line4.Y2 = 0.4952756F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM08";
            this.txtValue05.Height = 0.1574803F;
            this.txtValue05.Left = 2.998425F;
            this.txtValue05.LineSpacing = 1F;
            this.txtValue05.MultiLine = false;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0F;
            this.txtValue05.Width = 0.2948819F;
            // 
            // pageFooter
            // 
            this.pageFooter.Name = "pageFooter";
            // 
            // HNCR1011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.29528F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblBet01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiNmFr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiCdTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiCdFr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFunanushiNmTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday_tate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunanushiCdTo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunanushiCdFr;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBet01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunanushiNmTo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunanushiNmFr;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBet02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBet03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
    }
}
