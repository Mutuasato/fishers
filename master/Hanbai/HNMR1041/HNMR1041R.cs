﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1041
{
    /// <summary>
    /// HNMR1041R の概要の説明です。
    /// </summary>
    public partial class HNMR1041R : BaseReport
    {

        public HNMR1041R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            ////和暦でDataTimeを文字列に変換する
            //System.Globalization.CultureInfo ci =
            //    new System.Globalization.CultureInfo("ja-JP", false);
            //ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            //this.txtToday.Text = DateTime.Now.ToString("gy年MM月dd日", ci);
        }
        
        /// <summary>
        /// ページフッターの設定(小計)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void groupFooter1_Format(object sender, EventArgs e)
        {
            //20181128 SJ
            // 合計金額をフォーマット
            this.txtShokei01.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei01.Value),2);
            this.txtShokei02.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei02.Value),2);
            this.txtShokei03.Text = Util.FormatNum(this.txtShokei03.Value);
            this.txtShokei04.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei04.Value),2);
            this.txtShokei05.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei05.Value),2);
            this.txtShokei06.Text = Util.FormatNum(this.txtShokei06.Value);
            this.txtShokei07.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei07.Value),2);
            this.txtShokei08.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei08.Value),2);
            this.txtShokei09.Text = Util.FormatNum(this.txtShokei09.Value);
            this.txtShokei10.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei10.Value),2);
            this.txtShokei11.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei11.Value),2);
            this.txtShokei12.Text = Util.FormatNum(this.txtShokei12.Value);
            
            // 合計金額が0の場合、空表示とする
            /*
                        if (this.txtShokei01.Text == "0")
                        {
                            this.txtShokei01.Text = "";
                        }
                        if (this.txtShokei02.Text == "0")
                        {
                            this.txtShokei02.Text = "";
                        }
                        if (this.txtShokei03.Text == "0")
                        {
                            this.txtShokei03.Text = "";
                        }
                        if (this.txtShokei04.Text == "0")
                        {
                            this.txtShokei04.Text = "";
                        }
                        if (this.txtShokei05.Text == "0")
                        {
                            this.txtShokei05.Text = "";
                        }
                        if (this.txtShokei06.Text == "0")
                        {
                            this.txtShokei06.Text = "";
                        }
                        if (this.txtShokei07.Text == "0")
                        {
                            this.txtShokei07.Text = "";
                        }
                        if (this.txtShokei08.Text == "0")
                        {
                            this.txtShokei08.Text = "";
                        }
                        if (this.txtShokei09.Text == "0")
                        {
                            this.txtShokei09.Text = "";
                        }
                        if (this.txtShokei10.Text == "0")
                        {
                            this.txtShokei10.Text = "";
                        }
                        if (this.txtShokei11.Text == "0")
                        {
                            this.txtShokei11.Text = "";
                        }
                        if (this.txtShokei12.Text == "0")
                        {
                            this.txtShokei12.Text = "";
                        }
            */
        }

        /// <summary>
        /// ページフッターの設定(総合計)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportFooter1_Format(object sender, EventArgs e)
        {
            //20181128 SJ
            // 合計金額をフォーマット
            this.txtSogokei03.Text = Util.FormatNum(this.txtSogokei03.Value);
            this.txtSogokei04.Text = Util.FormatNum(this.txtSogokei04.Value);
            this.txtSogokei05.Text = Util.FormatNum(this.txtSogokei05.Value);
            this.txtSogokei06.Text = Util.FormatNum(this.txtSogokei06.Value);
            this.txtSogokei07.Text = Util.FormatNum(this.txtSogokei07.Value);
            this.txtSogokei08.Text = Util.FormatNum(this.txtSogokei08.Value);
            this.txtSogokei09.Text = Util.FormatNum(this.txtSogokei09.Value);
            this.txtSogokei10.Text = Util.FormatNum(this.txtSogokei10.Value);
            this.txtSogokei11.Text = Util.FormatNum(this.txtSogokei11.Value);
            this.txtSogokei12.Text = Util.FormatNum(this.txtSogokei12.Value);
            this.txtSogokei01.Text = Util.FormatNum(this.txtSogokei01.Value);
            this.txtSogokei02.Text = Util.FormatNum(this.txtSogokei02.Value);
            // 合計金額が0の場合、空表示とする
            /*
                        if (this.txtSogokei03.Text == "0")
                        {
                            this.txtSogokei03.Text = "";
                        }
                        if (this.txtSogokei04.Text == "0")
                        {
                            this.txtSogokei04.Text = "";
                        }
                        if (this.txtSogokei05.Text == "0")
                        {
                            this.txtSogokei05.Text = "";
                        }
                        if (this.txtSogokei06.Text == "0")
                        {
                            this.txtSogokei06.Text = "";
                        }
                        if (this.txtSogokei07.Text == "0")
                        {
                            this.txtSogokei07.Text = "";
                        }
                        if (this.txtSogokei08.Text == "0")
                        {
                            this.txtSogokei08.Text = "";
                        }
                        if (this.txtSogokei09.Text == "0")
                        {
                            this.txtSogokei09.Text = "";
                        }
                        if (this.txtSogokei10.Text == "0")
                        {
                            this.txtSogokei10.Text = "";
                        }
                        if (this.txtSogokei11.Text == "0")
                        {
                            this.txtSogokei11.Text = "";
                        }
                        if (this.txtSogokei12.Text == "0")
                        {
                            this.txtSogokei12.Text = "";
                        }
                        if (this.txtSogokei01.Text == "0")
                        {
                            this.txtSogokei01.Text = "";
                        }
                        if (this.txtSogokei02.Text == "0")
                        {
                            this.txtSogokei02.Text = "";
                        }
            */
        }


    }
}
