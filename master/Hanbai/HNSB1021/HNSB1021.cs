﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsb1021
{
    /// <summary>
    /// セリ仕訳データ作成(HNSB1021)
    /// </summary>
    public partial class HNSB1021 : BasePgForm
    {
        #region 列挙体
        /// <summary>
        /// 作成区分
        /// </summary>
        public enum SKbn
        {
            /// <summary>
            /// セリ販売
            /// </summary>
            Seri = 3,
            /// <summary>
            /// 控除
            /// </summary>
            Kojo = 4,
            /// 支払
            /// </summary>
            Shiharai = 6,
            /// <summary>
            /// 選択なし
            /// </summary>
            None = 0
        }
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        private int _mode;
        /// <summary>
        /// モード(1:登録、2:修正)
        /// </summary>
        public int Mode
        {
            get
            {
                return this._mode;
            }
        }

        private int _packDpyNo;
        /// <summary>
        /// 修正対象の一括仕訳伝票番号
        /// </summary>
        public int PackDpyNo
        {
            get
            {
                return this._packDpyNo;
            }
        }

        private DataTable _zeiSetting;
        /// <summary>
        /// 消費税に関する設定
        /// </summary>
        public DataTable ZeiSetting
        {
            get
            {
                return this._zeiSetting;
            }
        }

        private DataTable _jdSwkSettingA;
        /// <summary>
        /// 自動仕訳設定Ａ
        /// </summary>
        public DataTable JdSwkSettingA
        {
            get
            {
                return this._jdSwkSettingA;
            }
        }

        private DataTable _jdSwkSettingB;
        /// <summary>
        /// 自動仕訳設定Ｂ
        /// </summary>
        public DataTable JdSwkSettingB
        {
            get
            {
                return this._jdSwkSettingB;
            }
        }

        private DataTable _dtSwkTgtData = new DataTable();
        /// <summary>
        /// 仕訳対象データ
        /// </summary>
        public DataTable SwkTgtData
        {
            get
            {
                return this._dtSwkTgtData;
            }
        }

        private DataSet _dsTaishakuData = new DataSet();
        /// <summary>
        /// 貸借データ
        /// </summary>
        /// <remarks>
        /// 貸借を仕訳したデータ(1伝票あたり1DataTable)
        /// </remarks>
        public DataSet TaishakuData
        {
            get
            {
                return this._dsTaishakuData;
            }
        }

        /// <summary>
        /// 画面入力値(他の画面との連携に用いる)
        /// </summary>
        public Hashtable Condition
        {
            get
            {
                return GetCondition();
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSB1021()
        {
            InitializeComponent();
            BindGotFocusEvent();
            // 初期起動時は登録モードに設定
            this._mode = 1;
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 選択されている会計年度を表示
            this.lblMessage.Text = "『会計期間第" + Util.ToString(this.UInfo.KessanKi) + "期が選択されています』";

            // 消費税に関する設定を取得
            GetZeiSetting();

            // 消費税情報チェック
            if (!IsZeiSetting())
            {
                this.Close();
            }

            // 水揚支所
            //this.txtMizuageShishoCd.Text = "1";
            //this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }
            // 取り合えず入力、更新系は触れない様に
            txtMizuageShishoCd.Enabled = false;

            // 自動仕訳設定Aの存在チェック
            if (GetTB_HN_ZIDO_SHIWAKE_SETTEI_A().Rows.Count == 0)
            {
                Msg.Notice("仕訳情報が設定されていません。");
                this.Close();
            }
            // 自動仕訳設定Bの存在チェック
            if (GetTB_HN_ZIDO_SHIWAKE_SETTEI_B().Rows.Count == 0)
            {
                Msg.Notice("仕訳情報が設定されていません。");
                this.Close();
            }

            // 初期フォーカスを設定
            this.rdbSeri.Checked = true;
            this.rdbSeri.Focus();

            // 画面値をクリア
            ClearForm();

            // 各項目の使用可否設定
            ControlItemsBySettings();

            // 暫定（現時点でセリと同じ動作の為）
            rdbSeriKojo.Enabled = false;

            // Enter処理を無効化
            this._dtFlg = false;

            // 基本終了
            this.btnEsc.Text = "Esc" + "\n\r" + "終了";
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,作成区分,日付(年),船主CD,担当者CD,摘要CDにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "rdbSeri":
                case "rdbKojo":
                case "txtSeisanDtFrJpYear":
                case "txtSeisanDtToJpYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                case "txtTantoCd":
                case "txtSwkDpyDtJpYear":
                case "txtTekiyoCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // 更新モード時は初期化
            //if (this._mode == 2)
            //{
            //    // 画面をクリア
            //    InitForm();
            //    return;
            //}

            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd":
                    //result = this.openSearchWindow("COMC8011", "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
                    result = this.openSearchWindow(stCurrentDir + @"\EXE\"+"CMCM2031", "1", this.txtMizuageShishoCd.Text);
                    if (!ValChk.IsEmpty(result[0]))
                    {
                        this.txtMizuageShishoCd.Text = result[0];
                        this.lblMizuageShishoNm.Text = result[1];
                    }
                    break;
                #endregion

                #region 精算区分
                case "rdbSeri":
                case "rdbKojo":
                case "rdbSeriKojo":
                    using (HNSB1022 frm1022 = new HNSB1022(this))
                    {
                        frm1022.ShowDialog(this);

                        if (frm1022.DialogResult == DialogResult.OK)
                        {
                            result = (String[])frm1022.OutData;

                            // 画面の各項目をセット
                            SetUpdateItems(result);

                            // 更新モードを設定
                            this._mode = 2;
                            this.lblUpdMode.Visible = true;
                            // 削除を可能にする
                            this.btnF3.Enabled = true;


                            // 更新モード時は更新は不可
                            this.btnF6.Enabled = false;

                            // 更新モード時はクリア
                            this.btnEsc.Text = "Esc" + "\n\r" + "\n\r" + "クリア";

                        }
                    }
                    break;
                #endregion

                #region 精算日付
                case "txtSeisanDtFrJpYear":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeisanDtFrGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSeisanDtFrGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpFr();
                            }
                        }
                    }
                    break;

                case "txtSeisanDtToJpYear":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeisanDtToGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSeisanDtToGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpTo();
                            }
                        }
                    }
                    break;
                #endregion

                #region 船主CD
                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("KOBC9021.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiNmFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("KOBC9021.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiNmTo.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion

                #region 担当者CD
                case "txtTantoCd":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("KOBC9041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9041.KOBC9041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoCd.Text = outData[0];
                                this.lblTantoNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion

                #region 仕訳伝票日付
                case "txtSwkDpyDtJpYear":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSwkDpyDtGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSwkDpyDtGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJp();
                            }
                        }
                    }
                    break;
                #endregion

                #region 摘要CD
                case "txtTekiyoCd":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("SKDC9051.exe");
                    //asm = Assembly.LoadFrom("HNCM1201.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2061.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.skd.skdc9051.SKDC9051");
                    //t = asm.GetType("jp.co.fsi.hn.hncm1201.HNCM1201");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2061.CMCM2061");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = Util.ToString(txtMizuageShishoCd.Text);
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTekiyoCd.Text = outData[0];
                                this.txtTekiyo.Text = outData[1];
                            }
                        }
                    }
                    break;
                    #endregion

                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled) return;

            // 削除処理
            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」が押されたら処理終了
                return;
            }

            // 更新中メッセージ表示
            HNSB1026 msgFrm = new HNSB1026();
            msgFrm.Show();
            msgFrm.Refresh();

            try
            {
                this.Dba.BeginTransaction();

                // 削除処理を実行
                HNSB1021DA da = new HNSB1021DA(this.UInfo, this.Dba, this.Config);
                bool dataExists = false;
                bool result;

                if (this.rdbSeri.Checked)
                {
                    // セリ販売の更新処理
                    result = da.MakeSeriSwkData(3, this._packDpyNo, this.Condition, ref dataExists);
                }
                else if (this.rdbKojo.Checked)
                {
                    // 控除の更新処理
                    result = da.MakeKojoSwkData(3, this._packDpyNo, this.Condition, this._zeiSetting, ref dataExists);
                }
                else
                {
                    // セリ販売、控除の更新処理
                    result = da.MakeSeriSwkData(3, this._packDpyNo, this.Condition, ref dataExists);
                }

                // 更新終了後、メッセージを閉じる
                msgFrm.Close();

                // 更新に失敗していればその旨表示する
                if (result)
                {
                    this.Dba.Commit();

                    Msg.Info("削除が完了しました。");

                    // 画面をクリア
                    InitForm();
                }
                else
                {
                    this.Dba.Rollback();
                    if (dataExists)
                    {
                        Msg.Error("更新に失敗しました。" + Environment.NewLine + "もう一度やり直して下さい。");
                    }
                    else
                    {
                        //Msg.Error("該当データがありません。");
                        Msg.Info("該当データがありません。");
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (!this.btnF4.Enabled) return;

            // 参照処理
            // 全項目の入力チェック
            if (!ValidateAll())
            {
                return;
            }

            // 参照画面の起動
            if (this.rdbSeri.Checked)
            {
                // 仕訳対象データを抽出
                HNSB1021DA da = new HNSB1021DA(this.UInfo, this.Dba, this.Config);
                this._dtSwkTgtData = da.GetSeriSwkTgtData(this._mode, this.Condition);

                // 貸借データの作成
                //this._dsTaishakuData = da.GetTaishakuData(this.Condition, this._zeiSetting,
                //    this._jdSwkSettingA, this._jdSwkSettingB, this._dtSwkTgtData);
                // 本来はセリ用のハズが若干違う別メソッドを呼んでいたのでセリを呼び出す様に
                this._dsTaishakuData = da.GetTaishakuData(this.Condition, 
                    this._jdSwkSettingA, this._jdSwkSettingB, this._dtSwkTgtData);

                using (HNSB1023 frm1023 = new HNSB1023(this))
                {
                    // 更新モード時はInDataを設定し更新は不可に
                    if (this._mode == 2)
                        frm1023.InData = "2";

                    if (frm1023.ShowDialog(this) == DialogResult.OK)
                    {
                        // 更新処理が完了時、初期状態に戻す
                        InitForm();
                    }
                }
            }
            else if (this.rdbKojo.Checked)
            {
                using (HNSB1024 frm1024 = new HNSB1024(this))
                {
                    // 更新モード時はInDataを設定し更新は不可に
                    if (this._mode == 2)
                        frm1024.InData = "2";

                    if (frm1024.ShowDialog(this) == DialogResult.OK)
                    {
                        // 更新処理が完了時、初期状態に戻す
                        InitForm();
                    }
                }
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled) return;

            // 更新処理
            this.UpdateData();
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定処理
            HNSB1025 frm1025 = new HNSB1025();

            if (frm1025.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                this.Config.ReloadConfig();
                ControlItemsBySettings();
            }
            frm1025.Dispose();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
        }

        /// <summary>
        /// 精算日付(自)・年の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanDtFrJpYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtSeisanDtFrJpYear.Text, this.txtSeisanDtFrJpYear.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanDtFrJpYear.SelectAll();
            }
            else
            {
                this.txtSeisanDtFrJpYear.Text = Util.ToString(IsValid.SetYear(this.txtSeisanDtFrJpYear.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 精算日付(自)・月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanDtFrMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtSeisanDtFrMonth.Text, this.txtSeisanDtFrMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanDtFrMonth.SelectAll();
            }
            else
            {
                this.txtSeisanDtFrMonth.Text = Util.ToString(IsValid.SetMonth(this.txtSeisanDtFrMonth.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 精算日付(自)・日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanDtFrDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtSeisanDtFrDay.Text, this.txtSeisanDtFrDay.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanDtFrDay.SelectAll();
            }
            else
            {
                this.txtSeisanDtFrDay.Text = Util.ToString(IsValid.SetDay(this.txtSeisanDtFrDay.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 精算日付(至)・年の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanDtToJpYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtSeisanDtToJpYear.Text, this.txtSeisanDtToJpYear.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanDtToJpYear.SelectAll();
            }
            else
            {
                this.txtSeisanDtToJpYear.Text = Util.ToString(IsValid.SetYear(this.txtSeisanDtToJpYear.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 精算日付(至)・月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanDtToMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtSeisanDtToMonth.Text, this.txtSeisanDtToMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanDtToMonth.SelectAll();
            }
            else
            {
                this.txtSeisanDtToMonth.Text = Util.ToString(IsValid.SetMonth(this.txtSeisanDtToMonth.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 精算日付(至)・日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanDtToDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtSeisanDtToDay.Text, this.txtSeisanDtToDay.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanDtToDay.SelectAll();
                return;
            }
            else
            {
                this.txtSeisanDtToDay.Text = Util.ToString(IsValid.SetDay(this.txtSeisanDtToDay.Text));
                CheckJpTo();
                SetJpTo();
            }

            // 仕訳伝票日付に値をコピー
            this.lblSwkDpyDtGengo.Text = this.lblSeisanDtToGengo.Text;
            this.txtSwkDpyDtJpYear.Text = this.txtSeisanDtToJpYear.Text;
            this.txtSwkDpyDtMonth.Text = this.txtSeisanDtToMonth.Text;
            this.txtSwkDpyDtDay.Text = this.txtSeisanDtToDay.Text;
        }

        /// <summary>
        /// 船主コード(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text);
            this.lblFunanushiNmFr.Text = name;
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.lblFunanushiNmFr.Text))
            {
                this.lblFunanushiNmFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 船主コード(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text);
            this.lblFunanushiNmTo.Text = name;
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.lblFunanushiNmTo.Text))
            {
                this.lblFunanushiNmTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 担当者コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoCd_Validating(object sender, CancelEventArgs e)
        {
            // 名称を表示
            string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoCd.Text);
            this.lblTantoNm.Text = name;
          }

        /// <summary>
        /// 仕訳伝票日付・年の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSwkDpyDtJpYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtSwkDpyDtJpYear.Text, this.txtSwkDpyDtJpYear.MaxLength))
            {
                e.Cancel = true;
                this.txtSwkDpyDtJpYear.SelectAll();
            }
            else
            {
                this.txtSwkDpyDtJpYear.Text = Util.ToString(IsValid.SetYear(this.txtSwkDpyDtJpYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 仕訳伝票日付・月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSwkDpyDtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtSwkDpyDtMonth.Text, this.txtSwkDpyDtMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtSwkDpyDtMonth.SelectAll();
            }
            else
            {
                this.txtSwkDpyDtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtSwkDpyDtMonth.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 仕訳伝票日付・日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSwkDpyDtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtSwkDpyDtDay.Text, this.txtSwkDpyDtDay.MaxLength))
            {
                e.Cancel = true;
                this.txtSwkDpyDtDay.SelectAll();
            }
            else
            {
                this.txtSwkDpyDtDay.Text = Util.ToString(IsValid.SetDay(this.txtSwkDpyDtDay.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 摘要コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoCd_Validating(object sender, CancelEventArgs e)
        {
            // 名称を表示
            string name = this.Dba.GetName(this.UInfo, "TB_HN_TEKIYO", this.txtMizuageShishoCd.Text, this.txtTekiyoCd.Text);
            this.txtTekiyo.Text = name;
        }

        /// <summary>
        /// 摘要のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyo_KeyDown(object sender, KeyEventArgs e)
        {
            if (!this.btnF6.Enabled) return;

            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 更新処理
                this.UpdateData();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text))
            {
                //// 水揚支所名称を表示する
                //this.lblMizuageShishoNm.Text = "全て";
                //return true;

                // 更新処理時は必須
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0入力の場合
            if (Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                //// 水揚支所名称を表示する
                //this.lblMizuageShishoNm.Text = "全て";
                //return true;

                // 更新処理時は必須
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 支所の切り替え
            if (Util.ToInt(this.UInfo.ShishoCd) != Util.ToInt(this.txtMizuageShishoCd.Text))
            {
                this.UInfo.ShishoCd = this.txtMizuageShishoCd.Text;
                this.UInfo.ShishoNm = this.lblMizuageShishoNm.Text;
            }

            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidFunanushiCdFr()
        {
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiNmFr.Text = "先　頭";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text);
            this.lblFunanushiNmFr.Text = name;

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidFunanushiCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiNmTo.Text = "最　後";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text);
            this.lblFunanushiNmTo.Text = name;

            return true;
        }

        /// <summary>
        /// 担当者コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTantoCd()
        {
            // 未入力はエラー
            if (ValChk.IsEmpty(this.txtTantoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTantoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoCd.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            this.lblTantoNm.Text = name;

            return true;
        }

        /// <summary>
        /// 摘要コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTekiyoCd()
        {
            // 未入力はOK
            if (ValChk.IsEmpty(this.txtTekiyoCd.Text))
            {
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTekiyoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            string name = this.Dba.GetName(this.UInfo, "TB_HN_TEKIYO", this.txtMizuageShishoCd.Text, this.txtTekiyoCd.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            this.txtTekiyo.Text = name;

            return true;
        }

        /// <summary>
        /// 摘要の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTekiyo()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTekiyo.Text, this.txtTekiyo.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 単項目チェック
            // ※伝票日付の範囲については会計期間のチェックもする

            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 精算日付(自)
            if (!IsValid.IsYear(this.txtSeisanDtFrJpYear.Text, this.txtSeisanDtFrJpYear.MaxLength))
            {
                this.txtSeisanDtFrJpYear.Focus();
                this.txtSeisanDtFrJpYear.SelectAll();
                return false;
            }
            if (!IsValid.IsMonth(this.txtSeisanDtFrMonth.Text, this.txtSeisanDtFrMonth.MaxLength))
            {
                this.txtSeisanDtFrMonth.Focus();
                this.txtSeisanDtFrMonth.SelectAll();
                return false;
            }
            if (!IsValid.IsDay(this.txtSeisanDtFrDay.Text, this.txtSeisanDtFrDay.MaxLength))
            {
                this.txtSeisanDtFrDay.Focus();
                this.txtSeisanDtFrDay.SelectAll();
                return false;
            }
            // 月末入力チェック処理
            CheckJpFr();
            // 正しい和暦への変換処理
            SetJpFr();

            DateTime tmpDpyDtFr = Util.ConvAdDate(this.lblSeisanDtFrGengo.Text,
                Util.ToInt(this.txtSeisanDtFrJpYear.Text),
                Util.ToInt(this.txtSeisanDtFrMonth.Text),
                Util.ToInt(this.txtSeisanDtFrDay.Text), this.Dba);
            if (tmpDpyDtFr.CompareTo(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"])) < 0
                || tmpDpyDtFr.CompareTo(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"])) > 0)
            {
                Msg.Error("会計期間内ではありません！");
                this.txtSeisanDtFrJpYear.Focus();
                this.txtSeisanDtFrJpYear.SelectAll();
                return false;
            }

            // 精算日付(至)
            if (!IsValid.IsYear(this.txtSeisanDtToJpYear.Text, this.txtSeisanDtToJpYear.MaxLength))
            {
                this.txtSeisanDtToJpYear.Focus();
                this.txtSeisanDtToJpYear.SelectAll();
                return false;
            }
            if (!IsValid.IsMonth(this.txtSeisanDtToMonth.Text, this.txtSeisanDtToMonth.MaxLength))
            {
                this.txtSeisanDtToMonth.Focus();
                this.txtSeisanDtToMonth.SelectAll();
                return false;
            }
            if (!IsValid.IsDay(this.txtSeisanDtToDay.Text, this.txtSeisanDtToDay.MaxLength))
            {
                this.txtSeisanDtToDay.Focus();
                this.txtSeisanDtToDay.SelectAll();
                return false;
            }
            // 月末入力チェック処理
            CheckJpTo();
            // 正しい和暦への変換処理
            SetJpTo();
            DateTime tmpDpyDtTo = Util.ConvAdDate(this.lblSeisanDtToGengo.Text,
                Util.ToInt(this.txtSeisanDtToJpYear.Text),
                Util.ToInt(this.txtSeisanDtToMonth.Text),
                Util.ToInt(this.txtSeisanDtToDay.Text), this.Dba);
            if (tmpDpyDtTo.CompareTo(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"])) < 0
                || tmpDpyDtTo.CompareTo(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"])) > 0)
            {
                Msg.Error("会計期間内ではありません！");
                this.txtSeisanDtToJpYear.Focus();
                this.txtSeisanDtToJpYear.SelectAll();
                return false;
            }
            
            // 伝票日付の範囲が逆転していたらエラーとする
            if (tmpDpyDtFr.CompareTo(tmpDpyDtTo) > 0)
            {
                Msg.Error("入力に誤りがあります。");
                this.txtSeisanDtFrJpYear.Focus();
                this.txtSeisanDtFrJpYear.SelectAll();
                return false;
            }

            // 船主コード(自)
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }

            // 船主コード(至)
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            // 担当者コード
            if (!IsValidTantoCd())
            {
                this.txtTantoCd.Focus();
                this.txtTantoCd.SelectAll();
                return false;
            }

            // 仕訳伝票日付
            if (!IsValid.IsYear(this.txtSwkDpyDtJpYear.Text, this.txtSwkDpyDtJpYear.MaxLength))
            {
                this.txtSwkDpyDtJpYear.Focus();
                this.txtSwkDpyDtJpYear.SelectAll();
                return false;
            }
            if (!IsValid.IsMonth(this.txtSwkDpyDtMonth.Text, this.txtSwkDpyDtMonth.MaxLength))
            {
                this.txtSwkDpyDtMonth.Focus();
                this.txtSwkDpyDtMonth.SelectAll();
                return false;
            }
            if (!IsValid.IsDay(this.txtSwkDpyDtDay.Text, this.txtSwkDpyDtDay.MaxLength))
            {
                this.txtSwkDpyDtDay.Focus();
                this.txtSwkDpyDtDay.SelectAll();
                return false;
            }

            // 摘要コード
            if (!IsValidTekiyoCd())
            {
                this.txtTekiyoCd.Focus();
                this.txtTekiyoCd.SelectAll();
                return false;
            }

            // 摘要
            if (!IsValidTekiyo())
            {
                this.txtTekiyo.Focus();
                this.txtTekiyo.SelectAll();
                return false;
            }

            // 仕訳伝票日付が会計期間内でなければ会計期間の最終日にする
            DateTime tmpSwkDpyDt = Util.ConvAdDate(this.lblSwkDpyDtGengo.Text,
                Util.ToInt(this.txtSwkDpyDtJpYear.Text),
                Util.ToInt(this.txtSwkDpyDtMonth.Text),
                Util.ToInt(this.txtSwkDpyDtDay.Text), this.Dba);
            if (tmpSwkDpyDt.CompareTo(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"])) < 0
                || tmpSwkDpyDt.CompareTo(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"])) > 0)
            {
                string[] aryKaikeiKikanTo = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);
                this.lblSwkDpyDtGengo.Text = aryKaikeiKikanTo[0];
                this.txtSwkDpyDtJpYear.Text = aryKaikeiKikanTo[2];
                this.txtSwkDpyDtMonth.Text = aryKaikeiKikanTo[3];
                this.txtSwkDpyDtDay.Text = aryKaikeiKikanTo[4];
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.FixJpDate(this.lblSeisanDtFrGengo.Text, this.txtSeisanDtFrJpYear.Text,
                this.txtSeisanDtFrMonth.Text, this.txtSeisanDtFrDay.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.FixJpDate(this.lblSeisanDtToGengo.Text, this.txtSeisanDtToJpYear.Text,
                this.txtSeisanDtToMonth.Text, this.txtSeisanDtToDay.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(伝票日付)の正しい和暦への変換処理
        /// </summary>
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            //SetJpDateToSwkDpyDt(Util.FixJpDate(this.lblSwkDpyDtGengo.Text, this.txtSwkDpyDtJpYear.Text,
            //    this.txtSwkDpyDtMonth.Text, this.txtSwkDpyDtDay.Text, this.Dba));
            SetJp(Util.ConvJpDate(
                                this.FixNendoDate(
                                Util.ConvAdDate(this.lblSwkDpyDtGengo.Text,
                                                this.txtSwkDpyDtJpYear.Text,
                                                this.txtSwkDpyDtMonth.Text,
                                                this.txtSwkDpyDtDay.Text, this.Dba)), this.Dba));
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeisanDtFrGengo.Text, this.txtSeisanDtFrJpYear.Text,
                this.txtSeisanDtFrMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeisanDtFrDay.Text) > lastDayInMonth)
            {
                this.txtSeisanDtFrDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeisanDtToGengo.Text, this.txtSeisanDtToJpYear.Text,
                this.txtSeisanDtToMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeisanDtToDay.Text) > lastDayInMonth)
            {
                this.txtSeisanDtToDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(伝票日付)の月末入力チェック
        /// </summary>
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSwkDpyDtGengo.Text, this.txtSwkDpyDtJpYear.Text,
                this.txtSwkDpyDtMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSwkDpyDtDay.Text) > lastDayInMonth)
            {
                this.txtSwkDpyDtDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 配列に格納された和暦を精算日付(自)にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblSeisanDtFrGengo.Text = arrJpDate[0];
            this.txtSeisanDtFrJpYear.Text = arrJpDate[2];
            this.txtSeisanDtFrMonth.Text = arrJpDate[3];
            this.txtSeisanDtFrDay.Text = arrJpDate[4];
        }
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblSeisanDtToGengo.Text = arrJpDate[0];
            this.txtSeisanDtToJpYear.Text = arrJpDate[2];
            this.txtSeisanDtToMonth.Text = arrJpDate[3];
            this.txtSeisanDtToDay.Text = arrJpDate[4];
        }
        private void SetJp(string[] arrJpDate)
        {
            this.lblSwkDpyDtGengo.Text = arrJpDate[0];
            this.txtSwkDpyDtJpYear.Text = arrJpDate[2];
            this.txtSwkDpyDtMonth.Text = arrJpDate[3];
            this.txtSwkDpyDtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            //string nameSpace = lowerModuleName.Substring(0, 3);
            string nameSpace = lowerModuleName.Substring(0, 2);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 消費税に関する設定を取得
        /// </summary>
        private void GetZeiSetting()
        {
            bool krukFlg = false;
            bool krbriFlg = false;

            // 1.仮受消費税に関する設定を取得(会社情報の仮受消費税コードから)
            DataTable dtKrukZei = GetVI_ZM_KANJO_KAMOKU(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));

            // 2.仮払消費税に関する設定を取得(会社情報の仮払消費税コードから)
            DataTable dtKrbriZei = GetVI_ZM_KANJO_KAMOKU(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));

            // 以上の設定内容を1つのDataTableとしてマージした上でプロパティとして他画面から参照可能にする
            DataTable dtZeiSetting = dtKrukZei.Clone(); // 仮受の定義をコピー
            if (dtKrukZei.Rows.Count > 0)
            {
                dtZeiSetting.ImportRow(dtKrukZei.Rows[0]);
                krukFlg = true;
            }
            if (dtKrbriZei.Rows.Count > 0)
            {
                dtZeiSetting.ImportRow(dtKrbriZei.Rows[0]);
                krbriFlg = true;
            }

            // 仮受・仮払を区別するカラムを追加
            dtZeiSetting.Columns.Add("KBN", typeof(int));
            if (krukFlg)
            {
                // 「仮受」の区分"1"を設定
                dtZeiSetting.Rows[0]["KBN"] = 1;
            }
            if (krbriFlg)
            {
                // 「仮払」の区分"2"を設定
                if (krukFlg)
                {
                    dtZeiSetting.Rows[1]["KBN"] = 2;
                }
                else
                {
                    dtZeiSetting.Rows[0]["KBN"] = 2;
                }
            }

            // プロパティに保持
            this._zeiSetting = dtZeiSetting;
        }

        /// <summary>
        /// VI_勘定科目から設定を取得
        /// </summary>
        /// <param name="kanjoKmkCd">勘定科目コード</param>
        /// <returns>VI_勘定科目から取得したデータ</returns>
        private DataTable GetVI_ZM_KANJO_KAMOKU(string kanjoKmkCd)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" * ");
            sql.Append("FROM");
            sql.Append(" VI_ZM_KANJO_KAMOKU ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKmkCd);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// TB_自動仕訳設定Aから設定を取得
        /// </summary>
        /// <returns>取得した自動仕訳設定A</returns>
        private DataTable GetTB_HN_ZIDO_SHIWAKE_SETTEI_A(DateTime DenpyoDay)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" A.SHIWAKE_CD AS 仕訳コード,");
            sql.Append(" A.KANJO_KAMOKU_CD AS 勘定科目コード,");
            sql.Append(" B.KANJO_KAMOKU_NM AS 勘定科目名,");
            sql.Append(" (CASE WHEN B.HOJO_KAMOKU_UMU = 0 THEN 0");
            sql.Append("  WHEN A.HOJO_KAMOKU_CD = 0 THEN -1");
            sql.Append("  ELSE A.HOJO_KAMOKU_CD");
            sql.Append("  END) AS 補助科目コード,");
            sql.Append(" (CASE WHEN B.BUMON_UMU = 0 THEN 0");
            sql.Append("  WHEN A.BUMON_CD = 0 THEN -1");
            sql.Append("  ELSE A.BUMON_CD");
            sql.Append("  END) AS 部門コード,");
            sql.Append(" B.HOJO_SHIYO_KUBUN AS 補助使用区分,");
            sql.Append(" B.TAISHAKU_KUBUN AS 貸借区分,");
            sql.Append(" A.ZEI_KUBUN AS 税区分,");
            sql.Append(" C.KAZEI_KUBUN AS 課税区分,");
            sql.Append(" C.TORIHIKI_KUBUN AS 取引区分,");

            //sql.Append(" C.ZEI_RITSU AS 税率,");
            sql.Append(" dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS 税率,");

            sql.Append(" A.JIGYO_KUBUN AS 事業区分 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_ZIDO_SHIWAKE_SETTEI_A AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_ZM_KANJO_KAMOKU AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_ZM_F_ZEI_KUBUN AS C ");
            sql.Append("ON");
            sql.Append(" A.ZEI_KUBUN = C.ZEI_KUBUN ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.DENPYO_KUBUN IN (3, 4) AND");
            sql.Append(" ISNULL(B.KANJO_KAMOKU_CD, 0) <> 0");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            DataTable dtSetting = dtResult.Clone();
            DataRow drSetting;
            DataTable dtHojoKmkNm;
            for (int i = 0; i < dtResult.Rows.Count; i++)
            {
                drSetting = dtSetting.NewRow();

                for (int j = 0; j < dtResult.Columns.Count; j++)
                {
                    if (dtResult.Columns[j].ColumnName.Equals("補助科目名"))
                    {
                        dtHojoKmkNm = GetTB_ZM_HOJO_KAMOKU(Util.ToString(dtResult.Rows[i]["勘定科目コード"]),
                            Util.ToString(dtResult.Rows[i]["補助科目コード"]));
                        if (dtHojoKmkNm.Rows.Count > 0)
                        {
                            drSetting[j] = dtHojoKmkNm.Rows[0]["HOJO_KAMOKU_NM"];
                        }
                        else
                        {
                            drSetting[j] = null;
                        }
                    }
                    else
                    {
                        drSetting[j] = dtResult.Rows[i][j];
                    }
                }

                dtSetting.Rows.Add(drSetting);
            }

            this._jdSwkSettingA = dtSetting;

            return dtResult;
        }
        private DataTable GetTB_HN_ZIDO_SHIWAKE_SETTEI_A()
        {
            return GetTB_HN_ZIDO_SHIWAKE_SETTEI_A(DateTime.Today);
        }

        /// <summary>
        /// TB_自動仕訳設定Bから設定を取得
        /// </summary>
        /// <returns>取得した自動仕訳設定B</returns>
        private DataTable GetTB_HN_ZIDO_SHIWAKE_SETTEI_B(DateTime DenpyoDay)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" A.SHIWAKE_CD AS 仕訳コード,");
            sql.Append(" A.KANJO_KAMOKU_CD AS 勘定科目コード,");
            sql.Append(" B.KANJO_KAMOKU_NM  AS 勘定科目名,");
            sql.Append(" (CASE WHEN B.HOJO_KAMOKU_UMU = 0 THEN 0");
            sql.Append("  ELSE CASE WHEN A.HOJO_KAMOKU_CD = 0 THEN -1");
            sql.Append("   ELSE A.HOJO_KAMOKU_CD");
            sql.Append("   END");
            sql.Append("  END) AS 補助科目コード,");
            sql.Append(" (CASE WHEN B.BUMON_UMU = 0 THEN 0");
            sql.Append("  ELSE CASE WHEN A.BUMON_CD = 0 THEN -1");
            sql.Append("   ELSE A.BUMON_CD");
            sql.Append("   END");
            sql.Append("  END) AS 部門コード,");
            sql.Append(" B.HOJO_SHIYO_KUBUN AS 補助使用区分,");
            sql.Append(" A.TAISHAKU_KUBUN AS 貸借区分,");
            sql.Append(" A.ZEI_KUBUN AS 税区分,");
            sql.Append(" C.KAZEI_KUBUN AS 課税区分,");
            sql.Append(" C.TORIHIKI_KUBUN AS 取引区分,");

            //sql.Append(" C.ZEI_RITSU AS 税率,");
            sql.Append(" dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS 税率,");

            sql.Append(" A.JIGYO_KUBUN AS 事業区分 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_ZIDO_SHIWAKE_SETTEI_B AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_ZM_KANJO_KAMOKU AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_ZM_F_ZEI_KUBUN AS C ");
            sql.Append("ON");
            sql.Append(" A.ZEI_KUBUN = C.ZEI_KUBUN ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.DENPYO_KUBUN IN (3, 4) AND");
            sql.Append(" ISNULL(B.KANJO_KAMOKU_CD, 0) <> 0");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            DataTable dtSetting = dtResult.Clone();
            DataRow drSetting;
            DataTable dtHojoKmkNm;
            for (int i = 0; i < dtResult.Rows.Count; i++)
            {
                drSetting = dtSetting.NewRow();

                for (int j = 0; j < dtResult.Columns.Count; j++)
                {
                    if (dtResult.Columns[j].ColumnName.Equals("補助科目名"))
                    {
                        dtHojoKmkNm = GetTB_ZM_HOJO_KAMOKU(Util.ToString(dtResult.Rows[i]["勘定科目コード"]),
                            Util.ToString(dtResult.Rows[i]["補助科目コード"]));
                        if (dtHojoKmkNm.Rows.Count > 0)
                        {
                            drSetting[j] = dtHojoKmkNm.Rows[0]["HOJO_KAMOKU_NM"];
                        }
                        else
                        {
                            drSetting[j] = null;
                        }
                    }
                    else
                    {
                        drSetting[j] = dtResult.Rows[i][j];
                    }
                }

                dtSetting.Rows.Add(drSetting);
            }

            this._jdSwkSettingB = dtSetting;

            return dtResult;
        }
        private DataTable GetTB_HN_ZIDO_SHIWAKE_SETTEI_B()
        {
            return GetTB_HN_ZIDO_SHIWAKE_SETTEI_B(DateTime.Today);
        }

        /// <summary>
        /// TB_補助科目から設定を取得
        /// </summary>
        /// <param name="kanjoKmkCd">勘定科目コード</param>
        /// <param name="hojoKmkCd">補助科目コード</param>
        /// <returns>TB_補助科目から取得したデータ</returns>
        private DataTable GetTB_ZM_HOJO_KAMOKU(string kanjoKmkCd, string hojoKmkCd)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" HOJO_KAMOKU_NM ");
            sql.Append("FROM");
            sql.Append(" TB_ZM_HOJO_KAMOKU ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND");
            sql.Append(" HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKmkCd);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, hojoKmkCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// フォームの内容をクリアする
        /// </summary>
        private void ClearForm()
        {
            // 初期モードを「登録」に設定
            this._mode = 1;
            this._packDpyNo = 0;
            this.lblUpdMode.Visible = false;

            // 更新可能
            this.btnF6.Enabled = true;

            // この時点では削除不可能
            this.btnF3.Enabled = false;

            // 日付の初期表示
            string[] arrJpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            this.lblSeisanDtFrGengo.Text = arrJpDate[0];
            this.txtSeisanDtFrJpYear.Text = arrJpDate[2];
            this.txtSeisanDtFrMonth.Text = arrJpDate[3];
            this.txtSeisanDtFrDay.Text = arrJpDate[4];
            this.lblSeisanDtToGengo.Text = arrJpDate[0];
            this.txtSeisanDtToJpYear.Text = arrJpDate[2];
            this.txtSeisanDtToMonth.Text = arrJpDate[3];
            this.txtSeisanDtToDay.Text = arrJpDate[4];
            this.lblSwkDpyDtGengo.Text = arrJpDate[0];
            this.txtSwkDpyDtJpYear.Text = arrJpDate[2];
            this.txtSwkDpyDtMonth.Text = arrJpDate[3];
            this.txtSwkDpyDtDay.Text = arrJpDate[4];

            // 船主コード範囲
            this.txtFunanushiCdFr.Text = string.Empty;
            this.lblFunanushiNmFr.Text = "先　頭";
            this.txtFunanushiCdTo.Text = string.Empty;
            this.lblFunanushiNmTo.Text = "最　後";

            // 担当者
            this.txtTantoCd.Text = this.UInfo.UserCd;
            // 存在しないコードを入力されたらエラー
            string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoCd.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
            }
            this.lblTantoNm.Text = name;

            // 摘要
            this.txtTekiyoCd.Text = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "TekiyoCd");
            this.txtTekiyo.Text = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "Tekiyo");
        }

        /// <summary>
        /// 更新モードでの各項目をセットする
        /// </summary>
        /// <param name="outData">履歴参照画面からの戻り値</param>
        private void SetUpdateItems(string[] outData)
        {
            DateTime tmpDate;
            string[] aryJpDate;

            // 返却用_伝票番号
            this._packDpyNo = Util.ToInt(outData[0]);

            // 返却用_伝票日付
            tmpDate = Util.ToDate(outData[1]);
            aryJpDate = Util.ConvJpDate(tmpDate, this.Dba);
            this.lblSwkDpyDtGengo.Text = aryJpDate[0];
            this.txtSwkDpyDtJpYear.Text = aryJpDate[2];
            this.txtSwkDpyDtMonth.Text = aryJpDate[3];
            this.txtSwkDpyDtDay.Text = aryJpDate[4];

            // 返却用_処理区分
            switch (outData[9])
            {
                case "3":
                    this.rdbSeri.Checked = true;
                    break;
                case "4":
                    this.rdbKojo.Checked = true;
                    break;
                default:
                    break;
            }

            // 返却用_開始伝票日付
            tmpDate = Util.ToDate(outData[3]);
            aryJpDate = Util.ConvJpDate(tmpDate, this.Dba);
            this.lblSeisanDtFrGengo.Text = aryJpDate[0];
            this.txtSeisanDtFrJpYear.Text = aryJpDate[2];
            this.txtSeisanDtFrMonth.Text = aryJpDate[3];
            this.txtSeisanDtFrDay.Text = aryJpDate[4];

            // 返却用_終了伝票日付
            tmpDate = Util.ToDate(outData[4]);
            aryJpDate = Util.ConvJpDate(tmpDate, this.Dba);
            this.lblSeisanDtToGengo.Text = aryJpDate[0];
            this.txtSeisanDtToJpYear.Text = aryJpDate[2];
            this.txtSeisanDtToMonth.Text = aryJpDate[3];
            this.txtSeisanDtToDay.Text = aryJpDate[4];

            // 返却用_開始請求先コード
            this.txtFunanushiCdFr.Text = outData[5];
            if (ValChk.IsEmpty(outData[5]))
            {
                this.lblFunanushiNmFr.Text = "先　頭";
            }
            else
            {
                this.lblFunanushiNmFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text);
            }

            // 返却用_終了請求先コード
            this.txtFunanushiCdTo.Text = outData[6];
            if (ValChk.IsEmpty(outData[6]))
            {
                this.lblFunanushiNmTo.Text = "最　後";
            }
            else
            {
                this.lblFunanushiNmTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text);
            }

            // 返却用_摘要コード
            this.txtTekiyoCd.Text = outData[7];

            // 返却用_摘要
            this.txtTekiyo.Text = outData[8];
        }

        /// <summary>
        /// 設定内容によって項目の使用可否を再度制御します。
        /// </summary>
        private void ControlItemsBySettings()
        {
            //if (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenGenToriDp")) == 1
            //    || Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenGenHnDp")) == 1)
            //{
            //    this.rdbSeri.Enabled = true;
            //}
            //else
            //{
            //    this.rdbSeri.Enabled = false;
            //    this.rdbSeri.Checked = false;
            //}
            //if (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenKakeKakeToriDp")) == 1
            //    || Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenKakeKakeHnDp")) == 1
            //    || Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenKakeGenToriDp")) == 1
            //    || Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenKakeGenHnDp")) == 1)
            //{
            //    this.rdbKojo.Enabled = true;
            //}
            //else
            //{
            //    this.rdbKojo.Enabled = false;
            //    this.rdbKojo.Checked = false;
            //}
            //if (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SmbKakeToriDp")) == 1
            //    || Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SmbKakeHnDp")) == 1
            //    || Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SmbGenToriDp")) == 1
            //    || Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SmbGenHnDp")) == 1)
            //{
            //    this.rdbSeriKojo.Enabled = true;
            //}
            //else
            //{
            //    this.rdbSeriKojo.Enabled = false;
            //    this.rdbSeriKojo.Checked = false;
            //}
        }

        /// <summary>
        /// 画面値をHashTableに格納する
        /// </summary>
        /// <returns>画面値を格納したHashTable</returns>
        private Hashtable GetCondition()
        {
            Hashtable htCondition = new Hashtable();

            // 支所コード
            htCondition["ShishoCode"] = Util.ToString(txtMizuageShishoCd.Text);

            // 作成区分
            SKbn sakuseiKbn;
            if (this.rdbSeri.Checked)
            {
                sakuseiKbn = SKbn.Seri;
            }
            else if (this.rdbKojo.Checked)
            {
                sakuseiKbn = SKbn.Kojo;
            }
            else
            {
                sakuseiKbn = SKbn.None;
            }
            htCondition["SakuseiKbn"] = sakuseiKbn;

            // 精算日付From
            htCondition["SeisanDtFr"] = Util.ConvAdDate(this.lblSeisanDtFrGengo.Text,
                    Util.ToInt(this.txtSeisanDtFrJpYear.Text),
                    Util.ToInt(this.txtSeisanDtFrMonth.Text),
                    Util.ToInt(this.txtSeisanDtFrDay.Text), this.Dba);

            // 精算日付To
            htCondition["SeisanDtTo"] = Util.ConvAdDate(this.lblSeisanDtToGengo.Text,
                    Util.ToInt(this.txtSeisanDtToJpYear.Text),
                    Util.ToInt(this.txtSeisanDtToMonth.Text),
                    Util.ToInt(this.txtSeisanDtToDay.Text), this.Dba);

            // 船主コードFrom
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                htCondition["FunanushiCdFr"] = "0";
            }
            else
            {
                htCondition["FunanushiCdFr"] = this.txtFunanushiCdFr.Text;
            }

            // 船主コードTo
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                htCondition["FunanushiCdTo"] = "9999";
            }
            else
            {
                htCondition["FunanushiCdTo"] = this.txtFunanushiCdTo.Text;
            }

            // 担当者コード
            htCondition["TantoCd"] = this.txtTantoCd.Text;

            // 仕訳伝票日付
            htCondition["SwkDpyDt"] = Util.ConvAdDate(this.lblSwkDpyDtGengo.Text,
                    Util.ToInt(this.txtSwkDpyDtJpYear.Text),
                    Util.ToInt(this.txtSwkDpyDtMonth.Text),
                    Util.ToInt(this.txtSwkDpyDtDay.Text), this.Dba);

            // 摘要コード
            htCondition["TekiyoCd"] = this.txtTekiyoCd.Text;

            // 摘要
            htCondition["Tekiyo"] = this.txtTekiyo.Text;

            return htCondition;
        }

        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }

        /// <summary>
        /// 会計年度内日付に変換
        /// </summary>
        private DateTime FixNendoDate(DateTime date)
        {
            DateTime dateFr = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            DateTime dateTo = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
            if (date < dateFr)
            {
                return dateFr;
            }
            else if (date > dateTo)
            {
                return dateTo;
            }
            else
            {
                return date;
            }
        }

        /// <summary>
        /// 消費税情報チェック
        /// </summary>
        /// <returns>OK:true NG:false</returns>
        private bool IsZeiSetting()
        {
            // 1.仮受消費税に関する設定を取得(会社情報の仮受消費税コードから)
            DataTable dtKrukZei = GetVI_ZM_KANJO_KAMOKU(Util.ToString(this.UInfo.KaikeiSettings["KARIUKE_SHOHIZEI_KAMOKU_CD"]));
            // 2.仮払消費税に関する設定を取得(会社情報の仮払消費税コードから)
            DataTable dtKrbriZei = GetVI_ZM_KANJO_KAMOKU(Util.ToString(this.UInfo.KaikeiSettings["KARIBARAI_SHOHIZEI_KAMOKU_CD"]));

            if (dtKrukZei.Rows.Count == 0 || dtKrbriZei.Rows.Count == 0)
            {
                Msg.Error((dtKrukZei.Rows.Count == 0 ? "仮受" : "仮払") + "消費税情報が読込みできません。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        /// <returns>OK:true NG:false</returns>
        private bool UpdateData()
        {
            // 登録処理
            // 全項目の入力チェック
            if (!ValidateAll())
            {
                return false;
            }

            // 自動仕訳設定を取得（伝票日付）
            GetTB_HN_ZIDO_SHIWAKE_SETTEI_A(Util.ToDate(this.Condition["SwkDpyDt"]));
            GetTB_HN_ZIDO_SHIWAKE_SETTEI_B(Util.ToDate(this.Condition["SwkDpyDt"]));

            string modeNm = this._mode == 2 ? "更新" : "登録";
            if (Msg.ConfYesNo(modeNm + "しますか？") == DialogResult.No)
            {
                // 「いいえ」が押されたら処理終了
                return false;
            }

            // 更新中メッセージ表示
            HNSB1026 msgFrm = new HNSB1026();
            msgFrm.Show();
            msgFrm.Refresh();

            try
            {
                this.Dba.BeginTransaction();

                // 更新処理を実行
                HNSB1021DA da = new HNSB1021DA(this.UInfo, this.Dba, this.Config);
                bool dataExists = false;
                bool result;
                if (this.rdbSeri.Checked)
                {
                    // セリ販売の更新処理
                    result = da.MakeSeriSwkData(this._mode, this._packDpyNo, this.Condition, ref dataExists);
                }
                else if (this.rdbKojo.Checked)
                {
                    // 控除の更新処理
                    result = da.MakeKojoSwkData(this._mode, this._packDpyNo, this.Condition, this._zeiSetting, ref dataExists);
                }
                else
                {
                    // セリ販売の更新処理
                    result = da.MakeSeriSwkData(this._mode, this._packDpyNo, this.Condition, ref dataExists);
                }

                // 更新終了後、メッセージを閉じる
                msgFrm.Close();

                // 更新に失敗していればその旨表示する
                if (result)
                {
                    this.Dba.Commit();

                    Msg.Info("更新処理が終了しました。");

                    // 画面をクリア
                    InitForm();

                    return true;
                }
                else
                {
                    this.Dba.Rollback();
                    if (dataExists)
                    {
                        Msg.Error("更新に失敗しました。" + Environment.NewLine + "もう一度やり直して下さい。");
                    }
                    else
                    {
                        //Msg.Error("該当データがありません。");
                        Msg.Info("該当データがありません。");
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
            return false;
        }
        #endregion
    }
}
