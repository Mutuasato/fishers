﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsb1021
{
    /// <summary>
    /// 仕訳データ作成 動作設定(HNSB1025)
    /// </summary>
    public partial class HNSB1025 : BasePgForm
    {

        #region private変数
        // 支所
        int ShishoCode;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSB1025()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            // 画面の初期表示
            InitDisp();

            // 自動仕訳設定Aの取得(件数のみ)
            // TODO:部門有無の判断?
            DataTable dtSettei = GetTB_HN_ZIDO_SHIWAKE_SETTEI_A();

            // 購買に関する部門の取得(部門の初期表示のため?)
            if (Util.ToInt(dtSettei.Rows[0]["件数"]) > 0)
            {
                DataTable dtBumon = GetVI_ZM_BUMON();
                if (dtBumon.Rows.Count > 0)
                {
                    this.txtBumonCd.Text = Util.ToString(dtBumon.Rows[0]["BUMON_CD"]);
                    this.lblBumonNm.Text = Util.ToString(dtBumon.Rows[0]["BUMON_NM"]);
                }
            }

            // 現金に対する掛取引は無効
            this.chkGenKakeToriDp.Enabled = false;
            this.chkGenKakeHnDp.Enabled = false;
            // 初期フォーカス
            this.chkGenGenToriDp.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtBumonCd":
                case "txtTekiyoCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            switch (this.ActiveCtlNm)
            {
                case "txtBumonCd":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("COMC8011.exe");
                    //asm = Assembly.LoadFrom("CMCM2041.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "TB_CM_BUMON";
                            //frm.InData = this.txtBumonCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtBumonCd.Text = result[0];
                                this.lblBumonNm.Text = result[1];

                                // 次の項目(摘要コード)にフォーカス
                                this.txtTekiyoCd.Focus();
                            }
                        }
                    }
                    break;

                case "txtTekiyoCd":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("SKDC9051.exe");
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2061.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.skd.skdc9051.SKDC9051");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2061.CMCM2061");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = Util.ToString(this.ShishoCode);
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTekiyoCd.Text = outData[0];
                                this.txtTekiyo.Text = outData[1];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F6押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 各項目の入力チェック
            if (!ValidateAll())
            {
                return;
            }

            // 確認メッセージを表示
            if (Msg.ConfYesNo("更新しますか？") == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            // 更新処理
            // 内部で保持してるっぽい
            SaveSettings();

            // 画面を閉じる
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 部門コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCd_Validating(object sender, CancelEventArgs e)
        {
            // 名称を表示
            string name = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", this.ShishoCode.ToString(), this.txtBumonCd.Text);
            this.lblBumonNm.Text = name;

            // 空白はOKとみなしチェックしない
            if (ValChk.IsEmpty(this.txtBumonCd.Text) || this.txtBumonCd.Text == "0")
            {
                this.lblBumonNm.Text = "";
            }
        }

        /// <summary>
        /// 摘要の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyo())
            {
                e.Cancel = true;
                this.txtTekiyo.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 初期情報の表示
        /// </summary>
        private void InitDisp()
        {
            switch (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SwkDpMk")))
            {
                case 1:
                    this.rdbFukugoDp.Checked = true;
                    break;

                case 2:
                    this.rdbTanitsuDp.Checked = true;
                    break;

                case 3:
                    this.rdbSeikyuDp.Checked = true;
                    break;
            }
            this.txtTekiyoCd.Text = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "TekiyoCd");
            this.txtTekiyo.Text = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "Tekiyo");

            this.txtBumonCd.Text = this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "BumonCd");
            // 名称を表示
            string name = "";
            try
            {
                name = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", this.ShishoCode.ToString(), this.txtBumonCd.Text);
            }
            catch (Exception) { }
            this.lblBumonNm.Text = name;
        }

        /// <summary>
        /// 自動仕訳設定Aの有無を取得
        /// </summary>
        /// <returns>取得したデータ</returns>
        private DataTable GetTB_HN_ZIDO_SHIWAKE_SETTEI_A()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" COUNT(*) AS 件数 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_ZIDO_SHIWAKE_SETTEI_A AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_ZM_KANJO_KAMOKU AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");

            //sql.Append(" A.DENPYO_KUBUN = 2 AND"); // 3か？
            sql.Append(" A.DENPYO_KUBUN = 3 AND");

            sql.Append(" B.BUMON_UMU = 1 ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 部門の設定を取得
        /// </summary>
        /// <returns>取得したデータ</returns>
        private DataTable GetVI_ZM_BUMON()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" * ");
            sql.Append("FROM");
            sql.Append(" VI_ZM_BUMON ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" BUMON_CD = @BUMON_CD");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            int bmnCd = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "BumonCd")));
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, bmnCd);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 部門コード
            if (!IsValidBumonCd())
            {
                this.txtBumonCd.SelectAll();
                this.txtBumonCd.Focus();
                return false;
            }

            // 摘要コード
            if (!IsValidTekiyoCd())
            {
                this.txtTekiyoCd.SelectAll();
                this.txtTekiyoCd.Focus();
                return false;
            }

            // 摘要
            if (!IsValidTekiyo())
            {
                this.txtTekiyo.SelectAll();
                this.txtTekiyo.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidBumonCd()
        {
            // 空白はOKとみなしチェックしない
            if (ValChk.IsEmpty(this.txtBumonCd.Text) || this.txtBumonCd.Text == "0")
            {
                this.lblBumonNm.Text = "";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBumonCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたらエラー)
            string name = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", this.ShishoCode.ToString(), this.txtBumonCd.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            this.lblBumonNm.Text = name;

            return true;
        }

        /// <summary>
        /// 摘要コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTekiyoCd()
        {
            // 未入力はOK
            if (ValChk.IsEmpty(this.txtTekiyoCd.Text))
            {
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTekiyoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            string name = this.Dba.GetName(this.UInfo, "TB_HN_TEKIYO", this.ShishoCode.ToString(), this.txtTekiyoCd.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            this.txtTekiyo.Text = name;

            return true;
        }

        /// <summary>
        /// 摘要の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTekiyo()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTekiyo.Text, this.txtTekiyo.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 設定を保存する
        /// </summary>
        private void SaveSettings()
        {
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenGenToriDp",
            //    this.chkGenGenToriDp.Checked ? "1" : "0");
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenGenHnDp",
            //    this.chkGenGenHnDp.Checked ? "1" : "0");
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenKakeKakeToriDp",
            //    this.chkGenKakeKakeToriDp.Checked ? "1" : "0");
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenKakeKakeHnDp",
            //    this.chkGenKakeKakeHnDp.Checked ? "1" : "0");
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenKakeGenToriDp",
            //    this.chkGenKakeGenToriDp.Checked ? "1" : "0");
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "GenKakeGenHnDp",
            //    this.chkGenKakeGenHnDp.Checked ? "1" : "0");
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SmbKakeToriDp",
            //    this.chkSmbKakeToriDp.Checked ? "1" : "0");
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SmbKakeHnDp",
            //    this.chkSmbKakeHnDp.Checked ? "1" : "0");
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SmbGenToriDp",
            //    this.chkSmbGenToriDp.Checked ? "1" : "0");
            //this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SmbGenHnDp",
            //    this.chkSmbGenHnDp.Checked ? "1" : "0");
            string swkDpMk = "";
            if (this.rdbFukugoDp.Checked)
            {
                swkDpMk = "1";
            }
            else if (this.rdbTanitsuDp.Checked)
            {
                swkDpMk = "2";
            }
            else if (this.rdbSeikyuDp.Checked)
            {
                swkDpMk = "3";
            }
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SwkDpMk", swkDpMk);
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "BumonCd", this.txtBumonCd.Text);
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "TekiyoCd", this.txtTekiyoCd.Text);
            this.Config.SetPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "Tekiyo", this.txtTekiyo.Text);

            // 設定を保存する
            this.Config.SaveConfig();
        }
        #endregion
    }
}
