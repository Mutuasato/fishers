﻿namespace jp.co.fsi.hn.hnyr1071
{
    partial class HNYR1071
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblDateMonth = new System.Windows.Forms.Label();
			this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateYear = new System.Windows.Forms.Label();
			this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengo = new System.Windows.Forms.Label();
			this.lblFunanushiCdTo = new System.Windows.Forms.Label();
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiCdFr = new System.Windows.Forms.Label();
			this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 609);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.Text = "";
			// 
			// lblDateMonth
			// 
			this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateMonth.ForeColor = System.Drawing.Color.Black;
			this.lblDateMonth.Location = new System.Drawing.Point(288, 2);
			this.lblDateMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateMonth.Name = "lblDateMonth";
			this.lblDateMonth.Size = new System.Drawing.Size(23, 24);
			this.lblDateMonth.TabIndex = 5;
			this.lblDateMonth.Tag = "CHANGE";
			this.lblDateMonth.Text = "月";
			this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateMonth
			// 
			this.txtDateMonth.AutoSizeFromLength = false;
			this.txtDateMonth.DisplayLength = null;
			this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDateMonth.ForeColor = System.Drawing.Color.Black;
			this.txtDateMonth.Location = new System.Drawing.Point(245, 3);
			this.txtDateMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateMonth.MaxLength = 2;
			this.txtDateMonth.Name = "txtDateMonth";
			this.txtDateMonth.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonth.TabIndex = 4;
			this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
			// 
			// lblDateYear
			// 
			this.lblDateYear.BackColor = System.Drawing.Color.Silver;
			this.lblDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateYear.ForeColor = System.Drawing.Color.Black;
			this.lblDateYear.Location = new System.Drawing.Point(217, 2);
			this.lblDateYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateYear.Name = "lblDateYear";
			this.lblDateYear.Size = new System.Drawing.Size(23, 24);
			this.lblDateYear.TabIndex = 3;
			this.lblDateYear.Tag = "CHANGE";
			this.lblDateYear.Text = "年";
			this.lblDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateYear
			// 
			this.txtDateYear.AutoSizeFromLength = false;
			this.txtDateYear.DisplayLength = null;
			this.txtDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDateYear.ForeColor = System.Drawing.Color.Black;
			this.txtDateYear.Location = new System.Drawing.Point(175, 3);
			this.txtDateYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateYear.MaxLength = 2;
			this.txtDateYear.Name = "txtDateYear";
			this.txtDateYear.Size = new System.Drawing.Size(39, 23);
			this.txtDateYear.TabIndex = 2;
			this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
			// 
			// lblDateGengo
			// 
			this.lblDateGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateGengo.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengo.Location = new System.Drawing.Point(115, 2);
			this.lblDateGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateGengo.Name = "lblDateGengo";
			this.lblDateGengo.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengo.TabIndex = 1;
			this.lblDateGengo.Tag = "DISPNAME";
			this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiCdTo
			// 
			this.lblFunanushiCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFunanushiCdTo.Location = new System.Drawing.Point(564, 2);
			this.lblFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
			this.lblFunanushiCdTo.Size = new System.Drawing.Size(289, 24);
			this.lblFunanushiCdTo.TabIndex = 4;
			this.lblFunanushiCdTo.Tag = "DISPNAME";
			this.lblFunanushiCdTo.Text = "最　後";
			this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblCodeBet.Location = new System.Drawing.Point(472, 2);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 24);
			this.lblCodeBet.TabIndex = 2;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdFr
			// 
			this.txtFunanushiCdFr.AutoSizeFromLength = false;
			this.txtFunanushiCdFr.DisplayLength = null;
			this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtFunanushiCdFr.Location = new System.Drawing.Point(116, 4);
			this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiCdFr.MaxLength = 4;
			this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
			this.txtFunanushiCdFr.Size = new System.Drawing.Size(52, 23);
			this.txtFunanushiCdFr.TabIndex = 0;
			this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
			// 
			// lblFunanushiCdFr
			// 
			this.lblFunanushiCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblFunanushiCdFr.Location = new System.Drawing.Point(173, 2);
			this.lblFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
			this.lblFunanushiCdFr.Size = new System.Drawing.Size(289, 24);
			this.lblFunanushiCdFr.TabIndex = 1;
			this.lblFunanushiCdFr.Tag = "DISPNAME";
			this.lblFunanushiCdFr.Text = "先　頭";
			this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdTo
			// 
			this.txtFunanushiCdTo.AutoSizeFromLength = false;
			this.txtFunanushiCdTo.DisplayLength = null;
			this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtFunanushiCdTo.Location = new System.Drawing.Point(507, 4);
			this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiCdTo.MaxLength = 4;
			this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
			this.txtFunanushiCdTo.Size = new System.Drawing.Size(52, 23);
			this.txtFunanushiCdTo.TabIndex = 3;
			this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
			this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(115, 3);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(161, 2);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(879, 29);
			this.lblMizuageShisho.TabIndex = 0;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "水揚支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(879, 30);
			this.label2.TabIndex = 902;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "年指定";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(879, 31);
			this.label3.TabIndex = 902;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "船主CD範囲";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(889, 118);
			this.fsiTableLayoutPanel1.TabIndex = 903;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblFunanushiCdTo);
			this.fsiPanel3.Controls.Add(this.txtFunanushiCdFr);
			this.fsiPanel3.Controls.Add(this.lblCodeBet);
			this.fsiPanel3.Controls.Add(this.txtFunanushiCdTo);
			this.fsiPanel3.Controls.Add(this.lblFunanushiCdFr);
			this.fsiPanel3.Controls.Add(this.label3);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 82);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(879, 31);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblDateGengo);
			this.fsiPanel2.Controls.Add(this.lblDateMonth);
			this.fsiPanel2.Controls.Add(this.txtDateYear);
			this.fsiPanel2.Controls.Add(this.lblDateYear);
			this.fsiPanel2.Controls.Add(this.txtDateMonth);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 43);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(879, 30);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(879, 29);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNYR1071
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNYR1071";
			this.Text = "";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private System.Windows.Forms.Label lblDateYear;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblDateMonth;
        private common.controls.FsiTextBox txtDateMonth;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}