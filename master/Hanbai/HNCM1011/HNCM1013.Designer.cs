﻿namespace jp.co.fsi.hn.hncm1011
{
    partial class HNCM1013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblNakagaininCdTo = new System.Windows.Forms.Label();
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.txtNakagaininCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblNakagaininCdFr = new System.Windows.Forms.Label();
			this.txtNakagaininCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF2
			// 
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Text = "F3\r\n\r\nさくじょ";
			this.btnF3.Visible = false;
			// 
			// btnF7
			// 
			this.btnF7.Visible = false;
			// 
			// btnF6
			// 
			this.btnF6.Text = "F6\r\n\r\nとうろく";
			this.btnF6.Visible = false;
			// 
			// btnF8
			// 
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(7, 73);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(996, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(985, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblNakagaininCdTo
			// 
			this.lblNakagaininCdTo.BackColor = System.Drawing.Color.Silver;
			this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininCdTo.Location = new System.Drawing.Point(548, 4);
			this.lblNakagaininCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
			this.lblNakagaininCdTo.Size = new System.Drawing.Size(289, 27);
			this.lblNakagaininCdTo.TabIndex = 4;
			this.lblNakagaininCdTo.Tag = "DISPNAME";
			this.lblNakagaininCdTo.Text = "最　後";
			this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBet.Location = new System.Drawing.Point(456, 4);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 27);
			this.lblCodeBet.TabIndex = 2;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNakagaininCdFr
			// 
			this.txtNakagaininCdFr.AutoSizeFromLength = false;
			this.txtNakagaininCdFr.DisplayLength = null;
			this.txtNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNakagaininCdFr.Location = new System.Drawing.Point(105, 5);
			this.txtNakagaininCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtNakagaininCdFr.MaxLength = 4;
			this.txtNakagaininCdFr.Name = "txtNakagaininCdFr";
			this.txtNakagaininCdFr.Size = new System.Drawing.Size(52, 23);
			this.txtNakagaininCdFr.TabIndex = 0;
			this.txtNakagaininCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCdFr_Validating);
			// 
			// lblNakagaininCdFr
			// 
			this.lblNakagaininCdFr.BackColor = System.Drawing.Color.Silver;
			this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininCdFr.Location = new System.Drawing.Point(163, 4);
			this.lblNakagaininCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
			this.lblNakagaininCdFr.Size = new System.Drawing.Size(289, 27);
			this.lblNakagaininCdFr.TabIndex = 1;
			this.lblNakagaininCdFr.Tag = "DISPNAME";
			this.lblNakagaininCdFr.Text = "先　頭";
			this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNakagaininCdTo
			// 
			this.txtNakagaininCdTo.AutoSizeFromLength = false;
			this.txtNakagaininCdTo.DisplayLength = null;
			this.txtNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNakagaininCdTo.Location = new System.Drawing.Point(491, 6);
			this.txtNakagaininCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtNakagaininCdTo.MaxLength = 4;
			this.txtNakagaininCdTo.Name = "txtNakagaininCdTo";
			this.txtNakagaininCdTo.Size = new System.Drawing.Size(52, 23);
			this.txtNakagaininCdTo.TabIndex = 3;
			this.txtNakagaininCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCdTo_Validating);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(11, 24);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 1;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(944, 42);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.lblNakagaininCdTo);
			this.fsiPanel1.Controls.Add(this.txtNakagaininCdFr);
			this.fsiPanel1.Controls.Add(this.lblCodeBet);
			this.fsiPanel1.Controls.Add(this.txtNakagaininCdTo);
			this.fsiPanel1.Controls.Add(this.lblNakagaininCdFr);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(936, 34);
			this.fsiPanel1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(936, 34);
			this.label1.TabIndex = 2;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "仲買人CD範囲";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// HNCM1013
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(985, 211);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1013";
			this.ShowFButton = true;
			this.Text = "仲買人コード範囲";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtNakagaininCdFr;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.FsiTextBox txtNakagaininCdTo;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel1;
		private System.Windows.Forms.Label label1;
	}
}