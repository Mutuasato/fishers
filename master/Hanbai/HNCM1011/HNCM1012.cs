﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.mynumber;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Security.Cryptography;

namespace jp.co.fsi.hn.hncm1011
{
    /// <summary>
    /// 仲買人マスタ登録(HNCM1012)
    /// </summary>
    public partial class HNCM1012 : BasePgForm
    {

        #region ログ
        readonly static log4net.ILog log =
            log4net.LogManager.GetLogger(
                System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion


        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";

        /// <summary>
        /// SQL関連
        /// </summary>
        private const int KAISHA_CDDE = 1; // 会社コード
        private const int TORIHIKISAKI_KUBUN2 = 2; // 取引先区分２
        #endregion

        #region プロパティ
        /// <summary>
        /// 公開フラグ用変数
        /// </summary>
        private bool _numberFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._numberFlg;
            }
        }
        #endregion
        
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1012()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1012(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            DataRow roginUser = GetroginUserdetail();//ログインしている人のユーザ情報取得
            if (Util.ToInt(roginUser["USER_KUBUN"]).Equals(1))// 管理者権限のみ公開ボタンを表示
            {
                this.button1.Visible = true;
                this.txtMyNumber.ReadOnly = false;
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;

            this.ShowFButton = true;

            // 引数：Par1／モード(1:新規、2:変更)、InData：仕入先コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // ボタンを調整
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 郵便番号１・２、住所１・２、担当者コードに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtYubinBango1":
                case "txtYubinBango2":
                case "txtJusho1":
                case "txtJusho2":
                case "txtTantoshaCd":
                case "txtShohizeiNyuryokuHoho":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理（検索）
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtYubinBango1":
                case "txtYubinBango2":
                case "txtJusho1":
                case "txtJusho2":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1031.CMCM1031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.ShowDialog();

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtYubinBango1.Text = outData[0];
                                this.txtYubinBango2.Text = outData[1];
                                this.txtJusho1.Text = outData[2];
                            }
                        }
                    }
                    break;

                case "txtTantoshaCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoshaCd.Text = outData[0];
                                this.lblTantoshaNm.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShohizeiNyuryokuHoho":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO";
                            frm.InData = this.txtShohizeiNyuryokuHoho.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtShohizeiNyuryokuHoho.Text = result[0];
                                this.lblShzNrkHohoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理（削除）
        /// </summary>
        public override void PressF3()
        {
            // 新規モード時
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 6, this.txtNakagaininCd.Text);
            dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 4);
            try
            {
                this.Dba.BeginTransaction();
                // マイナンバーデータ削除
                this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @NAKAGAININ_CD AND KUBUN = @KUBUN", dpc);
                this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @NAKAGAININ_CD AND KUBUN = @KUBUN", dpc);
                
                // データ削除
                // 取引先
                this.Dba.Delete("TB_CM_TORIHIKISAKI",
                    "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                    (DbParamCollection)delParams[0]);
                // 取引先情報
                this.Dba.Delete("TB_HN_TORIHIKISAKI_JOHO",
                    "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                    (DbParamCollection)delParams[0]);
                // 納品先
                this.Dba.Delete("TB_HN_NOHINSAKI",
                    "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                    (DbParamCollection)delParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();

                // DialogResultに「OK」をセットし結果を返却
                this.DialogResult = DialogResult.OK;

            }
            catch (Exception e)
            {


                log.Error("HNCM1012でエラー" + e.Message, e);

                // ロールバック
                this.Dba.Rollback();

                // 失敗通知ダイアログ
                Msg.Error("削除に失敗しました。");
            }
            finally
            {
                this.Close();
            }

        }

        /// <summary>
        /// F6キー押下時処理（登録）
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string modeText = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新");
            string msg = modeText + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTori = SetCmToriParams();
            ArrayList alParamsHnTori = SetHnToriParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // マイナンバー登録
                    Mynumber.InsertNumber(Util.ToString(txtMyNumber.Text),4, Util.ToInt(txtNakagaininCd.Text), 0,this.Dba);
                    // データ登録
                    // 共通.取引先マスタ
                    this.Dba.Insert("TB_CM_TORIHIKISAKI", (DbParamCollection)alParamsCmTori[0]);
                    // 販売.取引先情報
                    this.Dba.Insert("TB_HN_TORIHIKISAKI_JOHO", (DbParamCollection)alParamsHnTori[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // 削除用パラメータ
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                    dpc.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 6, this.txtNakagaininCd.Text);
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 4);
                    //dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 4);
                    // マイナンバー削除
                    this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @NAKAGAININ_CD AND KUBUN = @KUBUN", dpc);
                    this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @NAKAGAININ_CD AND KUBUN = @KUBUN", dpc);
                    // マイナンバー更新
                    Mynumber.InsertNumber(Util.ToString(txtMyNumber.Text), 4, Util.ToInt(txtNakagaininCd.Text), 0,this.Dba);
                    // データ更新
                    // 共通.取引先マスタ
                    this.Dba.Update("TB_CM_TORIHIKISAKI",
                        (DbParamCollection)alParamsCmTori[1],
                        "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                        (DbParamCollection)alParamsCmTori[0]);
                    // 販売.取引先情報
                    this.Dba.Update("TB_HN_TORIHIKISAKI_JOHO",
                        (DbParamCollection)alParamsHnTori[1],
                        "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                        (DbParamCollection)alParamsHnTori[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();

                // DialogResultに「OK」をセットし結果を返却
                this.DialogResult = DialogResult.OK;

            }
            catch (Exception e)
            {
                // ロールバック
                this.Dba.Rollback();

                // 失敗通知ダイアログ
                Msg.Error(modeText + "が失敗しました。");
            }
            finally
            {
                // 閉じる
                this.Close();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// マイナンバーの公開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.FsiTextBox1.Visible = false;
                this.txtMyNumber.ReadOnly = false;
                _numberFlg = true;
                this.button1.Text = "非公開";
            }
            else
            {
                this.FsiTextBox1.Visible = true;
                this.txtMyNumber.ReadOnly = true;
                _numberFlg = false;
                this.button1.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMyNumber_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMyNumber.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMyNumber.SelectAll();
                e.Cancel = true;
                return;
            }

            if (!IsValidMyNumber())
            {
                e.Cancel = true;
                this.txtMyNumber.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCd())
            {
                e.Cancel = true;
                this.txtNakagaininCd.SelectAll();
            }
        }

        /// <summary>
        /// 正式仲買人名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininNm())
            {
                e.Cancel = true;
                this.txtNakagaininNm.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininKanaNm())
            {
                e.Cancel = true;
                this.txtNakagaininKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 略称仲買人名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRyNakagaininNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidRyNakagaininNm())
            {
                e.Cancel = true;
                this.txtRyNakagaininNm.SelectAll();
            }
        }

        /// <summary>
        /// 郵便番号(上3桁)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango1())
            {
                e.Cancel = true;
                this.txtYubinBango1.SelectAll();
            }
        }

        /// <summary>
        /// 郵便番号(下4桁)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango2())
            {
                e.Cancel = true;
                this.txtYubinBango2.SelectAll();
            }
        }

        /// <summary>
        /// 住所１の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho1())
            {
                e.Cancel = true;
                this.txtJusho1.SelectAll();
            }
        }

        /// <summary>
        /// 住所２の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho2())
            {
                e.Cancel = true;
                this.txtJusho2.SelectAll();
            }
        }

        /// <summary>
        /// 電話番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTel_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTel())
            {
                e.Cancel = true;
                this.txtTel.SelectAll();
            }
        }

        /// <summary>
        /// FAX番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFax_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFax())
            {
                e.Cancel = true;
                this.txtFax.SelectAll();
            }
        }

        /// <summary>
        /// 担当者コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoshaCd())
            {
                e.Cancel = true;
                this.txtTantoshaCd.SelectAll();
            }
        }

        /// <summary>
        /// 単価取得方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTankaShutokuHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTankaShutokuHoho())
            {
                e.Cancel = true;
                this.txtTankaShutokuHoho.SelectAll();
            }
        }

        /// <summary>
        /// 金額端数処理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKingakuHasuShori_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKingakuHasuShori())
            {
                e.Cancel = true;
                this.txtKingakuHasuShori.SelectAll();
            }
        }

        /// <summary>
        /// 締日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShimebi_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShimebi())
            {
                e.Cancel = true;
                this.txtShimebi.SelectAll();
            }
        }

        /// <summary>
        /// 請求書形式の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeikyushoKeishiki_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeikyushoKeishiki())
            {
                e.Cancel = true;
                this.txtSeikyushoKeishiki.SelectAll();
            }
        }

        /// <summary>
        /// 消費税端数処理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiHasuShori_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiHasuShori())
            {
                e.Cancel = true;
                this.txtShohizeiHasuShori.SelectAll();
            }
        }

        /// <summary>
        /// 回収月の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKaishuTsuki_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKaishuTsuki())
            {
                e.Cancel = true;
                this.txtKaishuTsuki.SelectAll();
            }
        }

        /// <summary>
        /// 回収日の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKaishuBi_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKaishuBi())
            {
                e.Cancel = true;
                this.txtKaishuBi.SelectAll();
            }
        }

        /// <summary>
        /// 一覧表示の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txthyoji_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidhyoji())
            {
                e.Cancel = true;
                this.txthyoji.SelectAll();
            }

        }

        /// <summary>
        /// 請求書発行の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeikyushoHakko_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeikyushoHakko())
            {
                e.Cancel = true;
                this.txtSeikyushoHakko.SelectAll();
            }
        }

        /// <summary>
        /// 消費税転嫁方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiTenkaHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiTenkaHoho())
            {
                e.Cancel = true;
                this.txtShohizeiTenkaHoho.SelectAll();
            }
        }

        private void txthyoji_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 今ログインしている人のデータを取得
        /// </summary>
        /// <returns>roginUser</returns>
        private DataRow GetroginUserdetail()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.UInfo.UserCd);
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT ");
            sql.Append(" * ");
            sql.Append(" FROM ");
            sql.Append(" TB_CM_TANTOSHA ");
            sql.Append(" WHERE ");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            sql.Append(" TANTOSHA_CD = @TANTOSHA_CD ");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if (dt.Rows.Count == 0)
            {
                Msg.Info("ユーザ情報取得に失敗しました。");
            }

            return dt.Rows[0];
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装
            string ret = "";

            // 仲買人コードの初期値を取得
            // 仲買人の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            //dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, KAISHA_CDDE); // 会社コードは1
            //StringBuilder where = new StringBuilder("TORIHIKISAKI_KUBUN2 = " + TORIHIKISAKI_KUBUN2.ToString()); // 取引先区分２は2
            //where.Append(" AND KAISHA_CD = @KAISHA_CD");
            //DataTable dtMaxFnns =
            //    this.Dba.GetDataTableByConditionWithParams("MAX(TORIHIKISAKI_CD) AS MAX_CD",
            //        "TB_HN_TORIHIKISAKI_JOHO", Util.ToString(where), dpc);
            // 取引先コードで登録されていない最小の番号を初期表示
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            DataTable dtMaxFnns =
                this.Dba.GetDataTableByConditionWithParams("MIN(TORIHIKISAKI_CD)+1 AS MAX_CD",
                    "VI_HN_TORIHIKISAKI_JOHO",
                    "KAISHA_CD = @KAISHA_CD AND (TORIHIKISAKI_CD + 1) NOT IN (SELECT TORIHIKISAKI_CD FROM VI_HN_TORIHIKISAKI_JOHO WHERE SHUBETU_KUBUN = 1 AND KAISHA_CD = @KAISHA_CD)",
                    dpc);
            if (dtMaxFnns.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxFnns.Rows[0]["MAX_CD"]))
            {
                //this.txtNakagaininCd.Text = Util.ToString(Util.ToInt(dtMaxFnns.Rows[0]["MAX_CD"]) + 1);
                this.txtNakagaininCd.Text = Util.ToString(Util.ToInt(dtMaxFnns.Rows[0]["MAX_CD"]));
            }
            else
            {
                this.txtNakagaininCd.Text = "1001";
            }
            // 仲買人コードの値を請求先コードにコピー
            this.txtSeikyusakiCd.Text = this.txtNakagaininCd.Text;

            // 各項目の初期値を設定
            // 担当者コード0
            this.txtTantoshaCd.Text = "0";
            // 単価取得方法0
            //this.txtTankaShutokuHoho.Text = "0";
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "TankaShutokuHoho"));
            this.txtTankaShutokuHoho.Text = ret;
            // 金額端数処理2
            //this.txtKingakuHasuShori.Text = "2";
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "KingakuHasuShori"));
            this.txtKingakuHasuShori.Text = ret;
            // 消費税入力方法は入力不可
            //// this.txtShohizeiNyuryokuHoho.Text = "2";
            //// this.lblShzNrkHohoNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", this.txtShohizeiNyuryokuHoho.Text);
            //this.txtShohizeiNyuryokuHoho.Enabled = false;
            //this.lblShzNrkHohoNm.Enabled = false;
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiNyuryokuHoho"));
            this.txtShohizeiNyuryokuHoho.Text = ret;
            this.lblShzNrkHohoNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text);

            // 締日99
            this.txtShimebi.Text = "99";
            // 請求書発行1
            this.txtSeikyushoHakko.Text = "1";
            // 請求書形式2
            this.txtSeikyushoKeishiki.Text = "2";
            // 消費税端数処理2
            //this.txtShohizeiHasuShori.Text = "2";
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiHasuShori"));
            this.txtShohizeiHasuShori.Text = ret;
            // 消費税転嫁方法は入力不可
            //this.txtShohizeiTenkaHoho.Text = "2";
            //// this.txtShohizeiTenkaHoho.Enabled = true;
            //this.lblShohizeiTenkaHoho.Enabled = false;
            //this.txtShohizeiTenkaHoho.Enabled = false;
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiTenkaHoho"));
            this.txtShohizeiTenkaHoho.Text = ret;
            // 回収月0
            this.txtKaishuTsuki.Text = "0";
            // 回収日99
            this.txtKaishuBi.Text = "99";
            // 一覧表示
            this.txthyoji.Text = "0";

            // 請求先コードは入力不可
            this.lblSeikyusakiCd.Enabled = false;
            this.txtSeikyusakiCd.Enabled = false;

            // 正式仲買人名に初期フォーカス
            this.ActiveControl = this.txtNakagaininNm;
            this.txtNakagaininNm.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;

        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            //マイナンバー取得
            txtMyNumber.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 4, 0, this.Dba);
            if (!ValChk.IsEmpty(Util.ToString(txtMyNumber.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtMyNumber.ReadOnly = true;
                this.FsiTextBox1.Visible = true;
            }

            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.TORIHIKISAKI_CD"); // 仲買人コード
            cols.Append(" ,A.SEIKYUSAKI_CD"); // 請求先コード
            cols.Append(" ,A.TANKA_SHUTOKU_HOHO"); // 単価取得方法
            cols.Append(" ,A.KINGAKU_HASU_SHORI"); //　金額端数処理
            cols.Append(" ,A.SHOHIZEI_NYURYOKU_HOHO"); // 消費税入力方法
            cols.Append(" ,B.TORIHIKISAKI_NM"); // 正式仲買人名
            cols.Append(" ,B.TORIHIKISAKI_KANA_NM"); // 仲買人カナ名
            cols.Append(" ,B.RYAKUSHO"); // 略称仲買人名
            cols.Append(" ,B.YUBIN_BANGO1"); // 郵便番号1
            cols.Append(" ,B.YUBIN_BANGO2"); // 郵便番号2
            cols.Append(" ,B.JUSHO1"); // 住所1
            cols.Append(" ,B.JUSHO2"); // 住所2
            cols.Append(" ,B.DENWA_BANGO"); // 電話番号
            cols.Append(" ,B.FAX_BANGO"); // FAX番号
            cols.Append(" ,B.TANTOSHA_CD"); // 担当者コード
            cols.Append(" ,B.SIMEBI"); // 締日
            cols.Append(" ,B.SEIKYUSHO_HAKKO"); // 請求書発行
            cols.Append(" ,B.SEIKYUSHO_KEISHIKI"); // 請求書形式
            cols.Append(" ,B.SHOHIZEI_HASU_SHORI"); // 消費税端数処理
            cols.Append(" ,B.SHOHIZEI_TENKA_HOHO"); // 消費税転嫁方法
            cols.Append(" ,B.KAISHU_TSUKI"); // 回収月
            cols.Append(" ,B.KAISHU_BI"); // 回収日
            cols.Append(" ,B.HYOJI_FLG"); // 回収日

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_TORIHIKISAKI_JOHO AS A");
            from.Append(" INNER JOIN");
            from.Append(" TB_CM_TORIHIKISAKI AS B");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            from.Append(" AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.TORIHIKISAKI_CD = @TORIHIKISAKI_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtNakagaininCd.Text = Util.ToString(drDispData["TORIHIKISAKI_CD"]); // 仲買人コード
            this.txtNakagaininNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]); // 正式仲買人名
            this.txtNakagaininKanaNm.Text = Util.ToString(drDispData["TORIHIKISAKI_KANA_NM"]); // 仲買人カナ名
            this.txtRyNakagaininNm.Text = Util.ToString(drDispData["RYAKUSHO"]); // 略称仲買人名
            this.txtYubinBango1.Text = Util.ToString(drDispData["YUBIN_BANGO1"]); // 郵便番号1
            this.txtYubinBango2.Text = Util.ToString(drDispData["YUBIN_BANGO2"]); // 郵便番号2
            this.txtJusho1.Text = Util.ToString(drDispData["JUSHO1"]); // 住所1
            this.txtJusho2.Text = Util.ToString(drDispData["JUSHO2"]); // 住所2
            this.txtTel.Text = Util.ToString(drDispData["DENWA_BANGO"]); // 電話番号
            this.txtFax.Text = Util.ToString(drDispData["FAX_BANGO"]); // FAX番号
            this.txtTantoshaCd.Text = Util.ToString(drDispData["TANTOSHA_CD"]); // 担当者コード
            if (ValChk.IsNumber(this.txtTantoshaCd.Text) && !ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                this.lblTantoshaNm.Text =
                    this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "", this.txtTantoshaCd.Text); // 担当者コード
            }
            this.txtTankaShutokuHoho.Text = Util.ToString(drDispData["TANKA_SHUTOKU_HOHO"]); // 単価取得方法
            this.txtKingakuHasuShori.Text = Util.ToString(drDispData["KINGAKU_HASU_SHORI"]); // 金額端数処理
            this.txtShohizeiNyuryokuHoho.Text = Util.ToString(drDispData["SHOHIZEI_NYURYOKU_HOHO"]); // 消費税入力方法
            if (this.txtShohizeiNyuryokuHoho.Text != "")
            { 
                this.lblShzNrkHohoNm.Text =
                this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text); // 消費税入力方法
            }
            this.txtSeikyusakiCd.Text = Util.ToString(drDispData["SEIKYUSAKI_CD"]); // 請求先コード
            this.lblSeikyusakiNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]); // 請求先名
            this.txtShimebi.Text = Util.ToString(drDispData["SIMEBI"]); // 締日
            this.txtSeikyushoHakko.Text = Util.ToString(drDispData["SEIKYUSHO_HAKKO"]); // 請求書発行
            this.txtSeikyushoKeishiki.Text = Util.ToString(drDispData["SEIKYUSHO_KEISHIKI"]); // 請求書形式
            this.txtShohizeiHasuShori.Text = Util.ToString(drDispData["SHOHIZEI_HASU_SHORI"]); // 消費税端数処理
            this.txtShohizeiTenkaHoho.Text = Util.ToString(drDispData["SHOHIZEI_TENKA_HOHO"]); // 消費税転嫁方法
            this.txtKaishuTsuki.Text = Util.ToString(drDispData["KAISHU_TSUKI"]); // 回収月
            this.txtKaishuBi.Text = Util.ToString(drDispData["KAISHU_BI"]); // 回収日
            this.txthyoji.Text = Util.ToString(drDispData["HYOJI_FLG"]); // 表示区分

            // 仲買人コード・消費税入力方法・請求先コード・消費税転嫁方法は入力不可
            //this.lblShzNrkHohoNm.Enabled = false;
            //this.txtShohizeiNyuryokuHoho.Enabled = false;
            this.lblNakagaininCd.Enabled = false;
            this.txtNakagaininCd.Enabled = false;
            //this.lblSeikyusakiCd.Enabled = false;
            //this.txtSeikyusakiCd.Enabled = false;
            //this.lblShohizeiTenkaHoho.Enabled = false;
            //this.txtShohizeiTenkaHoho.Enabled = false;

            // 削除ボタン制御を実装
            this.btnF3.Enabled = IsValidNakagaininCd(this.txtNakagaininCd.Text);
            //// TB_ZM_SHIWAKE_MEISAIのデータを取得
            //DbParamCollection dpcDenpyo = new DbParamCollection();
            //StringBuilder Sql = new StringBuilder();
            //Sql.Append(" SELECT");
            //Sql.Append(" DENPYO_BANGO");
            //Sql.Append(" FROM");
            //Sql.Append(" TB_HN_TORIHIKI_DENPYO");
            //Sql.Append(" WHERE");
            //Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            //Sql.Append(" (TOKUISAKI_CD = @TORIHIKISAKI_CD OR SEIKYUSAKI_CD = @TORIHIKISAKI_CD)");
            //dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            //dpcDenpyo.SetParam("@TORIHIKISAKI_CD", SqlDbType.VarChar, 6, this.txtNakagaininCd.Text);

            //DataTable dtDenpyoUmu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpcDenpyo);
            //if (dtDenpyoUmu.Rows.Count == 0)
            //{
            //    this.btnF3.Enabled = true;
            //}
            //else
            //{
            //    this.btnF3.Enabled = false;
            //}
        }

        /// <summary>
        /// マイナンバーの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMyNumber()
        {
            // 入力サイズ
            if (!ValChk.IsWithinLength(txtMyNumber.Text, txtMyNumber.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仲買人コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNakagaininCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtNakagaininCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD");
            DataTable dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("TORIHIKISAKI_CD",
                    "TB_HN_TORIHIKISAKI_JOHO", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                Msg.Error("既に存在する仕入先コードと重複しています。");
                return false;
            }

            // OKの場合、請求先コードに入力値をコピーする
            this.txtSeikyusakiCd.Text = this.txtNakagaininCd.Text;

            return true;
        }
        /// <summary>
        /// 仲買人コードの存在チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNakagaininCd(string Code)
        {
            // 使用されているコードかどうかちぇっく
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);

            //仕切明細
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND NAKAGAININ_CD = @TORIHIKISAKI_CD");
            DataTable dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("NAKAGAININ_CD",
                    "TB_HN_SHIKIRI_MEISAI", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                return false;
            }

            //取引伝票
            where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND TOKUISAKI_CD = @TORIHIKISAKI_CD");
            dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("TOKUISAKI_CD",
                    "TB_HN_TORIHIKI_DENPYO", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                return false;
            }

            // 既に存在するコードを削除しようとした場合はエラーとする（購買データ）
            StringBuilder　Sql = new StringBuilder();
            Sql.Append(" SELECT A.KANJO_KAMOKU_CD FROM TB_ZM_SHIWAKE_MEISAI A ");
            Sql.Append(" INNER JOIN TB_ZM_KANJO_KAMOKU B ");
            Sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            Sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append(" B.HOJO_SHIYO_KUBUN = 2 AND ");
            Sql.Append(" A.HOJO_KAMOKU_CD = @TORIHIKISAKI_CD ");
            DataTable dtShiwakeMei = this.Dba.GetDataTableFromSqlWithParams(Sql.ToString(), dpc);
            if (dtShiwakeMei.Rows.Count > 0)
            {
                //Msg.Error("既に存在する仕入先コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 正式仲買人名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNakagaininNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtNakagaininNm.Text, this.txtNakagaininNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仲買人カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNakagaininKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtNakagaininKanaNm.Text, this.txtNakagaininKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 略称仲買人名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidRyNakagaininNm()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtRyNakagaininNm.Text, this.txtRyNakagaininNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(上3桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango1()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(下4桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango2()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所1の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho1()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho1.Text, this.txtJusho1.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所2の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho2()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho2.Text, this.txtJusho2.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 電話番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTel()
        {
            if (!string.IsNullOrEmpty(this.txtTel.Text))
            {
                if (!IsPhoneNumber(this.txtTel.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// FAX番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFax()
        {
            if (!string.IsNullOrEmpty(this.txtFax.Text))
            {
                if (!IsPhoneNumber(this.txtFax.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        private bool IsPhoneNumber(string number)
        {
            return Regex.IsMatch(number, @"^0\d{1,4}-\d{1,4}-\d{4}$");
            //return Regex.IsMatch(number, @"\A0\d{1,4}-\d{1,4}-\d{4}\z");
        }

        /// <summary>
        /// 担当者コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTantoshaCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                this.txtTantoshaCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTantoshaCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtTantoshaCd.Text))
            {
                this.lblTantoshaNm.Text = string.Empty;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", "", this.txtTantoshaCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblTantoshaNm.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 単価取得方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTankaShutokuHoho()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtTankaShutokuHoho.Text))
            {
                this.txtTankaShutokuHoho.Text = "0";
            }

            // 0,1,2のみ入力を許可
            if (!ValChk.IsNumber(this.txtTankaShutokuHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtTankaShutokuHoho.Text);
            if (intval < 0 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 金額端数処理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKingakuHasuShori()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtKingakuHasuShori.Text))
            {
                this.txtKingakuHasuShori.Text = "1";
            }

            // 1,2,3のみ入力を許可
            if (!ValChk.IsNumber(this.txtKingakuHasuShori.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtKingakuHasuShori.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税入力方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiNyuryokuHoho()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShohizeiNyuryokuHoho.Text))
            {
                this.txtShohizeiNyuryokuHoho.Text = "0";
            }

            // 0,1,2,3のみ入力を許可
            if (!ValChk.IsNumber(this.txtShohizeiNyuryokuHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 登録済みの値のみ許可
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text);
            this.lblShzNrkHohoNm.Text = name;
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            this.lblShzNrkHohoNm.Text = name;
            return true;
        }

        /// <summary>
        /// 締日の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShimebi()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShimebi.Text))
            {
                this.txtShimebi.Text = "0";
            }

            // 1～28,99のみ許可
            if (!ValChk.IsNumber(this.txtShimebi.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShimebi.Text);
            if (!((intval >= 1 && intval <= 28) || intval == 99))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 請求書形式の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeikyushoKeishiki()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtSeikyushoKeishiki.Text))
            {
                this.txtSeikyushoKeishiki.Text = "1";
            }

            // 1,2のみ入力許可
            if (!ValChk.IsNumber(this.txtSeikyushoKeishiki.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtSeikyushoKeishiki.Text);
            if (intval < 1 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 請求書発行の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeikyushoHakko()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtSeikyushoHakko.Text))
            {
                this.txtSeikyushoHakko.Text = "1";
            }

            // 1,2のみ入力許可
            if (!ValChk.IsNumber(this.txtSeikyushoHakko.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtSeikyushoHakko.Text);
            if (intval < 1 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税端数処理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiHasuShori()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtShohizeiHasuShori.Text))
            {
                this.txtShohizeiHasuShori.Text = "1";
            }

            // 1,2,3のみ入力許可
            if (!ValChk.IsNumber(this.txtShohizeiHasuShori.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShohizeiHasuShori.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税転嫁方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiTenkaHoho()
        {
            //// 未入力は1とする
            //if (ValChk.IsEmpty(this.txtShohizeiTenkaHoho.Text))
            //{
            //    this.txtShohizeiTenkaHoho.Text = "1";
            //}

            // 1,2,3のみ入力許可
            if (!ValChk.IsNumber(this.txtShohizeiTenkaHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShohizeiTenkaHoho.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 回収月の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKaishuTsuki()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKaishuTsuki.Text))
            {
                this.txtKaishuTsuki.Text = "0";
            }

            // 数字のみ入力許可
            if (!ValChk.IsNumber(this.txtKaishuTsuki.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 回収日の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKaishuBi()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKaishuBi.Text))
            {
                this.txtKaishuBi.Text = "0";
            }

            // 1～28,99のみ許可
            if (!ValChk.IsNumber(this.txtKaishuBi.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtKaishuBi.Text);
            if (!((intval >= 1 && intval <= 28) || intval == 99))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 一覧表示の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidhyoji()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txthyoji.Text))
            {
                this.txthyoji.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txthyoji.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txthyoji.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 仲買人コードのチェック
                if (!IsValidNakagaininCd())
                {
                    this.txtNakagaininCd.Focus();
                    return false;
                }
            }

            // 正式仲買人名のチェック
            if (!IsValidNakagaininNm())
            {
                this.txtNakagaininNm.Focus();
                return false;
            }

            // 仲買人カナ名のチェック
            if (!IsValidNakagaininKanaNm())
            {
                this.txtNakagaininKanaNm.Focus();
                return false;
            }

            // 略称仲買人名のチェック
            if (!IsValidRyNakagaininNm())
            {
                this.txtRyNakagaininNm.Focus();
                return false;
            }

            // 郵便番号１のチェック
            if (!IsValidYubinBango1())
            {
                this.txtYubinBango1.Focus();
                return false;
            }

            // 郵便番号２のチェック
            if (!IsValidYubinBango2())
            {
                this.txtYubinBango2.Focus();
                return false;
            }

            // 郵便番号２だけの入力はエラー
            if (ValChk.IsEmpty(this.txtYubinBango1.Text) &&
                !ValChk.IsEmpty(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtYubinBango1.Focus();
                return false;
            }

            // 住所１のチェック
            if (!IsValidJusho1())
            {
                this.txtJusho1.Focus();
                return false;
            }

            // 住所２のチェック
            if (!IsValidJusho2())
            {
                this.txtJusho2.Focus();
                return false;
            }

            // 担当者コードのチェック
            if (!IsValidTantoshaCd())
            {
                this.txtTantoshaCd.Focus();
                return false;
            }

            // 単価取得方法のチェック
            if (!IsValidTankaShutokuHoho())
            {
                this.txtTankaShutokuHoho.Focus();
                return false;
            }

            // 金額端数処理のチェック
            if (!IsValidKingakuHasuShori())
            {
                this.txtKingakuHasuShori.Focus();
                return false;
            }

            // 消費税入力方法のチェック
            if (!IsValidShohizeiNyuryokuHoho())
            {
                this.txtShohizeiNyuryokuHoho.Focus();
                return false;
            }

            // 締日のチェック
            if (!IsValidShimebi())
            {
                this.txtShimebi.Focus();
                return false;
            }

            // 請求書発行のチェック
            if (!IsValidSeikyushoHakko())
            {
                this.txtSeikyushoHakko.Focus();
                return false;
            }

            // 請求書形式のチェック
            if (!IsValidSeikyushoKeishiki())
            {
                this.txtSeikyushoKeishiki.Focus();
                return false;
            }

            // 消費税端数処理のチェック
            if (!IsValidShohizeiHasuShori())
            {
                this.txtShohizeiHasuShori.Focus();
                return false;
            }

            // 消費税転嫁方法のチェック
            if (!IsValidShohizeiTenkaHoho())
            {
                this.txtShohizeiTenkaHoho.Focus();
                return false;
            }

            // 回収月のチェック
            if (!IsValidKaishuTsuki())
            {
                this.txtKaishuTsuki.Focus();
                return false;
            }

            // 回収日のチェック
            if (!IsValidKaishuBi())
            {
                this.txtKaishuBi.Focus();
                return false;
            }

            // マイナンバーのチェック
            if (!IsValidMyNumber())
            {
                this.txtMyNumber.Focus();
                return false;
            }

            // 一覧表示のチェック
            if (!IsValidhyoji())
            {
                this.txthyoji.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// TB_CM_TORIHIKISAKIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと取引先コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと取引先コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);
                alParams.Add(whereParam);
            }

            // 取引先名
            updParam.SetParam("@TORIHIKISAKI_NM", SqlDbType.VarChar, 40, this.txtNakagaininNm.Text);
            // 取引先カナ名
            updParam.SetParam("@TORIHIKISAKI_KANA_NM", SqlDbType.VarChar, 30, this.txtNakagaininKanaNm.Text);
            // 略称
            updParam.SetParam("@RYAKUSHO", SqlDbType.VarChar, 20, this.txtRyNakagaininNm.Text);
            // 郵便番号1
            updParam.SetParam("@YUBIN_BANGO1", SqlDbType.VarChar, 3, this.txtYubinBango1.Text);
            // 郵便番号2
            updParam.SetParam("@YUBIN_BANGO2", SqlDbType.VarChar, 4, this.txtYubinBango2.Text);
            // 住所1
            updParam.SetParam("@JUSHO1", SqlDbType.VarChar, 30, this.txtJusho1.Text);
            // 住所2
            updParam.SetParam("@JUSHO2", SqlDbType.VarChar, 30, this.txtJusho2.Text);
            // 住所3
            updParam.SetParam("@JUSHO3", SqlDbType.VarChar, 30, "");
            // 電話番号
            updParam.SetParam("@DENWA_BANGO", SqlDbType.VarChar, 15, this.txtTel.Text);
            // FAX番号
            updParam.SetParam("@FAX_BANGO", SqlDbType.VarChar, 15, this.txtFax.Text);
            // 担当者コード
            updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.txtTantoshaCd.Text);
            // 締日
            updParam.SetParam("@SIMEBI", SqlDbType.Decimal, 2, this.txtShimebi.Text);
            // 回収月
            updParam.SetParam("@KAISHU_TSUKI", SqlDbType.Decimal, 2, this.txtKaishuTsuki.Text);
            // 回収日
            updParam.SetParam("@KAISHU_BI", SqlDbType.Decimal, 2, this.txtKaishuBi.Text);
            // 請求書発行
            updParam.SetParam("@SEIKYUSHO_HAKKO", SqlDbType.Decimal, 1, this.txtSeikyushoHakko.Text);
            // 請求書形式
            updParam.SetParam("@SEIKYUSHO_KEISHIKI", SqlDbType.Decimal, 1, this.txtSeikyushoKeishiki.Text);
            // 消費税転嫁方法
            updParam.SetParam("@SHOHIZEI_TENKA_HOHO", SqlDbType.Decimal, 1, this.txtShohizeiTenkaHoho.Text);
            // 消費税端数処理
            updParam.SetParam("@SHOHIZEI_HASU_SHORI", SqlDbType.Decimal, 1, this.txtShohizeiHasuShori.Text);
            // 表示区分
            updParam.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, this.txthyoji.Text);
            // 登録日付
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            // TODO:処理FLGには0を更新だはず
            updParam.SetParam("@SHORI_FLG", SqlDbType.VarChar, 1, "0");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_TORIHIKISAKI_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetHnToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと取引先コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと取引先コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);
                alParams.Add(whereParam);
            }

            // 請求先コード
            updParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);
            // TODO:事業区分には3を更新だはず
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, 3);

            // TODO:よくわからん。貸借区分を意識？？？？？？
            updParam.SetParam("@SHUBETU_KUBUN", SqlDbType.Decimal, 4, 1);

            // 単価取得方法
            updParam.SetParam("@TANKA_SHUTOKU_HOHO", SqlDbType.Decimal, 1, this.txtTankaShutokuHoho.Text);
            // 消費税入力方法
            updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, this.txtShohizeiNyuryokuHoho.Text);
            // 金額端数処理
            updParam.SetParam("@KINGAKU_HASU_SHORI", SqlDbType.Decimal, 1, this.txtKingakuHasuShori.Text);
            // 取引先区分2
            updParam.SetParam("@TORIHIKISAKI_KUBUN2", SqlDbType.Decimal, 4, 2);
            // 取引先店舗コード
            updParam.SetParam("@TORIHIKISAKI_TENPO_CD", SqlDbType.VarChar, 10, null);
            // 取引先分類コード
            updParam.SetParam("@TORIHIKISAKI_BUNRUI_CD", SqlDbType.VarChar, 4, null);
            // 取引先伝票区分
            updParam.SetParam("@TORIHIKISAKI_DENPYO_KUBUN", SqlDbType.VarChar, 4, null);
            // 取引先取引先コード
            updParam.SetParam("@TORIHIKISAKI_TORIHIKISAKI_CD", SqlDbType.VarChar, 4, null);
            // 取引先伝票番号
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO", SqlDbType.Decimal, 9, 0);
            // 取引先伝票番号増分
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO_ZBN", SqlDbType.Decimal, 3, 0);
            // 取引先伝票番号開始
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO_KIS", SqlDbType.Decimal, 9, 0);
            // 取引先伝票番号終了
            updParam.SetParam("@TORIHIKISAKI_DENPYO_BANGO_SR", SqlDbType.Decimal, 9, 0);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            // 表示区分
            updParam.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, this.txthyoji.Text);


            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_TMP_TORIHIKISAKIからデータを削除
        /// TB_HN_TORIHIKISAKI_JOHOからデータを削除
        /// TB_HN_NOHINSAKIからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 会社コードと仲買人コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcDenpyo = new DbParamCollection();
            dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcDenpyo.SetParam("@TORIHIKISAKI_CD", SqlDbType.VarChar, 6, this.txtNakagaininCd.Text);

            alParams.Add(dpcDenpyo);

            return alParams;
        }
		#endregion

		private void lblNakagaininKanaNm_Click(object sender, EventArgs e)
		{

		}
	}
}
