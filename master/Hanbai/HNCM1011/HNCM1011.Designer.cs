﻿namespace jp.co.fsi.hn.hncm1011
{
    partial class HNCM1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblKanaNm = new System.Windows.Forms.Label();
			this.txtKanaName = new jp.co.fsi.common.controls.FsiTextBox();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.btnEnter = new System.Windows.Forms.Button();
			this.ckboxAllHyoji = new System.Windows.Forms.CheckBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(5, 5);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(92, 65);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF2
			// 
			this.btnF2.Location = new System.Drawing.Point(180, 65);
			this.btnF2.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF3
			// 
			this.btnF3.Location = new System.Drawing.Point(268, 65);
			this.btnF3.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF4
			// 
			this.btnF4.Location = new System.Drawing.Point(356, 65);
			this.btnF4.Margin = new System.Windows.Forms.Padding(5);
			this.btnF4.Text = "F4\r\n\r\nついか";
			// 
			// btnF5
			// 
			this.btnF5.Location = new System.Drawing.Point(444, 65);
			this.btnF5.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF7
			// 
			this.btnF7.Location = new System.Drawing.Point(620, 65);
			this.btnF7.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(532, 65);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF8
			// 
			this.btnF8.Location = new System.Drawing.Point(708, 65);
			this.btnF8.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF9
			// 
			this.btnF9.Location = new System.Drawing.Point(796, 65);
			this.btnF9.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF12
			// 
			this.btnF12.Location = new System.Drawing.Point(1064, 65);
			this.btnF12.Margin = new System.Windows.Forms.Padding(5);
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Location = new System.Drawing.Point(972, 65);
			this.btnF11.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF10
			// 
			this.btnF10.Location = new System.Drawing.Point(885, 65);
			this.btnF10.Margin = new System.Windows.Forms.Padding(5);
			// 
			// pnlDebug
			// 
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(4, 438);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(657, 133);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(646, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblKanaNm
			// 
			this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanaNm.Name = "lblKanaNm";
			this.lblKanaNm.Size = new System.Drawing.Size(610, 31);
			this.lblKanaNm.TabIndex = 0;
			this.lblKanaNm.Tag = "CHANGE";
			this.lblKanaNm.Text = "カ　ナ　名";
			this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKanaName
			// 
			this.txtKanaName.AllowDrop = true;
			this.txtKanaName.AutoSizeFromLength = false;
			this.txtKanaName.DisplayLength = null;
			this.txtKanaName.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanaName.Location = new System.Drawing.Point(124, 3);
			this.txtKanaName.Margin = new System.Windows.Forms.Padding(5);
			this.txtKanaName.MaxLength = 30;
			this.txtKanaName.Name = "txtKanaName";
			this.txtKanaName.Size = new System.Drawing.Size(268, 23);
			this.txtKanaName.TabIndex = 1;
			this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(16, 90);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(619, 395);
			this.dgvList.TabIndex = 3;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// btnEnter
			// 
			this.btnEnter.BackColor = System.Drawing.Color.SkyBlue;
			this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEnter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnEnter.ForeColor = System.Drawing.Color.Navy;
			this.btnEnter.Location = new System.Drawing.Point(4, 65);
			this.btnEnter.Margin = new System.Windows.Forms.Padding(4);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new System.Drawing.Size(87, 60);
			this.btnEnter.TabIndex = 907;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n変更";
			this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = false;
			this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
			// 
			// ckboxAllHyoji
			// 
			this.ckboxAllHyoji.AutoSize = true;
			this.ckboxAllHyoji.BackColor = System.Drawing.Color.Silver;
			this.ckboxAllHyoji.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.ckboxAllHyoji.Location = new System.Drawing.Point(460, 5);
			this.ckboxAllHyoji.Margin = new System.Windows.Forms.Padding(5);
			this.ckboxAllHyoji.Name = "ckboxAllHyoji";
			this.ckboxAllHyoji.Size = new System.Drawing.Size(91, 20);
			this.ckboxAllHyoji.TabIndex = 2;
			this.ckboxAllHyoji.Tag = "CHANGE";
			this.ckboxAllHyoji.Text = "全て表示";
			this.ckboxAllHyoji.UseVisualStyleBackColor = false;
			this.ckboxAllHyoji.CheckedChanged += new System.EventHandler(this.ckboxAllHyoji_CheckedChanged);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(15, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 1;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(620, 41);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtKanaName);
			this.fsiPanel1.Controls.Add(this.ckboxAllHyoji);
			this.fsiPanel1.Controls.Add(this.lblKanaNm);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(610, 31);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNCM1011
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(646, 574);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1011";
			this.Par1 = "";
			this.Text = "仲買人コード検索";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        protected System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.CheckBox ckboxAllHyoji;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
    }
}