﻿namespace jp.co.fsi.hn.hnyr1051
{
    /// <summary>
    /// HNYR1051R の概要の説明です。
    /// </summary>
    partial class HNYR1051R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNYR1051R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ラベル1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト002Fr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト002To = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghSenShuCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.直線15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト012 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト015 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト018 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト021 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト024 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト027 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト030 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト033 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト036 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.水揚数量合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト013 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト016 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト019 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト022 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト025 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト028 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト031 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト034 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト037 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.水揚金額合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト014 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト017 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト020 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト023 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト026 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト029 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト032 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト035 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト038 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.手数料合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト003 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト006 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト009 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト004 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト007 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト010 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト005 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト008 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfSenShuCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.ラベル96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002Fr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002To)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト018)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト021)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト024)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト027)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト030)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト033)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト036)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.水揚数量合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト016)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト019)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト022)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト025)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト028)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト031)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト034)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト037)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.水揚金額合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト017)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト020)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト023)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト026)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト029)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト032)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト035)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト038)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.手数料合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル1,
            this.直線2,
            this.ラベル3,
            this.テキスト001,
            this.ラベル5,
            this.ラベル6,
            this.ラベル7,
            this.テキスト002Fr,
            this.テキスト002To,
            this.textBox1});
            this.pageHeader.Height = 2.010417F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ラベル1
            // 
            this.ラベル1.Height = 0.3333333F;
            this.ラベル1.HyperLink = null;
            this.ラベル1.Left = 0.3681185F;
            this.ラベル1.Name = "ラベル1";
            this.ラベル1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 22pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル1.Tag = "";
            this.ラベル1.Text = "水   揚   高   証   明   書";
            this.ラベル1.Top = 0.3125F;
            this.ラベル1.Width = 5.229167F;
            // 
            // 直線2
            // 
            this.直線2.Height = 0F;
            this.直線2.Left = 2.753536F;
            this.直線2.LineWeight = 0F;
            this.直線2.Name = "直線2";
            this.直線2.Tag = "";
            this.直線2.Top = 1.375F;
            this.直線2.Width = 3.149999F;
            this.直線2.X1 = 2.753536F;
            this.直線2.X2 = 5.903535F;
            this.直線2.Y1 = 1.375F;
            this.直線2.Y2 = 1.375F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.28125F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 5.586868F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "殿";
            this.ラベル3.Top = 1.09375F;
            this.ラベル3.Width = 0.3020833F;
            // 
            // テキスト001
            // 
            this.テキスト001.CanGrow = false;
            this.テキスト001.DataField = "ITEM01";
            this.テキスト001.Height = 0.28125F;
            this.テキスト001.Left = 2.698615F;
            this.テキスト001.Name = "テキスト001";
            this.テキスト001.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト001.Tag = "";
            this.テキスト001.Text = "ITEM01";
            this.テキスト001.Top = 1.068406F;
            this.テキスト001.Width = 2.834646F;
            // 
            // ラベル5
            // 
            this.ラベル5.Height = 0.2291667F;
            this.ラベル5.HyperLink = null;
            this.ラベル5.Left = 0.9410349F;
            this.ラベル5.Name = "ラベル5";
            this.ラベル5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.ラベル5.Tag = "";
            this.ラベル5.Text = "(          4月 ";
            this.ラベル5.Top = 1.645833F;
            this.ラベル5.Width = 1.770833F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.2291667F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 2.795202F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "～";
            this.ラベル6.Top = 1.645833F;
            this.ラベル6.Width = 0.25F;
            // 
            // ラベル7
            // 
            this.ラベル7.Height = 0.2291667F;
            this.ラベル7.HyperLink = null;
            this.ラベル7.Left = 3.670202F;
            this.ラベル7.Name = "ラベル7";
            this.ラベル7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 1";
            this.ラベル7.Tag = "";
            this.ラベル7.Text = "3月分まで）";
            this.ラベル7.Top = 1.645833F;
            this.ラベル7.Width = 1.583333F;
            // 
            // テキスト002Fr
            // 
            this.テキスト002Fr.DataField = "ITEM02";
            this.テキスト002Fr.Height = 0.2291667F;
            this.テキスト002Fr.Left = 1.097286F;
            this.テキスト002Fr.Name = "テキスト002Fr";
            this.テキスト002Fr.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト002Fr.Tag = "";
            this.テキスト002Fr.Text = "ITEM02";
            this.テキスト002Fr.Top = 1.645833F;
            this.テキスト002Fr.Width = 0.9416667F;
            // 
            // テキスト002To
            // 
            this.テキスト002To.DataField = "ITEM44";
            this.テキスト002To.Height = 0.2291667F;
            this.テキスト002To.Left = 3.138952F;
            this.テキスト002To.Name = "テキスト002To";
            this.テキスト002To.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト002To.Tag = "";
            this.テキスト002To.Text = "ITEM44";
            this.テキスト002To.Top = 1.645833F;
            this.テキスト002To.Width = 0.8982452F;
            // 
            // textBox1
            // 
            this.textBox1.CanGrow = false;
            this.textBox1.DataField = "ITEM43";
            this.textBox1.Height = 0.28125F;
            this.textBox1.Left = 3.479134F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM43";
            this.textBox1.Top = 0.7870079F;
            this.textBox1.Width = 1.292914F;
            // 
            // ghSenShuCd
            // 
            this.ghSenShuCd.CanGrow = false;
            this.ghSenShuCd.DataField = "ITEM01";
            this.ghSenShuCd.Height = 0F;
            this.ghSenShuCd.Name = "ghSenShuCd";
            this.ghSenShuCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghSenShuCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線15,
            this.直線16,
            this.直線17,
            this.直線18,
            this.直線19,
            this.直線20,
            this.直線21,
            this.直線22,
            this.直線23,
            this.直線24,
            this.直線25,
            this.直線26,
            this.直線27,
            this.直線30,
            this.直線31,
            this.直線32,
            this.直線33,
            this.直線34,
            this.ラベル35,
            this.ラベル40,
            this.ラベル41,
            this.ラベル42,
            this.ラベル44,
            this.ラベル45,
            this.ラベル46,
            this.ラベル47,
            this.ラベル48,
            this.ラベル49,
            this.直線51,
            this.直線52,
            this.ラベル53,
            this.ラベル54,
            this.ラベル55,
            this.ラベル56,
            this.テキスト012,
            this.テキスト015,
            this.テキスト018,
            this.テキスト021,
            this.テキスト024,
            this.テキスト027,
            this.テキスト030,
            this.テキスト033,
            this.テキスト036,
            this.水揚数量合計,
            this.テキスト013,
            this.テキスト016,
            this.テキスト019,
            this.テキスト022,
            this.テキスト025,
            this.テキスト028,
            this.テキスト031,
            this.テキスト034,
            this.テキスト037,
            this.水揚金額合計,
            this.テキスト014,
            this.テキスト017,
            this.テキスト020,
            this.テキスト023,
            this.テキスト026,
            this.テキスト029,
            this.テキスト032,
            this.テキスト035,
            this.テキスト038,
            this.手数料合計,
            this.ラベル37,
            this.ラベル38,
            this.ラベル39,
            this.テキスト003,
            this.テキスト006,
            this.テキスト009,
            this.テキスト004,
            this.テキスト007,
            this.テキスト010,
            this.テキスト005,
            this.テキスト008,
            this.テキスト011});
            this.detail.Height = 4.15625F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // 直線15
            // 
            this.直線15.Height = 4.134723F;
            this.直線15.Left = 0.0503937F;
            this.直線15.LineWeight = 0F;
            this.直線15.Name = "直線15";
            this.直線15.Tag = "";
            this.直線15.Top = 0F;
            this.直線15.Width = 0F;
            this.直線15.X1 = 0.0503937F;
            this.直線15.X2 = 0.0503937F;
            this.直線15.Y1 = 0F;
            this.直線15.Y2 = 4.134723F;
            // 
            // 直線16
            // 
            this.直線16.Height = 0F;
            this.直線16.Left = 0.0503937F;
            this.直線16.LineWeight = 0F;
            this.直線16.Name = "直線16";
            this.直線16.Tag = "";
            this.直線16.Top = 0.0006951094F;
            this.直線16.Width = 5.90625F;
            this.直線16.X1 = 0.0503937F;
            this.直線16.X2 = 5.956644F;
            this.直線16.Y1 = 0.0006951094F;
            this.直線16.Y2 = 0.0006951094F;
            // 
            // 直線17
            // 
            this.直線17.Height = 4.134723F;
            this.直線17.Left = 5.956644F;
            this.直線17.LineWeight = 0F;
            this.直線17.Name = "直線17";
            this.直線17.Tag = "";
            this.直線17.Top = 0.002778053F;
            this.直線17.Width = 0F;
            this.直線17.X1 = 5.956644F;
            this.直線17.X2 = 5.956644F;
            this.直線17.Y1 = 0.002778053F;
            this.直線17.Y2 = 4.137501F;
            // 
            // 直線18
            // 
            this.直線18.Height = 0F;
            this.直線18.Left = 0.0503937F;
            this.直線18.LineWeight = 0F;
            this.直線18.Name = "直線18";
            this.直線18.Tag = "";
            this.直線18.Top = 0.2965281F;
            this.直線18.Width = 5.90625F;
            this.直線18.X1 = 0.0503937F;
            this.直線18.X2 = 5.956644F;
            this.直線18.Y1 = 0.2965281F;
            this.直線18.Y2 = 0.2965281F;
            // 
            // 直線19
            // 
            this.直線19.Height = 0F;
            this.直線19.Left = 0.0503937F;
            this.直線19.LineWeight = 0F;
            this.直線19.Name = "直線19";
            this.直線19.Tag = "";
            this.直線19.Top = 0.5916669F;
            this.直線19.Width = 5.90625F;
            this.直線19.X1 = 0.0503937F;
            this.直線19.X2 = 5.956644F;
            this.直線19.Y1 = 0.5916669F;
            this.直線19.Y2 = 0.5916669F;
            // 
            // 直線20
            // 
            this.直線20.Height = 0F;
            this.直線20.Left = 0.0503937F;
            this.直線20.LineWeight = 0F;
            this.直線20.Name = "直線20";
            this.直線20.Tag = "";
            this.直線20.Top = 0.886806F;
            this.直線20.Width = 5.90625F;
            this.直線20.X1 = 0.0503937F;
            this.直線20.X2 = 5.956644F;
            this.直線20.Y1 = 0.886806F;
            this.直線20.Y2 = 0.886806F;
            // 
            // 直線21
            // 
            this.直線21.Height = 0F;
            this.直線21.Left = 0.0503937F;
            this.直線21.LineWeight = 0F;
            this.直線21.Name = "直線21";
            this.直線21.Tag = "";
            this.直線21.Top = 1.181945F;
            this.直線21.Width = 5.90625F;
            this.直線21.X1 = 0.0503937F;
            this.直線21.X2 = 5.956644F;
            this.直線21.Y1 = 1.181945F;
            this.直線21.Y2 = 1.181945F;
            // 
            // 直線22
            // 
            this.直線22.Height = 0F;
            this.直線22.Left = 0.0503937F;
            this.直線22.LineWeight = 0F;
            this.直線22.Name = "直線22";
            this.直線22.Tag = "";
            this.直線22.Top = 1.477778F;
            this.直線22.Width = 5.90625F;
            this.直線22.X1 = 0.0503937F;
            this.直線22.X2 = 5.956644F;
            this.直線22.Y1 = 1.477778F;
            this.直線22.Y2 = 1.477778F;
            // 
            // 直線23
            // 
            this.直線23.Height = 0F;
            this.直線23.Left = 0.0503937F;
            this.直線23.LineWeight = 0F;
            this.直線23.Name = "直線23";
            this.直線23.Tag = "";
            this.直線23.Top = 1.772917F;
            this.直線23.Width = 5.90625F;
            this.直線23.X1 = 0.0503937F;
            this.直線23.X2 = 5.956644F;
            this.直線23.Y1 = 1.772917F;
            this.直線23.Y2 = 1.772917F;
            // 
            // 直線24
            // 
            this.直線24.Height = 0F;
            this.直線24.Left = 0.0503937F;
            this.直線24.LineWeight = 0F;
            this.直線24.Name = "直線24";
            this.直線24.Tag = "";
            this.直線24.Top = 2.068056F;
            this.直線24.Width = 5.90625F;
            this.直線24.X1 = 0.0503937F;
            this.直線24.X2 = 5.956644F;
            this.直線24.Y1 = 2.068056F;
            this.直線24.Y2 = 2.068056F;
            // 
            // 直線25
            // 
            this.直線25.Height = 0F;
            this.直線25.Left = 0.0503937F;
            this.直線25.LineWeight = 0F;
            this.直線25.Name = "直線25";
            this.直線25.Tag = "";
            this.直線25.Top = 2.363194F;
            this.直線25.Width = 5.90625F;
            this.直線25.X1 = 0.0503937F;
            this.直線25.X2 = 5.956644F;
            this.直線25.Y1 = 2.363194F;
            this.直線25.Y2 = 2.363194F;
            // 
            // 直線26
            // 
            this.直線26.Height = 0F;
            this.直線26.Left = 0.0503937F;
            this.直線26.LineWeight = 0F;
            this.直線26.Name = "直線26";
            this.直線26.Tag = "";
            this.直線26.Top = 2.659028F;
            this.直線26.Width = 5.90625F;
            this.直線26.X1 = 0.0503937F;
            this.直線26.X2 = 5.956644F;
            this.直線26.Y1 = 2.659028F;
            this.直線26.Y2 = 2.659028F;
            // 
            // 直線27
            // 
            this.直線27.Height = 0F;
            this.直線27.Left = 0.0503937F;
            this.直線27.LineWeight = 0F;
            this.直線27.Name = "直線27";
            this.直線27.Tag = "";
            this.直線27.Top = 2.954167F;
            this.直線27.Width = 5.90625F;
            this.直線27.X1 = 0.0503937F;
            this.直線27.X2 = 5.956644F;
            this.直線27.Y1 = 2.954167F;
            this.直線27.Y2 = 2.954167F;
            // 
            // 直線30
            // 
            this.直線30.Height = 0F;
            this.直線30.Left = 0.0503937F;
            this.直線30.LineWeight = 0F;
            this.直線30.Name = "直線30";
            this.直線30.Tag = "";
            this.直線30.Top = 3.249306F;
            this.直線30.Width = 5.90625F;
            this.直線30.X1 = 0.0503937F;
            this.直線30.X2 = 5.956644F;
            this.直線30.Y1 = 3.249306F;
            this.直線30.Y2 = 3.249306F;
            // 
            // 直線31
            // 
            this.直線31.Height = 0F;
            this.直線31.Left = 0.0503937F;
            this.直線31.LineWeight = 0F;
            this.直線31.Name = "直線31";
            this.直線31.Tag = "";
            this.直線31.Top = 3.544445F;
            this.直線31.Width = 5.90625F;
            this.直線31.X1 = 0.0503937F;
            this.直線31.X2 = 5.956644F;
            this.直線31.Y1 = 3.544445F;
            this.直線31.Y2 = 3.544445F;
            // 
            // 直線32
            // 
            this.直線32.Height = 4.134723F;
            this.直線32.Left = 0.8420604F;
            this.直線32.LineWeight = 0F;
            this.直線32.Name = "直線32";
            this.直線32.Tag = "";
            this.直線32.Top = 0.002778053F;
            this.直線32.Width = 0F;
            this.直線32.X1 = 0.8420604F;
            this.直線32.X2 = 0.8420604F;
            this.直線32.Y1 = 0.002778053F;
            this.直線32.Y2 = 4.137501F;
            // 
            // 直線33
            // 
            this.直線33.Height = 4.134723F;
            this.直線33.Left = 2.53095F;
            this.直線33.LineWeight = 0F;
            this.直線33.Name = "直線33";
            this.直線33.Tag = "";
            this.直線33.Top = 0.002778053F;
            this.直線33.Width = 0F;
            this.直線33.X1 = 2.53095F;
            this.直線33.X2 = 2.53095F;
            this.直線33.Y1 = 0.002778053F;
            this.直線33.Y2 = 4.137501F;
            // 
            // 直線34
            // 
            this.直線34.Height = 4.134723F;
            this.直線34.Left = 4.224005F;
            this.直線34.LineWeight = 0F;
            this.直線34.Name = "直線34";
            this.直線34.Tag = "";
            this.直線34.Top = 0.002778053F;
            this.直線34.Width = 0F;
            this.直線34.X1 = 4.224005F;
            this.直線34.X2 = 4.224005F;
            this.直線34.Y1 = 0.002778053F;
            this.直線34.Y2 = 4.137501F;
            // 
            // ラベル35
            // 
            this.ラベル35.Height = 0.1979167F;
            this.ラベル35.HyperLink = null;
            this.ラベル35.Left = 0.2691437F;
            this.ラベル35.Name = "ラベル35";
            this.ラベル35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル35.Tag = "";
            this.ラベル35.Text = "月別";
            this.ラベル35.Top = 0.08611107F;
            this.ラベル35.Width = 0.3854167F;
            // 
            // ラベル40
            // 
            this.ラベル40.Height = 0.1979167F;
            this.ラベル40.HyperLink = null;
            this.ラベル40.Left = 0.39F;
            this.ラベル40.Name = "ラベル40";
            this.ラベル40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル40.Tag = "";
            this.ラベル40.Text = "4";
            this.ラベル40.Top = 0.399F;
            this.ラベル40.Width = 0.135F;
            // 
            // ラベル41
            // 
            this.ラベル41.Height = 0.1979167F;
            this.ラベル41.HyperLink = null;
            this.ラベル41.Left = 0.3900655F;
            this.ラベル41.Name = "ラベル41";
            this.ラベル41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル41.Tag = "";
            this.ラベル41.Text = "5";
            this.ラベル41.Top = 0.690415F;
            this.ラベル41.Width = 0.1354167F;
            // 
            // ラベル42
            // 
            this.ラベル42.Height = 0.1979167F;
            this.ラベル42.HyperLink = null;
            this.ラベル42.Left = 0.3900655F;
            this.ラベル42.Name = "ラベル42";
            this.ラベル42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル42.Tag = "";
            this.ラベル42.Text = "6";
            this.ラベル42.Top = 0.9820809F;
            this.ラベル42.Width = 0.1354167F;
            // 
            // ラベル44
            // 
            this.ラベル44.Height = 0.1979167F;
            this.ラベル44.HyperLink = null;
            this.ラベル44.Left = 0.3900655F;
            this.ラベル44.Name = "ラベル44";
            this.ラベル44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル44.Tag = "";
            this.ラベル44.Text = "8";
            this.ラベル44.Top = 1.575831F;
            this.ラベル44.Width = 0.1354167F;
            // 
            // ラベル45
            // 
            this.ラベル45.Height = 0.1979167F;
            this.ラベル45.HyperLink = null;
            this.ラベル45.Left = 0.3900655F;
            this.ラベル45.Name = "ラベル45";
            this.ラベル45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル45.Tag = "";
            this.ラベル45.Text = "9";
            this.ラベル45.Top = 1.867498F;
            this.ラベル45.Width = 0.1354167F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.1979167F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 0.3483989F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "10";
            this.ラベル46.Top = 2.169581F;
            this.ラベル46.Width = 0.21875F;
            // 
            // ラベル47
            // 
            this.ラベル47.Height = 0.1979167F;
            this.ラベル47.HyperLink = null;
            this.ラベル47.Left = 0.3483989F;
            this.ラベル47.Name = "ラベル47";
            this.ラベル47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル47.Tag = "";
            this.ラベル47.Text = "11";
            this.ラベル47.Top = 2.461248F;
            this.ラベル47.Width = 0.21875F;
            // 
            // ラベル48
            // 
            this.ラベル48.Height = 0.1979167F;
            this.ラベル48.HyperLink = null;
            this.ラベル48.Left = 0.3483989F;
            this.ラベル48.Name = "ラベル48";
            this.ラベル48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル48.Tag = "";
            this.ラベル48.Text = "12";
            this.ラベル48.Top = 2.752915F;
            this.ラベル48.Width = 0.21875F;
            // 
            // ラベル49
            // 
            this.ラベル49.Height = 0.1979167F;
            this.ラベル49.HyperLink = null;
            this.ラベル49.Left = 0.2691437F;
            this.ラベル49.Name = "ラベル49";
            this.ラベル49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル49.Tag = "";
            this.ラベル49.Text = "合計";
            this.ラベル49.Top = 3.940278F;
            this.ラベル49.Width = 0.3854167F;
            // 
            // 直線51
            // 
            this.直線51.Height = 0F;
            this.直線51.Left = 0.0503937F;
            this.直線51.LineWeight = 0F;
            this.直線51.Name = "直線51";
            this.直線51.Tag = "";
            this.直線51.Top = 3.840278F;
            this.直線51.Width = 5.90625F;
            this.直線51.X1 = 0.0503937F;
            this.直線51.X2 = 5.956644F;
            this.直線51.Y1 = 3.840278F;
            this.直線51.Y2 = 3.840278F;
            // 
            // 直線52
            // 
            this.直線52.Height = 0F;
            this.直線52.Left = 0.0503937F;
            this.直線52.LineWeight = 0F;
            this.直線52.Name = "直線52";
            this.直線52.Tag = "";
            this.直線52.Top = 4.136111F;
            this.直線52.Width = 5.90625F;
            this.直線52.X1 = 0.0503937F;
            this.直線52.X2 = 5.956644F;
            this.直線52.Y1 = 4.136111F;
            this.直線52.Y2 = 4.136111F;
            // 
            // ラベル53
            // 
            this.ラベル53.Height = 0.1979167F;
            this.ラベル53.HyperLink = null;
            this.ラベル53.Left = 0.3900655F;
            this.ラベル53.Name = "ラベル53";
            this.ラベル53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル53.Tag = "";
            this.ラベル53.Text = "7";
            this.ラベル53.Top = 1.284165F;
            this.ラベル53.Width = 0.1354167F;
            // 
            // ラベル54
            // 
            this.ラベル54.Height = 0.1979167F;
            this.ラベル54.HyperLink = null;
            this.ラベル54.Left = 1.092061F;
            this.ラベル54.Name = "ラベル54";
            this.ラベル54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル54.Tag = "";
            this.ラベル54.Text = "水揚数量（Kｇ）";
            this.ラベル54.Top = 0.08611107F;
            this.ラベル54.Width = 1.3125F;
            // 
            // ラベル55
            // 
            this.ラベル55.Height = 0.1979167F;
            this.ラベル55.HyperLink = null;
            this.ラベル55.Left = 2.852477F;
            this.ラベル55.Name = "ラベル55";
            this.ラベル55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル55.Tag = "";
            this.ラベル55.Text = "水揚金額（円）";
            this.ラベル55.Top = 0.08611107F;
            this.ラベル55.Width = 1.21875F;
            // 
            // ラベル56
            // 
            this.ラベル56.Height = 0.1979167F;
            this.ラベル56.HyperLink = null;
            this.ラベル56.Left = 4.508727F;
            this.ラベル56.Name = "ラベル56";
            this.ラベル56.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル56.Tag = "";
            this.ラベル56.Text = "備考／（手数料）";
            this.ラベル56.Top = 0.08611107F;
            this.ラベル56.Width = 1.385417F;
            // 
            // テキスト012
            // 
            this.テキスト012.DataField = "ITEM12";
            this.テキスト012.Height = 0.1979167F;
            this.テキスト012.Left = 0.89F;
            this.テキスト012.Name = "テキスト012";
            this.テキスト012.OutputFormat = resources.GetString("テキスト012.OutputFormat");
            this.テキスト012.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト012.Tag = "";
            this.テキスト012.Text = null;
            this.テキスト012.Top = 0.383F;
            this.テキスト012.Width = 1.410417F;
            // 
            // テキスト015
            // 
            this.テキスト015.DataField = "ITEM15";
            this.テキスト015.Height = 0.1979167F;
            this.テキスト015.Left = 0.89F;
            this.テキスト015.Name = "テキスト015";
            this.テキスト015.OutputFormat = resources.GetString("テキスト015.OutputFormat");
            this.テキスト015.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト015.Tag = "";
            this.テキスト015.Text = null;
            this.テキスト015.Top = 0.6743389F;
            this.テキスト015.Width = 1.410417F;
            // 
            // テキスト018
            // 
            this.テキスト018.DataField = "ITEM18";
            this.テキスト018.Height = 0.1979167F;
            this.テキスト018.Left = 0.89F;
            this.テキスト018.Name = "テキスト018";
            this.テキスト018.OutputFormat = resources.GetString("テキスト018.OutputFormat");
            this.テキスト018.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト018.Tag = "";
            this.テキスト018.Text = null;
            this.テキスト018.Top = 0.966071F;
            this.テキスト018.Width = 1.410417F;
            // 
            // テキスト021
            // 
            this.テキスト021.DataField = "ITEM21";
            this.テキスト021.Height = 0.1979167F;
            this.テキスト021.Left = 0.89F;
            this.テキスト021.Name = "テキスト021";
            this.テキスト021.OutputFormat = resources.GetString("テキスト021.OutputFormat");
            this.テキスト021.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト021.Tag = "";
            this.テキスト021.Text = null;
            this.テキスト021.Top = 1.268433F;
            this.テキスト021.Width = 1.410417F;
            // 
            // テキスト024
            // 
            this.テキスト024.DataField = "ITEM24";
            this.テキスト024.Height = 0.1979167F;
            this.テキスト024.Left = 0.89F;
            this.テキスト024.Name = "テキスト024";
            this.テキスト024.OutputFormat = resources.GetString("テキスト024.OutputFormat");
            this.テキスト024.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト024.Tag = "";
            this.テキスト024.Text = null;
            this.テキスト024.Top = 1.559772F;
            this.テキスト024.Width = 1.410417F;
            // 
            // テキスト027
            // 
            this.テキスト027.DataField = "ITEM27";
            this.テキスト027.Height = 0.1979167F;
            this.テキスト027.Left = 0.89F;
            this.テキスト027.Name = "テキスト027";
            this.テキスト027.OutputFormat = resources.GetString("テキスト027.OutputFormat");
            this.テキスト027.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト027.Tag = "";
            this.テキスト027.Text = null;
            this.テキスト027.Top = 1.851504F;
            this.テキスト027.Width = 1.410417F;
            // 
            // テキスト030
            // 
            this.テキスト030.DataField = "ITEM30";
            this.テキスト030.Height = 0.1979167F;
            this.テキスト030.Left = 0.89F;
            this.テキスト030.Name = "テキスト030";
            this.テキスト030.OutputFormat = resources.GetString("テキスト030.OutputFormat");
            this.テキスト030.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト030.Tag = "";
            this.テキスト030.Text = null;
            this.テキスト030.Top = 2.153473F;
            this.テキスト030.Width = 1.410417F;
            // 
            // テキスト033
            // 
            this.テキスト033.DataField = "ITEM33";
            this.テキスト033.Height = 0.1979167F;
            this.テキスト033.Left = 0.89F;
            this.テキスト033.Name = "テキスト033";
            this.テキスト033.OutputFormat = resources.GetString("テキスト033.OutputFormat");
            this.テキスト033.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト033.Tag = "";
            this.テキスト033.Text = null;
            this.テキスト033.Top = 2.445205F;
            this.テキスト033.Width = 1.410417F;
            // 
            // テキスト036
            // 
            this.テキスト036.DataField = "ITEM36";
            this.テキスト036.Height = 0.1979167F;
            this.テキスト036.Left = 0.89F;
            this.テキスト036.Name = "テキスト036";
            this.テキスト036.OutputFormat = resources.GetString("テキスト036.OutputFormat");
            this.テキスト036.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト036.Tag = "";
            this.テキスト036.Text = null;
            this.テキスト036.Top = 2.736938F;
            this.テキスト036.Width = 1.410417F;
            // 
            // 水揚数量合計
            // 
            this.水揚数量合計.Height = 0.1979167F;
            this.水揚数量合計.Left = 0.8836616F;
            this.水揚数量合計.Name = "水揚数量合計";
            this.水揚数量合計.OutputFormat = resources.GetString("水揚数量合計.OutputFormat");
            this.水揚数量合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.水揚数量合計.Tag = "";
            this.水揚数量合計.Text = null;
            this.水揚数量合計.Top = 3.924186F;
            this.水揚数量合計.Width = 1.420833F;
            // 
            // テキスト013
            // 
            this.テキスト013.DataField = "ITEM13";
            this.テキスト013.Height = 0.1979167F;
            this.テキスト013.Left = 2.5775F;
            this.テキスト013.Name = "テキスト013";
            this.テキスト013.OutputFormat = resources.GetString("テキスト013.OutputFormat");
            this.テキスト013.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト013.Tag = "";
            this.テキスト013.Text = null;
            this.テキスト013.Top = 0.383F;
            this.テキスト013.Width = 1.49375F;
            // 
            // テキスト016
            // 
            this.テキスト016.DataField = "ITEM16";
            this.テキスト016.Height = 0.1979167F;
            this.テキスト016.Left = 2.5775F;
            this.テキスト016.Name = "テキスト016";
            this.テキスト016.OutputFormat = resources.GetString("テキスト016.OutputFormat");
            this.テキスト016.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト016.Tag = "";
            this.テキスト016.Text = null;
            this.テキスト016.Top = 0.6743389F;
            this.テキスト016.Width = 1.49375F;
            // 
            // テキスト019
            // 
            this.テキスト019.DataField = "ITEM19";
            this.テキスト019.Height = 0.1979167F;
            this.テキスト019.Left = 2.5775F;
            this.テキスト019.Name = "テキスト019";
            this.テキスト019.OutputFormat = resources.GetString("テキスト019.OutputFormat");
            this.テキスト019.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト019.Tag = "";
            this.テキスト019.Text = null;
            this.テキスト019.Top = 0.966071F;
            this.テキスト019.Width = 1.49375F;
            // 
            // テキスト022
            // 
            this.テキスト022.DataField = "ITEM22";
            this.テキスト022.Height = 0.1979167F;
            this.テキスト022.Left = 2.5775F;
            this.テキスト022.Name = "テキスト022";
            this.テキスト022.OutputFormat = resources.GetString("テキスト022.OutputFormat");
            this.テキスト022.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト022.Tag = "";
            this.テキスト022.Text = null;
            this.テキスト022.Top = 1.268433F;
            this.テキスト022.Width = 1.49375F;
            // 
            // テキスト025
            // 
            this.テキスト025.DataField = "ITEM25";
            this.テキスト025.Height = 0.1979167F;
            this.テキスト025.Left = 2.577517F;
            this.テキスト025.Name = "テキスト025";
            this.テキスト025.OutputFormat = resources.GetString("テキスト025.OutputFormat");
            this.テキスト025.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト025.Tag = "";
            this.テキスト025.Text = null;
            this.テキスト025.Top = 1.559772F;
            this.テキスト025.Width = 1.49375F;
            // 
            // テキスト028
            // 
            this.テキスト028.DataField = "ITEM28";
            this.テキスト028.Height = 0.1979167F;
            this.テキスト028.Left = 2.5775F;
            this.テキスト028.Name = "テキスト028";
            this.テキスト028.OutputFormat = resources.GetString("テキスト028.OutputFormat");
            this.テキスト028.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト028.Tag = "";
            this.テキスト028.Text = null;
            this.テキスト028.Top = 1.851504F;
            this.テキスト028.Width = 1.49375F;
            // 
            // テキスト031
            // 
            this.テキスト031.DataField = "ITEM31";
            this.テキスト031.Height = 0.1979167F;
            this.テキスト031.Left = 2.5775F;
            this.テキスト031.Name = "テキスト031";
            this.テキスト031.OutputFormat = resources.GetString("テキスト031.OutputFormat");
            this.テキスト031.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト031.Tag = "";
            this.テキスト031.Text = null;
            this.テキスト031.Top = 2.153473F;
            this.テキスト031.Width = 1.49375F;
            // 
            // テキスト034
            // 
            this.テキスト034.DataField = "ITEM34";
            this.テキスト034.Height = 0.1979167F;
            this.テキスト034.Left = 2.5775F;
            this.テキスト034.Name = "テキスト034";
            this.テキスト034.OutputFormat = resources.GetString("テキスト034.OutputFormat");
            this.テキスト034.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト034.Tag = "";
            this.テキスト034.Text = null;
            this.テキスト034.Top = 2.445205F;
            this.テキスト034.Width = 1.49375F;
            // 
            // テキスト037
            // 
            this.テキスト037.DataField = "ITEM37";
            this.テキスト037.Height = 0.1979167F;
            this.テキスト037.Left = 2.5775F;
            this.テキスト037.Name = "テキスト037";
            this.テキスト037.OutputFormat = resources.GetString("テキスト037.OutputFormat");
            this.テキスト037.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト037.Tag = "";
            this.テキスト037.Text = null;
            this.テキスト037.Top = 2.736938F;
            this.テキスト037.Width = 1.49375F;
            // 
            // 水揚金額合計
            // 
            this.水揚金額合計.Height = 0.1979167F;
            this.水揚金額合計.Left = 2.581759F;
            this.水揚金額合計.Name = "水揚金額合計";
            this.水揚金額合計.OutputFormat = resources.GetString("水揚金額合計.OutputFormat");
            this.水揚金額合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.水揚金額合計.Tag = "";
            this.水揚金額合計.Text = null;
            this.水揚金額合計.Top = 3.924186F;
            this.水揚金額合計.Width = 1.49375F;
            // 
            // テキスト014
            // 
            this.テキスト014.DataField = "ITEM14";
            this.テキスト014.Height = 0.1979167F;
            this.テキスト014.Left = 4.275416F;
            this.テキスト014.Name = "テキスト014";
            this.テキスト014.OutputFormat = resources.GetString("テキスト014.OutputFormat");
            this.テキスト014.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト014.Tag = "";
            this.テキスト014.Text = null;
            this.テキスト014.Top = 0.383F;
            this.テキスト014.Width = 1.515371F;
            // 
            // テキスト017
            // 
            this.テキスト017.DataField = "ITEM17";
            this.テキスト017.Height = 0.1979167F;
            this.テキスト017.Left = 4.275416F;
            this.テキスト017.Name = "テキスト017";
            this.テキスト017.OutputFormat = resources.GetString("テキスト017.OutputFormat");
            this.テキスト017.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト017.Tag = "";
            this.テキスト017.Text = null;
            this.テキスト017.Top = 0.6743389F;
            this.テキスト017.Width = 1.515371F;
            // 
            // テキスト020
            // 
            this.テキスト020.DataField = "ITEM20";
            this.テキスト020.Height = 0.1979167F;
            this.テキスト020.Left = 4.275416F;
            this.テキスト020.Name = "テキスト020";
            this.テキスト020.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト020.Tag = "";
            this.テキスト020.Text = null;
            this.テキスト020.Top = 0.966071F;
            this.テキスト020.Width = 1.515371F;
            // 
            // テキスト023
            // 
            this.テキスト023.DataField = "ITEM23";
            this.テキスト023.Height = 0.1979167F;
            this.テキスト023.Left = 4.275416F;
            this.テキスト023.Name = "テキスト023";
            this.テキスト023.OutputFormat = resources.GetString("テキスト023.OutputFormat");
            this.テキスト023.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト023.Tag = "";
            this.テキスト023.Text = null;
            this.テキスト023.Top = 1.268433F;
            this.テキスト023.Width = 1.515371F;
            // 
            // テキスト026
            // 
            this.テキスト026.DataField = "ITEM26";
            this.テキスト026.Height = 0.1979167F;
            this.テキスト026.Left = 4.275416F;
            this.テキスト026.Name = "テキスト026";
            this.テキスト026.OutputFormat = resources.GetString("テキスト026.OutputFormat");
            this.テキスト026.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト026.Tag = "";
            this.テキスト026.Text = null;
            this.テキスト026.Top = 1.559772F;
            this.テキスト026.Width = 1.515371F;
            // 
            // テキスト029
            // 
            this.テキスト029.DataField = "ITEM29";
            this.テキスト029.Height = 0.1979167F;
            this.テキスト029.Left = 4.275416F;
            this.テキスト029.Name = "テキスト029";
            this.テキスト029.OutputFormat = resources.GetString("テキスト029.OutputFormat");
            this.テキスト029.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト029.Tag = "";
            this.テキスト029.Text = null;
            this.テキスト029.Top = 1.851504F;
            this.テキスト029.Width = 1.515371F;
            // 
            // テキスト032
            // 
            this.テキスト032.DataField = "ITEM32";
            this.テキスト032.Height = 0.1979167F;
            this.テキスト032.Left = 4.275416F;
            this.テキスト032.Name = "テキスト032";
            this.テキスト032.OutputFormat = resources.GetString("テキスト032.OutputFormat");
            this.テキスト032.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト032.Tag = "";
            this.テキスト032.Text = null;
            this.テキスト032.Top = 2.153473F;
            this.テキスト032.Width = 1.515371F;
            // 
            // テキスト035
            // 
            this.テキスト035.DataField = "ITEM35";
            this.テキスト035.Height = 0.1979167F;
            this.テキスト035.Left = 4.275416F;
            this.テキスト035.Name = "テキスト035";
            this.テキスト035.OutputFormat = resources.GetString("テキスト035.OutputFormat");
            this.テキスト035.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト035.Tag = "";
            this.テキスト035.Text = null;
            this.テキスト035.Top = 2.445205F;
            this.テキスト035.Width = 1.515371F;
            // 
            // テキスト038
            // 
            this.テキスト038.DataField = "ITEM38";
            this.テキスト038.Height = 0.1979167F;
            this.テキスト038.Left = 4.275416F;
            this.テキスト038.Name = "テキスト038";
            this.テキスト038.OutputFormat = resources.GetString("テキスト038.OutputFormat");
            this.テキスト038.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト038.Tag = "";
            this.テキスト038.Text = null;
            this.テキスト038.Top = 2.736938F;
            this.テキスト038.Width = 1.515371F;
            // 
            // 手数料合計
            // 
            this.手数料合計.Height = 0.1979167F;
            this.手数料合計.Left = 4.279495F;
            this.手数料合計.Name = "手数料合計";
            this.手数料合計.OutputFormat = resources.GetString("手数料合計.OutputFormat");
            this.手数料合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.手数料合計.Tag = "";
            this.手数料合計.Text = null;
            this.手数料合計.Top = 3.924186F;
            this.手数料合計.Width = 1.515371F;
            // 
            // ラベル37
            // 
            this.ラベル37.Height = 0.1979167F;
            this.ラベル37.HyperLink = null;
            this.ラベル37.Left = 0.389649F;
            this.ラベル37.Name = "ラベル37";
            this.ラベル37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル37.Tag = "";
            this.ラベル37.Text = "1";
            this.ラベル37.Top = 3.051638F;
            this.ラベル37.Width = 0.1354167F;
            // 
            // ラベル38
            // 
            this.ラベル38.Height = 0.1979167F;
            this.ラベル38.HyperLink = null;
            this.ラベル38.Left = 0.389649F;
            this.ラベル38.Name = "ラベル38";
            this.ラベル38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル38.Tag = "";
            this.ラベル38.Text = "2";
            this.ラベル38.Top = 3.343305F;
            this.ラベル38.Width = 0.1354167F;
            // 
            // ラベル39
            // 
            this.ラベル39.Height = 0.1979167F;
            this.ラベル39.HyperLink = null;
            this.ラベル39.Left = 0.389649F;
            this.ラベル39.Name = "ラベル39";
            this.ラベル39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル39.Tag = "";
            this.ラベル39.Text = "3";
            this.ラベル39.Top = 3.634972F;
            this.ラベル39.Width = 0.1354167F;
            // 
            // テキスト003
            // 
            this.テキスト003.DataField = "ITEM03";
            this.テキスト003.Height = 0.1979167F;
            this.テキスト003.Left = 0.8895835F;
            this.テキスト003.Name = "テキスト003";
            this.テキスト003.OutputFormat = resources.GetString("テキスト003.OutputFormat");
            this.テキスト003.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト003.Tag = "";
            this.テキスト003.Text = null;
            this.テキスト003.Top = 3.028F;
            this.テキスト003.Width = 1.410417F;
            // 
            // テキスト006
            // 
            this.テキスト006.DataField = "ITEM06";
            this.テキスト006.Height = 0.1979167F;
            this.テキスト006.Left = 0.8895835F;
            this.テキスト006.Name = "テキスト006";
            this.テキスト006.OutputFormat = resources.GetString("テキスト006.OutputFormat");
            this.テキスト006.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト006.Tag = "";
            this.テキスト006.Text = null;
            this.テキスト006.Top = 3.323276F;
            this.テキスト006.Width = 1.410417F;
            // 
            // テキスト009
            // 
            this.テキスト009.DataField = "ITEM09";
            this.テキスト009.Height = 0.1979167F;
            this.テキスト009.Left = 0.8895835F;
            this.テキスト009.Name = "テキスト009";
            this.テキスト009.OutputFormat = resources.GetString("テキスト009.OutputFormat");
            this.テキスト009.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト009.Tag = "";
            this.テキスト009.Text = null;
            this.テキスト009.Top = 3.618945F;
            this.テキスト009.Width = 1.410417F;
            // 
            // テキスト004
            // 
            this.テキスト004.DataField = "ITEM04";
            this.テキスト004.Height = 0.1979167F;
            this.テキスト004.Left = 2.577083F;
            this.テキスト004.Name = "テキスト004";
            this.テキスト004.OutputFormat = resources.GetString("テキスト004.OutputFormat");
            this.テキスト004.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト004.Tag = "";
            this.テキスト004.Text = null;
            this.テキスト004.Top = 3.028F;
            this.テキスト004.Width = 1.49375F;
            // 
            // テキスト007
            // 
            this.テキスト007.DataField = "ITEM07";
            this.テキスト007.Height = 0.1979167F;
            this.テキスト007.Left = 2.577083F;
            this.テキスト007.Name = "テキスト007";
            this.テキスト007.OutputFormat = resources.GetString("テキスト007.OutputFormat");
            this.テキスト007.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト007.Tag = "";
            this.テキスト007.Text = null;
            this.テキスト007.Top = 3.323276F;
            this.テキスト007.Width = 1.49375F;
            // 
            // テキスト010
            // 
            this.テキスト010.DataField = "ITEM10";
            this.テキスト010.Height = 0.1979167F;
            this.テキスト010.Left = 2.577083F;
            this.テキスト010.Name = "テキスト010";
            this.テキスト010.OutputFormat = resources.GetString("テキスト010.OutputFormat");
            this.テキスト010.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト010.Tag = "";
            this.テキスト010.Text = null;
            this.テキスト010.Top = 3.618945F;
            this.テキスト010.Width = 1.49375F;
            // 
            // テキスト005
            // 
            this.テキスト005.DataField = "ITEM05";
            this.テキスト005.Height = 0.1979167F;
            this.テキスト005.Left = 4.275F;
            this.テキスト005.Name = "テキスト005";
            this.テキスト005.OutputFormat = resources.GetString("テキスト005.OutputFormat");
            this.テキスト005.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト005.Tag = "";
            this.テキスト005.Text = null;
            this.テキスト005.Top = 3.028F;
            this.テキスト005.Width = 1.515371F;
            // 
            // テキスト008
            // 
            this.テキスト008.DataField = "ITEM08";
            this.テキスト008.Height = 0.1979167F;
            this.テキスト008.Left = 4.275F;
            this.テキスト008.Name = "テキスト008";
            this.テキスト008.OutputFormat = resources.GetString("テキスト008.OutputFormat");
            this.テキスト008.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト008.Tag = "";
            this.テキスト008.Text = null;
            this.テキスト008.Top = 3.323276F;
            this.テキスト008.Width = 1.515371F;
            // 
            // テキスト011
            // 
            this.テキスト011.DataField = "ITEM11";
            this.テキスト011.Height = 0.1979167F;
            this.テキスト011.Left = 4.275F;
            this.テキスト011.Name = "テキスト011";
            this.テキスト011.OutputFormat = resources.GetString("テキスト011.OutputFormat");
            this.テキスト011.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト011.Tag = "";
            this.テキスト011.Text = null;
            this.テキスト011.Top = 3.618945F;
            this.テキスト011.Width = 1.515371F;
            // 
            // gfSenShuCd
            // 
            this.gfSenShuCd.CanGrow = false;
            this.gfSenShuCd.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル96,
            this.ITEM39,
            this.ITEM40,
            this.ITEM41,
            this.txtToday});
            this.gfSenShuCd.Height = 2.17715F;
            this.gfSenShuCd.Name = "gfSenShuCd";
            this.gfSenShuCd.NewColumn = GrapeCity.ActiveReports.SectionReportModel.NewColumn.After;
            // 
            // ラベル96
            // 
            this.ラベル96.Height = 0.2291667F;
            this.ラベル96.HyperLink = null;
            this.ラベル96.Left = 2.58504F;
            this.ラベル96.Name = "ラベル96";
            this.ラベル96.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル96.Tag = "";
            this.ラベル96.Text = "上 記 の 通 り 証 明 し ま す 。";
            this.ラベル96.Top = 0.2188976F;
            this.ラベル96.Width = 3.1875F;
            // 
            // ITEM39
            // 
            this.ITEM39.DataField = "ITEM39";
            this.ITEM39.Height = 0.2291667F;
            this.ITEM39.Left = 2.622441F;
            this.ITEM39.Name = "ITEM39";
            this.ITEM39.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.ITEM39.Tag = "";
            this.ITEM39.Text = "ITEM39";
            this.ITEM39.Top = 1.285038F;
            this.ITEM39.Width = 3.539371F;
            // 
            // ITEM40
            // 
            this.ITEM40.DataField = "ITEM40";
            this.ITEM40.Height = 0.2291667F;
            this.ITEM40.Left = 2.622441F;
            this.ITEM40.Name = "ITEM40";
            this.ITEM40.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.ITEM40.Tag = "";
            this.ITEM40.Text = "ITEM40";
            this.ITEM40.Top = 1.614732F;
            this.ITEM40.Width = 3.543397F;
            // 
            // ITEM41
            // 
            this.ITEM41.DataField = "ITEM41";
            this.ITEM41.Height = 0.2291667F;
            this.ITEM41.Left = 2.622441F;
            this.ITEM41.Name = "ITEM41";
            this.ITEM41.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.ITEM41.Tag = "";
            this.ITEM41.Text = "ITEM41";
            this.ITEM41.Top = 1.948065F;
            this.ITEM41.Width = 3.543397F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM42";
            this.txtToday.Height = 0.2291667F;
            this.txtToday.Left = 2.622442F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.txtToday.Tag = "";
            this.txtToday.Text = "=Now()";
            this.txtToday.Top = 0.8543291F;
            this.txtToday.Width = 2.2125F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Format += new System.EventHandler(this.pageFooter_Format);
            // 
            // HNYR1051R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 1.299213F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 6.271654F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghSenShuCd);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfSenShuCd);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: normal; font-family: \"MS UI Gothic\"; ddo-char-set: " +
            "128", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: normal; font-style: inherit; font-family: \"MS UI Go" +
            "thic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: normal; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002Fr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002To)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト018)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト021)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト024)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト027)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト030)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト033)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト036)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.水揚数量合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト016)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト019)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト022)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト025)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト028)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト031)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト034)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト037)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.水揚金額合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト017)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト020)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト023)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト026)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト029)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト032)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト035)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト038)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.手数料合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線15;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線16;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線17;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線18;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線19;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線20;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線21;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線22;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線23;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線24;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線25;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線26;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線27;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線30;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線31;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線32;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線33;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線34;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル35;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル37;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル38;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル39;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル40;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル41;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル42;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル44;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル47;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル48;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル49;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線51;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線52;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル53;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル54;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル55;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト003;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト006;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト009;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト012;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト015;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト018;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト021;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト024;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト027;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト030;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト033;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト036;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 水揚数量合計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト004;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト007;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト010;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト013;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト016;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト019;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト022;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト025;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト028;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト031;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト034;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト037;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 水揚金額合計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト005;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト008;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト011;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト014;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト017;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト023;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト026;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト029;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト032;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト035;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト038;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 手数料合計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト020;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghSenShuCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfSenShuCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル1;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト001;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル5;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト002Fr;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト002To;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル96;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
    }
}
