﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hnyr1051
{
    /// <summary>
    /// 水揚高証明書(HNYR1051)
    /// </summary>
    public partial class HNYR1051 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 精算区分(地区内ｾﾘ)
        /// </summary>
        private const string CHIKUNAI = "3";

        /// <summary>
        /// 精算区分(浜売り)
        /// </summary>
        private const string HAMA_URI = "2";

        /// <summary>
        /// 精算区分(地区内ｾﾘ)
        /// </summary>
        private const string CHIKUGAI = "1";

        /// <summary>
        /// 精算区分(全て)
        /// </summary>
        private const string ALL = "0";

        #region 定数
        private const int prtCols = 44;
        #endregion
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNYR1051()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 年指定
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            // 精算区分
            this.txtSeisanKbn.Text = "0";
            IsValidSeisanKbn();

            // フォーカス設定
            this.txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 年始定、船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                case "txtSeisanKbn":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
                case "txtSeisanKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtSeisanKbn.Text = outData[0];
                                this.lblSeisanKbnNm.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNYR1051R" });
            psForm.ShowDialog();
        }

        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年指定の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                this.txtDateYear.Text = Util.ToString(IsValid.SetYear(this.txtDateYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 精算区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeisanKbn())
            {
                e.Cancel = true;
                this.txtSeisanKbn.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdFr.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba);
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba));
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        private bool IsValidSeisanKbn()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtSeisanKbn.Text, this.txtSeisanKbn.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text) || this.txtSeisanKbn.Text == "0")
            {
                this.txtSeisanKbn.Text = "0";
                this.lblSeisanKbnNm.Text = "全て";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                Msg.Notice("精算区分は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", this.txtMizuageShishoCd.Text, this.txtSeisanKbn.Text);
                if (ValChk.IsEmpty(this.lblSeisanKbnNm.Text))
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 精算区分の入力チェック
            if (!IsValidSeisanKbn())
            {
                this.txtSeisanKbn.Focus();
                this.txtSeisanKbn.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJp();
            // 年月日(自)の正しい和暦への変換処理
            SetJp();

            // 船主コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNYR1051R rpt = new HNYR1051R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.Text;
                    rpt.Document.Name = this.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 日付範囲を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    "1", "1", this.Dba);
            // 表示日付設定
            string hyojiDate = this.lblDateGengo.Text + this.txtDateYear.Text + "年";

            //年度末（翌年表示分）
            int tmpNextYear = int.Parse(txtDateYear.Text) + 1;
            string tmpDateYearTo = tmpNextYear.ToString();
            string[] arrJpDate = Util.FixJpDate(this.lblDateGengo.Text, tmpDateYearTo, "3", "31", this.Dba);
            string hyojiDateTo = arrJpDate[0] + arrJpDate[2] + "年";

            DateTime nowDate = System.DateTime.Now;
            string[] tmpnowDate = Util.ConvJpDate(nowDate, this.Dba);

            // 販売手数料設定
            string tesuryo01 = "";
            string tesuryo02 = "";
            string tesuryo03 = "";
            string tesuryo04 = "";
            string tesuryo05 = "";
            string tesuryo06 = "";
            string tesuryo07 = "";
            string tesuryo08 = "";
            string tesuryo09 = "";
            string tesuryo10 = "";
            string tesuryo11 = "";
            string tesuryo12 = "";

            // 精算区分設定
            string seisanKubunSettei = this.txtSeisanKbn.Text;

            // 船主コード設定
            string FUNANUSHI_CD_FR;
            string FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            #region 支所コードの退避
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            #endregion
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            // Com.TB_会社情報(TB_CM_KAISHA_JOHO)のデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" KAISHA_NM,");
            Sql.Append(" JUSHO1,");
            Sql.Append(" DAIHYOSHA_NM");
            Sql.Append(" FROM");
            Sql.Append(" TB_CM_KAISHA_JOHO");
            Sql.Append(" WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            DataTable dtKaishaJoho = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            // han.VI_水揚高証明書(VI_HN_MIZUAGEDAKA_SHOMEISHO)の日付範囲から発生しているデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" MIN(SENSHU_CD) AS SENSHU_CD,");
            Sql.Append(" MIN(SENSHU_NM) AS SENSHU_NM,");
            Sql.Append(" SUM(SURYO_1GATSU) AS SURYO_1GATSU,");
            Sql.Append(" SUM(KINGAKU_1GATSU) AS KINGAKU_1GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_1GATSU) AS HANBAI_TESURYO_1GATSU,");
            Sql.Append(" SUM(SURYO_2GATSU) AS SURYO_2GATSU,");
            Sql.Append(" SUM(KINGAKU_2GATSU) AS KINGAKU_2GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_2GATSU) AS HANBAI_TESURYO_2GATSU,");
            Sql.Append(" SUM(SURYO_3GATSU) AS SURYO_3GATSU,");
            Sql.Append(" SUM(KINGAKU_3GATSU) AS KINGAKU_3GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_3GATSU) AS HANBAI_TESURYO_3GATSU,");
            Sql.Append(" SUM(SURYO_4GATSU) AS SURYO_4GATSU,");
            Sql.Append(" SUM(KINGAKU_4GATSU) AS KINGAKU_4GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_4GATSU) AS HANBAI_TESURYO_4GATSU,");
            Sql.Append(" SUM(SURYO_5GATSU) AS SURYO_5GATSU,");
            Sql.Append(" SUM(KINGAKU_5GATSU) AS KINGAKU_5GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_5GATSU) AS HANBAI_TESURYO_5GATSU,");
            Sql.Append(" SUM(SURYO_6GATSU) AS SURYO_6GATSU,");
            Sql.Append(" SUM(KINGAKU_6GATSU) AS KINGAKU_6GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_6GATSU) AS HANBAI_TESURYO_6GATSU,");
            Sql.Append(" SUM(SURYO_7GATSU) AS SURYO_7GATSU,");
            Sql.Append(" SUM(KINGAKU_7GATSU) AS KINGAKU_7GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_7GATSU) AS HANBAI_TESURYO_7GATSU,");
            Sql.Append(" SUM(SURYO_8GATSU) AS SURYO_8GATSU,");
            Sql.Append(" SUM(KINGAKU_8GATSU) AS KINGAKU_8GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_8GATSU) AS HANBAI_TESURYO_8GATSU,");
            Sql.Append(" SUM(SURYO_9GATSU) AS SURYO_9GATSU,");
            Sql.Append(" SUM(KINGAKU_9GATSU) AS KINGAKU_9GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_9GATSU) AS HANBAI_TESURYO_9GATSU,");
            Sql.Append(" SUM(SURYO_10GATSU) AS SURYO_10GATSU,");
            Sql.Append(" SUM(KINGAKU_10GATSU) AS KINGAKU_10GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_10GATSU) AS HANBAI_TESURYO_10GATSU,");
            Sql.Append(" SUM(SURYO_11GATSU) AS SURYO_11GATSU,");
            Sql.Append(" SUM(KINGAKU_11GATSU) AS KINGAKU_11GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_11GATSU) AS HANBAI_TESURYO_11GATSU,");
            Sql.Append(" SUM(SURYO_12GATSU) AS SURYO_12GATSU,");
            Sql.Append(" SUM(KINGAKU_12GATSU) AS KINGAKU_12GATSU,");
            Sql.Append(" SUM(HANBAI_TESURYO_12GATSU) AS HANBAI_TESURYO_12GATSU");
            Sql.Append(" FROM");
            Sql.Append(" VI_HN_MIZUAGEDAKA_SHOMEISHO");
            Sql.Append(" WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" SHUKEI_NEN = @SHUKEI_NEN AND");
            Sql.Append(" SENSHU_CD BETWEEN @SENSHU_CD_FR AND @SENSHU_CD_TO");
            if (seisanKubunSettei != ALL)
            {
                Sql.Append(" AND SEISAN_KUBUN = @SEISAN_KUBUN");
            }
            Sql.Append(" GROUP BY");
            Sql.Append(" KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD,");
            Sql.Append(" SENSHU_CD, SHUKEI_NEN");
            Sql.Append(" ORDER BY");
            Sql.Append(" KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" SHISHO_CD,");
            Sql.Append(" SENSHU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHUKEI_NEN", SqlDbType.Decimal, 4, tmpDate.Year);
            dpc.SetParam("@SENSHU_CD_FR", SqlDbType.Decimal, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@SENSHU_CD_TO", SqlDbType.Decimal, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.VarChar, 1, seisanKubunSettei);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion
            
            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                string shamei = Util.ToString(dtKaishaJoho.Rows[0]["KAISHA_NM"]);
                string jusho = Util.ToString(dtKaishaJoho.Rows[0]["JUSHO1"]);
                string shimei = Util.ToString(dtKaishaJoho.Rows[0]["DAIHYOSHA_NM"]);

                while (dtMainLoop.Rows.Count > i)
                {
                    #region 手数料設定
                    if (chkTesuryo.Checked)
                    {
                        tesuryo01 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_1GATSU"]);
                        tesuryo02 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_2GATSU"]);
                        tesuryo03 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_3GATSU"]);
                        tesuryo04 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_4GATSU"]);
                        tesuryo05 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_5GATSU"]);
                        tesuryo06 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_6GATSU"]);
                        tesuryo07 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_7GATSU"]);
                        tesuryo08 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_8GATSU"]);
                        tesuryo09 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_9GATSU"]);
                        tesuryo10 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_10GATSU"]);
                        tesuryo11 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_11GATSU"]);
                        tesuryo12 = Util.ToString(dtMainLoop.Rows[i]["HANBAI_TESURYO_12GATSU"]);
                    }
                    #endregion

                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    //Sql.Append(" ,ITEM01");
                    //Sql.Append(" ,ITEM02");
                    //Sql.Append(" ,ITEM03");
                    //Sql.Append(" ,ITEM04");
                    //Sql.Append(" ,ITEM05");
                    //Sql.Append(" ,ITEM06");
                    //Sql.Append(" ,ITEM07");
                    //Sql.Append(" ,ITEM08");
                    //Sql.Append(" ,ITEM09");
                    //Sql.Append(" ,ITEM10");
                    //Sql.Append(" ,ITEM11");
                    //Sql.Append(" ,ITEM12");
                    //Sql.Append(" ,ITEM13");
                    //Sql.Append(" ,ITEM14");
                    //Sql.Append(" ,ITEM15");
                    //Sql.Append(" ,ITEM16");
                    //Sql.Append(" ,ITEM17");
                    //Sql.Append(" ,ITEM18");
                    //Sql.Append(" ,ITEM19");
                    //Sql.Append(" ,ITEM20");
                    //Sql.Append(" ,ITEM21");
                    //Sql.Append(" ,ITEM22");
                    //Sql.Append(" ,ITEM23");
                    //Sql.Append(" ,ITEM24");
                    //Sql.Append(" ,ITEM25");
                    //Sql.Append(" ,ITEM26");
                    //Sql.Append(" ,ITEM27");
                    //Sql.Append(" ,ITEM28");
                    //Sql.Append(" ,ITEM29");
                    //Sql.Append(" ,ITEM30");
                    //Sql.Append(" ,ITEM31");
                    //Sql.Append(" ,ITEM32");
                    //Sql.Append(" ,ITEM33");
                    //Sql.Append(" ,ITEM34");
                    //Sql.Append(" ,ITEM35");
                    //Sql.Append(" ,ITEM36");
                    //Sql.Append(" ,ITEM37");
                    //Sql.Append(" ,ITEM38");
                    //Sql.Append(" ,ITEM39");
                    //Sql.Append(" ,ITEM40");
                    //Sql.Append(" ,ITEM41");
                    //Sql.Append(") ");
                    //Sql.Append("VALUES(");
                    //Sql.Append("  @GUID");
                    //Sql.Append(" ,@SORT");
                    //Sql.Append(" ,@ITEM01");
                    //Sql.Append(" ,@ITEM02");
                    //Sql.Append(" ,@ITEM03");
                    //Sql.Append(" ,@ITEM04");
                    //Sql.Append(" ,@ITEM05");
                    //Sql.Append(" ,@ITEM06");
                    //Sql.Append(" ,@ITEM07");
                    //Sql.Append(" ,@ITEM08");
                    //Sql.Append(" ,@ITEM09");
                    //Sql.Append(" ,@ITEM10");
                    //Sql.Append(" ,@ITEM11");
                    //Sql.Append(" ,@ITEM12");
                    //Sql.Append(" ,@ITEM13");
                    //Sql.Append(" ,@ITEM14");
                    //Sql.Append(" ,@ITEM15");
                    //Sql.Append(" ,@ITEM16");
                    //Sql.Append(" ,@ITEM17");
                    //Sql.Append(" ,@ITEM18");
                    //Sql.Append(" ,@ITEM19");
                    //Sql.Append(" ,@ITEM20");
                    //Sql.Append(" ,@ITEM21");
                    //Sql.Append(" ,@ITEM22");
                    //Sql.Append(" ,@ITEM23");
                    //Sql.Append(" ,@ITEM24");
                    //Sql.Append(" ,@ITEM25");
                    //Sql.Append(" ,@ITEM26");
                    //Sql.Append(" ,@ITEM27");
                    //Sql.Append(" ,@ITEM28");
                    //Sql.Append(" ,@ITEM29");
                    //Sql.Append(" ,@ITEM30");
                    //Sql.Append(" ,@ITEM31");
                    //Sql.Append(" ,@ITEM32");
                    //Sql.Append(" ,@ITEM33");
                    //Sql.Append(" ,@ITEM34");
                    //Sql.Append(" ,@ITEM35");
                    //Sql.Append(" ,@ITEM36");
                    //Sql.Append(" ,@ITEM37");
                    //Sql.Append(" ,@ITEM38");
                    //Sql.Append(" ,@ITEM39");
                    //Sql.Append(" ,@ITEM40");
                    //Sql.Append(" ,@ITEM41");
                    //Sql.Append(") ");
                    Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                    Sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    //TODO 20181218 SJ 追加
                    dpc.SetParam("@ITEM43", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主名称
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyojiDate); // 年指定
                    dpc.SetParam("@ITEM44", SqlDbType.VarChar, 200, hyojiDateTo); // 年度末指定
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_1GATSU"]); // 水揚数量01
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_1GATSU"]); // 水揚金額01
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tesuryo01); // 水揚手数料01
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_2GATSU"]); // 水揚数量02
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_2GATSU"]); // 水揚金額02
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, tesuryo02); // 水揚手数料02
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_3GATSU"]); // 水揚数量03
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_3GATSU"]); // 水揚金額03
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, tesuryo03); // 水揚手数料03
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_4GATSU"]); // 水揚数量04
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_4GATSU"]); // 水揚金額04
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, tesuryo04); // 水揚手数料04
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_5GATSU"]); // 水揚数量05
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_5GATSU"]); // 水揚金額05
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, tesuryo05); // 水揚手数料05
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_6GATSU"]); // 水揚数量06
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_6GATSU"]); // 水揚金額06
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, tesuryo06); // 水揚手数料06
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_7GATSU"]); // 水揚数量07
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_7GATSU"]); // 水揚金額07
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, tesuryo07); // 水揚手数料07
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_8GATSU"]); // 水揚数量08
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_8GATSU"]); // 水揚金額08
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, tesuryo08); // 水揚手数料08
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_9GATSU"]); // 水揚数量09
                    dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_9GATSU"]); // 水揚金額09
                    dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, tesuryo09); // 水揚手数料09
                    dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_10GATSU"]); // 水揚数量10
                    dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_10GATSU"]); // 水揚金額10
                    dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, tesuryo10); // 水揚手数料10
                    dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_11GATSU"]); // 水揚数量11
                    dpc.SetParam("@ITEM34", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_11GATSU"]); // 水揚金額11
                    dpc.SetParam("@ITEM35", SqlDbType.VarChar, 200, tesuryo11); // 水揚手数料11
                    dpc.SetParam("@ITEM36", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SURYO_12GATSU"]); // 水揚数量12
                    dpc.SetParam("@ITEM37", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KINGAKU_12GATSU"]); // 水揚金額12
                    dpc.SetParam("@ITEM38", SqlDbType.VarChar, 200, tesuryo12); // 水揚手数料12
                    dpc.SetParam("@ITEM39", SqlDbType.VarChar, 200, jusho); // 住所
                    dpc.SetParam("@ITEM40", SqlDbType.VarChar, 200, shamei); // 社名
                    dpc.SetParam("@ITEM41", SqlDbType.VarChar, 200, shimei); // 氏名

                    dpc.SetParam("@ITEM42", SqlDbType.VarChar, 200, tmpnowDate[5]); // 

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion
                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
