﻿namespace jp.co.fsi.hn.hndr1081
{
    partial class HNDR1081
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblDateGengoFr = new System.Windows.Forms.Label();
			this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.labelDateYearFr = new System.Windows.Forms.Label();
			this.lblDateMonthFr = new System.Windows.Forms.Label();
			this.lblDateDayFr = new System.Windows.Forms.Label();
			this.lblDateDayTo = new System.Windows.Forms.Label();
			this.lblDateMonthTo = new System.Windows.Forms.Label();
			this.lblDateBet = new System.Windows.Forms.Label();
			this.labelDateYearTo = new System.Windows.Forms.Label();
			this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoTo = new System.Windows.Forms.Label();
			this.lblFunanushiCdTo = new System.Windows.Forms.Label();
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiCdFr = new System.Windows.Forms.Label();
			this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.rdoGokei = new System.Windows.Forms.RadioButton();
			this.rdoMeisai = new System.Windows.Forms.RadioButton();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(9, 812);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblDateGengoFr
			// 
			this.lblDateGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoFr.Location = new System.Drawing.Point(119, 2);
			this.lblDateGengoFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengoFr.Name = "lblDateGengoFr";
			this.lblDateGengoFr.Size = new System.Drawing.Size(66, 24);
			this.lblDateGengoFr.TabIndex = 1;
			this.lblDateGengoFr.Tag = "DISPNAME";
			this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateMonthFr
			// 
			this.txtDateMonthFr.AutoSizeFromLength = false;
			this.txtDateMonthFr.DisplayLength = null;
			this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthFr.Location = new System.Drawing.Point(266, 3);
			this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateMonthFr.MaxLength = 2;
			this.txtDateMonthFr.Name = "txtDateMonthFr";
			this.txtDateMonthFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthFr.TabIndex = 3;
			this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
			// 
			// txtDateYearFr
			// 
			this.txtDateYearFr.AutoSizeFromLength = false;
			this.txtDateYearFr.DisplayLength = null;
			this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearFr.Location = new System.Drawing.Point(190, 3);
			this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYearFr.MaxLength = 2;
			this.txtDateYearFr.Name = "txtDateYearFr";
			this.txtDateYearFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearFr.TabIndex = 2;
			this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
			// 
			// txtDateDayFr
			// 
			this.txtDateDayFr.AutoSizeFromLength = false;
			this.txtDateDayFr.DisplayLength = null;
			this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateDayFr.Location = new System.Drawing.Point(343, 3);
			this.txtDateDayFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateDayFr.MaxLength = 2;
			this.txtDateDayFr.Name = "txtDateDayFr";
			this.txtDateDayFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateDayFr.TabIndex = 4;
			this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
			// 
			// labelDateYearFr
			// 
			this.labelDateYearFr.AutoSize = true;
			this.labelDateYearFr.BackColor = System.Drawing.Color.Silver;
			this.labelDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.labelDateYearFr.Location = new System.Drawing.Point(233, 7);
			this.labelDateYearFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.labelDateYearFr.Name = "labelDateYearFr";
			this.labelDateYearFr.Size = new System.Drawing.Size(24, 16);
			this.labelDateYearFr.TabIndex = 3;
			this.labelDateYearFr.Tag = "CHANGE";
			this.labelDateYearFr.Text = "年";
			this.labelDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateMonthFr
			// 
			this.lblDateMonthFr.AutoSize = true;
			this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateMonthFr.Location = new System.Drawing.Point(309, 7);
			this.lblDateMonthFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateMonthFr.Name = "lblDateMonthFr";
			this.lblDateMonthFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthFr.TabIndex = 5;
			this.lblDateMonthFr.Tag = "CHANGE";
			this.lblDateMonthFr.Text = "月";
			this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateDayFr
			// 
			this.lblDateDayFr.AutoSize = true;
			this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateDayFr.Location = new System.Drawing.Point(386, 7);
			this.lblDateDayFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateDayFr.Name = "lblDateDayFr";
			this.lblDateDayFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateDayFr.TabIndex = 7;
			this.lblDateDayFr.Tag = "CHANGE";
			this.lblDateDayFr.Text = "日";
			this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateDayTo
			// 
			this.lblDateDayTo.AutoSize = true;
			this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateDayTo.Location = new System.Drawing.Point(711, 7);
			this.lblDateDayTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateDayTo.Name = "lblDateDayTo";
			this.lblDateDayTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateDayTo.TabIndex = 16;
			this.lblDateDayTo.Tag = "CHANGE";
			this.lblDateDayTo.Text = "日";
			this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateMonthTo
			// 
			this.lblDateMonthTo.AutoSize = true;
			this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateMonthTo.Location = new System.Drawing.Point(635, 7);
			this.lblDateMonthTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateMonthTo.Name = "lblDateMonthTo";
			this.lblDateMonthTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthTo.TabIndex = 14;
			this.lblDateMonthTo.Tag = "CHANGE";
			this.lblDateMonthTo.Text = "月";
			this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateBet
			// 
			this.lblDateBet.AutoSize = true;
			this.lblDateBet.BackColor = System.Drawing.Color.Silver;
			this.lblDateBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateBet.Location = new System.Drawing.Point(417, 7);
			this.lblDateBet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateBet.Name = "lblDateBet";
			this.lblDateBet.Size = new System.Drawing.Size(24, 16);
			this.lblDateBet.TabIndex = 8;
			this.lblDateBet.Tag = "CHANGE";
			this.lblDateBet.Text = "～";
			this.lblDateBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelDateYearTo
			// 
			this.labelDateYearTo.AutoSize = true;
			this.labelDateYearTo.BackColor = System.Drawing.Color.Silver;
			this.labelDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.labelDateYearTo.Location = new System.Drawing.Point(559, 7);
			this.labelDateYearTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.labelDateYearTo.Name = "labelDateYearTo";
			this.labelDateYearTo.Size = new System.Drawing.Size(24, 16);
			this.labelDateYearTo.TabIndex = 12;
			this.labelDateYearTo.Tag = "CHANGE";
			this.labelDateYearTo.Text = "年";
			this.labelDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateDayTo
			// 
			this.txtDateDayTo.AutoSizeFromLength = false;
			this.txtDateDayTo.DisplayLength = null;
			this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateDayTo.Location = new System.Drawing.Point(668, 3);
			this.txtDateDayTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateDayTo.MaxLength = 2;
			this.txtDateDayTo.Name = "txtDateDayTo";
			this.txtDateDayTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateDayTo.TabIndex = 7;
			this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
			// 
			// txtDateYearTo
			// 
			this.txtDateYearTo.AutoSizeFromLength = false;
			this.txtDateYearTo.DisplayLength = null;
			this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearTo.Location = new System.Drawing.Point(516, 3);
			this.txtDateYearTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYearTo.MaxLength = 2;
			this.txtDateYearTo.Name = "txtDateYearTo";
			this.txtDateYearTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearTo.TabIndex = 5;
			this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
			// 
			// txtDateMonthTo
			// 
			this.txtDateMonthTo.AutoSizeFromLength = false;
			this.txtDateMonthTo.DisplayLength = null;
			this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthTo.Location = new System.Drawing.Point(592, 3);
			this.txtDateMonthTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateMonthTo.MaxLength = 2;
			this.txtDateMonthTo.Name = "txtDateMonthTo";
			this.txtDateMonthTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthTo.TabIndex = 6;
			this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
			// 
			// lblDateGengoTo
			// 
			this.lblDateGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoTo.Location = new System.Drawing.Point(445, 2);
			this.lblDateGengoTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengoTo.Name = "lblDateGengoTo";
			this.lblDateGengoTo.Size = new System.Drawing.Size(66, 24);
			this.lblDateGengoTo.TabIndex = 10;
			this.lblDateGengoTo.Tag = "DISPNAME";
			this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiCdTo
			// 
			this.lblFunanushiCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
			this.lblFunanushiCdTo.Location = new System.Drawing.Point(559, 2);
			this.lblFunanushiCdTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
			this.lblFunanushiCdTo.Size = new System.Drawing.Size(283, 24);
			this.lblFunanushiCdTo.TabIndex = 4;
			this.lblFunanushiCdTo.Tag = "DISPNAME";
			this.lblFunanushiCdTo.Text = "最　後";
			this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.AutoSize = true;
			this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBet.ForeColor = System.Drawing.Color.Black;
			this.lblCodeBet.Location = new System.Drawing.Point(464, 7);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 16);
			this.lblCodeBet.TabIndex = 2;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdFr
			// 
			this.txtFunanushiCdFr.AutoSizeFromLength = false;
			this.txtFunanushiCdFr.DisplayLength = null;
			this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
			this.txtFunanushiCdFr.Location = new System.Drawing.Point(119, 3);
			this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtFunanushiCdFr.MaxLength = 4;
			this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
			this.txtFunanushiCdFr.Size = new System.Drawing.Size(52, 23);
			this.txtFunanushiCdFr.TabIndex = 8;
			this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
			// 
			// lblFunanushiCdFr
			// 
			this.lblFunanushiCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
			this.lblFunanushiCdFr.Location = new System.Drawing.Point(172, 2);
			this.lblFunanushiCdFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
			this.lblFunanushiCdFr.Size = new System.Drawing.Size(283, 24);
			this.lblFunanushiCdFr.TabIndex = 1;
			this.lblFunanushiCdFr.Tag = "DISPNAME";
			this.lblFunanushiCdFr.Text = "先　頭";
			this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdTo
			// 
			this.txtFunanushiCdTo.AutoSizeFromLength = false;
			this.txtFunanushiCdTo.DisplayLength = null;
			this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
			this.txtFunanushiCdTo.Location = new System.Drawing.Point(503, 3);
			this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtFunanushiCdTo.MaxLength = 4;
			this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
			this.txtFunanushiCdTo.Size = new System.Drawing.Size(52, 23);
			this.txtFunanushiCdTo.TabIndex = 9;
			this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
			this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(119, 3);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(165, 2);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// rdoGokei
			// 
			this.rdoGokei.AutoSize = true;
			this.rdoGokei.BackColor = System.Drawing.Color.Silver;
			this.rdoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoGokei.Location = new System.Drawing.Point(252, 5);
			this.rdoGokei.Margin = new System.Windows.Forms.Padding(5);
			this.rdoGokei.Name = "rdoGokei";
			this.rdoGokei.Size = new System.Drawing.Size(90, 20);
			this.rdoGokei.TabIndex = 1;
			this.rdoGokei.TabStop = true;
			this.rdoGokei.Tag = "CHANGE";
			this.rdoGokei.Text = "合計表示";
			this.rdoGokei.UseVisualStyleBackColor = false;
			// 
			// rdoMeisai
			// 
			this.rdoMeisai.AutoSize = true;
			this.rdoMeisai.BackColor = System.Drawing.Color.Silver;
			this.rdoMeisai.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoMeisai.Location = new System.Drawing.Point(117, 5);
			this.rdoMeisai.Margin = new System.Windows.Forms.Padding(5);
			this.rdoMeisai.Name = "rdoMeisai";
			this.rdoMeisai.Size = new System.Drawing.Size(90, 20);
			this.rdoMeisai.TabIndex = 0;
			this.rdoMeisai.TabStop = true;
			this.rdoMeisai.Tag = "CHANGE";
			this.rdoMeisai.Text = "明細表示";
			this.rdoMeisai.UseVisualStyleBackColor = false;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label1.MinimumSize = new System.Drawing.Size(0, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(891, 32);
			this.label1.TabIndex = 2;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "水揚支所";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label2.MinimumSize = new System.Drawing.Size(0, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(891, 32);
			this.label2.TabIndex = 2;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "表示方法";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label3.MinimumSize = new System.Drawing.Size(0, 32);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(891, 32);
			this.label3.TabIndex = 2;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "日付範囲";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.label4.MinimumSize = new System.Drawing.Size(0, 32);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(891, 32);
			this.label4.TabIndex = 2;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "船主CD範囲";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 47);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 4;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(901, 153);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lblFunanushiCdTo);
			this.fsiPanel4.Controls.Add(this.txtFunanushiCdFr);
			this.fsiPanel4.Controls.Add(this.lblCodeBet);
			this.fsiPanel4.Controls.Add(this.txtFunanushiCdTo);
			this.fsiPanel4.Controls.Add(this.lblFunanushiCdFr);
			this.fsiPanel4.Controls.Add(this.label4);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(5, 119);
			this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(891, 29);
			this.fsiPanel4.TabIndex = 3;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblDateGengoFr);
			this.fsiPanel3.Controls.Add(this.lblDateDayTo);
			this.fsiPanel3.Controls.Add(this.txtDateMonthFr);
			this.fsiPanel3.Controls.Add(this.lblDateGengoTo);
			this.fsiPanel3.Controls.Add(this.txtDateMonthTo);
			this.fsiPanel3.Controls.Add(this.txtDateYearFr);
			this.fsiPanel3.Controls.Add(this.lblDateDayFr);
			this.fsiPanel3.Controls.Add(this.txtDateYearTo);
			this.fsiPanel3.Controls.Add(this.txtDateDayFr);
			this.fsiPanel3.Controls.Add(this.lblDateMonthTo);
			this.fsiPanel3.Controls.Add(this.txtDateDayTo);
			this.fsiPanel3.Controls.Add(this.labelDateYearFr);
			this.fsiPanel3.Controls.Add(this.lblDateBet);
			this.fsiPanel3.Controls.Add(this.labelDateYearTo);
			this.fsiPanel3.Controls.Add(this.lblDateMonthFr);
			this.fsiPanel3.Controls.Add(this.label3);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 81);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(891, 29);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.rdoMeisai);
			this.fsiPanel2.Controls.Add(this.rdoGokei);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 43);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(891, 29);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(891, 29);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNDR1081
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNDR1081";
			this.Text = "";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.Label lblDateGengoFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayFr;
        private System.Windows.Forms.Label labelDateYearFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label labelDateYearTo;
        private common.controls.FsiTextBox txtDateDayTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateBet;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.RadioButton rdoGokei;
        private System.Windows.Forms.RadioButton rdoMeisai;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}
