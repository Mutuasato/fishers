﻿namespace jp.co.fsi.hn.hnsr1021
{
    partial class HNSR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.lblDayTo = new System.Windows.Forms.Label();
			this.lblMonthTo = new System.Windows.Forms.Label();
			this.lblYearTo = new System.Windows.Forms.Label();
			this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoTo = new System.Windows.Forms.Label();
			this.lblDayFr = new System.Windows.Forms.Label();
			this.lblMonthFr = new System.Windows.Forms.Label();
			this.lblYearFr = new System.Windows.Forms.Label();
			this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoFr = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 609);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "仕切チェックリスト";
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.AutoSize = true;
			this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBet.ForeColor = System.Drawing.Color.Black;
			this.lblCodeBet.Location = new System.Drawing.Point(388, 6);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 16);
			this.lblCodeBet.TabIndex = 8;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblDayTo
			// 
			this.lblDayTo.AutoSize = true;
			this.lblDayTo.BackColor = System.Drawing.Color.Silver;
			this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayTo.ForeColor = System.Drawing.Color.Black;
			this.lblDayTo.Location = new System.Drawing.Point(666, 6);
			this.lblDayTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDayTo.Name = "lblDayTo";
			this.lblDayTo.Size = new System.Drawing.Size(24, 16);
			this.lblDayTo.TabIndex = 16;
			this.lblDayTo.Tag = "CHANGE";
			this.lblDayTo.Text = "日";
			this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMonthTo
			// 
			this.lblMonthTo.AutoSize = true;
			this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthTo.ForeColor = System.Drawing.Color.Black;
			this.lblMonthTo.Location = new System.Drawing.Point(593, 6);
			this.lblMonthTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMonthTo.Name = "lblMonthTo";
			this.lblMonthTo.Size = new System.Drawing.Size(24, 16);
			this.lblMonthTo.TabIndex = 14;
			this.lblMonthTo.Tag = "CHANGE";
			this.lblMonthTo.Text = "月";
			this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearTo
			// 
			this.lblYearTo.AutoSize = true;
			this.lblYearTo.BackColor = System.Drawing.Color.Silver;
			this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearTo.ForeColor = System.Drawing.Color.Black;
			this.lblYearTo.Location = new System.Drawing.Point(522, 6);
			this.lblYearTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblYearTo.Name = "lblYearTo";
			this.lblYearTo.Size = new System.Drawing.Size(24, 16);
			this.lblYearTo.TabIndex = 12;
			this.lblYearTo.Tag = "CHANGE";
			this.lblYearTo.Text = "年";
			this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDayTo
			// 
			this.txtDayTo.AutoSizeFromLength = false;
			this.txtDayTo.DisplayLength = null;
			this.txtDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDayTo.ForeColor = System.Drawing.Color.Black;
			this.txtDayTo.Location = new System.Drawing.Point(622, 2);
			this.txtDayTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtDayTo.MaxLength = 2;
			this.txtDayTo.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtDayTo.Name = "txtDayTo";
			this.txtDayTo.Size = new System.Drawing.Size(39, 23);
			this.txtDayTo.TabIndex = 7;
			this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDayTo_KeyDown);
			this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
			// 
			// txtYearTo
			// 
			this.txtYearTo.AutoSizeFromLength = false;
			this.txtYearTo.DisplayLength = null;
			this.txtYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYearTo.ForeColor = System.Drawing.Color.Black;
			this.txtYearTo.Location = new System.Drawing.Point(480, 2);
			this.txtYearTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtYearTo.MaxLength = 2;
			this.txtYearTo.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtYearTo.Name = "txtYearTo";
			this.txtYearTo.Size = new System.Drawing.Size(39, 23);
			this.txtYearTo.TabIndex = 5;
			this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
			// 
			// txtMonthTo
			// 
			this.txtMonthTo.AutoSizeFromLength = false;
			this.txtMonthTo.DisplayLength = null;
			this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonthTo.ForeColor = System.Drawing.Color.Black;
			this.txtMonthTo.Location = new System.Drawing.Point(550, 2);
			this.txtMonthTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtMonthTo.MaxLength = 2;
			this.txtMonthTo.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtMonthTo.Name = "txtMonthTo";
			this.txtMonthTo.Size = new System.Drawing.Size(39, 23);
			this.txtMonthTo.TabIndex = 6;
			this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
			// 
			// lblGengoTo
			// 
			this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengoTo.ForeColor = System.Drawing.Color.Black;
			this.lblGengoTo.Location = new System.Drawing.Point(420, 2);
			this.lblGengoTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblGengoTo.Name = "lblGengoTo";
			this.lblGengoTo.Size = new System.Drawing.Size(55, 24);
			this.lblGengoTo.TabIndex = 10;
			this.lblGengoTo.Tag = "DISPNAME";
			this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDayFr
			// 
			this.lblDayFr.AutoSize = true;
			this.lblDayFr.BackColor = System.Drawing.Color.Silver;
			this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayFr.ForeColor = System.Drawing.Color.Black;
			this.lblDayFr.Location = new System.Drawing.Point(357, 6);
			this.lblDayFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDayFr.Name = "lblDayFr";
			this.lblDayFr.Size = new System.Drawing.Size(24, 16);
			this.lblDayFr.TabIndex = 7;
			this.lblDayFr.Tag = "CHANGE";
			this.lblDayFr.Text = "日";
			this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMonthFr
			// 
			this.lblMonthFr.AutoSize = true;
			this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthFr.ForeColor = System.Drawing.Color.Black;
			this.lblMonthFr.Location = new System.Drawing.Point(284, 6);
			this.lblMonthFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMonthFr.Name = "lblMonthFr";
			this.lblMonthFr.Size = new System.Drawing.Size(24, 16);
			this.lblMonthFr.TabIndex = 5;
			this.lblMonthFr.Tag = "CHANGE";
			this.lblMonthFr.Text = "月";
			this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearFr
			// 
			this.lblYearFr.AutoSize = true;
			this.lblYearFr.BackColor = System.Drawing.Color.Silver;
			this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearFr.ForeColor = System.Drawing.Color.Black;
			this.lblYearFr.Location = new System.Drawing.Point(213, 6);
			this.lblYearFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblYearFr.Name = "lblYearFr";
			this.lblYearFr.Size = new System.Drawing.Size(24, 16);
			this.lblYearFr.TabIndex = 3;
			this.lblYearFr.Tag = "CHANGE";
			this.lblYearFr.Text = "年";
			this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDayFr
			// 
			this.txtDayFr.AutoSizeFromLength = false;
			this.txtDayFr.DisplayLength = null;
			this.txtDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDayFr.ForeColor = System.Drawing.Color.Black;
			this.txtDayFr.Location = new System.Drawing.Point(313, 2);
			this.txtDayFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtDayFr.MaxLength = 2;
			this.txtDayFr.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtDayFr.Name = "txtDayFr";
			this.txtDayFr.Size = new System.Drawing.Size(39, 23);
			this.txtDayFr.TabIndex = 4;
			this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
			// 
			// txtYearFr
			// 
			this.txtYearFr.AutoSizeFromLength = false;
			this.txtYearFr.DisplayLength = null;
			this.txtYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYearFr.ForeColor = System.Drawing.Color.Black;
			this.txtYearFr.Location = new System.Drawing.Point(171, 2);
			this.txtYearFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtYearFr.MaxLength = 2;
			this.txtYearFr.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtYearFr.Name = "txtYearFr";
			this.txtYearFr.Size = new System.Drawing.Size(39, 23);
			this.txtYearFr.TabIndex = 2;
			this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
			// 
			// txtMonthFr
			// 
			this.txtMonthFr.AutoSizeFromLength = false;
			this.txtMonthFr.DisplayLength = null;
			this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonthFr.ForeColor = System.Drawing.Color.Black;
			this.txtMonthFr.Location = new System.Drawing.Point(241, 2);
			this.txtMonthFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtMonthFr.MaxLength = 2;
			this.txtMonthFr.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtMonthFr.Name = "txtMonthFr";
			this.txtMonthFr.Size = new System.Drawing.Size(39, 23);
			this.txtMonthFr.TabIndex = 3;
			this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
			// 
			// lblGengoFr
			// 
			this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengoFr.ForeColor = System.Drawing.Color.Black;
			this.lblGengoFr.Location = new System.Drawing.Point(108, 2);
			this.lblGengoFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblGengoFr.Name = "lblGengoFr";
			this.lblGengoFr.Size = new System.Drawing.Size(55, 24);
			this.lblGengoFr.TabIndex = 1;
			this.lblGengoFr.Tag = "DISPNAME";
			this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(104, 2);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.MinimumSize = new System.Drawing.Size(4, 24);
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(156, 1);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(698, 29);
			this.lblMizuageShisho.TabIndex = 0;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "水揚支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(698, 30);
			this.label1.TabIndex = 17;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "日付範囲";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(708, 78);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblCodeBet);
			this.fsiPanel2.Controls.Add(this.lblGengoFr);
			this.fsiPanel2.Controls.Add(this.txtDayFr);
			this.fsiPanel2.Controls.Add(this.lblDayTo);
			this.fsiPanel2.Controls.Add(this.lblYearFr);
			this.fsiPanel2.Controls.Add(this.txtMonthFr);
			this.fsiPanel2.Controls.Add(this.lblMonthTo);
			this.fsiPanel2.Controls.Add(this.lblMonthFr);
			this.fsiPanel2.Controls.Add(this.lblYearTo);
			this.fsiPanel2.Controls.Add(this.lblDayFr);
			this.fsiPanel2.Controls.Add(this.txtDayTo);
			this.fsiPanel2.Controls.Add(this.txtYearFr);
			this.fsiPanel2.Controls.Add(this.txtYearTo);
			this.fsiPanel2.Controls.Add(this.lblGengoTo);
			this.fsiPanel2.Controls.Add(this.txtMonthTo);
			this.fsiPanel2.Controls.Add(this.label1);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 43);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(698, 30);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(698, 29);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNSR1021
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNSR1021";
			this.Text = "";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }
        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private common.controls.FsiTextBox txtDayTo;
        private common.controls.FsiTextBox txtYearTo;
        private common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblGengoTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label label1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}