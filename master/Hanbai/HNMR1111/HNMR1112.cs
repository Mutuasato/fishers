﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1111
{
    /// <summary>
    /// 項目設定(HNMR1112)
    /// </summary>
    public partial class HNMR1112 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// HNMR1112(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        //String _pSHUKEIHYO;

        string TB_NAME;
        #endregion

        #region 定数
        /// <summary>
        /// データ取得用
        /// </summary>
        //private const int POSITION_START_NO = 1;
        //private const int POSITION_END_NO = 8;


        /// <summary>
        /// 期首残科目テーブル
        /// </summary>
        private const string TB_NAME_KZ = "TB_HN_KISHU_ZAN_KAMOKU";
        /// <summary>
        /// 入金科目テーブル
        /// </summary>
        private const string TB_NAME_NK = "TB_HN_NYUKIN_KAMOKU";

        /// <summary>
        /// 勘定科目テーブル
        /// </summary>
        private const string TB_NAME_ZM = "TB_ZM_KANJO_KAMOKU";
        #endregion

        #region プロパティ
        /// <summary>
        /// タイトルを格納する用データテーブル
        /// </summary>
        private DataTable _dtTitle = new DataTable();
        public DataTable Title
        {
            get
            {
                return this._dtTitle;
            }
        }

        /// <summary>
        /// 集計区分を格納する用データテーブル
        /// </summary>
        private DataTable _dtShukeiKubun = new DataTable();
        public DataTable ShukeiKubun
        {
            get
            {
                return this._dtShukeiKubun;
            }
        }

        /// <summary>
        /// 選択項目を格納する用データテーブル
        /// </summary>
        private DataTable _dtSentakuKomoku = new DataTable();
        public DataTable SentakuKomoku
        {
            get
            {
                return this._dtSentakuKomoku;
            }
        }

        /// <summary>
        /// 選択候補を格納する用データテーブル
        /// </summary>
        private DataTable _dtSentakuKoho = new DataTable();
        public DataTable SentakuKoho
        {
            get
            {
                return this._dtSentakuKoho;
            }
        }

        /// <summary>
        /// タイトルNoを格納する用変数
        /// </summary>
        private Decimal _dtPositionNo = new Decimal();
        public Decimal PositionNo
        {
            get
            {
                return this._dtPositionNo;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1112(String SHUKEIHYO)//
        {
            TB_NAME = SHUKEIHYO == "1" ? TB_NAME_KZ : TB_NAME_NK;

            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルの表示非表示を設定
            this.lblTitle.Visible = false;

            // Escapeのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            if (TB_NAME == TB_NAME_KZ)
            {
                this.Text = "期首残科目の設定";
                this.Label1.Text = "期首残科目";
            }
            else
            {
                this.Text = "入金科目の設定";
                this.Label2.Text = "入金科目";
            }

            ShowItem();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // 選択項目を登録する
            this.InsertTB_HN_SHUKEIHYO_SETTEI_MEISAI();

            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 選択した項目の移動処理(左→右)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRMove_Click(object sender, System.EventArgs e)
        {
            int[] selectNum = new int[this.lbxShukeiKubun.SelectedIndices.Count];
            
            for (int i = 0; i < this.lbxShukeiKubun.SelectedIndices.Count; i++)
            {
                selectNum[i] = this.lbxShukeiKubun.SelectedIndices[i];
            }

            #region 選択タイトル値の追加
            // 選択した項目をセット
            string ShukeiKubunNm;
            string filter;
            DataRow[] dataRows;
            DataTable kubunList = new DataTable();
            kubunList.Columns.Add("num", Type.GetType("System.Int32"));
            kubunList.Columns.Add("cd", Type.GetType("System.Int32"));
            kubunList.Columns.Add("nm", Type.GetType("System.String"));
            DataRow row;
            for (int i = 0; i < selectNum.Length; i++)
            {
                // 集計区分名称を取得
                ShukeiKubunNm = this.lbxShukeiKubun.Items[selectNum[i]].ToString();
                // 集計区分情報を取得
                filter = "nm = \'" + ShukeiKubunNm + "\'";
                dataRows = this.ShukeiKubun.Select(filter);
                row = kubunList.NewRow();
                row["num"] = i;
                row["cd"] = Util.ToInt(dataRows[0]["cd"]);
                row["nm"] = Util.ToString(dataRows[0]["nm"]);
                kubunList.Rows.Add(row);
            }
            // 選択項目の情報を取得
            DataRow[] dataRowsSentaku = this.SentakuKomoku.Select();
        
            // 選択した項目の追加
            dataRows = kubunList.Select();
            foreach (DataRow dr in dataRows)
            {
                this.lbxSentakuKomoku.Items.Add(this.lbxShukeiKubun.Items[selectNum[Util.ToInt(dr["num"])]]);

                // 選択項目に情報をセット
                row = SentakuKomoku.NewRow();
                row["cd"] = Util.ToInt(dr["cd"]);
                row["nm"] = Util.ToString(dr["nm"]);
                SentakuKomoku.Rows.Add(row);

                // 集計区分の情報を取得
                filter = "nm = \'" + Util.ToString(dr["nm"]) + "\' and cd = \'" + Util.ToString(dr["cd"]) + "\'";
                DataRow[] dataRowsShukei = this.ShukeiKubun.Select(filter);

                // 集計区分から削除する
                foreach (DataRow drSK in dataRowsShukei)
                {
                    drSK.Delete();
                }
            }
            #endregion

            int l = 1;
            // 選択した項目の削除
            for (int i = 0; i < selectNum.Length; i++)
            {
                if (i == 0)
                {
                    this.lbxShukeiKubun.Items.Remove(this.lbxShukeiKubun.Items[selectNum[i]]);
                }
                else
                {
                    this.lbxShukeiKubun.Items.Remove(this.lbxShukeiKubun.Items[selectNum[i] - l]);
                    l++;
                }
            }
        }

        /// <summary>
        /// 選択した項目の移動処理(右→左)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLMove_Click(object sender, System.EventArgs e)
        {
            {
                int[] selectNum = new int[this.lbxSentakuKomoku.SelectedIndices.Count];
                for (int i = 0; i < this.lbxSentakuKomoku.SelectedIndices.Count; i++)
                {
                    selectNum[i] = this.lbxSentakuKomoku.SelectedIndices[i];
                }

                // 選択した項目の追加
                string shukeiKubunNm;
                string filter;
                DataRow[] dataRows;
                DataRow row;
                for (int i = 0; i < selectNum.Length; i++)
                {
                    this.lbxShukeiKubun.Items.Add(this.lbxSentakuKomoku.Items[selectNum[i]]);

                    if (this.PositionNo != 1)
                    {
                        // 集計区分名称を取得
                        shukeiKubunNm = this.lbxSentakuKomoku.Items[selectNum[i]].ToString();
                        // 選択項目の情報を取得
                        filter = "nm = \'" + shukeiKubunNm + "\'";
                        dataRows = this.SentakuKomoku.Select(filter);

                        // 集計区分に情報をセット
                        row = ShukeiKubun.NewRow();
                        row["cd"] = Util.ToInt(dataRows[0]["cd"]);
                        row["nm"] = shukeiKubunNm;
                        this.ShukeiKubun.Rows.Add(row);

                        // 選択項目から削除する
                        foreach (DataRow dr in dataRows)
                        {
                            dr.Delete();
                        }
                    }
                }

                int l = 1;
                // 選択した項目の削除
                for (int i = 0; i < selectNum.Length; i++)
                {
                    if (i == 0)
                    {
                        this.lbxSentakuKomoku.Items.Remove(this.lbxSentakuKomoku.Items[selectNum[i]]);
                    }
                    else
                    {
                        this.lbxSentakuKomoku.Items.Remove(this.lbxSentakuKomoku.Items[selectNum[i] - l]);
                        l++;
                    }
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 画面から取得したデータを格納(集計区分)
        /// </summary>
        private void ShowItem()
        {
            //科目一覧を表示する。
            DispDataSetShukeiKubun();
            //期首残科目一覧を表示する。
            DispDataSetSentakuKomoku();
        }

        /// <summary>
        /// 画面から取得したデータを格納(選択項目)
        /// </summary>
        private void SetSentakuKomoku()
        {
            // 選択項目をDBに反映する
            this.InsertTB_HN_SHUKEIHYO_SETTEI_MEISAI();

            // 選択項目を表示する
            this.lbxSentakuKomoku.Items.Clear();
            //foreach (DataRow dr in dataRows)
            //{
            //    // 選択項目にセットする
            //    if (Util.ToInt(dr["cd"]) != 0)
            //    {
            //        this.lbxSentakuKomoku.Items.Add(Util.ToString(dr["nm"]));
            //    }
            //}
        }

        /// <summary>
        /// DBから取得したデータを格納(期首残科目)
        /// 画面に反映(期首残科目)
        /// </summary>
        private void DispDataSetShukeiKubun()
        {
            // 対象データテーブルにカラムを２列定義
            this.ShukeiKubun.Columns.Add("cd", Type.GetType("System.Int32"));
            this.ShukeiKubun.Columns.Add("nm", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;

            dtResult = this.GetTB_HN_SHUKEIHYO_KOMOKU();
            foreach (DataRow dr in dtResult.Rows)
            {
                row = this.ShukeiKubun.NewRow();
                row["cd"] = Util.ToInt(dr["KOMOKU_CD"]);
                row["nm"] = Util.ToString(dr["KOMOKU_NM"]);
                this.ShukeiKubun.Rows.Add(row);

                // 集計区分にセットする
                this.lbxShukeiKubun.Items.Add(Util.ToString(dr["KOMOKU_NM"]));
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(選択項目)
        /// 画面に反映(選択項目)
        /// </summary>
        private void DispDataSetSentakuKomoku()
        {
            // 対象データテーブルにカラムを4列ずつ定義
            this.SentakuKomoku.Columns.Add("cd", Type.GetType("System.Int32"));
            this.SentakuKomoku.Columns.Add("nm", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;

            dtResult = this.GetVI_HN_SHUKEIHYO_SETTEI_MEISAI();
            foreach (DataRow dr in dtResult.Rows)
            {
                row = this.SentakuKomoku.NewRow();
                row["cd"] = Util.ToInt(dr["KOMOKU_CD"]);
                row["nm"] = Util.ToString(dr["KOMOKU_NM"]);
                this.SentakuKomoku.Rows.Add(row);

                // 選択項目にセットする
                if (Util.ToInt(dr["KOMOKU_CD"]) != 0)
                {
                    this.lbxSentakuKomoku.Items.Add(Util.ToString(dr["KOMOKU_NM"]));
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(選択項目候補)
        /// </summary>
        private void DispDataSetSentakuKoho()
        {
            // 対象データテーブルにカラムを3列ずつ定義
            this.SentakuKoho.Columns.Add("cd", Type.GetType("System.Int32"));
            this.SentakuKoho.Columns.Add("nm", Type.GetType("System.String"));

            DataTable dtResult;
            DataRow row;

                dtResult = this.GetTB_HN_SHUKEIHYO_KOMOKU();
                foreach (DataRow dr in dtResult.Rows)
                {
                    row = this.SentakuKoho.NewRow();
                    row["cd"] = Util.ToInt(dr["KOMOKU_CD"]);
                    row["nm"] = Util.ToString(dr["KOMOKU_NM"]);
                    this.SentakuKoho.Rows.Add(row);
                }
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetVI_HN_SHUKEIHYO_SETTEI_MEISAI()
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, this.Par3);

            Sql.Append(" SELECT");
            Sql.Append(" A.KANJO_KAMOKU_CD AS KOMOKU_CD,");
            Sql.Append(" B.KANJO_KAMOKU_NM AS KOMOKU_NM ");
            Sql.Append(" FROM " + TB_NAME + " AS A");
            Sql.Append(" LEFT OUTER JOIN " + TB_NAME_ZM + " AS B");
            Sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            Sql.Append(" AND B.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append("WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" A.MOTOCHO_KUBUN = @MOTOCHO_KUBUN AND");
            Sql.Append(" A.SHISHO_CD = @SHISHO_CD ");
            Sql.Append("ORDER BY");
            Sql.Append(" A.KANJO_KAMOKU_CD");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetTB_HN_SHUKEIHYO_KOMOKU()
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, this.Par3);

            Sql.Append(" SELECT");
            Sql.Append(" A.KANJO_KAMOKU_CD AS KOMOKU_CD,");
            Sql.Append(" A.KANJO_KAMOKU_NM AS KOMOKU_NM ");
            Sql.Append(" FROM " + TB_NAME_ZM + " AS A");
            Sql.Append(" WHERE ");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append(" A.KANJO_KAMOKU_CD NOT IN (");
            Sql.Append("    SELECT");
            Sql.Append("    B.KANJO_KAMOKU_CD ");
            Sql.Append("    FROM " + TB_NAME + " AS B ");
            Sql.Append("    WHERE ");
            Sql.Append("    B.KAISHA_CD = A.KAISHA_CD AND ");
            Sql.Append("    B.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append("    B.MOTOCHO_KUBUN = @MOTOCHO_KUBUN ) AND ");
            Sql.Append(" A.SHIYO_MISHIYO = 1 ");
            Sql.Append(" ORDER BY");
            Sql.Append(" A.KAISHA_CD ASC,");
            Sql.Append(" A.KANJO_KAMOKU_CD ASC ");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }


        /// <summary>
        /// データを登録(選択項目)
        /// </summary>
        private void InsertTB_HN_SHUKEIHYO_SETTEI_MEISAI()
        {
            // 削除
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // TB_期首残科目削除
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
                whereParam.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, this.Par3);
                this.Dba.Delete(TB_NAME,
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND MOTOCHO_KUBUN = @MOTOCHO_KUBUN",
                    whereParam);

                // TB_期首残科目登録
                DbParamCollection updParam;
                String SentakuKomokuNm;
                int titleCd = 0;

                for (int cnt = 0; cnt < this.lbxSentakuKomoku.Items.Count; cnt++)
                {
                    SentakuKomokuNm = this.lbxSentakuKomoku.Items[cnt].ToString();

                    String filter = "nm = \'" + SentakuKomokuNm + "\'";
                    DataRow[] dataRowsTitle = SentakuKomoku.Select(filter);
                    if (dataRowsTitle.Length > 0)
                    {
                        titleCd = Util.ToInt(dataRowsTitle[0]["cd"]);
                    }

                    updParam = new DbParamCollection();

                    updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
                    updParam.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, this.Par3);
                    updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 10, titleCd);

                    this.Dba.Insert(TB_NAME, updParam);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }
        #endregion
    }
}
