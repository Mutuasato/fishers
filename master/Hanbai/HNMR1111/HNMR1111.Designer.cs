﻿namespace jp.co.fsi.hn.hnmr1111
{
    partial class HNMR1111
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblDateDayFr = new System.Windows.Forms.Label();
			this.lblDateDayTo = new System.Windows.Forms.Label();
			this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblCodeBetDate = new System.Windows.Forms.Label();
			this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateMonthTo = new System.Windows.Forms.Label();
			this.lblDateYearTo = new System.Windows.Forms.Label();
			this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoTo = new System.Windows.Forms.Label();
			this.lblDateMonthFr = new System.Windows.Forms.Label();
			this.lblDateYearFr = new System.Windows.Forms.Label();
			this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengoFr = new System.Windows.Forms.Label();
			this.lblNakagaininCdBet = new System.Windows.Forms.Label();
			this.lblNakagaininCdTo = new System.Windows.Forms.Label();
			this.lblNakagaininCdFr = new System.Windows.Forms.Label();
			this.txtNakagaininCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtNakagaininCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(5, 163);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(944, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(934, 32);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblDateDayFr
			// 
			this.lblDateDayFr.AutoSize = true;
			this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateDayFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateDayFr.Location = new System.Drawing.Point(395, 7);
			this.lblDateDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateDayFr.Name = "lblDateDayFr";
			this.lblDateDayFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateDayFr.TabIndex = 7;
			this.lblDateDayFr.Tag = "CHANGE";
			this.lblDateDayFr.Text = "日";
			this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateDayTo
			// 
			this.lblDateDayTo.AutoSize = true;
			this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateDayTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateDayTo.Location = new System.Drawing.Point(735, 7);
			this.lblDateDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateDayTo.Name = "lblDateDayTo";
			this.lblDateDayTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateDayTo.TabIndex = 16;
			this.lblDateDayTo.Tag = "CHANGE";
			this.lblDateDayTo.Text = "日";
			this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateDayFr
			// 
			this.txtDateDayFr.AutoSizeFromLength = false;
			this.txtDateDayFr.DisplayLength = null;
			this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateDayFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateDayFr.Location = new System.Drawing.Point(352, 3);
			this.txtDateDayFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateDayFr.MaxLength = 2;
			this.txtDateDayFr.Name = "txtDateDayFr";
			this.txtDateDayFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateDayFr.TabIndex = 6;
			this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
			// 
			// lblCodeBetDate
			// 
			this.lblCodeBetDate.AutoSize = true;
			this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
			this.lblCodeBetDate.Location = new System.Drawing.Point(427, 7);
			this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBetDate.Name = "lblCodeBetDate";
			this.lblCodeBetDate.Size = new System.Drawing.Size(24, 16);
			this.lblCodeBetDate.TabIndex = 8;
			this.lblCodeBetDate.Tag = "CHANGE";
			this.lblCodeBetDate.Text = "～";
			this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtDateDayTo
			// 
			this.txtDateDayTo.AutoSizeFromLength = false;
			this.txtDateDayTo.DisplayLength = null;
			this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateDayTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateDayTo.Location = new System.Drawing.Point(692, 3);
			this.txtDateDayTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateDayTo.MaxLength = 2;
			this.txtDateDayTo.Name = "txtDateDayTo";
			this.txtDateDayTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateDayTo.TabIndex = 15;
			this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
			// 
			// lblDateMonthTo
			// 
			this.lblDateMonthTo.AutoSize = true;
			this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateMonthTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateMonthTo.Location = new System.Drawing.Point(664, 7);
			this.lblDateMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateMonthTo.Name = "lblDateMonthTo";
			this.lblDateMonthTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthTo.TabIndex = 14;
			this.lblDateMonthTo.Tag = "CHANGE";
			this.lblDateMonthTo.Text = "月";
			this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateYearTo
			// 
			this.lblDateYearTo.AutoSize = true;
			this.lblDateYearTo.BackColor = System.Drawing.Color.Silver;
			this.lblDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateYearTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateYearTo.Location = new System.Drawing.Point(591, 7);
			this.lblDateYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateYearTo.Name = "lblDateYearTo";
			this.lblDateYearTo.Size = new System.Drawing.Size(24, 16);
			this.lblDateYearTo.TabIndex = 12;
			this.lblDateYearTo.Tag = "CHANGE";
			this.lblDateYearTo.Text = "年";
			this.lblDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateYearTo
			// 
			this.txtDateYearTo.AutoSizeFromLength = false;
			this.txtDateYearTo.DisplayLength = null;
			this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateYearTo.Location = new System.Drawing.Point(548, 3);
			this.txtDateYearTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateYearTo.MaxLength = 2;
			this.txtDateYearTo.Name = "txtDateYearTo";
			this.txtDateYearTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearTo.TabIndex = 11;
			this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
			// 
			// txtDateMonthTo
			// 
			this.txtDateMonthTo.AutoSizeFromLength = false;
			this.txtDateMonthTo.DisplayLength = null;
			this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthTo.ForeColor = System.Drawing.Color.Black;
			this.txtDateMonthTo.Location = new System.Drawing.Point(621, 3);
			this.txtDateMonthTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateMonthTo.MaxLength = 2;
			this.txtDateMonthTo.Name = "txtDateMonthTo";
			this.txtDateMonthTo.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthTo.TabIndex = 13;
			this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
			// 
			// lblDateGengoTo
			// 
			this.lblDateGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoTo.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengoTo.Location = new System.Drawing.Point(488, 2);
			this.lblDateGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateGengoTo.Name = "lblDateGengoTo";
			this.lblDateGengoTo.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengoTo.TabIndex = 10;
			this.lblDateGengoTo.Tag = "DISPNAME";
			this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateMonthFr
			// 
			this.lblDateMonthFr.AutoSize = true;
			this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateMonthFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateMonthFr.Location = new System.Drawing.Point(321, 7);
			this.lblDateMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateMonthFr.Name = "lblDateMonthFr";
			this.lblDateMonthFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateMonthFr.TabIndex = 5;
			this.lblDateMonthFr.Tag = "CHANGE";
			this.lblDateMonthFr.Text = "月";
			this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateYearFr
			// 
			this.lblDateYearFr.AutoSize = true;
			this.lblDateYearFr.BackColor = System.Drawing.Color.Silver;
			this.lblDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateYearFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateYearFr.Location = new System.Drawing.Point(248, 7);
			this.lblDateYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateYearFr.Name = "lblDateYearFr";
			this.lblDateYearFr.Size = new System.Drawing.Size(24, 16);
			this.lblDateYearFr.TabIndex = 3;
			this.lblDateYearFr.Tag = "CHANGE";
			this.lblDateYearFr.Text = "年";
			this.lblDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateYearFr
			// 
			this.txtDateYearFr.AutoSizeFromLength = false;
			this.txtDateYearFr.DisplayLength = null;
			this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateYearFr.Location = new System.Drawing.Point(205, 3);
			this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateYearFr.MaxLength = 2;
			this.txtDateYearFr.Name = "txtDateYearFr";
			this.txtDateYearFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateYearFr.TabIndex = 2;
			this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
			// 
			// txtDateMonthFr
			// 
			this.txtDateMonthFr.AutoSizeFromLength = false;
			this.txtDateMonthFr.DisplayLength = null;
			this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
			this.txtDateMonthFr.Location = new System.Drawing.Point(279, 3);
			this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDateMonthFr.MaxLength = 2;
			this.txtDateMonthFr.Name = "txtDateMonthFr";
			this.txtDateMonthFr.Size = new System.Drawing.Size(39, 23);
			this.txtDateMonthFr.TabIndex = 4;
			this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
			// 
			// lblDateGengoFr
			// 
			this.lblDateGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengoFr.Location = new System.Drawing.Point(145, 2);
			this.lblDateGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateGengoFr.Name = "lblDateGengoFr";
			this.lblDateGengoFr.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengoFr.TabIndex = 1;
			this.lblDateGengoFr.Tag = "DISPNAME";
			this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblNakagaininCdBet
			// 
			this.lblNakagaininCdBet.AutoSize = true;
			this.lblNakagaininCdBet.BackColor = System.Drawing.Color.Silver;
			this.lblNakagaininCdBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininCdBet.ForeColor = System.Drawing.Color.Black;
			this.lblNakagaininCdBet.Location = new System.Drawing.Point(463, 8);
			this.lblNakagaininCdBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininCdBet.Name = "lblNakagaininCdBet";
			this.lblNakagaininCdBet.Size = new System.Drawing.Size(24, 16);
			this.lblNakagaininCdBet.TabIndex = 2;
			this.lblNakagaininCdBet.Tag = "CHANGE";
			this.lblNakagaininCdBet.Text = "～";
			this.lblNakagaininCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblNakagaininCdTo
			// 
			this.lblNakagaininCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininCdTo.Location = new System.Drawing.Point(595, 3);
			this.lblNakagaininCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
			this.lblNakagaininCdTo.Size = new System.Drawing.Size(240, 24);
			this.lblNakagaininCdTo.TabIndex = 4;
			this.lblNakagaininCdTo.Tag = "DISPNAME";
			this.lblNakagaininCdTo.Text = "最　後";
			this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblNakagaininCdFr
			// 
			this.lblNakagaininCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNakagaininCdFr.Location = new System.Drawing.Point(215, 3);
			this.lblNakagaininCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
			this.lblNakagaininCdFr.Size = new System.Drawing.Size(240, 24);
			this.lblNakagaininCdFr.TabIndex = 1;
			this.lblNakagaininCdFr.Tag = "DISPNAME";
			this.lblNakagaininCdFr.Text = "先　頭";
			this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNakagaininCdTo
			// 
			this.txtNakagaininCdTo.AutoSizeFromLength = false;
			this.txtNakagaininCdTo.DisplayLength = null;
			this.txtNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNakagaininCdTo.ForeColor = System.Drawing.Color.Black;
			this.txtNakagaininCdTo.Location = new System.Drawing.Point(525, 4);
			this.txtNakagaininCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtNakagaininCdTo.MaxLength = 4;
			this.txtNakagaininCdTo.Name = "txtNakagaininCdTo";
			this.txtNakagaininCdTo.Size = new System.Drawing.Size(64, 23);
			this.txtNakagaininCdTo.TabIndex = 3;
			this.txtNakagaininCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNakagaininCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNakagaininCdTo_KeyDown);
			this.txtNakagaininCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCdTo_Validating);
			// 
			// txtNakagaininCdFr
			// 
			this.txtNakagaininCdFr.AutoSizeFromLength = false;
			this.txtNakagaininCdFr.DisplayLength = null;
			this.txtNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNakagaininCdFr.ForeColor = System.Drawing.Color.Black;
			this.txtNakagaininCdFr.Location = new System.Drawing.Point(145, 4);
			this.txtNakagaininCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtNakagaininCdFr.MaxLength = 4;
			this.txtNakagaininCdFr.Name = "txtNakagaininCdFr";
			this.txtNakagaininCdFr.Size = new System.Drawing.Size(64, 23);
			this.txtNakagaininCdFr.TabIndex = 0;
			this.txtNakagaininCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNakagaininCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCdFr_Validating);
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(145, 3);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(192, 2);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 36);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.4F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(921, 120);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblNakagaininCdBet);
			this.fsiPanel3.Controls.Add(this.txtNakagaininCdFr);
			this.fsiPanel3.Controls.Add(this.lblNakagaininCdTo);
			this.fsiPanel3.Controls.Add(this.txtNakagaininCdTo);
			this.fsiPanel3.Controls.Add(this.lblNakagaininCdFr);
			this.fsiPanel3.Controls.Add(this.label3);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 83);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(911, 32);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.MinimumSize = new System.Drawing.Size(0, 32);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(911, 32);
			this.label3.TabIndex = 2;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "仲買人CD範囲";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblDateGengoFr);
			this.fsiPanel2.Controls.Add(this.lblDateDayFr);
			this.fsiPanel2.Controls.Add(this.lblDateYearFr);
			this.fsiPanel2.Controls.Add(this.lblDateMonthFr);
			this.fsiPanel2.Controls.Add(this.txtDateYearFr);
			this.fsiPanel2.Controls.Add(this.lblDateGengoTo);
			this.fsiPanel2.Controls.Add(this.lblDateDayTo);
			this.fsiPanel2.Controls.Add(this.txtDateMonthFr);
			this.fsiPanel2.Controls.Add(this.txtDateMonthTo);
			this.fsiPanel2.Controls.Add(this.txtDateYearTo);
			this.fsiPanel2.Controls.Add(this.lblDateYearTo);
			this.fsiPanel2.Controls.Add(this.txtDateDayFr);
			this.fsiPanel2.Controls.Add(this.lblDateMonthTo);
			this.fsiPanel2.Controls.Add(this.lblCodeBetDate);
			this.fsiPanel2.Controls.Add(this.txtDateDayTo);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 44);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(911, 30);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.MinimumSize = new System.Drawing.Size(0, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(911, 32);
			this.label2.TabIndex = 2;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "日付範囲";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(911, 30);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.MinimumSize = new System.Drawing.Size(0, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(911, 32);
			this.label1.TabIndex = 2;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "水揚支所";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// HNMR1111
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(934, 305);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNMR1111";
			this.Par1 = "11";
			this.Text = "";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label lblDateYearTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateDayTo;
        private common.controls.FsiTextBox txtDateDayFr;
        private common.controls.FsiTextBox txtDateDayTo;
        private System.Windows.Forms.Label lblNakagaininCdBet;
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.FsiTextBox txtNakagaininCdTo;
        private common.controls.FsiTextBox txtNakagaininCdFr;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label3;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label1;
    }
}