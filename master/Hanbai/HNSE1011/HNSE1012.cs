﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hnse1011
{
    /// <summary>
    /// 伝票検索(HNSE1012)
    /// </summary>
    public partial class HNSE1012 : BasePgForm
    {
        // DB列の番号
        const int COL_DENPYO_BANGO = 0;
        const int COL_SERIBI = 1;
        const int COL_SENSHU_CD = 2;
        const int COL_SENSHU_NM = 3;
        const int COL_GYOHO_CD = 4;
        const int COL_GYOHO_NM = 5;
        const int COL_GYOGYO_TESURYO = 6;
        const int COL_MIZUAGE_ZEIKOMI_KINGAKU = 7;
        const int COL_MIZUAGE_SHOHIZEIGAKU = 8;
        const int COL_SEISAN_KUBUN = 9;

        // データグリッドビュー列の番号
        const int DGV_SERIBI = 0;
        const int DGV_DENPYO_BANGO = 1;
        const int DGV_SENSHU_CD = 2;
        const int DGV_SENSHU_NM = 3;
        const int DGV_GYOHO_NM = 4;
        const int DGV_MIZUAGE_ZEIKOMI_KINGAKU = 5;
        const int DGV_MIZUAGE_SHOHIZEIGAKU = 6;
        const int DGV_SEISAN_KUBUN = 7;

        private decimal seisanKbn; //精算区分

        // 支所
        private int ShishoCode;

        private bool lastCodeCheck;
        private bool seisanKbnCheck;

        private string activeContorolName = "";

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSE1012()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {

            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);

            this.dtDenDateFrom.ExecBtn = this.btnF1;
            this.dtDenDateFrom.OpenDialogExe = "CMCM1021";
            this.dtDenDateFrom.DbConnect = this.Dba;
            this.dtDenDateFrom.JpDate = jpDate;


            this.dtDenDateTo.ExecBtn = this.btnF1;
            this.dtDenDateTo.OpenDialogExe = "CMCM1021";
            this.dtDenDateTo.DbConnect = this.Dba;
            this.dtDenDateTo.JpDate = jpDate;

            // TODO 2020-01-29 COMMENT
            // 伝票日付（開始）
            //this.lblGengoFr.Text = jpDate[0];
            //this.txtYearFr.Text = jpDate[2];
            //this.txtMonthFr.Text = jpDate[3];
            //this.txtDayFr.Text = jpDate[4];

            // TODO 2020-01-29 COMMENT
            // 伝票日付（終了）
            //this.lblGengoTo.Text = jpDate[0];
            //this.txtYearTo.Text = jpDate[2];
            //this.txtMonthTo.Text = jpDate[3];
            //this.txtDayTo.Text = jpDate[4];

            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 精算区分名
            this.seisanKbn = Util.ToInt(this.Par1);
            this.txtSeisanKbn.Text = this.seisanKbn.ToString();
            this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", Util.ToString(this.ShishoCode), this.txtSeisanKbn.Text);

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // 列幅を設定する
            this.setDenpyoListHeader();

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
            {
                c.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            //// 伝票日付（開始）年にフォーカスを移す
            // 条件の設定
            if (this.InData != null && !ValChk.IsEmpty(this.InData))
            {
                string inData = (string)this.InData;
                if (inData.Length == 0)
                {
                    // 伝票日付（開始）年にフォーカスを移す
                    this.dtDenDateFrom.Focus();
                }
                else
                {
                    this.setDenpyoCondition(inData);
                    this.btnF6.PerformClick();
                    if (this.dgvList.Rows.Count > 0)
                    {
                        ActiveControl = this.dgvList;
                        this.dgvList.Rows[0].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, 0];
                    }
                }
            }
            else
            {
                this.dtDenDateFrom.Focus();
            }

        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年と取引先コードに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
				case "dtDenDateFrom":

					this.btnF1.Enabled = true;     // 検索
					this.btnF4.Enabled = false;    // 条件
					this.btnF6.Enabled = false;     // 開始
					this.btnEnter.Enabled = false; // 決定
					break;
				case "dtDenDateTo":

					this.btnF1.Enabled = true;     // 検索
					this.btnF4.Enabled = false;    // 条件
					this.btnF6.Enabled = false;     // 開始
					this.btnEnter.Enabled = false; // 決定
					break;

				case "txtFunanushiCdFr":  // 船主CD（開始）
                case "txtFunanushiCdTo":  // 船主CD（開始）
                case "txtNakagaininCdFr": // 仲買人CD（開始）
                case "txtNakagaininCdTo": // 仲買人CD（終了）
                case "txtGyogyoshuCd":    // 漁業種CD
                case "txtGyoshuCd":       // 漁種CD
                case "txtSeisanKbn":      // 精算区分
                    this.btnF1.Enabled = true;     // 検索
                    this.btnF4.Enabled = false;    // 条件
                    this.btnF6.Enabled = true;     // 開始
                    this.btnEnter.Enabled = false; // 決定
                    break;
                case "rdoBashoNm01":      // 精算区分
                case "rdoBashoNm02":      // 精算区分
                case "rdoBashoNm03":      // 精算区分
                    this.btnF1.Enabled = false;    // 検索
                    this.btnF4.Enabled = false;    // 条件
                    this.btnF6.Enabled = true;     // 開始
                    this.btnEnter.Enabled = false; // 決定
                    break;
                default:
                    this.btnF1.Enabled = false;    // 検索
                    this.btnF4.Enabled = true;     // 条件
                    this.btnF6.Enabled = false;    // 開始
                    this.btnEnter.Enabled = true;  // 決定
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;

            this.CausesValidation = false;

            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。
            System.Reflection.Assembly asm; // 仲買人CDの検索小窓を呼び出すときに使う
            Type t;                         // 仲買人CDの検索小窓を呼び出すときに使う
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

			//string ActiveContorolName = this.ActiveCtlNm;
			string ActiveContorolName = this.ActiveControl.Name;

//			if (activeContorolName != "" && activeContorolName != ActiveContorolName && ActiveContorolName == "txtSeisanKbn")
                //ActiveContorolName = activeContorolName;

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            // switch (this.ActiveCtlNm)
            switch (ActiveContorolName)
            {
                #region 船主CD
                case "txtFunanushiCdFr": // 船主CD（開始）
                case "txtFunanushiCdTo": // 船主CD（終了）
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9021.exe");
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                //if ("txtFunanushiCdFr" == this.ActiveCtlNm)
                                if ("txtFunanushiCdFr" == ActiveContorolName)
                                {
                                    this.txtFunanushiCdFr.Text = outData[0];
                                    this.lblFunanishiNmFr.Text = outData[1];
                                }
                                else
                                {
                                    this.txtFunanushiCdTo.Text = outData[0];
                                    this.lblFunanushiNmTo.Text = outData[1];
                                }

                            }
                        }
                    }
                    break;
                #endregion

                #region 精算区分
                case "txtSeisanKbn":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.InData = this.txtSeisanKbn.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (String[])frm.OutData;
                                this.txtSeisanKbn.Text = outData[0];
                                this.lblSeisanKbnNm.Text = outData[1];
                                this.seisanKbn = Util.ToDecimal(this.txtSeisanKbn.Text);
                            }
                        }
                    }
                    break;
                    #endregion
            }
            try
            {
                Controls[ActiveContorolName].Focus();
            }
            catch (Exception) { }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 条件（検索結果をクリアして伝票日付Frの年にフォーカスする）
            this.dtDenDateFrom.Focus();
            this.dtDenDateTo.Focus();
//            this.txtYearFr.Focus();
//            this.txtYearFr.SelectAll();
            this.dgvList.Rows.Clear();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 検索開始
            this.getListData();
        }
        #endregion

        #region イベント

        /// <summary>
        /// 船主コード（開始）の値チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanishiNmFr.Text = "先　頭";
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.ShishoCode.ToString(), this.txtFunanushiCdFr.Text);


                this.lblFunanishiNmFr.Text = name;
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// 船主コード（終了）の値チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            this.lastCodeCheck = false;
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiNmTo.Text = "最　後";
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.ShishoCode.ToString(), this.txtFunanushiCdTo.Text);

                this.lblFunanushiNmTo.Text = name;
            }
            this.lastCodeCheck = true;
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// 船主コード（終了）のEnter押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.lastCodeCheck && e.KeyCode == Keys.Enter)
            {
                // 検索開始
                this.getListData();
            }
        }

        /// <summary>
        /// データグリッドにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Enter(object sender, EventArgs e)
        {
            this.btnF1.Enabled = false;
            this.btnF4.Enabled = true;
            this.btnF6.Enabled = false;
            this.btnEnter.Enabled = true;
        }

        /// <summary>
        /// データグリッド用のテキストにキーダウン時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            // エンターキー押下時は選択した行の伝票番号を親画面に返す
            if (e.KeyCode == Keys.Enter)
            {
                // 行が取得出来た場合選択されている伝票番号を返す
                if (this.dgvList.SelectedRows.Count >= 1)
                {
                    this.ReturnVal();
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理（ヘッダー以外）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // ヘッダー以外が選択されている場合に伝票番号を返す
            if (0 <= e.RowIndex)
            {
                this.ReturnVal();
            }
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            // 行が取得できない場合は何もしないで終了
            if (this.dgvList.SelectedRows.Count < 1)
            {
                this.btnF1.Enabled = false;   // 検索
                this.btnF4.Enabled = true;    // 条件
                this.btnF6.Enabled = false;   // 開始
                this.btnEnter.Enabled = true; // 決定
                return;
            }

            this.ReturnVal();
        }

        /// <summary>
        /// データグリッドがスクロールした時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            Rectangle rctCell = dgv.GetCellDisplayRectangle(this.dgvList.CurrentCell.ColumnIndex, this.dgvList.CurrentCell.RowIndex, false);

        }

        /// <summary>
        /// 県内,県外,地元のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoBashoNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // 検索開始
                this.getListData();
            }
        }

        /// <summary>
        /// 精算区分入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSetSeisanKubun())
            {
                e.Cancel = true;
                this.txtSeisanKbn.SelectAll();

                seisanKbnCheck = false;
            }
            else
            {
                this.seisanKbn = Util.ToDecimal(this.txtSeisanKbn.Text);

                seisanKbnCheck = true;
            }
            activeContorolName = this.ActiveCtlNm;
        }

        /// <summary>
        /// 精算区分からの移動制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_KeyDown(object sender, KeyEventArgs e)
        {
            // Enterキーの場合、年号開始にフォーカス
            if (seisanKbnCheck && e.KeyCode == Keys.Enter)
            {
                this.dtDenDateFrom.Focus();
//                txtYearFr.Focus();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSetSeisanKubun()
        {
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text))
            {
                this.lblSeisanKbnNm.Text = "";
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtSeisanKbn.Text, this.txtSeisanKbn.MaxLength))
            {
                this.lblSeisanKbnNm.Text = "";
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値のみ入力を許可
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                this.lblSeisanKbnNm.Text = "";
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // マスタチェック
            if (!ValChk.IsEmpty(this.txtSeisanKbn.Text))
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", Util.ToString(this.ShishoCode), this.txtSeisanKbn.Text);
                if (ValChk.IsEmpty(this.lblSeisanKbnNm.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// <summary>
        /// 伝票番号一覧のデータを取得
        /// </summary>
        /// <returns>自動仕訳履歴から取得したデータ</returns>
        private DataTable getDenpyoList()
        {
            // 伝票日付（開始、終了）
//            DateTime fromDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text, this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
//            DateTime foDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text, this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);

            DateTime fromDate = Util.ConvAdDate(this.dtDenDateFrom.Gengo, this.dtDenDateFrom.WYear, dtDenDateFrom.Month, dtDenDateFrom.Day, this.Dba);
            DateTime foDate = Util.ConvAdDate(this.dtDenDateTo.Gengo, this.dtDenDateTo.WYear, dtDenDateTo.Month, dtDenDateTo.Day, this.Dba);

            // 船主コード
            int funanushiCdFr = 0;
            int funanushiCdTo = 9999999;
            if (!ValChk.IsEmpty(this.txtFunanushiCdFr.Text) && ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                funanushiCdFr = Util.ToInt(this.txtFunanushiCdFr.Text);
            }
            if (!ValChk.IsEmpty(this.txtFunanushiCdTo.Text) && ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                funanushiCdTo = Util.ToInt(this.txtFunanushiCdTo.Text);
            }


            string sql = @"
                SELECT
                     A.DENPYO_BANGO AS DENPYO_BANGO
                    ,A.SERIBI AS DENPYO_DATE
                    ,A.SENSHU_CD AS SENSHU_CD
                    ,A.SENSHU_NM AS SENSHU_NM
                    ,A.GYOHO_CD AS GYOHO_CD
                    ,A.GYOHO_NM AS GYOHO_NM
                    ,A.GYOGYO_TESURYO AS GYOGYO_TESURYO
                    ,A.MIZUAGE_ZEIKOMI_KINGAKU AS KINGAKU
                    ,A.MIZUAGE_SHOHIZEIGAKU AS SHOHIZEI
                    ,A.SEISAN_KUBUN AS SEISAN_KUBUN
               FROM
                    TB_HN_SHIKIRI_DATA AS A
               WHERE
                    A.KAISHA_CD = @KAISHA_CD
               AND  A.SHISHO_CD = @SHISHO_CD
               AND (A.SERIBI BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO)
               AND A.KAIKEI_NENDO = @KAIKEI_NENDO
               AND A.SEISAN_KUBUN = @SEISAN_KUBUN
               AND (A.SENSHU_CD BETWEEN @SENSHU_CD_FR AND @SENSHU_CD_TO)
               ORDER BY
                    A.SERIBI
                   ,A.DENPYO_BANGO
            ";

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            //dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 10, 3); // 現行は伝票区分=3しか入っていないのでユニークキーにも関わらず未指定！
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            // 精算区分を取得
            seisanKbn = this.getSeisanKubun();
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 4, this.seisanKbn); // 精算区分（1=県外, 2=地元, 3=県内）

            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, fromDate);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, foDate);
            dpc.SetParam("@SENSHU_CD_FR", SqlDbType.Decimal, 10, funanushiCdFr);
            dpc.SetParam("@SENSHU_CD_TO", SqlDbType.Decimal, 10, funanushiCdTo);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql, dpc);

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private DataTable editDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 日付を編集する際に用いるワーク
            string[] aryDate;
            string tmpDate;

            // 一旦、伝票一覧（明細）をクリアする
            this.dgvList.Rows.Clear();

            // 列の定義を作成
            dtResult.Columns.Add("SERIBI", typeof(string));
            dtResult.Columns.Add("DENPYO_BANGO", typeof(int));
            dtResult.Columns.Add("SENSHU_CD", typeof(int));
            dtResult.Columns.Add("SENSHU_NM", typeof(string));
            dtResult.Columns.Add("GYOHO_CD", typeof(int));
            dtResult.Columns.Add("MIZUAGE_ZEIKOMI_KINGAKU", typeof(string));
            dtResult.Columns.Add("MIZUAGE_SHOHIZEIGAKU", typeof(string));
            dtResult.Columns.Add("SEISAN_KUBUN", typeof(string));

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                // 行追加
                this.dgvList.Rows.Add();
                drResult = dtResult.NewRow();

                // DBから取得したデータを表示用領域(DataGridView)にコピーする
                aryDate = Util.ConvJpDate(Util.ToDate(dtData.Rows[i].ItemArray[COL_SERIBI]), this.Dba);
                tmpDate = Util.ToInt(aryDate[2]).ToString("00") + "/" + aryDate[3].PadLeft(2, ' ') + "/" + aryDate[4].PadLeft(2, ' ');

                this.dgvList.Rows[i].Cells[DGV_SERIBI].Value = tmpDate;
                this.dgvList.Rows[i].Cells[DGV_DENPYO_BANGO].Value = dtData.Rows[i].ItemArray[COL_DENPYO_BANGO].ToString();
                this.dgvList.Rows[i].Cells[DGV_SENSHU_CD].Value = dtData.Rows[i].ItemArray[COL_SENSHU_CD].ToString();
                this.dgvList.Rows[i].Cells[DGV_SENSHU_NM].Value = dtData.Rows[i].ItemArray[COL_SENSHU_NM].ToString();
                this.dgvList.Rows[i].Cells[DGV_GYOHO_NM].Value = dtData.Rows[i].ItemArray[COL_GYOHO_NM].ToString();
                this.dgvList.Rows[i].Cells[DGV_MIZUAGE_ZEIKOMI_KINGAKU].Value = Util.FormatNum(dtData.Rows[i].ItemArray[COL_MIZUAGE_ZEIKOMI_KINGAKU].ToString());
                this.dgvList.Rows[i].Cells[DGV_MIZUAGE_SHOHIZEIGAKU].Value = Util.FormatNum(dtData.Rows[i].ItemArray[COL_MIZUAGE_SHOHIZEIGAKU].ToString());
                this.dgvList.Rows[i].Cells[DGV_SEISAN_KUBUN].Value = dtData.Rows[i].ItemArray[COL_SEISAN_KUBUN].ToString();
                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }

        /// <summary>
        /// 伝票検索
        /// </summary>
        private void getListData()
        {
            // データ取得のSQLを発行してGridに反映
            DataTable dtList = new DataTable();
            try
            {
                dtList = this.getDenpyoList();
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                return;
            }

            if (dtList.Rows.Count <= 0)
            {
                //Msg.Notice("該当するデータがありません。");
                Msg.Info("該当データはありません。");
                this.dtDenDateFrom.Focus();
//                this.txtYearFr.Focus();
//                this.txtYearFr.SelectAll();
                return;
            }
            else
            {
                // 取得したデータを一覧表示する
                DataTable dtDsp = this.editDataList(dtList);

                // 一覧へフォーカスを移動する
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 伝票検索結果一覧ヘッダー設定
        /// </summary>
        private void setDenpyoListHeader()
        {
            DataGridViewTextBoxColumn col;

            // ********************************
            // * 伝票検索結果一覧（見出し行） *
            // ********************************
            // 伝票日付
            col = new DataGridViewTextBoxColumn();
            col.Name = "SERIBI";
            col.HeaderText = "伝票日付";
            col.Width = 81;
            col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns.Add(col);

            // 伝票番号
            col = new DataGridViewTextBoxColumn();
            col.Name = "DENPYO_BANGO";
            col.HeaderText = "伝票番号";
            col.Width = 90;
            col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns.Add(col);

            // 船主CD
            col = new DataGridViewTextBoxColumn();
            col.Name = "SENSHU_CD";
            col.HeaderText = "船主CD";
            col.Width = 90;
            col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns.Add(col);

            // 船主名称
            col = new DataGridViewTextBoxColumn();
            col.Name = "SENSHU_NM";
            col.HeaderText = "船主名称";
            col.Width = 200;
            col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns.Add(col);

            // 漁法CD（実際は名称が表示される）
            col = new DataGridViewTextBoxColumn();
            col.Name = "GYOHO_CD";
            col.HeaderText = "漁法CD";
            col.Width = 180;
            col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns.Add(col);

            // 伝票金額 ※税抜きor税込み？
            col = new DataGridViewTextBoxColumn();
            col.Name = "MIZUAGE_ZEIKOMI_KINGAKU";
            col.HeaderText = "伝票金額";
            col.Width = 150;
            col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns.Add(col);

            // 消費税
            col = new DataGridViewTextBoxColumn();
            col.Name = "MIZUAGE_SHOHIZEIGAKU";
            col.HeaderText = "（消費税）";
            //col.Width = 95;
            col.Width = 120;
            col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns.Add(col);

            // 精算区分
            col = new DataGridViewTextBoxColumn();
            col.Name = "SEISAN_KUBUN";
            col.HeaderText = "（精算区分)";
            col.Width = 120;
            col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns.Add(col);
        }

        /// <summary>
        /// 呼び出し元に戻り値（選択行の伝票番号）を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells[DGV_SEISAN_KUBUN].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells[DGV_DENPYO_BANGO].Value),
                this.getDenpyoCondition(),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 伝票一覧からフォーカスが外れた時に伝票一覧を消去する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_Leave(object sender, EventArgs e)
        {
            this.dgvList.Rows.Clear();

        }

        /// <summary>
        /// 精算区分取得
        /// </summary>
        /// <returns>精算区分</returns>
        private int getSeisanKubun()
        {
            int seisanKubun = 0;
            seisanKubun = Util.ToInt(Util.ToString(this.txtSeisanKbn.Text));

            return seisanKubun;
        }
        private void setDenpyoCondition(string denpyoCondition)
        {
            string[] cnd = denpyoCondition.Split(',');
            if (cnd.Length != 0)
            {
                try
                {
                    this.seisanKbn = Util.ToInt(cnd[0]);
                    this.txtSeisanKbn.Text = this.seisanKbn.ToString();
                    this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, 
						 "TB_HN_COMBO_DATA_SEISAN", Util.ToString(this.ShishoCode), this.txtSeisanKbn.Text);

                    string[] d = Util.ConvJpDate(Util.ToDate(cnd[1]), this.Dba);
                    this.dtDenDateFrom.JpDate = d;

//                    SetJpFr(d);
                    d = Util.ConvJpDate(Util.ToDate(cnd[2]), this.Dba);
                    //                    SetJpTo(d);
                    this.dtDenDateTo.JpDate = d;

                    this.txtFunanushiCdFr.Text = cnd[3];
                    this.lblFunanishiNmFr.Text = cnd[4];
                    this.txtFunanushiCdTo.Text = cnd[5];
                    this.lblFunanushiNmTo.Text = cnd[6];
                }
                catch (Exception)
                {
                }
            }
        }
        private string getDenpyoCondition()
        {
            string[] cnd = {
                            this.getSeisanKubun().ToString(),
                            Util.ConvAdDate(this.dtDenDateFrom.Gengo, 
							                this.dtDenDateFrom.WYear, 
											this.dtDenDateFrom.Month, 
											this.dtDenDateFrom.Day, 
											this.Dba).ToString("yyyy/MM/dd"),
                            Util.ConvAdDate(this.dtDenDateTo.Gengo, 
							                this.dtDenDateTo.WYear, 
											this.dtDenDateTo.Month, 
											this.dtDenDateTo.Day, 
											this.Dba).ToString("yyyy/MM/dd"),
                            this.txtFunanushiCdFr.Text,
                            this.lblFunanishiNmFr.Text,
                            this.txtFunanushiCdTo.Text,
                            this.lblFunanushiNmTo.Text
            };

            return string.Join(",", cnd);
        }
		#endregion

		private void HNSE1012_Shown(object sender, EventArgs e)
		{
			this.dtDenDateFrom.Focus();
		}
	}
}
