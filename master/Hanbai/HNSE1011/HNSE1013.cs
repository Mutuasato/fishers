﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

using System.Collections.Generic;

namespace jp.co.fsi.hn.hnse1011
{
    /// <summary>
    /// 状態情報参照(HNSE1013)
    /// </summary>
    public partial class HNSE1013 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// HNSE1013(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>

        // 支所
        int ShishoCode;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSE1013()
        {

            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            // タイトルは非表示
            this.lblTitle.Visible = false;

            this.btnF2.Visible = false;
            this.btnF3.Enabled = true;
            this.btnF3.Visible = true;
            this.btnF3.Location = this.btnF2.Location;
            this.btnF3.Text = this.btnF2.Text;
            this.btnF3.Text = this.btnF3.Text.Replace("F2", "F3");
            this.btnF3.Enabled = false;
            this.btnF3.Visible = false;

            // データ取得のSQLを発行してGridに反映
            DataTable dtList = new DataTable();
            try
            {
                dtList = this.GetData();
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                return;
            }

            // 取得したデータを表示用に編集
            DataTable dtDsp = this.EditDataList(dtList);

            this.dgvList.DataSource = dtDsp;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvList.Columns[0].ReadOnly = false;
            this.dgvList.Columns[0].Width = 80;// 100;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[1].Width = 80;// 148;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[2].Width = 178;// 148;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            this.dgvList.Refresh();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                default:
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // 未使用
            return;
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ReturnVal();
            }
            else if (e.KeyCode == Keys.Space)
            {
                DataGridViewCell cell = dgvList[dgvList.CurrentCell.ColumnIndex, dgvList.CurrentCell.RowIndex];
                if (cell is DataGridViewCheckBoxCell)
                {
                    DataGridViewCheckBoxCell checkCell = cell as DataGridViewCheckBoxCell;
                    if (checkCell.TrueValue == null)
                    {
                        if (checkCell.Value.ToString() == "True")
                        {
                            checkCell.Value = false;
                        }
                        else
                        {
                            checkCell.Value = true;
                        }
                    }
                    else
                    {
                        checkCell.Value = (checkCell.Value == checkCell.TrueValue) ? checkCell.FalseValue : checkCell.TrueValue;
                    }
                    dgvList.RefreshEdit();
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        /// <summary>
        /// グリッドの行フォーカス時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            this.ReturnVal();
        }

        private void dgvList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewCell cell = dgvList[e.ColumnIndex, e.RowIndex];
            if (cell is DataGridViewCheckBoxCell)
            {
                DataGridViewCheckBoxCell checkCell = cell as DataGridViewCheckBoxCell;
                if (checkCell.TrueValue == null)
                {
                    if (checkCell.Value.ToString() == "True")
                    {
                        checkCell.Value = false;
                    }
                    else
                    {
                        checkCell.Value = true;
                    }
                }
                else
                {
                    checkCell.Value = (checkCell.Value == checkCell.TrueValue) ? checkCell.FalseValue : checkCell.TrueValue;
                }
                dgvList.RefreshEdit();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 状態情報のデータを取得
        /// </summary>
        /// <returns>状態情報の取得したデータ</returns>
        private DataTable GetData()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append(" 0 AS 選択,");
            sql.Append(" A.JYOUTAI_CD AS コード,");
            sql.Append(" A.JYOUTAI_NM AS 名称 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_JYOUTAI AS A ");
            sql.Append("WHERE ");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD ");
            sql.Append("ORDER BY");
            sql.Append(" A.JYOUTAI_CD");

            DbParamCollection dpc = new DbParamCollection();
            DataTable dtResult = null;
            try
            {
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 6, this.ShishoCode);

                dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 列の定義を作成
            dtResult.Columns.Add("選択", typeof(bool));
            dtResult.Columns.Add("コード", typeof(string));
            dtResult.Columns.Add("名称", typeof(string));

            List<string> kizList = new List<string>();
            if (this.InData != null && !ValChk.IsEmpty(this.InData))
            {
                string inData = (string)this.InData;
                if (inData.Length != 0)
                {
                    string[] kizs = inData.Split(',');
                    kizList.AddRange(kizs);
                }
            }

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();

                string cd = dtData.Rows[i]["コード"].ToString();
                kizList.Contains(cd);
                drResult["選択"] = (kizList.Contains(cd) ? true : false); 
                drResult["コード"] = dtData.Rows[i]["コード"];
                drResult["名称"] = dtData.Rows[i]["名称"];

                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            DataGridViewCell cell;
            string retCd = string.Empty;
            string retNm = string.Empty;
            int i = 0;
            while (this.dgvList.RowCount > i)
            {
                cell = this.dgvList[0, i];
                if (cell.Value.ToString() == "True")
                {
                    retCd += this.dgvList[1, i].Value.ToString() + ",";
                    retNm += this.dgvList[2, i].Value.ToString() + ",";
                }
                i++;
            }
            if (retCd.Length > 1)
                retCd = retCd.Substring(0, retCd.Length - 1);
            if (retNm.Length > 1)
                retNm = retNm.Substring(0, retNm.Length - 1);
            this.OutData = new string[2] {
                retCd,
                retNm,
            };

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
