﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hnse1011
{

    #region 構造体
    /// <summary>
    /// 伝票明細計算情報のデータ構造体
    /// </summary>
    struct MeisaiCalcInfo
    {
        public decimal KINGAKU_HASU_SHORI;     // 金額端数処理
        public decimal SHOHIZEI_NYURYOKU_HOHO; // 消費税入力方法
        public decimal SHOHIZEI_HASU_SHORI;    // 消費税端数処理
        public decimal SHOHIZEI_TENKA_HOHO;    // 消費税転嫁方法

        public int BUMON_CD;                   // 基本部門コード
        public int JIGYO_KUBUN;                // 基本事業区分
        public string DENPYO_DATE;             // 伝票日付保持
        public int FUNANISHI_CD; 　            // 変更前の船主コード
        public decimal NIUKENIN_RITSU;         // 荷受人手数料率

        public void Clear()
        {
            KINGAKU_HASU_SHORI = 0;            // 初期は四捨五入
            SHOHIZEI_NYURYOKU_HOHO = 0;        // 初期は税抜き
            SHOHIZEI_HASU_SHORI = 0;           // 初期は四捨五入
            SHOHIZEI_TENKA_HOHO = 0;           // 初期は明細転嫁

            BUMON_CD = 0;
            //JIGYO_KUBUN = 0;
            DENPYO_DATE = string.Empty;
            FUNANISHI_CD = 0;
            NIUKENIN_RITSU = 0;
        }
    }
    #endregion

    /// <summary>
    /// セリ入力(HNSE1011)
    /// </summary>
    public partial class HNSE1011 : BasePgForm
    {
        /// <summary>
        /// 船主コード初期値
        /// </summary>
        private const String INIT_FUNANUSHI_CODE = "0";

        /// <summary>
        /// 精算区分
        /// </summary>
        // 精算区分の値を要確認
        private const int KENGAI = 1; // 県外
        private const int JIMOTO = 2; // 地元
        private const int KENNAI = 3; // 県内

        // 列の番号
        const int COL_GYO_NO = 0;             // 行No.
        const int COL_YAMA_NO = 1;            // 山No.
        const int COL_GYOSHU_CD = 2;          // 魚種CD
        const int COL_GYOSHU_NM = 3;          // 魚種名
        const int COL_HONSU = 4;              // 本数
        const int COL_KIZY_CD = 5;            // 傷CD
        const int COL_SURYO = 6;              // 水揚数量
        const int COL_TANKA = 7;              // 水揚単価
        const int COL_KINGAKU = 8;            // 水揚金額
        const int COL_SHOHIZEI = 9;           // 消費税
        const int COL_PAYAO_NO = 10;          // パヤオNO
        const int COL_NAKAGAININ_CD = 11;     // 仲買CD
        const int COL_NAKAGAININ_NM = 12;     // 仲買人名
        const int COL_SHIWAKE_CD = 13;        // 仕訳CD
        const int COL_BUMON_CD = 14;          // 部門CD
        const int COL_ZEI_KUBUN = 15;         // 税区分
        const int COL_JIGYO_KUBUN = 16;       // 事業区分
        const int COL_ZEI_RITSU = 17;         // 税率
        const int COL_KAZEI_KUBUN = 18;       // 課税区分
        const int COL_PAYAO_RITSU = 19;       // パヤオ率
        const int COL_KIZY_NM = 20;           // 傷名称
		// TODO 2020/02/12 追加 Mutu Asato
		const int COL_KEIFLG = 21;           // 軽減税率フラグ

		// DB登録項目での固定値
		protected const int DUMMY_DENPYO_KUBUN = 3;     // 伝票区分

        private decimal seisanKbn;
        // 入力モード設定
        private int MODE_EDIT = 1; // 1:登録,2:修正
        // データ入力フラグ(フォームを閉じる時に確認画面を出力時使用)
        private int IS_INPUT = 1;  // 1:未入力,2:入力済
        // 伝票明細計算情報を設定
        MeisaiCalcInfo SeriInfo = new MeisaiCalcInfo();
        // 伝票検索条件
        private string DenpyoCondition = string.Empty;
        // 伝票番号
        private decimal MEISAI_DENPYO_BANGO = 0;

        // 消費税計算用
        class MeisaiTbl
        {
            public int Row { get; set; }
            public decimal Kingk { get; set; }
            public decimal Zeigk { get; set; }
            public decimal ZeiRt { get; set; }
            public decimal Gentan { get; set; }
            public decimal Genka { get; set; }
            public string Flag { get; set; }
        }

        //private bool isCHanged = false;

        private bool seisanKbnCheck;   // 精算区分制御
        private int zeikomi_seisanKbn; // 税込み用精算区分（名護のハマ売り等）
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSE1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();

            //MessageForwarder(マウスホイールスクロール用) のインスタンス生成．対象はdgvInputList
            new MessageForwarder(this.dgvInputList, 0x20A);

            new MessageForwarder(this.txtGridEdit, 0x20A);

        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
			// 精算区分名
			// 設定より初期精算区分の表示（見つからない場合は３を設定）

			// TODO 2020/02/20 返却値がわかっているならそれなりの聞き方で処理すべし try catchをつかって取り合えずごまかすのはもってのほか！！！！！
			if (string.IsNullOrEmpty(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "SeisanKbn")))
			{
				this.txtSeisanKbn.Text = "3";
			}
			else
			{
				int seisanKubun = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "SeisanKbn"));
				this.txtSeisanKbn.Text = seisanKubun.ToString();
			}

			#region 削除 むやみにtry catch 使うべからず、処理速度遅くなる。わかってるのかな？　Mさん
			//try
			//{
			//	int seisanKubun = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "SeisanKbn"));
			//	this.txtSeisanKbn.Text = seisanKubun.ToString();
			//}
			//catch (Exception)
			//{
			//	this.txtSeisanKbn.Text = "3";
			//}
			#endregion

			this.seisanKbn = Util.ToDecimal(this.txtSeisanKbn.Text);
            SetSeisanKubun(this.txtSeisanKbn.Text);

			// 強制設定
			// TODO 2020/02/20 返却値がわかっているならそれなりの聞き方で処理すべし try catchをつかって取り合えずごまかすのははもってのほか！！！！！
			if (string.IsNullOrEmpty(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Zeikomi_SeisanKbn")))
			{
				// 強制税込み精算区分が無ければ無し
				this.zeikomi_seisanKbn = 0;
			}
			else
			{
				// 強制税込み精算区分の設定があればその精算区分は税込み処理（ルールとしてシステム全体で統一する事）
				this.zeikomi_seisanKbn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Zeikomi_SeisanKbn"));

			}

			#region 削除むやみにtry catch 使うべからず、処理速度遅くなる。わかってるのかな？　Mさん
			//try
			//{
			//	// 強制税込み精算区分の設定があればその精算区分は税込み処理（ルールとしてシステム全体で統一する事）
			//	this.zeikomi_seisanKbn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Zeikomi_SeisanKbn"));
			//}
			//catch (Exception)
			//{
			//	// 強制税込み精算区分が無ければ無し
			//	this.zeikomi_seisanKbn = 0;
			//}
			#endregion

			// 列ヘッダの高さ調整を不可
			//this.dgvInputList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

			// 新規モードの初期表示
			InitDispOnNew();

            // 基本事業区分の取得
            DataRow r = this.Dba.GetKaikeiSettingsByKessanKi(this.UInfo.KaishaCd, this.UInfo.KessanKi).Rows[0];
            SeriInfo.JIGYO_KUBUN = Util.ToInt(r["JIGYO_KUBUN"]);


		}

		/// <summary>
		/// フォーカス移動時処理
		/// </summary>
		protected override void OnMoveFocus()
        {
            /* 検索用小窓が使える項目にフォーカス時のみF1キーを有効にする
                 ただし、明細は dgvInputList_CellEnter() でF1キーを有効にしている
             */
            switch (this.ActiveCtlNm)
            {
                case "txtYear":
                //case "txtMonth":
                //case "txtDay":
                case "txtDenpyoBango":
                case "txtMizuageShishoCd":
                case "txtFunanushiCd":
                case "txtGyogyoushuCd":
                case "txtMaeukeninCd":
                case "txtNiukeninCd":
                case "txtSeisanKbn":
                    this.btnF1.Enabled = true;
                    break;
                case "txtGridEdit":
                    switch (this.dgvInputList.CurrentCell.ColumnIndex)
                    {
                        case COL_GYOSHU_CD:
                        case COL_KIZY_CD:
                        case COL_PAYAO_NO:
                        case COL_NAKAGAININ_CD:
                            this.btnF1.Enabled = true;
                            break;
                        default:
                            this.btnF1.Enabled = false;
                            break;
                    }
                    break;
                default:

                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {

			if (this.IS_INPUT == 2)
			{
				string msg = "未更新データが存在します！　処理を終了しますか？";
				if (Msg.ConfYesNo(msg) == DialogResult.No)
				{
					return;
				}
				else
				{
					this.OrgnDialogResult = false;
				}
			}

			// DialogResultとしてCancelを返却する
			base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。

            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            String[] result;
            switch (this.ActiveCtlNm)
            {
                #region 元号
                //case "txtYear":
                //    //result = this.openSearchWindow("COMC9011", "", this.lblGengo.Text);
                //    result = this.openSearchWindow(stCurrentDir + @"\EXE\" + "CMCM1021", "", this.lblGengo.Text);
                //    if (!ValChk.IsEmpty(result[0]))
                //    {
                //        this.lblGengo.Text = result[1];

                //        // 存在しない日付の場合、補正して存在する日付に戻す
                //        SetJp();
                //    }
                //    break;
                #endregion

                #region 伝票番号
                case "txtDenpyoBango": // 伝票番号
                    // セリ伝票検索選択画面
                    using (HNSE1012 frmHNSE1012 = new HNSE1012())
                    {
                        // 精算区分（地元、県内、県外のラジオボタン選択値）を伝票検索に渡す
                        frmHNSE1012.Par1 = Util.ToString(this.getSeisanKubun());

                        // 伝票検索条件
                        frmHNSE1012.InData = this.DenpyoCondition;

                        frmHNSE1012.ShowDialog(this);

                        if (frmHNSE1012.DialogResult == DialogResult.OK)
                        {
                            string[] ret = (string[])frmHNSE1012.OutData;
                            this.seisanKbn = Util.ToDecimal(ret[0]);
                            this.txtSeisanKbn.Text = this.seisanKbn.ToString();
                            SetSeisanKubun(this.txtSeisanKbn.Text);

                            // 伝票検索条件
                            this.DenpyoCondition = ret[2];

                            this.txtDenpyoBango.Text = ret[1];
                            this.txtDenpyoBango.Focus();

                            // なんで伝票番号の呼び出しなし？
                            if (this.GetDenpyoData())
                            {
                                this.txtDenpyoBango.Focus();
                                return;
                            }

                            this.OrgnDialogResult = true;

                            // フォーカスを船主コードに移動する（Validatingイベントが発生して伝票番号チェックが呼ばれる）
                            this.txtFunanushiCd.Focus();
                        }
                        else
                        {
                            // 伝票検索条件クリア
                            this.DenpyoCondition = string.Empty;
                        }

                    }
                    break;
                #endregion

                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    //result = this.openSearchWindow("COMC8011", "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
                    result = this.openSearchWindow(stCurrentDir + @"\EXE\" + "CMCM2031", "1", this.txtMizuageShishoCd.Text);
                    if (!ValChk.IsEmpty(result[0]))
                    {
                        this.txtMizuageShishoCd.Text = result[0];
                        this.lblMizuageShishoNm.Text = result[1];

                        // 次の項目(船主コード)にフォーカス
                        this.txtFunanushiCd.Focus();
                    }
                    break;
                #endregion

                #region 精算区分
                case "txtSeisanKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.InData = this.txtSeisanKbn.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (String[])frm.OutData;
                                this.txtSeisanKbn.Text = outData[0];
                                this.lblSeisanKbnNm.Text = outData[1];
                                this.seisanKbn = Util.ToDecimal(this.txtSeisanKbn.Text);
                                this.txtNiukeninCd.Enabled = this.seisanKbn == KENNAI ? false : true;
                            }
                        }
                    }
                    break;
                #endregion

                #region 船主CD
                case "txtFunanushiCd": // 船主CD    new is "KOBC9021.exe" old is "COMC8011"   "VI_HN_FUNANUSHI"
                    //result = this.openSearchWindow("KOBC9021", "1", this.txtFunanushiCd.Text);
                    //result = this.openSearchWindow("CMCM2011", "1", this.txtFunanushiCd.Text);

                    string[] result1 = { "", "" };

                    //アセンブリロード
                    Assembly asm1 = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");

                    string NameSpace = "cm.cmcm2011";

                    //フォーム作成
                    string moduleNameSpace = "jp.co.fsi." + NameSpace + "." + "CMCM2011";
                    Type t1 = asm1.GetType(moduleNameSpace);

                    if (t1 != null)
                    {
                        Object obj = Activator.CreateInstance(t1);
                        if(obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtFunanushiCd.Text;
                            frm.ShowDialog(this);

                            if(frm.DialogResult == DialogResult.OK)
                            {
                                string[] ret = (string[])frm.OutData;
                                result1[0] = ret[0];
                                result1[1] = ret[1];
                            }
                        }
                    }


                    if (!ValChk.IsEmpty(result1[0]))
                    {
                        this.txtFunanushiCd.Text = result1[0];
                        this.lblFunanushiNm.Text = result1[1];

                        txtFunanushiCd.SelectAll();
                        txtFunanushiCd.Focus();
                    }
                    break;
                #endregion

                #region 漁業種
                case "txtGyogyoushuCd": // 漁業種
                    //result = this.openSearchWindow("COMC8111", "", this.txtGyogyoushuCd.Text);
                    //result = this.openSearchWindow("HNCM1031", "1", this.txtGyogyoushuCd.Text);

                    string[] result2 = { "", "" };

                    //アセンブリロード
                    Assembly asm2 = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1031.exe");

                    string NameSpace2 = "hn.hncm1031";

                    //フォーム作成
                    string moduleNameSpace2 = "jp.co.fsi." + NameSpace2 + "." + "HNCM1031";
                    Type t2 = asm2.GetType(moduleNameSpace2);

                    if (t2 != null)
                    {
                        Object obj = Activator.CreateInstance(t2);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtFunanushiCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] ret = (string[])frm.OutData;
                                result2[0] = ret[0];
                                result2[1] = ret[1];
                            }
                        }
                    }


                    if (!ValChk.IsEmpty(result2[0]))
                    {
                        this.txtGyogyoushuCd.Text = result2[0];
                        this.lblGyogyoushuNm.Text = result2[1];

                        // 次の項目(箱数)にフォーカス
                        //this.txtHakosuu.Focus();
                        //this.txtHakosuu.ReadOnly = true;
                        this.txtGyogyoushuCd.SelectAll();
                        this.txtGyogyoushuCd.Focus();
                    }
                    break;
                #endregion

                #region 荷受人（手数料）
                case "txtNiukeninCd": // 荷受人
                    //result = this.openSearchWindow("COMC8011", "TM_HN_TESURYO_RITSU_MST_NIUKE", this.txtNiukeninCd.Text);
                    //if (!ValChk.IsEmpty(result[0]))
                    //{
                    //    this.txtNiukeninCd.Text = result[0];
                    //    this.lblNiukeninNm.Text = result[1];
                    //}
                    {
                        // アセンブリのロード 
                        //System.Reflection.Assembly asmN = System.Reflection.Assembly.LoadFrom("HANC9211.exe");
                        //System.Reflection.Assembly asmN = System.Reflection.Assembly.LoadFrom("HNCM1101.exe");
                        asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1091.exe");
                        // フォーム作成
                        //Type tN = asmN.GetType("jp.co.fsi.han.hanc9211.HANC9211");
                        //Type tN = asmN.GetType("jp.co.fsi.hn.hncm1101.HNCM1101");
                        t = asm.GetType("jp.co.fsi.hn.hncm1091.HNCM1091");
                        if (t != null)
                        {
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.Par2 = this.getSeisanKubun().ToString();
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] outData = (string[])frm.OutData;

                                    try
                                    {
                                        this.txtNiukeninCd.Text = outData[1];
                                        this.lblNiukeninNm.Text = outData[2];
                                        this.SeriInfo.NIUKENIN_RITSU = Util.ToDecimal(outData[3]);
                                    }
                                    catch (InvalidOperationException)
                                    {
                                        ; // 例外を握りつぶす
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region 明細
                case "txtGridEdit":
                    switch (this.dgvInputList.CurrentCell.ColumnIndex)
                    {
                        case 2:  // 魚種CD
                            // アセンブリのロード
                            //asm = System.Reflection.Assembly.LoadFrom("HANC9061.exe");
                            asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1051.exe");
                            // フォーム作成
                            t = asm.GetType("jp.co.fsi.hn.hncm1051.HNCM1051");
                            if (t != null)
                            {
                                Object obj = System.Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.Par1 = "1";
                                    frm.Par2 = this.txtMizuageShishoCd.Text;
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        string[] outData = (string[])frm.OutData;

                                        try
                                        {

                                            this.txtGridEdit.Text = outData[0];
                                            this.txtGridEdit.SelectAll();
                                            // 魚種情報取得
                                            this.GetGyoshuInfo(2);

                                        }
                                        catch (InvalidOperationException)
                                        {
                                            ; // 例外を握りつぶす
                                        }
                                    }
                                }
                            }
                            this.txtGridEdit.Focus();
                            break;

                        case COL_KIZY_CD: // 状態
                            using (HNSE1013 frm1013 = new HNSE1013())
                            {
                                string inData = Util.ToString(this.dgvInputList[COL_KIZY_CD, this.dgvInputList.CurrentCell.RowIndex].Value);
                                if (Util.ToString(inData).EndsWith(","))
                                {
                                    inData = Util.ToString(inData).Substring(0, Util.ToString(inData).Length - 1);
                                }
                                frm1013.InData = inData;
                                frm1013.ShowDialog(this);

                                if (frm1013.DialogResult == DialogResult.OK)
                                {
                                    string[] outData = (string[])frm1013.OutData;
                                    this.txtGridEdit.Text = outData[0];
                                    this.dgvInputList[COL_KIZY_CD, this.dgvInputList.CurrentCell.RowIndex].Value = outData[0];
                                    this.dgvInputList[COL_KIZY_NM, this.dgvInputList.CurrentCell.RowIndex].Value = outData[1];
                                }
                            }
                            this.txtGridEdit.SelectAll();
                            this.txtGridEdit.Focus();
                            break;

                        case COL_PAYAO_NO:// 8: // パヤオ
                            // アセンブリのロード
                            //asm = System.Reflection.Assembly.LoadFrom("HANC9201.exe");
                            //asm = System.Reflection.Assembly.LoadFrom("HNCM1091.exe");
                            asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1081.exe");
                            // フォーム作成
                            //t = asm.GetType("jp.co.fsi.han.hanc9201.HANC9201");
                            //t = asm.GetType("jp.co.fsi.hn.hncm1091.HNCM1091");
                            t = asm.GetType("jp.co.fsi.hn.hncm1081.HNCM1081");
                            if (t != null)
                            {
                                Object obj = System.Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.Par1 = "1";
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        string[] outData = (string[])frm.OutData;

                                        try
                                        {
                                            this.txtGridEdit.Text = outData[0];
                                            this.dgvInputList[COL_PAYAO_NO, this.dgvInputList.CurrentCell.RowIndex].Value = outData[0]; // パヤオNO
                                            this.dgvInputList[COL_PAYAO_RITSU, this.dgvInputList.CurrentCell.RowIndex].Value = outData[3]; // 手数料率
                                        }
                                        catch (InvalidOperationException)
                                        {
                                            ; // 例外を握りつぶす
                                        }
                                    }
                                }
                            }
                            this.txtGridEdit.SelectAll();
                            this.txtGridEdit.Focus();
                            break;


                        case COL_NAKAGAININ_CD:// 9: // 仲買人CD
                            // アセンブリのロード
                            //asm = System.Reflection.Assembly.LoadFrom("HANC9021.exe");
                            asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1011.exe");
                            // フォーム作成
                            //t = asm.GetType("jp.co.fsi.han.hanc9021.HANC9021");
                            t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                            if (t != null)
                            {
                                Object obj = System.Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.Par1 = "1";
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        string[] outData = (string[])frm.OutData;

                                        this.txtGridEdit.Text = outData[0];

                                        // VI_HN_TORIHIKISAKI_JOHO
                                        // なぜ仲買人登録画面で利用しているビューとチェック用のビューが違うのかわからん
                                        //  TB_HN_TORIHIKISAKI_JOHOではなくTB_HN_NAKAGAININ_HOHOというテーブルを参照していたが
                                        //  どこから登録されるのだろう、、、、、、、
                                        //ALTER VIEW[dbo].[VI_HN_NAKAGAI]
                                        //AS
                                        //SELECT
                                        //  A.KAISHA_CD AS KAISHA_CD,
                                        //  A.TORIHIKISAKI_CD AS TORIHIKISAKI_CD,
                                        //  B.TORIHIKISAKI_NM AS TORIHIKISAKI_NM,
                                        //  B.TORIHIKISAKI_KANA_NM AS TORIHIKISAKI_KANA_NM
                                        //  FROM TB_HN_TORIHIKISAKI_JOHO AS A
                                        //  INNER JOIN TB_CM_TORIHIKISAKI AS B
                                        //  ON A.KAISHA_CD = B.KAISHA_CD
                                        //    AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD
                                        //  WHERE A.TORIHIKISAKI_KUBUN2 = 2

                                        string get_name = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtMizuageShishoCd.Text, this.txtGridEdit.Text);
                                        if (get_name == null)
                                        {
                                            this.txtGridEdit.Text = "";
                                            Msg.Notice("仲買CDの入力に誤りがあります。");
                                            this.dgvInputList[COL_NAKAGAININ_CD, this.dgvInputList.CurrentCell.RowIndex].Value = string.Empty; // 仲買人CD
                                            this.dgvInputList[COL_NAKAGAININ_NM, this.dgvInputList.CurrentCell.RowIndex].Value = string.Empty; // 仲買人名
                                            this.txtGridEdit.Focus();
                                            return;
                                        }
                                        else
                                        {
                                            this.txtGridEdit.Text = outData[0];
                                            this.dgvInputList[COL_NAKAGAININ_CD, this.dgvInputList.CurrentCell.RowIndex].Value = outData[0]; // 仲買人CD
                                            this.dgvInputList[COL_NAKAGAININ_NM, this.dgvInputList.CurrentCell.RowIndex].Value = outData[1]; // 仲買人名
                                        }
                                    }
                                }
                            }
                            this.txtGridEdit.SelectAll();
                            this.txtGridEdit.Focus();
                            break;
                    }
                    break;
                    #endregion
                    //    break; // this.ActiveControl.Name の switch文の defaultのbreak;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
                return;

            // 登録モードは処理しない
            if (this.MODE_EDIT == 1)
            {
                return;
            }

            // 伝票の入力チェック
            if (!this.isValidDenpyoBango())
            {
                this.txtDenpyoBango.SelectAll();
                this.txtDenpyoBango.Focus();
                return;
            }
            // 空の場合、特に何もしない
            else if (ValChk.IsEmpty(this.txtDenpyoBango.Text))
            {
                return;
            }

            // 以下は必要か？
            //// 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            //if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            //{
            //    Msg.Error("この会計年度は凍結されています。");
            //    return;
            //}

            // *************************
            // * 削除処理              *
            // *************************
            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 削除処理
            this.DeleteData();

            // 表示などを初期化する
            this.InitDispOnNew();

        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 印刷処理
            //DoPrint(false);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {

            if (!this.btnF5.Enabled)
                return;

            // 以下は必要か？
            //// 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            //if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            //{
            //    Msg.Error("この会計年度は凍結されています。");
            //    return;
            //}

            // *************************
            // * 登録処理              *
            // *************************

            // 全入力項目チェック
            if (!this.isValidDataAll(this.MODE_EDIT))
            {
                return;
            }
            this.DataGridSum();

            // +---------------------------------+
            // | ※参考は KOBC9022.cs とかです。 |
            // +---------------------------------+
            // 確認メッセージを表示
            //string msg = (this.modeFlg == MODE_NEW ? "登録" : "更新") + "しますか？";
            string msg = (this.MODE_EDIT == 1 ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 登録処理
            this.UpdateData();

            // 登録初期表示処理へ
            this.InitDispOnNew();

        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            // *************************
            // * 行削除処理            *
            // *************************

            // 初期起動時
            if (this.dgvInputList.CurrentCell == null)
                return;

            // 行削除処理
            if (this.dgvInputList.RowCount > 1 && this.dgvInputList.RowCount > this.dgvInputList.CurrentCell.RowIndex + 1)
            {
                this.dgvInputList.Rows.RemoveAt(this.dgvInputList.CurrentCell.RowIndex);
                int i = 0;
                while (this.dgvInputList.RowCount > i)
                {
                    this.dgvInputList[COL_GYO_NO, i].Value = i + 1;
                    i++;
                }
                this.DataGridSum();
            }
            if (this.ActiveControl != dgvInputList)
                dgvInputList.Focus();

        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            // **********************************************************************************************************
            // * 行複写処理                                                                                             *
            // *   新規の明細行でF9キーが押下された場合は、明細データを末尾に追加する                                   *
            // *   既存の明細行でF9キーが押下された場合は、行No.と山No.以外の明細データをF9キーが押された行に上書きする *
            // **********************************************************************************************************

            // 初期起動時
            if (this.dgvInputList.CurrentCell == null)
                return;

            // 複写処理(上の行データを選択行データにコピー)
            if (this.dgvInputList.CurrentCell.RowIndex > 0)
            {
                int col = 1;
                int rowCurPos = this.dgvInputList.CurrentCell.RowIndex;
                DataGridViewRow getRow = this.dgvInputList.Rows[rowCurPos - 1];
                while (getRow.Cells.Count > col)
                {
                    if (col == 1)
                        this.dgvInputList[col, rowCurPos].Value = Util.ToDecimal(this.dgvInputList[col, rowCurPos - 1].Value) + 1;
                    else
                        this.dgvInputList[col, rowCurPos].Value = this.dgvInputList[col, rowCurPos - 1].Value;
                    col++;
                }
                if (this.txtGridEdit.Visible == true)
                {
                    this.txtGridEdit.Text = Util.ToString(this.dgvInputList[this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex].Value);
                }
                if (this.dgvInputList.Rows.Count == rowCurPos + 1)
                {
                    this.dgvInputList.RowCount = this.dgvInputList.RowCount + 1;
                    this.dgvInputList[COL_GYO_NO, this.dgvInputList.RowCount - 1].Value = this.dgvInputList.RowCount;

                    // 山NOもアップ
                    this.dgvInputList[COL_YAMA_NO, this.dgvInputList.RowCount - 1].Value = Util.ToDecimal(this.dgvInputList[COL_YAMA_NO, this.dgvInputList.CurrentCell.RowIndex].Value) + 1;
                }
                this.DataGridSum();
                if (this.ActiveControl != dgvInputList)
                    dgvInputList.Focus();
            }

        }

        #endregion

        #region イベント

        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            this.IS_INPUT = 2;
			// 新たに変更フラグ追加
			this.OrgnDialogResult = true;
		}

		/// <summary>
		/// 伝票番号の入力チェック
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void txtDenpyoBango_Validating(object sender, CancelEventArgs e)
        {
            // 空の場合、特に何もしない
            if (ValChk.IsEmpty(this.txtDenpyoBango.Text))
            {
                e.Cancel = false;
                return;
            }

            this.IS_INPUT = 2;
			// 新たに変更フラグ追加
			this.OrgnDialogResult = true;

			// 伝票データチェック後、伝票修正処理へ
			e.Cancel = this.GetDenpyoData();

            return;
        }

        /// <summary>
        /// 船主コードの入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCd_Validating(object sender, CancelEventArgs e)
        {
            //船主名、内訳クリア
            this.lblFunanushiNm.Text = "";
            this.lblUchiwake.Text = "";

            if (!ValChk.IsNumber(this.txtFunanushiCd.Text))
            {
                e.Cancel = true;
                return;
            }

            if (this.txtFunanushiCd.Text == "0" || ValChk.IsEmpty(this.txtFunanushiCd.Text))
            {
                return;
            }

            // 消費税転嫁方法、消費税入力方法をクリア
            this.lblShohizeiInputHoho2.Text = "";
            this.lblShohizeiTenka2.Text = "";

            // 船主変更時（事前保持）
            int bef = this.SeriInfo.FUNANISHI_CD;

            this.SetFunanushiInfo();

            // 船主変更時の明細再計算
            if (bef != 0 && !ValChk.IsEmpty(this.txtFunanushiCd.Text) && (Util.ToInt(this.txtFunanushiCd.Text) != bef))
            {
                CellTaxRateSet();
            }

            this.IS_INPUT = 2;
			// 新たに変更チェックフラグ追加
			this.OrgnDialogResult = true;

			//if (this.rdoBashoNm01.Checked)
			if (this.seisanKbn == KENNAI)
            {
                // 箱数へフォーカス
                //this.txtHakosuu.Focus();
                //this.txtHakosuu.ReadOnly = true;
                //this.txtHakosuu.TabStop = false;
            }
            else
            {
                // 荷受人へフォーカス
                this.txtNiukeninCd.Focus();
            }
            //消費税計算
            this.DataGridSum();
        }

        /// <summary>
        /// 漁協手数料の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyokyouTesuryou_Validating(object sender, CancelEventArgs e)
        {
            this.IS_INPUT = 2;

			// 新たに変更フラグ追加
			this.OrgnDialogResult = true;

		}

		/// <summary>
		/// 荷受人コードの値チェック処理
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void txtNiukeninCd_Validating(object sender, CancelEventArgs e)
        {
            this.IS_INPUT = 2;
			// 新たに変更フラグ追加
			this.OrgnDialogResult = true;

			// 箱数へフォーカス
			this.txtHakosuu.TabStop = false;
        }

        /// <summary>
        /// 漁業種入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyogyoushuCd_Validating(object sender, CancelEventArgs e)
        {
			dgvInputList.Focus();
			dgvInputList.CurrentCell = dgvInputList[COL_YAMA_NO, 0];
			// 新たに変更フラグ追加
			this.OrgnDialogResult = true;

			this.IS_INPUT = 2;
        }

        /// <summary>
        /// 箱数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHakosuu_Validating(object sender, CancelEventArgs e)
        {

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidHakosuu())
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtHakosuu.SelectAll();
                e.Cancel = true;
                return;
            }
			// 新たに変更フラグ追加
			this.OrgnDialogResult = true;

			this.IS_INPUT = 2;
        }

        /// <summary>
        /// 箱数からの移動制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHakosuu_KeyDown(object sender, KeyEventArgs e)
        {
            // ↑キーの場合、船主CDにフォーカス
            if (e.KeyCode == Keys.Up)
            {
                txtFunanushiCd.Focus();
            }

            // Enterキーの場合、山Noにフォーカス
            if (e.KeyCode == Keys.Enter)
            {
                dgvInputList.Focus();
                dgvInputList.CurrentCell = dgvInputList[COL_YAMA_NO, 0];
            }
        }

        /// <summary>
        /// 精算区分入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                e.Cancel = true;
                this.txtSeisanKbn.SelectAll();

                seisanKbnCheck = false;
            }
            else
            {
                if (this.txtSeisanKbn.Text == "0")
                {
                    return;
                }
                if (this.seisanKbn != Util.ToDecimal(this.txtSeisanKbn.Text))
                {
                    this.seisanKbn = Util.ToDecimal(this.txtSeisanKbn.Text);

                    MeisaiClear();
                    //if (this.seisanKbn == JIMOTO)
                    if (this.seisanKbn == this.zeikomi_seisanKbn)
                        this.lblSyohiZei.Text = "消費税（内）";
                    else
                        this.lblSyohiZei.Text = "消費税（外）";

                    this.txtDenpyoBango.Text = "";
                    this.txtNiukeninCd.Enabled = this.seisanKbn == KENNAI ? false : true;
                    this.txtNiukeninCd.Text = "";
                    this.lblNiukeninNm.Text = "";
                    this.SeriInfo.NIUKENIN_RITSU = 0;
                    this.SetFunanushiInfo();
                }

                seisanKbnCheck = true;
            }
        }

        /// <summary>
        /// 精算区分からの移動制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_KeyDown(object sender, KeyEventArgs e)
        {
            // Enterキーの場合、船主CDにフォーカス
            if (seisanKbnCheck && e.KeyCode == Keys.Enter)
            {
                txtFunanushiCd.Focus();
            }
        }

        /// <summary>
        /// データグリッドにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Enter(object sender, EventArgs e)
        {
            DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
            if (dgvCell != null && dgvCell.ColumnIndex == 0)
            {
                this.dgvInputList.CurrentCell = this.dgvInputList[COL_YAMA_NO, dgvCell.RowIndex];
            }
        }

        /// <summary>
        /// データグリッドのセルにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case COL_YAMA_NO:       // 1: // 山NO
                case COL_GYOSHU_CD:     // 2: // 魚種CD
                case COL_HONSU:         // 4  // 本数
                case COL_KIZY_CD:       // 4: // 傷
                case COL_SURYO:         // 4: // 数量
                case COL_TANKA:         // 5: // 単価
                case COL_KINGAKU:       // 6: // 金額
                //case 7: // 消費税
                case COL_PAYAO_NO:      // 8: // パヤオ
                case COL_NAKAGAININ_CD: // 9: // 仲買人CD

                    // 消費税の部分を入力不可に
                    if (e.ColumnIndex == COL_SHOHIZEI && ValChk.IsEmpty(this.dgvInputList[COL_ZEI_RITSU, e.RowIndex].Value))
                    {
                        if (ValChk.IsEmpty(this.dgvInputList[COL_ZEI_KUBUN, e.RowIndex].Value))
                        {
                            break;
                        }
                    }
                    // 金額の部分を入力不可に
                    if (e.ColumnIndex == COL_KINGAKU)
                    {
                        break;
                    }
                    this.txtGridEdit.BackColor = Color.White;
                    if (e.RowIndex % 2 == 1)
                    {
                        this.txtGridEdit.BackColor = ColorTranslator.FromHtml("#BBFFFF");
                    }
                    this.txtGridEdit.Visible = true;
                    DataGridView dgv = (DataGridView)sender;
                    Rectangle rctCell = dgv.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);

                    this.txtGridEdit.Size = rctCell.Size;
                    this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
                    this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;
                    // 状態の場合はカンマを残す
                    if (e.ColumnIndex != COL_KIZY_CD)
                    {
                        this.txtGridEdit.Text = Util.ToString(dgvInputList[e.ColumnIndex, e.RowIndex].Value).Replace(",", "");
                    }
                    else
                    {
                        this.txtGridEdit.Text = Util.ToString(dgvInputList[e.ColumnIndex, e.RowIndex].Value);
                    }

                    this.txtGridEdit.Focus();

                    this.txtGridEdit.SelectAll();

                    break;
                default:
                    this.txtGridEdit.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// データグリッドのマウスダウン時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 検索制御
            switch (e.ColumnIndex)
            {
                case COL_YAMA_NO:       // 1: // 山NO
                case COL_HONSU:         // 4  // 本数
                case COL_SURYO:         // 6: // 数量
                case COL_TANKA:         // 7: // 単価
                case COL_KINGAKU:       // 8: // 金額
                    this.btnF1.Enabled = false;
                    break;
                case COL_GYOSHU_CD:     // 2: // 魚種CD
                case COL_KIZY_CD:       // 5: // 傷
                case COL_PAYAO_NO:      //10: // パヤオ
                case COL_NAKAGAININ_CD: //11: // 仲買人CD
                    this.btnF1.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// データグリッドがスクロールした時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            Rectangle rctCell = dgv.GetCellDisplayRectangle(this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex, false);

            this.txtGridEdit.Size = rctCell.Size;
            this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
            this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;
        }

        /// <summary>
        /// データグリッドの書式設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            // マイナスを赤字
            switch (e.ColumnIndex)
            {
                case COL_SURYO:    // 6:
                case COL_KINGAKU:  // 8:
                case COL_SHOHIZEI: // 9:
                    decimal d = Util.ToDecimal(Util.ToString(e.Value));
                    if (d < 0)
                    {
                        e.CellStyle.ForeColor = Color.Red;
                    }
                    else
                    {
                        e.CellStyle.ForeColor = Color.Black;
                    }
                    break;
            }
        }

        /// <summary>
        /// データグリッド用のテキストにキーダウン時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGridEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.Right)
            {
                DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
                decimal inputData = 0;

                // 追加制御
                GridCursorControl gcc = GridCursorControl.GetInstance(this.Config);

                // 新規行へ下カーソルは魚種のみ
                switch (dgvCell.ColumnIndex)
                {
                    case COL_YAMA_NO:
                    case COL_HONSU:
                    case COL_KIZY_CD:
                    case COL_SURYO:
                    case COL_TANKA:
                    case COL_KINGAKU:
                    case COL_PAYAO_NO:
                    case COL_NAKAGAININ_CD:
                        {
                            if (e.KeyCode == Keys.Down)
                            {
                                int rows = this.dgvInputList.Rows.Count - 1;
                                if ((dgvCell.RowIndex + 1) == rows)
                                {
                                    this.txtGridEdit.Focus();
                                    return;
                                }
                            }
                        }
                        break;
                }

                switch (dgvCell.ColumnIndex)
                {
                    case COL_YAMA_NO: // 山NO
                        if ((dgvCell.Value == null || this.txtGridEdit.Text.Length == 0))
                        {
                            if (dgvCell.RowIndex == 0)
                            {
                                this.dgvInputList[COL_YAMA_NO, dgvCell.RowIndex].Value = "";
                                this.txtGridEdit.Text = "";
                                Msg.Error("入力に誤りがあります。");
                                this.txtGridEdit.Focus();
                                return;
                            }
                            else
                            {
                                this.txtGridEdit.Text = Util.ToString(Util.ToDecimal(this.dgvInputList[COL_YAMA_NO, dgvCell.RowIndex - 1].Value) + 1);
                            }
                        }

                        this.dgvInputList[COL_YAMA_NO, dgvCell.RowIndex].Value = this.txtGridEdit.Text;

                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))

                        // カーソル移動制御
                        {
                            this.dgvInputList.CurrentCell = dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex];

                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }


                        break;
                    case COL_GYOSHU_CD: // 魚種コード

                        // 新規行で上↑移動の場合に行コピーをしない様に
                        if (dgvCell.RowIndex > 0 && e.KeyCode == Keys.Up)
                        {
                            int rows = this.dgvInputList.Rows.Count - 1;
                            if (dgvCell.RowIndex == rows && this.txtGridEdit.Text.Trim().Length == 0)
                            {
                                this.dgvInputList.CurrentCell = this.dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex - 1];
                                break;
                            }
                        }

                        if (!ValChk.IsNumber(txtGridEdit.Text))
                        {
                            this.dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_GYOSHU_NM, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_SHIWAKE_CD, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_JIGYO_KUBUN, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_PAYAO_RITSU, dgvCell.RowIndex].Value = "";

							this.dgvInputList[COL_HONSU, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_KIZY_CD, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_NAKAGAININ_CD, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_NAKAGAININ_NM, dgvCell.RowIndex].Value = "";

							// TODO 2020/02/12 ADD Mutu Asato
							this.dgvInputList[COL_KEIFLG, dgvCell.RowIndex].Value = "";


							Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        if ((dgvCell.Value == null && this.txtGridEdit.Text.Length != 0) ||
                            (dgvCell.Value + "" != this.txtGridEdit.Text))
                        {
                            this.GetGyoshuInfo(1);
                        }
                        else
                        {
                            // 空入力の場合
                            if (dgvCell.RowIndex != 0 && this.txtGridEdit.Text.Trim().Length == 0)
                            {
                                this.txtGridEdit.Text = Util.ToString(this.dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex - 1].Value);
                                this.dgvInputList[COL_TANKA, dgvCell.RowIndex].Value = this.dgvInputList[COL_TANKA, dgvCell.RowIndex - 1].Value;
                                this.GetGyoshuInfo(1);
                            }
                            else if (dgvCell.RowIndex == 0 && this.txtGridEdit.Text.Trim().Length == 0)
                            {
                                // 先頭行で空入力時
                                this.txtGridEdit.Focus();
                                return;
                            }
                            else
                            {
                                // 追加制御
                                //this.dgvInputList.CurrentCell = this.dgvInputList[COL_HONSU, dgvCell.RowIndex];
                                int c = gcc.GetNextCol(dgvCell.ColumnIndex);
                                if (c == -1)
                                    c = COL_HONSU;
                                this.dgvInputList.CurrentCell = this.dgvInputList[c, dgvCell.RowIndex];
                            }
                        }
                        if (!this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                            // カーソル移動制御
                        {
                            this.dgvInputList.CurrentCell = this.dgvInputList[COL_GYOSHU_CD, this.dgvInputList.CurrentCell.RowIndex];

                        }
                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_KIZY_CD, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }


                        break;
                    case COL_GYOSHU_NM:// 3:
                        break;

                    case COL_HONSU: // 本数
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 9, 0, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        if (Util.ToDecimal(txtGridEdit.Text) < 0)
                        {
                            this.txtGridEdit.Text = string.Empty;
                            this.txtGridEdit.Focus();
                            return;
                        }
                        inputData = Util.ToDecimal(txtGridEdit.Text);
                        inputData = TaxUtil.CalcFraction(inputData, 0, 1); // 切り捨て
                        dgvInputList[COL_HONSU, dgvCell.RowIndex].Value = Util.FormatNum(inputData);

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            // 追加制御
                            //dgvInputList.CurrentCell = dgvInputList[COL_KIZY_CD, dgvCell.RowIndex];
                            int c = gcc.GetNextCol(dgvCell.ColumnIndex);
                            if (c == -1)
                                c = COL_KIZY_CD;
                            this.dgvInputList.CurrentCell = this.dgvInputList[c, dgvCell.RowIndex];
                            this.txtGridEdit.SelectAll();

                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_SURYO, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }


                        break;

                    case COL_KIZY_CD:　// 状態

                        if (Util.ToString(this.txtGridEdit.Text).EndsWith(","))
                        {
                            this.txtGridEdit.Text = Util.ToString(this.txtGridEdit.Text).Substring(0, Util.ToString(this.txtGridEdit.Text).Length - 1);
                        }

                        if (!isValidJyoutaiCd(Util.ToString(this.txtGridEdit.Text)))
                        {
                            this.txtGridEdit.Focus();
                            return;
                        }

                        dgvInputList[COL_KIZY_CD, dgvCell.RowIndex].Value = Util.ToString(txtGridEdit.Text);

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            // 追加制御
                            //dgvInputList.CurrentCell = dgvInputList[COL_SURYO, dgvCell.RowIndex];
                            int c = gcc.GetNextCol(dgvCell.ColumnIndex);
                            if (c == -1)
                                c = COL_SURYO;
                            this.dgvInputList.CurrentCell = this.dgvInputList[c, dgvCell.RowIndex];

                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_SURYO, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }


                        break;

                    case COL_SURYO: // 数量
                        this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 9, 2, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        inputData = Util.ToDecimal(txtGridEdit.Text) * 100;
                        inputData = TaxUtil.CalcFraction(inputData, 0, 1) / 100; // 切り捨て
                        dgvInputList[COL_SURYO, dgvCell.RowIndex].Value = Util.FormatNum(inputData, 2);

                        // 金額と消費税計算
                        if (ValChk.IsEmpty(dgvInputList[COL_SURYO, dgvCell.RowIndex].Value) || ValChk.IsEmpty(dgvInputList[COL_TANKA, dgvCell.RowIndex].Value))
                        {
                            dgvInputList[COL_KINGAKU, dgvCell.RowIndex].Value = "";
                            dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex].Value = "";
                        }
                        else
                        {
                            if (!CellKingkSet(dgvCell.RowIndex))
                            {
                                Msg.Error("金額が大きすぎる為、複数明細へ分けて入力を行って下さい。");
                                this.txtGridEdit.Focus();
                                return;
                            }
                        }
 
                        this.DataGridSum();

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_TANKA, dgvCell.RowIndex];

                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_TANKA, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }



                        break;
                    case COL_TANKA: // 単価

                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 9, 0, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        if (Util.ToDecimal(txtGridEdit.Text) < 0)
                        {
                            this.txtGridEdit.Text = string.Empty;
                            this.txtGridEdit.Focus();
                            return;
                        }

                        inputData = Util.ToDecimal(txtGridEdit.Text);
                        inputData = TaxUtil.CalcFraction(inputData, 0, 1); // 切り捨て
                        dgvInputList[COL_TANKA, dgvCell.RowIndex].Value = Util.FormatNum(inputData);

                        // 金額と消費税計算
                        if (ValChk.IsEmpty(dgvInputList[COL_SURYO, dgvCell.RowIndex].Value) || ValChk.IsEmpty(dgvInputList[COL_TANKA, dgvCell.RowIndex].Value))
                        {
                            dgvInputList[COL_KINGAKU, dgvCell.RowIndex].Value = "";
                            dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex].Value = "";
                        }
                        else
                        {
                            if (!CellKingkSet(dgvCell.RowIndex))
                            {
                                Msg.Error("金額が大きすぎる為、複数明細へ分けて入力を行って下さい。");
                                this.txtGridEdit.Focus();
                                return;
                            }
                        }

                        this.DataGridSum();

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            // 追加制御
                            //dgvInputList.CurrentCell = dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex];
                            int c = gcc.GetNextCol(dgvCell.ColumnIndex);
                            if (c == -1)
                                c = COL_PAYAO_NO;
                            this.dgvInputList.CurrentCell = this.dgvInputList[c, dgvCell.RowIndex];

                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }


                        break;
                    case COL_KINGAKU: // 金額
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 15, 0, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }

                        dgvInputList[COL_KINGAKU, dgvCell.RowIndex].Value = Util.FormatNum(txtGridEdit.Text);

                        // 金額と消費税計算
                        if (!ValChk.IsEmpty(dgvInputList[COL_KINGAKU, dgvCell.RowIndex].Value))
                        {
                            if (!ValChk.IsEmpty(dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value) && Util.ToDecimal(this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value).Equals(1))
                            {
                                // 消費税計算
                                CellTaxSet(dgvCell.RowIndex);
                            }
                            else
                                dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex].Value = "";
                        }

                        this.DataGridSum();

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            // 明細転嫁時は消費税へ（現在は金額、消費税は触れない設定）
                            if (Util.ToInt(this.SeriInfo.SHOHIZEI_TENKA_HOHO) == 1)
                                dgvInputList.CurrentCell = dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex]; // 消費税
                            else
                                dgvInputList.CurrentCell = dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex]; // パヤオ
                        }

                        break;
                    case COL_SHOHIZEI: // 消費税
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 15, 0, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }

                        dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex].Value = Util.FormatNum(txtGridEdit.Text);

                        this.DataGridSum();

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex];
                        }

                        break;
                    case COL_PAYAO_NO: // パヤオ
                        if (ValChk.IsEmpty(txtGridEdit.Text) || txtGridEdit.Text == "0")
                        {
                            this.dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex].Value = "";
                            if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                            {
                                dgvInputList.CurrentCell = dgvInputList[COL_NAKAGAININ_CD, dgvCell.RowIndex];
                            }
                        }
                        else if (!ValChk.IsNumber(txtGridEdit.Text))
                        {
                            this.dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex].Value = Regex.Replace(this.txtGridEdit.Text, "\\D", "");
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        else if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 4, 0, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        else
                        {
                            this.GetPayaoInfo(1);

                            this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell);
                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_NAKAGAININ_CD, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }

                        break;
                    case COL_NAKAGAININ_CD: // 仲買人CD

                        if ((dgvCell.Value == null && this.txtGridEdit.Text.Length != 0) ||
                            (dgvCell.Value + "" != this.txtGridEdit.Text))
                        {
                            if (!isValidNakagaiCd(Util.ToString(this.txtGridEdit.Text)))
                            {
                                this.txtGridEdit.Focus();
                                return;
                            }
                            string get_name = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtMizuageShishoCd.Text, this.txtGridEdit.Text);
                            if (get_name == null)
                            {
                                this.dgvInputList[COL_NAKAGAININ_CD, dgvCell.RowIndex].Value = "";
                                this.dgvInputList[COL_NAKAGAININ_NM, dgvCell.RowIndex].Value = string.Empty;
                                this.txtGridEdit.Text = "";
                                Msg.Error("入力に誤りがあります。");
                                this.txtGridEdit.Focus();
                                return;
                            }
                            else
                            {
                                this.dgvInputList[COL_NAKAGAININ_CD, dgvCell.RowIndex].Value = this.txtGridEdit.Text;
                                this.dgvInputList[COL_NAKAGAININ_NM, dgvCell.RowIndex].Value = get_name;
                            }
                        }
                        else
                        {
                            // 空入力の場合
                            if (dgvCell.RowIndex != 0 && this.txtGridEdit.Text.Trim().Length == 0)
                            {
                                this.txtGridEdit.Text = Util.ToString(this.dgvInputList[COL_NAKAGAININ_CD, dgvCell.RowIndex - 1].Value);
                                this.dgvInputList[COL_NAKAGAININ_CD, dgvCell.RowIndex].Value = this.txtGridEdit.Text;
                                this.dgvInputList[COL_NAKAGAININ_NM, dgvCell.RowIndex].Value = this.dgvInputList[COL_NAKAGAININ_NM, dgvCell.RowIndex - 1].Value;
                            }
                            else if (dgvCell.RowIndex == 0 && this.txtGridEdit.Text.Trim().Length == 0)
                            {
                                // 先頭行で空入力時
                                this.txtGridEdit.Focus();
                                return;
                            }
                        }

                        // カーソル移動制御
                        if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
                        {
                            if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                            {
                                dgvInputList.CurrentCell = dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex + 1];
                            }
                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }


                        break;
                }
            }
            else
            {
                // [←キー]が押された
                // if (e.KeyCode == Keys.Left && this.txtGridEdit.SelectionLength == 0 && this.txtGridEdit.SelectionStart == 0)
                if (e.KeyCode == Keys.Left)
                {
                    DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
                    switch (dgvCell.ColumnIndex)
                    {
                        case COL_GYOSHU_CD: // 魚種
                            dgvInputList.CurrentCell = dgvInputList[COL_YAMA_NO, dgvCell.RowIndex];
                            break;
                        case COL_HONSU: // 数量
                            dgvInputList.CurrentCell = dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex];
                            break;
                        case COL_KIZY_CD: // 数量
                            dgvInputList.CurrentCell = dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex];
                            break;
                        case COL_SURYO: // 数量
                            dgvInputList.CurrentCell = dgvInputList[COL_KIZY_CD, dgvCell.RowIndex];
                            break;
                        case COL_TANKA: // 単価
                            dgvInputList.CurrentCell = dgvInputList[COL_SURYO, dgvCell.RowIndex];
                            break;
                        case COL_PAYAO_NO: // パヤオ
                            dgvInputList.CurrentCell = dgvInputList[COL_TANKA, dgvCell.RowIndex];
                            break;
                        case COL_NAKAGAININ_CD: // 仲買人
                            dgvInputList.CurrentCell = dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex];
                            break;
                    }
                        // txtGridEdit_Enterを経由して内容を全選択状態にしている
                        this.txtDummy.Focus();
                    this.txtGridEdit.Focus();
                }
            }
        }

        /// <summary>
        /// データグリッド用のテキストにEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGridEdit_Enter(object sender, EventArgs e)
        {
            BeginInvoke(new EventHandler(TextBoxSelectAll), sender, e);
        }

        /// <summary>
        /// KeyDown時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_KeyDown(object sender, KeyEventArgs e)
        {
            // メニューからのKeyDownは受け付けない
            if (this.ActiveControl != null &&
                this.ActiveControl.GetType().BaseType != null &&
                (this.ActiveControl.GetType().BaseType == typeof(BaseForm)
                || this.ActiveControl.GetType().BaseType == typeof(BasePgForm)))
            {
                return;
            }
            switch (e.KeyCode)
            {
                case Keys.S:
                    if (e.Control)
                    {
                        // カーソル設定画面の起動
                        using (HNSE1014 frm = new HNSE1014())
                        {
                            if (frm.ShowDialog(this) == DialogResult.OK)
                            {
                                this.Config.ReloadConfig();
                            }
                        }
                        return;
                    }
                    break;
            }
        }
        #endregion


        #region privateメソッド
        /// <summary>
        /// 年月日の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            DateTime tmpDate = Util.ConvAdDate(this.txtSeriDays.Gengo, this.txtSeriDays.WYear,
                this.txtSeriDays.Month, "1", this.Dba);


            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeriDays.Day) > lastDayInMonth)
            {
                this.txtSeriDays.Day = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 実際の伝票番号チェック
        /// </summary>
        /// <returns></returns>
        private bool isValidDenpyoBango()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtDenpyoBango.Text, this.txtDenpyoBango.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDenpyoBango.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDenpyoBango.SelectAll();
                //e.Cancel = true;
                return false;
            }

            // 番号変更時のクリア
            if (this.MEISAI_DENPYO_BANGO != 0 && ValChk.IsEmpty(this.txtDenpyoBango.Text))
            {
                this.InitDispOnNew();
                return true;
            }
            else if (ValChk.IsEmpty(this.txtDenpyoBango.Text) && this.MEISAI_DENPYO_BANGO == 0)
            {
                // 空の場合
                return true;
            }

            // 該当データがなければエラーメッセージを表示
            if (!this.existsDenpyoBango())
            {
                Msg.Info("該当データがありません。");
                this.lblMode.Text = "【登録】";

                // 追加モードにする
                this.MODE_EDIT = 1;

                return false;
            }
            return true;
        }

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                //this.txtMizuageShishoCd.Text = "0";
                //this.lblMizuageShishoNm.Text = "全て";
                //return true;

                // 伝票入力時は必須
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 支所の切り替え
            if (Util.ToInt(this.UInfo.ShishoCd) != Util.ToInt(this.txtMizuageShishoCd.Text))
            {
                this.UInfo.ShishoCd = this.txtMizuageShishoCd.Text;
                this.UInfo.ShishoNm = this.lblMizuageShishoNm.Text;
            }

            return true;
        }

        /// <summary>
        /// 船主コードの値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidFunanushiCd()
        {
            // 未入力時
            if (ValChk.IsEmpty(this.txtFunanushiCd.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtFunanushiCd.Text, this.txtFunanushiCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            string inputVal = this.txtFunanushiCd.Text.Trim();
            if (!ValChk.IsNumber(inputVal))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            this.lblFunanushiNm.Text = this.Dba.GetName(this.UInfo, 
				"VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, inputVal);
            if (ValChk.IsEmpty(this.lblFunanushiNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 漁協手数料の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidGyokyouTesuryou()
        {
            if (ValChk.IsEmpty(this.txtGyokyouTesuuryou.Text))
            {
                this.txtGyokyouTesuuryou.Text = "0.00";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtGyokyouTesuuryou.Text, 
				this.txtGyokyouTesuuryou.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            // 数値チェック
            if (!ValChk.IsDecNumWithinLength(this.txtGyokyouTesuuryou.Text, 3, 2))
            {
                // メッセージ表示などのエラー処理を実装
                Msg.Notice("整数部３桁以内、小数部２桁で入力してください。");
                return false;
            }

            // 小数部が３桁以上ならエラー（IsDecNumWithinLength()が小数点の桁数をチェックしないので）
            string[] num = this.txtGyokyouTesuuryou.Text.Split('.');
            if (num.Length > 2 && num[0].Length < 3)
            {
                if (num.Length != 2 || 3 < num[0].Length || 2 < num[1].Length)
                {
                    Msg.Notice("整数部３桁以内、小数部２桁で入力してください。");
                    return false;
                }
            }

            decimal d;
            if (!decimal.TryParse(this.txtGyokyouTesuuryou.Text, out d))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            d = decimal.Parse(this.txtGyokyouTesuuryou.Text);
            // 1000以上の値はエラーにする
            if ((decimal)1000 <= d)
            {
                Msg.Notice("1000未満の値を入力してください。");
                return false;
            }

            // 負の値はエラーにする
            if (d < 0)
            {
                Msg.Notice("正の値を入力してください。");
                return false;
            }

            // 小数点２桁表示
            this.txtGyokyouTesuuryou.Text = d.ToString("f2");

            return true;
        }

        /// <summary>
        /// 漁業種の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidGyogyoushuCd()
        {

            if (ValChk.IsEmpty(this.txtGyogyoushuCd.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                this.lblGyogyoushuNm.Text = "";
                return false;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtGyogyoushuCd.Text, this.txtGyogyoushuCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGyogyoushuCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに表示する
            //this.lblGyogyoushuNm.Text = this.Dba.GetName(this.UInfo, "TM_HN_GYOHO_MST", this.txtGyogyoushuCd.Text);
            DbParamCollection dpc;
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOHO_CD", SqlDbType.Decimal, 13, Util.ToInt(this.txtGyogyoushuCd.Text));
            DataTable dtTB_HN_GYOHO_MST = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_GYOHO_MST",
                "KAISHA_CD = @KAISHA_CD AND GYOHO_CD = @GYOHO_CD ",
                dpc);
            if (dtTB_HN_GYOHO_MST.Rows.Count == 0)
            {
                this.lblGyogyoushuNm.Text = "";
            }
            else
            {
                this.lblGyogyoushuNm.Text = dtTB_HN_GYOHO_MST.Rows[0]["GYOHO_NM"].ToString();
            }

            // 漁業種が取得できない場合はエラーにする
            if (0 == this.lblGyogyoushuNm.Text.Length)
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 荷受人コードの値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidNiukeninCd()
        {
            // 荷受人入力の精算区分の場合
            if (this.txtNiukeninCd.Enabled)
            {
                // 船主の入力が無い場合は船主へ移動へ
                if (ValChk.IsEmpty(this.txtNiukeninCd.Text) && (ValChk.IsEmpty(this.txtFunanushiCd.Text) || INIT_FUNANUSHI_CODE == this.txtFunanushiCd.Text))
                {
                    return true;
                }
                // 未入力不可
                if (ValChk.IsEmpty(this.txtNiukeninCd.Text))
                {
                    Msg.Notice("入力に誤りがあります。");
                    this.lblNiukeninNm.Text = "";
                    this.SeriInfo.NIUKENIN_RITSU = 0;
                    return false;
                }
            }
            else
            {
                // 未入力ＯＫ
                if (ValChk.IsEmpty(this.txtNiukeninCd.Text) || Util.ToInt(this.txtNiukeninCd.Text) == 0)
                {
                    this.txtNiukeninCd.Text = "";
                    this.lblNiukeninNm.Text = "";
                    this.SeriInfo.NIUKENIN_RITSU = 0;
                    return true;
                }
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtNiukeninCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtNiukeninCd.Text, this.txtNiukeninCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // よくわからないが、現行では100000以上を入力すると、99999 になっていた。
            // しかし、SJでは 10000 で落ちるため、9999 にした。
            if (9999 < Util.ToInt(this.txtNiukeninCd.Text))
            {
                this.txtNiukeninCd.Text = "9999";
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            //this.lblNiukeninNm.Text = this.Dba.GetName(this.UInfo, "TM_HN_TESURYO_RITSU_MST_NIUKE", this.txtNiukeninCd.Text); ;
            DbParamCollection dpc;
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, this.getSeisanKubun());
            dpc.SetParam("@URIBA_CD", SqlDbType.Decimal, 4, Util.ToInt(this.txtNiukeninCd.Text));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_TESURYO_RITSU_MST",
                "KAISHA_CD = @KAISHA_CD AND SEISAN_KUBUN = @SEISAN_KUBUN AND URIBA_CD = @URIBA_CD ",
                dpc);
            if (dt.Rows.Count == 0)
            {
                Msg.Notice("入力に誤りがあります。");
                this.lblNiukeninNm.Text = "";
                this.SeriInfo.NIUKENIN_RITSU = 0;
                return false;
            }
            else
            {
                this.txtNiukeninCd.Text = Util.ToString(dt.Rows[0]["URIBA_CD"]);
                this.lblNiukeninNm.Text = Util.ToString(dt.Rows[0]["URIBA_NM"]);
                this.SeriInfo.NIUKENIN_RITSU = Util.ToDecimal(Util.ToString(dt.Rows[0]["TESURYO"]));
            }

            return true;
        }

        /// <summary>
        /// 箱数の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidHakosuu()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtHakosuu.Text, this.txtHakosuu.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtHakosuu.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 山No.の値チェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidYamaNo(string inputData)
        {
            if (ValChk.IsEmpty(inputData))
            {
                Msg.Notice("山Noの入力に誤りがあります。");
                return false;
            }

            // 桁数チェック
            if (5 < inputData.Length)
            {
                Msg.Notice("山Noの入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(inputData))
            {
                Msg.Notice("山Noは数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 魚種CDの値チェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returnstrue=OK, false=NG></returns>
        private bool isValidGyoshuCd(string inputData)
        {
            if (ValChk.IsEmpty(inputData))
            {
                Msg.Notice("魚種CDの入力に誤りがあります。");
                return false;
            }

            // 桁数チェック
            if (5 < inputData.Length)
            {
                Msg.Notice("魚種CDの入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(inputData))
            {
                Msg.Notice("魚種CDは数値のみで入力してください。");
                return false;
            }

            // 存在チェック
            string strWk = "";
            try
            {
                //strWk = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_GYOSHU", inputData);
                DbParamCollection dpc;
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, inputData);
                DataTable dtVI_HN_SHOHIN = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "VI_HN_GYOSHU",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND GYOSHU_CD = @GYOSHU_CD ",
                    dpc);
                if (dtVI_HN_SHOHIN.Rows.Count == 0)
                {
                }
                else
                {
                    strWk = dtVI_HN_SHOHIN.Rows[0]["GYOSHU_NM"].ToString();
                }
            }
            catch (System.ArgumentException)
            {
                strWk = "";
            }
            if (ValChk.IsEmpty(strWk))
            {
                Msg.Notice("魚種CDの入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 水揚本数の値チェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidMizuageHosu(string inputData)
        {
            if (ValChk.IsEmpty(inputData))
            {
                //Msg.Notice("水揚本数の入力に誤りがあります。");
                // 空文字はOKとする
                return true;
            }

            // 数値チェック
            if (!ValChk.IsNumber(inputData))
            {
                Msg.Notice("水揚本数は数値のみで入力してください。");
                return false;
            }

            // 桁数チェック
            if (5 < inputData.Length)
            {
                Msg.Notice("水揚本数の入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            decimal mizuageHonsu = 0;
            if (!decimal.TryParse(inputData, out mizuageHonsu))
            {
                Msg.Notice("水揚本数は数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 水揚数量の値チェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidMizuageSuryo(string inputData)
        {
            if (ValChk.IsEmpty(inputData))
            {
                Msg.Notice("水揚数量の入力に誤りがあります。");
                return false;
            }

            // 桁数チェック
            if (8 < inputData.Replace(".", "").Length)
            {
                Msg.Notice("水揚数量の入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            decimal mizuageSuryo = 0;
            if (!decimal.TryParse(inputData, out mizuageSuryo))
            {
                Msg.Notice("水揚数量は数値のみで入力してください。");
                return false;
            }

            // 最大値チェック
            //if ((decimal)999999.99 < mizuageSuryo)
            if ((decimal)999999.9 < mizuageSuryo)
            {
                Msg.Notice("水揚数量の入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 水揚単価の値チェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidMizuageTanka(string inputData)
        {
            // 桁数チェック
            if (6 < inputData.Replace(".", "").Length)
            {
                Msg.Notice("水揚単価の入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            decimal mizuageTanka = 0;
            if (!decimal.TryParse(inputData, out mizuageTanka))
            {
                Msg.Notice("水揚単価は数値のみで入力してください。");
                return false;
            }

            // 最大値チェック
            if ((decimal)99999.99 < mizuageTanka)
            {
                Msg.Notice("水揚単価の入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 水揚金額の値チェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidMizuageKingaku(string inputData)
        {
            if (ValChk.IsEmpty(inputData))
            {
                Msg.Notice("水揚金額の入力に誤りがあります。");
                return false;
            }

            // 桁数チェック
            if (9 < inputData.Length)
            {
                Msg.Notice("水揚金額の入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(inputData))
            {
                Msg.Notice("水揚金額は数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税額の値チェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidShohizei(string inputData)
        {
            if (ValChk.IsEmpty(inputData))
            {
                Msg.Notice("消費税額の入力に誤りがあります。");
                return false;
            }

            // 桁数チェック
            if (7 < inputData.Length)
            {
                Msg.Notice("消費税額の入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(inputData))
            {
                Msg.Notice("消費税額は数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// パヤオの値チェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidPayao(string inputData)
        {
            if (ValChk.IsEmpty(inputData))
            {
                //Msg.Notice("パヤオの入力に誤りがあります。");
                // 空文字はOKとする
                return true;
            }

            // 桁数チェック
            if (5 < inputData.Length)
            {
                Msg.Notice("パヤオの入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(inputData))
            {
                Msg.Notice("パヤオは数値のみで入力してください。");
                return false;
            }

            // 0はOKにする
            if ("0" == inputData)
            {
                return true;
            }

            // 存在チェック
            String strWk = "";
            try
            {
                //strWk = this.Dba.GetName(this.UInfo, "TM_HN_PAYAO_TESURYO_RITSU", inputData);

                // パヤオマスタからデータを取得して表示
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@PAYAO_CD", SqlDbType.Decimal, 6, inputData);
                StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND PAYAO_CD = @PAYAO_CD");

                string cols = "PAYAO_CD";
                string from = "TB_HN_PAYAO_TESURYO_RITSU";

                DataTable dtGyosyu =
                    this.Dba.GetDataTableByConditionWithParams(cols, from,
                        Util.ToString(where), "PAYAO_CD", dpc);

                if (0 < dtGyosyu.Rows.Count)
                {
                    strWk = inputData;
                }
            }
            catch (System.ArgumentException)
            {
                strWk = "";
            }
            if (ValChk.IsEmpty(strWk))
            {
                Msg.Notice("パヤオの入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仲買CDの値チェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidNakagaiCd(string inputData)
        {
            if (ValChk.IsEmpty(inputData))
            {
                Msg.Notice("仲買CDの入力に誤りがあります。");
                return false;
            }

            // 桁数チェック
            if (6 < inputData.Length)
            {
                Msg.Notice("仲買CDの入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(inputData))
            {
                Msg.Notice("仲買CDは数値のみで入力してください。");
                return false;
            }

            // 存在チェック
            String strWk = "";
            try
            {
                strWk = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtMizuageShishoCd.Text, inputData);
            }
            catch (System.ArgumentException)
            {
                strWk = "";
            }
            if (ValChk.IsEmpty(strWk))
            {
                Msg.Notice("仲買CDの入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 傷CDのチェック処理
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        private bool isValidJyoutaiCd(string inputData)
        {
            if (inputData.Length == 0)
                return true;

            string inpData = inputData;

            // 数値チェック
            if (!ValChk.IsNumber(Util.ToString(inpData).Replace(",", "")))
            {
                Msg.Notice("状態は数値とカンマのみで入力してください。");
                return false;
            }

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND JYOUTAI_CD = @JYOUTAI_CD");

            string cols = "JYOUTAI_CD";
            string from = "TB_HN_JYOUTAI";
            DataTable dt = null;

            string[] kizList = inpData.Split(',');
            foreach (string kiz in kizList)
            {
                try
                {
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    dpc.SetParam("@JYOUTAI_CD", SqlDbType.Decimal, 4, Util.ToDecimal(kiz));
                    dt =
                        this.Dba.GetDataTableByConditionWithParams(cols, from,
                            Util.ToString(where), "JYOUTAI_CD", dpc);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                if (0 == dt.Rows.Count)
                {
                    Msg.Notice("状態CDの入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSetSeisanKubun()
        {
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text))
            {
                this.lblSeisanKbnNm.Text = "";
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtSeisanKbn.Text, this.txtSeisanKbn.MaxLength))
            {
                this.lblSeisanKbnNm.Text = "";
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値のみ入力を許可
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                this.lblSeisanKbnNm.Text = "";
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // マスタチェック
            if (!ValChk.IsEmpty(this.txtSeisanKbn.Text))
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", Util.ToString(txtMizuageShishoCd.Text), this.txtSeisanKbn.Text);
                if (ValChk.IsEmpty(this.lblSeisanKbnNm.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// 全入力項目チェック
        /// </summary>
        /// <param name="modeFlg">追加モード=MODE_NEW , 更新モード=MODE_EDIT</param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidDataAll(int modeFlg)
        {
            // 明細の選択を消す
            if (this.dgvInputList.CurrentCell != null)
                this.dgvInputList.CurrentCell.Selected = false;

            // 日付範囲(年)の入力チェック
            if (!IsValid.IsYear(this.txtYear.Text, this.txtYear.MaxLength))
            {
                this.txtYear.Focus();
                this.txtYear.SelectAll();
                return false;
            }
            // 日付範囲(月)の入力チェック
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                this.txtMonth.Focus();
                this.txtMonth.SelectAll();
                return false;
            }
            // 日付範囲(日)の入力チェック
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                this.txtDay.Focus();
                this.txtDay.SelectAll();
                return false;
            }
            // 日付範囲月末入力チェック処理
            CheckJp();
            // 日付範囲正しい和暦への変換処理
            //SetJp();

            // "水揚支所"の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 精算区分
            if (!this.IsValidSetSeisanKubun())
            {
                this.txtSeisanKbn.Focus();
                this.txtSeisanKbn.SelectAll();
                return false;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidHakosuu())
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtHakosuu.Focus();
                this.txtHakosuu.SelectAll();
                return false;
            }
            //if (modeFlg == MODE_EDIT)
            if (modeFlg == 2)
            {
                // "伝票番号"の入力チェック
                if (!this.isValidDenpyoBango())
                {
                    this.txtDenpyoBango.Focus();
                    this.txtDenpyoBango.SelectAll();
                    return false;
                }
            }

            // "船主コード"の入力チェック
            if (!this.isValidFunanushiCd())
            {
                this.txtFunanushiCd.Focus();
                this.txtFunanushiCd.SelectAll();
                return false;
            }

            // "漁協手数料"の入力チェック
            if (!this.isValidGyokyouTesuryou())
            {
                this.txtGyokyouTesuuryou.Focus();
                this.txtGyokyouTesuuryou.SelectAll();
                return false;
            }

            // "荷受人コード"の入力チェック
            if (!this.isValidNiukeninCd())
            {
                this.txtNiukeninCd.Focus();
                this.txtNiukeninCd.SelectAll();
                return false;
            }

            // "漁業種"の入力チェック
            if (!this.isValidGyogyoushuCd())
            {
                this.txtGyogyoushuCd.Focus();
                this.txtGyogyoushuCd.SelectAll();
                return false;
            }

            bool errFlg = false;
            int rowIndex = 0;
            int rows = this.dgvInputList.Rows.Count;

            // 明細行がない場合は船主CD入力待ちにする
            if (rows == 0)
            {
                Msg.Notice("明細が入力されていません。");
                this.txtFunanushiCd.Focus();
                this.txtFunanushiCd.SelectAll();
                return false;
            }

            if (!ValChk.IsDecNumWithinLength(this.txtTotalMizuageGaku.Text, 9, 0, true))
            {
                Msg.Error("金額が大きすぎる為、伝票を分けて入力を行って下さい。");
                this.txtGridEdit.Focus();
                return false;
            }

            int col = -1;

            // 明細データのチェック
            for (rowIndex = 0; rowIndex < rows; rowIndex++)
            {
                // 山No.以降の手入力値項目が全て未入力の場合を最終行とし、入力チェックを終了する
                //if (ValChk.IsEmpty(this.dgvInputList[COL_GYOSHU_CD, rowIndex].Value)     // 魚種CD
                // && ValChk.IsEmpty(this.dgvInputList[COL_SURYO, rowIndex].Value)         // 水揚数量
                // && ValChk.IsEmpty(this.dgvInputList[COL_TANKA, rowIndex].Value)         // 水揚単価
                // && ValChk.IsEmpty(this.dgvInputList[COL_KINGAKU, rowIndex].Value)       // 水揚金額
                // && ValChk.IsEmpty(this.dgvInputList[COL_SHOHIZEI, rowIndex].Value)      // 消費税
                // && ValChk.IsEmpty(this.dgvInputList[COL_NAKAGAININ_CD, rowIndex].Value) // 仲買CD
                //    )
                if (ValChk.IsEmpty(this.dgvInputList[COL_GYOSHU_CD, rowIndex].Value)     // 魚種CD
                 && ValChk.IsEmpty(this.dgvInputList[COL_SURYO, rowIndex].Value)         // 水揚数量
                 && ValChk.IsEmpty(this.dgvInputList[COL_TANKA, rowIndex].Value)         // 水揚単価
                 && ValChk.IsEmpty(this.dgvInputList[COL_KINGAKU, rowIndex].Value)       // 水揚金額
                 && ValChk.IsEmpty(this.dgvInputList[COL_NAKAGAININ_CD, rowIndex].Value) // 仲買CD
                    )
                {
                    // １行しかないときに山No.以降の手入力値項目が全て未入力の場合はエラー
                    if (rows == 1)
                    {
                        Msg.Notice("明細が入力されていません。");
                        errFlg = true;
                    }

                    break;
                }

                // 入力チェック
                // 山No.
                if (!this.isValidYamaNo(this.removeCommma(Util.ToString(this.dgvInputList[COL_YAMA_NO, rowIndex].Value))))
                {
                    col = COL_YAMA_NO;
                    errFlg = true;
                    break;
                }

                // 魚種CD
                if (!this.isValidGyoshuCd(this.removeCommma(Util.ToString(this.dgvInputList[COL_GYOSHU_CD, rowIndex].Value))))
                {
                    col = COL_GYOSHU_CD;
                    errFlg = true;
                    break;
                }

                // 本数
                if (!this.isValidMizuageHosu(this.removeCommma(Util.ToString(this.dgvInputList[COL_HONSU, rowIndex].Value))))
                {
                    col = COL_HONSU;
                    errFlg = true;
                    break;
                }

                // 水揚数量
                if (!this.isValidMizuageSuryo(this.removeCommma(Util.ToString(this.dgvInputList[COL_SURYO, rowIndex].Value))))
                {
                    col = COL_SURYO;
                    errFlg = true;
                    break;
                }

                // 水揚単価
                if (!this.isValidMizuageTanka(this.removeCommma(Util.ToString(this.dgvInputList[COL_TANKA, rowIndex].Value))))
                {
                    col = COL_TANKA;
                    errFlg = true;
                    break;
                }

                // 水揚金額
                if (!this.isValidMizuageKingaku(this.removeCommma(Util.ToString(this.dgvInputList[COL_KINGAKU, rowIndex].Value))))
                {
                    col = COL_TANKA;// COL_KINGAKU; // カーソルを当てて無い為
                    errFlg = true;
                    break;
                }

                // 消費税
                //if (!this.isValidShohizei(this.removeCommma(Util.ToString(this.dgvInputList[COL_SHOHIZEI, rowIndex].Value))))
                //{
                //    errFlg = true;
                //    break;
                //}
                if (Util.ToInt(this.SeriInfo.SHOHIZEI_TENKA_HOHO) == 1) // 明細転嫁
                {
                    //if (!this.isValidShohizei(this.removeCommma(Util.ToString(this.dgvInputList[7, rowIndex].Value))))
                    if (!this.isValidShohizei(this.removeCommma(Util.ToString(this.dgvInputList[COL_SHOHIZEI, rowIndex].Value))))
                    {
                        col = COL_TANKA;// COL_SHOHIZEI; // カーソルを当てて無い為
                        errFlg = true;
                        break;
                    }
                }

                // パヤオ
                if (!this.isValidPayao(this.removeCommma(Util.ToString(this.dgvInputList[COL_PAYAO_NO, rowIndex].Value))))
                {
                    col = COL_PAYAO_NO;
                    errFlg = true;
                    break;
                }

                // 仲買CD
                if (!this.isValidNakagaiCd(this.removeCommma(Util.ToString(this.dgvInputList[COL_NAKAGAININ_CD, rowIndex].Value))))
                {
                    col = COL_NAKAGAININ_CD;
                    errFlg = true;
                    break;
                }

                // 状態コード
                if (!this.isValidJyoutaiCd(Util.ToString(this.dgvInputList[COL_KIZY_CD, rowIndex].Value)))
                {
                    col = COL_KIZY_CD;
                    errFlg = true;
                    break;
                }
            }

            if (errFlg)
            {
                // エラーの発生した明細行の魚種CDにフォーカスを移動する
                this.dgvInputList.Focus();
                try
                {
                    if (col == -1)
                        this.dgvInputList.CurrentCell = this.dgvInputList[COL_GYOSHU_CD, rowIndex];
                    else
                        this.dgvInputList.CurrentCell = this.dgvInputList[col, rowIndex];
                }
                catch (Exception /*ex*/)
                {
                    this.dgvInputList.CurrentCell = this.dgvInputList[COL_GYOSHU_CD, rowIndex];
                }
                return false;
            }


            return true;
        }


        // TODO 2020-01-28 コメントアウト asato
        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        /// 
        //private void SetJp(string[] arrJpDate)
        //{
        //    this.lblGengo.Text = arrJpDate[0];
        //    this.txtYear.Text = arrJpDate[2];
        //    this.txtMonth.Text = arrJpDate[3];
        //    this.txtDay.Text = arrJpDate[4];

        //    DateTime d = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
        //            this.txtMonth.Text, this.txtDay.Text, this.Dba);
        //    // 新規は無条件、修正時は日付が変わったら行う（日付で税率が変わる対応）
        //    if ((this.MODE_EDIT == 2 && this.SeriInfo.DENPYO_DATE != d.ToString("yyyy/MM/dd")) ||
        //         this.MODE_EDIT == 1)
        //    {
        //        CellTaxRateSet();
        //        this.SeriInfo.DENPYO_DATE = d.ToString("yyyy/MM/dd");
        //    }
        //}

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();


            // ネームスペースの末尾
            //string nameSpace = lowerModuleName.Substring(0, 3);
            string nameSpace = lowerModuleName.Substring(0, 2);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
					frm.StartPosition = FormStartPosition.CenterScreen;

					frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            txtSeriDays.ExecBtn = this.btnF1;
            txtSeriDays.OpenDialogExe = "CMCM1021";
            txtSeriDays.DbConnect = this.Dba;


            // 登録に変更
            this.MODE_EDIT = 1;
            this.lblMode.Text = "【登録】";
            this.MEISAI_DENPYO_BANGO = 0;
            this.SeriInfo.Clear();

            this.seisanKbnCheck = false;

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);

            txtSeriDays.JpDate = jpDate;

            // TODO 2020-01-28 コメントする
            // 伝票日付
            //this.lblGengo.Text = jpDate[0];
            //this.txtYear.Text = jpDate[2];
            //this.txtMonth.Text = jpDate[3];
            //this.txtDay.Text = jpDate[4];

            this.SeriInfo.DENPYO_DATE = DateTime.Now.ToString("yyyy/MM/dd");

            // 伝票番号
            this.txtDenpyoBango.Text = "";
            // 水揚支所
            //this.txtMizuageShishoCd.Text = "1";　// ベースの支所が本所以外の場合は支所区分を触れない様にロックが必要
            ////this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
            //this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }
            // 取り合えず入力、更新系は触れない様に
            txtMizuageShishoCd.Enabled = false;

            // 船主CD、名称
            this.txtFunanushiCd.Text = INIT_FUNANUSHI_CODE;
            this.lblFunanushiNm.Text = "";
            // 漁業手数料 txtGyokyouTesuuryou
            this.txtGyokyouTesuuryou.Text = this.getGyokyoTesuryo(this.getSeisanKubun());
            // 漁業種（コードと名称）
            this.txtGyogyoushuCd.Text = "";
            this.lblGyogyoushuNm.Text = "";
            // 荷受人CD、名称
            this.txtNiukeninCd.Text = "";
            this.lblNiukeninNm.Text = "";
            // 県内の場合荷受人CD入力不可
            //this.txtNiukeninCd.Enabled = false;
            //if (rdoBashoNm01.Checked)
            //    this.txtNiukeninCd.Enabled = false;
            //else
            //    this.txtNiukeninCd.Enabled = true;
            this.txtNiukeninCd.Enabled = this.seisanKbn == KENNAI ? false : true;

            // 箱数
            this.txtHakosuu.Text = "";
            //this.txtHakosuu.ReadOnly = true;
            // 消費税転嫁方法、消費税入力方法をクリア
            this.lblShohizeiInputHoho2.Text = "";
            this.lblShohizeiTenka2.Text = "";

            // 削除ボタン使用不可
            this.btnF3.Enabled = false;

            // フォントを設定する
            //this.dgvInputList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvInputList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 11F);
            this.dgvInputList.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //奇数行を淡い水色にする
            this.dgvInputList.AlternatingRowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#BBFFFF");
            this.dgvInputList.DefaultCellStyle.SelectionBackColor = Color.Transparent;

            
            // 列追加時の確認用
            //foreach (DataGridViewColumn c in this.dgvInputList.Columns)
            //    System.Diagnostics.Debug.Print("Idx:" + c.Index.ToString() + " Name:" + c.Name);

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvInputList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvInputList1.ColumnHeadersDefaultCellStyle.Font = this.txtGridEdit.Font;
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvInputList.DefaultCellStyle.Font = this.txtGridEdit.Font;
            this.dgvInputList.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // ユーザ操作による行追加を無効(禁止)
            this.dgvInputList.AllowUserToAddRows = false;
            // 入力モード設定
            this.dgvInputList.EditMode = DataGridViewEditMode.EditProgrammatically;

            // 行追加(1行目)
            this.dgvInputList.Rows.Clear();
            this.dgvInputList.RowCount = 1;
            this.dgvInputList[COL_GYO_NO, 0].Value = "1";

            // 山No初期値設定
            int yamaNo = this.getMaxYamaNo();
            this.dgvInputList[COL_YAMA_NO, 0].ReadOnly = true;
            this.dgvInputList[COL_YAMA_NO, 0].Value = yamaNo.ToString();
            this.dgvInputList.CurrentCell = this.dgvInputList[COL_YAMA_NO, 0];
            this.dgvInputList.CurrentCell = null;

            // DataGridViewの編集用テキストコントロールを非表示
            this.txtGridEdit.Visible = false;

            this.IS_INPUT = 1;

            // 小計
            this.txtTotalSuuryou.Text = "0";
            // 消費税（外）
            this.txtTotalSyohiZei.Text = "0";
            // 合計額
            this.txtTotalMizuageGaku.Text = "0";
            // ［合計］水揚合計額
            this.txtTotalKingaku.Text = "0";

            // 新規モードにする
            this.lblMode.Text = "【登録】";
            //this.modeFlg = MODE_NEW;
            this.MODE_EDIT = 1;

			this.txtSeisanKbn.Focus();

			// TODO 2020/02/12 Add Mutu Asao
			this.lblUchiwake.Text = string.Empty;

			this.OrgnDialogResult = false;

		}

		/// <summary>
		/// 修正モードの初期表示
		/// </summary>
		/// <param name="dtDENPYO"></param>
		private void InitDispOnEdit(DataTable dtDENPYO)
        {
            // 初期値、入力制御を実装
            DbParamCollection dpc;

            // 修正に変更
            this.MODE_EDIT = 2;
            this.lblMode.Text = "【修正】";
            this.MEISAI_DENPYO_BANGO = Util.ToDecimal(dtDENPYO.Rows[0]["DENPYO_BANGO"]);
            this.SeriInfo.DENPYO_DATE = Util.ToDate(dtDENPYO.Rows[0]["SERIBI"]).ToString("yyyy/MM/dd");

            // 日付範囲の和暦設定
            //this.SetJpDate(Util.ConvJpDate(dtDENPYO.Rows[0]["SERIBI"].ToString(), this.Dba));
            // セリ日付
            DateTime seribi = DateTime.Parse(dtDENPYO.Rows[0]["SERIBI"].ToString());
            // 日付範囲の和暦設定

            // TODO 20200128 追加 asato
            string[] jpDate = Util.ConvJpDate(seribi, this.Dba);


            // TODO 2020-01-28 コメント asato
            // 元号と年月日
            //this.lblGengo.Text = jpDate[0];
            //this.txtYear.Text = jpDate[2];
            //this.txtMonth.Text = jpDate[3];
            //this.txtDay.Text = jpDate[4];

            txtSeriDays.JpDate = jpDate;


            // 精算区分
            this.seisanKbn = Util.ToDecimal(dtDENPYO.Rows[0]["SEISAN_KUBUN"]);
            this.txtSeisanKbn.Text = this.seisanKbn.ToString();
            SetSeisanKubun(this.txtSeisanKbn.Text);

            // 船主
            this.txtFunanushiCd.Text = dtDENPYO.Rows[0]["SENSHU_CD"].ToString();
            this.SetFunanushiInfo();
            this.lblFunanushiNm.Text = dtDENPYO.Rows[0]["SENSHU_NM"].ToString();

            // 水揚支所
            this.txtMizuageShishoCd.Text = dtDENPYO.Rows[0]["MIZUAGE_SHISHO_KUBUN"].ToString();
            this.lblMizuageShishoNm.Text = dtDENPYO.Rows[0]["MIZUAGE_SHISHO_NM"].ToString();

            // 漁協手数料
            this.txtGyokyouTesuuryou.Text = dtDENPYO.Rows[0]["GYOGYO_TESURYO"].ToString();

            // 漁業種
            this.txtGyogyoushuCd.Text = dtDENPYO.Rows[0]["GYOHO_CD"].ToString();
            this.lblGyogyoushuNm.Text = dtDENPYO.Rows[0]["GYOHO_NM"].ToString();

            // 荷受人
            this.txtNiukeninCd.Text = dtDENPYO.Rows[0]["NIUKENIN_CD"].ToString();
            this.lblNiukeninNm.Text = dtDENPYO.Rows[0]["NIUKENIN_NM"].ToString();

            // 箱数
            this.txtHakosuu.Text = dtDENPYO.Rows[0]["HAKOSU"].ToString();

            // 削除ボタン使用可能
            this.btnF3.Enabled = true;

            // ［合計］水揚数量
            this.txtTotalSuuryou.Text = Util.ToDecimal(dtDENPYO.Rows[0]["MIZUAGE_GOKEI_SURYO"]).ToString("n1");

            // ［合計］水揚金額
            this.txtTotalMizuageGaku.Text = Util.FormatNum(Util.ToDecimal(dtDENPYO.Rows[0]["MIZUAGE_ZEINUKI_KINGAKU"]));

            // ［合計］消費税（外）
            this.txtTotalSyohiZei.Text = Util.FormatNum(Util.ToDecimal(dtDENPYO.Rows[0]["MIZUAGE_SHOHIZEIGAKU"]));

            // ［合計］水揚合計額
            this.txtTotalKingaku.Text = Util.FormatNum(Util.ToDecimal(dtDENPYO.Rows[0]["MIZUAGE_ZEIKOMI_KINGAKU"]));


            // DataGridViewの編集用テキストコントロールを非表示
            this.txtGridEdit.Visible = false;

            // グリッドをクリア
            //            this.dgvInputList.Rows.Clear();
            this.dgvInputList.Rows.Clear();

            // 伝票明細取得してグリッドに設定
            // Han.TB_仕切明細(TB_HN_SHIKIRI_MEISAI)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            // dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 10, DUMMY_DENPYO_KUBUN); // 保存するときには伝票区分=3を設定しているが、検索するときは使わない
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(dtDENPYO.Rows[0]["DENPYO_BANGO"]));

            DataTable dtKiz = null;
            string kizList = string.Empty;
            DbParamCollection dpcKiz;

            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                " M.GYO_NO " +
                ",M.YAMA_NO " +
                ",M.GYOSHU_CD " +
                ",M.GYOSHU_NM " +
                ",M.SURYO " +
                ",M.HONSU " +
                ",M.TANKA " +
                ",M.KINGAKU " +
                ",M.SHOHIZEI " +
                ",M.PAYAO_NO " +
                ",M.PAYAO_RITSU " +
                ",M.NAKAGAININ_CD " +
                ",M.NAKAGAININ_NM " +
                ",M.SHIWAKE_CD " +
                ",M.BUMON_CD " +
                ",M.ZEI_KUBUN " +
                ",M.JIGYO_KUBUN " +
                ",M.ZEI_RITSU " +
                ",M.SHISHO_CD " +
                ",M.SHOHIZEI_NYURYOKU_HOHO " +
				",Z.KAZEI_KUBUN " +
				",(CASE WHEN M.ZEI_KUBUN = 50 THEN '*' ELSE '' END) KEIGEN_FLG " ,
				"VI_HN_SHIKIRI_MEISAI AS M " +
                "LEFT JOIN TB_ZM_F_ZEI_KUBUN AS Z " +
                    "ON M.ZEI_KUBUN = Z.ZEI_KUBUN ",
                "M.KAISHA_CD = @KAISHA_CD AND M.SHISHO_CD = @SHISHO_CD AND M.KAIKEI_NENDO = @KAIKEI_NENDO AND M.DENPYO_BANGO = @DENPYO_BANGO",
                "M.GYO_NO",
                dpc);

            if (dt.Rows.Count > 0)
            {
                // 明細に落ちている方を優先
                this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO = Util.ToDecimal(dt.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"]);
                if (this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO == 3)
                    this.lblSyohiZei.Text = "消費税（内）";
                else
                    this.lblSyohiZei.Text = "消費税（外）";

                this.dgvInputList.RowCount = dt.Rows.Count + 1;
                int i = 0;
                decimal lastYamaNo = 0;

                #region 削除する
                while (dt.Rows.Count > i)
                {
                    this.dgvInputList[COL_GYO_NO, i].Value = Util.ToString(dt.Rows[i]["GYO_NO"]);
                    this.dgvInputList[COL_YAMA_NO, i].Value = Util.ToString(dt.Rows[i]["YAMA_NO"]);
                    this.dgvInputList[COL_GYOSHU_CD, i].Value = Util.ToString(dt.Rows[i]["GYOSHU_CD"]);
                    this.dgvInputList[COL_GYOSHU_NM, i].Value = Util.ToString(dt.Rows[i]["GYOSHU_NM"]);
                    this.dgvInputList[COL_HONSU, i].Value = Util.FormatNum(dt.Rows[i]["HONSU"]);
                    this.dgvInputList[COL_SURYO, i].Value = Util.FormatNum(dt.Rows[i]["SURYO"], 2);
                    this.dgvInputList[COL_TANKA, i].Value = Util.FormatNum(dt.Rows[i]["TANKA"]);
                    this.dgvInputList[COL_KINGAKU, i].Value = Util.FormatNum(dt.Rows[i]["KINGAKU"]);
                    this.dgvInputList[COL_SHOHIZEI, i].Value = Util.FormatNum(dt.Rows[i]["SHOHIZEI"]);
                    this.dgvInputList[COL_PAYAO_NO, i].Value = Util.ToString(dt.Rows[i]["PAYAO_NO"]);
                    this.dgvInputList[COL_NAKAGAININ_CD, i].Value = Util.ToString(dt.Rows[i]["NAKAGAININ_CD"]);
                    this.dgvInputList[COL_NAKAGAININ_NM, i].Value = Util.ToString(dt.Rows[i]["NAKAGAININ_NM"]);

                    this.dgvInputList[COL_SHIWAKE_CD, i].Value = Util.ToString(dt.Rows[i]["SHIWAKE_CD"]);
                    this.dgvInputList[COL_BUMON_CD, i].Value = Util.ToString(dt.Rows[i]["BUMON_CD"]);
                    this.dgvInputList[COL_ZEI_KUBUN, i].Value = Util.ToDecimal(Util.ToString(dt.Rows[i]["ZEI_KUBUN"]));
                    this.dgvInputList[COL_JIGYO_KUBUN, i].Value = Util.ToString(dt.Rows[i]["JIGYO_KUBUN"]);
                    this.dgvInputList[COL_ZEI_RITSU, i].Value = Util.ToDecimal(Util.ToString(dt.Rows[i]["ZEI_RITSU"]));
                    this.dgvInputList[COL_KAZEI_KUBUN, i].Value = Util.ToString(dt.Rows[i]["KAZEI_KUBUN"]);
                    this.dgvInputList[COL_PAYAO_RITSU, i].Value = Util.ToString(dt.Rows[i]["PAYAO_RITSU"]);

					this.dgvInputList[COL_KEIFLG, i].Value = Util.ToString(dt.Rows[i]["KEIGEN_FLG"]);

					// 消費税が発生しない場合はクリア
					if (Util.ToInt(this.SeriInfo.SHOHIZEI_TENKA_HOHO) != 1)
                    {
                        this.dgvInputList[COL_SHOHIZEI, i].Value = 0;
                    }
                    if (Util.ToInt(this.dgvInputList[COL_ZEI_KUBUN, i].Value) == 0)
                    {
                        this.dgvInputList[COL_SHOHIZEI, i].Value = 0;
                    }
                    // 非課税の場合は税情報クリア
                    //if (Util.ToInt(this.dgvInputList[COL_KAZEI_KUBUN, i].Value) == 0)
                    if (Util.ToInt(this.dgvInputList[COL_KAZEI_KUBUN, i].Value) != 1)
                    {
                        this.dgvInputList[COL_SHOHIZEI, i].Value = 0;
                        this.dgvInputList[COL_ZEI_RITSU, i].Value = string.Empty;

						//TODO 2020/02/22 Add Mutu Asato
						this.dgvInputList[COL_KEIFLG, i].Value = string.Empty;
					}

					// 最終表示山No用
					lastYamaNo = Util.ToDecimal(this.dgvInputList[COL_YAMA_NO, i].Value);

                    // 状態列の表示
                    dpcKiz = new DbParamCollection();
                    dpcKiz.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
                    dpcKiz.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    dpcKiz.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    // dpcKiz.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 10, DUMMY_DENPYO_KUBUN); // 保存するときには伝票区分=3を設定しているが、検索するときは使わない
                    dpcKiz.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(dtDENPYO.Rows[0]["DENPYO_BANGO"]));
                    dpcKiz.SetParam("@GYO_NO", SqlDbType.Int, 4, Util.ToInt(dt.Rows[i]["GYO_NO"]));
                    dtKiz = this.Dba.GetDataTableByConditionWithParams(
                        "*",
                        "TB_HN_SHIKIRI_MEISAI_JYOUTAI",
                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_BANGO = @DENPYO_BANGO AND GYO_NO = @GYO_NO",
                        "SORT_NO",
                        dpcKiz);
                    if (dtKiz.Rows.Count > 0)
                    {
                        kizList = string.Empty;
                        foreach (DataRow r in dtKiz.Rows)
                        {
                            kizList += Util.ToString(r["JYOUTAI_CD"]) + ",";
                        }
                        if (kizList.EndsWith(","))
                        {
                            kizList = kizList.Substring(0, kizList.Length - 1);
                        }
                        this.dgvInputList[COL_KIZY_CD, i].Value = Util.ToString(kizList);
                    }

                    i++;
                }
                #endregion

                // 行追加(最後の行目)
                this.dgvInputList[COL_GYO_NO, this.dgvInputList.Rows.Count - 1].Value = this.dgvInputList.Rows.Count;
                // 山NOも
                this.dgvInputList[COL_YAMA_NO, this.dgvInputList.RowCount - 1].Value = lastYamaNo + 1;

				// TODO 2020/02/12 Add Mutu Asato
				DataGridSum();
			}

		}

		/// <summary>
		/// 削除処理
		/// </summary>
		private void DeleteData()
        {

            Decimal DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoBango.Text);
            int status = 0;

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // +----------------+
                // | 仕切データ削除 |
                // +----------------+
                // 仕切データ削除SQL
                StringBuilder sql = new StringBuilder();
                sql.Append("DELETE FROM TB_HN_SHIKIRI_DATA ");
                sql.Append("WHERE ");
                sql.Append("      KAISHA_CD = @KAISHA_CD");
                sql.Append("  AND SHISHO_CD = @SHISHO_CD");
                sql.Append("  AND DENPYO_KUBUN = @DENPYO_KUBUN");
                sql.Append("  AND DENPYO_BANGO = @DENPYO_BANGO");
                sql.Append("  AND KAIKEI_NENDO = @KAIKEI_NENDO");

                // 仕切データ削除用パラメータ
                DbParamCollection delParam = new DbParamCollection();
                delParam.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);       // 会社コード（1固定）
                delParam.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                delParam.SetParam("@DENPYO_KUBUN", SqlDbType.Int, 4, DUMMY_DENPYO_KUBUN);     // 伝票区分（3固定）
                delParam.SetParam("@DENPYO_BANGO", SqlDbType.Int, 8, DENPYO_BANGO);           // 伝票番号
                delParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo); // 会計年度

                // 仕切データ削除実行
                status = this.Dba.ModifyBySql(sql.ToString(), delParam);

                // +--------------+
                // | 仕切明細削除 |
                // +--------------+
                // 仕切明細削除SQL
                sql = new StringBuilder();
                sql.Append("DELETE FROM TB_HN_SHIKIRI_MEISAI ");
                sql.Append("WHERE ");
                sql.Append("      KAISHA_CD = @KAISHA_CD");
                sql.Append("  AND SHISHO_CD = @SHISHO_CD");
                sql.Append("  AND DENPYO_KUBUN = @DENPYO_KUBUN");
                sql.Append("  AND DENPYO_BANGO = @DENPYO_BANGO");
                sql.Append("  AND KAIKEI_NENDO = @KAIKEI_NENDO");
                sql.Append("  AND GYO_NO >= 0"); // 全行削除する

                // 仕切明細削除用パラメータは仕切データと同じなのでそのままにする

                // 仕切細削削除実行
                status = this.Dba.ModifyBySql(sql.ToString(), delParam);

                // 仕切明細状態削除SQL
                sql = new StringBuilder();
                sql.Append("DELETE FROM TB_HN_SHIKIRI_MEISAI_JYOUTAI ");
                sql.Append("WHERE ");
                sql.Append("      KAISHA_CD = @KAISHA_CD");
                sql.Append("  AND SHISHO_CD = @SHISHO_CD");
                sql.Append("  AND DENPYO_KUBUN = @DENPYO_KUBUN");
                sql.Append("  AND DENPYO_BANGO = @DENPYO_BANGO");
                sql.Append("  AND KAIKEI_NENDO = @KAIKEI_NENDO");
                sql.Append("  AND GYO_NO >= 0"); // 全行削除する

                // 仕切明細状態削除実行
                status = this.Dba.ModifyBySql(sql.ToString(), delParam);

                // トランザクションをコミット
                this.Dba.Commit();

                // 削除完了メッセージ（現行には無いけど、富山課長リクエストにより追加した。）
                Msg.Info(DENPYO_BANGO.ToString().PadLeft(6) + "番の伝票を削除しました");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // 削除した伝票番号を表示する
            this.lblZenkaiDenpyoBango.Text = "【前回削除伝票伝票番号：" + DENPYO_BANGO.ToString().PadLeft(6) + "】";
        }

        /// <summary>
        /// 登録処理
        /// </summary>
        /// <returns>登録した伝票番号を返す</returns>
        private Decimal UpdateData()
        {
            DbParamCollection dpc;
            DataTable dtCheck;
            DbParamCollection updParam;
            DbParamCollection whereParam;

            DateTime DENPYO_DATE = Util.ConvAdDate(this.txtSeriDays.Gengo, this.txtSeriDays.WYear,
                    this.txtSeriDays.Month, this.txtSeriDays.Day, this.Dba);

            Decimal DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoBango.Text);

            List<MeisaiTbl> taxTbl = new List<MeisaiTbl>();
            TaxSorting(ref taxTbl);

            try
            {

                // トランザクション開始
                this.Dba.BeginTransaction();

                // 伝票番号確定処理

                // Han.TB_仕切伝票(TB_HN_SHIKIRI_DATA)
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, DUMMY_DENPYO_KUBUN);
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                dtCheck = this.Dba.GetDataTableByConditionWithParams(
                    "COUNT(*) AS CNT",
                    "TB_HN_SHIKIRI_DATA",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    dpc);
                if (Util.ToLong(dtCheck.Rows[0]["CNT"]) == 0)
                {
                    DENPYO_BANGO = Util.ToDecimal(this.Dba.GetHNDenpyoNo(this.UInfo, Util.ToDecimal(Util.ToString(txtMizuageShishoCd.Text)), DUMMY_DENPYO_KUBUN, 0));
                    if (this.Dba.UpdateHNDenpyoNo(this.UInfo, Util.ToDecimal(Util.ToString(txtMizuageShishoCd.Text)), DUMMY_DENPYO_KUBUN, 0, Util.ToInt(DENPYO_BANGO)) != 1)
                    {
                        //
                        // エラーの場合
                        Msg.Error("伝票番号の発番に失敗しました。");
                        this.txtDenpyoBango.SelectAll();

                        // DialogResultに「OK」をセットし結果を返却
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                        return 0;
                    }
                }

                // 伝票番号が0ならば1にする
                if (DENPYO_BANGO == 0) DENPYO_BANGO = 1;

                // 伝票明細削除
                // Han.TB_仕切明細(TB_HN_SHIKIRI_MEISAI)
                whereParam = new DbParamCollection();
                
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, DUMMY_DENPYO_KUBUN);
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                this.Dba.Delete("TB_HN_SHIKIRI_MEISAI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    whereParam);

                // 仕切明細状態削除
                DbParamCollection updParamKiz;
                this.Dba.Delete("TB_HN_SHIKIRI_MEISAI_JYOUTAI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    whereParam);

                // 伝票の更新
                if (Util.ToLong(dtCheck.Rows[0]["CNT"]) == 0)
                {
                    // 伝票登録
                    updParam = new DbParamCollection();

                    updParam.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                    updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, DUMMY_DENPYO_KUBUN);
                    updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                    updParam.SetParam("@SERIBI", SqlDbType.DateTime, DENPYO_DATE);
                    updParam.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 3, 1); // （1固定）
                    updParam.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 3, 1); // （1固定）
                    updParam.SetParam("@MIZUAGE_SHISHO_KUBUN", SqlDbType.Int, 3, Util.ToInt(this.txtMizuageShishoCd.Text));
                    updParam.SetParam("@MIZUAGE_SHISHO_NM", SqlDbType.VarChar, 40, this.lblMizuageShishoNm.Text);

                    updParam.SetParam("@SENSHU_CD", SqlDbType.Int, 7, this.txtFunanushiCd.Text);
                    updParam.SetParam("@SENSHU_NM", SqlDbType.VarChar, 40, this.lblFunanushiNm.Text);

                    updParam.SetParam("@GYOGYO_TESURYO", SqlDbType.Decimal, 7, this.txtGyokyouTesuuryou.Text);
                    updParam.SetParam("@GYOHO_CD", SqlDbType.Int, 7, Util.ToInt(this.txtGyogyoushuCd.Text));
                    updParam.SetParam("@GYOHO_NM", SqlDbType.VarChar, 40, this.lblGyogyoushuNm.Text);
                    updParam.SetParam("@NIUKENIN_CD", SqlDbType.Int, 7, Util.ToInt(this.txtNiukeninCd.Text));
                    updParam.SetParam("@NIUKENIN_NM", SqlDbType.VarChar, 40, Util.ToString(this.lblNiukeninNm.Text));
                    updParam.SetParam("@SEISAN_KUBUN", SqlDbType.Int, 3, this.getSeisanKubun());
                    updParam.SetParam("@SEISAN_NO", SqlDbType.Int, 10, 0);

                    //updParam.SetParam("@NIUKENIN_RITSU", SqlDbType.Decimal, 7, 0.00); // 荷受人から
                    //if (this.rdoBashoNm01.Checked)
                    if (this.getSeisanKubun() == KENNAI)
                    {
                        updParam.SetParam("@NIUKENIN_RITSU", SqlDbType.Decimal, 7, 0.00);
                    }
                    else
                    {
                        updParam.SetParam("@NIUKENIN_RITSU", SqlDbType.Decimal, 7, this.SeriInfo.NIUKENIN_RITSU); // 荷受人から
                    }

                    updParam.SetParam("@MIZUAGE_GOKEI_SURYO", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalSuuryou.Text));
                    updParam.SetParam("@MIZUAGE_ZEINUKI_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalMizuageGaku.Text));
                    updParam.SetParam("@MIZUAGE_SHOHIZEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalSyohiZei.Text));
                    updParam.SetParam("@MIZUAGE_ZEIKOMI_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalKingaku.Text));
                    updParam.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Int, 8, 0);
                    updParam.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Int, 8, 0);
                    //updParam.SetParam("@KEIJO_NENGAPPI", SqlDbType.DateTime, 16, this.today);
                    //updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);
                    updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
                    updParam.SetParam("@SHORI_FLG", SqlDbType.Int, 3, 0);
                    if (ValChk.IsEmpty(this.txtHakosuu.Text) || !ValChk.IsNumber(this.txtHakosuu.Text))
                        updParam.SetParam("@HAKOSU", SqlDbType.Int, 4, DBNull.Value);
                    else
                        updParam.SetParam("@HAKOSU", SqlDbType.Int, 4, this.txtHakosuu.Text);

                    this.Dba.Insert("TB_HN_SHIKIRI_DATA", updParam);
                }
                else
                {
                    // 伝票更新
                    updParam = new DbParamCollection();

                    updParam.SetParam("@SERIBI", SqlDbType.DateTime, DENPYO_DATE);
                    //updParam.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 3, 1); // （1固定）
                    //updParam.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 3, 1); // （1固定）
                    updParam.SetParam("@MIZUAGE_SHISHO_KUBUN", SqlDbType.Int, 3, Util.ToInt(this.txtMizuageShishoCd.Text));
                    updParam.SetParam("@MIZUAGE_SHISHO_NM", SqlDbType.VarChar, 40, this.lblMizuageShishoNm.Text);

                    updParam.SetParam("@SENSHU_CD", SqlDbType.Int, 7, this.txtFunanushiCd.Text);
                    updParam.SetParam("@SENSHU_NM", SqlDbType.VarChar, 40, this.lblFunanushiNm.Text);

                    updParam.SetParam("@GYOGYO_TESURYO", SqlDbType.Decimal, 7, this.txtGyokyouTesuuryou.Text);
                    updParam.SetParam("@GYOHO_CD", SqlDbType.Int, 7, Util.ToInt(this.txtGyogyoushuCd.Text));
                    updParam.SetParam("@GYOHO_NM", SqlDbType.VarChar, 40, this.lblGyogyoushuNm.Text);
                    updParam.SetParam("@NIUKENIN_CD", SqlDbType.Int, 7, Util.ToInt(this.txtNiukeninCd.Text));
                    updParam.SetParam("@NIUKENIN_NM", SqlDbType.VarChar, 40, Util.ToString(this.lblNiukeninNm.Text));
                    updParam.SetParam("@SEISAN_KUBUN", SqlDbType.Int, 3, this.getSeisanKubun());
                    //updParam.SetParam("@SEISAN_NO", SqlDbType.Int, 10, 0);

                    //updParam.SetParam("@NIUKENIN_RITSU", SqlDbType.Decimal, 7, 0.00); // 荷受人から
                    //if (this.rdoBashoNm01.Checked)
                    if (this.getSeisanKubun() == KENNAI)
                    {
                        updParam.SetParam("@NIUKENIN_RITSU", SqlDbType.Decimal, 7, 0.00);
                    }
                    else
                    {
                        updParam.SetParam("@NIUKENIN_RITSU", SqlDbType.Decimal, 7, this.SeriInfo.NIUKENIN_RITSU); // 荷受人から
                    }

                    updParam.SetParam("@MIZUAGE_GOKEI_SURYO", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalSuuryou.Text));
                    updParam.SetParam("@MIZUAGE_ZEINUKI_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalMizuageGaku.Text));
                    updParam.SetParam("@MIZUAGE_SHOHIZEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalSyohiZei.Text));
                    updParam.SetParam("@MIZUAGE_ZEIKOMI_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalKingaku.Text));
                    //updParam.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Int, 8, 0);
                    //updParam.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Int, 8, 0);
                    //updParam.SetParam("@KEIJO_NENGAPPI", SqlDbType.DateTime, 16, this.today);
                    //updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Now.Date);
                    updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
                    updParam.SetParam("@SHORI_FLG", SqlDbType.Int, 3, 1);
                    if (ValChk.IsEmpty(this.txtHakosuu.Text) || !ValChk.IsNumber(this.txtHakosuu.Text))
                        updParam.SetParam("@HAKOSU", SqlDbType.Int, 4, DBNull.Value);
                    else
                        updParam.SetParam("@HAKOSU", SqlDbType.Int, 4, this.txtHakosuu.Text);

                    this.Dba.Update("TB_HN_SHIKIRI_DATA",
                                    updParam,
                                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                                    whereParam
                                    );
                }

                // 伝票明細(DataGridView[name=dgvInputList]を取得)
                int i = 0;
                while (this.dgvInputList.RowCount - 1 > i)
                {
                    // 伝票明細登録
                    updParam = new DbParamCollection();
                    
                    updParam.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                    updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, DUMMY_DENPYO_KUBUN);
                    updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);

                    //updParam.SetParam("@GYO_NO", SqlDbType.Int, 6, Util.ToDecimal(this.dgvInputList[0, i].Value));
                    updParam.SetParam("@GYO_NO", SqlDbType.Int, 6, (i + 1));

                    updParam.SetParam("@YAMA_NO", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_YAMA_NO, i].Value)));
                    updParam.SetParam("@GYOSHU_CD", SqlDbType.Int, 7, Util.ToInt(Util.ToString(this.dgvInputList[COL_GYOSHU_CD, i].Value)));
                    updParam.SetParam("@GYOSHU_NM", SqlDbType.VarChar, 40, Util.ToString(this.dgvInputList[COL_GYOSHU_NM, i].Value));
                    updParam.SetParam("@HONSU", SqlDbType.Decimal, 11, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_HONSU, i].Value)));
                    updParam.SetParam("@SURYO", SqlDbType.Decimal, 11, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_SURYO, i].Value)));
                    updParam.SetParam("@TANKA", SqlDbType.Decimal, 11, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_TANKA, i].Value)));
                    updParam.SetParam("@KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_KINGAKU, i].Value)));
                    updParam.SetParam("@SHOHIZEI", SqlDbType.Decimal, 11, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_SHOHIZEI, i].Value)));
                    updParam.SetParam("@PAYAO_NO", SqlDbType.Int, 7, Util.ToInt(Util.ToString(this.dgvInputList[COL_PAYAO_NO, i].Value)));
                    updParam.SetParam("@NAKAGAININ_CD", SqlDbType.Int, 7, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_NAKAGAININ_CD, i].Value)));
                    updParam.SetParam("@NAKAGAININ_NM", SqlDbType.VarChar, 40, Util.ToString(this.dgvInputList[COL_NAKAGAININ_NM, i].Value));

                    updParam.SetParam("@SHIWAKE_CD", SqlDbType.Int, 7, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_SHIWAKE_CD, i].Value)));
                    updParam.SetParam("@BUMON_CD", SqlDbType.Int, 7, Util.ToInt(Util.ToString(this.dgvInputList[COL_BUMON_CD, i].Value)));
                    updParam.SetParam("@ZEI_KUBUN", SqlDbType.Int, 7, Util.ToInt(Util.ToString(this.dgvInputList[COL_ZEI_KUBUN, i].Value)));
                    updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Int, 7, Util.ToInt(Util.ToString(this.dgvInputList[COL_JIGYO_KUBUN, i].Value)));
                    updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_ZEI_RITSU, i].Value)));
                    updParam.SetParam("@PAYAO_RITSU", SqlDbType.Decimal, 4, 2, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_PAYAO_RITSU, i].Value)));

                    updParam.SetParam("@SHOHIZEI_FURIWAKE", SqlDbType.Decimal, 14, taxTbl[i].Zeigk);
                    updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 3, this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO);

                    //updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);
                    updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
                    updParam.SetParam("@SHORI_FLG", SqlDbType.Int, 3, 0);

                    this.Dba.Insert("TB_HN_SHIKIRI_MEISAI", updParam);

                    // 仕切明細状態登録
                    string inData = Util.ToString(this.dgvInputList[COL_KIZY_CD, i].Value);
                    if (Util.ToString(inData).EndsWith(","))
                    {
                        inData = Util.ToString(inData).Substring(0, Util.ToString(inData).Length - 1);
                    }
                    if (!ValChk.IsEmpty(inData))
                    {
                        int kizSort = 1;
                        string[] kizList = inData.Split(',');
                        foreach (string kiz in kizList)
                        {
                            updParamKiz = new DbParamCollection();
                            updParamKiz.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
                            updParamKiz.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                            updParamKiz.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                            updParamKiz.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, DUMMY_DENPYO_KUBUN);
                            updParamKiz.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                            updParamKiz.SetParam("@GYO_NO", SqlDbType.Int, 6, (i + 1));

                            updParamKiz.SetParam("@JYOUTAI_CD", SqlDbType.Int, 6, Util.ToInt(kiz));
                            updParamKiz.SetParam("@SORT_NO", SqlDbType.Int, 6, kizSort);
                            updParamKiz.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);

                            this.Dba.Insert("TB_HN_SHIKIRI_MEISAI_JYOUTAI", updParamKiz);
                            kizSort++;
                        }
                    }

                    i++;
                }

                // トランザクションをコミット
                this.Dba.Commit();

            }
            catch (Exception e)
            {
                // ロールバック
                this.Dba.Rollback();
                Msg.Error(e.Message);
            }
            finally
            {
                // 処理なし
                ;
            }

			this.OrgnDialogResult = false;

			// 保存した伝票番号を表示する
			this.lblZenkaiDenpyoBango.Text = "【前回" + (this.MODE_EDIT == 1 ? "登録" : "更新") + "伝票番号：" + DENPYO_BANGO.ToString().PadLeft(6) + "】";

            return DENPYO_BANGO;
        }

        /// <summary>
        /// 伝票データチェック後、伝票修正処理へ
        /// </summary>
        /// <returns>イベントをキャンセルする真偽値(真:キャンセル,偽:続行)</returns>
        private bool GetDenpyoData()
        {
            DbParamCollection dpc;
            DataTable dt;
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            // dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 10, DUMMY_DENPYO_KUBUN); // 保存するときには伝票区分=3を設定しているが、検索するときは使わない
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, this.txtDenpyoBango.Text);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo); // 伝票番号が重複するので会計年度追加
            dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_SHIKIRI_DATA",
                "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_BANGO = @DENPYO_BANGO",
                dpc);

            if (dt.Rows.Count == 0)
            {
                Msg.Error("入力に誤りがあります。");
                this.txtDenpyoBango.SelectAll();
                return true;
            }

            // 既に修正モードへ切り替え時には処理しない
            if (this.MEISAI_DENPYO_BANGO != Util.ToDecimal(this.txtDenpyoBango.Text))
            {
                this.MEISAI_DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoBango.Text);
                // 修正モードへ切り替え
                this.InitDispOnEdit(dt);
            }
            return false;
        }

        /// <summary>
        /// カンマを削除した文字列をを返す
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private String removeCommma(String value)
        {
            if (ValChk.IsEmpty(value))
            {
                return "";
            }

            String wk = value.Replace(",", "");

            return wk;
        }

        /// <summary>
        /// 明細行をクリアする
        /// </summary>
        private void MeisaiClear()
        {
            // 行追加(1行目)
            this.dgvInputList.Rows.Clear();
            this.dgvInputList.RowCount = 1;
            //this.dgvInputList.Rows.Add();
            this.dgvInputList[COL_GYO_NO, 0].Value = "1";

            // 山No初期値設定
            int yamaNo = this.getMaxYamaNo();
            this.dgvInputList[COL_YAMA_NO, 0].ReadOnly = true;
            this.dgvInputList[COL_YAMA_NO, 0].Value = yamaNo.ToString();

            // DataGridViewの編集用テキストコントロールを非表示
            this.txtGridEdit.Visible = false;

            // 小計
            //this.txtTotalSuuryou.Text = "0";
            this.txtTotalSuuryou.Text = "0.0";
            // 消費税（外）
            this.txtTotalSyohiZei.Text = "0";
            // 合計額
            this.txtTotalMizuageGaku.Text = "0";
            // ［合計］水揚合計額
            this.txtTotalKingaku.Text = "0";
        }

        /// <summary>
        /// 最大 山No 取得
        /// </summary>
        /// <returns></returns>
        private int getMaxYamaNo()
        {
            DataTable vi_hn_shikiriMeisai = new DataTable(); // 明細データ
            DbParamCollection dpc = new DbParamCollection();

            // セリ日
            //DateTime seribi = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
            //this.txtMonth.Text, this.txtDay.Text, this.Dba);

            DateTime seribi = Util.ConvAdDate(this.txtSeriDays.Gengo, this.txtSeriDays.WYear,
            this.txtSeriDays.Month, this.txtSeriDays.Day, this.Dba);



            // 本日日付と会社コード取得
            dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo); // 会計年度追加
            // dpc.SetParam("@TODAY", SqlDbType.DateTime, 20, this.today);
            dpc.SetParam("@TODAY", SqlDbType.DateTime, 20, seribi);

            // SQL　※あえて伝票区分を条件から外してある
            string select = "MAX(YAMA_NO) AS YAMA_NO ";
            string from = "VI_HN_SHIKIRI_MEISAI ";
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD ");
            where.Append(" AND SHISHO_CD = @SHISHO_CD ");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append(" AND SERIBI = @TODAY ");

            // データセット設定（１行目）
            DataSet dataset = new DataSet();
            DataTable dt = new DataTable();

            // 列の設定
            String colName = "YAMA_NO";
            vi_hn_shikiriMeisai.Columns.Add(colName);
            dataset.Tables.Add(vi_hn_shikiriMeisai);

            // DBからデータ取得
            vi_hn_shikiriMeisai =
                this.Dba.GetDataTableByConditionWithParams(select, from,
                    Util.ToString(where), dpc);

            // 山No.の最大値取得
            int maxYamaNo = Util.ToInt(vi_hn_shikiriMeisai.DefaultView[0].Row.ItemArray[0]);
            if (maxYamaNo <= 0)
            {
                maxYamaNo = 1;
            }
            else
            {
                maxYamaNo++;
            }

            return maxYamaNo;
        }

        /// <summary>
        /// 伝票番号有無確認
        /// </summary>
        /// <returns>Boolean true:伝票有（更新）、false：伝票なし（新規）</returns>
        private Boolean existsDenpyoBango()
        {
            Boolean result = false;

            String denpyouBango = this.txtDenpyoBango.Text;
            if (ValChk.IsEmpty(denpyouBango) || !ValChk.IsNumber(denpyouBango))
            {
                return false;
            }

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd); // 1固定
            dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text))); 
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, DUMMY_DENPYO_KUBUN); // 3固定
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, denpyouBango);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            string select = "COUNT(*) AS CNT ";
            string from = "TB_HN_SHIKIRI_DATA ";
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD ");
            where.Append(" AND SHISHO_CD = @SHISHO_CD ");
            where.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN ");
            where.Append(" AND DENPYO_BANGO = @DENPYO_BANGO ");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO ");

            // DBからデータ取得
            DataTable shikiri_data =
                this.Dba.GetDataTableByConditionWithParams(select, from,
                    Util.ToString(where), dpc);

            // 伝票番号有無判定
            if (0 < shikiri_data.Rows.Count && 0 < Util.ToInt(shikiri_data.DefaultView[0].Row.ItemArray[0]))
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// 精算区分取得
        /// </summary>
        /// <returns>精算区分</returns>
        private int getSeisanKubun()
        {
            int seisanKubun = 0;
            seisanKubun = Util.ToInt(Util.ToString(this.txtSeisanKbn.Text));

            return seisanKubun;
        }

        /// <summary>
        /// 漁協手数料取得
        /// </summary>
        /// <param name="seisanKubun">精算区分</param>
        /// <returns>漁協手数料</returns>
        private string getGyokyoTesuryo(int seisanKubun)
        {
            string result = "0.00";

            switch (seisanKubun)
            {
                case KENGAI: // 県外
                    result = "0.00";
                    break;
                case JIMOTO: // 県外
                    result = "0.00";
                    break;
                case KENNAI: // 県内
                    result = "0.00";
                    break;
                default:
                    ;
                    break;
            }

            return result;
        }

        /// <summary>
        /// 船主CD設定情報
        /// </summary>
        /// <returns></returns>
        private bool SetFunanushiInfo()
        {
            if (!ValChk.IsEmpty(this.txtFunanushiCd.Text))
            {
                // 取引先情報マスタから船主コードに該当するデータを取得する
                StringBuilder sql = new StringBuilder();
                sql.Append("SELECT ");
                sql.Append(" GYOHO_CD, ");
                sql.Append(" GYOHO_NM, ");
                sql.Append(" KUMIAI_TESURYO_RITSU ");

                sql.Append(",SHOHIZEI_TENKA_HOHO_NM,");
                sql.Append(" SHOHIZEI_NYURYOKU_HOHO_NM ");

                sql.Append(",TORIHIKISAKI_NM,");
                sql.Append(" KINGAKU_HASU_SHORI,");
                sql.Append(" SHOHIZEI_NYURYOKU_HOHO,");
                sql.Append(" SHOHIZEI_HASU_SHORI,");
                sql.Append(" SHOHIZEI_TENKA_HOHO ");

                sql.Append("FROM ");
                sql.Append("    VI_HN_TORIHIKISAKI_JOHO ");
                sql.Append("WHERE ");
                sql.Append("      KAISHA_CD = @KAISHA_CD ");
                sql.Append("  AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD ");
                sql.Append("  AND TORIHIKISAKI_KUBUN1 = 1 ");

                decimal result = (decimal)0.00;
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtFunanushiCd.Text);

                DataTable TesuryoRitsu_data = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

                // 存在しない場合は、0.00を返す
                if (TesuryoRitsu_data.Rows.Count == 0)
                {
                    this.txtGyokyouTesuuryou.Text = Util.ToString(result);

                    if (INIT_FUNANUSHI_CODE != this.txtFunanushiCd.Text)
                        Msg.Error("入力に誤りがあります。");

                    this.txtFunanushiCd.SelectAll();
                    this.txtFunanushiCd.Focus();
                    return true;
                }

                result = Util.ToDecimal(TesuryoRitsu_data.DefaultView[0].Row.ItemArray[0]);

                DataRow rowData = TesuryoRitsu_data.Rows[0];

                string kumiaiTesuryo = rowData["KUMIAI_TESURYO_RITSU"].ToString();
                string gyoGyoushuCd = rowData["GYOHO_CD"].ToString();
                string gyoGyoushuNm = rowData["GYOHO_NM"].ToString();

                //this.SeriInfo.TANKA_SHUTOKU_HOHO = Util.ToDecimal(rowData["TANKA_SHUTOKU_HOHO"]);
                this.SeriInfo.KINGAKU_HASU_SHORI = Util.ToDecimal(rowData["KINGAKU_HASU_SHORI"]);
                this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO = Util.ToDecimal(rowData["SHOHIZEI_NYURYOKU_HOHO"]);
                this.SeriInfo.SHOHIZEI_HASU_SHORI = Util.ToDecimal(rowData["SHOHIZEI_HASU_SHORI"]);
                this.SeriInfo.SHOHIZEI_TENKA_HOHO = Util.ToDecimal(rowData["SHOHIZEI_TENKA_HOHO"]);

                this.lblFunanushiNm.Text = Util.ToString(rowData["TORIHIKISAKI_NM"]);

                //// 変更前船主CDに入力値を設定する
                //this.beforeFunanishiCd = Util.ToInt(inputVal);
                this.SeriInfo.FUNANISHI_CD = Util.ToInt(this.txtFunanushiCd.Text);

                this.lblShohizeiInputHoho2.Text = rowData["SHOHIZEI_NYURYOKU_HOHO"].ToString() + ":" + rowData["SHOHIZEI_NYURYOKU_HOHO_NM"].ToString();
                this.lblShohizeiTenka2.Text = rowData["SHOHIZEI_TENKA_HOHO"].ToString() + ":" + rowData["SHOHIZEI_TENKA_HOHO_NM"].ToString();

                seisanKbn = this.getSeisanKubun();
                // 強制税込み精算区分は設定より
                //if (seisanKbn == JIMOTO)
                if (seisanKbn == this.zeikomi_seisanKbn)
                {
                    this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO = 3; // ハマ売りの場合は税込みへ
                    this.SetTaxInputCategory(Util.ToInt(Util.ToString(this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO)));
                }

                // 事業区分を漁協手数料・漁業種コードとして表示、名称を取得する
                //this.txtGyokyouTesuuryou.Text = kumiaiTesuryo;
                if (Util.ToDecimal(this.txtGyokyouTesuuryou.Text) == 0)
                {
                    this.txtGyokyouTesuuryou.Text = kumiaiTesuryo;
                }
                //this.txtGyogyoushuCd.Text = gyoGyoushuCd;
                //this.lblGyogyoushuNm.Text = gyoGyoushuNm;
                if (this.txtGyogyoushuCd.Text == "" || this.txtGyogyoushuCd.Text == "0")
                {
                    this.txtGyogyoushuCd.Text = gyoGyoushuCd;
                    this.lblGyogyoushuNm.Text = gyoGyoushuNm;
                }

                return true;
            }
            else
            {
                Msg.Error("入力に誤りがあります。");
                this.txtFunanushiCd.SelectAll();
                this.txtFunanushiCd.Focus();
                return true;
            }
        }

        /// <summary>
        /// 魚種コードから魚種情報取得
        /// </summary>
        /// <param name="input_type">1:手入力,2:一覧画面から取得</param>
        private void GetGyoshuInfo(int input_type)
        {
            DbParamCollection dpc;
            DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;

            // 魚種情報取得
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, this.txtGridEdit.Text);
            //DataTable dtVI_HN_SHOHIN = this.Dba.GetDataTableByConditionWithParams(
            //    "*",
            //    "VI_HN_SHOHIN",
            //    "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND BARCODE1 = 999 ",
            //    dpc);
            DataTable dtVI_HN_SHOHIN = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_HN_GYOSHU",
                "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND GYOSHU_CD = @GYOSHU_CD ",
                dpc);
            if (dtVI_HN_SHOHIN.Rows.Count == 0)
            {
                this.dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_GYOSHU_NM, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_SHIWAKE_CD, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_JIGYO_KUBUN, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_PAYAO_RITSU, dgvCell.RowIndex].Value = "";

                this.dgvInputList[COL_HONSU, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_KIZY_CD, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_NAKAGAININ_CD, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_NAKAGAININ_NM, dgvCell.RowIndex].Value = "";

				// TODO 2020/02/12 Add Mutu Asato
				this.dgvInputList[COL_KEIFLG, dgvCell.RowIndex].Value = "";


				Msg.Error("入力に誤りがあります。");
                this.txtGridEdit.Focus();
                return;
            }
            else
            {
                if (Util.ToString(this.dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex].Value) != this.txtGridEdit.Text)
                {
                    // 魚種コード設定
                    this.dgvInputList[COL_GYOSHU_CD, dgvCell.RowIndex].Value = txtGridEdit.Text;
                    // 魚種名設定
                    this.dgvInputList[COL_GYOSHU_NM, dgvCell.RowIndex].Value = dtVI_HN_SHOHIN.Rows[0]["GYOSHU_NM"];
                    //// 単価設定
                    //if (ValChk.IsEmpty(dgvInputList[8, dgvCell.RowIndex].Value))
                    //{
                    //    //// 売単価
                    //    //this.dgvInputList[8, dgvCell.RowIndex].Value = Util.FormatNum(dtVI_HN_SHOHIN.Rows[0]["KORI_TANKA"], 1);
                    //}

                    seisanKbn = this.getSeisanKubun();
                    if (seisanKbn == KENNAI)
                    {
                        // 仕訳コード
                        this.dgvInputList[COL_SHIWAKE_CD, dgvCell.RowIndex].Value = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["JIMOTO_SHIWAKE_CD"]);
                        // 部門コード
                        this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex].Value = Util.ToString(SeriInfo.BUMON_CD != 0 ? SeriInfo.BUMON_CD : dtVI_HN_SHOHIN.Rows[0]["JIMOTO_BUMON_CD"]);
                        // 税区分
                        this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value = Util.ToDecimal(Util.ToString(dtVI_HN_SHOHIN.Rows[0]["JIMOTO_ZEI_KUBUN"]));
                        // 事業区分
                        this.dgvInputList[COL_JIGYO_KUBUN, dgvCell.RowIndex].Value = Util.ToString(SeriInfo.JIGYO_KUBUN != 0 ? SeriInfo.JIGYO_KUBUN : dtVI_HN_SHOHIN.Rows[0]["JIMOTO_JIGYO_KUBUN"]);
                    }
                    else
                    {
                        // 仕訳コード
                        this.dgvInputList[COL_SHIWAKE_CD, dgvCell.RowIndex].Value = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["SERI_SHIWAKE_CD"]);
                        // 部門コード
                        this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex].Value = Util.ToString(SeriInfo.BUMON_CD != 0 ? SeriInfo.BUMON_CD : dtVI_HN_SHOHIN.Rows[0]["SERI_BUMON_CD"]);
						// 税区分
						this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value = Util.ToDecimal(Util.ToString(dtVI_HN_SHOHIN.Rows[0]["JIMOTO_ZEI_KUBUN"]));
						// 事業区分
						this.dgvInputList[COL_JIGYO_KUBUN, dgvCell.RowIndex].Value = Util.ToString(SeriInfo.JIGYO_KUBUN != 0 ? SeriInfo.JIGYO_KUBUN : dtVI_HN_SHOHIN.Rows[0]["SERI_JIGYO_KUBUN"]);
                    }

                    // 税区分チェック 
                    if (!ValChk.IsEmpty(this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value))
                    {
                        // zam.TB_Ｆ税区分(TB_ZM_F_ZEI_KUBUN)
                        // 消費税率を取得
                        dpc = new DbParamCollection();
                        dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value);
                        DataTable dtTB_ZM_F_ZEI_KUBUN = this.Dba.GetDataTableByConditionWithParams(
                            "*",
                            "TB_ZM_F_ZEI_KUBUN",
                            "ZEI_KUBUN = @ZEI_KUBUN",
                            dpc);
                        // 税率
                        //DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                        //                                       this.txtMonth.Text, this.txtDay.Text, this.Dba);

                        DateTime DENPYO_DATE = Util.ConvAdDate(this.txtSeriDays.Gengo, this.txtSeriDays.WYear,
                                                               this.txtSeriDays.Month, this.txtSeriDays.Day, this.Dba);

						this.dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value = TaxUtil.GetTaxRate(DENPYO_DATE,
							Util.ToInt(Util.ToString(this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value)), this.Dba);
						// 課税区分
						this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value = Util.ToString(dtTB_ZM_F_ZEI_KUBUN.Rows[0]["KAZEI_KUBUN"]);

						// 軽減税率表記
						this.dgvInputList[COL_KEIFLG, dgvCell.RowIndex].Value = string.Empty;
						// TODO 2020/02/12 Add Mutu Asato
						if ("50"== Util.ToString(dtTB_ZM_F_ZEI_KUBUN.Rows[0]["ZEI_KUBUN"]))
						{
							// 軽減税率表記
							this.dgvInputList[COL_KEIFLG, dgvCell.RowIndex].Value = "*";

						}
					}
					else
                    {
                        this.dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value = "";
                        this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value = 0;

						// TODO 2020/02/12 Add Mutu Asato
						this.dgvInputList[COL_KEIFLG, dgvCell.RowIndex].Value = "";
					}

					if (this.dgvInputList.RowCount == dgvCell.RowIndex + 1)
                    {
                        // 行追加
                        this.dgvInputList.RowCount = this.dgvInputList.RowCount + 1;
                        this.dgvInputList[COL_GYO_NO, this.dgvInputList.RowCount - 1].Value = this.dgvInputList.RowCount;

                        // 山NOもアップ
                        this.dgvInputList[COL_YAMA_NO, this.dgvInputList.RowCount - 1].Value = Util.ToDecimal(this.dgvInputList[COL_YAMA_NO, dgvCell.RowIndex].Value) + 1;
                    }
                }
                if (input_type == 1)
                {
                    // カーソル制御（移動先が変わる場合は調整）
                    // 追加制御
                    //this.dgvInputList.CurrentCell = this.dgvInputList[COL_HONSU, dgvCell.RowIndex];
                    GridCursorControl gcc = GridCursorControl.GetInstance(this.Config);
                    int c = gcc.GetNextCol(dgvCell.ColumnIndex);
                    if (c == -1)
                        c = COL_HONSU;
                    this.dgvInputList.CurrentCell = this.dgvInputList[c, dgvCell.RowIndex];
                }
            }

            this.IS_INPUT = 2;
			// 新たに変更フラグ追加
			this.OrgnDialogResult = true;

		}

		/// <summary>
		/// パヤオNOからパヤオ情報取得
		/// </summary>
		/// <param name="input_type">1:手入力,2:一覧画面から取得</param>
		private void GetPayaoInfo(int input_type)
        {
            DbParamCollection dpc;
            DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;

            // パヤオ情報取得
            //dpc = new DbParamCollection();
            //dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            //dpc.SetParam("@SEISAN_KUBUN", SqlDbType.VarChar, 15, "4"); // 沿岸用で暫定
            //dpc.SetParam("@URIBA_CD", SqlDbType.VarChar, 15, this.txtGridEdit.Text);
            //DataTable dt = this.Dba.GetDataTableByConditionWithParams(
            //    "*",
            //    "TM_HN_TESURYO_RITSU_MST",
            //    "KAISHA_CD = @KAISHA_CD AND SEISAN_KUBUN = @SEISAN_KUBUN AND URIBA_CD = @URIBA_CD ",
            //    dpc);
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            //dpc.SetParam("@PAYAO_CD", SqlDbType.VarChar, 15, this.txtGridEdit.Text);
            dpc.SetParam("@PAYAO_CD", SqlDbType.VarChar, 4, this.txtGridEdit.Text);
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_PAYAO_TESURYO_RITSU",
                "KAISHA_CD = @KAISHA_CD AND PAYAO_CD = @PAYAO_CD ",
                dpc);
            if (dt.Rows.Count == 0)
            {
                Msg.Error("入力に誤りがあります。");
                this.txtGridEdit.Focus();
                return;
            }
            else
            {
                if (Util.ToString(this.dgvInputList[8, dgvCell.RowIndex].Value) != this.txtGridEdit.Text)
                {
                    // パヤオNO設定
                    this.dgvInputList[COL_PAYAO_NO, dgvCell.RowIndex].Value = txtGridEdit.Text;
                    // パヤオ率設定
                    this.dgvInputList[COL_PAYAO_RITSU, dgvCell.RowIndex].Value = dt.Rows[0]["TESURYO_RITSU"];
                }
                if (input_type == 1)
                {
                    // カーソル制御（移動先が変わる場合は調整）
                    this.dgvInputList.CurrentCell = this.dgvInputList[COL_NAKAGAININ_CD, dgvCell.RowIndex];
                }
            }
            this.IS_INPUT = 2;
			// 新たに変更フラグ追加
			this.OrgnDialogResult = true;

		}

		/// <summary>
		/// 伝票明細エンターキー以外の処理
		/// </summary>
		/// <returns>エンターキーを押したかの真偽値(真:押した,偽:押してない)</param>
		private bool GetDenpyoMeisaiKeyEnter(Keys KeyCode, DataGridViewCell dgvCell)
        {
            if ((KeyCode == Keys.Enter) || (KeyCode == Keys.Right))
            {
                return true;
            }
            if ((KeyCode == Keys.Down))
            {
                if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
                {
                    this.dgvInputList.CurrentCell = dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex + 1];

                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (dgvCell.RowIndex > 0)
                {
                    this.dgvInputList.CurrentCell = dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex - 1];

//                    return true;
                }
                else if (dgvCell.ColumnIndex == 1 && dgvCell.RowIndex == 0)
                {
                    this.txtGyogyoushuCd.Focus();//暫定
                }
            }
            return false;
        }

        /// <summary>
        /// データグリッド用のテキストの全選択
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBoxSelectAll(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox == null) return;

            //// 少し長めにクリックされた場合にも一応対処
            //while ((Control.MouseButtons & MouseButtons.Left) != MouseButtons.None)
            //    Application.DoEvents();

            textBox.SelectAll();
        }

        /// <summary>
        /// 消費税入力方法名の表示
        /// </summary>
        /// <param name="cat">消費税入力方法</param>
        private void SetTaxInputCategory(int cat)
        {
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", this.txtMizuageShishoCd.Text, cat.ToString());
            this.lblShohizeiInputHoho2.Text = cat.ToString() + ":" + name;
        }

        /// <summary>
        /// データグリッドを小計、消費税（外）、合計額に集計
        /// </summary>
        private void DataGridSum()
        {
			// TODO 2020/02/12 Add Mutu Asato 追加する
			decimal[] total = { 0, 0, 0, 0, 0 , 0,0,0 };
			//decimal[] total = { 0, 0, 0, 0 };
			decimal work = 0;
            bool setFlg = false;
            int taxIdx = 0;
            List<MeisaiTbl> taxTbl = new List<MeisaiTbl>();
            int i = 0;

			// TODO 2020/02/12 Add Mutu Asato
			decimal intKeiPar = 0;
			decimal intTsuPar = 0;

            while (this.dgvInputList.RowCount > i)
            {
                // 税区分が未設定のデータが出る場合に注意
                if (!ValChk.IsEmpty(Util.ToString(this.dgvInputList[COL_ZEI_KUBUN, i].Value)))
                {
                    // 数量集計
                    total[3] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO, i].Value));

                    // 税区分
                    if (Util.ToInt(Util.ToString(dgvInputList[COL_ZEI_KUBUN, i].Value)) == 0)
                    {
                        // 非課税額
                        total[0] += Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value));
                    }
                    else
                    {
                        // 課税額
                        switch (Util.ToInt(this.SeriInfo.SHOHIZEI_TENKA_HOHO))
                        {
                            case 1: // 明細転嫁
                            case 3: // 請求転嫁
                                switch (Util.ToInt(this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO))
                                {
                                    case 2: // 税抜き
										if (null != dgvInputList[COL_KEIFLG, i])
										{
											if (!string.IsNullOrEmpty(Util.ToString(dgvInputList[COL_KEIFLG, i].Value)))
											{

												// TODO 2020/02/12 ADD Mutu Asato 軽減税率
												if (Util.ToString(dgvInputList[COL_KEIFLG, i].Value)=="*")
												{

													intKeiPar = Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value));
													total[6] += Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value));
													total[7] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
												}
												else
												{
													intTsuPar = Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value));
													total[4] += Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value));
													total[5] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
												}
											}
										}

										total[1] += Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value));
                                        total[2] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));

										break;

                                    case 3: // 税込み

										if (null != dgvInputList[COL_KEIFLG, i])
										{
											if (!string.IsNullOrEmpty(Util.ToString(dgvInputList[COL_KEIFLG, i].Value)))
											{

												// TODO 2020/02/12 ADD Mutu Asato 軽減税率
												if (Util.ToString(dgvInputList[COL_KEIFLG, i].Value) == "*")
												{
													intKeiPar = Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value));
													total[6] += Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value))-Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
													total[7] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
												}
												else
												{
													intTsuPar = Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value));
													total[4] += Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value))-Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
													total[5] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
												}
											}
										}

										total[1] += Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value)) - Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
                                        total[2] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
                                        break;
                                    default:
                                        total[1] += Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value));
                                        total[2] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
                                        break;
                                }
                                break;
                            case 2: // 伝票転嫁 明細転嫁の場合
                                setFlg = false;
                                //税種別（軽減税orNot）に集計　←// 税率毎に集計
                                if (taxIdx != 0)
                                {
                                    for (int j = 0; j < taxTbl.Count; j++)
                                    {
                                        //if (taxTbl[j].ZeiRt == Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value)))
                                        if (taxTbl[j].Flag == Util.ToString(Util.ToString(dgvInputList[COL_KEIFLG, i].Value)))
                                        {
                                            taxTbl[j].Kingk += Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value));

                                            taxTbl[j].Flag = Util.ToString(Util.ToString(dgvInputList[COL_KEIFLG, i].Value));
                                            setFlg = true;
                                            break;
                                        }
                                    }
                                }
                                if (!setFlg)
                                {
                                    MeisaiTbl m = new MeisaiTbl();
                                    m.ZeiRt = Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value));
                                    m.Kingk = Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value));
                                    m.Flag = Util.ToString(Util.ToString(dgvInputList[COL_KEIFLG, i].Value));
                                    taxTbl.Add(m);
                                    taxIdx += 1;
                                }
                                break;
                        }
                    }
                }
                i++;
            }
            // 伝票転嫁消費税確定
            if (Util.ToInt(this.SeriInfo.SHOHIZEI_TENKA_HOHO) == 2 && taxIdx > 0)
            {
                for (int j = 0; j < taxTbl.Count; j++)
                {
                    total[1] += taxTbl[j].Kingk;
                    if (taxTbl[j].ZeiRt != 0 && taxTbl[j].Kingk != 0)
                    {
                        switch (Util.ToInt(this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO))
                        {
                            case 2: // 税抜き
								work = (taxTbl[j].Kingk * taxTbl[j].ZeiRt) / 100;

								if (null != dgvInputList[COL_KEIFLG, j])
								{
									// TODO 2020/02/12 ADD Mutu Asato 軽減税率
									if (taxTbl[j].Flag == "*")
									{

										total[6] += Util.ToInt(taxTbl[j].Kingk);
										intKeiPar = Util.ToDecimal(Util.ToString(taxTbl[j].ZeiRt));
										total[7] += TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
									}
									else
									{
										intTsuPar = Util.ToDecimal(Util.ToString(taxTbl[j].ZeiRt));
										total[4] += Util.ToInt(taxTbl[j].Kingk);
										total[5] += TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
									}
								}

								total[2] += TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
                                break;

                            case 3: // 税込み
                                work = (taxTbl[j].Kingk / (100 + taxTbl[j].ZeiRt)) * taxTbl[j].ZeiRt;

								if (null != dgvInputList[COL_KEIFLG, j])
								{
									// TODO 2020/02/12 ADD Mutu Asato 軽減税率
									if (taxTbl[j].Flag == "*")
									{
										intKeiPar = Util.ToDecimal(Util.ToString(taxTbl[j].ZeiRt));
										total[6] += total[1] - TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
										total[7] += TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
									}
									else
									{
										intTsuPar = Util.ToDecimal(Util.ToString(taxTbl[j].ZeiRt));
										total[4] += total[1] - TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
										total[5] += TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
									}
								}

								total[1] = total[1] - TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
                                total[2] += TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
                                break;
                        }
                    }
                }
            }

            // ［合計］水揚数量
            this.txtTotalSuuryou.Text = total[3].ToString("n1");

            // ［合計］水揚金額
            this.txtTotalMizuageGaku.Text = Util.FormatNum(total[0] + total[1]);

            // ［合計］消費税（外）
            this.txtTotalSyohiZei.Text = Util.FormatNum(total[2]);

            // ［合計］水揚合計額
            this.txtTotalKingaku.Text = Util.FormatNum(total[0] + total[1] + total[2]);

			// 軽減税率表示
			string TsuData = 
				//intTsuPar.ToString() + "%対象 " + string.Format("{0:N0}", Util.ToInt(total[4].ToString())).PadLeft(12, ' ') +
				                   " 消費税対象 " + string.Format("{0:N0}", Util.ToInt(total[4].ToString())).PadLeft(12, ' ') +
				"円 税 " + string.Format("{0:N0}", Util.ToInt(total[5].ToString())).PadLeft(12,' ') + "円";

			string keiData = "*" +
				//intKeiPar.ToString() + "%対象 " + string.Format("{0:N0}", Util.ToInt(total[6].ToString())).PadLeft(12, ' ') +
				                    "軽減税対象 " + string.Format("{0:N0}", Util.ToInt(total[6].ToString())).PadLeft(12, ' ') +
				"円 税 " + string.Format("{0:N0}",  Util.ToInt(total[7].ToString())).PadLeft(12, ' ') + "円";

			if ((0 == total[4]) && (0 == total[6]))
			{
				this.lblUchiwake.Text = string.Empty;
			}
			else if((0 < total[4]) && (0 == total[6]))
			{
				this.lblUchiwake.Text = TsuData;
			}
			else if ((0 == total[4]) && (0 <total[6]))
			{
				this.lblUchiwake.Text = keiData;
			}
			else
			{
				this.lblUchiwake.Text = 
					TsuData + Environment.NewLine + keiData;
			}
		}

		/// <summary>
		/// 金額の設定
		/// </summary>
		/// <param name="Row"></param>
		/// <returns></returns>
		private bool CellKingkSet(int Row)
        {
            decimal num = Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO, Row].Value));
            decimal tan = Util.ToDecimal(Util.ToString(dgvInputList[COL_TANKA, Row].Value));
            decimal kin = num * tan;
            kin = TaxUtil.CalcFraction(kin, 0, Util.ToInt(SeriInfo.KINGAKU_HASU_SHORI));
            if (!ValChk.IsDecNumWithinLength(kin, 9, 0, true))
            {
                return false;
            }

            dgvInputList[COL_KINGAKU, Row].Value = Util.FormatNum(kin);
            CellTaxSet(Row);

            DataGridSum();
            return true;
        }

        /// <summary>
        /// 税情報の設定及び明細転嫁時の税額設定
        /// </summary>
        /// <param name="Row"></param>
        private void CellTaxSet(int Row)
        {
            int ZeiKb = 0;
            int KziKb = 0;
            decimal ZeiRt = 0;
            decimal wKingk = 0;
            decimal wZeigk = 0;
            // 税区分
            if (dgvInputList[COL_ZEI_KUBUN, Row].Value != null)
            {
                ZeiKb = Util.ToInt(Util.ToString(dgvInputList[COL_ZEI_KUBUN, Row].Value));
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, ZeiKb);
                DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "TB_ZM_F_ZEI_KUBUN",
                    "ZEI_KUBUN = @ZEI_KUBUN",
                    dpc);
                // 課税区分
                if (dt.Rows.Count > 0)
                {
                    KziKb = Util.ToInt(Util.ToString(dt.Rows[0]["KAZEI_KUBUN"]));
                }

                // 税率
                //DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                //                                       this.txtMonth.Text, this.txtDay.Text, this.Dba);
                DateTime DENPYO_DATE = Util.ConvAdDate(this.txtSeriDays.Gengo, this.txtSeriDays.WYear,
                                                       this.txtSeriDays.Month, this.txtSeriDays.Day, this.Dba);
                //dpc = new DbParamCollection();
                //StringBuilder sql = new StringBuilder();
                //sql.Append("SELECT dbo.FNC_GetTaxRate( @ZEI_KUBUN, @DENPYO_DATE )");
                //dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, ZeiKb);
                //dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DENPYO_DATE);
                //// 新税区分より日付も指定して税率の取得
                //dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
                //if (dt.Rows.Count == 0)
                //    ZeiRt = 0;
                //else
                //    ZeiRt = Util.ToDecimal(dt.Rows[0][0]);
                ZeiRt = TaxUtil.GetTaxRate(DENPYO_DATE, ZeiKb, this.Dba);
            }


            //if (KziKb != 0)
            if (KziKb == 1)
            {
                wKingk = Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, Row].Value));
                if (dgvInputList[COL_SHOHIZEI, Row].Value != null)
                {
                    wZeigk = Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, Row].Value));
                }
                if (Util.ToInt(this.SeriInfo.SHOHIZEI_TENKA_HOHO) != 1)
                    wZeigk = 0;

                // 税額の算出
                switch (Util.ToInt(this.SeriInfo.SHOHIZEI_TENKA_HOHO))
                {
                    case 1:// 明細転嫁
                        switch (Util.ToInt(this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO))
                        {
                            case 2: // 税抜き
                                wZeigk = wKingk * ZeiRt / 100;
                                break;

                            case 3: // 税込み
                                wZeigk = (wKingk / (100 + ZeiRt)) * ZeiRt;
                                break;
                        }
                        break;
                }
                dgvInputList[COL_KINGAKU, Row].Value = Util.FormatNum(wKingk);
                dgvInputList[COL_SHOHIZEI, Row].Value = Util.FormatNum(TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI)));
                dgvInputList[COL_ZEI_RITSU, Row].Value = Util.ToDecimal(ZeiRt);
                dgvInputList[COL_KAZEI_KUBUN, Row].Value = KziKb;

				if (ZeiKb == 50)
				{
					dgvInputList[COL_KEIFLG, Row].Value = "*";
				}
				else
				{
					dgvInputList[COL_KEIFLG, Row].Value = string.Empty;
				}
			}
            else
            {
                dgvInputList[COL_SHOHIZEI, Row].Value = null;
                dgvInputList[COL_ZEI_RITSU, Row].Value = string.Empty;
                dgvInputList[COL_KAZEI_KUBUN, Row].Value = string.Empty;
				dgvInputList[COL_KEIFLG, Row].Value = string.Empty;
			}
			DataGridSum();
        }

        /// <summary>
        /// 税情報の一括設定
        /// </summary>
        private void CellTaxRateSet()
        {
            int i = 0;
            while (this.dgvInputList.RowCount > i)
            {
                if (dgvInputList[COL_ZEI_KUBUN, i].Value != null && Util.ToInt(dgvInputList[COL_ZEI_KUBUN, i].Value) != 0)
                {
                    CellTaxSet(i);
                }
                i++;
            }
        }

        /// <summary>
        /// 伝票転嫁消費税の振り分け
        /// </summary>
        /// <param name="tb">リスト</param>
        private void TaxSorting(ref List<MeisaiTbl> tb)
        {
            decimal work = 0;
            bool setFlg = false;
            int taxIdx = 0;
            List<MeisaiTbl> taxTbl = new List<MeisaiTbl>();
            int i = 0;

            tb = new List<MeisaiTbl>();
            while (this.dgvInputList.RowCount > i)
            {
                if (!ValChk.IsEmpty(Util.ToString(this.dgvInputList[COL_ZEI_KUBUN, i].Value)))
                {
                    MeisaiTbl m = new MeisaiTbl();
                    m.Kingk = Util.ToDecimal(Util.ToString(dgvInputList[COL_KINGAKU, i].Value));
                    m.Zeigk = Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
                    m.ZeiRt = Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value));

                    // 伝票転嫁
                    if (Util.ToInt(this.SeriInfo.SHOHIZEI_TENKA_HOHO) == 2 &&
                        Util.ToInt(dgvInputList[COL_KAZEI_KUBUN, i].Value) == 1 &&
                        m.Kingk != 0 && m.ZeiRt != 0)
                    {
                        switch (Util.ToInt(this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO))
                        {
                            case 2: // 税抜き
                                work = (m.Kingk * m.ZeiRt) / 100;
                                m.Zeigk = TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
                                break;

                            case 3: // 税込み
                                work = (m.Kingk / (100 + m.ZeiRt)) * m.ZeiRt;
                                m.Zeigk = TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI));
                                break;
                        }
                        // 税率毎に集計
                        setFlg = false;
                        if (taxIdx > 0)
                        {
                            for (int j = 0; j < taxTbl.Count; j++)
                            {
                                if (taxTbl[j].ZeiRt == Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value)))
                                {
                                    taxTbl[j].Kingk += m.Kingk;
                                    taxTbl[j].Zeigk += m.Zeigk;
                                    taxTbl[j].Row = i;
                                    setFlg = true;
                                    break;
                                }
                            }
                        }
                        if (!setFlg)
                        {
                            MeisaiTbl z = new MeisaiTbl();
                            z.Kingk = m.Kingk;
                            z.Zeigk = m.Zeigk;
                            z.ZeiRt = m.ZeiRt;
                            z.Row = i;
                            taxTbl.Add(z);
                            taxIdx += 1;
                        }
                    }
                    tb.Add(m);
                }
                i++;
            }
            // 伝票転嫁消費税の振り分け
            if (Util.ToInt(this.SeriInfo.SHOHIZEI_TENKA_HOHO) == 2 && taxIdx > 0)
            {
                for (int j = 0; j < taxTbl.Count; j++)
                {
                    work = 0;
                    switch (Util.ToInt(this.SeriInfo.SHOHIZEI_NYURYOKU_HOHO))
                    {
                        case 2: // 税抜き
                            work = (taxTbl[j].Kingk * taxTbl[j].ZeiRt) / 100;
                            break;

                        case 3: // 税込み
                            work = (taxTbl[j].Kingk / (100 + taxTbl[j].ZeiRt)) * taxTbl[j].ZeiRt;
                            break;
                    }
                    // 合計消費税額が一致しない場合最終行より減算
                    if (taxTbl[j].Zeigk != TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI)))
                    {
                        taxIdx = taxTbl[j].Row;
                        tb[taxIdx].Zeigk = tb[taxIdx].Zeigk - (taxTbl[j].Zeigk - TaxUtil.CalcFraction(work, 0, Util.ToInt(SeriInfo.SHOHIZEI_HASU_SHORI)));
                    }
                }
            }
        }

        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }

        /// <summary>
        /// 精算区分の設定（荷受人の制御含む）
        /// </summary>
        /// <param name="kbn"></param>
        private void SetSeisanKubun(string kbn)
        {
            this.txtSeisanKbn.Text = kbn;
            this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", Util.ToString(txtMizuageShishoCd.Text), this.txtSeisanKbn.Text);
            this.txtNiukeninCd.Enabled = Util.ToInt(kbn) == KENNAI ? false : true;
        }
        #endregion

        private void dgvInputList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtFunanushiCd_TextChanged(object sender, EventArgs e)
        {
            //isCHanged = true;
        }

        private void btnF1_Click(object sender, EventArgs e)
        {
            this.PressF1();
        }

        private void txtDenpyoBango_TextChanged(object sender, EventArgs e)
        {
            //isCHanged = true;

        }

        private void btnF2_Click(object sender, EventArgs e)
        {
            this.PressF2();
        }

        private void btnF3_Click(object sender, EventArgs e)
        {
            this.PressF3();
        }

        private void btnF4_Click(object sender, EventArgs e)
        {
            this.PressF4();
        }

        private void btnF5_Click(object sender, EventArgs e)
        {
            this.PressF5();
        }

        private void txtFunanushiCd_Validated(object sender, EventArgs e)
        {
            //未入力時は何もしない
            if (ValChk.IsEmpty(this.txtFunanushiCd.Text) || INIT_FUNANUSHI_CODE == this.txtFunanushiCd.Text)
            {
                return;
            }
            // 消費税転嫁方法、消費税入力方法をクリア
            this.lblShohizeiInputHoho2.Text = "";
            this.lblShohizeiTenka2.Text = "";

            // 船主変更時（事前保持）
            int bef = this.SeriInfo.FUNANISHI_CD;

            this.SetFunanushiInfo();

            // 船主変更時の明細再計算
            if (bef != 0 && !ValChk.IsEmpty(this.txtFunanushiCd.Text) && (Util.ToInt(this.txtFunanushiCd.Text) != bef))
            {
                CellTaxRateSet();
            }

            this.IS_INPUT = 2;
			// 新たに変更フラグ追加
			this.OrgnDialogResult = true;

			//if (this.rdoBashoNm01.Checked)
			if (this.seisanKbn == KENNAI)
            {
                // 箱数へフォーカス
                //this.txtHakosuu.Focus();
                //this.txtHakosuu.ReadOnly = true;
                //this.txtHakosuu.TabStop = false;
            }
            else
            {
                // 荷受人へフォーカス
                this.txtNiukeninCd.Focus();
            }
            //消費税計算
            this.DataGridSum();
        }

        private void txtSeisanKbn_TextChanged(object sender, EventArgs e)
        {
			//isCHanged = true;
        }

        private void fsiDate1_ChangeOrgnDate(object sender)
        {
            DateTime d = Util.ConvAdDate(
                this.txtSeriDays.Gengo,
                this.txtSeriDays.WYear,
                    this.txtSeriDays.Month,
                    this.txtSeriDays.Day, this.Dba);

            // 新規は無条件、修正時は日付が変わったら行う（日付で税率が変わる対応）
            if ((this.MODE_EDIT == 2 && this.SeriInfo.DENPYO_DATE != d.ToString("yyyy/MM/dd")) ||
                 this.MODE_EDIT == 1)
            {
                CellTaxRateSet();
                this.SeriInfo.DENPYO_DATE = d.ToString("yyyy/MM/dd");
            }

        }
	}
}

