﻿namespace jp.co.fsi.hn.hnse1011
{
    partial class HNSE1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblNiukeninNm = new System.Windows.Forms.Label();
			this.txtNiukeninCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblNiukeninCd = new System.Windows.Forms.Label();
			this.lblFunanushiNm = new System.Windows.Forms.Label();
			this.txtFunanushiCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiCd = new System.Windows.Forms.Label();
			this.lblGyogyoushuNm = new System.Windows.Forms.Label();
			this.txtGyokyouTesuuryou = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGyokyouTesuuryou = new System.Windows.Forms.Label();
			this.txtDenpyoBango = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoNo = new System.Windows.Forms.Label();
			this.lblDay = new System.Windows.Forms.Label();
			this.lblMonth = new System.Windows.Forms.Label();
			this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengo = new System.Windows.Forms.Label();
			this.lblJp = new System.Windows.Forms.Label();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.txtTotalKingaku = new jp.co.fsi.common.controls.FsiTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtTotalMizuageGaku = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTotalSyohiZei = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTotalSuuryou = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTotalGaku = new System.Windows.Forms.Label();
			this.lblSyohiZei = new System.Windows.Forms.Label();
			this.lblSyokei = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.lblGyogyoushu = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.txtGyogyoushuCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.rdoBashoNm01 = new System.Windows.Forms.RadioButton();
			this.rdoBashoNm02 = new System.Windows.Forms.RadioButton();
			this.rdoBashoNm03 = new System.Windows.Forms.RadioButton();
			this.lblZenkaiDenpyoBango = new System.Windows.Forms.Label();
			this.lblMode = new System.Windows.Forms.Label();
			this.lblShohizeiTenka2 = new System.Windows.Forms.Label();
			this.lblShohizeiInputHoho2 = new System.Windows.Forms.Label();
			this.lblShohizeiTenka1 = new System.Windows.Forms.Label();
			this.lblShohizeiInputHoho1 = new System.Windows.Forms.Label();
			this.dgvInputList = new System.Windows.Forms.DataGridView();
			this.colGYO_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colYAMA_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colGYOSHU_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colGYOSHU_NM = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.HONSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KIZU_NM = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colSURYO = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colTANKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colKINGAKU = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colSHOHIZEI = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colPAYAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colNAKAGAININ_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colNAKAGAININ_NM = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SHIWAKE_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BUMON_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.JIGYO_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ZEI_RITSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.GEN_TANKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KAZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KIZU_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KEIGEN_FLG = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.txtGridEdit = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanKbnNm = new System.Windows.Forms.Label();
			this.lblSeisanKbnNmTitle = new System.Windows.Forms.Label();
			this.txtHakosuu = new jp.co.fsi.common.controls.FsiTextBox();
			this.tableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.panel4 = new jp.co.fsi.common.FsiPanel();
			this.panel3 = new jp.co.fsi.common.FsiPanel();
			this.panel2 = new jp.co.fsi.common.FsiPanel();
			this.txtSeriDays = new jp.co.fsi.ClientCommon.FsiDate();
			this.panel1 = new jp.co.fsi.common.FsiPanel();
			this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblYear = new System.Windows.Forms.Label();
			this.tableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.tableLayoutPanel3 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.panel10 = new jp.co.fsi.common.FsiPanel();
			this.panel9 = new jp.co.fsi.common.FsiPanel();
			this.panel8 = new jp.co.fsi.common.FsiPanel();
			this.panel7 = new jp.co.fsi.common.FsiPanel();
			this.txtDummy = new jp.co.fsi.common.controls.FsiTextBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.lblUchiwake = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.panel10.SuspendLayout();
			this.panel9.SuspendLayout();
			this.panel8.SuspendLayout();
			this.panel7.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 523);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1219, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1209, 37);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Tag = "";
			this.lblTitle.Text = "";
			// 
			// lblNiukeninNm
			// 
			this.lblNiukeninNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblNiukeninNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNiukeninNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNiukeninNm.Location = new System.Drawing.Point(172, 5);
			this.lblNiukeninNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNiukeninNm.Name = "lblNiukeninNm";
			this.lblNiukeninNm.Size = new System.Drawing.Size(415, 24);
			this.lblNiukeninNm.TabIndex = 26;
			this.lblNiukeninNm.Tag = "DISPNAME";
			this.lblNiukeninNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNiukeninCd
			// 
			this.txtNiukeninCd.AutoSizeFromLength = true;
			this.txtNiukeninCd.BackColor = System.Drawing.Color.LightCyan;
			this.txtNiukeninCd.DisplayLength = null;
			this.txtNiukeninCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtNiukeninCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtNiukeninCd.Location = new System.Drawing.Point(105, 6);
			this.txtNiukeninCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtNiukeninCd.MaxLength = 6;
			this.txtNiukeninCd.Name = "txtNiukeninCd";
			this.txtNiukeninCd.Size = new System.Drawing.Size(63, 23);
			this.txtNiukeninCd.TabIndex = 10;
			this.txtNiukeninCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNiukeninCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtNiukeninCd_Validating);
			// 
			// lblNiukeninCd
			// 
			this.lblNiukeninCd.BackColor = System.Drawing.Color.Silver;
			this.lblNiukeninCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNiukeninCd.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblNiukeninCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblNiukeninCd.Location = new System.Drawing.Point(0, 0);
			this.lblNiukeninCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblNiukeninCd.Name = "lblNiukeninCd";
			this.lblNiukeninCd.Size = new System.Drawing.Size(593, 36);
			this.lblNiukeninCd.TabIndex = 24;
			this.lblNiukeninCd.Tag = "CHANGE";
			this.lblNiukeninCd.Text = "荷受人CD";
			this.lblNiukeninCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiNm
			// 
			this.lblFunanushiNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiNm.Location = new System.Drawing.Point(172, 3);
			this.lblFunanushiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiNm.Name = "lblFunanushiNm";
			this.lblFunanushiNm.Size = new System.Drawing.Size(415, 24);
			this.lblFunanushiNm.TabIndex = 18;
			this.lblFunanushiNm.Tag = "DISPNAME";
			this.lblFunanushiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCd
			// 
			this.txtFunanushiCd.AutoSizeFromLength = true;
			this.txtFunanushiCd.DisplayLength = null;
			this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCd.Location = new System.Drawing.Point(105, 4);
			this.txtFunanushiCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiCd.MaxLength = 4;
			this.txtFunanushiCd.Name = "txtFunanushiCd";
			this.txtFunanushiCd.Size = new System.Drawing.Size(63, 23);
			this.txtFunanushiCd.TabIndex = 7;
			this.txtFunanushiCd.Text = "0";
			this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCd.TextChanged += new System.EventHandler(this.txtFunanushiCd_TextChanged);
			this.txtFunanushiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCd_Validating);
			// 
			// lblFunanushiCd
			// 
			this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCd.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCd.Location = new System.Drawing.Point(0, 0);
			this.lblFunanushiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCd.Name = "lblFunanushiCd";
			this.lblFunanushiCd.Size = new System.Drawing.Size(593, 33);
			this.lblFunanushiCd.TabIndex = 16;
			this.lblFunanushiCd.Tag = "CHANGE";
			this.lblFunanushiCd.Text = "船主CD";
			this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblGyogyoushuNm
			// 
			this.lblGyogyoushuNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblGyogyoushuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyogyoushuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyogyoushuNm.Location = new System.Drawing.Point(887, 3);
			this.lblGyogyoushuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyogyoushuNm.Name = "lblGyogyoushuNm";
			this.lblGyogyoushuNm.Size = new System.Drawing.Size(286, 24);
			this.lblGyogyoushuNm.TabIndex = 23;
			this.lblGyogyoushuNm.Tag = "DISPNAME";
			this.lblGyogyoushuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyokyouTesuuryou
			// 
			this.txtGyokyouTesuuryou.AutoSizeFromLength = true;
			this.txtGyokyouTesuuryou.DisplayLength = null;
			this.txtGyokyouTesuuryou.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyokyouTesuuryou.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtGyokyouTesuuryou.Location = new System.Drawing.Point(695, 4);
			this.txtGyokyouTesuuryou.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyokyouTesuuryou.MaxLength = 6;
			this.txtGyokyouTesuuryou.Name = "txtGyokyouTesuuryou";
			this.txtGyokyouTesuuryou.Size = new System.Drawing.Size(67, 23);
			this.txtGyokyouTesuuryou.TabIndex = 8;
			this.txtGyokyouTesuuryou.Text = "0.00";
			this.txtGyokyouTesuuryou.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyokyouTesuuryou.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyokyouTesuryou_Validating);
			// 
			// lblGyokyouTesuuryou
			// 
			this.lblGyokyouTesuuryou.BackColor = System.Drawing.Color.Silver;
			this.lblGyokyouTesuuryou.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyokyouTesuuryou.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblGyokyouTesuuryou.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyokyouTesuuryou.Location = new System.Drawing.Point(593, 0);
			this.lblGyokyouTesuuryou.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyokyouTesuuryou.Name = "lblGyokyouTesuuryou";
			this.lblGyokyouTesuuryou.Size = new System.Drawing.Size(181, 33);
			this.lblGyokyouTesuuryou.TabIndex = 19;
			this.lblGyokyouTesuuryou.Tag = "CHANGE";
			this.lblGyokyouTesuuryou.Text = "漁協手数料";
			this.lblGyokyouTesuuryou.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDenpyoBango
			// 
			this.txtDenpyoBango.AutoSizeFromLength = true;
			this.txtDenpyoBango.DisplayLength = null;
			this.txtDenpyoBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDenpyoBango.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDenpyoBango.Location = new System.Drawing.Point(70, 4);
			this.txtDenpyoBango.Margin = new System.Windows.Forms.Padding(4);
			this.txtDenpyoBango.MaxLength = 6;
			this.txtDenpyoBango.Name = "txtDenpyoBango";
			this.txtDenpyoBango.Size = new System.Drawing.Size(95, 23);
			this.txtDenpyoBango.TabIndex = 2;
			this.txtDenpyoBango.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDenpyoBango.TextChanged += new System.EventHandler(this.txtDenpyoBango_TextChanged);
			this.txtDenpyoBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenpyoBango_Validating);
			// 
			// lblDenpyoNo
			// 
			this.lblDenpyoNo.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDenpyoNo.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblDenpyoNo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoNo.Location = new System.Drawing.Point(0, 0);
			this.lblDenpyoNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoNo.Name = "lblDenpyoNo";
			this.lblDenpyoNo.Size = new System.Drawing.Size(169, 33);
			this.lblDenpyoNo.TabIndex = 3;
			this.lblDenpyoNo.Tag = "CHANGE";
			this.lblDenpyoNo.Text = "伝票番号";
			this.lblDenpyoNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDay
			// 
			this.lblDay.AutoSize = true;
			this.lblDay.BackColor = System.Drawing.Color.Silver;
			this.lblDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDay.Location = new System.Drawing.Point(997, 274);
			this.lblDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDay.Name = "lblDay";
			this.lblDay.Size = new System.Drawing.Size(24, 16);
			this.lblDay.TabIndex = 12;
			this.lblDay.Tag = "CHANGE";
			this.lblDay.Text = "日";
			this.lblDay.Visible = false;
			// 
			// lblMonth
			// 
			this.lblMonth.AutoSize = true;
			this.lblMonth.BackColor = System.Drawing.Color.Silver;
			this.lblMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonth.Location = new System.Drawing.Point(911, 274);
			this.lblMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonth.Name = "lblMonth";
			this.lblMonth.Size = new System.Drawing.Size(24, 16);
			this.lblMonth.TabIndex = 10;
			this.lblMonth.Tag = "CHANGE";
			this.lblMonth.Text = "月";
			this.lblMonth.Visible = false;
			// 
			// txtDay
			// 
			this.txtDay.AutoSizeFromLength = false;
			this.txtDay.DisplayLength = null;
			this.txtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDay.Location = new System.Drawing.Point(941, 270);
			this.txtDay.Margin = new System.Windows.Forms.Padding(4);
			this.txtDay.MaxLength = 2;
			this.txtDay.Name = "txtDay";
			this.txtDay.Size = new System.Drawing.Size(52, 23);
			this.txtDay.TabIndex = 5;
			this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDay.Visible = false;
			// 
			// lblGengo
			// 
			this.lblGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengo.Location = new System.Drawing.Point(707, 270);
			this.lblGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengo.Name = "lblGengo";
			this.lblGengo.Size = new System.Drawing.Size(53, 24);
			this.lblGengo.TabIndex = 6;
			this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblGengo.Visible = false;
			// 
			// lblJp
			// 
			this.lblJp.BackColor = System.Drawing.Color.Silver;
			this.lblJp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJp.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJp.Location = new System.Drawing.Point(169, 0);
			this.lblJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJp.Name = "lblJp";
			this.lblJp.Size = new System.Drawing.Size(423, 33);
			this.lblJp.TabIndex = 5;
			this.lblJp.Tag = "CHANGE";
			this.lblJp.Text = "セリ日付";
			this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(783, 3);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(390, 24);
			this.lblMizuageShishoNm.TabIndex = 15;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTotalKingaku
			// 
			this.txtTotalKingaku.AutoSizeFromLength = true;
			this.txtTotalKingaku.BackColor = System.Drawing.Color.LightCyan;
			this.txtTotalKingaku.DisplayLength = null;
			this.txtTotalKingaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTotalKingaku.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTotalKingaku.Location = new System.Drawing.Point(269, 2);
			this.txtTotalKingaku.Margin = new System.Windows.Forms.Padding(4);
			this.txtTotalKingaku.MaxLength = 12;
			this.txtTotalKingaku.Name = "txtTotalKingaku";
			this.txtTotalKingaku.ReadOnly = true;
			this.txtTotalKingaku.Size = new System.Drawing.Size(119, 23);
			this.txtTotalKingaku.TabIndex = 7;
			this.txtTotalKingaku.TabStop = false;
			this.txtTotalKingaku.Text = "0";
			this.txtTotalKingaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(390, 28);
			this.label3.TabIndex = 6;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "水 揚 合 計 額";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTotalMizuageGaku
			// 
			this.txtTotalMizuageGaku.AutoSizeFromLength = true;
			this.txtTotalMizuageGaku.BackColor = System.Drawing.Color.LightCyan;
			this.txtTotalMizuageGaku.DisplayLength = null;
			this.txtTotalMizuageGaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTotalMizuageGaku.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTotalMizuageGaku.Location = new System.Drawing.Point(269, 2);
			this.txtTotalMizuageGaku.Margin = new System.Windows.Forms.Padding(4);
			this.txtTotalMizuageGaku.MaxLength = 12;
			this.txtTotalMizuageGaku.Name = "txtTotalMizuageGaku";
			this.txtTotalMizuageGaku.ReadOnly = true;
			this.txtTotalMizuageGaku.Size = new System.Drawing.Size(119, 23);
			this.txtTotalMizuageGaku.TabIndex = 3;
			this.txtTotalMizuageGaku.TabStop = false;
			this.txtTotalMizuageGaku.Text = "0";
			this.txtTotalMizuageGaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtTotalSyohiZei
			// 
			this.txtTotalSyohiZei.AutoSizeFromLength = true;
			this.txtTotalSyohiZei.BackColor = System.Drawing.Color.LightCyan;
			this.txtTotalSyohiZei.DisplayLength = null;
			this.txtTotalSyohiZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTotalSyohiZei.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTotalSyohiZei.Location = new System.Drawing.Point(269, 1);
			this.txtTotalSyohiZei.Margin = new System.Windows.Forms.Padding(4);
			this.txtTotalSyohiZei.MaxLength = 12;
			this.txtTotalSyohiZei.Name = "txtTotalSyohiZei";
			this.txtTotalSyohiZei.ReadOnly = true;
			this.txtTotalSyohiZei.Size = new System.Drawing.Size(119, 23);
			this.txtTotalSyohiZei.TabIndex = 5;
			this.txtTotalSyohiZei.TabStop = false;
			this.txtTotalSyohiZei.Text = "0";
			this.txtTotalSyohiZei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtTotalSuuryou
			// 
			this.txtTotalSuuryou.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.txtTotalSuuryou.AutoSizeFromLength = true;
			this.txtTotalSuuryou.BackColor = System.Drawing.Color.LightCyan;
			this.txtTotalSuuryou.DisplayLength = null;
			this.txtTotalSuuryou.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTotalSuuryou.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTotalSuuryou.Location = new System.Drawing.Point(269, 1);
			this.txtTotalSuuryou.Margin = new System.Windows.Forms.Padding(4);
			this.txtTotalSuuryou.MaxLength = 12;
			this.txtTotalSuuryou.Name = "txtTotalSuuryou";
			this.txtTotalSuuryou.ReadOnly = true;
			this.txtTotalSuuryou.Size = new System.Drawing.Size(119, 23);
			this.txtTotalSuuryou.TabIndex = 1;
			this.txtTotalSuuryou.TabStop = false;
			this.txtTotalSuuryou.Text = "0";
			this.txtTotalSuuryou.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblTotalGaku
			// 
			this.lblTotalGaku.BackColor = System.Drawing.Color.Silver;
			this.lblTotalGaku.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTotalGaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTotalGaku.Location = new System.Drawing.Point(0, 0);
			this.lblTotalGaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTotalGaku.Name = "lblTotalGaku";
			this.lblTotalGaku.Size = new System.Drawing.Size(390, 26);
			this.lblTotalGaku.TabIndex = 2;
			this.lblTotalGaku.Tag = "CHANGE";
			this.lblTotalGaku.Text = "水 揚 金 額";
			this.lblTotalGaku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSyohiZei
			// 
			this.lblSyohiZei.BackColor = System.Drawing.Color.Silver;
			this.lblSyohiZei.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblSyohiZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSyohiZei.Location = new System.Drawing.Point(0, 0);
			this.lblSyohiZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSyohiZei.Name = "lblSyohiZei";
			this.lblSyohiZei.Size = new System.Drawing.Size(390, 26);
			this.lblSyohiZei.TabIndex = 4;
			this.lblSyohiZei.Tag = "CHANGE";
			this.lblSyohiZei.Text = "消費税（外）";
			this.lblSyohiZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSyokei
			// 
			this.lblSyokei.BackColor = System.Drawing.Color.Silver;
			this.lblSyokei.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblSyokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSyokei.Location = new System.Drawing.Point(0, 0);
			this.lblSyokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSyokei.Name = "lblSyokei";
			this.lblSyokei.Size = new System.Drawing.Size(390, 26);
			this.lblSyokei.TabIndex = 0;
			this.lblSyokei.Tag = "CHANGE";
			this.lblSyokei.Text = "水 揚 数 量";
			this.lblSyokei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(709, 4);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(67, 23);
			this.txtMizuageShishoCd.TabIndex = 6;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(592, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(585, 33);
			this.lblMizuageShisho.TabIndex = 13;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "水揚支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblGyogyoushu
			// 
			this.lblGyogyoushu.BackColor = System.Drawing.Color.Silver;
			this.lblGyogyoushu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyogyoushu.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblGyogyoushu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGyogyoushu.Location = new System.Drawing.Point(774, 0);
			this.lblGyogyoushu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyogyoushu.Name = "lblGyogyoushu";
			this.lblGyogyoushu.Size = new System.Drawing.Size(407, 33);
			this.lblGyogyoushu.TabIndex = 21;
			this.lblGyogyoushu.Tag = "CHANGE";
			this.lblGyogyoushu.Text = "漁業種";
			this.lblGyogyoushu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label4.Location = new System.Drawing.Point(903, 3);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(274, 33);
			this.label4.TabIndex = 27;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "箱　数";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyogyoushuCd
			// 
			this.txtGyogyoushuCd.AutoSizeFromLength = true;
			this.txtGyogyoushuCd.DisplayLength = null;
			this.txtGyogyoushuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGyogyoushuCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtGyogyoushuCd.Location = new System.Drawing.Point(841, 4);
			this.txtGyogyoushuCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtGyogyoushuCd.MaxLength = 4;
			this.txtGyogyoushuCd.Name = "txtGyogyoushuCd";
			this.txtGyogyoushuCd.Size = new System.Drawing.Size(39, 23);
			this.txtGyogyoushuCd.TabIndex = 9;
			this.txtGyogyoushuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyogyoushuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyogyoushuCd_Validating);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.rdoBashoNm01);
			this.groupBox1.Controls.Add(this.rdoBashoNm02);
			this.groupBox1.Controls.Add(this.rdoBashoNm03);
			this.groupBox1.Location = new System.Drawing.Point(773, 67);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox1.Size = new System.Drawing.Size(295, 39);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Visible = false;
			// 
			// rdoBashoNm01
			// 
			this.rdoBashoNm01.Checked = true;
			this.rdoBashoNm01.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoBashoNm01.Location = new System.Drawing.Point(14, 11);
			this.rdoBashoNm01.Margin = new System.Windows.Forms.Padding(4);
			this.rdoBashoNm01.Name = "rdoBashoNm01";
			this.rdoBashoNm01.Size = new System.Drawing.Size(89, 23);
			this.rdoBashoNm01.TabIndex = 0;
			this.rdoBashoNm01.TabStop = true;
			this.rdoBashoNm01.Text = "県内";
			this.rdoBashoNm01.UseVisualStyleBackColor = true;
			// 
			// rdoBashoNm02
			// 
			this.rdoBashoNm02.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoBashoNm02.Location = new System.Drawing.Point(111, 11);
			this.rdoBashoNm02.Margin = new System.Windows.Forms.Padding(4);
			this.rdoBashoNm02.Name = "rdoBashoNm02";
			this.rdoBashoNm02.Size = new System.Drawing.Size(89, 23);
			this.rdoBashoNm02.TabIndex = 1;
			this.rdoBashoNm02.Text = "県外";
			this.rdoBashoNm02.UseVisualStyleBackColor = true;
			// 
			// rdoBashoNm03
			// 
			this.rdoBashoNm03.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdoBashoNm03.Location = new System.Drawing.Point(204, 9);
			this.rdoBashoNm03.Margin = new System.Windows.Forms.Padding(4);
			this.rdoBashoNm03.Name = "rdoBashoNm03";
			this.rdoBashoNm03.Size = new System.Drawing.Size(89, 23);
			this.rdoBashoNm03.TabIndex = 2;
			this.rdoBashoNm03.Text = "地元";
			this.rdoBashoNm03.UseVisualStyleBackColor = true;
			// 
			// lblZenkaiDenpyoBango
			// 
			this.lblZenkaiDenpyoBango.BackColor = System.Drawing.Color.White;
			this.lblZenkaiDenpyoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 10.5F, System.Drawing.FontStyle.Bold);
			this.lblZenkaiDenpyoBango.Location = new System.Drawing.Point(592, 4);
			this.lblZenkaiDenpyoBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZenkaiDenpyoBango.Name = "lblZenkaiDenpyoBango";
			this.lblZenkaiDenpyoBango.Size = new System.Drawing.Size(315, 24);
			this.lblZenkaiDenpyoBango.TabIndex = 2;
			this.lblZenkaiDenpyoBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMode
			// 
			this.lblMode.BackColor = System.Drawing.Color.White;
			this.lblMode.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblMode.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMode.Location = new System.Drawing.Point(0, 0);
			this.lblMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMode.Name = "lblMode";
			this.lblMode.Size = new System.Drawing.Size(96, 33);
			this.lblMode.TabIndex = 0;
			this.lblMode.Text = "【登録】";
			this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblShohizeiTenka2
			// 
			this.lblShohizeiTenka2.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiTenka2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiTenka2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiTenka2.Location = new System.Drawing.Point(3, 99);
			this.lblShohizeiTenka2.Margin = new System.Windows.Forms.Padding(2);
			this.lblShohizeiTenka2.Name = "lblShohizeiTenka2";
			this.lblShohizeiTenka2.Size = new System.Drawing.Size(385, 30);
			this.lblShohizeiTenka2.TabIndex = 3;
			this.lblShohizeiTenka2.Tag = "CHANGE";
			this.lblShohizeiTenka2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiInputHoho2
			// 
			this.lblShohizeiInputHoho2.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiInputHoho2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiInputHoho2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiInputHoho2.Location = new System.Drawing.Point(3, 35);
			this.lblShohizeiInputHoho2.Margin = new System.Windows.Forms.Padding(2);
			this.lblShohizeiInputHoho2.Name = "lblShohizeiInputHoho2";
			this.lblShohizeiInputHoho2.Size = new System.Drawing.Size(385, 27);
			this.lblShohizeiInputHoho2.TabIndex = 1;
			this.lblShohizeiInputHoho2.Tag = "CHANGE";
			this.lblShohizeiInputHoho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiTenka1
			// 
			this.lblShohizeiTenka1.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiTenka1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiTenka1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiTenka1.Location = new System.Drawing.Point(3, 67);
			this.lblShohizeiTenka1.Margin = new System.Windows.Forms.Padding(2);
			this.lblShohizeiTenka1.Name = "lblShohizeiTenka1";
			this.lblShohizeiTenka1.Size = new System.Drawing.Size(385, 27);
			this.lblShohizeiTenka1.TabIndex = 2;
			this.lblShohizeiTenka1.Tag = "CHANGE";
			this.lblShohizeiTenka1.Text = "消費税転嫁方法";
			this.lblShohizeiTenka1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiInputHoho1
			// 
			this.lblShohizeiInputHoho1.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiInputHoho1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiInputHoho1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiInputHoho1.Location = new System.Drawing.Point(3, 3);
			this.lblShohizeiInputHoho1.Margin = new System.Windows.Forms.Padding(2);
			this.lblShohizeiInputHoho1.Name = "lblShohizeiInputHoho1";
			this.lblShohizeiInputHoho1.Size = new System.Drawing.Size(385, 27);
			this.lblShohizeiInputHoho1.TabIndex = 0;
			this.lblShohizeiInputHoho1.Tag = "CHANGE";
			this.lblShohizeiInputHoho1.Text = "消費税入力方法";
			this.lblShohizeiInputHoho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dgvInputList
			// 
			this.dgvInputList.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
			this.dgvInputList.AllowUserToAddRows = false;
			this.dgvInputList.AllowUserToDeleteRows = false;
			this.dgvInputList.AllowUserToResizeColumns = false;
			this.dgvInputList.AllowUserToResizeRows = false;
			this.dgvInputList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dgvInputList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvInputList.ColumnHeadersHeight = 24;
			this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colGYO_NO,
            this.colYAMA_NO,
            this.colGYOSHU_CD,
            this.colGYOSHU_NM,
            this.HONSU,
            this.KIZU_NM,
            this.colSURYO,
            this.colTANKA,
            this.colKINGAKU,
            this.colSHOHIZEI,
            this.colPAYAO,
            this.colNAKAGAININ_CD,
            this.colNAKAGAININ_NM,
            this.SHIWAKE_CD,
            this.BUMON_CD,
            this.ZEI_KUBUN,
            this.JIGYO_KUBUN,
            this.ZEI_RITSU,
            this.GEN_TANKA,
            this.KAZEI_KUBUN,
            this.KIZU_CD,
            this.KEIGEN_FLG});
			this.dgvInputList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvInputList.EnableHeadersVisualStyles = false;
			this.dgvInputList.GridColor = System.Drawing.SystemColors.AppWorkspace;
			this.dgvInputList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvInputList.Location = new System.Drawing.Point(11, 217);
			this.dgvInputList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvInputList.MultiSelect = false;
			this.dgvInputList.Name = "dgvInputList";
			this.dgvInputList.RowHeadersVisible = false;
			this.dgvInputList.RowTemplate.Height = 21;
			this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.dgvInputList.Size = new System.Drawing.Size(1185, 174);
			this.dgvInputList.TabIndex = 11;
			this.dgvInputList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellContentClick);
			this.dgvInputList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellEnter);
			this.dgvInputList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvInputList_CellFormatting);
			this.dgvInputList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvInputList_CellMouseDown);
			this.dgvInputList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvInputList_Scroll);
			this.dgvInputList.Enter += new System.EventHandler(this.dgvInputList_Enter);
			this.dgvInputList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGridEdit_KeyDown);
			// 
			// colGYO_NO
			// 
			this.colGYO_NO.DataPropertyName = "GYO_NO";
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
			this.colGYO_NO.DefaultCellStyle = dataGridViewCellStyle2;
			this.colGYO_NO.HeaderText = "行";
			this.colGYO_NO.Name = "colGYO_NO";
			this.colGYO_NO.ReadOnly = true;
			this.colGYO_NO.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colGYO_NO.Width = 50;
			// 
			// colYAMA_NO
			// 
			this.colYAMA_NO.DataPropertyName = "YAMA_NO";
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
			this.colYAMA_NO.DefaultCellStyle = dataGridViewCellStyle3;
			this.colYAMA_NO.HeaderText = "山No";
			this.colYAMA_NO.MaxInputLength = 5;
			this.colYAMA_NO.Name = "colYAMA_NO";
			this.colYAMA_NO.ReadOnly = true;
			this.colYAMA_NO.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colYAMA_NO.Width = 50;
			// 
			// colGYOSHU_CD
			// 
			this.colGYOSHU_CD.DataPropertyName = "GYOSHU_CD";
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
			this.colGYOSHU_CD.DefaultCellStyle = dataGridViewCellStyle4;
			this.colGYOSHU_CD.HeaderText = "魚種CD";
			this.colGYOSHU_CD.MaxInputLength = 5;
			this.colGYOSHU_CD.Name = "colGYOSHU_CD";
			this.colGYOSHU_CD.ReadOnly = true;
			this.colGYOSHU_CD.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colGYOSHU_CD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.colGYOSHU_CD.Width = 70;
			// 
			// colGYOSHU_NM
			// 
			this.colGYOSHU_NM.DataPropertyName = "GYOSHU_NM";
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
			this.colGYOSHU_NM.DefaultCellStyle = dataGridViewCellStyle5;
			this.colGYOSHU_NM.HeaderText = "魚　種　名";
			this.colGYOSHU_NM.Name = "colGYOSHU_NM";
			this.colGYOSHU_NM.ReadOnly = true;
			this.colGYOSHU_NM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colGYOSHU_NM.Width = 140;
			// 
			// HONSU
			// 
			this.HONSU.DataPropertyName = "HONSU";
			dataGridViewCellStyle6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.HONSU.DefaultCellStyle = dataGridViewCellStyle6;
			this.HONSU.HeaderText = "本数";
			this.HONSU.Name = "HONSU";
			this.HONSU.ReadOnly = true;
			this.HONSU.Width = 50;
			// 
			// KIZU_NM
			// 
			this.KIZU_NM.DataPropertyName = "JYOUTAI_CD";
			dataGridViewCellStyle7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.KIZU_NM.DefaultCellStyle = dataGridViewCellStyle7;
			this.KIZU_NM.HeaderText = "状態";
			this.KIZU_NM.Name = "KIZU_NM";
			this.KIZU_NM.Width = 50;
			// 
			// colSURYO
			// 
			this.colSURYO.DataPropertyName = "SURYO";
			dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
			this.colSURYO.DefaultCellStyle = dataGridViewCellStyle8;
			this.colSURYO.HeaderText = "数量";
			this.colSURYO.MaxInputLength = 10;
			this.colSURYO.Name = "colSURYO";
			this.colSURYO.ReadOnly = true;
			this.colSURYO.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colSURYO.Width = 80;
			// 
			// colTANKA
			// 
			this.colTANKA.DataPropertyName = "TANKA";
			dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
			this.colTANKA.DefaultCellStyle = dataGridViewCellStyle9;
			this.colTANKA.HeaderText = "単価";
			this.colTANKA.MaxInputLength = 8;
			this.colTANKA.Name = "colTANKA";
			this.colTANKA.ReadOnly = true;
			this.colTANKA.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colTANKA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.colTANKA.Width = 80;
			// 
			// colKINGAKU
			// 
			this.colKINGAKU.DataPropertyName = "KINGAKU";
			dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
			this.colKINGAKU.DefaultCellStyle = dataGridViewCellStyle10;
			this.colKINGAKU.HeaderText = "金額";
			this.colKINGAKU.MaxInputLength = 11;
			this.colKINGAKU.Name = "colKINGAKU";
			this.colKINGAKU.ReadOnly = true;
			this.colKINGAKU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colKINGAKU.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.colKINGAKU.Width = 120;
			// 
			// colSHOHIZEI
			// 
			this.colSHOHIZEI.DataPropertyName = "SHOHIZEI";
			dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle11.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			dataGridViewCellStyle11.NullValue = null;
			dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
			this.colSHOHIZEI.DefaultCellStyle = dataGridViewCellStyle11;
			this.colSHOHIZEI.HeaderText = "消費税";
			this.colSHOHIZEI.MaxInputLength = 7;
			this.colSHOHIZEI.Name = "colSHOHIZEI";
			this.colSHOHIZEI.ReadOnly = true;
			this.colSHOHIZEI.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colSHOHIZEI.Width = 120;
			// 
			// colPAYAO
			// 
			this.colPAYAO.DataPropertyName = "PAYAO_NO";
			dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle12.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			dataGridViewCellStyle12.NullValue = null;
			dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
			this.colPAYAO.DefaultCellStyle = dataGridViewCellStyle12;
			this.colPAYAO.HeaderText = "ﾊﾟﾔｵ";
			this.colPAYAO.MaxInputLength = 4;
			this.colPAYAO.Name = "colPAYAO";
			this.colPAYAO.ReadOnly = true;
			this.colPAYAO.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colPAYAO.Width = 50;
			// 
			// colNAKAGAININ_CD
			// 
			this.colNAKAGAININ_CD.DataPropertyName = "NAKAGAININ_CD";
			dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle13.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			dataGridViewCellStyle13.NullValue = null;
			dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
			this.colNAKAGAININ_CD.DefaultCellStyle = dataGridViewCellStyle13;
			this.colNAKAGAININ_CD.HeaderText = "仲買CD";
			this.colNAKAGAININ_CD.MaxInputLength = 6;
			this.colNAKAGAININ_CD.Name = "colNAKAGAININ_CD";
			this.colNAKAGAININ_CD.ReadOnly = true;
			this.colNAKAGAININ_CD.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colNAKAGAININ_CD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.colNAKAGAININ_CD.Width = 80;
			// 
			// colNAKAGAININ_NM
			// 
			this.colNAKAGAININ_NM.DataPropertyName = "NAKAGAININ_NM";
			dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle14.NullValue = null;
			dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
			this.colNAKAGAININ_NM.DefaultCellStyle = dataGridViewCellStyle14;
			this.colNAKAGAININ_NM.HeaderText = "仲買人名";
			this.colNAKAGAININ_NM.Name = "colNAKAGAININ_NM";
			this.colNAKAGAININ_NM.ReadOnly = true;
			this.colNAKAGAININ_NM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.colNAKAGAININ_NM.Width = 120;
			// 
			// SHIWAKE_CD
			// 
			this.SHIWAKE_CD.DataPropertyName = "SHIWAKE_CD";
			this.SHIWAKE_CD.HeaderText = "仕訳コード";
			this.SHIWAKE_CD.Name = "SHIWAKE_CD";
			this.SHIWAKE_CD.Visible = false;
			// 
			// BUMON_CD
			// 
			this.BUMON_CD.DataPropertyName = "BUMON_CD";
			this.BUMON_CD.HeaderText = "部門コード";
			this.BUMON_CD.Name = "BUMON_CD";
			this.BUMON_CD.Visible = false;
			// 
			// ZEI_KUBUN
			// 
			this.ZEI_KUBUN.DataPropertyName = "ZEI_KUBUN";
			dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.ZEI_KUBUN.DefaultCellStyle = dataGridViewCellStyle15;
			this.ZEI_KUBUN.HeaderText = "区";
			this.ZEI_KUBUN.Name = "ZEI_KUBUN";
			this.ZEI_KUBUN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.ZEI_KUBUN.Width = 30;
			// 
			// JIGYO_KUBUN
			// 
			this.JIGYO_KUBUN.DataPropertyName = "JIGYO_KUBUN";
			this.JIGYO_KUBUN.HeaderText = "事業区分";
			this.JIGYO_KUBUN.Name = "JIGYO_KUBUN";
			this.JIGYO_KUBUN.Visible = false;
			// 
			// ZEI_RITSU
			// 
			this.ZEI_RITSU.DataPropertyName = "ZEI_RITSU";
			dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			dataGridViewCellStyle16.Format = "#,##0";
			this.ZEI_RITSU.DefaultCellStyle = dataGridViewCellStyle16;
			this.ZEI_RITSU.HeaderText = "税";
			this.ZEI_RITSU.Name = "ZEI_RITSU";
			this.ZEI_RITSU.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.ZEI_RITSU.Width = 50;
			// 
			// GEN_TANKA
			// 
			this.GEN_TANKA.HeaderText = "原単価";
			this.GEN_TANKA.Name = "GEN_TANKA";
			this.GEN_TANKA.Visible = false;
			// 
			// KAZEI_KUBUN
			// 
			this.KAZEI_KUBUN.DataPropertyName = "KAZEI_KUBUN";
			this.KAZEI_KUBUN.HeaderText = "課税区分";
			this.KAZEI_KUBUN.Name = "KAZEI_KUBUN";
			this.KAZEI_KUBUN.Visible = false;
			// 
			// KIZU_CD
			// 
			this.KIZU_CD.HeaderText = "傷名称";
			this.KIZU_CD.Name = "KIZU_CD";
			this.KIZU_CD.Visible = false;
			// 
			// KEIGEN_FLG
			// 
			this.KEIGEN_FLG.DataPropertyName = "KEIGEN_FLG";
			dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.KEIGEN_FLG.DefaultCellStyle = dataGridViewCellStyle17;
			this.KEIGEN_FLG.HeaderText = "軽";
			this.KEIGEN_FLG.Name = "KEIGEN_FLG";
			this.KEIGEN_FLG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.KEIGEN_FLG.Width = 30;
			// 
			// txtGridEdit
			// 
			this.txtGridEdit.AutoSizeFromLength = true;
			this.txtGridEdit.DisplayLength = null;
			this.txtGridEdit.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGridEdit.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtGridEdit.Location = new System.Drawing.Point(479, 270);
			this.txtGridEdit.Margin = new System.Windows.Forms.Padding(4);
			this.txtGridEdit.MaxLength = 10;
			this.txtGridEdit.Name = "txtGridEdit";
			this.txtGridEdit.Size = new System.Drawing.Size(100, 23);
			this.txtGridEdit.TabIndex = 12;
			this.txtGridEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGridEdit.Enter += new System.EventHandler(this.txtGridEdit_Enter);
			this.txtGridEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGridEdit_KeyDown);
			// 
			// txtSeisanKbn
			// 
			this.txtSeisanKbn.AutoSizeFromLength = true;
			this.txtSeisanKbn.DisplayLength = null;
			this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSeisanKbn.Location = new System.Drawing.Point(179, 4);
			this.txtSeisanKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanKbn.MaxLength = 1;
			this.txtSeisanKbn.Name = "txtSeisanKbn";
			this.txtSeisanKbn.Size = new System.Drawing.Size(67, 23);
			this.txtSeisanKbn.TabIndex = 1;
			this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanKbn.TextChanged += new System.EventHandler(this.txtSeisanKbn_TextChanged);
			this.txtSeisanKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSeisanKbn_KeyDown);
			this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
			// 
			// lblSeisanKbnNm
			// 
			this.lblSeisanKbnNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanKbnNm.Location = new System.Drawing.Point(253, 3);
			this.lblSeisanKbnNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
			this.lblSeisanKbnNm.Size = new System.Drawing.Size(334, 24);
			this.lblSeisanKbnNm.TabIndex = 65542;
			this.lblSeisanKbnNm.Tag = "DISPNAME";
			this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeisanKbnNmTitle
			// 
			this.lblSeisanKbnNmTitle.BackColor = System.Drawing.Color.Silver;
			this.lblSeisanKbnNmTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanKbnNmTitle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblSeisanKbnNmTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanKbnNmTitle.Location = new System.Drawing.Point(96, 0);
			this.lblSeisanKbnNmTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanKbnNmTitle.Name = "lblSeisanKbnNmTitle";
			this.lblSeisanKbnNmTitle.Size = new System.Drawing.Size(1081, 33);
			this.lblSeisanKbnNmTitle.TabIndex = 65540;
			this.lblSeisanKbnNmTitle.Tag = "CHANGE";
			this.lblSeisanKbnNmTitle.Text = "精算区分";
			this.lblSeisanKbnNmTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtHakosuu
			// 
			this.txtHakosuu.AutoSizeFromLength = true;
			this.txtHakosuu.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtHakosuu.DisplayLength = null;
			this.txtHakosuu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtHakosuu.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtHakosuu.Location = new System.Drawing.Point(970, 7);
			this.txtHakosuu.Margin = new System.Windows.Forms.Padding(4);
			this.txtHakosuu.MaxLength = 2;
			this.txtHakosuu.Name = "txtHakosuu";
			this.txtHakosuu.Size = new System.Drawing.Size(69, 23);
			this.txtHakosuu.TabIndex = 28;
			this.txtHakosuu.TabStop = false;
			this.txtHakosuu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtHakosuu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHakosuu_KeyDown);
			this.txtHakosuu.Validating += new System.ComponentModel.CancelEventHandler(this.txtHakosuu_Validating);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(11, 50);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 4;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1185, 164);
			this.tableLayoutPanel1.TabIndex = 65543;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.txtHakosuu);
			this.panel4.Controls.Add(this.label4);
			this.panel4.Controls.Add(this.txtNiukeninCd);
			this.panel4.Controls.Add(this.lblNiukeninNm);
			this.panel4.Controls.Add(this.lblNiukeninCd);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(4, 124);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(1177, 36);
			this.panel4.TabIndex = 3;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.txtFunanushiCd);
			this.panel3.Controls.Add(this.lblGyogyoushuNm);
			this.panel3.Controls.Add(this.txtGyogyoushuCd);
			this.panel3.Controls.Add(this.lblFunanushiNm);
			this.panel3.Controls.Add(this.lblGyogyoushu);
			this.panel3.Controls.Add(this.txtGyokyouTesuuryou);
			this.panel3.Controls.Add(this.lblGyokyouTesuuryou);
			this.panel3.Controls.Add(this.lblFunanushiCd);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(4, 84);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(1177, 33);
			this.panel3.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.txtSeriDays);
			this.panel2.Controls.Add(this.txtDenpyoBango);
			this.panel2.Controls.Add(this.txtMizuageShishoCd);
			this.panel2.Controls.Add(this.lblMizuageShishoNm);
			this.panel2.Controls.Add(this.lblMizuageShisho);
			this.panel2.Controls.Add(this.lblJp);
			this.panel2.Controls.Add(this.lblDenpyoNo);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(4, 44);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1177, 33);
			this.panel2.TabIndex = 1;
			// 
			// txtSeriDays
			// 
			this.txtSeriDays.Day = "";
			this.txtSeriDays.DbConnect = null;
			this.txtSeriDays.DispFormat = jp.co.fsi.ClientCommon.DispDateFormat.年月日;
			this.txtSeriDays.ExecBtn = null;
			this.txtSeriDays.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriDays.Gengo = "";
			this.txtSeriDays.JpDate = null;
			this.txtSeriDays.Location = new System.Drawing.Point(252, 1);
			this.txtSeriDays.Month = "";
			this.txtSeriDays.Name = "txtSeriDays";
			this.txtSeriDays.OpenDialogExe = null;
			this.txtSeriDays.SeirekiYmd = null;
			this.txtSeriDays.Size = new System.Drawing.Size(257, 28);
			this.txtSeriDays.SYear = "";
			this.txtSeriDays.TabIndex = 3;
			this.txtSeriDays.TabStop = true;
			this.txtSeriDays.Tag = "CHANGE";
			this.txtSeriDays.WYear = "";
			this.txtSeriDays.ChangeOrgnDate += new jp.co.fsi.ClientCommon.ChangeOrgnDateEventHandler(this.fsiDate1_ChangeOrgnDate);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.txtSeisanKbn);
			this.panel1.Controls.Add(this.lblSeisanKbnNm);
			this.panel1.Controls.Add(this.lblZenkaiDenpyoBango);
			this.panel1.Controls.Add(this.lblSeisanKbnNmTitle);
			this.panel1.Controls.Add(this.lblMode);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(4, 4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1177, 33);
			this.panel1.TabIndex = 0;
			// 
			// txtYear
			// 
			this.txtYear.AutoSizeFromLength = false;
			this.txtYear.DisplayLength = null;
			this.txtYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYear.Location = new System.Drawing.Point(767, 270);
			this.txtYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtYear.MaxLength = 2;
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(49, 23);
			this.txtYear.TabIndex = 3;
			this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtYear.Visible = false;
			// 
			// txtMonth
			// 
			this.txtMonth.AutoSizeFromLength = false;
			this.txtMonth.DisplayLength = null;
			this.txtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonth.Location = new System.Drawing.Point(853, 270);
			this.txtMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonth.MaxLength = 2;
			this.txtMonth.Name = "txtMonth";
			this.txtMonth.Size = new System.Drawing.Size(52, 23);
			this.txtMonth.TabIndex = 4;
			this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonth.Visible = false;
			// 
			// lblYear
			// 
			this.lblYear.AutoSize = true;
			this.lblYear.BackColor = System.Drawing.Color.Silver;
			this.lblYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYear.Location = new System.Drawing.Point(821, 274);
			this.lblYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(24, 16);
			this.lblYear.TabIndex = 8;
			this.lblYear.Tag = "CHANGE";
			this.lblYear.Text = "年";
			this.lblYear.Visible = false;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel2.ColumnCount = 1;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.Controls.Add(this.lblShohizeiTenka2, 0, 3);
			this.tableLayoutPanel2.Controls.Add(this.lblShohizeiInputHoho1, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.lblShohizeiTenka1, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.lblShohizeiInputHoho2, 0, 1);
			this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 398);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 4;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(391, 132);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel3.ColumnCount = 1;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel3.Controls.Add(this.panel10, 0, 3);
			this.tableLayoutPanel3.Controls.Add(this.panel9, 0, 2);
			this.tableLayoutPanel3.Controls.Add(this.panel8, 0, 1);
			this.tableLayoutPanel3.Controls.Add(this.panel7, 0, 0);
			this.tableLayoutPanel3.Location = new System.Drawing.Point(798, 396);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 4;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(398, 135);
			this.tableLayoutPanel3.TabIndex = 0;
			// 
			// panel10
			// 
			this.panel10.Controls.Add(this.txtTotalKingaku);
			this.panel10.Controls.Add(this.label3);
			this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel10.Location = new System.Drawing.Point(4, 103);
			this.panel10.Name = "panel10";
			this.panel10.Size = new System.Drawing.Size(390, 28);
			this.panel10.TabIndex = 65551;
			// 
			// panel9
			// 
			this.panel9.Controls.Add(this.txtTotalSyohiZei);
			this.panel9.Controls.Add(this.lblSyohiZei);
			this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel9.Location = new System.Drawing.Point(4, 70);
			this.panel9.Name = "panel9";
			this.panel9.Size = new System.Drawing.Size(390, 26);
			this.panel9.TabIndex = 65550;
			// 
			// panel8
			// 
			this.panel8.Controls.Add(this.txtTotalMizuageGaku);
			this.panel8.Controls.Add(this.lblTotalGaku);
			this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel8.Location = new System.Drawing.Point(4, 37);
			this.panel8.Name = "panel8";
			this.panel8.Size = new System.Drawing.Size(390, 26);
			this.panel8.TabIndex = 65549;
			// 
			// panel7
			// 
			this.panel7.Controls.Add(this.txtTotalSuuryou);
			this.panel7.Controls.Add(this.lblSyokei);
			this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel7.Location = new System.Drawing.Point(4, 4);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(390, 26);
			this.panel7.TabIndex = 65548;
			// 
			// txtDummy
			// 
			this.txtDummy.AutoSizeFromLength = true;
			this.txtDummy.DisplayLength = null;
			this.txtDummy.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDummy.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDummy.Location = new System.Drawing.Point(1100, 8);
			this.txtDummy.Margin = new System.Windows.Forms.Padding(4);
			this.txtDummy.MaxLength = 4;
			this.txtDummy.Name = "txtDummy";
			this.txtDummy.Size = new System.Drawing.Size(50, 20);
			this.txtDummy.TabIndex = 65539;
			this.txtDummy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(409, 398);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 1;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(383, 132);
			this.fsiTableLayoutPanel1.TabIndex = 65544;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.lblUchiwake);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(375, 124);
			this.fsiPanel1.TabIndex = 0;
			// 
			// lblUchiwake
			// 
			this.lblUchiwake.Location = new System.Drawing.Point(3, 22);
			this.lblUchiwake.Name = "lblUchiwake";
			this.lblUchiwake.Size = new System.Drawing.Size(361, 100);
			this.lblUchiwake.TabIndex = 1;
			this.lblUchiwake.Tag = "CHANGE";
			this.lblUchiwake.Text = "label2";
			// 
			// label1
			// 
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(375, 124);
			this.label1.TabIndex = 0;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "【内訳】";
			// 
			// HNSE1011
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1209, 678);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.tableLayoutPanel3);
			this.Controls.Add(this.tableLayoutPanel2);
			this.Controls.Add(this.txtGridEdit);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.dgvInputList);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.lblGengo);
			this.Controls.Add(this.txtYear);
			this.Controls.Add(this.txtMonth);
			this.Controls.Add(this.lblYear);
			this.Controls.Add(this.txtDay);
			this.Controls.Add(this.lblMonth);
			this.Controls.Add(this.lblDay);
			this.Controls.Add(this.txtDummy);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNSE1011";
			this.Text = "セリ入力";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_KeyDown);
			this.Controls.SetChildIndex(this.txtDummy, 0);
			this.Controls.SetChildIndex(this.lblDay, 0);
			this.Controls.SetChildIndex(this.lblMonth, 0);
			this.Controls.SetChildIndex(this.txtDay, 0);
			this.Controls.SetChildIndex(this.lblYear, 0);
			this.Controls.SetChildIndex(this.txtMonth, 0);
			this.Controls.SetChildIndex(this.txtYear, 0);
			this.Controls.SetChildIndex(this.lblGengo, 0);
			this.Controls.SetChildIndex(this.groupBox1, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.dgvInputList, 0);
			this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.txtGridEdit, 0);
			this.Controls.SetChildIndex(this.tableLayoutPanel2, 0);
			this.Controls.SetChildIndex(this.tableLayoutPanel3, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.pnlDebug.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel3.ResumeLayout(false);
			this.panel10.ResumeLayout(false);
			this.panel10.PerformLayout();
			this.panel9.ResumeLayout(false);
			this.panel9.PerformLayout();
			this.panel8.ResumeLayout(false);
			this.panel8.PerformLayout();
			this.panel7.ResumeLayout(false);
			this.panel7.PerformLayout();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNiukeninNm;
        private common.controls.FsiTextBox txtNiukeninCd;
        private System.Windows.Forms.Label lblNiukeninCd;
        private System.Windows.Forms.Label lblFunanushiNm;
        private common.controls.FsiTextBox txtFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCd;
        private System.Windows.Forms.Label lblGyogyoushuNm;
        private common.controls.FsiTextBox txtGyokyouTesuuryou;
        private System.Windows.Forms.Label lblGyokyouTesuuryou;
        private common.controls.FsiTextBox txtDenpyoBango;
        private System.Windows.Forms.Label lblDenpyoNo;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private common.controls.FsiTextBox txtDay;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private common.controls.FsiTextBox txtTotalMizuageGaku;
        private common.controls.FsiTextBox txtTotalSyohiZei;
        private common.controls.FsiTextBox txtTotalSuuryou;
        private System.Windows.Forms.Label lblTotalGaku;
        private System.Windows.Forms.Label lblSyohiZei;
        private System.Windows.Forms.Label lblSyokei;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label lblGyogyoushu;
        private System.Windows.Forms.Label label4;
        private common.controls.FsiTextBox txtGyogyoushuCd;
        private common.controls.FsiTextBox txtTotalKingaku;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoBashoNm03;
        private System.Windows.Forms.RadioButton rdoBashoNm02;
        private System.Windows.Forms.RadioButton rdoBashoNm01;
        private System.Windows.Forms.Label lblZenkaiDenpyoBango;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label lblShohizeiTenka2;
        private System.Windows.Forms.Label lblShohizeiInputHoho2;
        private System.Windows.Forms.Label lblShohizeiTenka1;
        private System.Windows.Forms.Label lblShohizeiInputHoho1;
        private System.Windows.Forms.DataGridView dgvInputList;
        private common.controls.FsiTextBox txtGridEdit;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label lblSeisanKbnNm;
        private System.Windows.Forms.Label lblSeisanKbnNmTitle;
        private common.controls.FsiTextBox txtHakosuu;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel1;
        private jp.co.fsi.common.FsiPanel panel4;
        private jp.co.fsi.common.FsiPanel panel3;
        private jp.co.fsi.common.FsiPanel panel2;
        private jp.co.fsi.common.FsiPanel panel1;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel2;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel3;
        private jp.co.fsi.common.FsiPanel panel10;
        private jp.co.fsi.common.FsiPanel panel9;
        private jp.co.fsi.common.FsiPanel panel8;
        private jp.co.fsi.common.FsiPanel panel7;
        private common.controls.FsiTextBox txtDummy;
        private common.controls.FsiTextBox txtYear;
        private common.controls.FsiTextBox txtMonth;
        private System.Windows.Forms.Label lblYear;
        private ClientCommon.FsiDate txtSeriDays;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblUchiwake;
		private System.Windows.Forms.DataGridViewTextBoxColumn colGYO_NO;
		private System.Windows.Forms.DataGridViewTextBoxColumn colYAMA_NO;
		private System.Windows.Forms.DataGridViewTextBoxColumn colGYOSHU_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn colGYOSHU_NM;
		private System.Windows.Forms.DataGridViewTextBoxColumn HONSU;
		private System.Windows.Forms.DataGridViewTextBoxColumn KIZU_NM;
		private System.Windows.Forms.DataGridViewTextBoxColumn colSURYO;
		private System.Windows.Forms.DataGridViewTextBoxColumn colTANKA;
		private System.Windows.Forms.DataGridViewTextBoxColumn colKINGAKU;
		private System.Windows.Forms.DataGridViewTextBoxColumn colSHOHIZEI;
		private System.Windows.Forms.DataGridViewTextBoxColumn colPAYAO;
		private System.Windows.Forms.DataGridViewTextBoxColumn colNAKAGAININ_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn colNAKAGAININ_NM;
		private System.Windows.Forms.DataGridViewTextBoxColumn SHIWAKE_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn BUMON_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn JIGYO_KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_RITSU;
		private System.Windows.Forms.DataGridViewTextBoxColumn GEN_TANKA;
		private System.Windows.Forms.DataGridViewTextBoxColumn KAZEI_KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn KIZU_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn KEIGEN_FLG;
	}
}