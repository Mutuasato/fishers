﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnse1011
{
    public sealed class GridCursorControl
    {
        const int COL_YAMA_NO = 1;            // 山No.
        const int COL_GYOSHU_CD = 2;          // 魚種CD
        const int COL_GYOSHU_NM = 3;          // 魚種名
        const int COL_HONSU = 4;              // 本数
        const int COL_KIZY_CD = 5;            // 傷CD
        const int COL_SURYO = 6;              // 水揚数量
        const int COL_TANKA = 7;              // 水揚単価
        const int COL_KINGAKU = 8;            // 水揚金額
        const int COL_SHOHIZEI = 9;           // 消費税
        const int COL_PAYAO_NO = 10;          // パヤオNO
        const int COL_NAKAGAININ_CD = 11;     // 仲買CD
        const int COL_NAKAGAININ_NM = 12;     // 仲買人名
		const int COL_KEIFLG = 21;            // 軽減税率フラグ

		private static readonly GridCursorControl instance = new GridCursorControl();
        private static Dictionary<int, bool> cursorList;

        private GridCursorControl() { }

        public static GridCursorControl GetInstance(ConfigLoader config)
        {
            // リスト設定
            SettingList(config);

            return instance;
        }

        /// <summary>
        /// セリ入力カーソル制御
        /// 本数、状態CD、パヤオのみ
        /// </summary>
        /// <param name="currentCol">現在の列</param>
        /// <returns>次の対象列</returns>
        public int GetNextCol(int currentCol)
        {
            bool f = false;
            int col = -1;
            foreach (KeyValuePair<int, bool> c in cursorList)
            {
                if (c.Key == currentCol)
                    f = true;
                else
                {
                    if (f && c.Value)
                        return c.Key;
                }
            }
            return col;
        }

        static void SettingList(ConfigLoader config)
        {
            // 初期設定（全て）
            cursorList = new Dictionary<int, bool>();
            cursorList.Add(COL_YAMA_NO, true);       // 設定不可
            cursorList.Add(COL_GYOSHU_CD, true);     // 設定不可
            cursorList.Add(COL_GYOSHU_NM, false);    // 設定不可
            cursorList.Add(COL_HONSU, true);
            cursorList.Add(COL_KIZY_CD, true);
            cursorList.Add(COL_SURYO, true);         // 設定不可
            cursorList.Add(COL_TANKA, true);         // 設定不可
            cursorList.Add(COL_KINGAKU, false);      // 設定不可
            cursorList.Add(COL_SHOHIZEI, false);     // 設定不可
            cursorList.Add(COL_PAYAO_NO, true);
            cursorList.Add(COL_NAKAGAININ_CD, true); // 設定不可
			cursorList.Add(COL_KEIFLG, true); // 設定不可

			try
			{
                cursorList[COL_HONSU] = "1".Equals(config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk03")) ? true : false;
                cursorList[COL_KIZY_CD] = "1".Equals(config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk04")) ? true : false;
                cursorList[COL_PAYAO_NO] = "1".Equals(config.LoadPgConfig(Constants.SubSys.Han, "HNSE1011", "Setting", "Chk07")) ? true : false;
            }
            catch (Exception){ }
        }
    }
}
