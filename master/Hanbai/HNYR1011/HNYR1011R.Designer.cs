﻿namespace jp.co.fsi.hn.hnyr1011
{
    /// <summary>
    /// HNYR1011R の概要の説明です。
    /// </summary>
    partial class HNYR1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNYR1011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.テキスト001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキストpage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線123 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト002 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト003 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト004 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト005 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト006 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト007 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト008 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト009 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト010 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト012 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト013 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト014 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト015 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト028 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト029 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト016 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト017 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト018 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト019 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト020 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト021 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト022 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト023 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト024 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト025 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト026 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト027 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト030 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.ラベル20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト031 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト032 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト033 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト034 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト035 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト036 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト037 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト038 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト039 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト040 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト041 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト042 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト055 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト056 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト043 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト044 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト045 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト046 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト047 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト048 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト049 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト050 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト051 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト052 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト053 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキス054 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト057 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線119 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線120 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキストpage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト028)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト029)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト016)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト017)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト018)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト019)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト020)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト021)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト022)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト023)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト024)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト025)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト026)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト027)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト030)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト031)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト032)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト033)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト034)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト035)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト036)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト037)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト038)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト039)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト040)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト041)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト042)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト055)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト056)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト043)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト044)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト045)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト046)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト047)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト048)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト049)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト050)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト051)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト052)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト053)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキス054)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト057)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト001,
            this.txtToday,
            this.テキストpage,
            this.ラベル1,
            this.ラベル3,
            this.直線2,
            this.ラベル33,
            this.ラベル34,
            this.ラベル35,
            this.ラベル36,
            this.ラベル37,
            this.ラベル38,
            this.ラベル39,
            this.ラベル40,
            this.ラベル41,
            this.ラベル42,
            this.ラベル43,
            this.ラベル44,
            this.ラベル45,
            this.ラベル46,
            this.ラベル47,
            this.ラベル48,
            this.ラベル49,
            this.ラベル50,
            this.ラベル51,
            this.ラベル52,
            this.ラベル53,
            this.ラベル54,
            this.ラベル55,
            this.ラベル56,
            this.ラベル61,
            this.ラベル62,
            this.ラベル63,
            this.ラベル64,
            this.ラベル65,
            this.直線123});
            this.pageHeader.Height = 1.176389F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // テキスト001
            // 
            this.テキスト001.DataField = "ITEM01";
            this.テキスト001.Height = 0.15625F;
            this.テキスト001.Left = 0.5520833F;
            this.テキスト001.Name = "テキスト001";
            this.テキスト001.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト001.Tag = "";
            this.テキスト001.Text = "ITEM01";
            this.テキスト001.Top = 0.07291666F;
            this.テキスト001.Width = 0.9857121F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.15625F;
            this.txtToday.Left = 10.01024F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.txtToday.Tag = "";
            this.txtToday.Text = "=Now()";
            this.txtToday.Top = 0F;
            this.txtToday.Width = 1.32726F;
            // 
            // テキストpage
            // 
            this.テキストpage.Height = 0.15625F;
            this.テキストpage.Left = 11.99173F;
            this.テキストpage.Name = "テキストpage";
            this.テキストpage.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキストpage.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.テキストpage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキストpage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.テキストpage.Tag = "";
            this.テキストpage.Text = "=[Page]";
            this.テキストpage.Top = 0F;
            this.テキストpage.Width = 0.6268216F;
            // 
            // ラベル1
            // 
            this.ラベル1.Height = 0.15625F;
            this.ラベル1.HyperLink = null;
            this.ラベル1.Left = 0.8771654F;
            this.ラベル1.Name = "ラベル1";
            this.ラベル1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル1.Tag = "";
            this.ラベル1.Text = "船主名称";
            this.ラベル1.Top = 0.4409449F;
            this.ラベル1.Width = 0.5520833F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.15625F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 3.625F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "1月数量";
            this.ラベル3.Top = 0.4409722F;
            this.ラベル3.Width = 0.4895833F;
            // 
            // 直線2
            // 
            this.直線2.Height = 0F;
            this.直線2.Left = 0F;
            this.直線2.LineWeight = 0F;
            this.直線2.Name = "直線2";
            this.直線2.Tag = "";
            this.直線2.Top = 1.176389F;
            this.直線2.Width = 13.19097F;
            this.直線2.X1 = 0F;
            this.直線2.X2 = 13.19097F;
            this.直線2.Y1 = 1.176389F;
            this.直線2.Y2 = 1.176389F;
            // 
            // ラベル33
            // 
            this.ラベル33.Height = 0.15625F;
            this.ラベル33.HyperLink = null;
            this.ラベル33.Left = 0.3062992F;
            this.ラベル33.Name = "ラベル33";
            this.ラベル33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル33.Tag = "";
            this.ラベル33.Text = "船主CD";
            this.ラベル33.Top = 0.4409449F;
            this.ラベル33.Width = 0.4270833F;
            // 
            // ラベル34
            // 
            this.ラベル34.Height = 0.15625F;
            this.ラベル34.HyperLink = null;
            this.ラベル34.Left = 3.625F;
            this.ラベル34.Name = "ラベル34";
            this.ラベル34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル34.Tag = "";
            this.ラベル34.Text = "7月数量";
            this.ラベル34.Top = 0.6284722F;
            this.ラベル34.Width = 0.4895833F;
            // 
            // ラベル35
            // 
            this.ラベル35.Height = 0.15625F;
            this.ラベル35.HyperLink = null;
            this.ラベル35.Left = 3.385F;
            this.ラベル35.Name = "ラベル35";
            this.ラベル35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル35.Tag = "";
            this.ラベル35.Text = "1月金額";
            this.ラベル35.Top = 0.8159722F;
            this.ラベル35.Width = 0.7295833F;
            // 
            // ラベル36
            // 
            this.ラベル36.Height = 0.15625F;
            this.ラベル36.HyperLink = null;
            this.ラベル36.Left = 3.385F;
            this.ラベル36.Name = "ラベル36";
            this.ラベル36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル36.Tag = "";
            this.ラベル36.Text = "7月金額";
            this.ラベル36.Top = 0.9930556F;
            this.ラベル36.Width = 0.7295833F;
            // 
            // ラベル37
            // 
            this.ラベル37.Height = 0.15625F;
            this.ラベル37.HyperLink = null;
            this.ラベル37.Left = 5.01875F;
            this.ラベル37.Name = "ラベル37";
            this.ラベル37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル37.Tag = "";
            this.ラベル37.Text = "2月数量";
            this.ラベル37.Top = 0.4409722F;
            this.ラベル37.Width = 0.4895833F;
            // 
            // ラベル38
            // 
            this.ラベル38.Height = 0.15625F;
            this.ラベル38.HyperLink = null;
            this.ラベル38.Left = 5.01875F;
            this.ラベル38.Name = "ラベル38";
            this.ラベル38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル38.Tag = "";
            this.ラベル38.Text = "8月数量";
            this.ラベル38.Top = 0.6284722F;
            this.ラベル38.Width = 0.4895833F;
            // 
            // ラベル39
            // 
            this.ラベル39.Height = 0.15625F;
            this.ラベル39.HyperLink = null;
            this.ラベル39.Left = 4.708333F;
            this.ラベル39.Name = "ラベル39";
            this.ラベル39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル39.Tag = "";
            this.ラベル39.Text = "2月金額";
            this.ラベル39.Top = 0.8159722F;
            this.ラベル39.Width = 0.8020833F;
            // 
            // ラベル40
            // 
            this.ラベル40.Height = 0.15625F;
            this.ラベル40.HyperLink = null;
            this.ラベル40.Left = 4.708333F;
            this.ラベル40.Name = "ラベル40";
            this.ラベル40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル40.Tag = "";
            this.ラベル40.Text = "8月金額";
            this.ラベル40.Top = 0.9930556F;
            this.ラベル40.Width = 0.8020833F;
            // 
            // ラベル41
            // 
            this.ラベル41.Height = 0.15625F;
            this.ラベル41.HyperLink = null;
            this.ラベル41.Left = 6.397222F;
            this.ラベル41.Name = "ラベル41";
            this.ラベル41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル41.Tag = "";
            this.ラベル41.Text = "3月数量";
            this.ラベル41.Top = 0.4409722F;
            this.ラベル41.Width = 0.4895833F;
            // 
            // ラベル42
            // 
            this.ラベル42.Height = 0.15625F;
            this.ラベル42.HyperLink = null;
            this.ラベル42.Left = 6.397222F;
            this.ラベル42.Name = "ラベル42";
            this.ラベル42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル42.Tag = "";
            this.ラベル42.Text = "9月数量";
            this.ラベル42.Top = 0.6284722F;
            this.ラベル42.Width = 0.4895833F;
            // 
            // ラベル43
            // 
            this.ラベル43.Height = 0.15625F;
            this.ラベル43.HyperLink = null;
            this.ラベル43.Left = 6.083333F;
            this.ラベル43.Name = "ラベル43";
            this.ラベル43.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル43.Tag = "";
            this.ラベル43.Text = "3月金額";
            this.ラベル43.Top = 0.8159722F;
            this.ラベル43.Width = 0.8020833F;
            // 
            // ラベル44
            // 
            this.ラベル44.Height = 0.15625F;
            this.ラベル44.HyperLink = null;
            this.ラベル44.Left = 6.083333F;
            this.ラベル44.Name = "ラベル44";
            this.ラベル44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル44.Tag = "";
            this.ラベル44.Text = "9月金額";
            this.ラベル44.Top = 0.9930556F;
            this.ラベル44.Width = 0.8020833F;
            // 
            // ラベル45
            // 
            this.ラベル45.Height = 0.15625F;
            this.ラベル45.HyperLink = null;
            this.ラベル45.Left = 7.772222F;
            this.ラベル45.Name = "ラベル45";
            this.ラベル45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル45.Tag = "";
            this.ラベル45.Text = "4月数量";
            this.ラベル45.Top = 0.4409722F;
            this.ラベル45.Width = 0.4895833F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.15625F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 7.709722F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "10月数量";
            this.ラベル46.Top = 0.6284722F;
            this.ラベル46.Width = 0.5520833F;
            // 
            // ラベル47
            // 
            this.ラベル47.Height = 0.15625F;
            this.ラベル47.HyperLink = null;
            this.ラベル47.Left = 7.458333F;
            this.ラベル47.Name = "ラベル47";
            this.ラベル47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル47.Tag = "";
            this.ラベル47.Text = "4月金額";
            this.ラベル47.Top = 0.8159722F;
            this.ラベル47.Width = 0.8020833F;
            // 
            // ラベル48
            // 
            this.ラベル48.Height = 0.15625F;
            this.ラベル48.HyperLink = null;
            this.ラベル48.Left = 7.385417F;
            this.ラベル48.Name = "ラベル48";
            this.ラベル48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル48.Tag = "";
            this.ラベル48.Text = "10月金額";
            this.ラベル48.Top = 0.9930556F;
            this.ラベル48.Width = 0.875F;
            // 
            // ラベル49
            // 
            this.ラベル49.Height = 0.15625F;
            this.ラベル49.HyperLink = null;
            this.ラベル49.Left = 9.150695F;
            this.ラベル49.Name = "ラベル49";
            this.ラベル49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル49.Tag = "";
            this.ラベル49.Text = "5月数量";
            this.ラベル49.Top = 0.4409722F;
            this.ラベル49.Width = 0.4895833F;
            // 
            // ラベル50
            // 
            this.ラベル50.Height = 0.15625F;
            this.ラベル50.HyperLink = null;
            this.ラベル50.Left = 9.088195F;
            this.ラベル50.Name = "ラベル50";
            this.ラベル50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル50.Tag = "";
            this.ラベル50.Text = "11月数量";
            this.ラベル50.Top = 0.6284722F;
            this.ラベル50.Width = 0.5520833F;
            // 
            // ラベル51
            // 
            this.ラベル51.Height = 0.15625F;
            this.ラベル51.HyperLink = null;
            this.ラベル51.Left = 8.833333F;
            this.ラベル51.Name = "ラベル51";
            this.ラベル51.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル51.Tag = "";
            this.ラベル51.Text = "5月金額";
            this.ラベル51.Top = 0.8159722F;
            this.ラベル51.Width = 0.8020833F;
            // 
            // ラベル52
            // 
            this.ラベル52.Height = 0.15625F;
            this.ラベル52.HyperLink = null;
            this.ラベル52.Left = 8.760417F;
            this.ラベル52.Name = "ラベル52";
            this.ラベル52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル52.Tag = "";
            this.ラベル52.Text = "11月金額";
            this.ラベル52.Top = 0.9930556F;
            this.ラベル52.Width = 0.875F;
            // 
            // ラベル53
            // 
            this.ラベル53.Height = 0.15625F;
            this.ラベル53.HyperLink = null;
            this.ラベル53.Left = 10.52847F;
            this.ラベル53.Name = "ラベル53";
            this.ラベル53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル53.Tag = "";
            this.ラベル53.Text = "6月数量";
            this.ラベル53.Top = 0.4409722F;
            this.ラベル53.Width = 0.4895833F;
            // 
            // ラベル54
            // 
            this.ラベル54.Height = 0.15625F;
            this.ラベル54.HyperLink = null;
            this.ラベル54.Left = 10.46597F;
            this.ラベル54.Name = "ラベル54";
            this.ラベル54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル54.Tag = "";
            this.ラベル54.Text = "12月数量";
            this.ラベル54.Top = 0.6284722F;
            this.ラベル54.Width = 0.5520833F;
            // 
            // ラベル55
            // 
            this.ラベル55.Height = 0.15625F;
            this.ラベル55.HyperLink = null;
            this.ラベル55.Left = 10.08333F;
            this.ラベル55.Name = "ラベル55";
            this.ラベル55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル55.Tag = "";
            this.ラベル55.Text = "6月金額";
            this.ラベル55.Top = 0.8159722F;
            this.ラベル55.Width = 0.9375F;
            // 
            // ラベル56
            // 
            this.ラベル56.Height = 0.15625F;
            this.ラベル56.HyperLink = null;
            this.ラベル56.Left = 10.14583F;
            this.ラベル56.Name = "ラベル56";
            this.ラベル56.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル56.Tag = "";
            this.ラベル56.Text = "12月金額";
            this.ラベル56.Top = 0.9930556F;
            this.ラベル56.Width = 0.875F;
            // 
            // ラベル61
            // 
            this.ラベル61.Height = 0.15625F;
            this.ラベル61.HyperLink = null;
            this.ラベル61.Left = 11.99167F;
            this.ラベル61.Name = "ラベル61";
            this.ラベル61.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル61.Tag = "";
            this.ラベル61.Text = "年間水揚数量";
            this.ラベル61.Top = 0.4409722F;
            this.ラベル61.Width = 0.8020833F;
            // 
            // ラベル62
            // 
            this.ラベル62.Height = 0.15625F;
            this.ラベル62.HyperLink = null;
            this.ラベル62.Left = 11.99167F;
            this.ラベル62.Name = "ラベル62";
            this.ラベル62.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル62.Tag = "";
            this.ラベル62.Text = "年間税込金額";
            this.ラベル62.Top = 0.6284722F;
            this.ラベル62.Width = 0.8020833F;
            // 
            // ラベル63
            // 
            this.ラベル63.Height = 0.15625F;
            this.ラベル63.HyperLink = null;
            this.ラベル63.Left = 11.80417F;
            this.ラベル63.Name = "ラベル63";
            this.ラベル63.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 128";
            this.ラベル63.Tag = "";
            this.ラベル63.Text = "（　年間手数料　）";
            this.ラベル63.Top = 0.9930556F;
            this.ラベル63.Width = 1.177083F;
            // 
            // ラベル64
            // 
            this.ラベル64.Height = 0.15625F;
            this.ラベル64.HyperLink = null;
            this.ラベル64.Left = 11.34514F;
            this.ラベル64.Name = "ラベル64";
            this.ラベル64.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル64.Tag = "";
            this.ラベル64.Text = "作成";
            this.ラベル64.Top = 0F;
            this.ラベル64.Width = 0.3020833F;
            // 
            // ラベル65
            // 
            this.ラベル65.Height = 0.25F;
            this.ラベル65.HyperLink = null;
            this.ラベル65.Left = 5.6875F;
            this.ラベル65.Name = "ラベル65";
            this.ラベル65.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル65.Tag = "";
            this.ラベル65.Text = "組合員別水揚年計表";
            this.ラベル65.Top = 0F;
            this.ラベル65.Width = 2.052083F;
            // 
            // 直線123
            // 
            this.直線123.Height = 0F;
            this.直線123.Left = 0F;
            this.直線123.LineWeight = 0F;
            this.直線123.Name = "直線123";
            this.直線123.Tag = "";
            this.直線123.Top = 0.3958333F;
            this.直線123.Width = 13.19097F;
            this.直線123.X1 = 0F;
            this.直線123.X2 = 13.19097F;
            this.直線123.Y1 = 0.3958333F;
            this.直線123.Y2 = 0.3958333F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM31";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト002,
            this.テキスト003,
            this.テキスト004,
            this.テキスト005,
            this.テキスト006,
            this.テキスト007,
            this.テキスト008,
            this.テキスト009,
            this.テキスト010,
            this.テキスト011,
            this.テキスト012,
            this.テキスト013,
            this.テキスト014,
            this.テキスト015,
            this.テキスト028,
            this.テキスト029,
            this.テキスト016,
            this.テキスト017,
            this.テキスト018,
            this.テキスト019,
            this.テキスト020,
            this.テキスト021,
            this.テキスト022,
            this.テキスト023,
            this.テキスト024,
            this.テキスト025,
            this.テキスト026,
            this.テキスト027,
            this.テキスト030,
            this.直線90,
            this.ラベル121,
            this.直線89});
            this.detail.Height = 0.6979166F;
            this.detail.Name = "detail";
            // 
            // テキスト002
            // 
            this.テキスト002.DataField = "ITEM02";
            this.テキスト002.Height = 0.15625F;
            this.テキスト002.Left = 0.1771654F;
            this.テキスト002.Name = "テキスト002";
            this.テキスト002.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト002.Tag = "";
            this.テキスト002.Text = "ITEM02";
            this.テキスト002.Top = 0.03937008F;
            this.テキスト002.Width = 0.5562499F;
            // 
            // テキスト003
            // 
            this.テキスト003.DataField = "ITEM03";
            this.テキスト003.Height = 0.15625F;
            this.テキスト003.Left = 0.8771654F;
            this.テキスト003.Name = "テキスト003";
            this.テキスト003.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト003.Tag = "";
            this.テキスト003.Text = "ITEM03";
            this.テキスト003.Top = 0.03937008F;
            this.テキスト003.Width = 1.9F;
            // 
            // テキスト004
            // 
            this.テキスト004.DataField = "ITEM04";
            this.テキスト004.Height = 0.15625F;
            this.テキスト004.Left = 2.808447F;
            this.テキスト004.Name = "テキスト004";
            this.テキスト004.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト004.Tag = "";
            this.テキスト004.Text = "ITEM04";
            this.テキスト004.Top = 0.03937008F;
            this.テキスト004.Width = 1.306218F;
            // 
            // テキスト005
            // 
            this.テキスト005.DataField = "ITEM05";
            this.テキスト005.Height = 0.15625F;
            this.テキスト005.Left = 4.208448F;
            this.テキスト005.Name = "テキスト005";
            this.テキスト005.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト005.Tag = "";
            this.テキスト005.Text = "ITEM05";
            this.テキスト005.Top = 0.03937008F;
            this.テキスト005.Width = 1.306218F;
            // 
            // テキスト006
            // 
            this.テキスト006.DataField = "ITEM06";
            this.テキスト006.Height = 0.15625F;
            this.テキスト006.Left = 5.580669F;
            this.テキスト006.Name = "テキスト006";
            this.テキスト006.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト006.Tag = "";
            this.テキスト006.Text = "ITEM06";
            this.テキスト006.Top = 0.03937008F;
            this.テキスト006.Width = 1.306218F;
            // 
            // テキスト007
            // 
            this.テキスト007.DataField = "ITEM07";
            this.テキスト007.Height = 0.15625F;
            this.テキスト007.Left = 6.955669F;
            this.テキスト007.Name = "テキスト007";
            this.テキスト007.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト007.Tag = "";
            this.テキスト007.Text = "ITEM07";
            this.テキスト007.Top = 0.03937008F;
            this.テキスト007.Width = 1.306217F;
            // 
            // テキスト008
            // 
            this.テキスト008.DataField = "ITEM08";
            this.テキスト008.Height = 0.15625F;
            this.テキスト008.Left = 8.334143F;
            this.テキスト008.Name = "テキスト008";
            this.テキスト008.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト008.Tag = "";
            this.テキスト008.Text = "ITEM08";
            this.テキスト008.Top = 0.03937008F;
            this.テキスト008.Width = 1.306217F;
            // 
            // テキスト009
            // 
            this.テキスト009.DataField = "ITEM09";
            this.テキスト009.Height = 0.15625F;
            this.テキスト009.Left = 9.71192F;
            this.テキスト009.Name = "テキスト009";
            this.テキスト009.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト009.Tag = "";
            this.テキスト009.Text = "ITEM09";
            this.テキスト009.Top = 0.03937008F;
            this.テキスト009.Width = 1.306217F;
            // 
            // テキスト010
            // 
            this.テキスト010.DataField = "ITEM10";
            this.テキスト010.Height = 0.15625F;
            this.テキスト010.Left = 2.812614F;
            this.テキスト010.Name = "テキスト010";
            this.テキスト010.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト010.Tag = "";
            this.テキスト010.Text = "ITEM10";
            this.テキスト010.Top = 0.1956201F;
            this.テキスト010.Width = 1.306218F;
            // 
            // テキスト011
            // 
            this.テキスト011.DataField = "ITEM11";
            this.テキスト011.Height = 0.15625F;
            this.テキスト011.Left = 4.212615F;
            this.テキスト011.Name = "テキスト011";
            this.テキスト011.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト011.Tag = "";
            this.テキスト011.Text = "ITEM11";
            this.テキスト011.Top = 0.1956201F;
            this.テキスト011.Width = 1.306218F;
            // 
            // テキスト012
            // 
            this.テキスト012.DataField = "ITEM12";
            this.テキスト012.Height = 0.15625F;
            this.テキスト012.Left = 5.584836F;
            this.テキスト012.Name = "テキスト012";
            this.テキスト012.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト012.Tag = "";
            this.テキスト012.Text = "ITEM12";
            this.テキスト012.Top = 0.1956201F;
            this.テキスト012.Width = 1.306218F;
            // 
            // テキスト013
            // 
            this.テキスト013.DataField = "ITEM13";
            this.テキスト013.Height = 0.15625F;
            this.テキスト013.Left = 6.959836F;
            this.テキスト013.Name = "テキスト013";
            this.テキスト013.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト013.Tag = "";
            this.テキスト013.Text = "ITEM13";
            this.テキスト013.Top = 0.1956201F;
            this.テキスト013.Width = 1.306218F;
            // 
            // テキスト014
            // 
            this.テキスト014.DataField = "ITEM14";
            this.テキスト014.Height = 0.15625F;
            this.テキスト014.Left = 8.338309F;
            this.テキスト014.Name = "テキスト014";
            this.テキスト014.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト014.Tag = "";
            this.テキスト014.Text = "ITEM14";
            this.テキスト014.Top = 0.1956201F;
            this.テキスト014.Width = 1.306217F;
            // 
            // テキスト015
            // 
            this.テキスト015.DataField = "ITEM15";
            this.テキスト015.Height = 0.15625F;
            this.テキスト015.Left = 9.716086F;
            this.テキスト015.Name = "テキスト015";
            this.テキスト015.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト015.Tag = "";
            this.テキスト015.Text = "ITEM15";
            this.テキスト015.Top = 0.1956201F;
            this.テキスト015.Width = 1.306217F;
            // 
            // テキスト028
            // 
            this.テキスト028.DataField = "ITEM28";
            this.テキスト028.Height = 0.15625F;
            this.テキスト028.Left = 11.69803F;
            this.テキスト028.Name = "テキスト028";
            this.テキスト028.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト028.Tag = "";
            this.テキスト028.Text = "ITEM28";
            this.テキスト028.Top = 0.03937008F;
            this.テキスト028.Width = 1.2333F;
            // 
            // テキスト029
            // 
            this.テキスト029.DataField = "ITEM29";
            this.テキスト029.Height = 0.15625F;
            this.テキスト029.Left = 11.63553F;
            this.テキスト029.Name = "テキスト029";
            this.テキスト029.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト029.Tag = "";
            this.テキスト029.Text = "ITEM29";
            this.テキスト029.Top = 0.1956201F;
            this.テキスト029.Width = 1.306217F;
            // 
            // テキスト016
            // 
            this.テキスト016.DataField = "ITEM16";
            this.テキスト016.Height = 0.15625F;
            this.テキスト016.Left = 2.812614F;
            this.テキスト016.Name = "テキスト016";
            this.テキスト016.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト016.Tag = "";
            this.テキスト016.Text = "ITEM16";
            this.テキスト016.Top = 0.3727034F;
            this.テキスト016.Width = 1.306218F;
            // 
            // テキスト017
            // 
            this.テキスト017.DataField = "ITEM17";
            this.テキスト017.Height = 0.15625F;
            this.テキスト017.Left = 4.212615F;
            this.テキスト017.Name = "テキスト017";
            this.テキスト017.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト017.Tag = "";
            this.テキスト017.Text = "ITEM17";
            this.テキスト017.Top = 0.3727034F;
            this.テキスト017.Width = 1.306218F;
            // 
            // テキスト018
            // 
            this.テキスト018.DataField = "ITEM18";
            this.テキスト018.Height = 0.15625F;
            this.テキスト018.Left = 5.584836F;
            this.テキスト018.Name = "テキスト018";
            this.テキスト018.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト018.Tag = "";
            this.テキスト018.Text = "ITEM18";
            this.テキスト018.Top = 0.3727034F;
            this.テキスト018.Width = 1.306218F;
            // 
            // テキスト019
            // 
            this.テキスト019.DataField = "ITEM19";
            this.テキスト019.Height = 0.15625F;
            this.テキスト019.Left = 6.959836F;
            this.テキスト019.Name = "テキスト019";
            this.テキスト019.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト019.Tag = "";
            this.テキスト019.Text = "ITEM19";
            this.テキスト019.Top = 0.3727034F;
            this.テキスト019.Width = 1.306218F;
            // 
            // テキスト020
            // 
            this.テキスト020.DataField = "ITEM20";
            this.テキスト020.Height = 0.15625F;
            this.テキスト020.Left = 8.338309F;
            this.テキスト020.Name = "テキスト020";
            this.テキスト020.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト020.Tag = "";
            this.テキスト020.Text = "ITEM20";
            this.テキスト020.Top = 0.3727034F;
            this.テキスト020.Width = 1.306217F;
            // 
            // テキスト021
            // 
            this.テキスト021.DataField = "ITEM21";
            this.テキスト021.Height = 0.15625F;
            this.テキスト021.Left = 9.718864F;
            this.テキスト021.Name = "テキスト021";
            this.テキスト021.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト021.Tag = "";
            this.テキスト021.Text = "ITEM21";
            this.テキスト021.Top = 0.3727034F;
            this.テキスト021.Width = 1.306217F;
            // 
            // テキスト022
            // 
            this.テキスト022.DataField = "ITEM22";
            this.テキスト022.Height = 0.15625F;
            this.テキスト022.Left = 2.812614F;
            this.テキスト022.Name = "テキスト022";
            this.テキスト022.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト022.Tag = "";
            this.テキスト022.Text = "ITEM22";
            this.テキスト022.Top = 0.5289534F;
            this.テキスト022.Width = 1.306218F;
            // 
            // テキスト023
            // 
            this.テキスト023.DataField = "ITEM23";
            this.テキスト023.Height = 0.15625F;
            this.テキスト023.Left = 4.212615F;
            this.テキスト023.Name = "テキスト023";
            this.テキスト023.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト023.Tag = "";
            this.テキスト023.Text = "ITEM23";
            this.テキスト023.Top = 0.5289534F;
            this.テキスト023.Width = 1.306218F;
            // 
            // テキスト024
            // 
            this.テキスト024.DataField = "ITEM24";
            this.テキスト024.Height = 0.15625F;
            this.テキスト024.Left = 5.584836F;
            this.テキスト024.Name = "テキスト024";
            this.テキスト024.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト024.Tag = "";
            this.テキスト024.Text = "ITEM24";
            this.テキスト024.Top = 0.5289534F;
            this.テキスト024.Width = 1.306218F;
            // 
            // テキスト025
            // 
            this.テキスト025.DataField = "ITEM25";
            this.テキスト025.Height = 0.15625F;
            this.テキスト025.Left = 6.959836F;
            this.テキスト025.Name = "テキスト025";
            this.テキスト025.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト025.Tag = "";
            this.テキスト025.Text = "ITEM25";
            this.テキスト025.Top = 0.5289534F;
            this.テキスト025.Width = 1.306218F;
            // 
            // テキスト026
            // 
            this.テキスト026.DataField = "ITEM26";
            this.テキスト026.Height = 0.15625F;
            this.テキスト026.Left = 8.338309F;
            this.テキスト026.Name = "テキスト026";
            this.テキスト026.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト026.Tag = "";
            this.テキスト026.Text = "ITEM26";
            this.テキスト026.Top = 0.5289534F;
            this.テキスト026.Width = 1.306217F;
            // 
            // テキスト027
            // 
            this.テキスト027.DataField = "ITEM27";
            this.テキスト027.Height = 0.15625F;
            this.テキスト027.Left = 9.718864F;
            this.テキスト027.Name = "テキスト027";
            this.テキスト027.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト027.Tag = "";
            this.テキスト027.Text = "ITEM27";
            this.テキスト027.Top = 0.5289534F;
            this.テキスト027.Width = 1.306217F;
            // 
            // テキスト030
            // 
            this.テキスト030.DataField = "ITEM30";
            this.テキスト030.Height = 0.15625F;
            this.テキスト030.Left = 11.73967F;
            this.テキスト030.Name = "テキスト030";
            this.テキスト030.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト030.Tag = "";
            this.テキスト030.Text = "ITEM30";
            this.テキスト030.Top = 0.5185367F;
            this.テキスト030.Width = 1.18125F;
            // 
            // 直線90
            // 
            this.直線90.Height = 0F;
            this.直線90.Left = 8.204579E-05F;
            this.直線90.LineWeight = 0F;
            this.直線90.Name = "直線90";
            this.直線90.Tag = "";
            this.直線90.Top = 0.6956201F;
            this.直線90.Width = 13.19097F;
            this.直線90.X1 = 8.204579E-05F;
            this.直線90.X2 = 13.19105F;
            this.直線90.Y1 = 0.6956201F;
            this.直線90.Y2 = 0.6956201F;
            // 
            // ラベル121
            // 
            this.ラベル121.Height = 0.15625F;
            this.ラベル121.HyperLink = null;
            this.ラベル121.Left = 11.69811F;
            this.ラベル121.Name = "ラベル121";
            this.ラベル121.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル121.Tag = "";
            this.ラベル121.Text = "（　　　　　　　　　）";
            this.ラベル121.Top = 0.5291339F;
            this.ラベル121.Width = 1.45169F;
            // 
            // 直線89
            // 
            this.直線89.Height = 0F;
            this.直線89.Left = 2.958415F;
            this.直線89.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線89.LineWeight = 0F;
            this.直線89.Name = "直線89";
            this.直線89.Tag = "";
            this.直線89.Top = 0.3622867F;
            this.直線89.Width = 10.2375F;
            this.直線89.X1 = 2.958415F;
            this.直線89.X2 = 13.19592F;
            this.直線89.Y1 = 0.3622867F;
            this.直線89.Y2 = 0.3622867F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル20,
            this.テキスト031,
            this.テキスト032,
            this.テキスト033,
            this.テキスト034,
            this.テキスト035,
            this.テキスト036,
            this.テキスト037,
            this.テキスト038,
            this.テキスト039,
            this.テキスト040,
            this.テキスト041,
            this.テキスト042,
            this.テキスト055,
            this.テキスト056,
            this.テキスト043,
            this.テキスト044,
            this.テキスト045,
            this.テキスト046,
            this.テキスト047,
            this.テキスト048,
            this.テキスト049,
            this.テキスト050,
            this.テキスト051,
            this.テキスト052,
            this.テキスト053,
            this.テキス054,
            this.テキスト057,
            this.ラベル122,
            this.直線119,
            this.直線120});
            this.reportFooter1.Height = 0.6666667F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // ラベル20
            // 
            this.ラベル20.Height = 0.15625F;
            this.ラベル20.HyperLink = null;
            this.ラベル20.Left = 0.414193F;
            this.ラベル20.Name = "ラベル20";
            this.ラベル20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル20.Tag = "";
            this.ラベル20.Text = "総計";
            this.ラベル20.Top = 0.04166667F;
            this.ラベル20.Width = 0.3669094F;
            // 
            // テキスト031
            // 
            this.テキスト031.DataField = "ITEM04";
            this.テキスト031.Height = 0.15625F;
            this.テキスト031.Left = 2.845157F;
            this.テキスト031.Name = "テキスト031";
            this.テキスト031.OutputFormat = resources.GetString("テキスト031.OutputFormat");
            this.テキスト031.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト031.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト031.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト031.Tag = "";
            this.テキスト031.Text = "ITEM04";
            this.テキスト031.Top = 0F;
            this.テキスト031.Width = 1.281535F;
            // 
            // テキスト032
            // 
            this.テキスト032.DataField = "ITEM05";
            this.テキスト032.Height = 0.15625F;
            this.テキスト032.Left = 4.245157F;
            this.テキスト032.Name = "テキスト032";
            this.テキスト032.OutputFormat = resources.GetString("テキスト032.OutputFormat");
            this.テキスト032.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト032.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト032.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト032.Tag = "";
            this.テキスト032.Text = "ITEM05";
            this.テキスト032.Top = 0F;
            this.テキスト032.Width = 1.281536F;
            // 
            // テキスト033
            // 
            this.テキスト033.DataField = "ITEM06";
            this.テキスト033.Height = 0.15625F;
            this.テキスト033.Left = 5.61738F;
            this.テキスト033.Name = "テキスト033";
            this.テキスト033.OutputFormat = resources.GetString("テキスト033.OutputFormat");
            this.テキスト033.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト033.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト033.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト033.Tag = "";
            this.テキスト033.Text = "ITEM06";
            this.テキスト033.Top = 0F;
            this.テキスト033.Width = 1.281536F;
            // 
            // テキスト034
            // 
            this.テキスト034.DataField = "ITEM07";
            this.テキスト034.Height = 0.15625F;
            this.テキスト034.Left = 6.99238F;
            this.テキスト034.Name = "テキスト034";
            this.テキスト034.OutputFormat = resources.GetString("テキスト034.OutputFormat");
            this.テキスト034.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト034.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト034.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト034.Tag = "";
            this.テキスト034.Text = "ITEM07";
            this.テキスト034.Top = 0F;
            this.テキスト034.Width = 1.281536F;
            // 
            // テキスト035
            // 
            this.テキスト035.DataField = "ITEM08";
            this.テキスト035.Height = 0.15625F;
            this.テキスト035.Left = 8.370852F;
            this.テキスト035.Name = "テキスト035";
            this.テキスト035.OutputFormat = resources.GetString("テキスト035.OutputFormat");
            this.テキスト035.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト035.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト035.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト035.Tag = "";
            this.テキスト035.Text = "ITEM08";
            this.テキスト035.Top = 0F;
            this.テキスト035.Width = 1.281535F;
            // 
            // テキスト036
            // 
            this.テキスト036.DataField = "ITEM09";
            this.テキスト036.Height = 0.15625F;
            this.テキスト036.Left = 9.74863F;
            this.テキスト036.Name = "テキスト036";
            this.テキスト036.OutputFormat = resources.GetString("テキスト036.OutputFormat");
            this.テキスト036.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト036.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト036.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト036.Tag = "";
            this.テキスト036.Text = "ITEM09";
            this.テキスト036.Top = 0F;
            this.テキスト036.Width = 1.281535F;
            // 
            // テキスト037
            // 
            this.テキスト037.DataField = "ITEM10";
            this.テキスト037.Height = 0.15625F;
            this.テキスト037.Left = 2.845157F;
            this.テキスト037.Name = "テキスト037";
            this.テキスト037.OutputFormat = resources.GetString("テキスト037.OutputFormat");
            this.テキスト037.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト037.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト037.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト037.Tag = "";
            this.テキスト037.Text = "ITEM10";
            this.テキスト037.Top = 0.15625F;
            this.テキスト037.Width = 1.281535F;
            // 
            // テキスト038
            // 
            this.テキスト038.DataField = "ITEM11";
            this.テキスト038.Height = 0.15625F;
            this.テキスト038.Left = 4.245157F;
            this.テキスト038.Name = "テキスト038";
            this.テキスト038.OutputFormat = resources.GetString("テキスト038.OutputFormat");
            this.テキスト038.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト038.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト038.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト038.Tag = "";
            this.テキスト038.Text = "ITEM11";
            this.テキスト038.Top = 0.15625F;
            this.テキスト038.Width = 1.281536F;
            // 
            // テキスト039
            // 
            this.テキスト039.DataField = "ITEM12";
            this.テキスト039.Height = 0.15625F;
            this.テキスト039.Left = 5.61738F;
            this.テキスト039.Name = "テキスト039";
            this.テキスト039.OutputFormat = resources.GetString("テキスト039.OutputFormat");
            this.テキスト039.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト039.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト039.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト039.Tag = "";
            this.テキスト039.Text = "ITEM12";
            this.テキスト039.Top = 0.15625F;
            this.テキスト039.Width = 1.281536F;
            // 
            // テキスト040
            // 
            this.テキスト040.DataField = "ITEM13";
            this.テキスト040.Height = 0.15625F;
            this.テキスト040.Left = 6.99238F;
            this.テキスト040.Name = "テキスト040";
            this.テキスト040.OutputFormat = resources.GetString("テキスト040.OutputFormat");
            this.テキスト040.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト040.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト040.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト040.Tag = "";
            this.テキスト040.Text = "ITEM13";
            this.テキスト040.Top = 0.15625F;
            this.テキスト040.Width = 1.281536F;
            // 
            // テキスト041
            // 
            this.テキスト041.DataField = "ITEM14";
            this.テキスト041.Height = 0.15625F;
            this.テキスト041.Left = 8.370852F;
            this.テキスト041.Name = "テキスト041";
            this.テキスト041.OutputFormat = resources.GetString("テキスト041.OutputFormat");
            this.テキスト041.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト041.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト041.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト041.Tag = "";
            this.テキスト041.Text = "ITEM14";
            this.テキスト041.Top = 0.15625F;
            this.テキスト041.Width = 1.281535F;
            // 
            // テキスト042
            // 
            this.テキスト042.DataField = "ITEM15";
            this.テキスト042.Height = 0.15625F;
            this.テキスト042.Left = 9.74863F;
            this.テキスト042.Name = "テキスト042";
            this.テキスト042.OutputFormat = resources.GetString("テキスト042.OutputFormat");
            this.テキスト042.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト042.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト042.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト042.Tag = "";
            this.テキスト042.Text = "ITEM15";
            this.テキスト042.Top = 0.15625F;
            this.テキスト042.Width = 1.281535F;
            // 
            // テキスト055
            // 
            this.テキスト055.DataField = "ITEM28";
            this.テキスト055.Height = 0.15625F;
            this.テキスト055.Left = 11.64724F;
            this.テキスト055.Name = "テキスト055";
            this.テキスト055.OutputFormat = resources.GetString("テキスト055.OutputFormat");
            this.テキスト055.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト055.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト055.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト055.Tag = "";
            this.テキスト055.Text = "ITEM28";
            this.テキスト055.Top = 0F;
            this.テキスト055.Width = 1.281535F;
            // 
            // テキスト056
            // 
            this.テキスト056.DataField = "ITEM29";
            this.テキスト056.Height = 0.15625F;
            this.テキスト056.Left = 11.64724F;
            this.テキスト056.Name = "テキスト056";
            this.テキスト056.OutputFormat = resources.GetString("テキスト056.OutputFormat");
            this.テキスト056.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト056.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト056.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト056.Tag = "";
            this.テキスト056.Text = "ITEM29";
            this.テキスト056.Top = 0.15625F;
            this.テキスト056.Width = 1.281535F;
            // 
            // テキスト043
            // 
            this.テキスト043.DataField = "ITEM16";
            this.テキスト043.Height = 0.15625F;
            this.テキスト043.Left = 2.815355F;
            this.テキスト043.Name = "テキスト043";
            this.テキスト043.OutputFormat = resources.GetString("テキスト043.OutputFormat");
            this.テキスト043.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト043.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト043.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト043.Tag = "";
            this.テキスト043.Text = "ITEM16";
            this.テキスト043.Top = 0.34375F;
            this.テキスト043.Width = 1.311338F;
            // 
            // テキスト044
            // 
            this.テキスト044.DataField = "ITEM17";
            this.テキスト044.Height = 0.15625F;
            this.テキスト044.Left = 4.215354F;
            this.テキスト044.Name = "テキスト044";
            this.テキスト044.OutputFormat = resources.GetString("テキスト044.OutputFormat");
            this.テキスト044.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト044.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト044.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト044.Tag = "";
            this.テキスト044.Text = "ITEM17";
            this.テキスト044.Top = 0.34375F;
            this.テキスト044.Width = 1.311338F;
            // 
            // テキスト045
            // 
            this.テキスト045.DataField = "ITEM18";
            this.テキスト045.Height = 0.15625F;
            this.テキスト045.Left = 5.587577F;
            this.テキスト045.Name = "テキスト045";
            this.テキスト045.OutputFormat = resources.GetString("テキスト045.OutputFormat");
            this.テキスト045.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト045.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト045.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト045.Tag = "";
            this.テキスト045.Text = "ITEM18";
            this.テキスト045.Top = 0.34375F;
            this.テキスト045.Width = 1.311338F;
            // 
            // テキスト046
            // 
            this.テキスト046.DataField = "ITEM19";
            this.テキスト046.Height = 0.15625F;
            this.テキスト046.Left = 6.962577F;
            this.テキスト046.Name = "テキスト046";
            this.テキスト046.OutputFormat = resources.GetString("テキスト046.OutputFormat");
            this.テキスト046.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト046.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト046.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト046.Tag = "";
            this.テキスト046.Text = "ITEM19";
            this.テキスト046.Top = 0.34375F;
            this.テキスト046.Width = 1.311338F;
            // 
            // テキスト047
            // 
            this.テキスト047.DataField = "ITEM20";
            this.テキスト047.Height = 0.15625F;
            this.テキスト047.Left = 8.341049F;
            this.テキスト047.Name = "テキスト047";
            this.テキスト047.OutputFormat = resources.GetString("テキスト047.OutputFormat");
            this.テキスト047.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト047.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト047.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト047.Tag = "";
            this.テキスト047.Text = "ITEM20";
            this.テキスト047.Top = 0.34375F;
            this.テキスト047.Width = 1.311338F;
            // 
            // テキスト048
            // 
            this.テキスト048.DataField = "ITEM21";
            this.テキスト048.Height = 0.15625F;
            this.テキスト048.Left = 9.718826F;
            this.テキスト048.Name = "テキスト048";
            this.テキスト048.OutputFormat = resources.GetString("テキスト048.OutputFormat");
            this.テキスト048.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト048.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト048.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト048.Tag = "";
            this.テキスト048.Text = "ITEM21";
            this.テキスト048.Top = 0.34375F;
            this.テキスト048.Width = 1.311338F;
            // 
            // テキスト049
            // 
            this.テキスト049.DataField = "ITEM22";
            this.テキスト049.Height = 0.15625F;
            this.テキスト049.Left = 2.815355F;
            this.テキスト049.Name = "テキスト049";
            this.テキスト049.OutputFormat = resources.GetString("テキスト049.OutputFormat");
            this.テキスト049.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト049.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト049.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト049.Tag = "";
            this.テキスト049.Text = "ITEM22";
            this.テキスト049.Top = 0.5104167F;
            this.テキスト049.Width = 1.311338F;
            // 
            // テキスト050
            // 
            this.テキスト050.DataField = "ITEM23";
            this.テキスト050.Height = 0.15625F;
            this.テキスト050.Left = 4.215354F;
            this.テキスト050.Name = "テキスト050";
            this.テキスト050.OutputFormat = resources.GetString("テキスト050.OutputFormat");
            this.テキスト050.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト050.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト050.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト050.Tag = "";
            this.テキスト050.Text = "ITEM23";
            this.テキスト050.Top = 0.5104167F;
            this.テキスト050.Width = 1.311338F;
            // 
            // テキスト051
            // 
            this.テキスト051.DataField = "ITEM24";
            this.テキスト051.Height = 0.15625F;
            this.テキスト051.Left = 5.587577F;
            this.テキスト051.Name = "テキスト051";
            this.テキスト051.OutputFormat = resources.GetString("テキスト051.OutputFormat");
            this.テキスト051.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト051.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト051.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト051.Tag = "";
            this.テキスト051.Text = "ITEM24";
            this.テキスト051.Top = 0.5104167F;
            this.テキスト051.Width = 1.311338F;
            // 
            // テキスト052
            // 
            this.テキスト052.DataField = "ITEM25";
            this.テキスト052.Height = 0.15625F;
            this.テキスト052.Left = 6.962577F;
            this.テキスト052.Name = "テキスト052";
            this.テキスト052.OutputFormat = resources.GetString("テキスト052.OutputFormat");
            this.テキスト052.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト052.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト052.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト052.Tag = "";
            this.テキスト052.Text = "ITEM25";
            this.テキスト052.Top = 0.5104167F;
            this.テキスト052.Width = 1.311338F;
            // 
            // テキスト053
            // 
            this.テキスト053.DataField = "ITEM26";
            this.テキスト053.Height = 0.15625F;
            this.テキスト053.Left = 8.341049F;
            this.テキスト053.Name = "テキスト053";
            this.テキスト053.OutputFormat = resources.GetString("テキスト053.OutputFormat");
            this.テキスト053.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト053.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト053.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト053.Tag = "";
            this.テキスト053.Text = "ITEM26";
            this.テキスト053.Top = 0.5104167F;
            this.テキスト053.Width = 1.311338F;
            // 
            // テキス054
            // 
            this.テキス054.DataField = "ITEM27";
            this.テキス054.Height = 0.15625F;
            this.テキス054.Left = 9.718826F;
            this.テキス054.Name = "テキス054";
            this.テキス054.OutputFormat = resources.GetString("テキス054.OutputFormat");
            this.テキス054.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキス054.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキス054.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキス054.Tag = "";
            this.テキス054.Text = "ITEM27";
            this.テキス054.Top = 0.5104167F;
            this.テキス054.Width = 1.311338F;
            // 
            // テキスト057
            // 
            this.テキスト057.DataField = "ITEM30";
            this.テキスト057.Height = 0.15625F;
            this.テキスト057.Left = 11.74753F;
            this.テキスト057.Name = "テキスト057";
            this.テキスト057.OutputFormat = resources.GetString("テキスト057.OutputFormat");
            this.テキスト057.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト057.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト057.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト057.Tag = "";
            this.テキスト057.Text = "ITEM30";
            this.テキスト057.Top = 0.5104167F;
            this.テキスト057.Width = 1.18125F;
            // 
            // ラベル122
            // 
            this.ラベル122.Height = 0.1562992F;
            this.ラベル122.HyperLink = null;
            this.ラベル122.Left = 11.71654F;
            this.ラベル122.Name = "ラベル122";
            this.ラベル122.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル122.Tag = "";
            this.ラベル122.Text = "（　　　　　　　　　）";
            this.ラベル122.Top = 0.503937F;
            this.ラベル122.Width = 1.451575F;
            // 
            // 直線119
            // 
            this.直線119.Height = 0F;
            this.直線119.Left = 2.992126F;
            this.直線119.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線119.LineWeight = 0F;
            this.直線119.Name = "直線119";
            this.直線119.Tag = "";
            this.直線119.Top = 0.3149607F;
            this.直線119.Width = 10.2375F;
            this.直線119.X1 = 2.992126F;
            this.直線119.X2 = 13.22963F;
            this.直線119.Y1 = 0.3149607F;
            this.直線119.Y2 = 0.3149607F;
            // 
            // 直線120
            // 
            this.直線120.Height = 0F;
            this.直線120.Left = 0F;
            this.直線120.LineWeight = 0F;
            this.直線120.Name = "直線120";
            this.直線120.Tag = "";
            this.直線120.Top = 0.6665355F;
            this.直線120.Width = 13.19097F;
            this.直線120.X1 = 0F;
            this.直線120.X2 = 13.19097F;
            this.直線120.Y1 = 0.6665355F;
            this.直線120.Y2 = 0.6665355F;
            // 
            // HNYR1011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937007F;
            this.PageSettings.Margins.Left = 0.5708662F;
            this.PageSettings.Margins.Right = 0.5708662F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.19097F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキストpage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト028)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト029)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト016)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト017)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト018)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト019)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト020)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト021)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト022)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト023)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト024)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト025)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト026)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト027)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト030)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト031)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト032)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト033)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト034)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト035)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト036)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト037)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト038)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト039)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト040)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト041)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト042)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト055)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト056)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト043)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト044)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト045)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト046)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト047)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト048)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト049)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト050)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト051)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト052)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト053)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキス054)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト057)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト001;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキストpage;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル33;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル34;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル35;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル36;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル37;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル38;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル39;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル40;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル41;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル42;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル43;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル44;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル47;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル48;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル49;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル50;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル51;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル52;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル53;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル54;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル55;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル56;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル61;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル62;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル63;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル64;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル65;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線123;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト002;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト003;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト004;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト005;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト006;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト007;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト008;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト009;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト010;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト011;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト012;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト013;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト014;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト015;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト028;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト029;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト016;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト017;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト018;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト019;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト020;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト021;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト022;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト023;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト024;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト025;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト026;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト027;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト030;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線89;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線90;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル121;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト031;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト032;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト033;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト034;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト035;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト036;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト037;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト038;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト039;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト040;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト041;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト042;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト055;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト056;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト043;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト044;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト045;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト046;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト047;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト048;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト049;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト050;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト051;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト052;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト053;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキス054;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト057;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線119;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線120;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル122;
    }
}
