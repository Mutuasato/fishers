﻿using jp.co.fsi.common.report;
using jp.co.fsi.common.util;
using System;
using System.Data;

namespace jp.co.fsi.hn.hnyr1041
{
    /// <summary>
    /// HNYR1041R の概要の説明です。
    /// </summary>
    public partial class HNYR1041R : BaseReport
    {

        public HNYR1041R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {
            decimal kaisuukei = Util.ToDecimal(this.テキスト1.Value) + Util.ToDecimal(this.テキスト2.Value) + Util.ToDecimal(this.テキスト3.Value) + Util.ToDecimal(this.テキスト4.Value) + Util.ToDecimal(this.テキスト5.Value) + Util.ToDecimal(this.テキスト6.Value) 
                               + Util.ToDecimal(this.テキスト7.Value) + Util.ToDecimal(this.テキスト8.Value) + Util.ToDecimal(this.テキスト9.Value) + Util.ToDecimal(this.テキスト10.Value) + Util.ToDecimal(this.テキスト11.Value) + Util.ToDecimal(this.テキスト12.Value);
            this.回数計.Value = Util.ToString(kaisuukei);


            decimal suuryokei = Util.ToDecimal(this.テキスト13.Value) + Util.ToDecimal(this.テキスト14.Value) + Util.ToDecimal(this.テキスト15.Value) + Util.ToDecimal(this.テキスト16.Value) + Util.ToDecimal(this.テキスト17.Value) + Util.ToDecimal(this.テキスト18.Value)
                               + Util.ToDecimal(this.テキスト19.Value) + Util.ToDecimal(this.テキスト20.Value) + Util.ToDecimal(this.テキスト21.Value) + Util.ToDecimal(this.テキスト22.Value) + Util.ToDecimal(this.テキスト23.Value) + Util.ToDecimal(this.テキスト24.Value);
            this.数量計.Value = Util.ToString(suuryokei);


            decimal kingakukei = Util.ToDecimal(this.テキスト25.Value) + Util.ToDecimal(this.テキスト26.Value) + Util.ToDecimal(this.テキスト27.Value) + Util.ToDecimal(this.テキスト28.Value) + Util.ToDecimal(this.テキスト29.Value) + Util.ToDecimal(this.テキスト30.Value)
                               + Util.ToDecimal(this.テキスト31.Value) + Util.ToDecimal(this.テキスト32.Value) + Util.ToDecimal(this.テキスト33.Value) + Util.ToDecimal(this.テキスト34.Value) + Util.ToDecimal(this.テキスト35.Value) + Util.ToDecimal(this.テキスト36.Value);
            this.金額計.Value = Util.ToString(kingakukei);
        }

        private void reportFooter1_Format(object sender, System.EventArgs e)
        {
            decimal kaisuukei = Util.ToDecimal(this.合計25.Value) + Util.ToDecimal(this.合計26.Value) + Util.ToDecimal(this.合計27.Value) + Util.ToDecimal(this.合計28.Value) + Util.ToDecimal(this.合計29.Value) + Util.ToDecimal(this.合計30.Value)
                               + Util.ToDecimal(this.合計31.Value) + Util.ToDecimal(this.合計32.Value) + Util.ToDecimal(this.合計33.Value) + Util.ToDecimal(this.合計34.Value) + Util.ToDecimal(this.合計35.Value) + Util.ToDecimal(this.合計36.Value);
            this.回数総合計.Value = Util.ToString(kaisuukei);


            decimal suuryokei = Util.ToDecimal(this.合計1.Value) + Util.ToDecimal(this.合計2.Value) + Util.ToDecimal(this.合計3.Value) + Util.ToDecimal(this.合計4.Value) + Util.ToDecimal(this.合計5.Value) + Util.ToDecimal(this.合計6.Value)
                               + Util.ToDecimal(this.合計13.Value) + Util.ToDecimal(this.合計14.Value) + Util.ToDecimal(this.合計15.Value) + Util.ToDecimal(this.合計16.Value) + Util.ToDecimal(this.合計17.Value) + Util.ToDecimal(this.合計18.Value);
            this.数量総合計.Value = Util.ToString(suuryokei);


            decimal kingakukei = Util.ToDecimal(this.合計7.Value) + Util.ToDecimal(this.合計8.Value) + Util.ToDecimal(this.合計9.Value) + Util.ToDecimal(this.合計10.Value) + Util.ToDecimal(this.合計11.Value) + Util.ToDecimal(this.合計12.Value)
                               + Util.ToDecimal(this.合計19.Value) + Util.ToDecimal(this.合計20.Value) + Util.ToDecimal(this.合計21.Value) + Util.ToDecimal(this.合計22.Value) + Util.ToDecimal(this.合計23.Value) + Util.ToDecimal(this.合計24.Value);
            this.金額総合計.Value = Util.ToString(kingakukei);
        }

        private void pageHeader_Format(object sender, System.EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }
    }
}
