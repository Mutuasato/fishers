﻿namespace jp.co.fsi.hn.hnyr1061
{
    /// <summary>
    /// HNYR1061R の概要の説明です。
    /// </summary>
    partial class HNYR1061R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNYR1061R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ラベル1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.aaa = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.bbb = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ccc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.合計1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aaa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bbb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル1,
            this.ラベル3,
            this.テキスト1,
            this.ラベル6,
            this.テキスト3,
            this.テキスト100,
            this.直線15,
            this.直線16,
            this.直線17,
            this.直線18,
            this.直線32,
            this.直線33,
            this.直線34,
            this.ラベル35,
            this.ラベル37,
            this.ラベル38,
            this.ラベル39,
            this.ラベル40,
            this.ラベル41,
            this.ラベル42,
            this.ラベル44,
            this.ラベル45,
            this.ラベル46,
            this.ラベル47,
            this.ラベル48,
            this.ラベル49,
            this.ラベル53,
            this.ラベル54,
            this.ラベル55,
            this.ラベル56,
            this.テキスト4,
            this.テキスト8,
            this.テキスト12,
            this.テキスト16,
            this.テキスト20,
            this.テキスト24,
            this.テキスト28,
            this.テキスト32,
            this.テキスト36,
            this.テキスト44,
            this.テキスト48,
            this.テキスト5,
            this.テキスト9,
            this.テキスト13,
            this.テキスト17,
            this.テキスト21,
            this.テキスト25,
            this.テキスト29,
            this.テキスト33,
            this.テキスト37,
            this.テキスト41,
            this.テキスト45,
            this.テキスト49,
            this.合計2,
            this.テキスト6,
            this.テキスト10,
            this.テキスト14,
            this.テキスト18,
            this.テキスト22,
            this.テキスト26,
            this.テキスト30,
            this.テキスト34,
            this.テキスト38,
            this.テキスト42,
            this.テキスト46,
            this.テキスト50,
            this.合計3,
            this.ラベル96,
            this.aaa,
            this.bbb,
            this.ccc,
            this.txtToday,
            this.ラベル0,
            this.テキスト2,
            this.ラベル2,
            this.ラベル4,
            this.ラベル5,
            this.直線6,
            this.ラベル7,
            this.テキスト7,
            this.テキスト11,
            this.テキスト15,
            this.テキスト19,
            this.テキスト23,
            this.テキスト27,
            this.テキスト31,
            this.テキスト35,
            this.テキスト39,
            this.テキスト43,
            this.テキスト47,
            this.テキスト51,
            this.合計4,
            this.合計1,
            this.テキスト40,
            this.ラベル36,
            this.直線19,
            this.直線20,
            this.直線21,
            this.直線22,
            this.直線23,
            this.直線24,
            this.直線25,
            this.直線26,
            this.直線27,
            this.直線30,
            this.直線31,
            this.直線52,
            this.直線51,
            this.label1,
            this.label2,
            this.label3,
            this.textBox1,
            this.textBox2});
            this.detail.Height = 9.072918F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // ラベル1
            // 
            this.ラベル1.Height = 0.3333333F;
            this.ラベル1.HyperLink = null;
            this.ラベル1.Left = 1.572917F;
            this.ラベル1.Name = "ラベル1";
            this.ラベル1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 22pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル1.Tag = "";
            this.ラベル1.Text = "鮮 魚 買 取 高 証 明 書";
            this.ラベル1.Top = 8.940697E-08F;
            this.ラベル1.Width = 3.583333F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.28125F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 2.364583F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "住　所";
            this.ラベル3.Top = 0.5486112F;
            this.ラベル3.Width = 0.8333333F;
            // 
            // テキスト1
            // 
            this.テキスト1.DataField = "ITEM01";
            this.テキスト1.Height = 0.28125F;
            this.テキスト1.Left = 3.260417F;
            this.テキスト1.Name = "テキスト1";
            this.テキスト1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト1.Tag = "";
            this.テキスト1.Text = "ITEM01";
            this.テキスト1.Top = 0.5486112F;
            this.テキスト1.Width = 3.733333F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.2291667F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 3.26F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "～";
            this.ラベル6.Top = 1.427F;
            this.ラベル6.Width = 0.25F;
            // 
            // テキスト3
            // 
            this.テキスト3.DataField = "ITEM03";
            this.テキスト3.Height = 0.229F;
            this.テキスト3.Left = 1.489583F;
            this.テキスト3.Name = "テキスト3";
            this.テキスト3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト3.Tag = "";
            this.テキスト3.Text = "ITEM03";
            this.テキスト3.Top = 1.427083F;
            this.テキスト3.Width = 0.839F;
            // 
            // テキスト100
            // 
            this.テキスト100.DataField = "ITEM59";
            this.テキスト100.Height = 0.2291667F;
            this.テキスト100.Left = 3.683F;
            this.テキスト100.Name = "テキスト100";
            this.テキスト100.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト100.Tag = "";
            this.テキスト100.Text = "ITEM59";
            this.テキスト100.Top = 1.427F;
            this.テキスト100.Width = 0.8376669F;
            // 
            // 直線15
            // 
            this.直線15.Height = 4.134723F;
            this.直線15.Left = -2.980232E-08F;
            this.直線15.LineWeight = 0F;
            this.直線15.Name = "直線15";
            this.直線15.Tag = "";
            this.直線15.Top = 1.96875F;
            this.直線15.Width = 0F;
            this.直線15.X1 = -2.980232E-08F;
            this.直線15.X2 = -2.980232E-08F;
            this.直線15.Y1 = 1.96875F;
            this.直線15.Y2 = 6.103473F;
            // 
            // 直線16
            // 
            this.直線16.Height = 0F;
            this.直線16.Left = -2.980232E-08F;
            this.直線16.LineWeight = 0F;
            this.直線16.Name = "直線16";
            this.直線16.Tag = "";
            this.直線16.Top = 1.96875F;
            this.直線16.Width = 7.0875F;
            this.直線16.X1 = -2.980232E-08F;
            this.直線16.X2 = 7.0875F;
            this.直線16.Y1 = 1.96875F;
            this.直線16.Y2 = 1.96875F;
            // 
            // 直線17
            // 
            this.直線17.Height = 4.134723F;
            this.直線17.Left = 5.4125F;
            this.直線17.LineWeight = 0F;
            this.直線17.Name = "直線17";
            this.直線17.Tag = "";
            this.直線17.Top = 1.970833F;
            this.直線17.Width = 0F;
            this.直線17.X1 = 5.4125F;
            this.直線17.X2 = 5.4125F;
            this.直線17.Y1 = 1.970833F;
            this.直線17.Y2 = 6.105556F;
            // 
            // 直線18
            // 
            this.直線18.Height = 0F;
            this.直線18.Left = -2.980232E-08F;
            this.直線18.LineWeight = 0F;
            this.直線18.Name = "直線18";
            this.直線18.Tag = "";
            this.直線18.Top = 2.264583F;
            this.直線18.Width = 7.0875F;
            this.直線18.X1 = -2.980232E-08F;
            this.直線18.X2 = 7.0875F;
            this.直線18.Y1 = 2.264583F;
            this.直線18.Y2 = 2.264583F;
            // 
            // 直線32
            // 
            this.直線32.Height = 4.134723F;
            this.直線32.Left = 0.3958333F;
            this.直線32.LineWeight = 0F;
            this.直線32.Name = "直線32";
            this.直線32.Tag = "";
            this.直線32.Top = 1.96875F;
            this.直線32.Width = 0F;
            this.直線32.X1 = 0.3958333F;
            this.直線32.X2 = 0.3958333F;
            this.直線32.Y1 = 1.96875F;
            this.直線32.Y2 = 6.103473F;
            // 
            // 直線33
            // 
            this.直線33.Height = 4.134723F;
            this.直線33.Left = 2.065278F;
            this.直線33.LineWeight = 0F;
            this.直線33.Name = "直線33";
            this.直線33.Tag = "";
            this.直線33.Top = 1.96875F;
            this.直線33.Width = 0F;
            this.直線33.X1 = 2.065278F;
            this.直線33.X2 = 2.065278F;
            this.直線33.Y1 = 1.96875F;
            this.直線33.Y2 = 6.103473F;
            // 
            // 直線34
            // 
            this.直線34.Height = 4.134723F;
            this.直線34.Left = 3.738889F;
            this.直線34.LineWeight = 0F;
            this.直線34.Name = "直線34";
            this.直線34.Tag = "";
            this.直線34.Top = 1.970833F;
            this.直線34.Width = 0F;
            this.直線34.X1 = 3.738889F;
            this.直線34.X2 = 3.738889F;
            this.直線34.Y1 = 1.970833F;
            this.直線34.Y2 = 6.105556F;
            // 
            // ラベル35
            // 
            this.ラベル35.Height = 0.1979167F;
            this.ラベル35.HyperLink = null;
            this.ラベル35.Left = 0.09652773F;
            this.ラベル35.Name = "ラベル35";
            this.ラベル35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル35.Tag = "";
            this.ラベル35.Text = "月";
            this.ラベル35.Top = 2.020833F;
            this.ラベル35.Width = 0.21875F;
            // 
            // ラベル37
            // 
            this.ラベル37.Height = 0.1979167F;
            this.ラベル37.HyperLink = null;
            this.ラベル37.Left = 0.09679715F;
            this.ラベル37.Name = "ラベル37";
            this.ラベル37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル37.Tag = "";
            this.ラベル37.Text = "1";
            this.ラベル37.Top = 5.026882F;
            this.ラベル37.Width = 0.1972222F;
            // 
            // ラベル38
            // 
            this.ラベル38.Height = 0.1979167F;
            this.ラベル38.HyperLink = null;
            this.ラベル38.Left = 0.09679715F;
            this.ラベル38.Name = "ラベル38";
            this.ラベル38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル38.Tag = "";
            this.ラベル38.Text = "2";
            this.ラベル38.Top = 5.318549F;
            this.ラベル38.Width = 0.1972222F;
            // 
            // ラベル39
            // 
            this.ラベル39.Height = 0.1979167F;
            this.ラベル39.HyperLink = null;
            this.ラベル39.Left = 0.09679715F;
            this.ラベル39.Name = "ラベル39";
            this.ラベル39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル39.Tag = "";
            this.ラベル39.Text = "3";
            this.ラベル39.Top = 5.610216F;
            this.ラベル39.Width = 0.1972222F;
            // 
            // ラベル40
            // 
            this.ラベル40.Height = 0.1979167F;
            this.ラベル40.HyperLink = null;
            this.ラベル40.Left = 0.09646288F;
            this.ラベル40.Name = "ラベル40";
            this.ラベル40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル40.Tag = "";
            this.ラベル40.Text = "4";
            this.ラベル40.Top = 2.365063F;
            this.ラベル40.Width = 0.1972222F;
            // 
            // ラベル41
            // 
            this.ラベル41.Height = 0.1979167F;
            this.ラベル41.HyperLink = null;
            this.ラベル41.Left = 0.09646288F;
            this.ラベル41.Name = "ラベル41";
            this.ラベル41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル41.Tag = "";
            this.ラベル41.Text = "5";
            this.ラベル41.Top = 2.65673F;
            this.ラベル41.Width = 0.1972222F;
            // 
            // ラベル42
            // 
            this.ラベル42.Height = 0.1979167F;
            this.ラベル42.HyperLink = null;
            this.ラベル42.Left = 0.09646288F;
            this.ラベル42.Name = "ラベル42";
            this.ラベル42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル42.Tag = "";
            this.ラベル42.Text = "6";
            this.ラベル42.Top = 2.948396F;
            this.ラベル42.Width = 0.1972222F;
            // 
            // ラベル44
            // 
            this.ラベル44.Height = 0.1979167F;
            this.ラベル44.HyperLink = null;
            this.ラベル44.Left = 0.09646288F;
            this.ラベル44.Name = "ラベル44";
            this.ラベル44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル44.Tag = "";
            this.ラベル44.Text = "8";
            this.ラベル44.Top = 3.542146F;
            this.ラベル44.Width = 0.1972222F;
            // 
            // ラベル45
            // 
            this.ラベル45.Height = 0.1979167F;
            this.ラベル45.HyperLink = null;
            this.ラベル45.Left = 0.09646288F;
            this.ラベル45.Name = "ラベル45";
            this.ラベル45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル45.Tag = "";
            this.ラベル45.Text = "9";
            this.ラベル45.Top = 3.833813F;
            this.ラベル45.Width = 0.1972222F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.1979167F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 0.09646288F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "10";
            this.ラベル46.Top = 4.135896F;
            this.ラベル46.Width = 0.1972222F;
            // 
            // ラベル47
            // 
            this.ラベル47.Height = 0.1979167F;
            this.ラベル47.HyperLink = null;
            this.ラベル47.Left = 0.09646288F;
            this.ラベル47.Name = "ラベル47";
            this.ラベル47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル47.Tag = "";
            this.ラベル47.Text = "11";
            this.ラベル47.Top = 4.427563F;
            this.ラベル47.Width = 0.1972222F;
            // 
            // ラベル48
            // 
            this.ラベル48.Height = 0.1979167F;
            this.ラベル48.HyperLink = null;
            this.ラベル48.Left = 0.09646288F;
            this.ラベル48.Name = "ラベル48";
            this.ラベル48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル48.Tag = "";
            this.ラベル48.Text = "12";
            this.ラベル48.Top = 4.71923F;
            this.ラベル48.Width = 0.1972222F;
            // 
            // ラベル49
            // 
            this.ラベル49.Height = 0.1979167F;
            this.ラベル49.HyperLink = null;
            this.ラベル49.Left = 0.09652773F;
            this.ラベル49.Name = "ラベル49";
            this.ラベル49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル49.Tag = "";
            this.ラベル49.Text = "計";
            this.ラベル49.Top = 5.90625F;
            this.ラベル49.Width = 0.1972222F;
            // 
            // ラベル53
            // 
            this.ラベル53.Height = 0.1979167F;
            this.ラベル53.HyperLink = null;
            this.ラベル53.Left = 0.09646288F;
            this.ラベル53.Name = "ラベル53";
            this.ラベル53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル53.Tag = "";
            this.ラベル53.Text = "7";
            this.ラベル53.Top = 3.25048F;
            this.ラベル53.Width = 0.1972222F;
            // 
            // ラベル54
            // 
            this.ラベル54.Height = 0.1979167F;
            this.ラベル54.HyperLink = null;
            this.ラベル54.Left = 0.71875F;
            this.ラベル54.Name = "ラベル54";
            this.ラベル54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル54.Tag = "";
            this.ラベル54.Text = "数量（Kg）";
            this.ラベル54.Top = 2.020833F;
            this.ラベル54.Width = 1.171014F;
            // 
            // ラベル55
            // 
            this.ラベル55.Height = 0.1979167F;
            this.ラベル55.HyperLink = null;
            this.ラベル55.Left = 2.364583F;
            this.ラベル55.Name = "ラベル55";
            this.ラベル55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル55.Tag = "";
            this.ラベル55.Text = "金額（税抜）";
            this.ラベル55.Top = 2.020833F;
            this.ラベル55.Width = 1.233514F;
            // 
            // ラベル56
            // 
            this.ラベル56.Height = 0.1979167F;
            this.ラベル56.HyperLink = null;
            this.ラベル56.Left = 4.28125F;
            this.ラベル56.Name = "ラベル56";
            this.ラベル56.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル56.Tag = "";
            this.ラベル56.Text = "消費税";
            this.ラベル56.Top = 2.020833F;
            this.ラベル56.Width = 0.733514F;
            // 
            // テキスト4
            // 
            this.テキスト4.DataField = "ITEM04";
            this.テキスト4.Height = 0.1979167F;
            this.テキスト4.Left = 0.4691671F;
            this.テキスト4.Name = "テキスト4";
            this.テキスト4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト4.Tag = "";
            this.テキスト4.Text = "ITEM04";
            this.テキスト4.Top = 4.979622F;
            this.テキスト4.Width = 1.575F;
            // 
            // テキスト8
            // 
            this.テキスト8.DataField = "ITEM08";
            this.テキスト8.Height = 0.1979167F;
            this.テキスト8.Left = 0.4691671F;
            this.テキスト8.Name = "テキスト8";
            this.テキスト8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト8.Tag = "";
            this.テキスト8.Text = "ITEM08";
            this.テキスト8.Top = 5.270961F;
            this.テキスト8.Width = 1.575F;
            // 
            // テキスト12
            // 
            this.テキスト12.DataField = "ITEM12";
            this.テキスト12.Height = 0.1979167F;
            this.テキスト12.Left = 0.4691671F;
            this.テキスト12.Name = "テキスト12";
            this.テキスト12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト12.Tag = "";
            this.テキスト12.Text = "ITEM12";
            this.テキスト12.Top = 5.556F;
            this.テキスト12.Width = 1.575F;
            // 
            // テキスト16
            // 
            this.テキスト16.DataField = "ITEM16";
            this.テキスト16.Height = 0.1979167F;
            this.テキスト16.Left = 0.4688329F;
            this.テキスト16.Name = "テキスト16";
            this.テキスト16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト16.Tag = "";
            this.テキスト16.Text = "ITEM16";
            this.テキスト16.Top = 2.319F;
            this.テキスト16.Width = 1.575F;
            // 
            // テキスト20
            // 
            this.テキスト20.DataField = "ITEM20";
            this.テキスト20.Height = 0.1979167F;
            this.テキスト20.Left = 0.4688329F;
            this.テキスト20.Name = "テキスト20";
            this.テキスト20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト20.Tag = "";
            this.テキスト20.Text = "ITEM20";
            this.テキスト20.Top = 2.602465F;
            this.テキスト20.Width = 1.575F;
            // 
            // テキスト24
            // 
            this.テキスト24.DataField = "ITEM24";
            this.テキスト24.Height = 0.1979167F;
            this.テキスト24.Left = 0.4688329F;
            this.テキスト24.Name = "テキスト24";
            this.テキスト24.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト24.Tag = "";
            this.テキスト24.Text = "ITEM24";
            this.テキスト24.Top = 2.894197F;
            this.テキスト24.Width = 1.575F;
            // 
            // テキスト28
            // 
            this.テキスト28.DataField = "ITEM28";
            this.テキスト28.Height = 0.1979167F;
            this.テキスト28.Left = 0.4688329F;
            this.テキスト28.Name = "テキスト28";
            this.テキスト28.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト28.Tag = "";
            this.テキスト28.Text = "ITEM28";
            this.テキスト28.Top = 3.19656F;
            this.テキスト28.Width = 1.575F;
            // 
            // テキスト32
            // 
            this.テキスト32.DataField = "ITEM32";
            this.テキスト32.Height = 0.1979167F;
            this.テキスト32.Left = 0.4688329F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "ITEM32";
            this.テキスト32.Top = 3.487899F;
            this.テキスト32.Width = 1.575F;
            // 
            // テキスト36
            // 
            this.テキスト36.DataField = "ITEM36";
            this.テキスト36.Height = 0.1979167F;
            this.テキスト36.Left = 0.4688329F;
            this.テキスト36.Name = "テキスト36";
            this.テキスト36.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト36.Tag = "";
            this.テキスト36.Text = "ITEM36";
            this.テキスト36.Top = 3.794197F;
            this.テキスト36.Width = 1.575F;
            // 
            // テキスト44
            // 
            this.テキスト44.DataField = "ITEM44";
            this.テキスト44.Height = 0.1979167F;
            this.テキスト44.Left = 0.4688329F;
            this.テキスト44.Name = "テキスト44";
            this.テキスト44.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト44.Tag = "";
            this.テキスト44.Text = "ITEM44";
            this.テキスト44.Top = 4.383567F;
            this.テキスト44.Width = 1.575F;
            // 
            // テキスト48
            // 
            this.テキスト48.DataField = "ITEM48";
            this.テキスト48.Height = 0.1979167F;
            this.テキスト48.Left = 0.4688329F;
            this.テキスト48.Name = "テキスト48";
            this.テキスト48.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト48.Tag = "";
            this.テキスト48.Text = "ITEM48";
            this.テキスト48.Top = 4.679237F;
            this.テキスト48.Width = 1.575F;
            // 
            // テキスト5
            // 
            this.テキスト5.DataField = "ITEM05";
            this.テキスト5.Height = 0.1979167F;
            this.テキスト5.Left = 2.14625F;
            this.テキスト5.Name = "テキスト5";
            this.テキスト5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト5.Tag = "";
            this.テキスト5.Text = "ITEM05";
            this.テキスト5.Top = 4.979622F;
            this.テキスト5.Width = 1.575F;
            // 
            // テキスト9
            // 
            this.テキスト9.DataField = "ITEM09";
            this.テキスト9.Height = 0.1979167F;
            this.テキスト9.Left = 2.14625F;
            this.テキスト9.Name = "テキスト9";
            this.テキスト9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト9.Tag = "";
            this.テキスト9.Text = "ITEM09";
            this.テキスト9.Top = 5.270961F;
            this.テキスト9.Width = 1.575F;
            // 
            // テキスト13
            // 
            this.テキスト13.DataField = "ITEM13";
            this.テキスト13.Height = 0.1979167F;
            this.テキスト13.Left = 2.14625F;
            this.テキスト13.Name = "テキスト13";
            this.テキスト13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト13.Tag = "";
            this.テキスト13.Text = "ITEM13";
            this.テキスト13.Top = 5.556F;
            this.テキスト13.Width = 1.575F;
            // 
            // テキスト17
            // 
            this.テキスト17.DataField = "ITEM17";
            this.テキスト17.Height = 0.1979167F;
            this.テキスト17.Left = 2.145916F;
            this.テキスト17.Name = "テキスト17";
            this.テキスト17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト17.Tag = "";
            this.テキスト17.Text = "ITEM17";
            this.テキスト17.Top = 2.319F;
            this.テキスト17.Width = 1.575F;
            // 
            // テキスト21
            // 
            this.テキスト21.DataField = "ITEM21";
            this.テキスト21.Height = 0.1979167F;
            this.テキスト21.Left = 2.145916F;
            this.テキスト21.Name = "テキスト21";
            this.テキスト21.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト21.Tag = "";
            this.テキスト21.Text = "ITEM21";
            this.テキスト21.Top = 2.602465F;
            this.テキスト21.Width = 1.575F;
            // 
            // テキスト25
            // 
            this.テキスト25.DataField = "ITEM25";
            this.テキスト25.Height = 0.1979167F;
            this.テキスト25.Left = 2.145916F;
            this.テキスト25.Name = "テキスト25";
            this.テキスト25.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト25.Tag = "";
            this.テキスト25.Text = "ITEM25";
            this.テキスト25.Top = 2.894197F;
            this.テキスト25.Width = 1.575F;
            // 
            // テキスト29
            // 
            this.テキスト29.DataField = "ITEM29";
            this.テキスト29.Height = 0.1979167F;
            this.テキスト29.Left = 2.145916F;
            this.テキスト29.Name = "テキスト29";
            this.テキスト29.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト29.Tag = "";
            this.テキスト29.Text = "ITEM29";
            this.テキスト29.Top = 3.19656F;
            this.テキスト29.Width = 1.575F;
            // 
            // テキスト33
            // 
            this.テキスト33.DataField = "ITEM33";
            this.テキスト33.Height = 0.1979167F;
            this.テキスト33.Left = 2.145916F;
            this.テキスト33.Name = "テキスト33";
            this.テキスト33.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト33.Tag = "";
            this.テキスト33.Text = "ITEM33";
            this.テキスト33.Top = 3.487899F;
            this.テキスト33.Width = 1.575F;
            // 
            // テキスト37
            // 
            this.テキスト37.DataField = "ITEM37";
            this.テキスト37.Height = 0.1979167F;
            this.テキスト37.Left = 2.145916F;
            this.テキスト37.Name = "テキスト37";
            this.テキスト37.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト37.Tag = "";
            this.テキスト37.Text = "ITEM37";
            this.テキスト37.Top = 3.794197F;
            this.テキスト37.Width = 1.575F;
            // 
            // テキスト41
            // 
            this.テキスト41.DataField = "ITEM41";
            this.テキスト41.Height = 0.1979167F;
            this.テキスト41.Left = 2.146064F;
            this.テキスト41.Name = "テキスト41";
            this.テキスト41.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト41.Tag = "";
            this.テキスト41.Text = "ITEM41";
            this.テキスト41.Top = 4.084109F;
            this.テキスト41.Width = 1.575F;
            // 
            // テキスト45
            // 
            this.テキスト45.DataField = "ITEM45";
            this.テキスト45.Height = 0.1979167F;
            this.テキスト45.Left = 2.145916F;
            this.テキスト45.Name = "テキスト45";
            this.テキスト45.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト45.Tag = "";
            this.テキスト45.Text = "ITEM45";
            this.テキスト45.Top = 4.383567F;
            this.テキスト45.Width = 1.575F;
            // 
            // テキスト49
            // 
            this.テキスト49.DataField = "ITEM49";
            this.テキスト49.Height = 0.1979167F;
            this.テキスト49.Left = 2.145916F;
            this.テキスト49.Name = "テキスト49";
            this.テキスト49.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト49.Tag = "";
            this.テキスト49.Text = "ITEM49";
            this.テキスト49.Top = 4.679237F;
            this.テキスト49.Width = 1.575F;
            // 
            // 合計2
            // 
            this.合計2.Height = 0.1979167F;
            this.合計2.Left = 2.145981F;
            this.合計2.Name = "合計2";
            this.合計2.OutputFormat = resources.GetString("合計2.OutputFormat");
            this.合計2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計2.Tag = "";
            this.合計2.Text = "=CLng([ITEM05])+CLng([ITEM09])+CLng([ITEM13])+CLng([ITEM17])+CLng([ITEM21])+CLng(" +
    "[ITEM25])+CLng([ITEM29])+CLng([ITEM33])+CLng([ITEM37])+CLng([ITEM41])+CLng([ITEM" +
    "45])+CLng([ITEM49])";
            this.合計2.Top = 5.858268F;
            this.合計2.Width = 1.575F;
            // 
            // テキスト6
            // 
            this.テキスト6.DataField = "ITEM06";
            this.テキスト6.Height = 0.1979167F;
            this.テキスト6.Left = 3.823334F;
            this.テキスト6.Name = "テキスト6";
            this.テキスト6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト6.Tag = "";
            this.テキスト6.Text = "ITEM06";
            this.テキスト6.Top = 4.979622F;
            this.テキスト6.Width = 1.575F;
            // 
            // テキスト10
            // 
            this.テキスト10.DataField = "ITEM10";
            this.テキスト10.Height = 0.1979167F;
            this.テキスト10.Left = 3.823334F;
            this.テキスト10.Name = "テキスト10";
            this.テキスト10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト10.Tag = "";
            this.テキスト10.Text = "ITEM10";
            this.テキスト10.Top = 5.270961F;
            this.テキスト10.Width = 1.575F;
            // 
            // テキスト14
            // 
            this.テキスト14.DataField = "ITEM14";
            this.テキスト14.Height = 0.1979167F;
            this.テキスト14.Left = 3.823334F;
            this.テキスト14.Name = "テキスト14";
            this.テキスト14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト14.Tag = "";
            this.テキスト14.Text = "ITEM14";
            this.テキスト14.Top = 5.556F;
            this.テキスト14.Width = 1.575F;
            // 
            // テキスト18
            // 
            this.テキスト18.DataField = "ITEM18";
            this.テキスト18.Height = 0.1979167F;
            this.テキスト18.Left = 3.823F;
            this.テキスト18.Name = "テキスト18";
            this.テキスト18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト18.Tag = "";
            this.テキスト18.Text = "ITEM18";
            this.テキスト18.Top = 2.319F;
            this.テキスト18.Width = 1.575F;
            // 
            // テキスト22
            // 
            this.テキスト22.DataField = "ITEM22";
            this.テキスト22.Height = 0.1979167F;
            this.テキスト22.Left = 3.823F;
            this.テキスト22.Name = "テキスト22";
            this.テキスト22.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト22.Tag = "";
            this.テキスト22.Text = "ITEM22";
            this.テキスト22.Top = 2.602465F;
            this.テキスト22.Width = 1.575F;
            // 
            // テキスト26
            // 
            this.テキスト26.DataField = "ITEM26";
            this.テキスト26.Height = 0.1979167F;
            this.テキスト26.Left = 3.823F;
            this.テキスト26.Name = "テキスト26";
            this.テキスト26.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト26.Tag = "";
            this.テキスト26.Text = "ITEM26";
            this.テキスト26.Top = 2.894197F;
            this.テキスト26.Width = 1.575F;
            // 
            // テキスト30
            // 
            this.テキスト30.DataField = "ITEM30";
            this.テキスト30.Height = 0.1979167F;
            this.テキスト30.Left = 3.823F;
            this.テキスト30.Name = "テキスト30";
            this.テキスト30.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト30.Tag = "";
            this.テキスト30.Text = "ITEM30";
            this.テキスト30.Top = 3.19656F;
            this.テキスト30.Width = 1.575F;
            // 
            // テキスト34
            // 
            this.テキスト34.DataField = "ITEM34";
            this.テキスト34.Height = 0.1979167F;
            this.テキスト34.Left = 3.823F;
            this.テキスト34.Name = "テキスト34";
            this.テキスト34.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト34.Tag = "";
            this.テキスト34.Text = "ITEM34";
            this.テキスト34.Top = 3.487899F;
            this.テキスト34.Width = 1.575F;
            // 
            // テキスト38
            // 
            this.テキスト38.DataField = "ITEM38";
            this.テキスト38.Height = 0.1979167F;
            this.テキスト38.Left = 3.823F;
            this.テキスト38.Name = "テキスト38";
            this.テキスト38.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト38.Tag = "";
            this.テキスト38.Text = "ITEM38";
            this.テキスト38.Top = 3.794197F;
            this.テキスト38.Width = 1.575F;
            // 
            // テキスト42
            // 
            this.テキスト42.DataField = "ITEM42";
            this.テキスト42.Height = 0.1979167F;
            this.テキスト42.Left = 3.823148F;
            this.テキスト42.Name = "テキスト42";
            this.テキスト42.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト42.Tag = "";
            this.テキスト42.Text = "ITEM42";
            this.テキスト42.Top = 4.084109F;
            this.テキスト42.Width = 1.575F;
            // 
            // テキスト46
            // 
            this.テキスト46.DataField = "ITEM46";
            this.テキスト46.Height = 0.1979167F;
            this.テキスト46.Left = 3.823F;
            this.テキスト46.Name = "テキスト46";
            this.テキスト46.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト46.Tag = "";
            this.テキスト46.Text = "ITEM46";
            this.テキスト46.Top = 4.383567F;
            this.テキスト46.Width = 1.575F;
            // 
            // テキスト50
            // 
            this.テキスト50.DataField = "ITEM50";
            this.テキスト50.Height = 0.1979167F;
            this.テキスト50.Left = 3.823F;
            this.テキスト50.Name = "テキスト50";
            this.テキスト50.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト50.Tag = "";
            this.テキスト50.Text = "ITEM50";
            this.テキスト50.Top = 4.679237F;
            this.テキスト50.Width = 1.575F;
            // 
            // 合計3
            // 
            this.合計3.Height = 0.1979167F;
            this.合計3.Left = 3.823065F;
            this.合計3.Name = "合計3";
            this.合計3.OutputFormat = resources.GetString("合計3.OutputFormat");
            this.合計3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計3.Tag = "";
            this.合計3.Text = "=CLng([ITEM06])+CLng([ITEM10])+CLng([ITEM14])+CLng([ITEM18])+CLng([ITEM22])+CLng(" +
    "[ITEM26])+CLng([ITEM30])+CLng([ITEM34])+CLng([ITEM38])+CLng([ITEM42])+CLng([ITEM" +
    "46])+CLng([ITEM50])";
            this.合計3.Top = 5.858268F;
            this.合計3.Width = 1.575F;
            // 
            // ラベル96
            // 
            this.ラベル96.Height = 0.2291667F;
            this.ラベル96.HyperLink = null;
            this.ラベル96.Left = 0.3958333F;
            this.ラベル96.Name = "ラベル96";
            this.ラベル96.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル96.Tag = "";
            this.ラベル96.Text = "上 記 の 通 り 証 明 し ま す 。";
            this.ラベル96.Top = 6.6875F;
            this.ラベル96.Width = 3.9375F;
            // 
            // aaa
            // 
            this.aaa.DataField = "ITEM52";
            this.aaa.Height = 0.2291667F;
            this.aaa.Left = 1.889764F;
            this.aaa.Name = "aaa";
            this.aaa.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.aaa.Tag = "";
            this.aaa.Text = "ITEM52";
            this.aaa.Top = 7.939431F;
            this.aaa.Width = 3.858333F;
            // 
            // bbb
            // 
            this.bbb.DataField = "ITEM53";
            this.bbb.Height = 0.2291667F;
            this.bbb.Left = 1.889764F;
            this.bbb.Name = "bbb";
            this.bbb.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.bbb.Tag = "";
            this.bbb.Text = "ITEM53";
            this.bbb.Top = 8.269292F;
            this.bbb.Width = 3.858333F;
            // 
            // ccc
            // 
            this.ccc.DataField = "ITEM54";
            this.ccc.Height = 0.2291667F;
            this.ccc.Left = 1.889764F;
            this.ccc.Name = "ccc";
            this.ccc.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.ccc.Tag = "";
            this.ccc.Text = "ITEM54";
            this.ccc.Top = 8.602626F;
            this.ccc.Visible = false;
            this.ccc.Width = 3.858333F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM55";
            this.txtToday.Height = 0.2291667F;
            this.txtToday.Left = 0.4166666F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.txtToday.Tag = "";
            this.txtToday.Text = "=Now()";
            this.txtToday.Top = 7.4375F;
            this.txtToday.Width = 3.879167F;
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.28125F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 2.364567F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "氏　名";
            this.ラベル0.Top = 0.9354331F;
            this.ラベル0.Width = 0.8333333F;
            // 
            // テキスト2
            // 
            this.テキスト2.DataField = "ITEM02";
            this.テキスト2.Height = 0.28125F;
            this.テキスト2.Left = 3.646457F;
            this.テキスト2.Name = "テキスト2";
            this.テキスト2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト2.Tag = "";
            this.テキスト2.Text = "ITEM02";
            this.テキスト2.Top = 0.9354331F;
            this.テキスト2.Width = 3.272441F;
            // 
            // ラベル2
            // 
            this.ラベル2.Height = 0.2291667F;
            this.ラベル2.HyperLink = null;
            this.ラベル2.Left = 0.5520833F;
            this.ラベル2.Name = "ラベル2";
            this.ラベル2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル2.Tag = "";
            this.ラベル2.Text = "買付期間";
            this.ラベル2.Top = 1.416667F;
            this.ラベル2.Width = 0.8333333F;
            // 
            // ラベル4
            // 
            this.ラベル4.Height = 0.2291667F;
            this.ラベル4.HyperLink = null;
            this.ラベル4.Left = 2.329F;
            this.ラベル4.Name = "ラベル4";
            this.ラベル4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル4.Tag = "";
            this.ラベル4.Text = "4月　1日";
            this.ラベル4.Top = 1.427F;
            this.ラベル4.Width = 0.8333333F;
            // 
            // ラベル5
            // 
            this.ラベル5.Height = 0.2291667F;
            this.ラベル5.HyperLink = null;
            this.ラベル5.Left = 4.520833F;
            this.ラベル5.Name = "ラベル5";
            this.ラベル5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル5.Tag = "";
            this.ラベル5.Text = "3月　31日";
            this.ラベル5.Top = 1.427083F;
            this.ラベル5.Width = 1.03125F;
            // 
            // 直線6
            // 
            this.直線6.Height = 4.134723F;
            this.直線6.Left = 7.083333F;
            this.直線6.LineWeight = 0F;
            this.直線6.Name = "直線6";
            this.直線6.Tag = "";
            this.直線6.Top = 1.96875F;
            this.直線6.Width = 0F;
            this.直線6.X1 = 7.083333F;
            this.直線6.X2 = 7.083333F;
            this.直線6.Y1 = 1.96875F;
            this.直線6.Y2 = 6.103473F;
            // 
            // ラベル7
            // 
            this.ラベル7.Height = 0.1979167F;
            this.ラベル7.HyperLink = null;
            this.ラベル7.Left = 5.927083F;
            this.ラベル7.Name = "ラベル7";
            this.ラベル7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル7.Tag = "";
            this.ラベル7.Text = "買付金額";
            this.ラベル7.Top = 2.020833F;
            this.ラベル7.Width = 0.9001807F;
            // 
            // テキスト7
            // 
            this.テキスト7.DataField = "ITEM07";
            this.テキスト7.Height = 0.1979167F;
            this.テキスト7.Left = 5.49F;
            this.テキスト7.Name = "テキスト7";
            this.テキスト7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト7.Tag = "";
            this.テキスト7.Text = "ITEM07";
            this.テキスト7.Top = 4.979622F;
            this.テキスト7.Width = 1.575F;
            // 
            // テキスト11
            // 
            this.テキスト11.DataField = "ITEM11";
            this.テキスト11.Height = 0.1979167F;
            this.テキスト11.Left = 5.49F;
            this.テキスト11.Name = "テキスト11";
            this.テキスト11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト11.Tag = "";
            this.テキスト11.Text = "ITEM11";
            this.テキスト11.Top = 5.270961F;
            this.テキスト11.Width = 1.575F;
            // 
            // テキスト15
            // 
            this.テキスト15.DataField = "ITEM15";
            this.テキスト15.Height = 0.1979167F;
            this.テキスト15.Left = 5.49F;
            this.テキスト15.Name = "テキスト15";
            this.テキスト15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト15.Tag = "";
            this.テキスト15.Text = "ITEM15";
            this.テキスト15.Top = 5.556F;
            this.テキスト15.Width = 1.575F;
            // 
            // テキスト19
            // 
            this.テキスト19.DataField = "ITEM19";
            this.テキスト19.Height = 0.1979167F;
            this.テキスト19.Left = 5.489666F;
            this.テキスト19.Name = "テキスト19";
            this.テキスト19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト19.Tag = "";
            this.テキスト19.Text = "ITEM19";
            this.テキスト19.Top = 2.319F;
            this.テキスト19.Width = 1.575F;
            // 
            // テキスト23
            // 
            this.テキスト23.DataField = "ITEM23";
            this.テキスト23.Height = 0.1979167F;
            this.テキスト23.Left = 5.489666F;
            this.テキスト23.Name = "テキスト23";
            this.テキスト23.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト23.Tag = "";
            this.テキスト23.Text = "ITEM23";
            this.テキスト23.Top = 2.602465F;
            this.テキスト23.Width = 1.575F;
            // 
            // テキスト27
            // 
            this.テキスト27.DataField = "ITEM27";
            this.テキスト27.Height = 0.1979167F;
            this.テキスト27.Left = 5.489666F;
            this.テキスト27.Name = "テキスト27";
            this.テキスト27.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト27.Tag = "";
            this.テキスト27.Text = "ITEM27";
            this.テキスト27.Top = 2.894197F;
            this.テキスト27.Width = 1.575F;
            // 
            // テキスト31
            // 
            this.テキスト31.DataField = "ITEM31";
            this.テキスト31.Height = 0.1979167F;
            this.テキスト31.Left = 5.489666F;
            this.テキスト31.Name = "テキスト31";
            this.テキスト31.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト31.Tag = "";
            this.テキスト31.Text = "ITEM31";
            this.テキスト31.Top = 3.19656F;
            this.テキスト31.Width = 1.575F;
            // 
            // テキスト35
            // 
            this.テキスト35.DataField = "ITEM35";
            this.テキスト35.Height = 0.1979167F;
            this.テキスト35.Left = 5.489666F;
            this.テキスト35.Name = "テキスト35";
            this.テキスト35.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト35.Tag = "";
            this.テキスト35.Text = "ITEM35";
            this.テキスト35.Top = 3.487899F;
            this.テキスト35.Width = 1.575F;
            // 
            // テキスト39
            // 
            this.テキスト39.DataField = "ITEM39";
            this.テキスト39.Height = 0.1979167F;
            this.テキスト39.Left = 5.489666F;
            this.テキスト39.Name = "テキスト39";
            this.テキスト39.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト39.Tag = "";
            this.テキスト39.Text = "ITEM39";
            this.テキスト39.Top = 3.794197F;
            this.テキスト39.Width = 1.575F;
            // 
            // テキスト43
            // 
            this.テキスト43.DataField = "ITEM43";
            this.テキスト43.Height = 0.1979167F;
            this.テキスト43.Left = 5.489814F;
            this.テキスト43.Name = "テキスト43";
            this.テキスト43.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト43.Tag = "";
            this.テキスト43.Text = "ITEM43";
            this.テキスト43.Top = 4.084109F;
            this.テキスト43.Width = 1.575F;
            // 
            // テキスト47
            // 
            this.テキスト47.DataField = "ITEM47";
            this.テキスト47.Height = 0.1979167F;
            this.テキスト47.Left = 5.489666F;
            this.テキスト47.Name = "テキスト47";
            this.テキスト47.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト47.Tag = "";
            this.テキスト47.Text = "ITEM47";
            this.テキスト47.Top = 4.383567F;
            this.テキスト47.Width = 1.575F;
            // 
            // テキスト51
            // 
            this.テキスト51.DataField = "ITEM51";
            this.テキスト51.Height = 0.1979167F;
            this.テキスト51.Left = 5.489666F;
            this.テキスト51.Name = "テキスト51";
            this.テキスト51.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト51.Tag = "";
            this.テキスト51.Text = "ITEM51";
            this.テキスト51.Top = 4.679237F;
            this.テキスト51.Width = 1.575F;
            // 
            // 合計4
            // 
            this.合計4.Height = 0.1979167F;
            this.合計4.Left = 5.489731F;
            this.合計4.Name = "合計4";
            this.合計4.OutputFormat = resources.GetString("合計4.OutputFormat");
            this.合計4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計4.Tag = "";
            this.合計4.Text = "=CLng([ITEM07])+CLng([ITEM11])+CLng([ITEM15])+CLng([ITEM19])+CLng([ITEM23])+CLng(" +
    "[ITEM27])+CLng([ITEM31])+CLng([ITEM35])+CLng([ITEM39])+CLng([ITEM43])+CLng([ITEM" +
    "47])+CLng([ITEM51])";
            this.合計4.Top = 5.858268F;
            this.合計4.Width = 1.575F;
            // 
            // 合計1
            // 
            this.合計1.Height = 0.1979167F;
            this.合計1.Left = 0.4688977F;
            this.合計1.Name = "合計1";
            this.合計1.OutputFormat = resources.GetString("合計1.OutputFormat");
            this.合計1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.合計1.Tag = "";
            this.合計1.Text = "=CDbl([ITEM04])+CDbl([ITEM08])+CDbl([ITEM12])+CDbl([ITEM16])+CDbl([ITEM20])+CDbl(" +
    "[ITEM24])+CDbl([ITEM28])+CDbl([ITEM32])+CDbl([ITEM36])+CDbl([ITEM40])+CDbl([ITEM" +
    "44])+CDbl([ITEM48])";
            this.合計1.Top = 5.858268F;
            this.合計1.Width = 1.575F;
            // 
            // テキスト40
            // 
            this.テキスト40.DataField = "ITEM40";
            this.テキスト40.Height = 0.1979167F;
            this.テキスト40.Left = 0.4689806F;
            this.テキスト40.Name = "テキスト40";
            this.テキスト40.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト40.Tag = "";
            this.テキスト40.Text = "ITEM40";
            this.テキスト40.Top = 4.084109F;
            this.テキスト40.Width = 1.575F;
            // 
            // ラベル36
            // 
            this.ラベル36.Height = 0.1979167F;
            this.ラベル36.HyperLink = null;
            this.ラベル36.Left = 6.208333F;
            this.ラベル36.Name = "ラベル36";
            this.ラベル36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル36.Tag = "";
            this.ラベル36.Text = "単位：円";
            this.ラベル36.Top = 1.6875F;
            this.ラベル36.Width = 0.71875F;
            // 
            // 直線19
            // 
            this.直線19.Height = 0F;
            this.直線19.Left = -2.980232E-08F;
            this.直線19.LineWeight = 0F;
            this.直線19.Name = "直線19";
            this.直線19.Tag = "";
            this.直線19.Top = 2.559722F;
            this.直線19.Width = 7.0875F;
            this.直線19.X1 = -2.980232E-08F;
            this.直線19.X2 = 7.0875F;
            this.直線19.Y1 = 2.559722F;
            this.直線19.Y2 = 2.559722F;
            // 
            // 直線20
            // 
            this.直線20.Height = 0F;
            this.直線20.Left = -2.980232E-08F;
            this.直線20.LineWeight = 0F;
            this.直線20.Name = "直線20";
            this.直線20.Tag = "";
            this.直線20.Top = 2.854861F;
            this.直線20.Width = 7.0875F;
            this.直線20.X1 = -2.980232E-08F;
            this.直線20.X2 = 7.0875F;
            this.直線20.Y1 = 2.854861F;
            this.直線20.Y2 = 2.854861F;
            // 
            // 直線21
            // 
            this.直線21.Height = 0F;
            this.直線21.Left = -2.980232E-08F;
            this.直線21.LineWeight = 0F;
            this.直線21.Name = "直線21";
            this.直線21.Tag = "";
            this.直線21.Top = 3.15F;
            this.直線21.Width = 7.0875F;
            this.直線21.X1 = -2.980232E-08F;
            this.直線21.X2 = 7.0875F;
            this.直線21.Y1 = 3.15F;
            this.直線21.Y2 = 3.15F;
            // 
            // 直線22
            // 
            this.直線22.Height = 0F;
            this.直線22.Left = -2.980232E-08F;
            this.直線22.LineWeight = 0F;
            this.直線22.Name = "直線22";
            this.直線22.Tag = "";
            this.直線22.Top = 3.445833F;
            this.直線22.Width = 7.0875F;
            this.直線22.X1 = -2.980232E-08F;
            this.直線22.X2 = 7.0875F;
            this.直線22.Y1 = 3.445833F;
            this.直線22.Y2 = 3.445833F;
            // 
            // 直線23
            // 
            this.直線23.Height = 0F;
            this.直線23.Left = -2.980232E-08F;
            this.直線23.LineWeight = 0F;
            this.直線23.Name = "直線23";
            this.直線23.Tag = "";
            this.直線23.Top = 3.740972F;
            this.直線23.Width = 7.0875F;
            this.直線23.X1 = -2.980232E-08F;
            this.直線23.X2 = 7.0875F;
            this.直線23.Y1 = 3.740972F;
            this.直線23.Y2 = 3.740972F;
            // 
            // 直線24
            // 
            this.直線24.Height = 0F;
            this.直線24.Left = -2.980232E-08F;
            this.直線24.LineWeight = 0F;
            this.直線24.Name = "直線24";
            this.直線24.Tag = "";
            this.直線24.Top = 4.036111F;
            this.直線24.Width = 7.0875F;
            this.直線24.X1 = -2.980232E-08F;
            this.直線24.X2 = 7.0875F;
            this.直線24.Y1 = 4.036111F;
            this.直線24.Y2 = 4.036111F;
            // 
            // 直線25
            // 
            this.直線25.Height = 0F;
            this.直線25.Left = -2.980232E-08F;
            this.直線25.LineWeight = 0F;
            this.直線25.Name = "直線25";
            this.直線25.Tag = "";
            this.直線25.Top = 4.33125F;
            this.直線25.Width = 7.0875F;
            this.直線25.X1 = -2.980232E-08F;
            this.直線25.X2 = 7.0875F;
            this.直線25.Y1 = 4.33125F;
            this.直線25.Y2 = 4.33125F;
            // 
            // 直線26
            // 
            this.直線26.Height = 0F;
            this.直線26.Left = -2.980232E-08F;
            this.直線26.LineWeight = 0F;
            this.直線26.Name = "直線26";
            this.直線26.Tag = "";
            this.直線26.Top = 4.627084F;
            this.直線26.Width = 7.0875F;
            this.直線26.X1 = -2.980232E-08F;
            this.直線26.X2 = 7.0875F;
            this.直線26.Y1 = 4.627084F;
            this.直線26.Y2 = 4.627084F;
            // 
            // 直線27
            // 
            this.直線27.Height = 0F;
            this.直線27.Left = -2.980232E-08F;
            this.直線27.LineWeight = 0F;
            this.直線27.Name = "直線27";
            this.直線27.Tag = "";
            this.直線27.Top = 4.922222F;
            this.直線27.Width = 7.0875F;
            this.直線27.X1 = -2.980232E-08F;
            this.直線27.X2 = 7.0875F;
            this.直線27.Y1 = 4.922222F;
            this.直線27.Y2 = 4.922222F;
            // 
            // 直線30
            // 
            this.直線30.Height = 0F;
            this.直線30.Left = -2.980232E-08F;
            this.直線30.LineWeight = 0F;
            this.直線30.Name = "直線30";
            this.直線30.Tag = "";
            this.直線30.Top = 5.217361F;
            this.直線30.Width = 7.0875F;
            this.直線30.X1 = -2.980232E-08F;
            this.直線30.X2 = 7.0875F;
            this.直線30.Y1 = 5.217361F;
            this.直線30.Y2 = 5.217361F;
            // 
            // 直線31
            // 
            this.直線31.Height = 0F;
            this.直線31.Left = -2.980232E-08F;
            this.直線31.LineWeight = 0F;
            this.直線31.Name = "直線31";
            this.直線31.Tag = "";
            this.直線31.Top = 5.5125F;
            this.直線31.Width = 7.0875F;
            this.直線31.X1 = -2.980232E-08F;
            this.直線31.X2 = 7.0875F;
            this.直線31.Y1 = 5.5125F;
            this.直線31.Y2 = 5.5125F;
            // 
            // 直線52
            // 
            this.直線52.Height = 0F;
            this.直線52.Left = -2.980232E-08F;
            this.直線52.LineWeight = 0F;
            this.直線52.Name = "直線52";
            this.直線52.Tag = "";
            this.直線52.Top = 6.104167F;
            this.直線52.Width = 7.0875F;
            this.直線52.X1 = -2.980232E-08F;
            this.直線52.X2 = 7.0875F;
            this.直線52.Y1 = 6.104167F;
            this.直線52.Y2 = 6.104167F;
            // 
            // 直線51
            // 
            this.直線51.Height = 0F;
            this.直線51.Left = -2.980232E-08F;
            this.直線51.LineWeight = 0F;
            this.直線51.Name = "直線51";
            this.直線51.Tag = "";
            this.直線51.Top = 5.808333F;
            this.直線51.Width = 7.0875F;
            this.直線51.X1 = -2.980232E-08F;
            this.直線51.X2 = 7.0875F;
            this.直線51.Y1 = 5.808333F;
            this.直線51.Y2 = 5.808333F;
            // 
            // label1
            // 
            this.label1.DataField = "ITEM58";
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 3.198032F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: center";
            this.label1.Text = "仲買人コード";
            this.label1.Top = 0.9354331F;
            this.label1.Width = 0.4519687F;
            // 
            // label2
            // 
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 5.816929F;
            this.label2.Name = "label2";
            this.label2.Style = "";
            this.label2.Text = "先頭～";
            this.label2.Top = 1.216536F;
            this.label2.Visible = false;
            this.label2.Width = 0.4582677F;
            // 
            // label3
            // 
            this.label3.Height = 0.2F;
            this.label3.HyperLink = null;
            this.label3.Left = 6.962599F;
            this.label3.Name = "label3";
            this.label3.Style = "";
            this.label3.Text = "最後";
            this.label3.Top = 1.216536F;
            this.label3.Visible = false;
            this.label3.Width = 0.3248029F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM56";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 5.279528F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: White";
            this.textBox1.Text = "ITEM56";
            this.textBox1.Top = 1.216536F;
            this.textBox1.Visible = false;
            this.textBox1.Width = 0.5374017F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM57";
            this.textBox2.Height = 0.2F;
            this.textBox2.Left = 6.341733F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White";
            this.textBox2.Text = "ITEM57";
            this.textBox2.Top = 1.216536F;
            this.textBox2.Visible = false;
            this.textBox2.Width = 0.5854335F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // HNYR1061R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 1.181102F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 1.181102F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.287402F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: normal; font-family: \"MS UI Gothic\"; ddo-char-set: " +
            "128", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: normal; font-style: inherit; font-family: \"MS UI Go" +
            "thic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: normal; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aaa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bbb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.合計1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト100;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線15;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線16;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線17;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線18;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線19;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線20;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線21;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線22;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線23;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線24;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線25;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線26;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線27;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線30;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線31;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線32;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線33;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線34;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル35;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル37;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル38;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル39;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル40;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル41;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル42;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル44;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル47;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル48;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル49;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線51;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線52;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル53;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル54;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル55;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計3;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル96;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox aaa;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox bbb;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ccc;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル4;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル5;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線6;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 合計1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト40;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル36;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
    }
}
