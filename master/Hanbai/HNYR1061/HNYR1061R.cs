﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using jp.co.fsi.common.report;
using System.Data;
using System.Globalization;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnyr1061
{
    /// <summary>
    /// HNYR1061R の概要の説明です。
    /// </summary>
    public partial class HNYR1061R : BaseReport
    {
        public HNYR1061R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, EventArgs e)
        {
            ////西暦から和暦に変換
            //CultureInfo culture = new CultureInfo("ja-JP", true);
            //culture.DateTimeFormat.Calendar = new JapaneseCalendar();
            //txtToday.Text = DateTime.Now.ToString("gg　yy年　M月　d日", culture);

            this.合計1.Value = Util.ToDecimal(this.テキスト4.Value) + Util.ToDecimal(this.テキスト8.Value) + Util.ToDecimal(this.テキスト12.Value) + Util.ToDecimal(this.テキスト16.Value) + Util.ToDecimal(this.テキスト20.Value) + Util.ToDecimal(this.テキスト24.Value)
                        + Util.ToDecimal(this.テキスト28.Value) + Util.ToDecimal(this.テキスト32.Value) + Util.ToDecimal(this.テキスト36.Value) + Util.ToDecimal(this.テキスト40.Value) + Util.ToDecimal(this.テキスト44.Value) + Util.ToDecimal(this.テキスト48.Value);

            this.合計2.Value = Util.ToDecimal(this.テキスト5.Value) + Util.ToDecimal(this.テキスト9.Value) + Util.ToDecimal(this.テキスト13.Value) + Util.ToDecimal(this.テキスト17.Value) + Util.ToDecimal(this.テキスト21.Value) + Util.ToDecimal(this.テキスト25.Value)
                                    + Util.ToDecimal(this.テキスト29.Value) + Util.ToDecimal(this.テキスト33.Value) + Util.ToDecimal(this.テキスト37.Value) + Util.ToDecimal(this.テキスト41.Value) + Util.ToDecimal(this.テキスト45.Value) + Util.ToDecimal(this.テキスト49.Value);

            this.合計3.Value = Util.ToDecimal(this.テキスト6.Value) + Util.ToDecimal(this.テキスト10.Value) + Util.ToDecimal(this.テキスト14.Value) + Util.ToDecimal(this.テキスト18.Value) + Util.ToDecimal(this.テキスト22.Value) + Util.ToDecimal(this.テキスト26.Value)
                                    + Util.ToDecimal(this.テキスト30.Value) + Util.ToDecimal(this.テキスト34.Value) + Util.ToDecimal(this.テキスト38.Value) + Util.ToDecimal(this.テキスト42.Value) + Util.ToDecimal(this.テキスト46.Value) + Util.ToDecimal(this.テキスト50.Value);

            this.合計4.Value = Util.ToDecimal(this.テキスト7.Value) + Util.ToDecimal(this.テキスト11.Value) + Util.ToDecimal(this.テキスト15.Value) + Util.ToDecimal(this.テキスト19.Value) + Util.ToDecimal(this.テキスト23.Value) + Util.ToDecimal(this.テキスト27.Value)
                                    + Util.ToDecimal(this.テキスト31.Value) + Util.ToDecimal(this.テキスト35.Value) + Util.ToDecimal(this.テキスト39.Value) + Util.ToDecimal(this.テキスト43.Value) + Util.ToDecimal(this.テキスト47.Value) + Util.ToDecimal(this.テキスト51.Value);
        }
    }
}
