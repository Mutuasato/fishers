﻿namespace jp.co.fsi.hn.hnyr1061
{
    partial class HNYR1061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblDateYear = new System.Windows.Forms.Label();
			this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateGengo = new System.Windows.Forms.Label();
			this.lblNakagaininCdTo = new System.Windows.Forms.Label();
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.txtNakagaininCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblNakagaininCdFr = new System.Windows.Forms.Label();
			this.txtNakagaininCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 609);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.Text = "";
			// 
			// lblDateYear
			// 
			this.lblDateYear.BackColor = System.Drawing.Color.Silver;
			this.lblDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateYear.ForeColor = System.Drawing.Color.Black;
			this.lblDateYear.Location = new System.Drawing.Point(241, -1);
			this.lblDateYear.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateYear.Name = "lblDateYear";
			this.lblDateYear.Size = new System.Drawing.Size(27, 32);
			this.lblDateYear.TabIndex = 3;
			this.lblDateYear.Tag = "CHANGE";
			this.lblDateYear.Text = "年";
			this.lblDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDateYear
			// 
			this.txtDateYear.AutoSizeFromLength = false;
			this.txtDateYear.DisplayLength = null;
			this.txtDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtDateYear.ForeColor = System.Drawing.Color.Black;
			this.txtDateYear.Location = new System.Drawing.Point(199, 4);
			this.txtDateYear.Margin = new System.Windows.Forms.Padding(5);
			this.txtDateYear.MaxLength = 2;
			this.txtDateYear.Name = "txtDateYear";
			this.txtDateYear.Size = new System.Drawing.Size(39, 23);
			this.txtDateYear.TabIndex = 1;
			this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
			// 
			// lblDateGengo
			// 
			this.lblDateGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblDateGengo.ForeColor = System.Drawing.Color.Black;
			this.lblDateGengo.Location = new System.Drawing.Point(139, 3);
			this.lblDateGengo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblDateGengo.Name = "lblDateGengo";
			this.lblDateGengo.Size = new System.Drawing.Size(55, 24);
			this.lblDateGengo.TabIndex = 1;
			this.lblDateGengo.Tag = "DISPNAME";
			this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblNakagaininCdTo
			// 
			this.lblNakagaininCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblNakagaininCdTo.Location = new System.Drawing.Point(588, 3);
			this.lblNakagaininCdTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
			this.lblNakagaininCdTo.Size = new System.Drawing.Size(289, 24);
			this.lblNakagaininCdTo.TabIndex = 4;
			this.lblNakagaininCdTo.Tag = "DISPNAME";
			this.lblNakagaininCdTo.Text = "最　後";
			this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblCodeBet.Location = new System.Drawing.Point(496, 0);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 32);
			this.lblCodeBet.TabIndex = 2;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNakagaininCdFr
			// 
			this.txtNakagaininCdFr.AutoSizeFromLength = false;
			this.txtNakagaininCdFr.DisplayLength = null;
			this.txtNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtNakagaininCdFr.Location = new System.Drawing.Point(140, 4);
			this.txtNakagaininCdFr.Margin = new System.Windows.Forms.Padding(5);
			this.txtNakagaininCdFr.MaxLength = 4;
			this.txtNakagaininCdFr.Name = "txtNakagaininCdFr";
			this.txtNakagaininCdFr.Size = new System.Drawing.Size(52, 23);
			this.txtNakagaininCdFr.TabIndex = 2;
			this.txtNakagaininCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNakagaininCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
			// 
			// lblNakagaininCdFr
			// 
			this.lblNakagaininCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblNakagaininCdFr.Location = new System.Drawing.Point(197, 3);
			this.lblNakagaininCdFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
			this.lblNakagaininCdFr.Size = new System.Drawing.Size(289, 24);
			this.lblNakagaininCdFr.TabIndex = 1;
			this.lblNakagaininCdFr.Tag = "DISPNAME";
			this.lblNakagaininCdFr.Text = "先　頭";
			this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNakagaininCdTo
			// 
			this.txtNakagaininCdTo.AutoSizeFromLength = false;
			this.txtNakagaininCdTo.DisplayLength = null;
			this.txtNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtNakagaininCdTo.Location = new System.Drawing.Point(531, 4);
			this.txtNakagaininCdTo.Margin = new System.Windows.Forms.Padding(5);
			this.txtNakagaininCdTo.MaxLength = 4;
			this.txtNakagaininCdTo.Name = "txtNakagaininCdTo";
			this.txtNakagaininCdTo.Size = new System.Drawing.Size(52, 23);
			this.txtNakagaininCdTo.TabIndex = 3;
			this.txtNakagaininCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtNakagaininCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNakagaininCdTo_KeyDown);
			this.txtNakagaininCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(139, 2);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(184, 1);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(934, 29);
			this.lblMizuageShisho.TabIndex = 0;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "水揚支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(934, 31);
			this.label1.TabIndex = 902;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "年指定";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(934, 31);
			this.label2.TabIndex = 902;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "仲買人CD範囲";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(944, 119);
			this.fsiTableLayoutPanel1.TabIndex = 903;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtNakagaininCdFr);
			this.fsiPanel3.Controls.Add(this.txtNakagaininCdTo);
			this.fsiPanel3.Controls.Add(this.lblNakagaininCdTo);
			this.fsiPanel3.Controls.Add(this.lblNakagaininCdFr);
			this.fsiPanel3.Controls.Add(this.lblCodeBet);
			this.fsiPanel3.Controls.Add(this.label2);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(5, 83);
			this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(934, 31);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblDateGengo);
			this.fsiPanel2.Controls.Add(this.txtDateYear);
			this.fsiPanel2.Controls.Add(this.lblDateYear);
			this.fsiPanel2.Controls.Add(this.label1);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 43);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(934, 31);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(934, 29);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// HNYR1061
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNYR1061";
			this.Text = "";
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private System.Windows.Forms.Label lblDateYear;
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtNakagaininCdFr;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.FsiTextBox txtNakagaininCdTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}