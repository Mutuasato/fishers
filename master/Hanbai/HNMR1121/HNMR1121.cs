﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1121
{
    /// <summary>
    /// 販売未払金一覧表(HNMR1121)
    /// </summary>
    public partial class HNMR1121 : BasePgForm
    {
        //定数
        public const int prtCols = 10;

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        private decimal MOTOCHO_KUBUN = 12;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1121()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = UInfo.ShishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 現在の年号を取得する
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            //今月の最初の日を取得する
            DateTime dtMonthFr = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            string[] jpMontFr = Util.ConvJpDate(dtMonthFr, this.Dba);
            // 日付範囲前
            this.lblDateGengoFr.Text = jpMontFr[0];
            this.txtDateYearFr.Text = jpMontFr[2];
            this.txtDateMonthFr.Text = jpMontFr[3];
            this.txtDateDayFr.Text = jpMontFr[4];
            // 日付範囲後
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            MOTOCHO_KUBUN = (Util.ToDecimal(this.Par1) == 0) ? MOTOCHO_KUBUN : Util.ToDecimal(this.Par1);
            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年),船主CDにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                #endregion

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        public override void PressF8()
        {
            EditKamoku("1");
        }

        public override void PressF9()
        {
            EditKamoku("2");
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNMR1121R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }
        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        ///// <summary>
        ///// 検索サブウィンドウオープン
        ///// </summary>
        ///// <param name="moduleName">例："COMC8111"</param>
        ///// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        ///// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        ///// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        ///// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        //private String[] openSearchWindow(String moduleName, String para1, String indata)
        //{
        //    string[] result = { "", "" };

        //    // ネームスペースに使うモジュール名の小文字
        //    string lowerModuleName = moduleName.ToLower();

        //    // ネームスペースの末尾
        //    string nameSpace = lowerModuleName.Substring(0, 3);

        //    // アセンブリのロード
        //    Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

        //    // フォーム作成
        //    string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
        //    Type t = asm.GetType(moduleNameSpace);

        //    if (t != null)
        //    {
        //        Object obj = Activator.CreateInstance(t);
        //        if (obj != null)
        //        {
        //            BasePgForm frm = (BasePgForm)obj;
        //            frm.Par1 = para1;
        //            frm.InData = indata;
        //            frm.ShowDialog(this);

        //            if (frm.DialogResult == DialogResult.OK)
        //            {
        //                string[] ret = (string[])frm.OutData;
        //                result[0] = ret[0];
        //                result[1] = ret[1];
        //                return result;
        //            }
        //        }
        //    }

        //    return result;
        //}
        /// <summary>
        /// 勘定科目の設定画面表示
        /// </summary>
        /// <param name="code">登録対象: 1 期首残科目、 2 入金科目</param>
        private void EditKamoku(string code)
        {
            // アセンブリのロード
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNMR1111.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.hn.hnmr1111.HNMR1112");
            if (t != null)
            {
                Object obj = Activator.CreateInstance(t, code);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    
                    frm.Par2 = this.txtMizuageShishoCd.Text;
                    frm.Par3 = MOTOCHO_KUBUN.ToString();
                    frm.ShowDialog(this);
                    if (frm.DialogResult == DialogResult.OK)
                    {
                        // 開始年に再度フォーカスをセット
                        this.ActiveControl = this.txtDateYearFr;
                        this.txtDateYearFr.Focus();
                    }
                }
            }

        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNMR1121R rpt = new HNMR1121R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                // デバッグ時はデータの確認をしたいので、データが残るようにする。
                this.Dba.Commit();
#else
                // 印刷後なので、帳票出力用ワークテーブルをロールバックで元に戻す
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region 準備
            // 会計期間開始日を取得
            DateTime kaikeiNendoKaishiBi = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);

            // 会計年度の設定
            string kaikeiNendo;
            if (kaikeiNendoKaishiBi.Month <= tmpDateFr.Month)
            {
                kaikeiNendo = Util.ToString(tmpDateFr.Year);
            }
            else
            {
                kaikeiNendo = Util.ToString(tmpDateFr.Year - 1);
            }
            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpjpDateTo2 = Util.ConvJpDate(tmpDateTo2, this.Dba);

            // 表示する日付を設定する
            string hyojiDate; // 表示用日付
            string dateFrChk = string.Format("{0}{1}{2}{3}", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4]);
            string dateToChk = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            string dateFrChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], "1");
            string dateToChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo2[4]);
            if (dateFrChk == dateFrChk2 && dateToChk == dateToChk2)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // ループ用カウント変数
            int i = 1; 

            // 帳票表示用変数
            Decimal suryo = 0;
            Decimal shohizei = 0;
            Decimal karikata = 0;
            Decimal kashikata = 0;
            Decimal zandaka = 0;

            // 仲買人コード設定
            string FunanushiCdFr;
            string FunanushiCdTo;
            if (Util.ToString(txtFunanushiCdFr.Text) != "")
            {
                FunanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                FunanushiCdFr = "0";
            }
            if (Util.ToString(txtFunanushiCdTo.Text) != "")
            {
                FunanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                FunanushiCdTo = "9999";
            }
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            #endregion

            #region メインデータの取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            Sql.Append(" WITH ZK AS ");
            Sql.Append(" ( ");
            Sql.Append("       SELECT ");
            Sql.Append("            KAISHA_CD ");
            Sql.Append("       ,    SHISHO_CD ");
            Sql.Append("       ,    KANJO_KAMOKU_CD ");
            Sql.Append("       ,    MOTOCHO_KUBUN ");
            Sql.Append("       FROM ");
            Sql.Append("            TB_HN_KISHU_ZAN_KAMOKU ");
            Sql.Append("       WHERE ");
            Sql.Append("            KAISHA_CD = @KAISHA_CD ");
            Sql.Append("         AND ");
            Sql.Append("            SHISHO_CD = @SHISHO_CD ");
            Sql.Append("         AND ");
            Sql.Append("            MOTOCHO_KUBUN = @MOTOCHO_KUBUN ");
            Sql.Append(" ) ");
            Sql.Append(" , ");
            Sql.Append(" ZZ AS ");
            Sql.Append(" ( ");
            Sql.Append(" SELECT     ");
            Sql.Append(" A.KAISHA_CD,");
            Sql.Append(" A.HOJO_KAMOKU_CD,");
            Sql.Append(" SUM(CASE WHEN A.TAISHAKU_KUBUN = C.TAISHAKU_KUBUN ");
            Sql.Append(" 		 THEN A.ZEIKOMI_KINGAKU ");
            Sql.Append(" 		 ELSE (A.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA,");
            Sql.Append(" SUM(CASE WHEN A.DENPYO_DATE < @DENPYO_DATE_FR ");
            Sql.Append(" 		 THEN");
            Sql.Append(" 			 CASE WHEN A.TAISHAKU_KUBUN = C.TAISHAKU_KUBUN ");
            Sql.Append(" 				  THEN A.ZEIKOMI_KINGAKU ");
            Sql.Append(" 				  ELSE (A.ZEIKOMI_KINGAKU * -1) END");
            Sql.Append(" 		 ELSE 0");
            Sql.Append(" 		 END) AS KISHU_ZANDAKA");
            Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS A");
            Sql.Append("   INNER JOIN ZK AS B ");
            Sql.Append(" 	ON	A.KAISHA_CD			= B.KAISHA_CD   ");
            Sql.Append(" 	AND A.SHISHO_CD			= B.SHISHO_CD  ");
            Sql.Append(" 	AND A.KANJO_KAMOKU_CD	= B.KANJO_KAMOKU_CD   ");
            Sql.Append(" INNER JOIN TB_ZM_KANJO_KAMOKU AS C ");
            Sql.Append(" 	ON	A.KAISHA_CD			= C.KAISHA_CD   ");
            Sql.Append(" 	AND A.KANJO_KAMOKU_CD	= C.KANJO_KAMOKU_CD ");
            Sql.Append(" 	AND A.KAIKEI_NENDO		= C.KAIKEI_NENDO   ");
            Sql.Append(" WHERE");
            Sql.Append("    A.KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append("    A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
            {
                Sql.Append("    A.SHISHO_CD = @SHISHO_CD AND");
            }
            Sql.Append("    A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append("    A.DENPYO_DATE < @DENPYO_DATE_TO");
            Sql.Append(" GROUP BY");
            Sql.Append("    A.KAISHA_CD,");
            Sql.Append("    A.HOJO_KAMOKU_CD");
            Sql.Append(" )");
            Sql.Append(" SELECT ");
            Sql.Append(" T.TORIHIKISAKI_CD AS KAIIN_BANGO, ");
            Sql.Append(" T.TORIHIKISAKI_NM AS KAIIN_NM, ");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA, ");
            Sql.Append(" ISNULL(Z.KISHU_ZANDAKA, 0) AS KISHU_ZAN ");
            Sql.Append(" FROM VI_HN_TORIHIKISAKI_JOHO T ");
            Sql.Append(" INNER JOIN TB_HN_FUNANUSHI_JOHO TM ");
            Sql.Append(" ON (T.KAISHA_CD = TM.KAISHA_CD AND T.TORIHIKISAKI_CD = TM.TORIHIKISAKI_CD AND TM.GYOHO_CD != 0) ");
            Sql.Append(" LEFT OUTER JOIN ZZ AS Z ");
            Sql.Append(" ON	 T.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND T.TORIHIKISAKI_CD = Z.HOJO_KAMOKU_CD");
            Sql.Append(" WHERE");
            Sql.Append("     T.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append("     T.TORIHIKISAKI_CD  BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO AND");
            Sql.Append("     T.TORIHIKISAKI_KUBUN1 = 1");
            Sql.Append(" ORDER BY");
            Sql.Append("     T.TORIHIKISAKI_CD ASC");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 2, this.UInfo.KessanKi);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 4, FunanushiCdFr);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 4, FunanushiCdTo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    #region 貸方情報取得
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("SELECT");
                    Sql.Append(" B.ZEIKOMI_KINGAKU AS ZEIKOMI_KINGAKU ");
                    Sql.Append("FROM");
                    Sql.Append(" TB_HN_NYUKIN_KAMOKU AS A ");
                    Sql.Append("LEFT OUTER JOIN");
                    Sql.Append(" TB_ZM_SHIWAKE_MEISAI AS B ");
                    Sql.Append("ON");
                    Sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
                    Sql.Append(" A.SHISHO_CD = B.SHISHO_CD AND");
                    Sql.Append(" 1 = B.DENPYO_KUBUN AND");
                    Sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
                    Sql.Append("LEFT OUTER JOIN");
                    Sql.Append(" TB_ZM_KANJO_KAMOKU AS C ");
                    Sql.Append("ON");
                    Sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
                    Sql.Append(" A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD AND");
                    Sql.Append(" B.KAIKEI_NENDO = C.KAIKEI_NENDO ");
                    Sql.Append("WHERE");
                    Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
                    Sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                    Sql.Append(" B.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND");
                    Sql.Append(" B.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND");
                    Sql.Append(" B.TAISHAKU_KUBUN = C.TAISHAKU_KUBUN AND");
                    Sql.Append(" A.MOTOCHO_KUBUN = @MOTOCHO_KUBUN AND");
                    Sql.Append(" B.SHIWAKE_SAKUSEI_KUBUN IS NULL");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                    dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.VarChar, 4, Util.ToDecimal(dr["KAIIN_BANGO"]));
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                    dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);

                    DataTable dtKashikata2 = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                    #endregion

                    #region 借方情報取得
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("SELECT");
                    Sql.Append(" B.ZEIKOMI_KINGAKU AS ZEIKOMI_KINGAKU ");
                    Sql.Append("FROM");
                    Sql.Append(" TB_HN_NYUKIN_KAMOKU AS A ");
                    Sql.Append("LEFT OUTER JOIN");
                    Sql.Append(" TB_ZM_SHIWAKE_MEISAI AS B ");
                    Sql.Append("ON");
                    Sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
                    Sql.Append(" A.SHISHO_CD = B.SHISHO_CD AND");
                    Sql.Append(" 1 = B.DENPYO_KUBUN AND");
                    Sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
                    Sql.Append("LEFT OUTER JOIN");
                    Sql.Append(" TB_ZM_KANJO_KAMOKU AS C ");
                    Sql.Append("ON");
                    Sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
                    Sql.Append(" A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD AND");
                    Sql.Append(" B.KAIKEI_NENDO = C.KAIKEI_NENDO ");
                    Sql.Append("WHERE");
                    Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
                    if (shishoCd != "0")
                    {
                        Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
                    }
                    Sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                    Sql.Append(" B.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND");
                    Sql.Append(" B.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND");
                    Sql.Append(" B.TAISHAKU_KUBUN <> C.TAISHAKU_KUBUN AND");
                    Sql.Append(" A.MOTOCHO_KUBUN = @MOTOCHO_KUBUN ");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                    dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.VarChar, 4, Util.ToDecimal(dr["KAIIN_BANGO"]));
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                    dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);

                    DataTable dtKarikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                    #endregion

                    #region 発生数量,当月貸方発生,消費税額
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("SELECT");
                    Sql.Append(" A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
                    Sql.Append(" SUM(A.SURYO) AS MIZUAGE_SURYO,");
                    //Sql.Append(" A.MIZUAGE_SHOHIZEIGAKU AS SHOHIZEI,");
                    Sql.Append(" sum(A.SHOHIZEI_FURIWAKE) AS SHOHIZEI,");
                    Sql.Append(" SUM(A.URIAGE_KINGAKU) AS URIAGE_KINGAKU ");
                    Sql.Append("FROM");
                    Sql.Append(" VI_HN_SERI_MOTOCHO AS A ");
                    Sql.Append("WHERE");
                    Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
                    if (shishoCd != "0")
                        Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
                    Sql.Append(" A.TORIHIKI_KUBUN1 = 1 AND");
                    Sql.Append(" A.SEISAN_BI BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND");
                    Sql.Append(" A.SENSHU_CD = @FUNANUSHI_CD ");
                    Sql.Append("GROUP BY");
                    Sql.Append(" A.SEISAN_BI,");
                    Sql.Append(" A.SENSHU_CD,");
                    Sql.Append(" A.ZEI_RITSU,");
                    Sql.Append(" A.SHOHIZEI_NYURYOKU_HOHO,");
                    Sql.Append(" A.DENPYO_BANGO,");
                    Sql.Append(" A.MIZUAGE_SHOHIZEIGAKU ");
                    Sql.Append("ORDER BY");
                    Sql.Append(" A.SENSHU_CD");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                    dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@FUNANUSHI_CD", SqlDbType.VarChar, 4, Util.ToDecimal(dr["KAIIN_BANGO"]));
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

                    DataTable dtKashikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                    #endregion

                    if (!(0.Equals(dtKashikata.Rows.Count) && 0.Equals(dtKarikata.Rows.Count)) || Util.ToDecimal(dr["ZANDAKA"]) > 0)
                    {
                        // 金額の計算
                        foreach (DataRow dr2 in dtKashikata2.Rows)
                        {
                            kashikata += Util.ToDecimal(dr2["ZEIKOMI_KINGAKU"]);
                        }
                        foreach (DataRow dr2 in dtKashikata.Rows)
                        {
                            suryo += Util.ToDecimal(dr2["MIZUAGE_SURYO"]);
                            //kashikata += Util.ToDecimal(dr2["URIAGE_KINGAKU"]) + Util.ToDecimal(dr2["SHOHIZEI"]);
                            //shohizei += Util.ToDecimal(dr2["SHOHIZEI"]);                     
                            if (Util.ToDecimal(dr2["SHOHIZEI_NYURYOKU_HOHO"]).Equals(TaxUtil.INPUT_CATEGORY_IMPOSITION))
                            {
                                kashikata += Util.ToDecimal(dr2["URIAGE_KINGAKU"]) + Util.ToDecimal(dr2["SHOHIZEI"]);
                            }
                            else
                            {
                                kashikata += Util.ToDecimal(dr2["URIAGE_KINGAKU"]);
                            }
                            shohizei += Util.ToDecimal(dr2["SHOHIZEI"]);
                        }
                        foreach (DataRow dr2 in dtKarikata.Rows)
                        {
                            karikata += Util.ToDecimal(dr2["ZEIKOMI_KINGAKU"]);
                        }

                        zandaka = Util.ToDecimal(dr["KISHU_ZAN"]) + kashikata - karikata;

                        // 取得したデータをワークテーブルに更新をする
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyojiDate);
                        // データを設定
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["KAIIN_BANGO"]);
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["KAIIN_NM"]);
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.FormatNum(suryo, 1));
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(Util.ToDecimal(dr["KISHU_ZAN"])));
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(karikata));
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(kashikata));
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(zandaka));
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(shohizei));
                        
                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                        // 金額のリセット
                        suryo = 0;
                        karikata = 0;
                        kashikata = 0;
                        shohizei = 0;
                        i++;
                    }

                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }
            
            return dataFlag;
        }
        #endregion
    }
}
