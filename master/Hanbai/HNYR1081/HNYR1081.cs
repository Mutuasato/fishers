﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hnyr1081
{
    #region 構造体
    /// <summary>
    /// 印刷ワークテーブルのデータ構造体
    /// </summary>
    struct NumVals
    {
        public decimal MIZUAGESURYO;
        public decimal MIZUAGEKINGAKU;
        public decimal KENSU;

        public void Clear()
        {
            MIZUAGESURYO = 0;
            MIZUAGEKINGAKU = 0;
            KENSU = 0;
        }
    }
    #endregion

    /// <summary>
    /// 漁態別漁種別水揚月報(HNYR1081)
    /// </summary>
    public partial class HNYR1081 : BasePgForm
    {    
        #region 定数
        /// <summary>
        /// 精算区分(地区内ｾﾘ)
        /// </summary>
        private const string CHIKUNAI = "3";

        /// <summary>
        /// 精算区分(浜売り)
        /// </summary>
        private const string HAMA_URI = "2";

        /// <summary>
        /// 精算区分(地区外ｾﾘ)
        /// </summary>
        private const string CHIKUGAI = "1";

        /// <summary>
        /// 印刷テーブル列数
        /// </summary>
        private const int prtCols = 20;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNYR1081()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            //rdoAll.Checked = true;
            rdogyoshubetsu.Checked = true;
            // 精算区分
            this.txtSeisanKbn.Text = "0";
            IsValidSeisanKbn();

            txtDateYearFr.Focus();
            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                case "txtSeisanKbn":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtSeisanKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtSeisanKbn.Text = outData[0];
                                this.lblSeisanKbnNm.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNYR1081R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;

                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 精算区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeisanKbn())
            {
                e.Cancel = true;
                this.txtSeisanKbn.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }

        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        private bool IsValidSeisanKbn()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtSeisanKbn.Text, this.txtSeisanKbn.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text) || this.txtSeisanKbn.Text == "0")
            {
                this.txtSeisanKbn.Text = "0";
                this.lblSeisanKbnNm.Text = "全て";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                Msg.Notice("精算区分は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", this.txtMizuageShishoCd.Text, this.txtSeisanKbn.Text);
                if (ValChk.IsEmpty(this.lblSeisanKbnNm.Text))
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba));
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Notice("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Notice("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 精算区分の入力チェック
            if (!IsValidSeisanKbn())
            {
                this.txtSeisanKbn.Focus();
                this.txtSeisanKbn.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                int RepType = rdogyoshubetsu.Checked == true ? 1 : 2;
                dataFlag = MakeWkData(RepType);
                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNYR1081R rpt = new HNYR1081R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.Text;
                    rpt.Document.Name = this.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        #region 魚種別データ作成
        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。漁態魚種別水揚月報
        /// </summary>
        //private bool MakeWkData()
        //{
        //    #region メインデータを取得
        //    DbParamCollection dpc = new DbParamCollection();
        //    // 日付範囲を西暦にして取得
        //    DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
        //            this.txtDateMonthFr.Text, "1", this.Dba);

        //    int lastDayInMonth = DateTime.DaysInMonth(Util.ToInt(this.txtDateYearTo.Text), Util.ToInt(this.txtDateMonthTo.Text));

        //    DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
        //            this.txtDateMonthTo.Text, Util.ToString(lastDayInMonth), this.Dba);

        //    // 日付範囲を和暦で保持
        //    string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
        //    string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

        //    // 表示日付
        //    string hyojidate = tmpjpDateFr[0] + tmpjpDateFr[2] + "年" + tmpjpDateFr[3] +"月 ～ "+
        //                       tmpjpDateTo[0] + tmpjpDateTo[2] + "年" + tmpjpDateTo[3] + "月分";

        //    string hyoujititle;
        //    string headertitle;
        //    hyoujititle = "漁 態 別 魚 種 別 水 揚 月 報";
        //    headertitle = "魚種コード / 魚 種 名 称";

        //    // 船主コード設定
        //    string funanushiCdFr;
        //    string funanushiCdTo;
        //    if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
        //    {
        //        funanushiCdFr = txtFunanushiCdFr.Text;
        //    }
        //    else
        //    {
        //        funanushiCdFr = "0";
        //    }
        //    if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
        //    {
        //        funanushiCdTo = txtFunanushiCdTo.Text;
        //    }
        //    else
        //    {
        //        funanushiCdTo = "99999";
        //    }
        //    #region 支所コードの退避
        //    //支所コードの退避
        //    string shishoCd = this.txtMizuageShishoCd.Text;
        //    #endregion

        //    int i; // ループ用カウント変数
        //    DateTime dt = DateTime.MinValue; // 精算日

        //    // 精算区分設定
        //    string seisanKubunhyoji = this.lblSeisanKbnNm.Text;
        //    string seisanKbn = this.txtSeisanKbn.Text;

        //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
        //    // 検索する日付をセット
        //    dpc.SetParam("@SERIBI_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
        //    dpc.SetParam("@SERIBI_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
        //    dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, funanushiCdFr);
        //    dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, funanushiCdTo);
        //    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
        //    dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKbn);

        //    StringBuilder Sql = new StringBuilder();
        //    Sql.Append(" SELECT ");
        //    Sql.Append(" A.KAISHA_CD,");
        //    if (shishoCd!="0")
        //        Sql.Append(" A.SHISHO_CD,");
        //    Sql.Append(" MAX(A.SHISHO_NM) AS SHISHO_NM,");
        //    Sql.Append(" B.SEIJUN_KUBUN,");
        //    Sql.Append(" A.SENSHU_CD AS KUMIAIIN_CD,");
        //    Sql.Append(" MAX(MONTH(A.SERIBI)) AS SERITSUKI,");
        //    Sql.Append(" A.SERIBI,");
        //    Sql.Append(" A.SEISAN_KUBUN,");
        //    Sql.Append(" MAX(A.SENSHU_NM) AS KUMIAIIN_NM,");
        //    Sql.Append(" A.CHIKU_CD,");
        //    Sql.Append(" MAX(B.CHIKU_NM) AS CHIKU_NM,");
        //    Sql.Append(" A.GYOHO_CD AS GYOTAI_CD,");
        //    Sql.Append(" MAX(A.GYOHO_NM) AS GYOTAI_NM,");
        //    Sql.Append(" A.GYOSHU_CD AS GYOSHU_CD,");
        //    Sql.Append(" MAX(A.GYOSHU_NM) AS GYOSHU_NM,");
        //    Sql.Append(" A.GYOSHU_BUNRUI_CD AS GYOSHU_BUNRUI_CD,");
        //    Sql.Append(" MAX(A.GYOSHU_BUNRUI_NM) AS GYOSHU_BUNRUI_NM,");
        //    Sql.Append(" SUM(A.SURYO) AS MIZUAGE_SURYO,");
        //    Sql.Append(" SUM(A.KINGAKU) AS MIZUAGE_KINGAKU,");
        //    Sql.Append(" CASE WHEN A.SEISAN_KUBUN = 2 THEN SUM(A.KINGAKU) ELSE SUM(A.KINGAKU) + SUM(ISNULL(A.SHOHIZEI_FURIWAKE, A.SHOHIZEI)) END AS MIZUAGE_ZEIKOMI_KINGAKU,");
        //    Sql.Append(" MAX(A.DENPYO_BANGO) AS MIZUAGE_KAISU  ");
        //    Sql.Append(" FROM VI_HN_SHIKIRI_MEISAI2 A");
        //    Sql.Append(" LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO B");
        //    Sql.Append(" ON	A.KAISHA_CD = B.KAISHA_CD  ");
        //    Sql.Append(" AND A.SENSHU_CD = B.TORIHIKISAKI_CD  ");
        //    Sql.Append(" WHERE ");
        //    Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
        //    if (shishoCd != "0")
        //        Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
        //    Sql.Append(" B.TORIHIKISAKI_KUBUN1 = 1 AND ");
        //    Sql.Append(" A.SERIBI BETWEEN @SERIBI_FR AND @SERIBI_TO AND ");
        //    Sql.Append(" A.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO  ");
        //    // 精算区分設定
        //    if (seisanKbn != "0")
        //    {
        //        Sql.Append("AND A.SEISAN_KUBUN = @SEISAN_KUBUN ");
        //    }
        //    Sql.Append(" GROUP BY ");
        //    Sql.Append(" A.KAISHA_CD,");
        //    if (shishoCd != "0")
        //        Sql.Append(" A.SHISHO_CD,");
        //    Sql.Append(" B.SEIJUN_KUBUN,");
        //    Sql.Append(" A.SENSHU_CD,");
        //    Sql.Append(" A.GYOHO_CD,");
        //    Sql.Append(" A.GYOSHU_CD,");
        //    Sql.Append(" A.SERIBI,");
        //    Sql.Append(" A.CHIKU_CD,");
        //    Sql.Append(" A.GYOSHU_BUNRUI_CD,");
        //    Sql.Append(" A.GYO_NO,");
        //    Sql.Append(" A.SEISAN_KUBUN ");
        //    Sql.Append(" ORDER BY ");
        //    Sql.Append(" A.KAISHA_CD,");
        //    if (shishoCd != "0")
        //        Sql.Append(" A.SHISHO_CD,");
        //    Sql.Append(" A.CHIKU_CD,");
        //    Sql.Append(" B.SEIJUN_KUBUN,");
        //    Sql.Append(" KUMIAIIN_CD,");
        //    Sql.Append(" A.GYOSHU_CD,");
        //    Sql.Append(" GYOTAI_CD");

        //    DataTable dtMainLoop = Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
        //    #endregion

        //    #region 水揚回数の計算のため重複値を省く
        //    //DataViewの作成
        //    DataView dv = dtMainLoop.DefaultView;
        //    //Distinctをかける
        //    DataTable resultDt = dv.ToTable("DistinctTable", true, "MIZUAGE_KAISU", "KUMIAIIN_CD");
        //    #endregion

        //    #region 準備
        //    NumVals GYOSHU = new NumVals(); // 魚種グループ
        //    int gyoshuCd = 0;
        //    int senshuCd = 0;
        //    int cnt = 0;
        //    decimal MaxZeikomiGaku = 0;
        //    string seijun_nm;
        //    #endregion 

        //    if (dtMainLoop.Rows.Count == 0 )
        //    {
        //        Msg.Info("該当データがありません。");
        //        return false;
        //    }
        //    else
        //    {
        //        dpc = new DbParamCollection();
        //        i = 1;

        //        foreach (DataRow dr in dtMainLoop.Rows)
        //        {
        //            #region インサートするための情報の保持
        //            GYOSHU.MIZUAGESURYO += Util.ToDecimal(dr["MIZUAGE_SURYO"]);
        //            GYOSHU.MIZUAGEKINGAKU += Util.ToDecimal(dr["MIZUAGE_ZEIKOMI_KINGAKU"]);
        //            //GYOSHU.MIZUAGEKINGAKU += Util.ToDecimal(dr["MIZUAGE_KINGAKU"]);
        //            GYOSHU.KENSU += 1;
        //            //decimal ShohizeiRitsu = Util.ToDecimal(dtGetShohizei.Rows[0]["SHIN_SHOHIZEI_RITSU"]) / 100 + 1;
        //            if (Util.ToInt(dr["SEIJUN_KUBUN"]) == 1)
        //            {
        //                seijun_nm = "組合員";
        //            }
        //            else if (Util.ToInt(dr["SEIJUN_KUBUN"]) == 2)
        //            {
        //                seijun_nm = "准組合員";
        //            }
        //            else
        //            {
        //                seijun_nm = "組合員以外";
        //            }

        //            // 現在の魚種コードを保持
        //            gyoshuCd = Util.ToInt(dr["GYOSHU_CD"]);
        //            // 現在の船主コードを保持
        //            senshuCd = Util.ToInt(dr["KUMIAIIN_CD"]);
        //            cnt++;
        //            #endregion

        //            #region ループの最後 又は 次回の魚種コードが今回の魚種コードと一致しない場合
        //            if (cnt == dtMainLoop.Rows.Count || !gyoshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["GYOSHU_CD"])))
        //            {
        //                Sql = new StringBuilder();
        //                dpc = new DbParamCollection();
        //                Sql.Append("INSERT INTO PR_HN_TBL(");
        //                Sql.Append("  GUID");
        //                Sql.Append(" ,SORT");
        //                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
        //                Sql.Append(") ");
        //                Sql.Append("VALUES(");
        //                Sql.Append("  @GUID");
        //                Sql.Append(" ,@SORT");
        //                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
        //                Sql.Append(") ");

        //                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
        //                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
        //                // ページヘッダーデータを設定
        //                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojidate);// 日付
        //                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyoujititle);// タイトル
        //                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, headertitle);// ヘッダータイトル
        //                // データを設定
        //                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SEIJUN_KUBUN"]);// 正準区分
        //                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, seisanKubunhyoji);//精算区分
        //                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["CHIKU_NM"]);// 地区名
        //                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["KUMIAIIN_CD"]);// 組合員コード
        //                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["KUMIAIIN_NM"]);// 組合員名
        //                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["GYOTAI_CD"]);// 魚態コード
        //                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["GYOTAI_NM"]);// 魚態名称
        //                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["GYOSHU_CD"]);// 魚種コード
        //                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["GYOSHU_NM"]);// 魚種名
        //                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, GYOSHU.MIZUAGESURYO);// 水揚数量
        //                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, GYOSHU.MIZUAGEKINGAKU);// 水揚税込金額
        //                MaxZeikomiGaku += GYOSHU.MIZUAGEKINGAKU;
        //                // 水揚回数を取得
        //                string value = Util.ToString(resultDt.Compute("Count(MIZUAGE_KAISU)", "KUMIAIIN_CD =" + dr["KUMIAIIN_CD"]));
        //                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, value);// 水揚回数
        //                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, GYOSHU.KENSU);// カウント
        //                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, seijun_nm);// 正準区分名称
        //                dpc.SetParam("@ITEM18", SqlDbType.Decimal, 35, MaxZeikomiGaku);// 水揚税込合計金額

        //                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
        //                /**/
        //                if (cnt == dtMainLoop.Rows.Count || !senshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["KUMIAIIN_CD"])))
        //                {
        //                    MaxZeikomiGaku = 0;
        //                }
        //                GYOSHU.Clear();
        //                i++;
        //            }
        //            #endregion
        //            #region ループの最後 又は 魚種コードが一致して かつ 船主コードが一致しない場合
        //            else if (cnt == dtMainLoop.Rows.Count || (gyoshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["GYOSHU_CD"])) && !senshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["KUMIAIIN_CD"]))))
        //            {
        //                Sql = new StringBuilder();
        //                dpc = new DbParamCollection();
        //                Sql.Append("INSERT INTO PR_HN_TBL(");
        //                Sql.Append("  GUID");
        //                Sql.Append(" ,SORT");
        //                //Sql.Append(" ,ITEM01");
        //                //Sql.Append(" ,ITEM02");
        //                //Sql.Append(" ,ITEM03");
        //                //Sql.Append(" ,ITEM04");
        //                //Sql.Append(" ,ITEM05");
        //                //Sql.Append(" ,ITEM06");
        //                //Sql.Append(" ,ITEM07");
        //                //Sql.Append(" ,ITEM08");
        //                //Sql.Append(" ,ITEM09");
        //                //Sql.Append(" ,ITEM10");
        //                //Sql.Append(" ,ITEM11");
        //                //Sql.Append(" ,ITEM12");
        //                //Sql.Append(" ,ITEM13");
        //                //Sql.Append(" ,ITEM14");
        //                //Sql.Append(" ,ITEM15");
        //                //Sql.Append(" ,ITEM16");
        //                //Sql.Append(" ,ITEM17");
        //                //Sql.Append(" ,ITEM18");
        //                //Sql.Append(") ");
        //                //Sql.Append("VALUES(");
        //                //Sql.Append("  @GUID");
        //                //Sql.Append(" ,@SORT");
        //                //Sql.Append(" ,@ITEM01");
        //                //Sql.Append(" ,@ITEM02");
        //                //Sql.Append(" ,@ITEM03");
        //                //Sql.Append(" ,@ITEM04");
        //                //Sql.Append(" ,@ITEM05");
        //                //Sql.Append(" ,@ITEM06");
        //                //Sql.Append(" ,@ITEM07");
        //                //Sql.Append(" ,@ITEM08");
        //                //Sql.Append(" ,@ITEM09");
        //                //Sql.Append(" ,@ITEM10");
        //                //Sql.Append(" ,@ITEM11");
        //                //Sql.Append(" ,@ITEM12");
        //                //Sql.Append(" ,@ITEM13");
        //                //Sql.Append(" ,@ITEM14");
        //                //Sql.Append(" ,@ITEM15");
        //                //Sql.Append(" ,@ITEM16");
        //                //Sql.Append(" ,@ITEM17");
        //                //Sql.Append(" ,@ITEM18");
        //                //Sql.Append(") ");
        //                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
        //                Sql.Append(") ");
        //                Sql.Append("VALUES(");
        //                Sql.Append("  @GUID");
        //                Sql.Append(" ,@SORT");
        //                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
        //                Sql.Append(") ");

        //                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
        //                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
        //                // ページヘッダーデータを設定
        //                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojidate);// 日付
        //                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyoujititle);// タイトル
        //                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, headertitle);// ヘッダータイトル
        //                // データを設定
        //                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SEIJUN_KUBUN"]);// 正準区分
        //                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, seisanKubunhyoji);//精算区分
        //                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["CHIKU_NM"]);// 地区名
        //                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["KUMIAIIN_CD"]);// 組合員コード
        //                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["KUMIAIIN_NM"]);// 組合員名
        //                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["GYOTAI_CD"]);// 魚態コード
        //                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["GYOTAI_NM"]);// 魚態名称
        //                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["GYOSHU_CD"]);// 魚種コード
        //                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["GYOSHU_NM"]);// 魚種名
        //                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, GYOSHU.MIZUAGESURYO);// 水揚数量
        //                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, GYOSHU.MIZUAGEKINGAKU);// 水揚税込金額
        //                //if (rdoHamauri.Checked)// 浜売り
        //                //{
        //                //    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, GYOSHU.MIZUAGEKINGAKU);// 水揚金額
        //                //    MaxZeikomiGaku += Util.ToDecimal(GYOSHU.MIZUAGEKINGAKU);
        //                //}
        //                //else
        //                //{
        //                //    ZeikomiGaku = GYOSHU.MIZUAGEKINGAKU * ShohizeiRitsu;
        //                //    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, ZeikomiGaku);// 水揚金額
        //                //    MaxZeikomiGaku += ZeikomiGaku;
        //                //}
        //                MaxZeikomiGaku += GYOSHU.MIZUAGEKINGAKU;
        //                // 水揚回数を取得
        //                string value = Util.ToString(resultDt.Compute("Count(MIZUAGE_KAISU)", "KUMIAIIN_CD =" + dr["KUMIAIIN_CD"]));
        //                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, value);// 水揚回数
        //                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, GYOSHU.KENSU);// カウント
        //                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, seijun_nm);// 正準区分名称
        //                dpc.SetParam("@ITEM18", SqlDbType.Decimal, 35, MaxZeikomiGaku);// 水揚税込合計金額

        //                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
        //                if (cnt == dtMainLoop.Rows.Count || !senshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["KUMIAIIN_CD"])))
        //                {
        //                    MaxZeikomiGaku = 0;
        //                }
        //                GYOSHU.Clear();
        //                i++;
        //            }
        //            #endregion
        //        }

        //    }
        //        #region 印刷ワークテーブルに登録
        //        // 印刷ワークテーブルのデータ件数を取得
        //        dpc = new DbParamCollection();
        //        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
        //        DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
        //            "SORT",
        //            "PR_HN_TBL",
        //            "GUID = @GUID",
        //            dpc);

        //        bool dataFlag;
        //        if (tmpdtPR_HN_TBL.Rows.Count > 0)
        //        {
        //            dataFlag = true;
        //        }
        //        else
        //        {
        //            dataFlag = false;
        //        }

        //        return dataFlag;
        //        #endregion
        //}
        #endregion

        #region 魚種分類別データ作成
        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。漁態魚種分類別水揚月報
        /// </summary>
        //private bool MakeWkData2()
        //{
        //    #region メインデータを取得
        //    DbParamCollection dpc = new DbParamCollection();
        //    // 日付範囲を西暦にして取得
        //    DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
        //            this.txtDateMonthFr.Text, "1", this.Dba);

        //    int lastDayInMonth = DateTime.DaysInMonth(Util.ToInt(this.txtDateYearTo.Text), Util.ToInt(this.txtDateMonthTo.Text));

        //    DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
        //            this.txtDateMonthTo.Text, Util.ToString(lastDayInMonth), this.Dba);

        //    // 日付範囲を和暦で保持
        //    string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
        //    string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

        //    // 表示日付
        //    string hyojidate = tmpjpDateFr[0] + tmpjpDateFr[2] + "年" + tmpjpDateFr[3] +"月 ～ "+
        //                       tmpjpDateTo[0] + tmpjpDateTo[2] + "年" + tmpjpDateTo[3] + "月分";

        //    string hyoujititle;
        //    string headertitle;
        //    hyoujititle = "漁 態 別 魚 種 分 類 別 水 揚 月 報";
        //    headertitle = "魚種分類コード / 魚 種 分 類 名";
            
        //    // 船主コード設定
        //    string funanushiCdFr;
        //    string funanushiCdTo;
        //    if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
        //    {
        //        funanushiCdFr = txtFunanushiCdFr.Text;
        //    }
        //    else
        //    {
        //        funanushiCdFr = "0";
        //    }
        //    if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
        //    {
        //        funanushiCdTo = txtFunanushiCdTo.Text;
        //    }
        //    else
        //    {
        //        funanushiCdTo = "99999";
        //    }
        //    #region 支所コードの退避
        //    //支所コードの退避
        //    string shishoCd;
        //    if (this.txtMizuageShishoCd.Text == "0" || this.txtMizuageShishoCd.Text == "")
        //    {
        //        shishoCd = ";";
        //    }
        //    else
        //    {
        //        shishoCd = this.txtMizuageShishoCd.Text;
        //    }
        //    #endregion

        //    int i; // ループ用カウント変数
        //    DateTime dt = DateTime.MinValue; // 精算日

        //    // 精算区分設定
        //    string seisanKubunhyoji;
        //    if (rdoChikunai.Checked)
        //    {
        //        seisanKubunhyoji = "地区内";

        //    }
        //    else if (rdoHamauri.Checked)
        //    {
        //        seisanKubunhyoji ="浜売り";
        //    }
        //    else if (rdoChikugai.Checked)
        //    {
        //        seisanKubunhyoji ="地区外";
        //    }
        //    else
        //    {
        //        seisanKubunhyoji ="全て";
        //    }

        //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
        //    // 検索する日付をセット
        //    dpc.SetParam("@SERIBI_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
        //    dpc.SetParam("@SERIBI_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
        //    dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, funanushiCdFr);
        //    dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, funanushiCdTo);

        //    StringBuilder Sql = new StringBuilder();
        //    //Sql.Append(" SELECT");
        //    //Sql.Append("    D.SEIJUN_KUBUN,");
        //    //Sql.Append("    A.SENSHU_CD AS KUMIAIIN_CD,");
        //    //Sql.Append("    MAX({ fn MONTH(A.SERIBI) }) AS SERITSUKI,");
        //    //Sql.Append("    A.SERIBI,");
        //    //Sql.Append("    A.SEISAN_KUBUN,");
        //    //Sql.Append("    MAX(A.SENSHU_NM) AS KUMIAIIN_NM,");
        //    //Sql.Append("    D.CHIKU_CD,");
        //    //Sql.Append("    MAX(F.CHIKU_NM)                          AS CHIKU_NM,");
        //    //Sql.Append("    A.GYOHO_CD AS GYOTAI_CD,");
        //    //Sql.Append("    MAX(A.GYOHO_NM) AS GYOTAI_NM,");
        //    //Sql.Append("    A.GYOSHU_CD AS GYOSHU_CD,");
        //    //Sql.Append("    MAX(A.GYOSHU_NM) AS GYOSHU_NM,");
        //    //Sql.Append("    C.GYOSHU_BUNRUI_CD AS GYOSHU_BUNRUI_CD,");
        //    //Sql.Append("    MAX(C.GYOSHU_BUNRUI_NM) AS GYOSHU_BUNRUI_NM,");
        //    //Sql.Append("    SUM(A.SURYO) AS MIZUAGE_SURYO,");
        //    //Sql.Append("    SUM(A.KINGAKU) AS MIZUAGE_KINGAKU,");
        //    //Sql.Append("    CASE WHEN A.SEISAN_KUBUN = 2 THEN SUM(A.KINGAKU) ELSE SUM(A.KINGAKU)                          + SUM(A.MEISAI_SHOHIZEI) END AS MIZUAGE_ZEIKOMI_KINGAKU,");
        //    //Sql.Append("    MAX(A.DENPYO_BANGO) AS MIZUAGE_KAISU,");
        //    //Sql.Append("    MAX(A.ZEI_RITSU) AS ZEI_RITSU");
        //    //Sql.Append(" FROM");
        //    //Sql.Append("    (SELECT");
        //    //Sql.Append("        D2.KAISHA_CD,");
        //    //Sql.Append("        D2.DENPYO_BANGO,");
        //    //Sql.Append("        D2.SERIBI,");
        //    //Sql.Append("        D2.SENSHU_CD,");
        //    //Sql.Append("        MIN(D2.SENSHU_NM) AS SENSHU_NM,");
        //    //Sql.Append("        D2.GYOHO_CD,");
        //    //Sql.Append("        MIN(D2.GYOHO_NM) AS GYOHO_NM,");
        //    //Sql.Append("        D2.SEISAN_KUBUN,");
        //    //Sql.Append("        M2.GYO_NO,");
        //    //Sql.Append("        M2.GYOSHU_CD,");
        //    //Sql.Append("        MIN(M2.GYOSHU_NM) AS GYOSHU_NM,");
        //    //Sql.Append("        MIN(M2.SURYO) AS SURYO,");
        //    //Sql.Append("        MIN(M2.KINGAKU) AS KINGAKU,");
        //    //// Sql.Append("        CASE WHEN (CASE WHEN M2.GYO_NO = 1 THEN MIN(D2.MIZUAGE_SHOHIZEIGAKU) ELSE 0 END)                          = SUM(sub.GOKEI_MEISAI_SHOHIZEI) THEN ROUND(MIN(M2.KINGAKU) * (MIN(M2.ZEI_RITSU) / 100), 0) ELSE CASE WHEN (M2.GYO_NO) = 1 THEN ROUND(MIN(M2.KINGAKU) * (MIN(M2.ZEI_RITSU) / 100), 0)                          - (SUM(sub.GOKEI_MEISAI_SHOHIZEI) - (CASE WHEN M2.GYO_NO = 1 THEN MIN(D2.MIZUAGE_SHOHIZEIGAKU) ELSE 0 END)) ELSE ROUND(MIN(M2.KINGAKU) * (MIN(M2.ZEI_RITSU) / 100), 0)                          END END AS MEISAI_SHOHIZEI,");
        //    //Sql.Append("        ROUND(MIN(M2.KINGAKU) * (MIN(M2.ZEI_RITSU) / 100), 0)  AS MEISAI_SHOHIZEI,");
        //    //Sql.Append("        MIN(M2.ZEI_RITSU) AS ZEI_RITSU");
        //    //Sql.Append("    FROM");
        //    //Sql.Append("        TB_HN_SHIKIRI_DATA AS D2 LEFT OUTER JOIN                         TB_HN_SHIKIRI_MEISAI AS M2 ON D2.KAISHA_CD = M2.KAISHA_CD AND D2.KAIKEI_NENDO = M2.KAIKEI_NENDO AND D2.DENPYO_KUBUN = M2.DENPYO_KUBUN AND D2.DENPYO_BANGO = M2.DENPYO_BANGO LEFT OUTER JOIN                             (SELECT");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.KAISHA_CD,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.DENPYO_BANGO,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.DENPYO_KUBUN,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.SEISAN_KUBUN,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.SENSHU_CD,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.GYOHO_CD,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                B2.GYOSHU_CD,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                B2.GYO_NO,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                ROUND(SUM(B2.KINGAKU) * (MIN(B2.ZEI_RITSU) / 100), 0) AS GOKEI_MEISAI_SHOHIZEI");
        //    //Sql.Append("                                                                                                                                                                                                                                                            FROM");
        //    //Sql.Append("                                                                                                                                                                                                                                                                TB_HN_SHIKIRI_DATA AS A2 LEFT OUTER JOIN                                                        TB_HN_SHIKIRI_MEISAI AS B2 ON A2.KAISHA_CD = B2.KAISHA_CD AND A2.DENPYO_KUBUN = B2.DENPYO_KUBUN AND A2.DENPYO_BANGO = B2.DENPYO_BANGO");
        //    //Sql.Append("                                                                                                                                                                                                                                                            WHERE");
        //    //Sql.Append("                                                                                                                                                                                                                                                                (A2.KEIJO_NENGAPPI BETWEEN @SERIBI_FR AND @SERIBI_TO)");
        //    //Sql.Append("                                                                                                                                                                                                                                                            GROUP BY");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.KAISHA_CD,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.DENPYO_BANGO,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.DENPYO_KUBUN,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.SERIBI,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.SENSHU_CD,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.GYOHO_CD,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                A2.SEISAN_KUBUN,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                B2.GYO_NO,");
        //    //Sql.Append("                                                                                                                                                                                                                                                                B2.GYOSHU_CD");
        //    //Sql.Append("                                                                                                                                                                                                                                                            ) AS sub ON M2.KAISHA_CD = sub.KAISHA_CD AND                          M2.DENPYO_KUBUN = sub.DENPYO_KUBUN AND M2.DENPYO_BANGO = sub.DENPYO_BANGO AND D2.SEISAN_KUBUN = sub.SEISAN_KUBUN AND D2.SENSHU_CD = sub.SENSHU_CD AND D2.GYOHO_CD = sub.GYOHO_CD");
        //    //Sql.Append("    WHERE");
        //    //Sql.Append("        (D2.KEIJO_NENGAPPI BETWEEN @SERIBI_FR AND @SERIBI_TO) AND");
        //    //Sql.Append("        (D2.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
        //    Sql.Append(" SELECT");
        //    Sql.Append("    D.SEIJUN_KUBUN,");
        //    Sql.Append("    A.SENSHU_CD AS KUMIAIIN_CD,");
        //    Sql.Append("    MAX({ fn MONTH(A.SERIBI) }) AS SERITSUKI,");
        //    Sql.Append("    A.SERIBI,");
        //    Sql.Append("    A.SEISAN_KUBUN,");
        //    Sql.Append("    MAX(A.SENSHU_NM) AS KUMIAIIN_NM,");
        //    Sql.Append("    D.CHIKU_CD,");
        //    Sql.Append("    MAX(F.CHIKU_NM) AS CHIKU_NM,");
        //    Sql.Append("    A.GYOHO_CD AS GYOTAI_CD,");
        //    Sql.Append("    MAX(A.GYOHO_NM) AS GYOTAI_NM,");
        //    Sql.Append("    A.GYOSHU_CD AS GYOSHU_CD,");
        //    Sql.Append("    MAX(A.GYOSHU_NM) AS GYOSHU_NM,");
        //    Sql.Append("    C.GYOSHU_BUNRUI_CD AS GYOSHU_BUNRUI_CD,");
        //    Sql.Append("    MAX(C.GYOSHU_BUNRUI_NM) AS GYOSHU_BUNRUI_NM,");
        //    Sql.Append("    SUM(A.SURYO) AS MIZUAGE_SURYO,");
        //    Sql.Append("    SUM(A.KINGAKU) AS MIZUAGE_KINGAKU,");
        //    Sql.Append("    CASE WHEN A.SEISAN_KUBUN = 2 THEN SUM(A.KINGAKU) ELSE SUM(A.KINGAKU) ");
        //    Sql.Append("    	+ SUM(A.MEISAI_SHOHIZEI) END AS MIZUAGE_ZEIKOMI_KINGAKU,");
        //    Sql.Append("    MAX(A.DENPYO_BANGO) AS MIZUAGE_KAISU,");
        //    Sql.Append("    MAX(A.ZEI_RITSU) AS ZEI_RITSU");
        //    Sql.Append(" FROM");
        //    Sql.Append("    (SELECT");
        //    Sql.Append("        D2.KAISHA_CD,");
        //    Sql.Append("        D2.DENPYO_BANGO,");
        //    Sql.Append("        D2.SERIBI,");
        //    Sql.Append("        D2.SENSHU_CD,");
        //    Sql.Append("        MIN(D2.SENSHU_NM) AS SENSHU_NM,");
        //    Sql.Append("        D2.GYOHO_CD,");
        //    Sql.Append("        MIN(D2.GYOHO_NM) AS GYOHO_NM,");
        //    Sql.Append("        D2.SEISAN_KUBUN,");
        //    Sql.Append("        M2.GYO_NO,");
        //    Sql.Append("        M2.GYOSHU_CD,");
        //    Sql.Append("        MIN(M2.GYOSHU_NM) AS GYOSHU_NM,");
        //    Sql.Append("        MIN(M2.SURYO) AS SURYO,");
        //    Sql.Append("        MIN(M2.KINGAKU) AS KINGAKU,");
        //    Sql.Append("        ROUND(MIN(M2.KINGAKU) * (MIN(M2.ZEI_RITSU) / 100), 0)  AS MEISAI_SHOHIZEI,");
        //    Sql.Append("        MIN(M2.ZEI_RITSU) AS ZEI_RITSU");
        //    Sql.Append("    FROM");
        //    Sql.Append("        TB_HN_SHIKIRI_DATA AS D2 ");
        //    Sql.Append("    LEFT OUTER JOIN TB_HN_SHIKIRI_MEISAI AS M2 ");
        //    Sql.Append("    ON  D2.KAISHA_CD = M2.KAISHA_CD ");
        //    Sql.Append("    AND D2.KAIKEI_NENDO = M2.KAIKEI_NENDO ");
        //    Sql.Append("    AND D2.DENPYO_KUBUN = M2.DENPYO_KUBUN ");
        //    Sql.Append("    AND D2.DENPYO_BANGO = M2.DENPYO_BANGO ");
        //    Sql.Append("    LEFT OUTER JOIN (SELECT");
        //    Sql.Append("        A2.KAISHA_CD,");
        //    Sql.Append("        A2.DENPYO_BANGO,");
        //    Sql.Append("        A2.DENPYO_KUBUN,");
        //    Sql.Append("        A2.SEISAN_KUBUN,");
        //    Sql.Append("        A2.SENSHU_CD,");
        //    Sql.Append("        A2.GYOHO_CD,");
        //    Sql.Append("        B2.GYOSHU_CD,");
        //    Sql.Append("        B2.GYO_NO,");
        //    Sql.Append("        ROUND(SUM(B2.KINGAKU) * (MIN(B2.ZEI_RITSU) / 100), 0) AS GOKEI_MEISAI_SHOHIZEI");
        //    Sql.Append("    FROM");
        //    Sql.Append("        TB_HN_SHIKIRI_DATA AS A2 ");
        //    Sql.Append("    	LEFT OUTER JOIN TB_HN_SHIKIRI_MEISAI AS B2 ");
        //    Sql.Append("    	ON A2.KAISHA_CD = B2.KAISHA_CD ");
        //    Sql.Append("    	AND A2.DENPYO_KUBUN = B2.DENPYO_KUBUN ");
        //    Sql.Append("    	AND A2.DENPYO_BANGO = B2.DENPYO_BANGO");
        //    Sql.Append("    WHERE");
        //    Sql.Append("        (A2.KEIJO_NENGAPPI BETWEEN @SERIBI_FR AND @SERIBI_TO)");
        //    Sql.Append("    GROUP BY");
        //    Sql.Append("        A2.KAISHA_CD,");
        //    Sql.Append("        A2.DENPYO_BANGO,");
        //    Sql.Append("        A2.DENPYO_KUBUN,");
        //    Sql.Append("        A2.SERIBI,");
        //    Sql.Append("        A2.SENSHU_CD,");
        //    Sql.Append("        A2.GYOHO_CD,");
        //    Sql.Append("        A2.SEISAN_KUBUN,");
        //    Sql.Append("        B2.GYO_NO,");
        //    Sql.Append("        B2.GYOSHU_CD");
        //    Sql.Append("    ) AS sub ");
        //    Sql.Append("    ON  M2.KAISHA_CD = sub.KAISHA_CD ");
        //    Sql.Append("    AND M2.DENPYO_KUBUN = sub.DENPYO_KUBUN ");
        //    Sql.Append("    AND M2.DENPYO_BANGO = sub.DENPYO_BANGO ");
        //    Sql.Append("    AND D2.SEISAN_KUBUN = sub.SEISAN_KUBUN ");
        //    Sql.Append("    AND D2.SENSHU_CD = sub.SENSHU_CD ");
        //    Sql.Append("    AND D2.GYOHO_CD  = sub.GYOHO_CD");
        //    Sql.Append("    WHERE");
        //    Sql.Append("        (D2.KEIJO_NENGAPPI BETWEEN @SERIBI_FR AND @SERIBI_TO) AND");
        //    Sql.Append("        (D2.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
        //    // 精算区分設定
        //    if (rdoChikunai.Checked)// 地区内
        //    {
        //        Sql.Append("AND D2.SEISAN_KUBUN = 3 )");
        //    }
        //    else if (rdoHamauri.Checked)// 浜売り
        //    {
        //        Sql.Append("AND D2.SEISAN_KUBUN = 2 )");
        //    }
        //    else if (rdoChikugai.Checked)// 地区外
        //    {
        //        Sql.Append("AND D2.SEISAN_KUBUN = 1 )");
        //    }
        //    else// 全て
        //    {
        //        Sql.Append(")");
        //    }
        //    Sql.Append("    GROUP BY");
        //    Sql.Append("        D2.KAISHA_CD,");
        //    Sql.Append("        D2.DENPYO_BANGO,");
        //    Sql.Append("        D2.SERIBI,");
        //    Sql.Append("        D2.SENSHU_CD,");
        //    Sql.Append("        D2.GYOHO_CD,");
        //    Sql.Append("        D2.SEISAN_KUBUN,");
        //    Sql.Append("        M2.GYO_NO,");
        //    Sql.Append("        M2.GYOSHU_CD");
        //    Sql.Append("    ) AS A LEFT OUTER JOIN TB_HN_SHOHIN AS B ");
        //    Sql.Append("    ON A.KAISHA_CD = B.KAISHA_CD ");
        //    Sql.Append("    AND A.GYOSHU_CD = B.SHOHIN_CD ");
        //    Sql.Append("    LEFT OUTER JOIN TM_HN_GYOSHU_BUNRUI_MST AS C ");
        //    Sql.Append("    ON B.KAISHA_CD = C.KAISHA_CD ");
        //    Sql.Append("    AND B.SHOHIN_KUBUN1 = C.GYOSHU_BUNRUI_CD ");
        //    Sql.Append("    LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS D ");
        //    Sql.Append("    ON A.KAISHA_CD = D.KAISHA_CD ");
        //    Sql.Append("    AND A.SENSHU_CD = D.TORIHIKISAKI_CD ");
        //    Sql.Append("    LEFT OUTER JOIN TB_HN_TORIHIKISAKI_JOHO AS E ");
        //    Sql.Append("    ON D.KAISHA_CD = E.KAISHA_CD ");
        //    Sql.Append("    AND D.TORIHIKISAKI_CD = E.TORIHIKISAKI_CD ");
        //    Sql.Append("    LEFT OUTER JOIN TM_HN_CHIKU_MST AS F ");
        //    Sql.Append("    ON D.KAISHA_CD = F.KAISHA_CD ");
        //    Sql.Append("    AND D.CHIKU_CD = F.CHIKU_CD");
        //    Sql.Append(" WHERE");
        //    Sql.Append("    (A.KAISHA_CD = 1) AND");
        //    Sql.Append("    (B.BARCODE1 = '999') AND");
        //    Sql.Append("    (E.TORIHIKISAKI_KUBUN1 = 1) AND");
        //    Sql.Append("    (A.SERIBI BETWEEN @SERIBI_FR AND @SERIBI_TO) AND");
        //    Sql.Append("    (A.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
        //    // 精算区分設定
        //    if (rdoChikunai.Checked)// 地区内
        //    {
        //        Sql.Append("AND A.SEISAN_KUBUN = 3 )");
        //    }
        //    else if (rdoHamauri.Checked)// 浜売り
        //    {
        //        Sql.Append("AND A.SEISAN_KUBUN = 2 )");
        //    }
        //    else if (rdoChikugai.Checked)// 地区外
        //    {
        //        Sql.Append("AND A.SEISAN_KUBUN = 1 )");
        //    }
        //    else// 全て
        //    {
        //        Sql.Append(")");
        //    }
        //    Sql.Append(" GROUP BY");
        //    Sql.Append("    D.SEIJUN_KUBUN,");
        //    Sql.Append("    A.SENSHU_CD,");
        //    Sql.Append("    A.GYOHO_CD,");
        //    Sql.Append("    A.GYOSHU_CD,");
        //    Sql.Append("    A.SERIBI,");
        //    Sql.Append("    D.CHIKU_CD,");
        //    Sql.Append("    C.GYOSHU_BUNRUI_CD,");
        //    Sql.Append("    A.GYO_NO,");
        //    Sql.Append("    A.SEISAN_KUBUN");
        //    Sql.Append(" ORDER BY");
        //    Sql.Append("    CHIKU_CD,");
        //    Sql.Append("    SEIJUN_KUBUN,");
        //    Sql.Append("    KUMIAIIN_CD,");
        //    Sql.Append("    GYOSHU_BUNRUI_CD,");
        //    Sql.Append("    GYOTAI_CD");

        //    DataTable dtMainLoop = Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
        //    #endregion

        //    #region 水揚回数の計算のため重複値を省く
        //    //DataViewの作成
        //    DataView dv = dtMainLoop.DefaultView;
        //    //Distinctをかける
        //    DataTable resultDt = dv.ToTable("DistinctTable", true, "MIZUAGE_KAISU", "KUMIAIIN_CD");
        //    #endregion

        //    #region 準備
        //    NumVals GYOSHUBUNRUI = new NumVals(); // 魚種分類グループ
        //    int gyoshuCd = 0;
        //    int senshuCd = 0;
        //    int cnt = 0;
        //    decimal ZeikomiGaku = 0;
        //    decimal MaxZeikomiGaku = 0;
        //    string seijun_nm;

        //    Sql = new StringBuilder();
        //    dpc = new DbParamCollection();
        //    Sql.Append(" SELECT");
        //    Sql.Append("    KAZEI_HOHO,");
        //    Sql.Append("    SHOHIZEI_NYURYOKU_HOHO,");
        //    Sql.Append("    SHOHIZEI_HASU_SHORI,");
        //    Sql.Append("    SHIN_SHOHIZEI_RITSU");
        //    Sql.Append(" FROM");
        //    Sql.Append("    TB_ZM_SHOHIZEI_JOHO ");
        //    Sql.Append(" WHERE");
        //    Sql.Append("    KAIKEI_NENDO = @KAIKEI_NENDO");
        //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
        //    DataTable dtGetShohizei = Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
        //    #endregion 

        //    if (dtMainLoop.Rows.Count == 0)
        //    {
        //        Msg.Info("該当データがありません。");
        //        return false;
        //    }
        //    else
        //    {
        //        dpc = new DbParamCollection();
        //        i = 1;

        //        foreach (DataRow dr in dtMainLoop.Rows)
        //        {
        //            #region インサートするための情報の保持
        //            GYOSHUBUNRUI.MIZUAGESURYO += Util.ToDecimal(dr["MIZUAGE_SURYO"]);
        //            // GYOSHUBUNRUI.MIZUAGEKINGAKU += Util.ToDecimal(dr["MIZUAGE_ZEIKOMI_KINGAKU"]);
        //            GYOSHUBUNRUI.MIZUAGEKINGAKU += Util.ToDecimal(dr["MIZUAGE_KINGAKU"]);
        //            GYOSHUBUNRUI.KENSU += 1;
        //            decimal ShohizeiRitsu = Util.ToDecimal(dtGetShohizei.Rows[0]["SHIN_SHOHIZEI_RITSU"]) / 100 + 1;

        //            if (Util.ToInt(dr["SEIJUN_KUBUN"]) == 1)
        //            {
        //                seijun_nm = "組合員";
        //            }
        //            else if (Util.ToInt(dr["SEIJUN_KUBUN"]) == 2)
        //            {
        //                seijun_nm = "准組合員";
        //            }
        //            else
        //            {
        //                seijun_nm = "組合員以外";
        //            }

        //            // 現在の魚種分類コードを保持
        //            gyoshuCd = Util.ToInt(dr["GYOSHU_BUNRUI_CD"]);
        //            senshuCd = Util.ToInt(dr["KUMIAIIN_CD"]);
        //            cnt++;
        //            #endregion

        //            #region ループの最後 又は 次回の魚種コードが今回の魚種コードと一致しない場合・次の船主CDと今回の船主CDが違う場合
        //            if (cnt == dtMainLoop.Rows.Count || !gyoshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["GYOSHU_BUNRUI_CD"])))
        //            {
        //                Sql = new StringBuilder();
        //                dpc = new DbParamCollection();
        //                Sql.Append("INSERT INTO PR_HN_TBL(");
        //                Sql.Append("  GUID");
        //                Sql.Append(" ,SORT");
        //                //Sql.Append(" ,ITEM01");
        //                //Sql.Append(" ,ITEM02");
        //                //Sql.Append(" ,ITEM03");
        //                //Sql.Append(" ,ITEM04");
        //                //Sql.Append(" ,ITEM05");
        //                //Sql.Append(" ,ITEM06");
        //                //Sql.Append(" ,ITEM07");
        //                //Sql.Append(" ,ITEM08");
        //                //Sql.Append(" ,ITEM09");
        //                //Sql.Append(" ,ITEM10");
        //                //Sql.Append(" ,ITEM11");
        //                //Sql.Append(" ,ITEM12");
        //                //Sql.Append(" ,ITEM13");
        //                //Sql.Append(" ,ITEM14");
        //                //Sql.Append(" ,ITEM15");
        //                //Sql.Append(" ,ITEM16");
        //                //Sql.Append(" ,ITEM17");
        //                //Sql.Append(" ,ITEM18");
        //                //Sql.Append(") ");
        //                //Sql.Append("VALUES(");
        //                //Sql.Append("  @GUID");
        //                //Sql.Append(" ,@SORT");
        //                //Sql.Append(" ,@ITEM01");
        //                //Sql.Append(" ,@ITEM02");
        //                //Sql.Append(" ,@ITEM03");
        //                //Sql.Append(" ,@ITEM04");
        //                //Sql.Append(" ,@ITEM05");
        //                //Sql.Append(" ,@ITEM06");
        //                //Sql.Append(" ,@ITEM07");
        //                //Sql.Append(" ,@ITEM08");
        //                //Sql.Append(" ,@ITEM09");
        //                //Sql.Append(" ,@ITEM10");
        //                //Sql.Append(" ,@ITEM11");
        //                //Sql.Append(" ,@ITEM12");
        //                //Sql.Append(" ,@ITEM13");
        //                //Sql.Append(" ,@ITEM14");
        //                //Sql.Append(" ,@ITEM15");
        //                //Sql.Append(" ,@ITEM16");
        //                //Sql.Append(" ,@ITEM17");
        //                //Sql.Append(" ,@ITEM18");
        //                //Sql.Append(") ");
        //                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
        //                Sql.Append(") ");
        //                Sql.Append("VALUES(");
        //                Sql.Append("  @GUID");
        //                Sql.Append(" ,@SORT");
        //                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
        //                Sql.Append(") ");

        //                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
        //                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
        //                // ページヘッダーデータを設定
        //                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojidate);// 日付
        //                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyoujititle);// タイトル
        //                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, headertitle);// ヘッダータイトル
        //                // データを設定
        //                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SEIJUN_KUBUN"]);// 正準区分
        //                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, seisanKubunhyoji);//精算区分
        //                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["CHIKU_NM"]);// 地区名
        //                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["KUMIAIIN_CD"]);// 組合員コード
        //                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["KUMIAIIN_NM"]);// 組合員名
        //                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["GYOTAI_CD"]);// 魚態コード
        //                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["GYOTAI_NM"]);// 魚態名称
        //                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["GYOSHU_BUNRUI_CD"]);// 魚種分類コード
        //                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["GYOSHU_BUNRUI_NM"]);// 魚種分類名
        //                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, GYOSHUBUNRUI.MIZUAGESURYO);// 水揚数量
        //                // dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, GYOSHU.MIZUAGEKINGAKU);// 水揚税込金額
        //                if (rdoHamauri.Checked)// 浜売り
        //                {
        //                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, GYOSHUBUNRUI.MIZUAGEKINGAKU);// 水揚金額
        //                    MaxZeikomiGaku += Util.ToDecimal(GYOSHUBUNRUI.MIZUAGEKINGAKU);
        //                }
        //                else
        //                {
        //                    ZeikomiGaku = GYOSHUBUNRUI.MIZUAGEKINGAKU * ShohizeiRitsu;
        //                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, ZeikomiGaku);// 水揚金額
        //                    MaxZeikomiGaku += ZeikomiGaku;
        //                }
        //                // 水揚回数を取得
        //                string value = Util.ToString(resultDt.Compute("Count(MIZUAGE_KAISU)", "KUMIAIIN_CD =" + dr["KUMIAIIN_CD"]));
        //                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, value);// 水揚回数
        //                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, GYOSHUBUNRUI.KENSU);// カウント
        //                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, seijun_nm);// 正準区分名称
        //                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, MaxZeikomiGaku);// 水揚税込合計金額

        //                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
        //                // ループの最後 又は 船主コードが一致しない場合
        //                if (cnt == dtMainLoop.Rows.Count || !senshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["KUMIAIIN_CD"])))
        //                {
        //                    MaxZeikomiGaku = 0;
        //                }
        //                GYOSHUBUNRUI.Clear();
        //                i++;
        //            }
        //            #endregion
        //            #region ループの最後 又は 魚種コードが一致して かつ 船主コードが一致しない場合
        //            else if (cnt == dtMainLoop.Rows.Count || (gyoshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["GYOSHU_BUNRUI_CD"])) && !senshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["KUMIAIIN_CD"]))))
        //            {
        //                Sql = new StringBuilder();
        //                dpc = new DbParamCollection();
        //                Sql.Append("INSERT INTO PR_HN_TBL(");
        //                Sql.Append("  GUID");
        //                Sql.Append(" ,SORT");
        //                //Sql.Append(" ,ITEM01");
        //                //Sql.Append(" ,ITEM02");
        //                //Sql.Append(" ,ITEM03");
        //                //Sql.Append(" ,ITEM04");
        //                //Sql.Append(" ,ITEM05");
        //                //Sql.Append(" ,ITEM06");
        //                //Sql.Append(" ,ITEM07");
        //                //Sql.Append(" ,ITEM08");
        //                //Sql.Append(" ,ITEM09");
        //                //Sql.Append(" ,ITEM10");
        //                //Sql.Append(" ,ITEM11");
        //                //Sql.Append(" ,ITEM12");
        //                //Sql.Append(" ,ITEM13");
        //                //Sql.Append(" ,ITEM14");
        //                //Sql.Append(" ,ITEM15");
        //                //Sql.Append(" ,ITEM16");
        //                //Sql.Append(" ,ITEM17");
        //                //Sql.Append(" ,ITEM18");
        //                //Sql.Append(") ");
        //                //Sql.Append("VALUES(");
        //                //Sql.Append("  @GUID");
        //                //Sql.Append(" ,@SORT");
        //                //Sql.Append(" ,@ITEM01");
        //                //Sql.Append(" ,@ITEM02");
        //                //Sql.Append(" ,@ITEM03");
        //                //Sql.Append(" ,@ITEM04");
        //                //Sql.Append(" ,@ITEM05");
        //                //Sql.Append(" ,@ITEM06");
        //                //Sql.Append(" ,@ITEM07");
        //                //Sql.Append(" ,@ITEM08");
        //                //Sql.Append(" ,@ITEM09");
        //                //Sql.Append(" ,@ITEM10");
        //                //Sql.Append(" ,@ITEM11");
        //                //Sql.Append(" ,@ITEM12");
        //                //Sql.Append(" ,@ITEM13");
        //                //Sql.Append(" ,@ITEM14");
        //                //Sql.Append(" ,@ITEM15");
        //                //Sql.Append(" ,@ITEM16");
        //                //Sql.Append(" ,@ITEM17");
        //                //Sql.Append(" ,@ITEM18");
        //                //Sql.Append(") ");
        //                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
        //                Sql.Append(") ");
        //                Sql.Append("VALUES(");
        //                Sql.Append("  @GUID");
        //                Sql.Append(" ,@SORT");
        //                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
        //                Sql.Append(") ");

        //                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
        //                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
        //                // ページヘッダーデータを設定
        //                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojidate);// 日付
        //                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyoujititle);// タイトル
        //                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, headertitle);// ヘッダータイトル
        //                // データを設定
        //                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SEIJUN_KUBUN"]);// 正準区分
        //                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, seisanKubunhyoji);//精算区分
        //                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["CHIKU_NM"]);// 地区名
        //                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["KUMIAIIN_CD"]);// 組合員コード
        //                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["KUMIAIIN_NM"]);// 組合員名
        //                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["GYOTAI_CD"]);// 魚態コード
        //                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["GYOTAI_NM"]);// 魚態名称
        //                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["GYOSHU_BUNRUI_CD"]);// 魚種分類コード
        //                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["GYOSHU_BUNRUI_NM"]);// 魚種分類名
        //                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, GYOSHUBUNRUI.MIZUAGESURYO);// 水揚数量
        //                // dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, GYOSHU.MIZUAGEKINGAKU);// 水揚税込金額
        //                if (rdoHamauri.Checked)// 浜売り
        //                {
        //                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, GYOSHUBUNRUI.MIZUAGEKINGAKU);// 水揚金額
        //                    MaxZeikomiGaku += Util.ToDecimal(GYOSHUBUNRUI.MIZUAGEKINGAKU);
        //                }
        //                else
        //                {
        //                    ZeikomiGaku = GYOSHUBUNRUI.MIZUAGEKINGAKU * ShohizeiRitsu;
        //                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, ZeikomiGaku);// 水揚金額
        //                    MaxZeikomiGaku += ZeikomiGaku;
        //                }
        //                // 水揚回数を取得
        //                string value = Util.ToString(resultDt.Compute("Count(MIZUAGE_KAISU)", "KUMIAIIN_CD =" + dr["KUMIAIIN_CD"]));
        //                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, value);// 水揚回数
        //                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, GYOSHUBUNRUI.KENSU);// カウント
        //                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, seijun_nm);// 正準区分名称
        //                dpc.SetParam("@ITEM18", SqlDbType.Decimal, 35, MaxZeikomiGaku);// 水揚税込合計金額

        //                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
        //                // ループの最後 又は 船主コードが一致しない場合
        //                if (cnt == dtMainLoop.Rows.Count || !senshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["KUMIAIIN_CD"])))
        //                {
        //                    MaxZeikomiGaku = 0;
        //                }
        //                GYOSHUBUNRUI.Clear();
        //                i++;
        //            }
        //            #endregion
        //        }
        //    }
        //        #region 印刷ワークテーブルに登録
        //        // 印刷ワークテーブルのデータ件数を取得
        //        dpc = new DbParamCollection();
        //        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
        //        DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
        //            "SORT",
        //            "PR_HN_TBL",
        //            "GUID = @GUID",
        //            dpc);

        //        bool dataFlag;
        //        if (tmpdtPR_HN_TBL.Rows.Count > 0)
        //        {
        //            dataFlag = true;
        //        }
        //        else
        //        {
        //            dataFlag = false;
        //        }

        //        return dataFlag;
        //        #endregion
        //    }
        #endregion

        #region データ作成
        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。漁態魚種別水揚月報
        /// </summary>
        private bool MakeWkData(int TypeNo)
        {
            #region メインデータを取得
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, "1", this.Dba);

            int lastDayInMonth = DateTime.DaysInMonth(Util.ToInt(this.txtDateYearTo.Text), Util.ToInt(this.txtDateMonthTo.Text));

            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(lastDayInMonth), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 表示日付
            string hyojidate = tmpjpDateFr[0] + tmpjpDateFr[2] + "年" + tmpjpDateFr[3] + "月 ～ " +
                               tmpjpDateTo[0] + tmpjpDateTo[2] + "年" + tmpjpDateTo[3] + "月分";

            string hyoujititle;
            string headertitle;
            if (TypeNo == 1)
            {
                hyoujititle = "漁 態 別 魚 種 別 水 揚 月 報";
                headertitle = "魚種コード / 魚 種 名 称";
            }
            else
            {
                hyoujititle = "漁 態 別 魚 種 分 類 別 水 揚 月 報";
                headertitle = "魚種分類コード / 魚 種 分 類 名";
            }

            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "99999";
            }
            #region 支所コードの退避
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            #endregion

            int i; // ループ用カウント変数
            DateTime dt = DateTime.MinValue; // 精算日

            // 精算区分設定
            string seisanKubunhyoji = this.lblSeisanKbnNm.Text;
            string seisanKbn = this.txtSeisanKbn.Text;

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 検索する日付をセット
            dpc.SetParam("@SERIBI_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SERIBI_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, funanushiCdTo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            StringBuilder Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD,");
            else
            {
                Sql.Append(" 0 AS SHISHO_CD,");
            }
            Sql.Append(" MAX(A.SHISHO_NM) AS SHISHO_NM,");
            Sql.Append(" B.SEIJUN_KUBUN,");
            Sql.Append(" A.SENSHU_CD AS KUMIAIIN_CD,");
            Sql.Append(" MAX(MONTH(A.SERIBI)) AS SERITSUKI,");
            Sql.Append(" A.SERIBI,");
            Sql.Append(" A.SEISAN_KUBUN,");
            Sql.Append(" MAX(A.SENSHU_NM) AS KUMIAIIN_NM,");
            Sql.Append(" A.CHIKU_CD,");
            Sql.Append(" MAX(B.CHIKU_NM) AS CHIKU_NM,");
            Sql.Append(" A.GYOHO_CD AS GYOTAI_CD,");
            Sql.Append(" MAX(A.GYOHO_NM) AS GYOTAI_NM,");
            if (TypeNo == 1)
            {
                Sql.Append(" A.GYOSHU_CD AS GYOSHU_CD,");
                Sql.Append(" MAX(A.GYOSHU_NM) AS GYOSHU_NM,");
            }
            else
            {
                Sql.Append(" A.GYOSHU_BUNRUI_CD AS GYOSHU_BUNRUI_CD,");
                Sql.Append(" MAX(A.GYOSHU_BUNRUI_NM) AS GYOSHU_BUNRUI_NM,");
            }
            Sql.Append(" SUM(A.SURYO) AS MIZUAGE_SURYO,");
            Sql.Append(" SUM(A.KINGAKU) AS MIZUAGE_KINGAKU,");
            Sql.Append(" SUM(CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = " + TaxUtil.INPUT_CATEGORY_INCLUDED.ToString());
            Sql.Append(" THEN A.KINGAKU ");
            Sql.Append(" ELSE A.KINGAKU + ISNULL(A.SHOHIZEI_FURIWAKE, ISNULL(A.SHOHIZEI, 0)) END) AS MIZUAGE_ZEIKOMI_KINGAKU,");
            Sql.Append(" MAX(A.DENPYO_BANGO) AS MIZUAGE_KAISU  ");
            Sql.Append(" FROM VI_HN_SHIKIRI_MEISAI2 A");
            Sql.Append(" LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO B");
            Sql.Append(" ON	A.KAISHA_CD = B.KAISHA_CD  ");
            Sql.Append(" AND A.SENSHU_CD = B.TORIHIKISAKI_CD  ");
            Sql.Append(" WHERE ");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" B.TORIHIKISAKI_KUBUN1 = 1 AND ");
            Sql.Append(" A.SERIBI BETWEEN @SERIBI_FR AND @SERIBI_TO AND ");
            Sql.Append(" A.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO  ");
            // 精算区分設定
            if (seisanKbn != "0")
            {
                Sql.Append("AND A.SEISAN_KUBUN = 3 ");
            }
            Sql.Append(" GROUP BY ");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.SERIBI,");
            Sql.Append(" B.SEIJUN_KUBUN,");
            Sql.Append(" A.SENSHU_CD,");
            Sql.Append(" A.GYOHO_CD,");
            Sql.Append(" A.CHIKU_CD,");
            if (TypeNo == 1)
            {
                Sql.Append(" A.GYOSHU_CD,");
            }
            else
            {
                Sql.Append(" A.GYOSHU_BUNRUI_CD,");
            }
            Sql.Append(" A.SEISAN_KUBUN ");
            Sql.Append(" ORDER BY ");
            Sql.Append(" A.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.CHIKU_CD,");
            Sql.Append(" B.SEIJUN_KUBUN,");
            Sql.Append(" KUMIAIIN_CD,");
            if (TypeNo == 1)
            {
                Sql.Append(" A.GYOSHU_CD,");
            }
            else
            {
                Sql.Append(" A.GYOSHU_BUNRUI_CD,");
            }
            Sql.Append(" GYOTAI_CD");
            DataTable dtMainLoop = Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region 水揚回数の計算のため重複値を省く
            //DataViewの作成
            DataView dv = dtMainLoop.DefaultView;
            //Distinctをかける
            DataTable resultDt = dv.ToTable("DistinctTable", true, "MIZUAGE_KAISU", "KUMIAIIN_CD");
            #endregion

            #region 準備
            NumVals GYOSHU = new NumVals(); // 魚種グループ
            int typeCd = 0;
            //int gyoshuCd = 0;
            int senshuCd = 0;
            int cnt = 0;
            decimal MaxZeikomiGaku = 0;
            string seijun_nm;
            string typeCd_Nm;
            string typeNm_nm;

            DateTime nowDate = System.DateTime.Now;
            string[] tmpnowDate = Util.ConvJpDate(nowDate, this.Dba);
            #endregion 

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                dpc = new DbParamCollection();
                i = 1;

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    #region インサートするための情報の保持
                    GYOSHU.MIZUAGESURYO += Util.ToDecimal(dr["MIZUAGE_SURYO"]);
                    GYOSHU.MIZUAGEKINGAKU += Util.ToDecimal(dr["MIZUAGE_ZEIKOMI_KINGAKU"]);
                    GYOSHU.KENSU += 1;
                    if (Util.ToInt(dr["SEIJUN_KUBUN"]) == 1)
                    {
                        seijun_nm = "組合員";
                    }
                    else if (Util.ToInt(dr["SEIJUN_KUBUN"]) == 2)
                    {
                        seijun_nm = "准組合員";
                    }
                    else
                    {
                        seijun_nm = "組合員以外";
                    }

                    // 現在の魚種コードを保持
                    if (TypeNo == 1)
                    {
                        typeCd = Util.ToInt(dr["GYOSHU_CD"]);
                        typeCd_Nm = "GYOSHU_CD";
                        typeNm_nm = "GYOSHU_NM";
                    }
                    else
                    {
                        typeCd = Util.ToInt(dr["GYOSHU_BUNRUI_CD"]);
                        typeCd_Nm = "GYOSHU_BUNRUI_CD";
                        typeNm_nm = "GYOSHU_BUNRUI_NM";
                    }
                    // 現在の船主コードを保持
                    senshuCd = Util.ToInt(dr["KUMIAIIN_CD"]);
                    cnt++;
                    #endregion

                    #region ループの最後 又は 次回の魚種コードが今回の魚種コードと一致しない場合
                    if (cnt == dtMainLoop.Rows.Count || !typeCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt][typeCd_Nm])))
                    {
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojidate);// 日付
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyoujititle);// タイトル
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, headertitle);// ヘッダータイトル
                        // データを設定
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SEIJUN_KUBUN"]);// 正準区分
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, seisanKubunhyoji);//精算区分
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["CHIKU_NM"]);// 地区名
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["KUMIAIIN_CD"]);// 組合員コード
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["KUMIAIIN_NM"]);// 組合員名
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["GYOTAI_CD"]);// 魚態コード
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["GYOTAI_NM"]);// 魚態名称
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr[typeCd_Nm]);// 魚種コード
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr[typeNm_nm]);// 魚種名
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, GYOSHU.MIZUAGESURYO);// 水揚数量
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, GYOSHU.MIZUAGEKINGAKU);// 水揚税込金額
                        MaxZeikomiGaku += GYOSHU.MIZUAGEKINGAKU;
                        // 水揚回数を取得
                        string value = Util.ToString(resultDt.Compute("Count(MIZUAGE_KAISU)", "KUMIAIIN_CD =" + dr["KUMIAIIN_CD"]));
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, value);// 水揚回数
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, GYOSHU.KENSU);// カウント
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, seijun_nm);// 正準区分名称
                        dpc.SetParam("@ITEM18", SqlDbType.Decimal, 35, MaxZeikomiGaku);// 水揚税込合計金額
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dr["SHISHO_CD"]);// 支所コード

                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, tmpnowDate[5]);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        /**/
                        if (cnt == dtMainLoop.Rows.Count || !senshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["KUMIAIIN_CD"])))
                        {
                            MaxZeikomiGaku = 0;
                        }
                        GYOSHU.Clear();
                        i++;
                    }
                    #endregion
                    #region ループの最後 又は 魚種コードが一致して かつ 船主コードが一致しない場合
                    else if (cnt == dtMainLoop.Rows.Count || (typeCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt][typeCd_Nm])) && !senshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["KUMIAIIN_CD"]))))
                    {
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojidate);// 日付
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyoujititle);// タイトル
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, headertitle);// ヘッダータイトル
                        // データを設定
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SEIJUN_KUBUN"]);// 正準区分
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, seisanKubunhyoji);//精算区分
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["CHIKU_NM"]);// 地区名
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["KUMIAIIN_CD"]);// 組合員コード
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["KUMIAIIN_NM"]);// 組合員名
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["GYOTAI_CD"]);// 魚態コード
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["GYOTAI_NM"]);// 魚態名称
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr[typeCd_Nm]);// 魚種コード
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr[typeNm_nm]);// 魚種名
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, GYOSHU.MIZUAGESURYO);// 水揚数量
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, GYOSHU.MIZUAGEKINGAKU);// 水揚税込金額
                        MaxZeikomiGaku += GYOSHU.MIZUAGEKINGAKU;
                        // 水揚回数を取得
                        string value = Util.ToString(resultDt.Compute("Count(MIZUAGE_KAISU)", "KUMIAIIN_CD =" + dr["KUMIAIIN_CD"]));
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, value);// 水揚回数
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, GYOSHU.KENSU);// カウント
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, seijun_nm);// 正準区分名称
                        dpc.SetParam("@ITEM18", SqlDbType.Decimal, 35, MaxZeikomiGaku);// 水揚税込合計金額
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dr["SHISHO_CD"]);// 支所コード

                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, tmpnowDate[5]);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        if (cnt == dtMainLoop.Rows.Count || !senshuCd.Equals(Util.ToInt(dtMainLoop.Rows[cnt]["KUMIAIIN_CD"])))
                        {
                            MaxZeikomiGaku = 0;
                        }
                        GYOSHU.Clear();
                        i++;
                    }
                    #endregion
                }

            }
            #region 印刷ワークテーブルに登録
            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
            #endregion
        }
        #endregion

    }
}
#endregion