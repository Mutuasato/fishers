﻿namespace jp.co.fsi.hn.hnyr1081
{
    /// <summary>
    /// HANR2131R の概要の説明です。
    /// </summary>
    partial class HNYR1081R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNYR1081R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtShiharaiKbn = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTanka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAzukarikin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHonnin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSashihikiKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.hyojidate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.seisankubun = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.chikunm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtgyoshu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtgyoshunm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtsuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHonnin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtkaisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKumiaiin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtgyotaicd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtgyotainm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.grpHeaderChiku = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.grpFooterChiku = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtChikuKaisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.grpHeaderseijun = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.grpFooterseijun = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSeijunKaisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.grpHeadersenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.grpFootersenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSenshuKaisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAllKaisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.grpHeadergyotai = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.grpFootergyotai = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtShiharaiKbn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAzukarikin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHonnin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSashihikiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyojidate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seisankubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chikunm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgyoshu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgyoshunm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHonnin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkaisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKumiaiin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgyotaicd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgyotainm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChikuKaisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeijunKaisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuKaisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllKaisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtShiharaiKbn,
            this.lblPage,
            this.txtPage,
            this.lblCd,
            this.line1,
            this.lblTanka,
            this.lblAzukarikin,
            this.lblHonnin,
            this.lblSashihikiKingaku,
            this.hyojidate,
            this.label4,
            this.seisankubun,
            this.chikunm,
            this.label6,
            this.label7,
            this.label8,
            this.textBox1,
            this.txtToday});
            this.pageHeader.Height = 1.063648F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtShiharaiKbn
            // 
            this.txtShiharaiKbn.DataField = "ITEM02";
            this.txtShiharaiKbn.Height = 0.2915026F;
            this.txtShiharaiKbn.Left = 4.593701F;
            this.txtShiharaiKbn.Name = "txtShiharaiKbn";
            this.txtShiharaiKbn.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 128";
            this.txtShiharaiKbn.Text = "漁態別魚種分類別水揚月報";
            this.txtShiharaiKbn.Top = 0.07440946F;
            this.txtShiharaiKbn.Width = 4.221654F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1458333F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 12.90512F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.07440946F;
            this.lblPage.Width = 0.4051183F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1456693F;
            this.txtPage.Left = 12.41654F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; text-align: right; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.07440946F;
            this.txtPage.Width = 0.4767723F;
            // 
            // lblCd
            // 
            this.lblCd.Height = 0.1874016F;
            this.lblCd.HyperLink = null;
            this.lblCd.Left = 0.2759843F;
            this.lblCd.Name = "lblCd";
            this.lblCd.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: bottom; ddo-char-set: 128";
            this.lblCd.Text = "組合員コード / 組 合 員 名";
            this.lblCd.Top = 0.844882F;
            this.lblCd.Width = 2.26063F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.08110237F;
            this.line1.LineWeight = 3.5F;
            this.line1.Name = "line1";
            this.line1.Top = 1.051969F;
            this.line1.Width = 13.30473F;
            this.line1.X1 = 0.08110237F;
            this.line1.X2 = 13.38583F;
            this.line1.Y1 = 1.051969F;
            this.line1.Y2 = 1.051969F;
            // 
            // lblTanka
            // 
            this.lblTanka.Height = 0.1874016F;
            this.lblTanka.HyperLink = null;
            this.lblTanka.Left = 3.140945F;
            this.lblTanka.Name = "lblTanka";
            this.lblTanka.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: bottom; ddo-char-set: 128";
            this.lblTanka.Text = "漁態コード / 漁 態 名 称\r\n";
            this.lblTanka.Top = 0.844882F;
            this.lblTanka.Width = 1.959449F;
            // 
            // lblAzukarikin
            // 
            this.lblAzukarikin.Height = 0.1874016F;
            this.lblAzukarikin.HyperLink = null;
            this.lblAzukarikin.Left = 8.200395F;
            this.lblAzukarikin.Name = "lblAzukarikin";
            this.lblAzukarikin.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: bottom; ddo-char-set: 128";
            this.lblAzukarikin.Text = "水揚数量（Kg）";
            this.lblAzukarikin.Top = 0.844882F;
            this.lblAzukarikin.Width = 1.282283F;
            // 
            // lblHonnin
            // 
            this.lblHonnin.Height = 0.1874016F;
            this.lblHonnin.HyperLink = null;
            this.lblHonnin.Left = 9.940552F;
            this.lblHonnin.Name = "lblHonnin";
            this.lblHonnin.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: bottom; ddo-char-set: 128";
            this.lblHonnin.Text = "水揚税込金額\r\n";
            this.lblHonnin.Top = 0.844882F;
            this.lblHonnin.Width = 1.061418F;
            // 
            // lblSashihikiKingaku
            // 
            this.lblSashihikiKingaku.Height = 0.1874016F;
            this.lblSashihikiKingaku.HyperLink = null;
            this.lblSashihikiKingaku.Left = 11.59803F;
            this.lblSashihikiKingaku.Name = "lblSashihikiKingaku";
            this.lblSashihikiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: bottom; ddo-char-set: 128";
            this.lblSashihikiKingaku.Text = "水揚回数";
            this.lblSashihikiKingaku.Top = 0.844882F;
            this.lblSashihikiKingaku.Width = 0.7972803F;
            // 
            // hyojidate
            // 
            this.hyojidate.DataField = "ITEM01";
            this.hyojidate.Height = 0.1559876F;
            this.hyojidate.Left = 0.08110237F;
            this.hyojidate.Name = "hyojidate";
            this.hyojidate.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.hyojidate.Text = "hyojidate";
            this.hyojidate.Top = 0F;
            this.hyojidate.Width = 2.610236F;
            // 
            // label4
            // 
            this.label4.Height = 0.1458333F;
            this.label4.HyperLink = null;
            this.label4.Left = 11.99961F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label4.Text = "作成";
            this.label4.Top = 0.07440946F;
            this.label4.Width = 0.3956693F;
            // 
            // seisankubun
            // 
            this.seisankubun.DataField = "ITEM05";
            this.seisankubun.Height = 0.1559876F;
            this.seisankubun.Left = 0.9771654F;
            this.seisankubun.Name = "seisankubun";
            this.seisankubun.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.seisankubun.Text = "seisankubun";
            this.seisankubun.Top = 0.3122047F;
            this.seisankubun.Width = 1.010236F;
            // 
            // chikunm
            // 
            this.chikunm.DataField = "ITEM06";
            this.chikunm.Height = 0.1559876F;
            this.chikunm.Left = 0.977559F;
            this.chikunm.Name = "chikunm";
            this.chikunm.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.chikunm.Text = "chikunm";
            this.chikunm.Top = 0.5141733F;
            this.chikunm.Width = 1.010236F;
            // 
            // label6
            // 
            this.label6.Height = 0.1728346F;
            this.label6.HyperLink = null;
            this.label6.Left = 0.02283464F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; font-weight: normal; text-align: center; " +
    "vertical-align: bottom; ddo-char-set: 1";
            this.label6.Text = "精算区分 :";
            this.label6.Top = 0.3122047F;
            this.label6.Width = 0.8850396F;
            // 
            // label7
            // 
            this.label7.Height = 0.1559055F;
            this.label7.HyperLink = null;
            this.label7.Left = 0.02283465F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; font-weight: normal; text-align: center; " +
    "vertical-align: bottom; ddo-char-set: 1";
            this.label7.Text = "地 区 名 :";
            this.label7.Top = 0.5141733F;
            this.label7.Width = 0.8850396F;
            // 
            // label8
            // 
            this.label8.Height = 0.1458333F;
            this.label8.HyperLink = null;
            this.label8.Left = 12.5F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label8.Text = "【税込み】";
            this.label8.Top = 0.322441F;
            this.label8.Visible = false;
            this.label8.Width = 0.8102363F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM03";
            this.textBox1.Height = 0.1874016F;
            this.textBox1.Left = 5.537008F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 128";
            this.textBox1.Text = "魚種分類コード / 魚 種 分 類 名";
            this.textBox1.Top = 0.8763781F;
            this.textBox1.Width = 2.380315F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM20";
            this.txtToday.Height = 0.1456693F;
            this.txtToday.Left = 10.3941F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; text-align: right; ddo-char-set: 1";
            this.txtToday.Text = "Page";
            this.txtToday.Top = 0.07440946F;
            this.txtToday.Width = 1.591338F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM19";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtgyoshu,
            this.txtgyoshunm,
            this.txtsuryo,
            this.txtHonnin,
            this.txtkaisu,
            this.txtCd,
            this.txtKumiaiin,
            this.txtgyotaicd,
            this.txtgyotainm});
            this.detail.Height = 0.21875F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // txtgyoshu
            // 
            this.txtgyoshu.DataField = "ITEM11";
            this.txtgyoshu.Height = 0.1979167F;
            this.txtgyoshu.Left = 6.022835F;
            this.txtgyoshu.MultiLine = false;
            this.txtgyoshu.Name = "txtgyoshu";
            this.txtgyoshu.OutputFormat = resources.GetString("txtgyoshu.OutputFormat");
            this.txtgyoshu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.txtgyoshu.Text = "Cd";
            this.txtgyoshu.Top = 0.02086614F;
            this.txtgyoshu.Width = 0.4850397F;
            // 
            // txtgyoshunm
            // 
            this.txtgyoshunm.DataField = "ITEM12";
            this.txtgyoshunm.Height = 0.1979167F;
            this.txtgyoshunm.Left = 6.665355F;
            this.txtgyoshunm.MultiLine = false;
            this.txtgyoshunm.Name = "txtgyoshunm";
            this.txtgyoshunm.OutputFormat = resources.GetString("txtgyoshunm.OutputFormat");
            this.txtgyoshunm.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.txtgyoshunm.Text = "Nm";
            this.txtgyoshunm.Top = 0.02086614F;
            this.txtgyoshunm.Width = 1.53504F;
            // 
            // txtsuryo
            // 
            this.txtsuryo.DataField = "ITEM13";
            this.txtsuryo.Height = 0.1979167F;
            this.txtsuryo.Left = 8.200395F;
            this.txtsuryo.MultiLine = false;
            this.txtsuryo.Name = "txtsuryo";
            this.txtsuryo.OutputFormat = resources.GetString("txtsuryo.OutputFormat");
            this.txtsuryo.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.txtsuryo.Text = "suryo\r\n";
            this.txtsuryo.Top = 0.02086608F;
            this.txtsuryo.Width = 1.045275F;
            // 
            // txtHonnin
            // 
            this.txtHonnin.DataField = "ITEM14";
            this.txtHonnin.Height = 0.1979167F;
            this.txtHonnin.Left = 9.482677F;
            this.txtHonnin.MultiLine = false;
            this.txtHonnin.Name = "txtHonnin";
            this.txtHonnin.OutputFormat = resources.GetString("txtHonnin.OutputFormat");
            this.txtHonnin.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle; d" +
    "do-char-set: 1";
            this.txtHonnin.Text = "999,999,999,999,999";
            this.txtHonnin.Top = 0.02086608F;
            this.txtHonnin.Width = 1.42005F;
            // 
            // txtkaisu
            // 
            this.txtkaisu.DataField = "ITEM15";
            this.txtkaisu.Height = 0.1979167F;
            this.txtkaisu.Left = 11.79216F;
            this.txtkaisu.MultiLine = false;
            this.txtkaisu.Name = "txtkaisu";
            this.txtkaisu.OutputFormat = resources.GetString("txtkaisu.OutputFormat");
            this.txtkaisu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.txtkaisu.Text = "kaisu";
            this.txtkaisu.Top = 0.02086608F;
            this.txtkaisu.Visible = false;
            this.txtkaisu.Width = 0.6031189F;
            // 
            // txtCd
            // 
            this.txtCd.DataField = "ITEM07";
            this.txtCd.Height = 0.1979167F;
            this.txtCd.Left = 0.7830708F;
            this.txtCd.MultiLine = false;
            this.txtCd.Name = "txtCd";
            this.txtCd.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: center; ddo-char-set: 1";
            this.txtCd.Text = "cd";
            this.txtCd.Top = 0F;
            this.txtCd.Width = 0.4370406F;
            // 
            // txtKumiaiin
            // 
            this.txtKumiaiin.DataField = "ITEM08";
            this.txtKumiaiin.Height = 0.1979167F;
            this.txtKumiaiin.Left = 1.222835F;
            this.txtKumiaiin.MultiLine = false;
            this.txtKumiaiin.Name = "txtKumiaiin";
            this.txtKumiaiin.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.txtKumiaiin.Text = "kumiaiin";
            this.txtKumiaiin.Top = 0F;
            this.txtKumiaiin.Width = 1.685433F;
            // 
            // txtgyotaicd
            // 
            this.txtgyotaicd.DataField = "ITEM09";
            this.txtgyotaicd.Height = 0.1979167F;
            this.txtgyotaicd.Left = 3.434646F;
            this.txtgyotaicd.MultiLine = false;
            this.txtgyotaicd.Name = "txtgyotaicd";
            this.txtgyotaicd.OutputFormat = resources.GetString("txtgyotaicd.OutputFormat");
            this.txtgyotaicd.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.txtgyotaicd.Text = "gyotaicd";
            this.txtgyotaicd.Top = 0F;
            this.txtgyotaicd.Width = 0.5507874F;
            // 
            // txtgyotainm
            // 
            this.txtgyotainm.DataField = "ITEM10";
            this.txtgyotainm.Height = 0.1979167F;
            this.txtgyotainm.Left = 4.090158F;
            this.txtgyotainm.MultiLine = false;
            this.txtgyotainm.Name = "txtgyotainm";
            this.txtgyotainm.OutputFormat = resources.GetString("txtgyotainm.OutputFormat");
            this.txtgyotainm.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.txtgyotainm.Text = "gyogatainm";
            this.txtgyotainm.Top = 0F;
            this.txtgyotainm.Width = 1.219685F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // grpHeaderChiku
            // 
            this.grpHeaderChiku.DataField = "ITEM06";
            this.grpHeaderChiku.Height = 0F;
            this.grpHeaderChiku.Name = "grpHeaderChiku";
            // 
            // grpFooterChiku
            // 
            this.grpFooterChiku.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label5,
            this.label12,
            this.textBox5,
            this.textBox9,
            this.textBox13,
            this.txtChikuKaisu});
            this.grpFooterChiku.Height = 0.1968504F;
            this.grpFooterChiku.Name = "grpFooterChiku";
            this.grpFooterChiku.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            this.grpFooterChiku.Format += new System.EventHandler(this.grpHeaderChiku_Format);
            // 
            // label5
            // 
            this.label5.Height = 0.1665354F;
            this.label5.HyperLink = null;
            this.label5.Left = 4.016929F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label5.Text = "[地 区 小 計]";
            this.label5.Top = 0.02125984F;
            this.label5.Width = 1.219685F;
            // 
            // label12
            // 
            this.label12.Height = 0.1665354F;
            this.label12.HyperLink = null;
            this.label12.Left = 6.10551F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label12.Text = "件数\r\n";
            this.label12.Top = 0.03031497F;
            this.label12.Width = 0.5263783F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM16";
            this.textBox5.Height = 0.1665354F;
            this.textBox5.Left = 6.945668F;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.textBox5.SummaryGroup = "grpHeaderChiku";
            this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox5.Text = "kensu";
            this.textBox5.Top = 0.03031497F;
            this.textBox5.Width = 0.5437014F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM13";
            this.textBox9.Height = 0.1665354F;
            this.textBox9.Left = 8.200395F;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.textBox9.SummaryGroup = "grpHeaderChiku";
            this.textBox9.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox9.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox9.Text = "kensu";
            this.textBox9.Top = 0.0303149F;
            this.textBox9.Width = 1.045275F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM14";
            this.textBox13.Height = 0.1665354F;
            this.textBox13.Left = 9.482677F;
            this.textBox13.Name = "textBox13";
            this.textBox13.OutputFormat = resources.GetString("textBox13.OutputFormat");
            this.textBox13.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.textBox13.SummaryGroup = "grpHeaderChiku";
            this.textBox13.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox13.Text = "kensu";
            this.textBox13.Top = 0.0303149F;
            this.textBox13.Width = 1.42005F;
            // 
            // txtChikuKaisu
            // 
            this.txtChikuKaisu.DataField = "ITEM15";
            this.txtChikuKaisu.Height = 0.1665354F;
            this.txtChikuKaisu.Left = 11.79216F;
            this.txtChikuKaisu.Name = "txtChikuKaisu";
            this.txtChikuKaisu.OutputFormat = resources.GetString("txtChikuKaisu.OutputFormat");
            this.txtChikuKaisu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.txtChikuKaisu.Text = "kensu";
            this.txtChikuKaisu.Top = 0.0303149F;
            this.txtChikuKaisu.Width = 0.4980001F;
            // 
            // grpHeaderseijun
            // 
            this.grpHeaderseijun.DataField = "ITEM04";
            this.grpHeaderseijun.Height = 0F;
            this.grpHeaderseijun.Name = "grpHeaderseijun";
            // 
            // grpFooterseijun
            // 
            this.grpFooterseijun.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line2,
            this.label3,
            this.label11,
            this.textBox4,
            this.textBox8,
            this.textBox12,
            this.txtSeijunKaisu,
            this.textBox21});
            this.grpFooterseijun.Height = 0.1968504F;
            this.grpFooterseijun.Name = "grpFooterseijun";
            this.grpFooterseijun.Format += new System.EventHandler(this.grpHeaderseijun_Format);
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.08110237F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.1850394F;
            this.line2.Width = 13.30473F;
            this.line2.X1 = 0.08110237F;
            this.line2.X2 = 13.38583F;
            this.line2.Y1 = 0.1850394F;
            this.line2.Y2 = 0.1850394F;
            // 
            // label3
            // 
            this.label3.Height = 0.1562992F;
            this.label3.HyperLink = null;
            this.label3.Left = 4.016929F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label3.Text = "[         計]";
            this.label3.Top = 0.009055119F;
            this.label3.Width = 1.219685F;
            // 
            // label11
            // 
            this.label11.Height = 0.1562992F;
            this.label11.HyperLink = null;
            this.label11.Left = 6.10551F;
            this.label11.Name = "label11";
            this.label11.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label11.Text = "件数\r\n";
            this.label11.Top = 0.01811025F;
            this.label11.Width = 0.5263783F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM16";
            this.textBox4.Height = 0.1562992F;
            this.textBox4.Left = 6.945669F;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.textBox4.SummaryGroup = "grpHeaderseijun";
            this.textBox4.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox4.Text = "kensu";
            this.textBox4.Top = 0.01811025F;
            this.textBox4.Width = 0.5437014F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM13";
            this.textBox8.Height = 0.1562992F;
            this.textBox8.Left = 8.200395F;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.textBox8.SummaryGroup = "grpHeaderseijun";
            this.textBox8.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox8.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox8.Text = "kensu";
            this.textBox8.Top = 0.01811019F;
            this.textBox8.Width = 1.045275F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM14";
            this.textBox12.Height = 0.1562992F;
            this.textBox12.Left = 9.482677F;
            this.textBox12.Name = "textBox12";
            this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.textBox12.SummaryGroup = "grpHeaderseijun";
            this.textBox12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox12.Text = "kensu";
            this.textBox12.Top = 0.01811018F;
            this.textBox12.Width = 1.42005F;
            // 
            // txtSeijunKaisu
            // 
            this.txtSeijunKaisu.DataField = "ITEM15";
            this.txtSeijunKaisu.Height = 0.1562992F;
            this.txtSeijunKaisu.Left = 11.79216F;
            this.txtSeijunKaisu.Name = "txtSeijunKaisu";
            this.txtSeijunKaisu.OutputFormat = resources.GetString("txtSeijunKaisu.OutputFormat");
            this.txtSeijunKaisu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.txtSeijunKaisu.Text = "kensu";
            this.txtSeijunKaisu.Top = 0.01811018F;
            this.txtSeijunKaisu.Width = 0.4980001F;
            // 
            // textBox21
            // 
            this.textBox21.CanGrow = false;
            this.textBox21.DataField = "ITEM17";
            this.textBox21.Height = 0.1562992F;
            this.textBox21.Left = 4.090158F;
            this.textBox21.MultiLine = false;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.textBox21.Text = "seijunkubn";
            this.textBox21.Top = 0.02047244F;
            this.textBox21.Width = 0.8370078F;
            // 
            // grpHeadersenshu
            // 
            this.grpHeadersenshu.CanGrow = false;
            this.grpHeadersenshu.DataField = "ITEM07";
            this.grpHeadersenshu.Height = 0F;
            this.grpHeadersenshu.Name = "grpHeadersenshu";
            this.grpHeadersenshu.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.grpHeadersenshu.UnderlayNext = true;
            // 
            // grpFootersenshu
            // 
            this.grpFootersenshu.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line4,
            this.label9,
            this.label10,
            this.textBox3,
            this.textBox7,
            this.textBox11,
            this.txtSenshuKaisu});
            this.grpFootersenshu.Height = 0.1968504F;
            this.grpFootersenshu.Name = "grpFootersenshu";
            this.grpFootersenshu.Format += new System.EventHandler(this.grpHeadersenshu_Format);
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 0.08110237F;
            this.line4.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.1771654F;
            this.line4.Width = 13.30473F;
            this.line4.X1 = 0.08110237F;
            this.line4.X2 = 13.38583F;
            this.line4.Y1 = 0.1771654F;
            this.line4.Y2 = 0.1771654F;
            // 
            // label9
            // 
            this.label9.Height = 0.1562992F;
            this.label9.HyperLink = null;
            this.label9.Left = 4.016929F;
            this.label9.Name = "label9";
            this.label9.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label9.Text = "[組 合 員 計]";
            this.label9.Top = 0.02086614F;
            this.label9.Width = 1.219686F;
            // 
            // label10
            // 
            this.label10.Height = 0.1353511F;
            this.label10.HyperLink = null;
            this.label10.Left = 6.105511F;
            this.label10.Name = "label10";
            this.label10.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label10.Text = "件数\r\n";
            this.label10.Top = 0.04173227F;
            this.label10.Width = 0.5263782F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM16";
            this.textBox3.Height = 0.1353511F;
            this.textBox3.Left = 6.945669F;
            this.textBox3.Name = "textBox3";
            this.textBox3.OutputFormat = resources.GetString("textBox3.OutputFormat");
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.textBox3.SummaryGroup = "grpHeadersenshu";
            this.textBox3.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox3.Text = "kensu";
            this.textBox3.Top = 0.04173227F;
            this.textBox3.Width = 0.5437016F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM13";
            this.textBox7.Height = 0.1353511F;
            this.textBox7.Left = 8.200395F;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.textBox7.SummaryGroup = "grpHeadersenshu";
            this.textBox7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox7.Text = "kensu";
            this.textBox7.Top = 0.04173221F;
            this.textBox7.Width = 1.045275F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM18";
            this.textBox11.Height = 0.1353511F;
            this.textBox11.Left = 9.482677F;
            this.textBox11.Name = "textBox11";
            this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; text-justify: auto; ddo-c" +
    "har-set: 1";
            this.textBox11.Text = "kensu";
            this.textBox11.Top = 0.04173221F;
            this.textBox11.Width = 1.42005F;
            // 
            // txtSenshuKaisu
            // 
            this.txtSenshuKaisu.DataField = "ITEM15";
            this.txtSenshuKaisu.Height = 0.1353511F;
            this.txtSenshuKaisu.Left = 11.79216F;
            this.txtSenshuKaisu.Name = "txtSenshuKaisu";
            this.txtSenshuKaisu.OutputFormat = resources.GetString("txtSenshuKaisu.OutputFormat");
            this.txtSenshuKaisu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.txtSenshuKaisu.Text = "kensu";
            this.txtSenshuKaisu.Top = 0.04173223F;
            this.txtSenshuKaisu.Width = 0.4980001F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label13,
            this.label14,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.txtAllKaisu});
            this.reportFooter1.Height = 0.2604167F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // label13
            // 
            this.label13.Height = 0.1770833F;
            this.label13.HyperLink = null;
            this.label13.Left = 4.01693F;
            this.label13.Name = "label13";
            this.label13.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; font-weight: normal; ddo-char-set: 1";
            this.label13.Text = "[総       計]";
            this.label13.Top = 0.03307087F;
            this.label13.Width = 1.219685F;
            // 
            // label14
            // 
            this.label14.Height = 0.1770833F;
            this.label14.HyperLink = null;
            this.label14.Left = 6.105511F;
            this.label14.Name = "label14";
            this.label14.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label14.Text = "件数\r\n";
            this.label14.Top = 0.03307087F;
            this.label14.Width = 0.5263783F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM16";
            this.textBox14.Height = 0.1770833F;
            this.textBox14.Left = 6.945669F;
            this.textBox14.Name = "textBox14";
            this.textBox14.OutputFormat = resources.GetString("textBox14.OutputFormat");
            this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.textBox14.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox14.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox14.Text = "kensu";
            this.textBox14.Top = 0.03307087F;
            this.textBox14.Width = 0.5437014F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM13";
            this.textBox15.Height = 0.1770833F;
            this.textBox15.Left = 8.200395F;
            this.textBox15.Name = "textBox15";
            this.textBox15.OutputFormat = resources.GetString("textBox15.OutputFormat");
            this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.textBox15.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox15.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox15.Text = "kensu";
            this.textBox15.Top = 0.03307081F;
            this.textBox15.Width = 1.045275F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM14";
            this.textBox16.Height = 0.1770833F;
            this.textBox16.Left = 9.482678F;
            this.textBox16.Name = "textBox16";
            this.textBox16.OutputFormat = resources.GetString("textBox16.OutputFormat");
            this.textBox16.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.textBox16.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox16.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox16.Text = "kensu";
            this.textBox16.Top = 0.03307081F;
            this.textBox16.Width = 1.42005F;
            // 
            // txtAllKaisu
            // 
            this.txtAllKaisu.DataField = "ITEM15";
            this.txtAllKaisu.Height = 0.1770833F;
            this.txtAllKaisu.Left = 11.79216F;
            this.txtAllKaisu.Name = "txtAllKaisu";
            this.txtAllKaisu.OutputFormat = resources.GetString("txtAllKaisu.OutputFormat");
            this.txtAllKaisu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.txtAllKaisu.Text = "kensu";
            this.txtAllKaisu.Top = 0.03307081F;
            this.txtAllKaisu.Width = 0.4980001F;
            // 
            // grpHeadergyotai
            // 
            this.grpHeadergyotai.CanGrow = false;
            this.grpHeadergyotai.DataField = "ITEM09";
            this.grpHeadergyotai.Height = 0F;
            this.grpHeadergyotai.Name = "grpHeadergyotai";
            this.grpHeadergyotai.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.grpHeadergyotai.UnderlayNext = true;
            // 
            // grpFootergyotai
            // 
            this.grpFootergyotai.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line3,
            this.label1,
            this.label2,
            this.textBox2,
            this.textBox6,
            this.textBox10});
            this.grpFootergyotai.Height = 0.1968504F;
            this.grpFootergyotai.Name = "grpFootergyotai";
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 2.755906F;
            this.line3.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.1862205F;
            this.line3.Width = 10.62992F;
            this.line3.X1 = 2.755906F;
            this.line3.X2 = 13.38583F;
            this.line3.Y1 = 0.1862205F;
            this.line3.Y2 = 0.1862205F;
            // 
            // label1
            // 
            this.label1.Height = 0.1770833F;
            this.label1.HyperLink = null;
            this.label1.Left = 4.016929F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label1.Text = "[漁 態 小 計]";
            this.label1.Top = 0.007874017F;
            this.label1.Width = 1.219685F;
            // 
            // label2
            // 
            this.label2.Height = 0.1562172F;
            this.label2.HyperLink = null;
            this.label2.Left = 6.105511F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 10.75pt; ddo-char-set: 1";
            this.label2.Text = "件数\r\n";
            this.label2.Top = 0.02874015F;
            this.label2.Width = 0.5263782F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM16";
            this.textBox2.Height = 0.1562172F;
            this.textBox2.Left = 6.945669F;
            this.textBox2.Name = "textBox2";
            this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.textBox2.SummaryGroup = "grpHeadergyotai";
            this.textBox2.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox2.Text = "kensu";
            this.textBox2.Top = 0.02874015F;
            this.textBox2.Width = 0.5437016F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM13";
            this.textBox6.Height = 0.1562172F;
            this.textBox6.Left = 8.200395F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.textBox6.SummaryGroup = "grpHeadergyotai";
            this.textBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox6.Text = "kensu";
            this.textBox6.Top = 0.02874009F;
            this.textBox6.Width = 1.045275F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM14";
            this.textBox10.Height = 0.1562172F;
            this.textBox10.Left = 9.482677F;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.textBox10.SummaryGroup = "grpHeadergyotai";
            this.textBox10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox10.Text = "kensu";
            this.textBox10.Top = 0.02874009F;
            this.textBox10.Width = 1.42005F;
            // 
            // HNYR1081R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1968504F;
            this.PageSettings.Margins.Left = 0.3937007F;
            this.PageSettings.Margins.Right = 0.3937007F;
            this.PageSettings.Margins.Top = 0.1968504F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.38583F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.grpHeaderChiku);
            this.Sections.Add(this.grpHeaderseijun);
            this.Sections.Add(this.grpHeadersenshu);
            this.Sections.Add(this.grpHeadergyotai);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.grpFootergyotai);
            this.Sections.Add(this.grpFootersenshu);
            this.Sections.Add(this.grpFooterseijun);
            this.Sections.Add(this.grpFooterChiku);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtShiharaiKbn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAzukarikin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHonnin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSashihikiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyojidate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seisankubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chikunm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgyoshu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgyoshunm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHonnin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkaisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKumiaiin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgyotaicd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgyotainm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChikuKaisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeijunKaisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuKaisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllKaisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShiharaiKbn;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader grpHeaderChiku;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter grpFooterChiku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCd;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAzukarikin;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHonnin;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSashihikiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKumiaiin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtgyotaicd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtgyotainm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtgyoshu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtgyoshunm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtsuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHonnin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox hyojidate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox seisankubun;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox chikunm;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader grpHeaderseijun;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter grpFooterseijun;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader grpHeadersenshu;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter grpFootersenshu;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader grpHeadergyotai;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter grpFootergyotai;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSenshuKaisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtChikuKaisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeijunKaisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllKaisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtkaisu;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
    }
}
