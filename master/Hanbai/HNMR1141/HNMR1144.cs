﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1141
{
    /// <summary>
    /// 項目設定(HNMR1144)
    /// </summary>
    public partial class HNMR1144 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// HNMR1144(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        String _pSHUKEIHYO;

        private int _shishoCode = 0;                    // 支所コード
        #endregion

        #region 定数
        /// <summary>
        /// データ取得用
        /// </summary>
        private const int POSITION_START_NO = 1;
        private const int POSITION_END_NO = 8;
        #endregion

        #region プロパティ
        /// <summary>
        /// タイトルを格納する用データテーブル
        /// </summary>
        private DataTable _dtTitle = new DataTable();
        public DataTable Title
        {
            get
            {
                return this._dtTitle;
            }
        }

        /// <summary>
        /// 集計区分を格納する用データテーブル
        /// </summary>
        private DataTable _dtShukeiKubun = new DataTable();
        public DataTable ShukeiKubun
        {
            get
            {
                return this._dtShukeiKubun;
            }
        }

        /// <summary>
        /// 選択項目を格納する用データテーブル
        /// </summary>
        private DataTable _dtSentakuKomoku = new DataTable();
        public DataTable SentakuKomoku
        {
            get
            {
                return this._dtSentakuKomoku;
            }
        }

        /// <summary>
        /// 選択候補を格納する用データテーブル
        /// </summary>
        private DataTable _dtSentakuKoho = new DataTable();
        public DataTable SentakuKoho
        {
            get
            {
                return this._dtSentakuKoho;
            }
        }

        /// <summary>
        /// タイトルNoを格納する用変数
        /// </summary>
        private Decimal _dtPositionNo = new Decimal();
        public Decimal PositionNo
        {
            get
            {
                return this._dtPositionNo;
            }
        }

        public int ShishoCode
        {
            set
            {
                this._shishoCode = value;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1144(String SHUKEIHYO)//
        {
            this._pSHUKEIHYO = SHUKEIHYO;

            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルの表示非表示を設定
            this.lblTitle.Visible = false;

            // Escapeのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 画面の初期表示時
            DispDataSetTitle();
            DispDataSetSentakuKomoku();
            DispDataSetShukeiKubun();
            DispDataSetSentakuKoho();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // タイトルを登録する ※現状、POSITIONが1でない場合、固定で0
            int juni = 0;
            if (_dtPositionNo == 1)
            {
                if (this.rdoShojun.Checked)
                {
                    juni = 1;
                }
                else if (this.rdoKojun.Checked)
                {
                    juni = 2;
                }
            }

            this.InsertTB_HN_SHUKEIHYO_SETTEI_SERI(juni);
            // 選択項目を登録する
            this.InsertTB_HN_SHUKEIHYO_SETTEI_MEISAI_SERI();

            // 不要なデータの削除
            Delete_Trush();

            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// タイトル名の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTitleNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTitleNm())
            {
                e.Cancel = true;
                this.txtTitleNm.SelectAll();
            }
        }

        /// <summary>
        /// タイトルが変更された場合の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nudTitleCd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // タイトルをセットする ※現状、POSITIONが1でない場合、固定で0
                int juni = 0;
                if (this.PositionNo == 1)
                {
                    if (this.rdoShojun.Checked)
                    {
                        juni = 1;
                    }
                    else if (this.rdoKojun.Checked)
                    {
                        juni = 2;
                    }
                }
                this.SetTitle(juni);
                // 選択項目をセットする
                this.SetSentakuKomoku();
                // 集計区分をセットする
                this.SetShukeiKubun();

                // タイトルNoを保持
                this._dtPositionNo = (Decimal)this.nudTitleCd.Value;
            }
        }

        /// <summary>
        /// タイトルが変更された場合の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nudTitleCd_ValueChanged(object sender, EventArgs e)
        {
            // タイトルをセットする ※現状、POSITIONが1でない場合、固定で0
            int juni = 0;
            if (this.PositionNo == 1)
            {
                if (this.rdoShojun.Checked)
                {
                    juni = 1;
                }
                else if (this.rdoKojun.Checked)
                {
                    juni = 2;
                }
            }
            this.SetTitle(juni);
            // 選択項目をセットする
            this.SetSentakuKomoku();
            // 集計区分をセットする
            this.SetShukeiKubun();

            // タイトルNoを保持
            this._dtPositionNo = (Decimal)this.nudTitleCd.Value;
        }

        /// <summary>
        /// 選択した項目の移動処理(左→右)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRMove_Click(object sender, System.EventArgs e)
        {
            int[] selectNum = new int[this.lbxShukeiKubun.SelectedIndices.Count];

            for (int i = 0; i < this.lbxShukeiKubun.SelectedIndices.Count; i++)
            {
                selectNum[i] = this.lbxShukeiKubun.SelectedIndices[i];
            }

            #region 選択タイトル値が2以上の場合
            if (this.PositionNo > 1)
            {
                // 選択した項目をセット
                string ShukeiKubunNm;
                string filter;
                DataRow[] dataRows;
                DataTable kubunList = new DataTable();
                kubunList.Columns.Add("num", Type.GetType("System.Int32"));
                kubunList.Columns.Add("cd", Type.GetType("System.Int32"));
                kubunList.Columns.Add("nm", Type.GetType("System.String"));
                kubunList.Columns.Add("shukeiKubun", Type.GetType("System.Int32"));
                kubunList.Columns.Add("henshuKubun", Type.GetType("System.Int32"));
                DataRow row;
                for (int i = 0; i < selectNum.Length; i++)
                {
                    // 集計区分名称を取得
                    ShukeiKubunNm = this.lbxShukeiKubun.Items[selectNum[i]].ToString();
                    // 集計区分情報を取得
                    filter = "position = " + this.PositionNo + " and nm = \'" + ShukeiKubunNm + "\'";
                    dataRows = this.ShukeiKubun.Select(filter);
                    row = kubunList.NewRow();
                    row["num"] = i;
                    row["cd"] = Util.ToInt(dataRows[0]["cd"]);
                    row["nm"] = Util.ToString(dataRows[0]["nm"]);
                    row["shukeiKubun"] = Util.ToInt(dataRows[0]["shukeiKubun"]);
                    row["henshuKubun"] = Util.ToInt(dataRows[0]["henshuKubun"]);
                    kubunList.Rows.Add(row);
                }

                // 選択項目の情報を取得
                DataRow[] dataRowsSentaku = this.SentakuKomoku.Select();

                // 選択した集計区分コードを取得
                DataRow[] dataRowsAll = kubunList.Select();
                // セリ集計では取り合えず外す
                //// 集計区分が2の集計区分が選択されている場合、選択項目が2件以上になる場合はエラーを表示する
                //filter = "shukeiKubun = 2";
                //dataRows = kubunList.Select(filter);
                //if (dataRows.Length >= 1 && (dataRowsSentaku.Length > 0 || dataRowsAll.Length > 1))
                //{
                //    Msg.Notice("選択できません。");
                //    return;
                //}

                // 集計区分が9が存在する場合、集計区分が9以外の選択項目が存在する場合はエラーを表示する
                filter = "shukeiKubun = 9";
                dataRows = kubunList.Select(filter);
                filter += " and position = " + this.PositionNo;
                dataRowsSentaku = this.SentakuKomoku.Select(filter);
                //filter = "shukeiKubun <> 9 and shukeiKubun <>0";
                filter = "shukeiKubun not in (0, 9)";
                DataRow[] dataRowsNot = kubunList.Select(filter);
                filter = "shukeiKubun <> 0 and position = " + this.PositionNo;
                DataRow[] dataRowsSentakuNot = this.SentakuKomoku.Select(filter);
                if ((dataRows.Length > 0 || dataRowsSentaku.Length > 0) && (dataRowsNot.Length > 0 || dataRowsSentakuNot.Length > 0))
                {
                    Msg.Notice("選択できません。");
                    return;
                }

                // 集計区分が9の集計区分が選択されている場合、編集区分が異なる選択項目が存在する場合はエラーを表示する
                ArrayList henshuKubunList = new ArrayList();
                foreach (DataRow dr in dataRowsSentaku)
                {
                    // 選択項目にセットする
                    henshuKubunList.Add(Util.ToString(dr["henshuKubun"]));
                }
                bool checkFlg;
                foreach (DataRow dr in dataRows)
                {
                    checkFlg = henshuKubunList.Contains(Util.ToInt(dr["henshuKubun"]));
                    if (!checkFlg && henshuKubunList.Count > 0)
                    {
                        Msg.Notice("選択できません。");
                        return;
                    }
                    // 選択項目にセットする
                    henshuKubunList.Add(Util.ToInt(dr["henshuKubun"]));
                }

                // 選択した項目の追加
                dataRows = kubunList.Select();
                foreach (DataRow dr in dataRows)
                {
                    this.lbxSentakuKomoku.Items.Add(this.lbxShukeiKubun.Items[selectNum[Util.ToInt(dr["num"])]]);

                    // 選択項目に情報をセット
                    row = SentakuKomoku.NewRow();
                    row["position"] = this.PositionNo;
                    row["cd"] = Util.ToInt(dr["cd"]);
                    row["nm"] = Util.ToString(dr["nm"]);
                    row["shukeiKubun"] = Util.ToInt(dr["shukeiKubun"]);
                    row["henshuKubun"] = Util.ToInt(dr["henshuKubun"]);
                    SentakuKomoku.Rows.Add(row);

                    // 集計区分の情報を取得
                    filter = "position = " + this.PositionNo + " and nm = \'" + Util.ToString(dr["nm"]) + "\' and cd = \'" + Util.ToString(dr["cd"]) + "\'";
                    DataRow[] dataRowsShukei = this.ShukeiKubun.Select(filter);

                    // 集計区分から削除する
                    foreach (DataRow drSK in dataRowsShukei)
                    {
                        drSK.Delete();
                    }
                }
            }
            #endregion
            #region 選択タイトル値が1の場合
            else
            {
                // 選択した項目の追加
                for (int i = 0; i < selectNum.Length; i++)
                {
                    this.lbxSentakuKomoku.Items.Add(this.lbxShukeiKubun.Items[selectNum[i]]);
                }
            }
            #endregion

            int l = 1;
            // 選択した項目の削除
            for (int i = 0; i < selectNum.Length; i++)
            {
                if (i == 0)
                {
                    this.lbxShukeiKubun.Items.Remove(this.lbxShukeiKubun.Items[selectNum[i]]);
                }
                else
                {
                    this.lbxShukeiKubun.Items.Remove(this.lbxShukeiKubun.Items[selectNum[i] - l]);
                    l++;
                }
            }
        }

        /// <summary>
        /// 選択した項目の移動処理(右→左)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLMove_Click(object sender, System.EventArgs e)
        {
            {
                int[] selectNum = new int[this.lbxSentakuKomoku.SelectedIndices.Count];
                for (int i = 0; i < this.lbxSentakuKomoku.SelectedIndices.Count; i++)
                {
                    selectNum[i] = this.lbxSentakuKomoku.SelectedIndices[i];
                }

                // 選択した項目の追加
                string shukeiKubunNm;
                string filter;
                DataRow[] dataRows;
                DataRow row;
                for (int i = 0; i < selectNum.Length; i++)
                {
                    this.lbxShukeiKubun.Items.Add(this.lbxSentakuKomoku.Items[selectNum[i]]);

                    if (this.PositionNo != 1)
                    {
                        // 集計区分名称を取得
                        shukeiKubunNm = this.lbxSentakuKomoku.Items[selectNum[i]].ToString();
                        // 選択項目の情報を取得
                        filter = "position = " + this.PositionNo + " and nm = \'" + shukeiKubunNm + "\'";
                        dataRows = this.SentakuKomoku.Select(filter);

                        // 集計区分に情報をセット
                        row = ShukeiKubun.NewRow();
                        row["position"] = this.PositionNo;
                        row["cd"] = Util.ToInt(dataRows[0]["cd"]);
                        row["nm"] = shukeiKubunNm;
                        row["shukeiKubun"] = Util.ToInt(dataRows[0]["shukeiKubun"]);
                        row["henshuKubun"] = Util.ToInt(dataRows[0]["henshuKubun"]);
                        this.ShukeiKubun.Rows.Add(row);

                        // 選択項目から削除する
                        foreach (DataRow dr in dataRows)
                        {
                            dr.Delete();
                        }
                    }
                }

                int l = 1;
                // 選択した項目の削除
                for (int i = 0; i < selectNum.Length; i++)
                {
                    if (i == 0)
                    {
                        this.lbxSentakuKomoku.Items.Remove(this.lbxSentakuKomoku.Items[selectNum[i]]);
                    }
                    else
                    {
                        this.lbxSentakuKomoku.Items.Remove(this.lbxSentakuKomoku.Items[selectNum[i] - l]);
                        l++;
                    }
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// タイトル名の入力チェック
        /// </summary>
        private bool IsValidTitleNm()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtTitleNm.Text, this.txtTitleNm.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 画面から取得したデータを格納(タイトル)
        /// </summary>
        private void SetTitle(int juni)
        {
            // タイトルをDBに反映する
            this.InsertTB_HN_SHUKEIHYO_SETTEI_SERI(juni);

            // タイトルをDataTableに登録する
            int num = (int)this.PositionNo - 1;
            this.Title.Rows[num]["TITLE"] = this.txtTitleNm.Text;
            this.Title.Rows[num]["SHUTSURYOKU_JUNI"] = juni;

            // タイトルを表示する
            String filter = "position = " + this.nudTitleCd.Value;
            DataRow[] dataRows = this.Title.Select(filter);
            String titleNm = Util.ToString(dataRows[0]["title"]);
            this.txtTitleNm.Text = titleNm;
            // 順位を表示する
            if (this.nudTitleCd.Value == 1)
            {
                this.rdoNashi.Enabled = false;
            }
            else
            {
                this.rdoNashi.Enabled = true;
            }

            if (dataRows[0]["shutsuryoku_juni"].ToString() == "1")
            {
                this.rdoShojun.Checked = true;
                this.rdoKojun.Checked = false;
            }
            else if (dataRows[0]["shutsuryoku_juni"].ToString() == "2")
            {
                this.rdoShojun.Checked = false;
                this.rdoKojun.Checked = true;
            }
            else
            {
                this.rdoNashi.Checked = true;
            }
        }
        
        /// <summary>
        /// 画面から取得したデータを格納(集計区分)
        /// </summary>
        private void SetShukeiKubun()
        {
            // 集計区分を表示する
            this.lbxShukeiKubun.Items.Clear();
            string filter = "position = " + (Decimal)this.nudTitleCd.Value;
            DataRow[] dataRows = this.ShukeiKubun.Select(filter);
            foreach (DataRow dr in dataRows)
            {
                // 選択項目にセットする
                this.lbxShukeiKubun.Items.Add(Util.ToString(dr["nm"]));
            }
        }

        /// <summary>
        /// 画面から取得したデータを格納(選択項目)
        /// </summary>
        private void SetSentakuKomoku()
        {
            // 選択項目をDBに反映する
            this.InsertTB_HN_SHUKEIHYO_SETTEI_MEISAI_SERI();

            // 選択項目を表示する
            this.lbxSentakuKomoku.Items.Clear();
            string filter = "position = " + (Decimal)this.nudTitleCd.Value;
            DataRow[] dataRows = this.SentakuKomoku.Select(filter);
            foreach (DataRow dr in dataRows)
            {
                // 選択項目にセットする
                if (Util.ToInt(dr["cd"]) != 0)
                {
                    this.lbxSentakuKomoku.Items.Add(Util.ToString(dr["nm"]));
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(タイトル)
        /// 画面に反映(タイトル)
        /// </summary>
        private void DispDataSetTitle()
        {
            // 対象データテーブルにカラムを2列ずつ定義
            this.Title.Columns.Add("position", Type.GetType("System.Int32"));
            this.Title.Columns.Add("title", Type.GetType("System.String"));
            this.Title.Columns.Add("shutsuryoku_juni", Type.GetType("System.Int32"));

            DataTable dtResult;
            DataRow row;
            string title = "";
            string shutsuryoku_juni = "0";
            string juni = "0";
            string titleNm = "";

            // タイトルをデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= POSITION_END_NO; cnt++)
            {
                dtResult = this.GetTB_HN_SHUKEIHYO_SETTEI_SERI(cnt);
                if (dtResult.Rows.Count > 0)
                {
                    title = dtResult.Rows[0]["TITLE"].ToString();
                    shutsuryoku_juni = dtResult.Rows[0]["SHUTSURYOKU_JUNI"].ToString();
                    // 1件目のタイトルをセット
                    if (cnt == POSITION_START_NO)
                    {
                        titleNm = dtResult.Rows[0]["TITLE"].ToString();
                        juni = dtResult.Rows[0]["SHUTSURYOKU_JUNI"].ToString();
                    }
                }

                row = this.Title.NewRow();
                row["position"] = cnt;
                row["title"] = title;
                row["shutsuryoku_juni"] = shutsuryoku_juni;
                this.Title.Rows.Add(row);
            }

            // タイトルをセット
            this.txtTitleNm.Text = titleNm;
            _dtPositionNo = (Decimal)POSITION_START_NO;
            this.rdoNashi.Enabled = false;
            this.rdoNashi.Checked = false;
            if (juni == "1")
            {
                this.rdoShojun.Checked = true;
                this.rdoKojun.Checked = false;
            }
            else if (juni == "2")
            {
                this.rdoShojun.Checked = false;
                this.rdoKojun.Checked = true;
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(集計区分)
        /// 画面に反映(集計区分)
        /// </summary>
        private void DispDataSetShukeiKubun()
        {
            // 対象データテーブルにカラムを4列ずつ定義
            this.ShukeiKubun.Columns.Add("position", Type.GetType("System.Int32"));
            this.ShukeiKubun.Columns.Add("cd", Type.GetType("System.Int32"));
            this.ShukeiKubun.Columns.Add("nm", Type.GetType("System.String"));
            this.ShukeiKubun.Columns.Add("shukeiKubun", Type.GetType("System.Int32"));
            this.ShukeiKubun.Columns.Add("henshuKubun", Type.GetType("System.Int32"));

            DataTable dtResult;
            DataRow row;

            // 項目名をデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= POSITION_END_NO; cnt++)
            {
                dtResult = this.GetTB_HN_SHUKEIHYO_KOMOKU_SERI(cnt, true);
                foreach (DataRow dr in dtResult.Rows)
                {
                    row = this.ShukeiKubun.NewRow();
                    row["position"] = cnt;
                    row["cd"] = Util.ToInt(dr["KOMOKU_CD"]);
                    row["nm"] = Util.ToString(dr["KOMOKU_NM"]);
                    row["shukeiKubun"] = Util.ToInt(dr["SHUKEI_KUBUN"]);
                    row["henshuKubun"] = Util.ToInt(dr["HENSHU_KUBUN"]);
                    this.ShukeiKubun.Rows.Add(row);

                    // 1件目のデータをセット
                    if (cnt == POSITION_START_NO)
                    {
                        // 集計区分にセットする
                        this.lbxShukeiKubun.Items.Add(Util.ToString(dr["KOMOKU_NM"]));
                    }
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(選択項目)
        /// 画面に反映(選択項目)
        /// </summary>
        private void DispDataSetSentakuKomoku()
        {
            // 対象データテーブルにカラムを4列ずつ定義
            this.SentakuKomoku.Columns.Add("position", Type.GetType("System.Int32"));
            this.SentakuKomoku.Columns.Add("cd", Type.GetType("System.Int32"));
            this.SentakuKomoku.Columns.Add("nm", Type.GetType("System.String"));
            this.SentakuKomoku.Columns.Add("shukeiKubun", Type.GetType("System.Int32"));
            this.SentakuKomoku.Columns.Add("henshuKubun", Type.GetType("System.Int32"));

            DataTable dtResult;
            DataRow row;

            // 項目名をデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= POSITION_END_NO; cnt++)
            {
                dtResult = this.GetVI_HN_SHUKEIHYO_SETTEI_MEISAI_SERI(cnt);
                foreach (DataRow dr in dtResult.Rows)
                {
                    row = this.SentakuKomoku.NewRow();
                    row["position"] = cnt;
                    row["cd"] = Util.ToInt(dr["KOMOKU_CD"]);
                    row["nm"] = Util.ToString(dr["KOMOKU_NM"]);
                    row["shukeiKubun"] = Util.ToInt(dr["SHUKEI_KUBUN"]);
                    row["henshuKubun"] = Util.ToInt(dr["HENSHU_KUBUN"]);
                    this.SentakuKomoku.Rows.Add(row);

                    // 1件目のデータをセット
                    if (cnt == POSITION_START_NO)
                    {
                        // 選択項目にセットする
                        if (Util.ToInt(dr["KOMOKU_CD"]) != 0)
                        {
                            this.lbxSentakuKomoku.Items.Add(Util.ToString(dr["KOMOKU_NM"]));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// DBから取得したデータを格納(選択項目候補)
        /// </summary>
        private void DispDataSetSentakuKoho()
        {
            // 対象データテーブルにカラムを3列ずつ定義
            this.SentakuKoho.Columns.Add("position", Type.GetType("System.Int32"));
            this.SentakuKoho.Columns.Add("cd", Type.GetType("System.Int32"));
            this.SentakuKoho.Columns.Add("nm", Type.GetType("System.String"));
            this.SentakuKoho.Columns.Add("shukeiKubun", Type.GetType("System.Int32"));
            this.SentakuKoho.Columns.Add("henshuKubun", Type.GetType("System.Int32"));

            DataTable dtResult;
            DataRow row;

            // 項目名をデータテーブルに格納
            for (int cnt = POSITION_START_NO; cnt <= POSITION_END_NO; cnt++)
            {
                dtResult = this.GetTB_HN_SHUKEIHYO_KOMOKU_SERI(cnt, false);
                foreach (DataRow dr in dtResult.Rows)
                {
                    row = this.SentakuKoho.NewRow();
                    row["position"] = cnt;
                    row["cd"] = Util.ToInt(dr["KOMOKU_CD"]);
                    row["nm"] = Util.ToString(dr["KOMOKU_NM"]);
                    row["shukeiKubun"] = Util.ToInt(dr["SHUKEI_KUBUN"]);
                    row["henshuKubun"] = Util.ToInt(dr["HENSHU_KUBUN"]);
                    this.SentakuKoho.Rows.Add(row);
                }
            }
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetTB_HN_SHUKEIHYO_SETTEI_SERI(int cnt)
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 6, this._shishoCode);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this._pSHUKEIHYO);
            dpc.SetParam("@POSITION", SqlDbType.Decimal, 2, cnt);

            Sql.Append("SELECT");
            Sql.Append(" POSITION,");
            Sql.Append(" TITLE,");
            Sql.Append(" SHUTSURYOKU_JUNI ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SHUKEIHYO_SETTEI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND");
            Sql.Append(" SETTEI_CD = @SETTEI_CD AND");
            Sql.Append(" POSITION = @POSITION ");
            Sql.Append("ORDER BY");
            Sql.Append(" POSITION");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetVI_HN_SHUKEIHYO_SETTEI_MEISAI_SERI(int cnt)
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this._pSHUKEIHYO);
            dpc.SetParam("@POSITION", SqlDbType.Decimal, 2, cnt);

            Sql.Append("SELECT");
            Sql.Append(" KOMOKU_CD,");
            Sql.Append(" KOMOKU_NM,");
            Sql.Append(" SHUKEI_KUBUN,");
            Sql.Append(" HENSHU_KUBUN ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHUKEIHYO_SETTEI_MEISAI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND");
            Sql.Append(" SETTEI_CD = @SETTEI_CD AND");
            Sql.Append(" POSITION = @POSITION ");
            Sql.Append("ORDER BY");
            Sql.Append(" RENBAN");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable GetTB_HN_SHUKEIHYO_KOMOKU_SERI(int cnt, bool flg)
        {
            String notCd = "";

            // 集計区分表示用のデータを取得する場合
            if (flg)
            {
                String filter = "position = " + cnt;
                DataRow[] dataRowsSentakuKomoku = this.SentakuKomoku.Select(filter);
                ArrayList cdList = new ArrayList();
                foreach (DataRow dr in dataRowsSentakuKomoku)
                {
                    // 選択項目にセットする
                    cdList.Add(Util.ToString(dr.ItemArray[1].ToString()));
                }
                string[] cds = (string[])cdList.ToArray(typeof(string));
                notCd = String.Join(",", cds);
            }

            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUKEI_KUBUN", SqlDbType.Decimal, 1, 0);

            Sql.Append("SELECT");
            Sql.Append(" KOMOKU_CD,");
            Sql.Append(" KOMOKU_NM,");
            Sql.Append(" SHUKEI_KUBUN,");
            Sql.Append(" HENSHU_KUBUN ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SHUKEIHYO_KOMOKU_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            if (cnt == 1)
            {
                Sql.Append(" SHUKEI_KUBUN = @SHUKEI_KUBUN ");
            }
            else
            {
                Sql.Append(" SHUKEI_KUBUN != @SHUKEI_KUBUN ");
            }
            if (notCd != "")
            {
                Sql.Append(" AND KOMOKU_CD NOT IN (" + notCd + ") ");
            }

            // 
            Sql.Append(" AND SHUKEI_KUBUN <> 9 ");
            Sql.Append(" AND KOMOKU_NM NOT IN ( 'セリ年月', 'セリ日' ) ");

            Sql.Append("ORDER BY");
            Sql.Append(" KOMOKU_CD");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// データを登録(タイトル)
        /// </summary>
        private void InsertTB_HN_SHUKEIHYO_SETTEI_SERI(int juni)
        {
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // TB_集計表設定削除
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
                whereParam.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
                whereParam.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this._pSHUKEIHYO);
                whereParam.SetParam("@POSITION", SqlDbType.Decimal, 2, this.PositionNo);
                this.Dba.Delete("TB_HN_SHUKEIHYO_SETTEI_SERI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND DENPYO_KUBUN = @DENPYO_KUBUN AND SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND SETTEI_CD = @SETTEI_CD AND POSITION = @POSITION",
                    whereParam);

                // TB_集計表設定登録
                DbParamCollection updParam = new DbParamCollection();
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
                updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
                updParam.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
                updParam.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this._pSHUKEIHYO);
                updParam.SetParam("@POSITION", SqlDbType.Decimal, 2, this.PositionNo);
                updParam.SetParam("@TITLE", SqlDbType.VarChar, 20, this.txtTitleNm.Text);
                updParam.SetParam("@SHUTSURYOKU_JUNI", SqlDbType.Decimal, 1, juni);

                this.Dba.Insert("TB_HN_SHUKEIHYO_SETTEI_SERI", updParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// データを登録(選択項目)
        /// </summary>
        private void InsertTB_HN_SHUKEIHYO_SETTEI_MEISAI_SERI()
        {
            // 削除
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // TB_集計表設定明細削除
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
                whereParam.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
                whereParam.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this._pSHUKEIHYO);
                whereParam.SetParam("@POSITION", SqlDbType.Decimal, 2, this.PositionNo);
                this.Dba.Delete("TB_HN_SHUKEIHYO_SETTEI_MEISAI_SERI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND DENPYO_KUBUN = @DENPYO_KUBUN AND SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND SETTEI_CD = @SETTEI_CD AND POSITION = @POSITION",
                    whereParam);

                // TB_集計表設定明細登録
                DbParamCollection updParam;
                String SentakuKomokuNm;
                int titleCd = 0;

                for (int cnt = 0; cnt < this.lbxSentakuKomoku.Items.Count; cnt++)
                {
                    SentakuKomokuNm = this.lbxSentakuKomoku.Items[cnt].ToString();

                    String filter = "nm = \'" + SentakuKomokuNm + "\'";
                    DataRow[] dataRowsTitle = SentakuKoho.Select(filter);
                    if (dataRowsTitle.Length > 0)
                    {
                        titleCd = Util.ToInt(dataRowsTitle[0]["cd"]);
                    }

                    updParam = new DbParamCollection();

                    updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
                    updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
                    updParam.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
                    updParam.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this._pSHUKEIHYO);
                    updParam.SetParam("@POSITION", SqlDbType.Decimal, 2, this.PositionNo);
                    updParam.SetParam("@RENBAN", SqlDbType.Decimal, 2, cnt+1);
                    updParam.SetParam("@KOMOKU_CD", SqlDbType.Decimal, 2, titleCd);

                    this.Dba.Insert("TB_HN_SHUKEIHYO_SETTEI_MEISAI_SERI", updParam);
                }
                // 選択項目が0件の場合、項目コードを0として1件登録する
                if (this.lbxSentakuKomoku.Items.Count == 0)
                {
                    updParam = new DbParamCollection();

                    updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
                    updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
                    updParam.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
                    updParam.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this._pSHUKEIHYO);
                    updParam.SetParam("@POSITION", SqlDbType.Decimal, 2, PositionNo);
                    updParam.SetParam("@RENBAN", SqlDbType.Decimal, 2, 1);
                    updParam.SetParam("@KOMOKU_CD", SqlDbType.Decimal, 2, 0);

                    this.Dba.Insert("TB_HN_SHUKEIHYO_SETTEI_MEISAI_SERI", updParam);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// ゴミの削除
        /// </summary>
        private void Delete_Trush()
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this._pSHUKEIHYO);

            Sql.Append("SELECT");
            Sql.Append(" KOMOKU_CD,");
            Sql.Append(" POSITION ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHUKEIHYO_SETTEI_MEISAI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND");
            Sql.Append(" SETTEI_CD = @SETTEI_CD ");
            Sql.Append("ORDER BY");
            Sql.Append(" POSITION DESC, RENBAN ASC");

            try
            {

                DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                // トランザクション開始
                this.Dba.BeginTransaction();

                foreach (DataRow r in dt.Rows)
                {
                    if (Util.ToInt(r["KOMOKU_CD"]) == 0)
                    {
                        DbParamCollection whereParam = new DbParamCollection();
                        whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this._shishoCode);
                        whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
                        whereParam.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
                        whereParam.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this._pSHUKEIHYO);
                        whereParam.SetParam("@POSITION", SqlDbType.Decimal, 2, Util.ToInt(r["POSITION"]));

                        this.Dba.Delete("TB_HN_SHUKEIHYO_SETTEI_SERI",
                            "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND DENPYO_KUBUN = @DENPYO_KUBUN AND SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND SETTEI_CD = @SETTEI_CD AND POSITION = @POSITION",
                            whereParam);

                        this.Dba.Delete("TB_HN_SHUKEIHYO_SETTEI_MEISAI_SERI",
                            "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND DENPYO_KUBUN = @DENPYO_KUBUN AND SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND SETTEI_CD = @SETTEI_CD AND POSITION = @POSITION",
                            whereParam);
                    }
                    else
                        break;
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }
        #endregion
    }
}
