﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.hn.hnmr1141
{
    /// <summary>
    /// セリ推移表(HNMR1142)
    /// </summary>
    public partial class HNMR1142 : BasePgForm
    {
        #region 変数
        private int _shishoCode = 0;                    // 支所コード
        #endregion

        #region プロパティ
        public int ShishoCode
        {
            //get
            //{
            //    return this._shishoCode;
            //}
            set
            {
                this._shishoCode = value;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1142()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // タイトルの表示非表示を設定
            this.lblTitle.Visible = false;
            // ボタンの表示非表示を設定
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = true;
            this.btnF12.Visible = false;
            // ボタンの活性非活性を設定
            this.btnF1.Enabled = false;
            this.btnF2.Enabled = false;
            this.btnF3.Enabled = false;
            this.btnF4.Enabled = false;
            this.btnF5.Enabled = false;
            this.btnF6.Enabled = false;
            this.btnF7.Enabled = false;
            this.btnF8.Enabled = false;
            this.btnF9.Enabled = false;
            this.btnF10.Enabled = false;
            this.btnF11.Enabled = true;
            this.btnF12.Enabled = false;

            this.btnF11.Location = this.btnF1.Location;

            // データ取得のSQLを発行してGridに反映
            DataTable dtList = new DataTable();
            try
            {
                dtList = this.GetTB_HN_SHUKEIHYO_SETTEI_SERI();
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                return;
            }

            // 取得したデータを表示用に編集
            DataTable dtDsp = this.EditDataList(dtList);

            this.dgvList.DataSource = dtDsp;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 80;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 168;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                default:
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            // 推移表の設定画面
            using (HNMR1143 frm = new HNMR1143())
            {
                frm.ShishoCode = this._shishoCode;
                frm.ShowDialog(this);

                if (frm.DialogResult == DialogResult.OK)
                {
                    // データ取得のSQLを発行してGridに反映
                    DataTable dtList = new DataTable();
                    try
                    {
                        dtList = this.GetTB_HN_SHUKEIHYO_SETTEI_SERI();
                    }
                    catch (Exception e)
                    {
                        Msg.Error(e.Message);
                        return;
                    }

                    // 取得したデータを表示用に編集
                    DataTable dtDsp = this.EditDataList(dtList);

                    this.dgvList.DataSource = dtDsp;

                    // ユーザーによるソートを禁止させる
                    foreach (DataGridViewColumn c in this.dgvList.Columns)
                        c.SortMode = DataGridViewColumnSortMode.NotSortable;

                    // フォントを設定する
                    this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
                    this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

                    // 列幅を設定する
                    this.dgvList.Columns[0].Width = 80;
                    this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    this.dgvList.Columns[1].Width = 168;
                    this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
                else
                {
                    if (this.dgvList.RowCount == 0)
                    {
                        this.DialogResult = DialogResult.Cancel;
                        this.Close();
                    }
                }
            }

        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.ReturnVal();
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            this.ReturnVal();
        }

        /// <summary>
        /// フォーム表示後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HNMR1142_Shown(object sender, EventArgs e)
        {
            if (this.dgvList.RowCount == 0)
            {
                if (Msg.ConfNmYesNo("セリ推移表", "該当データがありません、登録しますか？") == DialogResult.Yes)
                {
                    this.PressF11();
                }
                else
                {
                    this.DialogResult = DialogResult.Cancel;
                    this.Close();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 売上集計表のデータを取得
        /// </summary>
        /// <returns>売上集計表の取得したデータ</returns>
        private DataTable GetTB_HN_SHUKEIHYO_SETTEI_SERI()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" SETTEI_CD,");
            sql.Append(" TITLE ");
            sql.Append("FROM");
            sql.Append(" TB_HN_SHUKEIHYO_SETTEI_SERI ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHISHO_CD = @SHISHO_CD");
            sql.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
            sql.Append(" AND SHUBETSU_KUBUN  = @SHUBETSU_KUBUN");
            sql.Append(" AND POSITION = @POSITION ");
            sql.Append("ORDER BY");
            sql.Append(" SETTEI_CD ASC ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 6, this._shishoCode);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@POSITION", SqlDbType.Decimal, 2, 0);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 列の定義を作成
            dtResult.Columns.Add("コード", typeof(string));
            dtResult.Columns.Add("名　称", typeof(string));

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();

                drResult["コード"] = dtData.Rows[i]["SETTEI_CD"];
                drResult["名　称"] = dtData.Rows[i]["TITLE"];

                dtResult.Rows.Add(drResult);
            }
            return dtResult;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[2] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["名　称"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
