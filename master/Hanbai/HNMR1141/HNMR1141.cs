﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

using ClosedXML.Excel;

namespace jp.co.fsi.hn.hnmr1141
{
    /// <summary>
    /// セリ推移表(HNMR1141)
    /// </summary>
    public partial class HNMR1141 : BasePgForm
    {
        #region 構造体
        /// <summary>
        /// 合計情報
        /// </summary>
        private struct Summary
        {
            public decimal Komoku01;
            public decimal Komoku02;
            public decimal Komoku03;
            public decimal Komoku04;
            public decimal Komoku05;
            public decimal Komoku06;
            public decimal Komoku07;

            /// <summary>
            /// 金額をクリア
            /// </summary>
            public void Clear()
            {
                Komoku01 = 0;
                Komoku02 = 0;
                Komoku03 = 0;
                Komoku04 = 0;
                Komoku05 = 0;
                Komoku06 = 0;
                Komoku07 = 0;
            }
        }
        #endregion

        #region 定数
        /// <summary>
        /// データ取得用
        /// </summary>
        private const int POSITION_START_NO = 1;
        private const int POSITION_END_NO = 8;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        /// <summary>
        /// カラム名を格納する用データテーブル
        /// </summary>
        private Hashtable _dtKomokuCaramNm = new Hashtable();
        public Hashtable KomokuCaramNm
        {
            get
            {
                return this._dtKomokuCaramNm;
            }
        }

        /// <summary>
        /// タイトルを格納する用データテーブル
        /// </summary>
        private DataTable _dtTitle = new DataTable();
        public DataTable Title
        {
            get
            {
                return this._dtTitle;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1141()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }

            // 設定より初期精算区分の表示（見つからない場合は３を設定）
            try
            {
                int seisanKubun = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Han, "HNMR1141", "Setting", "SeisanKubun"));
                this.txtSeisanKubun.Text = seisanKubun.ToString();
                this.lblSeisanKubun.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", Util.ToString(txtMizuageShishoCd.Text), this.txtSeisanKubun.Text);
            }
            catch (Exception)
            {
                this.txtSeisanKubun.Text = "3";
                this.lblSeisanKubun.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", Util.ToString(txtMizuageShishoCd.Text), this.txtSeisanKubun.Text);
            }

            /* 日付範囲を設定する */
            // 現在の年号を取得する
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            //今月の最初の日を取得する
            DateTime dtMonthFr = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            string[] jpMontFr = Util.ConvJpDate(dtMonthFr, this.Dba);
            // 日付範囲(自)
            this.lblGengoFr.Text = Util.ToString(jpMontFr[0]);
            this.txtYearFr.Text = jpMontFr[2];
            this.txtMonthFr.Text = jpMontFr[3];
            this.txtDayFr.Text = jpMontFr[4];
            // 日付範囲(至)
            this.lblGengoTo.Text = Util.ToString(jpDate[0]);
            this.txtYearTo.Text = jpDate[2];
            this.txtMonthTo.Text = jpDate[3];
            this.txtDayTo.Text = jpDate[4];

            // 集計表
            this.txtShukeihyo.Text = "0";

            // Enter処理を無効化
            this._dtFlg = false;

            // 集計表にフォーカス
            this.txtShukeihyo.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtShukeihyo":
                case "txtYearFr":
                case "txtYearTo":
                case "txtSeisanKubun":
                case "txtSenshuCdFr":
                case "txtSenshuCdTo":
                case "txtGyohoCdFr":
                case "txtGyohoCdTo":
                case "txtChikuCdFr":
                case "txtChikuCdTo":
                case "txtGyoshuBunruiCdFr":
                case "txtGyoshuBunruiCdTo":
                case "txtGyoshuCdFr":
                case "txtGyoshuCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShukeihyo":
                    #region 集計票検索
                    // セリ集計選択画面
                    using (HNMR1142 frm = new HNMR1142())
                    {
                        // 支所コードはゼロで統一
                        //frm.ShishoCode = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));
                        frm.ShowDialog(this);

                        if (frm.DialogResult == DialogResult.OK)
                        {
                            string[] result = (string[])frm.OutData;

                            this.txtShukeihyo.Text = result[0];
                            this.lblShukeihyoNm.Text = result[1];
                        }
                    }
                    #endregion
                    break;

                case "txtYearFr":
                    #region 元号検索
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpFr();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtYearTo":
                    #region 元号検索
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpTo();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtSeisanKubun":
                    #region 精算区分検索
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.InData = this.txtSeisanKubun.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (String[])frm.OutData;
                                this.txtSeisanKubun.Text = outData[0];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtSenshuCdFr":
                case "txtSenshuCdTo":
                    #region 船主
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtSenshuCdFr")
                                {
                                    this.txtSenshuCdFr.Text = outData[0];
                                    this.lblSenshuCdFr.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtSenshuCdTo")
                                {
                                    this.txtSenshuCdTo.Text = outData[0];
                                    this.lblSenshuCdTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtGyohoCdFr":
                case "txtGyohoCdTo":
                    #region 漁法
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1031.HNCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtGyohoCdFr")
                                {
                                    this.txtGyohoCdFr.Text = outData[0];
                                    this.lblGyohoCdFr.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtGyohoCdTo")
                                {
                                    this.txtGyohoCdTo.Text = outData[0];
                                    this.lblGyohoCdTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtChikuCdFr":
                case "txtChikuCdTo":
                    #region 地区
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1021.HNCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtChikuCdFr")
                                {
                                    this.txtChikuCdFr.Text = outData[0];
                                    this.lblChikuCdFr.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtChikuCdTo")
                                {
                                    this.txtChikuCdTo.Text = outData[0];
                                    this.lblChikuCdTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtGyoshuBunruiCdFr":
                case "txtGyoshuBunruiCdTo":
                    #region 魚種分類
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1041.HNCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtGyoshuBunruiCdFr")
                                {
                                    this.txtGyoshuBunruiCdFr.Text = outData[0];
                                    this.lblGyoshuBunruiCdFr.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtGyoshuBunruiCdTo")
                                {
                                    this.txtGyoshuBunruiCdTo.Text = outData[0];
                                    this.lblGyoshuBunruiCdTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtGyoshuCdFr":
                case "txtGyoshuCdTo":
                    #region 魚種
                    if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
                    {
                        Msg.Notice("支所を指定して下さい。");
                        this.txtMizuageShishoCd.Focus();
                        this.txtMizuageShishoCd.SelectAll();
                        return;
                    }

                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1051.HNCM1051");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.Par2 = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtGyoshuCdFr")
                                {
                                    this.txtGyoshuBunruiCdFr.Text = outData[0];
                                    this.lblGyoshuBunruiCdFr.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtGyoshuCdTo")
                                {
                                    this.txtGyoshuBunruiCdTo.Text = outData[0];
                                    this.lblGyoshuBunruiCdTo.Text = outData[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }

        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // EXCEL出力処理
                DoExcelPut();
            }

        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return;
            }

            // 支所がゼロの場合は設定不可
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                Msg.Notice("支所を指定して下さい。");
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return;
            }

            if (!IsValidShukeihyo())
            {
                this.txtShukeihyo.SelectAll();
                return;
            }

            // 項目設定画面
            using (HNMR1144 frm = new HNMR1144(this.txtShukeihyo.Text))
            {
                // 支所コードはゼロで統一
                //frm.ShishoCode = Util.ToInt(this.txtMizuageShishoCd.Text);
                frm.ShowDialog(this);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            using (PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNMR11411R" }))
            {
                psForm.ShowDialog();
            }
        }
        #endregion

        #region イベント

        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 集計表の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShukeihyo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShukeihyo())
            {
                e.Cancel = true;
                this.txtShukeihyo.SelectAll();
            }
        }

        /// <summary>
        /// 期間(年)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtYearFr.SelectAll();
            }
            else
            {
                this.txtYearFr.Text = Util.ToString(IsValid.SetYear(this.txtYearFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 期間(月)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthFr.SelectAll();
            }
            else
            {
                this.txtMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 期間(日)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayFr.SelectAll();
            }
            else
            {
                this.txtDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDayFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 期間(年)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtYearTo.SelectAll();
            }
            else
            {
                this.txtYearTo.Text = Util.ToString(IsValid.SetYear(this.txtYearTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 期間(月)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthTo.SelectAll();
            }
            else
            {
                this.txtMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 期間(日)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayTo.SelectAll();
            }
            else
            {
                this.txtDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDayTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSetSeisanKubun())
            {
                e.Cancel = true;
                this.txtSeisanKubun.SelectAll();
            }
        }

        /// <summary>
        /// 船主(自)の入力チェック
        /// </summary>
        private void txtSenshuCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSenshuCdFr())
            {
                e.Cancel = true;
                this.txtSenshuCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主(至)の入力チェック
        /// </summary>
        private void txtSenshuCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSenshuCdTo())
            {
                e.Cancel = true;
                this.txtSenshuCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 漁法(自)の入力チェック
        /// </summary>
        private void txtGyohoCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyohoCdFr())
            {
                e.Cancel = true;
                this.txtGyohoCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 漁法(自)の入力チェック
        /// </summary>
        private void txtGyohoCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyohoCdTo())
            {
                e.Cancel = true;
                this.txtGyohoCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 地区(自)の入力チェック
        /// </summary>
        private void txtChikuCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidChikuCdFr())
            {
                e.Cancel = true;
                this.txtChikuCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 地区(至)の入力チェック
        /// </summary>
        private void txtChikuCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidChikuCdTo())
            {
                e.Cancel = true;
                this.txtChikuCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 魚種分類(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuBunruiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshuBunruiCdFr())
            {
                e.Cancel = true;
                this.txtGyoshuBunruiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 魚種分類(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuBunruiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshuBunruiCdTo())
            {
                e.Cancel = true;
                this.txtGyoshuBunruiCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 魚種(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshuCdFr())
            {
                e.Cancel = true;
                this.txtGyoshuCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 魚種(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshuCdTo())
            {
                e.Cancel = true;
                this.txtGyoshuCdTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 魚種(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtGyoshuCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 集計表の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidShukeihyo()
        {
            // 空の場合、エラーメッセージを表示し、フォーカスを移動しない
            if (ValChk.IsEmpty(this.txtShukeihyo.Text))
            {
                this.lblShukeihyoNm.Text = "";
                Msg.Notice("数値を入力してください。");
                return false;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtShukeihyo.Text))
            {
                this.lblShukeihyoNm.Text = "";
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // ゼロの場合は許可（但し出力時の全チェック時はエラーとする）
            else if (this.txtShukeihyo.Text == "0")
            {
                return true;
            }
            // 名称を表示
            else
            {
                DataTable dtResult = this.GetShukeihyoSetteiData();

                if (dtResult.Rows.Count == 0)
                {
                    this.lblShukeihyoNm.Text = "";
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblShukeihyoNm.Text = dtResult.Rows[0]["Title"].ToString();
                }
            }

            return true;
        }


        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayFr.Text) > lastDayInMonth)
            {
                this.txtDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayTo.Text) > lastDayInMonth)
            {
                this.txtDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba));
        }
        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidYearFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtYearFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtYearFr.Text))
            {
                this.txtYearFr.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba));

            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSetSeisanKubun()
        {
            // 数値のみ入力を許可
            if (!ValChk.IsNumber(this.txtSeisanKubun.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            else if (ValChk.IsEmpty(this.txtSeisanKubun.Text) || this.txtSeisanKubun.Text == "0")
            {
                this.txtSeisanKubun.Text = "0";
                this.lblSeisanKubun.Text = "全て";
            }
            //マスタを見てのチェックへ
            else
            {
                // 1～3のみ入力を許可
                if (!ValChk.IsEmpty(this.txtSeisanKubun.Text))
                {
                    //int intval = Util.ToInt(this.txtSeisanKubun.Text);
                    //if (intval == 0 || intval > 3)
                    //{
                    //    Msg.Error("入力に誤りがあります。");
                    //    return false;
                    //}
                    // コードを元に名称を取得する
                    // 取得された場合、名称をラベルに反映する
                    this.lblSeisanKubun.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", Util.ToString(txtMizuageShishoCd.Text), this.txtSeisanKubun.Text);
                    if (ValChk.IsEmpty(this.lblSeisanKubun.Text))
                    {
                        Msg.Error("入力に誤りがあります。");
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 船主(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidSenshuCdFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtSenshuCdFr.Text))
            {
                this.lblSenshuCdFr.Text = "先　頭";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtSenshuCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 名称を表示
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtSenshuCdFr.Text);
                dpc.SetParam("@TORIHIKISAKI_KUBUN1", SqlDbType.Decimal, 4, 1);
                DataTable dtTB_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                    "TORIHIKISAKI_NM",
                    "VI_HN_TORIHIKISAKI_JOHO",
                    "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN1 = @TORIHIKISAKI_KUBUN1",
                    dpc);
                if (dtTB_HN_TORIHIKISAKI_JOHO.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblSenshuCdFr.Text = dtTB_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"].ToString();
                }
            }

            return true;
        }

        /// <summary>
        /// 船主(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidSenshuCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtSenshuCdTo.Text))
            {
                this.lblSenshuCdTo.Text = "最　後";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtSenshuCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 名称を表示
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtSenshuCdTo.Text);
                dpc.SetParam("@TORIHIKISAKI_KUBUN1", SqlDbType.Decimal, 4, 1);
                DataTable dtTB_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                    "TORIHIKISAKI_NM",
                    "VI_HN_TORIHIKISAKI_JOHO",
                    "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN1 = @TORIHIKISAKI_KUBUN1",
                    dpc);
                if (dtTB_HN_TORIHIKISAKI_JOHO.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblSenshuCdTo.Text = dtTB_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"].ToString();
                }
            }

            return true;
        }

        /// <summary>
        /// 漁法(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidGyohoCdFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtGyohoCdFr.Text))
            {
                this.lblGyohoCdFr.Text = "先　頭";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtGyohoCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 名称を表示
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@GYOHO_CD", SqlDbType.Decimal, 6, this.txtGyohoCdFr.Text);
                DataTable dtTB_HN_GYOHO_MST = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "TB_HN_GYOHO_MST",
                    "KAISHA_CD = @KAISHA_CD AND GYOHO_CD = @GYOHO_CD",
                    dpc);
                if (dtTB_HN_GYOHO_MST.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblGyohoCdFr.Text = dtTB_HN_GYOHO_MST.Rows[0]["GYOHO_NM"].ToString();
                }
            }

            return true;
        }

        /// <summary>
        /// 漁法(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidGyohoCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtGyohoCdTo.Text))
            {
                this.lblGyohoCdTo.Text = "最　後";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtGyohoCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 名称を表示
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@GYOHO_CD", SqlDbType.Decimal, 6, this.txtGyohoCdTo.Text);
                DataTable dtTB_HN_GYOHO_MST = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "TB_HN_GYOHO_MST",
                    "KAISHA_CD = @KAISHA_CD AND GYOHO_CD = @GYOHO_CD",
                    dpc);
                if (dtTB_HN_GYOHO_MST.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblGyohoCdTo.Text = dtTB_HN_GYOHO_MST.Rows[0]["GYOHO_NM"].ToString();
                }
            }

            return true;
        }

        /// <summary>
        /// 地区(自)の値チェック処理
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidChikuCdFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtChikuCdFr.Text))
            {
                this.lblChikuCdFr.Text = "先　頭";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtChikuCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                this.lblChikuCdFr.Text = this.Dba.GetName(this.UInfo, "TB_HN_CHIKU_MST", Util.ToString(txtMizuageShishoCd.Text), this.txtChikuCdFr.Text);
                if (ValChk.IsEmpty(this.lblChikuCdFr.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 地区(至)の値チェック処理
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidChikuCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtChikuCdTo.Text))
            {
                this.lblChikuCdTo.Text = "最　後";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtChikuCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                this.lblChikuCdTo.Text = this.Dba.GetName(this.UInfo, "TB_HN_CHIKU_MST", Util.ToString(txtMizuageShishoCd.Text), this.txtChikuCdTo.Text);
                if (ValChk.IsEmpty(this.lblChikuCdTo.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 魚種分類(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidGyoshuBunruiCdFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtGyoshuBunruiCdFr.Text))
            {
                this.lblGyoshuBunruiCdFr.Text = "先　頭";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtGyoshuBunruiCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.Decimal, 6, this.txtGyoshuBunruiCdFr.Text);
                DataTable dtTB_HN_GYOSHU_BUNRUI_MST = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "TB_HN_GYOSHU_BUNRUI_MST",
                    "KAISHA_CD = @KAISHA_CD AND GYOSHU_BUNRUI_CD = @GYOSHU_BUNRUI_CD",
                    dpc);
                if (dtTB_HN_GYOSHU_BUNRUI_MST.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblGyoshuBunruiCdFr.Text = dtTB_HN_GYOSHU_BUNRUI_MST.Rows[0]["GYOSHU_BUNRUI_NM"].ToString();
                }
            }

            return true;
        }

        /// <summary>
        /// 魚種分類(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidGyoshuBunruiCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtGyoshuBunruiCdTo.Text))
            {
                this.lblGyoshuBunruiCdTo.Text = "最　後";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtGyoshuBunruiCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.Decimal, 6, this.txtGyoshuBunruiCdTo.Text);
                DataTable dtTB_HN_GYOSHU_BUNRUI_MST = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "TB_HN_GYOSHU_BUNRUI_MST",
                    "KAISHA_CD = @KAISHA_CD AND GYOSHU_BUNRUI_CD = @GYOSHU_BUNRUI_CD",
                    dpc);
                if (dtTB_HN_GYOSHU_BUNRUI_MST.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblGyoshuBunruiCdTo.Text = dtTB_HN_GYOSHU_BUNRUI_MST.Rows[0]["GYOSHU_BUNRUI_NM"].ToString();
                }
            }

            return true;
        }

        /// <summary>
        /// 魚種(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidGyoshuCdFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtGyoshuCdFr.Text))
            {
                this.lblGyoshuCdFr.Text = "先　頭";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtGyoshuCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 6, Util.ToInt(Util.ToString(this.txtGyoshuCdFr.Text)));
                DataTable dtTB_HN_GYOSHU = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "VI_HN_GYOSHU",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND GYOSHU_CD = @GYOSHU_CD",
                    dpc);
                if (dtTB_HN_GYOSHU.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblGyoshuCdFr.Text = dtTB_HN_GYOSHU.Rows[0]["GYOSHU_NM"].ToString();
                }
            }

            return true;
        }

        /// <summary>
        /// 魚種(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidGyoshuCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtGyoshuCdTo.Text))
            {
                this.lblGyoshuCdTo.Text = "最　後";
                return true;
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtGyoshuCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 6, Util.ToInt(Util.ToString(this.txtGyoshuCdTo.Text)));
                DataTable dtTB_HN_GYOSHU = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "VI_HN_GYOSHU",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND GYOSHU_CD = @GYOSHU_CD",
                    dpc);
                if (dtTB_HN_GYOSHU.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblGyoshuCdTo.Text = dtTB_HN_GYOSHU.Rows[0]["GYOSHU_NM"].ToString();
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 集計表のチェック
            if (!IsValidShukeihyo())
            {
                this.txtShukeihyo.Focus();
                this.txtShukeihyo.SelectAll();
                return false;
            }
            else if (this.txtShukeihyo.Text == "0")
            {
                this.lblShukeihyoNm.Text = "";
                Msg.Notice("入力に誤りがあります。");

                this.txtShukeihyo.Focus();
                this.txtShukeihyo.SelectAll();
                return false;
            }

            // 精算区分のチェック
            if (!IsValidSetSeisanKubun())
            {
                this.txtSeisanKubun.Focus();
                this.txtSeisanKubun.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtYearFr.Text, this.txtYearFr.MaxLength))
            {
                this.txtYearFr.Focus();
                this.txtYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
            {
                this.txtMonthFr.Focus();
                this.txtMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
            {
                this.txtDayFr.Focus();
                this.txtDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtYearTo.Text, this.txtYearTo.MaxLength))
            {
                this.txtYearTo.Focus();
                this.txtYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
            {
                this.txtMonthTo.Focus();
                this.txtMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
            {
                this.txtDayTo.Focus();
                this.txtDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpTo();

            /// 船主(自)
            if (!IsValidSenshuCdFr())
            {
                this.txtSenshuCdFr.Focus();
                this.txtSenshuCdFr.SelectAll();
                return false;
            }
            /// 船主(至)
            if (!IsValidSenshuCdTo())
            {
                this.txtSenshuCdTo.Focus();
                this.txtSenshuCdTo.SelectAll();
                return false;
            }

            /// 漁法(自)
            if (!IsValidGyohoCdFr())
            {
                this.txtGyohoCdFr.Focus();
                this.txtGyohoCdFr.SelectAll();
                return false;
            }
            /// 漁法(至)
            if (!IsValidGyohoCdTo())
            {
                this.txtGyohoCdTo.Focus();
                this.txtGyohoCdTo.SelectAll();
                return false;
            }

            /// 地区(自)
            if (!IsValidChikuCdFr())
            {
                this.txtChikuCdFr.Focus();
                this.txtChikuCdFr.SelectAll();
                return false;
            }
            /// 地区(至)
            if (!IsValidChikuCdTo())
            {
                this.txtChikuCdTo.Focus();
                this.txtChikuCdTo.SelectAll();
                return false;
            }

            /// 魚種分類(自)
            if (!IsValidGyoshuBunruiCdFr())
            {
                this.txtGyoshuBunruiCdFr.Focus();
                this.txtGyoshuBunruiCdFr.SelectAll();
                return false;
            }
            /// 魚種分類(至)
            if (!IsValidGyoshuBunruiCdTo())
            {
                this.txtGyoshuBunruiCdTo.Focus();
                this.txtGyoshuBunruiCdTo.SelectAll();
                return false;
            }

            /// 魚種(自)
            if (!IsValidGyoshuCdFr())
            {
                this.txtGyoshuCdFr.Focus();
                this.txtGyoshuCdFr.SelectAll();
                return false;
            }
            /// 魚種(至)
            if (!IsValidGyoshuCdTo())
            {
                this.txtGyoshuCdTo.Focus();
                this.txtGyoshuCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblGengoFr.Text = arrJpDate[0];
            this.txtYearFr.Text = arrJpDate[2];
            this.txtMonthFr.Text = arrJpDate[3];
            this.txtDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblGengoTo.Text = arrJpDate[0];
            this.txtYearTo.Text = arrJpDate[2];
            this.txtMonthTo.Text = arrJpDate[3];
            this.txtDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 集計表名を取得
        /// </summary>
        /// <returns>集計表設定の取得したデータ</returns>
        private DataTable GetShukeihyoSetteiData()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" TITLE ");
            sql.Append("FROM");
            sql.Append(" TB_HN_SHUKEIHYO_SETTEI_SERI ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHISHO_CD = @SHISHO_CD");
            sql.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
            sql.Append(" AND SHUBETSU_KUBUN  = @SHUBETSU_KUBUN");
            sql.Append(" AND POSITION = @POSITION ");
            sql.Append(" AND SETTEI_CD = @SETTEI_CD ");
            sql.Append("ORDER BY");
            sql.Append(" SETTEI_CD ASC ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            //dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(this.txtMizuageShishoCd.Text));
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@POSITION", SqlDbType.Decimal, 2, 0);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtShukeihyo.Text);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// 魚種名を取得
        /// </summary>
        /// <returns>集計表設定の取得したデータ</returns>
        private DataTable GetGyoshuData(String shohinCd)
        {
            // 魚種情報取得
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" GYOSHU_NM ");
            sql.Append("FROM");
            sql.Append(" VI_HN_GYOSHU ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHISHO_CD = @SHISHO_CD");
            sql.Append(" AND GYOSHU_CD = @GYOSHU_CD");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, shohinCd);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false)
        {
            HNMR1145 msgFrm = new HNMR1145();
            try
            {
#if DEBUG                
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                this.Dba.BeginTransaction();

                // 処理中メッセージ表示
                msgFrm.Show();
                msgFrm.Refresh();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");

                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");
                    cols.Append(" ,ITEM28");
                    cols.Append(" ,ITEM29");
                    cols.Append(" ,ITEM30");
                    cols.Append(" ,ITEM31");
                    cols.Append(" ,ITEM32");
                    cols.Append(" ,ITEM33");
                    cols.Append(" ,ITEM34");
                    cols.Append(" ,ITEM35");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNMR11411R rpt = new HNMR11411R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();

                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, "セリ推移表", 1);
                        //string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);

                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif

                msgFrm.Close();
            }
        }

        /// <summary>
        /// EXCELに出力する
        /// </summary>
        private void DoExcelPut()
        {
            HNMR1145 msgFrm = new HNMR1145();
            try
            {
                this.Dba.BeginTransaction();

                // 処理中メッセージ表示
                msgFrm.Show();
                msgFrm.Refresh();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");

                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");
                    cols.Append(" ,ITEM28");
                    cols.Append(" ,ITEM29");
                    cols.Append(" ,ITEM30");
                    cols.Append(" ,ITEM31");
                    cols.Append(" ,ITEM32");
                    cols.Append(" ,ITEM33");
                    cols.Append(" ,ITEM34");
                    cols.Append(" ,ITEM35");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 項目名の設定
                    ArrayList title = new ArrayList();
                    for (int i = 6; i < 20; i++)
                    {
                        if (Util.ToString(dtOutput.Rows[0]["ITEM" + i.ToString("00")]) != "")
                        {
                            if (i == 6)
                            {
                                title.Add("コード");
                                title.Add(Util.ToString(dtOutput.Rows[0]["ITEM" + i.ToString("00")]));
                                title.Add("");
                            }
                            else
                                title.Add(Util.ToString(dtOutput.Rows[0]["ITEM" + i.ToString("00")]));
                        }
                        else
                        {
                            title.Add("");
                        }
                    }
                    string shisho = "";
                    var workbook = new XLWorkbook();
                    IXLWorksheet ws = null;
                    int lin = 1;
                    foreach (DataRow dr in dtOutput.Rows)
                    {
                        // 支所コードでシート分け
                        if (shisho != Util.ToString(dr["ITEM35"]))
                        {
                            // 見出しの設定
                            lin = 1;
                            shisho = Util.ToString(dr["ITEM35"]);
                            ws = workbook.Worksheets.Add("セリ推移表" + shisho);
                            ws.Cell(lin, 1).Value = Util.ToString(dr["ITEM03"]) + " ～ " + Util.ToString(dr["ITEM04"]);
                            lin++;
                            for (int i = 0; i < title.Count; i++)
                            {
                                ws.Cell(lin, (i + 1)).Style.NumberFormat.Format = "@";
                                ws.Cell(lin, (i + 1)).Value = title[i];
                            }
                            lin++;
                        }

                        // 明細部の設定
                        string codename = Util.ToString(dr["ITEM20"]).Trim();
                        if (codename.StartsWith("【"))
                            ws.Cell(lin, 1).Value = codename;
                        else
                        {
                            int fi = codename.IndexOf(' ');
                            if (fi != -1)
                            {
                                string cd = codename.Substring(0, fi);
                                string nm = codename.Substring(fi);
                                ws.Cell(lin, 1).Value = cd;
                                ws.Cell(lin, 2).Style.NumberFormat.Format = "@";
                                ws.Cell(lin, 2).Value = nm;
                            }
                            else
                            {
                                ws.Cell(lin, 1).Style.NumberFormat.Format = "@";
                                ws.Cell(lin, 1).Value = codename;
                            }
                        }
                        ws.Cell(lin, 3).Value = Util.ToString(dr["ITEM21"]);

                        string val = "";
                        int col = 4;
                        int itm = 22;
                        // 数値項目の設定
                        for (col = 4; col < 17; col++)
                        {
                            val = Util.ToString(dr["ITEM" + itm.ToString("00")]);
                            ws.Cell(lin, col).Value = val;
                            if (val != "")
                            {
                                string fmt = IsDecimal(val) ? "#,###.00" : "#,###";
                                ws.Cell(lin, col).Style.NumberFormat.Format = fmt;
                            }
                            itm++;
                        }

                        lin++;
                    }

                    // 保存先の指定
                    string saveFileName = GetSavePath("セリ推移表", 2);
                    //string saveFileName = Util.GetSavePath(Constants.SubSys.Han, "セリ推移表", 2);
                    if (!ValChk.IsEmpty(saveFileName))
                    {
                        workbook.SaveAs(saveFileName);

                        Msg.InfoNm("EXCEL出力", "保存しました。");
                        Util.OpenFolder(saveFileName);
                    }

                }
            }
            finally
            {
                this.Dba.Rollback();

                msgFrm.Close();
            }
        }

        private bool IsDecimal(string val)
        {
            int fi = val.IndexOf('.');
            if (fi != -1)
            {
                fi = val.IndexOf('%');
                if (fi != -1)
                    return false;
                else
                    return true;
            }
            return false;
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 全て選択時は支所毎に抽出
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            // 支所コード
            int shishoCd = Util.ToInt(Util.ToString(txtMizuageShishoCd.Text));
            if (shishoCd != 0)
                Sql.Append(" AND SHISHO_CD = @SHISHO_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_SHISHO",
                "KAISHA_CD = @KAISHA_CD " + Sql.ToString(),
                "SHISHO_CD",
                dpc);

            foreach (DataRow dr in dt.Rows)
            {
                MakeWkDataMain(Util.ToInt(Util.ToString(dr["SHISHO_CD"])));
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }

            return dataFlag;
        }



        #region 推移表データ登録

        #region 変数

        #region 保持部品
        [Serializable()]
        class KomokuInfo
        {
            public string Code;
            public string Name;
            public string Komoku;
            public string KeyName;

            public List<string> SumKmk;
            public List<int> SumKbn;

            public KomokuInfo()
            {
                Code = "";
                Name = "";
                Komoku = "";
                KeyName = "";
            }
        }

        [Serializable()]
        class TotalInfo
        {
            public List<LineInfo> LineR;

            public decimal[] Suryo = new decimal[14];
            public decimal[] Kingk = new decimal[14];
        }

        [Serializable()]
        class LineInfo
        {
            private decimal[] total = new decimal[14];

            public void setTotal(int i, decimal value)
            {
                if (i >= 0 && i < 14)
                    total[i] = value;
            }
            public decimal getTotal(int i)
            {
                if (i >= 0 && i < 14)
                    return total[i];
                return 0m;
            }
        }
        #endregion

        List<KomokuInfo> _Komoku;
        List<KomokuInfo> _Key;
        List<KomokuInfo> _Title;
        bool _KeyFlg;
        List<TotalInfo> _Total;
        DataTable dtMain = null;
        // 印刷データソート
        int _dbSORT;
        // 日付
        DateTime _DateTimeFr;
        DateTime _DateTimeTo;
        // 日付を和暦で保持
        string[] _JpFr;
        string[] _JpTo;
        // 支所コード
        int _shishoCd;
        #endregion

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private void MakeWkDataMain(int shishoCd)
        {
            try
            {
                // タイトル及び各設定情報の保持
                if (!TitleSet())
                    return;

                this._dbSORT = (shishoCd * 1000000);
                // 日付を西暦にして取得
                this._DateTimeFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                        this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
                this._DateTimeTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                        this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);
                this._JpFr = Util.ConvJpDate(this._DateTimeFr, this.Dba);
                this._JpTo = Util.ConvJpDate(this._DateTimeTo, this.Dba);

                this._shishoCd = shishoCd;

                // 支所分のデータ取得
                if (SelectData(shishoCd, ref dtMain))
                {
                    // 集計処理
                    int count = dtMain.Rows.Count;
                    for (int i = 0; i < dtMain.Rows.Count; i++)
                    {
                        // 集計項目毎の集計処理
                        TotalSummary(dtMain.Rows[i]);

                        // 集計項目のブレイク確認
                        KeyCheck((count == 1 ? true : false), dtMain.Rows[(i < count - 1 ? i + 1 : i - 1)]);
                    }

                    // 最終分の登録処理
                    InsertMain((this._Total.Count - 1), true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// 集計項目毎の集計処理
        /// </summary>
        /// <param name="r"></param>
        private void TotalSummary(DataRow r)
        {
            int ymKei = 0;
            // 縦分のループ
            for (int posNo = 0; posNo < this._Total.Count; posNo++)
            {
                // 集計項目の設定
                if (this._KeyFlg && posNo < this._Key.Count)
                {
                    this._Key[posNo].Code = Util.ToString(r[this._Key[posNo].Komoku]);
                    this._Key[posNo].KeyName = Util.ToString(r[this._Key[posNo].Name]);
                }
                // 横分のループ
                for (int ym = 1; ym < this._Title.Count; ym++)
                {
                    if (this._Title[ym].Komoku == Util.ToString(r["DENPYO_NENGETSU"]))
                    {
                        ymKei = 13;
                        // 項目合計集計
                        for (int i = 0; i < this._Komoku.Count; i++)
                        {
                            string item = this._Komoku[i].Komoku;
                            if (item != "")
                            {
                                if (Util.ToInt(this._Komoku[i].Code) == 1 ||
                                    Util.ToInt(this._Komoku[i].Code) == 2 ||
                                    Util.ToInt(this._Komoku[i].Code) == 3)
                                {
                                    for (int j = 0; j < this._Komoku[i].SumKmk.Count; j++)
                                    {
                                        decimal val = Util.ToDecimal(r[this._Komoku[i].SumKmk[j]]); // マイナス区分
                                        decimal def = 0m;
                                        def = this._Total[posNo].LineR[i].getTotal(ym);
                                        this._Total[posNo].LineR[i].setTotal(ym, (def + val));
                                        def = this._Total[posNo].LineR[i].getTotal(ymKei);
                                        this._Total[posNo].LineR[i].setTotal(ymKei, (def + val));
                                    }
                                }
                                else if (Util.ToInt(this._Komoku[i].Code) == 9)
                                {
                                    decimal val = Util.ToDecimal(r[this._Komoku[i].SumKmk[0]]); // マイナス区分
                                    decimal def = 0m;
                                    def = this._Total[posNo].LineR[i].getTotal(ym);
                                    this._Total[posNo].LineR[i].setTotal(ym, (def + val));
                                }
                            }
                        }

                        this._Total[posNo].Suryo[ym] += Util.ToDecimal(r["BARA_SOSU"]);
                        this._Total[posNo].Suryo[ymKei] += Util.ToDecimal(r["BARA_SOSU"]);
                        this._Total[posNo].Kingk[ym] += Util.ToDecimal(r["BAIKA_KINGAKU"]);
                        this._Total[posNo].Kingk[ymKei] += Util.ToDecimal(r["BAIKA_KINGAKU"]);

                        // 総合計
                        if (posNo == GetUBoundmKey(1))
                        {
                            this._Total[posNo].Suryo[ym] += Util.ToDecimal(r["SOUSUU"]);
                            this._Total[posNo].Kingk[ym] += Util.ToDecimal(r["SOUGAK"]);
                            this._Total[posNo].Suryo[ymKei] = 0m;
                            this._Total[posNo].Kingk[ymKei] = 0m;

                            for (int i = 1; i < 13; i++)
                            {
                                this._Total[posNo].Suryo[ymKei] += this._Total[posNo].Suryo[i];
                                this._Total[posNo].Kingk[ymKei] += this._Total[posNo].Kingk[i];
                            }
                        }
                    }
                }
            }
        }

        private int GetUBoundmKey(int num)
        {
            if (this._KeyFlg)
                return (this._Key.Count - 1) + num;
            else
                return -1 + num;
        }

        /// <summary>
        /// 集計項目のブレイク確認
        /// </summary>
        /// <param name="flg"></param>
        /// <param name="r"></param>
        private void KeyCheck(bool flg, DataRow r)
        {
            if (!this._KeyFlg) return;

            bool brk = false;
            int count = this._Key.Count - 1;
            for (int i = 0; i <= count; i++)
            {
                if (flg)
                    brk = true;
                else if (Util.ToInt(Util.ToString(this._Key[i].Code)) != Util.ToInt(Util.ToString(r[this._Key[i].Komoku])))
                    brk = true;

                if (brk)
                {
                    for (int j = count; j >= i; j--)
                    {
                        // データ登録メイン
                        InsertMain(j, false);
                    }
                }
            }
        }

        /// <summary>
        /// データ登録メイン
        /// </summary>
        /// <param name="idx"></param>
        /// <param name="totalFlg"></param>
        private void InsertMain(int idx, bool totalFlg)
        {
            // 集計項目毎に登録
            for (int i = 1; i < this._Komoku.Count; i++)
            {
                InsertRecord(idx, i, totalFlg);
            }

            // 合計のクリア（集計項目分）
            if (!totalFlg)
            {
                for (int i = 0; i < this._Komoku.Count; i++)
                {
                    for (int j = 0; j < 14; j++)
                    {
                        this._Total[idx].LineR[i].setTotal(j, 0m);
                    }
                }
                for (int i = 0; i < 14; i++)
                {
                    this._Total[idx].Suryo[i] = 0m;
                    this._Total[idx].Kingk[i] = 0m;
                }
            }
        }

        /// <summary>
        /// 項目名編集処理
        /// </summary>
        /// <param name="idx"></param>
        /// <param name="totalFlg"></param>
        /// <returns></returns>
        private string EditKomokuName(int idx, bool totalFlg)
        {
            string item = "";
            string ifmt = "";

            // コード／名称
            if (!totalFlg && idx == GetUBoundmKey(0))
            {
                //ifmt = this._Key[idx].Komoku == "SHOHIN_CD" ? "############0" : "#######0";
                ifmt = "#######0";
                //item = Util.ToDecimal(Util.ToString(this._Key[idx].Code)).ToString(ifmt).PadLeft(Util.ToString(this._Key[idx].Komoku) == "SHOHIN_CD" ? 13 : 8) + " " + Util.ToString(this._Key[idx].KeyName);
                item = Util.ToDecimal(Util.ToString(this._Key[idx].Code)).ToString(ifmt).PadLeft(8) + " " + Util.ToString(this._Key[idx].KeyName);
                if (this._Komoku.Count > 0)
                {
                    return item;
                }
            }
            else if (!totalFlg && GetUBoundmKey(1) != idx)
            {
                int len = this._Komoku.Count > 0 ? 33 : 37;
                for (int i = 0; i < idx + 1; i++)
                    ifmt += " ";
                item = ifmt + "【" + this._Key[idx].KeyName + " 計】";
            }
            else
            {
                item = "【　 総 　合　 計　 】";
            }
            return item;
        }

        private void InsertRecord(int idx, int detail, bool totalFlg)
        {
            string item = "";
            string ifmt = "";

            // １レコード分保持用
            Dictionary<string, string> rec = new Dictionary<string, string>();

            // 見出し部
            rec.Add("ITEM01", this.UInfo.KaishaNm);
            rec.Add("ITEM02", this.lblShukeihyoNm.Text); // this._Title[0].Komoku
            rec.Add("ITEM03", this._JpFr[5]);
            rec.Add("ITEM04", this._JpTo[5]);
            rec.Add("ITEM05", this._Title[0].KeyName);
            // 項目タイトル
            int feildIndex = 6;
            for (int i = 0; i < 13; i++)
            {
                rec.Add("ITEM" + feildIndex.ToString("00"), this._Title[i].Name);
                feildIndex++;
            }

            rec.Add("ITEM19", "合  計");

            // 明細部
            // コード／名称
            //if (detail == 0)
            if (detail == 1)
            {
                item = EditKomokuName(idx, totalFlg);
            }
            else
            {
                item = "";
            }
            rec.Add("ITEM20", item);
            // 集計項目名
            if (this._Komoku.Count > 0)
            {
                item = "[" + Util.ToString(this._Komoku[detail].Name) + "]";
            }
            else
            {
                item = "";
            }
            rec.Add("ITEM21", item);

            // 項目
            int KeyIndex = GetUBoundmKey(1);
            string field = "";
            int index = 0;

            // 横分の変数へ保存
            feildIndex = 22;
            for (int i = 1; i < 13; i++)
            {
                item = "";
                field = Util .ToString(this._Title[i].Komoku);
                if (field != "")
                {
                    if (Util.ToInt(Util.ToString(this._Komoku[detail].Code)) == 1)
                    {
                        ifmt = Math.Abs(this._Total[idx].LineR[detail].getTotal(i)) <= 999999999 ? "###,###,###" : "###########";
                        item = this._Total[idx].LineR[detail].getTotal(i).ToString(ifmt);
                    }
                    else if (Util.ToInt(Util.ToString(this._Komoku[detail].Code)) == 2)
                    {
                        item = this._Total[idx].LineR[detail].getTotal(i).ToString("####,##0.00");
                    }
                    else if (Util.ToInt(Util.ToString(this._Komoku[detail].Code)) == 3)
                    {
                        item = this._Total[idx].LineR[detail].getTotal(i).ToString("####,##0.00");
                    }
                    else if (Util.ToInt(Util.ToString(this._Komoku[detail].Code)) == 9)
                    {
                        field = this._Komoku[detail].Komoku;
                        decimal val = 0m;
                        if (idx == GetUBoundmKey(0))
                        {
                            val = this._Total[idx].LineR[detail].getTotal(i);
                        }
                        else if (field == "KOUSEIHI")
                        {
                            val = GetKouseihi(this._Total[idx].Kingk[i], this._Total[KeyIndex].Kingk[13]);
                        }

                        if (val != 0)
                            item = val.ToString("######0.0") + " %";
                        else
                            item = " ";
                    }
                }
                if (item == "0.00") item = "";

                rec.Add("ITEM" + feildIndex.ToString("00"), item);
                feildIndex++;
            }

            // 合計（横計）
            index = 13;
            if (Util.ToInt(Util.ToString(this._Komoku[detail].Code)) == 1)
            {
                ifmt = Math.Abs(this._Total[idx].LineR[detail].getTotal(index)) <= 999999999 ? "###,###,###" : "###########";
                item = this._Total[idx].LineR[detail].getTotal(index).ToString(ifmt);
            }
            else if (Util.ToInt(Util.ToString(this._Komoku[detail].Code)) == 2)
            {
                item = this._Total[idx].LineR[detail].getTotal(index).ToString("####,##0.00");
            }
            else if (Util.ToInt(Util.ToString(this._Komoku[detail].Code)) == 3)
            {
                item = this._Total[idx].LineR[detail].getTotal(index).ToString("####,##0.00");
            }
            else if (Util.ToInt(Util.ToString(this._Komoku[detail].Code)) == 9)
            {
                field = this._Komoku[detail].Komoku;
                decimal val = 0m;
                if (field == "金額構成比")
                {
                    val = GetKouseihi(this._Total[idx].Kingk[index], this._Total[KeyIndex].Kingk[13]);
                }
                if (val != 0)
                    item = val.ToString("######0.0") + " %";
                else
                    item = " ";
            }
            if (item == "0.00") item = "";

            rec.Add("ITEM34", item);

            // 出来上がった１行分の登録

            #region データ登録
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            Sql.Append("INSERT INTO PR_HN_TBL(");
            Sql.Append(" GUID");
            Sql.Append(" ,SORT");
            Sql.Append(" ,ITEM01");
            Sql.Append(" ,ITEM02");
            Sql.Append(" ,ITEM03");
            Sql.Append(" ,ITEM04");
            Sql.Append(" ,ITEM05");
            Sql.Append(" ,ITEM06");
            Sql.Append(" ,ITEM07");
            Sql.Append(" ,ITEM08");
            Sql.Append(" ,ITEM09");
            Sql.Append(" ,ITEM10");
            Sql.Append(" ,ITEM11");
            Sql.Append(" ,ITEM12");
            Sql.Append(" ,ITEM13");
            Sql.Append(" ,ITEM14");
            Sql.Append(" ,ITEM15");
            Sql.Append(" ,ITEM16");
            Sql.Append(" ,ITEM17");
            Sql.Append(" ,ITEM18");
            Sql.Append(" ,ITEM19");
            Sql.Append(" ,ITEM20");
            Sql.Append(" ,ITEM21");
            Sql.Append(" ,ITEM22");
            Sql.Append(" ,ITEM23");
            Sql.Append(" ,ITEM24");
            Sql.Append(" ,ITEM25");
            Sql.Append(" ,ITEM26");
            Sql.Append(" ,ITEM27");
            Sql.Append(" ,ITEM28");
            Sql.Append(" ,ITEM29");
            Sql.Append(" ,ITEM30");
            Sql.Append(" ,ITEM31");
            Sql.Append(" ,ITEM32");
            Sql.Append(" ,ITEM33");
            Sql.Append(" ,ITEM34");
            Sql.Append(" ,ITEM35");
            Sql.Append(") ");
            Sql.Append("VALUES(");
            Sql.Append("  @GUID");
            Sql.Append(" ,@SORT");
            Sql.Append(" ,@ITEM01");
            Sql.Append(" ,@ITEM02");
            Sql.Append(" ,@ITEM03");
            Sql.Append(" ,@ITEM04");
            Sql.Append(" ,@ITEM05");
            Sql.Append(" ,@ITEM06");
            Sql.Append(" ,@ITEM07");
            Sql.Append(" ,@ITEM08");
            Sql.Append(" ,@ITEM09");
            Sql.Append(" ,@ITEM10");
            Sql.Append(" ,@ITEM11");
            Sql.Append(" ,@ITEM12");
            Sql.Append(" ,@ITEM13");
            Sql.Append(" ,@ITEM14");
            Sql.Append(" ,@ITEM15");
            Sql.Append(" ,@ITEM16");
            Sql.Append(" ,@ITEM17");
            Sql.Append(" ,@ITEM18");
            Sql.Append(" ,@ITEM19");
            Sql.Append(" ,@ITEM20");
            Sql.Append(" ,@ITEM21");
            Sql.Append(" ,@ITEM22");
            Sql.Append(" ,@ITEM23");
            Sql.Append(" ,@ITEM24");
            Sql.Append(" ,@ITEM25");
            Sql.Append(" ,@ITEM26");
            Sql.Append(" ,@ITEM27");
            Sql.Append(" ,@ITEM28");
            Sql.Append(" ,@ITEM29");
            Sql.Append(" ,@ITEM30");
            Sql.Append(" ,@ITEM31");
            Sql.Append(" ,@ITEM32");
            Sql.Append(" ,@ITEM33");
            Sql.Append(" ,@ITEM34");
            Sql.Append(" ,@ITEM35");
            Sql.Append(") ");

            // ページヘッダーデータを設定
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            dpc.SetParam("@SORT", SqlDbType.VarChar, 9, this._dbSORT);

            // 作成データの設定
            for (int i = 1; i < 35; i++)
            {
                dpc.SetParam("@ITEM" + i.ToString("00"), SqlDbType.VarChar, 200, rec["ITEM" + i.ToString("00")]);
            }

            dpc.SetParam("@ITEM35", SqlDbType.VarChar, 200, this._shishoCd.ToString());

            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

            this._dbSORT++;

            #endregion
        }

        private decimal GetKouseihi(decimal si, decimal bo)
        {
            if (si == 0 || bo == 0)
            {
                return 0m;
            }
            else
            {
                return si * 100m / bo;
            }
        }

        private DbParamCollection getParam(int shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            // 日付を西暦にして取得
            DateTime tmpJpFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                    this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
            DateTime tmpJpTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                    this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);
            // 日付を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpJpFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpJpTo, this.Dba);

            #region コード範囲を取得
            // 船主コードを取得
            string SenshuCdFr;
            string SenshuCdTo;
            if (Util.ToString(this.txtSenshuCdFr.Text) != "")
            {
                SenshuCdFr = this.txtSenshuCdFr.Text;
            }
            else
            {
                SenshuCdFr = "0";
            }
            if (Util.ToString(this.txtSenshuCdTo.Text) != "")
            {
                SenshuCdTo = this.txtSenshuCdTo.Text;
            }
            else
            {
                SenshuCdTo = "9999";
            }
            // 漁法コードを取得
            string GyohoCdFr;
            string GyohoCdTo;
            if (Util.ToString(this.txtGyohoCdFr.Text) != "")
            {
                GyohoCdFr = this.txtGyohoCdFr.Text;
            }
            else
            {
                GyohoCdFr = "0";
            }
            if (Util.ToString(this.txtGyohoCdTo.Text) != "")
            {
                GyohoCdTo = this.txtGyohoCdTo.Text;
            }
            else
            {
                GyohoCdTo = "9999";
            }
            // 地区コードを取得
            string ChikuCdFr;
            string ChikuCdTo;
            if (Util.ToString(this.txtChikuCdFr.Text) != "")
            {
                ChikuCdFr = this.txtChikuCdFr.Text;
            }
            else
            {
                ChikuCdFr = "0";
            }
            if (Util.ToString(this.txtChikuCdTo.Text) != "")
            {
                ChikuCdTo = this.txtChikuCdTo.Text;
            }
            else
            {
                ChikuCdTo = "9999";
            }
            // 魚種分類コード
            string GyoshuBunruiCdFr;
            string GyoshuBunruiCdTo;
            if (Util.ToString(this.txtGyoshuBunruiCdFr.Text) != "")
            {
                GyoshuBunruiCdFr = this.txtGyoshuBunruiCdFr.Text;
            }
            else
            {
                GyoshuBunruiCdFr = "0";
            }
            if (Util.ToString(this.txtGyoshuBunruiCdTo.Text) != "")
            {
                GyoshuBunruiCdTo = this.txtGyoshuBunruiCdTo.Text;
            }
            else
            {
                GyoshuBunruiCdTo = "9999";
            }
            // 区分コード
            string GyoshuCdFr;
            string GyoshuCdTo;
            if (Util.ToString(this.txtGyoshuCdFr.Text) != "")
            {
                GyoshuCdFr = this.txtGyoshuCdFr.Text;
            }
            else
            {
                GyoshuCdFr = "0";
            }
            if (Util.ToString(this.txtGyoshuCdTo.Text) != "")
            {
                GyoshuCdTo = this.txtGyoshuCdTo.Text;
            }
            else
            {
                GyoshuCdTo = "9999";
            }
            #endregion

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd); // 会社コード
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd); // 支所コード
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 4, Util.ToInt(this.txtSeisanKubun.Text)); // 精算区分
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpJpFr.Date.ToString("yyyy/MM/dd")); // 日付Fr
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpJpTo.Date.ToString("yyyy/MM/dd")); // 日付To

            #region コード範囲の設定項目
            dpc.SetParam("@TOKUISAKI_CD_FR", SqlDbType.Decimal, 4, SenshuCdFr); // 船主コードFr
            dpc.SetParam("@TOKUISAKI_CD_TO", SqlDbType.Decimal, 4, SenshuCdTo); // 船主コードTo
            dpc.SetParam("@SEIKYUSAKI_CD_FR", SqlDbType.Decimal, 4, GyohoCdFr); // 漁法コードFr
            dpc.SetParam("@SEIKYUSAKI_CD_TO", SqlDbType.Decimal, 4, GyohoCdTo); // 漁法コードTo
            dpc.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 4, ChikuCdFr); // 地区コードFr
            dpc.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 4, ChikuCdTo); // 地区コードTo
            dpc.SetParam("@SHUKEIKUBUN_CD_FR", SqlDbType.Decimal, 4, GyoshuBunruiCdFr); // 魚種分類コードFr
            dpc.SetParam("@SHUKEIKUBUN_CD_TO", SqlDbType.Decimal, 4, GyoshuBunruiCdTo); // 魚種分類コードTo
            dpc.SetParam("@KUBUN_CD_FR", SqlDbType.Decimal, 4, GyoshuCdFr); // 魚種コードFr
            dpc.SetParam("@KUBUN_CD_TO", SqlDbType.Decimal, 4, GyoshuCdTo); // 魚種コードTo
            #endregion

            return dpc;
        }

        /// <summary>
        /// 総合計データ取得
        /// </summary>
        /// <param name="shishoCd"></param>
        /// <param name="suryo"></param>
        /// <param name="kingk"></param>
        /// <returns></returns>
        private bool SelectTotalData(int shishoCd, ref decimal suryo, ref decimal kingk)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc = getParam(shishoCd);

            // 一旦先に、合計の抽出（構成比用）
            Sql.Append("SELECT");
            Sql.Append(" SUM(CAST(BARA_SOSU AS DECIMAL(12, 3))) AS SOSU,");
            Sql.Append(" SUM(CAST(BAIKA_KINGAKU AS DECIMAL(12))) AS KINGAKU ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_TORIHIKI_SHUKEI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD");
            Sql.Append(" AND SHISHO_CD = @SHISHO_CD");
            //Sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append(" AND DENPYO_KUBUN = 3");

            if (Util.ToInt(this.txtSeisanKubun.Text) != 0)
                Sql.Append(" AND SEISAN_KUBUN = @SEISAN_KUBUN");

            Sql.Append(" AND DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO");
            #region コード範囲の設定項目
            Sql.Append(" AND SENSHU_CD BETWEEN @TOKUISAKI_CD_FR AND @TOKUISAKI_CD_TO");
            Sql.Append(" AND GYOHO_CD BETWEEN @SEIKYUSAKI_CD_FR AND @SEIKYUSAKI_CD_TO");
            Sql.Append(" AND CHIKU_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO");
            Sql.Append(" AND GYOSHU_BUNRUI_CD BETWEEN @SHUKEIKUBUN_CD_FR AND @SHUKEIKUBUN_CD_TO");
            Sql.Append(" AND SHOHIN_CD BETWEEN @KUBUN_CD_FR AND @KUBUN_CD_TO");
            #endregion

            DataTable dtTotal = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            suryo = 0m;
            kingk = 0m;
            if (dtTotal.Rows.Count != 0)
            {
                suryo = Util.ToDecimal(Util.ToString(dtTotal.Rows[0]["SOSU"]));
                kingk = Util.ToDecimal(Util.ToString(dtTotal.Rows[0]["KINGAKU"]));
                return true;
            }
            return false;
        }

        /// <summary>
        /// 推移表データ取得
        /// </summary>
        /// <param name="shishoCd"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        private bool SelectData(int shishoCd, ref DataTable t)
        {
            string selectSql = "";
            string groupSql = "";
            string orderSql = "";
            bool gyoshu = false;
            decimal suryo = 0m;
            decimal kingk = 0m;

            // 設定データ存在確認
            if (!CheckSetR())
                return false;

            // SQL
            SelectTotalData(shishoCd, ref suryo, ref kingk);
            EditSelect(ref selectSql, ref groupSql, ref orderSql, ref gyoshu);

            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc = getParam(shishoCd);

            Sql.Append("SELECT ");
            Sql.Append(selectSql);
            Sql.Append(" DENPYO_NENGETSU,");

            Sql.Append(" SUM(CAST(BARA_SOSU AS DECIMAL(12,2))) AS BARA_SOSU,");
            Sql.Append(" SUM(CAST(BAIKA_KINGAKU AS DECIMAL(12))) AS BAIKA_KINGAKU,");
            Sql.Append(" SUM(CAST(MEISAI_SHOHIZEIGAKU AS DECIMAL(12))) AS MEISAI_SHOHIZEIGAKU,");
            Sql.Append(" SUM(CAST(SENSHU_SHOHIZEIGAKU AS DECIMAL(12))) AS SENSHU_SHOHIZEIGAKU,");
            Sql.Append(" SUM(CAST(SENSHU_KOJO_GOKEIGAKU AS DECIMAL(12))) AS SENSHU_KOJO_GOKEIGAKU,");
            Sql.Append(" SUM(CAST(SENSHU_KOJO_GOKEIGAKU * -1 AS DECIMAL(12))) AS SENSHU_KOJO_GOKEIGAKU_MINUS,");
            Sql.Append(" SUM(CAST(SENSHU_ZEINUKI_KINGAKU AS DECIMAL(12))) AS SENSHU_ZEINUKI_KINGAKU,");
            Sql.Append(" " + suryo.ToString() + " AS SOUSUU,");
            Sql.Append(" " + kingk.ToString() + " AS SOUGAK,");
            if (gyoshu)
            {
                // 魚種、魚種分類の場合のみ有効扱いとする
                Sql.Append(" MAX(CAST(URI_TANKA AS DECIMAL(12,2))) AS TAKANE,");
                Sql.Append(" (CASE WHEN SUM(BARA_SOSU) = 0 THEN 0.00 ");
                Sql.Append("  ELSE CAST((SUM(BAIKA_KINGAKU)) / SUM(BARA_SOSU) AS DECIMAL(12,2)) END) AS NAKANE,");
                Sql.Append(" MIN(CAST(URI_TANKA AS DECIMAL(12,2))) AS YASUNE,");
            }
            else
            {
                Sql.Append(" CAST('0' AS DECIMAL(12,2)) AS TAKANE,");
                Sql.Append(" CAST('0' AS DECIMAL(12,2)) AS NAKANE,");
                Sql.Append(" CAST('0' AS DECIMAL(12,2)) AS YASUNE,");
            }

            Sql.Append(" CASE WHEN " + kingk.ToString() + " <> 0 THEN ");
            Sql.Append("     CASE WHEN SUM(BAIKA_KINGAKU) <> 0 THEN ");
            Sql.Append("         CAST((SUM(BAIKA_KINGAKU) ");
            Sql.Append("          * 100 / " + kingk.ToString() + ") AS DECIMAL(12, 3)) ");
            Sql.Append("     ELSE 0 END ");
            Sql.Append(" ELSE 0 END AS KOUSEIHI ");

            Sql.Append("FROM");
            Sql.Append(" VI_HN_TORIHIKI_SHUKEI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD");
            Sql.Append(" AND SHISHO_CD = @SHISHO_CD");
            //Sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append(" AND DENPYO_KUBUN = 3");

            if (Util.ToInt(this.txtSeisanKubun.Text) != 0)
                Sql.Append(" AND SEISAN_KUBUN = @SEISAN_KUBUN");

            Sql.Append(" AND DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO");
            #region コード範囲の設定項目
            Sql.Append(" AND SENSHU_CD BETWEEN @TOKUISAKI_CD_FR AND @TOKUISAKI_CD_TO");
            Sql.Append(" AND GYOHO_CD BETWEEN @SEIKYUSAKI_CD_FR AND @SEIKYUSAKI_CD_TO");
            Sql.Append(" AND CHIKU_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO");
            Sql.Append(" AND GYOSHU_BUNRUI_CD BETWEEN @SHUKEIKUBUN_CD_FR AND @SHUKEIKUBUN_CD_TO");
            Sql.Append(" AND SHOHIN_CD BETWEEN @KUBUN_CD_FR AND @KUBUN_CD_TO ");
            #endregion

            Sql.Append(" GROUP BY ");
            if (groupSql != "")
                Sql.Append(groupSql + ",");
            Sql.Append(" DENPYO_NENGETSU ");

            Sql.Append(" ORDER BY ");
            if (orderSql != "")
                Sql.Append(orderSql + ",");
            Sql.Append(" DENPYO_NENGETSU ");

            t = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (t.Rows.Count > 0)
                return true;

            return false;
        }

        #region 各種抽出

        /// <summary>
        /// 推移表設定より集計用項目の抽出
        /// </summary>
        /// <param name="sumKb"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        private bool GetSelectR(int sumKb, ref DataTable t)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtShukeihyo.Text);
            dpc.SetParam("@SHUKEI_KUBUN", SqlDbType.Decimal, 1, sumKb);

            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHUKEIHYO_SETTEI_MEISAI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND");
            Sql.Append(" SETTEI_CD = @SETTEI_CD AND");
            Sql.Append(" SHUKEI_KUBUN = @SHUKEI_KUBUN ");
            Sql.Append("ORDER BY");
            Sql.Append(" RENBAN ");

            t = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (t.Rows.Count > 0)
                return true;

            return false;
        }

        /// <summary>
        /// 推移表設定よりソート項目の抽出
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private bool GetOrderByR(ref DataTable t)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtShukeihyo.Text);

            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHUKEIHYO_SETTEI_MEISAI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND");
            Sql.Append(" SETTEI_CD = @SETTEI_CD AND");
            Sql.Append(" SHUTSURYOKU_JUNI > 0 ");
            Sql.Append("ORDER BY");
            Sql.Append(" POSITION DESC,");
            Sql.Append(" SHUTSURYOKU_JUNI ASC, ");
            Sql.Append(" RENBAN ASC");

            t = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (t.Rows.Count > 0)
                return true;

            return false;
        }

        /// <summary>
        /// SQL作成用の部品作成
        /// </summary>
        /// <param name="selectSql"></param>
        /// <param name="groupSql"></param>
        /// <param name="orderSql"></param>
        /// <param name="gyoshu"></param>
        private void EditSelect(ref string selectSql, ref string groupSql, ref string orderSql, ref bool gyoshu)
        {
            selectSql = "";
            groupSql = "";
            orderSql = "";
            gyoshu = false;
            int count = 0;

            DataTable dt = null;
            // [SELECT句][GROUP BY句]編集
            if (GetSelectR(0, ref dt))
            {
                count = 0;
                foreach (DataRow r in dt.Rows)
                {
                    if (count > 0)
                        groupSql += ",";
                    selectSql += Util.ToString(r["SHUKEI_KOMOKU"]) + " AS " + Util.ToString(r["SHUKEI_KOMOKU"]) + ",";
                    groupSql += Util.ToString(r["SHUKEI_KOMOKU"]);
                    // 名称の場合はMAXで追加
                    if (Util.ToString(r["SHUKEI_KOMOKU2"]) != "" && Util.ToString(r["SHUKEI_KOMOKU"]) != Util.ToString(r["SHUKEI_KOMOKU2"]))
                        selectSql += "MAX(" + Util.ToString(r["SHUKEI_KOMOKU2"]) + ") AS " + Util.ToString(r["SHUKEI_KOMOKU2"]) + ",";
                    if (Util.ToString(r["SHUKEI_KOMOKU"]) == "SHOHIN_CD" || Util.ToString(r["SHUKEI_KOMOKU"]) == "GYOSHU_BUNRUI_CD")
                        gyoshu = true;
                    count++;
                }
            }
            // [ORDER By句]編集
            if (GetOrderByR(ref dt))
            {
                count = 0;
                foreach (DataRow r in dt.Rows)
                {
                    if (Util.ToString(r["SHUKEI_KOMOKU"]) != "")
                    {
                        if (count > 0)
                            orderSql += ",";
                        orderSql += Util.ToString(r["SHUKEI_KOMOKU"]) + " ASC";
                        count++;
                    }
                }
            }
        }

        /// <summary>
        /// 設定帳票の存在確認
        /// </summary>
        /// <returns></returns>
        private bool CheckSetR()
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtShukeihyo.Text);

            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHUKEIHYO_SETTEI_MEISAI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND");
            Sql.Append(" SETTEI_CD = @SETTEI_CD ");

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (dtResult.Rows.Count > 0)
                return true;

            return false;
        }
        #endregion

        #region タイトル及び各設定情報の保持
        /// <summary>
        /// 推移表設定より集計項目の抽出
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private bool GetKomokuR(ref DataTable t)
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtShukeihyo.Text);

            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHUKEIHYO_SETTEI_MEISAI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND");
            Sql.Append(" SETTEI_CD = @SETTEI_CD ");
            Sql.Append("ORDER BY");
            Sql.Append(" POSITION,");
            Sql.Append(" RENBAN ");

            t = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (t.Rows.Count > 0)
                return true;

            return false;
        }

        /// <summary>
        /// 推移表設定より集計キー項目の抽出
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private bool GetKeyR(ref DataTable t)
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtShukeihyo.Text);

            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHUKEIHYO_SETTEI_MEISAI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND");
            Sql.Append(" SETTEI_CD = @SETTEI_CD AND");
            Sql.Append(" SHUKEI_KUBUN = 0 ");
            Sql.Append("ORDER BY");
            Sql.Append(" RENBAN ");

            t = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (t.Rows.Count > 0)
                return true;

            return false;
        }

        /// <summary>
        /// 推移表設定より表示位置項目の抽出
        /// </summary>
        /// <param name="posNo"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        private bool GetTitlR(int posNo, ref DataTable t)
        {
            // データを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, 3);
            dpc.SetParam("@SHUBETSU_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@SETTEI_CD", SqlDbType.Decimal, 4, this.txtShukeihyo.Text);
            dpc.SetParam("@POSITION", SqlDbType.Decimal, 1, posNo);

            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SHUKEIHYO_SETTEI_SERI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
            Sql.Append(" SHUBETSU_KUBUN = @SHUBETSU_KUBUN AND");
            Sql.Append(" SETTEI_CD = @SETTEI_CD AND");
            Sql.Append(" POSITION = @POSITION ");

            t = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (t.Rows.Count > 0)
                return true;

            return false;
        }

        /// <summary>
        /// タイトル及び各設定情報の保持
        /// </summary>
        /// <returns></returns>
        private bool TitleSet()
        {
            this._KeyFlg = false;

            KomokuInfo k;
            this._Title = new List<KomokuInfo>();
            // 日付を西暦にして取得
            DateTime tmpJpFr = Util.ConvAdDate(this.lblGengoFr.Text, this.txtYearFr.Text,
                    this.txtMonthFr.Text, this.txtDayFr.Text, this.Dba);
            DateTime tmpJpTo = Util.ConvAdDate(this.lblGengoTo.Text, this.txtYearTo.Text,
                    this.txtMonthTo.Text, this.txtDayTo.Text, this.Dba);
            // タイトル設定
            for (int i = 0; i < 13; i++)
            {
                k = new KomokuInfo();
                if (i != 0 && Util.ToInt(tmpJpFr.ToString("yyyyMM")) <= Util.ToInt(tmpJpTo.ToString("yyyyMM")))
                {
                    string[] tmpjpDate = Util.ConvJpDate(tmpJpFr, this.Dba);
                    k.Name = tmpjpDate[2] + "年" + tmpjpDate[3] + "月";
                    k.Komoku = tmpJpFr.ToString("yyyyMM");
                    tmpJpFr = tmpJpFr.AddMonths(1);
                }
                else
                {
                    k.Name = "";
                    k.Komoku = "";
                }
                this._Title.Add(k);
            }

            // 出力項目設定
            this._Komoku = new List<KomokuInfo>();
            k = new KomokuInfo();
            DataTable t = null;
            if (GetKomokuR(ref t))
            {
                int posNo = -1;
                foreach (DataRow r in t.Rows)
                {
                    if (posNo != Util.ToInt(r["POSITION"]))
                    {
                        if (posNo != -1)
                            this._Komoku.Add(k);
                        k = new KomokuInfo();
                        posNo = Util.ToInt(r["POSITION"]);
                        k.Komoku = Util.ToString(r["SHUKEI_KOMOKU"]);
                        k.Name = Util.ToString(r["TITLE"]);
                        k.Code = Util.ToString(r["HENSHU_KUBUN"]);
                    }
                    if (k.SumKmk == null)
                    {
                        k.SumKmk = new List<string>();
                        k.SumKbn = new List<int>();
                    }
                    k.SumKmk.Add(Util.ToString(r["SHUKEI_KOMOKU"]));
                    k.SumKbn.Add(Util.ToString(r["SHUKEI_KOMOKU"]) == "-" ? -1 : 1);
                }
                this._Komoku.Add(k);
            }
            if (this._Komoku.Count == 0)
                return false;

            // 集計キー設定
            this._Key = new List<KomokuInfo>();
            string KeyTitle = "";
            if (GetKeyR(ref t))
            {
                foreach (DataRow r in t.Rows)
                {
                    KeyTitle += Util.ToString(r["KOMOKU_NM"]) + "別";
                    k = new KomokuInfo();
                    k.Komoku = Util.ToString(r["SHUKEI_KOMOKU"]);
                    k.Name = Util.ToString(r["SHUKEI_KOMOKU2"]);
                    this._Key.Add(k);
                }
                this._KeyFlg = true;
            }

            // 合計集計設定
            this._Total = new List<TotalInfo>();
            for (int i = 0; i < this._Key.Count + 1; i++)
            {
                TotalInfo z = new TotalInfo();
                z.LineR = new List<LineInfo>();
                for (int j = 0; j < this._Komoku.Count; j++)
                {
                    z.LineR.Add(new LineInfo());
                }
                this._Total.Add(z);
            }

            // 推移表ﾀｲﾄﾙ情報取得
            this._Title[0].Komoku = "セリ推移表";
            this._Title[0].Name = "項　　目　　名";
            this._Title[0].KeyName = "";
            if (GetTitlR(0, ref t))
                this._Title[0].Komoku = Util.ToString(t.Rows[0]["TITLE"]);
            if (GetTitlR(1, ref t))
                this._Title[0].Name = Util.ToString(t.Rows[0]["TITLE"]);
            if (KeyTitle != "")
                this._Title[0].KeyName = "【" + KeyTitle + "】";

            return true;
        }
        #endregion

        #endregion

        /// <summary>
        /// ファイル保存先の表示
        /// </summary>
        /// <param name="fName"></param>
        /// <param name="filetype"></param>
        /// <returns></returns>
        private string GetSavePath(string fName, decimal filetype)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.FileName = fName;
            string fType = "";
            if (filetype == 1)
            {
                fType = "PDF";
            }
            else
            {
                fType = "EXCEL";
            }
            string folderName = @"C:\";
            try
            {
                folderName = this.Config.LoadPgConfig(Constants.SubSys.Han, "OUTPUT", fType, "PATH");
            }
            catch (Exception)
            {
                folderName = @"C:\";
            }
            if (ValChk.IsEmpty(folderName))
            {
                folderName = @"C:\";
            }
            sfd.InitialDirectory = folderName;

            sfd.AddExtension = true;

            if (filetype == 1)
            {
                sfd.Title = "PDFファイルの保存";
                sfd.Filter = "全てのファイル(*.*)|*.*|" +
                             "ＰＤＦファイル(*.pdf)|*.pdf";
            }
            else
            {
                sfd.Title = "EXCELファイルの保存";
                sfd.Filter = "全てのファイル(*.*)|*.*|" +
                             "Excelファイル(*.xlsx)|*.xlsx";
            }
            sfd.FilterIndex = 2;
            DialogResult ret = sfd.ShowDialog();
            if (ret == DialogResult.OK)
            {
                folderName = System.IO.Path.GetDirectoryName(sfd.FileName);
                try
                {
                    this.Config.SetPgConfig(Constants.SubSys.Han, "OUTPUT", fType, "PATH", folderName);
                    this.Config.SaveConfig();
                }
                catch (Exception) { }
                return sfd.FileName;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }
        #endregion
    }
}
