﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1051
{
    /// <summary>
    /// 魚種マスタメンテ(変更・追加)(HNCM1052)
    /// </summary>
    public partial class HNCM1052 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1052()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1052(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            this.txtMizuageShishoCd.Text = this.Par2;
            // 引数：Par1／モード(1:新規、2:変更)、InData：担当者コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShiwakeKbn":
                case "txtJimotoShiwakeCd":
                case "txtGyosyuBunrui":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            System.Reflection.Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtShiwakeKbn":
                case "txtJimotoShiwakeCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2051.CMCM2053");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.ActiveCtlNm == "txtShiwakeKbn")
                            {
                                frm.InData = this.txtShiwakeKbn.Text;
                            }
                            else if (this.ActiveCtlNm == "txtJimotoShiwakeCd")
                            {
                                frm.InData = this.txtJimotoShiwakeCd.Text;
                            }
                            frm.Par1 = "3";
                            frm.Par2 = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtShiwakeKbn")
                                {
                                    this.txtShiwakeKbn.Text = outData[0];
                                    this.lblShiwakeKbnNm.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtJimotoShiwakeCd")
                                {
                                    this.txtJimotoShiwakeCd.Text = outData[0];
                                    this.lblJimotoShiwakeNm.Text = outData[1];
                                }

                            }
                        }
                    }
                    break;
                
                case "txtGyosyuBunrui":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "HNCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1041.HNCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.txtGyosyuBunrui.Text;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtGyosyuBunrui.Text = outData[0];
                                this.lblGyosyuBunruiNm.Text = outData[1];

                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (MODE_NEW.Equals(Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                DbParamCollection whereParam;
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, this.txtGyosyuCd.Text);
                this.Dba.Delete("TB_HN_GYOSHU",
                    "KAISHA_CD = @KAISHA_CD AND GYOSHU_CD = @GYOSHU_CD ",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTanto = SetCmTantoParams();

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.商品マスタ
                    this.Dba.Insert("TB_HN_GYOSHU", (DbParamCollection)alParamsCmTanto[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.商品マスタ
                    this.Dba.Update("TB_HN_GYOSHU",
                        (DbParamCollection)alParamsCmTanto[1],
                        "KAISHA_CD = @KAISHA_CD AND GYOSHU_CD = @GYOSHU_CD ",
                        (DbParamCollection)alParamsCmTanto[0]);

                }

                // トランザクションをコミット
                this.Dba.Commit();

            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 魚種CDの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyosyuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuCd())
            {
                e.Cancel = true;
                this.txtGyosyuCd.SelectAll();
            }
        }

        /// <summary>
        /// 魚種名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyosyuNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuNm())
            {
                e.Cancel = true;
                this.txtGyosyuNm.SelectAll();
            }
        }

        /// <summary>
        /// 魚種カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyosyuKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuKanaNm())
            {
                e.Cancel = true;
                this.txtGyosyuKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 仕訳区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiwakeKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiwakeKbn())
            {
                e.Cancel = true;
                this.txtShiwakeKbn.SelectAll();
            }
        }

        /// <summary>
        /// 地元仕訳コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJimotoShiwakeCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJimotoShiwake())
            {
                e.Cancel = true;
                this.txtJimotoShiwakeCd.SelectAll();
            }
        }

        /// <summary>
        /// 魚種分類コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyosyuBunrui_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuBunrui())
            {
                e.Cancel = true;
                this.txtGyosyuBunrui.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyosyuBunrui_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 魚種CDの初期値を取得
            // 魚種の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            //where.Append(" AND BARCODE1 = @BARCODE1");
            DataTable dtMaxTanto =
                this.Dba.GetDataTableByConditionWithParams("MAX(GYOSHU_CD) AS MAX_CD",
                    "TB_HN_GYOSHU", Util.ToString(where), dpc);
            if (dtMaxTanto.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxTanto.Rows[0]["MAX_CD"]))
            {
                this.txtGyosyuCd.Text = Util.ToString(Util.ToInt(dtMaxTanto.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtGyosyuCd.Text = "1";
            }

            //// 仕訳区分の初期値を設定
            //this.txtShiwakeKbn.Text = "0";
            //// 地元仕訳コードの初期値を設定
            //this.txtJimotoShiwakeCd.Text = "0";
            // 仕訳区分の初期値を設定
            string ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShiwakeCd"));
            this.txtShiwakeKbn.Text = ret;
            this.lblShiwakeKbnNm.Text = this.GetZidShiwakeSetteiBNm(this.txtShiwakeKbn.Text);
            // 地元仕訳コードの初期値を設定
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "JimotoShiwakeCd"));
            this.txtJimotoShiwakeCd.Text = ret;
            this.lblJimotoShiwakeNm.Text = this.GetZidShiwakeSetteiBNm(this.txtJimotoShiwakeCd.Text);

            // 魚種分類の初期値を設定
            this.txtGyosyuBunrui.Text = "0";

            // 魚種CDに初期フォーカス
            this.ActiveControl = this.txtGyosyuCd;
            this.txtGyosyuCd.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append("    ,GYOSHU_CD");
            cols.Append("    ,GYOSHU_NM");
            cols.Append("    ,GYOSHU_KANA_NM");
            cols.Append("    ,SERI_SHIWAKE_CD");
            cols.Append("    ,JIMOTO_SHIWAKE_CD");
            cols.Append("    ,GYOSHU_BUNRUI_CD");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_GYOSHU");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND GYOSHU_CD = @GYOSHU_CD ",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtGyosyuCd.Text = Util.ToString(drDispData["GYOSHU_CD"]);
            this.txtGyosyuNm.Text = Util.ToString(drDispData["GYOSHU_NM"]);
            this.txtGyosyuKanaNm.Text = Util.ToString(drDispData["GYOSHU_KANA_NM"]);
            this.txtShiwakeKbn.Text = Util.ToString(drDispData["SERI_SHIWAKE_CD"]);
            this.lblShiwakeKbnNm.Text = this.GetZidShiwakeSetteiBNm(this.txtShiwakeKbn.Text);
            this.txtJimotoShiwakeCd.Text = Util.ToString(drDispData["JIMOTO_SHIWAKE_CD"]);
            this.lblJimotoShiwakeNm.Text = this.GetZidShiwakeSetteiBNm(this.txtJimotoShiwakeCd.Text);
            this.txtGyosyuBunrui.Text = Util.ToString(drDispData["GYOSHU_BUNRUI_CD"]);
            this.lblGyosyuBunruiNm.Text = this.GetGyoshuBunruiMstNm(this.txtGyosyuBunrui.Text);

            // 魚種CDは入力不可
            this.txtGyosyuCd.Focus();
            this.txtGyosyuCd.Enabled = false;
            this.ActiveControl = this.txtGyosyuCd;
            this.txtGyosyuNm.Focus();

            //削除ボタンの制御
            this.btnF3.Enabled = IsValidGyosyuCd(this.txtGyosyuCd.Text);
        }

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            string shishoCd = this.txtMizuageShishoCd.Text;
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 魚種CDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuCd()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtGyosyuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyosyuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 既に存在するコードを入力した場合はエラーとする
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, this.txtGyosyuCd.Text);
                StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND GYOSHU_CD = @GYOSHU_CD");
                DataTable dtCheck =
                    this.Dba.GetDataTableByConditionWithParams("GYOSHU_CD",
                        "TB_HN_GYOSHU", Util.ToString(where), dpc);
                if (dtCheck.Rows.Count > 0)
                {
                    Msg.Error("既に存在する魚種CDと重複しています。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 魚種CDの存在チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuCd(string Code)
        {
            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, Code);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND GYOSHU_CD = @GYOSHU_CD");
            DataTable dtCheck =
                this.Dba.GetDataTableByConditionWithParams("GYOSHU_CD",
                    "TB_HN_SHIKIRI_MEISAI", Util.ToString(where), dpc);
            if (dtCheck.Rows.Count > 0)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 魚種名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGyosyuNm.Text, this.txtGyosyuNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 魚種カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuKanaNm()
        {
            // 15バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGyosyuKanaNm.Text, this.txtGyosyuKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仕訳区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiwakeKbn()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShiwakeKbn.Text))
            {
                this.txtShiwakeKbn.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiwakeKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtShiwakeKbn.Text))
            {
                this.lblShiwakeKbnNm.Text = string.Empty;
            }
            else
            {
                string ShiwakeKbn = this.GetZidShiwakeSetteiBNm(this.txtShiwakeKbn.Text);
                if (ValChk.IsEmpty(ShiwakeKbn))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblShiwakeKbnNm.Text = ShiwakeKbn;
            }

            return true;
        }

        /// <summary>
        /// 地元仕訳コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJimotoShiwake()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtJimotoShiwakeCd.Text))
            {
                this.txtJimotoShiwakeCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtJimotoShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtJimotoShiwakeCd.Text))
            {
                this.lblJimotoShiwakeNm.Text = string.Empty;
            }
            else
            {
                string JimotoShiwake = this.GetZidShiwakeSetteiBNm(this.txtJimotoShiwakeCd.Text);
                if (ValChk.IsEmpty(JimotoShiwake))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblJimotoShiwakeNm.Text = JimotoShiwake;
            }

            return true;
        }

        /// <summary>
        /// 魚種分類の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuBunrui()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtGyosyuBunrui.Text))
            {
                this.txtGyosyuBunrui.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyosyuBunrui.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtGyosyuBunrui.Text))
            {
                this.lblGyosyuBunruiNm.Text = string.Empty;
            }
            else
            {
                string GyosyuBunrui = this.GetGyoshuBunruiMstNm(this.txtGyosyuBunrui.Text);
                if (ValChk.IsEmpty(GyosyuBunrui))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblGyosyuBunruiNm.Text = GyosyuBunrui;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 魚種CDのチェック
                if (!IsValidGyosyuCd())
                {
                    this.txtGyosyuCd.Focus();
                    return false;
                }
            }

            // 魚種名のチェック
            if (!IsValidGyosyuNm())
            {
                this.txtGyosyuNm.Focus();
                return false;
            }

            // 魚種カナ名のチェック
            if (!IsValidGyosyuKanaNm())
            {
                this.txtGyosyuKanaNm.Focus();
                return false;
            }

            // 仕訳区分のチェック
            if (!IsValidShiwakeKbn())
            {
                this.txtShiwakeKbn.Focus();
                return false;
            }

            // 地元仕訳コードのチェック
            if (!IsValidJimotoShiwake())
            {
                this.txtJimotoShiwakeCd.Focus();
                return false;
            }

            // 仕訳区分のチェック
            if (!IsValidGyosyuBunrui())
            {
                this.txtGyosyuBunrui.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_GYOSHUに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmTantoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと魚種コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                updParam.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, this.txtGyosyuCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと担当者コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                whereParam.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, this.txtGyosyuCd.Text);
                alParams.Add(whereParam);
            }

            // 魚種名
            updParam.SetParam("@GYOSHU_NM", SqlDbType.VarChar, 40, this.txtGyosyuNm.Text);
            // 魚種カナ名
            updParam.SetParam("@GYOSHU_KANA_NM", SqlDbType.VarChar, 30, this.txtGyosyuKanaNm.Text);
            // 仕訳区分
            updParam.SetParam("@SERI_SHIWAKE_CD", SqlDbType.Decimal, 6, this.txtShiwakeKbn.Text);
            // 地元仕訳コード
            updParam.SetParam("@JIMOTO_SHIWAKE_CD", SqlDbType.Decimal, 6, this.txtJimotoShiwakeCd.Text);
            // 地元仕訳コード
            updParam.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.Decimal, 6, this.txtGyosyuBunrui.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 自動仕訳設定Ｂテーブルから仕訳名を取得
        /// </summary>
        /// <param name="code">仕訳コード</param>
        /// <returns>
        /// 仕訳名
        /// </returns>
        private string GetZidShiwakeSetteiBNm(string code)
        {
            string ret = "";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 3);
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 6, code);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
            where.Append(" AND SHIWAKE_CD = @SHIWAKE_CD");
            DataTable dtNm =
                this.Dba.GetDataTableByConditionWithParams("SHIWAKE_NM AS NM",
                    "VI_HN_ZIDO_SHIWAKE_SETTEI_B", Util.ToString(where), dpc);
            if (dtNm.Rows.Count > 0 && !ValChk.IsEmpty(dtNm.Rows[0]["NM"]))
            {
                ret = Util.ToString(dtNm.Rows[0]["NM"]);
            }
            
            return ret;
        }

        /// <summary>
        /// 魚種分類マスタテーブルから魚種分類名を取得
        /// </summary>
        /// <param name="code">魚種分類CD</param>
        /// <returns>
        /// 魚種分類名
        /// </returns>
        private string GetGyoshuBunruiMstNm(string code)
        {
            string ret = "";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.Decimal, 7, code);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND GYOSHU_BUNRUI_CD = @GYOSHU_BUNRUI_CD");
            DataTable dtNm =
                this.Dba.GetDataTableByConditionWithParams("GYOSHU_BUNRUI_NM AS NM",
                    "TB_HN_GYOSHU_BUNRUI_MST", Util.ToString(where), dpc);
            if (dtNm.Rows.Count > 0 && !ValChk.IsEmpty(dtNm.Rows[0]["NM"]))
            {
                ret = Util.ToString(dtNm.Rows[0]["NM"]);
            }

            return ret;
        }
        #endregion

    }
}
