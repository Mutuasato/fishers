﻿namespace jp.co.fsi.hn.hncm1051
{
    partial class HNCM1054
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.lblJimotoShiwakeNm = new System.Windows.Forms.Label();
			this.txtJimotoShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblJimotoShiwakeCd = new System.Windows.Forms.Label();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.lblShiwakeKbnNm = new System.Windows.Forms.Label();
			this.txtShiwakeKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiwakeKbn = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 117);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(556, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(545, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 34);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(521, 77);
			this.fsiTableLayoutPanel1.TabIndex = 1002;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblJimotoShiwakeNm);
			this.fsiPanel2.Controls.Add(this.txtJimotoShiwakeCd);
			this.fsiPanel2.Controls.Add(this.lblJimotoShiwakeCd);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 42);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(513, 31);
			this.fsiPanel2.TabIndex = 1;
			// 
			// lblJimotoShiwakeNm
			// 
			this.lblJimotoShiwakeNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblJimotoShiwakeNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblJimotoShiwakeNm.Location = new System.Drawing.Point(206, 4);
			this.lblJimotoShiwakeNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJimotoShiwakeNm.Name = "lblJimotoShiwakeNm";
			this.lblJimotoShiwakeNm.Size = new System.Drawing.Size(281, 24);
			this.lblJimotoShiwakeNm.TabIndex = 17;
			this.lblJimotoShiwakeNm.Tag = "DISPNAME";
			this.lblJimotoShiwakeNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtJimotoShiwakeCd
			// 
			this.txtJimotoShiwakeCd.AutoSizeFromLength = false;
			this.txtJimotoShiwakeCd.DisplayLength = null;
			this.txtJimotoShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtJimotoShiwakeCd.Location = new System.Drawing.Point(129, 4);
			this.txtJimotoShiwakeCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtJimotoShiwakeCd.MaxLength = 6;
			this.txtJimotoShiwakeCd.Name = "txtJimotoShiwakeCd";
			this.txtJimotoShiwakeCd.Size = new System.Drawing.Size(69, 23);
			this.txtJimotoShiwakeCd.TabIndex = 16;
			this.txtJimotoShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblJimotoShiwakeCd
			// 
			this.lblJimotoShiwakeCd.BackColor = System.Drawing.Color.Silver;
			this.lblJimotoShiwakeCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJimotoShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblJimotoShiwakeCd.Location = new System.Drawing.Point(0, 0);
			this.lblJimotoShiwakeCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJimotoShiwakeCd.Name = "lblJimotoShiwakeCd";
			this.lblJimotoShiwakeCd.Size = new System.Drawing.Size(513, 31);
			this.lblJimotoShiwakeCd.TabIndex = 15;
			this.lblJimotoShiwakeCd.Tag = "CHANGE";
			this.lblJimotoShiwakeCd.Text = "地元仕訳コード";
			this.lblJimotoShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.lblShiwakeKbnNm);
			this.fsiPanel1.Controls.Add(this.txtShiwakeKbn);
			this.fsiPanel1.Controls.Add(this.lblShiwakeKbn);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(513, 31);
			this.fsiPanel1.TabIndex = 0;
			// 
			// lblShiwakeKbnNm
			// 
			this.lblShiwakeKbnNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblShiwakeKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShiwakeKbnNm.Location = new System.Drawing.Point(208, 3);
			this.lblShiwakeKbnNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiwakeKbnNm.Name = "lblShiwakeKbnNm";
			this.lblShiwakeKbnNm.Size = new System.Drawing.Size(281, 24);
			this.lblShiwakeKbnNm.TabIndex = 14;
			this.lblShiwakeKbnNm.Tag = "DISPNAME";
			this.lblShiwakeKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiwakeKbn
			// 
			this.txtShiwakeKbn.AutoSizeFromLength = false;
			this.txtShiwakeKbn.DisplayLength = null;
			this.txtShiwakeKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShiwakeKbn.Location = new System.Drawing.Point(129, 4);
			this.txtShiwakeKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiwakeKbn.MaxLength = 6;
			this.txtShiwakeKbn.Name = "txtShiwakeKbn";
			this.txtShiwakeKbn.Size = new System.Drawing.Size(69, 23);
			this.txtShiwakeKbn.TabIndex = 13;
			this.txtShiwakeKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblShiwakeKbn
			// 
			this.lblShiwakeKbn.BackColor = System.Drawing.Color.Silver;
			this.lblShiwakeKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShiwakeKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShiwakeKbn.Location = new System.Drawing.Point(0, 0);
			this.lblShiwakeKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiwakeKbn.Name = "lblShiwakeKbn";
			this.lblShiwakeKbn.Size = new System.Drawing.Size(513, 31);
			this.lblShiwakeKbn.TabIndex = 12;
			this.lblShiwakeKbn.Tag = "CHANGE";
			this.lblShiwakeKbn.Text = "仕　訳　区　分";
			this.lblShiwakeKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// HNCM1054
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(545, 254);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1054";
			this.ShowFButton = true;
			this.Text = "魚種マスタ初期設定";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label lblJimotoShiwakeNm;
        private common.controls.FsiTextBox txtJimotoShiwakeCd;
        private System.Windows.Forms.Label lblJimotoShiwakeCd;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label lblShiwakeKbnNm;
        private common.controls.FsiTextBox txtShiwakeKbn;
        private System.Windows.Forms.Label lblShiwakeKbn;
    }
}