﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.hn.hndr1051
{
    /// <summary>
    /// HNDR1051R の帳票
    /// </summary>
    public partial class HNDR1051R : BaseReport
    {

        public HNDR1051R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }

        private void groupFooter1_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            this.txtValue08Total.Text = Util.FormatNum(this.txtValue08Total.Text);
            this.txtValue09Total.Text = Util.FormatNum(this.txtValue09Total.Text);
            this.txtValue10Total.Text = Util.FormatNum(this.txtValue10Total.Text);
            // 合計金額が0の場合、空表示とする
            if (this.txtValue08Total.Text == "0")
            {
                this.txtValue08Total.Text = "";
            }
            if (this.txtValue09Total.Text == "0")
            {
                this.txtValue09Total.Text = "";
            }
            if (this.txtValue10Total.Text == "0")
            {
                this.txtValue10Total.Text = "";
            }
        }
    }
}
