﻿namespace jp.co.fsi.hn.hnmr1021
{
    /// <summary>
    /// HNMR1021R の概要の説明です。
    /// </summary>
    partial class HNMR1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNMR1021R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTikuNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGyoshu = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGyoshuCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGyoshuNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblJojun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTyujun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGejun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGokei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbltitle02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDateFr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.page = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.date = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblSogokei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSogokei01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSogokei12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblTyukei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTyukei01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyukei12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblShokei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtShokei01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokei12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTikuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJojun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyujun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGejun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbltitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSogokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyukei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle01,
            this.lblTikuNm,
            this.lblGyoshu,
            this.lblGyoshuCd,
            this.lblGyoshuNm,
            this.lblJojun,
            this.lblSekisu01,
            this.lblSuryo01,
            this.lblKingaku01,
            this.lblTyujun,
            this.lblSekisu02,
            this.lblSuryo02,
            this.lblKingaku02,
            this.lblGejun,
            this.lblSekisu03,
            this.lblSuryo03,
            this.lblKingaku03,
            this.lblGokei,
            this.lblSekisu04,
            this.lblSuryo04,
            this.lblKingaku04,
            this.lblTitle03,
            this.lblPage,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.lbltitle02,
            this.txtDateFr,
            this.txtDateTo,
            this.page,
            this.date});
            this.pageHeader.Height = 1.241518F;
            this.pageHeader.Name = "pageHeader";
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.2625984F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 4.950788F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold";
            this.lblTitle01.Text = "＊＊ 地区別漁業種別月計表 ＊＊";
            this.lblTitle01.Top = 0F;
            this.lblTitle01.Width = 3.208661F;
            // 
            // lblTikuNm
            // 
            this.lblTikuNm.Height = 0.2F;
            this.lblTikuNm.HyperLink = null;
            this.lblTikuNm.Left = 0.1559055F;
            this.lblTikuNm.Name = "lblTikuNm";
            this.lblTikuNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; vertical-align: middle";
            this.lblTikuNm.Text = "＜＜  地区名  ＞＞";
            this.lblTikuNm.Top = 0.5940945F;
            this.lblTikuNm.Width = 1.302362F;
            // 
            // lblGyoshu
            // 
            this.lblGyoshu.Height = 0.2F;
            this.lblGyoshu.HyperLink = null;
            this.lblGyoshu.Left = 0.218504F;
            this.lblGyoshu.Name = "lblGyoshu";
            this.lblGyoshu.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold";
            this.lblGyoshu.Text = "☆  漁業種  ☆";
            this.lblGyoshu.Top = 0.856693F;
            this.lblGyoshu.Width = 1.239764F;
            // 
            // lblGyoshuCd
            // 
            this.lblGyoshuCd.Height = 0.2F;
            this.lblGyoshuCd.HyperLink = null;
            this.lblGyoshuCd.Left = 0.06181103F;
            this.lblGyoshuCd.Name = "lblGyoshuCd";
            this.lblGyoshuCd.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblGyoshuCd.Text = "魚種CD";
            this.lblGyoshuCd.Top = 1.056693F;
            this.lblGyoshuCd.Width = 0.5413386F;
            // 
            // lblGyoshuNm
            // 
            this.lblGyoshuNm.Height = 0.2F;
            this.lblGyoshuNm.HyperLink = null;
            this.lblGyoshuNm.Left = 0.6716536F;
            this.lblGyoshuNm.Name = "lblGyoshuNm";
            this.lblGyoshuNm.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold";
            this.lblGyoshuNm.Text = "魚種名";
            this.lblGyoshuNm.Top = 1.056693F;
            this.lblGyoshuNm.Width = 0.5724412F;
            // 
            // lblJojun
            // 
            this.lblJojun.Height = 0.2F;
            this.lblJojun.HyperLink = null;
            this.lblJojun.Left = 3.275197F;
            this.lblJojun.Name = "lblJojun";
            this.lblJojun.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center";
            this.lblJojun.Text = "上旬";
            this.lblJojun.Top = 0.7940945F;
            this.lblJojun.Width = 0.4271657F;
            // 
            // lblSekisu01
            // 
            this.lblSekisu01.Height = 0.2F;
            this.lblSekisu01.HyperLink = null;
            this.lblSekisu01.Left = 2.409055F;
            this.lblSekisu01.Name = "lblSekisu01";
            this.lblSekisu01.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblSekisu01.Text = "隻数";
            this.lblSekisu01.Top = 0.9940946F;
            this.lblSekisu01.Width = 0.364567F;
            // 
            // lblSuryo01
            // 
            this.lblSuryo01.Height = 0.2F;
            this.lblSuryo01.HyperLink = null;
            this.lblSuryo01.Left = 3.111418F;
            this.lblSuryo01.Name = "lblSuryo01";
            this.lblSuryo01.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblSuryo01.Text = "数量";
            this.lblSuryo01.Top = 0.9940946F;
            this.lblSuryo01.Width = 0.4208665F;
            // 
            // lblKingaku01
            // 
            this.lblKingaku01.Height = 0.2F;
            this.lblKingaku01.HyperLink = null;
            this.lblKingaku01.Left = 4.110237F;
            this.lblKingaku01.Name = "lblKingaku01";
            this.lblKingaku01.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblKingaku01.Text = "金額";
            this.lblKingaku01.Top = 0.9940946F;
            this.lblKingaku01.Width = 0.4582677F;
            // 
            // lblTyujun
            // 
            this.lblTyujun.Height = 0.2F;
            this.lblTyujun.HyperLink = null;
            this.lblTyujun.Left = 6.041339F;
            this.lblTyujun.Name = "lblTyujun";
            this.lblTyujun.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center";
            this.lblTyujun.Text = "中旬";
            this.lblTyujun.Top = 0.7940945F;
            this.lblTyujun.Width = 0.4271655F;
            // 
            // lblSekisu02
            // 
            this.lblSekisu02.Height = 0.2F;
            this.lblSekisu02.HyperLink = null;
            this.lblSekisu02.Left = 5.096063F;
            this.lblSekisu02.Name = "lblSekisu02";
            this.lblSekisu02.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblSekisu02.Text = "隻数";
            this.lblSekisu02.Top = 0.9940946F;
            this.lblSekisu02.Width = 0.4480314F;
            // 
            // lblSuryo02
            // 
            this.lblSuryo02.Height = 0.2F;
            this.lblSuryo02.HyperLink = null;
            this.lblSuryo02.Left = 5.83937F;
            this.lblSuryo02.Name = "lblSuryo02";
            this.lblSuryo02.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblSuryo02.Text = "数量";
            this.lblSuryo02.Top = 0.9940946F;
            this.lblSuryo02.Width = 0.427165F;
            // 
            // lblKingaku02
            // 
            this.lblKingaku02.Height = 0.2F;
            this.lblKingaku02.HyperLink = null;
            this.lblKingaku02.Left = 6.840552F;
            this.lblKingaku02.Name = "lblKingaku02";
            this.lblKingaku02.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblKingaku02.Text = "金額";
            this.lblKingaku02.Top = 0.9940946F;
            this.lblKingaku02.Width = 0.4480319F;
            // 
            // lblGejun
            // 
            this.lblGejun.Height = 0.2F;
            this.lblGejun.HyperLink = null;
            this.lblGejun.Left = 8.812205F;
            this.lblGejun.Name = "lblGejun";
            this.lblGejun.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center";
            this.lblGejun.Text = "下旬";
            this.lblGejun.Top = 0.7940945F;
            this.lblGejun.Width = 0.4271655F;
            // 
            // lblSekisu03
            // 
            this.lblSekisu03.Height = 0.2F;
            this.lblSekisu03.HyperLink = null;
            this.lblSekisu03.Left = 7.919292F;
            this.lblSekisu03.Name = "lblSekisu03";
            this.lblSekisu03.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblSekisu03.Text = "隻数";
            this.lblSekisu03.Top = 0.9940946F;
            this.lblSekisu03.Width = 0.427165F;
            // 
            // lblSuryo03
            // 
            this.lblSuryo03.Height = 0.2F;
            this.lblSuryo03.HyperLink = null;
            this.lblSuryo03.Left = 8.682284F;
            this.lblSuryo03.Name = "lblSuryo03";
            this.lblSuryo03.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblSuryo03.Text = "数量";
            this.lblSuryo03.Top = 0.9940946F;
            this.lblSuryo03.Width = 0.3748026F;
            // 
            // lblKingaku03
            // 
            this.lblKingaku03.Height = 0.2F;
            this.lblKingaku03.HyperLink = null;
            this.lblKingaku03.Left = 9.646852F;
            this.lblKingaku03.Name = "lblKingaku03";
            this.lblKingaku03.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblKingaku03.Text = "金額";
            this.lblKingaku03.Top = 0.9940946F;
            this.lblKingaku03.Width = 0.4586601F;
            // 
            // lblGokei
            // 
            this.lblGokei.Height = 0.2F;
            this.lblGokei.HyperLink = null;
            this.lblGokei.Left = 11.73268F;
            this.lblGokei.Name = "lblGokei";
            this.lblGokei.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center";
            this.lblGokei.Text = "合計";
            this.lblGokei.Top = 0.7940946F;
            this.lblGokei.Width = 0.4271651F;
            // 
            // lblSekisu04
            // 
            this.lblSekisu04.Height = 0.2F;
            this.lblSekisu04.HyperLink = null;
            this.lblSekisu04.Left = 10.6563F;
            this.lblSekisu04.Name = "lblSekisu04";
            this.lblSekisu04.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblSekisu04.Text = "隻数";
            this.lblSekisu04.Top = 0.9940946F;
            this.lblSekisu04.Width = 0.4897671F;
            // 
            // lblSuryo04
            // 
            this.lblSuryo04.Height = 0.2F;
            this.lblSuryo04.HyperLink = null;
            this.lblSuryo04.Left = 11.46535F;
            this.lblSuryo04.Name = "lblSuryo04";
            this.lblSuryo04.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblSuryo04.Text = "数量";
            this.lblSuryo04.Top = 0.9940946F;
            this.lblSuryo04.Width = 0.4165316F;
            // 
            // lblKingaku04
            // 
            this.lblKingaku04.Height = 0.2F;
            this.lblKingaku04.HyperLink = null;
            this.lblKingaku04.Left = 12.64055F;
            this.lblKingaku04.Name = "lblKingaku04";
            this.lblKingaku04.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right";
            this.lblKingaku04.Text = "金額";
            this.lblKingaku04.Top = 0.9940946F;
            this.lblKingaku04.Width = 0.4208679F;
            // 
            // lblTitle03
            // 
            this.lblTitle03.Height = 0.2F;
            this.lblTitle03.HyperLink = null;
            this.lblTitle03.Left = 11.37599F;
            this.lblTitle03.Name = "lblTitle03";
            this.lblTitle03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center";
            this.lblTitle03.Text = "【税抜き】";
            this.lblTitle03.Top = 0F;
            this.lblTitle03.Visible = false;
            this.lblTitle03.Width = 1F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 12.36024F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.2F;
            this.lblPage.Width = 0.2448874F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.409055F;
            this.line1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line1.LineWeight = 3F;
            this.line1.Name = "line1";
            this.line1.Top = 0.856693F;
            this.line1.Width = 0.866142F;
            this.line1.X1 = 2.409055F;
            this.line1.X2 = 3.275197F;
            this.line1.Y1 = 0.856693F;
            this.line1.Y2 = 0.856693F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 3.702363F;
            this.line2.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line2.LineWeight = 3F;
            this.line2.Name = "line2";
            this.line2.Top = 0.856693F;
            this.line2.Width = 0.8661408F;
            this.line2.X1 = 3.702363F;
            this.line2.X2 = 4.568504F;
            this.line2.Y1 = 0.856693F;
            this.line2.Y2 = 0.856693F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 5.175197F;
            this.line3.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line3.LineWeight = 3F;
            this.line3.Name = "line3";
            this.line3.Top = 0.856693F;
            this.line3.Width = 0.8661418F;
            this.line3.X1 = 5.175197F;
            this.line3.X2 = 6.041339F;
            this.line3.Y1 = 0.856693F;
            this.line3.Y2 = 0.856693F;
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 6.468504F;
            this.line4.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line4.LineWeight = 3F;
            this.line4.Name = "line4";
            this.line4.Top = 0.856693F;
            this.line4.Width = 0.8661423F;
            this.line4.X1 = 6.468504F;
            this.line4.X2 = 7.334646F;
            this.line4.Y1 = 0.856693F;
            this.line4.Y2 = 0.856693F;
            // 
            // line5
            // 
            this.line5.Height = 0F;
            this.line5.Left = 7.946064F;
            this.line5.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line5.LineWeight = 3F;
            this.line5.Name = "line5";
            this.line5.Top = 0.856693F;
            this.line5.Width = 0.8661413F;
            this.line5.X1 = 7.946064F;
            this.line5.X2 = 8.812205F;
            this.line5.Y1 = 0.856693F;
            this.line5.Y2 = 0.856693F;
            // 
            // line6
            // 
            this.line6.Height = 0F;
            this.line6.Left = 9.23937F;
            this.line6.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line6.LineWeight = 3F;
            this.line6.Name = "line6";
            this.line6.Top = 0.856693F;
            this.line6.Width = 0.8661394F;
            this.line6.X1 = 9.23937F;
            this.line6.X2 = 10.10551F;
            this.line6.Y1 = 0.856693F;
            this.line6.Y2 = 0.856693F;
            // 
            // line7
            // 
            this.line7.Height = 0F;
            this.line7.Left = 10.78386F;
            this.line7.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line7.LineWeight = 3F;
            this.line7.Name = "line7";
            this.line7.Top = 0.856693F;
            this.line7.Width = 0.8661394F;
            this.line7.X1 = 10.78386F;
            this.line7.X2 = 11.65F;
            this.line7.Y1 = 0.856693F;
            this.line7.Y2 = 0.856693F;
            // 
            // line8
            // 
            this.line8.Height = 0F;
            this.line8.Left = 12.15984F;
            this.line8.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line8.LineWeight = 3F;
            this.line8.Name = "line8";
            this.line8.Top = 0.856693F;
            this.line8.Width = 0.8661404F;
            this.line8.X1 = 12.15984F;
            this.line8.X2 = 13.02598F;
            this.line8.Y1 = 0.856693F;
            this.line8.Y2 = 0.856693F;
            // 
            // lbltitle02
            // 
            this.lbltitle02.Height = 0.2F;
            this.lbltitle02.HyperLink = null;
            this.lbltitle02.Left = 1.458268F;
            this.lbltitle02.Name = "lbltitle02";
            this.lbltitle02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal";
            this.lbltitle02.Text = "～";
            this.lbltitle02.Top = 0.2401575F;
            this.lbltitle02.Width = 0.1874015F;
            // 
            // txtDateFr
            // 
            this.txtDateFr.DataField = "ITEM01";
            this.txtDateFr.Height = 0.2F;
            this.txtDateFr.Left = 0.2185038F;
            this.txtDateFr.MultiLine = false;
            this.txtDateFr.Name = "txtDateFr";
            this.txtDateFr.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.txtDateFr.Text = null;
            this.txtDateFr.Top = 0.2401575F;
            this.txtDateFr.Width = 1.156693F;
            // 
            // txtDateTo
            // 
            this.txtDateTo.DataField = "ITEM02";
            this.txtDateTo.Height = 0.2F;
            this.txtDateTo.Left = 1.74685F;
            this.txtDateTo.MultiLine = false;
            this.txtDateTo.Name = "txtDateTo";
            this.txtDateTo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.txtDateTo.Text = null;
            this.txtDateTo.Top = 0.2401575F;
            this.txtDateTo.Width = 1.25F;
            // 
            // page
            // 
            this.page.Height = 0.2F;
            this.page.Left = 11.81732F;
            this.page.Name = "page";
            this.page.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.page.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.page.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page.Text = "textBox4";
            this.page.Top = 0.2F;
            this.page.Width = 0.388586F;
            // 
            // date
            // 
            this.date.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.date.Height = 0.2F;
            this.date.Left = 10.73898F;
            this.date.MultiLine = false;
            this.date.Name = "date";
            this.date.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.date.Top = 0.2F;
            this.date.Width = 1.173232F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM21";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox20,
            this.textBox21,
            this.textBox58,
            this.textBox19});
            this.detail.Height = 0.2291666F;
            this.detail.Name = "detail";
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM07";
            this.textBox9.Height = 0.2F;
            this.textBox9.Left = 0.1559055F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 1";
            this.textBox9.Text = null;
            this.textBox9.Top = 0F;
            this.textBox9.Width = 0.4472442F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM08";
            this.textBox10.Height = 0.2F;
            this.textBox10.Left = 0.6716536F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.textBox10.Text = null;
            this.textBox10.Top = 0F;
            this.textBox10.Width = 1.28189F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM09";
            this.textBox11.Height = 0.2F;
            this.textBox11.Left = 1.953543F;
            this.textBox11.MultiLine = false;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox11.Text = null;
            this.textBox11.Top = 0F;
            this.textBox11.Width = 0.8200791F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM10";
            this.textBox12.Height = 0.2F;
            this.textBox12.Left = 2.712205F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox12.Text = null;
            this.textBox12.Top = 0F;
            this.textBox12.Width = 0.8200784F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM11";
            this.textBox13.Height = 0.2F;
            this.textBox13.Left = 3.650394F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox13.Text = null;
            this.textBox13.Top = 0F;
            this.textBox13.Width = 0.9181099F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM12";
            this.textBox14.Height = 0.2F;
            this.textBox14.Left = 4.719686F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox14.Text = null;
            this.textBox14.Top = 0F;
            this.textBox14.Width = 0.8244095F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM13";
            this.textBox15.Height = 0.2F;
            this.textBox15.Left = 5.498425F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox15.Text = null;
            this.textBox15.Top = 0F;
            this.textBox15.Width = 0.7681103F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM14";
            this.textBox16.Height = 0.2F;
            this.textBox16.Left = 6.234646F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox16.Text = null;
            this.textBox16.Top = 0F;
            this.textBox16.Width = 1.053936F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM15";
            this.textBox17.Height = 0.2F;
            this.textBox17.Left = 7.526379F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox17.Text = null;
            this.textBox17.Top = 0F;
            this.textBox17.Width = 0.8200779F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM16";
            this.textBox18.Height = 0.2F;
            this.textBox18.Left = 8.268505F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox18.Text = null;
            this.textBox18.Top = 0F;
            this.textBox18.Width = 0.7885828F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM18";
            this.textBox20.Height = 0.2F;
            this.textBox20.Left = 10.31811F;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox20.Text = null;
            this.textBox20.Top = 0F;
            this.textBox20.Width = 0.8279524F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM19";
            this.textBox21.Height = 0.2F;
            this.textBox21.Left = 11.06182F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox21.Text = null;
            this.textBox21.Top = 0F;
            this.textBox21.Width = 0.8200788F;
            // 
            // textBox58
            // 
            this.textBox58.DataField = "ITEM20";
            this.textBox58.Height = 0.2F;
            this.textBox58.Left = 12.01181F;
            this.textBox58.Name = "textBox58";
            this.textBox58.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox58.Text = null;
            this.textBox58.Top = 0F;
            this.textBox58.Width = 1.049608F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM17";
            this.textBox19.Height = 0.2F;
            this.textBox19.Left = 9.187009F;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox19.Text = null;
            this.textBox19.Top = 0F;
            this.textBox19.Width = 0.9185038F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSogokei,
            this.txtSogokei01,
            this.txtSogokei02,
            this.txtSogokei03,
            this.txtSogokei04,
            this.txtSogokei05,
            this.txtSogokei06,
            this.txtSogokei07,
            this.txtSogokei08,
            this.txtSogokei09,
            this.txtSogokei10,
            this.txtSogokei11,
            this.txtSogokei12});
            this.reportFooter1.Height = 0.218914F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // lblSogokei
            // 
            this.lblSogokei.Height = 0.2F;
            this.lblSogokei.HyperLink = null;
            this.lblSogokei.Left = 0.218504F;
            this.lblSogokei.Name = "lblSogokei";
            this.lblSogokei.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center";
            this.lblSogokei.Text = "☆  総合計  ☆";
            this.lblSogokei.Top = 0F;
            this.lblSogokei.Width = 1.239764F;
            // 
            // txtSogokei01
            // 
            this.txtSogokei01.DataField = "ITEM09";
            this.txtSogokei01.Height = 0.2F;
            this.txtSogokei01.Left = 1.901575F;
            this.txtSogokei01.Name = "txtSogokei01";
            this.txtSogokei01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei01.Text = "txtSogokei01";
            this.txtSogokei01.Top = 0F;
            this.txtSogokei01.Width = 0.8720473F;
            // 
            // txtSogokei02
            // 
            this.txtSogokei02.DataField = "ITEM10";
            this.txtSogokei02.Height = 0.2F;
            this.txtSogokei02.Left = 2.712205F;
            this.txtSogokei02.Name = "txtSogokei02";
            this.txtSogokei02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei02.Text = "txtSogokei02";
            this.txtSogokei02.Top = 0F;
            this.txtSogokei02.Width = 0.8200791F;
            // 
            // txtSogokei03
            // 
            this.txtSogokei03.DataField = "ITEM11";
            this.txtSogokei03.Height = 0.2F;
            this.txtSogokei03.Left = 3.650394F;
            this.txtSogokei03.Name = "txtSogokei03";
            this.txtSogokei03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei03.Text = "txtSogokei03";
            this.txtSogokei03.Top = 0F;
            this.txtSogokei03.Width = 0.9181099F;
            // 
            // txtSogokei04
            // 
            this.txtSogokei04.DataField = "ITEM12";
            this.txtSogokei04.Height = 0.2F;
            this.txtSogokei04.Left = 4.724016F;
            this.txtSogokei04.Name = "txtSogokei04";
            this.txtSogokei04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei04.Text = "txtSogokei04";
            this.txtSogokei04.Top = 0F;
            this.txtSogokei04.Width = 0.820079F;
            // 
            // txtSogokei05
            // 
            this.txtSogokei05.DataField = "ITEM13";
            this.txtSogokei05.Height = 0.2F;
            this.txtSogokei05.Left = 5.446457F;
            this.txtSogokei05.Name = "txtSogokei05";
            this.txtSogokei05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei05.Text = "txtSogokei05";
            this.txtSogokei05.Top = 0F;
            this.txtSogokei05.Width = 0.8200788F;
            // 
            // txtSogokei06
            // 
            this.txtSogokei06.DataField = "ITEM14";
            this.txtSogokei06.Height = 0.2F;
            this.txtSogokei06.Left = 6.234646F;
            this.txtSogokei06.Name = "txtSogokei06";
            this.txtSogokei06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei06.Text = "txtSogokei06";
            this.txtSogokei06.Top = 0F;
            this.txtSogokei06.Width = 1.053936F;
            // 
            // txtSogokei07
            // 
            this.txtSogokei07.DataField = "ITEM15";
            this.txtSogokei07.Height = 0.2F;
            this.txtSogokei07.Left = 7.526379F;
            this.txtSogokei07.Name = "txtSogokei07";
            this.txtSogokei07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei07.Text = "txtSogokei07";
            this.txtSogokei07.Top = 0F;
            this.txtSogokei07.Width = 0.8200784F;
            // 
            // txtSogokei08
            // 
            this.txtSogokei08.DataField = "ITEM16";
            this.txtSogokei08.Height = 0.2F;
            this.txtSogokei08.Left = 8.268505F;
            this.txtSogokei08.Name = "txtSogokei08";
            this.txtSogokei08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei08.Text = "txtSogokei08";
            this.txtSogokei08.Top = 0F;
            this.txtSogokei08.Width = 0.7885828F;
            // 
            // txtSogokei09
            // 
            this.txtSogokei09.DataField = "ITEM17";
            this.txtSogokei09.Height = 0.2F;
            this.txtSogokei09.Left = 9.187009F;
            this.txtSogokei09.Name = "txtSogokei09";
            this.txtSogokei09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei09.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei09.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei09.Text = "txtSogokei09";
            this.txtSogokei09.Top = 0F;
            this.txtSogokei09.Width = 0.9185038F;
            // 
            // txtSogokei10
            // 
            this.txtSogokei10.DataField = "ITEM18";
            this.txtSogokei10.Height = 0.2F;
            this.txtSogokei10.Left = 10.32598F;
            this.txtSogokei10.Name = "txtSogokei10";
            this.txtSogokei10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei10.Text = "txtSogokei10";
            this.txtSogokei10.Top = 0F;
            this.txtSogokei10.Width = 0.8200788F;
            // 
            // txtSogokei11
            // 
            this.txtSogokei11.DataField = "ITEM19";
            this.txtSogokei11.Height = 0.2F;
            this.txtSogokei11.Left = 11.06182F;
            this.txtSogokei11.Name = "txtSogokei11";
            this.txtSogokei11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei11.Text = "txtSogokei11";
            this.txtSogokei11.Top = 0F;
            this.txtSogokei11.Width = 0.8200788F;
            // 
            // txtSogokei12
            // 
            this.txtSogokei12.DataField = "ITEM20";
            this.txtSogokei12.Height = 0.2F;
            this.txtSogokei12.Left = 12.01181F;
            this.txtSogokei12.Name = "txtSogokei12";
            this.txtSogokei12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSogokei12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSogokei12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSogokei12.Text = "txtSogokei12";
            this.txtSogokei12.Top = 0F;
            this.txtSogokei12.Width = 1.049609F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.textBox5,
            this.textBox6});
            this.groupHeader1.DataField = "ITEM03";
            this.groupHeader1.Height = 0.2291667F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // label1
            // 
            this.label1.Height = 0.1979167F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.06181103F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt";
            this.label1.Text = "＜　　　　　　　　　　　　　　　　　　　＞";
            this.label1.Top = 0.04133859F;
            this.label1.Width = 4.55197F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM03";
            this.textBox5.Height = 0.2F;
            this.textBox5.Left = 2.674016F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.textBox5.Text = null;
            this.textBox5.Top = 0.03937008F;
            this.textBox5.Visible = false;
            this.textBox5.Width = 0.3228347F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM04";
            this.textBox6.Height = 0.2F;
            this.textBox6.Left = 0.3807087F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-decoration: none; ddo-char-set: 128";
            this.textBox6.Text = null;
            this.textBox6.Top = 0.03937008F;
            this.textBox6.Width = 2.894489F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTyukei,
            this.txtTyukei01,
            this.txtTyukei02,
            this.txtTyukei03,
            this.txtTyukei04,
            this.txtTyukei05,
            this.txtTyukei06,
            this.txtTyukei07,
            this.txtTyukei08,
            this.txtTyukei09,
            this.txtTyukei10,
            this.txtTyukei11,
            this.txtTyukei12});
            this.groupFooter1.Height = 0.2604167F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // lblTyukei
            // 
            this.lblTyukei.Height = 0.2F;
            this.lblTyukei.HyperLink = null;
            this.lblTyukei.Left = 0.218504F;
            this.lblTyukei.Name = "lblTyukei";
            this.lblTyukei.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center";
            this.lblTyukei.Text = "☆  中計  ☆";
            this.lblTyukei.Top = 0F;
            this.lblTyukei.Width = 1.094095F;
            // 
            // txtTyukei01
            // 
            this.txtTyukei01.DataField = "ITEM09";
            this.txtTyukei01.Height = 0.2F;
            this.txtTyukei01.Left = 1.953543F;
            this.txtTyukei01.MultiLine = false;
            this.txtTyukei01.Name = "txtTyukei01";
            this.txtTyukei01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei01.SummaryGroup = "groupHeader1";
            this.txtTyukei01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei01.Text = "txtTyukei01";
            this.txtTyukei01.Top = 0F;
            this.txtTyukei01.Width = 0.8200791F;
            // 
            // txtTyukei02
            // 
            this.txtTyukei02.DataField = "ITEM10";
            this.txtTyukei02.Height = 0.2F;
            this.txtTyukei02.Left = 2.712205F;
            this.txtTyukei02.MultiLine = false;
            this.txtTyukei02.Name = "txtTyukei02";
            this.txtTyukei02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei02.SummaryGroup = "groupHeader1";
            this.txtTyukei02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei02.Text = "txtTyukei02";
            this.txtTyukei02.Top = 0F;
            this.txtTyukei02.Width = 0.8200791F;
            // 
            // txtTyukei03
            // 
            this.txtTyukei03.DataField = "ITEM11";
            this.txtTyukei03.Height = 0.2F;
            this.txtTyukei03.Left = 3.650394F;
            this.txtTyukei03.Name = "txtTyukei03";
            this.txtTyukei03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei03.SummaryGroup = "groupHeader1";
            this.txtTyukei03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei03.Text = "txtTyukei03";
            this.txtTyukei03.Top = 0F;
            this.txtTyukei03.Width = 0.9181099F;
            // 
            // txtTyukei04
            // 
            this.txtTyukei04.DataField = "ITEM12";
            this.txtTyukei04.Height = 0.2F;
            this.txtTyukei04.Left = 4.719686F;
            this.txtTyukei04.MultiLine = false;
            this.txtTyukei04.Name = "txtTyukei04";
            this.txtTyukei04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei04.SummaryGroup = "groupHeader1";
            this.txtTyukei04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei04.Text = "txtTyukei04";
            this.txtTyukei04.Top = 0F;
            this.txtTyukei04.Width = 0.824409F;
            // 
            // txtTyukei05
            // 
            this.txtTyukei05.DataField = "ITEM13";
            this.txtTyukei05.Height = 0.2F;
            this.txtTyukei05.Left = 5.498425F;
            this.txtTyukei05.MultiLine = false;
            this.txtTyukei05.Name = "txtTyukei05";
            this.txtTyukei05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei05.SummaryGroup = "groupHeader1";
            this.txtTyukei05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei05.Text = "txtTyukei05";
            this.txtTyukei05.Top = 0F;
            this.txtTyukei05.Width = 0.7681103F;
            // 
            // txtTyukei06
            // 
            this.txtTyukei06.DataField = "ITEM14";
            this.txtTyukei06.Height = 0.2F;
            this.txtTyukei06.Left = 6.234646F;
            this.txtTyukei06.Name = "txtTyukei06";
            this.txtTyukei06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei06.SummaryGroup = "groupHeader1";
            this.txtTyukei06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei06.Text = "txtTyukei06";
            this.txtTyukei06.Top = 0F;
            this.txtTyukei06.Width = 1.053936F;
            // 
            // txtTyukei07
            // 
            this.txtTyukei07.DataField = "ITEM15";
            this.txtTyukei07.Height = 0.2F;
            this.txtTyukei07.Left = 7.526379F;
            this.txtTyukei07.MultiLine = false;
            this.txtTyukei07.Name = "txtTyukei07";
            this.txtTyukei07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei07.SummaryGroup = "groupHeader1";
            this.txtTyukei07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei07.Text = "txtTyukei07";
            this.txtTyukei07.Top = 0F;
            this.txtTyukei07.Width = 0.8200779F;
            // 
            // txtTyukei08
            // 
            this.txtTyukei08.DataField = "ITEM16";
            this.txtTyukei08.Height = 0.2F;
            this.txtTyukei08.Left = 8.268505F;
            this.txtTyukei08.MultiLine = false;
            this.txtTyukei08.Name = "txtTyukei08";
            this.txtTyukei08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei08.SummaryGroup = "groupHeader1";
            this.txtTyukei08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei08.Text = "txtTyukei08";
            this.txtTyukei08.Top = 0F;
            this.txtTyukei08.Width = 0.7885828F;
            // 
            // txtTyukei09
            // 
            this.txtTyukei09.DataField = "ITEM17";
            this.txtTyukei09.Height = 0.2F;
            this.txtTyukei09.Left = 9.187009F;
            this.txtTyukei09.Name = "txtTyukei09";
            this.txtTyukei09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei09.SummaryGroup = "groupHeader1";
            this.txtTyukei09.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei09.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei09.Text = "txtTyukei09";
            this.txtTyukei09.Top = 0F;
            this.txtTyukei09.Width = 0.9185038F;
            // 
            // txtTyukei10
            // 
            this.txtTyukei10.DataField = "ITEM18";
            this.txtTyukei10.Height = 0.2F;
            this.txtTyukei10.Left = 10.31811F;
            this.txtTyukei10.MultiLine = false;
            this.txtTyukei10.Name = "txtTyukei10";
            this.txtTyukei10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei10.SummaryGroup = "groupHeader1";
            this.txtTyukei10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei10.Text = "txtTyukei10";
            this.txtTyukei10.Top = 0F;
            this.txtTyukei10.Width = 0.8279533F;
            // 
            // txtTyukei11
            // 
            this.txtTyukei11.DataField = "ITEM19";
            this.txtTyukei11.Height = 0.2F;
            this.txtTyukei11.Left = 11.06182F;
            this.txtTyukei11.MultiLine = false;
            this.txtTyukei11.Name = "txtTyukei11";
            this.txtTyukei11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei11.SummaryGroup = "groupHeader1";
            this.txtTyukei11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei11.Text = "txtTyukei11";
            this.txtTyukei11.Top = 0F;
            this.txtTyukei11.Width = 0.8200788F;
            // 
            // txtTyukei12
            // 
            this.txtTyukei12.DataField = "ITEM20";
            this.txtTyukei12.Height = 0.2F;
            this.txtTyukei12.Left = 12.01181F;
            this.txtTyukei12.Name = "txtTyukei12";
            this.txtTyukei12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTyukei12.SummaryGroup = "groupHeader1";
            this.txtTyukei12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTyukei12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTyukei12.Text = "txtTyukei12";
            this.txtTyukei12.Top = 0F;
            this.txtTyukei12.Width = 1.049608F;
            // 
            // groupHeader2
            // 
            this.groupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox7,
            this.textBox8});
            this.groupHeader2.DataField = "ITEM05";
            this.groupHeader2.Height = 0.3125F;
            this.groupHeader2.Name = "groupHeader2";
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM05";
            this.textBox7.Height = 0.2F;
            this.textBox7.Left = 2.735827F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.textBox7.Text = null;
            this.textBox7.Top = 0.03937008F;
            this.textBox7.Visible = false;
            this.textBox7.Width = 0.3228346F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM06";
            this.textBox8.Height = 0.2F;
            this.textBox8.Left = 0.3807087F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-decoration: underline; ddo-char-set:" +
    " 128";
            this.textBox8.Text = null;
            this.textBox8.Top = 0.0472441F;
            this.textBox8.Width = 2.612205F;
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblShokei,
            this.txtShokei01,
            this.txtShokei02,
            this.txtShokei03,
            this.txtShokei04,
            this.txtShokei05,
            this.txtShokei06,
            this.txtShokei07,
            this.txtShokei08,
            this.txtShokei09,
            this.txtShokei10,
            this.txtShokei11,
            this.txtShokei12});
            this.groupFooter2.Height = 0.2604167F;
            this.groupFooter2.Name = "groupFooter2";
            this.groupFooter2.Format += new System.EventHandler(this.groupFooter2_Format);
            // 
            // lblShokei
            // 
            this.lblShokei.Height = 0.2F;
            this.lblShokei.HyperLink = null;
            this.lblShokei.Left = 0.218504F;
            this.lblShokei.Name = "lblShokei";
            this.lblShokei.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: center";
            this.lblShokei.Text = "☆  小計  ☆";
            this.lblShokei.Top = 0F;
            this.lblShokei.Width = 1.094095F;
            // 
            // txtShokei01
            // 
            this.txtShokei01.DataField = "ITEM09";
            this.txtShokei01.Height = 0.2F;
            this.txtShokei01.Left = 1.953543F;
            this.txtShokei01.MultiLine = false;
            this.txtShokei01.Name = "txtShokei01";
            this.txtShokei01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei01.SummaryGroup = "groupHeader2";
            this.txtShokei01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei01.Text = "txtShokei01";
            this.txtShokei01.Top = 0F;
            this.txtShokei01.Width = 0.8200791F;
            // 
            // txtShokei02
            // 
            this.txtShokei02.DataField = "ITEM10";
            this.txtShokei02.Height = 0.2F;
            this.txtShokei02.Left = 2.712205F;
            this.txtShokei02.MultiLine = false;
            this.txtShokei02.Name = "txtShokei02";
            this.txtShokei02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei02.SummaryGroup = "groupHeader2";
            this.txtShokei02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei02.Text = "txtShokei02";
            this.txtShokei02.Top = 0F;
            this.txtShokei02.Width = 0.8200791F;
            // 
            // txtShokei03
            // 
            this.txtShokei03.DataField = "ITEM11";
            this.txtShokei03.Height = 0.2F;
            this.txtShokei03.Left = 3.650394F;
            this.txtShokei03.Name = "txtShokei03";
            this.txtShokei03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei03.SummaryGroup = "groupHeader2";
            this.txtShokei03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei03.Text = "txtShokei03";
            this.txtShokei03.Top = 0F;
            this.txtShokei03.Width = 0.9181099F;
            // 
            // txtShokei04
            // 
            this.txtShokei04.DataField = "ITEM12";
            this.txtShokei04.Height = 0.2F;
            this.txtShokei04.Left = 4.719686F;
            this.txtShokei04.MultiLine = false;
            this.txtShokei04.Name = "txtShokei04";
            this.txtShokei04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei04.SummaryGroup = "groupHeader2";
            this.txtShokei04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei04.Text = "txtShokei04";
            this.txtShokei04.Top = 0F;
            this.txtShokei04.Width = 0.824409F;
            // 
            // txtShokei05
            // 
            this.txtShokei05.DataField = "ITEM13";
            this.txtShokei05.Height = 0.2F;
            this.txtShokei05.Left = 5.498425F;
            this.txtShokei05.MultiLine = false;
            this.txtShokei05.Name = "txtShokei05";
            this.txtShokei05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei05.SummaryGroup = "groupHeader2";
            this.txtShokei05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei05.Text = "txtShokei05";
            this.txtShokei05.Top = 0F;
            this.txtShokei05.Width = 0.7681103F;
            // 
            // txtShokei06
            // 
            this.txtShokei06.DataField = "ITEM14";
            this.txtShokei06.Height = 0.2F;
            this.txtShokei06.Left = 6.234646F;
            this.txtShokei06.Name = "txtShokei06";
            this.txtShokei06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei06.SummaryGroup = "groupHeader2";
            this.txtShokei06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei06.Text = "txtShokei06";
            this.txtShokei06.Top = 0F;
            this.txtShokei06.Width = 1.053936F;
            // 
            // txtShokei07
            // 
            this.txtShokei07.DataField = "ITEM15";
            this.txtShokei07.Height = 0.2F;
            this.txtShokei07.Left = 7.526379F;
            this.txtShokei07.MultiLine = false;
            this.txtShokei07.Name = "txtShokei07";
            this.txtShokei07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei07.SummaryGroup = "groupHeader2";
            this.txtShokei07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei07.Text = "txtShokei07";
            this.txtShokei07.Top = 0F;
            this.txtShokei07.Width = 0.8200779F;
            // 
            // txtShokei08
            // 
            this.txtShokei08.DataField = "ITEM16";
            this.txtShokei08.Height = 0.2F;
            this.txtShokei08.Left = 8.268505F;
            this.txtShokei08.MultiLine = false;
            this.txtShokei08.Name = "txtShokei08";
            this.txtShokei08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei08.SummaryGroup = "groupHeader2";
            this.txtShokei08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei08.Text = "txtShokei08";
            this.txtShokei08.Top = 0F;
            this.txtShokei08.Width = 0.7885828F;
            // 
            // txtShokei09
            // 
            this.txtShokei09.DataField = "ITEM17";
            this.txtShokei09.Height = 0.2F;
            this.txtShokei09.Left = 9.187009F;
            this.txtShokei09.Name = "txtShokei09";
            this.txtShokei09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei09.SummaryGroup = "groupHeader2";
            this.txtShokei09.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei09.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei09.Text = "txtShokei09";
            this.txtShokei09.Top = 0F;
            this.txtShokei09.Width = 0.9185038F;
            // 
            // txtShokei10
            // 
            this.txtShokei10.DataField = "ITEM18";
            this.txtShokei10.Height = 0.2F;
            this.txtShokei10.Left = 10.31811F;
            this.txtShokei10.MultiLine = false;
            this.txtShokei10.Name = "txtShokei10";
            this.txtShokei10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei10.SummaryGroup = "groupHeader2";
            this.txtShokei10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei10.Text = "txtShokei10";
            this.txtShokei10.Top = 0F;
            this.txtShokei10.Width = 0.8279533F;
            // 
            // txtShokei11
            // 
            this.txtShokei11.DataField = "ITEM19";
            this.txtShokei11.Height = 0.2F;
            this.txtShokei11.Left = 11.06182F;
            this.txtShokei11.MultiLine = false;
            this.txtShokei11.Name = "txtShokei11";
            this.txtShokei11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei11.SummaryGroup = "groupHeader2";
            this.txtShokei11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei11.Text = "txtShokei11";
            this.txtShokei11.Top = 0F;
            this.txtShokei11.Width = 0.8200788F;
            // 
            // txtShokei12
            // 
            this.txtShokei12.DataField = "ITEM20";
            this.txtShokei12.Height = 0.2F;
            this.txtShokei12.Left = 12.01181F;
            this.txtShokei12.Name = "txtShokei12";
            this.txtShokei12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokei12.SummaryGroup = "groupHeader2";
            this.txtShokei12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokei12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokei12.Text = "9,999,999,999";
            this.txtShokei12.Top = 0F;
            this.txtShokei12.Width = 1.049608F;
            // 
            // HNMR1021R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.5905512F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.14961F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTikuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJojun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyujun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGejun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbltitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSogokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSogokei12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyukei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyukei12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokei12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTikuNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblJojun;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTyujun;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGejun;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGokei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Label lbltitle02;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSogokei;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTyukei;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShokei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateFr;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateTo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox page;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSogokei12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyukei12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokei12;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo date;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
    }
}
