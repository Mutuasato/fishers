﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1021
{
    /// <summary>
    /// HNMR1021R の帳票
    /// </summary>
    public partial class HNMR1021R : BaseReport
    {

        public HNMR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページフッターの設定(小計)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void groupFooter2_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            this.txtShokei01.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei01.Text),2);
            this.txtShokei02.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei02.Text),2);
            this.txtShokei03.Text = Util.FormatNum(this.txtShokei03.Text);
            this.txtShokei04.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei04.Text),2);
            this.txtShokei05.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei05.Text),2);
            this.txtShokei06.Text = Util.FormatNum(this.txtShokei06.Text);
            this.txtShokei07.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei07.Text),2);
            this.txtShokei08.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei08.Text),2);
            this.txtShokei09.Text = Util.FormatNum(this.txtShokei09.Text);
            this.txtShokei10.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei10.Text),2);
            this.txtShokei11.Text = Util.FormatNum(Util.ToDecimal(this.txtShokei11.Text),2);
            this.txtShokei12.Text = Util.FormatNum(this.txtShokei12.Text);

        }

        /// <summary>
        /// ページフッターの設定(中計)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void groupFooter1_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            this.txtTyukei01.Text = Util.FormatNum(Util.ToDecimal(this.txtTyukei01.Text),2);
            this.txtTyukei02.Text = Util.FormatNum(Util.ToDecimal(this.txtTyukei02.Text), 2);
            this.txtTyukei03.Text = Util.FormatNum(this.txtTyukei03.Text);
            this.txtTyukei04.Text = Util.FormatNum(Util.ToDecimal(this.txtTyukei04.Text), 2);
            this.txtTyukei05.Text = Util.FormatNum(Util.ToDecimal(this.txtTyukei05.Text), 2);
            this.txtTyukei06.Text = Util.FormatNum(this.txtTyukei06.Text);
            this.txtTyukei07.Text = Util.FormatNum(Util.ToDecimal(this.txtTyukei07.Text), 2);
            this.txtTyukei08.Text = Util.FormatNum(Util.ToDecimal(this.txtTyukei08.Text), 2);
            this.txtTyukei09.Text = Util.FormatNum(this.txtTyukei09.Text);
            this.txtTyukei10.Text = Util.FormatNum(Util.ToDecimal(this.txtTyukei10.Text), 2);
            this.txtTyukei11.Text = Util.FormatNum(Util.ToDecimal(this.txtTyukei11.Text), 2);
            this.txtTyukei12.Text = Util.FormatNum(this.txtTyukei12.Text);

        }

        /// <summary>
        /// ページフッターの設定(総合計)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportFooter1_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            this.txtSogokei01.Text = Util.FormatNum(Util.ToDecimal(this.txtSogokei01.Text),2);
            this.txtSogokei02.Text = Util.FormatNum(Util.ToDecimal(this.txtSogokei02.Text), 2);
            this.txtSogokei03.Text = Util.FormatNum(this.txtSogokei03.Text);
            this.txtSogokei04.Text = Util.FormatNum(Util.ToDecimal(this.txtSogokei04.Text), 2);
            this.txtSogokei05.Text = Util.FormatNum(Util.ToDecimal(this.txtSogokei05.Text), 2);
            this.txtSogokei06.Text = Util.FormatNum(this.txtSogokei06.Text);
            this.txtSogokei07.Text = Util.FormatNum(Util.ToDecimal(this.txtSogokei07.Text), 2);
            this.txtSogokei08.Text = Util.FormatNum(Util.ToDecimal(this.txtSogokei08.Text), 2);
            this.txtSogokei09.Text = Util.FormatNum(this.txtSogokei09.Text);
            this.txtSogokei10.Text = Util.FormatNum(Util.ToDecimal(this.txtSogokei10.Text), 2);
            this.txtSogokei11.Text = Util.FormatNum(Util.ToDecimal(this.txtSogokei11.Text), 2);
            this.txtSogokei12.Text = Util.FormatNum(this.txtSogokei12.Text);
        }
    }
}
