﻿namespace jp.co.fsi.hn.hncm1071
{
    partial class HNCM1072
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txtSetteiCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSetteiCd = new System.Windows.Forms.Label();
			this.lblBumonnCd = new System.Windows.Forms.Label();
			this.lblHojoKamokuCd = new System.Windows.Forms.Label();
			this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
			this.lblKamokbn = new System.Windows.Forms.Label();
			this.lblTaishakKbnn = new System.Windows.Forms.Label();
			this.txtTaishakKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblJigyoKubn = new System.Windows.Forms.Label();
			this.txtJigyoKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblJigyoKbn = new System.Windows.Forms.Label();
			this.lblZeiKbnn = new System.Windows.Forms.Label();
			this.txtZeiKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblZeiKbn = new System.Windows.Forms.Label();
			this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblBumonCd = new System.Windows.Forms.Label();
			this.txtHojoKamokCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblHojoKamokCd = new System.Windows.Forms.Label();
			this.txtKanjoKamokCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKeisanFugo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKojoTaishoShikriKomok = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKojoTaishoShikriKomok = new System.Windows.Forms.Label();
			this.lblKeisanhugo = new System.Windows.Forms.Label();
			this.lblKanjoKamokCd = new System.Windows.Forms.Label();
			this.lblTaishakKbn = new System.Windows.Forms.Label();
			this.lblKojoKomokNm = new System.Windows.Forms.Label();
			this.txtKojoKomokSettei = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKojoKomokSettei = new System.Windows.Forms.Label();
			this.txtKojoKomokNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShishoNm = new System.Windows.Forms.Label();
			this.lblShisho = new System.Windows.Forms.Label();
			this.chkKeisanKbn = new System.Windows.Forms.CheckBox();
			this.txtKomoku = new jp.co.fsi.common.controls.FsiTextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
			this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
			this.txtScript = new jp.co.fsi.common.controls.FsiTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtShikiriSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiTableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel17 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel16 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel15 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel14 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel13 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel12.SuspendLayout();
			this.fsiPanel11.SuspendLayout();
			this.fsiPanel10.SuspendLayout();
			this.fsiPanel9.SuspendLayout();
			this.fsiPanel8.SuspendLayout();
			this.fsiPanel7.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiTableLayoutPanel2.SuspendLayout();
			this.fsiPanel17.SuspendLayout();
			this.fsiPanel16.SuspendLayout();
			this.fsiPanel15.SuspendLayout();
			this.fsiPanel14.SuspendLayout();
			this.fsiPanel13.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(7, 615);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1056, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1045, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			this.lblTitle.Visible = false;
			// 
			// txtSetteiCd
			// 
			this.txtSetteiCd.AutoSizeFromLength = true;
			this.txtSetteiCd.DisplayLength = null;
			this.txtSetteiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSetteiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSetteiCd.Location = new System.Drawing.Point(133, 2);
			this.txtSetteiCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtSetteiCd.MaxLength = 6;
			this.txtSetteiCd.Name = "txtSetteiCd";
			this.txtSetteiCd.Size = new System.Drawing.Size(63, 23);
			this.txtSetteiCd.TabIndex = 2;
			this.txtSetteiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSetteiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtSetteiCd_Validating);
			// 
			// lblSetteiCd
			// 
			this.lblSetteiCd.BackColor = System.Drawing.Color.Silver;
			this.lblSetteiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSetteiCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblSetteiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSetteiCd.Location = new System.Drawing.Point(0, 0);
			this.lblSetteiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSetteiCd.Name = "lblSetteiCd";
			this.lblSetteiCd.Size = new System.Drawing.Size(885, 28);
			this.lblSetteiCd.TabIndex = 0;
			this.lblSetteiCd.Tag = "CHANGE";
			this.lblSetteiCd.Text = "設定コード";
			this.lblSetteiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblBumonnCd
			// 
			this.lblBumonnCd.BackColor = System.Drawing.Color.LightCyan;
			this.lblBumonnCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBumonnCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBumonnCd.Location = new System.Drawing.Point(235, 1);
			this.lblBumonnCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBumonnCd.Name = "lblBumonnCd";
			this.lblBumonnCd.Size = new System.Drawing.Size(303, 24);
			this.lblBumonnCd.TabIndex = 13;
			this.lblBumonnCd.Tag = "DISPNAME";
			this.lblBumonnCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblHojoKamokuCd
			// 
			this.lblHojoKamokuCd.BackColor = System.Drawing.Color.LightCyan;
			this.lblHojoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblHojoKamokuCd.Location = new System.Drawing.Point(235, 1);
			this.lblHojoKamokuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblHojoKamokuCd.Name = "lblHojoKamokuCd";
			this.lblHojoKamokuCd.Size = new System.Drawing.Size(303, 24);
			this.lblHojoKamokuCd.TabIndex = 10;
			this.lblHojoKamokuCd.Tag = "DISPNAME";
			this.lblHojoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKanjoKamokuCd
			// 
			this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.LightCyan;
			this.lblKanjoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKanjoKamokuCd.Location = new System.Drawing.Point(235, 1);
			this.lblKanjoKamokuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
			this.lblKanjoKamokuCd.Size = new System.Drawing.Size(303, 24);
			this.lblKanjoKamokuCd.TabIndex = 7;
			this.lblKanjoKamokuCd.Tag = "DISPNAME";
			this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKamokbn
			// 
			this.lblKamokbn.BackColor = System.Drawing.Color.Silver;
			this.lblKamokbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKamokbn.Location = new System.Drawing.Point(235, 1);
			this.lblKamokbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKamokbn.Name = "lblKamokbn";
			this.lblKamokbn.Size = new System.Drawing.Size(280, 24);
			this.lblKamokbn.TabIndex = 4;
			this.lblKamokbn.Tag = "CHANGE";
			this.lblKamokbn.Text = "1:正　2:負";
			this.lblKamokbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTaishakKbnn
			// 
			this.lblTaishakKbnn.BackColor = System.Drawing.Color.Silver;
			this.lblTaishakKbnn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTaishakKbnn.Location = new System.Drawing.Point(235, 3);
			this.lblTaishakKbnn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTaishakKbnn.Name = "lblTaishakKbnn";
			this.lblTaishakKbnn.Size = new System.Drawing.Size(303, 24);
			this.lblTaishakKbnn.TabIndex = 22;
			this.lblTaishakKbnn.Tag = "CHANGE";
			this.lblTaishakKbnn.Text = "1:借方　2:貸方";
			this.lblTaishakKbnn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTaishakKbn
			// 
			this.txtTaishakKbn.AutoSizeFromLength = true;
			this.txtTaishakKbn.DisplayLength = 2;
			this.txtTaishakKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTaishakKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTaishakKbn.Location = new System.Drawing.Point(133, 3);
			this.txtTaishakKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtTaishakKbn.MaxLength = 3;
			this.txtTaishakKbn.Name = "txtTaishakKbn";
			this.txtTaishakKbn.Size = new System.Drawing.Size(72, 23);
			this.txtTaishakKbn.TabIndex = 12;
			this.txtTaishakKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTaishakKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaishakKbn_Validating);
			// 
			// lblJigyoKubn
			// 
			this.lblJigyoKubn.BackColor = System.Drawing.Color.LightCyan;
			this.lblJigyoKubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJigyoKubn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJigyoKubn.Location = new System.Drawing.Point(235, 1);
			this.lblJigyoKubn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJigyoKubn.Name = "lblJigyoKubn";
			this.lblJigyoKubn.Size = new System.Drawing.Size(303, 24);
			this.lblJigyoKubn.TabIndex = 19;
			this.lblJigyoKubn.Tag = "DISPNAME";
			this.lblJigyoKubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtJigyoKbn
			// 
			this.txtJigyoKbn.AutoSizeFromLength = true;
			this.txtJigyoKbn.DisplayLength = null;
			this.txtJigyoKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtJigyoKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtJigyoKbn.Location = new System.Drawing.Point(133, 2);
			this.txtJigyoKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtJigyoKbn.MaxLength = 3;
			this.txtJigyoKbn.Name = "txtJigyoKbn";
			this.txtJigyoKbn.Size = new System.Drawing.Size(72, 23);
			this.txtJigyoKbn.TabIndex = 11;
			this.txtJigyoKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtJigyoKbn.TextChanged += new System.EventHandler(this.txtJigyoKbn_TextChanged);
			this.txtJigyoKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtZigyoKbn_Validating);
			// 
			// lblJigyoKbn
			// 
			this.lblJigyoKbn.BackColor = System.Drawing.Color.Silver;
			this.lblJigyoKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJigyoKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJigyoKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJigyoKbn.Location = new System.Drawing.Point(0, 0);
			this.lblJigyoKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJigyoKbn.Name = "lblJigyoKbn";
			this.lblJigyoKbn.Size = new System.Drawing.Size(885, 28);
			this.lblJigyoKbn.TabIndex = 17;
			this.lblJigyoKbn.Tag = "CHANGE";
			this.lblJigyoKbn.Text = "事　業　区　分";
			this.lblJigyoKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblZeiKbnn
			// 
			this.lblZeiKbnn.BackColor = System.Drawing.Color.LightCyan;
			this.lblZeiKbnn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblZeiKbnn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblZeiKbnn.Location = new System.Drawing.Point(235, 1);
			this.lblZeiKbnn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZeiKbnn.Name = "lblZeiKbnn";
			this.lblZeiKbnn.Size = new System.Drawing.Size(303, 24);
			this.lblZeiKbnn.TabIndex = 16;
			this.lblZeiKbnn.Tag = "DISPNAME";
			this.lblZeiKbnn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtZeiKbn
			// 
			this.txtZeiKbn.AutoSizeFromLength = true;
			this.txtZeiKbn.DisplayLength = null;
			this.txtZeiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtZeiKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtZeiKbn.Location = new System.Drawing.Point(133, 2);
			this.txtZeiKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtZeiKbn.MaxLength = 4;
			this.txtZeiKbn.Name = "txtZeiKbn";
			this.txtZeiKbn.Size = new System.Drawing.Size(72, 23);
			this.txtZeiKbn.TabIndex = 10;
			this.txtZeiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtZeiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKbn_Validating);
			// 
			// lblZeiKbn
			// 
			this.lblZeiKbn.BackColor = System.Drawing.Color.Silver;
			this.lblZeiKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblZeiKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblZeiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblZeiKbn.Location = new System.Drawing.Point(0, 0);
			this.lblZeiKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZeiKbn.Name = "lblZeiKbn";
			this.lblZeiKbn.Size = new System.Drawing.Size(885, 28);
			this.lblZeiKbn.TabIndex = 14;
			this.lblZeiKbn.Tag = "CHANGE";
			this.lblZeiKbn.Text = "税　　区　　分";
			this.lblZeiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBumonCd
			// 
			this.txtBumonCd.AutoSizeFromLength = true;
			this.txtBumonCd.DisplayLength = null;
			this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtBumonCd.Location = new System.Drawing.Point(133, 2);
			this.txtBumonCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtBumonCd.MaxLength = 7;
			this.txtBumonCd.Name = "txtBumonCd";
			this.txtBumonCd.Size = new System.Drawing.Size(72, 23);
			this.txtBumonCd.TabIndex = 9;
			this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
			// 
			// lblBumonCd
			// 
			this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
			this.lblBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBumonCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBumonCd.Location = new System.Drawing.Point(0, 0);
			this.lblBumonCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBumonCd.Name = "lblBumonCd";
			this.lblBumonCd.Size = new System.Drawing.Size(885, 28);
			this.lblBumonCd.TabIndex = 11;
			this.lblBumonCd.Tag = "CHANGE";
			this.lblBumonCd.Text = "部門コード";
			this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtHojoKamokCd
			// 
			this.txtHojoKamokCd.AutoSizeFromLength = true;
			this.txtHojoKamokCd.DisplayLength = null;
			this.txtHojoKamokCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtHojoKamokCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtHojoKamokCd.Location = new System.Drawing.Point(133, 2);
			this.txtHojoKamokCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtHojoKamokCd.MaxLength = 10;
			this.txtHojoKamokCd.Name = "txtHojoKamokCd";
			this.txtHojoKamokCd.Size = new System.Drawing.Size(100, 23);
			this.txtHojoKamokCd.TabIndex = 8;
			this.txtHojoKamokCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtHojoKamokCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoKamokCd_Validating);
			// 
			// lblHojoKamokCd
			// 
			this.lblHojoKamokCd.BackColor = System.Drawing.Color.Silver;
			this.lblHojoKamokCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblHojoKamokCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblHojoKamokCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblHojoKamokCd.Location = new System.Drawing.Point(0, 0);
			this.lblHojoKamokCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblHojoKamokCd.Name = "lblHojoKamokCd";
			this.lblHojoKamokCd.Size = new System.Drawing.Size(885, 28);
			this.lblHojoKamokCd.TabIndex = 8;
			this.lblHojoKamokCd.Tag = "CHANGE";
			this.lblHojoKamokCd.Text = "補助科目コード";
			this.lblHojoKamokCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKanjoKamokCd
			// 
			this.txtKanjoKamokCd.AutoSizeFromLength = false;
			this.txtKanjoKamokCd.DisplayLength = null;
			this.txtKanjoKamokCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKanjoKamokCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtKanjoKamokCd.Location = new System.Drawing.Point(133, 2);
			this.txtKanjoKamokCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanjoKamokCd.MaxLength = 7;
			this.txtKanjoKamokCd.Name = "txtKanjoKamokCd";
			this.txtKanjoKamokCd.Size = new System.Drawing.Size(72, 23);
			this.txtKanjoKamokCd.TabIndex = 7;
			this.txtKanjoKamokCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKanjoKamokCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokCd_Validating);
			// 
			// txtKeisanFugo
			// 
			this.txtKeisanFugo.AutoSizeFromLength = false;
			this.txtKeisanFugo.DisplayLength = null;
			this.txtKeisanFugo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKeisanFugo.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtKeisanFugo.Location = new System.Drawing.Point(133, 2);
			this.txtKeisanFugo.Margin = new System.Windows.Forms.Padding(4);
			this.txtKeisanFugo.MaxLength = 1;
			this.txtKeisanFugo.Name = "txtKeisanFugo";
			this.txtKeisanFugo.Size = new System.Drawing.Size(72, 23);
			this.txtKeisanFugo.TabIndex = 6;
			this.txtKeisanFugo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKeisanFugo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKeisanhugo_Validating);
			// 
			// txtKojoTaishoShikriKomok
			// 
			this.txtKojoTaishoShikriKomok.AutoSizeFromLength = true;
			this.txtKojoTaishoShikriKomok.DisplayLength = null;
			this.txtKojoTaishoShikriKomok.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKojoTaishoShikriKomok.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtKojoTaishoShikriKomok.Location = new System.Drawing.Point(133, 2);
			this.txtKojoTaishoShikriKomok.Margin = new System.Windows.Forms.Padding(4);
			this.txtKojoTaishoShikriKomok.MaxLength = 15;
			this.txtKojoTaishoShikriKomok.Name = "txtKojoTaishoShikriKomok";
			this.txtKojoTaishoShikriKomok.Size = new System.Drawing.Size(147, 23);
			this.txtKojoTaishoShikriKomok.TabIndex = 5;
			this.txtKojoTaishoShikriKomok.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoTaishoShikriKomok_Validating);
			// 
			// lblKojoTaishoShikriKomok
			// 
			this.lblKojoTaishoShikriKomok.BackColor = System.Drawing.Color.Silver;
			this.lblKojoTaishoShikriKomok.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKojoTaishoShikriKomok.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKojoTaishoShikriKomok.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKojoTaishoShikriKomok.Location = new System.Drawing.Point(0, 0);
			this.lblKojoTaishoShikriKomok.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKojoTaishoShikriKomok.Name = "lblKojoTaishoShikriKomok";
			this.lblKojoTaishoShikriKomok.Size = new System.Drawing.Size(885, 28);
			this.lblKojoTaishoShikriKomok.TabIndex = 0;
			this.lblKojoTaishoShikriKomok.Tag = "CHANGE";
			this.lblKojoTaishoShikriKomok.Text = "控除対象仕切項目";
			this.lblKojoTaishoShikriKomok.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKeisanhugo
			// 
			this.lblKeisanhugo.BackColor = System.Drawing.Color.Silver;
			this.lblKeisanhugo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKeisanhugo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKeisanhugo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKeisanhugo.Location = new System.Drawing.Point(0, 0);
			this.lblKeisanhugo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKeisanhugo.Name = "lblKeisanhugo";
			this.lblKeisanhugo.Size = new System.Drawing.Size(885, 28);
			this.lblKeisanhugo.TabIndex = 2;
			this.lblKeisanhugo.Tag = "CHANGE";
			this.lblKeisanhugo.Text = "計　算　符　号";
			this.lblKeisanhugo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKanjoKamokCd
			// 
			this.lblKanjoKamokCd.BackColor = System.Drawing.Color.Silver;
			this.lblKanjoKamokCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanjoKamokCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKanjoKamokCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKanjoKamokCd.Location = new System.Drawing.Point(0, 0);
			this.lblKanjoKamokCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanjoKamokCd.Name = "lblKanjoKamokCd";
			this.lblKanjoKamokCd.Size = new System.Drawing.Size(885, 28);
			this.lblKanjoKamokCd.TabIndex = 5;
			this.lblKanjoKamokCd.Tag = "CHANGE";
			this.lblKanjoKamokCd.Text = "勘定科目コード";
			this.lblKanjoKamokCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTaishakKbn
			// 
			this.lblTaishakKbn.BackColor = System.Drawing.Color.Silver;
			this.lblTaishakKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTaishakKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTaishakKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTaishakKbn.Location = new System.Drawing.Point(0, 0);
			this.lblTaishakKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTaishakKbn.Name = "lblTaishakKbn";
			this.lblTaishakKbn.Size = new System.Drawing.Size(885, 30);
			this.lblTaishakKbn.TabIndex = 20;
			this.lblTaishakKbn.Tag = "CHANGE";
			this.lblTaishakKbn.Text = "貸　借　区　分";
			this.lblTaishakKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKojoKomokNm
			// 
			this.lblKojoKomokNm.BackColor = System.Drawing.Color.Silver;
			this.lblKojoKomokNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKojoKomokNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKojoKomokNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKojoKomokNm.Location = new System.Drawing.Point(0, 0);
			this.lblKojoKomokNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKojoKomokNm.Name = "lblKojoKomokNm";
			this.lblKojoKomokNm.Size = new System.Drawing.Size(885, 28);
			this.lblKojoKomokNm.TabIndex = 4;
			this.lblKojoKomokNm.Tag = "CHANGE";
			this.lblKojoKomokNm.Text = "控除項目名称";
			this.lblKojoKomokNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKojoKomokSettei
			// 
			this.txtKojoKomokSettei.AutoSizeFromLength = false;
			this.txtKojoKomokSettei.DisplayLength = null;
			this.txtKojoKomokSettei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKojoKomokSettei.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtKojoKomokSettei.Location = new System.Drawing.Point(133, 2);
			this.txtKojoKomokSettei.Margin = new System.Windows.Forms.Padding(4);
			this.txtKojoKomokSettei.MaxLength = 15;
			this.txtKojoKomokSettei.Name = "txtKojoKomokSettei";
			this.txtKojoKomokSettei.Size = new System.Drawing.Size(359, 23);
			this.txtKojoKomokSettei.TabIndex = 3;
			this.txtKojoKomokSettei.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoKomokSettei_Validating);
			// 
			// lblKojoKomokSettei
			// 
			this.lblKojoKomokSettei.BackColor = System.Drawing.Color.Silver;
			this.lblKojoKomokSettei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKojoKomokSettei.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKojoKomokSettei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKojoKomokSettei.Location = new System.Drawing.Point(0, 0);
			this.lblKojoKomokSettei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKojoKomokSettei.Name = "lblKojoKomokSettei";
			this.lblKojoKomokSettei.Size = new System.Drawing.Size(885, 28);
			this.lblKojoKomokSettei.TabIndex = 2;
			this.lblKojoKomokSettei.Tag = "CHANGE";
			this.lblKojoKomokSettei.Text = "控除項目設定";
			this.lblKojoKomokSettei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKojoKomokNm
			// 
			this.txtKojoKomokNm.AutoSizeFromLength = false;
			this.txtKojoKomokNm.DisplayLength = null;
			this.txtKojoKomokNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKojoKomokNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtKojoKomokNm.Location = new System.Drawing.Point(133, 2);
			this.txtKojoKomokNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtKojoKomokNm.MaxLength = 15;
			this.txtKojoKomokNm.Name = "txtKojoKomokNm";
			this.txtKojoKomokNm.Size = new System.Drawing.Size(359, 23);
			this.txtKojoKomokNm.TabIndex = 4;
			this.txtKojoKomokNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoKomokNm_Validating);
			// 
			// txtShishoCd
			// 
			this.txtShishoCd.AutoSizeFromLength = true;
			this.txtShishoCd.DisplayLength = null;
			this.txtShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShishoCd.Location = new System.Drawing.Point(133, 2);
			this.txtShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtShishoCd.MaxLength = 4;
			this.txtShishoCd.Name = "txtShishoCd";
			this.txtShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtShishoCd.TabIndex = 1;
			this.txtShishoCd.TabStop = false;
			this.txtShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCd_Validating);
			// 
			// lblShishoNm
			// 
			this.lblShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShishoNm.Location = new System.Drawing.Point(184, 1);
			this.lblShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShishoNm.Name = "lblShishoNm";
			this.lblShishoNm.Size = new System.Drawing.Size(303, 24);
			this.lblShishoNm.TabIndex = 1002;
			this.lblShishoNm.Tag = "DISPNAME";
			this.lblShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShisho
			// 
			this.lblShisho.BackColor = System.Drawing.Color.Silver;
			this.lblShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShisho.Location = new System.Drawing.Point(0, 0);
			this.lblShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShisho.Name = "lblShisho";
			this.lblShisho.Size = new System.Drawing.Size(885, 28);
			this.lblShisho.TabIndex = 1000;
			this.lblShisho.Tag = "CHANGE";
			this.lblShisho.Text = "支　　所";
			this.lblShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkKeisanKbn
			// 
			this.chkKeisanKbn.AutoSize = true;
			this.chkKeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.chkKeisanKbn.Location = new System.Drawing.Point(7, 5);
			this.chkKeisanKbn.Margin = new System.Windows.Forms.Padding(4);
			this.chkKeisanKbn.Name = "chkKeisanKbn";
			this.chkKeisanKbn.Size = new System.Drawing.Size(91, 20);
			this.chkKeisanKbn.TabIndex = 13;
			this.chkKeisanKbn.Text = "計算有り";
			this.chkKeisanKbn.UseVisualStyleBackColor = true;
			this.chkKeisanKbn.CheckedChanged += new System.EventHandler(this.chkKeisanKbn_CheckedChanged);
			// 
			// txtKomoku
			// 
			this.txtKomoku.AutoSizeFromLength = false;
			this.txtKomoku.DisplayLength = null;
			this.txtKomoku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtKomoku.Location = new System.Drawing.Point(174, 1);
			this.txtKomoku.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku.MaxLength = 15;
			this.txtKomoku.Name = "txtKomoku";
			this.txtKomoku.Size = new System.Drawing.Size(176, 23);
			this.txtKomoku.TabIndex = 16;
			this.txtKomoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(885, 27);
			this.label4.TabIndex = 5;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "計算項目";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(333, 6);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(424, 16);
			this.label3.TabIndex = 35;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "対象となる精算区分をカンマ区切りで入力してください。";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanKbn
			// 
			this.txtSeisanKbn.AutoSizeFromLength = false;
			this.txtSeisanKbn.DisplayLength = null;
			this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtSeisanKbn.Location = new System.Drawing.Point(173, 2);
			this.txtSeisanKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanKbn.MaxLength = 20;
			this.txtSeisanKbn.Name = "txtSeisanKbn";
			this.txtSeisanKbn.Size = new System.Drawing.Size(151, 23);
			this.txtSeisanKbn.TabIndex = 14;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(885, 27);
			this.label2.TabIndex = 1;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "精算処理対象精算区分";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShzNrkHohoNm
			// 
			this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzNrkHohoNm.Location = new System.Drawing.Point(518, 1);
			this.lblShzNrkHohoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
			this.lblShzNrkHohoNm.Size = new System.Drawing.Size(349, 24);
			this.lblShzNrkHohoNm.TabIndex = 9;
			this.lblShzNrkHohoNm.Tag = "CHANGE";
			this.lblShzNrkHohoNm.Text = "税抜き入力（自動計算あり）";
			this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiNyuryokuHoho
			// 
			this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
			this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.White;
			this.txtShohizeiNyuryokuHoho.DisplayLength = null;
			this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(490, 2);
			this.txtShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiNyuryokuHoho.MaxLength = 1;
			this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
			this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(19, 23);
			this.txtShohizeiNyuryokuHoho.TabIndex = 17;
			this.txtShohizeiNyuryokuHoho.Tag = "";
			this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblShohizeiNyuryokuHoho
			// 
			this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(357, 1);
			this.lblShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
			this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(128, 24);
			this.lblShohizeiNyuryokuHoho.TabIndex = 7;
			this.lblShohizeiNyuryokuHoho.Tag = "CHANGE";
			this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
			this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtScript
			// 
			this.txtScript.AutoSizeFromLength = false;
			this.txtScript.DisplayLength = null;
			this.txtScript.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtScript.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtScript.Location = new System.Drawing.Point(173, 2);
			this.txtScript.Margin = new System.Windows.Forms.Padding(4);
			this.txtScript.MaxLength = 100;
			this.txtScript.Name = "txtScript";
			this.txtScript.Size = new System.Drawing.Size(694, 23);
			this.txtScript.TabIndex = 15;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(885, 27);
			this.label1.TabIndex = 3;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "計算式";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShikiriSeisanKbn
			// 
			this.txtShikiriSeisanKbn.AutoSizeFromLength = false;
			this.txtShikiriSeisanKbn.DisplayLength = null;
			this.txtShikiriSeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShikiriSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.txtShikiriSeisanKbn.Location = new System.Drawing.Point(174, 2);
			this.txtShikiriSeisanKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtShikiriSeisanKbn.MaxLength = 20;
			this.txtShikiriSeisanKbn.Name = "txtShikiriSeisanKbn";
			this.txtShikiriSeisanKbn.Size = new System.Drawing.Size(151, 23);
			this.txtShikiriSeisanKbn.TabIndex = 1006;
			this.txtShikiriSeisanKbn.Tag = "18";
			this.txtShikiriSeisanKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Silver;
			this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label5.Location = new System.Drawing.Point(0, 0);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(885, 29);
			this.label5.TabIndex = 1005;
			this.label5.Tag = "CHANGE";
			this.label5.Text = "仕切書対象精算区分";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.BackColor = System.Drawing.Color.Silver;
			this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label6.Location = new System.Drawing.Point(336, 6);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(424, 16);
			this.label6.TabIndex = 1007;
			this.label6.Tag = "CHANGE";
			this.label6.Text = "対象となる精算区分をカンマ区切りで入力してください。";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel12, 0, 11);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel11, 0, 10);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel10, 0, 9);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel9, 0, 8);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 7);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(25, 34);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 12;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(893, 423);
			this.fsiTableLayoutPanel1.TabIndex = 1007;
			// 
			// fsiPanel12
			// 
			this.fsiPanel12.Controls.Add(this.lblTaishakKbnn);
			this.fsiPanel12.Controls.Add(this.txtTaishakKbn);
			this.fsiPanel12.Controls.Add(this.lblTaishakKbn);
			this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel12.Location = new System.Drawing.Point(4, 389);
			this.fsiPanel12.Name = "fsiPanel12";
			this.fsiPanel12.Size = new System.Drawing.Size(885, 30);
			this.fsiPanel12.TabIndex = 11;
			// 
			// fsiPanel11
			// 
			this.fsiPanel11.Controls.Add(this.txtJigyoKbn);
			this.fsiPanel11.Controls.Add(this.lblJigyoKubn);
			this.fsiPanel11.Controls.Add(this.lblJigyoKbn);
			this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel11.Location = new System.Drawing.Point(4, 354);
			this.fsiPanel11.Name = "fsiPanel11";
			this.fsiPanel11.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel11.TabIndex = 10;
			// 
			// fsiPanel10
			// 
			this.fsiPanel10.Controls.Add(this.txtZeiKbn);
			this.fsiPanel10.Controls.Add(this.lblZeiKbnn);
			this.fsiPanel10.Controls.Add(this.lblZeiKbn);
			this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel10.Location = new System.Drawing.Point(4, 319);
			this.fsiPanel10.Name = "fsiPanel10";
			this.fsiPanel10.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel10.TabIndex = 9;
			// 
			// fsiPanel9
			// 
			this.fsiPanel9.Controls.Add(this.lblBumonnCd);
			this.fsiPanel9.Controls.Add(this.txtBumonCd);
			this.fsiPanel9.Controls.Add(this.lblBumonCd);
			this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel9.Location = new System.Drawing.Point(4, 284);
			this.fsiPanel9.Name = "fsiPanel9";
			this.fsiPanel9.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel9.TabIndex = 8;
			// 
			// fsiPanel8
			// 
			this.fsiPanel8.Controls.Add(this.lblHojoKamokuCd);
			this.fsiPanel8.Controls.Add(this.txtHojoKamokCd);
			this.fsiPanel8.Controls.Add(this.lblHojoKamokCd);
			this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel8.Location = new System.Drawing.Point(4, 249);
			this.fsiPanel8.Name = "fsiPanel8";
			this.fsiPanel8.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel8.TabIndex = 7;
			// 
			// fsiPanel7
			// 
			this.fsiPanel7.Controls.Add(this.txtKanjoKamokCd);
			this.fsiPanel7.Controls.Add(this.lblKanjoKamokuCd);
			this.fsiPanel7.Controls.Add(this.lblKanjoKamokCd);
			this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel7.Location = new System.Drawing.Point(4, 214);
			this.fsiPanel7.Name = "fsiPanel7";
			this.fsiPanel7.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel7.TabIndex = 6;
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.txtKeisanFugo);
			this.fsiPanel6.Controls.Add(this.lblKamokbn);
			this.fsiPanel6.Controls.Add(this.lblKeisanhugo);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel6.Location = new System.Drawing.Point(4, 179);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel6.TabIndex = 5;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.txtKojoTaishoShikriKomok);
			this.fsiPanel5.Controls.Add(this.lblKojoTaishoShikriKomok);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 144);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel5.TabIndex = 4;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtKojoKomokNm);
			this.fsiPanel4.Controls.Add(this.lblKojoKomokNm);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 109);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel4.TabIndex = 3;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtKojoKomokSettei);
			this.fsiPanel3.Controls.Add(this.lblKojoKomokSettei);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 74);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtSetteiCd);
			this.fsiPanel2.Controls.Add(this.lblSetteiCd);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 39);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtShishoCd);
			this.fsiPanel1.Controls.Add(this.lblShishoNm);
			this.fsiPanel1.Controls.Add(this.lblShisho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(885, 28);
			this.fsiPanel1.TabIndex = 0;
			// 
			// fsiTableLayoutPanel2
			// 
			this.fsiTableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel2.ColumnCount = 1;
			this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel17, 0, 4);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel16, 0, 3);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel15, 0, 2);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel14, 0, 1);
			this.fsiTableLayoutPanel2.Controls.Add(this.fsiPanel13, 0, 0);
			this.fsiTableLayoutPanel2.Location = new System.Drawing.Point(25, 459);
			this.fsiTableLayoutPanel2.Name = "fsiTableLayoutPanel2";
			this.fsiTableLayoutPanel2.RowCount = 5;
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel2.Size = new System.Drawing.Size(893, 173);
			this.fsiTableLayoutPanel2.TabIndex = 1008;
			// 
			// fsiPanel17
			// 
			this.fsiPanel17.Controls.Add(this.label6);
			this.fsiPanel17.Controls.Add(this.txtShikiriSeisanKbn);
			this.fsiPanel17.Controls.Add(this.label5);
			this.fsiPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel17.Location = new System.Drawing.Point(4, 140);
			this.fsiPanel17.Name = "fsiPanel17";
			this.fsiPanel17.Size = new System.Drawing.Size(885, 29);
			this.fsiPanel17.TabIndex = 4;
			// 
			// fsiPanel16
			// 
			this.fsiPanel16.Controls.Add(this.lblShzNrkHohoNm);
			this.fsiPanel16.Controls.Add(this.txtKomoku);
			this.fsiPanel16.Controls.Add(this.txtShohizeiNyuryokuHoho);
			this.fsiPanel16.Controls.Add(this.lblShohizeiNyuryokuHoho);
			this.fsiPanel16.Controls.Add(this.label4);
			this.fsiPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel16.Location = new System.Drawing.Point(4, 106);
			this.fsiPanel16.Name = "fsiPanel16";
			this.fsiPanel16.Size = new System.Drawing.Size(885, 27);
			this.fsiPanel16.TabIndex = 3;
			// 
			// fsiPanel15
			// 
			this.fsiPanel15.Controls.Add(this.txtScript);
			this.fsiPanel15.Controls.Add(this.label1);
			this.fsiPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel15.Location = new System.Drawing.Point(4, 72);
			this.fsiPanel15.Name = "fsiPanel15";
			this.fsiPanel15.Size = new System.Drawing.Size(885, 27);
			this.fsiPanel15.TabIndex = 2;
			// 
			// fsiPanel14
			// 
			this.fsiPanel14.Controls.Add(this.label3);
			this.fsiPanel14.Controls.Add(this.txtSeisanKbn);
			this.fsiPanel14.Controls.Add(this.label2);
			this.fsiPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel14.Location = new System.Drawing.Point(4, 38);
			this.fsiPanel14.Name = "fsiPanel14";
			this.fsiPanel14.Size = new System.Drawing.Size(885, 27);
			this.fsiPanel14.TabIndex = 1;
			// 
			// fsiPanel13
			// 
			this.fsiPanel13.Controls.Add(this.chkKeisanKbn);
			this.fsiPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel13.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel13.Name = "fsiPanel13";
			this.fsiPanel13.Size = new System.Drawing.Size(885, 27);
			this.fsiPanel13.TabIndex = 0;
			// 
			// HNCM1072
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1045, 745);
			this.Controls.Add(this.fsiTableLayoutPanel2);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1072";
			this.ShowFButton = true;
			this.Text = "控除項目の設定";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel2, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel12.ResumeLayout(false);
			this.fsiPanel12.PerformLayout();
			this.fsiPanel11.ResumeLayout(false);
			this.fsiPanel11.PerformLayout();
			this.fsiPanel10.ResumeLayout(false);
			this.fsiPanel10.PerformLayout();
			this.fsiPanel9.ResumeLayout(false);
			this.fsiPanel9.PerformLayout();
			this.fsiPanel8.ResumeLayout(false);
			this.fsiPanel8.PerformLayout();
			this.fsiPanel7.ResumeLayout(false);
			this.fsiPanel7.PerformLayout();
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.fsiTableLayoutPanel2.ResumeLayout(false);
			this.fsiPanel17.ResumeLayout(false);
			this.fsiPanel17.PerformLayout();
			this.fsiPanel16.ResumeLayout(false);
			this.fsiPanel16.PerformLayout();
			this.fsiPanel15.ResumeLayout(false);
			this.fsiPanel15.PerformLayout();
			this.fsiPanel14.ResumeLayout(false);
			this.fsiPanel14.PerformLayout();
			this.fsiPanel13.ResumeLayout(false);
			this.fsiPanel13.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtSetteiCd;
        private System.Windows.Forms.Label lblSetteiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKojoKomokSettei;
        private System.Windows.Forms.Label lblKojoKomokSettei;
        private System.Windows.Forms.Label lblKojoKomokNm;
        private System.Windows.Forms.Label lblKojoTaishoShikriKomok;
        private jp.co.fsi.common.controls.FsiTextBox txtKojoTaishoShikriKomok;
        private jp.co.fsi.common.controls.FsiTextBox txtKeisanFugo;
        private System.Windows.Forms.Label lblKeisanhugo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokCd;
        private System.Windows.Forms.Label lblKanjoKamokCd;
        private System.Windows.Forms.Label lblHojoKamokCd;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokCd;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblBumonCd;
        private System.Windows.Forms.Label lblZeiKbnn;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKbn;
        private System.Windows.Forms.Label lblZeiKbn;
        private System.Windows.Forms.Label lblJigyoKbn;
        private jp.co.fsi.common.controls.FsiTextBox txtJigyoKbn;
        private System.Windows.Forms.Label lblJigyoKubn;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakKbn;
        private System.Windows.Forms.Label lblTaishakKbnn;
        private System.Windows.Forms.Label lblTaishakKbn;
        private System.Windows.Forms.Label lblBumonnCd;
        private System.Windows.Forms.Label lblHojoKamokuCd;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private System.Windows.Forms.Label lblKamokbn;
        private common.controls.FsiTextBox txtKojoKomokNm;
        private common.controls.FsiTextBox txtShishoCd;
        private System.Windows.Forms.Label lblShishoNm;
        private System.Windows.Forms.Label lblShisho;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private common.controls.FsiTextBox txtScript;
        private System.Windows.Forms.Label label1;
        private common.controls.FsiTextBox txtKomoku;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkKeisanKbn;
        private common.controls.FsiTextBox txtShikiriSeisanKbn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel12;
		private common.FsiPanel fsiPanel11;
		private common.FsiPanel fsiPanel10;
		private common.FsiPanel fsiPanel9;
		private common.FsiPanel fsiPanel8;
		private common.FsiPanel fsiPanel7;
		private common.FsiPanel fsiPanel6;
		private common.FsiPanel fsiPanel5;
		private common.FsiPanel fsiPanel4;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel2;
		private common.FsiPanel fsiPanel17;
		private common.FsiPanel fsiPanel16;
		private common.FsiPanel fsiPanel15;
		private common.FsiPanel fsiPanel14;
		private common.FsiPanel fsiPanel13;
	}
}