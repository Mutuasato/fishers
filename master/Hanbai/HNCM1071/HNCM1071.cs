﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1071
{
    /// <summary>
    /// 控除項目の登録(HNCM1071)
    /// </summary>
    public partial class HNCM1071 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "控除科目の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1071()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            string shishoCd;
            // Par1が"1"の場合、コード検索画面としての挙動をする
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
//                this.Size = new Size(698, 550);
                this.Size = new Size(918, 788);
                // フォームの配置を上へ移動する
                //this.lblShisho.Location = new System.Drawing.Point(13, 13);
                //this.txtShishoCd.Location = new System.Drawing.Point(96, 15);
                //this.lblShishoNm.Location = new System.Drawing.Point(131, 15);
                //this.lblMeisho.Location = new System.Drawing.Point(13, 46);
                //this.txtMeisho.Location = new System.Drawing.Point(96, 48);
                //this.dgvList.Location = new System.Drawing.Point(12, 74);
                //this.dgvList.Size = new Size(450, 359);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnEnter.Location = this.btnF2.Location;
                this.btnF1.Location = this.btnF3.Location;
                this.btnEsc.Visible = true;
                this.btnEnter.Visible = true;
                this.btnF1.Visible = true;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = true;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;

                shishoCd = (this.Par2 == "1") ? "1" : this.Par2;

            }
#if DEBUG
            shishoCd = "1"; //UInfo.ShishoCd;
#else
            shishoCd = this.UInfo.ShishoCd;
#endif
            this.txtShishoCd.Text = shishoCd;
            this.lblShishoNm.Text = Dba.GetName(UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);
            this.txtShishoCd.Enabled = (shishoCd == "1" && this.Par1 == "") ? true : false;

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // カナ名にフォーカス
            this.txtMeisho.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // 名称にフォーカスを戻す
            this.txtMeisho.Focus();
            this.txtMeisho.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // 控除項目の登録画面の起動
            EditKojoKmok(string.Empty);
        }
#endregion

#region イベント
        /// <summary>
        /// 支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtShishoCd.Text, this.lblShishoNm.Text, this.txtShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtShishoCd.SelectAll();
                this.txtShishoCd.Focus();
            }
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EditKojoKmok(Util.ToString(this.dgvList.SelectedRows[0].Cells["設定コード"].Value));
                e.Handled = true;
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditKojoKmok(Util.ToString(this.dgvList.SelectedRows[0].Cells["設定コード"].Value));
        }
#endregion

#region privateメソッド
        /// <summary>
        /// 支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 控除項目ではすべては不可とする
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtShishoCd.Text) || Equals(this.txtShishoCd.Text, "0"))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtShishoCd.Text, this.txtShishoCd.Text);

            if (ValChk.IsEmpty(this.lblShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            string shishoCd = this.txtShishoCd.Text;

            // 控除科目ビューからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal( shishoCd));
            StringBuilder where = new StringBuilder("HN.KAISHA_CD = @KAISHA_CD");
            where.Append(" AND HN.KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND HN.SHISHO_CD = @SHISHO_CD");
            if (isInitial)
            {
                //where.Append(" AND HN.SETTEI_CD = -1");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtMeisho.Text))
                {
                    where.Append(" AND HN.KOJO_KOMOKU_NM LIKE @KOJO_KOMOKU_NM");
                    
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@KOJO_KOMOKU_NM", SqlDbType.VarChar, 42, "%" + this.txtMeisho.Text + "%");
                }
            }
            string cols = "HN.SETTEI_CD AS 設定コード";
            cols += ", HN.KOJO_KOMOKU_SETTEI AS 控除項目設定";
            cols += ", HN.KOJO_KOMOKU_NM AS 控除項目名称";

            string from = "VI_HN_KOJO_KAMOKU_SETTEI AS HN";

            DataTable dtkanjo =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "HN.SETTEI_CD", dpc);
                
            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtkanjo.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtkanjo.Rows.Add(dtkanjo.NewRow());
            }
            this.dgvList.DataSource = dtkanjo;
            
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 140;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //if (MODE_CD_SRC.Equals(this.Par1))
            //{
            //    this.dgvList.Columns[1].Width = 150;
            //    this.dgvList.Columns[2].Width = 150;
            //}
            //else
            //{
                this.dgvList.Columns[1].Width = 370;
                this.dgvList.Columns[2].Width = 350;
            //}
        }

        /// <summary>
        /// 控除項目を追加編集する
        /// </summary>
        /// <param name="code">設定コード(空：新規登録、以外：編集)</param>
        private void EditKojoKmok(string code)
        {
            string shishoCd = this.txtShishoCd.Text;

            HNCM1072 frmHNCM1072;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHNCM1072 = new HNCM1072("1");
                frmHNCM1072.Par2 = shishoCd;
            }
            else
            {
                // 編集モードで登録画面を起動
                frmHNCM1072 = new HNCM1072("2");
                frmHNCM1072.InData = code;
                frmHNCM1072.Par2 = shishoCd;
            }

            DialogResult result = frmHNCM1072.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["設定コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }
        #endregion
    }
}
