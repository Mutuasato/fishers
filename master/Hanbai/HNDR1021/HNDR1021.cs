﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Globalization;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hndr1021
{
    /// <summary>
    /// 水揚仕切書出力(HANR2021)
    /// </summary>
    public partial class HNDR1021 : BasePgForm
    {
        #region 構造体
        /// <summary>
        /// 印刷ワークテーブルのデータ構造体
        /// </summary>
        struct NumVals
        {
            public decimal SURYO;
            public decimal KINGAKU;
            public decimal SHOHIZEI;

            public void Clear()
            {
                SURYO = 0;
                KINGAKU = 0;
                SHOHIZEI = 0;
            }
        }

        /// <summary>
        /// 会社・支所情報構造体
        /// </summary>
        struct KaishaVals
        {
            public string KaishaNm;
            public string KaishaAddr;
            public string KaishaTel;
            public string KaishaFax;
            public string ShishoNm;            
        }
        // 会社・支所情報変数（出力時に取得）
        private KaishaVals Kjoho;

        //消費税コレクション
        private Dictionary<decimal, decimal> ZeiList;
        private Dictionary<decimal, decimal> ZeiKinList;
        private Dictionary<decimal, decimal> ZeiList8;
        private Dictionary<decimal, decimal> ZeiKinList8;
        #endregion

        #region 定数
        /// <summary>
        /// 精算区分(地区内ｾﾘ)
        /// </summary>
        private const string CHIKUNAI = "3";

        /// <summary>
        /// 精算区分(浜売り)
        /// </summary>
        private const string HAMA_URI = "2";

        /// <summary>
        /// 精算区分(地区外ｾﾘ)
        /// </summary>
        private const string CHIKUGAI = "1";

        /// <summary>
        /// ｾﾘ日付
        /// </summary>
        private DateTime seriDate = DateTime.Today;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNDR1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
#if DEBUG
            // 水揚支所
            this.txtMizuageShishoCd.Text = "1"; 
#else
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd; //"1"; 
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Today, this.Dba);
            int yobiNum = Util.ToInt(DateTime.Today.DayOfWeek.ToString("d"));
            string[] shiharaiDate;

            // 翌週の火曜日をセット
            shiharaiDate = Util.ConvJpDate(DateTime.Today.AddDays(9 - yobiNum), this.Dba);

            // 日付(セリ日付)
            lblSeriDateGengo.Text = jpDate[0];
            txtSeriDateYear.Text = jpDate[2];
            txtSeriDateMonth.Text = jpDate[3];
            txtSeriDateDay.Text = jpDate[4];
            // 日付(支払予定日)
            lblShiharaiDateGengo.Text = shiharaiDate[0];
            txtShiharaiDateYear.Text = shiharaiDate[2];
            txtShiharaiDateMonth.Text = shiharaiDate[3];
            txtShiharaiDateDay.Text = shiharaiDate[4];
            // 精算区分は初期値全て
            this.txtSeisanKbn.Text = "0";
            IsValidSeisanKbn();

            // フォーカス設定
            this.txtSeriDateYear.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年),船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtSeriDateYear":
                case "txtShiharaiDateYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                case "txtSeisanKbn":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtSeriDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeriDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSeriDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblSeriDateGengo.Text, this.txtSeriDateYear.Text,
                                    this.txtSeriDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtSeriDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtSeriDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblSeriDateGengo.Text,
                                        this.txtSeriDateYear.Text,
                                        this.txtSeriDateMonth.Text,
                                        this.txtSeriDateDay.Text,
                                        this.Dba);
                                this.lblSeriDateGengo.Text = arrJpDate[0];
                                this.txtSeriDateYear.Text = arrJpDate[2];
                                this.txtSeriDateMonth.Text = arrJpDate[3];
                                this.txtSeriDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtShiharaiDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblShiharaiDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblShiharaiDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblShiharaiDateGengo.Text, this.txtShiharaiDateYear.Text,
                                    this.txtShiharaiDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtShiharaiDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtShiharaiDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblShiharaiDateGengo.Text,
                                        this.txtShiharaiDateYear.Text,
                                        this.txtShiharaiDateMonth.Text,
                                        this.txtShiharaiDateDay.Text,
                                        this.Dba);
                                this.lblShiharaiDateGengo.Text = arrJpDate[0];
                                this.txtShiharaiDateYear.Text = arrJpDate[2];
                                this.txtShiharaiDateMonth.Text = arrJpDate[3];
                                this.txtShiharaiDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
                case "txtSeisanKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtSeisanKbn.Text = outData[0];
                                this.lblSeisanKbnNm.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNDR1021R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(セリ日付)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtSeriDateYear.Text, this.txtSeriDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtSeriDateYear.SelectAll();
            }
            else
            {
                this.txtSeriDateYear.Text = Util.ToString(IsValid.SetYear(this.txtSeriDateYear.Text));
                CheckJpSeri();
                SetJpSeri();
            }
        }

        /// <summary>
        /// 月(セリ日付)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtSeriDateMonth.Text, this.txtSeriDateMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtSeriDateMonth.SelectAll();
            }
            else
            {
                this.txtSeriDateMonth.Text = Util.ToString(IsValid.SetMonth(this.txtSeriDateMonth.Text));
                CheckJpSeri();
                SetJpSeri();
            }
        }

        /// <summary>
        /// 日(セリ日付)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtSeriDateDay.Text, this.txtSeriDateDay.MaxLength))
            {
                e.Cancel = true;
                this.txtSeriDateDay.SelectAll();
            }
            else
            {
                this.txtSeriDateDay.Text = Util.ToString(IsValid.SetDay(this.txtSeriDateDay.Text));
                CheckJpSeri();
                SetJpSeri();
            }
        }

        /// <summary>
        /// 年(支払予定日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiharaiDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtShiharaiDateYear.Text, this.txtShiharaiDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtShiharaiDateYear.SelectAll();
            }
            else
            {
                this.txtShiharaiDateYear.Text = Util.ToString(IsValid.SetYear(this.txtShiharaiDateYear.Text));
                CheckJpShiharai();
                SetJpShiharai();
            }
        }

        /// <summary>
        /// 月(支払予定日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiharaiDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtShiharaiDateMonth.Text, this.txtShiharaiDateMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtShiharaiDateMonth.SelectAll();
            }
            else
            {
                this.txtShiharaiDateMonth.Text = Util.ToString(IsValid.SetMonth(this.txtShiharaiDateMonth.Text));
                CheckJpShiharai();
                SetJpShiharai();
            }
        }

        /// <summary>
        /// 日(支払予定日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiharaiDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtShiharaiDateDay.Text, this.txtShiharaiDateDay.MaxLength))
            {
                e.Cancel = true;
                this.txtShiharaiDateDay.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                this.txtShiharaiDateDay.Text = Util.ToString(IsValid.SetDay(this.txtShiharaiDateDay.Text));
                CheckJpShiharai();
                SetJpShiharai();
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }

        /// <summary>
        /// 精算区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeisanKbn())
            {
                e.Cancel = true;
                this.txtSeisanKbn.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        private bool IsValidSeisanKbn()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtSeisanKbn.Text, this.txtSeisanKbn.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text) || this.txtSeisanKbn.Text == "0")
            {
                this.txtSeisanKbn.Text = "0";
                this.lblSeisanKbnNm.Text = "全て";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                Msg.Notice("精算区分は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", this.txtMizuageShishoCd.Text, this.txtSeisanKbn.Text);
                if (ValChk.IsEmpty(this.lblSeisanKbnNm.Text))
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpSeri()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeriDateGengo.Text, this.txtSeriDateYear.Text,
                this.txtSeriDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeriDateDay.Text) > lastDayInMonth)
            {
                this.txtSeriDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpSeri()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpSeri(Util.FixJpDate(this.lblSeriDateGengo.Text, this.txtSeriDateYear.Text,
                this.txtSeriDateMonth.Text, this.txtSeriDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpShiharai()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblShiharaiDateGengo.Text, this.txtShiharaiDateYear.Text,
                this.txtShiharaiDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtShiharaiDateDay.Text) > lastDayInMonth)
            {
                this.txtShiharaiDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpShiharai()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpShiharai(Util.FixJpDate(this.lblShiharaiDateGengo.Text, this.txtShiharaiDateYear.Text,
                this.txtShiharaiDateMonth.Text, this.txtShiharaiDateDay.Text, this.Dba));
        }


        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpSeri(string[] arrJpDate)
        {
            this.lblSeriDateGengo.Text = arrJpDate[0];
            this.txtSeriDateYear.Text = arrJpDate[2];
            this.txtSeriDateMonth.Text = arrJpDate[3];
            this.txtSeriDateDay.Text = arrJpDate[4];
        }
        private void SetJpShiharai(string[] arrJpDate)
        {
            this.lblShiharaiDateGengo.Text = arrJpDate[0];
            this.txtShiharaiDateYear.Text = arrJpDate[2];
            this.txtShiharaiDateMonth.Text = arrJpDate[3];
            this.txtShiharaiDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Notice("コード(自)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Notice("コード(至)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtSeriDateYear.Text, this.txtSeriDateYear.MaxLength))
            {
                this.txtSeriDateYear.Focus();
                this.txtSeriDateYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtSeriDateMonth.Text, this.txtSeriDateMonth.MaxLength))
            {
                this.txtSeriDateMonth.Focus();
                this.txtSeriDateMonth.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtSeriDateDay.Text, this.txtSeriDateDay.MaxLength))
            {
                this.txtSeriDateDay.Focus();
                this.txtSeriDateDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpSeri();
            // 年月日(自)の正しい和暦への変換処理
            SetJpSeri();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtShiharaiDateYear.Text, this.txtShiharaiDateYear.MaxLength))
            {
                this.txtShiharaiDateYear.Focus();
                this.txtShiharaiDateYear.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtShiharaiDateMonth.Text, this.txtShiharaiDateMonth.MaxLength))
            {
                this.txtShiharaiDateMonth.Focus();
                this.txtShiharaiDateMonth.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtShiharaiDateDay.Text, this.txtShiharaiDateDay.MaxLength))
            {
                this.txtShiharaiDateDay.Focus();
                this.txtShiharaiDateDay.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpShiharai();
            // 年月日(至)の正しい和暦への変換処理
            SetJpShiharai();

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetSeriDate(string[] arrJpDate)
        {
            this.lblSeriDateGengo.Text = arrJpDate[0];
            this.txtSeriDateYear.Text = arrJpDate[2];
            this.txtSeriDateMonth.Text = arrJpDate[3];
            this.txtSeriDateDay.Text = arrJpDate[4];
        }
        private void SetShiharaiDate(string[] arrJpDate)
        {
            this.lblShiharaiDateGengo.Text = arrJpDate[0];
            this.txtShiharaiDateYear.Text = arrJpDate[2];
            this.txtShiharaiDateMonth.Text = arrJpDate[3];
            this.txtShiharaiDateDay.Text = arrJpDate[4];
        }

        ///// <summary>
        ///// 検索サブウィンドウオープン
        ///// </summary>
        ///// <param name="moduleName">例："COMC8111"</param>
        ///// <param name="para1">例："TB_CM_SHISHO"</param>
        ///// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        ///// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        ///// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        //private String[] openSearchWindow(String moduleName, String para1, String indata)
        //{
        //    string[] result = { "", "" };

        //    // ネームスペースに使うモジュール名の小文字
        //    string lowerModuleName = moduleName.ToLower();

        //    // ネームスペースの末尾
        //    string nameSpace = lowerModuleName.Substring(0, 3);

        //    // アセンブリのロード
        //    Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

        //    // フォーム作成
        //    string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
        //    Type t = asm.GetType(moduleNameSpace);

        //    if (t != null)
        //    {
        //        Object obj = Activator.CreateInstance(t);
        //        if (obj != null)
        //        {
        //            BasePgForm frm = (BasePgForm)obj;
        //            frm.Par1 = para1;
        //            frm.InData = indata;
        //            frm.ShowDialog(this);

        //            if (frm.DialogResult == DialogResult.OK)
        //            {
        //                string[] ret = (string[])frm.OutData;
        //                result[0] = ret[0];
        //                result[1] = ret[1];
        //                return result;
        //            }
        //        }
        //    }

        //    return result;
        //}

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false)
        {
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    #region "取得列の定義"
                    StringBuilder cols = Util.ColsArray(127, "");
                    #endregion

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNDR1021R rpt = new HNDR1021R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {

            bool dataFlag =false;


            #region データ取得の準備
            // 日付(セリ日付)を西暦にして取得
            DateTime tmpSRDate = Util.ConvAdDate(this.lblSeriDateGengo.Text, this.txtSeriDateYear.Text,
                    this.txtSeriDateMonth.Text, this.txtSeriDateDay.Text, this.Dba);
            // 日付を和暦で保持
            string[] tmpSRWareki = Util.ConvJpDate(tmpSRDate, this.Dba);
            tmpSRWareki[3] = Util.PadZero(tmpSRWareki[3], 2);
            tmpSRWareki[4] = Util.PadZero(tmpSRWareki[4], 2);

            //日付（支払予定日）を西暦取得
            DateTime tmpSYDate = Util.ConvAdDate(this.lblShiharaiDateGengo.Text, this.txtShiharaiDateYear.Text,
                this.txtShiharaiDateMonth.Text, this.txtShiharaiDateDay.Text, this.Dba);

            //日付（支払予定日）を和暦で保持
            string[] tmpSYWareki = Util.ConvJpDate(tmpSYDate, this.Dba);
            tmpSYWareki[3] = Util.PadZero(tmpSYWareki[3], 2);
            tmpSYWareki[4] = Util.PadZero(tmpSYWareki[4], 2);

            #region 船主コード設定
            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "99999";
            }
            #endregion
            #region 支所コードの退避
            //支所コードの退避
            string shishoCd;
            if (this.txtMizuageShishoCd.Text == "0" || this.txtMizuageShishoCd.Text == "")
            {
                shishoCd = "0";
            }
            else
            {
                shishoCd = this.txtMizuageShishoCd.Text;
            }
            #endregion
            #region 精算区分設定
            // 精算区分設定
            string seisanKubunSettei = this.txtSeisanKbn.Text;
            #endregion

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            #region 変数宣言
            int dbSORT = 1;
            int i = 0; // メインループ用カウント変数
            int j = 7; // データ格納用変数
            int k = 7; // インサート項目数格納用変数
            int flg = 0; // インサートフラグ
            int flgLast = 0; // 合計表示用ページ表示フラグ
            int pageCount = 1; // 改ページ用カウント変数
            int tmpLastPageData = 0; // 船主毎最終頁表示データ残数
            decimal tmpMeisaiCount = 0; // 船主データ件数変数
            int tmpPage = 0; // 船主CD毎の件数によるﾍﾟｰｼﾞ設定変数
            decimal tmpGokeiKingaku = 0; // 船主CD毎の金額合計変数
            decimal tmpGokeiShohizei = 0; // 船主CD毎の消費税額合計変数
            decimal tmpGokei = 0; // 船主CD毎の金額合計+消費税額合計変数
            decimal kojoGokei = 0; // 船主CD毎の金額合計+消費税額合計変数
            int before = -1; // 改ページ比較用カウント変数
            int next = 1; // 改ページ比較用カウント変数
            int seisanKbn = 0;
            //消費税率毎消費税額の計算辞書
            ZeiList = new Dictionary<decimal, decimal>();
            ZeiKinList = new Dictionary<decimal, decimal>();
            ZeiList8 = new Dictionary<decimal, decimal>();
            ZeiKinList8 = new Dictionary<decimal, decimal>();


            #endregion
            #endregion

            #region 会社情報取得
            getKaishaJoho(shishoCd);
            #endregion

            #region メインデータ取得
            dpc = new DbParamCollection();
            sql = new StringBuilder();

            // han.VI_水揚仕切書(VI_HN_MIZUAGE_SHIKIRISHO)の日付から発生しているデータを取得
            sql.Append(" SELECT");
            sql.Append(" *");
            sql.Append(" FROM");
            sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SERIBI = @DATE_SR AND");
            sql.Append(" SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            if (shishoCd != "0")
            {
                sql.Append(" AND SHISHO_CD = @SHISHO_CD ");
            }
            if (seisanKubunSettei != "0")
            {
                sql.Append(" AND SEISAN_KUBUN = @SEISAN_KUBUN ");
            }
            sql.Append(" ORDER BY");
            sql.Append(" KAISHA_CD,");
            if (shishoCd != "")
            {
                sql.Append(" SHISHO_CD,");
            }
            sql.Append(" SENSHU_CD,");
            sql.Append(" SERIBI,");
            sql.Append(" GYOGYO_TESURYO,");
            sql.Append(" DENPYO_BANGO,");
            sql.Append(" GYO_NO");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_SR", SqlDbType.DateTime, tmpSRDate);
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, funanushiCdTo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, seisanKubunSettei);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            string strFunanusiCd = string.Empty;

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                NumVals GOKEI = new NumVals(); // 伝票グループ

                while (dtMainLoop.Rows.Count > i)
                {

                    #region 船主別のデータ件数を取得
                    // 現在の船主コードと前の船主コードが一致しない場合
                    if (i == 0)
                    {
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append(" SELECT");
                        sql.Append(" COUNT");
                        sql.Append(" (SENSHU_CD) AS MEISAI_COUNT");
                        sql.Append(" FROM");
                        sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
                        sql.Append(" WHERE");
                        sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        sql.Append(" SERIBI = @DATE_SR AND");
                        sql.Append(" SENSHU_CD = @SENSHU_CD");
                        sql.Append(" AND SHISHO_CD = @SHISHO_CD");
                        sql.Append(" AND SEISAN_KUBUN = @SEISAN_KUBUN");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@DATE_SR", SqlDbType.VarChar, 10, tmpSRDate.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(dtMainLoop.Rows[i]["SHISHO_CD"]));
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(dtMainLoop.Rows[i]["SEISAN_KUBUN"]));

                        DataTable dtFunanushiCount = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        tmpMeisaiCount = (decimal)(Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - 1) / 17;
                        tmpPage = (int)Math.Truncate(tmpMeisaiCount) + 1;
                        tmpLastPageData = Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - (int)Math.Truncate(tmpMeisaiCount) * 17;
                        if (tmpLastPageData == 17 || tmpLastPageData == 16)
                        {
                            tmpPage = tmpPage + 1;
                            flgLast = 1;
                        }
                    }
                    else if (Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[before]["SENSHU_CD"]))
                    {
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append(" SELECT");
                        sql.Append(" COUNT");
                        sql.Append(" (SENSHU_CD) AS MEISAI_COUNT");
                        sql.Append(" FROM");
                        sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
                        sql.Append(" WHERE");
                        sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        sql.Append(" SERIBI = @DATE_SR AND");
                        sql.Append(" SENSHU_CD = @SENSHU_CD");
                        sql.Append(" AND SEISAN_KUBUN = @SEISAN_KUBUN");
                        sql.Append(" AND SHISHO_CD = @SHISHO_CD");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@DATE_SR", SqlDbType.VarChar, 10, tmpSRDate.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(dtMainLoop.Rows[i]["SHISHO_CD"]));
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(dtMainLoop.Rows[i]["SEISAN_KUBUN"]));

                        DataTable dtFunanushiCount = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        tmpMeisaiCount = (decimal)(Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - 1) / 17;
                        tmpPage = (int)Math.Truncate(tmpMeisaiCount) + 1;
                        tmpLastPageData = Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - (int)Math.Truncate(tmpMeisaiCount) * 17;
                        if (tmpLastPageData == 17 || tmpLastPageData == 16)
                        {
                            tmpPage = tmpPage + 1;
                            flgLast = 1;
                        }
                    }
                    #endregion

                    #region 船主別の税抜合計、消費税額を取得
                    // 現在の船主コードと前の船主コードが一致しない場合
                    if (i == 0)
                    {
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();

                        sql.Append(" SELECT");
                        sql.Append(" SUM(MIZUAGE_ZEINUKI_KINGAKU) AS GOKEI_KINGAKU,");
                        sql.Append(" SUM(MIZUAGE_SHOHIZEIGAKU) AS GOKEI_SHOHIZEI");
                        sql.Append(" FROM");
                        sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
                        sql.Append(" WHERE");
                        sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        sql.Append(" SERIBI = @DATE_SR AND");
                        sql.Append(" SENSHU_CD = @SENSHU_CD AND");
                        sql.Append(" SEISAN_KUBUN = @SEISAN_KUBUN AND");
                        sql.Append(" SHISHO_CD = @SHISHO_CD AND");
                        sql.Append(" GYO_NO = 1");
                        sql.Append(" AND SHISHO_CD = @SHISHO_CD");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@DATE_SR", SqlDbType.VarChar, 10, tmpSRDate.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(dtMainLoop.Rows[i]["SHISHO_CD"]));
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(dtMainLoop.Rows[i]["SEISAN_KUBUN"]));

                        DataTable dtGokeiKingaku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        tmpGokeiKingaku = Util.ToInt(dtGokeiKingaku.Rows[0]["GOKEI_KINGAKU"]);
                        tmpGokeiShohizei = Util.ToInt(dtGokeiKingaku.Rows[0]["GOKEI_SHOHIZEI"]);

                    }
                    else if (Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[before]["SENSHU_CD"]))
                    {
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();

                        sql.Append(" SELECT");
                        sql.Append(" SUM(MIZUAGE_ZEINUKI_KINGAKU) AS GOKEI_KINGAKU,");
                        sql.Append(" SUM(MIZUAGE_SHOHIZEIGAKU) AS GOKEI_SHOHIZEI");
                        sql.Append(" FROM");
                        sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
                        sql.Append(" WHERE");
                        sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        sql.Append(" SERIBI = @DATE_SR AND");
                        sql.Append(" SENSHU_CD = @SENSHU_CD AND");
                        sql.Append(" SEISAN_KUBUN = @SEISAN_KUBUN AND");
                        sql.Append(" SHISHO_CD = @SHISHO_CD AND");
                        sql.Append(" GYO_NO = 1");
                        sql.Append(" AND SHISHO_CD = @SHISHO_CD");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@DATE_SR", SqlDbType.VarChar, 10, tmpSRDate.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(dtMainLoop.Rows[i]["SHISHO_CD"]));
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(dtMainLoop.Rows[i]["SEISAN_KUBUN"]));

                        DataTable dtGokeiKingaku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        tmpGokeiKingaku = Util.ToInt(dtGokeiKingaku.Rows[0]["GOKEI_KINGAKU"]);
                        tmpGokeiShohizei = Util.ToInt(dtGokeiKingaku.Rows[0]["GOKEI_SHOHIZEI"]);

                    }
                    #endregion

                    #region データのセット
                    // データを1行毎にセット
                    if (j == 7)
                    {
                        dpc.SetParam("@ITEM0" + j, SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YAMA_NO"]); // 山NO
                        j++;
                        dpc.SetParam("@ITEM0" + j, SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOSHU_NM"]); // 魚種名称
                        j++;
                        dpc.SetParam("@ITEM0" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["TANKA"])); // 単価
                        j++;

                        if (50 == Util.ToDecimal(dtMainLoop.Rows[i]["ZEI_KUBUN"]))
                        {
                            dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, "* " +Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU"])); // 金額
                        }
                        else
                        {
                            dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU"])); // 金額
                        }
                        j++;
                    }
                    else
                    {
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YAMA_NO"]); // 山NO
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOSHU_NM"]); // 魚種名称
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["TANKA"])); // 単価
                        j++;
                        if (50 == Util.ToDecimal(dtMainLoop.Rows[i]["ZEI_KUBUN"]))
                        {
                            dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, "* " + Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU"])); // 金額
                        }
                        else
                        {
                            dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU"])); // 金額
                        }
                        j++;
                    }

                    // 現在の仲買人コードと次の仲買人コードが一致しない場合、フラグをたてる
                    if (Util.ToInt(dtMainLoop.Rows.Count) != next && Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[next]["SENSHU_CD"]))
                    {
                        flg = 1;
                    }
                    // 仲買人毎の最終ページではない & ページの最大までデータがセットされた場合、フラグをたてる
                    else if (pageCount != tmpPage && j == 92)
                    {
                        flg = 1;
                    }

                    // 最後のデータの場合、フラグをたてる
                    if (Util.ToInt(dtMainLoop.Rows.Count) == next)
                    {
                        flg = 1;
                    }

                    // 合計金額の足しこみ
                    GOKEI.SURYO += Util.ToDecimal(dtMainLoop.Rows[i]["SURYO"]);
                    GOKEI.KINGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]);

                    decimal Zeikgaku = Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI_FURIWAKE"]);
                    decimal Zeigaku = Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                    //税率毎消費税金額
                    if (Util.ToDecimal(dtMainLoop.Rows[i]["ZEI_RITSU"]) != 0)
                    {

                        decimal ZeiRitsu = Util.ToDecimal(dtMainLoop.Rows[i]["ZEI_RITSU"]);

                        if ("50" == dtMainLoop.Rows[i]["ZEI_KUBUN"].ToString())
                        {
                            if (ZeiList8.ContainsKey(ZeiRitsu) == true)
                            {
                                ZeiList8[ZeiRitsu] += Zeikgaku;
                            }
                            else
                            {
                                ZeiList8.Add(ZeiRitsu, Zeikgaku);
                            }
                            if (ZeiKinList8.ContainsKey(ZeiRitsu) == true)
                            {
                                ZeiKinList8[ZeiRitsu] += Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]);
                            }
                            else
                            {
                                ZeiKinList8.Add(ZeiRitsu, Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]));
                            }
                        }
                        else
                        {
                            if (ZeiList.ContainsKey(ZeiRitsu) == true)
                            {
                                ZeiList[ZeiRitsu] += Zeikgaku;
                            }
                            else
                            {
                                ZeiList.Add(ZeiRitsu, Zeikgaku);
                            }
                            if (ZeiKinList.ContainsKey(ZeiRitsu) == true)
                            {
                                ZeiKinList[ZeiRitsu] += Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]);
                            }
                            else
                            {
                                ZeiKinList.Add(ZeiRitsu, Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]));
                            }
                        }
                    }
                    #endregion

                    #region データのインサート
                    // フラグが立っていればインサート
                    if (flg == 1)
                    {
                        sql = new StringBuilder();
                        sql.Append("INSERT INTO PR_HN_TBL(");
                        sql.Append("  GUID");
                        sql.Append(" ,SORT");
                        sql.Append(" ,ITEM01");
                        sql.Append(" ,ITEM02");
                        sql.Append(" ,ITEM03");
                        sql.Append(" ,ITEM04");
                        sql.Append(" ,ITEM05");
                        sql.Append(" ,ITEM06");
                        while (k < j)
                        {
                            if (k < 10)
                            {
                                sql.Append(" ,ITEM0" + k);
                            }
                            else
                            {
                                sql.Append(" ,ITEM" + k);
                            }

                            k++;
                        }
                        // 各船主データの最終頁
                        if (pageCount == tmpPage)
                        {
                            sql.Append(" ,ITEM82");
                            sql.Append(" ,ITEM83");
                            sql.Append(" ,ITEM84");
                            sql.Append(" ,ITEM85");
                            sql.Append(" ,ITEM86");
                            sql.Append(" ,ITEM87");
                            sql.Append(" ,ITEM88");
                            sql.Append(" ,ITEM89");
                            sql.Append(" ,ITEM90");
                            sql.Append(" ,ITEM91");
                        }
                        sql.Append(" ,ITEM92");
                        sql.Append(" ,ITEM93");
                        sql.Append(" ,ITEM94");
                        sql.Append(" ,ITEM95");
                        sql.Append(" ,ITEM96");
                        sql.Append(" ,ITEM97");
                        sql.Append(" ,ITEM98");
                        sql.Append(" ,ITEM99");
                        sql.Append(" ,ITEM100");
                        sql.Append(" ,ITEM101");
                        sql.Append(" ,ITEM102");
                        sql.Append(" ,ITEM103");
                        sql.Append(" ,ITEM104");
                        sql.Append(" ,ITEM105");
                        sql.Append(" ,ITEM106");
                        sql.Append(" ,ITEM107");
                        sql.Append(" ,ITEM108");
                        sql.Append(" ,ITEM109");
                        sql.Append(" ,ITEM110");
                        sql.Append(" ,ITEM111");
                        sql.Append(" ,ITEM112");
                        sql.Append(" ,ITEM113");
                        sql.Append(" ,ITEM114");
                        sql.Append(" ,ITEM115");
                        sql.Append(" ,ITEM116");
                        sql.Append(" ,ITEM117");
                        sql.Append(" ,ITEM118");
                        sql.Append(" ,ITEM119");
                        sql.Append(" ,ITEM120");
                        sql.Append(" ,ITEM121");
                        sql.Append(" ,ITEM122");
                        sql.Append(" ,ITEM123");
                        sql.Append(" ,ITEM124");
                        sql.Append(" ,ITEM125");
                        sql.Append(" ,ITEM126");
                        sql.Append(" ,ITEM127");

                        k = 7;

                        sql.Append(") ");
                        sql.Append("VALUES(");
                        sql.Append("  @GUID");
                        sql.Append(" ,@SORT");
                        sql.Append(" ,@ITEM01");
                        sql.Append(" ,@ITEM02");
                        sql.Append(" ,@ITEM03");
                        sql.Append(" ,@ITEM04");
                        sql.Append(" ,@ITEM05");
                        sql.Append(" ,@ITEM06");
                        while (k < j)
                        {
                            if (k < 10)
                            {
                                sql.Append(" ,@ITEM0" + k);
                            }
                            else
                            {
                                sql.Append(" ,@ITEM" + k);
                            }

                            k++;
                        }
                        // 各船主データの最終頁
                        if (pageCount == tmpPage)
                        {
                            sql.Append(" ,@ITEM82");
                            sql.Append(" ,@ITEM83");
                            sql.Append(" ,@ITEM84");
                            sql.Append(" ,@ITEM85");
                            sql.Append(" ,@ITEM86");
                            sql.Append(" ,@ITEM87");
                            sql.Append(" ,@ITEM88");
                            sql.Append(" ,@ITEM89");
                            sql.Append(" ,@ITEM90");
                            sql.Append(" ,@ITEM91");
                        }
                        sql.Append(" ,@ITEM92");
                        sql.Append(" ,@ITEM93");
                        sql.Append(" ,@ITEM94");
                        sql.Append(" ,@ITEM95");
                        sql.Append(" ,@ITEM96");
                        sql.Append(" ,@ITEM97");
                        sql.Append(" ,@ITEM98");
                        sql.Append(" ,@ITEM99");
                        sql.Append(" ,@ITEM100");
                        sql.Append(" ,@ITEM101");
                        sql.Append(" ,@ITEM102");
                        sql.Append(" ,@ITEM103");
                        sql.Append(" ,@ITEM104");
                        sql.Append(" ,@ITEM105");
                        sql.Append(" ,@ITEM106");
                        sql.Append(" ,@ITEM107");
                        sql.Append(" ,@ITEM108");
                        sql.Append(" ,@ITEM109");
                        sql.Append(" ,@ITEM110");
                        sql.Append(" ,@ITEM111");
                        sql.Append(" ,@ITEM112");
                        sql.Append(" ,@ITEM113");
                        sql.Append(" ,@ITEM114");
                        sql.Append(" ,@ITEM115");
                        sql.Append(" ,@ITEM116");
                        sql.Append(" ,@ITEM117");
                        sql.Append(" ,@ITEM118");
                        sql.Append(" ,@ITEM119");
                        sql.Append(" ,@ITEM120");
                        sql.Append(" ,@ITEM121");
                        sql.Append(" ,@ITEM122");
                        sql.Append(" ,@ITEM123");
                        sql.Append(" ,@ITEM124");
                        sql.Append(" ,@ITEM125");
                        sql.Append(" ,@ITEM126");
                        sql.Append(" ,@ITEM127");

                        sql.Append(") ");

                        // 各船主データの最終頁
                        if (pageCount == tmpPage)
                        {
                            #region データ登録
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpSRWareki[5]); // セリ日
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSRWareki[3]); // セリ日(月)
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSRWareki[4]); // セリ日(日)
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.PadZero(pageCount, 4)); // 改ページ用カウント
                            dpc.SetParam("@ITEM82", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM83", SqlDbType.VarChar, 200, "税抜合計");
                            dpc.SetParam("@ITEM84", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM85", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM86", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokeiKingaku)); // 税抜合計
                            dpc.SetParam("@ITEM87", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM88", SqlDbType.VarChar, 200, "消費税");
                            dpc.SetParam("@ITEM89", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM90", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM91", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokeiShohizei)); // 消費税
                            dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, "合計");
                            dpc.SetParam("@ITEM94", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SURYO, 1));
                            dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU / GOKEI.SURYO));
                            tmpGokei = tmpGokeiKingaku + tmpGokeiShohizei;
                            dpc.SetParam("@ITEM96", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei)); // 合計
                            dpc.SetParam("@ITEM97", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei)); // 水揚金額
                                                                                                        //控除項目の名称と金額の設定
                            seisanKbn = Util.ToInt(dtMainLoop.Rows[i]["SEISAN_KUBUN"]);
                            String[] KojoKomokuArray = GetKojoKomoku(shishoCd, seisanKbn);
                            int IDX = 0;
                            kojoGokei = 0;
                            foreach (string kj in KojoKomokuArray)
                            {
                                //金額パラメータ設定
                                string KjNm = "@ITEM" + Util.ToString(98 + IDX);
                                //string FldNm = "KOJO_KOMOKU" + Util.ToString(IDX + 1);

                                string FldNm = "KOJO_KOMOKU" + kj.Substring(0, kj.IndexOf(","));
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i][FldNm]));
                                //控除項目名称設定
                                KjNm = "@ITEM" + Util.ToString(110 + IDX);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, kj.Substring(kj.IndexOf(",") + 1));
                                kojoGokei += Util.ToDecimal(dtMainLoop.Rows[i][FldNm]);
                                IDX++;
                            }
                            //控除項目の残りを設定
                            for (int ni = IDX; ni < 10; ni++)
                            {
                                string KjNm = "@ITEM" + Util.ToString(98 + ni);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, Util.FormatNum(""));
                                //控除項目名称設定
                                KjNm = "@ITEM" + Util.ToString(110 + ni);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, "");
                            }
                            dpc.SetParam("@ITEM108", SqlDbType.VarChar, 200, Util.FormatNum(kojoGokei)); // 税込み控除額合計
                            dpc.SetParam("@ITEM109", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei - kojoGokei)); // 差引支払額                            // 摘要欄への「口座無し」出力設定
                            if (dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"].ToString() == ""
                                || Util.ToDecimal(dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"]) < 1)
                            {
                                dpc.SetParam("@ITEM121", SqlDbType.VarChar, 200, "口座無し"); // 差引支払額
                            }
                            else
                            {
                                dpc.SetParam("@ITEM121", SqlDbType.VarChar, 200, ""); // 差引支払額
                            }
                            dpc.SetParam("@ITEM120", SqlDbType.VarChar, 200, SetJapaneseLunisolar(Util.ToDate(tmpSRWareki[5]))); // 旧暦

                            dpc.SetParam("@ITEM122", SqlDbType.VarChar, 200, Kjoho.KaishaNm);   // 会社名
                            dpc.SetParam("@ITEM123", SqlDbType.VarChar, 200, Kjoho.KaishaAddr); // 住所
                            dpc.SetParam("@ITEM124", SqlDbType.VarChar, 200, Kjoho.KaishaTel);  // 電話番号
                            dpc.SetParam("@ITEM125", SqlDbType.VarChar, 200, Kjoho.ShishoNm);  // 電話番号

                            // 軽減税率対応
                            int intPer10 = 0;
                            int intVal10 = 0;
                            int intPer8 = 0;
                            int intVal8 = 0;

                            string Utiwake = string.Empty;
                            string sBuf = string.Empty;


                            if ((ZeiList8.Count > 0) && (ZeiList.Count > 0))
                            {
                                Utiwake = @"
内訳税抜き  {0}% {1} 円
内訳消費税  {2}% {3} 円
内訳税抜き  *{4}% {5} 円
内訳消費税  *{6}% {7} 円";
                                int intk10 = 0;
                                int intk8 = 0;

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiKinList)
                                {
                                    intk10 = int.Parse(keys.Key.ToString());
                                    intVal10 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiList)
                                {
                                    intPer10 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiKinList8)
                                {
                                    intk8 = int.Parse(keys.Key.ToString());
                                    intVal8 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiList8)
                                {
                                    intPer8 = int.Parse(keys.Value.ToString());
                                }
                                sBuf = string.Format(Utiwake, intk10,
                                  string.Format("{0:N0}", intVal10).PadLeft(10, ' '),
                                  intk10,
                                  string.Format("{0:N0}", intPer10).PadLeft(10, ' '),
                                  intk8,
                                  string.Format("{0:N0}", intVal8).PadLeft(10, ' '),
                                  intk8,
                                  string.Format("{0:N0}", intPer8).PadLeft(10, ' '));

                            }
                            else if ((ZeiList8.Count == 0) && (ZeiList.Count > 0))
                            {

                                Utiwake =
                                    @"
内訳税抜き  {0}% {1} 円
内訳消費税  {2}% {3} 円";

                                int intk = 0;

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiKinList)
                                {
                                    intk = int.Parse(keys.Key.ToString());
                                    intVal10 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiList)
                                {
                                    intPer10 = int.Parse(keys.Value.ToString());
                                }

                                sBuf = string.Format(Utiwake, intk, string.Format("{0:N0}", intVal10).PadLeft(10, ' '), intk, string.Format("{0:N0}", intPer10).PadLeft(10, ' '));

                            }
                            else if ((ZeiList8.Count > 0) && (ZeiList.Count == 0))
                            {
                                Utiwake =
                                    @"
内訳税抜き  *{0}% {1} 円
内訳消費税  *{2}% {3} 円";
                                int intk = 0;

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiKinList8)
                                {
                                    intk = int.Parse(keys.Key.ToString());

                                    intVal8 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiList8)
                                {
                                    intPer8 = int.Parse(keys.Value.ToString());
                                }

                                sBuf = string.Format(Utiwake, intk, string.Format("{0:N0}", intVal8).PadLeft(10, ' '), intk, string.Format("{0:N0}", intPer8).PadLeft(10, ' '));


                            }


                            //foreach (KeyValuePair<decimal, decimal> dic in ZeiList)
                            //{
                            //    Utiwake += "消費税率(" + string.Format("{0, 2}", dic.Key) + "%) " +
                            //        Util.FormatNum(Util.ToString(dic.Value)) + "円\r\n";
                            //}
//                            ZeiUtiwake = Utiwake;
                            dpc.SetParam("@ITEM126", SqlDbType.VarChar, 200, sBuf);  // 消費税内訳
                            
                            dpc.SetParam("@ITEM127", SqlDbType.VarChar, 200, tmpSYWareki[5]);  // 支払予定日 add 20181114

                            this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                            // ページ数リセット
                            pageCount = 1;

                            // 合計金額リセット
                            GOKEI.Clear();
                            ZeiList.Clear();
                            ZeiKinList.Clear();
                            ZeiList8.Clear();
                            ZeiKinList8.Clear();
                            #endregion
                        }
                        else
                        {
                            #region データ登録
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpSRWareki[5]); // セリ日(年)
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSRWareki[3]); // セリ日(月)
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSRWareki[4]); // セリ日(日)
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.PadZero(pageCount, 4)); // 改ページ用カウント
                            dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, "小計");
                            dpc.SetParam("@ITEM94", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SURYO, 1));
                            dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU / GOKEI.SURYO));
                            dpc.SetParam("@ITEM96", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU)); // 合計
                            dpc.SetParam("@ITEM97", SqlDbType.VarChar, 200, ""); // 水揚金額
                                                                                    //控除項目を設定
                            seisanKbn = Util.ToInt(dtMainLoop.Rows[i]["SEISAN_KUBUN"]);
                            String[] KojoKomokuArray = GetKojoKomoku(shishoCd, seisanKbn);
                            int IDX = 0;
                            foreach (string kj in KojoKomokuArray)
                            {
                                //金額パラメータ設定
                                string KjNm = "@ITEM" + Util.ToString(98 + IDX);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, Util.FormatNum(""));
                                //控除項目名称設定
                                KjNm = "@ITEM" + Util.ToString(110 + IDX);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, kj);
                                IDX++;
                            }
                            //控除項目の残りを設定
                            for (int ni = IDX; ni < 10; ni++)
                            {
                                string KjNm = "@ITEM" + Util.ToString(98 + ni);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, Util.FormatNum(""));
                                //控除項目名称設定
                                KjNm = "@ITEM" + Util.ToString(110 + ni);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, "");
                            }
                            dpc.SetParam("@ITEM108", SqlDbType.VarChar, 200, ""); // 税込み控除額合計
                            dpc.SetParam("@ITEM109", SqlDbType.VarChar, 200, ""); // 差引支払額                            // 摘要欄への「口座無し」出力設定
                                                                                    // 摘要欄への「口座無し」出力設定
                            if (dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"].ToString() == ""
                                || Util.ToDecimal(dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"]) < 1)
                            {
                                dpc.SetParam("@ITEM121", SqlDbType.VarChar, 200, "口座無し"); // 差引支払額
                            }
                            else
                            {
                                dpc.SetParam("@ITEM121", SqlDbType.VarChar, 200, ""); // 差引支払額
                            }
                            dpc.SetParam("@ITEM120", SqlDbType.VarChar, 200, SetJapaneseLunisolar(Util.ToDate(tmpSRWareki[5]))); // 旧暦
                            dpc.SetParam("@ITEM122", SqlDbType.VarChar, 200, Kjoho.KaishaNm);   // 会社名
                            dpc.SetParam("@ITEM123", SqlDbType.VarChar, 200, Kjoho.KaishaAddr); // 住所
                            dpc.SetParam("@ITEM124", SqlDbType.VarChar, 200, Kjoho.KaishaTel);  // 電話番号
                            dpc.SetParam("@ITEM125", SqlDbType.VarChar, 200, Kjoho.ShishoNm);  // 電話番号
                            dpc.SetParam("@ITEM126", SqlDbType.VarChar, 200, "");  // 消費税内訳
                            dpc.SetParam("@ITEM127", SqlDbType.VarChar, 200, tmpSYWareki[5]);  // 支払予定日 add 20181114

                            this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                            pageCount++;

                            #endregion
                        }

                        // 合計表示用ページのインサート
                        if (flgLast == 1 && pageCount == tmpPage)
                        {
                            #region データ登録
                            #region "インサートＳＱＬの生成"
                            sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            sql.Append("INSERT INTO PR_HN_TBL(");
                            sql.Append("  GUID");
                            sql.Append(" ,SORT");
                            sql.Append(" ,ITEM01");
                            sql.Append(" ,ITEM02");
                            sql.Append(" ,ITEM03");
                            sql.Append(" ,ITEM04");
                            sql.Append(" ,ITEM05");
                            sql.Append(" ,ITEM06");
                            sql.Append(" ,ITEM82");
                            sql.Append(" ,ITEM83");
                            sql.Append(" ,ITEM84");
                            sql.Append(" ,ITEM85");
                            sql.Append(" ,ITEM86");
                            sql.Append(" ,ITEM87");
                            sql.Append(" ,ITEM88");
                            sql.Append(" ,ITEM89");
                            sql.Append(" ,ITEM90");
                            sql.Append(" ,ITEM91");
                            sql.Append(" ,ITEM92");
                            sql.Append(" ,ITEM93");
                            sql.Append(" ,ITEM94");
                            sql.Append(" ,ITEM95");
                            sql.Append(" ,ITEM96");
                            sql.Append(" ,ITEM97");
                            sql.Append(" ,ITEM98");
                            sql.Append(" ,ITEM99");
                            sql.Append(" ,ITEM100");
                            sql.Append(" ,ITEM101");
                            sql.Append(" ,ITEM102");
                            sql.Append(" ,ITEM103");
                            sql.Append(" ,ITEM104");
                            sql.Append(" ,ITEM105");
                            sql.Append(" ,ITEM106");
                            sql.Append(" ,ITEM107");
                            sql.Append(" ,ITEM108");
                            sql.Append(" ,ITEM109");
                            sql.Append(" ,ITEM110");
                            sql.Append(" ,ITEM111");
                            sql.Append(" ,ITEM112");
                            sql.Append(" ,ITEM113");
                            sql.Append(" ,ITEM114");
                            sql.Append(" ,ITEM115");
                            sql.Append(" ,ITEM116");
                            sql.Append(" ,ITEM117");
                            sql.Append(" ,ITEM118");
                            sql.Append(" ,ITEM119");
                            sql.Append(" ,ITEM120");
                            sql.Append(" ,ITEM121");
                            sql.Append(" ,ITEM122");
                            sql.Append(" ,ITEM123");
                            sql.Append(" ,ITEM124");
                            sql.Append(" ,ITEM125");
                            sql.Append(" ,ITEM126");
                            sql.Append(" ,ITEM127");

                            sql.Append(") ");
                            sql.Append("VALUES(");
                            sql.Append("  @GUID");
                            sql.Append(" ,@SORT");
                            sql.Append(" ,@ITEM01");
                            sql.Append(" ,@ITEM02");
                            sql.Append(" ,@ITEM03");
                            sql.Append(" ,@ITEM04");
                            sql.Append(" ,@ITEM05");
                            sql.Append(" ,@ITEM06");
                            sql.Append(" ,@ITEM82");
                            sql.Append(" ,@ITEM83");
                            sql.Append(" ,@ITEM84");
                            sql.Append(" ,@ITEM85");
                            sql.Append(" ,@ITEM86");
                            sql.Append(" ,@ITEM87");
                            sql.Append(" ,@ITEM88");
                            sql.Append(" ,@ITEM89");
                            sql.Append(" ,@ITEM90");
                            sql.Append(" ,@ITEM91");
                            sql.Append(" ,@ITEM92");
                            sql.Append(" ,@ITEM93");
                            sql.Append(" ,@ITEM94");
                            sql.Append(" ,@ITEM95");
                            sql.Append(" ,@ITEM96");
                            sql.Append(" ,@ITEM97");
                            sql.Append(" ,@ITEM98");
                            sql.Append(" ,@ITEM99");
                            sql.Append(" ,@ITEM100");
                            sql.Append(" ,@ITEM101");
                            sql.Append(" ,@ITEM102");
                            sql.Append(" ,@ITEM103");
                            sql.Append(" ,@ITEM104");
                            sql.Append(" ,@ITEM105");
                            sql.Append(" ,@ITEM106");
                            sql.Append(" ,@ITEM107");
                            sql.Append(" ,@ITEM108");
                            sql.Append(" ,@ITEM109");
                            sql.Append(" ,@ITEM110");
                            sql.Append(" ,@ITEM111");
                            sql.Append(" ,@ITEM112");
                            sql.Append(" ,@ITEM113");
                            sql.Append(" ,@ITEM114");
                            sql.Append(" ,@ITEM115");
                            sql.Append(" ,@ITEM116");
                            sql.Append(" ,@ITEM117");
                            sql.Append(" ,@ITEM118");
                            sql.Append(" ,@ITEM119");
                            sql.Append(" ,@ITEM120");
                            sql.Append(" ,@ITEM121");
                            sql.Append(" ,@ITEM122");
                            sql.Append(" ,@ITEM123");
                            sql.Append(" ,@ITEM124");
                            sql.Append(" ,@ITEM125");
                            sql.Append(" ,@ITEM126");
                            sql.Append(" ,@ITEM127");
                            sql.Append(") ");
                            #endregion

                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpSRWareki[5]); // セリ日(年)
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSRWareki[3]); // セリ日(月)
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSRWareki[4]); // セリ日(日)
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.PadZero(pageCount, 4)); // 改ページ用カウント
                            dpc.SetParam("@ITEM82", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM83", SqlDbType.VarChar, 200, "税抜合計");
                            dpc.SetParam("@ITEM84", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM85", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM86", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokeiKingaku)); // 税抜合計
                            dpc.SetParam("@ITEM87", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM88", SqlDbType.VarChar, 200, "消費税");
                            dpc.SetParam("@ITEM89", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM90", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM91", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokeiShohizei)); // 消費税
                            dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, "合計");
                            dpc.SetParam("@ITEM94", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SURYO, 1));
                            dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU / GOKEI.SURYO));
                            tmpGokei = tmpGokeiKingaku + tmpGokeiShohizei;
                            dpc.SetParam("@ITEM96", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei)); // 合計
                            dpc.SetParam("@ITEM97", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei)); // 水揚金額
                            seisanKbn = Util.ToInt(dtMainLoop.Rows[i]["SEISAN_KUBUN"]);
                            String[] KojoKomokuArray = GetKojoKomoku(shishoCd, seisanKbn);
                            int IDX = 0;
                            kojoGokei = 0;
                            foreach (string kj in KojoKomokuArray)
                            {
                                //金額パラメータ設定
                                string KjNm = "@ITEM" + Util.ToString(98 + IDX);
                                string FldNm = "KOJO_KOMOKU" + Util.ToString(IDX + 1);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i][FldNm]));
                                //控除項目名称設定
                                KjNm = "@ITEM" + Util.ToString(110 + IDX);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, kj);
                                kojoGokei += Util.ToDecimal(dtMainLoop.Rows[i][FldNm]);
                                IDX++;
                            }
                            //控除項目の残りを設定
                            for (int ni = IDX; ni < 10; ni++)
                            {
                                string KjNm = "@ITEM" + Util.ToString(98 + ni);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, Util.FormatNum(0));
                                //控除項目名称設定
                                KjNm = "@ITEM" + Util.ToString(110 + ni);
                                dpc.SetParam(KjNm, SqlDbType.VarChar, 200, "");
                            }
                            dpc.SetParam("@ITEM108", SqlDbType.VarChar, 200, Util.FormatNum(kojoGokei)); // 税込み控除額合計
                            dpc.SetParam("@ITEM109", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei - kojoGokei)); // 差引支払額                            // 摘要欄への「口座無し」出力設定

                            // 摘要欄への「口座無し」出力設定
                            if (dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"].ToString() == ""
                                || Util.ToDecimal(dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"]) < 1)
                            {
                                dpc.SetParam("@ITEM121", SqlDbType.VarChar, 200, "口座無し"); // 差引支払額
                            }
                            else
                            {
                                dpc.SetParam("@ITEM121", SqlDbType.VarChar, 200, ""); // 差引支払額
                            }
                            dpc.SetParam("@ITEM120", SqlDbType.VarChar, 200, SetJapaneseLunisolar(Util.ToDate(tmpSRWareki[5]))); // 旧暦
                            dpc.SetParam("@ITEM122", SqlDbType.VarChar, 200, Kjoho.KaishaNm);   // 会社名
                            dpc.SetParam("@ITEM123", SqlDbType.VarChar, 200, Kjoho.KaishaAddr); // 住所
                            dpc.SetParam("@ITEM124", SqlDbType.VarChar, 200, Kjoho.KaishaTel);  // 電話番号
                            dpc.SetParam("@ITEM125", SqlDbType.VarChar, 200, Kjoho.ShishoNm);  // 支所名



                            // 軽減税率対応
                            int intPer10 = 0;
                            int intVal10 = 0;
                            int intPer8 = 0;
                            int intVal8 = 0;

                            string Utiwake = string.Empty;
                            string sBuf = string.Empty;


                            if ((ZeiList8.Count > 0) && (ZeiList.Count > 0))
                            {
                                Utiwake = @"
内訳税抜き  {0}% {1} 円
内訳消費税  {2}% {3} 円
内訳税抜き *{4}% {5} 円
内訳消費税 *{6}% {7} 円";
                                int intk10 = 0;
                                int intk8 = 0;

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiKinList)
                                {
                                    intk10 = int.Parse(keys.Key.ToString());
                                    intVal10 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiList)
                                {
                                    intPer10 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiKinList8)
                                {
                                    intk8 = int.Parse(keys.Key.ToString());
                                    intVal8 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiList8)
                                {
                                    intPer8 = int.Parse(keys.Value.ToString());
                                }
                                sBuf = string.Format(Utiwake, intk10,
                                  string.Format("{0:N0}", intVal10).PadLeft(10, ' '),
                                  intk10,
                                  string.Format("{0:N0}", intPer10).PadLeft(10, ' '),
                                  intk8,
                                  string.Format("{0:N0}", intVal8).PadLeft(10, ' '),
                                  intk8,
                                  string.Format("{0:N0}", intPer8).PadLeft(10, ' '));

                            }
                            else if ((ZeiList8.Count == 0) && (ZeiList.Count > 0))
                            {

                                Utiwake =
                                    @"
内訳税抜き  {0}% {1} 円
内訳消費税  {2}% {3} 円";

                                int intk = 0;

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiKinList)
                                {
                                    intk = int.Parse(keys.Key.ToString());
                                    intVal10 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiList)
                                {
                                    intPer10 = int.Parse(keys.Value.ToString());
                                }

                                sBuf = string.Format(Utiwake, intk, string.Format("{0:N0}", intVal10).PadLeft(10, ' '), intk, string.Format("{0:N0}", intPer10).PadLeft(10, ' '));

                            }
                            else if ((ZeiList8.Count > 0) && (ZeiList.Count == 0))
                            {
                                Utiwake =
                                    @"
内訳税抜き  {0}% {1} 円
内訳消費税  {2}% {3} 円";
                                int intk = 0;

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiKinList8)
                                {
                                    intk = int.Parse(keys.Key.ToString());

                                    intVal8 = int.Parse(keys.Value.ToString());
                                }

                                foreach (KeyValuePair<decimal, decimal> keys in ZeiList8)
                                {
                                    intPer8 = int.Parse(keys.Value.ToString());
                                }

                                sBuf = string.Format(Utiwake, intk, string.Format("{0:N0}", intVal8).PadLeft(10, ' '), intk, string.Format("{0:N0}", intPer8).PadLeft(10, ' '));


                            }


//                            string ZeiUtiwake;
//                            string Utiwake = "";
                            foreach (KeyValuePair<decimal, decimal> dic in ZeiList)
                            {
                                Utiwake += "消費税率(" + string.Format("{0, 2}", dic.Key) + "%) " +
                                    Util.FormatNum(Util.ToString(dic.Value)) + "円\r\n";
                            }
//                            ZeiUtiwake = Utiwake;
                            dpc.SetParam("@ITEM126", SqlDbType.VarChar, 200, sBuf);  // 消費税内訳
                            dpc.SetParam("@ITEM127", SqlDbType.VarChar, 200, tmpSYWareki[5]);  // 支払予定日 add 20181114


                            this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                            // ページ数リセット
                            pageCount = 1;

                            flgLast = 0;

                            // 合計金額リセット
                            GOKEI.Clear();
                            ZeiList.Clear();
                            ZeiKinList.Clear();
                            ZeiList8.Clear();
                            ZeiKinList8.Clear();
                            #endregion
                        }
                        flg = 0;
                        j = 7;
                        k = 7;

                        dpc = new DbParamCollection();

                    }
                    #endregion

                    i++;
                    before++;
                    next++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }
            
            return dataFlag;
        }

        /// <summary>
        /// 支所毎精算区分毎の控除項目設定の内容を取得
        /// </summary>
        /// <param name="shishoCd"></param>
        /// <param name="seisanKbn"></param>
        /// <returns>string[]</returns>
        private string[] GetKojoKomoku(string shishoCd, int seisanKbn)
        {
            //控除項目の取得
            DbParamCollection dpk = new DbParamCollection();
            StringBuilder sqlk = new StringBuilder();
            sqlk.Append(" SELECT * FROM VI_HN_KOJO_KAMOKU_SETTEI ");
            sqlk.Append(" WHERE ");
            sqlk.Append(" KAISHA_CD = @KAISHA_CD AND ");
            sqlk.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            sqlk.Append(" SHISHO_CD = @SHISHO_CD ");
            dpk.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, UInfo.KaishaCd);
            dpk.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, UInfo.KaikeiNendo);
            dpk.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DataTable KojoTable = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sqlk), dpk);
            string[] KojoKomokuArray = new string[0];
            int i = 0;
            foreach (DataRow dr in KojoTable.Rows)
            {
                string Kbn = Util.ToString(dr["SHIKIRISHO_SEISAN_KBN"]);
                string[] seisan_kbn = Kbn.Split(',');
                foreach (string seikbn in seisan_kbn)
                {
                    if (seisanKbn.Equals(Util.ToInt( seikbn)) || seikbn == "0")
                    {
                        //i = Util.ToInt(dr["SETTEI_CD"]);
                        Array.Resize(ref KojoKomokuArray, KojoKomokuArray.Length + 1);
                        KojoKomokuArray[i] = Util.ToInt(dr["SETTEI_CD"]) + "," + Util.ToString(dr["KOJO_KOMOKU_NM"]);
                        i++;
                    }
                }

            }
            return KojoKomokuArray;
        }

        /// <summary>
        /// 半角数字を全角数字に変換する。
        /// </summary>
        /// <param name="s">変換する文字列</param>
        /// <returns>変換後の文字列</returns>
        static public string HanToZenNum(string s)
        {
            string ret = s;
            ret = ret.Replace("0", "０");
            ret = ret.Replace("1", "１");
            ret = ret.Replace("2", "２");
            ret = ret.Replace("3", "３");
            ret = ret.Replace("4", "４");
            ret = ret.Replace("5", "５");
            ret = ret.Replace("6", "６");
            ret = ret.Replace("7", "７");
            ret = ret.Replace("8", "８");
            ret = ret.Replace("9", "９");
            ret = ret.Replace("-", "―");
            return ret;
        }

        /// <summary>
        /// 会社・支所情報の取得
        /// </summary>
        /// <param name="shishoCd"></param>
        private void getKaishaJoho(string shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            shishoCd = (shishoCd == "0") ? "1" : shishoCd;

            sql.Append(" SELECT * FROM VI_CM_SHISHO_KAISHA_JOHO ");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            sql.Append(" SHISHO_CD = @SHISHO_CD ");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            if (dt.Rows.Count != 0)
            {
                Kjoho.KaishaNm = Util.ToString(dt.Rows[0]["KAISHA_NM"]);
                Kjoho.ShishoNm = Util.ToString(dt.Rows[0]["SHISHO_NM"]);
                if (shishoCd == "1")
                {
                    Kjoho.KaishaAddr = HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_JUSHO1"])) + " " +
                        HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_JUSHO2"])) + " " +
                        HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_JUSHO3"]));
                    Kjoho.KaishaTel = HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_DENWA_BANGO"]));
                    Kjoho.KaishaFax = HanToZenNum(Util.ToString(dt.Rows[0]["KAISHA_FAX_BANGO"]));
                }
                else
                {
                    Kjoho.KaishaAddr = (Util.ToString(dt.Rows[0]["SHISHO_JUSHO1"])) + " " +
                        HanToZenNum(Util.ToString(dt.Rows[0]["SHISHO_JUSHO2"])) + " " +
                        HanToZenNum(Util.ToString(dt.Rows[0]["SHISHO_JUSHO3"]));
                    Kjoho.KaishaTel = HanToZenNum(Util.ToString(dt.Rows[0]["SHISHO_DENWA_BANGO"]));
                    Kjoho.KaishaFax = HanToZenNum(Util.ToString(dt.Rows[0]["SHISHO_FAX_BANGO"]));
                }
            }
            else
            {
                Kjoho.KaishaNm = UInfo.KaishaNm;
                Kjoho.ShishoNm = UInfo.ShishoNm;
            }
        }

        //JapaneseLunisolarCalendar（日本の太陰太陽暦＝旧暦）
        private string SetJapaneseLunisolar(DateTime selectDay)
        {
             JapaneseLunisolarCalendar jpnOldDays = new JapaneseLunisolarCalendar();

            int oldMonth = jpnOldDays.GetMonth(selectDay); //旧暦の月を取得
            int oldDay = jpnOldDays.GetDayOfMonth(selectDay);//旧暦の日を取得　
            //閏月を取得
            int uruMonth = jpnOldDays.GetLeapMonth(
                    jpnOldDays.GetYear(selectDay),
                    jpnOldDays.GetEra(selectDay));
            //閏月がある場合の旧暦月の補正
            if ((uruMonth > 0) && (oldMonth - uruMonth >= 0))
            {
                oldMonth = oldMonth - 1;              //旧暦月の補正
            }
            return oldMonth.ToString() + "月" + " " + oldDay.ToString() + "日";
        }
        #endregion
    }
}
