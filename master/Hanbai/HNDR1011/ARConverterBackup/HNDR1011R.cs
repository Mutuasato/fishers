﻿using System.Data;
using jp.co.fsi.common.util;
using jp.co.fsi.common.report;

namespace jp.co.fsi.hn.hndr1011
{
    /// <summary>
    /// HANR2011R の概要の説明です。
    /// </summary>
    public partial class HNDR1011R : BaseReport
    {
        public HNDR1011R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
