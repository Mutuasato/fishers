﻿namespace jp.co.fsi.hn.hndr1011
{
    /// <summary>
    /// HANR2011R の概要の説明です。
    /// </summary>
    partial class HNDR1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HNDR1011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txt_ITEM07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt_ITEM08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt_ITEM09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt_ITEM10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt_ITEM11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.crossSectionBox2 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txt_ITEM07,
            this.txt_ITEM08,
            this.txt_ITEM09,
            this.txt_ITEM10,
            this.txt_ITEM11,
            this.line5,
            this.line7,
            this.line9,
            this.line11,
            this.line13,
            this.line15});
            this.detail.Height = 0.3427713F;
            this.detail.Name = "detail";
            // 
            // txt_ITEM07
            // 
            this.txt_ITEM07.DataField = "ITEM07";
            this.txt_ITEM07.Height = 0.21875F;
            this.txt_ITEM07.Left = 0.01023622F;
            this.txt_ITEM07.Name = "txt_ITEM07";
            this.txt_ITEM07.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.txt_ITEM07.Tag = "";
            this.txt_ITEM07.Text = "ITEM07";
            this.txt_ITEM07.Top = 0.05472441F;
            this.txt_ITEM07.Width = 0.4220473F;
            // 
            // txt_ITEM08
            // 
            this.txt_ITEM08.DataField = "ITEM08";
            this.txt_ITEM08.Height = 0.21875F;
            this.txt_ITEM08.Left = 0.484252F;
            this.txt_ITEM08.MultiLine = false;
            this.txt_ITEM08.Name = "txt_ITEM08";
            this.txt_ITEM08.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 1";
            this.txt_ITEM08.Tag = "";
            this.txt_ITEM08.Text = "ITEM08";
            this.txt_ITEM08.Top = 0.05472441F;
            this.txt_ITEM08.Width = 1.467717F;
            // 
            // txt_ITEM09
            // 
            this.txt_ITEM09.DataField = "ITEM09";
            this.txt_ITEM09.Height = 0.21875F;
            this.txt_ITEM09.Left = 1.951969F;
            this.txt_ITEM09.Name = "txt_ITEM09";
            this.txt_ITEM09.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.txt_ITEM09.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.txt_ITEM09.Tag = "";
            this.txt_ITEM09.Text = "ITEM09";
            this.txt_ITEM09.Top = 0.05472441F;
            this.txt_ITEM09.Width = 0.6562991F;
            // 
            // txt_ITEM10
            // 
            this.txt_ITEM10.DataField = "ITEM10";
            this.txt_ITEM10.Height = 0.21875F;
            this.txt_ITEM10.Left = 2.639764F;
            this.txt_ITEM10.Name = "txt_ITEM10";
            this.txt_ITEM10.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.txt_ITEM10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.txt_ITEM10.Tag = "";
            this.txt_ITEM10.Text = "ITEM10";
            this.txt_ITEM10.Top = 0.05472441F;
            this.txt_ITEM10.Width = 0.7574801F;
            // 
            // txt_ITEM11
            // 
            this.txt_ITEM11.DataField = "ITEM11";
            this.txt_ITEM11.Height = 0.21875F;
            this.txt_ITEM11.Left = 3.438977F;
            this.txt_ITEM11.Name = "txt_ITEM11";
            this.txt_ITEM11.OutputFormat = resources.GetString("txt_ITEM11.OutputFormat");
            this.txt_ITEM11.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.txt_ITEM11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.txt_ITEM11.Tag = "";
            this.txt_ITEM11.Text = "ITEM11";
            this.txt_ITEM11.Top = 0.05472441F;
            this.txt_ITEM11.Width = 1.223228F;
            // 
            // line5
            // 
            this.line5.Height = 0F;
            this.line5.Left = 0.01023622F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.3358268F;
            this.line5.Width = 7.072982F;
            this.line5.X1 = 0.01023622F;
            this.line5.X2 = 7.083218F;
            this.line5.Y1 = 0.3358268F;
            this.line5.Y2 = 0.3358268F;
            // 
            // line7
            // 
            this.line7.Height = 0.3358268F;
            this.line7.Left = 0.4472441F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 0.4472441F;
            this.line7.X2 = 0.4472441F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.3358268F;
            // 
            // line9
            // 
            this.line9.Height = 0.3358268F;
            this.line9.Left = 1.951969F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 1.951969F;
            this.line9.X2 = 1.951969F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.3358268F;
            // 
            // line11
            // 
            this.line11.Height = 0.3358268F;
            this.line11.Left = 2.639764F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 2.639764F;
            this.line11.X2 = 2.639764F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.3358268F;
            // 
            // line13
            // 
            this.line13.Height = 0.3358268F;
            this.line13.Left = 3.433858F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 3.433858F;
            this.line13.X2 = 3.433858F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.3358268F;
            // 
            // line15
            // 
            this.line15.Height = 0.3358268F;
            this.line15.Left = 4.805F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 4.805F;
            this.line15.X2 = 4.805F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.3358268F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM05";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // groupHeader2
            // 
            this.groupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox2,
            this.line16,
            this.line17,
            this.shape2,
            this.label12,
            this.line18,
            this.label13,
            this.line19,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.label14,
            this.label15,
            this.label16,
            this.textBox7,
            this.label17,
            this.label18,
            this.label19,
            this.line20,
            this.label20,
            this.line22,
            this.label21,
            this.line30,
            this.label22,
            this.line31,
            this.label23,
            this.crossSectionBox2,
            this.textBox1,
            this.line21,
            this.textBox17});
            this.groupHeader2.DataField = "ITEM01";
            this.groupHeader2.Height = 1.972441F;
            this.groupHeader2.Name = "groupHeader2";
            this.groupHeader2.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM06";
            this.textBox2.Height = 0.21875F;
            this.textBox2.Left = 0.4272381F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM06";
            this.textBox2.Top = 1.205922F;
            this.textBox2.Width = 2.212451F;
            // 
            // line16
            // 
            this.line16.Height = 0F;
            this.line16.Left = 0.2551908F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 1.48189F;
            this.line16.Width = 2.760417F;
            this.line16.X1 = 0.2551908F;
            this.line16.X2 = 3.015608F;
            this.line16.Y1 = 1.48189F;
            this.line16.Y2 = 1.48189F;
            // 
            // line17
            // 
            this.line17.Height = 0F;
            this.line17.Left = 0.2551908F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 1.504725F;
            this.line17.Width = 2.760417F;
            this.line17.X1 = 0.2551908F;
            this.line17.X2 = 3.015608F;
            this.line17.Y1 = 1.504725F;
            this.line17.Y2 = 1.504725F;
            // 
            // shape2
            // 
            this.shape2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.shape2.Height = 0.3125F;
            this.shape2.Left = 0.005190849F;
            this.shape2.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape2.Top = 1.658268F;
            this.shape2.Width = 7.073156F;
            // 
            // label12
            // 
            this.label12.Height = 0.375F;
            this.label12.HyperLink = null;
            this.label12.Left = 1.791832F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align: justify; text" +
    "-justify: distribute-all-lines";
            this.label12.Text = "鮮魚代金明細書兼領収書";
            this.label12.Top = 0.2098425F;
            this.label12.Width = 3.489583F;
            // 
            // line18
            // 
            this.line18.Height = 0F;
            this.line18.Left = 0.01023622F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 1.658268F;
            this.line18.Width = 7.068185F;
            this.line18.X1 = 0.01023622F;
            this.line18.X2 = 7.078421F;
            this.line18.Y1 = 1.658268F;
            this.line18.Y2 = 1.658268F;
            // 
            // label13
            // 
            this.label13.Height = 0.2291667F;
            this.label13.HyperLink = null;
            this.label13.Left = 2.713705F;
            this.label13.Name = "label13";
            this.label13.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; ddo-char-set: 128";
            this.label13.Text = "様";
            this.label13.Top = 1.197917F;
            this.label13.Width = 0.3020833F;
            // 
            // line19
            // 
            this.line19.Height = 0F;
            this.line19.Left = 0.01023622F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 1.970866F;
            this.line19.Width = 7.067934F;
            this.line19.X1 = 0.01023622F;
            this.line19.X2 = 7.07817F;
            this.line19.Y1 = 1.970866F;
            this.line19.Y2 = 1.970866F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM02";
            this.textBox3.Height = 0.21875F;
            this.textBox3.Left = 5.43905F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM02";
            this.textBox3.Top = 0.8716536F;
            this.textBox3.Width = 0.2960629F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM03";
            this.textBox4.Height = 0.21875F;
            this.textBox4.Left = 5.912278F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM02";
            this.textBox4.Top = 0.870866F;
            this.textBox4.Width = 0.2960629F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM04";
            this.textBox5.Height = 0.21875F;
            this.textBox5.Left = 6.385506F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM02";
            this.textBox5.Top = 0.8716536F;
            this.textBox5.Width = 0.2960629F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM21";
            this.textBox6.Height = 0.21875F;
            this.textBox6.Left = 4.982357F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM02";
            this.textBox6.Top = 0.8716536F;
            this.textBox6.Width = 0.4566927F;
            // 
            // label14
            // 
            this.label14.Height = 0.2178314F;
            this.label14.HyperLink = null;
            this.label14.Left = 5.735113F;
            this.label14.Name = "label14";
            this.label14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: top; ddo-char-set: 1";
            this.label14.Text = "年";
            this.label14.Top = 0.8795279F;
            this.label14.Width = 0.1770501F;
            // 
            // label15
            // 
            this.label15.Height = 0.2178314F;
            this.label15.HyperLink = null;
            this.label15.Left = 6.208341F;
            this.label15.Name = "label15";
            this.label15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: top; ddo-char-set: 1";
            this.label15.Text = "月";
            this.label15.Top = 0.8803154F;
            this.label15.Width = 0.1770501F;
            // 
            // label16
            // 
            this.label16.Height = 0.2178314F;
            this.label16.HyperLink = null;
            this.label16.Left = 6.68157F;
            this.label16.Name = "label16";
            this.label16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: top; ddo-char-set: 1";
            this.label16.Text = "日";
            this.label16.Top = 0.8803154F;
            this.label16.Width = 0.1770501F;
            // 
            // textBox7
            // 
            this.textBox7.Height = 0.1228347F;
            this.textBox7.Left = 6.310785F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox7.SummaryGroup = "groupHeader1";
            this.textBox7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox7.Text = "page";
            this.textBox7.Top = 0.6439305F;
            this.textBox7.Width = 0.3229167F;
            // 
            // label17
            // 
            this.label17.Height = 0.1226706F;
            this.label17.HyperLink = null;
            this.label17.Left = 6.633538F;
            this.label17.Name = "label17";
            this.label17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label17.Text = "/";
            this.label17.Top = 0.6440946F;
            this.label17.Width = 0.08661409F;
            // 
            // label18
            // 
            this.label18.Height = 0.2291338F;
            this.label18.HyperLink = null;
            this.label18.Left = 0.0729074F;
            this.label18.Name = "label18";
            this.label18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: justify; te" +
    "xt-justify: distribute-all-lines; ddo-char-set: 1";
            this.label18.Text = "番号";
            this.label18.Top = 1.737795F;
            this.label18.Width = 0.3543307F;
            // 
            // label19
            // 
            this.label19.Height = 0.2291338F;
            this.label19.HyperLink = null;
            this.label19.Left = 0.9583408F;
            this.label19.Name = "label19";
            this.label19.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: justify; te" +
    "xt-justify: distribute-all-lines; ddo-char-set: 1";
            this.label19.Text = "魚種";
            this.label19.Top = 1.741732F;
            this.label19.Width = 0.6188976F;
            // 
            // line20
            // 
            this.line20.Height = 0.329527F;
            this.line20.Left = 0.4473169F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 1.658268F;
            this.line20.Width = 0F;
            this.line20.X1 = 0.4473169F;
            this.line20.X2 = 0.4473169F;
            this.line20.Y1 = 1.658268F;
            this.line20.Y2 = 1.987795F;
            // 
            // label20
            // 
            this.label20.Height = 0.2291338F;
            this.label20.HyperLink = null;
            this.label20.Left = 2.104009F;
            this.label20.Name = "label20";
            this.label20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: justify; te" +
    "xt-justify: distribute-all-lines; ddo-char-set: 1";
            this.label20.Text = "数量";
            this.label20.Top = 1.737795F;
            this.label20.Width = 0.4212599F;
            // 
            // line22
            // 
            this.line22.Height = 0.329527F;
            this.line22.Left = 2.639837F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 1.658268F;
            this.line22.Width = 0F;
            this.line22.X1 = 2.639837F;
            this.line22.X2 = 2.639837F;
            this.line22.Y1 = 1.658268F;
            this.line22.Y2 = 1.987795F;
            // 
            // label21
            // 
            this.label21.Height = 0.2291338F;
            this.label21.HyperLink = null;
            this.label21.Left = 2.786687F;
            this.label21.Name = "label21";
            this.label21.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: justify; te" +
    "xt-justify: distribute-all-lines; ddo-char-set: 1";
            this.label21.Text = "単価";
            this.label21.Top = 1.737795F;
            this.label21.Width = 0.5248032F;
            // 
            // line30
            // 
            this.line30.Height = 0.329527F;
            this.line30.Left = 3.433932F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 1.658268F;
            this.line30.Width = 0F;
            this.line30.X1 = 3.433932F;
            this.line30.X2 = 3.433932F;
            this.line30.Y1 = 1.658268F;
            this.line30.Y2 = 1.987795F;
            // 
            // label22
            // 
            this.label22.Height = 0.2291338F;
            this.label22.HyperLink = null;
            this.label22.Left = 3.685507F;
            this.label22.Name = "label22";
            this.label22.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: justify; te" +
    "xt-justify: distribute-all-lines; ddo-char-set: 1";
            this.label22.Text = "金額";
            this.label22.Top = 1.737795F;
            this.label22.Width = 0.7850396F;
            // 
            // line31
            // 
            this.line31.Height = 0.329527F;
            this.line31.Left = 4.804926F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 1.658F;
            this.line31.Width = 7.390976E-05F;
            this.line31.X1 = 4.805F;
            this.line31.X2 = 4.804926F;
            this.line31.Y1 = 1.658F;
            this.line31.Y2 = 1.987527F;
            // 
            // label23
            // 
            this.label23.Height = 0.2291338F;
            this.label23.HyperLink = null;
            this.label23.Left = 5.502042F;
            this.label23.Name = "label23";
            this.label23.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: justify; te" +
    "xt-justify: distribute-all-lines; ddo-char-set: 1";
            this.label23.Text = "摘要";
            this.label23.Top = 1.737795F;
            this.label23.Width = 0.722441F;
            // 
            // crossSectionBox2
            // 
            this.crossSectionBox2.Bottom = 2.458268F;
            this.crossSectionBox2.Left = 0F;
            this.crossSectionBox2.LineWeight = 1F;
            this.crossSectionBox2.Name = "crossSectionBox2";
            this.crossSectionBox2.Radius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.crossSectionBox2.Right = 7.072917F;
            this.crossSectionBox2.Top = 0F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.1228347F;
            this.textBox1.Left = 6.720079F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; ddo-char-set: 1";
            this.textBox1.SummaryGroup = "groupHeader1";
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox1.Text = "page";
            this.textBox1.Top = 0.6440945F;
            this.textBox1.Width = 0.3229167F;
            // 
            // line21
            // 
            this.line21.Height = 0.329527F;
            this.line21.Left = 1.952041F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 1.658268F;
            this.line21.Width = 0F;
            this.line21.X1 = 1.952041F;
            this.line21.X2 = 1.952041F;
            this.line21.Y1 = 1.658268F;
            this.line21.Y2 = 1.987795F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM05";
            this.textBox17.Height = 0.21875F;
            this.textBox17.Left = 1.204331F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox17.Tag = "";
            this.textBox17.Text = "ITEM05";
            this.textBox17.Top = 0.8803151F;
            this.textBox17.Width = 0.5874999F;
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape6,
            this.shape5,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.label26,
            this.label27,
            this.label28,
            this.shape7,
            this.label29,
            this.line24,
            this.label30,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.label31,
            this.label32,
            this.label33,
            this.label34,
            this.label35,
            this.label36,
            this.label37,
            this.label38,
            this.label39,
            this.line25,
            this.line26,
            this.line27,
            this.line28,
            this.line29,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.line23,
            this.label5,
            this.picture1,
            this.label6,
            this.label7});
            this.groupFooter2.Height = 2.52441F;
            this.groupFooter2.Name = "groupFooter2";
            // 
            // shape6
            // 
            this.shape6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.shape6.Height = 0.3366142F;
            this.shape6.Left = 2.187402F;
            this.shape6.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape6.Name = "shape6";
            this.shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape6.Top = 0.3417323F;
            this.shape6.Width = 1.010236F;
            // 
            // shape5
            // 
            this.shape5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.shape5.Height = 0.3366142F;
            this.shape5.Left = 0.00511837F;
            this.shape5.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape5.Name = "shape5";
            this.shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape5.Top = 0.3417323F;
            this.shape5.Width = 1.010236F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM15";
            this.textBox14.Height = 0.21875F;
            this.textBox14.Left = 1.037402F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox14.Tag = "";
            this.textBox14.Text = "ITEM15";
            this.textBox14.Top = 0.4216536F;
            this.textBox14.Width = 1.125984F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM20";
            this.textBox15.Height = 0.21875F;
            this.textBox15.Left = 3.226772F;
            this.textBox15.Name = "textBox15";
            this.textBox15.OutputFormat = resources.GetString("textBox15.OutputFormat");
            this.textBox15.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox15.Tag = "";
            this.textBox15.Text = "ITEM20";
            this.textBox15.Top = 0.4216536F;
            this.textBox15.Width = 1.220079F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM16";
            this.textBox16.Height = 0.21875F;
            this.textBox16.Left = 5.515355F;
            this.textBox16.Name = "textBox16";
            this.textBox16.OutputFormat = resources.GetString("textBox16.OutputFormat");
            this.textBox16.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox16.Tag = "";
            this.textBox16.Text = "ITEM16";
            this.textBox16.Top = 0.4216536F;
            this.textBox16.Width = 1.5563F;
            // 
            // label26
            // 
            this.label26.Height = 0.1980971F;
            this.label26.HyperLink = null;
            this.label26.Left = 0.1615488F;
            this.label26.Name = "label26";
            this.label26.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.label26.Text = "本日のお買い上げ代金は上記のとおりです。";
            this.label26.Top = 0.04993436F;
            this.label26.Width = 3.036452F;
            // 
            // label27
            // 
            this.label27.Height = 0.1570866F;
            this.label27.HyperLink = null;
            this.label27.Left = 0.1614176F;
            this.label27.Name = "label27";
            this.label27.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 1";
            this.label27.Text = "前回請求残";
            this.label27.Top = 0.4224409F;
            this.label27.Width = 0.7291667F;
            // 
            // label28
            // 
            this.label28.Height = 0.1570866F;
            this.label28.HyperLink = null;
            this.label28.Left = 2.203543F;
            this.label28.Name = "label28";
            this.label28.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 1";
            this.label28.Text = "今回買上高";
            this.label28.Top = 0.4224409F;
            this.label28.Width = 0.9791341F;
            // 
            // shape7
            // 
            this.shape7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.shape7.Height = 0.3318898F;
            this.shape7.Left = 4.470473F;
            this.shape7.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape7.Name = "shape7";
            this.shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape7.Top = 0.3417323F;
            this.shape7.Width = 1.031496F;
            // 
            // label29
            // 
            this.label29.Height = 0.1570866F;
            this.label29.HyperLink = null;
            this.label29.Left = 4.552362F;
            this.label29.Name = "label29";
            this.label29.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 1";
            this.label29.Text = "合計請求金額";
            this.label29.Top = 0.4216536F;
            this.label29.Width = 0.8866139F;
            // 
            // line24
            // 
            this.line24.Height = 0F;
            this.line24.Left = 0.01023622F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.6783465F;
            this.line24.Width = 7.068111F;
            this.line24.X1 = 0.01023622F;
            this.line24.X2 = 7.078347F;
            this.line24.Y1 = 0.6783465F;
            this.line24.Y2 = 0.6783465F;
            // 
            // label30
            // 
            this.label30.Height = 0.2083333F;
            this.label30.HyperLink = null;
            this.label30.Left = 0.4295278F;
            this.label30.Name = "label30";
            this.label30.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 1";
            this.label30.Text = "上記のとおり請求しますので、確認のうえ";
            this.label30.Top = 0.8015747F;
            this.label30.Width = 3.062599F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM22";
            this.textBox18.Height = 0.21875F;
            this.textBox18.Left = 4.035824F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox18.Tag = "";
            this.textBox18.Text = "ITEM02";
            this.textBox18.Top = 0.8023621F;
            this.textBox18.Width = 0.2960629F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM23";
            this.textBox19.Height = 0.21875F;
            this.textBox19.Left = 4.509029F;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox19.Tag = "";
            this.textBox19.Text = "ITEM02";
            this.textBox19.Top = 0.8015747F;
            this.textBox19.Width = 0.2960629F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM24";
            this.textBox20.Height = 0.21875F;
            this.textBox20.Left = 4.982257F;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox20.Tag = "";
            this.textBox20.Text = "ITEM02";
            this.textBox20.Top = 0.8023621F;
            this.textBox20.Width = 0.2960629F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM25";
            this.textBox21.Height = 0.21875F;
            this.textBox21.Left = 3.579134F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.textBox21.Tag = "";
            this.textBox21.Text = "ITEM02";
            this.textBox21.Top = 0.8023621F;
            this.textBox21.Width = 0.4566927F;
            // 
            // label31
            // 
            this.label31.Height = 0.2178314F;
            this.label31.HyperLink = null;
            this.label31.Left = 4.331871F;
            this.label31.Name = "label31";
            this.label31.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: top; ddo-char-set: 1";
            this.label31.Text = "年";
            this.label31.Top = 0.8102363F;
            this.label31.Width = 0.1770501F;
            // 
            // label32
            // 
            this.label32.Height = 0.2178314F;
            this.label32.HyperLink = null;
            this.label32.Left = 4.805093F;
            this.label32.Name = "label32";
            this.label32.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: top; ddo-char-set: 1";
            this.label32.Text = "月";
            this.label32.Top = 0.811024F;
            this.label32.Width = 0.1770501F;
            // 
            // label33
            // 
            this.label33.Height = 0.2178314F;
            this.label33.HyperLink = null;
            this.label33.Left = 5.27832F;
            this.label33.Name = "label33";
            this.label33.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: top; ddo-char-set: 1";
            this.label33.Text = "日";
            this.label33.Top = 0.811024F;
            this.label33.Width = 0.1770501F;
            // 
            // label34
            // 
            this.label34.Height = 0.2083333F;
            this.label34.HyperLink = null;
            this.label34.Left = 5.677561F;
            this.label34.Name = "label34";
            this.label34.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 1";
            this.label34.Text = "(当日が";
            this.label34.Top = 0.8015747F;
            this.label34.Width = 0.6055117F;
            // 
            // label35
            // 
            this.label35.Height = 0.2083333F;
            this.label35.HyperLink = null;
            this.label35.Left = 0.4472444F;
            this.label35.Name = "label35";
            this.label35.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 1";
            this.label35.Text = "休日の場合は翌営業日)までにお支払いください。";
            this.label35.Top = 1.028741F;
            this.label35.Width = 3.818504F;
            // 
            // label36
            // 
            this.label36.Height = 0.1562992F;
            this.label36.HyperLink = null;
            this.label36.Left = 3.41811F;
            this.label36.Name = "label36";
            this.label36.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.label36.Text = "漁協名";
            this.label36.Top = 1.314173F;
            this.label36.Width = 0.4700792F;
            // 
            // label37
            // 
            this.label37.Height = 0.1562992F;
            this.label37.HyperLink = null;
            this.label37.Left = 3.41811F;
            this.label37.Name = "label37";
            this.label37.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.label37.Text = "住 所";
            this.label37.Top = 1.735039F;
            this.label37.Width = 0.4700792F;
            // 
            // label38
            // 
            this.label38.Height = 0.1562992F;
            this.label38.HyperLink = null;
            this.label38.Left = 3.41811F;
            this.label38.Name = "label38";
            this.label38.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.label38.Text = "T E L";
            this.label38.Top = 1.930708F;
            this.label38.Width = 0.4700792F;
            // 
            // label39
            // 
            this.label39.Height = 0.1562992F;
            this.label39.HyperLink = null;
            this.label39.Left = 3.41811F;
            this.label39.Name = "label39";
            this.label39.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.label39.Text = "F A X";
            this.label39.Top = 2.126377F;
            this.label39.Width = 0.4700792F;
            // 
            // line25
            // 
            this.line25.Height = 0.3318894F;
            this.line25.Left = 1.015355F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.3417323F;
            this.line25.Width = 0F;
            this.line25.X1 = 1.015355F;
            this.line25.X2 = 1.015355F;
            this.line25.Y1 = 0.3417323F;
            this.line25.Y2 = 0.6736217F;
            // 
            // line26
            // 
            this.line26.Height = 0.3318894F;
            this.line26.Left = 3.197639F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 0.3417323F;
            this.line26.Width = 0F;
            this.line26.X1 = 3.197639F;
            this.line26.X2 = 3.197639F;
            this.line26.Y1 = 0.3417323F;
            this.line26.Y2 = 0.6736217F;
            // 
            // line27
            // 
            this.line27.Height = 0.3318894F;
            this.line27.Left = 2.187402F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 0.3417323F;
            this.line27.Width = 0F;
            this.line27.X1 = 2.187402F;
            this.line27.X2 = 2.187402F;
            this.line27.Y1 = 0.3417323F;
            this.line27.Y2 = 0.6736217F;
            // 
            // line28
            // 
            this.line28.Height = 0.3318894F;
            this.line28.Left = 4.470473F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 0.3417323F;
            this.line28.Width = 0F;
            this.line28.X1 = 4.470473F;
            this.line28.X2 = 4.470473F;
            this.line28.Y1 = 0.3417323F;
            this.line28.Y2 = 0.6736217F;
            // 
            // line29
            // 
            this.line29.Height = 0.3366137F;
            this.line29.Left = 5.50197F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 0.3417323F;
            this.line29.Width = 0F;
            this.line29.X1 = 5.50197F;
            this.line29.X2 = 5.50197F;
            this.line29.Y1 = 0.3417323F;
            this.line29.Y2 = 0.678346F;
            // 
            // label1
            // 
            this.label1.DataField = "ITEM27";
            this.label1.Height = 0.1979167F;
            this.label1.HyperLink = null;
            this.label1.Left = 4.005905F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 1";
            this.label1.Text = "名 護 漁 業 協 同 組 合";
            this.label1.Top = 1.272441F;
            this.label1.Width = 2.852757F;
            // 
            // label2
            // 
            this.label2.DataField = "ITEM28";
            this.label2.Height = 0.1586615F;
            this.label2.HyperLink = null;
            this.label2.Left = 4.006037F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.label2.Text = "沖縄県名護市城三丁目１番１号";
            this.label2.Top = 1.732611F;
            this.label2.Width = 2.852757F;
            // 
            // label3
            // 
            this.label3.DataField = "ITEM29";
            this.label3.Height = 0.1562993F;
            this.label3.HyperLink = null;
            this.label3.Left = 4.006037F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.label3.Text = "(０９８０)５２-２８１２";
            this.label3.Top = 1.930643F;
            this.label3.Width = 2.852757F;
            // 
            // label4
            // 
            this.label4.Height = 0.1562993F;
            this.label4.HyperLink = null;
            this.label4.Left = 4.006037F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.label4.Text = "０９８―８６１―０８１９";
            this.label4.Top = 2.126312F;
            this.label4.Width = 2.852757F;
            // 
            // line23
            // 
            this.line23.Height = 0F;
            this.line23.Left = 0.01023622F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.3417323F;
            this.line23.Width = 7.068111F;
            this.line23.X1 = 0.01023622F;
            this.line23.X2 = 7.078347F;
            this.line23.Y1 = 0.3417323F;
            this.line23.Y2 = 0.3417323F;
            // 
            // label5
            // 
            this.label5.DataField = "ITEM30";
            this.label5.Height = 0.2334644F;
            this.label5.HyperLink = null;
            this.label5.Left = 4.005906F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 1";
            this.label5.Text = "本所";
            this.label5.Top = 1.499213F;
            this.label5.Visible = false;
            this.label5.Width = 2.852757F;
            // 
            // picture1
            // 
            this.picture1.Height = 0.9578742F;
            this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
            this.picture1.Left = 5.912205F;
            this.picture1.Name = "picture1";
            this.picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.picture1.Top = 1.168504F;
            this.picture1.Visible = false;
            this.picture1.Width = 0.9984255F;
            // 
            // label6
            // 
            this.label6.Height = 0.1980971F;
            this.label6.HyperLink = null;
            this.label6.Left = 4.332F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.label6.Text = "「*」は軽減税率であることを示します。";
            this.label6.Top = 0.05F;
            this.label6.Width = 2.643F;
            // 
            // label7
            // 
            this.label7.DataField = "ITEM31";
            this.label7.Height = 0.8620001F;
            this.label7.HyperLink = null;
            this.label7.Left = 0.43F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 1";
            this.label7.Text = "軽減税率FLD";
            this.label7.Top = 1.47F;
            this.label7.Width = 2.797F;
            // 
            // HNDR1011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.8070866F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.5905512F;
            this.PageSettings.Margins.Top = 0.8070866F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.083611F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ITEM11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt_ITEM07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt_ITEM08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt_ITEM09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt_ITEM10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt_ITEM11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Label label26;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label label28;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Label label30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.Label label31;
        private GrapeCity.ActiveReports.SectionReportModel.Label label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label label33;
        private GrapeCity.ActiveReports.SectionReportModel.Label label34;
        private GrapeCity.ActiveReports.SectionReportModel.Label label35;
        private GrapeCity.ActiveReports.SectionReportModel.Label label36;
        private GrapeCity.ActiveReports.SectionReportModel.Label label37;
        private GrapeCity.ActiveReports.SectionReportModel.Label label38;
        private GrapeCity.ActiveReports.SectionReportModel.Label label39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Label label21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Label label22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Label label23;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox crossSectionBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Picture picture1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label label7;
	}
}
