﻿namespace jp.co.fsi.hn.hncr1031
{
    partial class HNCR1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblGyoshuCdTo = new System.Windows.Forms.Label();
			this.lblCodeBet = new System.Windows.Forms.Label();
			this.txtGyoshuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGyoshuCdFr = new System.Windows.Forms.Label();
			this.txtGyoshuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF2
			// 
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Visible = false;
			// 
			// btnF8
			// 
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(9, 812);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.pnlDebug.Size = new System.Drawing.Size(1155, 132);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "魚種マスタ一覧";
			// 
			// lblGyoshuCdTo
			// 
			this.lblGyoshuCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblGyoshuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblGyoshuCdTo.Location = new System.Drawing.Point(565, 2);
			this.lblGyoshuCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyoshuCdTo.Name = "lblGyoshuCdTo";
			this.lblGyoshuCdTo.Size = new System.Drawing.Size(289, 24);
			this.lblGyoshuCdTo.TabIndex = 4;
			this.lblGyoshuCdTo.Tag = "DISPNAME";
			this.lblGyoshuCdTo.Text = "最　後";
			this.lblGyoshuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCodeBet
			// 
			this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
			this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblCodeBet.Location = new System.Drawing.Point(472, -2);
			this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeBet.Name = "lblCodeBet";
			this.lblCodeBet.Size = new System.Drawing.Size(24, 32);
			this.lblCodeBet.TabIndex = 2;
			this.lblCodeBet.Tag = "CHANGE";
			this.lblCodeBet.Text = "～";
			this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyoshuCdFr
			// 
			this.txtGyoshuCdFr.AutoSizeFromLength = false;
			this.txtGyoshuCdFr.DisplayLength = null;
			this.txtGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtGyoshuCdFr.Location = new System.Drawing.Point(117, 3);
			this.txtGyoshuCdFr.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.txtGyoshuCdFr.MaxLength = 4;
			this.txtGyoshuCdFr.Name = "txtGyoshuCdFr";
			this.txtGyoshuCdFr.Size = new System.Drawing.Size(52, 23);
			this.txtGyoshuCdFr.TabIndex = 0;
			this.txtGyoshuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyoshuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdFr_Validating);
			// 
			// lblGyoshuCdFr
			// 
			this.lblGyoshuCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblGyoshuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblGyoshuCdFr.Location = new System.Drawing.Point(175, 2);
			this.lblGyoshuCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGyoshuCdFr.Name = "lblGyoshuCdFr";
			this.lblGyoshuCdFr.Size = new System.Drawing.Size(289, 24);
			this.lblGyoshuCdFr.TabIndex = 1;
			this.lblGyoshuCdFr.Tag = "DISPNAME";
			this.lblGyoshuCdFr.Text = "先　頭";
			this.lblGyoshuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtGyoshuCdTo
			// 
			this.txtGyoshuCdTo.AutoSizeFromLength = false;
			this.txtGyoshuCdTo.DisplayLength = null;
			this.txtGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtGyoshuCdTo.Location = new System.Drawing.Point(508, 3);
			this.txtGyoshuCdTo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.txtGyoshuCdTo.MaxLength = 4;
			this.txtGyoshuCdTo.Name = "txtGyoshuCdTo";
			this.txtGyoshuCdTo.Size = new System.Drawing.Size(52, 23);
			this.txtGyoshuCdTo.TabIndex = 3;
			this.txtGyoshuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGyoshuCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGyoshuCdTo_KeyDown);
			this.txtGyoshuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdTo_Validating);
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(116, 3);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(176, 2);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(921, 79);
			this.fsiTableLayoutPanel1.TabIndex = 1001;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtGyoshuCdFr);
			this.fsiPanel2.Controls.Add(this.txtGyoshuCdTo);
			this.fsiPanel2.Controls.Add(this.lblGyoshuCdTo);
			this.fsiPanel2.Controls.Add(this.lblGyoshuCdFr);
			this.fsiPanel2.Controls.Add(this.lblCodeBet);
			this.fsiPanel2.Controls.Add(this.label2);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 44);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(911, 30);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(911, 30);
			this.label2.TabIndex = 1003;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "魚種CD範囲";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel1.Controls.Add(this.label1);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(911, 30);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(911, 30);
			this.label1.TabIndex = 1002;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "支所";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// HNCR1031
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
			this.Name = "HNCR1031";
			this.ShowFButton = true;
			this.Text = "ReportSample";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblGyoshuCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtGyoshuCdFr;
        private System.Windows.Forms.Label lblGyoshuCdFr;
        private common.controls.FsiTextBox txtGyoshuCdTo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label1;
    }
}