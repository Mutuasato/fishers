﻿namespace jp.co.fsi.hn.hnse1021
{
    partial class HNSE1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.Label lblKojoGokei;
			System.Windows.Forms.Label lblSashihikigaku;
			this.lblFunanushi = new System.Windows.Forms.Label();
			this.lblFunanushiResults = new System.Windows.Forms.Label();
			this.txtFunanushi = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeriGengoFr = new System.Windows.Forms.Label();
			this.txtSeriMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeriYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeriDayFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeriYearFr = new System.Windows.Forms.Label();
			this.lblSeriMonthFr = new System.Windows.Forms.Label();
			this.lblSeriDayFr = new System.Windows.Forms.Label();
			this.lblSeisanBango = new System.Windows.Forms.Label();
			this.txtSeisanBango = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTsumitatekin = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuagegaku = new System.Windows.Forms.Label();
			this.txtMizuagegaku = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeriDayTo = new System.Windows.Forms.Label();
			this.lblSeriGengoTo = new System.Windows.Forms.Label();
			this.txtSeisanMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSeriYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeriYearTo = new System.Windows.Forms.Label();
			this.txtSeriDayTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeriMonthTo = new System.Windows.Forms.Label();
			this.txtSeriMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanDay = new System.Windows.Forms.Label();
			this.lblSeisanGengo = new System.Windows.Forms.Label();
			this.txtSeisanYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanYear = new System.Windows.Forms.Label();
			this.txtSeisanDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeisanMonth = new System.Windows.Forms.Label();
			this.pnlKomoku = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.panel40 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku35 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku35 = new System.Windows.Forms.Label();
			this.panel39 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku34 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku34 = new System.Windows.Forms.Label();
			this.panel38 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku33 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku33 = new System.Windows.Forms.Label();
			this.panel37 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku32 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku32 = new System.Windows.Forms.Label();
			this.panel36 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku31 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku31 = new System.Windows.Forms.Label();
			this.panel35 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku30 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku30 = new System.Windows.Forms.Label();
			this.panel34 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku29 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku29 = new System.Windows.Forms.Label();
			this.panel33 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku28 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku28 = new System.Windows.Forms.Label();
			this.panel32 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku27 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku27 = new System.Windows.Forms.Label();
			this.panel31 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku26 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku26 = new System.Windows.Forms.Label();
			this.panel30 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku25 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku25 = new System.Windows.Forms.Label();
			this.panel29 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku24 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku24 = new System.Windows.Forms.Label();
			this.panel28 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku23 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku23 = new System.Windows.Forms.Label();
			this.panel27 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku22 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku22 = new System.Windows.Forms.Label();
			this.panel26 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku21 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku21 = new System.Windows.Forms.Label();
			this.panel25 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku20 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku20 = new System.Windows.Forms.Label();
			this.panel24 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku19 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku19 = new System.Windows.Forms.Label();
			this.panel23 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku18 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku18 = new System.Windows.Forms.Label();
			this.panel22 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku17 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku17 = new System.Windows.Forms.Label();
			this.panel21 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku16 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku16 = new System.Windows.Forms.Label();
			this.panel20 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku15 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku15 = new System.Windows.Forms.Label();
			this.panel19 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku14 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku14 = new System.Windows.Forms.Label();
			this.panel18 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku13 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku13 = new System.Windows.Forms.Label();
			this.panel17 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku12 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku12 = new System.Windows.Forms.Label();
			this.panel16 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku11 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku11 = new System.Windows.Forms.Label();
			this.panel15 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku10 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku10 = new System.Windows.Forms.Label();
			this.panel14 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku9 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku9 = new System.Windows.Forms.Label();
			this.panel13 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku8 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku8 = new System.Windows.Forms.Label();
			this.panel12 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku7 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku7 = new System.Windows.Forms.Label();
			this.panel11 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku6 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku6 = new System.Windows.Forms.Label();
			this.panel10 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku5 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku5 = new System.Windows.Forms.Label();
			this.panel9 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku4 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku4 = new System.Windows.Forms.Label();
			this.panel8 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku3 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku3 = new System.Windows.Forms.Label();
			this.panel7 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku2 = new System.Windows.Forms.Label();
			this.panel6 = new jp.co.fsi.common.FsiPanel();
			this.txtKomoku1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKomoku1 = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.panel4 = new jp.co.fsi.common.FsiPanel();
			this.panel3 = new jp.co.fsi.common.FsiPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.panel2 = new jp.co.fsi.common.FsiPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.panel1 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.txtKojoGokei = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSashihikigaku = new jp.co.fsi.common.controls.FsiTextBox();
			lblKojoGokei = new System.Windows.Forms.Label();
			lblSashihikigaku = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.pnlKomoku.SuspendLayout();
			this.panel40.SuspendLayout();
			this.panel39.SuspendLayout();
			this.panel38.SuspendLayout();
			this.panel37.SuspendLayout();
			this.panel36.SuspendLayout();
			this.panel35.SuspendLayout();
			this.panel34.SuspendLayout();
			this.panel33.SuspendLayout();
			this.panel32.SuspendLayout();
			this.panel31.SuspendLayout();
			this.panel30.SuspendLayout();
			this.panel29.SuspendLayout();
			this.panel28.SuspendLayout();
			this.panel27.SuspendLayout();
			this.panel26.SuspendLayout();
			this.panel25.SuspendLayout();
			this.panel24.SuspendLayout();
			this.panel23.SuspendLayout();
			this.panel22.SuspendLayout();
			this.panel21.SuspendLayout();
			this.panel20.SuspendLayout();
			this.panel19.SuspendLayout();
			this.panel18.SuspendLayout();
			this.panel17.SuspendLayout();
			this.panel16.SuspendLayout();
			this.panel15.SuspendLayout();
			this.panel14.SuspendLayout();
			this.panel13.SuspendLayout();
			this.panel12.SuspendLayout();
			this.panel11.SuspendLayout();
			this.panel10.SuspendLayout();
			this.panel9.SuspendLayout();
			this.panel8.SuspendLayout();
			this.panel7.SuspendLayout();
			this.panel6.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(7, 540);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1350, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1340, 41);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// lblKojoGokei
			// 
			lblKojoGokei.AutoEllipsis = true;
			lblKojoGokei.BackColor = System.Drawing.Color.Silver;
			lblKojoGokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			lblKojoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			lblKojoGokei.Location = new System.Drawing.Point(335, 569);
			lblKojoGokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			lblKojoGokei.Name = "lblKojoGokei";
			lblKojoGokei.Size = new System.Drawing.Size(280, 33);
			lblKojoGokei.TabIndex = 70;
			lblKojoGokei.Tag = "CHANGE";
			lblKojoGokei.Text = "控 除 合 計";
			lblKojoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSashihikigaku
			// 
			lblSashihikigaku.AutoEllipsis = true;
			lblSashihikigaku.BackColor = System.Drawing.Color.Silver;
			lblSashihikigaku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			lblSashihikigaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			lblSashihikigaku.Location = new System.Drawing.Point(621, 570);
			lblSashihikigaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			lblSashihikigaku.Name = "lblSashihikigaku";
			lblSashihikigaku.Size = new System.Drawing.Size(295, 33);
			lblSashihikigaku.TabIndex = 72;
			lblSashihikigaku.Tag = "CHANGE";
			lblSashihikigaku.Text = "差 引 精 算 額";
			lblSashihikigaku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushi
			// 
			this.lblFunanushi.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushi.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFunanushi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushi.Location = new System.Drawing.Point(0, 0);
			this.lblFunanushi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushi.Name = "lblFunanushi";
			this.lblFunanushi.Size = new System.Drawing.Size(895, 36);
			this.lblFunanushi.TabIndex = 7;
			this.lblFunanushi.Tag = "CHANGE";
			this.lblFunanushi.Text = "船主CD";
			this.lblFunanushi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiResults
			// 
			this.lblFunanushiResults.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiResults.Location = new System.Drawing.Point(146, 5);
			this.lblFunanushiResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiResults.Name = "lblFunanushiResults";
			this.lblFunanushiResults.Size = new System.Drawing.Size(327, 24);
			this.lblFunanushiResults.TabIndex = 9;
			this.lblFunanushiResults.Tag = "DISPNAME";
			this.lblFunanushiResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushi
			// 
			this.txtFunanushi.AutoSizeFromLength = false;
			this.txtFunanushi.DisplayLength = null;
			this.txtFunanushi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushi.Location = new System.Drawing.Point(76, 5);
			this.txtFunanushi.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushi.MaxLength = 4;
			this.txtFunanushi.Name = "txtFunanushi";
			this.txtFunanushi.Size = new System.Drawing.Size(65, 23);
			this.txtFunanushi.TabIndex = 3;
			this.txtFunanushi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushi.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushi_Validating);
			// 
			// lblSeriGengoFr
			// 
			this.lblSeriGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeriGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeriGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriGengoFr.Location = new System.Drawing.Point(568, 5);
			this.lblSeriGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeriGengoFr.Name = "lblSeriGengoFr";
			this.lblSeriGengoFr.Size = new System.Drawing.Size(55, 24);
			this.lblSeriGengoFr.TabIndex = 14;
			this.lblSeriGengoFr.Tag = "DISPNAME";
			this.lblSeriGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeriMonthFr
			// 
			this.txtSeriMonthFr.AutoSizeFromLength = false;
			this.txtSeriMonthFr.DisplayLength = null;
			this.txtSeriMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriMonthFr.Location = new System.Drawing.Point(698, 5);
			this.txtSeriMonthFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeriMonthFr.MaxLength = 2;
			this.txtSeriMonthFr.Name = "txtSeriMonthFr";
			this.txtSeriMonthFr.Size = new System.Drawing.Size(39, 23);
			this.txtSeriMonthFr.TabIndex = 6;
			this.txtSeriMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeriMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriMonthFr_Validating);
			// 
			// txtSeriYearFr
			// 
			this.txtSeriYearFr.AutoSizeFromLength = false;
			this.txtSeriYearFr.DisplayLength = null;
			this.txtSeriYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriYearFr.Location = new System.Drawing.Point(631, 5);
			this.txtSeriYearFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeriYearFr.MaxLength = 2;
			this.txtSeriYearFr.Name = "txtSeriYearFr";
			this.txtSeriYearFr.Size = new System.Drawing.Size(39, 23);
			this.txtSeriYearFr.TabIndex = 5;
			this.txtSeriYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeriYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriYearFr_Validating);
			// 
			// txtSeriDayFr
			// 
			this.txtSeriDayFr.AutoSizeFromLength = false;
			this.txtSeriDayFr.DisplayLength = null;
			this.txtSeriDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriDayFr.Location = new System.Drawing.Point(765, 5);
			this.txtSeriDayFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeriDayFr.MaxLength = 2;
			this.txtSeriDayFr.Name = "txtSeriDayFr";
			this.txtSeriDayFr.Size = new System.Drawing.Size(39, 23);
			this.txtSeriDayFr.TabIndex = 7;
			this.txtSeriDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeriDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDayFr_Validating);
			// 
			// lblSeriYearFr
			// 
			this.lblSeriYearFr.BackColor = System.Drawing.Color.Silver;
			this.lblSeriYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriYearFr.Location = new System.Drawing.Point(674, 5);
			this.lblSeriYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeriYearFr.Name = "lblSeriYearFr";
			this.lblSeriYearFr.Size = new System.Drawing.Size(23, 28);
			this.lblSeriYearFr.TabIndex = 16;
			this.lblSeriYearFr.Tag = "CHANGE";
			this.lblSeriYearFr.Text = "年";
			this.lblSeriYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeriMonthFr
			// 
			this.lblSeriMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblSeriMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriMonthFr.Location = new System.Drawing.Point(741, 5);
			this.lblSeriMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeriMonthFr.Name = "lblSeriMonthFr";
			this.lblSeriMonthFr.Size = new System.Drawing.Size(20, 25);
			this.lblSeriMonthFr.TabIndex = 18;
			this.lblSeriMonthFr.Tag = "CHANGE";
			this.lblSeriMonthFr.Text = "月";
			this.lblSeriMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeriDayFr
			// 
			this.lblSeriDayFr.BackColor = System.Drawing.Color.Silver;
			this.lblSeriDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriDayFr.Location = new System.Drawing.Point(807, 5);
			this.lblSeriDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeriDayFr.Name = "lblSeriDayFr";
			this.lblSeriDayFr.Size = new System.Drawing.Size(27, 24);
			this.lblSeriDayFr.TabIndex = 20;
			this.lblSeriDayFr.Tag = "CHANGE";
			this.lblSeriDayFr.Text = "日";
			this.lblSeriDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeisanBango
			// 
			this.lblSeisanBango.BackColor = System.Drawing.Color.Silver;
			this.lblSeisanBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanBango.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblSeisanBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanBango.Location = new System.Drawing.Point(0, 0);
			this.lblSeisanBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanBango.Name = "lblSeisanBango";
			this.lblSeisanBango.Size = new System.Drawing.Size(895, 36);
			this.lblSeisanBango.TabIndex = 3;
			this.lblSeisanBango.Tag = "CHANGE";
			this.lblSeisanBango.Text = "精算番号";
			this.lblSeisanBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanBango
			// 
			this.txtSeisanBango.AutoSizeFromLength = false;
			this.txtSeisanBango.DisplayLength = null;
			this.txtSeisanBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanBango.Location = new System.Drawing.Point(76, 5);
			this.txtSeisanBango.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanBango.MaxLength = 6;
			this.txtSeisanBango.Name = "txtSeisanBango";
			this.txtSeisanBango.Size = new System.Drawing.Size(115, 23);
			this.txtSeisanBango.TabIndex = 1;
			this.txtSeisanBango.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtTsumitatekin
			// 
			this.txtTsumitatekin.AutoSizeFromLength = false;
			this.txtTsumitatekin.DisplayLength = null;
			this.txtTsumitatekin.Enabled = false;
			this.txtTsumitatekin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTsumitatekin.Location = new System.Drawing.Point(289, 5);
			this.txtTsumitatekin.Margin = new System.Windows.Forms.Padding(4);
			this.txtTsumitatekin.MaxLength = 9;
			this.txtTsumitatekin.Name = "txtTsumitatekin";
			this.txtTsumitatekin.Size = new System.Drawing.Size(136, 23);
			this.txtTsumitatekin.TabIndex = 2;
			this.txtTsumitatekin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblMizuagegaku
			// 
			this.lblMizuagegaku.BackColor = System.Drawing.Color.Silver;
			this.lblMizuagegaku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuagegaku.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuagegaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuagegaku.Location = new System.Drawing.Point(0, 0);
			this.lblMizuagegaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuagegaku.Name = "lblMizuagegaku";
			this.lblMizuagegaku.Size = new System.Drawing.Size(895, 38);
			this.lblMizuagegaku.TabIndex = 10;
			this.lblMizuagegaku.Tag = "CHANGE";
			this.lblMizuagegaku.Text = "税込水揚金額";
			this.lblMizuagegaku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtMizuagegaku
			// 
			this.txtMizuagegaku.AutoSizeFromLength = false;
			this.txtMizuagegaku.DisplayLength = null;
			this.txtMizuagegaku.Enabled = false;
			this.txtMizuagegaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuagegaku.Location = new System.Drawing.Point(108, 4);
			this.txtMizuagegaku.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuagegaku.MaxLength = 9;
			this.txtMizuagegaku.Name = "txtMizuagegaku";
			this.txtMizuagegaku.Size = new System.Drawing.Size(136, 23);
			this.txtMizuagegaku.TabIndex = 4;
			this.txtMizuagegaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblSeriDayTo
			// 
			this.lblSeriDayTo.BackColor = System.Drawing.Color.Silver;
			this.lblSeriDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriDayTo.Location = new System.Drawing.Point(807, 3);
			this.lblSeriDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeriDayTo.Name = "lblSeriDayTo";
			this.lblSeriDayTo.Size = new System.Drawing.Size(27, 24);
			this.lblSeriDayTo.TabIndex = 28;
			this.lblSeriDayTo.Tag = "CHANGE";
			this.lblSeriDayTo.Text = "日";
			this.lblSeriDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeriGengoTo
			// 
			this.lblSeriGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeriGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeriGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriGengoTo.Location = new System.Drawing.Point(568, 4);
			this.lblSeriGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeriGengoTo.Name = "lblSeriGengoTo";
			this.lblSeriGengoTo.Size = new System.Drawing.Size(55, 24);
			this.lblSeriGengoTo.TabIndex = 22;
			this.lblSeriGengoTo.Tag = "DISPNAME";
			this.lblSeriGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanMonth
			// 
			this.txtSeisanMonth.AutoSizeFromLength = false;
			this.txtSeisanMonth.DisplayLength = null;
			this.txtSeisanMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanMonth.Location = new System.Drawing.Point(698, 5);
			this.txtSeisanMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanMonth.MaxLength = 2;
			this.txtSeisanMonth.Name = "txtSeisanMonth";
			this.txtSeisanMonth.Size = new System.Drawing.Size(39, 23);
			this.txtSeisanMonth.TabIndex = 12;
			this.txtSeisanMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanMonth_Validating);
			// 
			// txtSeriYearTo
			// 
			this.txtSeriYearTo.AutoSizeFromLength = false;
			this.txtSeriYearTo.DisplayLength = null;
			this.txtSeriYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriYearTo.Location = new System.Drawing.Point(631, 4);
			this.txtSeriYearTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeriYearTo.MaxLength = 2;
			this.txtSeriYearTo.Name = "txtSeriYearTo";
			this.txtSeriYearTo.Size = new System.Drawing.Size(39, 23);
			this.txtSeriYearTo.TabIndex = 8;
			this.txtSeriYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeriYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriYearTo_Validating);
			// 
			// lblSeriYearTo
			// 
			this.lblSeriYearTo.BackColor = System.Drawing.Color.Silver;
			this.lblSeriYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriYearTo.Location = new System.Drawing.Point(674, 3);
			this.lblSeriYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeriYearTo.Name = "lblSeriYearTo";
			this.lblSeriYearTo.Size = new System.Drawing.Size(23, 28);
			this.lblSeriYearTo.TabIndex = 24;
			this.lblSeriYearTo.Tag = "CHANGE";
			this.lblSeriYearTo.Text = "年";
			this.lblSeriYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeriDayTo
			// 
			this.txtSeriDayTo.AutoSizeFromLength = false;
			this.txtSeriDayTo.DisplayLength = null;
			this.txtSeriDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriDayTo.Location = new System.Drawing.Point(765, 4);
			this.txtSeriDayTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeriDayTo.MaxLength = 2;
			this.txtSeriDayTo.Name = "txtSeriDayTo";
			this.txtSeriDayTo.Size = new System.Drawing.Size(39, 23);
			this.txtSeriDayTo.TabIndex = 10;
			this.txtSeriDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeriDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDayTo_Validating);
			// 
			// lblSeriMonthTo
			// 
			this.lblSeriMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblSeriMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeriMonthTo.Location = new System.Drawing.Point(741, 3);
			this.lblSeriMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeriMonthTo.Name = "lblSeriMonthTo";
			this.lblSeriMonthTo.Size = new System.Drawing.Size(20, 25);
			this.lblSeriMonthTo.TabIndex = 26;
			this.lblSeriMonthTo.Tag = "CHANGE";
			this.lblSeriMonthTo.Text = "月";
			this.lblSeriMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeriMonthTo
			// 
			this.txtSeriMonthTo.AutoSizeFromLength = false;
			this.txtSeriMonthTo.DisplayLength = null;
			this.txtSeriMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeriMonthTo.Location = new System.Drawing.Point(698, 4);
			this.txtSeriMonthTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeriMonthTo.MaxLength = 2;
			this.txtSeriMonthTo.Name = "txtSeriMonthTo";
			this.txtSeriMonthTo.Size = new System.Drawing.Size(39, 23);
			this.txtSeriMonthTo.TabIndex = 9;
			this.txtSeriMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeriMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriMonthTo_Validating);
			// 
			// lblSeisanDay
			// 
			this.lblSeisanDay.BackColor = System.Drawing.Color.Silver;
			this.lblSeisanDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanDay.Location = new System.Drawing.Point(807, 4);
			this.lblSeisanDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanDay.Name = "lblSeisanDay";
			this.lblSeisanDay.Size = new System.Drawing.Size(27, 24);
			this.lblSeisanDay.TabIndex = 37;
			this.lblSeisanDay.Tag = "CHANGE";
			this.lblSeisanDay.Text = "日";
			this.lblSeisanDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeisanGengo
			// 
			this.lblSeisanGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeisanGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeisanGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanGengo.Location = new System.Drawing.Point(568, 5);
			this.lblSeisanGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanGengo.Name = "lblSeisanGengo";
			this.lblSeisanGengo.Size = new System.Drawing.Size(55, 24);
			this.lblSeisanGengo.TabIndex = 31;
			this.lblSeisanGengo.Tag = "DISPNAME";
			this.lblSeisanGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanYear
			// 
			this.txtSeisanYear.AutoSizeFromLength = false;
			this.txtSeisanYear.DisplayLength = null;
			this.txtSeisanYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanYear.Location = new System.Drawing.Point(631, 5);
			this.txtSeisanYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanYear.MaxLength = 2;
			this.txtSeisanYear.Name = "txtSeisanYear";
			this.txtSeisanYear.Size = new System.Drawing.Size(39, 23);
			this.txtSeisanYear.TabIndex = 11;
			this.txtSeisanYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanYear_Validating);
			// 
			// lblSeisanYear
			// 
			this.lblSeisanYear.BackColor = System.Drawing.Color.Silver;
			this.lblSeisanYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanYear.Location = new System.Drawing.Point(674, 4);
			this.lblSeisanYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanYear.Name = "lblSeisanYear";
			this.lblSeisanYear.Size = new System.Drawing.Size(23, 28);
			this.lblSeisanYear.TabIndex = 33;
			this.lblSeisanYear.Tag = "CHANGE";
			this.lblSeisanYear.Text = "年";
			this.lblSeisanYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeisanDay
			// 
			this.txtSeisanDay.AutoSizeFromLength = false;
			this.txtSeisanDay.DisplayLength = null;
			this.txtSeisanDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeisanDay.Location = new System.Drawing.Point(765, 4);
			this.txtSeisanDay.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeisanDay.MaxLength = 2;
			this.txtSeisanDay.Name = "txtSeisanDay";
			this.txtSeisanDay.Size = new System.Drawing.Size(39, 23);
			this.txtSeisanDay.TabIndex = 13;
			this.txtSeisanDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSeisanDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanDay_Validating);
			// 
			// lblSeisanMonth
			// 
			this.lblSeisanMonth.BackColor = System.Drawing.Color.Silver;
			this.lblSeisanMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeisanMonth.Location = new System.Drawing.Point(741, 4);
			this.lblSeisanMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeisanMonth.Name = "lblSeisanMonth";
			this.lblSeisanMonth.Size = new System.Drawing.Size(20, 25);
			this.lblSeisanMonth.TabIndex = 35;
			this.lblSeisanMonth.Tag = "CHANGE";
			this.lblSeisanMonth.Text = "月";
			this.lblSeisanMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pnlKomoku
			// 
			this.pnlKomoku.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.pnlKomoku.ColumnCount = 7;
			this.pnlKomoku.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.pnlKomoku.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.pnlKomoku.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.pnlKomoku.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.pnlKomoku.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.pnlKomoku.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.pnlKomoku.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.pnlKomoku.Controls.Add(this.panel40, 6, 4);
			this.pnlKomoku.Controls.Add(this.panel39, 5, 4);
			this.pnlKomoku.Controls.Add(this.panel38, 4, 4);
			this.pnlKomoku.Controls.Add(this.panel37, 3, 4);
			this.pnlKomoku.Controls.Add(this.panel36, 2, 4);
			this.pnlKomoku.Controls.Add(this.panel35, 1, 4);
			this.pnlKomoku.Controls.Add(this.panel34, 0, 4);
			this.pnlKomoku.Controls.Add(this.panel33, 6, 3);
			this.pnlKomoku.Controls.Add(this.panel32, 5, 3);
			this.pnlKomoku.Controls.Add(this.panel31, 4, 3);
			this.pnlKomoku.Controls.Add(this.panel30, 3, 3);
			this.pnlKomoku.Controls.Add(this.panel29, 2, 3);
			this.pnlKomoku.Controls.Add(this.panel28, 1, 3);
			this.pnlKomoku.Controls.Add(this.panel27, 0, 3);
			this.pnlKomoku.Controls.Add(this.panel26, 6, 2);
			this.pnlKomoku.Controls.Add(this.panel25, 5, 2);
			this.pnlKomoku.Controls.Add(this.panel24, 4, 2);
			this.pnlKomoku.Controls.Add(this.panel23, 3, 2);
			this.pnlKomoku.Controls.Add(this.panel22, 2, 2);
			this.pnlKomoku.Controls.Add(this.panel21, 1, 2);
			this.pnlKomoku.Controls.Add(this.panel20, 0, 2);
			this.pnlKomoku.Controls.Add(this.panel19, 6, 1);
			this.pnlKomoku.Controls.Add(this.panel18, 5, 1);
			this.pnlKomoku.Controls.Add(this.panel17, 4, 1);
			this.pnlKomoku.Controls.Add(this.panel16, 3, 1);
			this.pnlKomoku.Controls.Add(this.panel15, 2, 1);
			this.pnlKomoku.Controls.Add(this.panel14, 1, 1);
			this.pnlKomoku.Controls.Add(this.panel13, 0, 1);
			this.pnlKomoku.Controls.Add(this.panel12, 6, 0);
			this.pnlKomoku.Controls.Add(this.panel11, 5, 0);
			this.pnlKomoku.Controls.Add(this.panel10, 4, 0);
			this.pnlKomoku.Controls.Add(this.panel9, 3, 0);
			this.pnlKomoku.Controls.Add(this.panel8, 2, 0);
			this.pnlKomoku.Controls.Add(this.panel7, 1, 0);
			this.pnlKomoku.Controls.Add(this.panel6, 0, 0);
			this.pnlKomoku.Location = new System.Drawing.Point(15, 239);
			this.pnlKomoku.Name = "pnlKomoku";
			this.pnlKomoku.RowCount = 5;
			this.pnlKomoku.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.pnlKomoku.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.pnlKomoku.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.pnlKomoku.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.pnlKomoku.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.pnlKomoku.Size = new System.Drawing.Size(900, 327);
			this.pnlKomoku.TabIndex = 1002;
			// 
			// panel40
			// 
			this.panel40.Controls.Add(this.txtKomoku35);
			this.panel40.Controls.Add(this.lblKomoku35);
			this.panel40.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel40.Location = new System.Drawing.Point(772, 264);
			this.panel40.Name = "panel40";
			this.panel40.Size = new System.Drawing.Size(124, 59);
			this.panel40.TabIndex = 34;
			// 
			// txtKomoku35
			// 
			this.txtKomoku35.AutoSizeFromLength = false;
			this.txtKomoku35.DisplayLength = null;
			this.txtKomoku35.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku35.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku35.Location = new System.Drawing.Point(0, 36);
			this.txtKomoku35.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku35.MaxLength = 9;
			this.txtKomoku35.Name = "txtKomoku35";
			this.txtKomoku35.Size = new System.Drawing.Size(124, 23);
			this.txtKomoku35.TabIndex = 69;
			this.txtKomoku35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku35.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku35
			// 
			this.lblKomoku35.AutoEllipsis = true;
			this.lblKomoku35.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku35.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku35.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku35.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku35.Name = "lblKomoku35";
			this.lblKomoku35.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku35.Size = new System.Drawing.Size(124, 59);
			this.lblKomoku35.TabIndex = 68;
			this.lblKomoku35.Tag = "CHANGE";
			this.lblKomoku35.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel39
			// 
			this.panel39.Controls.Add(this.txtKomoku34);
			this.panel39.Controls.Add(this.lblKomoku34);
			this.panel39.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel39.Location = new System.Drawing.Point(644, 264);
			this.panel39.Name = "panel39";
			this.panel39.Size = new System.Drawing.Size(121, 59);
			this.panel39.TabIndex = 33;
			// 
			// txtKomoku34
			// 
			this.txtKomoku34.AutoSizeFromLength = false;
			this.txtKomoku34.DisplayLength = null;
			this.txtKomoku34.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku34.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku34.Location = new System.Drawing.Point(0, 36);
			this.txtKomoku34.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku34.MaxLength = 9;
			this.txtKomoku34.Name = "txtKomoku34";
			this.txtKomoku34.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku34.TabIndex = 67;
			this.txtKomoku34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku34.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku34
			// 
			this.lblKomoku34.AutoEllipsis = true;
			this.lblKomoku34.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku34.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku34.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku34.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku34.Name = "lblKomoku34";
			this.lblKomoku34.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku34.Size = new System.Drawing.Size(121, 59);
			this.lblKomoku34.TabIndex = 66;
			this.lblKomoku34.Tag = "CHANGE";
			this.lblKomoku34.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel38
			// 
			this.panel38.Controls.Add(this.txtKomoku33);
			this.panel38.Controls.Add(this.lblKomoku33);
			this.panel38.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel38.Location = new System.Drawing.Point(516, 264);
			this.panel38.Name = "panel38";
			this.panel38.Size = new System.Drawing.Size(121, 59);
			this.panel38.TabIndex = 32;
			// 
			// txtKomoku33
			// 
			this.txtKomoku33.AutoSizeFromLength = false;
			this.txtKomoku33.DisplayLength = null;
			this.txtKomoku33.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku33.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku33.Location = new System.Drawing.Point(0, 36);
			this.txtKomoku33.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku33.MaxLength = 9;
			this.txtKomoku33.Name = "txtKomoku33";
			this.txtKomoku33.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku33.TabIndex = 65;
			this.txtKomoku33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku33.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku33
			// 
			this.lblKomoku33.AutoEllipsis = true;
			this.lblKomoku33.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku33.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku33.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku33.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku33.Name = "lblKomoku33";
			this.lblKomoku33.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku33.Size = new System.Drawing.Size(121, 59);
			this.lblKomoku33.TabIndex = 64;
			this.lblKomoku33.Tag = "CHANGE";
			this.lblKomoku33.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel37
			// 
			this.panel37.Controls.Add(this.txtKomoku32);
			this.panel37.Controls.Add(this.lblKomoku32);
			this.panel37.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel37.Location = new System.Drawing.Point(388, 264);
			this.panel37.Name = "panel37";
			this.panel37.Size = new System.Drawing.Size(121, 59);
			this.panel37.TabIndex = 31;
			// 
			// txtKomoku32
			// 
			this.txtKomoku32.AutoSizeFromLength = false;
			this.txtKomoku32.DisplayLength = null;
			this.txtKomoku32.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku32.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku32.Location = new System.Drawing.Point(0, 36);
			this.txtKomoku32.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku32.MaxLength = 9;
			this.txtKomoku32.Name = "txtKomoku32";
			this.txtKomoku32.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku32.TabIndex = 63;
			this.txtKomoku32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku32.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku32
			// 
			this.lblKomoku32.AutoEllipsis = true;
			this.lblKomoku32.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku32.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku32.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku32.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku32.Name = "lblKomoku32";
			this.lblKomoku32.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku32.Size = new System.Drawing.Size(121, 59);
			this.lblKomoku32.TabIndex = 62;
			this.lblKomoku32.Tag = "CHANGE";
			this.lblKomoku32.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel36
			// 
			this.panel36.Controls.Add(this.txtKomoku31);
			this.panel36.Controls.Add(this.lblKomoku31);
			this.panel36.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel36.Location = new System.Drawing.Point(260, 264);
			this.panel36.Name = "panel36";
			this.panel36.Size = new System.Drawing.Size(121, 59);
			this.panel36.TabIndex = 30;
			// 
			// txtKomoku31
			// 
			this.txtKomoku31.AutoSizeFromLength = false;
			this.txtKomoku31.DisplayLength = null;
			this.txtKomoku31.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku31.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku31.Location = new System.Drawing.Point(0, 36);
			this.txtKomoku31.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku31.MaxLength = 9;
			this.txtKomoku31.Name = "txtKomoku31";
			this.txtKomoku31.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku31.TabIndex = 61;
			this.txtKomoku31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku31.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku31
			// 
			this.lblKomoku31.AutoEllipsis = true;
			this.lblKomoku31.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku31.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku31.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku31.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku31.Name = "lblKomoku31";
			this.lblKomoku31.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku31.Size = new System.Drawing.Size(121, 59);
			this.lblKomoku31.TabIndex = 60;
			this.lblKomoku31.Tag = "CHANGE";
			this.lblKomoku31.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel35
			// 
			this.panel35.Controls.Add(this.txtKomoku30);
			this.panel35.Controls.Add(this.lblKomoku30);
			this.panel35.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel35.Location = new System.Drawing.Point(132, 264);
			this.panel35.Name = "panel35";
			this.panel35.Size = new System.Drawing.Size(121, 59);
			this.panel35.TabIndex = 29;
			// 
			// txtKomoku30
			// 
			this.txtKomoku30.AutoSizeFromLength = false;
			this.txtKomoku30.DisplayLength = null;
			this.txtKomoku30.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku30.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku30.Location = new System.Drawing.Point(0, 36);
			this.txtKomoku30.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku30.MaxLength = 9;
			this.txtKomoku30.Name = "txtKomoku30";
			this.txtKomoku30.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku30.TabIndex = 59;
			this.txtKomoku30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku30.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku30
			// 
			this.lblKomoku30.AutoEllipsis = true;
			this.lblKomoku30.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku30.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku30.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku30.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku30.Name = "lblKomoku30";
			this.lblKomoku30.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku30.Size = new System.Drawing.Size(121, 59);
			this.lblKomoku30.TabIndex = 58;
			this.lblKomoku30.Tag = "CHANGE";
			this.lblKomoku30.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel34
			// 
			this.panel34.Controls.Add(this.txtKomoku29);
			this.panel34.Controls.Add(this.lblKomoku29);
			this.panel34.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel34.Location = new System.Drawing.Point(4, 264);
			this.panel34.Name = "panel34";
			this.panel34.Size = new System.Drawing.Size(121, 59);
			this.panel34.TabIndex = 28;
			// 
			// txtKomoku29
			// 
			this.txtKomoku29.AutoSizeFromLength = false;
			this.txtKomoku29.DisplayLength = null;
			this.txtKomoku29.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku29.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku29.Location = new System.Drawing.Point(0, 36);
			this.txtKomoku29.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku29.MaxLength = 9;
			this.txtKomoku29.Name = "txtKomoku29";
			this.txtKomoku29.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku29.TabIndex = 57;
			this.txtKomoku29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku29.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku29
			// 
			this.lblKomoku29.AutoEllipsis = true;
			this.lblKomoku29.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku29.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku29.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku29.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku29.Name = "lblKomoku29";
			this.lblKomoku29.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku29.Size = new System.Drawing.Size(121, 59);
			this.lblKomoku29.TabIndex = 56;
			this.lblKomoku29.Tag = "CHANGE";
			this.lblKomoku29.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel33
			// 
			this.panel33.Controls.Add(this.txtKomoku28);
			this.panel33.Controls.Add(this.lblKomoku28);
			this.panel33.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel33.Location = new System.Drawing.Point(772, 199);
			this.panel33.Name = "panel33";
			this.panel33.Size = new System.Drawing.Size(124, 58);
			this.panel33.TabIndex = 27;
			// 
			// txtKomoku28
			// 
			this.txtKomoku28.AutoSizeFromLength = false;
			this.txtKomoku28.DisplayLength = null;
			this.txtKomoku28.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku28.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku28.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku28.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku28.MaxLength = 9;
			this.txtKomoku28.Name = "txtKomoku28";
			this.txtKomoku28.Size = new System.Drawing.Size(124, 23);
			this.txtKomoku28.TabIndex = 55;
			this.txtKomoku28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku28.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku28
			// 
			this.lblKomoku28.AutoEllipsis = true;
			this.lblKomoku28.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku28.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku28.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku28.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku28.Name = "lblKomoku28";
			this.lblKomoku28.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku28.Size = new System.Drawing.Size(124, 58);
			this.lblKomoku28.TabIndex = 54;
			this.lblKomoku28.Tag = "CHANGE";
			this.lblKomoku28.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel32
			// 
			this.panel32.Controls.Add(this.txtKomoku27);
			this.panel32.Controls.Add(this.lblKomoku27);
			this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel32.Location = new System.Drawing.Point(644, 199);
			this.panel32.Name = "panel32";
			this.panel32.Size = new System.Drawing.Size(121, 58);
			this.panel32.TabIndex = 26;
			// 
			// txtKomoku27
			// 
			this.txtKomoku27.AutoSizeFromLength = false;
			this.txtKomoku27.DisplayLength = null;
			this.txtKomoku27.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku27.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku27.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku27.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku27.MaxLength = 9;
			this.txtKomoku27.Name = "txtKomoku27";
			this.txtKomoku27.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku27.TabIndex = 53;
			this.txtKomoku27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku27.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku27
			// 
			this.lblKomoku27.AutoEllipsis = true;
			this.lblKomoku27.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku27.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku27.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku27.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku27.Name = "lblKomoku27";
			this.lblKomoku27.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku27.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku27.TabIndex = 52;
			this.lblKomoku27.Tag = "CHANGE";
			this.lblKomoku27.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel31
			// 
			this.panel31.Controls.Add(this.txtKomoku26);
			this.panel31.Controls.Add(this.lblKomoku26);
			this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel31.Location = new System.Drawing.Point(516, 199);
			this.panel31.Name = "panel31";
			this.panel31.Size = new System.Drawing.Size(121, 58);
			this.panel31.TabIndex = 25;
			// 
			// txtKomoku26
			// 
			this.txtKomoku26.AutoSizeFromLength = false;
			this.txtKomoku26.DisplayLength = null;
			this.txtKomoku26.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku26.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku26.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku26.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku26.MaxLength = 9;
			this.txtKomoku26.Name = "txtKomoku26";
			this.txtKomoku26.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku26.TabIndex = 51;
			this.txtKomoku26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku26.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku26
			// 
			this.lblKomoku26.AutoEllipsis = true;
			this.lblKomoku26.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku26.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku26.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku26.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku26.Name = "lblKomoku26";
			this.lblKomoku26.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku26.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku26.TabIndex = 50;
			this.lblKomoku26.Tag = "CHANGE";
			this.lblKomoku26.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel30
			// 
			this.panel30.Controls.Add(this.txtKomoku25);
			this.panel30.Controls.Add(this.lblKomoku25);
			this.panel30.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel30.Location = new System.Drawing.Point(388, 199);
			this.panel30.Name = "panel30";
			this.panel30.Size = new System.Drawing.Size(121, 58);
			this.panel30.TabIndex = 24;
			// 
			// txtKomoku25
			// 
			this.txtKomoku25.AutoSizeFromLength = false;
			this.txtKomoku25.DisplayLength = null;
			this.txtKomoku25.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku25.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku25.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku25.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku25.MaxLength = 9;
			this.txtKomoku25.Name = "txtKomoku25";
			this.txtKomoku25.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku25.TabIndex = 49;
			this.txtKomoku25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku25
			// 
			this.lblKomoku25.AutoEllipsis = true;
			this.lblKomoku25.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku25.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku25.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku25.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku25.Name = "lblKomoku25";
			this.lblKomoku25.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku25.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku25.TabIndex = 48;
			this.lblKomoku25.Tag = "CHANGE";
			this.lblKomoku25.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel29
			// 
			this.panel29.Controls.Add(this.txtKomoku24);
			this.panel29.Controls.Add(this.lblKomoku24);
			this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel29.Location = new System.Drawing.Point(260, 199);
			this.panel29.Name = "panel29";
			this.panel29.Size = new System.Drawing.Size(121, 58);
			this.panel29.TabIndex = 23;
			// 
			// txtKomoku24
			// 
			this.txtKomoku24.AutoSizeFromLength = false;
			this.txtKomoku24.DisplayLength = null;
			this.txtKomoku24.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku24.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku24.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku24.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku24.MaxLength = 9;
			this.txtKomoku24.Name = "txtKomoku24";
			this.txtKomoku24.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku24.TabIndex = 47;
			this.txtKomoku24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku24
			// 
			this.lblKomoku24.AutoEllipsis = true;
			this.lblKomoku24.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku24.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku24.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku24.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku24.Name = "lblKomoku24";
			this.lblKomoku24.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku24.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku24.TabIndex = 46;
			this.lblKomoku24.Tag = "CHANGE";
			this.lblKomoku24.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel28
			// 
			this.panel28.Controls.Add(this.txtKomoku23);
			this.panel28.Controls.Add(this.lblKomoku23);
			this.panel28.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel28.Location = new System.Drawing.Point(132, 199);
			this.panel28.Name = "panel28";
			this.panel28.Size = new System.Drawing.Size(121, 58);
			this.panel28.TabIndex = 22;
			// 
			// txtKomoku23
			// 
			this.txtKomoku23.AutoSizeFromLength = false;
			this.txtKomoku23.DisplayLength = null;
			this.txtKomoku23.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku23.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku23.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku23.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku23.MaxLength = 9;
			this.txtKomoku23.Name = "txtKomoku23";
			this.txtKomoku23.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku23.TabIndex = 45;
			this.txtKomoku23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku23
			// 
			this.lblKomoku23.AutoEllipsis = true;
			this.lblKomoku23.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku23.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku23.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku23.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku23.Name = "lblKomoku23";
			this.lblKomoku23.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku23.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku23.TabIndex = 44;
			this.lblKomoku23.Tag = "CHANGE";
			this.lblKomoku23.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel27
			// 
			this.panel27.Controls.Add(this.txtKomoku22);
			this.panel27.Controls.Add(this.lblKomoku22);
			this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel27.Location = new System.Drawing.Point(4, 199);
			this.panel27.Name = "panel27";
			this.panel27.Size = new System.Drawing.Size(121, 58);
			this.panel27.TabIndex = 21;
			// 
			// txtKomoku22
			// 
			this.txtKomoku22.AutoSizeFromLength = false;
			this.txtKomoku22.DisplayLength = null;
			this.txtKomoku22.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku22.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku22.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku22.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku22.MaxLength = 9;
			this.txtKomoku22.Name = "txtKomoku22";
			this.txtKomoku22.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku22.TabIndex = 43;
			this.txtKomoku22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku22
			// 
			this.lblKomoku22.AutoEllipsis = true;
			this.lblKomoku22.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku22.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku22.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku22.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku22.Name = "lblKomoku22";
			this.lblKomoku22.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku22.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku22.TabIndex = 42;
			this.lblKomoku22.Tag = "CHANGE";
			this.lblKomoku22.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel26
			// 
			this.panel26.Controls.Add(this.txtKomoku21);
			this.panel26.Controls.Add(this.lblKomoku21);
			this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel26.Location = new System.Drawing.Point(772, 134);
			this.panel26.Name = "panel26";
			this.panel26.Size = new System.Drawing.Size(124, 58);
			this.panel26.TabIndex = 20;
			// 
			// txtKomoku21
			// 
			this.txtKomoku21.AutoSizeFromLength = false;
			this.txtKomoku21.DisplayLength = null;
			this.txtKomoku21.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku21.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku21.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku21.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku21.MaxLength = 9;
			this.txtKomoku21.Name = "txtKomoku21";
			this.txtKomoku21.Size = new System.Drawing.Size(124, 23);
			this.txtKomoku21.TabIndex = 41;
			this.txtKomoku21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku21
			// 
			this.lblKomoku21.AutoEllipsis = true;
			this.lblKomoku21.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku21.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku21.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku21.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku21.Name = "lblKomoku21";
			this.lblKomoku21.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku21.Size = new System.Drawing.Size(124, 58);
			this.lblKomoku21.TabIndex = 40;
			this.lblKomoku21.Tag = "CHANGE";
			this.lblKomoku21.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel25
			// 
			this.panel25.Controls.Add(this.txtKomoku20);
			this.panel25.Controls.Add(this.lblKomoku20);
			this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel25.Location = new System.Drawing.Point(644, 134);
			this.panel25.Name = "panel25";
			this.panel25.Size = new System.Drawing.Size(121, 58);
			this.panel25.TabIndex = 19;
			// 
			// txtKomoku20
			// 
			this.txtKomoku20.AutoSizeFromLength = false;
			this.txtKomoku20.DisplayLength = null;
			this.txtKomoku20.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku20.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku20.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku20.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku20.MaxLength = 9;
			this.txtKomoku20.Name = "txtKomoku20";
			this.txtKomoku20.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku20.TabIndex = 39;
			this.txtKomoku20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku20
			// 
			this.lblKomoku20.AutoEllipsis = true;
			this.lblKomoku20.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku20.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku20.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku20.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku20.Name = "lblKomoku20";
			this.lblKomoku20.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku20.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku20.TabIndex = 38;
			this.lblKomoku20.Tag = "CHANGE";
			this.lblKomoku20.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel24
			// 
			this.panel24.Controls.Add(this.txtKomoku19);
			this.panel24.Controls.Add(this.lblKomoku19);
			this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel24.Location = new System.Drawing.Point(516, 134);
			this.panel24.Name = "panel24";
			this.panel24.Size = new System.Drawing.Size(121, 58);
			this.panel24.TabIndex = 18;
			// 
			// txtKomoku19
			// 
			this.txtKomoku19.AutoSizeFromLength = false;
			this.txtKomoku19.DisplayLength = null;
			this.txtKomoku19.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku19.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku19.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku19.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku19.MaxLength = 9;
			this.txtKomoku19.Name = "txtKomoku19";
			this.txtKomoku19.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku19.TabIndex = 37;
			this.txtKomoku19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku19
			// 
			this.lblKomoku19.AutoEllipsis = true;
			this.lblKomoku19.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku19.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku19.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku19.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku19.Name = "lblKomoku19";
			this.lblKomoku19.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku19.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku19.TabIndex = 36;
			this.lblKomoku19.Tag = "CHANGE";
			this.lblKomoku19.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel23
			// 
			this.panel23.Controls.Add(this.txtKomoku18);
			this.panel23.Controls.Add(this.lblKomoku18);
			this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel23.Location = new System.Drawing.Point(388, 134);
			this.panel23.Name = "panel23";
			this.panel23.Size = new System.Drawing.Size(121, 58);
			this.panel23.TabIndex = 17;
			// 
			// txtKomoku18
			// 
			this.txtKomoku18.AutoSizeFromLength = false;
			this.txtKomoku18.DisplayLength = null;
			this.txtKomoku18.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku18.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku18.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku18.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku18.MaxLength = 9;
			this.txtKomoku18.Name = "txtKomoku18";
			this.txtKomoku18.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku18.TabIndex = 35;
			this.txtKomoku18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku18
			// 
			this.lblKomoku18.AutoEllipsis = true;
			this.lblKomoku18.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku18.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku18.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku18.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku18.Name = "lblKomoku18";
			this.lblKomoku18.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku18.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku18.TabIndex = 34;
			this.lblKomoku18.Tag = "CHANGE";
			this.lblKomoku18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel22
			// 
			this.panel22.Controls.Add(this.txtKomoku17);
			this.panel22.Controls.Add(this.lblKomoku17);
			this.panel22.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel22.Location = new System.Drawing.Point(260, 134);
			this.panel22.Name = "panel22";
			this.panel22.Size = new System.Drawing.Size(121, 58);
			this.panel22.TabIndex = 16;
			// 
			// txtKomoku17
			// 
			this.txtKomoku17.AutoSizeFromLength = false;
			this.txtKomoku17.DisplayLength = null;
			this.txtKomoku17.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku17.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku17.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku17.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku17.MaxLength = 9;
			this.txtKomoku17.Name = "txtKomoku17";
			this.txtKomoku17.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku17.TabIndex = 33;
			this.txtKomoku17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku17
			// 
			this.lblKomoku17.AutoEllipsis = true;
			this.lblKomoku17.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku17.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku17.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku17.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku17.Name = "lblKomoku17";
			this.lblKomoku17.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku17.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku17.TabIndex = 32;
			this.lblKomoku17.Tag = "CHANGE";
			this.lblKomoku17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel21
			// 
			this.panel21.Controls.Add(this.txtKomoku16);
			this.panel21.Controls.Add(this.lblKomoku16);
			this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel21.Location = new System.Drawing.Point(132, 134);
			this.panel21.Name = "panel21";
			this.panel21.Size = new System.Drawing.Size(121, 58);
			this.panel21.TabIndex = 15;
			// 
			// txtKomoku16
			// 
			this.txtKomoku16.AutoSizeFromLength = false;
			this.txtKomoku16.DisplayLength = null;
			this.txtKomoku16.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku16.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku16.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku16.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku16.MaxLength = 9;
			this.txtKomoku16.Name = "txtKomoku16";
			this.txtKomoku16.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku16.TabIndex = 31;
			this.txtKomoku16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku16
			// 
			this.lblKomoku16.AutoEllipsis = true;
			this.lblKomoku16.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku16.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku16.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku16.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku16.Name = "lblKomoku16";
			this.lblKomoku16.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku16.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku16.TabIndex = 30;
			this.lblKomoku16.Tag = "CHANGE";
			this.lblKomoku16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel20
			// 
			this.panel20.Controls.Add(this.txtKomoku15);
			this.panel20.Controls.Add(this.lblKomoku15);
			this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel20.Location = new System.Drawing.Point(4, 134);
			this.panel20.Name = "panel20";
			this.panel20.Size = new System.Drawing.Size(121, 58);
			this.panel20.TabIndex = 14;
			// 
			// txtKomoku15
			// 
			this.txtKomoku15.AutoSizeFromLength = false;
			this.txtKomoku15.DisplayLength = null;
			this.txtKomoku15.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku15.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku15.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku15.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku15.MaxLength = 9;
			this.txtKomoku15.Name = "txtKomoku15";
			this.txtKomoku15.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku15.TabIndex = 29;
			this.txtKomoku15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku15
			// 
			this.lblKomoku15.AutoEllipsis = true;
			this.lblKomoku15.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku15.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku15.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku15.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku15.Name = "lblKomoku15";
			this.lblKomoku15.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku15.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku15.TabIndex = 28;
			this.lblKomoku15.Tag = "CHANGE";
			this.lblKomoku15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel19
			// 
			this.panel19.Controls.Add(this.txtKomoku14);
			this.panel19.Controls.Add(this.lblKomoku14);
			this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel19.Location = new System.Drawing.Point(772, 69);
			this.panel19.Name = "panel19";
			this.panel19.Size = new System.Drawing.Size(124, 58);
			this.panel19.TabIndex = 13;
			// 
			// txtKomoku14
			// 
			this.txtKomoku14.AutoSizeFromLength = false;
			this.txtKomoku14.DisplayLength = null;
			this.txtKomoku14.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku14.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku14.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku14.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku14.MaxLength = 9;
			this.txtKomoku14.Name = "txtKomoku14";
			this.txtKomoku14.Size = new System.Drawing.Size(124, 23);
			this.txtKomoku14.TabIndex = 27;
			this.txtKomoku14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku14
			// 
			this.lblKomoku14.AutoEllipsis = true;
			this.lblKomoku14.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku14.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku14.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku14.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku14.Name = "lblKomoku14";
			this.lblKomoku14.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku14.Size = new System.Drawing.Size(124, 58);
			this.lblKomoku14.TabIndex = 26;
			this.lblKomoku14.Tag = "CHANGE";
			this.lblKomoku14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel18
			// 
			this.panel18.Controls.Add(this.txtKomoku13);
			this.panel18.Controls.Add(this.lblKomoku13);
			this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel18.Location = new System.Drawing.Point(644, 69);
			this.panel18.Name = "panel18";
			this.panel18.Size = new System.Drawing.Size(121, 58);
			this.panel18.TabIndex = 12;
			// 
			// txtKomoku13
			// 
			this.txtKomoku13.AutoSizeFromLength = false;
			this.txtKomoku13.DisplayLength = null;
			this.txtKomoku13.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku13.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku13.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku13.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku13.MaxLength = 9;
			this.txtKomoku13.Name = "txtKomoku13";
			this.txtKomoku13.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku13.TabIndex = 25;
			this.txtKomoku13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku13
			// 
			this.lblKomoku13.AutoEllipsis = true;
			this.lblKomoku13.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku13.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku13.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku13.Name = "lblKomoku13";
			this.lblKomoku13.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku13.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku13.TabIndex = 24;
			this.lblKomoku13.Tag = "CHANGE";
			this.lblKomoku13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel17
			// 
			this.panel17.Controls.Add(this.txtKomoku12);
			this.panel17.Controls.Add(this.lblKomoku12);
			this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel17.Location = new System.Drawing.Point(516, 69);
			this.panel17.Name = "panel17";
			this.panel17.Size = new System.Drawing.Size(121, 58);
			this.panel17.TabIndex = 11;
			// 
			// txtKomoku12
			// 
			this.txtKomoku12.AutoSizeFromLength = false;
			this.txtKomoku12.DisplayLength = null;
			this.txtKomoku12.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku12.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku12.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku12.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku12.MaxLength = 9;
			this.txtKomoku12.Name = "txtKomoku12";
			this.txtKomoku12.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku12.TabIndex = 23;
			this.txtKomoku12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku12
			// 
			this.lblKomoku12.AutoEllipsis = true;
			this.lblKomoku12.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku12.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku12.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku12.Name = "lblKomoku12";
			this.lblKomoku12.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku12.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku12.TabIndex = 22;
			this.lblKomoku12.Tag = "CHANGE";
			this.lblKomoku12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel16
			// 
			this.panel16.Controls.Add(this.txtKomoku11);
			this.panel16.Controls.Add(this.lblKomoku11);
			this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel16.Location = new System.Drawing.Point(388, 69);
			this.panel16.Name = "panel16";
			this.panel16.Size = new System.Drawing.Size(121, 58);
			this.panel16.TabIndex = 10;
			// 
			// txtKomoku11
			// 
			this.txtKomoku11.AutoSizeFromLength = false;
			this.txtKomoku11.DisplayLength = null;
			this.txtKomoku11.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku11.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku11.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku11.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku11.MaxLength = 9;
			this.txtKomoku11.Name = "txtKomoku11";
			this.txtKomoku11.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku11.TabIndex = 21;
			this.txtKomoku11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku11
			// 
			this.lblKomoku11.AutoEllipsis = true;
			this.lblKomoku11.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku11.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku11.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku11.Name = "lblKomoku11";
			this.lblKomoku11.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku11.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku11.TabIndex = 20;
			this.lblKomoku11.Tag = "CHANGE";
			this.lblKomoku11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel15
			// 
			this.panel15.Controls.Add(this.txtKomoku10);
			this.panel15.Controls.Add(this.lblKomoku10);
			this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel15.Location = new System.Drawing.Point(260, 69);
			this.panel15.Name = "panel15";
			this.panel15.Size = new System.Drawing.Size(121, 58);
			this.panel15.TabIndex = 9;
			// 
			// txtKomoku10
			// 
			this.txtKomoku10.AutoSizeFromLength = false;
			this.txtKomoku10.DisplayLength = null;
			this.txtKomoku10.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku10.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku10.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku10.MaxLength = 9;
			this.txtKomoku10.Name = "txtKomoku10";
			this.txtKomoku10.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku10.TabIndex = 19;
			this.txtKomoku10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku10
			// 
			this.lblKomoku10.AutoEllipsis = true;
			this.lblKomoku10.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku10.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku10.Name = "lblKomoku10";
			this.lblKomoku10.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku10.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku10.TabIndex = 18;
			this.lblKomoku10.Tag = "CHANGE";
			this.lblKomoku10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel14
			// 
			this.panel14.Controls.Add(this.txtKomoku9);
			this.panel14.Controls.Add(this.lblKomoku9);
			this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel14.Location = new System.Drawing.Point(132, 69);
			this.panel14.Name = "panel14";
			this.panel14.Size = new System.Drawing.Size(121, 58);
			this.panel14.TabIndex = 8;
			// 
			// txtKomoku9
			// 
			this.txtKomoku9.AutoSizeFromLength = false;
			this.txtKomoku9.DisplayLength = null;
			this.txtKomoku9.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku9.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku9.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku9.MaxLength = 9;
			this.txtKomoku9.Name = "txtKomoku9";
			this.txtKomoku9.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku9.TabIndex = 17;
			this.txtKomoku9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku9
			// 
			this.lblKomoku9.AutoEllipsis = true;
			this.lblKomoku9.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku9.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku9.Name = "lblKomoku9";
			this.lblKomoku9.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku9.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku9.TabIndex = 16;
			this.lblKomoku9.Tag = "CHANGE";
			this.lblKomoku9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel13
			// 
			this.panel13.Controls.Add(this.txtKomoku8);
			this.panel13.Controls.Add(this.lblKomoku8);
			this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel13.Location = new System.Drawing.Point(4, 69);
			this.panel13.Name = "panel13";
			this.panel13.Size = new System.Drawing.Size(121, 58);
			this.panel13.TabIndex = 7;
			// 
			// txtKomoku8
			// 
			this.txtKomoku8.AutoSizeFromLength = false;
			this.txtKomoku8.DisplayLength = null;
			this.txtKomoku8.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku8.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku8.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku8.MaxLength = 9;
			this.txtKomoku8.Name = "txtKomoku8";
			this.txtKomoku8.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku8.TabIndex = 15;
			this.txtKomoku8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku8
			// 
			this.lblKomoku8.AutoEllipsis = true;
			this.lblKomoku8.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku8.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku8.Name = "lblKomoku8";
			this.lblKomoku8.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku8.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku8.TabIndex = 14;
			this.lblKomoku8.Tag = "CHANGE";
			this.lblKomoku8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel12
			// 
			this.panel12.Controls.Add(this.txtKomoku7);
			this.panel12.Controls.Add(this.lblKomoku7);
			this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel12.Location = new System.Drawing.Point(772, 4);
			this.panel12.Name = "panel12";
			this.panel12.Size = new System.Drawing.Size(124, 58);
			this.panel12.TabIndex = 6;
			// 
			// txtKomoku7
			// 
			this.txtKomoku7.AutoSizeFromLength = false;
			this.txtKomoku7.DisplayLength = null;
			this.txtKomoku7.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku7.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku7.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku7.MaxLength = 9;
			this.txtKomoku7.Name = "txtKomoku7";
			this.txtKomoku7.Size = new System.Drawing.Size(124, 23);
			this.txtKomoku7.TabIndex = 13;
			this.txtKomoku7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku7
			// 
			this.lblKomoku7.AutoEllipsis = true;
			this.lblKomoku7.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku7.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku7.Name = "lblKomoku7";
			this.lblKomoku7.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku7.Size = new System.Drawing.Size(124, 58);
			this.lblKomoku7.TabIndex = 12;
			this.lblKomoku7.Tag = "CHANGE";
			this.lblKomoku7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel11
			// 
			this.panel11.Controls.Add(this.txtKomoku6);
			this.panel11.Controls.Add(this.lblKomoku6);
			this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel11.Location = new System.Drawing.Point(644, 4);
			this.panel11.Name = "panel11";
			this.panel11.Size = new System.Drawing.Size(121, 58);
			this.panel11.TabIndex = 5;
			// 
			// txtKomoku6
			// 
			this.txtKomoku6.AutoSizeFromLength = false;
			this.txtKomoku6.DisplayLength = null;
			this.txtKomoku6.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku6.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku6.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku6.MaxLength = 9;
			this.txtKomoku6.Name = "txtKomoku6";
			this.txtKomoku6.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku6.TabIndex = 11;
			this.txtKomoku6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku6
			// 
			this.lblKomoku6.AutoEllipsis = true;
			this.lblKomoku6.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku6.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku6.Name = "lblKomoku6";
			this.lblKomoku6.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku6.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku6.TabIndex = 10;
			this.lblKomoku6.Tag = "CHANGE";
			this.lblKomoku6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel10
			// 
			this.panel10.Controls.Add(this.txtKomoku5);
			this.panel10.Controls.Add(this.lblKomoku5);
			this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel10.Location = new System.Drawing.Point(516, 4);
			this.panel10.Name = "panel10";
			this.panel10.Size = new System.Drawing.Size(121, 58);
			this.panel10.TabIndex = 4;
			// 
			// txtKomoku5
			// 
			this.txtKomoku5.AutoSizeFromLength = false;
			this.txtKomoku5.DisplayLength = null;
			this.txtKomoku5.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku5.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku5.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku5.MaxLength = 9;
			this.txtKomoku5.Name = "txtKomoku5";
			this.txtKomoku5.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku5.TabIndex = 9;
			this.txtKomoku5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku5
			// 
			this.lblKomoku5.AutoEllipsis = true;
			this.lblKomoku5.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku5.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku5.Name = "lblKomoku5";
			this.lblKomoku5.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku5.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku5.TabIndex = 8;
			this.lblKomoku5.Tag = "CHANGE";
			this.lblKomoku5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel9
			// 
			this.panel9.Controls.Add(this.txtKomoku4);
			this.panel9.Controls.Add(this.lblKomoku4);
			this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel9.Location = new System.Drawing.Point(388, 4);
			this.panel9.Name = "panel9";
			this.panel9.Size = new System.Drawing.Size(121, 58);
			this.panel9.TabIndex = 3;
			// 
			// txtKomoku4
			// 
			this.txtKomoku4.AutoSizeFromLength = false;
			this.txtKomoku4.DisplayLength = null;
			this.txtKomoku4.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku4.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku4.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku4.MaxLength = 9;
			this.txtKomoku4.Name = "txtKomoku4";
			this.txtKomoku4.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku4.TabIndex = 7;
			this.txtKomoku4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku4
			// 
			this.lblKomoku4.AutoEllipsis = true;
			this.lblKomoku4.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku4.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku4.Name = "lblKomoku4";
			this.lblKomoku4.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku4.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku4.TabIndex = 6;
			this.lblKomoku4.Tag = "CHANGE";
			this.lblKomoku4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel8
			// 
			this.panel8.Controls.Add(this.txtKomoku3);
			this.panel8.Controls.Add(this.lblKomoku3);
			this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel8.Location = new System.Drawing.Point(260, 4);
			this.panel8.Name = "panel8";
			this.panel8.Size = new System.Drawing.Size(121, 58);
			this.panel8.TabIndex = 2;
			// 
			// txtKomoku3
			// 
			this.txtKomoku3.AutoSizeFromLength = false;
			this.txtKomoku3.DisplayLength = null;
			this.txtKomoku3.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku3.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku3.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku3.MaxLength = 9;
			this.txtKomoku3.Name = "txtKomoku3";
			this.txtKomoku3.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku3.TabIndex = 5;
			this.txtKomoku3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku3
			// 
			this.lblKomoku3.AutoEllipsis = true;
			this.lblKomoku3.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku3.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku3.Name = "lblKomoku3";
			this.lblKomoku3.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku3.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku3.TabIndex = 4;
			this.lblKomoku3.Tag = "CHANGE";
			this.lblKomoku3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel7
			// 
			this.panel7.Controls.Add(this.txtKomoku2);
			this.panel7.Controls.Add(this.lblKomoku2);
			this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel7.Location = new System.Drawing.Point(132, 4);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(121, 58);
			this.panel7.TabIndex = 1;
			// 
			// txtKomoku2
			// 
			this.txtKomoku2.AutoSizeFromLength = false;
			this.txtKomoku2.DisplayLength = null;
			this.txtKomoku2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku2.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku2.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku2.MaxLength = 9;
			this.txtKomoku2.Name = "txtKomoku2";
			this.txtKomoku2.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku2.TabIndex = 3;
			this.txtKomoku2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku2
			// 
			this.lblKomoku2.AutoEllipsis = true;
			this.lblKomoku2.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku2.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku2.Name = "lblKomoku2";
			this.lblKomoku2.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku2.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku2.TabIndex = 2;
			this.lblKomoku2.Tag = "CHANGE";
			this.lblKomoku2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.txtKomoku1);
			this.panel6.Controls.Add(this.lblKomoku1);
			this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel6.Location = new System.Drawing.Point(4, 4);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(121, 58);
			this.panel6.TabIndex = 0;
			// 
			// txtKomoku1
			// 
			this.txtKomoku1.AutoSizeFromLength = false;
			this.txtKomoku1.DisplayLength = null;
			this.txtKomoku1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtKomoku1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKomoku1.Location = new System.Drawing.Point(0, 35);
			this.txtKomoku1.Margin = new System.Windows.Forms.Padding(4);
			this.txtKomoku1.MaxLength = 9;
			this.txtKomoku1.Name = "txtKomoku1";
			this.txtKomoku1.Size = new System.Drawing.Size(121, 23);
			this.txtKomoku1.TabIndex = 1;
			this.txtKomoku1.Text = "123,456,789";
			this.txtKomoku1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKomoku1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
			// 
			// lblKomoku1
			// 
			this.lblKomoku1.AutoEllipsis = true;
			this.lblKomoku1.BackColor = System.Drawing.Color.LightCyan;
			this.lblKomoku1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKomoku1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKomoku1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKomoku1.Location = new System.Drawing.Point(0, 0);
			this.lblKomoku1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKomoku1.Name = "lblKomoku1";
			this.lblKomoku1.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
			this.lblKomoku1.Size = new System.Drawing.Size(121, 58);
			this.lblKomoku1.TabIndex = 0;
			this.lblKomoku1.Tag = "CHANGE";
			this.lblKomoku1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(76, 6);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 4;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
			this.txtMizuageShishoCd.TabIndex = 0;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(125, 5);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(313, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(895, 36);
			this.lblMizuageShisho.TabIndex = 0;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "水揚支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(11, 58);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 4;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(903, 175);
			this.tableLayoutPanel1.TabIndex = 1000;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.txtMizuagegaku);
			this.panel4.Controls.Add(this.lblMizuagegaku);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(4, 133);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(895, 38);
			this.panel4.TabIndex = 3;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.label2);
			this.panel3.Controls.Add(this.txtFunanushi);
			this.panel3.Controls.Add(this.lblFunanushiResults);
			this.panel3.Controls.Add(this.txtSeisanMonth);
			this.panel3.Controls.Add(this.lblSeisanGengo);
			this.panel3.Controls.Add(this.lblSeisanMonth);
			this.panel3.Controls.Add(this.lblSeisanDay);
			this.panel3.Controls.Add(this.txtSeisanDay);
			this.panel3.Controls.Add(this.txtSeisanYear);
			this.panel3.Controls.Add(this.lblSeisanYear);
			this.panel3.Controls.Add(this.lblFunanushi);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(4, 90);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(895, 36);
			this.panel3.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(476, 5);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(86, 25);
			this.label2.TabIndex = 1002;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "精 算 日";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.txtSeisanBango);
			this.panel2.Controls.Add(this.lblSeriGengoTo);
			this.panel2.Controls.Add(this.lblSeriYearTo);
			this.panel2.Controls.Add(this.txtSeriYearTo);
			this.panel2.Controls.Add(this.txtTsumitatekin);
			this.panel2.Controls.Add(this.txtSeriDayTo);
			this.panel2.Controls.Add(this.lblSeriDayTo);
			this.panel2.Controls.Add(this.lblSeriMonthTo);
			this.panel2.Controls.Add(this.txtSeriMonthTo);
			this.panel2.Controls.Add(this.lblSeisanBango);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(4, 47);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(895, 36);
			this.panel2.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(201, 5);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(86, 25);
			this.label3.TabIndex = 1002;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "積 立 金";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.txtMizuageShishoCd);
			this.panel1.Controls.Add(this.lblMizuageShishoNm);
			this.panel1.Controls.Add(this.lblSeriGengoFr);
			this.panel1.Controls.Add(this.lblSeriDayFr);
			this.panel1.Controls.Add(this.txtSeriYearFr);
			this.panel1.Controls.Add(this.lblSeriYearFr);
			this.panel1.Controls.Add(this.txtSeriDayFr);
			this.panel1.Controls.Add(this.lblSeriMonthFr);
			this.panel1.Controls.Add(this.txtSeriMonthFr);
			this.panel1.Controls.Add(this.lblMizuageShisho);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(4, 4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(895, 36);
			this.panel1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(476, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(86, 25);
			this.label1.TabIndex = 1001;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "セリ期間";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtKojoGokei
			// 
			this.txtKojoGokei.AutoSizeFromLength = false;
			this.txtKojoGokei.DisplayLength = null;
			this.txtKojoGokei.Enabled = false;
			this.txtKojoGokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKojoGokei.Location = new System.Drawing.Point(440, 572);
			this.txtKojoGokei.Margin = new System.Windows.Forms.Padding(4);
			this.txtKojoGokei.MaxLength = 9;
			this.txtKojoGokei.Name = "txtKojoGokei";
			this.txtKojoGokei.Size = new System.Drawing.Size(165, 23);
			this.txtKojoGokei.TabIndex = 71;
			this.txtKojoGokei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtSashihikigaku
			// 
			this.txtSashihikigaku.AutoSizeFromLength = false;
			this.txtSashihikigaku.DisplayLength = null;
			this.txtSashihikigaku.Enabled = false;
			this.txtSashihikigaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSashihikigaku.Location = new System.Drawing.Point(746, 573);
			this.txtSashihikigaku.Margin = new System.Windows.Forms.Padding(4);
			this.txtSashihikigaku.MaxLength = 9;
			this.txtSashihikigaku.Name = "txtSashihikigaku";
			this.txtSashihikigaku.Size = new System.Drawing.Size(165, 23);
			this.txtSashihikigaku.TabIndex = 73;
			this.txtSashihikigaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// HNSE1021
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1340, 678);
			this.Controls.Add(this.txtSashihikigaku);
			this.Controls.Add(this.txtKojoGokei);
			this.Controls.Add(lblSashihikigaku);
			this.Controls.Add(lblKojoGokei);
			this.Controls.Add(this.pnlKomoku);
			this.Controls.Add(this.tableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNSE1021";
			this.Text = "";
			this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.pnlKomoku, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(lblKojoGokei, 0);
			this.Controls.SetChildIndex(lblSashihikigaku, 0);
			this.Controls.SetChildIndex(this.txtKojoGokei, 0);
			this.Controls.SetChildIndex(this.txtSashihikigaku, 0);
			this.pnlDebug.ResumeLayout(false);
			this.pnlKomoku.ResumeLayout(false);
			this.panel40.ResumeLayout(false);
			this.panel40.PerformLayout();
			this.panel39.ResumeLayout(false);
			this.panel39.PerformLayout();
			this.panel38.ResumeLayout(false);
			this.panel38.PerformLayout();
			this.panel37.ResumeLayout(false);
			this.panel37.PerformLayout();
			this.panel36.ResumeLayout(false);
			this.panel36.PerformLayout();
			this.panel35.ResumeLayout(false);
			this.panel35.PerformLayout();
			this.panel34.ResumeLayout(false);
			this.panel34.PerformLayout();
			this.panel33.ResumeLayout(false);
			this.panel33.PerformLayout();
			this.panel32.ResumeLayout(false);
			this.panel32.PerformLayout();
			this.panel31.ResumeLayout(false);
			this.panel31.PerformLayout();
			this.panel30.ResumeLayout(false);
			this.panel30.PerformLayout();
			this.panel29.ResumeLayout(false);
			this.panel29.PerformLayout();
			this.panel28.ResumeLayout(false);
			this.panel28.PerformLayout();
			this.panel27.ResumeLayout(false);
			this.panel27.PerformLayout();
			this.panel26.ResumeLayout(false);
			this.panel26.PerformLayout();
			this.panel25.ResumeLayout(false);
			this.panel25.PerformLayout();
			this.panel24.ResumeLayout(false);
			this.panel24.PerformLayout();
			this.panel23.ResumeLayout(false);
			this.panel23.PerformLayout();
			this.panel22.ResumeLayout(false);
			this.panel22.PerformLayout();
			this.panel21.ResumeLayout(false);
			this.panel21.PerformLayout();
			this.panel20.ResumeLayout(false);
			this.panel20.PerformLayout();
			this.panel19.ResumeLayout(false);
			this.panel19.PerformLayout();
			this.panel18.ResumeLayout(false);
			this.panel18.PerformLayout();
			this.panel17.ResumeLayout(false);
			this.panel17.PerformLayout();
			this.panel16.ResumeLayout(false);
			this.panel16.PerformLayout();
			this.panel15.ResumeLayout(false);
			this.panel15.PerformLayout();
			this.panel14.ResumeLayout(false);
			this.panel14.PerformLayout();
			this.panel13.ResumeLayout(false);
			this.panel13.PerformLayout();
			this.panel12.ResumeLayout(false);
			this.panel12.PerformLayout();
			this.panel11.ResumeLayout(false);
			this.panel11.PerformLayout();
			this.panel10.ResumeLayout(false);
			this.panel10.PerformLayout();
			this.panel9.ResumeLayout(false);
			this.panel9.PerformLayout();
			this.panel8.ResumeLayout(false);
			this.panel8.PerformLayout();
			this.panel7.ResumeLayout(false);
			this.panel7.PerformLayout();
			this.panel6.ResumeLayout(false);
			this.panel6.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFunanushiResults;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushi;
        private System.Windows.Forms.Label lblFunanushi;
        private System.Windows.Forms.Label lblSeriGengoFr;
        private common.controls.FsiTextBox txtSeriMonthFr;
        private common.controls.FsiTextBox txtSeriYearFr;
        private common.controls.FsiTextBox txtSeriDayFr;
        private System.Windows.Forms.Label lblSeriYearFr;
        private System.Windows.Forms.Label lblSeriMonthFr;
        private System.Windows.Forms.Label lblSeriDayFr;
        private System.Windows.Forms.Label lblSeisanBango;
        private common.controls.FsiTextBox txtSeisanBango;
        private common.controls.FsiTextBox txtTsumitatekin;
        private System.Windows.Forms.Label lblMizuagegaku;
        private common.controls.FsiTextBox txtMizuagegaku;
        private System.Windows.Forms.Label lblSeriDayTo;
        private System.Windows.Forms.Label lblSeriGengoTo;
        private common.controls.FsiTextBox txtSeisanMonth;
        private common.controls.FsiTextBox txtSeriYearTo;
        private System.Windows.Forms.Label lblSeriYearTo;
        private common.controls.FsiTextBox txtSeriDayTo;
        private System.Windows.Forms.Label lblSeriMonthTo;
        private common.controls.FsiTextBox txtSeriMonthTo;
        private System.Windows.Forms.Label lblSeisanDay;
        private System.Windows.Forms.Label lblSeisanGengo;
        private common.controls.FsiTextBox txtSeisanYear;
        private System.Windows.Forms.Label lblSeisanYear;
        private common.controls.FsiTextBox txtSeisanDay;
        private System.Windows.Forms.Label lblSeisanMonth;
        private common.controls.FsiTextBox txtKomoku1;
        private System.Windows.Forms.Label lblKomoku29;
        private System.Windows.Forms.Label lblKomoku30;
        private System.Windows.Forms.Label lblKomoku31;
        private System.Windows.Forms.Label lblKomoku32;
        private System.Windows.Forms.Label lblKomoku33;
        private common.controls.FsiTextBox txtKomoku35;
        private System.Windows.Forms.Label lblKomoku34;
        private common.controls.FsiTextBox txtKomoku34;
        private System.Windows.Forms.Label lblKomoku35;
        private common.controls.FsiTextBox txtKomoku33;
        private System.Windows.Forms.Label lblKomoku22;
        private common.controls.FsiTextBox txtKomoku32;
        private System.Windows.Forms.Label lblKomoku23;
        private common.controls.FsiTextBox txtKomoku31;
        private System.Windows.Forms.Label lblKomoku24;
        private common.controls.FsiTextBox txtKomoku30;
        private System.Windows.Forms.Label lblKomoku25;
        private common.controls.FsiTextBox txtKomoku29;
        private System.Windows.Forms.Label lblKomoku26;
        private common.controls.FsiTextBox txtKomoku28;
        private System.Windows.Forms.Label lblKomoku27;
        private common.controls.FsiTextBox txtKomoku27;
        private System.Windows.Forms.Label lblKomoku28;
        private common.controls.FsiTextBox txtKomoku26;
        private System.Windows.Forms.Label lblKomoku15;
        private common.controls.FsiTextBox txtKomoku25;
        private System.Windows.Forms.Label lblKomoku16;
        private common.controls.FsiTextBox txtKomoku24;
        private System.Windows.Forms.Label lblKomoku17;
        private common.controls.FsiTextBox txtKomoku23;
        private System.Windows.Forms.Label lblKomoku18;
        private common.controls.FsiTextBox txtKomoku22;
        private System.Windows.Forms.Label lblKomoku19;
        private common.controls.FsiTextBox txtKomoku21;
        private System.Windows.Forms.Label lblKomoku20;
        private common.controls.FsiTextBox txtKomoku20;
        private System.Windows.Forms.Label lblKomoku21;
        private common.controls.FsiTextBox txtKomoku19;
        private System.Windows.Forms.Label lblKomoku8;
        private common.controls.FsiTextBox txtKomoku18;
        private System.Windows.Forms.Label lblKomoku9;
        private common.controls.FsiTextBox txtKomoku17;
        private System.Windows.Forms.Label lblKomoku10;
        private common.controls.FsiTextBox txtKomoku16;
        private System.Windows.Forms.Label lblKomoku11;
        private common.controls.FsiTextBox txtKomoku15;
        private System.Windows.Forms.Label lblKomoku12;
        private common.controls.FsiTextBox txtKomoku14;
        private System.Windows.Forms.Label lblKomoku13;
        private common.controls.FsiTextBox txtKomoku13;
        private System.Windows.Forms.Label lblKomoku14;
        private common.controls.FsiTextBox txtKomoku12;
        private System.Windows.Forms.Label lblKomoku1;
        private common.controls.FsiTextBox txtKomoku11;
        private System.Windows.Forms.Label lblKomoku2;
        private common.controls.FsiTextBox txtKomoku10;
        private System.Windows.Forms.Label lblKomoku3;
        private common.controls.FsiTextBox txtKomoku9;
        private System.Windows.Forms.Label lblKomoku4;
        private common.controls.FsiTextBox txtKomoku8;
        private System.Windows.Forms.Label lblKomoku5;
        private common.controls.FsiTextBox txtKomoku7;
        private System.Windows.Forms.Label lblKomoku6;
        private common.controls.FsiTextBox txtKomoku6;
        private System.Windows.Forms.Label lblKomoku7;
        private common.controls.FsiTextBox txtKomoku5;
        private common.controls.FsiTextBox txtKomoku2;
        private common.controls.FsiTextBox txtKomoku4;
        private common.controls.FsiTextBox txtKomoku3;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel1;
        private jp.co.fsi.common.FsiPanel panel4;
        private jp.co.fsi.common.FsiPanel panel3;
        private jp.co.fsi.common.FsiPanel panel2;
        private jp.co.fsi.common.FsiPanel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private jp.co.fsi.common.FsiTableLayoutPanel pnlKomoku;
        private jp.co.fsi.common.FsiPanel panel40;
        private jp.co.fsi.common.FsiPanel panel39;
        private jp.co.fsi.common.FsiPanel panel38;
        private jp.co.fsi.common.FsiPanel panel37;
        private jp.co.fsi.common.FsiPanel panel36;
        private jp.co.fsi.common.FsiPanel panel35;
        private jp.co.fsi.common.FsiPanel panel34;
        private jp.co.fsi.common.FsiPanel panel33;
        private jp.co.fsi.common.FsiPanel panel32;
        private jp.co.fsi.common.FsiPanel panel31;
        private jp.co.fsi.common.FsiPanel panel30;
        private jp.co.fsi.common.FsiPanel panel29;
        private jp.co.fsi.common.FsiPanel panel28;
        private jp.co.fsi.common.FsiPanel panel27;
        private jp.co.fsi.common.FsiPanel panel26;
        private jp.co.fsi.common.FsiPanel panel25;
        private jp.co.fsi.common.FsiPanel panel24;
        private jp.co.fsi.common.FsiPanel panel23;
        private jp.co.fsi.common.FsiPanel panel22;
        private jp.co.fsi.common.FsiPanel panel21;
        private jp.co.fsi.common.FsiPanel panel20;
        private jp.co.fsi.common.FsiPanel panel19;
        private jp.co.fsi.common.FsiPanel panel18;
        private jp.co.fsi.common.FsiPanel panel17;
        private jp.co.fsi.common.FsiPanel panel16;
        private jp.co.fsi.common.FsiPanel panel15;
        private jp.co.fsi.common.FsiPanel panel14;
        private jp.co.fsi.common.FsiPanel panel13;
        private jp.co.fsi.common.FsiPanel panel12;
        private jp.co.fsi.common.FsiPanel panel11;
        private jp.co.fsi.common.FsiPanel panel10;
        private jp.co.fsi.common.FsiPanel panel9;
        private jp.co.fsi.common.FsiPanel panel8;
        private jp.co.fsi.common.FsiPanel panel7;
        private jp.co.fsi.common.FsiPanel panel6;
        private common.controls.FsiTextBox txtKojoGokei;
        private common.controls.FsiTextBox txtSashihikigaku;
    }
}