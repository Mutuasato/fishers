﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

using Microsoft.VisualBasic;

namespace jp.co.fsi.hn.hnse1021
{
    /// <summary>
    /// 控除入力(HNSE1021)
    /// </summary>
    public partial class HNSE1021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 最大控除項目数
        /// </summary>
        private const int KOMOKUSU = 35;
        #endregion

        #region 変数
        string _shishoCd;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSE1021()
        {
            
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {

            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = = this.UInfo.ShishoCd;
#endif
            //ログイン担当者が本所の登録でないなら変更不可
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            // 項目の初期表示
            FirstDisp();

            // 初期フォーカス
            txtSeisanBango.Focus();

        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,精算番号,船主CD,日付にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtSeisanBango":
                case "txtFunanushi":
                case "txtSeriYearFr":
                case "txtSeriYearTo":
                case "txtSeisanYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
		/// 
		/// なんでまとめることを知らんのだ！！！！！！
		/// 無駄ロジックいっぱい、、、、、、、
		/// わかりずらくなってる、、、、、、、
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
							frm.StartPosition = FormStartPosition.CenterScreen;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];

                                //該当支所の控除項目を表示する
                                SearchKomokuNm();
                            }
                        }
                    }
                    break;
                    #endregion

                #region セリ期間
                case "txtSeriYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeriGengoFr.Text;
							frm.StartPosition = FormStartPosition.CenterScreen;
							frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSeriGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblSeriGengoFr.Text, this.txtSeriYearFr.Text,
                                    this.txtSeriMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtSeriDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtSeriDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblSeriGengoFr.Text,
                                        this.txtSeriYearFr.Text,
                                        this.txtSeriMonthFr.Text,
                                        this.txtSeriDayFr.Text,
                                        this.Dba);
                                this.lblSeriGengoFr.Text = arrJpDate[0];
                                this.txtSeriYearFr.Text = arrJpDate[2];
                                this.txtSeriMonthFr.Text = arrJpDate[3];
                                this.txtSeriDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtSeriYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeriGengoTo.Text;
							frm.StartPosition = FormStartPosition.CenterScreen;
							frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSeriGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblSeriGengoTo.Text, this.txtSeriYearTo.Text,
                                    this.txtSeriMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtSeriDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtSeriDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblSeriGengoTo.Text,
                                        this.txtSeriYearTo.Text,
                                        this.txtSeriMonthTo.Text,
                                        this.txtSeriDayTo.Text,
                                        this.Dba);
                                this.lblSeriGengoTo.Text = arrJpDate[0];
                                this.txtSeriYearTo.Text = arrJpDate[2];
                                this.txtSeriMonthTo.Text = arrJpDate[3];
                                this.txtSeriDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
                #endregion

                #region 精算日
                case "txtSeisanYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeisanGengo.Text;
							frm.StartPosition = FormStartPosition.CenterScreen;
							frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSeisanGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblSeisanGengo.Text, this.txtSeisanYear.Text,
                                    this.txtSeisanMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtSeisanDay.Text) > lastDayInMonth)
                                {
                                    this.txtSeisanDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblSeisanGengo.Text,
                                        this.txtSeisanYear.Text,
                                        this.txtSeisanMonth.Text,
                                        this.txtSeisanDay.Text,
                                        this.Dba);
                                this.lblSeisanGengo.Text = arrJpDate[0];
                                this.txtSeisanYear.Text = arrJpDate[2];
                                this.txtSeisanMonth.Text = arrJpDate[3];
                                this.txtSeisanDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
#endregion

                case "txtSeisanBango":
                    // 精算番号検索画面の起動
                    EditSeisan();
                    break;
                case "txtFunanushi":

                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.StartPosition = FormStartPosition.CenterScreen;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushi.Text = outData[0];
                                this.lblFunanushiResults.Text = outData[1];
                            }
                        }
                    }


                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("削除", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfOKCancel("削除しても宜しいですか？") == DialogResult.OK)
            {
                // 削除処理
                DeleteData();
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("登録", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfOKCancel("登録しても宜しいですか？") == DialogResult.OK)
            {
                // 登録処理
                RegistData();

                txtSeisanBango.Focus();

            }
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            string shishoCd = this.txtMizuageShishoCd.Text;
            // 控除科目の登録画面を起動
            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1071.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.hn.hncm1071.HNCM1071");
            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = "1";
                    frm.Par2 = shishoCd;
                    frm.ShowDialog(this);
                }
            }
        }
#endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            // 水揚支所名称を表示する
            string shishoCd = this.txtMizuageShishoCd.Text;
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, this.txtMizuageShishoCd.Text);

            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
            }

            //該当支所の控除項目を表示する
            SearchKomokuNm();
        }

        /// <summary>
        /// 船主コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushi_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            string shishoCd = this.txtMizuageShishoCd.Text;
            this.lblFunanushiResults.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", shishoCd, this.txtFunanushi.Text);
        }

        /// <summary>
        /// 年(セリ期間)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtSeriYearFr.Text, this.txtSeriYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtSeriYearFr.SelectAll();
            }
            else
            {
                this.txtSeriYearFr.Text = Util.ToString(IsValid.SetYear(this.txtSeriYearFr.Text));
                CheckJpSeriFr();
                SetJpSeriFr();
            }
        }

        /// <summary>
        /// 月(セリ期間)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtSeriMonthFr.Text, this.txtSeriMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtSeriMonthFr.SelectAll();
            }
            else
            {
                this.txtSeriMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtSeriMonthFr.Text));
                CheckJpSeriFr();
                SetJpSeriFr();
            }
        }

        /// <summary>
        /// 日(セリ期間)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtSeriDayFr.Text, this.txtSeriDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtSeriDayFr.SelectAll();
            }
            else
            {
                this.txtSeriDayFr.Text = Util.ToString(IsValid.SetDay(this.txtSeriDayFr.Text));
                CheckJpSeriFr();
                SetJpSeriFr();
            }
        }

        /// <summary>
        /// 年(セリ期間)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtSeriYearTo.Text, this.txtSeriYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtSeriYearTo.SelectAll();
            }
            else
            {
                this.txtSeriYearTo.Text = Util.ToString(IsValid.SetYear(this.txtSeriYearTo.Text));
                CheckJpSeriTo();
                SetJpSeriTo();
            }
        }

        /// <summary>
        /// 月(セリ期間)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtSeriMonthTo.Text, this.txtSeriMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtSeriMonthTo.SelectAll();
            }
            else
            {
                this.txtSeriMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtSeriMonthTo.Text));
                CheckJpSeriTo();
                SetJpSeriTo();
            }
        }

        /// <summary>
        /// 日(セリ期間)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtSeriDayTo.Text, this.txtSeriDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtSeriDayTo.SelectAll();
            }
            else
            {
                this.txtSeriDayTo.Text = Util.ToString(IsValid.SetDay(this.txtSeriDayTo.Text));
                CheckJpSeriTo();
                SetJpSeriTo();
            }
        }

        /// <summary>
        /// 年(精算日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtSeisanYear.Text, this.txtSeisanYear.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanYear.SelectAll();
            }
            else
            {
                this.txtSeisanYear.Text = Util.ToString(IsValid.SetYear(this.txtSeisanYear.Text));
                CheckJpSeisan();
                SetJpSeisan();
            }
        }

        /// <summary>
        /// 月(精算日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtSeisanMonth.Text, this.txtSeisanMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanMonth.SelectAll();
            }
            else
            {
                this.txtSeisanMonth.Text = Util.ToString(IsValid.SetMonth(this.txtSeisanMonth.Text));
                CheckJpSeisan();
                SetJpSeisan();
            }
        }

        /// <summary>
        /// 日(精算日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtSeisanDay.Text, this.txtSeisanDay.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanDay.SelectAll();
            }
            else
            {
                this.txtSeisanDay.Text = Util.ToString(IsValid.SetDay(this.txtSeisanDay.Text));
                CheckJpSeisan();
                SetJpSeisan();
            }
        }

        /// <summary>
        /// 控除項目の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidKojo()
        {
            for (int i = 1; i <= 35; i++)
            {
                if (KojoCheck("txtKomoku" + i.ToString()) == false)
                {
                    return false;
                }
            }
            return true;

        }
       
        /// <summary>
        /// 入力項目キーダウン処理
        /// 最終項目なら登録処理を実行する。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                jp.co.fsi.common.controls.FsiTextBox obj = (jp.co.fsi.common.controls.FsiTextBox)sender;
                if (!checkEnabled(obj.Name))
                {
                    // 最後のアクティブコントロールなら登録処理を実行する。
                    this.PressF4();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            string shishoCd = this.txtMizuageShishoCd.Text;
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            //該当支所の控除項目を表示する
            SearchKomokuNm();

            return true;
        }

        /// <summary>
        /// 精算番号の入力チェック
        /// </summary>
        private bool IsValidSeisanBango()
        {
            if (ValChk.IsEmpty(this.txtSeisanBango.Text))
            {
                this.txtSeisanBango.Text = "0";
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtSeisanBango.Text))
            {
                Msg.Error("精算番号は数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 船主コードの入力チェック
        /// </summary>
        private bool IsValidFunanushi()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushi.Text))
            {
                Msg.Error("船主コードは数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpSeriFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeriGengoFr.Text, this.txtSeriYearFr.Text,
                this.txtSeriMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeriDayFr.Text) > lastDayInMonth)
            {
                this.txtSeriDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpSeriFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpSeriFr(Util.FixJpDate(this.lblSeriGengoFr.Text, this.txtSeriYearFr.Text,
                this.txtSeriMonthFr.Text, this.txtSeriDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpSeriTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeriGengoTo.Text, this.txtSeriYearTo.Text,
                this.txtSeriMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeriDayTo.Text) > lastDayInMonth)
            {
                this.txtSeriDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpSeriTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpSeriTo(Util.FixJpDate(this.lblSeriGengoTo.Text, this.txtSeriYearTo.Text,
                this.txtSeriMonthTo.Text, this.txtSeriDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(精算日)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpSeisan()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeisanGengo.Text, this.txtSeisanYear.Text,
                this.txtSeisanMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeisanDay.Text) > lastDayInMonth)
            {
                this.txtSeisanDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(精算日)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpSeisan()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpSeisan(Util.FixJpDate(this.lblSeisanGengo.Text, this.txtSeisanYear.Text,
                this.txtSeisanMonth.Text, this.txtSeisanDay.Text, this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 精算番号の入力チェック
            if (!IsValidSeisanBango())
            {
                this.txtSeisanBango.Focus();
                this.txtSeisanBango.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushi())
            {
                this.txtFunanushi.Focus();
                this.txtFunanushi.SelectAll();
                return false;
            }

            // 日付範囲(セリ期間)(年)(自)の入力チェック
            this.txtSeriYearFr.Text = Util.ToString(IsValid.SetYear(this.txtSeriYearFr.Text));
            if (!IsValid.IsYear(this.txtSeriYearFr.Text, this.txtSeriYearFr.MaxLength))
            {
                this.txtSeriYearFr.Focus();
                this.txtSeriYearFr.SelectAll();
                return false;
            }
            // 日付範囲(セリ期間)(月)(自)の入力チェック
            this.txtSeriMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtSeriMonthFr.Text));
            if (!IsValid.IsMonth(this.txtSeriMonthFr.Text, this.txtSeriMonthFr.MaxLength))
            {
                this.txtSeriMonthFr.Focus();
                this.txtSeriMonthFr.SelectAll();
                return false;
            }
            // 日付範囲(セリ期間)(日)(自)の入力チェック
            this.txtSeriDayFr.Text = Util.ToString(IsValid.SetDay(this.txtSeriDayFr.Text));
            if (!IsValid.IsDay(this.txtSeriDayFr.Text, this.txtSeriDayFr.MaxLength))
            {
                this.txtSeriDayFr.Focus();
                this.txtSeriDayFr.SelectAll();
                return false;
            }
            // 日付範囲(セリ期間)(自)月末入力チェック処理
            CheckJpSeriFr();
            // 日付範囲(セリ期間)(自)正しい和暦への変換処理
            SetJpSeriFr();

            // 日付範囲(セリ期間)(年)(至)の入力チェック
            this.txtSeriYearTo.Text = Util.ToString(IsValid.SetYear(this.txtSeriYearTo.Text));
            if (!IsValid.IsYear(this.txtSeriYearTo.Text, this.txtSeriYearTo.MaxLength))
            {
                this.txtSeriYearTo.Focus();
                this.txtSeriYearTo.SelectAll();
                return false;
            }
            // 日付範囲(セリ期間)(月)(至)の入力チェック
            this.txtSeriMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtSeriMonthTo.Text));
            if (!IsValid.IsMonth(this.txtSeriMonthTo.Text, this.txtSeriMonthTo.MaxLength))
            {
                this.txtSeriMonthTo.Focus();
                this.txtSeriMonthTo.SelectAll();
                return false;
            }
            // 日付範囲(セリ期間)(日)(至)の入力チェック
            this.txtSeriDayTo.Text = Util.ToString(IsValid.SetDay(this.txtSeriDayTo.Text));
            if (!IsValid.IsDay(this.txtSeriDayTo.Text, this.txtSeriDayTo.MaxLength))
            {
                this.txtSeriDayTo.Focus();
                this.txtSeriDayTo.SelectAll();
                return false;
            }
            // 日付範囲(セリ期間)(至)月末入力チェック処理
            CheckJpSeriTo();
            // 日付範囲(セリ期間)(至)正しい和暦への変換処理
            SetJpSeriTo();

            // 日付範囲(精算日)(年)の入力チェック
            this.txtSeisanYear.Text = Util.ToString(IsValid.SetYear(this.txtSeisanYear.Text));
            if (!IsValid.IsYear(this.txtSeisanYear.Text, this.txtSeisanYear.MaxLength))
            {
                this.txtSeisanYear.Focus();
                this.txtSeisanYear.SelectAll();
                return false;
            }
            // 日付範囲(精算日)(月)の入力チェック
            this.txtSeisanMonth.Text = Util.ToString(IsValid.SetMonth(this.txtSeisanMonth.Text));
            if (!IsValid.IsMonth(this.txtSeisanMonth.Text, this.txtSeisanMonth.MaxLength))
            {
                this.txtSeisanMonth.Focus();
                this.txtSeisanMonth.SelectAll();
                return false;
            }
            // 日付範囲(精算日)(日)の入力チェック
            this.txtSeisanDay.Text = Util.ToString(IsValid.SetDay(this.txtSeisanDay.Text));
            if (!IsValid.IsDay(this.txtSeisanDay.Text, this.txtSeisanDay.MaxLength))
            {
                this.txtSeisanDay.Focus();
                this.txtSeisanDay.SelectAll();
                return false;
            }
            // 日付範囲(精算日)月末入力チェック処理
            CheckJpSeisan();
            // 日付範囲(精算日)正しい和暦への変換処理
            SetJpSeisan();

            //控除項目の値チェック処理
            if (!IsValidKojo())
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpSeriFr(string[] arrJpDate)
        {
            this.lblSeriGengoFr.Text = arrJpDate[0];
            this.txtSeriYearFr.Text = arrJpDate[2];
            this.txtSeriMonthFr.Text = arrJpDate[3];
            this.txtSeriDayFr.Text = arrJpDate[4];
        }
        private void SetJpSeriTo(string[] arrJpDate)
        {
            this.lblSeriGengoTo.Text = arrJpDate[0];
            this.txtSeriYearTo.Text = arrJpDate[2];
            this.txtSeriMonthTo.Text = arrJpDate[3];
            this.txtSeriDayTo.Text = arrJpDate[4];
        }
        private void SetJpSeisan(string[] arrJpDate)
        {
            this.lblSeisanGengo.Text = arrJpDate[0];
            this.txtSeisanYear.Text = arrJpDate[2];
            this.txtSeisanMonth.Text = arrJpDate[3];
            this.txtSeisanDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 初期表示
        /// </summary>
        private void FirstDisp()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付の初期表示
            lblSeriGengoFr.Text = jpDate[0];
            txtSeriYearFr.Text = jpDate[2];
            txtSeriMonthFr.Text = jpDate[3];
            txtSeriDayFr.Text = jpDate[4];

            lblSeriGengoTo.Text = jpDate[0];
            txtSeriYearTo.Text = jpDate[2];
            txtSeriMonthTo.Text = jpDate[3];
            txtSeriDayTo.Text = jpDate[4];

            lblSeisanGengo.Text = jpDate[0];
            txtSeisanYear.Text = jpDate[2];
            txtSeisanMonth.Text = jpDate[3];
            txtSeisanDay.Text = jpDate[4];

            // 項目名の取得
            SearchKomokuNm();

            // 船主名の初期表示
            lblFunanushiResults.Text = "";

            // 金額の初期表示
            txtSeisanBango.Text = "0";
            txtTsumitatekin.Text = "0";
            txtFunanushi.Text = "0";
            txtMizuagegaku.Text = "0";
            txtKojoGokei.Text = "0";
            txtSashihikigaku.Text = "0";
            for (int i = 1; i <= KOMOKUSU; i++)
            {
                Control[] d = this.pnlKomoku.Controls.Find("txtKomoku" + i, true);

                if (0<d.Length)
                {
                    d[0].Text = "0";
                }
            }
        }

        /// <summary>
        /// 項目名を取得し、セットします。
        /// </summary>
        private void SearchKomokuNm()
        {
            string shishoCd = this.txtMizuageShishoCd.Text;

            // TM_控除科目設定テーブルから項目名を取得
            DbParamCollection dpc;
            StringBuilder Sql;
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" SETTEI_CD,");
            Sql.Append(" KOJO_KOMOKU_NM ");
            Sql.Append("FROM");
            //Sql.Append(" TM_HN_KOJO_KAMOKU_SETTEI ");
            Sql.Append(" TB_HN_KOJO_KAMOKU_SETTEI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD ");
            Sql.Append(" AND SHISHO_CD = @SHISHO_CD ");
            Sql.Append("ORDER BY");
            Sql.Append(" SETTEI_CD ASC");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DataTable dtKomokuNm = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);



            //入力項目の名称をクリアする
            for (int i = 1; i <= KOMOKUSU; i++)
            {

                Control[] d = this.pnlKomoku.Controls.Find("lblKomoku" + i,true);

                if (0 < d.Length)
                {
                    d[0].Text = string.Empty;
                }

                //this.pnlKomoku.Controls["lblKomoku" + i].Text = "";
            }

            // 取得した項目名を、設定コードを元に該当する箇所にセット
            foreach (DataRow dr in dtKomokuNm.Rows)
            {

                Control[] d = this.pnlKomoku.Controls.Find("lblKomoku" + Util.ToInt(dr["SETTEI_CD"]), true);

                if (0<d.Length)
                {
                    d[0].Text = Util.ToString(dr["KOJO_KOMOKU_NM"]);
                }

//                this.pnlKomoku.Controls["lblKomoku" + Util.ToInt(dr["SETTEI_CD"])].Text = Util.ToString(dr["KOJO_KOMOKU_NM"]);
            }

            // 項目名がない箇所を入力不可に設定する
            for (int i = 1; i <= KOMOKUSU; i++)
            {

                Control[] d = this.pnlKomoku.Controls.Find("lblKomoku" + i, true);
                Control[] dd = this.pnlKomoku.Controls.Find("txtKomoku" + i, true);

                if (0 < d.Length)
                {
                    if (d[0].Text == string.Empty)
                    {
                        dd[0].Enabled = false;
                    }
                    else
                    {
                        dd[0].Enabled = true;
                    }
                }

                //if (this.pnlKomoku.Controls["lblKomoku" + i].Text == "")
                //{
                //    this.pnlKomoku.Controls["txtKomoku" + i].Enabled = false;
                //}
                //else
                //{
                //    this.pnlKomoku.Controls["txtKomoku" + i].Enabled = true;
                //}
            }
        }

        /// <summary>
        /// 精算番号を検索する。
        /// </summary>
        private void EditSeisan()
        {
            HNSE1022 frmHNSE1022;

            // 精算番号検索画面を起動
            frmHNSE1022 = new HNSE1022();
            frmHNSE1022.Par2 = txtMizuageShishoCd.Text;

            DialogResult result = frmHNSE1022.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                string[] outData = (string[])frmHNSE1022.OutData;
                // 精算番号から控除データを取得して表示
                DispData(outData[0]);
            }
        }

        /// <summary>
        /// 控除項目を登録する。
        /// </summary>
        private void EditKojo()
        {
            //支所コード取得
            string shishoCd = this.txtMizuageShishoCd.Text;
            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom("HNCM1071.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.hn.hncm10711.HNCM1071");
            if (t != null)
            {
                Object obj = System.Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = "1";
                    frm.Par2 = shishoCd;
                    frm.ShowDialog(this);
                }
            }
        }

        /// <summary>
        /// 控除データをセットする。
        /// </summary>
        private void DispData(string seisanBango)
        {
            string shishoCd = this.txtMizuageShishoCd.Text;

            StringBuilder Sql;
            DbParamCollection dpc;
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_KOJO_DATA ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" SEISAN_BANGO = @SEISAN_BANGO ");
            Sql.Append(" AND SHISHO_CD = @SHISHO_CD ");

            Sql.Append("ORDER BY ");
            Sql.Append(" SEISAN_BANGO ASC");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@SEISAN_BANGO", SqlDbType.VarChar, 6, seisanBango);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtKojoData = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtKojoData.Rows.Count > 0)
            {
                // 控除金額のセット
                for (int i = 1; i <= KOMOKUSU; i++)
                {
                    Control[] d = this.pnlKomoku.Controls.Find("txtKomoku" + i, true);
//					this.pnlKomoku.Controls["txtKomoku" + i].Text = Util.ToString(dtKojoData.Rows[0]["KOJO_KOMOKU" + i]);

					if (0<d.Length)
                    {
                        d[0].Text = Util.ToString(dtKojoData.Rows[0]["KOJO_KOMOKU" + i]);
                    }
                }
                // セリ開始期間をセット
                string[] seriKaishi = Util.ConvJpDate(Util.ToDate(dtKojoData.Rows[0]["SEISAN_SERI_KAISHI_KIKAN"]), this.Dba);
                this.lblSeriGengoFr.Text = seriKaishi[0];
                this.txtSeriYearFr.Text = seriKaishi[2];
                this.txtSeriMonthFr.Text = seriKaishi[3];
                this.txtSeriDayFr.Text = seriKaishi[4];
                // セリ終了期間をセット
                string[] seriShuryo = Util.ConvJpDate(Util.ToDate(dtKojoData.Rows[0]["SEISAN_SERI_SHURYO_KIKAN"]), this.Dba);
                this.lblSeriGengoTo.Text = seriKaishi[0];
                this.txtSeriYearTo.Text = seriKaishi[2];
                this.txtSeriMonthTo.Text = seriKaishi[3];
                this.txtSeriDayTo.Text = seriKaishi[4];
                // 精算日をセット
                string[] seisanbi = Util.ConvJpDate(Util.ToDate(dtKojoData.Rows[0]["SEISAN_BI"]), this.Dba);
                this.lblSeisanGengo.Text = seisanbi[0];
                this.txtSeisanYear.Text = seisanbi[2];
                this.txtSeisanMonth.Text = seisanbi[3];
                this.txtSeisanDay.Text = seisanbi[4];
                // 船主データをセット
                txtSeisanBango.Text = Util.ToString(dtKojoData.Rows[0]["SEISAN_BANGO"]);
                txtFunanushi.Text = Util.ToString(dtKojoData.Rows[0]["SENSHU_CD"]);
                lblFunanushiResults.Text = Util.ToString(dtKojoData.Rows[0]["SENSHU_NM"]);
                if (Util.ToString(dtKojoData.Rows[0]["TSUMITATEKIN"]) != "")
                {
                    txtTsumitatekin.Text = Util.ToString(dtKojoData.Rows[0]["TSUMITATEKIN"]);
                }
                txtMizuagegaku.Text = Util.ToString(dtKojoData.Rows[0]["ZEIKOMI_MIZUAGE_KINGAKU"]);
                txtKojoGokei.Text = Util.ToString(dtKojoData.Rows[0]["KOJO_GOKEIGAKU"]);
                txtSashihikigaku.Text = Util.ToString(dtKojoData.Rows[0]["SASHIHIKI_SEISANGAKU"]);
            }
            else
            {
                FirstDisp();
            }
        }

        /// <summary>
        /// 控除データを削除する
        /// </summary>
        private void DeleteData()
        {
            try
            {
                string shishoCd = this.txtMizuageShishoCd.Text;
                // トランザクションの開始
                this.Dba.BeginTransaction();

                StringBuilder Sql;
                DbParamCollection dpc;
                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("DELETE FROM");
                Sql.Append(" TB_HN_KOJO_DATA ");
                Sql.Append("WHERE");
                Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
                Sql.Append(" SEISAN_BANGO = @SEISAN_BANGO");
                Sql.Append(" AND SHISHO_CD = @SHISHO_CD ");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
                dpc.SetParam("@SEISAN_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(txtSeisanBango.Text));
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("削除が完了しました。");

                // 初期表示に戻す
                FirstDisp();
            }
            catch
            {
                Msg.Info("削除処理に失敗しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 控除データを登録する
        /// </summary>
        private void RegistData()
        {
            string shishoCd = this.txtMizuageShishoCd.Text;

            StringBuilder Sql;
            DbParamCollection dpc;
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" COUNT(*) AS KENSU ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_KOJO_DATA ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" SEISAN_BANGO = @SEISAN_BANGO");
            Sql.Append(" AND SHISHO_CD = @SHISHO_CD ");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@SEISAN_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(txtSeisanBango.Text));
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtKojoDataCt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 控除データがなければエラー
            if (Util.ToDecimal(dtKojoDataCt.Rows[0]["KENSU"]) == 0)
            {
                Msg.Error("データがありません");
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("UPDATE");
                Sql.Append(" TB_HN_KOJO_DATA ");
                Sql.Append("SET");
                Sql.Append(" SENSHU_CD = @SENSHU_CD,");
                Sql.Append(" SENSHU_NM = @SENSHU_NM,");
                Sql.Append(" ZEIKOMI_MIZUAGE_KINGAKU = @ZEIKOMI_MIZUAGE_KINGAKU,");
                Sql.Append(" KOJO_KOMOKU1 = @KOJO_KOMOKU1,");
                Sql.Append(" KOJO_KOMOKU2 = @KOJO_KOMOKU2,");
                Sql.Append(" KOJO_KOMOKU3 = @KOJO_KOMOKU3,");
                Sql.Append(" KOJO_KOMOKU4 = @KOJO_KOMOKU4,");
                Sql.Append(" KOJO_KOMOKU5 = @KOJO_KOMOKU5,");
                Sql.Append(" KOJO_KOMOKU6 = @KOJO_KOMOKU6,");
                Sql.Append(" KOJO_KOMOKU7 = @KOJO_KOMOKU7,");
                Sql.Append(" KOJO_KOMOKU8 = @KOJO_KOMOKU8,");
                Sql.Append(" KOJO_KOMOKU9 = @KOJO_KOMOKU9,");
                Sql.Append(" KOJO_KOMOKU10 = @KOJO_KOMOKU10,");
                Sql.Append(" KOJO_KOMOKU11 = @KOJO_KOMOKU11,");
                Sql.Append(" KOJO_KOMOKU12 = @KOJO_KOMOKU12,");
                Sql.Append(" KOJO_KOMOKU13 = @KOJO_KOMOKU13,");
                Sql.Append(" KOJO_KOMOKU14 = @KOJO_KOMOKU14,");
                Sql.Append(" KOJO_KOMOKU15 = @KOJO_KOMOKU15,");
                Sql.Append(" KOJO_KOMOKU16 = @KOJO_KOMOKU16,");
                Sql.Append(" KOJO_KOMOKU17 = @KOJO_KOMOKU17,");
                Sql.Append(" KOJO_KOMOKU18 = @KOJO_KOMOKU18,");
                Sql.Append(" KOJO_KOMOKU19 = @KOJO_KOMOKU19,");
                Sql.Append(" KOJO_KOMOKU20 = @KOJO_KOMOKU20,");
                Sql.Append(" KOJO_KOMOKU21 = @KOJO_KOMOKU21,");
                Sql.Append(" KOJO_KOMOKU22 = @KOJO_KOMOKU22,");
                Sql.Append(" KOJO_KOMOKU23 = @KOJO_KOMOKU23,");
                Sql.Append(" KOJO_KOMOKU24 = @KOJO_KOMOKU24,");
                Sql.Append(" KOJO_KOMOKU25 = @KOJO_KOMOKU25,");
                Sql.Append(" KOJO_KOMOKU26 = @KOJO_KOMOKU26,");
                Sql.Append(" KOJO_KOMOKU27 = @KOJO_KOMOKU27,");
                Sql.Append(" KOJO_KOMOKU28 = @KOJO_KOMOKU28,");
                Sql.Append(" KOJO_KOMOKU29 = @KOJO_KOMOKU29,");
                Sql.Append(" KOJO_KOMOKU30 = @KOJO_KOMOKU30,");
                Sql.Append(" KOJO_KOMOKU31 = @KOJO_KOMOKU31,");
                Sql.Append(" KOJO_KOMOKU32 = @KOJO_KOMOKU32,");
                Sql.Append(" KOJO_KOMOKU33 = @KOJO_KOMOKU33,");
                Sql.Append(" KOJO_KOMOKU34 = @KOJO_KOMOKU34,");
                Sql.Append(" KOJO_KOMOKU35 = @KOJO_KOMOKU35,");
                Sql.Append(" KOJO_GOKEIGAKU = @KOJO_GOKEIGAKU,");
                Sql.Append(" SASHIHIKI_SEISANGAKU = @SASHIHIKI_SEISANGAKU,");
                Sql.Append(" UPDATE_DATE = @UPDATE_DATE ");
                Sql.Append("WHERE");
                Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
                Sql.Append(" SEISAN_BANGO = @SEISAN_BANGO");
                Sql.Append(" AND SHISHO_CD = @SHISHO_CD ");


                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@SEISAN_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(txtSeisanBango.Text));
                dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 5, Util.ToDecimal(txtFunanushi.Text));
                dpc.SetParam("@SENSHU_NM", SqlDbType.VarChar, 40, lblFunanushiResults.Text);
                dpc.SetParam("@ZEIKOMI_MIZUAGE_KINGAKU", SqlDbType.Decimal, 12, Util.ToDecimal(txtMizuagegaku.Text));
                dpc.SetParam("@KOJO_KOMOKU1", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku1.Text));
                dpc.SetParam("@KOJO_KOMOKU2", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku2.Text));
                dpc.SetParam("@KOJO_KOMOKU3", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku3.Text));
                dpc.SetParam("@KOJO_KOMOKU4", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku4.Text));
                dpc.SetParam("@KOJO_KOMOKU5", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku5.Text));
                dpc.SetParam("@KOJO_KOMOKU6", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku6.Text));
                dpc.SetParam("@KOJO_KOMOKU7", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku7.Text));
                dpc.SetParam("@KOJO_KOMOKU8", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku8.Text));
                dpc.SetParam("@KOJO_KOMOKU9", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku9.Text));
                dpc.SetParam("@KOJO_KOMOKU10", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku10.Text));
                dpc.SetParam("@KOJO_KOMOKU11", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku11.Text));
                dpc.SetParam("@KOJO_KOMOKU12", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku12.Text));
                dpc.SetParam("@KOJO_KOMOKU13", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku13.Text));
                dpc.SetParam("@KOJO_KOMOKU14", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku14.Text));
                dpc.SetParam("@KOJO_KOMOKU15", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku15.Text));
                dpc.SetParam("@KOJO_KOMOKU16", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku16.Text));
                dpc.SetParam("@KOJO_KOMOKU17", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku17.Text));
                dpc.SetParam("@KOJO_KOMOKU18", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku18.Text));
                dpc.SetParam("@KOJO_KOMOKU19", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku19.Text));
                dpc.SetParam("@KOJO_KOMOKU20", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku20.Text));
                dpc.SetParam("@KOJO_KOMOKU21", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku21.Text));
                dpc.SetParam("@KOJO_KOMOKU22", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku22.Text));
                dpc.SetParam("@KOJO_KOMOKU23", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku23.Text));
                dpc.SetParam("@KOJO_KOMOKU24", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku24.Text));
                dpc.SetParam("@KOJO_KOMOKU25", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku25.Text));
                dpc.SetParam("@KOJO_KOMOKU26", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku26.Text));
                dpc.SetParam("@KOJO_KOMOKU27", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku27.Text));
                dpc.SetParam("@KOJO_KOMOKU28", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku28.Text));
                dpc.SetParam("@KOJO_KOMOKU29", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku29.Text));
                dpc.SetParam("@KOJO_KOMOKU30", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku30.Text));
                dpc.SetParam("@KOJO_KOMOKU31", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku31.Text));
                dpc.SetParam("@KOJO_KOMOKU32", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku32.Text));
                dpc.SetParam("@KOJO_KOMOKU33", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku33.Text));
                dpc.SetParam("@KOJO_KOMOKU34", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku34.Text));
                dpc.SetParam("@KOJO_KOMOKU35", SqlDbType.Decimal, 12, Util.ToDecimal(txtKomoku35.Text));
                dpc.SetParam("@KOJO_GOKEIGAKU", SqlDbType.Decimal, 12, Util.ToDecimal(txtKojoGokei.Text));
                dpc.SetParam("@SASHIHIKI_SEISANGAKU", SqlDbType.Decimal, 12, Util.ToDecimal(txtSashihikigaku.Text));
                dpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, 20, DateTime.Today);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("登録が完了しました。");

                // 初期表示に戻す
                FirstDisp();
            }
            catch
            {
                Msg.Info("登録に失敗しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 控除データの値チェック
        /// </summary>
        private bool KojoCheck(string komoku)
        {
            string shishoCd = this.txtMizuageShishoCd.Text;

            Control[] d = this.pnlKomoku.Controls.Find(komoku,true);

            if (0<d.Length)
            {
                if (ValChk.IsEmpty(d[0].Text))
                {
                    d[0].Text = "0";
                }
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                else if (!ValChk.IsNumber(d[0].Text))
                {
                    Msg.Notice("控除項目は数値のみで入力してください。");
                    return false;
                }
            }

            // 計算する控除項目を取得
            StringBuilder Sql;
            DbParamCollection dpc;
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_KOJO_KAMOKU_SETTEI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD ");
            Sql.Append(" AND SHISHO_CD = @SHISHO_CD ");
            Sql.Append("ORDER BY");
            Sql.Append(" SETTEI_CD ");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtKojoKamoku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            Decimal KojoKingaku = 0;
            string kojoKomoku;
            // 控除額が水揚金額を超えていたらエラー
            foreach (DataRow dr in dtKojoKamoku.Rows)
            {
                kojoKomoku = Util.ToString(dr["KOJO_KOMOKU_SETTEI"]).Replace("控除項目", "");
                kojoKomoku = Microsoft.VisualBasic.Strings.StrConv(kojoKomoku, VbStrConv.Narrow);


                Control[] dd = this.pnlKomoku.Controls.Find("txtKomoku" + kojoKomoku,true);

                if (0< dd.Length)
                {
                    if ("1".Equals(dr["KEISAN_FUGO"]))
                    {
                        KojoKingaku -= Util.ToDecimal(dd[0].Text);
                    }
                    else
                    {
                        KojoKingaku += Util.ToDecimal(dd[0].Text);
                    }
                }

            }

            if (KojoKingaku > Util.ToDecimal(txtMizuagegaku.Text))
            {
                Msg.Error("金額がマイナスになります！");
                return false;
            }

            // 控除合計額,差引精算額をセット
            txtKojoGokei.Text = Util.ToString(KojoKingaku);
            txtSashihikigaku.Text = Util.ToString(Util.ToDecimal(txtMizuagegaku.Text) - Util.ToDecimal(txtKojoGokei.Text));

            return true;
        }

        /// <summary>
        /// 入力項目で最終コントロールであるかのチェック処理
        /// キーダウンから呼ばれて処理する。
        /// </summary>
        /// <param name="ctrlNm"></param>
        /// <returns>true: まだ入力項目がある、false:無い！最後</returns>
        private bool checkEnabled(string ctrlNm)
        {
            //string[] ctrlName = new string[4] { "txtKintai", "txtShikyu", "txtKojo", "txtGokei" };
            string[] ctrlName = new string[1] { "txtKomoku" };
            int arrayIndex = 0;
            for (arrayIndex = 0; arrayIndex < ctrlName.Length; arrayIndex++)
            {
                if (ctrlNm.IndexOf(ctrlName[arrayIndex]) >= 0)
                {
                    break;
                }
            }
            // チェックを始める項目番号＋１を取得（自分の次から）
            int ctrlNo = Util.ToInt(ctrlNm.Substring(ctrlName[arrayIndex].Length)) + 1;

            for (int i = arrayIndex; i < ctrlName.Length; i++)
            {
                for (int j = ctrlNo; j < 21; j++)
                {
                    Control[] c = this.Controls.Find(ctrlName[i] + j.ToString(), true);
                    if (c.Length > 0)
                    {

                        if (((jp.co.fsi.common.controls.FsiTextBox)c[0]).Enabled)
                            return true;
                    }
                }
                // 次の項目のチェックを始める前に番号を初期化
                ctrlNo = 1;
            }
            return false;
        }
        #endregion
    }
}
