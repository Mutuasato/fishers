﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnse1021
{
    /// <summary>
    /// 精算番号検索(HNSE1022)
    /// </summary>
    public partial class HNSE1022 : BasePgForm
    {
        #region 定数
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSE1022()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            lblFunanushiCdResults.Text = "先　頭";

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付の初期表示
            lblSeisanbiGengo.Text = jpDate[0];
            txtSeisanbiYear.Text = jpDate[2];
            txtSeisanbiMonth.Text = jpDate[3];
            txtSeisanbiDay.Text = jpDate[4];
            lblDenpyoGengoFr.Text = jpDate[0];
            txtDenpyoYearFr.Text = jpDate[2];
            txtDenpyoMonthFr.Text = jpDate[3];
            txtDenpyoDayFr.Text = jpDate[4];
            lblDenpyoGengoTo.Text = jpDate[0];
            txtDenpyoYearTo.Text = jpDate[2];
            txtDenpyoMonthTo.Text = jpDate[3];
            txtDenpyoDayTo.Text = jpDate[4];

            // サイズを縮める731, 734
            this.Size = new Size(742, 602);
            // 必要なボタンのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnEnter.Visible = true;
            this.btnEnter.Location = this.btnF2.Location;
            this.btnF1.Location = this.btnF3.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Location = this.btnF4.Location;
            this.btnF5.Visible = false;
            this.btnF6.Location = this.btnF5.Location;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 初期表示
            SearchData(true);

            // Enter処理を無効化
            this._dtFlg = false;

			txtFunanushiCd.Focus();

		}

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 船主CD、精算日に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtFunanushiCd":
                case "txtSeisanbiYear":
                    this.btnF1.Enabled = true;
                    this.btnEnter.Enabled = false;
                    break;

                case "dgvList":
                    this.btnEnter.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    this.btnEnter.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtFunanushiCd":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCd.Text = outData[0];
                                this.lblFunanushiCdResults.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtSeisanbiYear":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeisanbiGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblSeisanbiGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblSeisanbiGengo.Text, this.txtSeisanbiYear.Text,
                                    this.txtSeisanbiMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtSeisanbiDay.Text) > lastDayInMonth)
                                {
                                    this.txtSeisanbiDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblSeisanbiGengo.Text,
                                        this.txtSeisanbiYear.Text,
                                        this.txtSeisanbiMonth.Text,
                                        this.txtSeisanbiDay.Text,
                                        this.Dba);
                                this.lblSeisanbiGengo.Text = arrJpDate[0];
                                this.txtSeisanbiYear.Text = arrJpDate[2];
                                this.txtSeisanbiMonth.Text = arrJpDate[3];
                                this.txtSeisanbiDay.Text = arrJpDate[4];

                                this.lblDenpyoGengoFr.Text = arrJpDate[0];
                                this.txtDenpyoYearFr.Text = arrJpDate[2];
                                this.txtDenpyoMonthFr.Text = arrJpDate[3];
                                this.txtDenpyoDayFr.Text = arrJpDate[4];

                                this.lblDenpyoGengoTo.Text = arrJpDate[0];
                                this.txtDenpyoYearTo.Text = arrJpDate[2];
                                this.txtDenpyoMonthTo.Text = arrJpDate[3];
                                this.txtDenpyoDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 船主コードにフォーカスを当てる
            txtFunanushiCd.Focus();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            //検索処理
            SearchData(false);
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
                if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
                {
                    Msg.Error("この会計年度は凍結されています。");
                    return;
                }

                // 入力日付に該当する会計年度が選択会計年度と一するかチェック
                DateTime DENPYO_DATE = Util.ConvAdDate(this.lblSeisanbiGengo.Text, this.txtSeisanbiYear.Text,
                        this.txtSeisanbiMonth.Text, this.txtSeisanbiDay.Text, this.Dba);
                if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
                {
                    Msg.Error("入力日付が選択会計年度の範囲外です。");
                    return;
                }

                // 選択会計年度が今年度かチェック
                DateTime today = DateTime.Today;
                if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
                {
                    if (Msg.ConfNmYesNo("精算番号検索", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                    {
                        return;
                    }
                }

                if (this.dgvList.SelectedRows.Count > 0)
                {
                    ReturnVal();
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 入力日付に該当する会計年度が選択会計年度と一するかチェック
            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblSeisanbiGengo.Text, this.txtSeisanbiYear.Text,
                    this.txtSeisanbiMonth.Text, this.txtSeisanbiDay.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return;
            }

            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("精算番号検索", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            ReturnVal();
        }

        /// <summary>
        /// 精算区分でのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gbxSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            //検索処理
            SearchData(false);
        }

        /// <summary>
        /// 船主コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCd_Validating(object sender, CancelEventArgs e)
        {
            // コードを元に名称を取得する
            this.lblFunanushiCdResults.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", null, this.txtFunanushiCd.Text);

            if (ValChk.IsEmpty(this.txtFunanushiCd.Text))
            {
                this.lblFunanushiCdResults.Text = "先　頭";
            }
        }

        /// <summary>
        /// 年(精算日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanbiYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtSeisanbiYear.Text, this.txtSeisanbiYear.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanbiYear.SelectAll();
            }
            else
            {
                this.txtSeisanbiYear.Text = Util.ToString(IsValid.SetYear(this.txtSeisanbiYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 月(精算日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanbiMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtSeisanbiMonth.Text, this.txtSeisanbiMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanbiMonth.SelectAll();
            }
            else
            {
                this.txtSeisanbiMonth.Text = Util.ToString(IsValid.SetMonth(this.txtSeisanbiMonth.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 日(精算日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanbiDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtSeisanbiDay.Text, this.txtSeisanbiDay.MaxLength))
            {
                e.Cancel = true;
                this.txtSeisanbiDay.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                this.txtSeisanbiDay.Text = Util.ToString(IsValid.SetDay(this.txtSeisanbiDay.Text));
                CheckJp();
                SetJp();

                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 日(精算日)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanbiDay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 船主コードの入力チェック
        /// </summary>
        private bool IsValidFunanushiCd()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCd.Text))
            {
                this.lblFunanushiCdResults.Text = "先　頭";
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtFunanushiCd.Text))
            {
                Msg.Error("船主コードは数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdResults.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", null, this.txtFunanushiCd.Text);
            }

            return true;
        }

        /// <summary>
        /// 年月日の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeisanbiGengo.Text, this.txtSeisanbiYear.Text,
                this.txtSeisanbiMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeisanbiDay.Text) > lastDayInMonth)
            {
                this.txtSeisanbiDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblSeisanbiGengo.Text, this.txtSeisanbiYear.Text,
                this.txtSeisanbiMonth.Text, this.txtSeisanbiDay.Text, this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 船主コードの入力チェック
            if (!IsValidFunanushiCd())
            {
                this.txtFunanushiCd.Focus();
                this.txtFunanushiCd.SelectAll();
                return false;
            }

            // 日付範囲(年)(自)の入力チェック
            this.txtSeisanbiYear.Text = Util.ToString(IsValid.SetYear(this.txtSeisanbiYear.Text));
            if (!IsValid.IsYear(this.txtSeisanbiYear.Text, this.txtSeisanbiYear.MaxLength))
            {
                this.txtSeisanbiYear.Focus();
                this.txtSeisanbiYear.SelectAll();
                return false;
            }
            // 日付範囲(月)(自)の入力チェック
            this.txtSeisanbiMonth.Text = Util.ToString(IsValid.SetMonth(this.txtSeisanbiMonth.Text));
            if (!IsValid.IsMonth(this.txtSeisanbiMonth.Text, this.txtSeisanbiMonth.MaxLength))
            {
                this.txtSeisanbiMonth.Focus();
                this.txtSeisanbiMonth.SelectAll();
                return false;
            }
            // 日付範囲(日)(自)の入力チェック
            this.txtSeisanbiDay.Text = Util.ToString(IsValid.SetDay(this.txtSeisanbiDay.Text));
            if (!IsValid.IsDay(this.txtSeisanbiMonth.Text, this.txtSeisanbiMonth.MaxLength))
            {
                this.txtSeisanbiMonth.Focus();
                this.txtSeisanbiMonth.SelectAll();
                return false;
            }
            // 日付範囲(自)月末入力チェック処理
            CheckJp();
            // 日付範囲(自)正しい和暦への変換処理
            SetJp();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblSeisanbiGengo.Text = arrJpDate[0];
            this.txtSeisanbiYear.Text = arrJpDate[2];
            this.txtSeisanbiMonth.Text = arrJpDate[3];
            this.txtSeisanbiDay.Text = arrJpDate[4];

            this.lblDenpyoGengoFr.Text = arrJpDate[0];
            this.txtDenpyoYearFr.Text = arrJpDate[2];
            this.txtDenpyoMonthFr.Text = arrJpDate[3];
            this.txtDenpyoDayFr.Text = arrJpDate[4];

            this.lblDenpyoGengoTo.Text = arrJpDate[0];
            this.txtDenpyoYearTo.Text = arrJpDate[2];
            this.txtDenpyoMonthTo.Text = arrJpDate[3];
            this.txtDenpyoDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private void SearchData(bool isNew)
        {

            //支所コードの受け取り
            string ShishoCd = Util.ToString(this.Par2);

            // 日付を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblSeisanbiGengo.Text, this.txtSeisanbiYear.Text,
                    this.txtSeisanbiMonth.Text, this.txtSeisanbiDay.Text, this.Dba);

            // データを取得して表示
            DbParamCollection dpc = new DbParamCollection();

            string cols = "SEISAN_BANGO AS 精算番号, ";
            cols += "CASE WHEN TSUMITATEKIN IS NULL THEN '0' ELSE TSUMITATEKIN END AS 積立金, ";
            cols += "SENSHU_CD AS 船主CD, ";
            cols += "SENSHU_NM AS 船主名称, ";
            cols += "ZEIKOMI_MIZUAGE_KINGAKU AS 税込水揚金額 ";
            string from = "TB_HN_KOJO_DATA ";
            string where = "KAISHA_CD = @KAISHA_CD ";
            where+= " AND SHISHO_CD = @SHISHO_CD ";
            // 初期表示の場合は条件に一致させない
            if (isNew)
            {
                where += "AND SENSHU_CD = -1 ";
            }
            else
            {
                if (txtFunanushiCd.Text != "")
                {
                    where += "AND SENSHU_CD = @SENSHU_CD ";
                }
                where += "AND SEISAN_BI = @SEISAN_BI ";
            }

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 40, this.txtFunanushiCd.Text);
            dpc.SetParam("@SEISAN_BI", SqlDbType.DateTime, tmpDate);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, ShishoCd);


            DataTable dtTantosha =
                this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);

            if (!isNew)
            {
                if (dtTantosha.Rows.Count == 0)
                {
                    Msg.Error("データがありません。");
                    this.txtSeisanbiMonth.Focus();
                }
            }

            this.dgvList.DataSource = dtTantosha;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            // this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[1].Width = 110;
            this.dgvList.Columns[2].Width = 100;
            this.dgvList.Columns[3].Width = 230;
            this.dgvList.Columns[4].Width = 120;

            // 書式を設定する
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].DefaultCellStyle.Format = "#,0";
            this.dgvList.Columns[4].DefaultCellStyle.Format = "#,0";

            // データがある場合はフォーカスを当てる
            if (this.dgvList.Rows.Count > 0)
            {
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            if (this.dgvList.SelectedRows.Count == 0)
            {
                Msg.InfoNm("精算番号検索", "該当データがありません。");
                this.txtSeisanbiMonth.Focus();
            }
            else
            {
                this.OutData = new string[1] { 
                    Util.ToString(this.dgvList.SelectedRows[0].Cells["精算番号"].Value),
                };
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        #endregion
    }
}
