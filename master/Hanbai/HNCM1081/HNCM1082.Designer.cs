﻿namespace jp.co.fsi.hn.hncm1081
{
    partial class HNCM1082
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblPayaoCd = new System.Windows.Forms.Label();
			this.txtPayaoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtPayaoNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblPayaoNm = new System.Windows.Forms.Label();
			this.txtPayaoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblPayaoKanaNm = new System.Windows.Forms.Label();
			this.lblTesuryoRitsu = new System.Windows.Forms.Label();
			this.lblRitsuNm = new System.Windows.Forms.Label();
			this.txtTesuryoRitsu = new jp.co.fsi.common.controls.FsiTextBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 190);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(486, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(475, 31);
			this.lblTitle.Text = "";
			// 
			// lblPayaoCd
			// 
			this.lblPayaoCd.BackColor = System.Drawing.Color.Silver;
			this.lblPayaoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblPayaoCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblPayaoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblPayaoCd.Location = new System.Drawing.Point(0, 0);
			this.lblPayaoCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblPayaoCd.Name = "lblPayaoCd";
			this.lblPayaoCd.Size = new System.Drawing.Size(454, 30);
			this.lblPayaoCd.TabIndex = 1;
			this.lblPayaoCd.Tag = "CHANGE";
			this.lblPayaoCd.Text = "パヤオコード";
			this.lblPayaoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtPayaoCd
			// 
			this.txtPayaoCd.AutoSizeFromLength = false;
			this.txtPayaoCd.DisplayLength = null;
			this.txtPayaoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtPayaoCd.Location = new System.Drawing.Point(132, 3);
			this.txtPayaoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtPayaoCd.MaxLength = 6;
			this.txtPayaoCd.Name = "txtPayaoCd";
			this.txtPayaoCd.Size = new System.Drawing.Size(69, 23);
			this.txtPayaoCd.TabIndex = 1;
			this.txtPayaoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtPayaoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtPayaoCd_Validating);
			// 
			// txtPayaoNm
			// 
			this.txtPayaoNm.AutoSizeFromLength = false;
			this.txtPayaoNm.DisplayLength = null;
			this.txtPayaoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtPayaoNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtPayaoNm.Location = new System.Drawing.Point(132, 3);
			this.txtPayaoNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtPayaoNm.MaxLength = 40;
			this.txtPayaoNm.Name = "txtPayaoNm";
			this.txtPayaoNm.Size = new System.Drawing.Size(299, 23);
			this.txtPayaoNm.TabIndex = 2;
			this.txtPayaoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtPayaoNm_Validating);
			// 
			// lblPayaoNm
			// 
			this.lblPayaoNm.BackColor = System.Drawing.Color.Silver;
			this.lblPayaoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblPayaoNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblPayaoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblPayaoNm.Location = new System.Drawing.Point(0, 0);
			this.lblPayaoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblPayaoNm.Name = "lblPayaoNm";
			this.lblPayaoNm.Size = new System.Drawing.Size(454, 30);
			this.lblPayaoNm.TabIndex = 3;
			this.lblPayaoNm.Tag = "CHANGE";
			this.lblPayaoNm.Text = "パ ヤ オ 名";
			this.lblPayaoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtPayaoKanaNm
			// 
			this.txtPayaoKanaNm.AutoSizeFromLength = false;
			this.txtPayaoKanaNm.DisplayLength = null;
			this.txtPayaoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtPayaoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtPayaoKanaNm.Location = new System.Drawing.Point(132, 3);
			this.txtPayaoKanaNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtPayaoKanaNm.MaxLength = 40;
			this.txtPayaoKanaNm.Name = "txtPayaoKanaNm";
			this.txtPayaoKanaNm.Size = new System.Drawing.Size(299, 23);
			this.txtPayaoKanaNm.TabIndex = 3;
			this.txtPayaoKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtPayaoKanaNm_Validating);
			// 
			// lblPayaoKanaNm
			// 
			this.lblPayaoKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblPayaoKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblPayaoKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblPayaoKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblPayaoKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblPayaoKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblPayaoKanaNm.Name = "lblPayaoKanaNm";
			this.lblPayaoKanaNm.Size = new System.Drawing.Size(454, 30);
			this.lblPayaoKanaNm.TabIndex = 5;
			this.lblPayaoKanaNm.Tag = "CHANGE";
			this.lblPayaoKanaNm.Text = "パヤオカナ名";
			this.lblPayaoKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTesuryoRitsu
			// 
			this.lblTesuryoRitsu.BackColor = System.Drawing.Color.Silver;
			this.lblTesuryoRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTesuryoRitsu.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTesuryoRitsu.Location = new System.Drawing.Point(0, 0);
			this.lblTesuryoRitsu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTesuryoRitsu.Name = "lblTesuryoRitsu";
			this.lblTesuryoRitsu.Size = new System.Drawing.Size(454, 32);
			this.lblTesuryoRitsu.TabIndex = 7;
			this.lblTesuryoRitsu.Tag = "CHANGE";
			this.lblTesuryoRitsu.Text = "手　数　料　率";
			this.lblTesuryoRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblRitsuNm
			// 
			this.lblRitsuNm.BackColor = System.Drawing.Color.Silver;
			this.lblRitsuNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblRitsuNm.Location = new System.Drawing.Point(209, 2);
			this.lblRitsuNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblRitsuNm.Name = "lblRitsuNm";
			this.lblRitsuNm.Size = new System.Drawing.Size(27, 24);
			this.lblRitsuNm.TabIndex = 9;
			this.lblRitsuNm.Tag = "CHANGE";
			this.lblRitsuNm.Text = "％";
			this.lblRitsuNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtTesuryoRitsu
			// 
			this.txtTesuryoRitsu.AutoSizeFromLength = false;
			this.txtTesuryoRitsu.DisplayLength = null;
			this.txtTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTesuryoRitsu.Location = new System.Drawing.Point(132, 4);
			this.txtTesuryoRitsu.Margin = new System.Windows.Forms.Padding(4);
			this.txtTesuryoRitsu.MaxLength = 5;
			this.txtTesuryoRitsu.Name = "txtTesuryoRitsu";
			this.txtTesuryoRitsu.Size = new System.Drawing.Size(69, 23);
			this.txtTesuryoRitsu.TabIndex = 4;
			this.txtTesuryoRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTesuryoRitsu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
			this.txtTesuryoRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtTesuryoRitsu_Validating);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 34);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 4;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(462, 151);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lblRitsuNm);
			this.fsiPanel4.Controls.Add(this.txtTesuryoRitsu);
			this.fsiPanel4.Controls.Add(this.lblTesuryoRitsu);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 115);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(454, 32);
			this.fsiPanel4.TabIndex = 3;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtPayaoKanaNm);
			this.fsiPanel3.Controls.Add(this.lblPayaoKanaNm);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 78);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(454, 30);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtPayaoNm);
			this.fsiPanel2.Controls.Add(this.lblPayaoNm);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 41);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(454, 30);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtPayaoCd);
			this.fsiPanel1.Controls.Add(this.lblPayaoCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(454, 30);
			this.fsiPanel1.TabIndex = 0;
			// 
			// HNCM1082
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(475, 327);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "HNCM1082";
			this.ShowFButton = true;
			this.Text = "";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPayaoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtPayaoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtPayaoNm;
        private System.Windows.Forms.Label lblPayaoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtPayaoKanaNm;
        private System.Windows.Forms.Label lblPayaoKanaNm;
        private System.Windows.Forms.Label lblTesuryoRitsu;
        private System.Windows.Forms.Label lblRitsuNm;
        private common.controls.FsiTextBox txtTesuryoRitsu;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel4;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
	}
}