﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1081
{
    /// <summary>
    /// パヤオマスタメンテ(変更・追加)(HNCM1082)
    /// </summary>
    public partial class HNCM1082 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1082()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1082(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {

            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：担当者コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF3.Location = this.btnF2.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                //case "txtTesuryoRitsu":
                case "txtJimotoShiwakeCd":
                case "txtGyosyuBunrui":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
                return;

            if (MODE_NEW.Equals(Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                DbParamCollection whereParam;
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@PAYAO_CD", SqlDbType.Decimal, 15, this.txtPayaoCd.Text);
                this.Dba.Delete("TB_HN_PAYAO_TESURYO_RITSU",
                    "KAISHA_CD = @KAISHA_CD AND PAYAO_CD = @PAYAO_CD",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled)
                return;

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTanto = SetCmTantoParams();

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.パヤオマスタ
                    this.Dba.Insert("TB_HN_PAYAO_TESURYO_RITSU", (DbParamCollection)alParamsCmTanto[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.パヤオマスタ
                    this.Dba.Update("TB_HN_PAYAO_TESURYO_RITSU",
                        (DbParamCollection)alParamsCmTanto[1],
                        "KAISHA_CD = @KAISHA_CD AND PAYAO_CD = @PAYAO_CD",
                        (DbParamCollection)alParamsCmTanto[0]);

                }

                // トランザクションをコミット
                this.Dba.Commit();

            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// パヤオコードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPayaoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuCd())
            {
                e.Cancel = true;
                this.txtPayaoCd.SelectAll();
            }
        }

        /// <summary>
        /// パヤオ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPayaoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuNm())
            {
                e.Cancel = true;
                this.txtPayaoNm.SelectAll();
            }
        }

        /// <summary>
        /// パヤオカナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPayaoKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuKanaNm())
            {
                e.Cancel = true;
                this.txtPayaoKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 手数料率の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTesuryoRitsu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTesuryoRitsu())
            {
                e.Cancel = true;
                this.txtTesuryoRitsu.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // パヤオコードの初期値を取得
            // パヤオの中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            DataTable dtMaxTanto =
                this.Dba.GetDataTableByConditionWithParams("MAX(PAYAO_CD) AS MAX_CD",
                    "TB_HN_PAYAO_TESURYO_RITSU", Util.ToString(where), dpc);
            if (dtMaxTanto.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxTanto.Rows[0]["MAX_CD"]))
            {
                this.txtPayaoCd.Text = Util.ToString(Util.ToInt(dtMaxTanto.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtPayaoCd.Text = "1";
            }

            // 手数料率の初期値を設定
            this.txtTesuryoRitsu.Text = "0.0";
            
            // パヤオコードに初期フォーカス
            this.ActiveControl = this.txtPayaoCd;
            this.txtPayaoCd.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append("    ,PAYAO_CD");
            cols.Append("    ,PAYAO_NM");
            cols.Append("    ,PAYAO_NM_KANA");
            cols.Append("    ,TESURYO_RITSU");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_PAYAO_TESURYO_RITSU");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@PAYAO_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND PAYAO_CD = @PAYAO_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtPayaoCd.Text = Util.ToString(drDispData["PAYAO_CD"]);
            this.txtPayaoNm.Text = Util.ToString(drDispData["PAYAO_NM"]);
            this.txtPayaoKanaNm.Text = Util.ToString(drDispData["PAYAO_NM_KANA"]);
            this.txtTesuryoRitsu.Text = Util.FormatNum(drDispData["TESURYO_RITSU"],1);

            // パヤオコードは入力不可
            this.txtPayaoCd.Focus();
            this.txtPayaoCd.Enabled = false;
            this.ActiveControl = this.txtPayaoCd;
            this.txtPayaoNm.Focus();
        }

        /// <summary>
        /// パヤオコードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuCd()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtPayaoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtPayaoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 既に存在するコードを入力した場合はエラーとする
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@PAYAO_CD", SqlDbType.Decimal, 6, this.txtPayaoCd.Text);
                StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND PAYAO_CD = @PAYAO_CD");
                DataTable dtCheck =
                    this.Dba.GetDataTableByConditionWithParams("PAYAO_CD",
                        "TB_HN_PAYAO_TESURYO_RITSU", Util.ToString(where), dpc);
                if (dtCheck.Rows.Count > 0)
                {
                    Msg.Error("既に存在するパヤオコードと重複しています。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// パヤオ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtPayaoNm.Text, this.txtPayaoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// パヤオカナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuKanaNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtPayaoKanaNm.Text, this.txtPayaoKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 手数料率の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTesuryoRitsu()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtTesuryoRitsu.Text))
            {
                this.txtTesuryoRitsu.Text = "0.0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsDecNumWithinLength(this.txtTesuryoRitsu.Text, 3, 1, true))
            {
                this.txtTesuryoRitsu.Text = Regex.Replace(this.txtTesuryoRitsu.Text, "^[a-zA-Z]+$", "");
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // パヤオコードのチェック
                if (!IsValidGyosyuCd())
                {
                    this.txtPayaoCd.Focus();
                    return false;
                }
            }

            // パヤオ名のチェック
            if (!IsValidGyosyuNm())
            {
                this.txtPayaoNm.Focus();
                return false;
            }

            // パヤオカナ名のチェック
            if (!IsValidGyosyuKanaNm())
            {
                this.txtPayaoKanaNm.Focus();
                return false;
            }

            // 手数料率のチェック
            if (!ValChk.IsDecNumWithinLength(this.txtTesuryoRitsu.Text, 3, 1, true))
            {
                this.txtTesuryoRitsu.Focus();
                return false;
            }

            // 手数料率のチェック
            if (Util.ToDecimal(Util.ToString(this.txtTesuryoRitsu.Text)) == 0m)
            {
                Msg.Error("入力に誤りがあります。");
                this.txtTesuryoRitsu.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_PAYAO_TESURYO_RITSUに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmTantoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードとパヤオコードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                updParam.SetParam("@PAYAO_CD", SqlDbType.Decimal, 6, this.txtPayaoCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードとパヤオコードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@PAYAO_CD", SqlDbType.Decimal, 6, this.txtPayaoCd.Text);
                alParams.Add(whereParam);
            }

            // パヤオ名
            updParam.SetParam("@PAYAO_NM", SqlDbType.VarChar, 40, this.txtPayaoNm.Text);
            // パヤオカナ名
            updParam.SetParam("@PAYAO_NM_KANA", SqlDbType.VarChar, 40, this.txtPayaoKanaNm.Text);
            // 手数料率
            updParam.SetParam("@TESURYO_RITSU", SqlDbType.Decimal, 8, Util.ToDecimal(this.txtTesuryoRitsu.Text));
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            // 処理FLG
            updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 3, 1);

            alParams.Add(updParam);

            return alParams;
        }
        #endregion
    }
}
