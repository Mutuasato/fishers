﻿namespace jp.co.fsi.kb.kbdb1021
{
    partial class KBDB1023
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.lblTerm = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.lblGenkinShiire = new System.Windows.Forms.Label();
			this.lblGenkinZei = new System.Windows.Forms.Label();
			this.lblKakeShiire = new System.Windows.Forms.Label();
			this.lblKakeZei = new System.Windows.Forms.Label();
			this.lblKeiShiire = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(4, 65);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(89, 65);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF2
			// 
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Visible = false;
			// 
			// btnF4
			// 
			this.btnF4.Visible = false;
			// 
			// btnF5
			// 
			this.btnF5.Visible = false;
			// 
			// btnF7
			// 
			this.btnF7.Visible = false;
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(175, 65);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF8
			// 
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Visible = false;
			// 
			// btnF12
			// 
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 432);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(852, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(863, 31);
			this.lblTitle.Text = "仕訳データ参照";
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(17, 39);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(813, 421);
			this.dgvList.TabIndex = 22;
			// 
			// lblTerm
			// 
			this.lblTerm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTerm.ForeColor = System.Drawing.Color.Blue;
			this.lblTerm.Location = new System.Drawing.Point(19, 17);
			this.lblTerm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTerm.Name = "lblTerm";
			this.lblTerm.Size = new System.Drawing.Size(320, 16);
			this.lblTerm.TabIndex = 903;
			this.lblTerm.Text = " 平成 21年 4月 1日 ～ 平成 22年 3月31日";
			this.lblTerm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(17, 459);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(255, 26);
			this.label1.TabIndex = 904;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "[ 合 計 ] ";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblGenkinShiire
			// 
			this.lblGenkinShiire.BackColor = System.Drawing.Color.White;
			this.lblGenkinShiire.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblGenkinShiire.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGenkinShiire.ForeColor = System.Drawing.Color.Black;
			this.lblGenkinShiire.Location = new System.Drawing.Point(272, 459);
			this.lblGenkinShiire.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGenkinShiire.Name = "lblGenkinShiire";
			this.lblGenkinShiire.Size = new System.Drawing.Size(107, 26);
			this.lblGenkinShiire.TabIndex = 905;
			this.lblGenkinShiire.Text = "-999,999,999";
			this.lblGenkinShiire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblGenkinZei
			// 
			this.lblGenkinZei.BackColor = System.Drawing.Color.White;
			this.lblGenkinZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblGenkinZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGenkinZei.ForeColor = System.Drawing.Color.Black;
			this.lblGenkinZei.Location = new System.Drawing.Point(379, 459);
			this.lblGenkinZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGenkinZei.Name = "lblGenkinZei";
			this.lblGenkinZei.Size = new System.Drawing.Size(107, 26);
			this.lblGenkinZei.TabIndex = 906;
			this.lblGenkinZei.Text = "-999,999,999";
			this.lblGenkinZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblKakeShiire
			// 
			this.lblKakeShiire.BackColor = System.Drawing.Color.White;
			this.lblKakeShiire.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKakeShiire.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKakeShiire.ForeColor = System.Drawing.Color.Black;
			this.lblKakeShiire.Location = new System.Drawing.Point(485, 459);
			this.lblKakeShiire.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKakeShiire.Name = "lblKakeShiire";
			this.lblKakeShiire.Size = new System.Drawing.Size(107, 26);
			this.lblKakeShiire.TabIndex = 907;
			this.lblKakeShiire.Text = "-999,999,999";
			this.lblKakeShiire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblKakeZei
			// 
			this.lblKakeZei.BackColor = System.Drawing.Color.White;
			this.lblKakeZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKakeZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKakeZei.ForeColor = System.Drawing.Color.Black;
			this.lblKakeZei.Location = new System.Drawing.Point(592, 459);
			this.lblKakeZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKakeZei.Name = "lblKakeZei";
			this.lblKakeZei.Size = new System.Drawing.Size(107, 26);
			this.lblKakeZei.TabIndex = 908;
			this.lblKakeZei.Text = "-999,999,999";
			this.lblKakeZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblKeiShiire
			// 
			this.lblKeiShiire.BackColor = System.Drawing.Color.White;
			this.lblKeiShiire.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKeiShiire.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKeiShiire.ForeColor = System.Drawing.Color.Black;
			this.lblKeiShiire.Location = new System.Drawing.Point(699, 459);
			this.lblKeiShiire.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKeiShiire.Name = "lblKeiShiire";
			this.lblKeiShiire.Size = new System.Drawing.Size(107, 26);
			this.lblKeiShiire.TabIndex = 909;
			this.lblKeiShiire.Text = "-999,999,999";
			this.lblKeiShiire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// KBDB1023
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(863, 569);
			this.Controls.Add(this.lblKeiShiire);
			this.Controls.Add(this.lblKakeZei);
			this.Controls.Add(this.lblKakeShiire);
			this.Controls.Add(this.lblGenkinZei);
			this.Controls.Add(this.lblGenkinShiire);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lblTerm);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "KBDB1023";
			this.ShowFButton = true;
			this.ShowTitle = false;
			this.Text = "仕訳データ参照";
			this.Shown += new System.EventHandler(this.KBDB1023_Shown);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.lblTerm, 0);
			this.Controls.SetChildIndex(this.label1, 0);
			this.Controls.SetChildIndex(this.lblGenkinShiire, 0);
			this.Controls.SetChildIndex(this.lblGenkinZei, 0);
			this.Controls.SetChildIndex(this.lblKakeShiire, 0);
			this.Controls.SetChildIndex(this.lblKakeZei, 0);
			this.Controls.SetChildIndex(this.lblKeiShiire, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblTerm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblGenkinShiire;
        private System.Windows.Forms.Label lblGenkinZei;
        private System.Windows.Forms.Label lblKakeShiire;
        private System.Windows.Forms.Label lblKakeZei;
        private System.Windows.Forms.Label lblKeiShiire;



    }
}