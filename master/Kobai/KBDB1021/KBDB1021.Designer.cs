﻿namespace jp.co.fsi.kb.kbdb1021
{
    partial class KBDB1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblMessage = new System.Windows.Forms.Label();
			this.rdbShimebi = new System.Windows.Forms.RadioButton();
			this.rdbGenkinKake = new System.Windows.Forms.RadioButton();
			this.rdbGenkin = new System.Windows.Forms.RadioButton();
			this.lblShimebiMemo = new System.Windows.Forms.Label();
			this.txtShimebi = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtToGengo = new System.Windows.Forms.Label();
			this.lblDpyDtFrGengo = new System.Windows.Forms.Label();
			this.lblDpyDtToDay = new System.Windows.Forms.Label();
			this.txtDpyDtToDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtToMonth = new System.Windows.Forms.Label();
			this.txtDpyDtToMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtToJpYear = new System.Windows.Forms.Label();
			this.txtDpyDtToJpYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtBetween = new System.Windows.Forms.Label();
			this.lblDpyDtFrDay = new System.Windows.Forms.Label();
			this.txtDpyDtFrDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtFrMonth = new System.Windows.Forms.Label();
			this.txtDpyDtFrMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtFrJpYear = new System.Windows.Forms.Label();
			this.txtDpyDtFrJpYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiharaiCdBetween = new System.Windows.Forms.Label();
			this.lblShiharaiNmTo = new System.Windows.Forms.Label();
			this.txtShiharaiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiharaiNmFr = new System.Windows.Forms.Label();
			this.txtShiharaiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTekiyo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTekiyoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSwkDpyDtGengo = new System.Windows.Forms.Label();
			this.lblSwkDpyDtDay = new System.Windows.Forms.Label();
			this.txtSwkDpyDtDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSwkDpyDtMonth = new System.Windows.Forms.Label();
			this.txtSwkDpyDtMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSwkDpyDtJpYear = new System.Windows.Forms.Label();
			this.txtSwkDpyDtJpYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTantoNm = new System.Windows.Forms.Label();
			this.txtTantoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblUpdMode = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
			this.label8 = new System.Windows.Forms.Label();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.label10 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.label6 = new System.Windows.Forms.Label();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.label5 = new System.Windows.Forms.Label();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.label9 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel7.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 519);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1073, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1063, 41);
			this.lblTitle.Text = "仕入仕訳データ作成";
			// 
			// lblMessage
			// 
			this.lblMessage.BackColor = System.Drawing.Color.LightCyan;
			this.lblMessage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMessage.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMessage.ForeColor = System.Drawing.Color.Blue;
			this.lblMessage.Location = new System.Drawing.Point(147, 0);
			this.lblMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(656, 33);
			this.lblMessage.TabIndex = 2;
			this.lblMessage.Text = "『会計期間第25期が選択されています』";
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// rdbShimebi
			// 
			this.rdbShimebi.AutoSize = true;
			this.rdbShimebi.BackColor = System.Drawing.Color.Silver;
			this.rdbShimebi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdbShimebi.Location = new System.Drawing.Point(372, 6);
			this.rdbShimebi.Margin = new System.Windows.Forms.Padding(4);
			this.rdbShimebi.Name = "rdbShimebi";
			this.rdbShimebi.Size = new System.Drawing.Size(90, 20);
			this.rdbShimebi.TabIndex = 2;
			this.rdbShimebi.TabStop = true;
			this.rdbShimebi.Tag = "CHANGE";
			this.rdbShimebi.Text = "締日基準";
			this.rdbShimebi.UseVisualStyleBackColor = false;
			this.rdbShimebi.CheckedChanged += new System.EventHandler(this.rdbShimebi_CheckedChanged);
			// 
			// rdbGenkinKake
			// 
			this.rdbGenkinKake.AutoSize = true;
			this.rdbGenkinKake.BackColor = System.Drawing.Color.Silver;
			this.rdbGenkinKake.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdbGenkinKake.Location = new System.Drawing.Point(246, 6);
			this.rdbGenkinKake.Margin = new System.Windows.Forms.Padding(4);
			this.rdbGenkinKake.Name = "rdbGenkinKake";
			this.rdbGenkinKake.Size = new System.Drawing.Size(122, 20);
			this.rdbGenkinKake.TabIndex = 1;
			this.rdbGenkinKake.TabStop = true;
			this.rdbGenkinKake.Tag = "CHANGE";
			this.rdbGenkinKake.Text = "現金、掛取引";
			this.rdbGenkinKake.UseVisualStyleBackColor = false;
			// 
			// rdbGenkin
			// 
			this.rdbGenkin.AutoSize = true;
			this.rdbGenkin.BackColor = System.Drawing.Color.Silver;
			this.rdbGenkin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.rdbGenkin.Location = new System.Drawing.Point(147, 6);
			this.rdbGenkin.Margin = new System.Windows.Forms.Padding(4);
			this.rdbGenkin.Name = "rdbGenkin";
			this.rdbGenkin.Size = new System.Drawing.Size(90, 20);
			this.rdbGenkin.TabIndex = 0;
			this.rdbGenkin.TabStop = true;
			this.rdbGenkin.Tag = "CHANGE";
			this.rdbGenkin.Text = "現金取引";
			this.rdbGenkin.UseVisualStyleBackColor = false;
			// 
			// lblShimebiMemo
			// 
			this.lblShimebiMemo.BackColor = System.Drawing.Color.LightCyan;
			this.lblShimebiMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShimebiMemo.Location = new System.Drawing.Point(543, 4);
			this.lblShimebiMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShimebiMemo.Name = "lblShimebiMemo";
			this.lblShimebiMemo.Size = new System.Drawing.Size(107, 24);
			this.lblShimebiMemo.TabIndex = 1;
			this.lblShimebiMemo.Tag = "CHANGE";
			this.lblShimebiMemo.Text = "99：末締め";
			this.lblShimebiMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShimebi
			// 
			this.txtShimebi.AutoSizeFromLength = true;
			this.txtShimebi.DisplayLength = null;
			this.txtShimebi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShimebi.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShimebi.Location = new System.Drawing.Point(514, 4);
			this.txtShimebi.Margin = new System.Windows.Forms.Padding(4);
			this.txtShimebi.MaxLength = 2;
			this.txtShimebi.Name = "txtShimebi";
			this.txtShimebi.Size = new System.Drawing.Size(25, 23);
			this.txtShimebi.TabIndex = 0;
			this.txtShimebi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShimebi.Validating += new System.ComponentModel.CancelEventHandler(this.txtShimebi_Validating);
			// 
			// lblDpyDtToGengo
			// 
			this.lblDpyDtToGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDpyDtToGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDpyDtToGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDpyDtToGengo.Location = new System.Drawing.Point(400, 4);
			this.lblDpyDtToGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDpyDtToGengo.Name = "lblDpyDtToGengo";
			this.lblDpyDtToGengo.Size = new System.Drawing.Size(52, 24);
			this.lblDpyDtToGengo.TabIndex = 903;
			this.lblDpyDtToGengo.Tag = "DISPNAME";
			this.lblDpyDtToGengo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblDpyDtFrGengo
			// 
			this.lblDpyDtFrGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblDpyDtFrGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDpyDtFrGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDpyDtFrGengo.Location = new System.Drawing.Point(147, 4);
			this.lblDpyDtFrGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDpyDtFrGengo.Name = "lblDpyDtFrGengo";
			this.lblDpyDtFrGengo.Size = new System.Drawing.Size(52, 24);
			this.lblDpyDtFrGengo.TabIndex = 902;
			this.lblDpyDtFrGengo.Tag = "DISPNAME";
			this.lblDpyDtFrGengo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblDpyDtToDay
			// 
			this.lblDpyDtToDay.AutoSize = true;
			this.lblDpyDtToDay.BackColor = System.Drawing.Color.Silver;
			this.lblDpyDtToDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDpyDtToDay.Location = new System.Drawing.Point(595, 8);
			this.lblDpyDtToDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDpyDtToDay.Name = "lblDpyDtToDay";
			this.lblDpyDtToDay.Size = new System.Drawing.Size(24, 16);
			this.lblDpyDtToDay.TabIndex = 14;
			this.lblDpyDtToDay.Tag = "CHANGE";
			this.lblDpyDtToDay.Text = "日";
			this.lblDpyDtToDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDpyDtToDay
			// 
			this.txtDpyDtToDay.AutoSizeFromLength = true;
			this.txtDpyDtToDay.DisplayLength = null;
			this.txtDpyDtToDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDpyDtToDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtToDay.Location = new System.Drawing.Point(565, 4);
			this.txtDpyDtToDay.Margin = new System.Windows.Forms.Padding(4);
			this.txtDpyDtToDay.MaxLength = 2;
			this.txtDpyDtToDay.Name = "txtDpyDtToDay";
			this.txtDpyDtToDay.Size = new System.Drawing.Size(25, 23);
			this.txtDpyDtToDay.TabIndex = 6;
			this.txtDpyDtToDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtToDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtToDay_Validating);
			// 
			// lblDpyDtToMonth
			// 
			this.lblDpyDtToMonth.AutoSize = true;
			this.lblDpyDtToMonth.BackColor = System.Drawing.Color.Silver;
			this.lblDpyDtToMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDpyDtToMonth.Location = new System.Drawing.Point(540, 8);
			this.lblDpyDtToMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDpyDtToMonth.Name = "lblDpyDtToMonth";
			this.lblDpyDtToMonth.Size = new System.Drawing.Size(24, 16);
			this.lblDpyDtToMonth.TabIndex = 12;
			this.lblDpyDtToMonth.Tag = "CHANGE";
			this.lblDpyDtToMonth.Text = "月";
			this.lblDpyDtToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDpyDtToMonth
			// 
			this.txtDpyDtToMonth.AutoSizeFromLength = true;
			this.txtDpyDtToMonth.DisplayLength = null;
			this.txtDpyDtToMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDpyDtToMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtToMonth.Location = new System.Drawing.Point(511, 4);
			this.txtDpyDtToMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtDpyDtToMonth.MaxLength = 2;
			this.txtDpyDtToMonth.Name = "txtDpyDtToMonth";
			this.txtDpyDtToMonth.Size = new System.Drawing.Size(25, 23);
			this.txtDpyDtToMonth.TabIndex = 5;
			this.txtDpyDtToMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtToMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtToMonth_Validating);
			// 
			// lblDpyDtToJpYear
			// 
			this.lblDpyDtToJpYear.AutoSize = true;
			this.lblDpyDtToJpYear.BackColor = System.Drawing.Color.Silver;
			this.lblDpyDtToJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDpyDtToJpYear.Location = new System.Drawing.Point(485, 8);
			this.lblDpyDtToJpYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDpyDtToJpYear.Name = "lblDpyDtToJpYear";
			this.lblDpyDtToJpYear.Size = new System.Drawing.Size(24, 16);
			this.lblDpyDtToJpYear.TabIndex = 10;
			this.lblDpyDtToJpYear.Tag = "CHANGE";
			this.lblDpyDtToJpYear.Text = "年";
			this.lblDpyDtToJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDpyDtToJpYear
			// 
			this.txtDpyDtToJpYear.AutoSizeFromLength = true;
			this.txtDpyDtToJpYear.DisplayLength = null;
			this.txtDpyDtToJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDpyDtToJpYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtToJpYear.Location = new System.Drawing.Point(456, 4);
			this.txtDpyDtToJpYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtDpyDtToJpYear.MaxLength = 2;
			this.txtDpyDtToJpYear.Name = "txtDpyDtToJpYear";
			this.txtDpyDtToJpYear.Size = new System.Drawing.Size(25, 23);
			this.txtDpyDtToJpYear.TabIndex = 4;
			this.txtDpyDtToJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtToJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtToJpYear_Validating);
			// 
			// lblDpyDtBetween
			// 
			this.lblDpyDtBetween.AutoSize = true;
			this.lblDpyDtBetween.BackColor = System.Drawing.Color.Silver;
			this.lblDpyDtBetween.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDpyDtBetween.Location = new System.Drawing.Point(369, 8);
			this.lblDpyDtBetween.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDpyDtBetween.Name = "lblDpyDtBetween";
			this.lblDpyDtBetween.Size = new System.Drawing.Size(24, 16);
			this.lblDpyDtBetween.TabIndex = 7;
			this.lblDpyDtBetween.Tag = "CHANGE";
			this.lblDpyDtBetween.Text = "～";
			this.lblDpyDtBetween.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblDpyDtFrDay
			// 
			this.lblDpyDtFrDay.AutoSize = true;
			this.lblDpyDtFrDay.BackColor = System.Drawing.Color.Silver;
			this.lblDpyDtFrDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDpyDtFrDay.Location = new System.Drawing.Point(343, 8);
			this.lblDpyDtFrDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDpyDtFrDay.Name = "lblDpyDtFrDay";
			this.lblDpyDtFrDay.Size = new System.Drawing.Size(24, 16);
			this.lblDpyDtFrDay.TabIndex = 6;
			this.lblDpyDtFrDay.Tag = "CHANGE";
			this.lblDpyDtFrDay.Text = "日";
			this.lblDpyDtFrDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDpyDtFrDay
			// 
			this.txtDpyDtFrDay.AutoSizeFromLength = true;
			this.txtDpyDtFrDay.DisplayLength = null;
			this.txtDpyDtFrDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDpyDtFrDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtFrDay.Location = new System.Drawing.Point(314, 4);
			this.txtDpyDtFrDay.Margin = new System.Windows.Forms.Padding(4);
			this.txtDpyDtFrDay.MaxLength = 2;
			this.txtDpyDtFrDay.Name = "txtDpyDtFrDay";
			this.txtDpyDtFrDay.Size = new System.Drawing.Size(25, 23);
			this.txtDpyDtFrDay.TabIndex = 3;
			this.txtDpyDtFrDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtFrDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtFrDay_Validating);
			// 
			// lblDpyDtFrMonth
			// 
			this.lblDpyDtFrMonth.AutoSize = true;
			this.lblDpyDtFrMonth.BackColor = System.Drawing.Color.Silver;
			this.lblDpyDtFrMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDpyDtFrMonth.Location = new System.Drawing.Point(289, 8);
			this.lblDpyDtFrMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDpyDtFrMonth.Name = "lblDpyDtFrMonth";
			this.lblDpyDtFrMonth.Size = new System.Drawing.Size(24, 16);
			this.lblDpyDtFrMonth.TabIndex = 4;
			this.lblDpyDtFrMonth.Tag = "CHANGE";
			this.lblDpyDtFrMonth.Text = "月";
			this.lblDpyDtFrMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDpyDtFrMonth
			// 
			this.txtDpyDtFrMonth.AutoSizeFromLength = true;
			this.txtDpyDtFrMonth.DisplayLength = null;
			this.txtDpyDtFrMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDpyDtFrMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtFrMonth.Location = new System.Drawing.Point(259, 4);
			this.txtDpyDtFrMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtDpyDtFrMonth.MaxLength = 2;
			this.txtDpyDtFrMonth.Name = "txtDpyDtFrMonth";
			this.txtDpyDtFrMonth.Size = new System.Drawing.Size(25, 23);
			this.txtDpyDtFrMonth.TabIndex = 2;
			this.txtDpyDtFrMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtFrMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtFrMonth_Validating);
			// 
			// lblDpyDtFrJpYear
			// 
			this.lblDpyDtFrJpYear.AutoSize = true;
			this.lblDpyDtFrJpYear.BackColor = System.Drawing.Color.Silver;
			this.lblDpyDtFrJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDpyDtFrJpYear.Location = new System.Drawing.Point(234, 8);
			this.lblDpyDtFrJpYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDpyDtFrJpYear.Name = "lblDpyDtFrJpYear";
			this.lblDpyDtFrJpYear.Size = new System.Drawing.Size(24, 16);
			this.lblDpyDtFrJpYear.TabIndex = 2;
			this.lblDpyDtFrJpYear.Tag = "CHANGE";
			this.lblDpyDtFrJpYear.Text = "年";
			this.lblDpyDtFrJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDpyDtFrJpYear
			// 
			this.txtDpyDtFrJpYear.AutoSizeFromLength = true;
			this.txtDpyDtFrJpYear.DisplayLength = null;
			this.txtDpyDtFrJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDpyDtFrJpYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtFrJpYear.Location = new System.Drawing.Point(205, 4);
			this.txtDpyDtFrJpYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtDpyDtFrJpYear.MaxLength = 2;
			this.txtDpyDtFrJpYear.Name = "txtDpyDtFrJpYear";
			this.txtDpyDtFrJpYear.Size = new System.Drawing.Size(25, 23);
			this.txtDpyDtFrJpYear.TabIndex = 1;
			this.txtDpyDtFrJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtFrJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtFrJpYear_Validating);
			// 
			// lblShiharaiCdBetween
			// 
			this.lblShiharaiCdBetween.BackColor = System.Drawing.Color.Silver;
			this.lblShiharaiCdBetween.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiharaiCdBetween.Location = new System.Drawing.Point(439, 5);
			this.lblShiharaiCdBetween.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiharaiCdBetween.Name = "lblShiharaiCdBetween";
			this.lblShiharaiCdBetween.Size = new System.Drawing.Size(27, 27);
			this.lblShiharaiCdBetween.TabIndex = 2;
			this.lblShiharaiCdBetween.Tag = "CHANGE";
			this.lblShiharaiCdBetween.Text = "～";
			this.lblShiharaiCdBetween.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblShiharaiNmTo
			// 
			this.lblShiharaiNmTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblShiharaiNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShiharaiNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiharaiNmTo.Location = new System.Drawing.Point(518, 5);
			this.lblShiharaiNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiharaiNmTo.Name = "lblShiharaiNmTo";
			this.lblShiharaiNmTo.Size = new System.Drawing.Size(239, 24);
			this.lblShiharaiNmTo.TabIndex = 4;
			this.lblShiharaiNmTo.Tag = "DISPNAME";
			this.lblShiharaiNmTo.Text = "最　後";
			this.lblShiharaiNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiharaiCdTo
			// 
			this.txtShiharaiCdTo.AutoSizeFromLength = true;
			this.txtShiharaiCdTo.DisplayLength = null;
			this.txtShiharaiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiharaiCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShiharaiCdTo.Location = new System.Drawing.Point(470, 5);
			this.txtShiharaiCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiharaiCdTo.MaxLength = 4;
			this.txtShiharaiCdTo.Name = "txtShiharaiCdTo";
			this.txtShiharaiCdTo.Size = new System.Drawing.Size(44, 23);
			this.txtShiharaiCdTo.TabIndex = 8;
			this.txtShiharaiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiharaiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiCdTo_Validating);
			// 
			// lblShiharaiNmFr
			// 
			this.lblShiharaiNmFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblShiharaiNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShiharaiNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiharaiNmFr.Location = new System.Drawing.Point(195, 5);
			this.lblShiharaiNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiharaiNmFr.Name = "lblShiharaiNmFr";
			this.lblShiharaiNmFr.Size = new System.Drawing.Size(239, 24);
			this.lblShiharaiNmFr.TabIndex = 1;
			this.lblShiharaiNmFr.Tag = "DISPNAME";
			this.lblShiharaiNmFr.Text = "先　頭";
			this.lblShiharaiNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiharaiCdFr
			// 
			this.txtShiharaiCdFr.AutoSizeFromLength = true;
			this.txtShiharaiCdFr.DisplayLength = null;
			this.txtShiharaiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiharaiCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShiharaiCdFr.Location = new System.Drawing.Point(147, 5);
			this.txtShiharaiCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiharaiCdFr.MaxLength = 4;
			this.txtShiharaiCdFr.Name = "txtShiharaiCdFr";
			this.txtShiharaiCdFr.Size = new System.Drawing.Size(44, 23);
			this.txtShiharaiCdFr.TabIndex = 7;
			this.txtShiharaiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiharaiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiCdFr_Validating);
			// 
			// txtTekiyo
			// 
			this.txtTekiyo.AutoSizeFromLength = true;
			this.txtTekiyo.DisplayLength = 30;
			this.txtTekiyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTekiyo.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtTekiyo.Location = new System.Drawing.Point(195, 6);
			this.txtTekiyo.Margin = new System.Windows.Forms.Padding(4);
			this.txtTekiyo.MaxLength = 40;
			this.txtTekiyo.Name = "txtTekiyo";
			this.txtTekiyo.Size = new System.Drawing.Size(287, 23);
			this.txtTekiyo.TabIndex = 14;
			this.txtTekiyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo_Validating);
			// 
			// txtTekiyoCd
			// 
			this.txtTekiyoCd.AutoSizeFromLength = true;
			this.txtTekiyoCd.DisplayLength = null;
			this.txtTekiyoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTekiyoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTekiyoCd.Location = new System.Drawing.Point(147, 6);
			this.txtTekiyoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtTekiyoCd.MaxLength = 4;
			this.txtTekiyoCd.Name = "txtTekiyoCd";
			this.txtTekiyoCd.Size = new System.Drawing.Size(44, 23);
			this.txtTekiyoCd.TabIndex = 13;
			this.txtTekiyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTekiyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
			// 
			// lblSwkDpyDtGengo
			// 
			this.lblSwkDpyDtGengo.BackColor = System.Drawing.Color.LightCyan;
			this.lblSwkDpyDtGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSwkDpyDtGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSwkDpyDtGengo.Location = new System.Drawing.Point(393, 3);
			this.lblSwkDpyDtGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSwkDpyDtGengo.Name = "lblSwkDpyDtGengo";
			this.lblSwkDpyDtGengo.Size = new System.Drawing.Size(52, 24);
			this.lblSwkDpyDtGengo.TabIndex = 904;
			this.lblSwkDpyDtGengo.Tag = "DISPNAME";
			this.lblSwkDpyDtGengo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblSwkDpyDtDay
			// 
			this.lblSwkDpyDtDay.BackColor = System.Drawing.Color.Silver;
			this.lblSwkDpyDtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSwkDpyDtDay.Location = new System.Drawing.Point(585, 3);
			this.lblSwkDpyDtDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSwkDpyDtDay.Name = "lblSwkDpyDtDay";
			this.lblSwkDpyDtDay.Size = new System.Drawing.Size(24, 27);
			this.lblSwkDpyDtDay.TabIndex = 6;
			this.lblSwkDpyDtDay.Tag = "CHANGE";
			this.lblSwkDpyDtDay.Text = "日";
			this.lblSwkDpyDtDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSwkDpyDtDay
			// 
			this.txtSwkDpyDtDay.AutoSizeFromLength = true;
			this.txtSwkDpyDtDay.DisplayLength = null;
			this.txtSwkDpyDtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSwkDpyDtDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSwkDpyDtDay.Location = new System.Drawing.Point(558, 3);
			this.txtSwkDpyDtDay.Margin = new System.Windows.Forms.Padding(4);
			this.txtSwkDpyDtDay.MaxLength = 2;
			this.txtSwkDpyDtDay.Name = "txtSwkDpyDtDay";
			this.txtSwkDpyDtDay.Size = new System.Drawing.Size(25, 23);
			this.txtSwkDpyDtDay.TabIndex = 12;
			this.txtSwkDpyDtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSwkDpyDtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtDay_Validating);
			// 
			// lblSwkDpyDtMonth
			// 
			this.lblSwkDpyDtMonth.BackColor = System.Drawing.Color.Silver;
			this.lblSwkDpyDtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSwkDpyDtMonth.Location = new System.Drawing.Point(534, 3);
			this.lblSwkDpyDtMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSwkDpyDtMonth.Name = "lblSwkDpyDtMonth";
			this.lblSwkDpyDtMonth.Size = new System.Drawing.Size(24, 27);
			this.lblSwkDpyDtMonth.TabIndex = 4;
			this.lblSwkDpyDtMonth.Tag = "CHANGE";
			this.lblSwkDpyDtMonth.Text = "月";
			this.lblSwkDpyDtMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSwkDpyDtMonth
			// 
			this.txtSwkDpyDtMonth.AutoSizeFromLength = true;
			this.txtSwkDpyDtMonth.DisplayLength = null;
			this.txtSwkDpyDtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSwkDpyDtMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSwkDpyDtMonth.Location = new System.Drawing.Point(506, 3);
			this.txtSwkDpyDtMonth.Margin = new System.Windows.Forms.Padding(4);
			this.txtSwkDpyDtMonth.MaxLength = 2;
			this.txtSwkDpyDtMonth.Name = "txtSwkDpyDtMonth";
			this.txtSwkDpyDtMonth.Size = new System.Drawing.Size(25, 23);
			this.txtSwkDpyDtMonth.TabIndex = 11;
			this.txtSwkDpyDtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSwkDpyDtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtMonth_Validating);
			// 
			// lblSwkDpyDtJpYear
			// 
			this.lblSwkDpyDtJpYear.BackColor = System.Drawing.Color.Silver;
			this.lblSwkDpyDtJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSwkDpyDtJpYear.Location = new System.Drawing.Point(479, 3);
			this.lblSwkDpyDtJpYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSwkDpyDtJpYear.Name = "lblSwkDpyDtJpYear";
			this.lblSwkDpyDtJpYear.Size = new System.Drawing.Size(24, 27);
			this.lblSwkDpyDtJpYear.TabIndex = 2;
			this.lblSwkDpyDtJpYear.Tag = "CHANGE";
			this.lblSwkDpyDtJpYear.Text = "年";
			this.lblSwkDpyDtJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSwkDpyDtJpYear
			// 
			this.txtSwkDpyDtJpYear.AutoSizeFromLength = true;
			this.txtSwkDpyDtJpYear.DisplayLength = null;
			this.txtSwkDpyDtJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSwkDpyDtJpYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSwkDpyDtJpYear.Location = new System.Drawing.Point(450, 3);
			this.txtSwkDpyDtJpYear.Margin = new System.Windows.Forms.Padding(4);
			this.txtSwkDpyDtJpYear.MaxLength = 2;
			this.txtSwkDpyDtJpYear.Name = "txtSwkDpyDtJpYear";
			this.txtSwkDpyDtJpYear.Size = new System.Drawing.Size(25, 23);
			this.txtSwkDpyDtJpYear.TabIndex = 10;
			this.txtSwkDpyDtJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSwkDpyDtJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtJpYear_Validating);
			// 
			// lblTantoNm
			// 
			this.lblTantoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblTantoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoNm.Location = new System.Drawing.Point(195, 3);
			this.lblTantoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoNm.Name = "lblTantoNm";
			this.lblTantoNm.Size = new System.Drawing.Size(187, 24);
			this.lblTantoNm.TabIndex = 6;
			this.lblTantoNm.Tag = "DISPNAME";
			this.lblTantoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTantoCd
			// 
			this.txtTantoCd.AutoSizeFromLength = true;
			this.txtTantoCd.DisplayLength = null;
			this.txtTantoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTantoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTantoCd.Location = new System.Drawing.Point(148, 4);
			this.txtTantoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtTantoCd.MaxLength = 4;
			this.txtTantoCd.Name = "txtTantoCd";
			this.txtTantoCd.Size = new System.Drawing.Size(44, 23);
			this.txtTantoCd.TabIndex = 9;
			this.txtTantoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCd_Validating);
			// 
			// lblUpdMode
			// 
			this.lblUpdMode.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblUpdMode.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblUpdMode.ForeColor = System.Drawing.Color.Red;
			this.lblUpdMode.Location = new System.Drawing.Point(0, 0);
			this.lblUpdMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblUpdMode.Name = "lblUpdMode";
			this.lblUpdMode.Size = new System.Drawing.Size(147, 33);
			this.lblUpdMode.TabIndex = 1;
			this.lblUpdMode.Text = "【更新モード】";
			this.lblUpdMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(147, 4);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtMizuageShishoCd.MaxLength = 5;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(67, 23);
			this.txtMizuageShishoCd.TabIndex = 0;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(218, 3);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(333, 24);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.Tag = "DISPNAME";
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(803, 33);
			this.lblMizuageShisho.TabIndex = 0;
			this.lblMizuageShisho.Tag = "CHANGE";
			this.lblMizuageShisho.Text = "支所";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 54);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 7;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(811, 282);
			this.fsiTableLayoutPanel1.TabIndex = 904;
			// 
			// fsiPanel7
			// 
			this.fsiPanel7.Controls.Add(this.txtTekiyo);
			this.fsiPanel7.Controls.Add(this.txtTekiyoCd);
			this.fsiPanel7.Controls.Add(this.label8);
			this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel7.Location = new System.Drawing.Point(4, 244);
			this.fsiPanel7.Name = "fsiPanel7";
			this.fsiPanel7.Size = new System.Drawing.Size(803, 34);
			this.fsiPanel7.TabIndex = 6;
			// 
			// label8
			// 
			this.label8.BackColor = System.Drawing.Color.Silver;
			this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label8.Location = new System.Drawing.Point(0, 0);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(803, 34);
			this.label8.TabIndex = 1;
			this.label8.Tag = "CHANGE";
			this.label8.Text = "摘要";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.lblSwkDpyDtGengo);
			this.fsiPanel6.Controls.Add(this.lblSwkDpyDtDay);
			this.fsiPanel6.Controls.Add(this.lblTantoNm);
			this.fsiPanel6.Controls.Add(this.txtSwkDpyDtDay);
			this.fsiPanel6.Controls.Add(this.label10);
			this.fsiPanel6.Controls.Add(this.lblSwkDpyDtMonth);
			this.fsiPanel6.Controls.Add(this.txtTantoCd);
			this.fsiPanel6.Controls.Add(this.txtSwkDpyDtMonth);
			this.fsiPanel6.Controls.Add(this.lblSwkDpyDtJpYear);
			this.fsiPanel6.Controls.Add(this.txtSwkDpyDtJpYear);
			this.fsiPanel6.Controls.Add(this.label7);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel6.Location = new System.Drawing.Point(4, 204);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(803, 33);
			this.fsiPanel6.TabIndex = 5;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.BackColor = System.Drawing.Color.Silver;
			this.label10.Location = new System.Drawing.Point(78, 8);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(56, 16);
			this.label10.TabIndex = 906;
			this.label10.Tag = "CHANGE";
			this.label10.Text = "担当者";
			// 
			// label7
			// 
			this.label7.BackColor = System.Drawing.Color.Silver;
			this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label7.Location = new System.Drawing.Point(0, 0);
			this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(803, 33);
			this.label7.TabIndex = 1;
			this.label7.Tag = "CHANGE";
			this.label7.Text = "仕訳伝票";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.lblShiharaiCdBetween);
			this.fsiPanel5.Controls.Add(this.lblShiharaiNmTo);
			this.fsiPanel5.Controls.Add(this.txtShiharaiCdFr);
			this.fsiPanel5.Controls.Add(this.txtShiharaiCdTo);
			this.fsiPanel5.Controls.Add(this.lblShiharaiNmFr);
			this.fsiPanel5.Controls.Add(this.label6);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 164);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(803, 33);
			this.fsiPanel5.TabIndex = 4;
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.Silver;
			this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label6.Location = new System.Drawing.Point(0, 0);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(803, 33);
			this.label6.TabIndex = 1;
			this.label6.Tag = "CHANGE";
			this.label6.Text = "支払先コード範囲";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lblDpyDtToGengo);
			this.fsiPanel4.Controls.Add(this.lblDpyDtFrGengo);
			this.fsiPanel4.Controls.Add(this.lblDpyDtToDay);
			this.fsiPanel4.Controls.Add(this.txtDpyDtFrJpYear);
			this.fsiPanel4.Controls.Add(this.txtDpyDtToDay);
			this.fsiPanel4.Controls.Add(this.lblDpyDtFrJpYear);
			this.fsiPanel4.Controls.Add(this.lblDpyDtToMonth);
			this.fsiPanel4.Controls.Add(this.txtDpyDtFrMonth);
			this.fsiPanel4.Controls.Add(this.txtDpyDtToMonth);
			this.fsiPanel4.Controls.Add(this.lblDpyDtFrMonth);
			this.fsiPanel4.Controls.Add(this.lblDpyDtToJpYear);
			this.fsiPanel4.Controls.Add(this.txtDpyDtFrDay);
			this.fsiPanel4.Controls.Add(this.txtDpyDtToJpYear);
			this.fsiPanel4.Controls.Add(this.lblDpyDtFrDay);
			this.fsiPanel4.Controls.Add(this.lblDpyDtBetween);
			this.fsiPanel4.Controls.Add(this.label5);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 124);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(803, 33);
			this.fsiPanel4.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Silver;
			this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label5.Location = new System.Drawing.Point(0, 0);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(803, 33);
			this.label5.TabIndex = 1;
			this.label5.Tag = "CHANGE";
			this.label5.Text = "伝票日付範囲";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.lblShimebiMemo);
			this.fsiPanel3.Controls.Add(this.label9);
			this.fsiPanel3.Controls.Add(this.txtShimebi);
			this.fsiPanel3.Controls.Add(this.rdbShimebi);
			this.fsiPanel3.Controls.Add(this.rdbGenkinKake);
			this.fsiPanel3.Controls.Add(this.rdbGenkin);
			this.fsiPanel3.Controls.Add(this.label4);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 84);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(803, 33);
			this.fsiPanel3.TabIndex = 2;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.BackColor = System.Drawing.Color.Silver;
			this.label9.Location = new System.Drawing.Point(469, 8);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(40, 16);
			this.label9.TabIndex = 905;
			this.label9.Tag = "CHANGE";
			this.label9.Text = "締日";
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(803, 33);
			this.label4.TabIndex = 1;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "作成区分";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtMizuageShishoCd);
			this.fsiPanel2.Controls.Add(this.lblMizuageShishoNm);
			this.fsiPanel2.Controls.Add(this.lblMizuageShisho);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 44);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(803, 33);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.lblMessage);
			this.fsiPanel1.Controls.Add(this.lblUpdMode);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(803, 33);
			this.fsiPanel1.TabIndex = 0;
			// 
			// KBDB1021
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1063, 655);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "KBDB1021";
			this.Text = "仕入仕訳データ作成";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel7.ResumeLayout(false);
			this.fsiPanel7.PerformLayout();
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.RadioButton rdbGenkin;
        private System.Windows.Forms.RadioButton rdbGenkinKake;
        private System.Windows.Forms.RadioButton rdbShimebi;
        private System.Windows.Forms.Label lblShimebiMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtShimebi;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtFrJpYear;
        private System.Windows.Forms.Label lblDpyDtFrJpYear;
        private System.Windows.Forms.Label lblDpyDtFrMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtFrMonth;
        private System.Windows.Forms.Label lblDpyDtFrDay;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtFrDay;
        private System.Windows.Forms.Label lblDpyDtToDay;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtToDay;
        private System.Windows.Forms.Label lblDpyDtToMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtToMonth;
        private System.Windows.Forms.Label lblDpyDtToJpYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtToJpYear;
        private System.Windows.Forms.Label lblDpyDtBetween;
        private System.Windows.Forms.Label lblShiharaiNmFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShiharaiCdFr;
        private System.Windows.Forms.Label lblShiharaiNmTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShiharaiCdTo;
        private System.Windows.Forms.Label lblShiharaiCdBetween;
        private System.Windows.Forms.Label lblTantoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoCd;
        private System.Windows.Forms.Label lblSwkDpyDtDay;
        private jp.co.fsi.common.controls.FsiTextBox txtSwkDpyDtDay;
        private System.Windows.Forms.Label lblSwkDpyDtMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtSwkDpyDtMonth;
        private System.Windows.Forms.Label lblSwkDpyDtJpYear;
        private jp.co.fsi.common.controls.FsiTextBox txtSwkDpyDtJpYear;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyo;
        private System.Windows.Forms.Label lblUpdMode;
        private System.Windows.Forms.Label lblDpyDtFrGengo;
        private System.Windows.Forms.Label lblDpyDtToGengo;
        private System.Windows.Forms.Label lblSwkDpyDtGengo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel7;
        private System.Windows.Forms.Label label8;
        private common.FsiPanel fsiPanel6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private common.FsiPanel fsiPanel5;
        private System.Windows.Forms.Label label6;
        private common.FsiPanel fsiPanel4;
        private System.Windows.Forms.Label label5;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}