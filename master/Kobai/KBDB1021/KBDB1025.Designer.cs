﻿namespace jp.co.fsi.kb.kbdb1021
{
    partial class KBDB1025
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkGenGenHnDp = new System.Windows.Forms.CheckBox();
            this.chkGenGenToriDp = new System.Windows.Forms.CheckBox();
            this.chkGenKakeHnDp = new System.Windows.Forms.CheckBox();
            this.chkGenKakeToriDp = new System.Windows.Forms.CheckBox();
            this.chkGenKakeGenHnDp = new System.Windows.Forms.CheckBox();
            this.chkGenKakeGenToriDp = new System.Windows.Forms.CheckBox();
            this.chkGenKakeKakeHnDp = new System.Windows.Forms.CheckBox();
            this.chkGenKakeKakeToriDp = new System.Windows.Forms.CheckBox();
            this.chkSmbGenHnDp = new System.Windows.Forms.CheckBox();
            this.chkSmbGenToriDp = new System.Windows.Forms.CheckBox();
            this.chkSmbKakeHnDp = new System.Windows.Forms.CheckBox();
            this.chkSmbKakeToriDp = new System.Windows.Forms.CheckBox();
            this.rdbShiharaisakiDp = new System.Windows.Forms.RadioButton();
            this.rdbTanitsuDp = new System.Windows.Forms.RadioButton();
            this.rdbFukugoDp = new System.Windows.Forms.RadioButton();
            this.lblBumonNm = new System.Windows.Forms.Label();
            this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTekiyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTekiyoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(4, 65);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(89, 65);
            this.btnF1.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(175, 65);
            this.btnF6.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 406);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(865, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(876, 31);
            this.lblTitle.Text = "仕訳データ作成 動作設定";
            // 
            // chkGenGenHnDp
            // 
            this.chkGenGenHnDp.AutoSize = true;
            this.chkGenGenHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkGenGenHnDp.Location = new System.Drawing.Point(563, 16);
            this.chkGenGenHnDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkGenGenHnDp.Name = "chkGenGenHnDp";
            this.chkGenGenHnDp.Size = new System.Drawing.Size(123, 20);
            this.chkGenGenHnDp.TabIndex = 3;
            this.chkGenGenHnDp.Tag = "CHANGE";
            this.chkGenGenHnDp.Text = "現金返品伝票";
            this.chkGenGenHnDp.UseVisualStyleBackColor = true;
            // 
            // chkGenGenToriDp
            // 
            this.chkGenGenToriDp.AutoSize = true;
            this.chkGenGenToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkGenGenToriDp.Location = new System.Drawing.Point(409, 16);
            this.chkGenGenToriDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkGenGenToriDp.Name = "chkGenGenToriDp";
            this.chkGenGenToriDp.Size = new System.Drawing.Size(123, 20);
            this.chkGenGenToriDp.TabIndex = 2;
            this.chkGenGenToriDp.Tag = "CHANGE";
            this.chkGenGenToriDp.Text = "現金取引伝票";
            this.chkGenGenToriDp.UseVisualStyleBackColor = true;
            // 
            // chkGenKakeHnDp
            // 
            this.chkGenKakeHnDp.AutoSize = true;
            this.chkGenKakeHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkGenKakeHnDp.Location = new System.Drawing.Point(267, 16);
            this.chkGenKakeHnDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkGenKakeHnDp.Name = "chkGenKakeHnDp";
            this.chkGenKakeHnDp.Size = new System.Drawing.Size(107, 20);
            this.chkGenKakeHnDp.TabIndex = 1;
            this.chkGenKakeHnDp.Tag = "CHANGE";
            this.chkGenKakeHnDp.Text = "掛返品伝票";
            this.chkGenKakeHnDp.UseVisualStyleBackColor = true;
            // 
            // chkGenKakeToriDp
            // 
            this.chkGenKakeToriDp.AutoSize = true;
            this.chkGenKakeToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkGenKakeToriDp.Location = new System.Drawing.Point(125, 16);
            this.chkGenKakeToriDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkGenKakeToriDp.Name = "chkGenKakeToriDp";
            this.chkGenKakeToriDp.Size = new System.Drawing.Size(107, 20);
            this.chkGenKakeToriDp.TabIndex = 0;
            this.chkGenKakeToriDp.Tag = "CHANGE";
            this.chkGenKakeToriDp.Text = "掛取引伝票";
            this.chkGenKakeToriDp.UseVisualStyleBackColor = true;
            // 
            // chkGenKakeGenHnDp
            // 
            this.chkGenKakeGenHnDp.AutoSize = true;
            this.chkGenKakeGenHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkGenKakeGenHnDp.Location = new System.Drawing.Point(563, 15);
            this.chkGenKakeGenHnDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkGenKakeGenHnDp.Name = "chkGenKakeGenHnDp";
            this.chkGenKakeGenHnDp.Size = new System.Drawing.Size(123, 20);
            this.chkGenKakeGenHnDp.TabIndex = 3;
            this.chkGenKakeGenHnDp.Tag = "CHANGE";
            this.chkGenKakeGenHnDp.Text = "現金返品伝票";
            this.chkGenKakeGenHnDp.UseVisualStyleBackColor = true;
            // 
            // chkGenKakeGenToriDp
            // 
            this.chkGenKakeGenToriDp.AutoSize = true;
            this.chkGenKakeGenToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkGenKakeGenToriDp.Location = new System.Drawing.Point(409, 15);
            this.chkGenKakeGenToriDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkGenKakeGenToriDp.Name = "chkGenKakeGenToriDp";
            this.chkGenKakeGenToriDp.Size = new System.Drawing.Size(123, 20);
            this.chkGenKakeGenToriDp.TabIndex = 2;
            this.chkGenKakeGenToriDp.Tag = "CHANGE";
            this.chkGenKakeGenToriDp.Text = "現金取引伝票";
            this.chkGenKakeGenToriDp.UseVisualStyleBackColor = true;
            // 
            // chkGenKakeKakeHnDp
            // 
            this.chkGenKakeKakeHnDp.AutoSize = true;
            this.chkGenKakeKakeHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkGenKakeKakeHnDp.Location = new System.Drawing.Point(267, 15);
            this.chkGenKakeKakeHnDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkGenKakeKakeHnDp.Name = "chkGenKakeKakeHnDp";
            this.chkGenKakeKakeHnDp.Size = new System.Drawing.Size(107, 20);
            this.chkGenKakeKakeHnDp.TabIndex = 1;
            this.chkGenKakeKakeHnDp.Tag = "CHANGE";
            this.chkGenKakeKakeHnDp.Text = "掛返品伝票";
            this.chkGenKakeKakeHnDp.UseVisualStyleBackColor = true;
            // 
            // chkGenKakeKakeToriDp
            // 
            this.chkGenKakeKakeToriDp.AutoSize = true;
            this.chkGenKakeKakeToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkGenKakeKakeToriDp.Location = new System.Drawing.Point(125, 15);
            this.chkGenKakeKakeToriDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkGenKakeKakeToriDp.Name = "chkGenKakeKakeToriDp";
            this.chkGenKakeKakeToriDp.Size = new System.Drawing.Size(107, 20);
            this.chkGenKakeKakeToriDp.TabIndex = 0;
            this.chkGenKakeKakeToriDp.Tag = "CHANGE";
            this.chkGenKakeKakeToriDp.Text = "掛取引伝票";
            this.chkGenKakeKakeToriDp.UseVisualStyleBackColor = true;
            // 
            // chkSmbGenHnDp
            // 
            this.chkSmbGenHnDp.AutoSize = true;
            this.chkSmbGenHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkSmbGenHnDp.Location = new System.Drawing.Point(563, 15);
            this.chkSmbGenHnDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkSmbGenHnDp.Name = "chkSmbGenHnDp";
            this.chkSmbGenHnDp.Size = new System.Drawing.Size(123, 20);
            this.chkSmbGenHnDp.TabIndex = 3;
            this.chkSmbGenHnDp.Tag = "CHANGE";
            this.chkSmbGenHnDp.Text = "現金返品伝票";
            this.chkSmbGenHnDp.UseVisualStyleBackColor = true;
            // 
            // chkSmbGenToriDp
            // 
            this.chkSmbGenToriDp.AutoSize = true;
            this.chkSmbGenToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkSmbGenToriDp.Location = new System.Drawing.Point(409, 15);
            this.chkSmbGenToriDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkSmbGenToriDp.Name = "chkSmbGenToriDp";
            this.chkSmbGenToriDp.Size = new System.Drawing.Size(123, 20);
            this.chkSmbGenToriDp.TabIndex = 2;
            this.chkSmbGenToriDp.Tag = "CHANGE";
            this.chkSmbGenToriDp.Text = "現金取引伝票";
            this.chkSmbGenToriDp.UseVisualStyleBackColor = true;
            // 
            // chkSmbKakeHnDp
            // 
            this.chkSmbKakeHnDp.AutoSize = true;
            this.chkSmbKakeHnDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkSmbKakeHnDp.Location = new System.Drawing.Point(267, 15);
            this.chkSmbKakeHnDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkSmbKakeHnDp.Name = "chkSmbKakeHnDp";
            this.chkSmbKakeHnDp.Size = new System.Drawing.Size(107, 20);
            this.chkSmbKakeHnDp.TabIndex = 1;
            this.chkSmbKakeHnDp.Tag = "CHANGE";
            this.chkSmbKakeHnDp.Text = "掛返品伝票";
            this.chkSmbKakeHnDp.UseVisualStyleBackColor = true;
            // 
            // chkSmbKakeToriDp
            // 
            this.chkSmbKakeToriDp.AutoSize = true;
            this.chkSmbKakeToriDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkSmbKakeToriDp.Location = new System.Drawing.Point(125, 15);
            this.chkSmbKakeToriDp.Margin = new System.Windows.Forms.Padding(4);
            this.chkSmbKakeToriDp.Name = "chkSmbKakeToriDp";
            this.chkSmbKakeToriDp.Size = new System.Drawing.Size(107, 20);
            this.chkSmbKakeToriDp.TabIndex = 0;
            this.chkSmbKakeToriDp.Tag = "CHANGE";
            this.chkSmbKakeToriDp.Text = "掛取引伝票";
            this.chkSmbKakeToriDp.UseVisualStyleBackColor = true;
            // 
            // rdbShiharaisakiDp
            // 
            this.rdbShiharaisakiDp.AutoSize = true;
            this.rdbShiharaisakiDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdbShiharaisakiDp.Location = new System.Drawing.Point(493, 2);
            this.rdbShiharaisakiDp.Margin = new System.Windows.Forms.Padding(4);
            this.rdbShiharaisakiDp.Name = "rdbShiharaisakiDp";
            this.rdbShiharaisakiDp.Size = new System.Drawing.Size(266, 20);
            this.rdbShiharaisakiDp.TabIndex = 2;
            this.rdbShiharaisakiDp.TabStop = true;
            this.rdbShiharaisakiDp.Tag = "CHANGE";
            this.rdbShiharaisakiDp.Text = "支払先毎に仕訳伝票を作成する。";
            this.rdbShiharaisakiDp.UseVisualStyleBackColor = true;
            // 
            // rdbTanitsuDp
            // 
            this.rdbTanitsuDp.AutoSize = true;
            this.rdbTanitsuDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdbTanitsuDp.Location = new System.Drawing.Point(123, 30);
            this.rdbTanitsuDp.Margin = new System.Windows.Forms.Padding(4);
            this.rdbTanitsuDp.Name = "rdbTanitsuDp";
            this.rdbTanitsuDp.Size = new System.Drawing.Size(362, 20);
            this.rdbTanitsuDp.TabIndex = 1;
            this.rdbTanitsuDp.TabStop = true;
            this.rdbTanitsuDp.Tag = "CHANGE";
            this.rdbTanitsuDp.Text = "１枚の仕訳伝票で、単一仕訳伝票を作成する。";
            this.rdbTanitsuDp.UseVisualStyleBackColor = true;
            // 
            // rdbFukugoDp
            // 
            this.rdbFukugoDp.AutoSize = true;
            this.rdbFukugoDp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdbFukugoDp.Location = new System.Drawing.Point(123, 2);
            this.rdbFukugoDp.Margin = new System.Windows.Forms.Padding(4);
            this.rdbFukugoDp.Name = "rdbFukugoDp";
            this.rdbFukugoDp.Size = new System.Drawing.Size(362, 20);
            this.rdbFukugoDp.TabIndex = 0;
            this.rdbFukugoDp.TabStop = true;
            this.rdbFukugoDp.Tag = "CHANGE";
            this.rdbFukugoDp.Text = "１枚の仕訳伝票で、複合仕訳伝票を作成する。";
            this.rdbFukugoDp.UseVisualStyleBackColor = true;
            // 
            // lblBumonNm
            // 
            this.lblBumonNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonNm.Location = new System.Drawing.Point(171, 10);
            this.lblBumonNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonNm.Name = "lblBumonNm";
            this.lblBumonNm.Size = new System.Drawing.Size(288, 24);
            this.lblBumonNm.TabIndex = 1;
            this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AutoSizeFromLength = true;
            this.txtBumonCd.DisplayLength = null;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBumonCd.Location = new System.Drawing.Point(123, 11);
            this.txtBumonCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtBumonCd.MaxLength = 4;
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.Size = new System.Drawing.Size(44, 23);
            this.txtBumonCd.TabIndex = 0;
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
            // 
            // txtTekiyo
            // 
            this.txtTekiyo.AutoSizeFromLength = true;
            this.txtTekiyo.DisplayLength = null;
            this.txtTekiyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTekiyo.Location = new System.Drawing.Point(171, 11);
            this.txtTekiyo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTekiyo.MaxLength = 30;
            this.txtTekiyo.Name = "txtTekiyo";
            this.txtTekiyo.Size = new System.Drawing.Size(287, 23);
            this.txtTekiyo.TabIndex = 1;
            this.txtTekiyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo_Validating);
            // 
            // txtTekiyoCd
            // 
            this.txtTekiyoCd.AutoSizeFromLength = true;
            this.txtTekiyoCd.DisplayLength = null;
            this.txtTekiyoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTekiyoCd.Location = new System.Drawing.Point(123, 11);
            this.txtTekiyoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTekiyoCd.MaxLength = 4;
            this.txtTekiyoCd.Name = "txtTekiyoCd";
            this.txtTekiyoCd.Size = new System.Drawing.Size(44, 23);
            this.txtTekiyoCd.TabIndex = 0;
            this.txtTekiyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(11, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 6;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(855, 361);
            this.fsiTableLayoutPanel1.TabIndex = 902;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.txtTekiyo);
            this.fsiPanel6.Controls.Add(this.txtTekiyoCd);
            this.fsiPanel6.Controls.Add(this.label6);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 299);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(847, 58);
            this.fsiPanel6.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(847, 58);
            this.label6.TabIndex = 3;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "摘要";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblBumonNm);
            this.fsiPanel5.Controls.Add(this.txtBumonCd);
            this.fsiPanel5.Controls.Add(this.label5);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 240);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(847, 52);
            this.fsiPanel5.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(847, 52);
            this.label5.TabIndex = 3;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "省略時設定部門";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.rdbShiharaisakiDp);
            this.fsiPanel4.Controls.Add(this.rdbFukugoDp);
            this.fsiPanel4.Controls.Add(this.rdbTanitsuDp);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 181);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(847, 52);
            this.fsiPanel4.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(847, 52);
            this.label4.TabIndex = 3;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "仕訳伝票作成";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.chkSmbGenHnDp);
            this.fsiPanel3.Controls.Add(this.chkSmbGenToriDp);
            this.fsiPanel3.Controls.Add(this.chkSmbKakeToriDp);
            this.fsiPanel3.Controls.Add(this.chkSmbKakeHnDp);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 122);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(847, 52);
            this.fsiPanel3.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(847, 52);
            this.label3.TabIndex = 3;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "締日基準";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.chkGenKakeGenHnDp);
            this.fsiPanel2.Controls.Add(this.chkGenKakeGenToriDp);
            this.fsiPanel2.Controls.Add(this.chkGenKakeKakeToriDp);
            this.fsiPanel2.Controls.Add(this.chkGenKakeKakeHnDp);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 63);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(847, 52);
            this.fsiPanel2.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(847, 52);
            this.label2.TabIndex = 3;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "現金、掛取引";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.chkGenGenHnDp);
            this.fsiPanel1.Controls.Add(this.chkGenGenToriDp);
            this.fsiPanel1.Controls.Add(this.chkGenKakeToriDp);
            this.fsiPanel1.Controls.Add(this.chkGenKakeHnDp);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(847, 52);
            this.fsiPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(847, 52);
            this.label1.TabIndex = 2;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "現金取引";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDB1025
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 543);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBDB1025";
            this.ShowFButton = true;
            this.ShowTitle = false;
            this.Text = "仕訳データ作成 動作設定";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.CheckBox chkGenKakeToriDp;
        private System.Windows.Forms.CheckBox chkGenKakeHnDp;
        private System.Windows.Forms.CheckBox chkGenGenToriDp;
        private System.Windows.Forms.CheckBox chkGenGenHnDp;
        private System.Windows.Forms.CheckBox chkGenKakeGenHnDp;
        private System.Windows.Forms.CheckBox chkGenKakeGenToriDp;
        private System.Windows.Forms.CheckBox chkGenKakeKakeHnDp;
        private System.Windows.Forms.CheckBox chkGenKakeKakeToriDp;
        private System.Windows.Forms.CheckBox chkSmbGenHnDp;
        private System.Windows.Forms.CheckBox chkSmbGenToriDp;
        private System.Windows.Forms.CheckBox chkSmbKakeHnDp;
        private System.Windows.Forms.CheckBox chkSmbKakeToriDp;
        private System.Windows.Forms.RadioButton rdbFukugoDp;
        private System.Windows.Forms.RadioButton rdbTanitsuDp;
        private System.Windows.Forms.RadioButton rdbShiharaisakiDp;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyo;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;
        private System.Windows.Forms.Label lblBumonNm;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel6;
        private System.Windows.Forms.Label label6;
        private common.FsiPanel fsiPanel5;
        private System.Windows.Forms.Label label5;
        private common.FsiPanel fsiPanel4;
        private System.Windows.Forms.Label label4;
        private common.FsiPanel fsiPanel3;
        private System.Windows.Forms.Label label3;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label1;
    }
}