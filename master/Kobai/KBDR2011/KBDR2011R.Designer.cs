﻿namespace jp.co.fsi.kb.kbdr2011
{
    /// <summary>
    /// KBDR2011R の概要の説明です。
    /// </summary>
    partial class KBDR2011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR2011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.rptDate = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ITEM03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル0,
            this.直線1,
            this.ITEM01,
            this.ITEM02,
            this.shape1,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.rptDate,
            this.txtPage,
            this.lblPage});
            this.pageHeader.Height = 1.102805F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.2819445F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 2.402778F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "仕入日報";
            this.ラベル0.Top = 0.04583335F;
            this.ラベル0.Width = 1.771528F;
            // 
            // 直線1
            // 
            this.直線1.Height = 0F;
            this.直線1.Left = 2.245139F;
            this.直線1.LineWeight = 2F;
            this.直線1.Name = "直線1";
            this.直線1.Tag = "";
            this.直線1.Top = 0.361111F;
            this.直線1.Width = 2.008333F;
            this.直線1.X1 = 2.245139F;
            this.直線1.X2 = 4.253472F;
            this.直線1.Y1 = 0.361111F;
            this.直線1.Y2 = 0.361111F;
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.15625F;
            this.ITEM01.Left = 0.04027778F;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM01";
            this.ITEM01.Top = 0.4826389F;
            this.ITEM01.Width = 2.504211F;
            // 
            // ITEM02
            // 
            this.ITEM02.DataField = "ITEM02";
            this.ITEM02.Height = 0.15625F;
            this.ITEM02.Left = 2.677778F;
            this.ITEM02.Name = "ITEM02";
            this.ITEM02.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM02.Tag = "";
            this.ITEM02.Text = "ITEM02";
            this.ITEM02.Top = 0.4791667F;
            this.ITEM02.Width = 1.18125F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape1.Height = 0.3834646F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F);
            this.shape1.Top = 0.7169292F;
            this.shape1.Width = 7.161024F;
            // 
            // label1
            // 
            this.label1.Height = 0.1965278F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.Name = "label1";
            this.label1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 1";
            this.label1.Tag = "";
            this.label1.Text = "伝票No.";
            this.label1.Top = 0.8248032F;
            this.label1.Width = 0.5506945F;
            // 
            // label2
            // 
            this.label2.Height = 0.1965278F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.5519685F;
            this.label2.Name = "label2";
            this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 1";
            this.label2.Tag = "";
            this.label2.Text = "取引区分";
            this.label2.Top = 0.8248032F;
            this.label2.Width = 0.6696851F;
            // 
            // label3
            // 
            this.label3.Height = 0.1972222F;
            this.label3.HyperLink = null;
            this.label3.Left = 1.221654F;
            this.label3.Name = "label3";
            this.label3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "商　　品　　名";
            this.label3.Top = 0.8248032F;
            this.label3.Width = 2.471401F;
            // 
            // label4
            // 
            this.label4.Height = 0.1972222F;
            this.label4.HyperLink = null;
            this.label4.Left = 3.698032F;
            this.label4.Name = "label4";
            this.label4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 1";
            this.label4.Tag = "";
            this.label4.Text = "数　量";
            this.label4.Top = 0.8248032F;
            this.label4.Width = 0.5957186F;
            // 
            // label5
            // 
            this.label5.Height = 0.1965278F;
            this.label5.HyperLink = null;
            this.label5.Left = 4.291733F;
            this.label5.Name = "label5";
            this.label5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 1";
            this.label5.Tag = "";
            this.label5.Text = "単　価";
            this.label5.Top = 0.8248032F;
            this.label5.Width = 0.5943775F;
            // 
            // label6
            // 
            this.label6.Height = 0.1972222F;
            this.label6.HyperLink = null;
            this.label6.Left = 4.886221F;
            this.label6.Name = "label6";
            this.label6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 1";
            this.label6.Tag = "";
            this.label6.Text = "金　額";
            this.label6.Top = 0.8248032F;
            this.label6.Width = 0.7838583F;
            // 
            // label7
            // 
            this.label7.Height = 0.1965278F;
            this.label7.HyperLink = null;
            this.label7.Left = 5.670079F;
            this.label7.Name = "label7";
            this.label7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 1";
            this.label7.Tag = "";
            this.label7.Text = "消費税";
            this.label7.Top = 0.8248032F;
            this.label7.Width = 0.6480312F;
            // 
            // label8
            // 
            this.label8.Height = 0.1965278F;
            this.label8.HyperLink = null;
            this.label8.Left = 6.300001F;
            this.label8.Name = "label8";
            this.label8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 1";
            this.label8.Tag = "";
            this.label8.Text = "金額計";
            this.label8.Top = 0.8248032F;
            this.label8.Width = 0.8249979F;
            // 
            // line1
            // 
            this.line1.Height = 0.3835689F;
            this.line1.Left = 0.5519685F;
            this.line1.LineWeight = 0F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 0.7169291F;
            this.line1.Width = 0.0001147985F;
            this.line1.X1 = 0.5519685F;
            this.line1.X2 = 0.5520833F;
            this.line1.Y1 = 0.7169291F;
            this.line1.Y2 = 1.100498F;
            // 
            // line2
            // 
            this.line2.Height = 0.3787079F;
            this.line2.Left = 1.221528F;
            this.line2.LineWeight = 0F;
            this.line2.Name = "line2";
            this.line2.Tag = "";
            this.line2.Top = 0.7169291F;
            this.line2.Width = 0.0001260042F;
            this.line2.X1 = 1.221654F;
            this.line2.X2 = 1.221528F;
            this.line2.Y1 = 0.7169291F;
            this.line2.Y2 = 1.095637F;
            // 
            // line3
            // 
            this.line3.Height = 0.3858758F;
            this.line3.Left = 3.698031F;
            this.line3.LineWeight = 0F;
            this.line3.Name = "line3";
            this.line3.Tag = "";
            this.line3.Top = 0.7169292F;
            this.line3.Width = 0F;
            this.line3.X1 = 3.698031F;
            this.line3.X2 = 3.698031F;
            this.line3.Y1 = 0.7169292F;
            this.line3.Y2 = 1.102805F;
            // 
            // line4
            // 
            this.line4.Height = 0.3835689F;
            this.line4.Left = 4.291667F;
            this.line4.LineWeight = 0F;
            this.line4.Name = "line4";
            this.line4.Tag = "";
            this.line4.Top = 0.7169291F;
            this.line4.Width = 0F;
            this.line4.X1 = 4.291667F;
            this.line4.X2 = 4.291667F;
            this.line4.Y1 = 0.7169291F;
            this.line4.Y2 = 1.100498F;
            // 
            // line5
            // 
            this.line5.Height = 0.3787079F;
            this.line5.Left = 4.886183F;
            this.line5.LineWeight = 0F;
            this.line5.Name = "line5";
            this.line5.Tag = "";
            this.line5.Top = 0.7169291F;
            this.line5.Width = 3.814697E-05F;
            this.line5.X1 = 4.886221F;
            this.line5.X2 = 4.886183F;
            this.line5.Y1 = 0.7169291F;
            this.line5.Y2 = 1.095637F;
            // 
            // line6
            // 
            this.line6.Height = 0.3787079F;
            this.line6.Left = 5.670079F;
            this.line6.LineWeight = 0F;
            this.line6.Name = "line6";
            this.line6.Tag = "";
            this.line6.Top = 0.7169291F;
            this.line6.Width = 5.960464E-05F;
            this.line6.X1 = 5.670079F;
            this.line6.X2 = 5.670139F;
            this.line6.Y1 = 0.7169291F;
            this.line6.Y2 = 1.095637F;
            // 
            // line7
            // 
            this.line7.Height = 0.3787079F;
            this.line7.Left = 6.3F;
            this.line7.LineWeight = 0F;
            this.line7.Name = "line7";
            this.line7.Tag = "";
            this.line7.Top = 0.7169291F;
            this.line7.Width = 9.536743E-07F;
            this.line7.X1 = 6.300001F;
            this.line7.X2 = 6.3F;
            this.line7.Y1 = 0.7169291F;
            this.line7.Y2 = 1.095637F;
            // 
            // rptDate
            // 
            this.rptDate.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.rptDate.Height = 0.1875F;
            this.rptDate.Left = 5.281103F;
            this.rptDate.MultiLine = false;
            this.rptDate.Name = "rptDate";
            this.rptDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.rptDate.Top = 0.2395833F;
            this.rptDate.Width = 1.229314F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1874836F;
            this.txtPage.Left = 6.598032F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.2397638F;
            this.txtPage.Width = 0.3145666F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1874836F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.912599F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.2397638F;
            this.lblPage.Width = 0.1666665F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ITEM03,
            this.ITEM04,
            this.ITEM05,
            this.ITEM06,
            this.ITEM07,
            this.ITEM08,
            this.ITEM09,
            this.ITEM10,
            this.直線29,
            this.直線30,
            this.直線31,
            this.直線40,
            this.直線41,
            this.直線42,
            this.直線43,
            this.直線44,
            this.直線45,
            this.直線46});
            this.detail.Height = 0.1875F;
            this.detail.Name = "detail";
            // 
            // ITEM03
            // 
            this.ITEM03.DataField = "ITEM03";
            this.ITEM03.Height = 0.15625F;
            this.ITEM03.Left = 0.004724409F;
            this.ITEM03.Name = "ITEM03";
            this.ITEM03.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 128";
            this.ITEM03.Tag = "";
            this.ITEM03.Text = "ITEM03";
            this.ITEM03.Top = 0.02086614F;
            this.ITEM03.Width = 0.5083498F;
            // 
            // ITEM04
            // 
            this.ITEM04.DataField = "ITEM04";
            this.ITEM04.Height = 0.15625F;
            this.ITEM04.Left = 0.5783465F;
            this.ITEM04.MultiLine = false;
            this.ITEM04.Name = "ITEM04";
            this.ITEM04.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 128";
            this.ITEM04.Tag = "";
            this.ITEM04.Text = "ITEM04";
            this.ITEM04.Top = 0.02086614F;
            this.ITEM04.Width = 0.6084317F;
            // 
            // ITEM05
            // 
            this.ITEM05.DataField = "ITEM05";
            this.ITEM05.Height = 0.15625F;
            this.ITEM05.Left = 1.259843F;
            this.ITEM05.MultiLine = false;
            this.ITEM05.Name = "ITEM05";
            this.ITEM05.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 128";
            this.ITEM05.Tag = "";
            this.ITEM05.Text = "ITEM05";
            this.ITEM05.Top = 0.02086614F;
            this.ITEM05.Width = 2.408726F;
            // 
            // ITEM06
            // 
            this.ITEM06.DataField = "ITEM06";
            this.ITEM06.Height = 0.15625F;
            this.ITEM06.Left = 3.698032F;
            this.ITEM06.Name = "ITEM06";
            this.ITEM06.OutputFormat = resources.GetString("ITEM06.OutputFormat");
            this.ITEM06.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 128";
            this.ITEM06.Tag = "";
            this.ITEM06.Text = "ITEM06";
            this.ITEM06.Top = 0.02086614F;
            this.ITEM06.Width = 0.5658572F;
            // 
            // ITEM07
            // 
            this.ITEM07.DataField = "ITEM07";
            this.ITEM07.Height = 0.15625F;
            this.ITEM07.Left = 4.293701F;
            this.ITEM07.Name = "ITEM07";
            this.ITEM07.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 128";
            this.ITEM07.Tag = "";
            this.ITEM07.Text = "ITEM07";
            this.ITEM07.Top = 0.02086614F;
            this.ITEM07.Width = 0.5604653F;
            // 
            // ITEM08
            // 
            this.ITEM08.DataField = "ITEM08";
            this.ITEM08.Height = 0.15625F;
            this.ITEM08.Left = 4.899213F;
            this.ITEM08.Name = "ITEM08";
            this.ITEM08.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 128";
            this.ITEM08.Tag = "";
            this.ITEM08.Text = "ITEM08";
            this.ITEM08.Top = 0.02086614F;
            this.ITEM08.Width = 0.7436681F;
            // 
            // ITEM09
            // 
            this.ITEM09.DataField = "ITEM09";
            this.ITEM09.Height = 0.15625F;
            this.ITEM09.Left = 5.688977F;
            this.ITEM09.Name = "ITEM09";
            this.ITEM09.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 128";
            this.ITEM09.Tag = "";
            this.ITEM09.Text = "ITEM09";
            this.ITEM09.Top = 0.02086614F;
            this.ITEM09.Width = 0.5770006F;
            // 
            // ITEM10
            // 
            this.ITEM10.DataField = "ITEM10";
            this.ITEM10.Height = 0.15625F;
            this.ITEM10.Left = 6.31811F;
            this.ITEM10.Name = "ITEM10";
            this.ITEM10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 128";
            this.ITEM10.Tag = "";
            this.ITEM10.Text = "ITEM10";
            this.ITEM10.Top = 0.02086614F;
            this.ITEM10.Width = 0.8124666F;
            // 
            // 直線29
            // 
            this.直線29.Height = 0F;
            this.直線29.Left = 0.00472441F;
            this.直線29.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.直線29.LineWeight = 0F;
            this.直線29.Name = "直線29";
            this.直線29.Tag = "";
            this.直線29.Top = 0.1771654F;
            this.直線29.Width = 7.146851F;
            this.直線29.X1 = 0.00472441F;
            this.直線29.X2 = 7.151575F;
            this.直線29.Y1 = 0.1771654F;
            this.直線29.Y2 = 0.1771654F;
            // 
            // 直線30
            // 
            this.直線30.Height = 0.1979167F;
            this.直線30.Left = 0.003937008F;
            this.直線30.LineWeight = 0F;
            this.直線30.Name = "直線30";
            this.直線30.Tag = "";
            this.直線30.Top = 0F;
            this.直線30.Width = 0F;
            this.直線30.X1 = 0.003937008F;
            this.直線30.X2 = 0.003937008F;
            this.直線30.Y1 = 0F;
            this.直線30.Y2 = 0.1979167F;
            // 
            // 直線31
            // 
            this.直線31.Height = 0.1979167F;
            this.直線31.Left = 7.15625F;
            this.直線31.LineWeight = 0F;
            this.直線31.Name = "直線31";
            this.直線31.Tag = "";
            this.直線31.Top = 0F;
            this.直線31.Width = 0F;
            this.直線31.X1 = 7.15625F;
            this.直線31.X2 = 7.15625F;
            this.直線31.Y1 = 0F;
            this.直線31.Y2 = 0.1979167F;
            // 
            // 直線40
            // 
            this.直線40.Height = 0.1965278F;
            this.直線40.Left = 0.5519685F;
            this.直線40.LineWeight = 0F;
            this.直線40.Name = "直線40";
            this.直線40.Tag = "";
            this.直線40.Top = 0F;
            this.直線40.Width = 0F;
            this.直線40.X1 = 0.5519685F;
            this.直線40.X2 = 0.5519685F;
            this.直線40.Y1 = 0F;
            this.直線40.Y2 = 0.1965278F;
            // 
            // 直線41
            // 
            this.直線41.Height = 0.1965278F;
            this.直線41.Left = 1.221654F;
            this.直線41.LineWeight = 0F;
            this.直線41.Name = "直線41";
            this.直線41.Tag = "";
            this.直線41.Top = 0F;
            this.直線41.Width = 0F;
            this.直線41.X1 = 1.221654F;
            this.直線41.X2 = 1.221654F;
            this.直線41.Y1 = 0F;
            this.直線41.Y2 = 0.1965278F;
            // 
            // 直線42
            // 
            this.直線42.Height = 0.1965278F;
            this.直線42.Left = 3.698032F;
            this.直線42.LineWeight = 0F;
            this.直線42.Name = "直線42";
            this.直線42.Tag = "";
            this.直線42.Top = 0.001574803F;
            this.直線42.Width = 0F;
            this.直線42.X1 = 3.698032F;
            this.直線42.X2 = 3.698032F;
            this.直線42.Y1 = 0.001574803F;
            this.直線42.Y2 = 0.1981026F;
            // 
            // 直線43
            // 
            this.直線43.Height = 0.1965278F;
            this.直線43.Left = 4.291733F;
            this.直線43.LineWeight = 0F;
            this.直線43.Name = "直線43";
            this.直線43.Tag = "";
            this.直線43.Top = 0F;
            this.直線43.Width = 0F;
            this.直線43.X1 = 4.291733F;
            this.直線43.X2 = 4.291733F;
            this.直線43.Y1 = 0F;
            this.直線43.Y2 = 0.1965278F;
            // 
            // 直線44
            // 
            this.直線44.Height = 0.1965278F;
            this.直線44.Left = 4.886221F;
            this.直線44.LineWeight = 0F;
            this.直線44.Name = "直線44";
            this.直線44.Tag = "";
            this.直線44.Top = 0F;
            this.直線44.Width = 0F;
            this.直線44.X1 = 4.886221F;
            this.直線44.X2 = 4.886221F;
            this.直線44.Y1 = 0F;
            this.直線44.Y2 = 0.1965278F;
            // 
            // 直線45
            // 
            this.直線45.Height = 0.1965278F;
            this.直線45.Left = 5.670079F;
            this.直線45.LineWeight = 0F;
            this.直線45.Name = "直線45";
            this.直線45.Tag = "";
            this.直線45.Top = 0F;
            this.直線45.Width = 0F;
            this.直線45.X1 = 5.670079F;
            this.直線45.X2 = 5.670079F;
            this.直線45.Y1 = 0F;
            this.直線45.Y2 = 0.1965278F;
            // 
            // 直線46
            // 
            this.直線46.Height = 0.1965278F;
            this.直線46.Left = 6.300001F;
            this.直線46.LineWeight = 0F;
            this.直線46.Name = "直線46";
            this.直線46.Tag = "";
            this.直線46.Top = 0F;
            this.直線46.Width = 0F;
            this.直線46.X1 = 6.300001F;
            this.直線46.X2 = 6.300001F;
            this.直線46.Y1 = 0F;
            this.直線46.Y2 = 0.1965278F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line8,
            this.line11,
            this.line9,
            this.line10,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17});
            this.reportFooter1.Height = 0.1650317F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // line8
            // 
            this.line8.Height = 1.043081E-07F;
            this.line8.Left = 0.003937008F;
            this.line8.LineWeight = 0F;
            this.line8.Name = "line8";
            this.line8.Tag = "";
            this.line8.Top = 0.1649606F;
            this.line8.Width = 7.152362F;
            this.line8.X1 = 0.003937008F;
            this.line8.X2 = 7.156299F;
            this.line8.Y1 = 0.1649606F;
            this.line8.Y2 = 0.1649607F;
            // 
            // line11
            // 
            this.line11.Height = 0.1650317F;
            this.line11.Left = 0.5519685F;
            this.line11.LineWeight = 0F;
            this.line11.Name = "line11";
            this.line11.Tag = "";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 0.5519685F;
            this.line11.X2 = 0.5519685F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.1650317F;
            // 
            // line9
            // 
            this.line9.Height = 0.1650317F;
            this.line9.Left = 1.221653F;
            this.line9.LineWeight = 0F;
            this.line9.Name = "line9";
            this.line9.Tag = "";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 1.221653F;
            this.line9.X2 = 1.221653F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.1650317F;
            // 
            // line10
            // 
            this.line10.Height = 0.1650317F;
            this.line10.Left = 3.698032F;
            this.line10.LineWeight = 0F;
            this.line10.Name = "line10";
            this.line10.Tag = "";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 3.698032F;
            this.line10.X2 = 3.698032F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.1650317F;
            // 
            // line12
            // 
            this.line12.Height = 0.1650317F;
            this.line12.Left = 4.291732F;
            this.line12.LineWeight = 0F;
            this.line12.Name = "line12";
            this.line12.Tag = "";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 4.291732F;
            this.line12.X2 = 4.291732F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.1650317F;
            // 
            // line13
            // 
            this.line13.Height = 0.1650317F;
            this.line13.Left = 4.886221F;
            this.line13.LineWeight = 0F;
            this.line13.Name = "line13";
            this.line13.Tag = "";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 4.886221F;
            this.line13.X2 = 4.886221F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.1650317F;
            // 
            // line14
            // 
            this.line14.Height = 0.1650317F;
            this.line14.Left = 5.670079F;
            this.line14.LineWeight = 0F;
            this.line14.Name = "line14";
            this.line14.Tag = "";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 5.670079F;
            this.line14.X2 = 5.670079F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.1650317F;
            // 
            // line15
            // 
            this.line15.Height = 0.1650317F;
            this.line15.Left = 6.3F;
            this.line15.LineWeight = 0F;
            this.line15.Name = "line15";
            this.line15.Tag = "";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 6.3F;
            this.line15.X2 = 6.3F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.1650317F;
            // 
            // line16
            // 
            this.line16.Height = 0.1650317F;
            this.line16.Left = 7.1563F;
            this.line16.LineWeight = 0F;
            this.line16.Name = "line16";
            this.line16.Tag = "";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 7.1563F;
            this.line16.X2 = 7.1563F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.1650317F;
            // 
            // line17
            // 
            this.line17.Height = 0.1650317F;
            this.line17.Left = 0.003937008F;
            this.line17.LineWeight = 0F;
            this.line17.Name = "line17";
            this.line17.Tag = "";
            this.line17.Top = 0F;
            this.line17.Width = 0F;
            this.line17.X1 = 0.003937008F;
            this.line17.X2 = 0.003937008F;
            this.line17.Y1 = 0F;
            this.line17.Y2 = 0.1650317F;
            // 
            // KBDR2011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937007F;
            this.PageSettings.Margins.Left = 0.5511811F;
            this.PageSettings.Margins.Right = 0.5511811F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.170079F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM02;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo rptDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM10;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線29;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線30;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線31;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線40;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線41;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線42;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線43;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線44;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線45;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線46;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
    }
}
