﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr2011
{
    /// <summary>
    /// 仕入日報(KBDR2011)
    /// </summary>
    public partial class KBDR2011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 掛返品
        /// </summary>
        private const string KAKE_HENPIN = "12";

        /// <summary>
        /// 現金仕入
        /// </summary>
        private const string GENKIN_SHIIRE = "21";
        #endregion

        #region 変数
        /// <summary>
        /// 掛け
        /// </summary>
        private string KAKE = "1";

        /// <summary>
        /// 現金
        /// </summary>
        private string GENKIN = "2";

        /// <summary>
        /// 返品
        /// </summary>
        private string HENPIN = "2";
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDR2011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            try
            {
                KAKE = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR2011", "Setting", "KAKE");
                GENKIN = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR2011", "Setting", "GENKIN");
                HENPIN = this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDR2011", "Setting", "HENPIN");
            }
            catch (Exception)
            {
                KAKE = "1";
                GENKIN = "2";
                HENPIN = "2";
            }

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            txtDateMonth.Text = jpDate[3];
            txtDateDay.Text = jpDate[4];

            // 初期フォーカス
            txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、仕入先コード１・２に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtShiireCdFr":
                case "txtShiireCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShiireCdFr":
                    #region 仕入先CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShiireCdFr.Text = outData[0];
                                this.lblShiireCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShiireCdTo":
                    #region 仕入先CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShiireCdTo.Text = outData[0];
                                this.lblShiireCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYear":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                                    this.txtDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengo.Text,
                                        this.txtDateYear.Text,
                                        this.txtDateMonth.Text,
                                        this.txtDateDay.Text,
                                        this.Dba);
                                this.lblDateGengo.Text = arrJpDate[0];
                                this.txtDateYear.Text = arrJpDate[2];
                                this.txtDateMonth.Text = arrJpDate[3];
                                this.txtDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBDR2011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireCdFr())
            {
                e.Cancel = true;
                this.txtShiireCdFr.SelectAll();
            }
        }

        /// <summary>
        /// コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireCdTo())
            {
                e.Cancel = true;
                this.txtShiireCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                this.txtDateYear.Text = Util.ToString(IsValid.SetYear(this.txtDateYear.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonth.SelectAll();
            }
            else
            {
                this.txtDateMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonth.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDay.SelectAll();
            }
            else
            {
                this.txtDateDay.Text = Util.ToString(IsValid.SetDay(this.txtDateDay.Text));
                CheckJpDate();
                SetJpDate();
            }
        }

        /// <summary>
        /// 仕入先CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtShiireCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年月日の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
            {
                this.txtDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(請求書発行)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 仕入先コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireCdFr()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtShiireCdFr.Text);
            DataTable dtVI_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "TORIHIKISAKI_NM", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN3 = 3", dpc);

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiireCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            if (ValChk.IsEmpty(this.txtShiireCdFr.Text))
            {
                this.lblShiireCdFr.Text = "先　頭";
            }
            else
            {
                //string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiireCdFr.Text);
                //this.lblShiireCdFr.Text = name;
                this.lblShiireCdFr.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"]);
            }
            return true;
        }

        /// <summary>
        /// 仕入先コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireCdTo()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtShiireCdTo.Text);
            DataTable dtVI_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "TORIHIKISAKI_NM", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN3 = 3", dpc);

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiireCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtShiireCdTo.Text))
            {
                this.lblShiireCdTo.Text = "最　後";
            }
            else
            {
                //string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiireCdTo.Text);
                //this.lblShiireCdTo.Text = name;
                this.lblShiireCdTo.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"]);
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                this.txtDateDay.Focus();
                this.txtDateDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpDate();
            // 年月日(自)の正しい和暦への変換処理
            SetJpDate();

            // 仕入先コード(自)の入力チェック
            if (!IsValidShiireCdFr())
            {
                this.txtShiireCdFr.SelectAll();
                return false;
            }
            // 仕入先コード(至)の入力チェック
            if (!IsValidShiireCdTo())
            {
                this.txtShiireCdTo.SelectAll();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
            this.txtDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBDR2011R rpt = new KBDR2011R(dtOutput);
                    rpt.Document.Name = this.lblTitle.Text;
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        struct NumVals
        {
            public decimal kingaku;
            public decimal shohizei;
            public decimal gokei;

            public void Clear()
            {
                kingaku = 0;
                shohizei = 0;
                gokei = 0;
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {

            DbParamCollection dpc = new DbParamCollection();
            // 日付を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);
            // 日付を和暦で保持
            string[] tmpjpDate = Util.ConvJpDate(tmpDate, this.Dba);
            int i; // ループ用カウント変数
            // 取引先コード設定
            string shiiresakiCdFr;
            string shiiresakiCdTo;
            if (Util.ToDecimal(txtShiireCdFr.Text) > 0)
            {
                shiiresakiCdFr = txtShiireCdFr.Text;
            }
            else
            {
                shiiresakiCdFr = "0";
            }
            if (Util.ToDecimal(txtShiireCdTo.Text) > 0)
            {
                shiiresakiCdTo = txtShiireCdTo.Text;
            }
            else
            {
                shiiresakiCdTo = "9999";
            }
            string shishoCd = this.txtMizuageShishoCd.Text;

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 検索する日付をセット
            dpc.SetParam("@DATE", SqlDbType.VarChar, 10, tmpDate.Date.ToString("yyyy/MM/dd"));
            // 検索する仕入先コードをセット
            dpc.SetParam("@SHIIRESAKI_CD_FR", SqlDbType.VarChar, 6, shiiresakiCdFr);
            dpc.SetParam("@SHIIRESAKI_CD_TO", SqlDbType.VarChar, 6, shiiresakiCdTo);
            // 検索する支所コードをセット
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            // han.VI_取引明細(VI_HN_TORIHIKI_MEISAI)
            // の検索日付に発生しているデータを取得
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT ");
            Sql.Append("  *");
            Sql.Append(" FROM ");
            Sql.Append("  VI_HN_TORIHIKI_MEISAI");
            Sql.Append(" WHERE ");
            Sql.Append("      KAISHA_CD    = @KAISHA_CD ");
            if (shishoCd != "0")
                Sql.Append("  AND SHISHO_CD    = @SHISHO_CD ");
            Sql.Append("  AND DENPYO_KUBUN = 2 ");
            Sql.Append("  AND DENPYO_DATE  = @DATE ");
            Sql.Append("  AND SEIKYUSAKI_CD  BETWEEN @SHIIRESAKI_CD_FR AND @SHIIRESAKI_CD_TO ");
            Sql.Append("ORDER BY KAISHA_CD ASC");
            if (shishoCd != "0")
                Sql.Append(" ,SHISHO_CD ASC");
            Sql.Append(" ,DENPYO_DATE ASC");
            Sql.Append(" ,SEIKYUSAKI_CD ASC");
            Sql.Append(" ,DENPYO_KUBUN ASC");
            Sql.Append(" ,TORIHIKI_KUBUN ASC");
            Sql.Append(" ,DENPYO_BANGO ASC");
            Sql.Append(" ,GYO_BANGO ASC");
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                dpc = new DbParamCollection();

                NumVals shokei = new NumVals(); ;
                NumVals genkinShiireGokei = new NumVals(); ;
                NumVals kakeShiireGokei = new NumVals(); ;
                NumVals sokei = new NumVals(); ;
                i = 1;
                string denpyoNo = "";
                string torihikiKubun = "";

                int sgn = 1;

                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    if (!ValChk.IsEmpty(denpyoNo) && !Util.ToString(dr["DENPYO_BANGO"]).Equals(denpyoNo))
                    {
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                        // 数値にカンマをセット
                        string shokeiKin = shokei.kingaku.ToString("N0");
                        string shokeiSho = shokei.shohizei.ToString("N0");
                        string shokeiGo = shokei.gokei.ToString("N0");
                        // データを設定
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, torihikiKubun);
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, shokeiKin);
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, shokeiSho);
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, shokeiGo);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        // 小計をリセット
                        shokei.Clear();
                        i++;
                    }

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                    // 会員番号、得意先名、商品名を連結
                    //string kaiinBango = dr["KAIIN_BANGO"].ToString();
                    string kaiinBango = dr["TOKUISAKI_CD"].ToString();
                    string tokuisakiNm = dr["TOKUISAKI_NM"].ToString();
                    string shohinNm = dr["SHOHIN_NM"].ToString();
                    // 得意先名が５文字以上なら５文字にする。(バイト数に変更予定)
                    int tokuisakiLen = tokuisakiNm.Length;
                    if (tokuisakiLen >= 5)
                    {
                        tokuisakiNm = tokuisakiNm.Substring(0, 5);
                    }
                    string sumShohinNm = kaiinBango + tokuisakiNm + " " + shohinNm;
                    // 小数点以下が存在するデータの四捨五入
                    decimal baraSosu = Util.Round((decimal)dr["BARA_SOSU"],1);
                    decimal uriTanka = Util.Round((decimal)dr["URI_TANKA"], 1);
                    //decimal baikaKingaku = (decimal)dr["BAIKA_KINGAKU"];
                    decimal baikaKingaku = (decimal)dr["ZEINUKI_KINGAKU"];
                    decimal shohizei = (decimal)dr["SHOHIZEI"];
                    //if (Util.ToString(dr["TORIHIKI_KUBUN"]) == KAKE_HENPIN)
                    if (Util.ToString(dr["TORIHIKI_KUBUN2"]) == HENPIN)
                    {
                        baraSosu = baraSosu * -1;
                        baikaKingaku = baikaKingaku * -1;
                        shohizei = shohizei * -1;
                    }
                    // 数値にカンマをセット
                    string baraSosu1 = baraSosu.ToString("N1");
                    string uriTanka1 = uriTanka.ToString("N1");
                    string baikaKingaku1 = baikaKingaku.ToString("N0");
                    string shohizei1 = shohizei.ToString("N0");
                    if (Util.ToString(dr["SHOHIZEI_TENKA_HOHO"]) == "1")
                    {
                        shohizei1 = Util.FormatNum(shohizei.ToString("N0"));
                    }
                    else
                    {
                        shohizei1 = "";
                    }
                    //データを設定
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["DENPYO_BANGO"]);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["TORIHIKI_KUBUN_NM"]);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, sumShohinNm);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, baraSosu1);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, uriTanka1);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, baikaKingaku1);
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, shohizei1);


                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;

                    sgn = Util.ToString(dr["TORIHIKI_KUBUN2"]) == HENPIN ? -1 : 1;

                    //// 明細データから合計の値を足しこむ
                    //if (Util.ToString(dr["TORIHIKI_KUBUN"]) == KAKE_HENPIN)
                    //{
                    //    shokei.kingaku += Util.ToDecimal(dr["BAIKA_KINGAKU"]) * -1;
                    //    shokei.shohizei += Util.ToDecimal(dr["SHOHIZEI"]) * -1;
                    //    shokei.gokei = (shokei.kingaku + shokei.shohizei);
                    //    kakeShiireGokei.kingaku += Util.ToDecimal(dr["BAIKA_KINGAKU"]) * -1;
                    //    kakeShiireGokei.shohizei += Util.ToDecimal(dr["SHOHIZEI"]) * -1;
                    //    kakeShiireGokei.gokei = (kakeShiireGokei.kingaku + kakeShiireGokei.shohizei);
                    //    sokei.kingaku += Util.ToDecimal(dr["BAIKA_KINGAKU"]) * -1;
                    //    sokei.shohizei += Util.ToDecimal(dr["SHOHIZEI"]) * -1;
                    //    sokei.gokei = (sokei.kingaku + sokei.shohizei);
                    //}
                    //else
                    //{
                    //if (Util.ToString(dr["TORIHIKI_KUBUN"]) == GENKIN_SHIIRE)
                        if (Util.ToString(dr["TORIHIKI_KUBUN1"]) == GENKIN)
                        {
                            //genkinShiireGokei.kingaku += Util.ToDecimal(dr["BAIKA_KINGAKU"]) * sgn;
                            genkinShiireGokei.kingaku += Util.ToDecimal(dr["ZEINUKI_KINGAKU"]) * sgn;
                            genkinShiireGokei.shohizei += Util.ToDecimal(dr["SHOHIZEI"]) * sgn;
                            genkinShiireGokei.gokei = (genkinShiireGokei.kingaku + genkinShiireGokei.shohizei);
                        }
                        else
                        {
                            //kakeShiireGokei.kingaku += Util.ToDecimal(dr["BAIKA_KINGAKU"]) * sgn;
                            kakeShiireGokei.kingaku += Util.ToDecimal(dr["ZEINUKI_KINGAKU"]) * sgn;
                            kakeShiireGokei.shohizei += Util.ToDecimal(dr["SHOHIZEI"]) * sgn;
                            kakeShiireGokei.gokei = (kakeShiireGokei.kingaku + kakeShiireGokei.shohizei);
                        }
                        //shokei.kingaku += Util.ToDecimal(dr["BAIKA_KINGAKU"]) * sgn;
                        shokei.kingaku += Util.ToDecimal(dr["ZEINUKI_KINGAKU"]) * sgn;
                        shokei.shohizei += Util.ToDecimal(dr["SHOHIZEI"]) * sgn;
                        shokei.gokei = (shokei.kingaku + shokei.shohizei);
                        //sokei.kingaku += Util.ToDecimal(dr["BAIKA_KINGAKU"]) * sgn;
                        sokei.kingaku += Util.ToDecimal(dr["ZEINUKI_KINGAKU"]) * sgn;
                        sokei.shohizei += Util.ToDecimal(dr["SHOHIZEI"]) * sgn;
                        sokei.gokei = (sokei.kingaku + sokei.shohizei);
                    //}

                    // 伝票番号の保持
                    denpyoNo = dr["DENPYO_BANGO"].ToString();
                    // 取引区分名称から、表示する文字列を設定
                    //if (Util.ToString(dr["TORIHIKI_KUBUN"]) == KAKE_HENPIN)
                    if (Util.ToString(dr["TORIHIKI_KUBUN2"]) == HENPIN)
                    {
                        torihikiKubun = "　　　　　　 【  返品伝票 合 計  】";
                    }
                    else
                    {
                        torihikiKubun = "　　　　　　 【  伝 票 合 計  】";
                    }
            }

            // ループ終了後に小計を登録
            Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                // 数値にカンマをセット
                string shokeiKin2 = shokei.kingaku.ToString("N0");
                string shokeiSho2 = shokei.shohizei.ToString("N0");
                string shokeiGo2 = shokei.gokei.ToString("N0");
                // データを設定
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, torihikiKubun);
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, shokeiKin2);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, shokeiSho2);
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, shokeiGo2);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                // 小計をリセット
                shokei.Clear();
                i++;

                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(") ");

                // 空行登録 １つ
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;

                // 現金仕入合計を登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                // 数値にカンマをセット
                string genkinShiireKingaku = genkinShiireGokei.kingaku.ToString("N0");
                string genkinShiireShohizei = genkinShiireGokei.shohizei.ToString("N0");
                string genkinShiireSokei = genkinShiireGokei.gokei.ToString("N0");
                // データを設定
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "【 総合計 】　　①現金仕入合計");
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, genkinShiireKingaku);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, genkinShiireShohizei);
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, genkinShiireSokei);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;

                // 掛仕入合計を登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                // 数値にカンマをセット
                string kakeShiireKingaku = kakeShiireGokei.kingaku.ToString("N0");
                string kakeShiireShohizei = kakeShiireGokei.shohizei.ToString("N0");
                string kakeShiireSokei = kakeShiireGokei.gokei.ToString("N0");
                // データを設定
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "　　　　　　　　②掛仕入合計");
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, kakeShiireKingaku);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, kakeShiireShohizei);
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, kakeShiireSokei);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;

                // 仕入合計を登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDate[5]);
                // 数値にカンマをセット
                string soShiireKingaku = sokei.kingaku.ToString("N0");
                string soShiireShohizei = sokei.shohizei.ToString("N0");
                string soShiireSokei = sokei.gokei.ToString("N0");
                // データをセット
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "　　　　　　　　③仕入合計　(①＋②)");
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, soShiireKingaku);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, soShiireShohizei);
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, soShiireSokei);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        #endregion

        private void txtDateYear_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMizuageShishoCd_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtShiireCdFr_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDateMonth_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDateDay_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
