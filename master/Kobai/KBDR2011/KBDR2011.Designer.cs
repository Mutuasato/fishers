﻿namespace jp.co.fsi.kb.kbdr2011
{
    partial class KBDR2011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblShiireCdBet = new System.Windows.Forms.Label();
            this.lblShiireCdFr = new System.Windows.Forms.Label();
            this.lblShiireCdTo = new System.Windows.Forms.Label();
            this.txtShiireCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShiireCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.labelDateYear = new System.Windows.Forms.Label();
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.lblDateDay = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "";
            // 
            // lblShiireCdBet
            // 
            this.lblShiireCdBet.BackColor = System.Drawing.Color.Silver;
            this.lblShiireCdBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireCdBet.Location = new System.Drawing.Point(532, 7);
            this.lblShiireCdBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiireCdBet.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblShiireCdBet.Name = "lblShiireCdBet";
            this.lblShiireCdBet.Size = new System.Drawing.Size(23, 32);
            this.lblShiireCdBet.TabIndex = 2;
            this.lblShiireCdBet.Tag = "CHANGE";
            this.lblShiireCdBet.Text = "～";
            this.lblShiireCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShiireCdFr
            // 
            this.lblShiireCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblShiireCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireCdFr.Location = new System.Drawing.Point(252, 7);
            this.lblShiireCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiireCdFr.Name = "lblShiireCdFr";
            this.lblShiireCdFr.Size = new System.Drawing.Size(272, 24);
            this.lblShiireCdFr.TabIndex = 1;
            this.lblShiireCdFr.Tag = "DISPNAME";
            this.lblShiireCdFr.Text = "先　頭";
            this.lblShiireCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiireCdTo
            // 
            this.lblShiireCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblShiireCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireCdTo.Location = new System.Drawing.Point(637, 7);
            this.lblShiireCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiireCdTo.Name = "lblShiireCdTo";
            this.lblShiireCdTo.Size = new System.Drawing.Size(272, 24);
            this.lblShiireCdTo.TabIndex = 4;
            this.lblShiireCdTo.Tag = "DISPNAME";
            this.lblShiireCdTo.Text = "最　後";
            this.lblShiireCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiireCdTo
            // 
            this.txtShiireCdTo.AutoSizeFromLength = false;
            this.txtShiireCdTo.DisplayLength = null;
            this.txtShiireCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireCdTo.Location = new System.Drawing.Point(563, 8);
            this.txtShiireCdTo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtShiireCdTo.MaxLength = 4;
            this.txtShiireCdTo.Name = "txtShiireCdTo";
            this.txtShiireCdTo.Size = new System.Drawing.Size(65, 23);
            this.txtShiireCdTo.TabIndex = 3;
            this.txtShiireCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiireCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShiireCdTo_KeyDown);
            this.txtShiireCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireCdTo_Validating);
            // 
            // txtShiireCdFr
            // 
            this.txtShiireCdFr.AutoSizeFromLength = false;
            this.txtShiireCdFr.DisplayLength = null;
            this.txtShiireCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireCdFr.Location = new System.Drawing.Point(179, 8);
            this.txtShiireCdFr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtShiireCdFr.MaxLength = 4;
            this.txtShiireCdFr.Name = "txtShiireCdFr";
            this.txtShiireCdFr.Size = new System.Drawing.Size(65, 23);
            this.txtShiireCdFr.TabIndex = 0;
            this.txtShiireCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiireCdFr.TextChanged += new System.EventHandler(this.txtShiireCdFr_TextChanged);
            this.txtShiireCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireCdFr_Validating);
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.LightCyan;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.Location = new System.Drawing.Point(169, 7);
            this.lblDateGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(55, 24);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.Tag = "DISPNAME";
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = false;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonth.Location = new System.Drawing.Point(297, 8);
            this.txtDateMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(39, 23);
            this.txtDateMonth.TabIndex = 4;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.TextChanged += new System.EventHandler(this.txtDateMonth_TextChanged);
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.Location = new System.Drawing.Point(227, 8);
            this.txtDateYear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(39, 23);
            this.txtDateYear.TabIndex = 2;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.TextChanged += new System.EventHandler(this.txtDateYear_TextChanged);
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // txtDateDay
            // 
            this.txtDateDay.AutoSizeFromLength = false;
            this.txtDateDay.DisplayLength = null;
            this.txtDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDay.Location = new System.Drawing.Point(367, 8);
            this.txtDateDay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDateDay.MaxLength = 2;
            this.txtDateDay.Name = "txtDateDay";
            this.txtDateDay.Size = new System.Drawing.Size(39, 23);
            this.txtDateDay.TabIndex = 6;
            this.txtDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDay.TextChanged += new System.EventHandler(this.txtDateDay_TextChanged);
            this.txtDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDay_Validating);
            // 
            // labelDateYear
            // 
            this.labelDateYear.BackColor = System.Drawing.Color.Silver;
            this.labelDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYear.Location = new System.Drawing.Point(268, 7);
            this.labelDateYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDateYear.MinimumSize = new System.Drawing.Size(0, 32);
            this.labelDateYear.Name = "labelDateYear";
            this.labelDateYear.Size = new System.Drawing.Size(25, 32);
            this.labelDateYear.TabIndex = 3;
            this.labelDateYear.Tag = "CHANGE";
            this.labelDateYear.Text = "年";
            this.labelDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonth.Location = new System.Drawing.Point(339, 7);
            this.lblDateMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateMonth.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(24, 32);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Tag = "CHANGE";
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDay
            // 
            this.lblDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDay.Location = new System.Drawing.Point(409, 7);
            this.lblDateDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateDay.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblDateDay.Name = "lblDateDay";
            this.lblDateDay.Size = new System.Drawing.Size(27, 32);
            this.lblDateDay.TabIndex = 7;
            this.lblDateDay.Tag = "CHANGE";
            this.lblDateDay.Text = "日";
            this.lblDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(179, 7);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.TextChanged += new System.EventHandler(this.txtMizuageShishoCd_TextChanged);
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(225, 6);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 907;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.MinimumSize = new System.Drawing.Size(0, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(929, 37);
            this.label1.TabIndex = 906;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "伝票日付";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(929, 37);
            this.label2.TabIndex = 906;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "支所";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(929, 38);
            this.label3.TabIndex = 906;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "仕入先コード範囲";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.4F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(939, 140);
            this.fsiTableLayoutPanel1.TabIndex = 908;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtShiireCdFr);
            this.fsiPanel3.Controls.Add(this.txtShiireCdTo);
            this.fsiPanel3.Controls.Add(this.lblShiireCdBet);
            this.fsiPanel3.Controls.Add(this.lblShiireCdTo);
            this.fsiPanel3.Controls.Add(this.lblShiireCdFr);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 97);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(929, 38);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel2.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 51);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(929, 37);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.lblDateGengo);
            this.fsiPanel1.Controls.Add(this.txtDateMonth);
            this.fsiPanel1.Controls.Add(this.txtDateYear);
            this.fsiPanel1.Controls.Add(this.txtDateDay);
            this.fsiPanel1.Controls.Add(this.labelDateYear);
            this.fsiPanel1.Controls.Add(this.lblDateMonth);
            this.fsiPanel1.Controls.Add(this.lblDateDay);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(929, 37);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // KBDR2011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.Name = "KBDR2011";
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblShiireCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireCdFr;
        private System.Windows.Forms.Label lblShiireCdBet;
        private System.Windows.Forms.Label lblShiireCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireCdTo;
        private System.Windows.Forms.Label lblDateGengo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDay;
        private System.Windows.Forms.Label labelDateYear;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label lblDateDay;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}