﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.controls;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x02000003 RID: 3
	public partial class KBDE1012 : BasePgForm
	{
		// Token: 0x06000006 RID: 6 RVA: 0x00002085 File Offset: 0x00000285
		public KBDE1012()
		{
			this.InitializeComponent();
			base.BindGotFocusEvent();
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002654 File Offset: 0x00000854
		protected override void InitForm()
		{
			this.ShishoCode = Util.ToInt(base.UInfo.ShishoCd);
			try
			{
				this.HIN_CHUSHI_KUBUN = Util.ToInt(Util.ToString(base.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1011", "Setting", "CHUSHI_KUBUN")));
			}
			catch (Exception)
			{
				this.HIN_CHUSHI_KUBUN = 1;
			}
			this.lblTitle.Visible = false;
			string[] array = Util.ConvJpDate(DateTime.Now, base.Dba);
			this.lblGengoFr.Text = array[0];
			this.txtGengoYearFr.Text = array[2];
			this.txtMonthFr.Text = array[3];
			this.txtDayFr.Text = array[4];
			this.lblGengoTo.Text = array[0];
			this.txtGengoYearTo.Text = array[2];
			this.txtMonthTo.Text = array[3];
			this.txtDayTo.Text = array[4];
			this.getListData(true);
			foreach (object obj in this.dgvList.Columns)
			{
				((DataGridViewColumn)obj).SortMode = DataGridViewColumnSortMode.NotSortable;
			}
			//this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			//this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9f);
			this.dgvList.Columns[0].Width = 100;
			this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvList.Columns[1].Width = 100;
			this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvList.Columns[2].Width = 100;
			this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvList.Columns[3].Width = 200;
			this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
			this.dgvList.Columns[4].Width = 130;
			this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
			this.dgvList.Columns[5].Width = 130;
			this.dgvList.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvList.Columns[6].Width = 100;
			this.dgvList.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.btnEnter.Enabled = false;
			this.dgvList.Enabled = false;
			if (base.InData != null && !ValChk.IsEmpty(base.InData))
			{
				string text = (string)base.InData;
				if (text.Length == 0)
				{
					this.txtGengoYearFr.Focus();
					return;
				}
				this.setDenpyoCondition(text);
				this.btnF6.PerformClick();
				if (this.dgvList.Rows.Count > 0)
				{
					base.ActiveControl = this.dgvList;
					this.dgvList.Rows[0].Selected = true;
					this.dgvList.CurrentCell = this.dgvList[0, 0];
				}
			}

			txtGengoYearFr.Focus();
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002A18 File Offset: 0x00000C18
		// 頭がいいのか悪いのか、、、、、よくわからん。
		// C#わかる人ならハッシュコードに変換する前にロジックでわかる、、、、、
		// 可読性を悪くするために書いてるつもりだとおもうけど、、、、
		// やりきったって自慢してんだろなｗｗｗｗｗｗｗ
		// 苦労だけして結局いっぱいヒント書いて、、、、、、
		// こうはなりたくない、、、、
		// 結局ファンクションボタンの活性・非活性を行ってるのに無駄なロジックを、、、、
		// 生産性を考えていない。厄介なプログラマがいたもんだ
		// シーラ・ジャパンでこの開発した人、使えない
		// このOnMoveFocusもちゃんと検証してつかってるのか？
		// goto はご法度ですけど！！！ｗｗｗｗｗ
		// プログラムアルゴリズムもわかってないｗｗｗｗｗ
		// 最初でボタンEnable全部trueにしてその後条件に応じて
		// 不要ボタンだけEnablefalseすればいいのにｗｗｗｗｗｗ
		// やべー昭和のプログラマーか
		// こいつのせいで先頭項目にフォーカスが当たらないってしらんのかな？
		protected override void OnMoveFocus()
		{
			// 初期活性ボタン
			this.btnF1.Enabled = false;
			this.btnF4.Enabled = false;
			this.btnF6.Enabled = true;
			this.btnEnter.Enabled = false;

			switch(((TextBox)this.ActiveControl).Name)
			{
				case "txtGengoYearFr":
				case "txtGengoYearTo":
				case "txtTantoshaCdFr":
				case "txtTantoshaCdTo":
				case "txtShohinCdFr":
				case "txtShohinCdTo":
				case "txtFunanushiCdFr":
				case "txtFunanushiCdTo":
					this.btnF1.Enabled = true;
					break;
			}

			#region まねしちゃいかんロジック
			//string activeCtlNm = base.ActiveCtlNm;
			//uint num = PrivateImplementationDetails.ComputeStringHash(activeCtlNm);
			//if (num <= 687073737u)
			//{
			//	if (num <= 198257120u)
			//	{
			//		if (num != 190880578u)
			//		{
			//			if (num != 198257120u)
			//			{
			//				goto IL_12B;
			//			}
			//			if (!(activeCtlNm == "txtGengoYearFr"))
			//			{
			//				goto IL_12B;
			//			}
			//		}
			//		else if (!(activeCtlNm == "txtFunanushiCdTo"))
			//		{
			//			goto IL_12B;
			//		}
			//	}
			//	else if (num != 509617632u)
			//	{
			//		if (num != 687073737u)
			//		{
			//			goto IL_12B;
			//		}
			//		if (!(activeCtlNm == "txtGengoYearTo"))
			//		{
			//			goto IL_12B;
			//		}
			//	}
			//	else if (!(activeCtlNm == "txtTantoshaCdFr"))
			//	{
			//		goto IL_12B;
			//	}
			//}
			//else if (num <= 2989574635u)
			//{
			//	if (num != 998434249u)
			//	{
			//		if (num != 2989574635u)
			//		{
			//			goto IL_12B;
			//		}
			//		if (!(activeCtlNm == "txtShohinCdFr"))
			//		{
			//			goto IL_12B;
			//		}
			//	}
			//	else if (!(activeCtlNm == "txtTantoshaCdTo"))
			//	{
			//		goto IL_12B;
			//	}
			//}
			//else if (num != 3171862778u)
			//{
			//	if (num != 4035117827u)
			//	{
			//		goto IL_12B;
			//	}
			//	if (!(activeCtlNm == "txtFunanushiCdFr"))
			//	{
			//		goto IL_12B;
			//	}
			//}
			//else if (!(activeCtlNm == "txtShohinCdTo"))
			//{
			//	goto IL_12B;
			//}
			//this.btnF1.Enabled = true;
			//this.btnF4.Enabled = false;
			//this.btnF6.Enabled = true;
			//this.btnEnter.Enabled = false;
			//return;
			//IL_12B:
			//this.btnF1.Enabled = false;
			//this.btnF4.Enabled = false;
			//this.btnF6.Enabled = true;
			//this.btnEnter.Enabled = false;
			#endregion

		}

		// Token: 0x06000009 RID: 9 RVA: 0x000020A4 File Offset: 0x000002A4
		public override void PressEsc()
		{
			base.DialogResult = DialogResult.Cancel;
			base.PressEsc();
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002B80 File Offset: 0x00000D80
		public override void PressF1()
		{
			string activeCtlNm = base.ActiveCtlNm;
			if (this.activeContorolName != "" && this.activeContorolName != activeCtlNm && activeCtlNm == "txtGengoYearFr")
			{
				activeCtlNm = this.activeContorolName;
			}
			uint num = PrivateImplementationDetails.ComputeStringHash(activeCtlNm);
			Type type;
			if (num <= 687073737u)
			{
				if (num <= 198257120u)
				{
					if (num != 190880578u)
					{
						if (num != 198257120u)
						{
							goto IL_4C1;
						}
						if (!(activeCtlNm == "txtGengoYearFr"))
						{
							goto IL_4C1;
						}
					}
					else
					{
						if (!(activeCtlNm == "txtFunanushiCdTo"))
						{
							goto IL_4C1;
						}
						goto IL_250;
					}
				}
				else if (num != 509617632u)
				{
					if (num != 687073737u)
					{
						goto IL_4C1;
					}
					if (!(activeCtlNm == "txtGengoYearTo"))
					{
						goto IL_4C1;
					}
				}
				else
				{
					if (!(activeCtlNm == "txtTantoshaCdFr"))
					{
						goto IL_4C1;
					}
					goto IL_31C;
				}
				type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe").GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
				if (!(type != null))
				{
					goto IL_4C1;
				}
				object obj = Activator.CreateInstance(type);
				if (obj == null)
				{
					goto IL_4C1;
				}
				BasePgForm basePgForm = (BasePgForm)obj;
				if (activeCtlNm == "txtGengoYearFr")
				{
					basePgForm.InData = this.lblGengoFr.Text;
				}
				else if (activeCtlNm == "txtGengoYearTo")
				{
					basePgForm.InData = this.lblGengoTo.Text;
				}
				basePgForm.ShowDialog(this);
				if (basePgForm.DialogResult != DialogResult.OK)
				{
					goto IL_4C1;
				}
				string[] array = (string[])basePgForm.OutData;
				if (activeCtlNm == "txtGengoYearFr")
				{
					this.lblGengoFr.Text = array[1];
					this.SetJpFr();
					goto IL_4C1;
				}
				if (activeCtlNm == "txtGengoYearTo")
				{
					this.lblGengoTo.Text = array[1];
					this.SetJpTo();
					goto IL_4C1;
				}
				goto IL_4C1;
			}
			else
			{
				if (num <= 2989574635u)
				{
					if (num != 998434249u)
					{
						if (num != 2989574635u)
						{
							goto IL_4C1;
						}
						if (!(activeCtlNm == "txtShohinCdFr"))
						{
							goto IL_4C1;
						}
					}
					else
					{
						if (!(activeCtlNm == "txtTantoshaCdTo"))
						{
							goto IL_4C1;
						}
						goto IL_31C;
					}
				}
				else if (num != 3171862778u)
				{
					if (num != 4035117827u)
					{
						goto IL_4C1;
					}
					if (!(activeCtlNm == "txtFunanushiCdFr"))
					{
						goto IL_4C1;
					}
					goto IL_250;
				}
				else if (!(activeCtlNm == "txtShohinCdTo"))
				{
					goto IL_4C1;
				}
				type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe").GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
				if (!(type != null))
				{
					goto IL_4C1;
				}
				object obj2 = Activator.CreateInstance(type);
				if (obj2 == null)
				{
					goto IL_4C1;
				}
				BasePgForm basePgForm2 = (BasePgForm)obj2;
				basePgForm2.Par1 = "1";
				if (this.HIN_CHUSHI_KUBUN == 1)
				{
					basePgForm2.Par3 = this.HIN_CHUSHI_KUBUN.ToString();
				}
				basePgForm2.ShowDialog(this);
				if (basePgForm2.DialogResult != DialogResult.OK)
				{
					goto IL_4C1;
				}
				string[] array2 = (string[])basePgForm2.OutData;
				if (activeCtlNm == "txtShohinCdFr")
				{
					this.txtShohinCdFr.Text = array2[0];
					this.lblShohinNmFr.Text = array2[1];
					goto IL_4C1;
				}
				if (activeCtlNm == "txtShohinCdTo")
				{
					this.txtShohinCdTo.Text = array2[0];
					this.lblShohinNmTo.Text = array2[1];
					goto IL_4C1;
				}
				goto IL_4C1;
			}
			IL_250:
			type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe").GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
			if (!(type != null))
			{
				goto IL_4C1;
			}
			object obj3 = Activator.CreateInstance(type);
			if (obj3 == null)
			{
				goto IL_4C1;
			}
			BasePgForm basePgForm3 = (BasePgForm)obj3;
			basePgForm3.Par1 = "1";
			basePgForm3.ShowDialog(this);
			if (basePgForm3.DialogResult != DialogResult.OK)
			{
				goto IL_4C1;
			}
			string[] array3 = (string[])basePgForm3.OutData;
			if (activeCtlNm == "txtFunanushiCdFr")
			{
				this.txtFunanushiCdFr.Text = array3[0];
				this.lblFunanushiNmFr.Text = array3[1];
				goto IL_4C1;
			}
			if (activeCtlNm == "txtFunanushiCdTo")
			{
				this.txtFunanushiCdTo.Text = array3[0];
				this.lblFunanushiNmTo.Text = array3[1];
				goto IL_4C1;
			}
			goto IL_4C1;
			IL_31C:
			type = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2021.exe").GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
			if (type != null)
			{
				object obj4 = Activator.CreateInstance(type);
				if (obj4 != null)
				{
					BasePgForm basePgForm4 = (BasePgForm)obj4;
					basePgForm4.Par1 = "1";
					basePgForm4.ShowDialog(this);
					if (basePgForm4.DialogResult == DialogResult.OK)
					{
						string[] array4 = (string[])basePgForm4.OutData;
						if (activeCtlNm == "txtTantoshaCdFr")
						{
							this.txtTantoshaCdFr.Text = array4[0];
							this.lblTantoshaNmFr.Text = array4[1];
						}
						else if (activeCtlNm == "txtTantoshaCdTo")
						{
							this.txtTantoshaCdTo.Text = array4[0];
							this.lblTantoshaNmTo.Text = array4[1];
						}
					}
				}
			}
			IL_4C1:
			try
			{
				base.Controls[activeCtlNm].Focus();
			}
			catch (Exception)
			{
			}
		}

		// Token: 0x0600000B RID: 11 RVA: 0x000020B3 File Offset: 0x000002B3
		public override void PressF4()
		{
			this.getListData(true);
			this.txtGengoYearFr.SelectAll();
		}

		// Token: 0x0600000C RID: 12 RVA: 0x000020C7 File Offset: 0x000002C7
		public override void PressF6()
		{
			this.getListData(false);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000020D0 File Offset: 0x000002D0
		private void dgvList_Enter(object sender, EventArgs e)
		{
			this.btnF1.Enabled = false;
			this.btnF4.Enabled = true;
			this.btnF6.Enabled = false;
			this.btnEnter.Enabled = true;
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002102 File Offset: 0x00000302
		private void dgvList_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				this.ReturnVal();
			}
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002114 File Offset: 0x00000314
		private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex != -1)
			{
				this.ReturnVal();
			}
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00003078 File Offset: 0x00001278
		private void dgvList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			int columnIndex = e.ColumnIndex;
			if (columnIndex - 5 <= 1 && Util.ToDecimal(e.Value) < 0m)
			{
				e.CellStyle.ForeColor = Color.Red;
			}
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002125 File Offset: 0x00000325
		private void btnEnter_Click(object sender, EventArgs e)
		{
			this.ReturnVal();
		}

		// Token: 0x06000012 RID: 18 RVA: 0x000030BC File Offset: 0x000012BC
		private void txtGengoYearFr_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsYear(this.txtGengoYearFr.Text, this.txtGengoYearFr.MaxLength))
			{
				e.Cancel = true;
				this.txtGengoYearFr.SelectAll();
			}
			else
			{
				this.txtGengoYearFr.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearFr.Text));
				this.CheckJpFr();
				this.SetJpFr();
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00003138 File Offset: 0x00001338
		private void txtMonthFr_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsMonth(this.txtMonthFr.Text, this.txtMonthFr.MaxLength))
			{
				e.Cancel = true;
				this.txtMonthFr.SelectAll();
			}
			else
			{
				this.txtMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthFr.Text));
				this.CheckJpFr();
				this.SetJpFr();
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000031B4 File Offset: 0x000013B4
		private void txtDayFr_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsDay(this.txtDayFr.Text, this.txtDayFr.MaxLength))
			{
				e.Cancel = true;
				this.txtDayFr.SelectAll();
			}
			else
			{
				this.txtDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDayFr.Text));
				this.CheckJpFr();
				this.SetJpFr();
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00003230 File Offset: 0x00001430
		private void txtGengoYearTo_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsYear(this.txtGengoYearTo.Text, this.txtGengoYearTo.MaxLength))
			{
				e.Cancel = true;
				this.txtGengoYearTo.SelectAll();
			}
			else
			{
				this.txtGengoYearTo.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearTo.Text));
				this.CheckJpTo();
				this.SetJpTo();
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x06000016 RID: 22 RVA: 0x000032AC File Offset: 0x000014AC
		private void txtMonthTo_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsMonth(this.txtMonthTo.Text, this.txtMonthTo.MaxLength))
			{
				e.Cancel = true;
				this.txtMonthTo.SelectAll();
			}
			else
			{
				this.txtMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthTo.Text));
				this.CheckJpTo();
				this.SetJpTo();
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00003328 File Offset: 0x00001528
		private void txtDayTo_Validating(object sender, CancelEventArgs e)
		{
			if (!IsValid.IsDay(this.txtDayTo.Text, this.txtDayTo.MaxLength))
			{
				e.Cancel = true;
				this.txtDayTo.SelectAll();
			}
			else
			{
				this.txtDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDayTo.Text));
				this.CheckJpTo();
				this.SetJpTo();
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x06000018 RID: 24 RVA: 0x000033A4 File Offset: 0x000015A4
		private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
		{
			if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
			{
				Msg.Error("数値のみで入力してください。");
				this.txtFunanushiCdFr.SelectAll();
				e.Cancel = true;
				return;
			}
			if (!ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
			{
				this.lblFunanushiNmFr.Text = base.Dba.GetName(base.UInfo, "VI_HN_FUNANUSHI", this.ShishoCode.ToString(), this.txtFunanushiCdFr.Text);
			}
			else
			{
				this.lblFunanushiNmFr.Text = "先\u3000頭";
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00003448 File Offset: 0x00001648
		private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
		{
			if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
			{
				Msg.Error("数値のみで入力してください。");
				this.txtFunanushiCdTo.SelectAll();
				e.Cancel = true;
				return;
			}
			if (!ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
			{
				this.lblFunanushiNmTo.Text = base.Dba.GetName(base.UInfo, "VI_HN_FUNANUSHI", this.ShishoCode.ToString(), this.txtFunanushiCdTo.Text);
			}
			else
			{
				this.lblFunanushiNmTo.Text = "最\u3000後";
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x0600001A RID: 26 RVA: 0x000034EC File Offset: 0x000016EC
		private void txtTantoshaCdFr_Validating(object sender, CancelEventArgs e)
		{
			if (!ValChk.IsNumber(this.txtTantoshaCdFr.Text))
			{
				Msg.Notice("数値のみで入力してください。");
				this.txtTantoshaCdFr.SelectAll();
				e.Cancel = true;
				return;
			}
			if (!ValChk.IsEmpty(this.txtTantoshaCdFr.Text))
			{
				this.lblTantoshaNmFr.Text = base.Dba.GetName(base.UInfo, "TB_CM_TANTOSHA", this.ShishoCode.ToString(), this.txtTantoshaCdFr.Text);
			}
			else
			{
				this.lblTantoshaNmFr.Text = "先\u3000頭";
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00003590 File Offset: 0x00001790
		private void txtTantoshaCdTo_Validating(object sender, CancelEventArgs e)
		{
			if (!ValChk.IsNumber(this.txtTantoshaCdTo.Text))
			{
				Msg.Notice("数値のみで入力してください。");
				this.txtTantoshaCdTo.SelectAll();
				e.Cancel = true;
				return;
			}
			if (!ValChk.IsEmpty(this.txtTantoshaCdTo.Text))
			{
				this.lblTantoshaNmTo.Text = base.Dba.GetName(base.UInfo, "TB_CM_TANTOSHA", this.ShishoCode.ToString(), this.txtTantoshaCdTo.Text);
			}
			else
			{
				this.lblTantoshaNmTo.Text = "最\u3000後";
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00003634 File Offset: 0x00001834
		private void txtShohinCdFr_Validating(object sender, CancelEventArgs e)
		{
			if (!ValChk.IsNumber(this.txtShohinCdFr.Text))
			{
				Msg.Notice("数値のみで入力してください。");
				this.txtShohinCdFr.SelectAll();
				e.Cancel = true;
				return;
			}
			if (!ValChk.IsEmpty(this.txtShohinCdFr.Text))
			{
				DbParamCollection dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, base.UInfo.KaishaCd);
				dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Int, 6, this.ShishoCode);
				dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 15, this.txtShohinCdFr.Text);
				dbParamCollection.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 4, this.HIN_CHUSHI_KUBUN);
				string where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SHOHIN_KUBUN5 <> 1";
				if (this.HIN_CHUSHI_KUBUN == 1)
				{
					where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND ISNULL(CHUSHI_KUBUN, 0) = @CHUSHI_KUBUN AND SHOHIN_KUBUN5 <> 1";
				}
				DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "VI_HN_SHOHIN", where, dbParamCollection);
				if (dataTableByConditionWithParams.Rows.Count != 0)
				{
					this.lblShohinNmFr.Text = Util.ToString(dataTableByConditionWithParams.Rows[0]["SHOHIN_NM"]);
				}
				else
				{
					this.lblShohinNmFr.Text = "";
				}
			}
			else
			{
				this.lblShohinNmFr.Text = "先\u3000頭";
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00003780 File Offset: 0x00001980
		private void txtShohinCdTo_Validating(object sender, CancelEventArgs e)
		{
			if (!ValChk.IsNumber(this.txtShohinCdTo.Text))
			{
				Msg.Notice("数値のみで入力してください。");
				this.txtShohinCdTo.SelectAll();
				e.Cancel = true;
				return;
			}
			if (!ValChk.IsEmpty(this.txtShohinCdTo.Text))
			{
				DbParamCollection dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, base.UInfo.KaishaCd);
				dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Int, 6, this.ShishoCode);
				dbParamCollection.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 15, this.txtShohinCdTo.Text);
				dbParamCollection.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 4, this.HIN_CHUSHI_KUBUN);
				string where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SHOHIN_KUBUN5 <> 1";
				if (this.HIN_CHUSHI_KUBUN == 1)
				{
					where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND ISNULL(CHUSHI_KUBUN, 0) = @CHUSHI_KUBUN AND SHOHIN_KUBUN5 <> 1";
				}
				DataTable dataTableByConditionWithParams = base.Dba.GetDataTableByConditionWithParams("*", "VI_HN_SHOHIN", where, dbParamCollection);
				if (dataTableByConditionWithParams.Rows.Count != 0)
				{
					this.lblShohinNmTo.Text = Util.ToString(dataTableByConditionWithParams.Rows[0]["SHOHIN_NM"]);
				}
				else
				{
					this.lblShohinNmTo.Text = "";
				}
			}
			else
			{
				this.lblShohinNmTo.Text = "最\u3000後";
			}
			this.activeContorolName = base.ActiveCtlNm;
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000020C7 File Offset: 0x000002C7
		private void txtSearchCode_Validating(object sender, CancelEventArgs e)
		{
			this.getListData(false);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000038CC File Offset: 0x00001ACC
		private void CheckJpFr()
		{
			DateTime dateTime = Util.ConvAdDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text, this.txtMonthFr.Text, "1", base.Dba);
			int num = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
			if (Util.ToInt(this.txtDayFr.Text) > num)
			{
				this.txtDayFr.Text = Util.ToString(num);
			}
		}

		// Token: 0x06000020 RID: 32 RVA: 0x0000212D File Offset: 0x0000032D
		private void SetJpFr()
		{
			this.SetJpFr(Util.FixJpDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text, this.txtMonthFr.Text, this.txtDayFr.Text, base.Dba));
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00003948 File Offset: 0x00001B48
		private void CheckJpTo()
		{
			DateTime dateTime = Util.ConvAdDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text, this.txtMonthTo.Text, "1", base.Dba);
			int num = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
			if (Util.ToInt(this.txtDayTo.Text) > num)
			{
				this.txtDayTo.Text = Util.ToString(num);
			}
		}

		// Token: 0x06000022 RID: 34 RVA: 0x0000216C File Offset: 0x0000036C
		private void SetJpTo()
		{
			this.SetJpTo(Util.FixJpDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text, this.txtMonthTo.Text, this.txtDayTo.Text, base.Dba));
		}

		// Token: 0x06000023 RID: 35 RVA: 0x000021AB File Offset: 0x000003AB
		private void SetJpFr(string[] arrJpDate)
		{
			this.lblGengoFr.Text = arrJpDate[0];
			this.txtGengoYearFr.Text = arrJpDate[2];
			this.txtMonthFr.Text = arrJpDate[3];
			this.txtDayFr.Text = arrJpDate[4];
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000021E5 File Offset: 0x000003E5
		private void SetJpTo(string[] arrJpDate)
		{
			this.lblGengoTo.Text = arrJpDate[0];
			this.txtGengoYearTo.Text = arrJpDate[2];
			this.txtMonthTo.Text = arrJpDate[3];
			this.txtDayTo.Text = arrJpDate[4];
		}

		// Token: 0x06000025 RID: 37 RVA: 0x000039C4 File Offset: 0x00001BC4
		private DataTable GetTB_HN_ZIDO_SHIWAKE_RIREKI(bool isInitial)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT");
			stringBuilder.Append(" A.DENPYO_BANGO    AS 伝票番号,");
			stringBuilder.Append(" A.KAIKEI_NENDO    AS 会計年度,");
			stringBuilder.Append(" A.DENPYO_DATE     AS 伝票日付,");
			stringBuilder.Append(" A.TOKUISAKI_CD    AS 会員番号,");
			stringBuilder.Append(" A.TOKUISAKI_NM    AS 会員名称,");
			stringBuilder.Append(" A.TANTOSHA_CD     AS 担当者コード,");
			stringBuilder.Append(" D.TANTOSHA_NM     AS 担当者名,");
			stringBuilder.Append(" A.TORIHIKI_KUBUN2 AS 取引区分２ ");
			stringBuilder.Append("FROM");
			stringBuilder.Append(" TB_HN_TORIHIKI_DENPYO AS A ");
			stringBuilder.Append("LEFT OUTER JOIN");
			stringBuilder.Append(" TB_CM_TANTOSHA AS D");
			stringBuilder.Append(" ON A.KAISHA_CD = D.KAISHA_CD");
			stringBuilder.Append(" AND A.TANTOSHA_CD = D.TANTOSHA_CD ");
			stringBuilder.Append("WHERE");
			stringBuilder.Append(" A.KAISHA_CD = @KAISHA_CD");
			stringBuilder.Append(" AND A.SHISHO_CD = @SHISHO_CD");
			stringBuilder.Append(" AND A.DENPYO_KUBUN = 1");
			stringBuilder.Append(" AND A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO");
			stringBuilder.Append(" AND A.TOKUISAKI_CD BETWEEN @KAIIN_BANGO_FR AND @KAIIN_BANGO_TO");
			stringBuilder.Append(" AND A.TANTOSHA_CD BETWEEN @TANTOSHA_CD_FR AND @TANTOSHA_CD_TO");
			stringBuilder.Append(" AND A.DENPYO_BANGO IN (");
			stringBuilder.Append("SELECT");
			stringBuilder.Append(" DENPYO_BANGO ");
			stringBuilder.Append("FROM");
			stringBuilder.Append(" TB_HN_TORIHIKI_MEISAI ");
			stringBuilder.Append("WHERE");
			stringBuilder.Append(" KAISHA_CD = A.KAISHA_CD");
			stringBuilder.Append(" AND SHISHO_CD = A.SHISHO_CD");
			stringBuilder.Append(" AND DENPYO_KUBUN = A.DENPYO_KUBUN");
			stringBuilder.Append(" AND KAIKEI_NENDO = A.KAIKEI_NENDO");
			stringBuilder.Append(" AND (SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO)");
			stringBuilder.Append(")");
			if (!ValChk.IsEmpty(this.txtSearchCode.Text))
			{
				stringBuilder.AppendFormat(" AND A.SHOHYO_BANGO = '{0}'", this.txtSearchCode.Text);
			}
			if (isInitial)
			{
				stringBuilder.Append(" AND 1 = 0");
			}
			stringBuilder.Append(" ORDER BY");
			stringBuilder.Append(" A.KAISHA_CD ASC,");
			stringBuilder.Append(" A.DENPYO_DATE ASC,");
			stringBuilder.Append(" A.DENPYO_BANGO ASC ");
			DateTime dateTime = Util.ConvAdDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text, this.txtMonthFr.Text, this.txtDayFr.Text, base.Dba);
			DateTime dateTime2 = Util.ConvAdDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text, this.txtMonthTo.Text, this.txtDayTo.Text, base.Dba);
			string val;
			if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
			{
				val = "0";
			}
			else
			{
				val = this.txtFunanushiCdFr.Text;
			}
			string val2;
			if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
			{
				val2 = "9999";
			}
			else
			{
				val2 = this.txtFunanushiCdTo.Text;
			}
			decimal num;
			if (ValChk.IsEmpty(this.txtTantoshaCdFr.Text))
			{
				num = 0m;
			}
			else
			{
				num = Util.ToDecimal(this.txtTantoshaCdFr.Text);
			}
			decimal num2;
			if (ValChk.IsEmpty(this.txtTantoshaCdTo.Text))
			{
				num2 = 999999m;
			}
			else
			{
				num2 = Util.ToDecimal(this.txtTantoshaCdTo.Text);
			}
			decimal num3;
			if (ValChk.IsEmpty(this.txtShohinCdFr.Text))
			{
				num3 = 0m;
			}
			else
			{
				num3 = Util.ToDecimal(this.txtShohinCdFr.Text);
			}
			decimal num4;
			if (ValChk.IsEmpty(this.txtShohinCdTo.Text))
			{
				num4 = 999999999999m;
			}
			else
			{
				num4 = Util.ToDecimal(this.txtShohinCdTo.Text);
			}
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, dateTime);
			dbParamCollection.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, dateTime2);
			dbParamCollection.SetParam("@KAIIN_BANGO_FR", SqlDbType.VarChar, 4, val);
			dbParamCollection.SetParam("@KAIIN_BANGO_TO", SqlDbType.VarChar, 4, val2);
			dbParamCollection.SetParam("@TANTOSHA_CD_FR", SqlDbType.Decimal, 6, num);
			dbParamCollection.SetParam("@TANTOSHA_CD_TO", SqlDbType.Decimal, 6, num2);
			dbParamCollection.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 15, num3);
			dbParamCollection.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 15, num4);
			return base.Dba.GetDataTableFromSqlWithParams(Util.ToString(stringBuilder), dbParamCollection);
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00003E50 File Offset: 0x00002050
		private DataTable EditDataList(DataTable dtData)
		{
			DataTable dataTable = new DataTable();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT");
			stringBuilder.Append(" SUM(CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3");
			stringBuilder.Append("  THEN (A.BAIKA_KINGAKU - A.SHOHIZEI)");
			stringBuilder.Append("  ELSE A.BAIKA_KINGAKU");
			stringBuilder.Append("  END) AS 金額合計,");
			stringBuilder.Append(" SUM(A.SHOHIZEI) AS 消費税合計 ");
			stringBuilder.Append("FROM");
			stringBuilder.Append(" TB_HN_TORIHIKI_MEISAI AS A ");
			stringBuilder.Append("WHERE");
			stringBuilder.Append(" A.KAISHA_CD = @KAISHA_CD");
			stringBuilder.Append(" AND A.SHISHO_CD = @SHISHO_CD");
			stringBuilder.Append(" AND A.DENPYO_KUBUN = 1");
			stringBuilder.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");
			stringBuilder.Append(" AND A.DENPYO_BANGO = @DENPYO_BANGO");
			dataTable.Columns.Add("伝票日付", typeof(string));
			dataTable.Columns.Add("伝票番号", typeof(int));
			dataTable.Columns.Add("船主ｺｰﾄﾞ", typeof(int));
			dataTable.Columns.Add("船主名", typeof(string));
			dataTable.Columns.Add("担当者", typeof(string));
			dataTable.Columns.Add("伝票金額", typeof(string));
			dataTable.Columns.Add("（消費税）", typeof(string));
			for (int i = 0; i < dtData.Rows.Count; i++)
			{
				DataRow dataRow = dataTable.NewRow();
				string[] array = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["伝票日付"]), base.Dba);
				string value = string.Concat(new string[]
				{
					Util.ToInt(array[2]).ToString("00"),
					"/",
					array[3].PadLeft(2, ' '),
					"/",
					array[4].PadLeft(2, ' ')
				});
				dataRow["伝票日付"] = value;
				dataRow["伝票番号"] = dtData.Rows[i]["伝票番号"];
				dataRow["船主ｺｰﾄﾞ"] = dtData.Rows[i]["会員番号"];
				dataRow["船主名"] = dtData.Rows[i]["会員名称"];
				dataRow["担当者"] = dtData.Rows[i]["担当者名"];
				DbParamCollection dbParamCollection = new DbParamCollection();
				dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, base.UInfo.KaishaCd);
				dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
				dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, dtData.Rows[i]["会計年度"]);
				dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, dtData.Rows[i]["伝票番号"]);
				DataTable dataTableFromSqlWithParams = base.Dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
				if (Util.ToDecimal(dtData.Rows[i]["取引区分２"]) == 2m)
				{
					dataRow["伝票金額"] = Util.FormatNum((Util.ToDecimal(dataTableFromSqlWithParams.Rows[0]["金額合計"]) + Util.ToDecimal(dataTableFromSqlWithParams.Rows[0]["消費税合計"])) * -1m);
					dataRow["（消費税）"] = Util.FormatNum(Util.ToDecimal(dataTableFromSqlWithParams.Rows[0]["消費税合計"]) * -1m);
				}
				else
				{
					dataRow["伝票金額"] = Util.FormatNum(Util.ToDecimal(dataTableFromSqlWithParams.Rows[0]["金額合計"]) + Util.ToDecimal(dataTableFromSqlWithParams.Rows[0]["消費税合計"]));
					dataRow["（消費税）"] = Util.FormatNum(dataTableFromSqlWithParams.Rows[0]["消費税合計"]);
				}
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		// Token: 0x06000027 RID: 39 RVA: 0x000042E8 File Offset: 0x000024E8
		private void getListData(bool isInitial)
		{
			DataTable dataTable = new DataTable();
			try
			{
				dataTable = this.GetTB_HN_ZIDO_SHIWAKE_RIREKI(isInitial);
				if (!isInitial && dataTable.Rows.Count == 0)
				{
					this.btnEnter.Enabled = false;
					this.dgvList.Enabled = false;
					Msg.Info("該当データはありません。");
					return;
				}
			}
			catch (Exception ex)
			{
				Msg.Error(ex.Message);
				return;
			}
			this.btnEnter.Enabled = true;
			this.dgvList.Enabled = true;
			DataTable dataSource = this.EditDataList(dataTable);
			this.dgvList.DataSource = dataSource;
			if (!isInitial)
			{
				this.dgvList.Focus();
			}
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00004394 File Offset: 0x00002594
		private void ReturnVal()
		{
			base.OutData = new string[]
			{
				Util.ToString(this.dgvList.SelectedRows[0].Cells["伝票番号"].Value),
				this.getDenpyoCondition()
			};
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000043F0 File Offset: 0x000025F0
		private void setDenpyoCondition(string denpyoCondition)
		{
			string[] array = denpyoCondition.Split(new char[]
			{
				','
			});
			if (array.Length != 0)
			{
				try
				{
					string[] array2 = Util.ConvJpDate(Util.ToDate(array[0]), base.Dba);
					this.SetJpFr(array2);
					array2 = Util.ConvJpDate(Util.ToDate(array[1]), base.Dba);
					this.SetJpTo(array2);
					this.txtFunanushiCdFr.Text = array[2];
					this.lblFunanushiNmFr.Text = array[3];
					this.txtFunanushiCdTo.Text = array[4];
					this.lblFunanushiNmTo.Text = array[5];
					this.txtTantoshaCdFr.Text = array[6];
					this.lblTantoshaNmFr.Text = array[7];
					this.txtTantoshaCdTo.Text = array[8];
					this.lblTantoshaNmTo.Text = array[9];
					this.txtShohinCdFr.Text = array[10];
					this.lblShohinNmFr.Text = array[11];
					this.txtShohinCdTo.Text = array[12];
					this.lblShohinNmTo.Text = array[13];
					this.txtSearchCode.Text = array[14];
				}
				catch (Exception)
				{
				}
			}
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00004520 File Offset: 0x00002720
		private string getDenpyoCondition()
		{
			string[] value = new string[]
			{
				Util.ConvAdDate(this.lblGengoFr.Text, this.txtGengoYearFr.Text, this.txtMonthFr.Text, this.txtDayFr.Text, base.Dba).ToString("yyyy/MM/dd"),
				Util.ConvAdDate(this.lblGengoTo.Text, this.txtGengoYearTo.Text, this.txtMonthTo.Text, this.txtDayTo.Text, base.Dba).ToString("yyyy/MM/dd"),
				this.txtFunanushiCdFr.Text,
				this.lblFunanushiNmFr.Text,
				this.txtFunanushiCdTo.Text,
				this.lblFunanushiNmTo.Text,
				this.txtTantoshaCdFr.Text,
				this.lblTantoshaNmFr.Text,
				this.txtTantoshaCdTo.Text,
				this.lblTantoshaNmTo.Text,
				this.txtShohinCdFr.Text,
				this.lblShohinNmFr.Text,
				this.txtShohinCdTo.Text,
				this.lblShohinNmTo.Text,
				this.txtSearchCode.Text
			};
			return string.Join(",", value);
		}

		// Token: 0x04000002 RID: 2
		private int ShishoCode;

		// Token: 0x04000003 RID: 3
		private int HIN_CHUSHI_KUBUN;

		// Token: 0x04000004 RID: 4
		private string activeContorolName = "";
	}
}
