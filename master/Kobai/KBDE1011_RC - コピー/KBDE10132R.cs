﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using DDCssLib;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.SectionReportModel;
using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x02000005 RID: 5
	public class KBDE10132R : BaseReport
	{
		// Token: 0x06000030 RID: 48 RVA: 0x00002258 File Offset: 0x00000458
		public KBDE10132R(DataTable tgtData) : base(tgtData)
		{
			this.InitializeComponent();
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002267 File Offset: 0x00000467
		private void pageHeader_Format(object sender, EventArgs e)
		{
		}

		// Token: 0x06000032 RID: 50 RVA: 0x0000224D File Offset: 0x0000044D
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00013DEC File Offset: 0x00011FEC
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(KBDE10132R));
			this.pageHeader = new PageHeader();
			this.detail = new Detail();
			this.ラベル378 = new Label();
			this.BackClrCg = new Label();
			this.ラベル340 = new Label();
			this.ラベル359 = new Label();
			this.テキスト389 = new TextBox();
			this.テキスト332 = new TextBox();
			this.テキスト351 = new TextBox();
			this.テキスト370 = new TextBox();
			this.テキスト231 = new TextBox();
			this.テキスト342 = new TextBox();
			this.テキスト361 = new TextBox();
			this.テキスト380 = new TextBox();
			this.ラベル203 = new Label();
			this.テキスト258 = new TextBox();
			this.ITEM02 = new TextBox();
			this.ITEM01 = new TextBox();
			this.ラベル71 = new Label();
			this.ラベル196 = new Label();
			this.ラベル201 = new Label();
			this.テキスト204 = new TextBox();
			this.テキスト205 = new TextBox();
			this.直線206 = new Line();
			this.ラベル207 = new Label();
			this.ラベル208 = new Label();
			this.テキスト209 = new TextBox();
			this.ラベル210 = new Label();
			this.テキスト211 = new TextBox();
			this.テキスト212 = new TextBox();
			this.テキスト213 = new TextBox();
			this.テキスト217 = new TextBox();
			this.テキスト218 = new TextBox();
			this.ラベル219 = new Label();
			this.テキスト220 = new TextBox();
			this.ラベル221 = new Label();
			this.ラベル222 = new Label();
			this.ラベル223 = new Label();
			this.ラベル224 = new Label();
			this.ラベル225 = new Label();
			this.ラベル226 = new Label();
			this.ラベル227 = new Label();
			this.ラベル228 = new Label();
			this.ラベル229 = new Label();
			this.テキスト230 = new TextBox();
			this.テキスト232 = new TextBox();
			this.テキスト233 = new TextBox();
			this.テキスト234 = new TextBox();
			this.テキスト235 = new TextBox();
			this.テキスト236 = new TextBox();
			this.テキスト237 = new TextBox();
			this.テキスト238 = new TextBox();
			this.テキスト331 = new TextBox();
			this.テキスト333 = new TextBox();
			this.テキスト334 = new TextBox();
			this.テキスト335 = new TextBox();
			this.テキスト336 = new TextBox();
			this.テキスト337 = new TextBox();
			this.テキスト338 = new TextBox();
			this.テキスト339 = new TextBox();
			this.テキスト341 = new TextBox();
			this.テキスト343 = new TextBox();
			this.テキスト344 = new TextBox();
			this.テキスト345 = new TextBox();
			this.テキスト346 = new TextBox();
			this.テキスト347 = new TextBox();
			this.テキスト348 = new TextBox();
			this.テキスト349 = new TextBox();
			this.テキスト350 = new TextBox();
			this.テキスト352 = new TextBox();
			this.テキスト353 = new TextBox();
			this.テキスト354 = new TextBox();
			this.テキスト355 = new TextBox();
			this.テキスト356 = new TextBox();
			this.テキスト357 = new TextBox();
			this.テキスト358 = new TextBox();
			this.テキスト360 = new TextBox();
			this.テキスト362 = new TextBox();
			this.テキスト363 = new TextBox();
			this.テキスト364 = new TextBox();
			this.テキスト365 = new TextBox();
			this.テキスト366 = new TextBox();
			this.テキスト367 = new TextBox();
			this.テキスト368 = new TextBox();
			this.テキスト369 = new TextBox();
			this.テキスト371 = new TextBox();
			this.テキスト372 = new TextBox();
			this.テキスト373 = new TextBox();
			this.テキスト374 = new TextBox();
			this.テキスト375 = new TextBox();
			this.テキスト376 = new TextBox();
			this.テキスト377 = new TextBox();
			this.テキスト379 = new TextBox();
			this.テキスト381 = new TextBox();
			this.テキスト382 = new TextBox();
			this.テキスト383 = new TextBox();
			this.テキスト384 = new TextBox();
			this.テキスト385 = new TextBox();
			this.テキスト386 = new TextBox();
			this.テキスト387 = new TextBox();
			this.テキスト388 = new TextBox();
			this.テキスト390 = new TextBox();
			this.テキスト391 = new TextBox();
			this.テキスト392 = new TextBox();
			this.テキスト393 = new TextBox();
			this.テキスト394 = new TextBox();
			this.テキスト395 = new TextBox();
			this.テキスト396 = new TextBox();
			this.ボックス309 = new Shape();
			this.ボックス310 = new Shape();
			this.テキスト311 = new TextBox();
			this.テキスト312 = new TextBox();
			this.テキスト313 = new TextBox();
			this.テキスト314 = new TextBox();
			this.テキスト315 = new TextBox();
			this.テキスト316 = new TextBox();
			this.テキスト317 = new TextBox();
			this.テキスト318 = new TextBox();
			this.直線407 = new Line();
			this.ラベル414 = new Label();
			this.テキスト415 = new TextBox();
			this.テキスト416 = new TextBox();
			this.テキスト417 = new TextBox();
			this.ラベル418 = new Label();
			this.ラベル420 = new Label();
			this.テキスト422 = new TextBox();
			this.直線423 = new Line();
			this.ラベル425 = new Label();
			this.テキスト426 = new TextBox();
			this.ラベル427 = new Label();
			this.テキスト428 = new TextBox();
			this.テキスト429 = new TextBox();
			this.テキスト430 = new TextBox();
			this.テキスト433 = new TextBox();
			this.ラベル434 = new Label();
			this.テキスト435 = new TextBox();
			this.ラベル535 = new Label();
			this.テキスト642 = new TextBox();
			this.テキスト643 = new TextBox();
			this.textBox73 = new TextBox();
			this.textBox74 = new TextBox();
			this.textBox75 = new TextBox();
			this.shape10 = new Shape();
			this.shape11 = new Shape();
			this.textBox76 = new TextBox();
			this.textBox77 = new TextBox();
			this.textBox78 = new TextBox();
			this.textBox79 = new TextBox();
			this.textBox80 = new TextBox();
			this.textBox81 = new TextBox();
			this.textBox82 = new TextBox();
			this.textBox83 = new TextBox();
			this.line3 = new Line();
			this.line4 = new Line();
			this.line5 = new Line();
			this.line6 = new Line();
			this.line7 = new Line();
			this.line8 = new Line();
			this.line9 = new Line();
			this.line10 = new Line();
			this.line11 = new Line();
			this.line12 = new Line();
			this.line13 = new Line();
			this.line14 = new Line();
			this.label1 = new Label();
			this.label2 = new Label();
			this.label3 = new Label();
			this.label4 = new Label();
			this.textBox1 = new TextBox();
			this.textBox2 = new TextBox();
			this.textBox3 = new TextBox();
			this.textBox4 = new TextBox();
			this.textBox5 = new TextBox();
			this.textBox6 = new TextBox();
			this.textBox7 = new TextBox();
			this.textBox8 = new TextBox();
			this.label5 = new Label();
			this.label6 = new Label();
			this.label7 = new Label();
			this.label8 = new Label();
			this.label9 = new Label();
			this.label10 = new Label();
			this.label11 = new Label();
			this.label12 = new Label();
			this.label13 = new Label();
			this.textBox9 = new TextBox();
			this.textBox10 = new TextBox();
			this.textBox11 = new TextBox();
			this.textBox12 = new TextBox();
			this.textBox13 = new TextBox();
			this.textBox14 = new TextBox();
			this.textBox15 = new TextBox();
			this.textBox16 = new TextBox();
			this.textBox17 = new TextBox();
			this.textBox18 = new TextBox();
			this.textBox19 = new TextBox();
			this.textBox20 = new TextBox();
			this.textBox21 = new TextBox();
			this.textBox22 = new TextBox();
			this.textBox23 = new TextBox();
			this.textBox24 = new TextBox();
			this.textBox25 = new TextBox();
			this.textBox26 = new TextBox();
			this.textBox27 = new TextBox();
			this.textBox28 = new TextBox();
			this.textBox29 = new TextBox();
			this.textBox30 = new TextBox();
			this.textBox31 = new TextBox();
			this.textBox32 = new TextBox();
			this.textBox33 = new TextBox();
			this.textBox34 = new TextBox();
			this.textBox35 = new TextBox();
			this.textBox36 = new TextBox();
			this.textBox37 = new TextBox();
			this.textBox38 = new TextBox();
			this.textBox39 = new TextBox();
			this.textBox40 = new TextBox();
			this.textBox41 = new TextBox();
			this.textBox42 = new TextBox();
			this.textBox43 = new TextBox();
			this.textBox44 = new TextBox();
			this.textBox45 = new TextBox();
			this.textBox46 = new TextBox();
			this.textBox47 = new TextBox();
			this.textBox48 = new TextBox();
			this.textBox49 = new TextBox();
			this.textBox50 = new TextBox();
			this.textBox51 = new TextBox();
			this.textBox52 = new TextBox();
			this.textBox53 = new TextBox();
			this.textBox54 = new TextBox();
			this.textBox55 = new TextBox();
			this.textBox56 = new TextBox();
			this.textBox57 = new TextBox();
			this.textBox58 = new TextBox();
			this.textBox59 = new TextBox();
			this.textBox60 = new TextBox();
			this.textBox61 = new TextBox();
			this.textBox62 = new TextBox();
			this.textBox63 = new TextBox();
			this.textBox64 = new TextBox();
			this.textBox65 = new TextBox();
			this.textBox66 = new TextBox();
			this.textBox67 = new TextBox();
			this.textBox68 = new TextBox();
			this.textBox69 = new TextBox();
			this.textBox70 = new TextBox();
			this.textBox71 = new TextBox();
			this.textBox72 = new TextBox();
			this.line1 = new Line();
			this.line15 = new Line();
			this.line16 = new Line();
			this.line17 = new Line();
			this.line18 = new Line();
			this.line19 = new Line();
			this.line20 = new Line();
			this.line21 = new Line();
			this.line22 = new Line();
			this.line23 = new Line();
			this.line24 = new Line();
			this.line25 = new Line();
			this.line26 = new Line();
			this.shape1 = new Shape();
			this.line2 = new Line();
			this.textBox84 = new TextBox();
			this.line27 = new Line();
			this.textBox85 = new TextBox();
			this.textBox87 = new TextBox();
			this.textBox88 = new TextBox();
			this.textBox89 = new TextBox();
			this.textBox90 = new TextBox();
			this.textBox91 = new TextBox();
			this.textBox92 = new TextBox();
			this.textBox93 = new TextBox();
			this.textBox94 = new TextBox();
			this.textBox95 = new TextBox();
			this.textBox96 = new TextBox();
			this.textBox97 = new TextBox();
			this.textBox98 = new TextBox();
			this.textBox99 = new TextBox();
			this.textBox100 = new TextBox();
			this.textBox101 = new TextBox();
			this.textBox102 = new TextBox();
			this.textBox103 = new TextBox();
			this.textBox104 = new TextBox();
			this.picture1 = new Picture();
			this.label14 = new Label();
			this.label16 = new Label();
			this.label23 = new Label();
			this.label15 = new Label();
			this.label17 = new Label();
			this.label18 = new Label();
			this.label19 = new Label();
			this.label20 = new Label();
			this.label21 = new Label();
			this.label22 = new Label();
			this.label24 = new Label();
			this.label25 = new Label();
			this.pageFooter = new PageFooter();
			((ISupportInitialize)this.ラベル378).BeginInit();
			((ISupportInitialize)this.BackClrCg).BeginInit();
			((ISupportInitialize)this.ラベル340).BeginInit();
			((ISupportInitialize)this.ラベル359).BeginInit();
			((ISupportInitialize)this.テキスト389).BeginInit();
			((ISupportInitialize)this.テキスト332).BeginInit();
			((ISupportInitialize)this.テキスト351).BeginInit();
			((ISupportInitialize)this.テキスト370).BeginInit();
			((ISupportInitialize)this.テキスト231).BeginInit();
			((ISupportInitialize)this.テキスト342).BeginInit();
			((ISupportInitialize)this.テキスト361).BeginInit();
			((ISupportInitialize)this.テキスト380).BeginInit();
			((ISupportInitialize)this.ラベル203).BeginInit();
			((ISupportInitialize)this.テキスト258).BeginInit();
			((ISupportInitialize)this.ITEM02).BeginInit();
			((ISupportInitialize)this.ITEM01).BeginInit();
			((ISupportInitialize)this.ラベル71).BeginInit();
			((ISupportInitialize)this.ラベル196).BeginInit();
			((ISupportInitialize)this.ラベル201).BeginInit();
			((ISupportInitialize)this.テキスト204).BeginInit();
			((ISupportInitialize)this.テキスト205).BeginInit();
			((ISupportInitialize)this.ラベル207).BeginInit();
			((ISupportInitialize)this.ラベル208).BeginInit();
			((ISupportInitialize)this.テキスト209).BeginInit();
			((ISupportInitialize)this.ラベル210).BeginInit();
			((ISupportInitialize)this.テキスト211).BeginInit();
			((ISupportInitialize)this.テキスト212).BeginInit();
			((ISupportInitialize)this.テキスト213).BeginInit();
			((ISupportInitialize)this.テキスト217).BeginInit();
			((ISupportInitialize)this.テキスト218).BeginInit();
			((ISupportInitialize)this.ラベル219).BeginInit();
			((ISupportInitialize)this.テキスト220).BeginInit();
			((ISupportInitialize)this.ラベル221).BeginInit();
			((ISupportInitialize)this.ラベル222).BeginInit();
			((ISupportInitialize)this.ラベル223).BeginInit();
			((ISupportInitialize)this.ラベル224).BeginInit();
			((ISupportInitialize)this.ラベル225).BeginInit();
			((ISupportInitialize)this.ラベル226).BeginInit();
			((ISupportInitialize)this.ラベル227).BeginInit();
			((ISupportInitialize)this.ラベル228).BeginInit();
			((ISupportInitialize)this.ラベル229).BeginInit();
			((ISupportInitialize)this.テキスト230).BeginInit();
			((ISupportInitialize)this.テキスト232).BeginInit();
			((ISupportInitialize)this.テキスト233).BeginInit();
			((ISupportInitialize)this.テキスト234).BeginInit();
			((ISupportInitialize)this.テキスト235).BeginInit();
			((ISupportInitialize)this.テキスト236).BeginInit();
			((ISupportInitialize)this.テキスト237).BeginInit();
			((ISupportInitialize)this.テキスト238).BeginInit();
			((ISupportInitialize)this.テキスト331).BeginInit();
			((ISupportInitialize)this.テキスト333).BeginInit();
			((ISupportInitialize)this.テキスト334).BeginInit();
			((ISupportInitialize)this.テキスト335).BeginInit();
			((ISupportInitialize)this.テキスト336).BeginInit();
			((ISupportInitialize)this.テキスト337).BeginInit();
			((ISupportInitialize)this.テキスト338).BeginInit();
			((ISupportInitialize)this.テキスト339).BeginInit();
			((ISupportInitialize)this.テキスト341).BeginInit();
			((ISupportInitialize)this.テキスト343).BeginInit();
			((ISupportInitialize)this.テキスト344).BeginInit();
			((ISupportInitialize)this.テキスト345).BeginInit();
			((ISupportInitialize)this.テキスト346).BeginInit();
			((ISupportInitialize)this.テキスト347).BeginInit();
			((ISupportInitialize)this.テキスト348).BeginInit();
			((ISupportInitialize)this.テキスト349).BeginInit();
			((ISupportInitialize)this.テキスト350).BeginInit();
			((ISupportInitialize)this.テキスト352).BeginInit();
			((ISupportInitialize)this.テキスト353).BeginInit();
			((ISupportInitialize)this.テキスト354).BeginInit();
			((ISupportInitialize)this.テキスト355).BeginInit();
			((ISupportInitialize)this.テキスト356).BeginInit();
			((ISupportInitialize)this.テキスト357).BeginInit();
			((ISupportInitialize)this.テキスト358).BeginInit();
			((ISupportInitialize)this.テキスト360).BeginInit();
			((ISupportInitialize)this.テキスト362).BeginInit();
			((ISupportInitialize)this.テキスト363).BeginInit();
			((ISupportInitialize)this.テキスト364).BeginInit();
			((ISupportInitialize)this.テキスト365).BeginInit();
			((ISupportInitialize)this.テキスト366).BeginInit();
			((ISupportInitialize)this.テキスト367).BeginInit();
			((ISupportInitialize)this.テキスト368).BeginInit();
			((ISupportInitialize)this.テキスト369).BeginInit();
			((ISupportInitialize)this.テキスト371).BeginInit();
			((ISupportInitialize)this.テキスト372).BeginInit();
			((ISupportInitialize)this.テキスト373).BeginInit();
			((ISupportInitialize)this.テキスト374).BeginInit();
			((ISupportInitialize)this.テキスト375).BeginInit();
			((ISupportInitialize)this.テキスト376).BeginInit();
			((ISupportInitialize)this.テキスト377).BeginInit();
			((ISupportInitialize)this.テキスト379).BeginInit();
			((ISupportInitialize)this.テキスト381).BeginInit();
			((ISupportInitialize)this.テキスト382).BeginInit();
			((ISupportInitialize)this.テキスト383).BeginInit();
			((ISupportInitialize)this.テキスト384).BeginInit();
			((ISupportInitialize)this.テキスト385).BeginInit();
			((ISupportInitialize)this.テキスト386).BeginInit();
			((ISupportInitialize)this.テキスト387).BeginInit();
			((ISupportInitialize)this.テキスト388).BeginInit();
			((ISupportInitialize)this.テキスト390).BeginInit();
			((ISupportInitialize)this.テキスト391).BeginInit();
			((ISupportInitialize)this.テキスト392).BeginInit();
			((ISupportInitialize)this.テキスト393).BeginInit();
			((ISupportInitialize)this.テキスト394).BeginInit();
			((ISupportInitialize)this.テキスト395).BeginInit();
			((ISupportInitialize)this.テキスト396).BeginInit();
			((ISupportInitialize)this.テキスト311).BeginInit();
			((ISupportInitialize)this.テキスト312).BeginInit();
			((ISupportInitialize)this.テキスト313).BeginInit();
			((ISupportInitialize)this.テキスト314).BeginInit();
			((ISupportInitialize)this.テキスト315).BeginInit();
			((ISupportInitialize)this.テキスト316).BeginInit();
			((ISupportInitialize)this.テキスト317).BeginInit();
			((ISupportInitialize)this.テキスト318).BeginInit();
			((ISupportInitialize)this.ラベル414).BeginInit();
			((ISupportInitialize)this.テキスト415).BeginInit();
			((ISupportInitialize)this.テキスト416).BeginInit();
			((ISupportInitialize)this.テキスト417).BeginInit();
			((ISupportInitialize)this.ラベル418).BeginInit();
			((ISupportInitialize)this.ラベル420).BeginInit();
			((ISupportInitialize)this.テキスト422).BeginInit();
			((ISupportInitialize)this.ラベル425).BeginInit();
			((ISupportInitialize)this.テキスト426).BeginInit();
			((ISupportInitialize)this.ラベル427).BeginInit();
			((ISupportInitialize)this.テキスト428).BeginInit();
			((ISupportInitialize)this.テキスト429).BeginInit();
			((ISupportInitialize)this.テキスト430).BeginInit();
			((ISupportInitialize)this.テキスト433).BeginInit();
			((ISupportInitialize)this.ラベル434).BeginInit();
			((ISupportInitialize)this.テキスト435).BeginInit();
			((ISupportInitialize)this.ラベル535).BeginInit();
			((ISupportInitialize)this.テキスト642).BeginInit();
			((ISupportInitialize)this.テキスト643).BeginInit();
			((ISupportInitialize)this.textBox73).BeginInit();
			((ISupportInitialize)this.textBox74).BeginInit();
			((ISupportInitialize)this.textBox75).BeginInit();
			((ISupportInitialize)this.textBox76).BeginInit();
			((ISupportInitialize)this.textBox77).BeginInit();
			((ISupportInitialize)this.textBox78).BeginInit();
			((ISupportInitialize)this.textBox79).BeginInit();
			((ISupportInitialize)this.textBox80).BeginInit();
			((ISupportInitialize)this.textBox81).BeginInit();
			((ISupportInitialize)this.textBox82).BeginInit();
			((ISupportInitialize)this.textBox83).BeginInit();
			((ISupportInitialize)this.label1).BeginInit();
			((ISupportInitialize)this.label2).BeginInit();
			((ISupportInitialize)this.label3).BeginInit();
			((ISupportInitialize)this.label4).BeginInit();
			((ISupportInitialize)this.textBox1).BeginInit();
			((ISupportInitialize)this.textBox2).BeginInit();
			((ISupportInitialize)this.textBox3).BeginInit();
			((ISupportInitialize)this.textBox4).BeginInit();
			((ISupportInitialize)this.textBox5).BeginInit();
			((ISupportInitialize)this.textBox6).BeginInit();
			((ISupportInitialize)this.textBox7).BeginInit();
			((ISupportInitialize)this.textBox8).BeginInit();
			((ISupportInitialize)this.label5).BeginInit();
			((ISupportInitialize)this.label6).BeginInit();
			((ISupportInitialize)this.label7).BeginInit();
			((ISupportInitialize)this.label8).BeginInit();
			((ISupportInitialize)this.label9).BeginInit();
			((ISupportInitialize)this.label10).BeginInit();
			((ISupportInitialize)this.label11).BeginInit();
			((ISupportInitialize)this.label12).BeginInit();
			((ISupportInitialize)this.label13).BeginInit();
			((ISupportInitialize)this.textBox9).BeginInit();
			((ISupportInitialize)this.textBox10).BeginInit();
			((ISupportInitialize)this.textBox11).BeginInit();
			((ISupportInitialize)this.textBox12).BeginInit();
			((ISupportInitialize)this.textBox13).BeginInit();
			((ISupportInitialize)this.textBox14).BeginInit();
			((ISupportInitialize)this.textBox15).BeginInit();
			((ISupportInitialize)this.textBox16).BeginInit();
			((ISupportInitialize)this.textBox17).BeginInit();
			((ISupportInitialize)this.textBox18).BeginInit();
			((ISupportInitialize)this.textBox19).BeginInit();
			((ISupportInitialize)this.textBox20).BeginInit();
			((ISupportInitialize)this.textBox21).BeginInit();
			((ISupportInitialize)this.textBox22).BeginInit();
			((ISupportInitialize)this.textBox23).BeginInit();
			((ISupportInitialize)this.textBox24).BeginInit();
			((ISupportInitialize)this.textBox25).BeginInit();
			((ISupportInitialize)this.textBox26).BeginInit();
			((ISupportInitialize)this.textBox27).BeginInit();
			((ISupportInitialize)this.textBox28).BeginInit();
			((ISupportInitialize)this.textBox29).BeginInit();
			((ISupportInitialize)this.textBox30).BeginInit();
			((ISupportInitialize)this.textBox31).BeginInit();
			((ISupportInitialize)this.textBox32).BeginInit();
			((ISupportInitialize)this.textBox33).BeginInit();
			((ISupportInitialize)this.textBox34).BeginInit();
			((ISupportInitialize)this.textBox35).BeginInit();
			((ISupportInitialize)this.textBox36).BeginInit();
			((ISupportInitialize)this.textBox37).BeginInit();
			((ISupportInitialize)this.textBox38).BeginInit();
			((ISupportInitialize)this.textBox39).BeginInit();
			((ISupportInitialize)this.textBox40).BeginInit();
			((ISupportInitialize)this.textBox41).BeginInit();
			((ISupportInitialize)this.textBox42).BeginInit();
			((ISupportInitialize)this.textBox43).BeginInit();
			((ISupportInitialize)this.textBox44).BeginInit();
			((ISupportInitialize)this.textBox45).BeginInit();
			((ISupportInitialize)this.textBox46).BeginInit();
			((ISupportInitialize)this.textBox47).BeginInit();
			((ISupportInitialize)this.textBox48).BeginInit();
			((ISupportInitialize)this.textBox49).BeginInit();
			((ISupportInitialize)this.textBox50).BeginInit();
			((ISupportInitialize)this.textBox51).BeginInit();
			((ISupportInitialize)this.textBox52).BeginInit();
			((ISupportInitialize)this.textBox53).BeginInit();
			((ISupportInitialize)this.textBox54).BeginInit();
			((ISupportInitialize)this.textBox55).BeginInit();
			((ISupportInitialize)this.textBox56).BeginInit();
			((ISupportInitialize)this.textBox57).BeginInit();
			((ISupportInitialize)this.textBox58).BeginInit();
			((ISupportInitialize)this.textBox59).BeginInit();
			((ISupportInitialize)this.textBox60).BeginInit();
			((ISupportInitialize)this.textBox61).BeginInit();
			((ISupportInitialize)this.textBox62).BeginInit();
			((ISupportInitialize)this.textBox63).BeginInit();
			((ISupportInitialize)this.textBox64).BeginInit();
			((ISupportInitialize)this.textBox65).BeginInit();
			((ISupportInitialize)this.textBox66).BeginInit();
			((ISupportInitialize)this.textBox67).BeginInit();
			((ISupportInitialize)this.textBox68).BeginInit();
			((ISupportInitialize)this.textBox69).BeginInit();
			((ISupportInitialize)this.textBox70).BeginInit();
			((ISupportInitialize)this.textBox71).BeginInit();
			((ISupportInitialize)this.textBox72).BeginInit();
			((ISupportInitialize)this.textBox84).BeginInit();
			((ISupportInitialize)this.textBox85).BeginInit();
			((ISupportInitialize)this.textBox87).BeginInit();
			((ISupportInitialize)this.textBox88).BeginInit();
			((ISupportInitialize)this.textBox89).BeginInit();
			((ISupportInitialize)this.textBox90).BeginInit();
			((ISupportInitialize)this.textBox91).BeginInit();
			((ISupportInitialize)this.textBox92).BeginInit();
			((ISupportInitialize)this.textBox93).BeginInit();
			((ISupportInitialize)this.textBox94).BeginInit();
			((ISupportInitialize)this.textBox95).BeginInit();
			((ISupportInitialize)this.textBox96).BeginInit();
			((ISupportInitialize)this.textBox97).BeginInit();
			((ISupportInitialize)this.textBox98).BeginInit();
			((ISupportInitialize)this.textBox99).BeginInit();
			((ISupportInitialize)this.textBox100).BeginInit();
			((ISupportInitialize)this.textBox101).BeginInit();
			((ISupportInitialize)this.textBox102).BeginInit();
			((ISupportInitialize)this.textBox103).BeginInit();
			((ISupportInitialize)this.textBox104).BeginInit();
			((ISupportInitialize)this.picture1).BeginInit();
			((ISupportInitialize)this.label14).BeginInit();
			((ISupportInitialize)this.label16).BeginInit();
			((ISupportInitialize)this.label23).BeginInit();
			((ISupportInitialize)this.label15).BeginInit();
			((ISupportInitialize)this.label17).BeginInit();
			((ISupportInitialize)this.label18).BeginInit();
			((ISupportInitialize)this.label19).BeginInit();
			((ISupportInitialize)this.label20).BeginInit();
			((ISupportInitialize)this.label21).BeginInit();
			((ISupportInitialize)this.label22).BeginInit();
			((ISupportInitialize)this.label24).BeginInit();
			((ISupportInitialize)this.label25).BeginInit();
			((ISupportInitialize)this).BeginInit();
			this.pageHeader.Height = 0f;
			this.pageHeader.Name = "pageHeader";
			this.pageHeader.Format += this.pageHeader_Format;
			this.detail.Controls.AddRange(new ARControl[]
			{
				this.ラベル378,
				this.BackClrCg,
				this.ラベル340,
				this.ラベル359,
				this.テキスト389,
				this.テキスト332,
				this.テキスト351,
				this.テキスト370,
				this.テキスト231,
				this.テキスト342,
				this.テキスト361,
				this.テキスト380,
				this.ラベル203,
				this.テキスト258,
				this.ITEM02,
				this.ITEM01,
				this.ラベル71,
				this.ラベル196,
				this.ラベル201,
				this.テキスト204,
				this.テキスト205,
				this.直線206,
				this.ラベル207,
				this.ラベル208,
				this.テキスト209,
				this.ラベル210,
				this.テキスト211,
				this.テキスト212,
				this.テキスト213,
				this.テキスト217,
				this.テキスト218,
				this.ラベル219,
				this.テキスト220,
				this.ラベル221,
				this.ラベル222,
				this.ラベル223,
				this.ラベル224,
				this.ラベル225,
				this.ラベル226,
				this.ラベル227,
				this.ラベル228,
				this.ラベル229,
				this.テキスト230,
				this.テキスト232,
				this.テキスト233,
				this.テキスト234,
				this.テキスト235,
				this.テキスト236,
				this.テキスト237,
				this.テキスト238,
				this.テキスト331,
				this.テキスト333,
				this.テキスト334,
				this.テキスト335,
				this.テキスト336,
				this.テキスト337,
				this.テキスト338,
				this.テキスト339,
				this.テキスト341,
				this.テキスト343,
				this.テキスト344,
				this.テキスト345,
				this.テキスト346,
				this.テキスト347,
				this.テキスト348,
				this.テキスト349,
				this.テキスト350,
				this.テキスト352,
				this.テキスト353,
				this.テキスト354,
				this.テキスト355,
				this.テキスト356,
				this.テキスト357,
				this.テキスト358,
				this.テキスト360,
				this.テキスト362,
				this.テキスト363,
				this.テキスト364,
				this.テキスト365,
				this.テキスト366,
				this.テキスト367,
				this.テキスト368,
				this.テキスト369,
				this.テキスト371,
				this.テキスト372,
				this.テキスト373,
				this.テキスト374,
				this.テキスト375,
				this.テキスト376,
				this.テキスト377,
				this.テキスト379,
				this.テキスト381,
				this.テキスト382,
				this.テキスト383,
				this.テキスト384,
				this.テキスト385,
				this.テキスト386,
				this.テキスト387,
				this.テキスト388,
				this.テキスト390,
				this.テキスト391,
				this.テキスト392,
				this.テキスト393,
				this.テキスト394,
				this.テキスト395,
				this.テキスト396,
				this.ボックス309,
				this.ボックス310,
				this.テキスト311,
				this.テキスト312,
				this.テキスト313,
				this.テキスト314,
				this.テキスト315,
				this.テキスト316,
				this.テキスト317,
				this.テキスト318,
				this.直線407,
				this.ラベル414,
				this.テキスト415,
				this.テキスト416,
				this.テキスト417,
				this.ラベル418,
				this.ラベル420,
				this.テキスト422,
				this.直線423,
				this.ラベル425,
				this.テキスト426,
				this.ラベル427,
				this.テキスト428,
				this.テキスト429,
				this.テキスト430,
				this.テキスト433,
				this.ラベル434,
				this.テキスト435,
				this.ラベル535,
				this.テキスト642,
				this.テキスト643,
				this.textBox73,
				this.textBox74,
				this.textBox75,
				this.shape10,
				this.shape11,
				this.textBox76,
				this.textBox77,
				this.textBox78,
				this.textBox79,
				this.textBox80,
				this.textBox81,
				this.textBox82,
				this.textBox83,
				this.line3,
				this.line4,
				this.line5,
				this.line6,
				this.line7,
				this.line8,
				this.line9,
				this.line10,
				this.line11,
				this.line12,
				this.line13,
				this.line14,
				this.label1,
				this.label2,
				this.label3,
				this.label4,
				this.textBox1,
				this.textBox2,
				this.textBox3,
				this.textBox4,
				this.textBox5,
				this.textBox6,
				this.textBox7,
				this.textBox8,
				this.label5,
				this.label6,
				this.label7,
				this.label8,
				this.label9,
				this.label10,
				this.label11,
				this.label12,
				this.label13,
				this.textBox9,
				this.textBox10,
				this.textBox11,
				this.textBox12,
				this.textBox13,
				this.textBox14,
				this.textBox15,
				this.textBox16,
				this.textBox17,
				this.textBox18,
				this.textBox19,
				this.textBox20,
				this.textBox21,
				this.textBox22,
				this.textBox23,
				this.textBox24,
				this.textBox25,
				this.textBox26,
				this.textBox27,
				this.textBox28,
				this.textBox29,
				this.textBox30,
				this.textBox31,
				this.textBox32,
				this.textBox33,
				this.textBox34,
				this.textBox35,
				this.textBox36,
				this.textBox37,
				this.textBox38,
				this.textBox39,
				this.textBox40,
				this.textBox41,
				this.textBox42,
				this.textBox43,
				this.textBox44,
				this.textBox45,
				this.textBox46,
				this.textBox47,
				this.textBox48,
				this.textBox49,
				this.textBox50,
				this.textBox51,
				this.textBox52,
				this.textBox53,
				this.textBox54,
				this.textBox55,
				this.textBox56,
				this.textBox57,
				this.textBox58,
				this.textBox59,
				this.textBox60,
				this.textBox61,
				this.textBox62,
				this.textBox63,
				this.textBox64,
				this.textBox65,
				this.textBox66,
				this.textBox67,
				this.textBox68,
				this.textBox69,
				this.textBox70,
				this.textBox71,
				this.textBox72,
				this.line1,
				this.line15,
				this.line16,
				this.line17,
				this.line18,
				this.line19,
				this.line20,
				this.line21,
				this.line22,
				this.line23,
				this.line24,
				this.line25,
				this.line26,
				this.shape1,
				this.line2,
				this.textBox84,
				this.line27,
				this.textBox85,
				this.textBox87,
				this.textBox88,
				this.textBox89,
				this.textBox90,
				this.textBox91,
				this.textBox92,
				this.textBox93,
				this.textBox94,
				this.textBox95,
				this.textBox96,
				this.textBox97,
				this.textBox98,
				this.textBox99,
				this.textBox100,
				this.textBox101,
				this.textBox102,
				this.textBox103,
				this.textBox104,
				this.picture1,
				this.label14,
				this.label16,
				this.label23,
				this.label15,
				this.label17,
				this.label18,
				this.label19,
				this.label20,
				this.label21,
				this.label22,
				this.label24,
				this.label25
			});
			this.detail.Height = 11.1484f;
			this.detail.Name = "detail";
			this.ラベル378.Height = 0.3354167f;
			this.ラベル378.HyperLink = null;
			this.ラベル378.Left = 0.0972441f;
			this.ラベル378.Name = "ラベル378";
			this.ラベル378.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.ラベル378.Tag = "";
			this.ラベル378.Text = "\u3000";
			this.ラベル378.Top = 4.185174f;
			this.ラベル378.Width = 7.659056f;
			this.BackClrCg.Height = 0.3354167f;
			this.BackClrCg.HyperLink = null;
			this.BackClrCg.Left = 0.0972441f;
			this.BackClrCg.Name = "BackClrCg";
			this.BackClrCg.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.BackClrCg.Tag = "";
			this.BackClrCg.Text = "\u3000";
			this.BackClrCg.Top = 2.167813f;
			this.BackClrCg.Width = 7.659056f;
			this.ラベル340.Height = 0.3354167f;
			this.ラベル340.HyperLink = null;
			this.ラベル340.Left = 0.09291339f;
			this.ラベル340.Name = "ラベル340";
			this.ラベル340.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.ラベル340.Tag = "";
			this.ラベル340.Text = "\u3000";
			this.ラベル340.Top = 2.85323f;
			this.ラベル340.Width = 7.663386f;
			this.ラベル359.Height = 0.3361111f;
			this.ラベル359.HyperLink = null;
			this.ラベル359.Left = 0.0972441f;
			this.ラベル359.Name = "ラベル359";
			this.ラベル359.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.ラベル359.Tag = "";
			this.ラベル359.Text = "\u3000";
			this.ラベル359.Top = 3.512258f;
			this.ラベル359.Width = 7.662599f;
			this.テキスト389.DataField = "ITEM78";
			this.テキスト389.Height = 0.1875f;
			this.テキスト389.Left = 0.9256942f;
			this.テキスト389.Name = "テキスト389";
			this.テキスト389.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト389.Tag = "";
			this.テキスト389.Text = "ITEM78";
			this.テキスト389.Top = 4.26573f;
			this.テキスト389.Width = 2.385417f;
			this.テキスト332.DataField = "ITEM24";
			this.テキスト332.Height = 0.1805556f;
			this.テキスト332.Left = 0.9256942f;
			this.テキスト332.Name = "テキスト332";
			this.テキスト332.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト332.Tag = "";
			this.テキスト332.Text = "ITEM24";
			this.テキスト332.Top = 2.262258f;
			this.テキスト332.Width = 2.385417f;
			this.テキスト351.DataField = "ITEM42";
			this.テキスト351.Height = 0.1875f;
			this.テキスト351.Left = 0.9256942f;
			this.テキスト351.Name = "テキスト351";
			this.テキスト351.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト351.Tag = "";
			this.テキスト351.Text = "ITEM42";
			this.テキスト351.Top = 2.944896f;
			this.テキスト351.Width = 2.385417f;
			this.テキスト370.DataField = "ITEM60";
			this.テキスト370.Height = 0.1875f;
			this.テキスト370.Left = 0.9256942f;
			this.テキスト370.Name = "テキスト370";
			this.テキスト370.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト370.Tag = "";
			this.テキスト370.Text = "ITEM60";
			this.テキスト370.Top = 3.610869f;
			this.テキスト370.Width = 2.385417f;
			this.テキスト231.DataField = "ITEM15";
			this.テキスト231.Height = 0.1875f;
			this.テキスト231.Left = 0.9256942f;
			this.テキスト231.Name = "テキスト231";
			this.テキスト231.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト231.Tag = "";
			this.テキスト231.Text = "ITEM15";
			this.テキスト231.Top = 1.94698f;
			this.テキスト231.Width = 2.385417f;
			this.テキスト342.DataField = "ITEM33";
			this.テキスト342.Height = 0.1875f;
			this.テキスト342.Left = 0.9256942f;
			this.テキスト342.Name = "テキスト342";
			this.テキスト342.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト342.Tag = "";
			this.テキスト342.Text = "ITEM33";
			this.テキスト342.Top = 2.60948f;
			this.テキスト342.Width = 2.385417f;
			this.テキスト361.DataField = "ITEM51";
			this.テキスト361.Height = 0.1875f;
			this.テキスト361.Left = 0.9256942f;
			this.テキスト361.Name = "テキスト361";
			this.テキスト361.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト361.Tag = "";
			this.テキスト361.Text = "ITEM51";
			this.テキスト361.Top = 3.272675f;
			this.テキスト361.Width = 2.385417f;
			this.テキスト380.DataField = "ITEM69";
			this.テキスト380.Height = 0.1875f;
			this.テキスト380.Left = 0.9256942f;
			this.テキスト380.Name = "テキスト380";
			this.テキスト380.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト380.Tag = "";
			this.テキスト380.Text = "ITEM69";
			this.テキスト380.Top = 3.923369f;
			this.テキスト380.Width = 2.385417f;
			this.ラベル203.Border.BottomStyle = BorderLineStyle.Solid;
			this.ラベル203.Border.LeftStyle = BorderLineStyle.Solid;
			this.ラベル203.Border.RightStyle = BorderLineStyle.Solid;
			this.ラベル203.Border.TopStyle = BorderLineStyle.Solid;
			this.ラベル203.Height = 0.3951389f;
			this.ラベル203.HyperLink = null;
			this.ラベル203.Left = 3.098611f;
			this.ラベル203.Name = "ラベル203";
			this.ラベル203.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.ラベル203.Tag = "";
			this.ラベル203.Text = "\u3000";
			this.ラベル203.Top = 0.3087857f;
			this.ラベル203.Width = 1.811111f;
			this.テキスト258.DataField = "ITEM13";
			this.テキスト258.Height = 0.1875f;
			this.テキスト258.Left = 7.295139f;
			this.テキスト258.Name = "テキスト258";
			this.テキスト258.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト258.Tag = "";
			this.テキスト258.Text = "ITEM13";
			this.テキスト258.Top = 0.4962857f;
			this.テキスト258.Width = 0.3229167f;
			this.ITEM02.DataField = "ITEM02";
			this.ITEM02.Height = 0.1979167f;
			this.ITEM02.Left = 6.590972f;
			this.ITEM02.Name = "ITEM02";
			this.ITEM02.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ITEM02.Tag = "";
			this.ITEM02.Text = "ITEM02";
			this.ITEM02.Top = 0.5011469f;
			this.ITEM02.Width = 0.59375f;
			this.ITEM01.DataField = "ITEM01";
			this.ITEM01.Height = 0.1875f;
			this.ITEM01.Left = 5.88189f;
			this.ITEM01.Name = "ITEM01";
			this.ITEM01.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ITEM01.Tag = "";
			this.ITEM01.Text = "ITEM01";
			this.ITEM01.Top = 0.2846457f;
			this.ITEM01.Width = 1.906348f;
			this.ラベル71.Height = 0.1972222f;
			this.ラベル71.HyperLink = null;
			this.ラベル71.Left = 7.60625f;
			this.ラベル71.Name = "ラベル71";
			this.ラベル71.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル71.Tag = "";
			this.ラベル71.Text = "頁";
			this.ラベル71.Top = 0.5011469f;
			this.ラベル71.Width = 0.1715278f;
			this.ラベル196.Height = 0.2333333f;
			this.ラベル196.HyperLink = null;
			this.ラベル196.Left = 3.295139f;
			this.ラベル196.Name = "ラベル196";
			this.ラベル196.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.ラベル196.Tag = "";
			this.ラベル196.Text = "売上票(控え)";
			this.ラベル196.Top = 0.3907302f;
			this.ラベル196.Width = 1.415278f;
			this.ラベル201.Height = 0.1979167f;
			this.ラベル201.HyperLink = null;
			this.ラベル201.Left = 5.722221f;
			this.ラベル201.Name = "ラベル201";
			this.ラベル201.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル201.Tag = "";
			this.ラベル201.Text = "伝票番号：";
			this.ラベル201.Top = 0.5011469f;
			this.ラベル201.Width = 0.8333333f;
			this.テキスト204.DataField = "ITEM03";
			this.テキスト204.Height = 0.1875f;
			this.テキスト204.Left = 0.1319441f;
			this.テキスト204.Name = "テキスト204";
			this.テキスト204.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト204.Tag = "";
			this.テキスト204.Text = "ITEM03";
			this.テキスト204.Top = 0.2254524f;
			this.テキスト204.Visible = false;
			this.テキスト204.Width = 1.34375f;
			this.テキスト205.DataField = "ITEM04";
			this.テキスト205.Height = 0.1875f;
			this.テキスト205.Left = 0.1318898f;
			this.テキスト205.Name = "テキスト205";
			this.テキスト205.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト205.Tag = "";
			this.テキスト205.Text = "ITEM04";
			this.テキスト205.Top = 0.9858268f;
			this.テキスト205.Width = 2.375f;
			this.直線206.Height = 0f;
			this.直線206.Left = 0.09722185f;
			this.直線206.LineWeight = 0f;
			this.直線206.Name = "直線206";
			this.直線206.Tag = "";
			this.直線206.Top = 1.204619f;
			this.直線206.Width = 2.710417f;
			this.直線206.X1 = 0.09722185f;
			this.直線206.X2 = 2.807639f;
			this.直線206.Y1 = 1.204619f;
			this.直線206.Y2 = 1.204619f;
			this.ラベル207.Height = 0.1972222f;
			this.ラベル207.HyperLink = null;
			this.ラベル207.Left = 2.537008f;
			this.ラベル207.Name = "ラベル207";
			this.ラベル207.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル207.Tag = "";
			this.ラベル207.Text = "御中";
			this.ラベル207.Top = 1.00748f;
			this.ラベル207.Width = 0.4270892f;
			this.ラベル208.Height = 0.15625f;
			this.ラベル208.HyperLink = null;
			this.ラベル208.Left = 0.1319441f;
			this.ラベル208.Name = "ラベル208";
			this.ラベル208.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル208.Tag = "";
			this.ラベル208.Text = "TEL";
			this.ラベル208.Top = 1.245591f;
			this.ラベル208.Visible = false;
			this.ラベル208.Width = 0.2395833f;
			this.テキスト209.DataField = "ITEM05";
			this.テキスト209.Height = 0.15625f;
			this.テキスト209.Left = 0.3680552f;
			this.テキスト209.Name = "テキスト209";
			this.テキスト209.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト209.Tag = "";
			this.テキスト209.Text = "ITEM05";
			this.テキスト209.Top = 1.246286f;
			this.テキスト209.Visible = false;
			this.テキスト209.Width = 0.9479167f;
			this.ラベル210.Height = 0.15625f;
			this.ラベル210.HyperLink = null;
			this.ラベル210.Left = 1.315972f;
			this.ラベル210.Name = "ラベル210";
			this.ラベル210.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル210.Tag = "";
			this.ラベル210.Text = "FAX";
			this.ラベル210.Top = 1.241424f;
			this.ラベル210.Visible = false;
			this.ラベル210.Width = 0.2395833f;
			this.テキスト211.DataField = "ITEM06";
			this.テキスト211.Height = 0.15625f;
			this.テキスト211.Left = 1.552083f;
			this.テキスト211.Name = "テキスト211";
			this.テキスト211.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト211.Tag = "";
			this.テキスト211.Text = "ITEM06";
			this.テキスト211.Top = 1.242119f;
			this.テキスト211.Visible = false;
			this.テキスト211.Width = 0.9791667f;
			this.テキスト212.DataField = "ITEM07";
			this.テキスト212.Height = 0.1875f;
			this.テキスト212.Left = 5.528347f;
			this.テキスト212.Name = "テキスト212";
			this.テキスト212.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト212.Tag = "";
			this.テキスト212.Text = "ITEM07";
			this.テキスト212.Top = 0.7346457f;
			this.テキスト212.Width = 2.21875f;
			this.テキスト213.DataField = "ITEM08";
			this.テキスト213.Height = 0.1875f;
			this.テキスト213.Left = 5.528347f;
			this.テキスト213.Name = "テキスト213";
			this.テキスト213.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 1";
			this.テキスト213.Tag = "";
			this.テキスト213.Text = "住所";
			this.テキスト213.Top = 0.9417323f;
			this.テキスト213.Width = 2.21875f;
			this.テキスト217.DataField = "ITEM10";
			this.テキスト217.Height = 0.15625f;
			this.テキスト217.Left = 5.566142f;
			this.テキスト217.Name = "テキスト217";
			this.テキスト217.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 1";
			this.テキスト217.Tag = "";
			this.テキスト217.Text = "ITEM10";
			this.テキスト217.Top = 1.462599f;
			this.テキスト217.Width = 2.21875f;
			this.テキスト218.DataField = "ITEM11";
			this.テキスト218.Height = 0.1875f;
			this.テキスト218.Left = 3.097222f;
			this.テキスト218.Name = "テキスト218";
			this.テキスト218.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.テキスト218.Tag = "";
			this.テキスト218.Text = "ITEM11";
			this.テキスト218.Top = 1.448369f;
			this.テキスト218.Width = 1.811111f;
			this.ラベル219.Height = 0.15625f;
			this.ラベル219.HyperLink = null;
			this.ラベル219.Left = 6.003471f;
			this.ラベル219.Name = "ラベル219";
			this.ラベル219.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル219.Tag = "";
			this.ラベル219.Text = "担当：";
			this.ラベル219.Top = 1.475452f;
			this.ラベル219.Width = 0.4791667f;
			this.テキスト220.DataField = "ITEM12";
			this.テキスト220.Height = 0.15625f;
			this.テキスト220.Left = 6.470139f;
			this.テキスト220.Name = "テキスト220";
			this.テキスト220.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト220.Tag = "";
			this.テキスト220.Text = "ITEM12";
			this.テキスト220.Top = 1.475452f;
			this.テキスト220.Width = 1.302083f;
			this.ラベル221.Height = 0.1979167f;
			this.ラベル221.HyperLink = null;
			this.ラベル221.Left = 0.0972441f;
			this.ラベル221.Name = "ラベル221";
			this.ラベル221.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.ラベル221.Tag = "";
			this.ラベル221.Text = "品名コード";
			this.ラベル221.Top = 1.685433f;
			this.ラベル221.Width = 0.8333111f;
			this.ラベル222.Height = 0.1979167f;
			this.ラベル222.HyperLink = null;
			this.ラベル222.Left = 0.8888887f;
			this.ラベル222.Name = "ラベル222";
			this.ラベル222.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128; ddo-font-vertical: none";
			this.ラベル222.Tag = "";
			this.ラベル222.Text = "\u3000商\u3000品\u3000名";
			this.ラベル222.Top = 1.685433f;
			this.ラベル222.Width = 2.489583f;
			this.ラベル223.Height = 0.1979167f;
			this.ラベル223.HyperLink = null;
			this.ラベル223.Left = 3.368055f;
			this.ラベル223.Name = "ラベル223";
			this.ラベル223.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.ラベル223.Tag = "";
			this.ラベル223.Text = "規\u3000格";
			this.ラベル223.Top = 1.685433f;
			this.ラベル223.Width = 1.03125f;
			this.ラベル224.Height = 0.1979167f;
			this.ラベル224.HyperLink = null;
			this.ラベル224.Left = 4.386111f;
			this.ラベル224.Name = "ラベル224";
			this.ラベル224.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.ラベル224.Tag = "";
			this.ラベル224.Text = "単位";
			this.ラベル224.Top = 1.685433f;
			this.ラベル224.Width = 0.3125f;
			this.ラベル225.Height = 0.1979167f;
			this.ラベル225.HyperLink = null;
			this.ラベル225.Left = 4.700695f;
			this.ラベル225.Name = "ラベル225";
			this.ラベル225.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.ラベル225.Tag = "";
			this.ラベル225.Text = "入数";
			this.ラベル225.Top = 1.685433f;
			this.ラベル225.Width = 0.3541667f;
			this.ラベル226.Height = 0.2018278f;
			this.ラベル226.HyperLink = null;
			this.ラベル226.Left = 5.024305f;
			this.ラベル226.Name = "ラベル226";
			this.ラベル226.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.ラベル226.Tag = "";
			this.ラベル226.Text = "ケース";
			this.ラベル226.Top = 1.685433f;
			this.ラベル226.Width = 0.4130969f;
			this.ラベル227.Height = 0.1979167f;
			this.ラベル227.HyperLink = null;
			this.ラベル227.Left = 5.437402f;
			this.ラベル227.Name = "ラベル227";
			this.ラベル227.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.ラベル227.Tag = "";
			this.ラベル227.Text = "バラ";
			this.ラベル227.Top = 1.685433f;
			this.ラベル227.Width = 0.6223202f;
			this.ラベル228.Height = 0.1979167f;
			this.ラベル228.HyperLink = null;
			this.ラベル228.Left = 6.045139f;
			this.ラベル228.Name = "ラベル228";
			this.ラベル228.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.ラベル228.Tag = "";
			this.ラベル228.Text = "単\u3000価";
			this.ラベル228.Top = 1.685433f;
			this.ラベル228.Width = 0.8229167f;
			this.ラベル229.Height = 0.1979167f;
			this.ラベル229.HyperLink = null;
			this.ラベル229.Left = 6.868055f;
			this.ラベル229.Name = "ラベル229";
			this.ラベル229.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.ラベル229.Tag = "";
			this.ラベル229.Text = "金\u3000額";
			this.ラベル229.Top = 1.685433f;
			this.ラベル229.Width = 0.8882453f;
			this.テキスト230.DataField = "ITEM14";
			this.テキスト230.Height = 0.1875f;
			this.テキスト230.Left = 0.1701385f;
			this.テキスト230.Name = "テキスト230";
			this.テキスト230.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト230.Tag = "";
			this.テキスト230.Text = "ITEM14";
			this.テキスト230.Top = 1.94698f;
			this.テキスト230.Width = 0.6770833f;
			this.テキスト232.DataField = "ITEM16";
			this.テキスト232.Height = 0.1875f;
			this.テキスト232.Left = 3.464174f;
			this.テキスト232.Name = "テキスト232";
			this.テキスト232.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト232.Tag = "";
			this.テキスト232.Text = "ITEM16";
			this.テキスト232.Top = 1.94698f;
			this.テキスト232.Width = 0.8830483f;
			this.テキスト233.DataField = "ITEM17";
			this.テキスト233.Height = 0.1875f;
			this.テキスト233.Left = 4.430555f;
			this.テキスト233.Name = "テキスト233";
			this.テキスト233.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト233.Tag = "";
			this.テキスト233.Text = "ITEM17";
			this.テキスト233.Top = 1.94698f;
			this.テキスト233.Width = 0.2291667f;
			this.テキスト234.DataField = "ITEM18";
			this.テキスト234.Height = 0.1875f;
			this.テキスト234.Left = 4.743055f;
			this.テキスト234.Name = "テキスト234";
			this.テキスト234.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト234.Tag = "";
			this.テキスト234.Text = "ITEM18";
			this.テキスト234.Top = 1.94698f;
			this.テキスト234.Width = 0.2395833f;
			this.テキスト235.DataField = "ITEM19";
			this.テキスト235.Height = 0.1875f;
			this.テキスト235.Left = 5.055555f;
			this.テキスト235.Name = "テキスト235";
			this.テキスト235.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト235.Tag = "";
			this.テキスト235.Text = "ITEM19";
			this.テキスト235.Top = 1.94698f;
			this.テキスト235.Width = 0.3452328f;
			this.テキスト236.DataField = "ITEM20";
			this.テキスト236.Height = 0.1875f;
			this.テキスト236.Left = 5.450394f;
			this.テキスト236.Name = "テキスト236";
			this.テキスト236.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト236.Tag = "";
			this.テキスト236.Text = "ITEM20";
			this.テキスト236.Top = 1.946851f;
			this.テキスト236.Width = 0.5728345f;
			this.テキスト237.DataField = "ITEM21";
			this.テキスト237.Height = 0.1875f;
			this.テキスト237.Left = 6.079167f;
			this.テキスト237.Name = "テキスト237";
			this.テキスト237.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト237.Tag = "";
			this.テキスト237.Text = "ITEM21";
			this.テキスト237.Top = 1.94698f;
			this.テキスト237.Width = 0.7604167f;
			this.テキスト238.DataField = "ITEM22";
			this.テキスト238.Height = 0.1875f;
			this.テキスト238.Left = 6.909721f;
			this.テキスト238.Name = "テキスト238";
			this.テキスト238.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト238.Tag = "";
			this.テキスト238.Text = "ITEM22";
			this.テキスト238.Top = 1.94698f;
			this.テキスト238.Width = 0.7604167f;
			this.テキスト331.DataField = "ITEM23";
			this.テキスト331.Height = 0.1805556f;
			this.テキスト331.Left = 0.1652774f;
			this.テキスト331.Name = "テキスト331";
			this.テキスト331.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト331.Tag = "";
			this.テキスト331.Text = "ITEM23";
			this.テキスト331.Top = 2.262258f;
			this.テキスト331.Width = 0.6770833f;
			this.テキスト333.DataField = "ITEM25";
			this.テキスト333.Height = 0.1805556f;
			this.テキスト333.Left = 3.464174f;
			this.テキスト333.Name = "テキスト333";
			this.テキスト333.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト333.Tag = "";
			this.テキスト333.Text = "ITEM25";
			this.テキスト333.Top = 2.262258f;
			this.テキスト333.Width = 0.8781869f;
			this.テキスト334.DataField = "ITEM26";
			this.テキスト334.Height = 0.1805556f;
			this.テキスト334.Left = 4.425694f;
			this.テキスト334.Name = "テキスト334";
			this.テキスト334.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト334.Tag = "";
			this.テキスト334.Text = "ITEM26";
			this.テキスト334.Top = 2.262258f;
			this.テキスト334.Width = 0.2291667f;
			this.テキスト335.DataField = "ITEM27";
			this.テキスト335.Height = 0.1805556f;
			this.テキスト335.Left = 4.738194f;
			this.テキスト335.Name = "テキスト335";
			this.テキスト335.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト335.Tag = "";
			this.テキスト335.Text = "ITEM27";
			this.テキスト335.Top = 2.262258f;
			this.テキスト335.Width = 0.2395833f;
			this.テキスト336.DataField = "ITEM28";
			this.テキスト336.Height = 0.1805556f;
			this.テキスト336.Left = 5.055555f;
			this.テキスト336.Name = "テキスト336";
			this.テキスト336.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト336.Tag = "";
			this.テキスト336.Text = "ITEM28";
			this.テキスト336.Top = 2.262258f;
			this.テキスト336.Width = 0.3452328f;
			this.テキスト337.DataField = "ITEM29";
			this.テキスト337.Height = 0.1874016f;
			this.テキスト337.Left = 5.450394f;
			this.テキスト337.Name = "テキスト337";
			this.テキスト337.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト337.Tag = "";
			this.テキスト337.Text = "ITEM29";
			this.テキスト337.Top = 2.262258f;
			this.テキスト337.Width = 0.5728345f;
			this.テキスト338.DataField = "ITEM30";
			this.テキスト338.Height = 0.1805556f;
			this.テキスト338.Left = 6.074306f;
			this.テキスト338.Name = "テキスト338";
			this.テキスト338.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト338.Tag = "";
			this.テキスト338.Text = "ITEM30";
			this.テキスト338.Top = 2.262258f;
			this.テキスト338.Width = 0.7604167f;
			this.テキスト339.DataField = "ITEM31";
			this.テキスト339.Height = 0.1805556f;
			this.テキスト339.Left = 6.904861f;
			this.テキスト339.Name = "テキスト339";
			this.テキスト339.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト339.Tag = "";
			this.テキスト339.Text = "ITEM31";
			this.テキスト339.Top = 2.262258f;
			this.テキスト339.Width = 0.7604167f;
			this.テキスト341.DataField = "ITEM32";
			this.テキスト341.Height = 0.1875f;
			this.テキスト341.Left = 0.1701385f;
			this.テキスト341.Name = "テキスト341";
			this.テキスト341.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト341.Tag = "";
			this.テキスト341.Text = "ITEM32";
			this.テキスト341.Top = 2.60948f;
			this.テキスト341.Width = 0.6770833f;
			this.テキスト343.DataField = "ITEM34";
			this.テキスト343.Height = 0.1875f;
			this.テキスト343.Left = 3.464174f;
			this.テキスト343.Name = "テキスト343";
			this.テキスト343.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト343.Tag = "";
			this.テキスト343.Text = "ITEM34";
			this.テキスト343.Top = 2.60948f;
			this.テキスト343.Width = 0.8830483f;
			this.テキスト344.DataField = "ITEM35";
			this.テキスト344.Height = 0.1875f;
			this.テキスト344.Left = 4.430555f;
			this.テキスト344.Name = "テキスト344";
			this.テキスト344.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト344.Tag = "";
			this.テキスト344.Text = "ITEM35";
			this.テキスト344.Top = 2.60948f;
			this.テキスト344.Width = 0.2291667f;
			this.テキスト345.DataField = "ITEM36";
			this.テキスト345.Height = 0.1875f;
			this.テキスト345.Left = 4.743055f;
			this.テキスト345.Name = "テキスト345";
			this.テキスト345.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト345.Tag = "";
			this.テキスト345.Text = "ITEM36";
			this.テキスト345.Top = 2.60948f;
			this.テキスト345.Width = 0.2395833f;
			this.テキスト346.DataField = "ITEM37";
			this.テキスト346.Height = 0.1875f;
			this.テキスト346.Left = 5.055555f;
			this.テキスト346.Name = "テキスト346";
			this.テキスト346.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト346.Tag = "";
			this.テキスト346.Text = "ITEM37";
			this.テキスト346.Top = 2.60948f;
			this.テキスト346.Width = 0.3452328f;
			this.テキスト347.DataField = "ITEM38";
			this.テキスト347.Height = 0.1875f;
			this.テキスト347.Left = 5.450394f;
			this.テキスト347.Name = "テキスト347";
			this.テキスト347.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト347.Tag = "";
			this.テキスト347.Text = "ITEM38";
			this.テキスト347.Top = 2.60948f;
			this.テキスト347.Width = 0.5728345f;
			this.テキスト348.DataField = "ITEM39";
			this.テキスト348.Height = 0.1875f;
			this.テキスト348.Left = 6.079167f;
			this.テキスト348.Name = "テキスト348";
			this.テキスト348.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト348.Tag = "";
			this.テキスト348.Text = "ITEM39";
			this.テキスト348.Top = 2.60948f;
			this.テキスト348.Width = 0.7604167f;
			this.テキスト349.DataField = "ITEM40";
			this.テキスト349.Height = 0.1875f;
			this.テキスト349.Left = 6.909721f;
			this.テキスト349.Name = "テキスト349";
			this.テキスト349.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト349.Tag = "";
			this.テキスト349.Text = "ITEM40";
			this.テキスト349.Top = 2.60948f;
			this.テキスト349.Width = 0.7604167f;
			this.テキスト350.DataField = "ITEM41";
			this.テキスト350.Height = 0.1875f;
			this.テキスト350.Left = 0.1701385f;
			this.テキスト350.Name = "テキスト350";
			this.テキスト350.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト350.Tag = "";
			this.テキスト350.Text = "ITEM41";
			this.テキスト350.Top = 2.944896f;
			this.テキスト350.Width = 0.6770833f;
			this.テキスト352.DataField = "ITEM43";
			this.テキスト352.Height = 0.1875f;
			this.テキスト352.Left = 3.464174f;
			this.テキスト352.Name = "テキスト352";
			this.テキスト352.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト352.Tag = "";
			this.テキスト352.Text = "ITEM43";
			this.テキスト352.Top = 2.944896f;
			this.テキスト352.Width = 0.8830483f;
			this.テキスト353.DataField = "ITEM44";
			this.テキスト353.Height = 0.1875f;
			this.テキスト353.Left = 4.430555f;
			this.テキスト353.Name = "テキスト353";
			this.テキスト353.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト353.Tag = "";
			this.テキスト353.Text = "ITEM44";
			this.テキスト353.Top = 2.944896f;
			this.テキスト353.Width = 0.2291667f;
			this.テキスト354.DataField = "ITEM45";
			this.テキスト354.Height = 0.1875f;
			this.テキスト354.Left = 4.743055f;
			this.テキスト354.Name = "テキスト354";
			this.テキスト354.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト354.Tag = "";
			this.テキスト354.Text = "ITEM45";
			this.テキスト354.Top = 2.944896f;
			this.テキスト354.Width = 0.2395833f;
			this.テキスト355.DataField = "ITEM46";
			this.テキスト355.Height = 0.1875f;
			this.テキスト355.Left = 5.055555f;
			this.テキスト355.Name = "テキスト355";
			this.テキスト355.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト355.Tag = "";
			this.テキスト355.Text = "ITEM46";
			this.テキスト355.Top = 2.944896f;
			this.テキスト355.Width = 0.3452328f;
			this.テキスト356.DataField = "ITEM47";
			this.テキスト356.Height = 0.1875f;
			this.テキスト356.Left = 5.450394f;
			this.テキスト356.Name = "テキスト356";
			this.テキスト356.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト356.Tag = "";
			this.テキスト356.Text = "ITEM47";
			this.テキスト356.Top = 2.944896f;
			this.テキスト356.Width = 0.5728345f;
			this.テキスト357.DataField = "ITEM48";
			this.テキスト357.Height = 0.1875f;
			this.テキスト357.Left = 6.079167f;
			this.テキスト357.Name = "テキスト357";
			this.テキスト357.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト357.Tag = "";
			this.テキスト357.Text = "ITEM48";
			this.テキスト357.Top = 2.944896f;
			this.テキスト357.Width = 0.7604167f;
			this.テキスト358.DataField = "ITEM49";
			this.テキスト358.Height = 0.1875f;
			this.テキスト358.Left = 6.909721f;
			this.テキスト358.Name = "テキスト358";
			this.テキスト358.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト358.Tag = "";
			this.テキスト358.Text = "ITEM49";
			this.テキスト358.Top = 2.944896f;
			this.テキスト358.Width = 0.7604167f;
			this.テキスト360.DataField = "ITEM50";
			this.テキスト360.Height = 0.1875f;
			this.テキスト360.Left = 0.1701385f;
			this.テキスト360.Name = "テキスト360";
			this.テキスト360.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト360.Tag = "";
			this.テキスト360.Text = "ITEM50";
			this.テキスト360.Top = 3.272675f;
			this.テキスト360.Width = 0.6770833f;
			this.テキスト362.DataField = "ITEM52";
			this.テキスト362.Height = 0.1875f;
			this.テキスト362.Left = 3.464174f;
			this.テキスト362.Name = "テキスト362";
			this.テキスト362.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト362.Tag = "";
			this.テキスト362.Text = "ITEM52";
			this.テキスト362.Top = 3.272675f;
			this.テキスト362.Width = 0.8830483f;
			this.テキスト363.DataField = "ITEM53";
			this.テキスト363.Height = 0.1875f;
			this.テキスト363.Left = 4.430555f;
			this.テキスト363.Name = "テキスト363";
			this.テキスト363.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト363.Tag = "";
			this.テキスト363.Text = "ITEM53";
			this.テキスト363.Top = 3.272675f;
			this.テキスト363.Width = 0.2291667f;
			this.テキスト364.DataField = "ITEM54";
			this.テキスト364.Height = 0.1875f;
			this.テキスト364.Left = 4.743055f;
			this.テキスト364.Name = "テキスト364";
			this.テキスト364.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト364.Tag = "";
			this.テキスト364.Text = "ITEM54";
			this.テキスト364.Top = 3.272675f;
			this.テキスト364.Width = 0.2395833f;
			this.テキスト365.DataField = "ITEM55";
			this.テキスト365.Height = 0.1875f;
			this.テキスト365.Left = 5.055555f;
			this.テキスト365.Name = "テキスト365";
			this.テキスト365.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト365.Tag = "";
			this.テキスト365.Text = "ITEM55";
			this.テキスト365.Top = 3.272675f;
			this.テキスト365.Width = 0.3452328f;
			this.テキスト366.DataField = "ITEM56";
			this.テキスト366.Height = 0.1875f;
			this.テキスト366.Left = 5.450394f;
			this.テキスト366.Name = "テキスト366";
			this.テキスト366.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト366.Tag = "";
			this.テキスト366.Text = "ITEM56";
			this.テキスト366.Top = 3.272675f;
			this.テキスト366.Width = 0.5728345f;
			this.テキスト367.DataField = "ITEM57";
			this.テキスト367.Height = 0.1875f;
			this.テキスト367.Left = 6.079167f;
			this.テキスト367.Name = "テキスト367";
			this.テキスト367.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト367.Tag = "";
			this.テキスト367.Text = "ITEM57";
			this.テキスト367.Top = 3.272675f;
			this.テキスト367.Width = 0.7604167f;
			this.テキスト368.DataField = "ITEM58";
			this.テキスト368.Height = 0.1875f;
			this.テキスト368.Left = 6.909721f;
			this.テキスト368.Name = "テキスト368";
			this.テキスト368.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト368.Tag = "";
			this.テキスト368.Text = "ITEM58";
			this.テキスト368.Top = 3.272675f;
			this.テキスト368.Width = 0.7604167f;
			this.テキスト369.DataField = "ITEM59";
			this.テキスト369.Height = 0.1875f;
			this.テキスト369.Left = 0.1701385f;
			this.テキスト369.Name = "テキスト369";
			this.テキスト369.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト369.Tag = "";
			this.テキスト369.Text = "ITEM59";
			this.テキスト369.Top = 3.610869f;
			this.テキスト369.Width = 0.6770833f;
			this.テキスト371.DataField = "ITEM61";
			this.テキスト371.Height = 0.1875f;
			this.テキスト371.Left = 3.464174f;
			this.テキスト371.Name = "テキスト371";
			this.テキスト371.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト371.Tag = "";
			this.テキスト371.Text = "ITEM61";
			this.テキスト371.Top = 3.610869f;
			this.テキスト371.Width = 0.8830483f;
			this.テキスト372.DataField = "ITEM62";
			this.テキスト372.Height = 0.1875f;
			this.テキスト372.Left = 4.430555f;
			this.テキスト372.Name = "テキスト372";
			this.テキスト372.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト372.Tag = "";
			this.テキスト372.Text = "ITEM62";
			this.テキスト372.Top = 3.610869f;
			this.テキスト372.Width = 0.2291667f;
			this.テキスト373.DataField = "ITEM63";
			this.テキスト373.Height = 0.1875f;
			this.テキスト373.Left = 4.743055f;
			this.テキスト373.Name = "テキスト373";
			this.テキスト373.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト373.Tag = "";
			this.テキスト373.Text = "ITEM63";
			this.テキスト373.Top = 3.610869f;
			this.テキスト373.Width = 0.2395833f;
			this.テキスト374.DataField = "ITEM64";
			this.テキスト374.Height = 0.1875f;
			this.テキスト374.Left = 5.055555f;
			this.テキスト374.Name = "テキスト374";
			this.テキスト374.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト374.Tag = "";
			this.テキスト374.Text = "ITEM64";
			this.テキスト374.Top = 3.610869f;
			this.テキスト374.Width = 0.3452328f;
			this.テキスト375.DataField = "ITEM65";
			this.テキスト375.Height = 0.1875f;
			this.テキスト375.Left = 5.450394f;
			this.テキスト375.Name = "テキスト375";
			this.テキスト375.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト375.Tag = "";
			this.テキスト375.Text = "ITEM65";
			this.テキスト375.Top = 3.610869f;
			this.テキスト375.Width = 0.5728345f;
			this.テキスト376.DataField = "ITEM66";
			this.テキスト376.Height = 0.1875f;
			this.テキスト376.Left = 6.079167f;
			this.テキスト376.Name = "テキスト376";
			this.テキスト376.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト376.Tag = "";
			this.テキスト376.Text = "ITEM66";
			this.テキスト376.Top = 3.610869f;
			this.テキスト376.Width = 0.7604167f;
			this.テキスト377.DataField = "ITEM67";
			this.テキスト377.Height = 0.1875f;
			this.テキスト377.Left = 6.909721f;
			this.テキスト377.Name = "テキスト377";
			this.テキスト377.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト377.Tag = "";
			this.テキスト377.Text = "ITEM67";
			this.テキスト377.Top = 3.610869f;
			this.テキスト377.Width = 0.7604167f;
			this.テキスト379.DataField = "ITEM68";
			this.テキスト379.Height = 0.1875f;
			this.テキスト379.Left = 0.1701385f;
			this.テキスト379.Name = "テキスト379";
			this.テキスト379.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト379.Tag = "";
			this.テキスト379.Text = "ITEM68";
			this.テキスト379.Top = 3.923369f;
			this.テキスト379.Width = 0.6770833f;
			this.テキスト381.DataField = "ITEM70";
			this.テキスト381.Height = 0.1875f;
			this.テキスト381.Left = 3.464174f;
			this.テキスト381.Name = "テキスト381";
			this.テキスト381.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト381.Tag = "";
			this.テキスト381.Text = "ITEM70";
			this.テキスト381.Top = 3.923369f;
			this.テキスト381.Width = 0.8830483f;
			this.テキスト382.DataField = "ITEM71";
			this.テキスト382.Height = 0.1875f;
			this.テキスト382.Left = 4.430555f;
			this.テキスト382.Name = "テキスト382";
			this.テキスト382.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト382.Tag = "";
			this.テキスト382.Text = "ITEM71";
			this.テキスト382.Top = 3.923369f;
			this.テキスト382.Width = 0.2291667f;
			this.テキスト383.DataField = "ITEM72";
			this.テキスト383.Height = 0.1875f;
			this.テキスト383.Left = 4.743055f;
			this.テキスト383.Name = "テキスト383";
			this.テキスト383.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト383.Tag = "";
			this.テキスト383.Text = "ITEM72";
			this.テキスト383.Top = 3.923369f;
			this.テキスト383.Width = 0.2395833f;
			this.テキスト384.DataField = "ITEM73";
			this.テキスト384.Height = 0.1875f;
			this.テキスト384.Left = 5.055555f;
			this.テキスト384.Name = "テキスト384";
			this.テキスト384.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト384.Tag = "";
			this.テキスト384.Text = "ITEM73";
			this.テキスト384.Top = 3.923369f;
			this.テキスト384.Width = 0.3452328f;
			this.テキスト385.DataField = "ITEM74";
			this.テキスト385.Height = 0.1875f;
			this.テキスト385.Left = 5.450394f;
			this.テキスト385.Name = "テキスト385";
			this.テキスト385.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト385.Tag = "";
			this.テキスト385.Text = "ITEM74";
			this.テキスト385.Top = 3.923369f;
			this.テキスト385.Width = 0.5728345f;
			this.テキスト386.DataField = "ITEM75";
			this.テキスト386.Height = 0.1875f;
			this.テキスト386.Left = 6.079167f;
			this.テキスト386.Name = "テキスト386";
			this.テキスト386.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト386.Tag = "";
			this.テキスト386.Text = "ITEM75";
			this.テキスト386.Top = 3.923369f;
			this.テキスト386.Width = 0.7604167f;
			this.テキスト387.DataField = "ITEM76";
			this.テキスト387.Height = 0.1875f;
			this.テキスト387.Left = 6.909721f;
			this.テキスト387.Name = "テキスト387";
			this.テキスト387.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト387.Tag = "";
			this.テキスト387.Text = "ITEM76";
			this.テキスト387.Top = 3.923369f;
			this.テキスト387.Width = 0.7604167f;
			this.テキスト388.DataField = "ITEM77";
			this.テキスト388.Height = 0.1875f;
			this.テキスト388.Left = 0.1701385f;
			this.テキスト388.Name = "テキスト388";
			this.テキスト388.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト388.Tag = "";
			this.テキスト388.Text = "ITEM77";
			this.テキスト388.Top = 4.26573f;
			this.テキスト388.Width = 0.6770833f;
			this.テキスト390.DataField = "ITEM79";
			this.テキスト390.Height = 0.1875f;
			this.テキスト390.Left = 3.464174f;
			this.テキスト390.Name = "テキスト390";
			this.テキスト390.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト390.Tag = "";
			this.テキスト390.Text = "ITEM79";
			this.テキスト390.Top = 4.26573f;
			this.テキスト390.Width = 0.8830483f;
			this.テキスト391.DataField = "ITEM80";
			this.テキスト391.Height = 0.1875f;
			this.テキスト391.Left = 4.430555f;
			this.テキスト391.Name = "テキスト391";
			this.テキスト391.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト391.Tag = "";
			this.テキスト391.Text = "ITEM80";
			this.テキスト391.Top = 4.26573f;
			this.テキスト391.Width = 0.2291667f;
			this.テキスト392.DataField = "ITEM81";
			this.テキスト392.Height = 0.1875f;
			this.テキスト392.Left = 4.743055f;
			this.テキスト392.Name = "テキスト392";
			this.テキスト392.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト392.Tag = "";
			this.テキスト392.Text = "ITEM81";
			this.テキスト392.Top = 4.26573f;
			this.テキスト392.Width = 0.2395833f;
			this.テキスト393.DataField = "ITEM82";
			this.テキスト393.Height = 0.1875f;
			this.テキスト393.Left = 5.055555f;
			this.テキスト393.Name = "テキスト393";
			this.テキスト393.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト393.Tag = "";
			this.テキスト393.Text = "ITEM82";
			this.テキスト393.Top = 4.26573f;
			this.テキスト393.Width = 0.3452328f;
			this.テキスト394.DataField = "ITEM83";
			this.テキスト394.Height = 0.1875f;
			this.テキスト394.Left = 5.450394f;
			this.テキスト394.Name = "テキスト394";
			this.テキスト394.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト394.Tag = "";
			this.テキスト394.Text = "ITEM83";
			this.テキスト394.Top = 4.26573f;
			this.テキスト394.Width = 0.5728345f;
			this.テキスト395.DataField = "ITEM84";
			this.テキスト395.Height = 0.1875f;
			this.テキスト395.Left = 6.079167f;
			this.テキスト395.Name = "テキスト395";
			this.テキスト395.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト395.Tag = "";
			this.テキスト395.Text = "ITEM84";
			this.テキスト395.Top = 4.26573f;
			this.テキスト395.Width = 0.7604167f;
			this.テキスト396.DataField = "ITEM85";
			this.テキスト396.Height = 0.1875f;
			this.テキスト396.Left = 6.909721f;
			this.テキスト396.Name = "テキスト396";
			this.テキスト396.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト396.Tag = "";
			this.テキスト396.Text = "ITEM85";
			this.テキスト396.Top = 4.26573f;
			this.テキスト396.Width = 0.7604167f;
			this.ボックス309.Border.BottomStyle = BorderLineStyle.Solid;
			this.ボックス309.Border.LeftStyle = BorderLineStyle.Solid;
			this.ボックス309.Border.RightStyle = BorderLineStyle.Solid;
			this.ボックス309.Border.TopStyle = BorderLineStyle.Solid;
			this.ボックス309.Height = 0.34375f;
			this.ボックス309.Left = 3.785433f;
			this.ボックス309.Name = "ボックス309";
			this.ボックス309.RoundingRadius = 9.999999f;
			this.ボックス309.Tag = "";
			this.ボックス309.Top = 4.881702f;
			this.ボックス309.Width = 3.970867f;
			this.ボックス310.BackColor = Color.FromArgb(170, 255, 255);
			this.ボックス310.Border.BottomStyle = BorderLineStyle.Solid;
			this.ボックス310.Border.LeftStyle = BorderLineStyle.Solid;
			this.ボックス310.Border.RightStyle = BorderLineStyle.Solid;
			this.ボックス310.Border.TopStyle = BorderLineStyle.Solid;
			this.ボックス310.Height = 0.2395833f;
			this.ボックス310.Left = 3.785433f;
			this.ボックス310.Name = "ボックス310";
			this.ボックス310.RoundingRadius = 9.999999f;
			this.ボックス310.Tag = "";
			this.ボックス310.Top = 4.642119f;
			this.ボックス310.Width = 3.970867f;
			this.テキスト311.DataField = "ITEM86";
			this.テキスト311.Height = 0.1875f;
			this.テキスト311.Left = 3.840551f;
			this.テキスト311.Name = "テキスト311";
			this.テキスト311.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.テキスト311.Tag = "";
			this.テキスト311.Text = "ITEM86";
			this.テキスト311.Top = 4.683942f;
			this.テキスト311.Width = 1.000394f;
			this.テキスト312.DataField = "ITEM87";
			this.テキスト312.Height = 0.1875f;
			this.テキスト312.Left = 4.929528f;
			this.テキスト312.Name = "テキスト312";
			this.テキスト312.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.テキスト312.Tag = "";
			this.テキスト312.Text = "ITEM87";
			this.テキスト312.Top = 4.683942f;
			this.テキスト312.Width = 0.8763778f;
			this.テキスト313.DataField = "ITEM88";
			this.テキスト313.Height = 0.1875f;
			this.テキスト313.Left = 5.95486f;
			this.テキスト313.Name = "テキスト313";
			this.テキスト313.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.テキスト313.Tag = "";
			this.テキスト313.Text = "ITEM88";
			this.テキスト313.Top = 4.683785f;
			this.テキスト313.Width = 0.7916667f;
			this.テキスト314.DataField = "ITEM89";
			this.テキスト314.Height = 0.1875f;
			this.テキスト314.Left = 6.864583f;
			this.テキスト314.Name = "テキスト314";
			this.テキスト314.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.テキスト314.Tag = "";
			this.テキスト314.Text = "ITEM89";
			this.テキスト314.Top = 4.683785f;
			this.テキスト314.Width = 0.7916667f;
			this.テキスト315.DataField = "ITEM90";
			this.テキスト315.Height = 0.1875f;
			this.テキスト315.Left = 3.840551f;
			this.テキスト315.Name = "テキスト315";
			this.テキスト315.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト315.Tag = "";
			this.テキスト315.Text = "ITEM90";
			this.テキスト315.Top = 4.954809f;
			this.テキスト315.Width = 1.000394f;
			this.テキスト316.DataField = "ITEM91";
			this.テキスト316.Height = 0.1875f;
			this.テキスト316.Left = 4.929528f;
			this.テキスト316.Name = "テキスト316";
			this.テキスト316.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト316.Tag = "";
			this.テキスト316.Text = "ITEM91";
			this.テキスト316.Top = 4.954809f;
			this.テキスト316.Width = 0.8763778f;
			this.テキスト317.DataField = "ITEM92";
			this.テキスト317.Height = 0.1875f;
			this.テキスト317.Left = 5.95486f;
			this.テキスト317.Name = "テキスト317";
			this.テキスト317.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト317.Tag = "";
			this.テキスト317.Text = "ITEM92";
			this.テキスト317.Top = 4.954619f;
			this.テキスト317.Width = 0.7916667f;
			this.テキスト318.DataField = "ITEM93";
			this.テキスト318.Height = 0.1875f;
			this.テキスト318.Left = 6.864583f;
			this.テキスト318.Name = "テキスト318";
			this.テキスト318.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト318.Tag = "";
			this.テキスト318.Text = "ITEM93";
			this.テキスト318.Top = 4.954619f;
			this.テキスト318.Width = 0.7916667f;
			this.直線407.Height = 0.003937006f;
			this.直線407.Left = 0.09291339f;
			this.直線407.LineWeight = 0f;
			this.直線407.Name = "直線407";
			this.直線407.Tag = "";
			this.直線407.Top = 1.87126f;
			this.直線407.Width = 7.666929f;
			this.直線407.X1 = 0.09291339f;
			this.直線407.X2 = 7.759843f;
			this.直線407.Y1 = 1.87126f;
			this.直線407.Y2 = 1.875197f;
			this.ラベル414.Border.BottomStyle = BorderLineStyle.Solid;
			this.ラベル414.Border.LeftStyle = BorderLineStyle.Solid;
			this.ラベル414.Border.RightStyle = BorderLineStyle.Solid;
			this.ラベル414.Border.TopStyle = BorderLineStyle.Solid;
			this.ラベル414.Height = 0.3951389f;
			this.ラベル414.HyperLink = null;
			this.ラベル414.Left = 3.098611f;
			this.ラベル414.Name = "ラベル414";
			this.ラベル414.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.ラベル414.Tag = "";
			this.ラベル414.Text = "\u3000";
			this.ラベル414.Top = 5.902536f;
			this.ラベル414.Width = 1.811111f;
			this.テキスト415.DataField = "ITEM13";
			this.テキスト415.Height = 0.1875f;
			this.テキスト415.Left = 7.295139f;
			this.テキスト415.Name = "テキスト415";
			this.テキスト415.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト415.Tag = "";
			this.テキスト415.Text = "ITEM13";
			this.テキスト415.Top = 6.094897f;
			this.テキスト415.Width = 0.3229167f;
			this.テキスト416.DataField = "ITEM02";
			this.テキスト416.Height = 0.1979167f;
			this.テキスト416.Left = 6.590972f;
			this.テキスト416.Name = "テキスト416";
			this.テキスト416.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト416.Tag = "";
			this.テキスト416.Text = "ITEM02";
			this.テキスト416.Top = 6.094897f;
			this.テキスト416.Width = 0.59375f;
			this.テキスト417.DataField = "ITEM01";
			this.テキスト417.Height = 0.1875f;
			this.テキスト417.Left = 6.444489f;
			this.テキスト417.Name = "テキスト417";
			this.テキスト417.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト417.Tag = "";
			this.テキスト417.Text = "ITEM01";
			this.テキスト417.Top = 5.870473f;
			this.テキスト417.Width = 1.34375f;
			this.ラベル418.Height = 0.1972222f;
			this.ラベル418.HyperLink = null;
			this.ラベル418.Left = 7.60625f;
			this.ラベル418.Name = "ラベル418";
			this.ラベル418.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル418.Tag = "";
			this.ラベル418.Text = "頁";
			this.ラベル418.Top = 6.094897f;
			this.ラベル418.Width = 0.1715278f;
			this.ラベル420.Height = 0.1979167f;
			this.ラベル420.HyperLink = null;
			this.ラベル420.Left = 5.722221f;
			this.ラベル420.Name = "ラベル420";
			this.ラベル420.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル420.Tag = "";
			this.ラベル420.Text = "伝票番号：";
			this.ラベル420.Top = 6.094897f;
			this.ラベル420.Width = 0.8333333f;
			this.テキスト422.DataField = "ITEM04";
			this.テキスト422.Height = 0.1875f;
			this.テキスト422.Left = 0.1319441f;
			this.テキスト422.Name = "テキスト422";
			this.テキスト422.Style = "color: Black; font-family: MS UI Gothic; font-size: 14.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト422.Tag = "";
			this.テキスト422.Text = "ITEM04";
			this.テキスト422.Top = 6.587481f;
			this.テキスト422.Width = 2.375f;
			this.直線423.Height = 0f;
			this.直線423.Left = 0.09722185f;
			this.直線423.LineWeight = 0f;
			this.直線423.Name = "直線423";
			this.直線423.Tag = "";
			this.直線423.Top = 6.798369f;
			this.直線423.Width = 2.710417f;
			this.直線423.X1 = 0.09722185f;
			this.直線423.X2 = 2.807639f;
			this.直線423.Y1 = 6.798369f;
			this.直線423.Y2 = 6.798369f;
			this.ラベル425.Height = 0.15625f;
			this.ラベル425.HyperLink = null;
			this.ラベル425.Left = 0.1319441f;
			this.ラベル425.Name = "ラベル425";
			this.ラベル425.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル425.Tag = "";
			this.ラベル425.Text = "TEL";
			this.ラベル425.Top = 6.835175f;
			this.ラベル425.Visible = false;
			this.ラベル425.Width = 0.2395833f;
			this.テキスト426.DataField = "ITEM05";
			this.テキスト426.Height = 0.15625f;
			this.テキスト426.Left = 0.3680552f;
			this.テキスト426.Name = "テキスト426";
			this.テキスト426.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト426.Tag = "";
			this.テキスト426.Text = "ITEM05";
			this.テキスト426.Top = 6.840036f;
			this.テキスト426.Visible = false;
			this.テキスト426.Width = 0.9479167f;
			this.ラベル427.Height = 0.15625f;
			this.ラベル427.HyperLink = null;
			this.ラベル427.Left = 1.315972f;
			this.ラベル427.Name = "ラベル427";
			this.ラベル427.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル427.Tag = "";
			this.ラベル427.Text = "FAX";
			this.ラベル427.Top = 6.835175f;
			this.ラベル427.Visible = false;
			this.ラベル427.Width = 0.2395833f;
			this.テキスト428.DataField = "ITEM06";
			this.テキスト428.Height = 0.15625f;
			this.テキスト428.Left = 1.552083f;
			this.テキスト428.Name = "テキスト428";
			this.テキスト428.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト428.Tag = "";
			this.テキスト428.Text = "ITEM06";
			this.テキスト428.Top = 6.835869f;
			this.テキスト428.Visible = false;
			this.テキスト428.Width = 0.9791667f;
			this.テキスト429.DataField = "ITEM07";
			this.テキスト429.Height = 0.1875f;
			this.テキスト429.Left = 5.565972f;
			this.テキスト429.Name = "テキスト429";
			this.テキスト429.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.テキスト429.Tag = "";
			this.テキスト429.Text = "ITEM07";
			this.テキスト429.Top = 6.32823f;
			this.テキスト429.Width = 2.21875f;
			this.テキスト430.DataField = "ITEM08";
			this.テキスト430.Height = 0.1875f;
			this.テキスト430.Left = 5.565972f;
			this.テキスト430.Name = "テキスト430";
			this.テキスト430.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 1";
			this.テキスト430.Tag = "";
			this.テキスト430.Text = "ITEM08";
			this.テキスト430.Top = 6.527536f;
			this.テキスト430.Width = 2.21875f;
			this.テキスト433.DataField = "ITEM11";
			this.テキスト433.Height = 0.1875f;
			this.テキスト433.Left = 3.097222f;
			this.テキスト433.Name = "テキスト433";
			this.テキスト433.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.テキスト433.Tag = "";
			this.テキスト433.Text = "ITEM11";
			this.テキスト433.Top = 7.042119f;
			this.テキスト433.Width = 1.811111f;
			this.ラベル434.Height = 0.15625f;
			this.ラベル434.HyperLink = null;
			this.ラベル434.Left = 6.003471f;
			this.ラベル434.Name = "ラベル434";
			this.ラベル434.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.ラベル434.Tag = "";
			this.ラベル434.Text = "担当：";
			this.ラベル434.Top = 7.069201f;
			this.ラベル434.Width = 0.4791667f;
			this.テキスト435.DataField = "ITEM12";
			this.テキスト435.Height = 0.15625f;
			this.テキスト435.Left = 6.470139f;
			this.テキスト435.Name = "テキスト435";
			this.テキスト435.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト435.Tag = "";
			this.テキスト435.Text = "ITEM12";
			this.テキスト435.Top = 7.069201f;
			this.テキスト435.Width = 1.302083f;
			this.ラベル535.Height = 0.2291667f;
			this.ラベル535.HyperLink = null;
			this.ラベル535.Left = 3.194488f;
			this.ラベル535.Name = "ラベル535";
			this.ラベル535.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.ラベル535.Tag = "";
			this.ラベル535.Text = "納品・請求書";
			this.ラベル535.Top = 5.981008f;
			this.ラベル535.Width = 1.614567f;
			this.テキスト642.DataField = "ITEM94";
			this.テキスト642.Height = 0.1875f;
			this.テキスト642.Left = 0.1319441f;
			this.テキスト642.Name = "テキスト642";
			this.テキスト642.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト642.Tag = "";
			this.テキスト642.Text = "ITEM94";
			this.テキスト642.Top = 0.4198969f;
			this.テキスト642.Width = 2.375f;
			this.テキスト643.DataField = "ITEM95";
			this.テキスト643.Height = 0.1875f;
			this.テキスト643.Left = 0.1319441f;
			this.テキスト643.Name = "テキスト643";
			this.テキスト643.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.テキスト643.Tag = "";
			this.テキスト643.Text = "ITEM95";
			this.テキスト643.Top = 0.6164247f;
			this.テキスト643.Width = 2.375f;
			this.textBox73.DataField = "ITEM03";
			this.textBox73.Height = 0.1875f;
			this.textBox73.Left = 0.1318896f;
			this.textBox73.Name = "textBox73";
			this.textBox73.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox73.Tag = "";
			this.textBox73.Text = "ITEM03";
			this.textBox73.Top = 5.805596f;
			this.textBox73.Visible = false;
			this.textBox73.Width = 1.34375f;
			this.textBox74.DataField = "ITEM94";
			this.textBox74.Height = 0.1875f;
			this.textBox74.Left = 0.1318896f;
			this.textBox74.Name = "textBox74";
			this.textBox74.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox74.Tag = "";
			this.textBox74.Text = "ITEM94";
			this.textBox74.Top = 6.000041f;
			this.textBox74.Width = 2.375f;
			this.textBox75.DataField = "ITEM95";
			this.textBox75.Height = 0.1875f;
			this.textBox75.Left = 0.1318896f;
			this.textBox75.Name = "textBox75";
			this.textBox75.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox75.Tag = "";
			this.textBox75.Text = "ITEM95";
			this.textBox75.Top = 6.196568f;
			this.textBox75.Width = 2.375f;
			this.shape10.Border.BottomStyle = BorderLineStyle.Solid;
			this.shape10.Border.LeftStyle = BorderLineStyle.Solid;
			this.shape10.Border.RightStyle = BorderLineStyle.Solid;
			this.shape10.Border.TopStyle = BorderLineStyle.Solid;
			this.shape10.Height = 0.34375f;
			this.shape10.Left = 3.785039f;
			this.shape10.Name = "shape10";
			this.shape10.RoundingRadius = 9.999999f;
			this.shape10.Tag = "";
			this.shape10.Top = 10.47529f;
			this.shape10.Width = 3.962206f;
			this.shape11.BackColor = Color.FromArgb(170, 255, 255);
			this.shape11.Border.BottomStyle = BorderLineStyle.Solid;
			this.shape11.Border.LeftStyle = BorderLineStyle.Solid;
			this.shape11.Border.RightStyle = BorderLineStyle.Solid;
			this.shape11.Border.TopStyle = BorderLineStyle.Solid;
			this.shape11.Height = 0.2395833f;
			this.shape11.Left = 3.785039f;
			this.shape11.Name = "shape11";
			this.shape11.RoundingRadius = 9.999999f;
			this.shape11.Tag = "";
			this.shape11.Top = 10.2357f;
			this.shape11.Width = 3.962206f;
			this.textBox76.DataField = "ITEM86";
			this.textBox76.Height = 0.1875f;
			this.textBox76.Left = 3.840158f;
			this.textBox76.Name = "textBox76";
			this.textBox76.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.textBox76.Tag = "";
			this.textBox76.Text = "ITEM86";
			this.textBox76.Top = 10.27752f;
			this.textBox76.Width = 1.000393f;
			this.textBox77.DataField = "ITEM87";
			this.textBox77.Height = 0.1875f;
			this.textBox77.Left = 4.929528f;
			this.textBox77.Name = "textBox77";
			this.textBox77.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.textBox77.Tag = "";
			this.textBox77.Text = "ITEM87";
			this.textBox77.Top = 10.27725f;
			this.textBox77.Width = 0.8763778f;
			this.textBox78.DataField = "ITEM88";
			this.textBox78.Height = 0.1875f;
			this.textBox78.Left = 5.954471f;
			this.textBox78.Name = "textBox78";
			this.textBox78.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.textBox78.Tag = "";
			this.textBox78.Text = "ITEM88";
			this.textBox78.Top = 10.27737f;
			this.textBox78.Width = 0.7916667f;
			this.textBox79.DataField = "ITEM89";
			this.textBox79.Height = 0.1875f;
			this.textBox79.Left = 6.864193f;
			this.textBox79.Name = "textBox79";
			this.textBox79.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.textBox79.Tag = "";
			this.textBox79.Text = "ITEM89";
			this.textBox79.Top = 10.27737f;
			this.textBox79.Width = 0.7916667f;
			this.textBox80.DataField = "ITEM90";
			this.textBox80.Height = 0.1875f;
			this.textBox80.Left = 3.840158f;
			this.textBox80.Name = "textBox80";
			this.textBox80.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox80.Tag = "";
			this.textBox80.Text = "ITEM90";
			this.textBox80.Top = 10.54839f;
			this.textBox80.Width = 1.000393f;
			this.textBox81.DataField = "ITEM91";
			this.textBox81.Height = 0.1875f;
			this.textBox81.Left = 4.929528f;
			this.textBox81.Name = "textBox81";
			this.textBox81.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox81.Tag = "";
			this.textBox81.Text = "ITEM91";
			this.textBox81.Top = 10.54812f;
			this.textBox81.Width = 0.8763778f;
			this.textBox82.DataField = "ITEM92";
			this.textBox82.Height = 0.1875f;
			this.textBox82.Left = 5.954471f;
			this.textBox82.Name = "textBox82";
			this.textBox82.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox82.Tag = "";
			this.textBox82.Text = "ITEM92";
			this.textBox82.Top = 10.5482f;
			this.textBox82.Width = 0.7916667f;
			this.textBox83.DataField = "ITEM93";
			this.textBox83.Height = 0.1875f;
			this.textBox83.Left = 6.864193f;
			this.textBox83.Name = "textBox83";
			this.textBox83.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox83.Tag = "";
			this.textBox83.Text = "ITEM93";
			this.textBox83.Top = 10.5482f;
			this.textBox83.Width = 0.7916667f;
			this.line3.Height = 2.847605f;
			this.line3.Left = 0.8854164f;
			this.line3.LineWeight = 1f;
			this.line3.Name = "line3";
			this.line3.Top = 1.673312f;
			this.line3.Width = 0.003559828f;
			this.line3.X1 = 0.8889762f;
			this.line3.X2 = 0.8854164f;
			this.line3.Y1 = 1.673312f;
			this.line3.Y2 = 4.520917f;
			this.line4.Height = 2.83973f;
			this.line4.Left = 3.350771f;
			this.line4.LineWeight = 1f;
			this.line4.Name = "line4";
			this.line4.Top = 1.677249f;
			this.line4.Width = 0.003560066f;
			this.line4.X1 = 3.354331f;
			this.line4.X2 = 3.350771f;
			this.line4.Y1 = 1.677249f;
			this.line4.Y2 = 4.516979f;
			this.line5.Height = 2.847605f;
			this.line5.Left = 4.381086f;
			this.line5.LineWeight = 1f;
			this.line5.Name = "line5";
			this.line5.Top = 1.673312f;
			this.line5.Width = 0.003560066f;
			this.line5.X1 = 4.384646f;
			this.line5.X2 = 4.381086f;
			this.line5.Y1 = 1.673312f;
			this.line5.Y2 = 4.520917f;
			this.line6.Height = 2.847605f;
			this.line6.Left = 4.698016f;
			this.line6.LineWeight = 1f;
			this.line6.Name = "line6";
			this.line6.Top = 1.673312f;
			this.line6.Width = 0.003560066f;
			this.line6.X1 = 4.701576f;
			this.line6.X2 = 4.698016f;
			this.line6.Y1 = 1.673312f;
			this.line6.Y2 = 4.520917f;
			this.line7.Height = 2.847605f;
			this.line7.Left = 5.016126f;
			this.line7.LineWeight = 1f;
			this.line7.Name = "line7";
			this.line7.Top = 1.673312f;
			this.line7.Width = 0.003560066f;
			this.line7.X1 = 5.019686f;
			this.line7.X2 = 5.016126f;
			this.line7.Y1 = 1.673312f;
			this.line7.Y2 = 4.520917f;
			this.line8.Height = 2.847604f;
			this.line8.Left = 5.437385f;
			this.line8.LineWeight = 1f;
			this.line8.Name = "line8";
			this.line8.Top = 1.673229f;
			this.line8.Width = 0.003560066f;
			this.line8.X1 = 5.440945f;
			this.line8.X2 = 5.437385f;
			this.line8.Y1 = 1.673229f;
			this.line8.Y2 = 4.520833f;
			this.line9.Height = 2.847605f;
			this.line9.Left = 6.036992f;
			this.line9.LineWeight = 1f;
			this.line9.Name = "line9";
			this.line9.Top = 1.673312f;
			this.line9.Width = 0.003560066f;
			this.line9.X1 = 6.040552f;
			this.line9.X2 = 6.036992f;
			this.line9.Y1 = 1.673312f;
			this.line9.Y2 = 4.520917f;
			this.line10.Height = 2.847605f;
			this.line10.Left = 6.859433f;
			this.line10.LineWeight = 1f;
			this.line10.Name = "line10";
			this.line10.Top = 1.673312f;
			this.line10.Width = 0.003559589f;
			this.line10.X1 = 6.862993f;
			this.line10.X2 = 6.859433f;
			this.line10.Y1 = 1.673312f;
			this.line10.Y2 = 4.520917f;
			this.line11.Height = 2.847604f;
			this.line11.Left = 7.756288f;
			this.line11.LineWeight = 1f;
			this.line11.Name = "line11";
			this.line11.Top = 1.673229f;
			this.line11.Width = 0.003554821f;
			this.line11.X1 = 7.759843f;
			this.line11.X2 = 7.756288f;
			this.line11.Y1 = 1.673229f;
			this.line11.Y2 = 4.520833f;
			this.line12.Height = 2.847604f;
			this.line12.Left = 0.09289709f;
			this.line12.LineWeight = 1f;
			this.line12.Name = "line12";
			this.line12.Top = 1.673229f;
			this.line12.Width = 0.003559612f;
			this.line12.X1 = 0.0964567f;
			this.line12.X2 = 0.09289709f;
			this.line12.Y1 = 1.673229f;
			this.line12.Y2 = 4.520833f;
			this.line13.Height = 0f;
			this.line13.Left = 0.09291339f;
			this.line13.LineWeight = 1f;
			this.line13.Name = "line13";
			this.line13.Top = 1.673229f;
			this.line13.Width = 7.663386f;
			this.line13.X1 = 0.09291339f;
			this.line13.X2 = 7.7563f;
			this.line13.Y1 = 1.673229f;
			this.line13.Y2 = 1.673229f;
			this.line14.Height = 0f;
			this.line14.Left = 0.0972441f;
			this.line14.LineWeight = 1f;
			this.line14.Name = "line14";
			this.line14.Top = 4.520866f;
			this.line14.Width = 7.659056f;
			this.line14.X1 = 0.0972441f;
			this.line14.X2 = 7.7563f;
			this.line14.Y1 = 4.520866f;
			this.line14.Y2 = 4.520866f;
			this.label1.Height = 0.3354167f;
			this.label1.HyperLink = null;
			this.label1.Left = 0.07165354f;
			this.label1.Name = "label1";
			this.label1.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.label1.Tag = "";
			this.label1.Text = "\u3000";
			this.label1.Top = 9.790244f;
			this.label1.Width = 7.684647f;
			this.label2.Height = 0.3354167f;
			this.label2.HyperLink = null;
			this.label2.Left = 0.07165354f;
			this.label2.Name = "label2";
			this.label2.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.label2.Tag = "";
			this.label2.Text = "\u3000";
			this.label2.Top = 7.772882f;
			this.label2.Width = 7.68819f;
			this.label3.Height = 0.3354167f;
			this.label3.HyperLink = null;
			this.label3.Left = 0.07519685f;
			this.label3.Name = "label3";
			this.label3.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.label3.Tag = "";
			this.label3.Text = "\u3000";
			this.label3.Top = 8.4583f;
			this.label3.Width = 7.684646f;
			this.label4.Height = 0.3361111f;
			this.label4.HyperLink = null;
			this.label4.Left = 0.07519685f;
			this.label4.Name = "label4";
			this.label4.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.label4.Tag = "";
			this.label4.Text = "\u3000";
			this.label4.Top = 9.117327f;
			this.label4.Width = 7.681103f;
			this.textBox1.DataField = "ITEM78";
			this.textBox1.Height = 0.1875f;
			this.textBox1.Left = 0.9281442f;
			this.textBox1.Name = "textBox1";
			this.textBox1.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox1.Tag = "";
			this.textBox1.Text = "ITEM78";
			this.textBox1.Top = 9.870799f;
			this.textBox1.Width = 2.385417f;
			this.textBox2.DataField = "ITEM24";
			this.textBox2.Height = 0.1805556f;
			this.textBox2.Left = 0.9281442f;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox2.Tag = "";
			this.textBox2.Text = "ITEM24";
			this.textBox2.Top = 7.867327f;
			this.textBox2.Width = 2.385417f;
			this.textBox3.DataField = "ITEM42";
			this.textBox3.Height = 0.1875f;
			this.textBox3.Left = 0.9281442f;
			this.textBox3.Name = "textBox3";
			this.textBox3.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox3.Tag = "";
			this.textBox3.Text = "ITEM42";
			this.textBox3.Top = 8.549966f;
			this.textBox3.Width = 2.385417f;
			this.textBox4.DataField = "ITEM60";
			this.textBox4.Height = 0.1875f;
			this.textBox4.Left = 0.9281442f;
			this.textBox4.Name = "textBox4";
			this.textBox4.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox4.Tag = "";
			this.textBox4.Text = "ITEM60";
			this.textBox4.Top = 9.215939f;
			this.textBox4.Width = 2.385417f;
			this.textBox5.DataField = "ITEM15";
			this.textBox5.Height = 0.1875f;
			this.textBox5.Left = 0.9281442f;
			this.textBox5.Name = "textBox5";
			this.textBox5.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox5.Tag = "";
			this.textBox5.Text = "ITEM15";
			this.textBox5.Top = 7.552049f;
			this.textBox5.Width = 2.367132f;
			this.textBox6.DataField = "ITEM33";
			this.textBox6.Height = 0.1875f;
			this.textBox6.Left = 0.9281442f;
			this.textBox6.Name = "textBox6";
			this.textBox6.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox6.Tag = "";
			this.textBox6.Text = "ITEM33";
			this.textBox6.Top = 8.214549f;
			this.textBox6.Width = 2.385417f;
			this.textBox7.DataField = "ITEM51";
			this.textBox7.Height = 0.1875f;
			this.textBox7.Left = 0.9281442f;
			this.textBox7.Name = "textBox7";
			this.textBox7.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox7.Tag = "";
			this.textBox7.Text = "ITEM51";
			this.textBox7.Top = 8.877744f;
			this.textBox7.Width = 2.385417f;
			this.textBox8.DataField = "ITEM69";
			this.textBox8.Height = 0.1875f;
			this.textBox8.Left = 0.9281442f;
			this.textBox8.Name = "textBox8";
			this.textBox8.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox8.Tag = "";
			this.textBox8.Text = "ITEM69";
			this.textBox8.Top = 9.528439f;
			this.textBox8.Width = 2.385417f;
			this.label5.Height = 0.1979167f;
			this.label5.HyperLink = null;
			this.label5.Left = 0.07162159f;
			this.label5.Name = "label5";
			this.label5.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.label5.Tag = "";
			this.label5.Text = "品名コード";
			this.label5.Top = 7.298426f;
			this.label5.Width = 0.8613515f;
			this.label6.Height = 0.1979167f;
			this.label6.HyperLink = null;
			this.label6.Left = 0.8913066f;
			this.label6.Name = "label6";
			this.label6.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.label6.Tag = "";
			this.label6.Text = "商\u3000品\u3000名";
			this.label6.Top = 7.298426f;
			this.label6.Width = 2.489583f;
			this.label7.Height = 0.1979167f;
			this.label7.HyperLink = null;
			this.label7.Left = 3.370473f;
			this.label7.Name = "label7";
			this.label7.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.label7.Tag = "";
			this.label7.Text = "規\u3000格";
			this.label7.Top = 7.298426f;
			this.label7.Width = 1.03125f;
			this.label8.Height = 0.1979167f;
			this.label8.HyperLink = null;
			this.label8.Left = 4.388528f;
			this.label8.Name = "label8";
			this.label8.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.label8.Tag = "";
			this.label8.Text = "単位";
			this.label8.Top = 7.298426f;
			this.label8.Width = 0.3125f;
			this.label9.Height = 0.1979167f;
			this.label9.HyperLink = null;
			this.label9.Left = 4.703112f;
			this.label9.Name = "label9";
			this.label9.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.label9.Tag = "";
			this.label9.Text = "入数";
			this.label9.Top = 7.298426f;
			this.label9.Width = 0.3541667f;
			this.label10.Height = 0.1979167f;
			this.label10.HyperLink = null;
			this.label10.Left = 5.026723f;
			this.label10.Name = "label10";
			this.label10.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.label10.Tag = "";
			this.label10.Text = "ケース";
			this.label10.Top = 7.298426f;
			this.label10.Width = 0.4236393f;
			this.label11.Height = 0.1979167f;
			this.label11.HyperLink = null;
			this.label11.Left = 5.43737f;
			this.label11.Name = "label11";
			this.label11.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.label11.Tag = "";
			this.label11.Text = "バラ";
			this.label11.Top = 7.298426f;
			this.label11.Width = 0.6247692f;
			this.label12.Height = 0.1979167f;
			this.label12.HyperLink = null;
			this.label12.Left = 6.047556f;
			this.label12.Name = "label12";
			this.label12.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.label12.Tag = "";
			this.label12.Text = "単\u3000価";
			this.label12.Top = 7.298426f;
			this.label12.Width = 0.8229167f;
			this.label13.Height = 0.1979167f;
			this.label13.HyperLink = null;
			this.label13.Left = 6.870473f;
			this.label13.Name = "label13";
			this.label13.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128";
			this.label13.Tag = "";
			this.label13.Text = "金\u3000額";
			this.label13.Top = 7.298426f;
			this.label13.Width = 0.8767402f;
			this.textBox9.DataField = "ITEM14";
			this.textBox9.Height = 0.1875f;
			this.textBox9.Left = 0.1725884f;
			this.textBox9.Name = "textBox9";
			this.textBox9.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox9.Tag = "";
			this.textBox9.Text = "ITEM14";
			this.textBox9.Top = 7.552049f;
			this.textBox9.Width = 0.6770833f;
			this.textBox10.DataField = "ITEM16";
			this.textBox10.Height = 0.1875f;
			this.textBox10.Left = 3.464174f;
			this.textBox10.Name = "textBox10";
			this.textBox10.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox10.Tag = "";
			this.textBox10.Text = "ITEM16";
			this.textBox10.Top = 7.552049f;
			this.textBox10.Width = 0.8854983f;
			this.textBox11.DataField = "ITEM17";
			this.textBox11.Height = 0.1875f;
			this.textBox11.Left = 4.433005f;
			this.textBox11.Name = "textBox11";
			this.textBox11.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox11.Tag = "";
			this.textBox11.Text = "ITEM17";
			this.textBox11.Top = 7.552049f;
			this.textBox11.Width = 0.2291667f;
			this.textBox12.DataField = "ITEM18";
			this.textBox12.Height = 0.1875f;
			this.textBox12.Left = 4.745505f;
			this.textBox12.Name = "textBox12";
			this.textBox12.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox12.Tag = "";
			this.textBox12.Text = "ITEM18";
			this.textBox12.Top = 7.552049f;
			this.textBox12.Width = 0.2395833f;
			this.textBox13.DataField = "ITEM19";
			this.textBox13.Height = 0.1874016f;
			this.textBox13.Left = 5.058005f;
			this.textBox13.Name = "textBox13";
			this.textBox13.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox13.Tag = "";
			this.textBox13.Text = "ITEM19";
			this.textBox13.Top = 7.552049f;
			this.textBox13.Width = 0.3452756f;
			this.textBox14.DataField = "ITEM20";
			this.textBox14.Height = 0.1874016f;
			this.textBox14.Left = 5.451575f;
			this.textBox14.Name = "textBox14";
			this.textBox14.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox14.Tag = "";
			this.textBox14.Text = "ITEM20";
			this.textBox14.Top = 7.551969f;
			this.textBox14.Width = 0.5728347f;
			this.textBox15.DataField = "ITEM21";
			this.textBox15.Height = 0.1875f;
			this.textBox15.Left = 6.081616f;
			this.textBox15.Name = "textBox15";
			this.textBox15.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox15.Tag = "";
			this.textBox15.Text = "ITEM21";
			this.textBox15.Top = 7.552049f;
			this.textBox15.Width = 0.7604167f;
			this.textBox16.DataField = "ITEM22";
			this.textBox16.Height = 0.1875f;
			this.textBox16.Left = 6.912171f;
			this.textBox16.Name = "textBox16";
			this.textBox16.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox16.Tag = "";
			this.textBox16.Text = "ITEM22";
			this.textBox16.Top = 7.552049f;
			this.textBox16.Width = 0.7604167f;
			this.textBox17.DataField = "ITEM23";
			this.textBox17.Height = 0.1805556f;
			this.textBox17.Left = 0.1677273f;
			this.textBox17.Name = "textBox17";
			this.textBox17.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox17.Tag = "";
			this.textBox17.Text = "ITEM23";
			this.textBox17.Top = 7.867327f;
			this.textBox17.Width = 0.6770833f;
			this.textBox18.DataField = "ITEM25";
			this.textBox18.Height = 0.1805556f;
			this.textBox18.Left = 3.464174f;
			this.textBox18.Name = "textBox18";
			this.textBox18.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox18.Tag = "";
			this.textBox18.Text = "ITEM25";
			this.textBox18.Top = 7.867327f;
			this.textBox18.Width = 0.8806369f;
			this.textBox19.DataField = "ITEM26";
			this.textBox19.Height = 0.1805556f;
			this.textBox19.Left = 4.428144f;
			this.textBox19.Name = "textBox19";
			this.textBox19.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox19.Tag = "";
			this.textBox19.Text = "ITEM26";
			this.textBox19.Top = 7.867327f;
			this.textBox19.Width = 0.2291667f;
			this.textBox20.DataField = "ITEM27";
			this.textBox20.Height = 0.1805556f;
			this.textBox20.Left = 4.740644f;
			this.textBox20.Name = "textBox20";
			this.textBox20.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox20.Tag = "";
			this.textBox20.Text = "ITEM27";
			this.textBox20.Top = 7.867327f;
			this.textBox20.Width = 0.2395833f;
			this.textBox21.DataField = "ITEM28";
			this.textBox21.Height = 0.1805556f;
			this.textBox21.Left = 5.058005f;
			this.textBox21.Name = "textBox21";
			this.textBox21.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox21.Tag = "";
			this.textBox21.Text = "ITEM28";
			this.textBox21.Top = 7.867327f;
			this.textBox21.Width = 0.3452756f;
			this.textBox22.DataField = "ITEM29";
			this.textBox22.Height = 0.1805556f;
			this.textBox22.Left = 5.451575f;
			this.textBox22.Name = "textBox22";
			this.textBox22.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox22.Tag = "";
			this.textBox22.Text = "ITEM29";
			this.textBox22.Top = 7.867327f;
			this.textBox22.Width = 0.5728347f;
			this.textBox23.DataField = "ITEM30";
			this.textBox23.Height = 0.1805556f;
			this.textBox23.Left = 6.076755f;
			this.textBox23.Name = "textBox23";
			this.textBox23.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox23.Tag = "";
			this.textBox23.Text = "ITEM30";
			this.textBox23.Top = 7.867327f;
			this.textBox23.Width = 0.7604167f;
			this.textBox24.DataField = "ITEM31";
			this.textBox24.Height = 0.1805556f;
			this.textBox24.Left = 6.90731f;
			this.textBox24.Name = "textBox24";
			this.textBox24.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox24.Tag = "";
			this.textBox24.Text = "ITEM31";
			this.textBox24.Top = 7.867327f;
			this.textBox24.Width = 0.7604167f;
			this.textBox25.DataField = "ITEM32";
			this.textBox25.Height = 0.1875f;
			this.textBox25.Left = 0.1725884f;
			this.textBox25.Name = "textBox25";
			this.textBox25.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox25.Tag = "";
			this.textBox25.Text = "ITEM32";
			this.textBox25.Top = 8.214549f;
			this.textBox25.Width = 0.6770833f;
			this.textBox26.DataField = "ITEM34";
			this.textBox26.Height = 0.1875f;
			this.textBox26.Left = 3.464174f;
			this.textBox26.Name = "textBox26";
			this.textBox26.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox26.Tag = "";
			this.textBox26.Text = "ITEM34";
			this.textBox26.Top = 8.214549f;
			this.textBox26.Width = 0.8854983f;
			this.textBox27.DataField = "ITEM35";
			this.textBox27.Height = 0.1875f;
			this.textBox27.Left = 4.433005f;
			this.textBox27.Name = "textBox27";
			this.textBox27.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox27.Tag = "";
			this.textBox27.Text = "ITEM35";
			this.textBox27.Top = 8.214549f;
			this.textBox27.Width = 0.2291667f;
			this.textBox28.DataField = "ITEM36";
			this.textBox28.Height = 0.1875f;
			this.textBox28.Left = 4.745505f;
			this.textBox28.Name = "textBox28";
			this.textBox28.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox28.Tag = "";
			this.textBox28.Text = "ITEM36";
			this.textBox28.Top = 8.214549f;
			this.textBox28.Width = 0.2395833f;
			this.textBox29.DataField = "ITEM37";
			this.textBox29.Height = 0.1875f;
			this.textBox29.Left = 5.058005f;
			this.textBox29.Name = "textBox29";
			this.textBox29.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox29.Tag = "";
			this.textBox29.Text = "ITEM37";
			this.textBox29.Top = 8.214549f;
			this.textBox29.Width = 0.3452756f;
			this.textBox30.DataField = "ITEM38";
			this.textBox30.Height = 0.1875f;
			this.textBox30.Left = 5.451575f;
			this.textBox30.Name = "textBox30";
			this.textBox30.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox30.Tag = "";
			this.textBox30.Text = "ITEM38";
			this.textBox30.Top = 8.214549f;
			this.textBox30.Width = 0.5728347f;
			this.textBox31.DataField = "ITEM39";
			this.textBox31.Height = 0.1875f;
			this.textBox31.Left = 6.081616f;
			this.textBox31.Name = "textBox31";
			this.textBox31.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox31.Tag = "";
			this.textBox31.Text = "ITEM39";
			this.textBox31.Top = 8.214549f;
			this.textBox31.Width = 0.7604167f;
			this.textBox32.DataField = "ITEM40";
			this.textBox32.Height = 0.1875f;
			this.textBox32.Left = 6.912171f;
			this.textBox32.Name = "textBox32";
			this.textBox32.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox32.Tag = "";
			this.textBox32.Text = "ITEM40";
			this.textBox32.Top = 8.214549f;
			this.textBox32.Width = 0.7604167f;
			this.textBox33.DataField = "ITEM41";
			this.textBox33.Height = 0.1875f;
			this.textBox33.Left = 0.1725884f;
			this.textBox33.Name = "textBox33";
			this.textBox33.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox33.Tag = "";
			this.textBox33.Text = "ITEM41";
			this.textBox33.Top = 8.549966f;
			this.textBox33.Width = 0.6770833f;
			this.textBox34.DataField = "ITEM43";
			this.textBox34.Height = 0.1875f;
			this.textBox34.Left = 3.464174f;
			this.textBox34.Name = "textBox34";
			this.textBox34.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox34.Tag = "";
			this.textBox34.Text = "ITEM43";
			this.textBox34.Top = 8.549966f;
			this.textBox34.Width = 0.8854983f;
			this.textBox35.DataField = "ITEM44";
			this.textBox35.Height = 0.1875f;
			this.textBox35.Left = 4.433005f;
			this.textBox35.Name = "textBox35";
			this.textBox35.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox35.Tag = "";
			this.textBox35.Text = "ITEM44";
			this.textBox35.Top = 8.549966f;
			this.textBox35.Width = 0.2291667f;
			this.textBox36.DataField = "ITEM45";
			this.textBox36.Height = 0.1875f;
			this.textBox36.Left = 4.745505f;
			this.textBox36.Name = "textBox36";
			this.textBox36.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox36.Tag = "";
			this.textBox36.Text = "ITEM45";
			this.textBox36.Top = 8.549966f;
			this.textBox36.Width = 0.2395833f;
			this.textBox37.DataField = "ITEM46";
			this.textBox37.Height = 0.1875f;
			this.textBox37.Left = 5.058005f;
			this.textBox37.Name = "textBox37";
			this.textBox37.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox37.Tag = "";
			this.textBox37.Text = "ITEM46";
			this.textBox37.Top = 8.549966f;
			this.textBox37.Width = 0.3452756f;
			this.textBox38.DataField = "ITEM47";
			this.textBox38.Height = 0.1875f;
			this.textBox38.Left = 5.451575f;
			this.textBox38.Name = "textBox38";
			this.textBox38.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox38.Tag = "";
			this.textBox38.Text = "ITEM47";
			this.textBox38.Top = 8.549966f;
			this.textBox38.Width = 0.5728347f;
			this.textBox39.DataField = "ITEM48";
			this.textBox39.Height = 0.1875f;
			this.textBox39.Left = 6.081616f;
			this.textBox39.Name = "textBox39";
			this.textBox39.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox39.Tag = "";
			this.textBox39.Text = "ITEM48";
			this.textBox39.Top = 8.549966f;
			this.textBox39.Width = 0.7604167f;
			this.textBox40.DataField = "ITEM49";
			this.textBox40.Height = 0.1875f;
			this.textBox40.Left = 6.912171f;
			this.textBox40.Name = "textBox40";
			this.textBox40.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox40.Tag = "";
			this.textBox40.Text = "ITEM49";
			this.textBox40.Top = 8.549966f;
			this.textBox40.Width = 0.7604167f;
			this.textBox41.DataField = "ITEM50";
			this.textBox41.Height = 0.1875f;
			this.textBox41.Left = 0.1725884f;
			this.textBox41.Name = "textBox41";
			this.textBox41.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox41.Tag = "";
			this.textBox41.Text = "ITEM50";
			this.textBox41.Top = 8.877744f;
			this.textBox41.Width = 0.6770833f;
			this.textBox42.DataField = "ITEM52";
			this.textBox42.Height = 0.1875f;
			this.textBox42.Left = 3.464174f;
			this.textBox42.Name = "textBox42";
			this.textBox42.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox42.Tag = "";
			this.textBox42.Text = "ITEM52";
			this.textBox42.Top = 8.877744f;
			this.textBox42.Width = 0.8854983f;
			this.textBox43.DataField = "ITEM53";
			this.textBox43.Height = 0.1875f;
			this.textBox43.Left = 4.433005f;
			this.textBox43.Name = "textBox43";
			this.textBox43.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox43.Tag = "";
			this.textBox43.Text = "ITEM53";
			this.textBox43.Top = 8.877744f;
			this.textBox43.Width = 0.2291667f;
			this.textBox44.DataField = "ITEM54";
			this.textBox44.Height = 0.1875f;
			this.textBox44.Left = 4.745505f;
			this.textBox44.Name = "textBox44";
			this.textBox44.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox44.Tag = "";
			this.textBox44.Text = "ITEM54";
			this.textBox44.Top = 8.877744f;
			this.textBox44.Width = 0.2395833f;
			this.textBox45.DataField = "ITEM55";
			this.textBox45.Height = 0.1875f;
			this.textBox45.Left = 5.058005f;
			this.textBox45.Name = "textBox45";
			this.textBox45.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox45.Tag = "";
			this.textBox45.Text = "ITEM55";
			this.textBox45.Top = 8.877744f;
			this.textBox45.Width = 0.3452756f;
			this.textBox46.DataField = "ITEM56";
			this.textBox46.Height = 0.1875f;
			this.textBox46.Left = 5.451575f;
			this.textBox46.Name = "textBox46";
			this.textBox46.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox46.Tag = "";
			this.textBox46.Text = "ITEM56";
			this.textBox46.Top = 8.877744f;
			this.textBox46.Width = 0.5728347f;
			this.textBox47.DataField = "ITEM57";
			this.textBox47.Height = 0.1875f;
			this.textBox47.Left = 6.081616f;
			this.textBox47.Name = "textBox47";
			this.textBox47.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox47.Tag = "";
			this.textBox47.Text = "ITEM57";
			this.textBox47.Top = 8.877744f;
			this.textBox47.Width = 0.7604167f;
			this.textBox48.DataField = "ITEM58";
			this.textBox48.Height = 0.1875f;
			this.textBox48.Left = 6.912171f;
			this.textBox48.Name = "textBox48";
			this.textBox48.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox48.Tag = "";
			this.textBox48.Text = "ITEM58";
			this.textBox48.Top = 8.877744f;
			this.textBox48.Width = 0.7604167f;
			this.textBox49.DataField = "ITEM59";
			this.textBox49.Height = 0.1875f;
			this.textBox49.Left = 0.1725884f;
			this.textBox49.Name = "textBox49";
			this.textBox49.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox49.Tag = "";
			this.textBox49.Text = "ITEM59";
			this.textBox49.Top = 9.215939f;
			this.textBox49.Width = 0.6770833f;
			this.textBox50.DataField = "ITEM61";
			this.textBox50.Height = 0.1875f;
			this.textBox50.Left = 3.464174f;
			this.textBox50.Name = "textBox50";
			this.textBox50.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox50.Tag = "";
			this.textBox50.Text = "ITEM61";
			this.textBox50.Top = 9.215939f;
			this.textBox50.Width = 0.8854983f;
			this.textBox51.DataField = "ITEM62";
			this.textBox51.Height = 0.1875f;
			this.textBox51.Left = 4.433005f;
			this.textBox51.Name = "textBox51";
			this.textBox51.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox51.Tag = "";
			this.textBox51.Text = "ITEM62";
			this.textBox51.Top = 9.215939f;
			this.textBox51.Width = 0.2291667f;
			this.textBox52.DataField = "ITEM63";
			this.textBox52.Height = 0.1875f;
			this.textBox52.Left = 4.745505f;
			this.textBox52.Name = "textBox52";
			this.textBox52.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox52.Tag = "";
			this.textBox52.Text = "ITEM63";
			this.textBox52.Top = 9.215939f;
			this.textBox52.Width = 0.2395833f;
			this.textBox53.DataField = "ITEM64";
			this.textBox53.Height = 0.1875f;
			this.textBox53.Left = 5.058005f;
			this.textBox53.Name = "textBox53";
			this.textBox53.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox53.Tag = "";
			this.textBox53.Text = "ITEM64";
			this.textBox53.Top = 9.215939f;
			this.textBox53.Width = 0.3452756f;
			this.textBox54.DataField = "ITEM65";
			this.textBox54.Height = 0.1875f;
			this.textBox54.Left = 5.451575f;
			this.textBox54.Name = "textBox54";
			this.textBox54.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox54.Tag = "";
			this.textBox54.Text = "ITEM65";
			this.textBox54.Top = 9.215939f;
			this.textBox54.Width = 0.5728347f;
			this.textBox55.DataField = "ITEM66";
			this.textBox55.Height = 0.1875f;
			this.textBox55.Left = 6.081616f;
			this.textBox55.Name = "textBox55";
			this.textBox55.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox55.Tag = "";
			this.textBox55.Text = "ITEM66";
			this.textBox55.Top = 9.215939f;
			this.textBox55.Width = 0.7604167f;
			this.textBox56.DataField = "ITEM67";
			this.textBox56.Height = 0.1875f;
			this.textBox56.Left = 6.912171f;
			this.textBox56.Name = "textBox56";
			this.textBox56.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox56.Tag = "";
			this.textBox56.Text = "ITEM67";
			this.textBox56.Top = 9.215939f;
			this.textBox56.Width = 0.7604167f;
			this.textBox57.DataField = "ITEM68";
			this.textBox57.Height = 0.1875f;
			this.textBox57.Left = 0.1725884f;
			this.textBox57.Name = "textBox57";
			this.textBox57.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox57.Tag = "";
			this.textBox57.Text = "ITEM68";
			this.textBox57.Top = 9.528439f;
			this.textBox57.Width = 0.6770833f;
			this.textBox58.DataField = "ITEM70";
			this.textBox58.Height = 0.1875f;
			this.textBox58.Left = 3.464174f;
			this.textBox58.Name = "textBox58";
			this.textBox58.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox58.Tag = "";
			this.textBox58.Text = "ITEM70";
			this.textBox58.Top = 9.528439f;
			this.textBox58.Width = 0.8854983f;
			this.textBox59.DataField = "ITEM71";
			this.textBox59.Height = 0.1875f;
			this.textBox59.Left = 4.433005f;
			this.textBox59.Name = "textBox59";
			this.textBox59.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox59.Tag = "";
			this.textBox59.Text = "ITEM71";
			this.textBox59.Top = 9.528439f;
			this.textBox59.Width = 0.2291667f;
			this.textBox60.DataField = "ITEM72";
			this.textBox60.Height = 0.1875f;
			this.textBox60.Left = 4.745505f;
			this.textBox60.Name = "textBox60";
			this.textBox60.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox60.Tag = "";
			this.textBox60.Text = "ITEM72";
			this.textBox60.Top = 9.528439f;
			this.textBox60.Width = 0.2395833f;
			this.textBox61.DataField = "ITEM73";
			this.textBox61.Height = 0.1875f;
			this.textBox61.Left = 5.058005f;
			this.textBox61.Name = "textBox61";
			this.textBox61.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox61.Tag = "";
			this.textBox61.Text = "ITEM73";
			this.textBox61.Top = 9.528439f;
			this.textBox61.Width = 0.3452756f;
			this.textBox62.DataField = "ITEM74";
			this.textBox62.Height = 0.1875f;
			this.textBox62.Left = 5.451575f;
			this.textBox62.Name = "textBox62";
			this.textBox62.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox62.Tag = "";
			this.textBox62.Text = "ITEM74";
			this.textBox62.Top = 9.528439f;
			this.textBox62.Width = 0.5728347f;
			this.textBox63.DataField = "ITEM75";
			this.textBox63.Height = 0.1875f;
			this.textBox63.Left = 6.081616f;
			this.textBox63.Name = "textBox63";
			this.textBox63.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox63.Tag = "";
			this.textBox63.Text = "ITEM75";
			this.textBox63.Top = 9.528439f;
			this.textBox63.Width = 0.7604167f;
			this.textBox64.DataField = "ITEM76";
			this.textBox64.Height = 0.1875f;
			this.textBox64.Left = 6.912171f;
			this.textBox64.Name = "textBox64";
			this.textBox64.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox64.Tag = "";
			this.textBox64.Text = "ITEM76";
			this.textBox64.Top = 9.528439f;
			this.textBox64.Width = 0.7604167f;
			this.textBox65.DataField = "ITEM77";
			this.textBox65.Height = 0.1875f;
			this.textBox65.Left = 0.1725884f;
			this.textBox65.Name = "textBox65";
			this.textBox65.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox65.Tag = "";
			this.textBox65.Text = "ITEM77";
			this.textBox65.Top = 9.870799f;
			this.textBox65.Width = 0.6770833f;
			this.textBox66.DataField = "ITEM79";
			this.textBox66.Height = 0.1875f;
			this.textBox66.Left = 3.464174f;
			this.textBox66.Name = "textBox66";
			this.textBox66.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox66.Tag = "";
			this.textBox66.Text = "ITEM79";
			this.textBox66.Top = 9.870799f;
			this.textBox66.Width = 0.8854983f;
			this.textBox67.DataField = "ITEM80";
			this.textBox67.Height = 0.1875f;
			this.textBox67.Left = 4.433005f;
			this.textBox67.Name = "textBox67";
			this.textBox67.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox67.Tag = "";
			this.textBox67.Text = "ITEM80";
			this.textBox67.Top = 9.870799f;
			this.textBox67.Width = 0.2291667f;
			this.textBox68.DataField = "ITEM81";
			this.textBox68.Height = 0.1875f;
			this.textBox68.Left = 4.745505f;
			this.textBox68.Name = "textBox68";
			this.textBox68.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox68.Tag = "";
			this.textBox68.Text = "ITEM81";
			this.textBox68.Top = 9.870799f;
			this.textBox68.Width = 0.2395833f;
			this.textBox69.DataField = "ITEM82";
			this.textBox69.Height = 0.1875f;
			this.textBox69.Left = 5.058005f;
			this.textBox69.Name = "textBox69";
			this.textBox69.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox69.Tag = "";
			this.textBox69.Text = "ITEM82";
			this.textBox69.Top = 9.870799f;
			this.textBox69.Width = 0.3452756f;
			this.textBox70.DataField = "ITEM83";
			this.textBox70.Height = 0.1875f;
			this.textBox70.Left = 5.451575f;
			this.textBox70.Name = "textBox70";
			this.textBox70.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox70.Tag = "";
			this.textBox70.Text = "ITEM83";
			this.textBox70.Top = 9.870799f;
			this.textBox70.Width = 0.5728347f;
			this.textBox71.DataField = "ITEM84";
			this.textBox71.Height = 0.1875f;
			this.textBox71.Left = 6.081616f;
			this.textBox71.Name = "textBox71";
			this.textBox71.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox71.Tag = "";
			this.textBox71.Text = "ITEM84";
			this.textBox71.Top = 9.870799f;
			this.textBox71.Width = 0.7604167f;
			this.textBox72.DataField = "ITEM85";
			this.textBox72.Height = 0.1875f;
			this.textBox72.Left = 6.912171f;
			this.textBox72.Name = "textBox72";
			this.textBox72.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.textBox72.Tag = "";
			this.textBox72.Text = "ITEM85";
			this.textBox72.Top = 9.870799f;
			this.textBox72.Width = 0.7604167f;
			this.line1.Height = 0f;
			this.line1.Left = 0.07519685f;
			this.line1.LineWeight = 0f;
			this.line1.Name = "line1";
			this.line1.Tag = "";
			this.line1.Top = 7.476378f;
			this.line1.Width = 7.684646f;
			this.line1.X1 = 0.07519685f;
			this.line1.X2 = 7.759843f;
			this.line1.Y1 = 7.476378f;
			this.line1.Y2 = 7.476378f;
			this.line15.Height = 2.847608f;
			this.line15.Left = 0.8878664f;
			this.line15.LineWeight = 1f;
			this.line15.Name = "line15";
			this.line15.Top = 7.278382f;
			this.line15.Width = 0.003559828f;
			this.line15.X1 = 0.8914262f;
			this.line15.X2 = 0.8878664f;
			this.line15.Y1 = 7.278382f;
			this.line15.Y2 = 10.12599f;
			this.line16.Height = 2.839737f;
			this.line16.Left = 3.353221f;
			this.line16.LineWeight = 1f;
			this.line16.Name = "line16";
			this.line16.Top = 7.282313f;
			this.line16.Width = 0.003560066f;
			this.line16.X1 = 3.356781f;
			this.line16.X2 = 3.353221f;
			this.line16.Y1 = 7.282313f;
			this.line16.Y2 = 10.12205f;
			this.line17.Height = 2.847608f;
			this.line17.Left = 4.383536f;
			this.line17.LineWeight = 1f;
			this.line17.Name = "line17";
			this.line17.Top = 7.278382f;
			this.line17.Width = 0.003559113f;
			this.line17.X1 = 4.387095f;
			this.line17.X2 = 4.383536f;
			this.line17.Y1 = 7.278382f;
			this.line17.Y2 = 10.12599f;
			this.line18.Height = 2.847608f;
			this.line18.Left = 4.700465f;
			this.line18.LineWeight = 1f;
			this.line18.Name = "line18";
			this.line18.Top = 7.278382f;
			this.line18.Width = 0.003559589f;
			this.line18.X1 = 4.704025f;
			this.line18.X2 = 4.700465f;
			this.line18.Y1 = 7.278382f;
			this.line18.Y2 = 10.12599f;
			this.line19.Height = 2.847608f;
			this.line19.Left = 5.018575f;
			this.line19.LineWeight = 1f;
			this.line19.Name = "line19";
			this.line19.Top = 7.278382f;
			this.line19.Width = 0.003559589f;
			this.line19.X1 = 5.022135f;
			this.line19.X2 = 5.018575f;
			this.line19.Y1 = 7.278382f;
			this.line19.Y2 = 10.12599f;
			this.line20.Height = 2.847613f;
			this.line20.Left = 5.446835f;
			this.line20.LineWeight = 1f;
			this.line20.Name = "line20";
			this.line20.Top = 7.278347f;
			this.line20.Width = 0.003559113f;
			this.line20.X1 = 5.450394f;
			this.line20.X2 = 5.446835f;
			this.line20.Y1 = 7.278347f;
			this.line20.Y2 = 10.12596f;
			this.line21.Height = 2.847608f;
			this.line21.Left = 6.039442f;
			this.line21.LineWeight = 1f;
			this.line21.Name = "line21";
			this.line21.Top = 7.278382f;
			this.line21.Width = 0.003559113f;
			this.line21.X1 = 6.043001f;
			this.line21.X2 = 6.039442f;
			this.line21.Y1 = 7.278382f;
			this.line21.Y2 = 10.12599f;
			this.line22.Height = 2.847608f;
			this.line22.Left = 6.861883f;
			this.line22.LineWeight = 1f;
			this.line22.Name = "line22";
			this.line22.Top = 7.278382f;
			this.line22.Width = 0.003560066f;
			this.line22.X1 = 6.865443f;
			this.line22.X2 = 6.861883f;
			this.line22.Y1 = 7.278382f;
			this.line22.Y2 = 10.12599f;
			this.line23.Height = 2.847613f;
			this.line23.Left = 7.747232f;
			this.line23.LineWeight = 1f;
			this.line23.Name = "line23";
			this.line23.Top = 7.278347f;
			this.line23.Width = 0.003556252f;
			this.line23.X1 = 7.750788f;
			this.line23.X2 = 7.747232f;
			this.line23.Y1 = 7.278347f;
			this.line23.Y2 = 10.12596f;
			this.line24.Height = 2.847613f;
			this.line24.Left = 0.07163725f;
			this.line24.LineWeight = 1f;
			this.line24.Name = "line24";
			this.line24.Top = 7.278347f;
			this.line24.Width = 0.003559597f;
			this.line24.X1 = 0.07519685f;
			this.line24.X2 = 0.07163725f;
			this.line24.Y1 = 7.278347f;
			this.line24.Y2 = 10.12596f;
			this.line25.Height = 0f;
			this.line25.Left = 0.07519685f;
			this.line25.LineWeight = 1f;
			this.line25.Name = "line25";
			this.line25.Top = 7.282284f;
			this.line25.Width = 7.672048f;
			this.line25.X1 = 0.07519685f;
			this.line25.X2 = 7.747245f;
			this.line25.Y1 = 7.282284f;
			this.line25.Y2 = 7.282284f;
			this.line26.Height = 0.003939629f;
			this.line26.Left = 0.07519685f;
			this.line26.LineWeight = 1f;
			this.line26.Name = "line26";
			this.line26.Top = 10.12205f;
			this.line26.Width = 7.684646f;
			this.line26.X1 = 0.07519685f;
			this.line26.X2 = 7.759843f;
			this.line26.Y1 = 10.12599f;
			this.line26.Y2 = 10.12205f;
			this.shape1.Height = 0.5416667f;
			this.shape1.Left = 0.2395833f;
			this.shape1.Name = "shape1";
			this.shape1.RoundingRadius = 30f;
			this.shape1.Top = 4.739583f;
			this.shape1.Width = 1.375f;
			this.line2.Height = 0.541667f;
			this.line2.Left = 0.5937008f;
			this.line2.LineWeight = 1f;
			this.line2.Name = "line2";
			this.line2.Top = 4.739764f;
			this.line2.Width = 0f;
			this.line2.X1 = 0.5937008f;
			this.line2.X2 = 0.5937008f;
			this.line2.Y1 = 4.739764f;
			this.line2.Y2 = 5.281431f;
			this.textBox84.Height = 0.4267716f;
			this.textBox84.Left = 0.3125984f;
			this.textBox84.Name = "textBox84";
			this.textBox84.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; ddo-font-vertical: true";
			this.textBox84.Text = "受領印";
			this.textBox84.Top = 4.798819f;
			this.textBox84.Width = 0.2291667f;
			this.line27.Height = 0f;
			this.line27.Left = 0f;
			this.line27.LineStyle = LineStyle.Dash;
			this.line27.LineWeight = 1f;
			this.line27.Name = "line27";
			this.line27.Top = 5.433071f;
			this.line27.Width = 7.874016f;
			this.line27.X1 = 0f;
			this.line27.X2 = 7.874016f;
			this.line27.Y1 = 5.433071f;
			this.line27.Y2 = 5.433071f;
			this.textBox85.DataField = "ITEM99";
			this.textBox85.Height = 0.5417323f;
			this.textBox85.Left = 1.75f;
			this.textBox85.Name = "textBox85";
			this.textBox85.Padding = new PaddingEx(4, 0, 0, 0);
			this.textBox85.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox85.Tag = "";
			this.textBox85.Text = "ITEM99";
			this.textBox85.Top = 4.739764f;
			this.textBox85.Visible = false;
			this.textBox85.Width = 1.947638f;
			this.textBox87.DataField = "ITEM100";
			this.textBox87.Height = 0.1875f;
			this.textBox87.Left = 3.127559f;
			this.textBox87.Name = "textBox87";
			this.textBox87.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox87.Tag = "";
			this.textBox87.Text = "10";
			this.textBox87.Top = 7.551969f;
			this.textBox87.Width = 0.1858268f;
			this.textBox88.DataField = "ITEM101";
			this.textBox88.Height = 0.1875f;
			this.textBox88.Left = 3.121654f;
			this.textBox88.Name = "textBox88";
			this.textBox88.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox88.Tag = "";
			this.textBox88.Text = "10";
			this.textBox88.Top = 7.867323f;
			this.textBox88.Width = 0.1917322f;
			this.textBox89.DataField = "ITEM102";
			this.textBox89.Height = 0.1875f;
			this.textBox89.Left = 3.127559f;
			this.textBox89.Name = "textBox89";
			this.textBox89.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox89.Tag = "";
			this.textBox89.Text = "10";
			this.textBox89.Top = 8.270866f;
			this.textBox89.Width = 0.1917322f;
			this.textBox90.DataField = "ITEM103";
			this.textBox90.Height = 0.1875f;
			this.textBox90.Left = 3.127559f;
			this.textBox90.Name = "textBox90";
			this.textBox90.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox90.Tag = "";
			this.textBox90.Text = "10";
			this.textBox90.Top = 8.55f;
			this.textBox90.Width = 0.1917322f;
			this.textBox91.DataField = "ITEM104";
			this.textBox91.Height = 0.1875f;
			this.textBox91.Left = 3.127559f;
			this.textBox91.Name = "textBox91";
			this.textBox91.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox91.Tag = "";
			this.textBox91.Text = "10";
			this.textBox91.Top = 8.87756f;
			this.textBox91.Width = 0.1917322f;
			this.textBox92.DataField = "ITEM105";
			this.textBox92.Height = 0.1875f;
			this.textBox92.Left = 3.127559f;
			this.textBox92.Name = "textBox92";
			this.textBox92.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox92.Tag = "";
			this.textBox92.Text = "10";
			this.textBox92.Top = 9.215749f;
			this.textBox92.Width = 0.1917322f;
			this.textBox93.DataField = "ITEM106";
			this.textBox93.Height = 0.1875f;
			this.textBox93.Left = 3.127559f;
			this.textBox93.Name = "textBox93";
			this.textBox93.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox93.Tag = "";
			this.textBox93.Text = "10";
			this.textBox93.Top = 9.528347f;
			this.textBox93.Width = 0.1917322f;
			this.textBox94.DataField = "ITEM107";
			this.textBox94.Height = 0.1875f;
			this.textBox94.Left = 3.127559f;
			this.textBox94.Name = "textBox94";
			this.textBox94.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox94.Tag = "";
			this.textBox94.Text = "10";
			this.textBox94.Top = 9.870867f;
			this.textBox94.Width = 0.1917322f;
			this.textBox95.DataField = "ITEM100";
			this.textBox95.Height = 0.1875f;
			this.textBox95.Left = 3.133465f;
			this.textBox95.Name = "textBox95";
			this.textBox95.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox95.Tag = "";
			this.textBox95.Text = "10";
			this.textBox95.Top = 1.947244f;
			this.textBox95.Width = 0.1858267f;
			this.textBox96.DataField = "ITEM101";
			this.textBox96.Height = 0.1875f;
			this.textBox96.Left = 3.12756f;
			this.textBox96.Name = "textBox96";
			this.textBox96.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox96.Tag = "";
			this.textBox96.Text = "10";
			this.textBox96.Top = 2.262605f;
			this.textBox96.Width = 0.1917322f;
			this.textBox97.DataField = "ITEM102";
			this.textBox97.Height = 0.1875f;
			this.textBox97.Left = 3.133465f;
			this.textBox97.Name = "textBox97";
			this.textBox97.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox97.Tag = "";
			this.textBox97.Text = "10";
			this.textBox97.Top = 2.666147f;
			this.textBox97.Width = 0.1917322f;
			this.textBox98.DataField = "ITEM103";
			this.textBox98.Height = 0.1875f;
			this.textBox98.Left = 3.133465f;
			this.textBox98.Name = "textBox98";
			this.textBox98.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox98.Tag = "";
			this.textBox98.Text = "10";
			this.textBox98.Top = 2.945279f;
			this.textBox98.Width = 0.1917322f;
			this.textBox99.DataField = "ITEM104";
			this.textBox99.Height = 0.1875f;
			this.textBox99.Left = 3.133465f;
			this.textBox99.Name = "textBox99";
			this.textBox99.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox99.Tag = "";
			this.textBox99.Text = "10";
			this.textBox99.Top = 3.272841f;
			this.textBox99.Width = 0.1917322f;
			this.textBox100.DataField = "ITEM105";
			this.textBox100.Height = 0.1875f;
			this.textBox100.Left = 3.133465f;
			this.textBox100.Name = "textBox100";
			this.textBox100.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox100.Tag = "";
			this.textBox100.Text = "10";
			this.textBox100.Top = 3.611028f;
			this.textBox100.Width = 0.1917322f;
			this.textBox101.DataField = "ITEM106";
			this.textBox101.Height = 0.1875f;
			this.textBox101.Left = 3.133465f;
			this.textBox101.Name = "textBox101";
			this.textBox101.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox101.Tag = "";
			this.textBox101.Text = "10";
			this.textBox101.Top = 3.923625f;
			this.textBox101.Width = 0.1917322f;
			this.textBox102.DataField = "ITEM107";
			this.textBox102.Height = 0.1875f;
			this.textBox102.Left = 3.133465f;
			this.textBox102.Name = "textBox102";
			this.textBox102.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox102.Tag = "";
			this.textBox102.Text = "10";
			this.textBox102.Top = 4.266146f;
			this.textBox102.Width = 0.1917322f;
			this.textBox103.DataField = "ITEM96";
			this.textBox103.Height = 0.1875f;
			this.textBox103.Left = 0.1318355f;
			this.textBox103.Name = "textBox103";
			this.textBox103.Style = "color: Black; font-family: MS UI Gothic; font-size: 14.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox103.Tag = "";
			this.textBox103.Text = "ITEM14";
			this.textBox103.Top = 0.780284f;
			this.textBox103.Width = 1.184252f;
			this.textBox104.DataField = "ITEM96";
			this.textBox104.Height = 0.1875f;
			this.textBox104.Left = 0.1287402f;
			this.textBox104.Name = "textBox104";
			this.textBox104.Style = "color: Black; font-family: MS UI Gothic; font-size: 14.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.textBox104.Tag = "";
			this.textBox104.Text = "ITEM14";
			this.textBox104.Top = 6.368505f;
			this.textBox104.Width = 1.187402f;
			this.picture1.Height = 0.8755903f;
			this.picture1.HyperLink = null;
//			this.picture1.ImageData = (Stream)componentResourceManager.GetObject("picture1.ImageData");
			this.picture1.Left = 6.776379f;
			this.picture1.Name = "picture1";
			this.picture1.SizeMode = SizeModes.Stretch;
			this.picture1.Top = 6.297638f;
			this.picture1.Width = 0.9708662f;
			this.label14.Height = 0.15625f;
			this.label14.HyperLink = null;
			this.label14.Left = 5.92441f;
			this.label14.Name = "label14";
			this.label14.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.label14.Tag = "";
			this.label14.Text = "FAX\r\n";
			this.label14.Top = 1.258268f;
			this.label14.Width = 0.8216538f;
			this.label16.Height = 0.15625f;
			this.label16.HyperLink = null;
			this.label16.Left = 6.444882f;
			this.label16.Name = "label16";
			this.label16.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.label16.Tag = "";
			this.label16.Text = "(098)861-2707";
			this.label16.Top = 1.107874f;
			this.label16.Width = 1.270078f;
			this.label23.Height = 0.15625f;
			this.label23.HyperLink = null;
			this.label23.Left = 5.954724f;
			this.label23.Name = "label23";
			this.label23.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.label23.Tag = "";
			this.label23.Text = "事務所";
			this.label23.Top = 1.107874f;
			this.label23.Width = 0.8216542f;
			this.label15.Height = 0.15625f;
			this.label15.HyperLink = null;
			this.label15.Left = 6.449607f;
			this.label15.Name = "label15";
			this.label15.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.label15.Tag = "";
			this.label15.Text = "(098)861-0819";
			this.label15.Top = 1.274016f;
			this.label15.Width = 1.270078f;
			this.label17.Height = 0.1972222f;
			this.label17.HyperLink = null;
			this.label17.Left = 2.537008f;
			this.label17.Name = "label17";
			this.label17.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.label17.Tag = "";
			this.label17.Text = "御中";
			this.label17.Top = 6.60315f;
			this.label17.Width = 0.4270892f;
			this.label18.Height = 0.15625f;
			this.label18.HyperLink = null;
			this.label18.Left = 5.95433f;
			this.label18.Name = "label18";
			this.label18.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.label18.Tag = "";
			this.label18.Text = "FAX\r\n";
			this.label18.Top = 6.834253f;
			this.label18.Width = 0.8216542f;
			this.label19.Height = 0.15625f;
			this.label19.HyperLink = null;
			this.label19.Left = 6.427559f;
			this.label19.Name = "label19";
			this.label19.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.label19.Tag = "";
			this.label19.Text = "(098)861-2707";
			this.label19.Top = 6.683859f;
			this.label19.Width = 1.270078f;
			this.label20.Height = 0.15625f;
			this.label20.HyperLink = null;
			this.label20.Left = 5.984645f;
			this.label20.Name = "label20";
			this.label20.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.label20.Tag = "";
			this.label20.Text = "事務所";
			this.label20.Top = 6.683859f;
			this.label20.Width = 0.8216542f;
			this.label21.Height = 0.15625f;
			this.label21.HyperLink = null;
			this.label21.Left = 6.432283f;
			this.label21.Name = "label21";
			this.label21.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; ddo-char-set: 128";
			this.label21.Tag = "";
			this.label21.Text = "(098)861-0819";
			this.label21.Top = 6.85f;
			this.label21.Width = 1.270078f;
			this.label22.Height = 0.1972222f;
			this.label22.HyperLink = null;
			this.label22.Left = 0.1925197f;
			this.label22.Name = "label22";
			this.label22.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 12pt; font-weight: normal; text-align: center; ddo-char-set: 128";
			this.label22.Tag = "";
			this.label22.Text = "振込先：";
			this.label22.Top = 10.14645f;
			this.label22.Width = 0.9122047f;
			this.label24.Height = 0.1972222f;
			this.label24.HyperLink = null;
			this.label24.Left = 0.3208662f;
			this.label24.Name = "label24";
			this.label24.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.label24.Tag = "";
			this.label24.Text = "沖縄銀行曙支店(普)No.0910018";
			this.label24.Top = 10.34369f;
			this.label24.Width = 3.329528f;
			this.label25.Height = 0.1972222f;
			this.label25.HyperLink = null;
			this.label25.Left = 0.3244095f;
			this.label25.Name = "label25";
			this.label25.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text-align: left; ddo-char-set: 128";
			this.label25.Tag = "";
			this.label25.Text = "沖縄県信漁連本所(普)No.0000639";
			this.label25.Top = 10.61417f;
			this.label25.Width = 3.329528f;
			this.pageFooter.Height = 0f;
			this.pageFooter.Name = "pageFooter";
			base.MasterReport = false;
			base.PageSettings.Margins.Bottom = 0f;
			base.PageSettings.Margins.Left = 0.1968504f;
			base.PageSettings.Margins.Right = 0.1968504f;
			base.PageSettings.Margins.Top = 0.3937007f;
			base.PageSettings.PaperHeight = 11f;
			base.PageSettings.PaperWidth = 8.5f;
			base.PrintWidth = 7.885417f;
			base.Sections.Add(this.pageHeader);
			base.Sections.Add(this.detail);
			base.Sections.Add(this.pageFooter);
			base.StyleSheet.Add(new StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
			base.StyleSheet.Add(new StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Heading1", "Normal"));
			base.StyleSheet.Add(new StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Heading2", "Normal"));
			base.StyleSheet.Add(new StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
			((ISupportInitialize)this.ラベル378).EndInit();
			((ISupportInitialize)this.BackClrCg).EndInit();
			((ISupportInitialize)this.ラベル340).EndInit();
			((ISupportInitialize)this.ラベル359).EndInit();
			((ISupportInitialize)this.テキスト389).EndInit();
			((ISupportInitialize)this.テキスト332).EndInit();
			((ISupportInitialize)this.テキスト351).EndInit();
			((ISupportInitialize)this.テキスト370).EndInit();
			((ISupportInitialize)this.テキスト231).EndInit();
			((ISupportInitialize)this.テキスト342).EndInit();
			((ISupportInitialize)this.テキスト361).EndInit();
			((ISupportInitialize)this.テキスト380).EndInit();
			((ISupportInitialize)this.ラベル203).EndInit();
			((ISupportInitialize)this.テキスト258).EndInit();
			((ISupportInitialize)this.ITEM02).EndInit();
			((ISupportInitialize)this.ITEM01).EndInit();
			((ISupportInitialize)this.ラベル71).EndInit();
			((ISupportInitialize)this.ラベル196).EndInit();
			((ISupportInitialize)this.ラベル201).EndInit();
			((ISupportInitialize)this.テキスト204).EndInit();
			((ISupportInitialize)this.テキスト205).EndInit();
			((ISupportInitialize)this.ラベル207).EndInit();
			((ISupportInitialize)this.ラベル208).EndInit();
			((ISupportInitialize)this.テキスト209).EndInit();
			((ISupportInitialize)this.ラベル210).EndInit();
			((ISupportInitialize)this.テキスト211).EndInit();
			((ISupportInitialize)this.テキスト212).EndInit();
			((ISupportInitialize)this.テキスト213).EndInit();
			((ISupportInitialize)this.テキスト217).EndInit();
			((ISupportInitialize)this.テキスト218).EndInit();
			((ISupportInitialize)this.ラベル219).EndInit();
			((ISupportInitialize)this.テキスト220).EndInit();
			((ISupportInitialize)this.ラベル221).EndInit();
			((ISupportInitialize)this.ラベル222).EndInit();
			((ISupportInitialize)this.ラベル223).EndInit();
			((ISupportInitialize)this.ラベル224).EndInit();
			((ISupportInitialize)this.ラベル225).EndInit();
			((ISupportInitialize)this.ラベル226).EndInit();
			((ISupportInitialize)this.ラベル227).EndInit();
			((ISupportInitialize)this.ラベル228).EndInit();
			((ISupportInitialize)this.ラベル229).EndInit();
			((ISupportInitialize)this.テキスト230).EndInit();
			((ISupportInitialize)this.テキスト232).EndInit();
			((ISupportInitialize)this.テキスト233).EndInit();
			((ISupportInitialize)this.テキスト234).EndInit();
			((ISupportInitialize)this.テキスト235).EndInit();
			((ISupportInitialize)this.テキスト236).EndInit();
			((ISupportInitialize)this.テキスト237).EndInit();
			((ISupportInitialize)this.テキスト238).EndInit();
			((ISupportInitialize)this.テキスト331).EndInit();
			((ISupportInitialize)this.テキスト333).EndInit();
			((ISupportInitialize)this.テキスト334).EndInit();
			((ISupportInitialize)this.テキスト335).EndInit();
			((ISupportInitialize)this.テキスト336).EndInit();
			((ISupportInitialize)this.テキスト337).EndInit();
			((ISupportInitialize)this.テキスト338).EndInit();
			((ISupportInitialize)this.テキスト339).EndInit();
			((ISupportInitialize)this.テキスト341).EndInit();
			((ISupportInitialize)this.テキスト343).EndInit();
			((ISupportInitialize)this.テキスト344).EndInit();
			((ISupportInitialize)this.テキスト345).EndInit();
			((ISupportInitialize)this.テキスト346).EndInit();
			((ISupportInitialize)this.テキスト347).EndInit();
			((ISupportInitialize)this.テキスト348).EndInit();
			((ISupportInitialize)this.テキスト349).EndInit();
			((ISupportInitialize)this.テキスト350).EndInit();
			((ISupportInitialize)this.テキスト352).EndInit();
			((ISupportInitialize)this.テキスト353).EndInit();
			((ISupportInitialize)this.テキスト354).EndInit();
			((ISupportInitialize)this.テキスト355).EndInit();
			((ISupportInitialize)this.テキスト356).EndInit();
			((ISupportInitialize)this.テキスト357).EndInit();
			((ISupportInitialize)this.テキスト358).EndInit();
			((ISupportInitialize)this.テキスト360).EndInit();
			((ISupportInitialize)this.テキスト362).EndInit();
			((ISupportInitialize)this.テキスト363).EndInit();
			((ISupportInitialize)this.テキスト364).EndInit();
			((ISupportInitialize)this.テキスト365).EndInit();
			((ISupportInitialize)this.テキスト366).EndInit();
			((ISupportInitialize)this.テキスト367).EndInit();
			((ISupportInitialize)this.テキスト368).EndInit();
			((ISupportInitialize)this.テキスト369).EndInit();
			((ISupportInitialize)this.テキスト371).EndInit();
			((ISupportInitialize)this.テキスト372).EndInit();
			((ISupportInitialize)this.テキスト373).EndInit();
			((ISupportInitialize)this.テキスト374).EndInit();
			((ISupportInitialize)this.テキスト375).EndInit();
			((ISupportInitialize)this.テキスト376).EndInit();
			((ISupportInitialize)this.テキスト377).EndInit();
			((ISupportInitialize)this.テキスト379).EndInit();
			((ISupportInitialize)this.テキスト381).EndInit();
			((ISupportInitialize)this.テキスト382).EndInit();
			((ISupportInitialize)this.テキスト383).EndInit();
			((ISupportInitialize)this.テキスト384).EndInit();
			((ISupportInitialize)this.テキスト385).EndInit();
			((ISupportInitialize)this.テキスト386).EndInit();
			((ISupportInitialize)this.テキスト387).EndInit();
			((ISupportInitialize)this.テキスト388).EndInit();
			((ISupportInitialize)this.テキスト390).EndInit();
			((ISupportInitialize)this.テキスト391).EndInit();
			((ISupportInitialize)this.テキスト392).EndInit();
			((ISupportInitialize)this.テキスト393).EndInit();
			((ISupportInitialize)this.テキスト394).EndInit();
			((ISupportInitialize)this.テキスト395).EndInit();
			((ISupportInitialize)this.テキスト396).EndInit();
			((ISupportInitialize)this.テキスト311).EndInit();
			((ISupportInitialize)this.テキスト312).EndInit();
			((ISupportInitialize)this.テキスト313).EndInit();
			((ISupportInitialize)this.テキスト314).EndInit();
			((ISupportInitialize)this.テキスト315).EndInit();
			((ISupportInitialize)this.テキスト316).EndInit();
			((ISupportInitialize)this.テキスト317).EndInit();
			((ISupportInitialize)this.テキスト318).EndInit();
			((ISupportInitialize)this.ラベル414).EndInit();
			((ISupportInitialize)this.テキスト415).EndInit();
			((ISupportInitialize)this.テキスト416).EndInit();
			((ISupportInitialize)this.テキスト417).EndInit();
			((ISupportInitialize)this.ラベル418).EndInit();
			((ISupportInitialize)this.ラベル420).EndInit();
			((ISupportInitialize)this.テキスト422).EndInit();
			((ISupportInitialize)this.ラベル425).EndInit();
			((ISupportInitialize)this.テキスト426).EndInit();
			((ISupportInitialize)this.ラベル427).EndInit();
			((ISupportInitialize)this.テキスト428).EndInit();
			((ISupportInitialize)this.テキスト429).EndInit();
			((ISupportInitialize)this.テキスト430).EndInit();
			((ISupportInitialize)this.テキスト433).EndInit();
			((ISupportInitialize)this.ラベル434).EndInit();
			((ISupportInitialize)this.テキスト435).EndInit();
			((ISupportInitialize)this.ラベル535).EndInit();
			((ISupportInitialize)this.テキスト642).EndInit();
			((ISupportInitialize)this.テキスト643).EndInit();
			((ISupportInitialize)this.textBox73).EndInit();
			((ISupportInitialize)this.textBox74).EndInit();
			((ISupportInitialize)this.textBox75).EndInit();
			((ISupportInitialize)this.textBox76).EndInit();
			((ISupportInitialize)this.textBox77).EndInit();
			((ISupportInitialize)this.textBox78).EndInit();
			((ISupportInitialize)this.textBox79).EndInit();
			((ISupportInitialize)this.textBox80).EndInit();
			((ISupportInitialize)this.textBox81).EndInit();
			((ISupportInitialize)this.textBox82).EndInit();
			((ISupportInitialize)this.textBox83).EndInit();
			((ISupportInitialize)this.label1).EndInit();
			((ISupportInitialize)this.label2).EndInit();
			((ISupportInitialize)this.label3).EndInit();
			((ISupportInitialize)this.label4).EndInit();
			((ISupportInitialize)this.textBox1).EndInit();
			((ISupportInitialize)this.textBox2).EndInit();
			((ISupportInitialize)this.textBox3).EndInit();
			((ISupportInitialize)this.textBox4).EndInit();
			((ISupportInitialize)this.textBox5).EndInit();
			((ISupportInitialize)this.textBox6).EndInit();
			((ISupportInitialize)this.textBox7).EndInit();
			((ISupportInitialize)this.textBox8).EndInit();
			((ISupportInitialize)this.label5).EndInit();
			((ISupportInitialize)this.label6).EndInit();
			((ISupportInitialize)this.label7).EndInit();
			((ISupportInitialize)this.label8).EndInit();
			((ISupportInitialize)this.label9).EndInit();
			((ISupportInitialize)this.label10).EndInit();
			((ISupportInitialize)this.label11).EndInit();
			((ISupportInitialize)this.label12).EndInit();
			((ISupportInitialize)this.label13).EndInit();
			((ISupportInitialize)this.textBox9).EndInit();
			((ISupportInitialize)this.textBox10).EndInit();
			((ISupportInitialize)this.textBox11).EndInit();
			((ISupportInitialize)this.textBox12).EndInit();
			((ISupportInitialize)this.textBox13).EndInit();
			((ISupportInitialize)this.textBox14).EndInit();
			((ISupportInitialize)this.textBox15).EndInit();
			((ISupportInitialize)this.textBox16).EndInit();
			((ISupportInitialize)this.textBox17).EndInit();
			((ISupportInitialize)this.textBox18).EndInit();
			((ISupportInitialize)this.textBox19).EndInit();
			((ISupportInitialize)this.textBox20).EndInit();
			((ISupportInitialize)this.textBox21).EndInit();
			((ISupportInitialize)this.textBox22).EndInit();
			((ISupportInitialize)this.textBox23).EndInit();
			((ISupportInitialize)this.textBox24).EndInit();
			((ISupportInitialize)this.textBox25).EndInit();
			((ISupportInitialize)this.textBox26).EndInit();
			((ISupportInitialize)this.textBox27).EndInit();
			((ISupportInitialize)this.textBox28).EndInit();
			((ISupportInitialize)this.textBox29).EndInit();
			((ISupportInitialize)this.textBox30).EndInit();
			((ISupportInitialize)this.textBox31).EndInit();
			((ISupportInitialize)this.textBox32).EndInit();
			((ISupportInitialize)this.textBox33).EndInit();
			((ISupportInitialize)this.textBox34).EndInit();
			((ISupportInitialize)this.textBox35).EndInit();
			((ISupportInitialize)this.textBox36).EndInit();
			((ISupportInitialize)this.textBox37).EndInit();
			((ISupportInitialize)this.textBox38).EndInit();
			((ISupportInitialize)this.textBox39).EndInit();
			((ISupportInitialize)this.textBox40).EndInit();
			((ISupportInitialize)this.textBox41).EndInit();
			((ISupportInitialize)this.textBox42).EndInit();
			((ISupportInitialize)this.textBox43).EndInit();
			((ISupportInitialize)this.textBox44).EndInit();
			((ISupportInitialize)this.textBox45).EndInit();
			((ISupportInitialize)this.textBox46).EndInit();
			((ISupportInitialize)this.textBox47).EndInit();
			((ISupportInitialize)this.textBox48).EndInit();
			((ISupportInitialize)this.textBox49).EndInit();
			((ISupportInitialize)this.textBox50).EndInit();
			((ISupportInitialize)this.textBox51).EndInit();
			((ISupportInitialize)this.textBox52).EndInit();
			((ISupportInitialize)this.textBox53).EndInit();
			((ISupportInitialize)this.textBox54).EndInit();
			((ISupportInitialize)this.textBox55).EndInit();
			((ISupportInitialize)this.textBox56).EndInit();
			((ISupportInitialize)this.textBox57).EndInit();
			((ISupportInitialize)this.textBox58).EndInit();
			((ISupportInitialize)this.textBox59).EndInit();
			((ISupportInitialize)this.textBox60).EndInit();
			((ISupportInitialize)this.textBox61).EndInit();
			((ISupportInitialize)this.textBox62).EndInit();
			((ISupportInitialize)this.textBox63).EndInit();
			((ISupportInitialize)this.textBox64).EndInit();
			((ISupportInitialize)this.textBox65).EndInit();
			((ISupportInitialize)this.textBox66).EndInit();
			((ISupportInitialize)this.textBox67).EndInit();
			((ISupportInitialize)this.textBox68).EndInit();
			((ISupportInitialize)this.textBox69).EndInit();
			((ISupportInitialize)this.textBox70).EndInit();
			((ISupportInitialize)this.textBox71).EndInit();
			((ISupportInitialize)this.textBox72).EndInit();
			((ISupportInitialize)this.textBox84).EndInit();
			((ISupportInitialize)this.textBox85).EndInit();
			((ISupportInitialize)this.textBox87).EndInit();
			((ISupportInitialize)this.textBox88).EndInit();
			((ISupportInitialize)this.textBox89).EndInit();
			((ISupportInitialize)this.textBox90).EndInit();
			((ISupportInitialize)this.textBox91).EndInit();
			((ISupportInitialize)this.textBox92).EndInit();
			((ISupportInitialize)this.textBox93).EndInit();
			((ISupportInitialize)this.textBox94).EndInit();
			((ISupportInitialize)this.textBox95).EndInit();
			((ISupportInitialize)this.textBox96).EndInit();
			((ISupportInitialize)this.textBox97).EndInit();
			((ISupportInitialize)this.textBox98).EndInit();
			((ISupportInitialize)this.textBox99).EndInit();
			((ISupportInitialize)this.textBox100).EndInit();
			((ISupportInitialize)this.textBox101).EndInit();
			((ISupportInitialize)this.textBox102).EndInit();
			((ISupportInitialize)this.textBox103).EndInit();
			((ISupportInitialize)this.textBox104).EndInit();
			((ISupportInitialize)this.picture1).EndInit();
			((ISupportInitialize)this.label14).EndInit();
			((ISupportInitialize)this.label16).EndInit();
			((ISupportInitialize)this.label23).EndInit();
			((ISupportInitialize)this.label15).EndInit();
			((ISupportInitialize)this.label17).EndInit();
			((ISupportInitialize)this.label18).EndInit();
			((ISupportInitialize)this.label19).EndInit();
			((ISupportInitialize)this.label20).EndInit();
			((ISupportInitialize)this.label21).EndInit();
			((ISupportInitialize)this.label22).EndInit();
			((ISupportInitialize)this.label24).EndInit();
			((ISupportInitialize)this.label25).EndInit();
			((ISupportInitialize)this).EndInit();
		}

		// Token: 0x0400014B RID: 331
		private PageHeader pageHeader;

		// Token: 0x0400014C RID: 332
		private Detail detail;

		// Token: 0x0400014D RID: 333
		private PageFooter pageFooter;

		// Token: 0x0400014E RID: 334
		private Label ラベル378;

		// Token: 0x0400014F RID: 335
		private Label BackClrCg;

		// Token: 0x04000150 RID: 336
		private Label ラベル340;

		// Token: 0x04000151 RID: 337
		private Label ラベル359;

		// Token: 0x04000152 RID: 338
		private TextBox テキスト389;

		// Token: 0x04000153 RID: 339
		private TextBox テキスト332;

		// Token: 0x04000154 RID: 340
		private TextBox テキスト351;

		// Token: 0x04000155 RID: 341
		private TextBox テキスト370;

		// Token: 0x04000156 RID: 342
		private TextBox テキスト231;

		// Token: 0x04000157 RID: 343
		private TextBox テキスト342;

		// Token: 0x04000158 RID: 344
		private TextBox テキスト361;

		// Token: 0x04000159 RID: 345
		private TextBox テキスト380;

		// Token: 0x0400015A RID: 346
		private Label ラベル203;

		// Token: 0x0400015B RID: 347
		private TextBox テキスト258;

		// Token: 0x0400015C RID: 348
		private TextBox ITEM02;

		// Token: 0x0400015D RID: 349
		private TextBox ITEM01;

		// Token: 0x0400015E RID: 350
		private Label ラベル71;

		// Token: 0x0400015F RID: 351
		private Label ラベル196;

		// Token: 0x04000160 RID: 352
		private Label ラベル201;

		// Token: 0x04000161 RID: 353
		private TextBox テキスト204;

		// Token: 0x04000162 RID: 354
		private TextBox テキスト205;

		// Token: 0x04000163 RID: 355
		private Line 直線206;

		// Token: 0x04000164 RID: 356
		private Label ラベル207;

		// Token: 0x04000165 RID: 357
		private Label ラベル208;

		// Token: 0x04000166 RID: 358
		private TextBox テキスト209;

		// Token: 0x04000167 RID: 359
		private Label ラベル210;

		// Token: 0x04000168 RID: 360
		private TextBox テキスト211;

		// Token: 0x04000169 RID: 361
		private TextBox テキスト212;

		// Token: 0x0400016A RID: 362
		private TextBox テキスト217;

		// Token: 0x0400016B RID: 363
		private TextBox テキスト218;

		// Token: 0x0400016C RID: 364
		private Label ラベル219;

		// Token: 0x0400016D RID: 365
		private TextBox テキスト220;

		// Token: 0x0400016E RID: 366
		private Label ラベル221;

		// Token: 0x0400016F RID: 367
		private Label ラベル222;

		// Token: 0x04000170 RID: 368
		private Label ラベル223;

		// Token: 0x04000171 RID: 369
		private Label ラベル224;

		// Token: 0x04000172 RID: 370
		private Label ラベル225;

		// Token: 0x04000173 RID: 371
		private Label ラベル226;

		// Token: 0x04000174 RID: 372
		private Label ラベル227;

		// Token: 0x04000175 RID: 373
		private Label ラベル228;

		// Token: 0x04000176 RID: 374
		private Label ラベル229;

		// Token: 0x04000177 RID: 375
		private TextBox テキスト230;

		// Token: 0x04000178 RID: 376
		private TextBox テキスト232;

		// Token: 0x04000179 RID: 377
		private TextBox テキスト233;

		// Token: 0x0400017A RID: 378
		private TextBox テキスト234;

		// Token: 0x0400017B RID: 379
		private TextBox テキスト235;

		// Token: 0x0400017C RID: 380
		private TextBox テキスト236;

		// Token: 0x0400017D RID: 381
		private TextBox テキスト237;

		// Token: 0x0400017E RID: 382
		private TextBox テキスト238;

		// Token: 0x0400017F RID: 383
		private TextBox テキスト331;

		// Token: 0x04000180 RID: 384
		private TextBox テキスト333;

		// Token: 0x04000181 RID: 385
		private TextBox テキスト334;

		// Token: 0x04000182 RID: 386
		private TextBox テキスト335;

		// Token: 0x04000183 RID: 387
		private TextBox テキスト336;

		// Token: 0x04000184 RID: 388
		private TextBox テキスト337;

		// Token: 0x04000185 RID: 389
		private TextBox テキスト338;

		// Token: 0x04000186 RID: 390
		private TextBox テキスト339;

		// Token: 0x04000187 RID: 391
		private TextBox テキスト341;

		// Token: 0x04000188 RID: 392
		private TextBox テキスト343;

		// Token: 0x04000189 RID: 393
		private TextBox テキスト344;

		// Token: 0x0400018A RID: 394
		private TextBox テキスト345;

		// Token: 0x0400018B RID: 395
		private TextBox テキスト346;

		// Token: 0x0400018C RID: 396
		private TextBox テキスト347;

		// Token: 0x0400018D RID: 397
		private TextBox テキスト348;

		// Token: 0x0400018E RID: 398
		private TextBox テキスト349;

		// Token: 0x0400018F RID: 399
		private TextBox テキスト350;

		// Token: 0x04000190 RID: 400
		private TextBox テキスト352;

		// Token: 0x04000191 RID: 401
		private TextBox テキスト353;

		// Token: 0x04000192 RID: 402
		private TextBox テキスト354;

		// Token: 0x04000193 RID: 403
		private TextBox テキスト355;

		// Token: 0x04000194 RID: 404
		private TextBox テキスト356;

		// Token: 0x04000195 RID: 405
		private TextBox テキスト357;

		// Token: 0x04000196 RID: 406
		private TextBox テキスト358;

		// Token: 0x04000197 RID: 407
		private TextBox テキスト360;

		// Token: 0x04000198 RID: 408
		private TextBox テキスト362;

		// Token: 0x04000199 RID: 409
		private TextBox テキスト363;

		// Token: 0x0400019A RID: 410
		private TextBox テキスト364;

		// Token: 0x0400019B RID: 411
		private TextBox テキスト365;

		// Token: 0x0400019C RID: 412
		private TextBox テキスト366;

		// Token: 0x0400019D RID: 413
		private TextBox テキスト367;

		// Token: 0x0400019E RID: 414
		private TextBox テキスト368;

		// Token: 0x0400019F RID: 415
		private TextBox テキスト369;

		// Token: 0x040001A0 RID: 416
		private TextBox テキスト371;

		// Token: 0x040001A1 RID: 417
		private TextBox テキスト372;

		// Token: 0x040001A2 RID: 418
		private TextBox テキスト373;

		// Token: 0x040001A3 RID: 419
		private TextBox テキスト374;

		// Token: 0x040001A4 RID: 420
		private TextBox テキスト375;

		// Token: 0x040001A5 RID: 421
		private TextBox テキスト376;

		// Token: 0x040001A6 RID: 422
		private TextBox テキスト377;

		// Token: 0x040001A7 RID: 423
		private TextBox テキスト379;

		// Token: 0x040001A8 RID: 424
		private TextBox テキスト381;

		// Token: 0x040001A9 RID: 425
		private TextBox テキスト382;

		// Token: 0x040001AA RID: 426
		private TextBox テキスト383;

		// Token: 0x040001AB RID: 427
		private TextBox テキスト384;

		// Token: 0x040001AC RID: 428
		private TextBox テキスト385;

		// Token: 0x040001AD RID: 429
		private TextBox テキスト386;

		// Token: 0x040001AE RID: 430
		private TextBox テキスト387;

		// Token: 0x040001AF RID: 431
		private TextBox テキスト388;

		// Token: 0x040001B0 RID: 432
		private TextBox テキスト390;

		// Token: 0x040001B1 RID: 433
		private TextBox テキスト391;

		// Token: 0x040001B2 RID: 434
		private TextBox テキスト392;

		// Token: 0x040001B3 RID: 435
		private TextBox テキスト393;

		// Token: 0x040001B4 RID: 436
		private TextBox テキスト394;

		// Token: 0x040001B5 RID: 437
		private TextBox テキスト395;

		// Token: 0x040001B6 RID: 438
		private TextBox テキスト396;

		// Token: 0x040001B7 RID: 439
		private Shape ボックス309;

		// Token: 0x040001B8 RID: 440
		private Shape ボックス310;

		// Token: 0x040001B9 RID: 441
		private TextBox テキスト311;

		// Token: 0x040001BA RID: 442
		private TextBox テキスト312;

		// Token: 0x040001BB RID: 443
		private TextBox テキスト313;

		// Token: 0x040001BC RID: 444
		private TextBox テキスト314;

		// Token: 0x040001BD RID: 445
		private TextBox テキスト315;

		// Token: 0x040001BE RID: 446
		private TextBox テキスト316;

		// Token: 0x040001BF RID: 447
		private TextBox テキスト317;

		// Token: 0x040001C0 RID: 448
		private TextBox テキスト318;

		// Token: 0x040001C1 RID: 449
		private Line 直線407;

		// Token: 0x040001C2 RID: 450
		private Label ラベル414;

		// Token: 0x040001C3 RID: 451
		private TextBox テキスト415;

		// Token: 0x040001C4 RID: 452
		private TextBox テキスト416;

		// Token: 0x040001C5 RID: 453
		private TextBox テキスト417;

		// Token: 0x040001C6 RID: 454
		private Label ラベル418;

		// Token: 0x040001C7 RID: 455
		private Label ラベル420;

		// Token: 0x040001C8 RID: 456
		private TextBox テキスト422;

		// Token: 0x040001C9 RID: 457
		private Line 直線423;

		// Token: 0x040001CA RID: 458
		private Label ラベル425;

		// Token: 0x040001CB RID: 459
		private TextBox テキスト426;

		// Token: 0x040001CC RID: 460
		private Label ラベル427;

		// Token: 0x040001CD RID: 461
		private TextBox テキスト428;

		// Token: 0x040001CE RID: 462
		private TextBox テキスト429;

		// Token: 0x040001CF RID: 463
		private TextBox テキスト430;

		// Token: 0x040001D0 RID: 464
		private TextBox テキスト433;

		// Token: 0x040001D1 RID: 465
		private Label ラベル434;

		// Token: 0x040001D2 RID: 466
		private TextBox テキスト435;

		// Token: 0x040001D3 RID: 467
		private Label ラベル535;

		// Token: 0x040001D4 RID: 468
		private TextBox テキスト642;

		// Token: 0x040001D5 RID: 469
		private TextBox テキスト643;

		// Token: 0x040001D6 RID: 470
		private TextBox textBox73;

		// Token: 0x040001D7 RID: 471
		private TextBox textBox74;

		// Token: 0x040001D8 RID: 472
		private TextBox textBox75;

		// Token: 0x040001D9 RID: 473
		private Shape shape10;

		// Token: 0x040001DA RID: 474
		private Shape shape11;

		// Token: 0x040001DB RID: 475
		private TextBox textBox76;

		// Token: 0x040001DC RID: 476
		private TextBox textBox77;

		// Token: 0x040001DD RID: 477
		private TextBox textBox78;

		// Token: 0x040001DE RID: 478
		private TextBox textBox79;

		// Token: 0x040001DF RID: 479
		private TextBox textBox80;

		// Token: 0x040001E0 RID: 480
		private TextBox textBox81;

		// Token: 0x040001E1 RID: 481
		private TextBox textBox82;

		// Token: 0x040001E2 RID: 482
		private TextBox textBox83;

		// Token: 0x040001E3 RID: 483
		private Line line3;

		// Token: 0x040001E4 RID: 484
		private Line line4;

		// Token: 0x040001E5 RID: 485
		private Line line5;

		// Token: 0x040001E6 RID: 486
		private Line line6;

		// Token: 0x040001E7 RID: 487
		private Line line7;

		// Token: 0x040001E8 RID: 488
		private Line line8;

		// Token: 0x040001E9 RID: 489
		private Line line9;

		// Token: 0x040001EA RID: 490
		private Line line10;

		// Token: 0x040001EB RID: 491
		private Line line11;

		// Token: 0x040001EC RID: 492
		private Line line12;

		// Token: 0x040001ED RID: 493
		private Line line13;

		// Token: 0x040001EE RID: 494
		private Line line14;

		// Token: 0x040001EF RID: 495
		private Label label1;

		// Token: 0x040001F0 RID: 496
		private Label label2;

		// Token: 0x040001F1 RID: 497
		private Label label3;

		// Token: 0x040001F2 RID: 498
		private Label label4;

		// Token: 0x040001F3 RID: 499
		private TextBox textBox1;

		// Token: 0x040001F4 RID: 500
		private TextBox textBox2;

		// Token: 0x040001F5 RID: 501
		private TextBox textBox3;

		// Token: 0x040001F6 RID: 502
		private TextBox textBox4;

		// Token: 0x040001F7 RID: 503
		private TextBox textBox5;

		// Token: 0x040001F8 RID: 504
		private TextBox textBox6;

		// Token: 0x040001F9 RID: 505
		private TextBox textBox7;

		// Token: 0x040001FA RID: 506
		private TextBox textBox8;

		// Token: 0x040001FB RID: 507
		private Label label5;

		// Token: 0x040001FC RID: 508
		private Label label6;

		// Token: 0x040001FD RID: 509
		private Label label7;

		// Token: 0x040001FE RID: 510
		private Label label8;

		// Token: 0x040001FF RID: 511
		private Label label9;

		// Token: 0x04000200 RID: 512
		private Label label10;

		// Token: 0x04000201 RID: 513
		private Label label11;

		// Token: 0x04000202 RID: 514
		private Label label12;

		// Token: 0x04000203 RID: 515
		private Label label13;

		// Token: 0x04000204 RID: 516
		private TextBox textBox9;

		// Token: 0x04000205 RID: 517
		private TextBox textBox10;

		// Token: 0x04000206 RID: 518
		private TextBox textBox11;

		// Token: 0x04000207 RID: 519
		private TextBox textBox12;

		// Token: 0x04000208 RID: 520
		private TextBox textBox13;

		// Token: 0x04000209 RID: 521
		private TextBox textBox14;

		// Token: 0x0400020A RID: 522
		private TextBox textBox15;

		// Token: 0x0400020B RID: 523
		private TextBox textBox16;

		// Token: 0x0400020C RID: 524
		private TextBox textBox17;

		// Token: 0x0400020D RID: 525
		private TextBox textBox18;

		// Token: 0x0400020E RID: 526
		private TextBox textBox19;

		// Token: 0x0400020F RID: 527
		private TextBox textBox20;

		// Token: 0x04000210 RID: 528
		private TextBox textBox21;

		// Token: 0x04000211 RID: 529
		private TextBox textBox22;

		// Token: 0x04000212 RID: 530
		private TextBox textBox23;

		// Token: 0x04000213 RID: 531
		private TextBox textBox24;

		// Token: 0x04000214 RID: 532
		private TextBox textBox25;

		// Token: 0x04000215 RID: 533
		private TextBox textBox26;

		// Token: 0x04000216 RID: 534
		private TextBox textBox27;

		// Token: 0x04000217 RID: 535
		private TextBox textBox28;

		// Token: 0x04000218 RID: 536
		private TextBox textBox29;

		// Token: 0x04000219 RID: 537
		private TextBox textBox30;

		// Token: 0x0400021A RID: 538
		private TextBox textBox31;

		// Token: 0x0400021B RID: 539
		private TextBox textBox32;

		// Token: 0x0400021C RID: 540
		private TextBox textBox33;

		// Token: 0x0400021D RID: 541
		private TextBox textBox34;

		// Token: 0x0400021E RID: 542
		private TextBox textBox35;

		// Token: 0x0400021F RID: 543
		private TextBox textBox36;

		// Token: 0x04000220 RID: 544
		private TextBox textBox37;

		// Token: 0x04000221 RID: 545
		private TextBox textBox38;

		// Token: 0x04000222 RID: 546
		private TextBox textBox39;

		// Token: 0x04000223 RID: 547
		private TextBox textBox40;

		// Token: 0x04000224 RID: 548
		private TextBox textBox41;

		// Token: 0x04000225 RID: 549
		private TextBox textBox42;

		// Token: 0x04000226 RID: 550
		private TextBox textBox43;

		// Token: 0x04000227 RID: 551
		private TextBox textBox44;

		// Token: 0x04000228 RID: 552
		private TextBox textBox45;

		// Token: 0x04000229 RID: 553
		private TextBox textBox46;

		// Token: 0x0400022A RID: 554
		private TextBox textBox47;

		// Token: 0x0400022B RID: 555
		private TextBox textBox48;

		// Token: 0x0400022C RID: 556
		private TextBox textBox49;

		// Token: 0x0400022D RID: 557
		private TextBox textBox50;

		// Token: 0x0400022E RID: 558
		private TextBox textBox51;

		// Token: 0x0400022F RID: 559
		private TextBox textBox52;

		// Token: 0x04000230 RID: 560
		private TextBox textBox53;

		// Token: 0x04000231 RID: 561
		private TextBox textBox54;

		// Token: 0x04000232 RID: 562
		private TextBox textBox55;

		// Token: 0x04000233 RID: 563
		private TextBox textBox56;

		// Token: 0x04000234 RID: 564
		private TextBox textBox57;

		// Token: 0x04000235 RID: 565
		private TextBox textBox58;

		// Token: 0x04000236 RID: 566
		private TextBox textBox59;

		// Token: 0x04000237 RID: 567
		private TextBox textBox60;

		// Token: 0x04000238 RID: 568
		private TextBox textBox61;

		// Token: 0x04000239 RID: 569
		private TextBox textBox62;

		// Token: 0x0400023A RID: 570
		private TextBox textBox63;

		// Token: 0x0400023B RID: 571
		private TextBox textBox64;

		// Token: 0x0400023C RID: 572
		private TextBox textBox65;

		// Token: 0x0400023D RID: 573
		private TextBox textBox66;

		// Token: 0x0400023E RID: 574
		private TextBox textBox67;

		// Token: 0x0400023F RID: 575
		private TextBox textBox68;

		// Token: 0x04000240 RID: 576
		private TextBox textBox69;

		// Token: 0x04000241 RID: 577
		private TextBox textBox70;

		// Token: 0x04000242 RID: 578
		private TextBox textBox71;

		// Token: 0x04000243 RID: 579
		private TextBox textBox72;

		// Token: 0x04000244 RID: 580
		private Line line1;

		// Token: 0x04000245 RID: 581
		private Line line15;

		// Token: 0x04000246 RID: 582
		private Line line16;

		// Token: 0x04000247 RID: 583
		private Line line17;

		// Token: 0x04000248 RID: 584
		private Line line18;

		// Token: 0x04000249 RID: 585
		private Line line19;

		// Token: 0x0400024A RID: 586
		private Line line20;

		// Token: 0x0400024B RID: 587
		private Line line21;

		// Token: 0x0400024C RID: 588
		private Line line22;

		// Token: 0x0400024D RID: 589
		private Line line23;

		// Token: 0x0400024E RID: 590
		private Line line24;

		// Token: 0x0400024F RID: 591
		private Line line25;

		// Token: 0x04000250 RID: 592
		private Line line26;

		// Token: 0x04000251 RID: 593
		private Shape shape1;

		// Token: 0x04000252 RID: 594
		private Line line2;

		// Token: 0x04000253 RID: 595
		private TextBox textBox84;

		// Token: 0x04000254 RID: 596
		private Line line27;

		// Token: 0x04000255 RID: 597
		private TextBox textBox85;

		// Token: 0x04000256 RID: 598
		private TextBox textBox87;

		// Token: 0x04000257 RID: 599
		private TextBox textBox88;

		// Token: 0x04000258 RID: 600
		private TextBox textBox89;

		// Token: 0x04000259 RID: 601
		private TextBox textBox90;

		// Token: 0x0400025A RID: 602
		private TextBox textBox91;

		// Token: 0x0400025B RID: 603
		private TextBox textBox92;

		// Token: 0x0400025C RID: 604
		private TextBox textBox93;

		// Token: 0x0400025D RID: 605
		private TextBox textBox94;

		// Token: 0x0400025E RID: 606
		private TextBox textBox95;

		// Token: 0x0400025F RID: 607
		private TextBox textBox96;

		// Token: 0x04000260 RID: 608
		private TextBox textBox97;

		// Token: 0x04000261 RID: 609
		private TextBox textBox98;

		// Token: 0x04000262 RID: 610
		private TextBox textBox99;

		// Token: 0x04000263 RID: 611
		private TextBox textBox100;

		// Token: 0x04000264 RID: 612
		private TextBox textBox101;

		// Token: 0x04000265 RID: 613
		private TextBox textBox102;

		// Token: 0x04000266 RID: 614
		private TextBox textBox103;

		// Token: 0x04000267 RID: 615
		private TextBox textBox104;

		// Token: 0x04000268 RID: 616
		private Picture picture1;

		// Token: 0x04000269 RID: 617
		private Label label14;

		// Token: 0x0400026A RID: 618
		private TextBox テキスト213;

		// Token: 0x0400026B RID: 619
		private Label label16;

		// Token: 0x0400026C RID: 620
		private Label label23;

		// Token: 0x0400026D RID: 621
		private Label label15;

		// Token: 0x0400026E RID: 622
		private Label label17;

		// Token: 0x0400026F RID: 623
		private Label label18;

		// Token: 0x04000270 RID: 624
		private Label label19;

		// Token: 0x04000271 RID: 625
		private Label label20;

		// Token: 0x04000272 RID: 626
		private Label label21;

		// Token: 0x04000273 RID: 627
		private Label label22;

		// Token: 0x04000274 RID: 628
		private Label label24;

		// Token: 0x04000275 RID: 629
		private Label label25;
	}
}
