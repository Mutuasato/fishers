﻿namespace jp.co.fsi.kb.kbdr1021
{
    /// <summary>
    /// KBDR1021R の概要の説明です。
    /// </summary>
    partial class KBDR1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR1021R));
			this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.直線1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
			this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.直線34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.totalRowNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.nowRouNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ITEM03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ITEM05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ITEM06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ITEM08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ITEM09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ITEM10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.直線30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.totalRowNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nowRouNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM03)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM05)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM06)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM08)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM09)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape1,
            this.label2,
            this.ITEM01,
            this.ITEM02,
            this.直線1,
            this.reportInfo1,
            this.textBox1,
            this.label1,
            this.ラベル9,
            this.ラベル10,
            this.ラベル11,
            this.ラベル14,
            this.ラベル15,
            this.ラベル16,
            this.直線34,
            this.直線35,
            this.直線37,
            this.直線38,
            this.直線39,
            this.totalRowNo,
            this.nowRouNo,
            this.textBox2,
            this.textBox4,
            this.textBox29,
            this.line12});
			this.pageHeader.Height = 1.394385F;
			this.pageHeader.Name = "pageHeader";
			// 
			// shape1
			// 
			this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.shape1.Height = 0.3834646F;
			this.shape1.Left = 0.1893701F;
			this.shape1.Name = "shape1";
			this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
			this.shape1.Top = 1.012205F;
			this.shape1.Width = 7.161024F;
			// 
			// label2
			// 
			this.label2.Height = 0.1972222F;
			this.label2.HyperLink = null;
			this.label2.Left = 5.03F;
			this.label2.Name = "label2";
			this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
			this.label2.Tag = "";
			this.label2.Text = "税　率";
			this.label2.Top = 1.121F;
			this.label2.Width = 0.5830002F;
			// 
			// ITEM01
			// 
			this.ITEM01.DataField = "ITEM01";
			this.ITEM01.Height = 0.15625F;
			this.ITEM01.Left = 0.1893701F;
			this.ITEM01.Name = "ITEM01";
			this.ITEM01.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 1";
			this.ITEM01.Tag = "";
			this.ITEM01.Text = "ITEM01";
			this.ITEM01.Top = 0.1673228F;
			this.ITEM01.Width = 2.191825F;
			// 
			// ITEM02
			// 
			this.ITEM02.DataField = "ITEM03";
			this.ITEM02.Height = 0.15625F;
			this.ITEM02.Left = 2.594488F;
			this.ITEM02.Name = "ITEM02";
			this.ITEM02.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
			this.ITEM02.Tag = "";
			this.ITEM02.Text = "ITEM03";
			this.ITEM02.Top = 0.7569555F;
			this.ITEM02.Width = 2.277166F;
			// 
			// 直線1
			// 
			this.直線1.Height = 3.838539E-05F;
			this.直線1.Left = 2.600788F;
			this.直線1.LineWeight = 2F;
			this.直線1.Name = "直線1";
			this.直線1.Tag = "";
			this.直線1.Top = 0.6409449F;
			this.直線1.Width = 2.185886F;
			this.直線1.X1 = 2.600788F;
			this.直線1.X2 = 4.786674F;
			this.直線1.Y1 = 0.6409449F;
			this.直線1.Y2 = 0.6409833F;
			// 
			// reportInfo1
			// 
			this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
			this.reportInfo1.Height = 0.1972441F;
			this.reportInfo1.Left = 5.847F;
			this.reportInfo1.Name = "reportInfo1";
			this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
			this.reportInfo1.Top = 0.324F;
			this.reportInfo1.Width = 1.072129F;
			// 
			// textBox1
			// 
			this.textBox1.Height = 0.2005741F;
			this.textBox1.Left = 6.919048F;
			this.textBox1.Name = "textBox1";
			this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
			this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.textBox1.Text = "textBox1";
			this.textBox1.Top = 0.324F;
			this.textBox1.Width = 0.2708659F;
			// 
			// label1
			// 
			this.label1.Height = 0.2005741F;
			this.label1.HyperLink = null;
			this.label1.Left = 7.187551F;
			this.label1.Name = "label1";
			this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
			this.label1.Text = "頁";
			this.label1.Top = 0.324F;
			this.label1.Width = 0.15625F;
			// 
			// ラベル9
			// 
			this.ラベル9.Height = 0.1965278F;
			this.ラベル9.HyperLink = null;
			this.ラベル9.Left = 0.1944882F;
			this.ラベル9.Name = "ラベル9";
			this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
			this.ラベル9.Tag = "";
			this.ラベル9.Text = "船主CD";
			this.ラベル9.Top = 1.120079F;
			this.ラベル9.Width = 0.4755118F;
			// 
			// ラベル10
			// 
			this.ラベル10.Height = 0.1965278F;
			this.ラベル10.HyperLink = null;
			this.ラベル10.Left = 3.282F;
			this.ラベル10.Name = "ラベル10";
			this.ラベル10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
			this.ラベル10.Tag = "";
			this.ラベル10.Text = "取引区分";
			this.ラベル10.Top = 1.121F;
			this.ラベル10.Width = 0.8803151F;
			// 
			// ラベル11
			// 
			this.ラベル11.Height = 0.1972222F;
			this.ラベル11.HyperLink = null;
			this.ラベル11.Left = 0.702F;
			this.ラベル11.Name = "ラベル11";
			this.ラベル11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
			this.ラベル11.Tag = "";
			this.ラベル11.Text = "船　主　名";
			this.ラベル11.Top = 1.12F;
			this.ラベル11.Width = 2.574118F;
			// 
			// ラベル14
			// 
			this.ラベル14.Height = 0.1972222F;
			this.ラベル14.HyperLink = null;
			this.ラベル14.Left = 4.162315F;
			this.ラベル14.Name = "ラベル14";
			this.ラベル14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
			this.ラベル14.Tag = "";
			this.ラベル14.Text = "金　額";
			this.ラベル14.Top = 1.120213F;
			this.ラベル14.Width = 0.8676849F;
			// 
			// ラベル15
			// 
			this.ラベル15.Height = 0.1965278F;
			this.ラベル15.HyperLink = null;
			this.ラベル15.Left = 5.613386F;
			this.ラベル15.Name = "ラベル15";
			this.ラベル15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
			this.ラベル15.Tag = "";
			this.ラベル15.Text = "消費税";
			this.ラベル15.Top = 1.120079F;
			this.ラベル15.Width = 0.8267718F;
			// 
			// ラベル16
			// 
			this.ラベル16.Height = 0.1965278F;
			this.ラベル16.HyperLink = null;
			this.ラベル16.Left = 6.440158F;
			this.ラベル16.Name = "ラベル16";
			this.ラベル16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
			this.ラベル16.Tag = "";
			this.ラベル16.Text = "金額計";
			this.ラベル16.Top = 1.120079F;
			this.ラベル16.Width = 0.8338586F;
			// 
			// 直線34
			// 
			this.直線34.Height = 0.3771321F;
			this.直線34.Left = 0.696897F;
			this.直線34.LineWeight = 1F;
			this.直線34.Name = "直線34";
			this.直線34.Tag = "";
			this.直線34.Top = 1.01378F;
			this.直線34.Width = 0.0001030564F;
			this.直線34.X1 = 0.697F;
			this.直線34.X2 = 0.696897F;
			this.直線34.Y1 = 1.01378F;
			this.直線34.Y2 = 1.390912F;
			// 
			// 直線35
			// 
			this.直線35.Height = 0.3818569F;
			this.直線35.Left = 3.280897F;
			this.直線35.LineWeight = 1F;
			this.直線35.Name = "直線35";
			this.直線35.Tag = "";
			this.直線35.Top = 1.016F;
			this.直線35.Width = 0.0001029968F;
			this.直線35.X1 = 3.281F;
			this.直線35.X2 = 3.280897F;
			this.直線35.Y1 = 1.016F;
			this.直線35.Y2 = 1.397857F;
			// 
			// 直線37
			// 
			this.直線37.Height = 0.377077F;
			this.直線37.Left = 4.162136F;
			this.直線37.LineWeight = 1F;
			this.直線37.Name = "直線37";
			this.直線37.Tag = "";
			this.直線37.Top = 1.013914F;
			this.直線37.Width = 0.0001788139F;
			this.直線37.X1 = 4.162315F;
			this.直線37.X2 = 4.162136F;
			this.直線37.Y1 = 1.013914F;
			this.直線37.Y2 = 1.390991F;
			// 
			// 直線38
			// 
			this.直線38.Height = 0.377077F;
			this.直線38.Left = 5.613205F;
			this.直線38.LineWeight = 1F;
			this.直線38.Name = "直線38";
			this.直線38.Tag = "";
			this.直線38.Top = 1.01378F;
			this.直線38.Width = 0.0001811981F;
			this.直線38.X1 = 5.613386F;
			this.直線38.X2 = 5.613205F;
			this.直線38.Y1 = 1.01378F;
			this.直線38.Y2 = 1.390857F;
			// 
			// 直線39
			// 
			this.直線39.Height = 0.377077F;
			this.直線39.Left = 6.439977F;
			this.直線39.LineWeight = 1F;
			this.直線39.Name = "直線39";
			this.直線39.Tag = "";
			this.直線39.Top = 1.01378F;
			this.直線39.Width = 0.0001807213F;
			this.直線39.X1 = 6.440158F;
			this.直線39.X2 = 6.439977F;
			this.直線39.Y1 = 1.01378F;
			this.直線39.Y2 = 1.390857F;
			// 
			// totalRowNo
			// 
			this.totalRowNo.DataField = "ITEM11";
			this.totalRowNo.Height = 0.1976378F;
			this.totalRowNo.Left = 1.261024F;
			this.totalRowNo.Name = "totalRowNo";
			this.totalRowNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
			this.totalRowNo.Tag = "";
			this.totalRowNo.Text = null;
			this.totalRowNo.Top = 0.3236221F;
			this.totalRowNo.Visible = false;
			this.totalRowNo.Width = 0.3826776F;
			// 
			// nowRouNo
			// 
			this.nowRouNo.CountNullValues = true;
			this.nowRouNo.DataField = "SORT";
			this.nowRouNo.Height = 0.15625F;
			this.nowRouNo.Left = 0.357874F;
			this.nowRouNo.Name = "nowRouNo";
			this.nowRouNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
			this.nowRouNo.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
			this.nowRouNo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.nowRouNo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.nowRouNo.Tag = "";
			this.nowRouNo.Text = "sort";
			this.nowRouNo.Top = 0.3649606F;
			this.nowRouNo.Visible = false;
			this.nowRouNo.Width = 0.3826773F;
			// 
			// textBox2
			// 
			this.textBox2.DataField = "ITEM02";
			this.textBox2.Height = 0.15625F;
			this.textBox2.Left = 0.1893702F;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 1";
			this.textBox2.Tag = "";
			this.textBox2.Text = "ITEM02";
			this.textBox2.Top = 0.7602363F;
			this.textBox2.Width = 1.454331F;
			// 
			// textBox4
			// 
			this.textBox4.CanGrow = false;
			this.textBox4.DataField = "ITEM10";
			this.textBox4.Height = 0.3015748F;
			this.textBox4.Left = 2.594488F;
			this.textBox4.MultiLine = false;
			this.textBox4.Name = "textBox4";
			this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: center; white-space: nowrap; ddo-char-set: 1; ddo-wrap" +
    "-mode: nowrap";
			this.textBox4.Tag = "";
			this.textBox4.Text = "ITEM10";
			this.textBox4.Top = 0.3236221F;
			this.textBox4.Width = 2.277165F;
			// 
			// textBox29
			// 
			this.textBox29.Height = 0.156F;
			this.textBox29.Left = 4.872F;
			this.textBox29.Name = "textBox29";
			this.textBox29.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 1";
			this.textBox29.Tag = "";
			this.textBox29.Text = "「*」は軽減税率であることを示します";
			this.textBox29.Top = 0.7600001F;
			this.textBox29.Width = 2.472F;
			// 
			// line12
			// 
			this.line12.Height = 0.377077F;
			this.line12.Left = 5.029819F;
			this.line12.LineWeight = 1F;
			this.line12.Name = "line12";
			this.line12.Tag = "";
			this.line12.Top = 1.014F;
			this.line12.Width = 0.0001811981F;
			this.line12.X1 = 5.03F;
			this.line12.X2 = 5.029819F;
			this.line12.Y1 = 1.014F;
			this.line12.Y2 = 1.391077F;
			// 
			// detail
			// 
			this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox8,
            this.ITEM03,
            this.ITEM05,
            this.ITEM06,
            this.ITEM08,
            this.ITEM09,
            this.ITEM10,
            this.直線30,
            this.直線31,
            this.直線40,
            this.直線42,
            this.直線44,
            this.直線45,
            this.直線46,
            this.line1,
            this.line11,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42});
			this.detail.Height = 0.1887959F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// textBox8
			// 
			this.textBox8.DataField = "ITEM12";
			this.textBox8.Height = 0.15625F;
			this.textBox8.Left = 5.047F;
			this.textBox8.Name = "textBox8";
			this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
			this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox8.Tag = "";
			this.textBox8.Text = "ITEM12";
			this.textBox8.Top = 0.038F;
			this.textBox8.Width = 0.535F;
			// 
			// ITEM03
			// 
			this.ITEM03.DataField = "ITEM04";
			this.ITEM03.Height = 0.15625F;
			this.ITEM03.Left = 0.2129921F;
			this.ITEM03.Name = "ITEM03";
			this.ITEM03.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.ITEM03.Tag = "";
			this.ITEM03.Text = "ITEM04";
			this.ITEM03.Top = 0.03779528F;
			this.ITEM03.Width = 0.4570079F;
			// 
			// ITEM05
			// 
			this.ITEM05.DataField = "ITEM05";
			this.ITEM05.Height = 0.15625F;
			this.ITEM05.Left = 0.764599F;
			this.ITEM05.MultiLine = false;
			this.ITEM05.Name = "ITEM05";
			this.ITEM05.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 128";
			this.ITEM05.Tag = "";
			this.ITEM05.Text = "ITEM05";
			this.ITEM05.Top = 0.03771624F;
			this.ITEM05.Width = 2.511519F;
			// 
			// ITEM06
			// 
			this.ITEM06.DataField = "ITEM06";
			this.ITEM06.Height = 0.15625F;
			this.ITEM06.Left = 3.281213F;
			this.ITEM06.Name = "ITEM06";
			this.ITEM06.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 128";
			this.ITEM06.Tag = "";
			this.ITEM06.Text = "ITEM06";
			this.ITEM06.Top = 0.03792939F;
			this.ITEM06.Width = 0.8807087F;
			// 
			// ITEM08
			// 
			this.ITEM08.DataField = "ITEM07";
			this.ITEM08.Height = 0.15625F;
			this.ITEM08.Left = 4.161922F;
			this.ITEM08.Name = "ITEM08";
			this.ITEM08.OutputFormat = resources.GetString("ITEM08.OutputFormat");
			this.ITEM08.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.ITEM08.Tag = "";
			this.ITEM08.Text = "ITEM07";
			this.ITEM08.Top = 0.03792939F;
			this.ITEM08.Width = 0.8270787F;
			// 
			// ITEM09
			// 
			this.ITEM09.DataField = "ITEM08";
			this.ITEM09.Height = 0.15625F;
			this.ITEM09.Left = 5.613386F;
			this.ITEM09.Name = "ITEM09";
			this.ITEM09.OutputFormat = resources.GetString("ITEM09.OutputFormat");
			this.ITEM09.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.ITEM09.Tag = "";
			this.ITEM09.Text = "ITEM08";
			this.ITEM09.Top = 0.03779528F;
			this.ITEM09.Width = 0.7641734F;
			// 
			// ITEM10
			// 
			this.ITEM10.DataField = "ITEM09";
			this.ITEM10.Height = 0.15625F;
			this.ITEM10.Left = 6.440158F;
			this.ITEM10.Name = "ITEM10";
			this.ITEM10.OutputFormat = resources.GetString("ITEM10.OutputFormat");
			this.ITEM10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.ITEM10.Tag = "";
			this.ITEM10.Text = "ITEM09";
			this.ITEM10.Top = 0.03779528F;
			this.ITEM10.Width = 0.8267716F;
			// 
			// 直線30
			// 
			this.直線30.Height = 0.1979167F;
			this.直線30.Left = 0.189F;
			this.直線30.LineWeight = 1F;
			this.直線30.Name = "直線30";
			this.直線30.Tag = "";
			this.直線30.Top = 0F;
			this.直線30.Width = 0F;
			this.直線30.X1 = 0.189F;
			this.直線30.X2 = 0.189F;
			this.直線30.Y1 = 0F;
			this.直線30.Y2 = 0.1979167F;
			// 
			// 直線31
			// 
			this.直線31.Height = 0.1979167F;
			this.直線31.Left = 7.344095F;
			this.直線31.LineWeight = 1F;
			this.直線31.Name = "直線31";
			this.直線31.Tag = "";
			this.直線31.Top = 0F;
			this.直線31.Width = 0F;
			this.直線31.X1 = 7.344095F;
			this.直線31.X2 = 7.344095F;
			this.直線31.Y1 = 0F;
			this.直線31.Y2 = 0.1979167F;
			// 
			// 直線40
			// 
			this.直線40.Height = 0.1965278F;
			this.直線40.Left = 0.697F;
			this.直線40.LineWeight = 1F;
			this.直線40.Name = "直線40";
			this.直線40.Tag = "";
			this.直線40.Top = 0F;
			this.直線40.Width = 0F;
			this.直線40.X1 = 0.697F;
			this.直線40.X2 = 0.697F;
			this.直線40.Y1 = 0F;
			this.直線40.Y2 = 0.1965278F;
			// 
			// 直線42
			// 
			this.直線42.Height = 0.1965278F;
			this.直線42.Left = 3.281F;
			this.直線42.LineWeight = 1F;
			this.直線42.Name = "直線42";
			this.直線42.Tag = "";
			this.直線42.Top = 0.002220035F;
			this.直線42.Width = 0F;
			this.直線42.X1 = 3.281F;
			this.直線42.X2 = 3.281F;
			this.直線42.Y1 = 0.002220035F;
			this.直線42.Y2 = 0.1987478F;
			// 
			// 直線44
			// 
			this.直線44.Height = 0.1965278F;
			this.直線44.Left = 4.162315F;
			this.直線44.LineWeight = 1F;
			this.直線44.Name = "直線44";
			this.直線44.Tag = "";
			this.直線44.Top = 0.0001341105F;
			this.直線44.Width = 0F;
			this.直線44.X1 = 4.162315F;
			this.直線44.X2 = 4.162315F;
			this.直線44.Y1 = 0.0001341105F;
			this.直線44.Y2 = 0.1966619F;
			// 
			// 直線45
			// 
			this.直線45.Height = 0.1965278F;
			this.直線45.Left = 5.613386F;
			this.直線45.LineWeight = 1F;
			this.直線45.Name = "直線45";
			this.直線45.Tag = "";
			this.直線45.Top = 0F;
			this.直線45.Width = 0F;
			this.直線45.X1 = 5.613386F;
			this.直線45.X2 = 5.613386F;
			this.直線45.Y1 = 0F;
			this.直線45.Y2 = 0.1965278F;
			// 
			// 直線46
			// 
			this.直線46.Height = 0.1965278F;
			this.直線46.Left = 6.440158F;
			this.直線46.LineWeight = 1F;
			this.直線46.Name = "直線46";
			this.直線46.Tag = "";
			this.直線46.Top = 0F;
			this.直線46.Width = 0F;
			this.直線46.X1 = 6.440158F;
			this.直線46.X2 = 6.440158F;
			this.直線46.Y1 = 0F;
			this.直線46.Y2 = 0.1965278F;
			// 
			// line1
			// 
			this.line1.Height = 3.150105E-05F;
			this.line1.Left = 0.189F;
			this.line1.LineWeight = 1F;
			this.line1.Name = "line1";
			this.line1.Tag = "";
			this.line1.Top = 0.198F;
			this.line1.Width = 7.155094F;
			this.line1.X1 = 0.189F;
			this.line1.X2 = 7.344094F;
			this.line1.Y1 = 0.198F;
			this.line1.Y2 = 0.1980315F;
			// 
			// line11
			// 
			this.line11.Height = 0.1965278F;
			this.line11.Left = 5.03F;
			this.line11.LineWeight = 1F;
			this.line11.Name = "line11";
			this.line11.Tag = "";
			this.line11.Top = 0F;
			this.line11.Width = 0F;
			this.line11.X1 = 5.03F;
			this.line11.X2 = 5.03F;
			this.line11.Y1 = 0F;
			this.line11.Y2 = 0.1965278F;
			// 
			// textBox39
			// 
			this.textBox39.DataField = "ITEM14";
			this.textBox39.Height = 0.15625F;
			this.textBox39.Left = 4.994F;
			this.textBox39.Name = "textBox39";
			this.textBox39.OutputFormat = resources.GetString("textBox39.OutputFormat");
			this.textBox39.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox39.Tag = "";
			this.textBox39.Text = "ITEM14";
			this.textBox39.Top = 0.319F;
			this.textBox39.Width = 0.535F;
			// 
			// textBox40
			// 
			this.textBox40.DataField = "ITEM18";
			this.textBox40.Height = 0.15625F;
			this.textBox40.Left = 4.994F;
			this.textBox40.Name = "textBox40";
			this.textBox40.OutputFormat = resources.GetString("textBox40.OutputFormat");
			this.textBox40.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox40.Tag = "";
			this.textBox40.Text = "ITEM18";
			this.textBox40.Top = 0.475F;
			this.textBox40.Width = 0.535F;
			// 
			// textBox41
			// 
			this.textBox41.DataField = "ITEM22";
			this.textBox41.Height = 0.15625F;
			this.textBox41.Left = 4.989F;
			this.textBox41.Name = "textBox41";
			this.textBox41.OutputFormat = resources.GetString("textBox41.OutputFormat");
			this.textBox41.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox41.Tag = "";
			this.textBox41.Text = "ITEM22";
			this.textBox41.Top = 0.631F;
			this.textBox41.Width = 0.535F;
			// 
			// textBox42
			// 
			this.textBox42.DataField = "ITEM26";
			this.textBox42.Height = 0.15625F;
			this.textBox42.Left = 4.989001F;
			this.textBox42.Name = "textBox42";
			this.textBox42.OutputFormat = resources.GetString("textBox42.OutputFormat");
			this.textBox42.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox42.Tag = "";
			this.textBox42.Text = "ITEM26";
			this.textBox42.Top = 0.7870001F;
			this.textBox42.Width = 0.535F;
			// 
			// pageFooter
			// 
			this.pageFooter.Height = 0F;
			this.pageFooter.Name = "pageFooter";
			this.pageFooter.Format += new System.EventHandler(this.pageFooter_Format);
			// 
			// reportHeader1
			// 
			this.reportHeader1.Height = 0F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// reportFooter1
			// 
			this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox21,
            this.textBox17,
            this.textBox13,
            this.textBox9,
            this.textBox3,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.line28,
            this.line29,
            this.line30,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.line31,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line36,
            this.line37,
            this.line38,
            this.line39,
            this.line40,
            this.line41,
            this.line42,
            this.line43,
            this.line44,
            this.line45,
            this.line46,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.line47,
            this.line48,
            this.line49,
            this.line50,
            this.line51,
            this.line52,
            this.line53,
            this.line54,
            this.line55,
            this.line56,
            this.line57});
			this.reportFooter1.Height = 1.8125F;
			this.reportFooter1.Name = "reportFooter1";
			this.reportFooter1.AfterPrint += new System.EventHandler(this.reportFooter1_AfterPrint);
			// 
			// textBox21
			// 
			this.textBox21.DataField = "ITEM26";
			this.textBox21.Height = 0.15625F;
			this.textBox21.Left = 5.052F;
			this.textBox21.Name = "textBox21";
			this.textBox21.OutputFormat = resources.GetString("textBox21.OutputFormat");
			this.textBox21.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox21.Tag = "";
			this.textBox21.Text = "ITEM26";
			this.textBox21.Top = 0.634F;
			this.textBox21.Width = 0.5349997F;
			// 
			// textBox17
			// 
			this.textBox17.DataField = "ITEM22";
			this.textBox17.Height = 0.15625F;
			this.textBox17.Left = 5.047F;
			this.textBox17.Name = "textBox17";
			this.textBox17.OutputFormat = resources.GetString("textBox17.OutputFormat");
			this.textBox17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128; ddo" +
    "-font-vertical: none";
			this.textBox17.Tag = "";
			this.textBox17.Text = "ITEM22";
			this.textBox17.Top = 0.435F;
			this.textBox17.Width = 0.5349997F;
			// 
			// textBox13
			// 
			this.textBox13.DataField = "ITEM18";
			this.textBox13.Height = 0.15625F;
			this.textBox13.Left = 5.047F;
			this.textBox13.Name = "textBox13";
			this.textBox13.OutputFormat = resources.GetString("textBox13.OutputFormat");
			this.textBox13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox13.Tag = "";
			this.textBox13.Text = "ITEM18";
			this.textBox13.Top = 0.236F;
			this.textBox13.Width = 0.5349997F;
			// 
			// textBox9
			// 
			this.textBox9.DataField = "ITEM14";
			this.textBox9.Height = 0.1560706F;
			this.textBox9.Left = 5.047F;
			this.textBox9.Name = "textBox9";
			this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
			this.textBox9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox9.Tag = "";
			this.textBox9.Text = "ITEM14";
			this.textBox9.Top = 0.038F;
			this.textBox9.Width = 0.5350002F;
			// 
			// textBox3
			// 
			this.textBox3.Height = 0.15625F;
			this.textBox3.Left = 0.697F;
			this.textBox3.MultiLine = false;
			this.textBox3.Name = "textBox3";
			this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 128";
			this.textBox3.Tag = "";
			this.textBox3.Text = "【消費税内訳合計】";
			this.textBox3.Top = 0.03771624F;
			this.textBox3.Width = 2.574F;
			// 
			// textBox5
			// 
			this.textBox5.DataField = "ITEM13";
			this.textBox5.Height = 0.1560706F;
			this.textBox5.Left = 4.162F;
			this.textBox5.Name = "textBox5";
			this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
			this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox5.Tag = "";
			this.textBox5.Text = "ITEM13";
			this.textBox5.Top = 0.03792939F;
			this.textBox5.Width = 0.8270001F;
			// 
			// textBox6
			// 
			this.textBox6.DataField = "ITEM15";
			this.textBox6.Height = 0.15625F;
			this.textBox6.Left = 5.613F;
			this.textBox6.Name = "textBox6";
			this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
			this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox6.Tag = "";
			this.textBox6.Text = "ITEM15";
			this.textBox6.Top = 0.03779528F;
			this.textBox6.Width = 0.7645593F;
			// 
			// textBox7
			// 
			this.textBox7.DataField = "ITEM16";
			this.textBox7.Height = 0.15625F;
			this.textBox7.Left = 6.43504F;
			this.textBox7.Name = "textBox7";
			this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
			this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox7.Tag = "";
			this.textBox7.Text = "ITEM16";
			this.textBox7.Top = 0.03779528F;
			this.textBox7.Width = 0.8267716F;
			// 
			// line2
			// 
			this.line2.Height = 0.1979167F;
			this.line2.Left = 0.189F;
			this.line2.LineWeight = 1F;
			this.line2.Name = "line2";
			this.line2.Tag = "";
			this.line2.Top = 0F;
			this.line2.Width = 0F;
			this.line2.X1 = 0.189F;
			this.line2.X2 = 0.189F;
			this.line2.Y1 = 0F;
			this.line2.Y2 = 0.1979167F;
			// 
			// line3
			// 
			this.line3.Height = 0.1979167F;
			this.line3.Left = 7.344094F;
			this.line3.LineWeight = 1F;
			this.line3.Name = "line3";
			this.line3.Tag = "";
			this.line3.Top = 0F;
			this.line3.Width = 0F;
			this.line3.X1 = 7.344094F;
			this.line3.X2 = 7.344094F;
			this.line3.Y1 = 0F;
			this.line3.Y2 = 0.1979167F;
			// 
			// line4
			// 
			this.line4.Height = 0.1965278F;
			this.line4.Left = 0.697F;
			this.line4.LineWeight = 1F;
			this.line4.Name = "line4";
			this.line4.Tag = "";
			this.line4.Top = 0F;
			this.line4.Width = 0F;
			this.line4.X1 = 0.697F;
			this.line4.X2 = 0.697F;
			this.line4.Y1 = 0F;
			this.line4.Y2 = 0.1965278F;
			// 
			// line5
			// 
			this.line5.Height = 0.1965278F;
			this.line5.Left = 3.281F;
			this.line5.LineWeight = 1F;
			this.line5.Name = "line5";
			this.line5.Tag = "";
			this.line5.Top = 0.002220035F;
			this.line5.Width = 0F;
			this.line5.X1 = 3.281F;
			this.line5.X2 = 3.281F;
			this.line5.Y1 = 0.002220035F;
			this.line5.Y2 = 0.1987478F;
			// 
			// line6
			// 
			this.line6.Height = 0.1965278F;
			this.line6.Left = 4.162F;
			this.line6.LineWeight = 1F;
			this.line6.Name = "line6";
			this.line6.Tag = "";
			this.line6.Top = 0.0001341105F;
			this.line6.Width = 0F;
			this.line6.X1 = 4.162F;
			this.line6.X2 = 4.162F;
			this.line6.Y1 = 0.0001341105F;
			this.line6.Y2 = 0.1966619F;
			// 
			// line7
			// 
			this.line7.Height = 0.1965278F;
			this.line7.Left = 5.613F;
			this.line7.LineWeight = 1F;
			this.line7.Name = "line7";
			this.line7.Tag = "";
			this.line7.Top = 0F;
			this.line7.Width = 0F;
			this.line7.X1 = 5.613F;
			this.line7.X2 = 5.613F;
			this.line7.Y1 = 0F;
			this.line7.Y2 = 0.1965278F;
			// 
			// line8
			// 
			this.line8.Height = 0.1965278F;
			this.line8.Left = 6.43504F;
			this.line8.LineWeight = 1F;
			this.line8.Name = "line8";
			this.line8.Tag = "";
			this.line8.Top = 0F;
			this.line8.Width = 0F;
			this.line8.X1 = 6.43504F;
			this.line8.X2 = 6.43504F;
			this.line8.Y1 = 0F;
			this.line8.Y2 = 0.1965278F;
			// 
			// line9
			// 
			this.line9.Height = 9.448826E-05F;
			this.line9.Left = 0.189F;
			this.line9.LineWeight = 1F;
			this.line9.Name = "line9";
			this.line9.Tag = "";
			this.line9.Top = 0.194F;
			this.line9.Width = 7.155094F;
			this.line9.X1 = 0.189F;
			this.line9.X2 = 7.344094F;
			this.line9.Y1 = 0.194F;
			this.line9.Y2 = 0.1940945F;
			// 
			// line10
			// 
			this.line10.Height = 0.1965278F;
			this.line10.Left = 5.03F;
			this.line10.LineWeight = 1F;
			this.line10.Name = "line10";
			this.line10.Tag = "";
			this.line10.Top = 0F;
			this.line10.Width = 0F;
			this.line10.X1 = 5.03F;
			this.line10.X2 = 5.03F;
			this.line10.Y1 = 0F;
			this.line10.Y2 = 0.1965278F;
			// 
			// textBox10
			// 
			this.textBox10.DataField = "ITEM17";
			this.textBox10.Height = 0.15625F;
			this.textBox10.Left = 4.162F;
			this.textBox10.Name = "textBox10";
			this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
			this.textBox10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox10.Tag = "";
			this.textBox10.Text = "ITEM17";
			this.textBox10.Top = 0.2362132F;
			this.textBox10.Width = 0.827F;
			// 
			// textBox11
			// 
			this.textBox11.DataField = "ITEM19";
			this.textBox11.Height = 0.15625F;
			this.textBox11.Left = 5.613001F;
			this.textBox11.Name = "textBox11";
			this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
			this.textBox11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox11.Tag = "";
			this.textBox11.Text = "ITEM19";
			this.textBox11.Top = 0.2360791F;
			this.textBox11.Width = 0.764559F;
			// 
			// textBox12
			// 
			this.textBox12.DataField = "ITEM20";
			this.textBox12.Height = 0.15625F;
			this.textBox12.Left = 6.43504F;
			this.textBox12.Name = "textBox12";
			this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
			this.textBox12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox12.Tag = "";
			this.textBox12.Text = "ITEM20";
			this.textBox12.Top = 0.2360791F;
			this.textBox12.Width = 0.8267716F;
			// 
			// line13
			// 
			this.line13.Height = 0.1979167F;
			this.line13.Left = 0.1890001F;
			this.line13.LineWeight = 1F;
			this.line13.Name = "line13";
			this.line13.Tag = "";
			this.line13.Top = 0.1982838F;
			this.line13.Width = 0F;
			this.line13.X1 = 0.1890001F;
			this.line13.X2 = 0.1890001F;
			this.line13.Y1 = 0.1982838F;
			this.line13.Y2 = 0.3962005F;
			// 
			// line14
			// 
			this.line14.Height = 0.1979167F;
			this.line14.Left = 7.344097F;
			this.line14.LineWeight = 1F;
			this.line14.Name = "line14";
			this.line14.Tag = "";
			this.line14.Top = 0.1982838F;
			this.line14.Width = 0F;
			this.line14.X1 = 7.344097F;
			this.line14.X2 = 7.344097F;
			this.line14.Y1 = 0.1982838F;
			this.line14.Y2 = 0.3962005F;
			// 
			// line15
			// 
			this.line15.Height = 0.1965278F;
			this.line15.Left = 0.697F;
			this.line15.LineWeight = 1F;
			this.line15.Name = "line15";
			this.line15.Tag = "";
			this.line15.Top = 0.1982838F;
			this.line15.Width = 0F;
			this.line15.X1 = 0.697F;
			this.line15.X2 = 0.697F;
			this.line15.Y1 = 0.1982838F;
			this.line15.Y2 = 0.3948116F;
			// 
			// line16
			// 
			this.line16.Height = 0.1965278F;
			this.line16.Left = 3.281F;
			this.line16.LineWeight = 1F;
			this.line16.Name = "line16";
			this.line16.Tag = "";
			this.line16.Top = 0.2005038F;
			this.line16.Width = 0F;
			this.line16.X1 = 3.281F;
			this.line16.X2 = 3.281F;
			this.line16.Y1 = 0.2005038F;
			this.line16.Y2 = 0.3970316F;
			// 
			// line17
			// 
			this.line17.Height = 0.1965278F;
			this.line17.Left = 4.162F;
			this.line17.LineWeight = 1F;
			this.line17.Name = "line17";
			this.line17.Tag = "";
			this.line17.Top = 0.1984179F;
			this.line17.Width = 0F;
			this.line17.X1 = 4.162F;
			this.line17.X2 = 4.162F;
			this.line17.Y1 = 0.1984179F;
			this.line17.Y2 = 0.3949457F;
			// 
			// line18
			// 
			this.line18.Height = 0.1965278F;
			this.line18.Left = 5.613F;
			this.line18.LineWeight = 1F;
			this.line18.Name = "line18";
			this.line18.Tag = "";
			this.line18.Top = 0.1982838F;
			this.line18.Width = 0F;
			this.line18.X1 = 5.613F;
			this.line18.X2 = 5.613F;
			this.line18.Y1 = 0.1982838F;
			this.line18.Y2 = 0.3948116F;
			// 
			// line19
			// 
			this.line19.Height = 0.1965278F;
			this.line19.Left = 6.43504F;
			this.line19.LineWeight = 1F;
			this.line19.Name = "line19";
			this.line19.Tag = "";
			this.line19.Top = 0.1982838F;
			this.line19.Width = 0F;
			this.line19.X1 = 6.43504F;
			this.line19.X2 = 6.43504F;
			this.line19.Y1 = 0.1982838F;
			this.line19.Y2 = 0.3948116F;
			// 
			// line20
			// 
			this.line20.Height = 9.459257E-05F;
			this.line20.Left = 0.1890001F;
			this.line20.LineWeight = 1F;
			this.line20.Name = "line20";
			this.line20.Tag = "";
			this.line20.Top = 0.3922837F;
			this.line20.Width = 7.155097F;
			this.line20.X1 = 0.1890001F;
			this.line20.X2 = 7.344097F;
			this.line20.Y1 = 0.3922837F;
			this.line20.Y2 = 0.3923783F;
			// 
			// line21
			// 
			this.line21.Height = 0.1965278F;
			this.line21.Left = 5.03F;
			this.line21.LineWeight = 1F;
			this.line21.Name = "line21";
			this.line21.Tag = "";
			this.line21.Top = 0.1982838F;
			this.line21.Width = 0F;
			this.line21.X1 = 5.03F;
			this.line21.X2 = 5.03F;
			this.line21.Y1 = 0.1982838F;
			this.line21.Y2 = 0.3948116F;
			// 
			// textBox14
			// 
			this.textBox14.DataField = "ITEM21";
			this.textBox14.Height = 0.15625F;
			this.textBox14.Left = 4.162F;
			this.textBox14.Name = "textBox14";
			this.textBox14.OutputFormat = resources.GetString("textBox14.OutputFormat");
			this.textBox14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox14.Tag = "";
			this.textBox14.Text = "ITEM21";
			this.textBox14.Top = 0.4352132F;
			this.textBox14.Width = 0.827F;
			// 
			// textBox15
			// 
			this.textBox15.DataField = "ITEM23";
			this.textBox15.Height = 0.15625F;
			this.textBox15.Left = 5.613001F;
			this.textBox15.Name = "textBox15";
			this.textBox15.OutputFormat = resources.GetString("textBox15.OutputFormat");
			this.textBox15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox15.Tag = "";
			this.textBox15.Text = "ITEM23";
			this.textBox15.Top = 0.4350791F;
			this.textBox15.Width = 0.764559F;
			// 
			// textBox16
			// 
			this.textBox16.DataField = "ITEM24";
			this.textBox16.Height = 0.15625F;
			this.textBox16.Left = 6.43504F;
			this.textBox16.Name = "textBox16";
			this.textBox16.OutputFormat = resources.GetString("textBox16.OutputFormat");
			this.textBox16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox16.Tag = "";
			this.textBox16.Text = "ITEM24";
			this.textBox16.Top = 0.4350791F;
			this.textBox16.Width = 0.8267716F;
			// 
			// line22
			// 
			this.line22.Height = 0.1979166F;
			this.line22.Left = 0.1890001F;
			this.line22.LineWeight = 1F;
			this.line22.Name = "line22";
			this.line22.Tag = "";
			this.line22.Top = 0.3972838F;
			this.line22.Width = 0F;
			this.line22.X1 = 0.1890001F;
			this.line22.X2 = 0.1890001F;
			this.line22.Y1 = 0.3972838F;
			this.line22.Y2 = 0.5952004F;
			// 
			// line23
			// 
			this.line23.Height = 0.1979166F;
			this.line23.Left = 7.344097F;
			this.line23.LineWeight = 1F;
			this.line23.Name = "line23";
			this.line23.Tag = "";
			this.line23.Top = 0.3972838F;
			this.line23.Width = 0F;
			this.line23.X1 = 7.344097F;
			this.line23.X2 = 7.344097F;
			this.line23.Y1 = 0.3972838F;
			this.line23.Y2 = 0.5952004F;
			// 
			// line24
			// 
			this.line24.Height = 0.1965278F;
			this.line24.Left = 0.697F;
			this.line24.LineWeight = 1F;
			this.line24.Name = "line24";
			this.line24.Tag = "";
			this.line24.Top = 0.3972838F;
			this.line24.Width = 0F;
			this.line24.X1 = 0.697F;
			this.line24.X2 = 0.697F;
			this.line24.Y1 = 0.3972838F;
			this.line24.Y2 = 0.5938116F;
			// 
			// line25
			// 
			this.line25.Height = 0.1965276F;
			this.line25.Left = 3.281F;
			this.line25.LineWeight = 1F;
			this.line25.Name = "line25";
			this.line25.Tag = "";
			this.line25.Top = 0.3995039F;
			this.line25.Width = 0F;
			this.line25.X1 = 3.281F;
			this.line25.X2 = 3.281F;
			this.line25.Y1 = 0.3995039F;
			this.line25.Y2 = 0.5960315F;
			// 
			// line26
			// 
			this.line26.Height = 0.1965277F;
			this.line26.Left = 4.162F;
			this.line26.LineWeight = 1F;
			this.line26.Name = "line26";
			this.line26.Tag = "";
			this.line26.Top = 0.3974179F;
			this.line26.Width = 0F;
			this.line26.X1 = 4.162F;
			this.line26.X2 = 4.162F;
			this.line26.Y1 = 0.3974179F;
			this.line26.Y2 = 0.5939456F;
			// 
			// line27
			// 
			this.line27.Height = 0.1965278F;
			this.line27.Left = 5.613F;
			this.line27.LineWeight = 1F;
			this.line27.Name = "line27";
			this.line27.Tag = "";
			this.line27.Top = 0.3972838F;
			this.line27.Width = 0F;
			this.line27.X1 = 5.613F;
			this.line27.X2 = 5.613F;
			this.line27.Y1 = 0.3972838F;
			this.line27.Y2 = 0.5938116F;
			// 
			// line28
			// 
			this.line28.Height = 0.1965278F;
			this.line28.Left = 6.43504F;
			this.line28.LineWeight = 1F;
			this.line28.Name = "line28";
			this.line28.Tag = "";
			this.line28.Top = 0.3972838F;
			this.line28.Width = 0F;
			this.line28.X1 = 6.43504F;
			this.line28.X2 = 6.43504F;
			this.line28.Y1 = 0.3972838F;
			this.line28.Y2 = 0.5938116F;
			// 
			// line29
			// 
			this.line29.Height = 9.459257E-05F;
			this.line29.Left = 0.1890001F;
			this.line29.LineWeight = 1F;
			this.line29.Name = "line29";
			this.line29.Tag = "";
			this.line29.Top = 0.5912837F;
			this.line29.Width = 7.155097F;
			this.line29.X1 = 0.1890001F;
			this.line29.X2 = 7.344097F;
			this.line29.Y1 = 0.5912837F;
			this.line29.Y2 = 0.5913783F;
			// 
			// line30
			// 
			this.line30.Height = 0.1965278F;
			this.line30.Left = 5.03F;
			this.line30.LineWeight = 1F;
			this.line30.Name = "line30";
			this.line30.Tag = "";
			this.line30.Top = 0.3972838F;
			this.line30.Width = 0F;
			this.line30.X1 = 5.03F;
			this.line30.X2 = 5.03F;
			this.line30.Y1 = 0.3972838F;
			this.line30.Y2 = 0.5938116F;
			// 
			// textBox18
			// 
			this.textBox18.DataField = "ITEM25";
			this.textBox18.Height = 0.15625F;
			this.textBox18.Left = 4.166999F;
			this.textBox18.Name = "textBox18";
			this.textBox18.OutputFormat = resources.GetString("textBox18.OutputFormat");
			this.textBox18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox18.Tag = "";
			this.textBox18.Text = "ITEM25";
			this.textBox18.Top = 0.6342131F;
			this.textBox18.Width = 0.827F;
			// 
			// textBox19
			// 
			this.textBox19.DataField = "ITEM27";
			this.textBox19.Height = 0.15625F;
			this.textBox19.Left = 5.618001F;
			this.textBox19.Name = "textBox19";
			this.textBox19.OutputFormat = resources.GetString("textBox19.OutputFormat");
			this.textBox19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox19.Tag = "";
			this.textBox19.Text = "ITEM27";
			this.textBox19.Top = 0.634079F;
			this.textBox19.Width = 0.764559F;
			// 
			// textBox20
			// 
			this.textBox20.DataField = "ITEM28";
			this.textBox20.Height = 0.15625F;
			this.textBox20.Left = 6.44004F;
			this.textBox20.Name = "textBox20";
			this.textBox20.OutputFormat = resources.GetString("textBox20.OutputFormat");
			this.textBox20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox20.Tag = "";
			this.textBox20.Text = "ITEM28";
			this.textBox20.Top = 0.634079F;
			this.textBox20.Width = 0.8267716F;
			// 
			// line31
			// 
			this.line31.Height = 0.1979168F;
			this.line31.Left = 0.189F;
			this.line31.LineWeight = 1F;
			this.line31.Name = "line31";
			this.line31.Tag = "";
			this.line31.Top = 0.596F;
			this.line31.Width = 0F;
			this.line31.X1 = 0.189F;
			this.line31.X2 = 0.189F;
			this.line31.Y1 = 0.596F;
			this.line31.Y2 = 0.7939168F;
			// 
			// line32
			// 
			this.line32.Height = 0.1965278F;
			this.line32.Left = 0.697F;
			this.line32.LineWeight = 1F;
			this.line32.Name = "line32";
			this.line32.Tag = "";
			this.line32.Top = 0.5962837F;
			this.line32.Width = 0F;
			this.line32.X1 = 0.697F;
			this.line32.X2 = 0.697F;
			this.line32.Y1 = 0.5962837F;
			this.line32.Y2 = 0.7928115F;
			// 
			// line33
			// 
			this.line33.Height = 0.1965278F;
			this.line33.Left = 3.281F;
			this.line33.LineWeight = 1F;
			this.line33.Name = "line33";
			this.line33.Tag = "";
			this.line33.Top = 0.5985038F;
			this.line33.Width = 0F;
			this.line33.X1 = 3.281F;
			this.line33.X2 = 3.281F;
			this.line33.Y1 = 0.5985038F;
			this.line33.Y2 = 0.7950316F;
			// 
			// line34
			// 
			this.line34.Height = 0.1965278F;
			this.line34.Left = 4.162F;
			this.line34.LineWeight = 1F;
			this.line34.Name = "line34";
			this.line34.Tag = "";
			this.line34.Top = 0.5964178F;
			this.line34.Width = 0F;
			this.line34.X1 = 4.162F;
			this.line34.X2 = 4.162F;
			this.line34.Y1 = 0.5964178F;
			this.line34.Y2 = 0.7929456F;
			// 
			// line35
			// 
			this.line35.Height = 0.1965278F;
			this.line35.Left = 5.613F;
			this.line35.LineWeight = 1F;
			this.line35.Name = "line35";
			this.line35.Tag = "";
			this.line35.Top = 0.5962837F;
			this.line35.Width = 0F;
			this.line35.X1 = 5.613F;
			this.line35.X2 = 5.613F;
			this.line35.Y1 = 0.5962837F;
			this.line35.Y2 = 0.7928115F;
			// 
			// line36
			// 
			this.line36.Height = 0.1965278F;
			this.line36.Left = 6.435F;
			this.line36.LineWeight = 1F;
			this.line36.Name = "line36";
			this.line36.Tag = "";
			this.line36.Top = 0.5962837F;
			this.line36.Width = 0F;
			this.line36.X1 = 6.435F;
			this.line36.X2 = 6.435F;
			this.line36.Y1 = 0.5962837F;
			this.line36.Y2 = 0.7928115F;
			// 
			// line37
			// 
			this.line37.Height = 9.459257E-05F;
			this.line37.Left = 0.189F;
			this.line37.LineWeight = 1F;
			this.line37.Name = "line37";
			this.line37.Tag = "";
			this.line37.Top = 0.79F;
			this.line37.Width = 7.155097F;
			this.line37.X1 = 0.189F;
			this.line37.X2 = 7.344097F;
			this.line37.Y1 = 0.79F;
			this.line37.Y2 = 0.7900946F;
			// 
			// line38
			// 
			this.line38.Height = 0.1965278F;
			this.line38.Left = 5.03F;
			this.line38.LineWeight = 1F;
			this.line38.Name = "line38";
			this.line38.Tag = "";
			this.line38.Top = 0.5962837F;
			this.line38.Width = 0F;
			this.line38.X1 = 5.03F;
			this.line38.X2 = 5.03F;
			this.line38.Y1 = 0.5962837F;
			this.line38.Y2 = 0.7928115F;
			// 
			// line39
			// 
			this.line39.Height = 0.1979167F;
			this.line39.Left = 0.1890001F;
			this.line39.LineWeight = 1F;
			this.line39.Name = "line39";
			this.line39.Tag = "";
			this.line39.Top = 0.7942837F;
			this.line39.Width = 0F;
			this.line39.X1 = 0.1890001F;
			this.line39.X2 = 0.1890001F;
			this.line39.Y1 = 0.7942837F;
			this.line39.Y2 = 0.9922004F;
			// 
			// line40
			// 
			this.line40.Height = 0.1965279F;
			this.line40.Left = 0.697F;
			this.line40.LineWeight = 1F;
			this.line40.Name = "line40";
			this.line40.Tag = "";
			this.line40.Top = 0.7945675F;
			this.line40.Width = 0F;
			this.line40.X1 = 0.697F;
			this.line40.X2 = 0.697F;
			this.line40.Y1 = 0.7945675F;
			this.line40.Y2 = 0.9910954F;
			// 
			// line41
			// 
			this.line41.Height = 0.1965277F;
			this.line41.Left = 3.281F;
			this.line41.LineWeight = 1F;
			this.line41.Name = "line41";
			this.line41.Tag = "";
			this.line41.Top = 0.7967876F;
			this.line41.Width = 0F;
			this.line41.X1 = 3.281F;
			this.line41.X2 = 3.281F;
			this.line41.Y1 = 0.7967876F;
			this.line41.Y2 = 0.9933153F;
			// 
			// line42
			// 
			this.line42.Height = 0.1965278F;
			this.line42.Left = 4.162F;
			this.line42.LineWeight = 1F;
			this.line42.Name = "line42";
			this.line42.Tag = "";
			this.line42.Top = 0.7947016F;
			this.line42.Width = 0F;
			this.line42.X1 = 4.162F;
			this.line42.X2 = 4.162F;
			this.line42.Y1 = 0.7947016F;
			this.line42.Y2 = 0.9912294F;
			// 
			// line43
			// 
			this.line43.Height = 0.1965279F;
			this.line43.Left = 5.613F;
			this.line43.LineWeight = 1F;
			this.line43.Name = "line43";
			this.line43.Tag = "";
			this.line43.Top = 0.7945675F;
			this.line43.Width = 0F;
			this.line43.X1 = 5.613F;
			this.line43.X2 = 5.613F;
			this.line43.Y1 = 0.7945675F;
			this.line43.Y2 = 0.9910954F;
			// 
			// line44
			// 
			this.line44.Height = 0.1965279F;
			this.line44.Left = 6.435F;
			this.line44.LineWeight = 1F;
			this.line44.Name = "line44";
			this.line44.Tag = "";
			this.line44.Top = 0.7945675F;
			this.line44.Width = 0F;
			this.line44.X1 = 6.435F;
			this.line44.X2 = 6.435F;
			this.line44.Y1 = 0.7945675F;
			this.line44.Y2 = 0.9910954F;
			// 
			// line45
			// 
			this.line45.Height = 9.447336E-05F;
			this.line45.Left = 0.1890001F;
			this.line45.LineWeight = 1F;
			this.line45.Name = "line45";
			this.line45.Tag = "";
			this.line45.Top = 0.9882838F;
			this.line45.Width = 7.155097F;
			this.line45.X1 = 0.1890001F;
			this.line45.X2 = 7.344097F;
			this.line45.Y1 = 0.9882838F;
			this.line45.Y2 = 0.9883783F;
			// 
			// line46
			// 
			this.line46.Height = 0.1965279F;
			this.line46.Left = 5.03F;
			this.line46.LineWeight = 1F;
			this.line46.Name = "line46";
			this.line46.Tag = "";
			this.line46.Top = 0.7945675F;
			this.line46.Width = 0F;
			this.line46.X1 = 5.03F;
			this.line46.X2 = 5.03F;
			this.line46.Y1 = 0.7945675F;
			this.line46.Y2 = 0.9910954F;
			// 
			// textBox25
			// 
			this.textBox25.Height = 0.15625F;
			this.textBox25.Left = 0.702F;
			this.textBox25.MultiLine = false;
			this.textBox25.Name = "textBox25";
			this.textBox25.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 128";
			this.textBox25.Tag = "";
			this.textBox25.Text = "【　　合　　計　　】";
			this.textBox25.Top = 1.031284F;
			this.textBox25.Width = 2.574F;
			// 
			// textBox26
			// 
			this.textBox26.Height = 0.15625F;
			this.textBox26.Left = 4.166999F;
			this.textBox26.Name = "textBox26";
			this.textBox26.OutputFormat = resources.GetString("textBox26.OutputFormat");
			this.textBox26.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox26.Tag = "";
			this.textBox26.Text = "ITEM07";
			this.textBox26.Top = 1.031497F;
			this.textBox26.Width = 0.827F;
			// 
			// textBox27
			// 
			this.textBox27.Height = 0.15625F;
			this.textBox27.Left = 5.618001F;
			this.textBox27.Name = "textBox27";
			this.textBox27.OutputFormat = resources.GetString("textBox27.OutputFormat");
			this.textBox27.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox27.Tag = "";
			this.textBox27.Text = "ITEM08";
			this.textBox27.Top = 1.031363F;
			this.textBox27.Width = 0.764559F;
			// 
			// textBox28
			// 
			this.textBox28.Height = 0.15625F;
			this.textBox28.Left = 6.44004F;
			this.textBox28.Name = "textBox28";
			this.textBox28.OutputFormat = resources.GetString("textBox28.OutputFormat");
			this.textBox28.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 128";
			this.textBox28.Tag = "";
			this.textBox28.Text = "ITEM09";
			this.textBox28.Top = 1.031363F;
			this.textBox28.Width = 0.8267716F;
			// 
			// line47
			// 
			this.line47.Height = 0.1979162F;
			this.line47.Left = 0.1890001F;
			this.line47.LineWeight = 1F;
			this.line47.Name = "line47";
			this.line47.Tag = "";
			this.line47.Top = 0.9932838F;
			this.line47.Width = 0F;
			this.line47.X1 = 0.1890001F;
			this.line47.X2 = 0.1890001F;
			this.line47.Y1 = 0.9932838F;
			this.line47.Y2 = 1.1912F;
			// 
			// line48
			// 
			this.line48.Height = 0.1965274F;
			this.line48.Left = 0.697F;
			this.line48.LineWeight = 1F;
			this.line48.Name = "line48";
			this.line48.Tag = "";
			this.line48.Top = 0.9935675F;
			this.line48.Width = 0F;
			this.line48.X1 = 0.697F;
			this.line48.X2 = 0.697F;
			this.line48.Y1 = 0.9935675F;
			this.line48.Y2 = 1.190095F;
			// 
			// line49
			// 
			this.line49.Height = 0.1965273F;
			this.line49.Left = 3.281F;
			this.line49.LineWeight = 1F;
			this.line49.Name = "line49";
			this.line49.Tag = "";
			this.line49.Top = 0.9957877F;
			this.line49.Width = 0F;
			this.line49.X1 = 3.281F;
			this.line49.X2 = 3.281F;
			this.line49.Y1 = 0.9957877F;
			this.line49.Y2 = 1.192315F;
			// 
			// line50
			// 
			this.line50.Height = 0.1965275F;
			this.line50.Left = 4.162F;
			this.line50.LineWeight = 1F;
			this.line50.Name = "line50";
			this.line50.Tag = "";
			this.line50.Top = 0.9937015F;
			this.line50.Width = 0F;
			this.line50.X1 = 4.162F;
			this.line50.X2 = 4.162F;
			this.line50.Y1 = 0.9937015F;
			this.line50.Y2 = 1.190229F;
			// 
			// line51
			// 
			this.line51.Height = 0.1965274F;
			this.line51.Left = 5.613F;
			this.line51.LineWeight = 1F;
			this.line51.Name = "line51";
			this.line51.Tag = "";
			this.line51.Top = 0.9935675F;
			this.line51.Width = 0F;
			this.line51.X1 = 5.613F;
			this.line51.X2 = 5.613F;
			this.line51.Y1 = 0.9935675F;
			this.line51.Y2 = 1.190095F;
			// 
			// line52
			// 
			this.line52.Height = 0.1965274F;
			this.line52.Left = 6.435F;
			this.line52.LineWeight = 1F;
			this.line52.Name = "line52";
			this.line52.Tag = "";
			this.line52.Top = 0.9935675F;
			this.line52.Width = 0F;
			this.line52.X1 = 6.435F;
			this.line52.X2 = 6.435F;
			this.line52.Y1 = 0.9935675F;
			this.line52.Y2 = 1.190095F;
			// 
			// line53
			// 
			this.line53.Height = 9.50098E-05F;
			this.line53.Left = 0.189F;
			this.line53.LineWeight = 1F;
			this.line53.Name = "line53";
			this.line53.Tag = "";
			this.line53.Top = 1.188F;
			this.line53.Width = 7.155097F;
			this.line53.X1 = 0.189F;
			this.line53.X2 = 7.344097F;
			this.line53.Y1 = 1.188F;
			this.line53.Y2 = 1.188095F;
			// 
			// line54
			// 
			this.line54.Height = 0.1965274F;
			this.line54.Left = 5.03F;
			this.line54.LineWeight = 1F;
			this.line54.Name = "line54";
			this.line54.Tag = "";
			this.line54.Top = 0.9935675F;
			this.line54.Width = 0F;
			this.line54.X1 = 5.03F;
			this.line54.X2 = 5.03F;
			this.line54.Y1 = 0.9935675F;
			this.line54.Y2 = 1.190095F;
			// 
			// line55
			// 
			this.line55.Height = 0.1979168F;
			this.line55.Left = 7.344F;
			this.line55.LineWeight = 1F;
			this.line55.Name = "line55";
			this.line55.Tag = "";
			this.line55.Top = 0.596F;
			this.line55.Width = 0F;
			this.line55.X1 = 7.344F;
			this.line55.X2 = 7.344F;
			this.line55.Y1 = 0.596F;
			this.line55.Y2 = 0.7939168F;
			// 
			// line56
			// 
			this.line56.Height = 0.1979168F;
			this.line56.Left = 7.344F;
			this.line56.LineWeight = 1F;
			this.line56.Name = "line56";
			this.line56.Tag = "";
			this.line56.Top = 0.7942837F;
			this.line56.Width = 0F;
			this.line56.X1 = 7.344F;
			this.line56.X2 = 7.344F;
			this.line56.Y1 = 0.7942837F;
			this.line56.Y2 = 0.9922005F;
			// 
			// line57
			// 
			this.line57.Height = 0.1979162F;
			this.line57.Left = 7.344F;
			this.line57.LineWeight = 1F;
			this.line57.Name = "line57";
			this.line57.Tag = "";
			this.line57.Top = 0.9932838F;
			this.line57.Width = 0F;
			this.line57.X1 = 7.344F;
			this.line57.X2 = 7.344F;
			this.line57.Y1 = 0.9932838F;
			this.line57.Y2 = 1.1912F;
			// 
			// KBDR1021R
			// 
			this.MasterReport = false;
			this.PageSettings.DefaultPaperSize = false;
			this.PageSettings.Margins.Bottom = 0.5905512F;
			this.PageSettings.Margins.Left = 0.3937008F;
			this.PageSettings.Margins.Right = 0.3937008F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
			this.PageSettings.PaperHeight = 11.69291F;
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
			this.PageSettings.PaperWidth = 8.267716F;
			this.PrintWidth = 7.448F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.pageFooter);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.totalRowNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nowRouNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM03)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM05)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM06)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM08)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM09)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ITEM10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM02;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM10;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線30;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線31;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線40;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線42;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線44;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線45;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線46;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル10;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル14;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル15;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル16;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線34;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線35;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線37;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線38;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線39;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox nowRouNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox totalRowNo;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM05;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line45;
        private GrapeCity.ActiveReports.SectionReportModel.Line line46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line line49;
        private GrapeCity.ActiveReports.SectionReportModel.Line line50;
        private GrapeCity.ActiveReports.SectionReportModel.Line line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line line52;
        private GrapeCity.ActiveReports.SectionReportModel.Line line53;
        private GrapeCity.ActiveReports.SectionReportModel.Line line54;
        private GrapeCity.ActiveReports.SectionReportModel.Line line55;
        private GrapeCity.ActiveReports.SectionReportModel.Line line56;
        private GrapeCity.ActiveReports.SectionReportModel.Line line57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
    }
}
