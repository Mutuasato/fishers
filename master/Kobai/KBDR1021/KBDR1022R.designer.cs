﻿namespace jp.co.fsi.kb.kbdr1021
{
    /// <summary>
    /// KBDR1022R の概要の説明です。
    /// </summary>
    partial class KBDR1022R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KBDR1022R));
			this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtGrpPages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.直線68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.テキスト188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.テキスト307 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ラベル321 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル322 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル323 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル324 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル325 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル326 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル327 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル328 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル329 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ラベル330 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
			this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.judgeNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.テキスト42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.detailLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.直線91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.直線102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lastLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト253 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.テキスト308 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line77 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line78 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line79 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line80 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line82 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line84 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line87 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line88 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line81 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line83 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line85 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line86 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line148 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line149 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line150 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.textBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line101 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line103 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line104 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line105 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line106 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line108 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line109 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line110 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line111 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line112 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line113 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line114 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line115 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line116 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line117 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line118 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line119 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line120 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line121 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line123 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line107 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line122 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line124 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line125 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line126 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line127 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line128 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line129 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line130 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line131 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line132 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line133 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line134 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line135 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line136 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line137 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line138 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line139 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line140 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line141 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line142 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line143 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line144 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line145 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line146 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line147 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト188)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト307)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト316)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト317)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル321)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル322)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル323)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル324)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル325)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル326)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル327)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル328)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル329)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル330)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.judgeNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト253)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト308)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGrpPages,
            this.直線68,
            this.テキスト188,
            this.ラベル71,
            this.テキスト307,
            this.テキスト316,
            this.テキスト317,
            this.ラベル321,
            this.ラベル322,
            this.ラベル323,
            this.ラベル324,
            this.ラベル325,
            this.ラベル326,
            this.ラベル327,
            this.ラベル328,
            this.ラベル329,
            this.ラベル330,
            this.reportInfo1,
            this.label3,
            this.judgeNo,
            this.label4,
            this.label5,
            this.label1,
            this.textBox29});
			this.pageHeader.Height = 0.8165136F;
			this.pageHeader.Name = "pageHeader";
			// 
			// txtGrpPages
			// 
			this.txtGrpPages.Height = 0.1875F;
			this.txtGrpPages.Left = 10.59348F;
			this.txtGrpPages.Name = "txtGrpPages";
			this.txtGrpPages.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 1";
			this.txtGrpPages.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtGrpPages.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtGrpPages.Tag = "";
			this.txtGrpPages.Text = null;
			this.txtGrpPages.Top = 0.08333334F;
			this.txtGrpPages.Width = 0.375F;
			// 
			// 直線68
			// 
			this.直線68.Height = 0F;
			this.直線68.Left = 4.5132F;
			this.直線68.LineWeight = 1F;
			this.直線68.Name = "直線68";
			this.直線68.Tag = "";
			this.直線68.Top = 0.3125F;
			this.直線68.Width = 2.473611F;
			this.直線68.X1 = 4.5132F;
			this.直線68.X2 = 6.986811F;
			this.直線68.Y1 = 0.3125F;
			this.直線68.Y2 = 0.3125F;
			// 
			// テキスト188
			// 
			this.テキスト188.DataField = "ITEM03";
			this.テキスト188.Height = 0.1770833F;
			this.テキスト188.Left = 4.027088F;
			this.テキスト188.Name = "テキスト188";
			this.テキスト188.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; vertical-align: middle; ddo-char-set: 1";
			this.テキスト188.Tag = "";
			this.テキスト188.Text = "ITEM03";
			this.テキスト188.Top = 0.3541667F;
			this.テキスト188.Width = 3.445833F;
			// 
			// ラベル71
			// 
			this.ラベル71.Height = 0.1925278F;
			this.ラベル71.HyperLink = null;
			this.ラベル71.Left = 10.968F;
			this.ラベル71.Name = "ラベル71";
			this.ラベル71.Style = "color: #4C535C; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; vertical-align: middle; ddo-char-set: 1";
			this.ラベル71.Tag = "";
			this.ラベル71.Text = "頁";
			this.ラベル71.Top = 0.079F;
			this.ラベル71.Width = 0.2027778F;
			// 
			// テキスト307
			// 
			this.テキスト307.DataField = "ITEM19";
			this.テキスト307.Height = 0.28125F;
			this.テキスト307.Left = 4.614589F;
			this.テキスト307.Name = "テキスト307";
			this.テキスト307.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 128";
			this.テキスト307.Tag = "";
			this.テキスト307.Text = "Title";
			this.テキスト307.Top = 0F;
			this.テキスト307.Width = 2.270833F;
			// 
			// テキスト316
			// 
			this.テキスト316.CanGrow = false;
			this.テキスト316.DataField = "ITEM01";
			this.テキスト316.Height = 0.2006944F;
			this.テキスト316.Left = 0.02086614F;
			this.テキスト316.Name = "テキスト316";
			this.テキスト316.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; vertical-align: middle; white-space: nowrap; ddo-char-set: 128; ddo-wrap" +
    "-mode: nowrap";
			this.テキスト316.Tag = "";
			this.テキスト316.Text = "ITEM01";
			this.テキスト316.Top = 0.0452756F;
			this.テキスト316.Width = 2.262599F;
			// 
			// テキスト317
			// 
			this.テキスト317.DataField = "ITEM02";
			this.テキスト317.Height = 0.2006944F;
			this.テキスト317.Left = 0F;
			this.テキスト317.Name = "テキスト317";
			this.テキスト317.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; vertical-align: middle; ddo-char-set: 128";
			this.テキスト317.Tag = "";
			this.テキスト317.Text = "ITEM02";
			this.テキスト317.Top = 0.3854331F;
			this.テキスト317.Width = 1.533465F;
			// 
			// ラベル321
			// 
			this.ラベル321.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル321.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル321.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル321.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル321.Height = 0.1990709F;
			this.ラベル321.HyperLink = null;
			this.ラベル321.Left = 0F;
			this.ラベル321.Name = "ラベル321";
			this.ラベル321.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル321.Tag = "";
			this.ラベル321.Text = "伝票日付";
			this.ラベル321.Top = 0.617F;
			this.ラベル321.Width = 0.5799213F;
			// 
			// ラベル322
			// 
			this.ラベル322.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル322.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル322.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル322.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル322.Height = 0.1990708F;
			this.ラベル322.HyperLink = null;
			this.ラベル322.Left = 0.5799213F;
			this.ラベル322.Name = "ラベル322";
			this.ラベル322.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル322.Tag = "";
			this.ラベル322.Text = "伝票№";
			this.ラベル322.Top = 0.6169292F;
			this.ラベル322.Width = 0.438189F;
			// 
			// ラベル323
			// 
			this.ラベル323.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル323.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル323.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル323.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル323.Height = 0.1972222F;
			this.ラベル323.HyperLink = null;
			this.ラベル323.Left = 2.205F;
			this.ラベル323.Name = "ラベル323";
			this.ラベル323.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル323.Tag = "";
			this.ラベル323.Text = "取引区分";
			this.ラベル323.Top = 0.617F;
			this.ラベル323.Width = 0.552F;
			// 
			// ラベル324
			// 
			this.ラベル324.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル324.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル324.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル324.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル324.Height = 0.1972222F;
			this.ラベル324.HyperLink = null;
			this.ラベル324.Left = 2.757F;
			this.ラベル324.Name = "ラベル324";
			this.ラベル324.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル324.Tag = "";
			this.ラベル324.Text = "商　品　名　・　規　格";
			this.ラベル324.Top = 0.617F;
			this.ラベル324.Width = 2.22F;
			// 
			// ラベル325
			// 
			this.ラベル325.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル325.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル325.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル325.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル325.Height = 0.1972222F;
			this.ラベル325.HyperLink = null;
			this.ラベル325.Left = 6.138F;
			this.ラベル325.Name = "ラベル325";
			this.ラベル325.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル325.Tag = "";
			this.ラベル325.Text = "数量(ﾊﾞﾗ)";
			this.ラベル325.Top = 0.619F;
			this.ラベル325.Width = 0.8119993F;
			// 
			// ラベル326
			// 
			this.ラベル326.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル326.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル326.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル326.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル326.Height = 0.197F;
			this.ラベル326.HyperLink = null;
			this.ラベル326.Left = 9.757001F;
			this.ラベル326.Name = "ラベル326";
			this.ラベル326.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル326.Tag = "";
			this.ラベル326.Text = "合計";
			this.ラベル326.Top = 0.617F;
			this.ラベル326.Width = 0.9F;
			// 
			// ラベル327
			// 
			this.ラベル327.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル327.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル327.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル327.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル327.Height = 0.1972222F;
			this.ラベル327.HyperLink = null;
			this.ラベル327.Left = 10.657F;
			this.ラベル327.Name = "ラベル327";
			this.ラベル327.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル327.Tag = "";
			this.ラベル327.Text = "担当者";
			this.ラベル327.Top = 0.6169292F;
			this.ラベル327.Width = 0.6660004F;
			// 
			// ラベル328
			// 
			this.ラベル328.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル328.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル328.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル328.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル328.Height = 0.1972222F;
			this.ラベル328.HyperLink = null;
			this.ラベル328.Left = 4.977F;
			this.ラベル328.Name = "ラベル328";
			this.ラベル328.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル328.Tag = "";
			this.ラベル328.Text = "入数";
			this.ラベル328.Top = 0.619F;
			this.ラベル328.Width = 0.5021653F;
			// 
			// ラベル329
			// 
			this.ラベル329.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル329.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル329.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル329.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル329.Height = 0.1972222F;
			this.ラベル329.HyperLink = null;
			this.ラベル329.Left = 7.633F;
			this.ラベル329.Name = "ラベル329";
			this.ラベル329.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル329.Tag = "";
			this.ラベル329.Text = "金　額";
			this.ラベル329.Top = 0.619F;
			this.ラベル329.Width = 0.8500004F;
			// 
			// ラベル330
			// 
			this.ラベル330.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル330.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル330.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル330.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.ラベル330.Height = 0.1972222F;
			this.ラベル330.HyperLink = null;
			this.ラベル330.Left = 6.95F;
			this.ラベル330.Name = "ラベル330";
			this.ラベル330.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.ラベル330.Tag = "";
			this.ラベル330.Text = "単　価";
			this.ラベル330.Top = 0.619F;
			this.ラベル330.Width = 0.6830001F;
			// 
			// reportInfo1
			// 
			this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
			this.reportInfo1.Height = 0.1924869F;
			this.reportInfo1.Left = 9.207874F;
			this.reportInfo1.Name = "reportInfo1";
			this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 1";
			this.reportInfo1.Top = 0.07834646F;
			this.reportInfo1.Width = 1.385466F;
			// 
			// label3
			// 
			this.label3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label3.Height = 0.197F;
			this.label3.HyperLink = null;
			this.label3.Left = 8.957001F;
			this.label3.Name = "label3";
			this.label3.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.label3.Tag = "";
			this.label3.Text = "消費税";
			this.label3.Top = 0.617F;
			this.label3.Width = 0.8F;
			// 
			// judgeNo
			// 
			this.judgeNo.DataField = " ITEM19";
			this.judgeNo.Height = 0.2007244F;
			this.judgeNo.Left = 2.303937F;
			this.judgeNo.Name = "judgeNo";
			this.judgeNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
			this.judgeNo.Tag = "";
			this.judgeNo.Text = " ITEM19";
			this.judgeNo.Top = 0.0452756F;
			this.judgeNo.Visible = false;
			this.judgeNo.Width = 0.7141732F;
			// 
			// label4
			// 
			this.label4.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label4.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label4.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label4.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label4.Height = 0.1972222F;
			this.label4.HyperLink = null;
			this.label4.Left = 5.479F;
			this.label4.Name = "label4";
			this.label4.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.label4.Tag = "";
			this.label4.Text = "数量(ｹｰｽ)";
			this.label4.Top = 0.619F;
			this.label4.Width = 0.6590002F;
			// 
			// label5
			// 
			this.label5.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label5.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label5.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label5.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label5.Height = 0.1972222F;
			this.label5.HyperLink = null;
			this.label5.Left = 1.01811F;
			this.label5.Name = "label5";
			this.label5.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.label5.Tag = "";
			this.label5.Text = "船　　　主";
			this.label5.Top = 0.6169292F;
			this.label5.Width = 1.18689F;
			// 
			// label1
			// 
			this.label1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
			this.label1.Height = 0.197F;
			this.label1.HyperLink = null;
			this.label1.Left = 8.483001F;
			this.label1.Name = "label1";
			this.label1.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
			this.label1.Tag = "";
			this.label1.Text = "税率";
			this.label1.Top = 0.617F;
			this.label1.Width = 0.48F;
			// 
			// textBox29
			// 
			this.textBox29.Height = 0.201F;
			this.textBox29.Left = 8.515F;
			this.textBox29.Name = "textBox29";
			this.textBox29.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; vertical-align: middle; ddo-char-set: 1";
			this.textBox29.Tag = "";
			this.textBox29.Text = "「*」は軽減税率であることを示します";
			this.textBox29.Top = 0.385F;
			this.textBox29.Width = 2.484F;
			// 
			// detail
			// 
			this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト42,
            this.textBox1,
            this.テキスト40,
            this.textBox15,
            this.detailLine,
            this.textBox36,
            this.textBox35,
            this.textBox34,
            this.textBox33,
            this.textBox32,
            this.textBox31,
            this.textBox30,
            this.textBox22,
            this.textBox18,
            this.テキスト38,
            this.テキスト72,
            this.テキスト39,
            this.textBox2,
            this.テキスト36,
            this.テキスト37,
            this.直線91,
            this.直線92,
            this.直線93,
            this.直線94,
            this.直線95,
            this.直線96,
            this.直線97,
            this.直線98,
            this.直線99,
            this.直線100,
            this.直線102,
            this.lastLine,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line55});
			this.detail.Height = 0.188F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// テキスト42
			// 
			this.テキスト42.DataField = " ITEM17";
			this.テキスト42.Height = 0.1787402F;
			this.テキスト42.Left = 9.757001F;
			this.テキスト42.Name = "テキスト42";
			this.テキスト42.OutputFormat = resources.GetString("テキスト42.OutputFormat");
			this.テキスト42.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.テキスト42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.テキスト42.Tag = "";
			this.テキスト42.Text = " ITEM17";
			this.テキスト42.Top = 0F;
			this.テキスト42.Width = 0.8999996F;
			// 
			// textBox1
			// 
			this.textBox1.DataField = " ITEM16";
			this.textBox1.Height = 0.1784722F;
			this.textBox1.Left = 8.963F;
			this.textBox1.Name = "textBox1";
			this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
			this.textBox1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox1.Tag = "";
			this.textBox1.Text = " ITEM16";
			this.textBox1.Top = 0F;
			this.textBox1.Width = 0.7940006F;
			// 
			// テキスト40
			// 
			this.テキスト40.DataField = "ITEM15";
			this.テキスト40.Height = 0.1784722F;
			this.テキスト40.Left = 7.633F;
			this.テキスト40.Name = "テキスト40";
			this.テキスト40.OutputFormat = resources.GetString("テキスト40.OutputFormat");
			this.テキスト40.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.テキスト40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.テキスト40.Tag = "";
			this.テキスト40.Text = "ITEM15";
			this.テキスト40.Top = 0F;
			this.テキスト40.Width = 0.8500004F;
			// 
			// textBox15
			// 
			this.textBox15.DataField = "ITEM20";
			this.textBox15.Height = 0.1784722F;
			this.textBox15.Left = 8.483001F;
			this.textBox15.Name = "textBox15";
			this.textBox15.OutputFormat = resources.GetString("textBox15.OutputFormat");
			this.textBox15.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox15.Tag = "";
			this.textBox15.Text = "ITEM20";
			this.textBox15.Top = 0F;
			this.textBox15.Width = 0.474F;
			// 
			// detailLine
			// 
			this.detailLine.Height = 9.840727E-05F;
			this.detailLine.Left = 0F;
			this.detailLine.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
			this.detailLine.LineWeight = 1F;
			this.detailLine.Name = "detailLine";
			this.detailLine.Tag = "";
			this.detailLine.Top = 0.1877953F;
			this.detailLine.Width = 11.323F;
			this.detailLine.X1 = 0F;
			this.detailLine.X2 = 11.323F;
			this.detailLine.Y1 = 0.1878937F;
			this.detailLine.Y2 = 0.1877953F;
			// 
			// textBox36
			// 
			this.textBox36.DataField = "ITEM29";
			this.textBox36.Height = 0.178F;
			this.textBox36.Left = 9.861F;
			this.textBox36.Name = "textBox36";
			this.textBox36.OutputFormat = resources.GetString("textBox36.OutputFormat");
			this.textBox36.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox36.Tag = "";
			this.textBox36.Text = "ITEM29";
			this.textBox36.Top = 0.0009999871F;
			this.textBox36.Visible = false;
			this.textBox36.Width = 0.5F;
			// 
			// textBox35
			// 
			this.textBox35.DataField = "ITEM28";
			this.textBox35.Height = 0.178F;
			this.textBox35.Left = 9.051001F;
			this.textBox35.Name = "textBox35";
			this.textBox35.OutputFormat = resources.GetString("textBox35.OutputFormat");
			this.textBox35.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox35.Tag = "";
			this.textBox35.Text = "ITEM28";
			this.textBox35.Top = 0F;
			this.textBox35.Visible = false;
			this.textBox35.Width = 0.5F;
			// 
			// textBox34
			// 
			this.textBox34.DataField = "ITEM27";
			this.textBox34.Height = 0.178F;
			this.textBox34.Left = 7.727F;
			this.textBox34.Name = "textBox34";
			this.textBox34.OutputFormat = resources.GetString("textBox34.OutputFormat");
			this.textBox34.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox34.Tag = "";
			this.textBox34.Text = "ITEM27";
			this.textBox34.Top = 0.0009999871F;
			this.textBox34.Visible = false;
			this.textBox34.Width = 0.5F;
			// 
			// textBox33
			// 
			this.textBox33.DataField = "ITEM26";
			this.textBox33.Height = 0.178F;
			this.textBox33.Left = 9.84F;
			this.textBox33.Name = "textBox33";
			this.textBox33.OutputFormat = resources.GetString("textBox33.OutputFormat");
			this.textBox33.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox33.Tag = "";
			this.textBox33.Text = "ITEM26";
			this.textBox33.Top = 0.0009999871F;
			this.textBox33.Visible = false;
			this.textBox33.Width = 0.5F;
			// 
			// textBox32
			// 
			this.textBox32.DataField = "ITEM25";
			this.textBox32.Height = 0.178F;
			this.textBox32.Left = 9.030001F;
			this.textBox32.Name = "textBox32";
			this.textBox32.OutputFormat = resources.GetString("textBox32.OutputFormat");
			this.textBox32.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox32.Tag = "";
			this.textBox32.Text = "ITEM25";
			this.textBox32.Top = 0.0009999871F;
			this.textBox32.Visible = false;
			this.textBox32.Width = 0.5F;
			// 
			// textBox31
			// 
			this.textBox31.DataField = "ITEM24";
			this.textBox31.Height = 0.178F;
			this.textBox31.Left = 7.706F;
			this.textBox31.Name = "textBox31";
			this.textBox31.OutputFormat = resources.GetString("textBox31.OutputFormat");
			this.textBox31.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox31.Tag = "";
			this.textBox31.Text = "ITEM24";
			this.textBox31.Top = 0.0009999871F;
			this.textBox31.Visible = false;
			this.textBox31.Width = 0.5F;
			// 
			// textBox30
			// 
			this.textBox30.DataField = "ITEM23";
			this.textBox30.Height = 0.178F;
			this.textBox30.Left = 9.820001F;
			this.textBox30.Name = "textBox30";
			this.textBox30.OutputFormat = resources.GetString("textBox30.OutputFormat");
			this.textBox30.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox30.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox30.Tag = "";
			this.textBox30.Text = "ITEM23";
			this.textBox30.Top = 0F;
			this.textBox30.Visible = false;
			this.textBox30.Width = 0.5F;
			// 
			// textBox22
			// 
			this.textBox22.DataField = "ITEM22";
			this.textBox22.Height = 0.178F;
			this.textBox22.Left = 9.009001F;
			this.textBox22.Name = "textBox22";
			this.textBox22.OutputFormat = resources.GetString("textBox22.OutputFormat");
			this.textBox22.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox22.Tag = "";
			this.textBox22.Text = "ITEM22";
			this.textBox22.Top = 0F;
			this.textBox22.Visible = false;
			this.textBox22.Width = 0.5F;
			// 
			// textBox18
			// 
			this.textBox18.DataField = "ITEM21";
			this.textBox18.Height = 0.178F;
			this.textBox18.Left = 7.685F;
			this.textBox18.Name = "textBox18";
			this.textBox18.OutputFormat = resources.GetString("textBox18.OutputFormat");
			this.textBox18.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox18.Tag = "";
			this.textBox18.Text = "ITEM21";
			this.textBox18.Top = 0F;
			this.textBox18.Visible = false;
			this.textBox18.Width = 0.5F;
			// 
			// テキスト38
			// 
			this.テキスト38.DataField = "ITEM13";
			this.テキスト38.Height = 0.1784722F;
			this.テキスト38.Left = 6.138F;
			this.テキスト38.Name = "テキスト38";
			this.テキスト38.OutputFormat = resources.GetString("テキスト38.OutputFormat");
			this.テキスト38.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.テキスト38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.テキスト38.Tag = "";
			this.テキスト38.Text = "ITEM13";
			this.テキスト38.Top = 0F;
			this.テキスト38.Width = 0.8119997F;
			// 
			// テキスト72
			// 
			this.テキスト72.DataField = "ITEM10";
			this.テキスト72.Height = 0.1784722F;
			this.テキスト72.Left = 3.639F;
			this.テキスト72.MultiLine = false;
			this.テキスト72.Name = "テキスト72";
			this.テキスト72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; vertical-align: middle; ddo-char-set: 128";
			this.テキスト72.Tag = "";
			this.テキスト72.Text = "ITEM10";
			this.テキスト72.Top = 0F;
			this.テキスト72.Width = 1.338F;
			// 
			// テキスト39
			// 
			this.テキスト39.DataField = "ITEM14";
			this.テキスト39.Height = 0.1784722F;
			this.テキスト39.Left = 6.95F;
			this.テキスト39.Name = "テキスト39";
			this.テキスト39.OutputFormat = resources.GetString("テキスト39.OutputFormat");
			this.テキスト39.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.テキスト39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.テキスト39.Tag = "";
			this.テキスト39.Text = "ITEM14";
			this.テキスト39.Top = 0F;
			this.テキスト39.Width = 0.683F;
			// 
			// textBox2
			// 
			this.textBox2.DataField = "ITEM12";
			this.textBox2.Height = 0.1784722F;
			this.textBox2.Left = 5.479F;
			this.textBox2.Name = "textBox2";
			this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
			this.textBox2.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox2.Tag = "";
			this.textBox2.Text = "ITEM12";
			this.textBox2.Top = 0F;
			this.textBox2.Width = 0.6590003F;
			// 
			// テキスト36
			// 
			this.テキスト36.DataField = "ITEM09";
			this.テキスト36.Height = 0.1784722F;
			this.テキスト36.Left = 2.757F;
			this.テキスト36.Name = "テキスト36";
			this.テキスト36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.テキスト36.Tag = "";
			this.テキスト36.Text = "ITEM09";
			this.テキスト36.Top = 0F;
			this.テキスト36.Width = 0.8818898F;
			// 
			// テキスト37
			// 
			this.テキスト37.DataField = "ITEM11";
			this.テキスト37.Height = 0.1784722F;
			this.テキスト37.Left = 4.977F;
			this.テキスト37.Name = "テキスト37";
			this.テキスト37.OutputFormat = resources.GetString("テキスト37.OutputFormat");
			this.テキスト37.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.テキスト37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.テキスト37.Tag = "";
			this.テキスト37.Text = "ITEM11";
			this.テキスト37.Top = 0F;
			this.テキスト37.Width = 0.5019999F;
			// 
			// 直線91
			// 
			this.直線91.Height = 0.1877953F;
			this.直線91.Left = 0.5799213F;
			this.直線91.LineWeight = 1F;
			this.直線91.Name = "直線91";
			this.直線91.Tag = "";
			this.直線91.Top = 0F;
			this.直線91.Width = 0F;
			this.直線91.X1 = 0.5799213F;
			this.直線91.X2 = 0.5799213F;
			this.直線91.Y1 = 0F;
			this.直線91.Y2 = 0.1877953F;
			// 
			// 直線92
			// 
			this.直線92.Height = 0.1877953F;
			this.直線92.Left = 0.0003937008F;
			this.直線92.LineWeight = 1F;
			this.直線92.Name = "直線92";
			this.直線92.Tag = "";
			this.直線92.Top = 0F;
			this.直線92.Width = 0F;
			this.直線92.X1 = 0.0003937008F;
			this.直線92.X2 = 0.0003937008F;
			this.直線92.Y1 = 0F;
			this.直線92.Y2 = 0.1877953F;
			// 
			// 直線93
			// 
			this.直線93.Height = 0.1877953F;
			this.直線93.Left = 2.757F;
			this.直線93.LineWeight = 1F;
			this.直線93.Name = "直線93";
			this.直線93.Tag = "";
			this.直線93.Top = 0F;
			this.直線93.Width = 0F;
			this.直線93.X1 = 2.757F;
			this.直線93.X2 = 2.757F;
			this.直線93.Y1 = 0F;
			this.直線93.Y2 = 0.1877953F;
			// 
			// 直線94
			// 
			this.直線94.Height = 0.1877953F;
			this.直線94.Left = 4.977F;
			this.直線94.LineWeight = 1F;
			this.直線94.Name = "直線94";
			this.直線94.Tag = "";
			this.直線94.Top = 0F;
			this.直線94.Width = 0F;
			this.直線94.X1 = 4.977F;
			this.直線94.X2 = 4.977F;
			this.直線94.Y1 = 0F;
			this.直線94.Y2 = 0.1877953F;
			// 
			// 直線95
			// 
			this.直線95.Height = 0.1877953F;
			this.直線95.Left = 6.138001F;
			this.直線95.LineWeight = 1F;
			this.直線95.Name = "直線95";
			this.直線95.Tag = "";
			this.直線95.Top = 0F;
			this.直線95.Width = 0F;
			this.直線95.X1 = 6.138001F;
			this.直線95.X2 = 6.138001F;
			this.直線95.Y1 = 0F;
			this.直線95.Y2 = 0.1877953F;
			// 
			// 直線96
			// 
			this.直線96.Height = 0.1877953F;
			this.直線96.Left = 6.95F;
			this.直線96.LineWeight = 1F;
			this.直線96.Name = "直線96";
			this.直線96.Tag = "";
			this.直線96.Top = 0F;
			this.直線96.Width = 0F;
			this.直線96.X1 = 6.95F;
			this.直線96.X2 = 6.95F;
			this.直線96.Y1 = 0F;
			this.直線96.Y2 = 0.1877953F;
			// 
			// 直線97
			// 
			this.直線97.Height = 0.1877953F;
			this.直線97.Left = 11.323F;
			this.直線97.LineWeight = 1F;
			this.直線97.Name = "直線97";
			this.直線97.Tag = "";
			this.直線97.Top = 0F;
			this.直線97.Width = 0F;
			this.直線97.X1 = 11.323F;
			this.直線97.X2 = 11.323F;
			this.直線97.Y1 = 0F;
			this.直線97.Y2 = 0.1877953F;
			// 
			// 直線98
			// 
			this.直線98.Height = 0.1877953F;
			this.直線98.Left = 10.657F;
			this.直線98.LineWeight = 1F;
			this.直線98.Name = "直線98";
			this.直線98.Tag = "";
			this.直線98.Top = 0F;
			this.直線98.Width = 0F;
			this.直線98.X1 = 10.657F;
			this.直線98.X2 = 10.657F;
			this.直線98.Y1 = 0F;
			this.直線98.Y2 = 0.1877953F;
			// 
			// 直線99
			// 
			this.直線99.Height = 0.1877953F;
			this.直線99.Left = 9.757001F;
			this.直線99.LineWeight = 1F;
			this.直線99.Name = "直線99";
			this.直線99.Tag = "";
			this.直線99.Top = 0F;
			this.直線99.Width = 0F;
			this.直線99.X1 = 9.757001F;
			this.直線99.X2 = 9.757001F;
			this.直線99.Y1 = 0F;
			this.直線99.Y2 = 0.1877953F;
			// 
			// 直線100
			// 
			this.直線100.Height = 0.1877953F;
			this.直線100.Left = 7.633F;
			this.直線100.LineWeight = 1F;
			this.直線100.Name = "直線100";
			this.直線100.Tag = "";
			this.直線100.Top = 0F;
			this.直線100.Width = 0F;
			this.直線100.X1 = 7.633F;
			this.直線100.X2 = 7.633F;
			this.直線100.Y1 = 0F;
			this.直線100.Y2 = 0.1877953F;
			// 
			// 直線102
			// 
			this.直線102.Height = 0.1877953F;
			this.直線102.Left = 0F;
			this.直線102.LineWeight = 1F;
			this.直線102.Name = "直線102";
			this.直線102.Tag = "";
			this.直線102.Top = 0F;
			this.直線102.Width = 0F;
			this.直線102.X1 = 0F;
			this.直線102.X2 = 0F;
			this.直線102.Y1 = 0F;
			this.直線102.Y2 = 0.1877953F;
			// 
			// lastLine
			// 
			this.lastLine.Height = 0F;
			this.lastLine.Left = 0.004F;
			this.lastLine.LineWeight = 1F;
			this.lastLine.Name = "lastLine";
			this.lastLine.Tag = "";
			this.lastLine.Top = 0.188F;
			this.lastLine.Width = 11.31946F;
			this.lastLine.X1 = 0.004F;
			this.lastLine.X2 = 11.32346F;
			this.lastLine.Y1 = 0.188F;
			this.lastLine.Y2 = 0.188F;
			// 
			// line1
			// 
			this.line1.Height = 0.1877953F;
			this.line1.Left = 8.963F;
			this.line1.LineWeight = 1F;
			this.line1.Name = "line1";
			this.line1.Tag = "";
			this.line1.Top = 0F;
			this.line1.Width = 0F;
			this.line1.X1 = 8.963F;
			this.line1.X2 = 8.963F;
			this.line1.Y1 = 0F;
			this.line1.Y2 = 0.1877953F;
			// 
			// line2
			// 
			this.line2.Height = 0.1877953F;
			this.line2.Left = 5.479F;
			this.line2.LineWeight = 1F;
			this.line2.Name = "line2";
			this.line2.Tag = "";
			this.line2.Top = 0F;
			this.line2.Width = 0F;
			this.line2.X1 = 5.479F;
			this.line2.X2 = 5.479F;
			this.line2.Y1 = 0F;
			this.line2.Y2 = 0.1877953F;
			// 
			// line3
			// 
			this.line3.Height = 0.1877953F;
			this.line3.Left = 2.205F;
			this.line3.LineWeight = 1F;
			this.line3.Name = "line3";
			this.line3.Tag = "";
			this.line3.Top = 0F;
			this.line3.Width = 0F;
			this.line3.X1 = 2.205F;
			this.line3.X2 = 2.205F;
			this.line3.Y1 = 0F;
			this.line3.Y2 = 0.1877953F;
			// 
			// line4
			// 
			this.line4.Height = 0.1877953F;
			this.line4.Left = 1.01811F;
			this.line4.LineWeight = 1F;
			this.line4.Name = "line4";
			this.line4.Tag = "";
			this.line4.Top = 0F;
			this.line4.Width = 0F;
			this.line4.X1 = 1.01811F;
			this.line4.X2 = 1.01811F;
			this.line4.Y1 = 0F;
			this.line4.Y2 = 0.1877953F;
			// 
			// line55
			// 
			this.line55.Height = 0.1877953F;
			this.line55.Left = 8.483001F;
			this.line55.LineWeight = 1F;
			this.line55.Name = "line55";
			this.line55.Tag = "";
			this.line55.Top = 0F;
			this.line55.Width = 0F;
			this.line55.X1 = 8.483001F;
			this.line55.X2 = 8.483001F;
			this.line55.Y1 = 0F;
			this.line55.Y2 = 0.1877953F;
			// 
			// textBox3
			// 
			this.textBox3.Height = 0.178F;
			this.textBox3.Left = 2.757F;
			this.textBox3.Name = "textBox3";
			this.textBox3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; vertical-align: middle; ddo-char-set: 128";
			this.textBox3.Tag = "";
			this.textBox3.Text = "【消費税内訳】";
			this.textBox3.Top = 0F;
			this.textBox3.Width = 1.173622F;
			// 
			// テキスト32
			// 
			this.テキスト32.DataField = "ITEM04";
			this.テキスト32.Height = 0.1784722F;
			this.テキスト32.Left = 0F;
			this.テキスト32.Name = "テキスト32";
			this.テキスト32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
			this.テキスト32.Tag = "";
			this.テキスト32.Text = "ITEM04";
			this.テキスト32.Top = 0F;
			this.テキスト32.Width = 0.58F;
			// 
			// テキスト33
			// 
			this.テキスト33.DataField = "ITEM05";
			this.テキスト33.Height = 0.1784722F;
			this.テキスト33.Left = 0.58F;
			this.テキスト33.Name = "テキスト33";
			this.テキスト33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
			this.テキスト33.Tag = "";
			this.テキスト33.Text = "ITEM05";
			this.テキスト33.Top = 9.313226E-10F;
			this.テキスト33.Width = 0.438F;
			// 
			// テキスト35
			// 
			this.テキスト35.DataField = "ITEM08";
			this.テキスト35.Height = 0.1784722F;
			this.テキスト35.Left = 2.205F;
			this.テキスト35.MultiLine = false;
			this.テキスト35.Name = "テキスト35";
			this.テキスト35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
			this.テキスト35.Tag = "";
			this.テキスト35.Text = "ITEM08";
			this.テキスト35.Top = 0F;
			this.テキスト35.Width = 0.5520001F;
			// 
			// テキスト43
			// 
			this.テキスト43.CanGrow = false;
			this.テキスト43.DataField = " ITEM18";
			this.テキスト43.Height = 0.1784722F;
			this.テキスト43.Left = 10.688F;
			this.テキスト43.Name = "テキスト43";
			this.テキスト43.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 1, 0);
			this.テキスト43.ShrinkToFit = true;
			this.テキスト43.Style = resources.GetString("テキスト43.Style");
			this.テキスト43.Tag = "";
			this.テキスト43.Text = " ITEM18";
			this.テキスト43.Top = 0F;
			this.テキスト43.Width = 0.6149998F;
			// 
			// テキスト253
			// 
			this.テキスト253.CanGrow = false;
			this.テキスト253.DataField = "ITEM06";
			this.テキスト253.Height = 0.1784722F;
			this.テキスト253.Left = 1.018F;
			this.テキスト253.MultiLine = false;
			this.テキスト253.Name = "テキスト253";
			this.テキスト253.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
			this.テキスト253.Tag = "";
			this.テキスト253.Text = "ITEM06";
			this.テキスト253.Top = 0F;
			this.テキスト253.Width = 0.3161418F;
			// 
			// テキスト308
			// 
			this.テキスト308.DataField = "ITEM07";
			this.テキスト308.Height = 0.1784722F;
			this.テキスト308.Left = 1.334F;
			this.テキスト308.MultiLine = false;
			this.テキスト308.Name = "テキスト308";
			this.テキスト308.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 128";
			this.テキスト308.Tag = "";
			this.テキスト308.Text = "ITEM07";
			this.テキスト308.Top = 0F;
			this.テキスト308.Width = 0.871F;
			// 
			// pageFooter
			// 
			this.pageFooter.Height = 0F;
			this.pageFooter.Name = "pageFooter";
			this.pageFooter.Format += new System.EventHandler(this.pageFooter_Format);
			// 
			// reportHeader1
			// 
			this.reportHeader1.Height = 0F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// reportFooter1
			// 
			this.reportFooter1.CanShrink = true;
			this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox47,
            this.textBox11,
            this.textBox54,
            this.textBox12,
            this.textBox46,
            this.textBox9,
            this.textBox55,
            this.textBox13,
            this.textBox37,
            this.textBox27,
            this.textBox7,
            this.textBox24,
            this.textBox4,
            this.textBox6,
            this.textBox26,
            this.textBox25,
            this.textBox23,
            this.textBox8,
            this.textBox28,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.line28,
            this.line29,
            this.line30,
            this.line31,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line37,
            this.line38,
            this.line58,
            this.line70,
            this.line71,
            this.line72,
            this.line73,
            this.line74,
            this.line75,
            this.line76,
            this.line77,
            this.line78,
            this.line79,
            this.line80,
            this.line82,
            this.line84,
            this.line87,
            this.line88,
            this.line91,
            this.line41,
            this.line42,
            this.line43,
            this.line44,
            this.line45,
            this.line46,
            this.line47,
            this.line48,
            this.line49,
            this.line50,
            this.line51,
            this.line52,
            this.line54,
            this.line57,
            this.line59,
            this.line60,
            this.line61,
            this.line62,
            this.line63,
            this.line64,
            this.line65,
            this.line66,
            this.line67,
            this.line68,
            this.line69,
            this.line81,
            this.line83,
            this.line85,
            this.line86,
            this.line148,
            this.line149,
            this.line150});
			this.reportFooter1.Height = 0.7978675F;
			this.reportFooter1.Name = "reportFooter1";
			this.reportFooter1.AfterPrint += new System.EventHandler(this.reportFooter1_AfterPrint);
			// 
			// textBox47
			// 
			this.textBox47.DataField = "ITEM45";
			this.textBox47.Height = 0.178F;
			this.textBox47.Left = 9.757001F;
			this.textBox47.Name = "textBox47";
			this.textBox47.OutputFormat = resources.GetString("textBox47.OutputFormat");
			this.textBox47.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; text-justify: auto; vertical-align: middle; ddo-char-set: 128";
			this.textBox47.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.textBox47.Tag = "";
			this.textBox47.Text = "ITEM45";
			this.textBox47.Top = 0.376F;
			this.textBox47.Width = 0.899F;
			// 
			// textBox11
			// 
			this.textBox11.DataField = "ITEM41";
			this.textBox11.Height = 0.178F;
			this.textBox11.Left = 9.757001F;
			this.textBox11.Name = "textBox11";
			this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
			this.textBox11.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.textBox11.Tag = "";
			this.textBox11.Text = "ITEM41";
			this.textBox11.Top = 0.187F;
			this.textBox11.Width = 0.899F;
			// 
			// textBox54
			// 
			this.textBox54.DataField = "ITEM44";
			this.textBox54.Height = 0.178F;
			this.textBox54.Left = 8.957001F;
			this.textBox54.Name = "textBox54";
			this.textBox54.OutputFormat = resources.GetString("textBox54.OutputFormat");
			this.textBox54.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox54.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.textBox54.Tag = "";
			this.textBox54.Text = "ITEM44";
			this.textBox54.Top = 0.376F;
			this.textBox54.Width = 0.8F;
			// 
			// textBox12
			// 
			this.textBox12.DataField = "ITEM40";
			this.textBox12.Height = 0.178F;
			this.textBox12.Left = 8.957001F;
			this.textBox12.Name = "textBox12";
			this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
			this.textBox12.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox12.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox12.Tag = "";
			this.textBox12.Text = "ITEM40";
			this.textBox12.Top = 0.187F;
			this.textBox12.Width = 0.8F;
			// 
			// textBox46
			// 
			this.textBox46.DataField = "ITEM43";
			this.textBox46.Height = 0.188F;
			this.textBox46.Left = 8.483001F;
			this.textBox46.Name = "textBox46";
			this.textBox46.OutputFormat = resources.GetString("textBox46.OutputFormat");
			this.textBox46.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox46.Tag = "";
			this.textBox46.Text = "ITEM43";
			this.textBox46.Top = 0.376F;
			this.textBox46.Width = 0.474F;
			// 
			// textBox9
			// 
			this.textBox9.DataField = "ITEM39";
			this.textBox9.Height = 0.203F;
			this.textBox9.Left = 8.483001F;
			this.textBox9.Name = "textBox9";
			this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
			this.textBox9.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox9.Tag = "";
			this.textBox9.Text = "ITEM39";
			this.textBox9.Top = 0.187F;
			this.textBox9.Width = 0.474F;
			// 
			// textBox55
			// 
			this.textBox55.DataField = "ITEM42";
			this.textBox55.Height = 0.178F;
			this.textBox55.Left = 7.633F;
			this.textBox55.Name = "textBox55";
			this.textBox55.OutputFormat = resources.GetString("textBox55.OutputFormat");
			this.textBox55.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox55.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.textBox55.Tag = "";
			this.textBox55.Text = "ITEM42";
			this.textBox55.Top = 0.376F;
			this.textBox55.Width = 0.8500004F;
			// 
			// textBox13
			// 
			this.textBox13.DataField = "ITEM38";
			this.textBox13.Height = 0.178F;
			this.textBox13.Left = 7.633F;
			this.textBox13.Name = "textBox13";
			this.textBox13.OutputFormat = resources.GetString("textBox13.OutputFormat");
			this.textBox13.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.textBox13.Tag = "";
			this.textBox13.Text = "ITEM38";
			this.textBox13.Top = 0.187F;
			this.textBox13.Width = 0.8500004F;
			// 
			// textBox37
			// 
			this.textBox37.DataField = "ITEM35";
			this.textBox37.Height = 0.178F;
			this.textBox37.Left = 8.483001F;
			this.textBox37.Name = "textBox37";
			this.textBox37.OutputFormat = resources.GetString("textBox37.OutputFormat");
			this.textBox37.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox37.Tag = "";
			this.textBox37.Text = "ITEM35";
			this.textBox37.Top = 0F;
			this.textBox37.Width = 0.474F;
			// 
			// textBox27
			// 
			this.textBox27.Height = 0.178F;
			this.textBox27.Left = 9.757001F;
			this.textBox27.Name = "textBox27";
			this.textBox27.OutputFormat = resources.GetString("textBox27.OutputFormat");
			this.textBox27.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox27.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox27.Tag = "";
			this.textBox27.Text = " ITEM17";
			this.textBox27.Top = 0.564F;
			this.textBox27.Width = 0.899F;
			// 
			// textBox7
			// 
			this.textBox7.DataField = "ITEM37";
			this.textBox7.Height = 0.178F;
			this.textBox7.Left = 9.757001F;
			this.textBox7.Name = "textBox7";
			this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
			this.textBox7.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.textBox7.Tag = "";
			this.textBox7.Text = "ITEM37";
			this.textBox7.Top = 0F;
			this.textBox7.Width = 0.899F;
			// 
			// textBox24
			// 
			this.textBox24.Height = 0.178F;
			this.textBox24.Left = 8.957001F;
			this.textBox24.Name = "textBox24";
			this.textBox24.OutputFormat = resources.GetString("textBox24.OutputFormat");
			this.textBox24.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox24.Tag = "";
			this.textBox24.Text = " ITEM16";
			this.textBox24.Top = 0.564F;
			this.textBox24.Width = 0.8F;
			// 
			// textBox4
			// 
			this.textBox4.DataField = "ITEM36";
			this.textBox4.Height = 0.178F;
			this.textBox4.Left = 8.957001F;
			this.textBox4.Name = "textBox4";
			this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
			this.textBox4.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.textBox4.Tag = "";
			this.textBox4.Text = "ITEM36";
			this.textBox4.Top = 0F;
			this.textBox4.Width = 0.8F;
			// 
			// textBox6
			// 
			this.textBox6.DataField = "ITEM34";
			this.textBox6.Height = 0.178F;
			this.textBox6.Left = 7.633F;
			this.textBox6.Name = "textBox6";
			this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
			this.textBox6.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.textBox6.Tag = "";
			this.textBox6.Text = "ITEM34";
			this.textBox6.Top = 0F;
			this.textBox6.Width = 0.85F;
			// 
			// textBox26
			// 
			this.textBox26.Height = 0.178F;
			this.textBox26.Left = 7.633F;
			this.textBox26.Name = "textBox26";
			this.textBox26.OutputFormat = resources.GetString("textBox26.OutputFormat");
			this.textBox26.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox26.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox26.Tag = "";
			this.textBox26.Text = "ITEM15";
			this.textBox26.Top = 0.564F;
			this.textBox26.Width = 0.8500004F;
			// 
			// textBox25
			// 
			this.textBox25.DataField = "ITEM13";
			this.textBox25.Height = 0.178F;
			this.textBox25.Left = 6.138F;
			this.textBox25.Name = "textBox25";
			this.textBox25.OutputFormat = resources.GetString("textBox25.OutputFormat");
			this.textBox25.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox25.Tag = "";
			this.textBox25.Text = "ITEM13";
			this.textBox25.Top = 0.564F;
			this.textBox25.Width = 0.8119997F;
			// 
			// textBox23
			// 
			this.textBox23.DataField = "ITEM12";
			this.textBox23.Height = 0.178F;
			this.textBox23.Left = 5.479F;
			this.textBox23.Name = "textBox23";
			this.textBox23.OutputFormat = resources.GetString("textBox23.OutputFormat");
			this.textBox23.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox23.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.textBox23.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
			this.textBox23.Tag = "";
			this.textBox23.Text = "ITEM12";
			this.textBox23.Top = 0.564F;
			this.textBox23.Width = 0.6590003F;
			// 
			// textBox8
			// 
			this.textBox8.Height = 0.188F;
			this.textBox8.Left = 2.757F;
			this.textBox8.MultiLine = false;
			this.textBox8.Name = "textBox8";
			this.textBox8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 128";
			this.textBox8.Tag = "";
			this.textBox8.Text = "【消費税内訳合計】";
			this.textBox8.Top = 0F;
			this.textBox8.Width = 2.22F;
			// 
			// textBox28
			// 
			this.textBox28.Height = 0.178F;
			this.textBox28.Left = 2.757F;
			this.textBox28.MultiLine = false;
			this.textBox28.Name = "textBox28";
			this.textBox28.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 128";
			this.textBox28.Tag = "";
			this.textBox28.Text = "【　合　　　計　】";
			this.textBox28.Top = 0.56F;
			this.textBox28.Width = 2.22F;
			// 
			// line23
			// 
			this.line23.Height = 0.1877953F;
			this.line23.Left = 0.5800402F;
			this.line23.LineWeight = 1F;
			this.line23.Name = "line23";
			this.line23.Tag = "";
			this.line23.Top = 0F;
			this.line23.Width = 0F;
			this.line23.X1 = 0.5800402F;
			this.line23.X2 = 0.5800402F;
			this.line23.Y1 = 0F;
			this.line23.Y2 = 0.1877953F;
			// 
			// line24
			// 
			this.line24.Height = 0.1877953F;
			this.line24.Left = 2.757F;
			this.line24.LineWeight = 1F;
			this.line24.Name = "line24";
			this.line24.Tag = "";
			this.line24.Top = 0F;
			this.line24.Width = 0F;
			this.line24.X1 = 2.757F;
			this.line24.X2 = 2.757F;
			this.line24.Y1 = 0F;
			this.line24.Y2 = 0.1877953F;
			// 
			// line25
			// 
			this.line25.Height = 0.1877953F;
			this.line25.Left = 4.977F;
			this.line25.LineWeight = 1F;
			this.line25.Name = "line25";
			this.line25.Tag = "";
			this.line25.Top = 0F;
			this.line25.Width = 0F;
			this.line25.X1 = 4.977F;
			this.line25.X2 = 4.977F;
			this.line25.Y1 = 0F;
			this.line25.Y2 = 0.1877953F;
			// 
			// line26
			// 
			this.line26.Height = 0.1877953F;
			this.line26.Left = 6.138F;
			this.line26.LineWeight = 1F;
			this.line26.Name = "line26";
			this.line26.Tag = "";
			this.line26.Top = 0F;
			this.line26.Width = 0F;
			this.line26.X1 = 6.138F;
			this.line26.X2 = 6.138F;
			this.line26.Y1 = 0F;
			this.line26.Y2 = 0.1877953F;
			// 
			// line27
			// 
			this.line27.Height = 0.1877953F;
			this.line27.Left = 6.95F;
			this.line27.LineWeight = 1F;
			this.line27.Name = "line27";
			this.line27.Tag = "";
			this.line27.Top = 0F;
			this.line27.Width = 0F;
			this.line27.X1 = 6.95F;
			this.line27.X2 = 6.95F;
			this.line27.Y1 = 0F;
			this.line27.Y2 = 0.1877953F;
			// 
			// line28
			// 
			this.line28.Height = 0.1877953F;
			this.line28.Left = 11.323F;
			this.line28.LineWeight = 1F;
			this.line28.Name = "line28";
			this.line28.Tag = "";
			this.line28.Top = 0F;
			this.line28.Width = 0F;
			this.line28.X1 = 11.323F;
			this.line28.X2 = 11.323F;
			this.line28.Y1 = 0F;
			this.line28.Y2 = 0.1877953F;
			// 
			// line29
			// 
			this.line29.Height = 0.1877953F;
			this.line29.Left = 10.65591F;
			this.line29.LineWeight = 1F;
			this.line29.Name = "line29";
			this.line29.Tag = "";
			this.line29.Top = 0F;
			this.line29.Width = 0F;
			this.line29.X1 = 10.65591F;
			this.line29.X2 = 10.65591F;
			this.line29.Y1 = 0F;
			this.line29.Y2 = 0.1877953F;
			// 
			// line30
			// 
			this.line30.Height = 0.1877953F;
			this.line30.Left = 9.757001F;
			this.line30.LineWeight = 1F;
			this.line30.Name = "line30";
			this.line30.Tag = "";
			this.line30.Top = 0F;
			this.line30.Width = 0F;
			this.line30.X1 = 9.757001F;
			this.line30.X2 = 9.757001F;
			this.line30.Y1 = 0F;
			this.line30.Y2 = 0.1877953F;
			// 
			// line31
			// 
			this.line31.Height = 0.1877953F;
			this.line31.Left = 7.633F;
			this.line31.LineWeight = 1F;
			this.line31.Name = "line31";
			this.line31.Tag = "";
			this.line31.Top = 0F;
			this.line31.Width = 0F;
			this.line31.X1 = 7.633F;
			this.line31.X2 = 7.633F;
			this.line31.Y1 = 0F;
			this.line31.Y2 = 0.1877953F;
			// 
			// line32
			// 
			this.line32.Height = 0.1877953F;
			this.line32.Left = 8.963F;
			this.line32.LineWeight = 1F;
			this.line32.Name = "line32";
			this.line32.Tag = "";
			this.line32.Top = 0F;
			this.line32.Width = 0F;
			this.line32.X1 = 8.963F;
			this.line32.X2 = 8.963F;
			this.line32.Y1 = 0F;
			this.line32.Y2 = 0.1877953F;
			// 
			// line33
			// 
			this.line33.Height = 0.1877953F;
			this.line33.Left = 5.478921F;
			this.line33.LineWeight = 1F;
			this.line33.Name = "line33";
			this.line33.Tag = "";
			this.line33.Top = 5.820766E-11F;
			this.line33.Width = 0F;
			this.line33.X1 = 5.478921F;
			this.line33.X2 = 5.478921F;
			this.line33.Y1 = 5.820766E-11F;
			this.line33.Y2 = 0.1877953F;
			// 
			// line34
			// 
			this.line34.Height = 0.1877953F;
			this.line34.Left = 2.205119F;
			this.line34.LineWeight = 1F;
			this.line34.Name = "line34";
			this.line34.Tag = "";
			this.line34.Top = 0F;
			this.line34.Width = 0F;
			this.line34.X1 = 2.205119F;
			this.line34.X2 = 2.205119F;
			this.line34.Y1 = 0F;
			this.line34.Y2 = 0.1877953F;
			// 
			// line35
			// 
			this.line35.Height = 0.1877953F;
			this.line35.Left = 1.018229F;
			this.line35.LineWeight = 1F;
			this.line35.Name = "line35";
			this.line35.Tag = "";
			this.line35.Top = 0F;
			this.line35.Width = 0F;
			this.line35.X1 = 1.018229F;
			this.line35.X2 = 1.018229F;
			this.line35.Y1 = 0F;
			this.line35.Y2 = 0.1877953F;
			// 
			// line37
			// 
			this.line37.Height = 0F;
			this.line37.Left = 0.004F;
			this.line37.LineWeight = 1F;
			this.line37.Name = "line37";
			this.line37.Tag = "";
			this.line37.Top = 0.1877953F;
			this.line37.Width = 11.319F;
			this.line37.X1 = 0.004F;
			this.line37.X2 = 11.323F;
			this.line37.Y1 = 0.1877953F;
			this.line37.Y2 = 0.1877953F;
			// 
			// line38
			// 
			this.line38.Height = 0.1877953F;
			this.line38.Left = 0F;
			this.line38.LineWeight = 1F;
			this.line38.Name = "line38";
			this.line38.Tag = "";
			this.line38.Top = 0F;
			this.line38.Width = 0F;
			this.line38.X1 = 0F;
			this.line38.X2 = 0F;
			this.line38.Y1 = 0F;
			this.line38.Y2 = 0.1877953F;
			// 
			// line58
			// 
			this.line58.Height = 0.188F;
			this.line58.Left = 8.483001F;
			this.line58.LineWeight = 1F;
			this.line58.Name = "line58";
			this.line58.Tag = "";
			this.line58.Top = 0F;
			this.line58.Width = 0F;
			this.line58.X1 = 8.483001F;
			this.line58.X2 = 8.483001F;
			this.line58.Y1 = 0F;
			this.line58.Y2 = 0.188F;
			// 
			// line70
			// 
			this.line70.Height = 0.1937953F;
			this.line70.Left = 2.757F;
			this.line70.LineWeight = 1F;
			this.line70.Name = "line70";
			this.line70.Tag = "";
			this.line70.Top = 0.5602047F;
			this.line70.Width = 0F;
			this.line70.X1 = 2.757F;
			this.line70.X2 = 2.757F;
			this.line70.Y1 = 0.5602047F;
			this.line70.Y2 = 0.754F;
			// 
			// line71
			// 
			this.line71.Height = 0.1937953F;
			this.line71.Left = 4.977001F;
			this.line71.LineWeight = 1F;
			this.line71.Name = "line71";
			this.line71.Tag = "";
			this.line71.Top = 0.5602047F;
			this.line71.Width = 0F;
			this.line71.X1 = 4.977001F;
			this.line71.X2 = 4.977001F;
			this.line71.Y1 = 0.5602047F;
			this.line71.Y2 = 0.754F;
			// 
			// line72
			// 
			this.line72.Height = 0.1937953F;
			this.line72.Left = 6.138F;
			this.line72.LineWeight = 1F;
			this.line72.Name = "line72";
			this.line72.Tag = "";
			this.line72.Top = 0.5602047F;
			this.line72.Width = 0F;
			this.line72.X1 = 6.138F;
			this.line72.X2 = 6.138F;
			this.line72.Y1 = 0.5602047F;
			this.line72.Y2 = 0.754F;
			// 
			// line73
			// 
			this.line73.Height = 0.1937953F;
			this.line73.Left = 6.949999F;
			this.line73.LineWeight = 1F;
			this.line73.Name = "line73";
			this.line73.Tag = "";
			this.line73.Top = 0.5602047F;
			this.line73.Width = 0F;
			this.line73.X1 = 6.949999F;
			this.line73.X2 = 6.949999F;
			this.line73.Y1 = 0.5602047F;
			this.line73.Y2 = 0.754F;
			// 
			// line74
			// 
			this.line74.Height = 0.176F;
			this.line74.Left = 11.323F;
			this.line74.LineWeight = 1F;
			this.line74.Name = "line74";
			this.line74.Tag = "";
			this.line74.Top = 0.578F;
			this.line74.Width = 0F;
			this.line74.X1 = 11.323F;
			this.line74.X2 = 11.323F;
			this.line74.Y1 = 0.578F;
			this.line74.Y2 = 0.754F;
			// 
			// line75
			// 
			this.line75.Height = 0.1937953F;
			this.line75.Left = 10.6559F;
			this.line75.LineWeight = 1F;
			this.line75.Name = "line75";
			this.line75.Tag = "";
			this.line75.Top = 0.5602047F;
			this.line75.Width = 0F;
			this.line75.X1 = 10.6559F;
			this.line75.X2 = 10.6559F;
			this.line75.Y1 = 0.5602047F;
			this.line75.Y2 = 0.754F;
			// 
			// line76
			// 
			this.line76.Height = 0.1937953F;
			this.line76.Left = 9.757001F;
			this.line76.LineWeight = 1F;
			this.line76.Name = "line76";
			this.line76.Tag = "";
			this.line76.Top = 0.5602047F;
			this.line76.Width = 0F;
			this.line76.X1 = 9.757001F;
			this.line76.X2 = 9.757001F;
			this.line76.Y1 = 0.5602047F;
			this.line76.Y2 = 0.754F;
			// 
			// line77
			// 
			this.line77.Height = 0.1937953F;
			this.line77.Left = 7.633001F;
			this.line77.LineWeight = 1F;
			this.line77.Name = "line77";
			this.line77.Tag = "";
			this.line77.Top = 0.5602047F;
			this.line77.Width = 0F;
			this.line77.X1 = 7.633001F;
			this.line77.X2 = 7.633001F;
			this.line77.Y1 = 0.5602047F;
			this.line77.Y2 = 0.754F;
			// 
			// line78
			// 
			this.line78.Height = 0.1937955F;
			this.line78.Left = 8.963F;
			this.line78.LineWeight = 1F;
			this.line78.Name = "line78";
			this.line78.Tag = "";
			this.line78.Top = 0.5602045F;
			this.line78.Width = 0F;
			this.line78.X1 = 8.963F;
			this.line78.X2 = 8.963F;
			this.line78.Y1 = 0.5602045F;
			this.line78.Y2 = 0.754F;
			// 
			// line79
			// 
			this.line79.Height = 0.1937953F;
			this.line79.Left = 5.47892F;
			this.line79.LineWeight = 1F;
			this.line79.Name = "line79";
			this.line79.Tag = "";
			this.line79.Top = 0.5602047F;
			this.line79.Width = 0F;
			this.line79.X1 = 5.47892F;
			this.line79.X2 = 5.47892F;
			this.line79.Y1 = 0.5602047F;
			this.line79.Y2 = 0.754F;
			// 
			// line80
			// 
			this.line80.Height = 0.1937953F;
			this.line80.Left = 1.018229F;
			this.line80.LineWeight = 1F;
			this.line80.Name = "line80";
			this.line80.Tag = "";
			this.line80.Top = 0.5602047F;
			this.line80.Width = 0F;
			this.line80.X1 = 1.018229F;
			this.line80.X2 = 1.018229F;
			this.line80.Y1 = 0.5602047F;
			this.line80.Y2 = 0.754F;
			// 
			// line82
			// 
			this.line82.Height = 0.1937953F;
			this.line82.Left = 2.205119F;
			this.line82.LineWeight = 1F;
			this.line82.Name = "line82";
			this.line82.Tag = "";
			this.line82.Top = 0.5602047F;
			this.line82.Width = 0F;
			this.line82.X1 = 2.205119F;
			this.line82.X2 = 2.205119F;
			this.line82.Y1 = 0.5602047F;
			this.line82.Y2 = 0.754F;
			// 
			// line84
			// 
			this.line84.Height = 0F;
			this.line84.Left = 0F;
			this.line84.LineWeight = 1F;
			this.line84.Name = "line84";
			this.line84.Tag = "";
			this.line84.Top = 0.752F;
			this.line84.Width = 11.323F;
			this.line84.X1 = 0F;
			this.line84.X2 = 11.323F;
			this.line84.Y1 = 0.752F;
			this.line84.Y2 = 0.752F;
			// 
			// line87
			// 
			this.line87.Height = 0.198F;
			this.line87.Left = 0F;
			this.line87.LineWeight = 1F;
			this.line87.Name = "line87";
			this.line87.Tag = "";
			this.line87.Top = 0.187F;
			this.line87.Width = 0F;
			this.line87.X1 = 0F;
			this.line87.X2 = 0F;
			this.line87.Y1 = 0.187F;
			this.line87.Y2 = 0.385F;
			// 
			// line88
			// 
			this.line88.Height = 0.194F;
			this.line88.Left = 8.483002F;
			this.line88.LineWeight = 1F;
			this.line88.Name = "line88";
			this.line88.Tag = "";
			this.line88.Top = 0.56F;
			this.line88.Width = 0F;
			this.line88.X1 = 8.483002F;
			this.line88.X2 = 8.483002F;
			this.line88.Y1 = 0.56F;
			this.line88.Y2 = 0.754F;
			// 
			// line91
			// 
			this.line91.Height = 0.1935907F;
			this.line91.Left = 0.58F;
			this.line91.LineWeight = 1F;
			this.line91.Name = "line91";
			this.line91.Tag = "";
			this.line91.Top = 0.5604093F;
			this.line91.Width = 0.0002055168F;
			this.line91.X1 = 0.58F;
			this.line91.X2 = 0.5802055F;
			this.line91.Y1 = 0.5604093F;
			this.line91.Y2 = 0.754F;
			// 
			// line41
			// 
			this.line41.Height = 0.1977953F;
			this.line41.Left = 0.58F;
			this.line41.LineWeight = 1F;
			this.line41.Name = "line41";
			this.line41.Tag = "";
			this.line41.Top = 0.187F;
			this.line41.Width = 4.070997E-05F;
			this.line41.X1 = 0.58F;
			this.line41.X2 = 0.5800407F;
			this.line41.Y1 = 0.187F;
			this.line41.Y2 = 0.3847953F;
			// 
			// line42
			// 
			this.line42.Height = 0.1977953F;
			this.line42.Left = 2.757F;
			this.line42.LineWeight = 1F;
			this.line42.Name = "line42";
			this.line42.Tag = "";
			this.line42.Top = 0.187F;
			this.line42.Width = 0F;
			this.line42.X1 = 2.757F;
			this.line42.X2 = 2.757F;
			this.line42.Y1 = 0.187F;
			this.line42.Y2 = 0.3847953F;
			// 
			// line43
			// 
			this.line43.Height = 0.1977953F;
			this.line43.Left = 4.977F;
			this.line43.LineWeight = 1F;
			this.line43.Name = "line43";
			this.line43.Tag = "";
			this.line43.Top = 0.187F;
			this.line43.Width = 0F;
			this.line43.X1 = 4.977F;
			this.line43.X2 = 4.977F;
			this.line43.Y1 = 0.187F;
			this.line43.Y2 = 0.3847953F;
			// 
			// line44
			// 
			this.line44.Height = 0.1977953F;
			this.line44.Left = 6.138F;
			this.line44.LineWeight = 1F;
			this.line44.Name = "line44";
			this.line44.Tag = "";
			this.line44.Top = 0.187F;
			this.line44.Width = 0F;
			this.line44.X1 = 6.138F;
			this.line44.X2 = 6.138F;
			this.line44.Y1 = 0.187F;
			this.line44.Y2 = 0.3847953F;
			// 
			// line45
			// 
			this.line45.Height = 0.1977953F;
			this.line45.Left = 6.95F;
			this.line45.LineWeight = 1F;
			this.line45.Name = "line45";
			this.line45.Tag = "";
			this.line45.Top = 0.187F;
			this.line45.Width = 0F;
			this.line45.X1 = 6.95F;
			this.line45.X2 = 6.95F;
			this.line45.Y1 = 0.187F;
			this.line45.Y2 = 0.3847953F;
			// 
			// line46
			// 
			this.line46.Height = 0.1977953F;
			this.line46.Left = 11.323F;
			this.line46.LineWeight = 1F;
			this.line46.Name = "line46";
			this.line46.Tag = "";
			this.line46.Top = 0.187F;
			this.line46.Width = 0F;
			this.line46.X1 = 11.323F;
			this.line46.X2 = 11.323F;
			this.line46.Y1 = 0.187F;
			this.line46.Y2 = 0.3847953F;
			// 
			// line47
			// 
			this.line47.Height = 0.1977953F;
			this.line47.Left = 10.65591F;
			this.line47.LineWeight = 1F;
			this.line47.Name = "line47";
			this.line47.Tag = "";
			this.line47.Top = 0.187F;
			this.line47.Width = 0F;
			this.line47.X1 = 10.65591F;
			this.line47.X2 = 10.65591F;
			this.line47.Y1 = 0.187F;
			this.line47.Y2 = 0.3847953F;
			// 
			// line48
			// 
			this.line48.Height = 0.1977953F;
			this.line48.Left = 9.757001F;
			this.line48.LineWeight = 1F;
			this.line48.Name = "line48";
			this.line48.Tag = "";
			this.line48.Top = 0.187F;
			this.line48.Width = 0F;
			this.line48.X1 = 9.757001F;
			this.line48.X2 = 9.757001F;
			this.line48.Y1 = 0.187F;
			this.line48.Y2 = 0.3847953F;
			// 
			// line49
			// 
			this.line49.Height = 0.1977953F;
			this.line49.Left = 7.633F;
			this.line49.LineWeight = 1F;
			this.line49.Name = "line49";
			this.line49.Tag = "";
			this.line49.Top = 0.187F;
			this.line49.Width = 0F;
			this.line49.X1 = 7.633F;
			this.line49.X2 = 7.633F;
			this.line49.Y1 = 0.187F;
			this.line49.Y2 = 0.3847953F;
			// 
			// line50
			// 
			this.line50.Height = 0.1977953F;
			this.line50.Left = 8.963F;
			this.line50.LineWeight = 1F;
			this.line50.Name = "line50";
			this.line50.Tag = "";
			this.line50.Top = 0.187F;
			this.line50.Width = 0F;
			this.line50.X1 = 8.963F;
			this.line50.X2 = 8.963F;
			this.line50.Y1 = 0.187F;
			this.line50.Y2 = 0.3847953F;
			// 
			// line51
			// 
			this.line51.Height = 0.1977953F;
			this.line51.Left = 5.478921F;
			this.line51.LineWeight = 1F;
			this.line51.Name = "line51";
			this.line51.Tag = "";
			this.line51.Top = 0.187F;
			this.line51.Width = 0F;
			this.line51.X1 = 5.478921F;
			this.line51.X2 = 5.478921F;
			this.line51.Y1 = 0.187F;
			this.line51.Y2 = 0.3847953F;
			// 
			// line52
			// 
			this.line52.Height = 0.1977953F;
			this.line52.Left = 2.205119F;
			this.line52.LineWeight = 1F;
			this.line52.Name = "line52";
			this.line52.Tag = "";
			this.line52.Top = 0.187F;
			this.line52.Width = 0F;
			this.line52.X1 = 2.205119F;
			this.line52.X2 = 2.205119F;
			this.line52.Y1 = 0.187F;
			this.line52.Y2 = 0.3847953F;
			// 
			// line54
			// 
			this.line54.Height = 0.1977953F;
			this.line54.Left = 1.01823F;
			this.line54.LineWeight = 1F;
			this.line54.Name = "line54";
			this.line54.Tag = "";
			this.line54.Top = 0.187F;
			this.line54.Width = 0F;
			this.line54.X1 = 1.01823F;
			this.line54.X2 = 1.01823F;
			this.line54.Y1 = 0.187F;
			this.line54.Y2 = 0.3847953F;
			// 
			// line57
			// 
			this.line57.Height = 0F;
			this.line57.Left = 0.004000477F;
			this.line57.LineWeight = 1F;
			this.line57.Name = "line57";
			this.line57.Tag = "";
			this.line57.Top = 0.376F;
			this.line57.Width = 11.319F;
			this.line57.X1 = 0.004000477F;
			this.line57.X2 = 11.323F;
			this.line57.Y1 = 0.376F;
			this.line57.Y2 = 0.376F;
			// 
			// line59
			// 
			this.line59.Height = 0.198F;
			this.line59.Left = 8.483F;
			this.line59.LineWeight = 1F;
			this.line59.Name = "line59";
			this.line59.Tag = "";
			this.line59.Top = 0.187F;
			this.line59.Width = 0F;
			this.line59.X1 = 8.483F;
			this.line59.X2 = 8.483F;
			this.line59.Y1 = 0.187F;
			this.line59.Y2 = 0.385F;
			// 
			// line60
			// 
			this.line60.Height = 0.1877953F;
			this.line60.Left = 0.5796716F;
			this.line60.LineWeight = 1F;
			this.line60.Name = "line60";
			this.line60.Tag = "";
			this.line60.Top = 0.39F;
			this.line60.Width = 0F;
			this.line60.X1 = 0.5796716F;
			this.line60.X2 = 0.5796716F;
			this.line60.Y1 = 0.39F;
			this.line60.Y2 = 0.5777953F;
			// 
			// line61
			// 
			this.line61.Height = 0.1877953F;
			this.line61.Left = 2.757F;
			this.line61.LineWeight = 1F;
			this.line61.Name = "line61";
			this.line61.Tag = "";
			this.line61.Top = 0.39F;
			this.line61.Width = 0F;
			this.line61.X1 = 2.757F;
			this.line61.X2 = 2.757F;
			this.line61.Y1 = 0.39F;
			this.line61.Y2 = 0.5777953F;
			// 
			// line62
			// 
			this.line62.Height = 0.1877954F;
			this.line62.Left = 4.976631F;
			this.line62.LineWeight = 1F;
			this.line62.Name = "line62";
			this.line62.Tag = "";
			this.line62.Top = 0.39F;
			this.line62.Width = 0F;
			this.line62.X1 = 4.976631F;
			this.line62.X2 = 4.976631F;
			this.line62.Y1 = 0.39F;
			this.line62.Y2 = 0.5777954F;
			// 
			// line63
			// 
			this.line63.Height = 0.1877953F;
			this.line63.Left = 6.137631F;
			this.line63.LineWeight = 1F;
			this.line63.Name = "line63";
			this.line63.Tag = "";
			this.line63.Top = 0.39F;
			this.line63.Width = 0F;
			this.line63.X1 = 6.137631F;
			this.line63.X2 = 6.137631F;
			this.line63.Y1 = 0.39F;
			this.line63.Y2 = 0.5777953F;
			// 
			// line64
			// 
			this.line64.Height = 0.1877953F;
			this.line64.Left = 6.949631F;
			this.line64.LineWeight = 1F;
			this.line64.Name = "line64";
			this.line64.Tag = "";
			this.line64.Top = 0.39F;
			this.line64.Width = 0F;
			this.line64.X1 = 6.949631F;
			this.line64.X2 = 6.949631F;
			this.line64.Y1 = 0.39F;
			this.line64.Y2 = 0.5777953F;
			// 
			// line65
			// 
			this.line65.Height = 0.1877953F;
			this.line65.Left = 11.32263F;
			this.line65.LineWeight = 1F;
			this.line65.Name = "line65";
			this.line65.Tag = "";
			this.line65.Top = 0.39F;
			this.line65.Width = 0F;
			this.line65.X1 = 11.32263F;
			this.line65.X2 = 11.32263F;
			this.line65.Y1 = 0.39F;
			this.line65.Y2 = 0.5777953F;
			// 
			// line66
			// 
			this.line66.Height = 0.1877953F;
			this.line66.Left = 10.65554F;
			this.line66.LineWeight = 1F;
			this.line66.Name = "line66";
			this.line66.Tag = "";
			this.line66.Top = 0.39F;
			this.line66.Width = 0F;
			this.line66.X1 = 10.65554F;
			this.line66.X2 = 10.65554F;
			this.line66.Y1 = 0.39F;
			this.line66.Y2 = 0.5777953F;
			// 
			// line67
			// 
			this.line67.Height = 0.1877953F;
			this.line67.Left = 9.757001F;
			this.line67.LineWeight = 1F;
			this.line67.Name = "line67";
			this.line67.Tag = "";
			this.line67.Top = 0.39F;
			this.line67.Width = 0F;
			this.line67.X1 = 9.757001F;
			this.line67.X2 = 9.757001F;
			this.line67.Y1 = 0.39F;
			this.line67.Y2 = 0.5777953F;
			// 
			// line68
			// 
			this.line68.Height = 0.1877953F;
			this.line68.Left = 7.632631F;
			this.line68.LineWeight = 1F;
			this.line68.Name = "line68";
			this.line68.Tag = "";
			this.line68.Top = 0.39F;
			this.line68.Width = 0F;
			this.line68.X1 = 7.632631F;
			this.line68.X2 = 7.632631F;
			this.line68.Y1 = 0.39F;
			this.line68.Y2 = 0.5777953F;
			// 
			// line69
			// 
			this.line69.Height = 0.1877953F;
			this.line69.Left = 8.963F;
			this.line69.LineWeight = 1F;
			this.line69.Name = "line69";
			this.line69.Tag = "";
			this.line69.Top = 0.39F;
			this.line69.Width = 0F;
			this.line69.X1 = 8.963F;
			this.line69.X2 = 8.963F;
			this.line69.Y1 = 0.39F;
			this.line69.Y2 = 0.5777953F;
			// 
			// line81
			// 
			this.line81.Height = 0.1877953F;
			this.line81.Left = 5.478552F;
			this.line81.LineWeight = 1F;
			this.line81.Name = "line81";
			this.line81.Tag = "";
			this.line81.Top = 0.39F;
			this.line81.Width = 0F;
			this.line81.X1 = 5.478552F;
			this.line81.X2 = 5.478552F;
			this.line81.Y1 = 0.39F;
			this.line81.Y2 = 0.5777953F;
			// 
			// line83
			// 
			this.line83.Height = 0.1877953F;
			this.line83.Left = 2.204751F;
			this.line83.LineWeight = 1F;
			this.line83.Name = "line83";
			this.line83.Tag = "";
			this.line83.Top = 0.39F;
			this.line83.Width = 0F;
			this.line83.X1 = 2.204751F;
			this.line83.X2 = 2.204751F;
			this.line83.Y1 = 0.39F;
			this.line83.Y2 = 0.5777953F;
			// 
			// line85
			// 
			this.line85.Height = 0.1877953F;
			this.line85.Left = 1.017861F;
			this.line85.LineWeight = 1F;
			this.line85.Name = "line85";
			this.line85.Tag = "";
			this.line85.Top = 0.39F;
			this.line85.Width = 0F;
			this.line85.X1 = 1.017861F;
			this.line85.X2 = 1.017861F;
			this.line85.Y1 = 0.39F;
			this.line85.Y2 = 0.5777953F;
			// 
			// line86
			// 
			this.line86.Height = 0F;
			this.line86.Left = 0.003631405F;
			this.line86.LineWeight = 1F;
			this.line86.Name = "line86";
			this.line86.Tag = "";
			this.line86.Top = 0.564F;
			this.line86.Width = 11.31937F;
			this.line86.X1 = 0.003631405F;
			this.line86.X2 = 11.323F;
			this.line86.Y1 = 0.564F;
			this.line86.Y2 = 0.564F;
			// 
			// line148
			// 
			this.line148.Height = 0.188F;
			this.line148.Left = 8.482631F;
			this.line148.LineWeight = 1F;
			this.line148.Name = "line148";
			this.line148.Tag = "";
			this.line148.Top = 0.39F;
			this.line148.Width = 0F;
			this.line148.X1 = 8.482631F;
			this.line148.X2 = 8.482631F;
			this.line148.Y1 = 0.39F;
			this.line148.Y2 = 0.578F;
			// 
			// line149
			// 
			this.line149.Height = 0.188F;
			this.line149.Left = 0F;
			this.line149.LineWeight = 1F;
			this.line149.Name = "line149";
			this.line149.Tag = "";
			this.line149.Top = 0.39F;
			this.line149.Width = 0F;
			this.line149.X1 = 0F;
			this.line149.X2 = 0F;
			this.line149.Y1 = 0.39F;
			this.line149.Y2 = 0.578F;
			// 
			// line150
			// 
			this.line150.Height = 0.194F;
			this.line150.Left = 0F;
			this.line150.LineWeight = 1F;
			this.line150.Name = "line150";
			this.line150.Tag = "";
			this.line150.Top = 0.578F;
			this.line150.Width = 0F;
			this.line150.X1 = 0F;
			this.line150.X2 = 0F;
			this.line150.Y1 = 0.578F;
			this.line150.Y2 = 0.772F;
			// 
			// groupHeader1
			// 
			this.groupHeader1.CanGrow = false;
			this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト35,
            this.テキスト308,
            this.テキスト253,
            this.テキスト33,
            this.テキスト32,
            this.テキスト43});
			this.groupHeader1.DataField = "ITEM05";
			this.groupHeader1.Height = 0.188F;
			this.groupHeader1.Name = "groupHeader1";
			this.groupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.groupHeader1.UnderlayNext = true;
			// 
			// groupFooter1
			// 
			this.groupFooter1.CanShrink = true;
			this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox53,
            this.textBox51,
            this.textBox52,
            this.textBox39,
            this.textBox43,
            this.textBox44,
            this.textBox40,
            this.textBox20,
            this.textBox17,
            this.textBox21,
            this.textBox19,
            this.textBox38,
            this.textBox41,
            this.textBox45,
            this.textBox42,
            this.textBox50,
            this.textBox49,
            this.textBox48,
            this.textBox3,
            this.line6,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line21,
            this.line7,
            this.line39,
            this.line56,
            this.line89,
            this.line90,
            this.line92,
            this.line93,
            this.line94,
            this.line95,
            this.line96,
            this.line97,
            this.line98,
            this.line99,
            this.line100,
            this.line101,
            this.line102,
            this.line103,
            this.line104,
            this.line105,
            this.line106,
            this.line108,
            this.line109,
            this.line110,
            this.line111,
            this.line112,
            this.line113,
            this.line114,
            this.line115,
            this.line116,
            this.line117,
            this.line118,
            this.line119,
            this.line120,
            this.line121,
            this.line123,
            this.line5,
            this.line20,
            this.line36,
            this.line53,
            this.line107,
            this.line122,
            this.line124,
            this.line125,
            this.line126,
            this.line127,
            this.line128,
            this.line129,
            this.line130,
            this.line131,
            this.line132,
            this.line133,
            this.line134,
            this.line135,
            this.line136,
            this.line137,
            this.line138,
            this.line139,
            this.line140,
            this.line141,
            this.line142,
            this.line143,
            this.line144,
            this.line145,
            this.line146,
            this.line147,
            this.line40});
			this.groupFooter1.Height = 0.94F;
			this.groupFooter1.Name = "groupFooter1";
			this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
			// 
			// textBox53
			// 
			this.textBox53.DataField = "ITEM33";
			this.textBox53.Height = 0.2F;
			this.textBox53.Left = 2.757236F;
			this.textBox53.Name = "textBox53";
			this.textBox53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; vertical-align: middle; ddo-char-set: 128";
			this.textBox53.Tag = "";
			this.textBox53.Text = "【 伝 票 合 計 】";
			this.textBox53.Top = 0.551F;
			this.textBox53.Width = 1.173622F;
			// 
			// textBox51
			// 
			this.textBox51.DataField = "ITEM12";
			this.textBox51.Height = 0.178F;
			this.textBox51.Left = 5.479F;
			this.textBox51.Name = "textBox51";
			this.textBox51.OutputFormat = resources.GetString("textBox51.OutputFormat");
			this.textBox51.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox51.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox51.SummaryGroup = "groupHeader1";
			this.textBox51.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox51.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox51.Tag = "";
			this.textBox51.Text = "ITEM12";
			this.textBox51.Top = 0.565F;
			this.textBox51.Width = 0.6590005F;
			// 
			// textBox52
			// 
			this.textBox52.DataField = "ITEM13";
			this.textBox52.Height = 0.178F;
			this.textBox52.Left = 6.138F;
			this.textBox52.Name = "textBox52";
			this.textBox52.OutputFormat = resources.GetString("textBox52.OutputFormat");
			this.textBox52.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox52.SummaryGroup = "groupHeader1";
			this.textBox52.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox52.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox52.Tag = "";
			this.textBox52.Text = "ITEM13";
			this.textBox52.Top = 0.565F;
			this.textBox52.Width = 0.812F;
			// 
			// textBox39
			// 
			this.textBox39.DataField = "ITEM25";
			this.textBox39.Height = 0.178F;
			this.textBox39.Left = 8.957001F;
			this.textBox39.Name = "textBox39";
			this.textBox39.OutputFormat = resources.GetString("textBox39.OutputFormat");
			this.textBox39.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox39.SummaryGroup = "groupHeader1";
			this.textBox39.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox39.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox39.Tag = "";
			this.textBox39.Text = "ITEM25";
			this.textBox39.Top = 0.191F;
			this.textBox39.Width = 0.8000001F;
			// 
			// textBox43
			// 
			this.textBox43.DataField = "ITEM28";
			this.textBox43.Height = 0.178F;
			this.textBox43.Left = 8.957001F;
			this.textBox43.Name = "textBox43";
			this.textBox43.OutputFormat = resources.GetString("textBox43.OutputFormat");
			this.textBox43.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox43.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox43.SummaryGroup = "groupHeader1";
			this.textBox43.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox43.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox43.Tag = "";
			this.textBox43.Text = "ITEM28";
			this.textBox43.Top = 0.377F;
			this.textBox43.Width = 0.8000001F;
			// 
			// textBox44
			// 
			this.textBox44.DataField = "ITEM29";
			this.textBox44.Height = 0.178F;
			this.textBox44.Left = 9.757001F;
			this.textBox44.Name = "textBox44";
			this.textBox44.OutputFormat = resources.GetString("textBox44.OutputFormat");
			this.textBox44.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox44.SummaryGroup = "groupHeader1";
			this.textBox44.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox44.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox44.Tag = "";
			this.textBox44.Text = "ITEM29";
			this.textBox44.Top = 0.377F;
			this.textBox44.Width = 0.8999997F;
			// 
			// textBox40
			// 
			this.textBox40.DataField = "ITEM26";
			this.textBox40.Height = 0.178F;
			this.textBox40.Left = 9.757001F;
			this.textBox40.Name = "textBox40";
			this.textBox40.OutputFormat = resources.GetString("textBox40.OutputFormat");
			this.textBox40.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox40.SummaryGroup = "groupHeader1";
			this.textBox40.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox40.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox40.Tag = "";
			this.textBox40.Text = "ITEM26";
			this.textBox40.Top = 0.191F;
			this.textBox40.Width = 0.8999997F;
			// 
			// textBox20
			// 
			this.textBox20.DataField = "ITEM23";
			this.textBox20.Height = 0.178F;
			this.textBox20.Left = 9.757001F;
			this.textBox20.Name = "textBox20";
			this.textBox20.OutputFormat = resources.GetString("textBox20.OutputFormat");
			this.textBox20.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox20.SummaryGroup = "groupHeader1";
			this.textBox20.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox20.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox20.Tag = "";
			this.textBox20.Text = "ITEM23";
			this.textBox20.Top = 0F;
			this.textBox20.Width = 0.8999997F;
			// 
			// textBox17
			// 
			this.textBox17.DataField = "ITEM22";
			this.textBox17.Height = 0.178F;
			this.textBox17.Left = 8.957001F;
			this.textBox17.Name = "textBox17";
			this.textBox17.OutputFormat = resources.GetString("textBox17.OutputFormat");
			this.textBox17.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox17.SummaryGroup = "groupHeader1";
			this.textBox17.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox17.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox17.Tag = "";
			this.textBox17.Text = "ITEM22";
			this.textBox17.Top = 0F;
			this.textBox17.Width = 0.8000001F;
			// 
			// textBox21
			// 
			this.textBox21.DataField = "ITEM30";
			this.textBox21.Height = 0.178F;
			this.textBox21.Left = 8.483001F;
			this.textBox21.Name = "textBox21";
			this.textBox21.OutputFormat = resources.GetString("textBox21.OutputFormat");
			this.textBox21.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128; ddo-font-vertical: none";
			this.textBox21.Tag = "";
			this.textBox21.Text = "ITEM30";
			this.textBox21.Top = 0F;
			this.textBox21.Width = 0.4739999F;
			// 
			// textBox19
			// 
			this.textBox19.DataField = "ITEM21";
			this.textBox19.Height = 0.178F;
			this.textBox19.Left = 7.633F;
			this.textBox19.Name = "textBox19";
			this.textBox19.OutputFormat = resources.GetString("textBox19.OutputFormat");
			this.textBox19.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox19.SummaryGroup = "groupHeader1";
			this.textBox19.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox19.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox19.Tag = "";
			this.textBox19.Text = "ITEM21";
			this.textBox19.Top = 0F;
			this.textBox19.Width = 0.8500004F;
			// 
			// textBox38
			// 
			this.textBox38.DataField = "ITEM24";
			this.textBox38.Height = 0.178F;
			this.textBox38.Left = 7.633F;
			this.textBox38.Name = "textBox38";
			this.textBox38.OutputFormat = resources.GetString("textBox38.OutputFormat");
			this.textBox38.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox38.SummaryGroup = "groupHeader1";
			this.textBox38.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox38.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox38.Tag = "";
			this.textBox38.Text = "ITEM24";
			this.textBox38.Top = 0.191F;
			this.textBox38.Width = 0.8500004F;
			// 
			// textBox41
			// 
			this.textBox41.DataField = "ITEM31";
			this.textBox41.Height = 0.178F;
			this.textBox41.Left = 8.483001F;
			this.textBox41.Name = "textBox41";
			this.textBox41.OutputFormat = resources.GetString("textBox41.OutputFormat");
			this.textBox41.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128; ddo-font-vertical: none";
			this.textBox41.Tag = "";
			this.textBox41.Text = "ITEM31";
			this.textBox41.Top = 0.191F;
			this.textBox41.Width = 0.4739999F;
			// 
			// textBox45
			// 
			this.textBox45.DataField = "ITEM32";
			this.textBox45.Height = 0.178F;
			this.textBox45.Left = 8.483001F;
			this.textBox45.Name = "textBox45";
			this.textBox45.OutputFormat = resources.GetString("textBox45.OutputFormat");
			this.textBox45.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox45.ShrinkToFit = true;
			this.textBox45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128; ddo-font-vertical: none; ddo" +
    "-shrink-to-fit: true";
			this.textBox45.Tag = "";
			this.textBox45.Text = "ITEM32";
			this.textBox45.Top = 0.377F;
			this.textBox45.Width = 0.4739999F;
			// 
			// textBox42
			// 
			this.textBox42.DataField = "ITEM27";
			this.textBox42.Height = 0.178F;
			this.textBox42.Left = 7.633F;
			this.textBox42.Name = "textBox42";
			this.textBox42.OutputFormat = resources.GetString("textBox42.OutputFormat");
			this.textBox42.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox42.SummaryGroup = "groupHeader1";
			this.textBox42.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox42.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox42.Tag = "";
			this.textBox42.Text = "ITEM27";
			this.textBox42.Top = 0.377F;
			this.textBox42.Width = 0.8500004F;
			// 
			// textBox50
			// 
			this.textBox50.DataField = "ITEM15";
			this.textBox50.Height = 0.178F;
			this.textBox50.Left = 7.633F;
			this.textBox50.Name = "textBox50";
			this.textBox50.OutputFormat = resources.GetString("textBox50.OutputFormat");
			this.textBox50.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox50.SummaryGroup = "groupHeader1";
			this.textBox50.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox50.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox50.Tag = "";
			this.textBox50.Text = "ITEM15";
			this.textBox50.Top = 0.561F;
			this.textBox50.Width = 0.8500004F;
			// 
			// textBox49
			// 
			this.textBox49.DataField = " ITEM16";
			this.textBox49.Height = 0.178F;
			this.textBox49.Left = 8.957001F;
			this.textBox49.Name = "textBox49";
			this.textBox49.OutputFormat = resources.GetString("textBox49.OutputFormat");
			this.textBox49.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox49.SummaryGroup = "groupHeader1";
			this.textBox49.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox49.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox49.Tag = "";
			this.textBox49.Text = " ITEM16";
			this.textBox49.Top = 0.561F;
			this.textBox49.Width = 0.8000001F;
			// 
			// textBox48
			// 
			this.textBox48.DataField = " ITEM17";
			this.textBox48.Height = 0.178F;
			this.textBox48.Left = 9.757001F;
			this.textBox48.Name = "textBox48";
			this.textBox48.OutputFormat = resources.GetString("textBox48.OutputFormat");
			this.textBox48.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
			this.textBox48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
			this.textBox48.SummaryGroup = "groupHeader1";
			this.textBox48.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.textBox48.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.textBox48.Tag = "";
			this.textBox48.Text = " ITEM17";
			this.textBox48.Top = 0.561F;
			this.textBox48.Width = 0.8999997F;
			// 
			// line6
			// 
			this.line6.Height = 0.1877953F;
			this.line6.Left = 0.5799213F;
			this.line6.LineWeight = 1F;
			this.line6.Name = "line6";
			this.line6.Tag = "";
			this.line6.Top = 0F;
			this.line6.Width = 0F;
			this.line6.X1 = 0.5799213F;
			this.line6.X2 = 0.5799213F;
			this.line6.Y1 = 0F;
			this.line6.Y2 = 0.1877953F;
			// 
			// line8
			// 
			this.line8.Height = 0.1877953F;
			this.line8.Left = 2.757F;
			this.line8.LineWeight = 1F;
			this.line8.Name = "line8";
			this.line8.Tag = "";
			this.line8.Top = 0F;
			this.line8.Width = 0F;
			this.line8.X1 = 2.757F;
			this.line8.X2 = 2.757F;
			this.line8.Y1 = 0F;
			this.line8.Y2 = 0.1877953F;
			// 
			// line9
			// 
			this.line9.Height = 0.1877953F;
			this.line9.Left = 4.977F;
			this.line9.LineWeight = 1F;
			this.line9.Name = "line9";
			this.line9.Tag = "";
			this.line9.Top = 0F;
			this.line9.Width = 0F;
			this.line9.X1 = 4.977F;
			this.line9.X2 = 4.977F;
			this.line9.Y1 = 0F;
			this.line9.Y2 = 0.1877953F;
			// 
			// line10
			// 
			this.line10.Height = 0.1877953F;
			this.line10.Left = 6.138F;
			this.line10.LineWeight = 1F;
			this.line10.Name = "line10";
			this.line10.Tag = "";
			this.line10.Top = 0F;
			this.line10.Width = 0F;
			this.line10.X1 = 6.138F;
			this.line10.X2 = 6.138F;
			this.line10.Y1 = 0F;
			this.line10.Y2 = 0.1877953F;
			// 
			// line11
			// 
			this.line11.Height = 0.1877953F;
			this.line11.Left = 6.95F;
			this.line11.LineWeight = 1F;
			this.line11.Name = "line11";
			this.line11.Tag = "";
			this.line11.Top = 0F;
			this.line11.Width = 0F;
			this.line11.X1 = 6.95F;
			this.line11.X2 = 6.95F;
			this.line11.Y1 = 0F;
			this.line11.Y2 = 0.1877953F;
			// 
			// line12
			// 
			this.line12.Height = 0.1877953F;
			this.line12.Left = 11.323F;
			this.line12.LineWeight = 1F;
			this.line12.Name = "line12";
			this.line12.Tag = "";
			this.line12.Top = 0F;
			this.line12.Width = 0F;
			this.line12.X1 = 11.323F;
			this.line12.X2 = 11.323F;
			this.line12.Y1 = 0F;
			this.line12.Y2 = 0.1877953F;
			// 
			// line13
			// 
			this.line13.Height = 0.1877953F;
			this.line13.Left = 10.657F;
			this.line13.LineWeight = 1F;
			this.line13.Name = "line13";
			this.line13.Tag = "";
			this.line13.Top = 9.313226E-10F;
			this.line13.Width = 0F;
			this.line13.X1 = 10.657F;
			this.line13.X2 = 10.657F;
			this.line13.Y1 = 9.313226E-10F;
			this.line13.Y2 = 0.1877953F;
			// 
			// line14
			// 
			this.line14.Height = 0.1877953F;
			this.line14.Left = 9.757001F;
			this.line14.LineWeight = 1F;
			this.line14.Name = "line14";
			this.line14.Tag = "";
			this.line14.Top = 9.313226E-10F;
			this.line14.Width = 0F;
			this.line14.X1 = 9.757001F;
			this.line14.X2 = 9.757001F;
			this.line14.Y1 = 9.313226E-10F;
			this.line14.Y2 = 0.1877953F;
			// 
			// line15
			// 
			this.line15.Height = 0.1877953F;
			this.line15.Left = 7.633F;
			this.line15.LineWeight = 1F;
			this.line15.Name = "line15";
			this.line15.Tag = "";
			this.line15.Top = 9.313226E-10F;
			this.line15.Width = 0F;
			this.line15.X1 = 7.633F;
			this.line15.X2 = 7.633F;
			this.line15.Y1 = 9.313226E-10F;
			this.line15.Y2 = 0.1877953F;
			// 
			// line16
			// 
			this.line16.Height = 0.1877953F;
			this.line16.Left = 8.957001F;
			this.line16.LineWeight = 1F;
			this.line16.Name = "line16";
			this.line16.Tag = "";
			this.line16.Top = 0F;
			this.line16.Width = 0F;
			this.line16.X1 = 8.957001F;
			this.line16.X2 = 8.957001F;
			this.line16.Y1 = 0F;
			this.line16.Y2 = 0.1877953F;
			// 
			// line17
			// 
			this.line17.Height = 0.1877953F;
			this.line17.Left = 5.479F;
			this.line17.LineWeight = 1F;
			this.line17.Name = "line17";
			this.line17.Tag = "";
			this.line17.Top = -2.910383E-11F;
			this.line17.Width = 0F;
			this.line17.X1 = 5.479F;
			this.line17.X2 = 5.479F;
			this.line17.Y1 = -2.910383E-11F;
			this.line17.Y2 = 0.1877953F;
			// 
			// line18
			// 
			this.line18.Height = 0.1877953F;
			this.line18.Left = 2.205F;
			this.line18.LineWeight = 1F;
			this.line18.Name = "line18";
			this.line18.Tag = "";
			this.line18.Top = 0F;
			this.line18.Width = 0F;
			this.line18.X1 = 2.205F;
			this.line18.X2 = 2.205F;
			this.line18.Y1 = 0F;
			this.line18.Y2 = 0.1877953F;
			// 
			// line19
			// 
			this.line19.Height = 0.1877953F;
			this.line19.Left = 1.01811F;
			this.line19.LineWeight = 1F;
			this.line19.Name = "line19";
			this.line19.Tag = "";
			this.line19.Top = 0F;
			this.line19.Width = 0F;
			this.line19.X1 = 1.01811F;
			this.line19.X2 = 1.01811F;
			this.line19.Y1 = 0F;
			this.line19.Y2 = 0.1877953F;
			// 
			// line21
			// 
			this.line21.Height = 0.0002046973F;
			this.line21.Left = 0F;
			this.line21.LineWeight = 1F;
			this.line21.Name = "line21";
			this.line21.Tag = "";
			this.line21.Top = 0.1877953F;
			this.line21.Width = 11.323F;
			this.line21.X1 = 0F;
			this.line21.X2 = 11.323F;
			this.line21.Y1 = 0.1877953F;
			this.line21.Y2 = 0.188F;
			// 
			// line7
			// 
			this.line7.Height = 0.1877953F;
			this.line7.Left = 0F;
			this.line7.LineWeight = 1F;
			this.line7.Name = "line7";
			this.line7.Tag = "";
			this.line7.Top = 0F;
			this.line7.Width = 0F;
			this.line7.X1 = 0F;
			this.line7.X2 = 0F;
			this.line7.Y1 = 0F;
			this.line7.Y2 = 0.1877953F;
			// 
			// line39
			// 
			this.line39.Height = 0.182F;
			this.line39.Left = 0F;
			this.line39.LineWeight = 1F;
			this.line39.Name = "line39";
			this.line39.Tag = "";
			this.line39.Top = 0.185F;
			this.line39.Width = 0F;
			this.line39.X1 = 0F;
			this.line39.X2 = 0F;
			this.line39.Y1 = 0.185F;
			this.line39.Y2 = 0.367F;
			// 
			// line56
			// 
			this.line56.Height = 0.1877953F;
			this.line56.Left = 8.483001F;
			this.line56.LineWeight = 1F;
			this.line56.Name = "line56";
			this.line56.Tag = "";
			this.line56.Top = 4.656613E-10F;
			this.line56.Width = 0F;
			this.line56.X1 = 8.483001F;
			this.line56.X2 = 8.483001F;
			this.line56.Y1 = 4.656613E-10F;
			this.line56.Y2 = 0.1877953F;
			// 
			// line89
			// 
			this.line89.Height = 0.1877954F;
			this.line89.Left = 0F;
			this.line89.LineWeight = 1F;
			this.line89.Name = "line89";
			this.line89.Tag = "";
			this.line89.Top = 0.369F;
			this.line89.Width = 0F;
			this.line89.X1 = 0F;
			this.line89.X2 = 0F;
			this.line89.Y1 = 0.369F;
			this.line89.Y2 = 0.5567954F;
			// 
			// line90
			// 
			this.line90.Height = 0.2F;
			this.line90.Left = 0F;
			this.line90.LineWeight = 1F;
			this.line90.Name = "line90";
			this.line90.Tag = "";
			this.line90.Top = 0.551F;
			this.line90.Width = 0F;
			this.line90.X1 = 0F;
			this.line90.X2 = 0F;
			this.line90.Y1 = 0.551F;
			this.line90.Y2 = 0.751F;
			// 
			// line92
			// 
			this.line92.Height = 0F;
			this.line92.Left = 0F;
			this.line92.LineWeight = 1F;
			this.line92.Name = "line92";
			this.line92.Tag = "";
			this.line92.Top = 0.376F;
			this.line92.Width = 11.323F;
			this.line92.X1 = 0F;
			this.line92.X2 = 11.323F;
			this.line92.Y1 = 0.376F;
			this.line92.Y2 = 0.376F;
			// 
			// line93
			// 
			this.line93.Height = 0F;
			this.line93.Left = 0F;
			this.line93.LineWeight = 1F;
			this.line93.Name = "line93";
			this.line93.Tag = "";
			this.line93.Top = 0.564F;
			this.line93.Width = 11.323F;
			this.line93.X1 = 0F;
			this.line93.X2 = 11.323F;
			this.line93.Y1 = 0.564F;
			this.line93.Y2 = 0.564F;
			// 
			// line94
			// 
			this.line94.Height = 0.1877953F;
			this.line94.Left = 0.58F;
			this.line94.LineWeight = 1F;
			this.line94.Name = "line94";
			this.line94.Tag = "";
			this.line94.Top = 0.181F;
			this.line94.Width = 0F;
			this.line94.X1 = 0.58F;
			this.line94.X2 = 0.58F;
			this.line94.Y1 = 0.181F;
			this.line94.Y2 = 0.3687953F;
			// 
			// line95
			// 
			this.line95.Height = 0.1877953F;
			this.line95.Left = 2.757F;
			this.line95.LineWeight = 1F;
			this.line95.Name = "line95";
			this.line95.Tag = "";
			this.line95.Top = 0.181F;
			this.line95.Width = 0F;
			this.line95.X1 = 2.757F;
			this.line95.X2 = 2.757F;
			this.line95.Y1 = 0.181F;
			this.line95.Y2 = 0.3687953F;
			// 
			// line96
			// 
			this.line96.Height = 0.1877953F;
			this.line96.Left = 4.977F;
			this.line96.LineWeight = 1F;
			this.line96.Name = "line96";
			this.line96.Tag = "";
			this.line96.Top = 0.181F;
			this.line96.Width = 0F;
			this.line96.X1 = 4.977F;
			this.line96.X2 = 4.977F;
			this.line96.Y1 = 0.181F;
			this.line96.Y2 = 0.3687953F;
			// 
			// line97
			// 
			this.line97.Height = 0.1877953F;
			this.line97.Left = 6.138079F;
			this.line97.LineWeight = 1F;
			this.line97.Name = "line97";
			this.line97.Tag = "";
			this.line97.Top = 0.181F;
			this.line97.Width = 0F;
			this.line97.X1 = 6.138079F;
			this.line97.X2 = 6.138079F;
			this.line97.Y1 = 0.181F;
			this.line97.Y2 = 0.3687953F;
			// 
			// line98
			// 
			this.line98.Height = 0.1877953F;
			this.line98.Left = 6.950079F;
			this.line98.LineWeight = 1F;
			this.line98.Name = "line98";
			this.line98.Tag = "";
			this.line98.Top = 0.191F;
			this.line98.Width = 0F;
			this.line98.X1 = 6.950079F;
			this.line98.X2 = 6.950079F;
			this.line98.Y1 = 0.191F;
			this.line98.Y2 = 0.3787953F;
			// 
			// line99
			// 
			this.line99.Height = 0.1877953F;
			this.line99.Left = 11.323F;
			this.line99.LineWeight = 1F;
			this.line99.Name = "line99";
			this.line99.Tag = "";
			this.line99.Top = 0.181F;
			this.line99.Width = 0F;
			this.line99.X1 = 11.323F;
			this.line99.X2 = 11.323F;
			this.line99.Y1 = 0.181F;
			this.line99.Y2 = 0.3687953F;
			// 
			// line100
			// 
			this.line100.Height = 0.1877953F;
			this.line100.Left = 10.657F;
			this.line100.LineWeight = 1F;
			this.line100.Name = "line100";
			this.line100.Tag = "";
			this.line100.Top = 0.181F;
			this.line100.Width = 0F;
			this.line100.X1 = 10.657F;
			this.line100.X2 = 10.657F;
			this.line100.Y1 = 0.181F;
			this.line100.Y2 = 0.3687953F;
			// 
			// line101
			// 
			this.line101.Height = 0.1877953F;
			this.line101.Left = 9.757001F;
			this.line101.LineWeight = 1F;
			this.line101.Name = "line101";
			this.line101.Tag = "";
			this.line101.Top = 0.181F;
			this.line101.Width = 0F;
			this.line101.X1 = 9.757001F;
			this.line101.X2 = 9.757001F;
			this.line101.Y1 = 0.181F;
			this.line101.Y2 = 0.3687953F;
			// 
			// line102
			// 
			this.line102.Height = 0.1877953F;
			this.line102.Left = 7.63308F;
			this.line102.LineWeight = 1F;
			this.line102.Name = "line102";
			this.line102.Tag = "";
			this.line102.Top = 0.181F;
			this.line102.Width = 0F;
			this.line102.X1 = 7.63308F;
			this.line102.X2 = 7.63308F;
			this.line102.Y1 = 0.181F;
			this.line102.Y2 = 0.3687953F;
			// 
			// line103
			// 
			this.line103.Height = 0.1877953F;
			this.line103.Left = 8.957001F;
			this.line103.LineWeight = 1F;
			this.line103.Name = "line103";
			this.line103.Tag = "";
			this.line103.Top = 0.191F;
			this.line103.Width = 0F;
			this.line103.X1 = 8.957001F;
			this.line103.X2 = 8.957001F;
			this.line103.Y1 = 0.191F;
			this.line103.Y2 = 0.3787953F;
			// 
			// line104
			// 
			this.line104.Height = 0.1877953F;
			this.line104.Left = 5.479F;
			this.line104.LineWeight = 1F;
			this.line104.Name = "line104";
			this.line104.Tag = "";
			this.line104.Top = 0.181F;
			this.line104.Width = 0F;
			this.line104.X1 = 5.479F;
			this.line104.X2 = 5.479F;
			this.line104.Y1 = 0.181F;
			this.line104.Y2 = 0.3687953F;
			// 
			// line105
			// 
			this.line105.Height = 0.1877953F;
			this.line105.Left = 2.205079F;
			this.line105.LineWeight = 1F;
			this.line105.Name = "line105";
			this.line105.Tag = "";
			this.line105.Top = 0.181F;
			this.line105.Width = 0F;
			this.line105.X1 = 2.205079F;
			this.line105.X2 = 2.205079F;
			this.line105.Y1 = 0.181F;
			this.line105.Y2 = 0.3687953F;
			// 
			// line106
			// 
			this.line106.Height = 0.1877953F;
			this.line106.Left = 1.018188F;
			this.line106.LineWeight = 1F;
			this.line106.Name = "line106";
			this.line106.Tag = "";
			this.line106.Top = 0.181F;
			this.line106.Width = 0F;
			this.line106.X1 = 1.018188F;
			this.line106.X2 = 1.018188F;
			this.line106.Y1 = 0.181F;
			this.line106.Y2 = 0.3687953F;
			// 
			// line108
			// 
			this.line108.Height = 0.1877953F;
			this.line108.Left = 8.483081F;
			this.line108.LineWeight = 1F;
			this.line108.Name = "line108";
			this.line108.Tag = "";
			this.line108.Top = 0.1869055F;
			this.line108.Width = 0F;
			this.line108.X1 = 8.483081F;
			this.line108.X2 = 8.483081F;
			this.line108.Y1 = 0.1869055F;
			this.line108.Y2 = 0.3747008F;
			// 
			// line109
			// 
			this.line109.Height = 0.1877953F;
			this.line109.Left = 0.5798116F;
			this.line109.LineWeight = 1F;
			this.line109.Name = "line109";
			this.line109.Tag = "";
			this.line109.Top = 0.367F;
			this.line109.Width = 0F;
			this.line109.X1 = 0.5798116F;
			this.line109.X2 = 0.5798116F;
			this.line109.Y1 = 0.367F;
			this.line109.Y2 = 0.5547953F;
			// 
			// line110
			// 
			this.line110.Height = 0.1877953F;
			this.line110.Left = 2.757F;
			this.line110.LineWeight = 1F;
			this.line110.Name = "line110";
			this.line110.Tag = "";
			this.line110.Top = 0.367F;
			this.line110.Width = 0F;
			this.line110.X1 = 2.757F;
			this.line110.X2 = 2.757F;
			this.line110.Y1 = 0.367F;
			this.line110.Y2 = 0.5547953F;
			// 
			// line111
			// 
			this.line111.Height = 0.1877954F;
			this.line111.Left = 4.977F;
			this.line111.LineWeight = 1F;
			this.line111.Name = "line111";
			this.line111.Tag = "";
			this.line111.Top = 0.367F;
			this.line111.Width = 0F;
			this.line111.X1 = 4.977F;
			this.line111.X2 = 4.977F;
			this.line111.Y1 = 0.367F;
			this.line111.Y2 = 0.5547954F;
			// 
			// line112
			// 
			this.line112.Height = 0.1877953F;
			this.line112.Left = 6.13789F;
			this.line112.LineWeight = 1F;
			this.line112.Name = "line112";
			this.line112.Tag = "";
			this.line112.Top = 0.367F;
			this.line112.Width = 0F;
			this.line112.X1 = 6.13789F;
			this.line112.X2 = 6.13789F;
			this.line112.Y1 = 0.367F;
			this.line112.Y2 = 0.5547953F;
			// 
			// line113
			// 
			this.line113.Height = 0.1877953F;
			this.line113.Left = 6.949892F;
			this.line113.LineWeight = 1F;
			this.line113.Name = "line113";
			this.line113.Tag = "";
			this.line113.Top = 0.377F;
			this.line113.Width = 0F;
			this.line113.X1 = 6.949892F;
			this.line113.X2 = 6.949892F;
			this.line113.Y1 = 0.377F;
			this.line113.Y2 = 0.5647953F;
			// 
			// line114
			// 
			this.line114.Height = 0.1877953F;
			this.line114.Left = 11.323F;
			this.line114.LineWeight = 1F;
			this.line114.Name = "line114";
			this.line114.Tag = "";
			this.line114.Top = 0.367F;
			this.line114.Width = 0F;
			this.line114.X1 = 11.323F;
			this.line114.X2 = 11.323F;
			this.line114.Y1 = 0.367F;
			this.line114.Y2 = 0.5547953F;
			// 
			// line115
			// 
			this.line115.Height = 0.1877953F;
			this.line115.Left = 10.657F;
			this.line115.LineWeight = 1F;
			this.line115.Name = "line115";
			this.line115.Tag = "";
			this.line115.Top = 0.367F;
			this.line115.Width = 0F;
			this.line115.X1 = 10.657F;
			this.line115.X2 = 10.657F;
			this.line115.Y1 = 0.367F;
			this.line115.Y2 = 0.5547953F;
			// 
			// line116
			// 
			this.line116.Height = 0.1877953F;
			this.line116.Left = 9.757001F;
			this.line116.LineWeight = 1F;
			this.line116.Name = "line116";
			this.line116.Tag = "";
			this.line116.Top = 0.367F;
			this.line116.Width = 0F;
			this.line116.X1 = 9.757001F;
			this.line116.X2 = 9.757001F;
			this.line116.Y1 = 0.367F;
			this.line116.Y2 = 0.5547953F;
			// 
			// line117
			// 
			this.line117.Height = 0.1877953F;
			this.line117.Left = 7.632893F;
			this.line117.LineWeight = 1F;
			this.line117.Name = "line117";
			this.line117.Tag = "";
			this.line117.Top = 0.367F;
			this.line117.Width = 0F;
			this.line117.X1 = 7.632893F;
			this.line117.X2 = 7.632893F;
			this.line117.Y1 = 0.367F;
			this.line117.Y2 = 0.5547953F;
			// 
			// line118
			// 
			this.line118.Height = 0.1877953F;
			this.line118.Left = 8.957001F;
			this.line118.LineWeight = 1F;
			this.line118.Name = "line118";
			this.line118.Tag = "";
			this.line118.Top = 0.367F;
			this.line118.Width = 0F;
			this.line118.X1 = 8.957001F;
			this.line118.X2 = 8.957001F;
			this.line118.Y1 = 0.367F;
			this.line118.Y2 = 0.5547953F;
			// 
			// line119
			// 
			this.line119.Height = 0.1877953F;
			this.line119.Left = 5.478811F;
			this.line119.LineWeight = 1F;
			this.line119.Name = "line119";
			this.line119.Tag = "";
			this.line119.Top = 0.367F;
			this.line119.Width = 0F;
			this.line119.X1 = 5.478811F;
			this.line119.X2 = 5.478811F;
			this.line119.Y1 = 0.367F;
			this.line119.Y2 = 0.5547953F;
			// 
			// line120
			// 
			this.line120.Height = 0.1877953F;
			this.line120.Left = 2.20489F;
			this.line120.LineWeight = 1F;
			this.line120.Name = "line120";
			this.line120.Tag = "";
			this.line120.Top = 0.367F;
			this.line120.Width = 0F;
			this.line120.X1 = 2.20489F;
			this.line120.X2 = 2.20489F;
			this.line120.Y1 = 0.367F;
			this.line120.Y2 = 0.5547953F;
			// 
			// line121
			// 
			this.line121.Height = 0.1877953F;
			this.line121.Left = 1.018F;
			this.line121.LineWeight = 1F;
			this.line121.Name = "line121";
			this.line121.Tag = "";
			this.line121.Top = 0.367F;
			this.line121.Width = 0F;
			this.line121.X1 = 1.018F;
			this.line121.X2 = 1.018F;
			this.line121.Y1 = 0.367F;
			this.line121.Y2 = 0.5547953F;
			// 
			// line123
			// 
			this.line123.Height = 0.1877953F;
			this.line123.Left = 8.482893F;
			this.line123.LineWeight = 1F;
			this.line123.Name = "line123";
			this.line123.Tag = "";
			this.line123.Top = 0.3729055F;
			this.line123.Width = 0F;
			this.line123.X1 = 8.482893F;
			this.line123.X2 = 8.482893F;
			this.line123.Y1 = 0.3729055F;
			this.line123.Y2 = 0.5607008F;
			// 
			// line5
			// 
			this.line5.Height = 0.1899607F;
			this.line5.Left = 0.5801575F;
			this.line5.LineWeight = 1F;
			this.line5.Name = "line5";
			this.line5.Tag = "";
			this.line5.Top = 0.5550393F;
			this.line5.Width = 0F;
			this.line5.X1 = 0.5801575F;
			this.line5.X2 = 0.5801575F;
			this.line5.Y1 = 0.5550393F;
			this.line5.Y2 = 0.745F;
			// 
			// line20
			// 
			this.line20.Height = 0.1899607F;
			this.line20.Left = 2.757F;
			this.line20.LineWeight = 1F;
			this.line20.Name = "line20";
			this.line20.Tag = "";
			this.line20.Top = 0.5550393F;
			this.line20.Width = 0F;
			this.line20.X1 = 2.757F;
			this.line20.X2 = 2.757F;
			this.line20.Y1 = 0.5550393F;
			this.line20.Y2 = 0.745F;
			// 
			// line36
			// 
			this.line36.Height = 0.1899607F;
			this.line36.Left = 4.977F;
			this.line36.LineWeight = 1F;
			this.line36.Name = "line36";
			this.line36.Tag = "";
			this.line36.Top = 0.555F;
			this.line36.Width = 0F;
			this.line36.X1 = 4.977F;
			this.line36.X2 = 4.977F;
			this.line36.Y1 = 0.555F;
			this.line36.Y2 = 0.7449607F;
			// 
			// line53
			// 
			this.line53.Height = 0.194F;
			this.line53.Left = 6.138F;
			this.line53.LineWeight = 1F;
			this.line53.Name = "line53";
			this.line53.Tag = "";
			this.line53.Top = 0.551F;
			this.line53.Width = 0F;
			this.line53.X1 = 6.138F;
			this.line53.X2 = 6.138F;
			this.line53.Y1 = 0.551F;
			this.line53.Y2 = 0.745F;
			// 
			// line107
			// 
			this.line107.Height = 0.1877954F;
			this.line107.Left = 6.950236F;
			this.line107.LineWeight = 1F;
			this.line107.Name = "line107";
			this.line107.Tag = "";
			this.line107.Top = 0.5610393F;
			this.line107.Width = 0F;
			this.line107.X1 = 6.950236F;
			this.line107.X2 = 6.950236F;
			this.line107.Y1 = 0.5610393F;
			this.line107.Y2 = 0.7488347F;
			// 
			// line122
			// 
			this.line122.Height = 0.19F;
			this.line122.Left = 11.323F;
			this.line122.LineWeight = 1F;
			this.line122.Name = "line122";
			this.line122.Tag = "";
			this.line122.Top = 0.555F;
			this.line122.Width = 0F;
			this.line122.X1 = 11.323F;
			this.line122.X2 = 11.323F;
			this.line122.Y1 = 0.555F;
			this.line122.Y2 = 0.745F;
			// 
			// line124
			// 
			this.line124.Height = 0.1877954F;
			this.line124.Left = 10.657F;
			this.line124.LineWeight = 1F;
			this.line124.Name = "line124";
			this.line124.Tag = "";
			this.line124.Top = 0.5570393F;
			this.line124.Width = 0F;
			this.line124.X1 = 10.657F;
			this.line124.X2 = 10.657F;
			this.line124.Y1 = 0.5570393F;
			this.line124.Y2 = 0.7448347F;
			// 
			// line125
			// 
			this.line125.Height = 0.1877954F;
			this.line125.Left = 9.757001F;
			this.line125.LineWeight = 1F;
			this.line125.Name = "line125";
			this.line125.Tag = "";
			this.line125.Top = 0.5570393F;
			this.line125.Width = 0F;
			this.line125.X1 = 9.757001F;
			this.line125.X2 = 9.757001F;
			this.line125.Y1 = 0.5570393F;
			this.line125.Y2 = 0.7448347F;
			// 
			// line126
			// 
			this.line126.Height = 0.1939607F;
			this.line126.Left = 7.633236F;
			this.line126.LineWeight = 1F;
			this.line126.Name = "line126";
			this.line126.Tag = "";
			this.line126.Top = 0.5510393F;
			this.line126.Width = 0F;
			this.line126.X1 = 7.633236F;
			this.line126.X2 = 7.633236F;
			this.line126.Y1 = 0.5510393F;
			this.line126.Y2 = 0.745F;
			// 
			// line127
			// 
			this.line127.Height = 0.1877953F;
			this.line127.Left = 8.957001F;
			this.line127.LineWeight = 1F;
			this.line127.Name = "line127";
			this.line127.Tag = "";
			this.line127.Top = 0.5570394F;
			this.line127.Width = 0F;
			this.line127.X1 = 8.957001F;
			this.line127.X2 = 8.957001F;
			this.line127.Y1 = 0.5570394F;
			this.line127.Y2 = 0.7448347F;
			// 
			// line128
			// 
			this.line128.Height = 0.1899606F;
			this.line128.Left = 5.479157F;
			this.line128.LineWeight = 1F;
			this.line128.Name = "line128";
			this.line128.Tag = "";
			this.line128.Top = 0.5550394F;
			this.line128.Width = 0F;
			this.line128.X1 = 5.479157F;
			this.line128.X2 = 5.479157F;
			this.line128.Y1 = 0.5550394F;
			this.line128.Y2 = 0.745F;
			// 
			// line129
			// 
			this.line129.Height = 0.1899607F;
			this.line129.Left = 2.205236F;
			this.line129.LineWeight = 1F;
			this.line129.Name = "line129";
			this.line129.Tag = "";
			this.line129.Top = 0.5550393F;
			this.line129.Width = 0F;
			this.line129.X1 = 2.205236F;
			this.line129.X2 = 2.205236F;
			this.line129.Y1 = 0.5550393F;
			this.line129.Y2 = 0.745F;
			// 
			// line130
			// 
			this.line130.Height = 0.1899607F;
			this.line130.Left = 1.018346F;
			this.line130.LineWeight = 1F;
			this.line130.Name = "line130";
			this.line130.Tag = "";
			this.line130.Top = 0.5550393F;
			this.line130.Width = 0F;
			this.line130.X1 = 1.018346F;
			this.line130.X2 = 1.018346F;
			this.line130.Y1 = 0.5550393F;
			this.line130.Y2 = 0.745F;
			// 
			// line131
			// 
			this.line131.Height = 0F;
			this.line131.Left = 0.0002360344F;
			this.line131.LineWeight = 1F;
			this.line131.Name = "line131";
			this.line131.Tag = "";
			this.line131.Top = 0.752F;
			this.line131.Width = 11.32276F;
			this.line131.X1 = 0.0002360344F;
			this.line131.X2 = 11.323F;
			this.line131.Y1 = 0.752F;
			this.line131.Y2 = 0.752F;
			// 
			// line132
			// 
			this.line132.Height = 0.1877953F;
			this.line132.Left = 8.483236F;
			this.line132.LineWeight = 1F;
			this.line132.Name = "line132";
			this.line132.Tag = "";
			this.line132.Top = 0.5629448F;
			this.line132.Width = 0F;
			this.line132.X1 = 8.483236F;
			this.line132.X2 = 8.483236F;
			this.line132.Y1 = 0.5629448F;
			this.line132.Y2 = 0.7507401F;
			// 
			// line133
			// 
			this.line133.Height = 0.1949607F;
			this.line133.Left = 0.58F;
			this.line133.LineWeight = 1F;
			this.line133.Name = "line133";
			this.line133.Tag = "";
			this.line133.Top = 0.7450393F;
			this.line133.Width = 0.000236094F;
			this.line133.X1 = 0.5802361F;
			this.line133.X2 = 0.58F;
			this.line133.Y1 = 0.7450393F;
			this.line133.Y2 = 0.94F;
			// 
			// line134
			// 
			this.line134.Height = 0.1999607F;
			this.line134.Left = 2.757F;
			this.line134.LineWeight = 1F;
			this.line134.Name = "line134";
			this.line134.Tag = "";
			this.line134.Top = 0.7400393F;
			this.line134.Width = 0F;
			this.line134.X1 = 2.757F;
			this.line134.X2 = 2.757F;
			this.line134.Y1 = 0.7400393F;
			this.line134.Y2 = 0.94F;
			// 
			// line135
			// 
			this.line135.Height = 0.196F;
			this.line135.Left = 4.977F;
			this.line135.LineWeight = 1F;
			this.line135.Name = "line135";
			this.line135.Tag = "";
			this.line135.Top = 0.744F;
			this.line135.Width = 0F;
			this.line135.X1 = 4.977F;
			this.line135.X2 = 4.977F;
			this.line135.Y1 = 0.744F;
			this.line135.Y2 = 0.94F;
			// 
			// line136
			// 
			this.line136.Height = 0.1999607F;
			this.line136.Left = 6.950236F;
			this.line136.LineWeight = 1F;
			this.line136.Name = "line136";
			this.line136.Tag = "";
			this.line136.Top = 0.7400393F;
			this.line136.Width = 0F;
			this.line136.X1 = 6.950236F;
			this.line136.X2 = 6.950236F;
			this.line136.Y1 = 0.7400393F;
			this.line136.Y2 = 0.94F;
			// 
			// line137
			// 
			this.line137.Height = 0.1999607F;
			this.line137.Left = 9.757001F;
			this.line137.LineWeight = 1F;
			this.line137.Name = "line137";
			this.line137.Tag = "";
			this.line137.Top = 0.7400393F;
			this.line137.Width = 0F;
			this.line137.X1 = 9.757001F;
			this.line137.X2 = 9.757001F;
			this.line137.Y1 = 0.7400393F;
			this.line137.Y2 = 0.94F;
			// 
			// line138
			// 
			this.line138.Height = 0.1999607F;
			this.line138.Left = 7.633236F;
			this.line138.LineWeight = 1F;
			this.line138.Name = "line138";
			this.line138.Tag = "";
			this.line138.Top = 0.7400393F;
			this.line138.Width = 0F;
			this.line138.X1 = 7.633236F;
			this.line138.X2 = 7.633236F;
			this.line138.Y1 = 0.7400393F;
			this.line138.Y2 = 0.94F;
			// 
			// line139
			// 
			this.line139.Height = 0.2002992F;
			this.line139.Left = 8.957001F;
			this.line139.LineWeight = 1F;
			this.line139.Name = "line139";
			this.line139.Tag = "";
			this.line139.Top = 0.7397008F;
			this.line139.Width = 0F;
			this.line139.X1 = 8.957001F;
			this.line139.X2 = 8.957001F;
			this.line139.Y1 = 0.7397008F;
			this.line139.Y2 = 0.94F;
			// 
			// line140
			// 
			this.line140.Height = 0.1999606F;
			this.line140.Left = 5.47916F;
			this.line140.LineWeight = 1F;
			this.line140.Name = "line140";
			this.line140.Tag = "";
			this.line140.Top = 0.7400394F;
			this.line140.Width = 0F;
			this.line140.X1 = 5.47916F;
			this.line140.X2 = 5.47916F;
			this.line140.Y1 = 0.7400394F;
			this.line140.Y2 = 0.94F;
			// 
			// line141
			// 
			this.line141.Height = 0.1999607F;
			this.line141.Left = 1.018424F;
			this.line141.LineWeight = 1F;
			this.line141.Name = "line141";
			this.line141.Tag = "";
			this.line141.Top = 0.7400393F;
			this.line141.Width = 0F;
			this.line141.X1 = 1.018424F;
			this.line141.X2 = 1.018424F;
			this.line141.Y1 = 0.7400393F;
			this.line141.Y2 = 0.94F;
			// 
			// line142
			// 
			this.line142.Height = 0.1999607F;
			this.line142.Left = 2.205315F;
			this.line142.LineWeight = 1F;
			this.line142.Name = "line142";
			this.line142.Tag = "";
			this.line142.Top = 0.7400393F;
			this.line142.Width = 0F;
			this.line142.X1 = 2.205315F;
			this.line142.X2 = 2.205315F;
			this.line142.Y1 = 0.7400393F;
			this.line142.Y2 = 0.94F;
			// 
			// line143
			// 
			this.line143.Height = 0.194F;
			this.line143.Left = 8.48332F;
			this.line143.LineWeight = 1F;
			this.line143.Name = "line143";
			this.line143.Tag = "";
			this.line143.Top = 0.746F;
			this.line143.Width = 0F;
			this.line143.X1 = 8.48332F;
			this.line143.X2 = 8.48332F;
			this.line143.Y1 = 0.746F;
			this.line143.Y2 = 0.94F;
			// 
			// line144
			// 
			this.line144.Height = 0.1999607F;
			this.line144.Left = 6.138044F;
			this.line144.LineWeight = 1F;
			this.line144.Name = "line144";
			this.line144.Tag = "";
			this.line144.Top = 0.7400393F;
			this.line144.Width = 0F;
			this.line144.X1 = 6.138044F;
			this.line144.X2 = 6.138044F;
			this.line144.Y1 = 0.7400393F;
			this.line144.Y2 = 0.94F;
			// 
			// line145
			// 
			this.line145.Height = 0.1999608F;
			this.line145.Left = 11.323F;
			this.line145.LineWeight = 1F;
			this.line145.Name = "line145";
			this.line145.Tag = "";
			this.line145.Top = 0.7400392F;
			this.line145.Width = 0F;
			this.line145.X1 = 11.323F;
			this.line145.X2 = 11.323F;
			this.line145.Y1 = 0.7400392F;
			this.line145.Y2 = 0.94F;
			// 
			// line146
			// 
			this.line146.Height = 0.1999214F;
			this.line146.Left = 10.656F;
			this.line146.LineWeight = 1F;
			this.line146.Name = "line146";
			this.line146.Tag = "";
			this.line146.Top = 0.7400786F;
			this.line146.Width = 0F;
			this.line146.X1 = 10.656F;
			this.line146.X2 = 10.656F;
			this.line146.Y1 = 0.7400786F;
			this.line146.Y2 = 0.94F;
			// 
			// line147
			// 
			this.line147.Height = 0F;
			this.line147.Left = 0F;
			this.line147.LineWeight = 1F;
			this.line147.Name = "line147";
			this.line147.Tag = "";
			this.line147.Top = 0.94F;
			this.line147.Width = 11.323F;
			this.line147.X1 = 0F;
			this.line147.X2 = 11.323F;
			this.line147.Y1 = 0.94F;
			this.line147.Y2 = 0.94F;
			// 
			// line40
			// 
			this.line40.Height = 0.194F;
			this.line40.Left = 0F;
			this.line40.LineWeight = 1F;
			this.line40.Name = "line40";
			this.line40.Tag = "";
			this.line40.Top = 0.751F;
			this.line40.Width = 0F;
			this.line40.X1 = 0F;
			this.line40.X2 = 0F;
			this.line40.Y1 = 0.751F;
			this.line40.Y2 = 0.945F;
			// 
			// KBDR1022R
			// 
			this.MasterReport = false;
			this.PageSettings.DefaultPaperSize = false;
			this.PageSettings.Margins.Bottom = 0.2F;
			this.PageSettings.Margins.Left = 0.1568504F;
			this.PageSettings.Margins.Right = 0.1968504F;
			this.PageSettings.Margins.Top = 0.1968504F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11.69291F;
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
			this.PageSettings.PaperWidth = 8.267716F;
			this.PrintWidth = 11.34381F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.groupHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.groupFooter1);
			this.Sections.Add(this.pageFooter);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
			this.ReportStart += new System.EventHandler(this.KBDR1022R_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト188)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト307)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト316)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト317)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル321)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル322)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル323)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル324)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル325)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル326)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル327)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル328)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル329)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ラベル330)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.judgeNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト253)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.テキスト308)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpPages;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト188;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト307;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト316;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト317;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル321;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル322;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル323;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル324;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル325;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル326;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル327;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル328;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル329;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル330;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト72;
        private GrapeCity.ActiveReports.SectionReportModel.Line detailLine;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線91;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線92;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線93;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線94;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線95;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線96;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線97;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線98;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線99;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線100;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト253;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト308;
        private GrapeCity.ActiveReports.SectionReportModel.Line lastLine;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox judgeNo;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line55;
        private GrapeCity.ActiveReports.SectionReportModel.Line line58;
        private GrapeCity.ActiveReports.SectionReportModel.Line line56;
        private GrapeCity.ActiveReports.SectionReportModel.Line line89;
        private GrapeCity.ActiveReports.SectionReportModel.Line line90;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line70;
        private GrapeCity.ActiveReports.SectionReportModel.Line line71;
        private GrapeCity.ActiveReports.SectionReportModel.Line line72;
        private GrapeCity.ActiveReports.SectionReportModel.Line line73;
        private GrapeCity.ActiveReports.SectionReportModel.Line line74;
        private GrapeCity.ActiveReports.SectionReportModel.Line line75;
        private GrapeCity.ActiveReports.SectionReportModel.Line line76;
        private GrapeCity.ActiveReports.SectionReportModel.Line line77;
        private GrapeCity.ActiveReports.SectionReportModel.Line line78;
        private GrapeCity.ActiveReports.SectionReportModel.Line line79;
        private GrapeCity.ActiveReports.SectionReportModel.Line line80;
        private GrapeCity.ActiveReports.SectionReportModel.Line line82;
        private GrapeCity.ActiveReports.SectionReportModel.Line line84;
        private GrapeCity.ActiveReports.SectionReportModel.Line line87;
        private GrapeCity.ActiveReports.SectionReportModel.Line line88;
        private GrapeCity.ActiveReports.SectionReportModel.Line line91;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.Line line92;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line93;
        private GrapeCity.ActiveReports.SectionReportModel.Line line94;
        private GrapeCity.ActiveReports.SectionReportModel.Line line95;
        private GrapeCity.ActiveReports.SectionReportModel.Line line96;
        private GrapeCity.ActiveReports.SectionReportModel.Line line97;
        private GrapeCity.ActiveReports.SectionReportModel.Line line98;
        private GrapeCity.ActiveReports.SectionReportModel.Line line99;
        private GrapeCity.ActiveReports.SectionReportModel.Line line100;
        private GrapeCity.ActiveReports.SectionReportModel.Line line101;
        private GrapeCity.ActiveReports.SectionReportModel.Line line102;
        private GrapeCity.ActiveReports.SectionReportModel.Line line103;
        private GrapeCity.ActiveReports.SectionReportModel.Line line104;
        private GrapeCity.ActiveReports.SectionReportModel.Line line105;
        private GrapeCity.ActiveReports.SectionReportModel.Line line106;
        private GrapeCity.ActiveReports.SectionReportModel.Line line108;
        private GrapeCity.ActiveReports.SectionReportModel.Line line109;
        private GrapeCity.ActiveReports.SectionReportModel.Line line110;
        private GrapeCity.ActiveReports.SectionReportModel.Line line111;
        private GrapeCity.ActiveReports.SectionReportModel.Line line112;
        private GrapeCity.ActiveReports.SectionReportModel.Line line113;
        private GrapeCity.ActiveReports.SectionReportModel.Line line114;
        private GrapeCity.ActiveReports.SectionReportModel.Line line115;
        private GrapeCity.ActiveReports.SectionReportModel.Line line116;
        private GrapeCity.ActiveReports.SectionReportModel.Line line117;
        private GrapeCity.ActiveReports.SectionReportModel.Line line118;
        private GrapeCity.ActiveReports.SectionReportModel.Line line119;
        private GrapeCity.ActiveReports.SectionReportModel.Line line120;
        private GrapeCity.ActiveReports.SectionReportModel.Line line121;
        private GrapeCity.ActiveReports.SectionReportModel.Line line123;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox53;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line53;
        private GrapeCity.ActiveReports.SectionReportModel.Line line107;
        private GrapeCity.ActiveReports.SectionReportModel.Line line122;
        private GrapeCity.ActiveReports.SectionReportModel.Line line124;
        private GrapeCity.ActiveReports.SectionReportModel.Line line125;
        private GrapeCity.ActiveReports.SectionReportModel.Line line126;
        private GrapeCity.ActiveReports.SectionReportModel.Line line127;
        private GrapeCity.ActiveReports.SectionReportModel.Line line128;
        private GrapeCity.ActiveReports.SectionReportModel.Line line129;
        private GrapeCity.ActiveReports.SectionReportModel.Line line130;
        private GrapeCity.ActiveReports.SectionReportModel.Line line131;
        private GrapeCity.ActiveReports.SectionReportModel.Line line132;
        private GrapeCity.ActiveReports.SectionReportModel.Line line133;
        private GrapeCity.ActiveReports.SectionReportModel.Line line134;
        private GrapeCity.ActiveReports.SectionReportModel.Line line135;
        private GrapeCity.ActiveReports.SectionReportModel.Line line136;
        private GrapeCity.ActiveReports.SectionReportModel.Line line137;
        private GrapeCity.ActiveReports.SectionReportModel.Line line138;
        private GrapeCity.ActiveReports.SectionReportModel.Line line139;
        private GrapeCity.ActiveReports.SectionReportModel.Line line140;
        private GrapeCity.ActiveReports.SectionReportModel.Line line141;
        private GrapeCity.ActiveReports.SectionReportModel.Line line142;
        private GrapeCity.ActiveReports.SectionReportModel.Line line143;
        private GrapeCity.ActiveReports.SectionReportModel.Line line144;
        private GrapeCity.ActiveReports.SectionReportModel.Line line145;
        private GrapeCity.ActiveReports.SectionReportModel.Line line146;
        private GrapeCity.ActiveReports.SectionReportModel.Line line147;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line45;
        private GrapeCity.ActiveReports.SectionReportModel.Line line46;
        private GrapeCity.ActiveReports.SectionReportModel.Line line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line line49;
        private GrapeCity.ActiveReports.SectionReportModel.Line line50;
        private GrapeCity.ActiveReports.SectionReportModel.Line line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line line52;
        private GrapeCity.ActiveReports.SectionReportModel.Line line54;
        private GrapeCity.ActiveReports.SectionReportModel.Line line57;
        private GrapeCity.ActiveReports.SectionReportModel.Line line59;
        private GrapeCity.ActiveReports.SectionReportModel.Line line60;
        private GrapeCity.ActiveReports.SectionReportModel.Line line61;
        private GrapeCity.ActiveReports.SectionReportModel.Line line62;
        private GrapeCity.ActiveReports.SectionReportModel.Line line63;
        private GrapeCity.ActiveReports.SectionReportModel.Line line64;
        private GrapeCity.ActiveReports.SectionReportModel.Line line65;
        private GrapeCity.ActiveReports.SectionReportModel.Line line66;
        private GrapeCity.ActiveReports.SectionReportModel.Line line67;
        private GrapeCity.ActiveReports.SectionReportModel.Line line68;
        private GrapeCity.ActiveReports.SectionReportModel.Line line69;
        private GrapeCity.ActiveReports.SectionReportModel.Line line81;
        private GrapeCity.ActiveReports.SectionReportModel.Line line83;
        private GrapeCity.ActiveReports.SectionReportModel.Line line85;
        private GrapeCity.ActiveReports.SectionReportModel.Line line86;
        private GrapeCity.ActiveReports.SectionReportModel.Line line148;
        private GrapeCity.ActiveReports.SectionReportModel.Line line149;
        private GrapeCity.ActiveReports.SectionReportModel.Line line150;
    }
}
