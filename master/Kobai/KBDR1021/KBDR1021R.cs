﻿using System.Data;
using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr1021
{
    /// <summary>
    /// KBDR1021R の帳票
    /// </summary>
    public partial class KBDR1021R : BaseReport
    {

        public KBDR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {
            //if (Util.ToInt(this.nowRouNo.Text) % 48 == 0 || this.nowRouNo.Text == this.totalRowNo.Text)
            //{
            //    this.detailLine.Visible = false;
            //    this.lastLine.Visible = true;
            //}
            //else
            //{
            //    this.detailLine.Visible = true;
            //    this.lastLine.Visible = false;
            //}
        }

        private void pageFooter_Format(object sender, System.EventArgs e)
        {

        }

        private void reportFooter1_AfterPrint(object sender, System.EventArgs e)
        {
            textBox5.Value = decimal.Parse(Fields["ITEM13"].Value.ToString());
            textBox9.Value = Fields["ITEM14"].Value.ToString();
            textBox6.Value = decimal.Parse(Fields["ITEM15"].Value.ToString());
            textBox7.Value = decimal.Parse(Fields["ITEM16"].Value.ToString());

            textBox10.Value = decimal.Parse(Fields["ITEM17"].Value.ToString());
            textBox13.Value = Fields["ITEM18"].Value.ToString();
            textBox11.Value = decimal.Parse(Fields["ITEM19"].Value.ToString());
            textBox12.Value = decimal.Parse(Fields["ITEM20"].Value.ToString());

            textBox14.Value = decimal.Parse(Fields["ITEM21"].Value.ToString());
            textBox17.Value = Fields["ITEM22"].Value.ToString();
            textBox15.Value = decimal.Parse(Fields["ITEM23"].Value.ToString());
            textBox16.Value = decimal.Parse(Fields["ITEM24"].Value.ToString());

            textBox18.Value = decimal.Parse(Fields["ITEM25"].Value.ToString());
            textBox21.Value = Fields["ITEM26"].Value.ToString();
            textBox19.Value = decimal.Parse(Fields["ITEM27"].Value.ToString());
            textBox20.Value = decimal.Parse(Fields["ITEM28"].Value.ToString());

            textBox26.Value =
                decimal.Parse(textBox5.Value.ToString()) + 
                decimal.Parse(textBox10.Value.ToString()) + 
                decimal.Parse(textBox14.Value.ToString()) + 
                decimal.Parse(textBox18.Value.ToString());

            textBox27.Value =
                decimal.Parse(textBox6.Value.ToString()) +
                decimal.Parse(textBox11.Value.ToString()) +
                decimal.Parse(textBox15.Value.ToString()) +
                decimal.Parse(textBox19.Value.ToString());

            textBox28.Value =
                decimal.Parse(textBox7.Value.ToString()) +
                decimal.Parse(textBox12.Value.ToString()) +
                decimal.Parse(textBox16.Value.ToString()) +
                decimal.Parse(textBox20.Value.ToString());

        }
    }
}
