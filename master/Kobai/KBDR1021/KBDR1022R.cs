﻿using System.Data;
using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr1021
{
    /// <summary>
    /// KBDR1022R の帳票
    /// </summary>
    public partial class KBDR1022R : BaseReport
    {

        public KBDR1022R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {

            if (Util.ToInt(this.judgeNo.Text) == 0)
            {
                this.detailLine.Visible = true;
                this.lastLine.Visible = false;
            }
            else
            {
                this.detailLine.Visible = false;
                this.lastLine.Visible = true;
            }
        }
        private void KBDR1022R_ReportStart(object sender, System.EventArgs e)
        {

        }

        private void pageFooter_Format(object sender, System.EventArgs e)
        {

        }

        private void groupFooter1_Format(object sender, System.EventArgs e)
        {

        }

        private void reportFooter1_AfterPrint(object sender, System.EventArgs e)
        {
            textBox6.Value = decimal.Parse(Fields["ITEM34"].Value.ToString());
            textBox37.Value = Fields["ITEM35"].Value.ToString();
            textBox4.Value = decimal.Parse(Fields["ITEM36"].Value.ToString());
            textBox7.Value = decimal.Parse(Fields["ITEM37"].Value.ToString());

            textBox13.Value = decimal.Parse(Fields["ITEM38"].Value.ToString());
            textBox9.Value = Fields["ITEM39"].Value.ToString();
            textBox12.Value = decimal.Parse(Fields["ITEM40"].Value.ToString());
            textBox11.Value = decimal.Parse(Fields["ITEM41"].Value.ToString());

            textBox55.Value = decimal.Parse(Fields["ITEM42"].Value.ToString());
            textBox46.Value = Fields["ITEM43"].Value.ToString();
            textBox54.Value = decimal.Parse(Fields["ITEM44"].Value.ToString());
            textBox47.Value = decimal.Parse(Fields["ITEM45"].Value.ToString());


            textBox26.Value =
                decimal.Parse(Fields["ITEM34"].Value.ToString()) +
                decimal.Parse(Fields["ITEM38"].Value.ToString()) +
                decimal.Parse(Fields["ITEM42"].Value.ToString());

            textBox24.Value =
                decimal.Parse(Fields["ITEM36"].Value.ToString()) +
                decimal.Parse(Fields["ITEM40"].Value.ToString()) +
                decimal.Parse(Fields["ITEM44"].Value.ToString());

            textBox27.Value =
                decimal.Parse(Fields["ITEM37"].Value.ToString()) +
                decimal.Parse(Fields["ITEM41"].Value.ToString()) +
                decimal.Parse(Fields["ITEM45"].Value.ToString());
        }
    }
}
