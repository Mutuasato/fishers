﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbcm1011
{
    /// <summary>
    /// 仕入マスタ一覧(KBCM1013)
    /// </summary>
    public partial class KBCM1013 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 末日を表す固定値
        /// </summary>
        private const string MATSUJITSU = "99";
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBCM1013()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // ボタンの位置調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;
            // フォーカス設定
            this.txtShiiresakiCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            //仕入先CDに、
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShiiresakiCdFr":
                case "txtShiiresakiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtShiiresakiCdFr":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShiiresakiCdFr.Text = outData[0];
                                this.lblNakagaininCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShiiresakiCdTo":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShiiresakiCdTo.Text = outData[0];
                                this.lblNakagaininCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 仕入先コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiiresakiCdFr())
            {
                e.Cancel = true;
                this.txtShiiresakiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 仕入先コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiiresakiCdTo())
            {
                e.Cancel = true;
                this.txtShiiresakiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 仕入先コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtShiiresakiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 仕入先コードの入力チェック(先頭）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiiresakiCdFr()
        {
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.txtShiiresakiCdFr.Text))
            {
                this.lblNakagaininCdFr.Text = "先　頭";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiiresakiCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiiresakiCdFr.Text);
            this.lblNakagaininCdFr.Text = name;
            return true;
        }

        /// <summary>
        /// 仕入先コードの入力チェック(最後）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiiresakiCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtShiiresakiCdTo.Text))
            {
                this.lblNakagaininCdTo.Text = "最　後";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiiresakiCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", "", this.txtShiiresakiCdTo.Text);
            this.lblNakagaininCdTo.Text = name;
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 仕入先コードのチェック
            if (!IsValidShiiresakiCdFr())
            {
                this.txtShiiresakiCdFr.Focus();
                this.txtShiiresakiCdFr.SelectAll();
                return false;
            }

            // 仕入先コードのチェック
            if (!IsValidShiiresakiCdTo())
            {
                this.txtShiiresakiCdTo.Focus();
                this.txtShiiresakiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    KBCM1011R rpt = new KBCM1011R(dtOutput);
                    if (isPreview)
                    {
                        // 帳票オブジェクトをインスタンス化
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 帳票オブジェクトをインスタンス化
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 仕入先コード設定
            string shiiresakiCdFr;
            string shiiresakiCdTo;
            if (Util.ToDecimal(txtShiiresakiCdFr.Text) > 0)
            {
                shiiresakiCdFr = txtShiiresakiCdFr.Text;
            }
            else
            {
                shiiresakiCdFr = "0";
            }
            if (Util.ToDecimal(txtShiiresakiCdTo.Text) > 0)
            {
                shiiresakiCdTo = txtShiiresakiCdTo.Text;
            }
            else
            {
                shiiresakiCdTo = "9999";
            }
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            string shiiresakiJusho;
            string shohizeiHasuShori;
            string shohizeiTenkaHoho;
            string shohizeiNyuryokuHoho;
            string jigyoKubun;
            string tankaShutokuHoho;
            string kingakuHasuShori;
            string shimebi;
            string kaishubi;
            string kaishutuki;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            //VI_HN_TORIHIKISAKI_JOHOからデータ取得
            sql.Append(" SELECT");
            sql.Append("  *  ");
            sql.Append(" FROM ");
            sql.Append(" VI_HN_TORIHIKISAKI_JOHO ");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" TORIHIKISAKI_KUBUN3 = 3 AND");
            sql.Append(" TORIHIKISAKI_CD BETWEEN @SHIIRESAKI_CD_FR AND @SHIIRESAKI_CD_TO");
            sql.Append(" ORDER BY ");
            sql.Append(" KAISHA_CD, ");
            sql.Append(" TORIHIKISAKI_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHIIRESAKI_CD_FR", SqlDbType.Decimal, 6, shiiresakiCdFr);
            dpc.SetParam("@SHIIRESAKI_CD_TO", SqlDbType.Decimal, 6, shiiresakiCdTo);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("INSERT INTO PR_HN_TBL(");
                    sql.Append("  GUID");
                    sql.Append(" ,SORT");
                    sql.Append(" ,ITEM01");
                    sql.Append(" ,ITEM02");
                    sql.Append(" ,ITEM03");
                    sql.Append(" ,ITEM04");
                    sql.Append(" ,ITEM05");
                    sql.Append(" ,ITEM06");
                    sql.Append(" ,ITEM07");
                    sql.Append(" ,ITEM08");
                    sql.Append(" ,ITEM09");
                    sql.Append(" ,ITEM10");
                    sql.Append(" ,ITEM11");
                    sql.Append(" ,ITEM12");
                    sql.Append(" ,ITEM13");
                    sql.Append(" ,ITEM14");
                    sql.Append(" ,ITEM15");
                    sql.Append(" ,ITEM16");
                    sql.Append(" ,ITEM17");
                    sql.Append(" ,ITEM18");
                    sql.Append(" ,ITEM19");
                    sql.Append(" ,ITEM20");
                    sql.Append(" ,ITEM21");
                    sql.Append(" ,ITEM22");
                    sql.Append(" ,ITEM23");
                    sql.Append(" ,ITEM24");
                    sql.Append(") ");
                    sql.Append("VALUES(");
                    sql.Append("  @GUID");
                    sql.Append(" ,@SORT");
                    sql.Append(" ,@ITEM01");
                    sql.Append(" ,@ITEM02");
                    sql.Append(" ,@ITEM03");
                    sql.Append(" ,@ITEM04");
                    sql.Append(" ,@ITEM05");
                    sql.Append(" ,@ITEM06");
                    sql.Append(" ,@ITEM07");
                    sql.Append(" ,@ITEM08");
                    sql.Append(" ,@ITEM09");
                    sql.Append(" ,@ITEM10");
                    sql.Append(" ,@ITEM11");
                    sql.Append(" ,@ITEM12");
                    sql.Append(" ,@ITEM13");
                    sql.Append(" ,@ITEM14");
                    sql.Append(" ,@ITEM15");
                    sql.Append(" ,@ITEM16");
                    sql.Append(" ,@ITEM17");
                    sql.Append(" ,@ITEM18");
                    sql.Append(" ,@ITEM19");
                    sql.Append(" ,@ITEM20");
                    sql.Append(" ,@ITEM21");
                    sql.Append(" ,@ITEM22");
                    sql.Append(" ,@ITEM23");
                    sql.Append(" ,@ITEM24");
                    sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]); // 仕入先コード
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_KANA_NM"]); // 仕入先カナ名
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_NM"]); // 仕入先名
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YUBIN_BANGO1"]);// 郵便番号１
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YUBIN_BANGO2"]);// 郵便番号２
                    shiiresakiJusho = Util.ToString(dtMainLoop.Rows[i]["JUSHO1"]) + "" + Util.ToString(dtMainLoop.Rows[i]["JUSHO2"]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, shiiresakiJusho); // 住所
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["DAIHYOSHA_NM"]); // 代表者名
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["DENWA_BANGO"]); // 電話番号
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FAX_BANGO"]); // FAX番号
                    shimebi = Util.ToString(dtMainLoop.Rows[i]["SIMEBI"]);
                    if (shimebi.Equals(MATSUJITSU))
                    {
                        shimebi = "末日締め";
                    }
                    else
                    {
                        shimebi += "日締め";
                    }
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, shimebi); // 締日
                    kaishutuki = Util.ToString(dtMainLoop.Rows[i]["KAISHU_TSUKI"]);
                    kaishubi = Util.ToString(dtMainLoop.Rows[i]["KAISHU_BI"]);
                    switch (kaishutuki)
                    {
                        case "0":
                            kaishutuki = "当月";
                            break;
                        case "1":
                            kaishutuki = "翌月";
                            break;
                        case "2":
                            kaishutuki = "翌々月";
                            break;
                        default:
                            kaishutuki += "ヶ月後";
                            break;
                    }
                    if (kaishubi.Equals(MATSUJITSU))
                    {
                        kaishubi = "末日";
                    }
                    else
                    {
                        kaishubi += "日";
                    }
                    kaishubi = kaishutuki + " " + kaishubi;
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, kaishubi); // 　回収日
                    shohizeiHasuShori = Util.ToString(dtMainLoop.Rows[i]["SHOHIZEI_HASU_SHORI"]) + Util.ToString(dtMainLoop.Rows[i]["SHOHIZEI_HASU_SHORI_NM"]);
                  　dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, shohizeiHasuShori); // 消費税端数処理
                    shohizeiTenkaHoho = Util.ToString(dtMainLoop.Rows[i]["SHOHIZEI_TENKA_HOHO"]) + Util.ToString(dtMainLoop.Rows[i]["SHOHIZEI_TENKA_HOHO_NM"]);
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, shohizeiTenkaHoho); // 消費税転嫁方法
                    shohizeiNyuryokuHoho = Util.ToString(dtMainLoop.Rows[i]["SHOHIZEI_NYURYOKU_HOHO"]) + Util.ToString(dtMainLoop.Rows[i]["SHOHIZEI_NYURYOKU_HOHO_NM"]);
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, shohizeiNyuryokuHoho); // 消費税入力方法
                    jigyoKubun = Util.ToString(dtMainLoop.Rows[i]["JIGYO_KUBUN"]) + Util.ToString(dtMainLoop.Rows[i]["JIGYO_KUBUN_NM"]);
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, jigyoKubun); // 事業区分
                    tankaShutokuHoho = Util.ToString(dtMainLoop.Rows[i]["TANKA_SHUTOKU_HOHO"]) + Util.ToString(dtMainLoop.Rows[i]["TANKA_SHUTOKU_HOHO_NM"]);
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, tankaShutokuHoho); // 単価取得
                    kingakuHasuShori = Util.ToString(dtMainLoop.Rows[i]["KINGAKU_HASU_SHORI"]) + Util.ToString(dtMainLoop.Rows[i]["KINGAKU_HASU_SHORI_NM"]);
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, kingakuHasuShori); // 金額端数
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TANTOSHA_NM"]); // 担当者
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_TENPO_CD"]); // 店舗／取引先
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_BUNRUI_CD"]); // 分類／伝票区分
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_KUBUN1"]); // 仕入先区分１
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_KUBUN2"]); // 仕入先区分２
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_KUBUN3"]); // 仕入先区分３
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion

                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}

