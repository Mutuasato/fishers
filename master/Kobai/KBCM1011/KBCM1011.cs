﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbcm1011
{
    /// <summary>
    /// 仕入先の登録(KBCM1011)
    /// </summary>
    public partial class KBCM1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "仕入先の検索";
        #endregion

        #region 変数
        //private bool gridEnterFlag;          // 
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBCM1011()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(748, 634);
                // フォームの配置を上へ移動する
                //this.lblKanaName.Location = new System.Drawing.Point(12, 11);
                //this.txtKanaName.Location = new System.Drawing.Point(96, 13);
                //this.ckboxAllHyoji.Location = new System.Drawing.Point(451, 14);
                //this.dgvList.Location = new System.Drawing.Point(12, 41);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // カナ名にフォーカス
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 
            switch (this.ActiveCtlNm)
            {
                case "txtKanaName":
                case "ckboxAllHyoji":
                    this.btnF1.Enabled = false;
                    break;
                default:
                    this.btnF1.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ仕入先登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 仕入先登録画面の起動
                EditShiireSk(string.Empty);
            }
        }


        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // メンテ機能で立ち上げている場合のみ仕入先マスタ一覧を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 仕入先マスタ一覧の起動
                ItiranShiiresaki();
            }
        }

        public override void PressF9()
        {
            ShowConfig();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 表示ボタンの変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckboxAllHyoji_CheckedChanged(object sender, System.EventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない

            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditShiireSk(Util.ToString(this.dgvList.SelectedRows[0].Cells["仕入先コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditShiireSk(Util.ToString(this.dgvList.SelectedRows[0].Cells["仕入先コード"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 仕入先マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("HN.TORIHIKISAKI_KUBUN3 = 3 AND HN.SHUBETU_KUBUN = 1 ");
            where.Append(" AND HN.KAISHA_CD = @KAISHA_CD");
            if (isInitial)
            {
                // 初期処理の場合　表示フラグが1の得意先は表示しない
                where.Append(" AND HN.HYOJI_FLG = 0");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaName.Text))
                {
                    where.Append(" AND HN.TORIHIKISAKI_KANA_NM LIKE @TORIHIKISAKI_KANA_NM");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@TORIHIKISAKI_KANA_NM", SqlDbType.VarChar, 32, "%" + this.txtKanaName.Text + "%");
                }
                if (this.ckboxAllHyoji.Checked == false)
                {
                    where.Append(" AND HN.HYOJI_FLG = 0");
                }
            }
            string cols = "HN.TORIHIKISAKI_CD AS 仕入先コード";
            cols += ", HN.TORIHIKISAKI_NM AS 仕入先名";
            cols += ", HN.TORIHIKISAKI_KANA_NM AS 仕入先カナ名";
            string from = "VI_HN_TORIHIKISAKI_JOHO AS HN";

            DataTable dtShiire =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "HN.TORIHIKISAKI_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtShiire.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtShiire.Rows.Add(dtShiire.NewRow());
            }

            this.dgvList.DataSource = dtShiire;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 350;
            this.dgvList.Columns[2].Width = 250;
        }

        /// <summary>
        /// 仕入先を追加編集する
        /// </summary>
        /// <param name="code">仕入先コード(空：新規登録、以外：編集)</param>
        private void EditShiireSk(string code)
        {
            KBCM1012 frmKBCM1012;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmKBCM1012 = new KBCM1012("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmKBCM1012 = new KBCM1012("2");
                frmKBCM1012.InData = code;
            }

            DialogResult result = frmKBCM1012.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["仕入先コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 仕入先マスタ一覧画面を起動する
        /// </summary>
        /// <param name="code"></param>
        private void ItiranShiiresaki()
        {
            KBCM1013 frmKBCM1013;
            // 仕入先マスタ一覧画面を起動
            frmKBCM1013 = new KBCM1013();

            DialogResult result = frmKBCM1013.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仕入先コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仕入先名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仕入先カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 初期値設定画面を表示する
        /// </summary>
        private void ShowConfig()
        {
            KBCM1014 frmKBCM1014;

            // 初期値設定画面を起動
            frmKBCM1014 = new KBCM1014();

            DialogResult result = frmKBCM1014.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // フォーカスをセット
                this.ActiveControl.Focus();
            }
        }
        #endregion
    }
}
