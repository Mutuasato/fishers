﻿namespace jp.co.fsi.kb.kbcm1011
{
    partial class KBCM1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txtShiireCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiireCd = new System.Windows.Forms.Label();
			this.txthyoji = new jp.co.fsi.common.controls.FsiTextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtKaishuBi = new jp.co.fsi.common.controls.FsiTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtKaishuTsuki = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKaishuTsuki = new System.Windows.Forms.Label();
			this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
			this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
			this.lblShzHsSrMemo = new System.Windows.Forms.Label();
			this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
			this.lblSkshKskMemo = new System.Windows.Forms.Label();
			this.txtSeikyushoKeishiki = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeikyushoKeishiki = new System.Windows.Forms.Label();
			this.lblSkshHkMemo = new System.Windows.Forms.Label();
			this.txtSeikyushoHakko = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeikyushoHakko = new System.Windows.Forms.Label();
			this.lblShimebiMemo = new System.Windows.Forms.Label();
			this.txtShimebi = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShimebi = new System.Windows.Forms.Label();
			this.lblSeikyusakiNm = new System.Windows.Forms.Label();
			this.txtSeikyusakiCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSeikyusakiCd = new System.Windows.Forms.Label();
			this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
			this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
			this.txtKingakuHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKingakuHasuShori = new System.Windows.Forms.Label();
			this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
			this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
			this.lblTantoshaNm = new System.Windows.Forms.Label();
			this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTantoshaCd = new System.Windows.Forms.Label();
			this.txtFaxBango = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFaxBango = new System.Windows.Forms.Label();
			this.txtDenwaBango = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenwaBango = new System.Windows.Forms.Label();
			this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblJusho2 = new System.Windows.Forms.Label();
			this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblJusho1 = new System.Windows.Forms.Label();
			this.txtYubinBango2 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblYubinBangoHyphen = new System.Windows.Forms.Label();
			this.txtYubinBango1 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblYubinBango = new System.Windows.Forms.Label();
			this.txtShiireKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiireKanaNm = new System.Windows.Forms.Label();
			this.txtShiireNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel12 = new jp.co.fsi.common.FsiPanel();
			this.label6 = new System.Windows.Forms.Label();
			this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel12.SuspendLayout();
			this.fsiPanel11.SuspendLayout();
			this.fsiPanel10.SuspendLayout();
			this.fsiPanel9.SuspendLayout();
			this.fsiPanel8.SuspendLayout();
			this.fsiPanel7.SuspendLayout();
			this.fsiPanel6.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 497);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1100, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1089, 31);
			this.lblTitle.Text = "仕入先の登録";
			// 
			// txtShiireCd
			// 
			this.txtShiireCd.AutoSizeFromLength = true;
			this.txtShiireCd.DisplayLength = null;
			this.txtShiireCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShiireCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShiireCd.Location = new System.Drawing.Point(120, 2);
			this.txtShiireCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiireCd.MaxLength = 4;
			this.txtShiireCd.MinimumSize = new System.Drawing.Size(4, 23);
			this.txtShiireCd.Name = "txtShiireCd";
			this.txtShiireCd.Size = new System.Drawing.Size(44, 23);
			this.txtShiireCd.TabIndex = 1;
			this.txtShiireCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiireCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireCd_Validating);
			// 
			// lblShiireCd
			// 
			this.lblShiireCd.BackColor = System.Drawing.Color.Silver;
			this.lblShiireCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShiireCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShiireCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiireCd.Location = new System.Drawing.Point(0, 0);
			this.lblShiireCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiireCd.Name = "lblShiireCd";
			this.lblShiireCd.Size = new System.Drawing.Size(1062, 29);
			this.lblShiireCd.TabIndex = 1;
			this.lblShiireCd.Tag = "CHANGE";
			this.lblShiireCd.Text = "仕入先コード";
			this.lblShiireCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txthyoji
			// 
			this.txthyoji.AutoSizeFromLength = false;
			this.txthyoji.BackColor = System.Drawing.Color.White;
			this.txthyoji.DisplayLength = null;
			this.txthyoji.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txthyoji.Location = new System.Drawing.Point(646, 2);
			this.txthyoji.Margin = new System.Windows.Forms.Padding(4);
			this.txthyoji.MaxLength = 1;
			this.txthyoji.Name = "txthyoji";
			this.txthyoji.Size = new System.Drawing.Size(25, 23);
			this.txthyoji.TabIndex = 22;
			this.txthyoji.Text = "0";
			this.txthyoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txthyoji.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txthyoji_KeyDown);
			this.txthyoji.Validating += new System.ComponentModel.CancelEventHandler(this.txthyoji_Validating);
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label4.Location = new System.Drawing.Point(675, 2);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(331, 24);
			this.label4.TabIndex = 975;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "0:表示する 1:表示しない ";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.BackColor = System.Drawing.Color.Silver;
			this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label5.Location = new System.Drawing.Point(519, 6);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 974;
			this.label5.Tag = "CHANGE";
			this.label5.Text = "一覧表示";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(1037, -2);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(27, 33);
			this.label2.TabIndex = 50;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "日";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKaishuBi
			// 
			this.txtKaishuBi.AutoSizeFromLength = true;
			this.txtKaishuBi.DisplayLength = null;
			this.txtKaishuBi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKaishuBi.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtKaishuBi.Location = new System.Drawing.Point(1009, 2);
			this.txtKaishuBi.Margin = new System.Windows.Forms.Padding(4);
			this.txtKaishuBi.MaxLength = 2;
			this.txtKaishuBi.Name = "txtKaishuBi";
			this.txtKaishuBi.Size = new System.Drawing.Size(25, 23);
			this.txtKaishuBi.TabIndex = 17;
			this.txtKaishuBi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKaishuBi.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuBi_Validating);
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(675, 2);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(331, 24);
			this.label1.TabIndex = 48;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "0:当月 1:翌月 2:翌々月 …";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKaishuTsuki
			// 
			this.txtKaishuTsuki.AutoSizeFromLength = true;
			this.txtKaishuTsuki.DisplayLength = null;
			this.txtKaishuTsuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKaishuTsuki.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtKaishuTsuki.Location = new System.Drawing.Point(646, 3);
			this.txtKaishuTsuki.Margin = new System.Windows.Forms.Padding(4);
			this.txtKaishuTsuki.MaxLength = 2;
			this.txtKaishuTsuki.Name = "txtKaishuTsuki";
			this.txtKaishuTsuki.Size = new System.Drawing.Size(25, 23);
			this.txtKaishuTsuki.TabIndex = 21;
			this.txtKaishuTsuki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKaishuTsuki.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuTsuki_Validating);
			// 
			// lblKaishuTsuki
			// 
			this.lblKaishuTsuki.AutoSize = true;
			this.lblKaishuTsuki.BackColor = System.Drawing.Color.Silver;
			this.lblKaishuTsuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKaishuTsuki.Location = new System.Drawing.Point(519, 7);
			this.lblKaishuTsuki.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKaishuTsuki.Name = "lblKaishuTsuki";
			this.lblKaishuTsuki.Size = new System.Drawing.Size(56, 16);
			this.lblKaishuTsuki.TabIndex = 46;
			this.lblKaishuTsuki.Tag = "CHANGE";
			this.lblKaishuTsuki.Text = "回収日";
			this.lblKaishuTsuki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShzTnkHohoMemo
			// 
			this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzTnkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(675, 2);
			this.lblShzTnkHohoMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
			this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(331, 24);
			this.lblShzTnkHohoMemo.TabIndex = 45;
			this.lblShzTnkHohoMemo.Tag = "CHANGE";
			this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
			this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiTenkaHoho
			// 
			this.txtShohizeiTenkaHoho.AutoSizeFromLength = true;
			this.txtShohizeiTenkaHoho.DisplayLength = 2;
			this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiTenkaHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(646, 2);
			this.txtShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiTenkaHoho.MaxLength = 1;
			this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
			this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(25, 23);
			this.txtShohizeiTenkaHoho.TabIndex = 20;
			this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
			// 
			// lblShohizeiTenkaHoho
			// 
			this.lblShohizeiTenkaHoho.AutoSize = true;
			this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(519, 7);
			this.lblShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
			this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(120, 16);
			this.lblShohizeiTenkaHoho.TabIndex = 43;
			this.lblShohizeiTenkaHoho.Tag = "CHANGE";
			this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
			this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShzHsSrMemo
			// 
			this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzHsSrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzHsSrMemo.Location = new System.Drawing.Point(675, 2);
			this.lblShzHsSrMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
			this.lblShzHsSrMemo.Size = new System.Drawing.Size(331, 24);
			this.lblShzHsSrMemo.TabIndex = 42;
			this.lblShzHsSrMemo.Tag = "CHANGE";
			this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
			this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiHasuShori
			// 
			this.txtShohizeiHasuShori.AutoSizeFromLength = true;
			this.txtShohizeiHasuShori.DisplayLength = 2;
			this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShohizeiHasuShori.Location = new System.Drawing.Point(646, 2);
			this.txtShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiHasuShori.MaxLength = 1;
			this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
			this.txtShohizeiHasuShori.Size = new System.Drawing.Size(25, 23);
			this.txtShohizeiHasuShori.TabIndex = 19;
			this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
			// 
			// lblShohizeiHasuShori
			// 
			this.lblShohizeiHasuShori.AutoSize = true;
			this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiHasuShori.Location = new System.Drawing.Point(519, 6);
			this.lblShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
			this.lblShohizeiHasuShori.Size = new System.Drawing.Size(120, 16);
			this.lblShohizeiHasuShori.TabIndex = 40;
			this.lblShohizeiHasuShori.Tag = "CHANGE";
			this.lblShohizeiHasuShori.Text = "消費税端数処理";
			this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSkshKskMemo
			// 
			this.lblSkshKskMemo.BackColor = System.Drawing.Color.Silver;
			this.lblSkshKskMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSkshKskMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSkshKskMemo.Location = new System.Drawing.Point(675, 2);
			this.lblSkshKskMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSkshKskMemo.Name = "lblSkshKskMemo";
			this.lblSkshKskMemo.Size = new System.Drawing.Size(331, 24);
			this.lblSkshKskMemo.TabIndex = 39;
			this.lblSkshKskMemo.Tag = "CHANGE";
			this.lblSkshKskMemo.Text = "1:合計型 2:明細型";
			this.lblSkshKskMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeikyushoKeishiki
			// 
			this.txtSeikyushoKeishiki.AutoSizeFromLength = true;
			this.txtSeikyushoKeishiki.DisplayLength = 2;
			this.txtSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeikyushoKeishiki.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSeikyushoKeishiki.Location = new System.Drawing.Point(646, 2);
			this.txtSeikyushoKeishiki.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeikyushoKeishiki.MaxLength = 1;
			this.txtSeikyushoKeishiki.Name = "txtSeikyushoKeishiki";
			this.txtSeikyushoKeishiki.Size = new System.Drawing.Size(25, 23);
			this.txtSeikyushoKeishiki.TabIndex = 18;
			this.txtSeikyushoKeishiki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblSeikyushoKeishiki
			// 
			this.lblSeikyushoKeishiki.AutoSize = true;
			this.lblSeikyushoKeishiki.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyushoKeishiki.Location = new System.Drawing.Point(519, 5);
			this.lblSeikyushoKeishiki.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoKeishiki.Name = "lblSeikyushoKeishiki";
			this.lblSeikyushoKeishiki.Size = new System.Drawing.Size(88, 16);
			this.lblSeikyushoKeishiki.TabIndex = 37;
			this.lblSeikyushoKeishiki.Tag = "CHANGE";
			this.lblSeikyushoKeishiki.Text = "請求書形式";
			this.lblSeikyushoKeishiki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSkshHkMemo
			// 
			this.lblSkshHkMemo.BackColor = System.Drawing.Color.Silver;
			this.lblSkshHkMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSkshHkMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSkshHkMemo.Location = new System.Drawing.Point(675, 2);
			this.lblSkshHkMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSkshHkMemo.Name = "lblSkshHkMemo";
			this.lblSkshHkMemo.Size = new System.Drawing.Size(331, 24);
			this.lblSkshHkMemo.TabIndex = 36;
			this.lblSkshHkMemo.Tag = "CHANGE";
			this.lblSkshHkMemo.Text = "1:する 2:しない";
			this.lblSkshHkMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeikyushoHakko
			// 
			this.txtSeikyushoHakko.AutoSizeFromLength = true;
			this.txtSeikyushoHakko.DisplayLength = 2;
			this.txtSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeikyushoHakko.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSeikyushoHakko.Location = new System.Drawing.Point(646, 3);
			this.txtSeikyushoHakko.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeikyushoHakko.MaxLength = 1;
			this.txtSeikyushoHakko.Name = "txtSeikyushoHakko";
			this.txtSeikyushoHakko.Size = new System.Drawing.Size(25, 23);
			this.txtSeikyushoHakko.TabIndex = 16;
			this.txtSeikyushoHakko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblSeikyushoHakko
			// 
			this.lblSeikyushoHakko.AutoSize = true;
			this.lblSeikyushoHakko.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyushoHakko.Location = new System.Drawing.Point(519, 5);
			this.lblSeikyushoHakko.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyushoHakko.Name = "lblSeikyushoHakko";
			this.lblSeikyushoHakko.Size = new System.Drawing.Size(88, 16);
			this.lblSeikyushoHakko.TabIndex = 34;
			this.lblSeikyushoHakko.Tag = "CHANGE";
			this.lblSeikyushoHakko.Text = "請求書発行";
			this.lblSeikyushoHakko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShimebiMemo
			// 
			this.lblShimebiMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShimebiMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShimebiMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShimebiMemo.Location = new System.Drawing.Point(675, 2);
			this.lblShimebiMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShimebiMemo.Name = "lblShimebiMemo";
			this.lblShimebiMemo.Size = new System.Drawing.Size(331, 24);
			this.lblShimebiMemo.TabIndex = 33;
			this.lblShimebiMemo.Tag = "CHANGE";
			this.lblShimebiMemo.Text = "1～28 ※末締めは99";
			this.lblShimebiMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShimebi
			// 
			this.txtShimebi.AutoSizeFromLength = true;
			this.txtShimebi.DisplayLength = null;
			this.txtShimebi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShimebi.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShimebi.Location = new System.Drawing.Point(646, 3);
			this.txtShimebi.Margin = new System.Windows.Forms.Padding(4);
			this.txtShimebi.MaxLength = 2;
			this.txtShimebi.Name = "txtShimebi";
			this.txtShimebi.Size = new System.Drawing.Size(25, 23);
			this.txtShimebi.TabIndex = 15;
			this.txtShimebi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShimebi.Validating += new System.ComponentModel.CancelEventHandler(this.txtShimebi_Validating);
			// 
			// lblShimebi
			// 
			this.lblShimebi.AutoSize = true;
			this.lblShimebi.BackColor = System.Drawing.Color.Silver;
			this.lblShimebi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShimebi.Location = new System.Drawing.Point(519, 6);
			this.lblShimebi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShimebi.Name = "lblShimebi";
			this.lblShimebi.Size = new System.Drawing.Size(40, 16);
			this.lblShimebi.TabIndex = 31;
			this.lblShimebi.Tag = "CHANGE";
			this.lblShimebi.Text = "締日";
			this.lblShimebi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSeikyusakiNm
			// 
			this.lblSeikyusakiNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblSeikyusakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeikyusakiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyusakiNm.Location = new System.Drawing.Point(692, 1);
			this.lblSeikyusakiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyusakiNm.Name = "lblSeikyusakiNm";
			this.lblSeikyusakiNm.Size = new System.Drawing.Size(312, 24);
			this.lblSeikyusakiNm.TabIndex = 30;
			this.lblSeikyusakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSeikyusakiCd
			// 
			this.txtSeikyusakiCd.AutoSizeFromLength = true;
			this.txtSeikyusakiCd.DisplayLength = null;
			this.txtSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSeikyusakiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSeikyusakiCd.Location = new System.Drawing.Point(646, 2);
			this.txtSeikyusakiCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtSeikyusakiCd.MaxLength = 4;
			this.txtSeikyusakiCd.Name = "txtSeikyusakiCd";
			this.txtSeikyusakiCd.Size = new System.Drawing.Size(44, 23);
			this.txtSeikyusakiCd.TabIndex = 3;
			this.txtSeikyusakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblSeikyusakiCd
			// 
			this.lblSeikyusakiCd.AutoSize = true;
			this.lblSeikyusakiCd.BackColor = System.Drawing.Color.Silver;
			this.lblSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSeikyusakiCd.Location = new System.Drawing.Point(519, 5);
			this.lblSeikyusakiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSeikyusakiCd.Name = "lblSeikyusakiCd";
			this.lblSeikyusakiCd.Size = new System.Drawing.Size(104, 16);
			this.lblSeikyusakiCd.TabIndex = 28;
			this.lblSeikyusakiCd.Tag = "CHANGE";
			this.lblSeikyusakiCd.Text = "支払先コード";
			this.lblSeikyusakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShzNrkHohoNm
			// 
			this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblShzNrkHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzNrkHohoNm.Location = new System.Drawing.Point(167, 3);
			this.lblShzNrkHohoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
			this.lblShzNrkHohoNm.Size = new System.Drawing.Size(331, 24);
			this.lblShzNrkHohoNm.TabIndex = 27;
			this.lblShzNrkHohoNm.Tag = "DISPNAME";
			this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiNyuryokuHoho
			// 
			this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = true;
			this.txtShohizeiNyuryokuHoho.DisplayLength = 2;
			this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiNyuryokuHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(120, 3);
			this.txtShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiNyuryokuHoho.MaxLength = 1;
			this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
			this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(25, 23);
			this.txtShohizeiNyuryokuHoho.TabIndex = 14;
			this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiNyuryokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiNyuryokuHoho_Validating);
			// 
			// lblKgkHsShrMemo
			// 
			this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblKgkHsShrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKgkHsShrMemo.Location = new System.Drawing.Point(167, 2);
			this.lblKgkHsShrMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
			this.lblKgkHsShrMemo.Size = new System.Drawing.Size(331, 24);
			this.lblKgkHsShrMemo.TabIndex = 24;
			this.lblKgkHsShrMemo.Tag = "CHANGE";
			this.lblKgkHsShrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
			this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKingakuHasuShori
			// 
			this.txtKingakuHasuShori.AutoSizeFromLength = true;
			this.txtKingakuHasuShori.DisplayLength = 2;
			this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKingakuHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtKingakuHasuShori.Location = new System.Drawing.Point(120, 2);
			this.txtKingakuHasuShori.Margin = new System.Windows.Forms.Padding(4);
			this.txtKingakuHasuShori.MaxLength = 1;
			this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
			this.txtKingakuHasuShori.Size = new System.Drawing.Size(25, 23);
			this.txtKingakuHasuShori.TabIndex = 13;
			this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
			// 
			// lblKingakuHasuShori
			// 
			this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblKingakuHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKingakuHasuShori.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKingakuHasuShori.Location = new System.Drawing.Point(0, 0);
			this.lblKingakuHasuShori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
			this.lblKingakuHasuShori.Size = new System.Drawing.Size(1062, 29);
			this.lblKingakuHasuShori.TabIndex = 22;
			this.lblKingakuHasuShori.Tag = "CHANGE";
			this.lblKingakuHasuShori.Text = "金額端数処理";
			this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTnkStkHohoMemo
			// 
			this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblTnkStkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(167, 2);
			this.lblTnkStkHohoMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
			this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(331, 24);
			this.lblTnkStkHohoMemo.TabIndex = 21;
			this.lblTnkStkHohoMemo.Tag = "CHANGE";
			this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
			this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTankaShutokuHoho
			// 
			this.txtTankaShutokuHoho.AutoSizeFromLength = true;
			this.txtTankaShutokuHoho.DisplayLength = 2;
			this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTankaShutokuHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTankaShutokuHoho.Location = new System.Drawing.Point(120, 3);
			this.txtTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtTankaShutokuHoho.MaxLength = 1;
			this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
			this.txtTankaShutokuHoho.Size = new System.Drawing.Size(25, 23);
			this.txtTankaShutokuHoho.TabIndex = 12;
			this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
			// 
			// lblTankaShutokuHoho
			// 
			this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblTankaShutokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTankaShutokuHoho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTankaShutokuHoho.Location = new System.Drawing.Point(0, 0);
			this.lblTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
			this.lblTankaShutokuHoho.Size = new System.Drawing.Size(1062, 29);
			this.lblTankaShutokuHoho.TabIndex = 19;
			this.lblTankaShutokuHoho.Tag = "CHANGE";
			this.lblTankaShutokuHoho.Text = "単価取得方法";
			this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTantoshaNm
			// 
			this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
			this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoshaNm.Location = new System.Drawing.Point(167, 2);
			this.lblTantoshaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaNm.Name = "lblTantoshaNm";
			this.lblTantoshaNm.Size = new System.Drawing.Size(331, 24);
			this.lblTantoshaNm.TabIndex = 18;
			this.lblTantoshaNm.Tag = "DISPNAME";
			this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTantoshaCd
			// 
			this.txtTantoshaCd.AutoSizeFromLength = true;
			this.txtTantoshaCd.DisplayLength = null;
			this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTantoshaCd.Location = new System.Drawing.Point(120, 2);
			this.txtTantoshaCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtTantoshaCd.MaxLength = 4;
			this.txtTantoshaCd.Name = "txtTantoshaCd";
			this.txtTantoshaCd.Size = new System.Drawing.Size(44, 23);
			this.txtTantoshaCd.TabIndex = 11;
			this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
			// 
			// lblTantoshaCd
			// 
			this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
			this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoshaCd.Location = new System.Drawing.Point(0, 0);
			this.lblTantoshaCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaCd.Name = "lblTantoshaCd";
			this.lblTantoshaCd.Size = new System.Drawing.Size(1062, 29);
			this.lblTantoshaCd.TabIndex = 16;
			this.lblTantoshaCd.Tag = "CHANGE";
			this.lblTantoshaCd.Text = "担当者コード";
			this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFaxBango
			// 
			this.txtFaxBango.AutoSizeFromLength = true;
			this.txtFaxBango.DisplayLength = null;
			this.txtFaxBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFaxBango.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtFaxBango.Location = new System.Drawing.Point(120, 2);
			this.txtFaxBango.Margin = new System.Windows.Forms.Padding(4);
			this.txtFaxBango.MaxLength = 15;
			this.txtFaxBango.Name = "txtFaxBango";
			this.txtFaxBango.Size = new System.Drawing.Size(147, 23);
			this.txtFaxBango.TabIndex = 10;
			this.txtFaxBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtFaxBango_Validating);
			// 
			// lblFaxBango
			// 
			this.lblFaxBango.BackColor = System.Drawing.Color.Silver;
			this.lblFaxBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFaxBango.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFaxBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFaxBango.Location = new System.Drawing.Point(0, 0);
			this.lblFaxBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFaxBango.Name = "lblFaxBango";
			this.lblFaxBango.Size = new System.Drawing.Size(1062, 29);
			this.lblFaxBango.TabIndex = 14;
			this.lblFaxBango.Tag = "CHANGE";
			this.lblFaxBango.Text = "ＦＡＸ番号";
			this.lblFaxBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDenwaBango
			// 
			this.txtDenwaBango.AutoSizeFromLength = true;
			this.txtDenwaBango.DisplayLength = null;
			this.txtDenwaBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDenwaBango.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDenwaBango.Location = new System.Drawing.Point(120, 3);
			this.txtDenwaBango.Margin = new System.Windows.Forms.Padding(4);
			this.txtDenwaBango.MaxLength = 15;
			this.txtDenwaBango.Name = "txtDenwaBango";
			this.txtDenwaBango.Size = new System.Drawing.Size(147, 23);
			this.txtDenwaBango.TabIndex = 9;
			this.txtDenwaBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenwaBango_Validating);
			// 
			// lblDenwaBango
			// 
			this.lblDenwaBango.BackColor = System.Drawing.Color.Silver;
			this.lblDenwaBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDenwaBango.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDenwaBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenwaBango.Location = new System.Drawing.Point(0, 0);
			this.lblDenwaBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenwaBango.Name = "lblDenwaBango";
			this.lblDenwaBango.Size = new System.Drawing.Size(1062, 29);
			this.lblDenwaBango.TabIndex = 12;
			this.lblDenwaBango.Tag = "CHANGE";
			this.lblDenwaBango.Text = "電話番号";
			this.lblDenwaBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtJusho2
			// 
			this.txtJusho2.AutoSizeFromLength = false;
			this.txtJusho2.DisplayLength = null;
			this.txtJusho2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtJusho2.Location = new System.Drawing.Point(120, 2);
			this.txtJusho2.Margin = new System.Windows.Forms.Padding(4);
			this.txtJusho2.MaxLength = 30;
			this.txtJusho2.Name = "txtJusho2";
			this.txtJusho2.Size = new System.Drawing.Size(357, 23);
			this.txtJusho2.TabIndex = 8;
			this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho2_Validating);
			// 
			// lblJusho2
			// 
			this.lblJusho2.BackColor = System.Drawing.Color.Silver;
			this.lblJusho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJusho2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJusho2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJusho2.Location = new System.Drawing.Point(0, 0);
			this.lblJusho2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJusho2.Name = "lblJusho2";
			this.lblJusho2.Size = new System.Drawing.Size(1062, 29);
			this.lblJusho2.TabIndex = 10;
			this.lblJusho2.Tag = "CHANGE";
			this.lblJusho2.Text = "住所２";
			this.lblJusho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtJusho1
			// 
			this.txtJusho1.AutoSizeFromLength = false;
			this.txtJusho1.DisplayLength = null;
			this.txtJusho1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtJusho1.Location = new System.Drawing.Point(120, 2);
			this.txtJusho1.Margin = new System.Windows.Forms.Padding(4);
			this.txtJusho1.MaxLength = 30;
			this.txtJusho1.Name = "txtJusho1";
			this.txtJusho1.Size = new System.Drawing.Size(357, 23);
			this.txtJusho1.TabIndex = 7;
			this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho1_Validating);
			// 
			// lblJusho1
			// 
			this.lblJusho1.BackColor = System.Drawing.Color.Silver;
			this.lblJusho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJusho1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJusho1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJusho1.Location = new System.Drawing.Point(0, 0);
			this.lblJusho1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJusho1.Name = "lblJusho1";
			this.lblJusho1.Size = new System.Drawing.Size(1062, 29);
			this.lblJusho1.TabIndex = 8;
			this.lblJusho1.Tag = "CHANGE";
			this.lblJusho1.Text = "住所１";
			this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtYubinBango2
			// 
			this.txtYubinBango2.AutoSizeFromLength = true;
			this.txtYubinBango2.DisplayLength = null;
			this.txtYubinBango2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYubinBango2.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtYubinBango2.Location = new System.Drawing.Point(172, 2);
			this.txtYubinBango2.Margin = new System.Windows.Forms.Padding(4);
			this.txtYubinBango2.MaxLength = 4;
			this.txtYubinBango2.Name = "txtYubinBango2";
			this.txtYubinBango2.Size = new System.Drawing.Size(44, 23);
			this.txtYubinBango2.TabIndex = 6;
			this.txtYubinBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango2_Validating);
			// 
			// lblYubinBangoHyphen
			// 
			this.lblYubinBangoHyphen.BackColor = System.Drawing.Color.Silver;
			this.lblYubinBangoHyphen.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYubinBangoHyphen.Location = new System.Drawing.Point(156, -2);
			this.lblYubinBangoHyphen.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYubinBangoHyphen.Name = "lblYubinBangoHyphen";
			this.lblYubinBangoHyphen.Size = new System.Drawing.Size(15, 33);
			this.lblYubinBangoHyphen.TabIndex = 6;
			this.lblYubinBangoHyphen.Tag = "CHANGE";
			this.lblYubinBangoHyphen.Text = "-";
			this.lblYubinBangoHyphen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtYubinBango1
			// 
			this.txtYubinBango1.AutoSizeFromLength = true;
			this.txtYubinBango1.DisplayLength = null;
			this.txtYubinBango1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYubinBango1.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtYubinBango1.Location = new System.Drawing.Point(120, 2);
			this.txtYubinBango1.Margin = new System.Windows.Forms.Padding(4);
			this.txtYubinBango1.MaxLength = 3;
			this.txtYubinBango1.Name = "txtYubinBango1";
			this.txtYubinBango1.Size = new System.Drawing.Size(35, 23);
			this.txtYubinBango1.TabIndex = 5;
			this.txtYubinBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango1_Validating);
			// 
			// lblYubinBango
			// 
			this.lblYubinBango.BackColor = System.Drawing.Color.Silver;
			this.lblYubinBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblYubinBango.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblYubinBango.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYubinBango.Location = new System.Drawing.Point(0, 0);
			this.lblYubinBango.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYubinBango.Name = "lblYubinBango";
			this.lblYubinBango.Size = new System.Drawing.Size(1062, 29);
			this.lblYubinBango.TabIndex = 4;
			this.lblYubinBango.Tag = "CHANGE";
			this.lblYubinBango.Text = "郵便番号";
			this.lblYubinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiireKanaNm
			// 
			this.txtShiireKanaNm.AutoSizeFromLength = false;
			this.txtShiireKanaNm.DisplayLength = null;
			this.txtShiireKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiireKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtShiireKanaNm.Location = new System.Drawing.Point(120, 2);
			this.txtShiireKanaNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiireKanaNm.MaxLength = 30;
			this.txtShiireKanaNm.Name = "txtShiireKanaNm";
			this.txtShiireKanaNm.Size = new System.Drawing.Size(357, 23);
			this.txtShiireKanaNm.TabIndex = 4;
			this.txtShiireKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireKanaNm_Validating);
			// 
			// lblShiireKanaNm
			// 
			this.lblShiireKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblShiireKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShiireKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShiireKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiireKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblShiireKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiireKanaNm.Name = "lblShiireKanaNm";
			this.lblShiireKanaNm.Size = new System.Drawing.Size(1062, 29);
			this.lblShiireKanaNm.TabIndex = 2;
			this.lblShiireKanaNm.Tag = "CHANGE";
			this.lblShiireKanaNm.Text = "仕入先カナ名";
			this.lblShiireKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiireNm
			// 
			this.txtShiireNm.AutoSizeFromLength = false;
			this.txtShiireNm.DisplayLength = null;
			this.txtShiireNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiireNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtShiireNm.Location = new System.Drawing.Point(120, 2);
			this.txtShiireNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiireNm.MaxLength = 40;
			this.txtShiireNm.Name = "txtShiireNm";
			this.txtShiireNm.Size = new System.Drawing.Size(357, 23);
			this.txtShiireNm.TabIndex = 2;
			this.txtShiireNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireNm_Validating);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel12, 0, 11);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel11, 0, 10);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel10, 0, 9);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel9, 0, 8);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 7);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 6);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 38);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 12;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1070, 436);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel12
			// 
			this.fsiPanel12.Controls.Add(this.txtShohizeiNyuryokuHoho);
			this.fsiPanel12.Controls.Add(this.lblShzNrkHohoNm);
			this.fsiPanel12.Controls.Add(this.label6);
			this.fsiPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel12.Location = new System.Drawing.Point(4, 400);
			this.fsiPanel12.Name = "fsiPanel12";
			this.fsiPanel12.Size = new System.Drawing.Size(1062, 32);
			this.fsiPanel12.TabIndex = 11;
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.Silver;
			this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label6.Location = new System.Drawing.Point(0, 0);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(1062, 32);
			this.label6.TabIndex = 26;
			this.label6.Tag = "CHANGE";
			this.label6.Text = "消費税入力方法";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel11
			// 
			this.fsiPanel11.Controls.Add(this.lblKgkHsShrMemo);
			this.fsiPanel11.Controls.Add(this.txtKingakuHasuShori);
			this.fsiPanel11.Controls.Add(this.lblKingakuHasuShori);
			this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel11.Location = new System.Drawing.Point(4, 364);
			this.fsiPanel11.Name = "fsiPanel11";
			this.fsiPanel11.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel11.TabIndex = 10;
			// 
			// fsiPanel10
			// 
			this.fsiPanel10.Controls.Add(this.txtTankaShutokuHoho);
			this.fsiPanel10.Controls.Add(this.lblTnkStkHohoMemo);
			this.fsiPanel10.Controls.Add(this.lblTankaShutokuHoho);
			this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel10.Location = new System.Drawing.Point(4, 328);
			this.fsiPanel10.Name = "fsiPanel10";
			this.fsiPanel10.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel10.TabIndex = 9;
			// 
			// fsiPanel9
			// 
			this.fsiPanel9.Controls.Add(this.txthyoji);
			this.fsiPanel9.Controls.Add(this.label4);
			this.fsiPanel9.Controls.Add(this.txtTantoshaCd);
			this.fsiPanel9.Controls.Add(this.lblTantoshaNm);
			this.fsiPanel9.Controls.Add(this.label5);
			this.fsiPanel9.Controls.Add(this.lblTantoshaCd);
			this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel9.Location = new System.Drawing.Point(4, 292);
			this.fsiPanel9.Name = "fsiPanel9";
			this.fsiPanel9.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel9.TabIndex = 8;
			// 
			// fsiPanel8
			// 
			this.fsiPanel8.Controls.Add(this.txtFaxBango);
			this.fsiPanel8.Controls.Add(this.lblKaishuTsuki);
			this.fsiPanel8.Controls.Add(this.txtKaishuTsuki);
			this.fsiPanel8.Controls.Add(this.label1);
			this.fsiPanel8.Controls.Add(this.lblFaxBango);
			this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel8.Location = new System.Drawing.Point(4, 256);
			this.fsiPanel8.Name = "fsiPanel8";
			this.fsiPanel8.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel8.TabIndex = 7;
			// 
			// fsiPanel7
			// 
			this.fsiPanel7.Controls.Add(this.txtDenwaBango);
			this.fsiPanel7.Controls.Add(this.lblShohizeiTenkaHoho);
			this.fsiPanel7.Controls.Add(this.txtShohizeiTenkaHoho);
			this.fsiPanel7.Controls.Add(this.lblShzTnkHohoMemo);
			this.fsiPanel7.Controls.Add(this.lblDenwaBango);
			this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel7.Location = new System.Drawing.Point(4, 220);
			this.fsiPanel7.Name = "fsiPanel7";
			this.fsiPanel7.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel7.TabIndex = 6;
			// 
			// fsiPanel6
			// 
			this.fsiPanel6.Controls.Add(this.txtJusho2);
			this.fsiPanel6.Controls.Add(this.lblShohizeiHasuShori);
			this.fsiPanel6.Controls.Add(this.txtShohizeiHasuShori);
			this.fsiPanel6.Controls.Add(this.lblShzHsSrMemo);
			this.fsiPanel6.Controls.Add(this.lblJusho2);
			this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel6.Location = new System.Drawing.Point(4, 184);
			this.fsiPanel6.Name = "fsiPanel6";
			this.fsiPanel6.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel6.TabIndex = 5;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.txtJusho1);
			this.fsiPanel5.Controls.Add(this.lblSeikyushoKeishiki);
			this.fsiPanel5.Controls.Add(this.txtSeikyushoKeishiki);
			this.fsiPanel5.Controls.Add(this.lblSkshKskMemo);
			this.fsiPanel5.Controls.Add(this.lblJusho1);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 148);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel5.TabIndex = 4;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.label2);
			this.fsiPanel4.Controls.Add(this.txtYubinBango1);
			this.fsiPanel4.Controls.Add(this.txtKaishuBi);
			this.fsiPanel4.Controls.Add(this.lblYubinBangoHyphen);
			this.fsiPanel4.Controls.Add(this.txtYubinBango2);
			this.fsiPanel4.Controls.Add(this.lblSeikyushoHakko);
			this.fsiPanel4.Controls.Add(this.txtSeikyushoHakko);
			this.fsiPanel4.Controls.Add(this.lblSkshHkMemo);
			this.fsiPanel4.Controls.Add(this.lblYubinBango);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 112);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel4.TabIndex = 3;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtShiireKanaNm);
			this.fsiPanel3.Controls.Add(this.lblShimebi);
			this.fsiPanel3.Controls.Add(this.txtShimebi);
			this.fsiPanel3.Controls.Add(this.lblShimebiMemo);
			this.fsiPanel3.Controls.Add(this.lblShiireKanaNm);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 76);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtShiireNm);
			this.fsiPanel2.Controls.Add(this.lblSeikyusakiCd);
			this.fsiPanel2.Controls.Add(this.txtSeikyusakiCd);
			this.fsiPanel2.Controls.Add(this.lblSeikyusakiNm);
			this.fsiPanel2.Controls.Add(this.label3);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 40);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel2.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(1062, 29);
			this.label3.TabIndex = 1;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "仕入先名";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtShiireCd);
			this.fsiPanel1.Controls.Add(this.lblShiireCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(1062, 29);
			this.fsiPanel1.TabIndex = 1;
			// 
			// KBCM1012
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1089, 634);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "KBCM1012";
			this.ShowFButton = true;
			this.Text = "仕入先の登録";
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel12.ResumeLayout(false);
			this.fsiPanel12.PerformLayout();
			this.fsiPanel11.ResumeLayout(false);
			this.fsiPanel11.PerformLayout();
			this.fsiPanel10.ResumeLayout(false);
			this.fsiPanel10.PerformLayout();
			this.fsiPanel9.ResumeLayout(false);
			this.fsiPanel9.PerformLayout();
			this.fsiPanel8.ResumeLayout(false);
			this.fsiPanel8.PerformLayout();
			this.fsiPanel7.ResumeLayout(false);
			this.fsiPanel7.PerformLayout();
			this.fsiPanel6.ResumeLayout(false);
			this.fsiPanel6.PerformLayout();
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtShiireCd;
        private System.Windows.Forms.Label lblShiireCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireNm;
        private System.Windows.Forms.Label lblShiireKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireKanaNm;
        private System.Windows.Forms.Label lblYubinBango;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango1;
        private System.Windows.Forms.Label lblYubinBangoHyphen;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango2;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho1;
        private System.Windows.Forms.Label lblJusho1;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho2;
        private System.Windows.Forms.Label lblJusho2;
        private System.Windows.Forms.Label lblDenwaBango;
        private jp.co.fsi.common.controls.FsiTextBox txtDenwaBango;
        private jp.co.fsi.common.controls.FsiTextBox txtFaxBango;
        private System.Windows.Forms.Label lblFaxBango;
        private System.Windows.Forms.Label lblTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblTantoshaNm;
        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtTankaShutokuHoho;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtKingakuHasuShori;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblSeikyusakiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyusakiCd;
        private System.Windows.Forms.Label lblSeikyusakiCd;
        private System.Windows.Forms.Label lblShimebi;
        private jp.co.fsi.common.controls.FsiTextBox txtShimebi;
        private System.Windows.Forms.Label lblShimebiMemo;
        private System.Windows.Forms.Label lblSkshHkMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoHakko;
        private System.Windows.Forms.Label lblSeikyushoHakko;
        private System.Windows.Forms.Label lblSkshKskMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoKeishiki;
        private System.Windows.Forms.Label lblSeikyushoKeishiki;
        private System.Windows.Forms.Label lblShzHsSrMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiHasuShori;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private System.Windows.Forms.Label label1;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuTsuki;
        private System.Windows.Forms.Label lblKaishuTsuki;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuBi;
        private System.Windows.Forms.Label label2;
        private common.controls.FsiTextBox txthyoji;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel12;
		private common.FsiPanel fsiPanel11;
		private common.FsiPanel fsiPanel10;
		private common.FsiPanel fsiPanel9;
		private common.FsiPanel fsiPanel8;
		private common.FsiPanel fsiPanel7;
		private common.FsiPanel fsiPanel6;
		private common.FsiPanel fsiPanel5;
		private common.FsiPanel fsiPanel4;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label6;
	}
}