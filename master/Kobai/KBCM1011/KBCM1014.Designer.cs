﻿namespace jp.co.fsi.kb.kbcm1011
{
    partial class KBCM1014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
			this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
			this.lblKingakuHasuShori = new System.Windows.Forms.Label();
			this.txtKingakuHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
			this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
			this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
			this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
			this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShzHsSrMemo = new System.Windows.Forms.Label();
			this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
			this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 179);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(559, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(549, 31);
			this.lblTitle.Text = "";
			// 
			// lblTankaShutokuHoho
			// 
			this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblTankaShutokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTankaShutokuHoho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTankaShutokuHoho.Location = new System.Drawing.Point(0, 0);
			this.lblTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
			this.lblTankaShutokuHoho.Size = new System.Drawing.Size(522, 26);
			this.lblTankaShutokuHoho.TabIndex = 19;
			this.lblTankaShutokuHoho.Tag = "CHANGE";
			this.lblTankaShutokuHoho.Text = "単価取得方法";
			this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTankaShutokuHoho
			// 
			this.txtTankaShutokuHoho.AutoSizeFromLength = true;
			this.txtTankaShutokuHoho.DisplayLength = 2;
			this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtTankaShutokuHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTankaShutokuHoho.Location = new System.Drawing.Point(140, 2);
			this.txtTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtTankaShutokuHoho.MaxLength = 1;
			this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
			this.txtTankaShutokuHoho.Size = new System.Drawing.Size(25, 23);
			this.txtTankaShutokuHoho.TabIndex = 20;
			this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
			// 
			// lblTnkStkHohoMemo
			// 
			this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(174, 2);
			this.lblTnkStkHohoMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
			this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(331, 24);
			this.lblTnkStkHohoMemo.TabIndex = 21;
			this.lblTnkStkHohoMemo.Tag = "CHANGE";
			this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
			this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKingakuHasuShori
			// 
			this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblKingakuHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKingakuHasuShori.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKingakuHasuShori.Location = new System.Drawing.Point(0, 0);
			this.lblKingakuHasuShori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
			this.lblKingakuHasuShori.Size = new System.Drawing.Size(522, 26);
			this.lblKingakuHasuShori.TabIndex = 22;
			this.lblKingakuHasuShori.Tag = "CHANGE";
			this.lblKingakuHasuShori.Text = "金額端数処理";
			this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKingakuHasuShori
			// 
			this.txtKingakuHasuShori.AutoSizeFromLength = true;
			this.txtKingakuHasuShori.DisplayLength = 2;
			this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKingakuHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtKingakuHasuShori.Location = new System.Drawing.Point(140, 2);
			this.txtKingakuHasuShori.Margin = new System.Windows.Forms.Padding(4);
			this.txtKingakuHasuShori.MaxLength = 1;
			this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
			this.txtKingakuHasuShori.Size = new System.Drawing.Size(25, 23);
			this.txtKingakuHasuShori.TabIndex = 23;
			this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
			// 
			// lblKgkHsShrMemo
			// 
			this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKgkHsShrMemo.Location = new System.Drawing.Point(174, 2);
			this.lblKgkHsShrMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
			this.lblKgkHsShrMemo.Size = new System.Drawing.Size(331, 24);
			this.lblKgkHsShrMemo.TabIndex = 24;
			this.lblKgkHsShrMemo.Tag = "CHANGE";
			this.lblKgkHsShrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
			this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiNyuryokuHoho
			// 
			this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohizeiNyuryokuHoho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(0, 0);
			this.lblShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
			this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(522, 26);
			this.lblShohizeiNyuryokuHoho.TabIndex = 25;
			this.lblShohizeiNyuryokuHoho.Tag = "CHANGE";
			this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
			this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiNyuryokuHoho
			// 
			this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = true;
			this.txtShohizeiNyuryokuHoho.DisplayLength = 2;
			this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShohizeiNyuryokuHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(140, 2);
			this.txtShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiNyuryokuHoho.MaxLength = 1;
			this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
			this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(25, 23);
			this.txtShohizeiNyuryokuHoho.TabIndex = 26;
			this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiNyuryokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiNyuryokuHoho_Validating);
			// 
			// lblShzNrkHohoNm
			// 
			this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.LightCyan;
			this.lblShzNrkHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShzNrkHohoNm.Location = new System.Drawing.Point(174, 0);
			this.lblShzNrkHohoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
			this.lblShzNrkHohoNm.Size = new System.Drawing.Size(331, 24);
			this.lblShzNrkHohoNm.TabIndex = 27;
			this.lblShzNrkHohoNm.Tag = "DISPNAME";
			this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiHasuShori
			// 
			this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohizeiHasuShori.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShohizeiHasuShori.Location = new System.Drawing.Point(0, 0);
			this.lblShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
			this.lblShohizeiHasuShori.Size = new System.Drawing.Size(522, 26);
			this.lblShohizeiHasuShori.TabIndex = 40;
			this.lblShohizeiHasuShori.Tag = "CHANGE";
			this.lblShohizeiHasuShori.Text = "消費税端数処理";
			this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiHasuShori
			// 
			this.txtShohizeiHasuShori.AutoSizeFromLength = true;
			this.txtShohizeiHasuShori.DisplayLength = 2;
			this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShohizeiHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShohizeiHasuShori.Location = new System.Drawing.Point(140, 1);
			this.txtShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiHasuShori.MaxLength = 1;
			this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
			this.txtShohizeiHasuShori.Size = new System.Drawing.Size(25, 23);
			this.txtShohizeiHasuShori.TabIndex = 41;
			this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
			// 
			// lblShzHsSrMemo
			// 
			this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShzHsSrMemo.Location = new System.Drawing.Point(174, 1);
			this.lblShzHsSrMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
			this.lblShzHsSrMemo.Size = new System.Drawing.Size(331, 24);
			this.lblShzHsSrMemo.TabIndex = 42;
			this.lblShzHsSrMemo.Tag = "CHANGE";
			this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
			this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiTenkaHoho
			// 
			this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiTenkaHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohizeiTenkaHoho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(0, 0);
			this.lblShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
			this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(522, 29);
			this.lblShohizeiTenkaHoho.TabIndex = 43;
			this.lblShohizeiTenkaHoho.Tag = "CHANGE";
			this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
			this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiTenkaHoho
			// 
			this.txtShohizeiTenkaHoho.AutoSizeFromLength = true;
			this.txtShohizeiTenkaHoho.DisplayLength = 2;
			this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtShohizeiTenkaHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(140, 2);
			this.txtShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiTenkaHoho.MaxLength = 1;
			this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
			this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(25, 23);
			this.txtShohizeiTenkaHoho.TabIndex = 44;
			this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiTenkaHoho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohizeiTenkaHoho_KeyDown);
			this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
			// 
			// lblShzTnkHohoMemo
			// 
			this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(174, 2);
			this.lblShzTnkHohoMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
			this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(331, 24);
			this.lblShzTnkHohoMemo.TabIndex = 45;
			this.lblShzTnkHohoMemo.Tag = "CHANGE";
			this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
			this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(8, 40);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 5;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(530, 169);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtTankaShutokuHoho);
			this.fsiPanel1.Controls.Add(this.lblTnkStkHohoMemo);
			this.fsiPanel1.Controls.Add(this.lblTankaShutokuHoho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(522, 26);
			this.fsiPanel1.TabIndex = 0;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtKingakuHasuShori);
			this.fsiPanel2.Controls.Add(this.lblKgkHsShrMemo);
			this.fsiPanel2.Controls.Add(this.lblKingakuHasuShori);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 37);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(522, 26);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtShohizeiNyuryokuHoho);
			this.fsiPanel3.Controls.Add(this.lblShzNrkHohoNm);
			this.fsiPanel3.Controls.Add(this.lblShohizeiNyuryokuHoho);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 70);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(522, 26);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lblShzHsSrMemo);
			this.fsiPanel4.Controls.Add(this.txtShohizeiHasuShori);
			this.fsiPanel4.Controls.Add(this.lblShohizeiHasuShori);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 103);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(522, 26);
			this.fsiPanel4.TabIndex = 3;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.lblShzTnkHohoMemo);
			this.fsiPanel5.Controls.Add(this.txtShohizeiTenkaHoho);
			this.fsiPanel5.Controls.Add(this.lblShohizeiTenkaHoho);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 136);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(522, 29);
			this.fsiPanel5.TabIndex = 4;
			// 
			// KBCM1014
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(549, 316);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "KBCM1014";
			this.ShowFButton = true;
			this.Text = "仕入先マスタ初期値設定";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private common.controls.FsiTextBox txtTankaShutokuHoho;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private common.controls.FsiTextBox txtKingakuHasuShori;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private common.controls.FsiTextBox txtShohizeiHasuShori;
        private System.Windows.Forms.Label lblShzHsSrMemo;
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private common.controls.FsiTextBox txtShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel5;
		private common.FsiPanel fsiPanel4;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
	}
}