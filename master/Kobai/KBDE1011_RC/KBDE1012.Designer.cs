﻿namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x02000003 RID: 3
	public partial class KBDE1012 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x0600002B RID: 43 RVA: 0x0000221F File Offset: 0x0000041F
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x0000468C File Offset: 0x0000288C
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblTantoshaNmFr = new System.Windows.Forms.Label();
			this.txtTantoshaCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTantoshaCd = new System.Windows.Forms.Label();
			this.lblFunanushiNmFr = new System.Windows.Forms.Label();
			this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiCd = new System.Windows.Forms.Label();
			this.txtSearchCode = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblSearchCode = new System.Windows.Forms.Label();
			this.lblDayFr = new System.Windows.Forms.Label();
			this.lblMonthFr = new System.Windows.Forms.Label();
			this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblYearFr = new System.Windows.Forms.Label();
			this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtGengoYearFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoFr = new System.Windows.Forms.Label();
			this.lblDenpyoDate = new System.Windows.Forms.Label();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.lblDateBet1 = new System.Windows.Forms.Label();
			this.lblDayTo = new System.Windows.Forms.Label();
			this.lblMonthTo = new System.Windows.Forms.Label();
			this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblYearTo = new System.Windows.Forms.Label();
			this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtGengoYearTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoTo = new System.Windows.Forms.Label();
			this.lblDateBet2 = new System.Windows.Forms.Label();
			this.lblFunanushiNmTo = new System.Windows.Forms.Label();
			this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateBet3 = new System.Windows.Forms.Label();
			this.lblTantoshaNmTo = new System.Windows.Forms.Label();
			this.txtTantoshaCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.btnEnter = new System.Windows.Forms.Button();
			this.lblShohinNmTo = new System.Windows.Forms.Label();
			this.txtShohinCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.lblShohinNmFr = new System.Windows.Forms.Label();
			this.txtShohinCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinCd = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(4, 65);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(176, 65);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF2
			// 
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Location = new System.Drawing.Point(261, 65);
			this.btnF3.Margin = new System.Windows.Forms.Padding(5);
			this.btnF3.Visible = false;
			// 
			// btnF4
			// 
			this.btnF4.Location = new System.Drawing.Point(261, 65);
			this.btnF4.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF5
			// 
			this.btnF5.Location = new System.Drawing.Point(432, 65);
			this.btnF5.Margin = new System.Windows.Forms.Padding(5);
			this.btnF5.Visible = false;
			// 
			// btnF7
			// 
			this.btnF7.Location = new System.Drawing.Point(603, 65);
			this.btnF7.Margin = new System.Windows.Forms.Padding(5);
			this.btnF7.Visible = false;
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(347, 65);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF8
			// 
			this.btnF8.Location = new System.Drawing.Point(688, 65);
			this.btnF8.Margin = new System.Windows.Forms.Padding(5);
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Location = new System.Drawing.Point(773, 65);
			this.btnF9.Margin = new System.Windows.Forms.Padding(5);
			this.btnF9.Visible = false;
			// 
			// btnF12
			// 
			this.btnF12.Location = new System.Drawing.Point(1029, 65);
			this.btnF12.Margin = new System.Windows.Forms.Padding(5);
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Location = new System.Drawing.Point(944, 65);
			this.btnF11.Margin = new System.Windows.Forms.Padding(5);
			this.btnF11.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Location = new System.Drawing.Point(859, 65);
			this.btnF10.Margin = new System.Windows.Forms.Padding(5);
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Location = new System.Drawing.Point(7, 467);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1029, 135);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1052, 31);
			this.lblTitle.Text = "仕入伝票検索";
			// 
			// lblTantoshaNmFr
			// 
			this.lblTantoshaNmFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblTantoshaNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoshaNmFr.ForeColor = System.Drawing.Color.Black;
			this.lblTantoshaNmFr.Location = new System.Drawing.Point(131, 4);
			this.lblTantoshaNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaNmFr.Name = "lblTantoshaNmFr";
			this.lblTantoshaNmFr.Size = new System.Drawing.Size(280, 24);
			this.lblTantoshaNmFr.TabIndex = 26;
			this.lblTantoshaNmFr.Tag = "DISPNAME";
			this.lblTantoshaNmFr.Text = "先　頭";
			this.lblTantoshaNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTantoshaCdFr
			// 
			this.txtTantoshaCdFr.AutoSizeFromLength = true;
			this.txtTantoshaCdFr.DisplayLength = null;
			this.txtTantoshaCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTantoshaCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTantoshaCdFr.Location = new System.Drawing.Point(84, 4);
			this.txtTantoshaCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtTantoshaCdFr.MaxLength = 4;
			this.txtTantoshaCdFr.Name = "txtTantoshaCdFr";
			this.txtTantoshaCdFr.Size = new System.Drawing.Size(44, 23);
			this.txtTantoshaCdFr.TabIndex = 25;
			this.txtTantoshaCdFr.Tag = "CHANGE";
			this.txtTantoshaCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCdFr_Validating);
			// 
			// lblTantoshaCd
			// 
			this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
			this.lblTantoshaCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoshaCd.Location = new System.Drawing.Point(0, 0);
			this.lblTantoshaCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaCd.Name = "lblTantoshaCd";
			this.lblTantoshaCd.Size = new System.Drawing.Size(1017, 31);
			this.lblTantoshaCd.TabIndex = 24;
			this.lblTantoshaCd.Tag = "CHANGE";
			this.lblTantoshaCd.Text = "担 当 者";
			this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiNmFr
			// 
			this.lblFunanushiNmFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiNmFr.ForeColor = System.Drawing.Color.Black;
			this.lblFunanushiNmFr.Location = new System.Drawing.Point(131, 4);
			this.lblFunanushiNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiNmFr.Name = "lblFunanushiNmFr";
			this.lblFunanushiNmFr.Size = new System.Drawing.Size(280, 24);
			this.lblFunanushiNmFr.TabIndex = 20;
			this.lblFunanushiNmFr.Tag = "DISPNAME";
			this.lblFunanushiNmFr.Text = "先　頭";
			this.lblFunanushiNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdFr
			// 
			this.txtFunanushiCdFr.AutoSizeFromLength = true;
			this.txtFunanushiCdFr.DisplayLength = null;
			this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCdFr.Location = new System.Drawing.Point(84, 4);
			this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiCdFr.MaxLength = 4;
			this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
			this.txtFunanushiCdFr.Size = new System.Drawing.Size(44, 23);
			this.txtFunanushiCdFr.TabIndex = 19;
			this.txtFunanushiCdFr.Tag = "CHANGE";
			this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
			// 
			// lblFunanushiCd
			// 
			this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushiCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCd.Location = new System.Drawing.Point(0, 0);
			this.lblFunanushiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCd.Name = "lblFunanushiCd";
			this.lblFunanushiCd.Size = new System.Drawing.Size(1017, 31);
			this.lblFunanushiCd.TabIndex = 18;
			this.lblFunanushiCd.Tag = "CHANGE";
			this.lblFunanushiCd.Text = "船主CD";
			this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSearchCode
			// 
			this.txtSearchCode.AutoSizeFromLength = true;
			this.txtSearchCode.DisplayLength = null;
			this.txtSearchCode.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSearchCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSearchCode.Location = new System.Drawing.Point(877, 4);
			this.txtSearchCode.Margin = new System.Windows.Forms.Padding(4);
			this.txtSearchCode.MaxLength = 10;
			this.txtSearchCode.Name = "txtSearchCode";
			this.txtSearchCode.Size = new System.Drawing.Size(100, 23);
			this.txtSearchCode.TabIndex = 37;
			this.txtSearchCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtSearchCode_Validating);
			// 
			// lblSearchCode
			// 
			this.lblSearchCode.BackColor = System.Drawing.Color.Silver;
			this.lblSearchCode.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSearchCode.Location = new System.Drawing.Point(781, 4);
			this.lblSearchCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSearchCode.Name = "lblSearchCode";
			this.lblSearchCode.Size = new System.Drawing.Size(88, 27);
			this.lblSearchCode.TabIndex = 36;
			this.lblSearchCode.Tag = "CHANGE";
			this.lblSearchCode.Text = "検索コード";
			this.lblSearchCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDayFr
			// 
			this.lblDayFr.AutoSize = true;
			this.lblDayFr.BackColor = System.Drawing.Color.Silver;
			this.lblDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayFr.Location = new System.Drawing.Point(378, 8);
			this.lblDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDayFr.Name = "lblDayFr";
			this.lblDayFr.Size = new System.Drawing.Size(24, 16);
			this.lblDayFr.TabIndex = 8;
			this.lblDayFr.Tag = "CHANGE";
			this.lblDayFr.Text = "日";
			// 
			// lblMonthFr
			// 
			this.lblMonthFr.AutoSize = true;
			this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
			this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthFr.Location = new System.Drawing.Point(289, 8);
			this.lblMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonthFr.Name = "lblMonthFr";
			this.lblMonthFr.Size = new System.Drawing.Size(24, 16);
			this.lblMonthFr.TabIndex = 6;
			this.lblMonthFr.Tag = "CHANGE";
			this.lblMonthFr.Text = "月";
			// 
			// txtDayFr
			// 
			this.txtDayFr.AutoSizeFromLength = false;
			this.txtDayFr.DisplayLength = null;
			this.txtDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDayFr.Location = new System.Drawing.Point(320, 4);
			this.txtDayFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtDayFr.MaxLength = 2;
			this.txtDayFr.Name = "txtDayFr";
			this.txtDayFr.Size = new System.Drawing.Size(52, 23);
			this.txtDayFr.TabIndex = 7;
			this.txtDayFr.Tag = "CHANGE";
			this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
			// 
			// lblYearFr
			// 
			this.lblYearFr.AutoSize = true;
			this.lblYearFr.BackColor = System.Drawing.Color.Silver;
			this.lblYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearFr.Location = new System.Drawing.Point(200, 8);
			this.lblYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYearFr.Name = "lblYearFr";
			this.lblYearFr.Size = new System.Drawing.Size(24, 16);
			this.lblYearFr.TabIndex = 4;
			this.lblYearFr.Tag = "CHANGE";
			this.lblYearFr.Text = "年";
			// 
			// txtMonthFr
			// 
			this.txtMonthFr.AutoSizeFromLength = false;
			this.txtMonthFr.DisplayLength = null;
			this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonthFr.Location = new System.Drawing.Point(230, 4);
			this.txtMonthFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonthFr.MaxLength = 2;
			this.txtMonthFr.Name = "txtMonthFr";
			this.txtMonthFr.Size = new System.Drawing.Size(52, 23);
			this.txtMonthFr.TabIndex = 5;
			this.txtMonthFr.Tag = "CHANGE";
			this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
			// 
			// txtGengoYearFr
			// 
			this.txtGengoYearFr.AutoSizeFromLength = false;
			this.txtGengoYearFr.DisplayLength = null;
			this.txtGengoYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGengoYearFr.Location = new System.Drawing.Point(141, 4);
			this.txtGengoYearFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtGengoYearFr.MaxLength = 2;
			this.txtGengoYearFr.Name = "txtGengoYearFr";
			this.txtGengoYearFr.Size = new System.Drawing.Size(52, 23);
			this.txtGengoYearFr.TabIndex = 3;
			this.txtGengoYearFr.Tag = "CHANGE";
			this.txtGengoYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearFr_Validating);
			// 
			// lblGengoFr
			// 
			this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengoFr.ForeColor = System.Drawing.Color.Black;
			this.lblGengoFr.Location = new System.Drawing.Point(84, 4);
			this.lblGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengoFr.Name = "lblGengoFr";
			this.lblGengoFr.Size = new System.Drawing.Size(53, 24);
			this.lblGengoFr.TabIndex = 2;
			this.lblGengoFr.Tag = "DISPNAME";
			this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblDenpyoDate
			// 
			this.lblDenpyoDate.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoDate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoDate.Location = new System.Drawing.Point(0, 0);
			this.lblDenpyoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoDate.Name = "lblDenpyoDate";
			this.lblDenpyoDate.Size = new System.Drawing.Size(1017, 31);
			this.lblDenpyoDate.TabIndex = 0;
			this.lblDenpyoDate.Tag = "CHANGE";
			this.lblDenpyoDate.Text = "伝票日付";
			this.lblDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(16, 165);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(1022, 349);
			this.dgvList.TabIndex = 38;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvList_CellFormatting);
			this.dgvList.Enter += new System.EventHandler(this.dgvList_Enter);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// lblDateBet1
			// 
			this.lblDateBet1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateBet1.Location = new System.Drawing.Point(416, 4);
			this.lblDateBet1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateBet1.Name = "lblDateBet1";
			this.lblDateBet1.Size = new System.Drawing.Size(24, 24);
			this.lblDateBet1.TabIndex = 9;
			this.lblDateBet1.Tag = "CHANGE";
			this.lblDateBet1.Text = "～";
			this.lblDateBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDayTo
			// 
			this.lblDayTo.AutoSize = true;
			this.lblDayTo.BackColor = System.Drawing.Color.Silver;
			this.lblDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDayTo.Location = new System.Drawing.Point(743, 9);
			this.lblDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDayTo.Name = "lblDayTo";
			this.lblDayTo.Size = new System.Drawing.Size(24, 16);
			this.lblDayTo.TabIndex = 17;
			this.lblDayTo.Tag = "CHANGE";
			this.lblDayTo.Text = "日";
			// 
			// lblMonthTo
			// 
			this.lblMonthTo.AutoSize = true;
			this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
			this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonthTo.Location = new System.Drawing.Point(653, 9);
			this.lblMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonthTo.Name = "lblMonthTo";
			this.lblMonthTo.Size = new System.Drawing.Size(24, 16);
			this.lblMonthTo.TabIndex = 15;
			this.lblMonthTo.Tag = "CHANGE";
			this.lblMonthTo.Text = "月";
			// 
			// txtDayTo
			// 
			this.txtDayTo.AutoSizeFromLength = false;
			this.txtDayTo.DisplayLength = null;
			this.txtDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDayTo.Location = new System.Drawing.Point(684, 4);
			this.txtDayTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtDayTo.MaxLength = 2;
			this.txtDayTo.Name = "txtDayTo";
			this.txtDayTo.Size = new System.Drawing.Size(52, 23);
			this.txtDayTo.TabIndex = 16;
			this.txtDayTo.Tag = "CHANGE";
			this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
			// 
			// lblYearTo
			// 
			this.lblYearTo.AutoSize = true;
			this.lblYearTo.BackColor = System.Drawing.Color.Silver;
			this.lblYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYearTo.Location = new System.Drawing.Point(564, 9);
			this.lblYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYearTo.Name = "lblYearTo";
			this.lblYearTo.Size = new System.Drawing.Size(24, 16);
			this.lblYearTo.TabIndex = 13;
			this.lblYearTo.Tag = "CHANGE";
			this.lblYearTo.Text = "年";
			// 
			// txtMonthTo
			// 
			this.txtMonthTo.AutoSizeFromLength = false;
			this.txtMonthTo.DisplayLength = null;
			this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonthTo.Location = new System.Drawing.Point(595, 4);
			this.txtMonthTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtMonthTo.MaxLength = 2;
			this.txtMonthTo.Name = "txtMonthTo";
			this.txtMonthTo.Size = new System.Drawing.Size(52, 23);
			this.txtMonthTo.TabIndex = 14;
			this.txtMonthTo.Tag = "CHANGE";
			this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
			// 
			// txtGengoYearTo
			// 
			this.txtGengoYearTo.AutoSizeFromLength = false;
			this.txtGengoYearTo.DisplayLength = null;
			this.txtGengoYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGengoYearTo.Location = new System.Drawing.Point(505, 4);
			this.txtGengoYearTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtGengoYearTo.MaxLength = 2;
			this.txtGengoYearTo.Name = "txtGengoYearTo";
			this.txtGengoYearTo.Size = new System.Drawing.Size(52, 23);
			this.txtGengoYearTo.TabIndex = 12;
			this.txtGengoYearTo.Tag = "CHANGE";
			this.txtGengoYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearTo_Validating);
			// 
			// lblGengoTo
			// 
			this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengoTo.ForeColor = System.Drawing.Color.Black;
			this.lblGengoTo.Location = new System.Drawing.Point(448, 4);
			this.lblGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengoTo.Name = "lblGengoTo";
			this.lblGengoTo.Size = new System.Drawing.Size(53, 24);
			this.lblGengoTo.TabIndex = 11;
			this.lblGengoTo.Tag = "DISPNAME";
			this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblDateBet2
			// 
			this.lblDateBet2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateBet2.Location = new System.Drawing.Point(417, 4);
			this.lblDateBet2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateBet2.Name = "lblDateBet2";
			this.lblDateBet2.Size = new System.Drawing.Size(24, 24);
			this.lblDateBet2.TabIndex = 21;
			this.lblDateBet2.Tag = "CHANGE";
			this.lblDateBet2.Text = "～";
			this.lblDateBet2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiNmTo
			// 
			this.lblFunanushiNmTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblFunanushiNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiNmTo.ForeColor = System.Drawing.Color.Black;
			this.lblFunanushiNmTo.Location = new System.Drawing.Point(494, 4);
			this.lblFunanushiNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiNmTo.Name = "lblFunanushiNmTo";
			this.lblFunanushiNmTo.Size = new System.Drawing.Size(281, 24);
			this.lblFunanushiNmTo.TabIndex = 23;
			this.lblFunanushiNmTo.Tag = "DISPNAME";
			this.lblFunanushiNmTo.Text = "最　後";
			this.lblFunanushiNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCdTo
			// 
			this.txtFunanushiCdTo.AutoSizeFromLength = true;
			this.txtFunanushiCdTo.DisplayLength = null;
			this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCdTo.Location = new System.Drawing.Point(447, 4);
			this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtFunanushiCdTo.MaxLength = 4;
			this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
			this.txtFunanushiCdTo.Size = new System.Drawing.Size(44, 23);
			this.txtFunanushiCdTo.TabIndex = 22;
			this.txtFunanushiCdTo.Tag = "CHANGE";
			this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
			// 
			// lblDateBet3
			// 
			this.lblDateBet3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDateBet3.Location = new System.Drawing.Point(417, 3);
			this.lblDateBet3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateBet3.Name = "lblDateBet3";
			this.lblDateBet3.Size = new System.Drawing.Size(24, 24);
			this.lblDateBet3.TabIndex = 27;
			this.lblDateBet3.Tag = "CHANGE";
			this.lblDateBet3.Text = "～";
			this.lblDateBet3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTantoshaNmTo
			// 
			this.lblTantoshaNmTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblTantoshaNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoshaNmTo.ForeColor = System.Drawing.Color.Black;
			this.lblTantoshaNmTo.Location = new System.Drawing.Point(492, 4);
			this.lblTantoshaNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaNmTo.Name = "lblTantoshaNmTo";
			this.lblTantoshaNmTo.Size = new System.Drawing.Size(281, 24);
			this.lblTantoshaNmTo.TabIndex = 29;
			this.lblTantoshaNmTo.Tag = "DISPNAME";
			this.lblTantoshaNmTo.Text = "最　後";
			this.lblTantoshaNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTantoshaCdTo
			// 
			this.txtTantoshaCdTo.AutoSizeFromLength = true;
			this.txtTantoshaCdTo.DisplayLength = null;
			this.txtTantoshaCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTantoshaCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTantoshaCdTo.Location = new System.Drawing.Point(445, 4);
			this.txtTantoshaCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtTantoshaCdTo.MaxLength = 4;
			this.txtTantoshaCdTo.Name = "txtTantoshaCdTo";
			this.txtTantoshaCdTo.Size = new System.Drawing.Size(44, 23);
			this.txtTantoshaCdTo.TabIndex = 28;
			this.txtTantoshaCdTo.Tag = "CHANGE";
			this.txtTantoshaCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCdTo_Validating);
			// 
			// btnEnter
			// 
			this.btnEnter.BackColor = System.Drawing.Color.SkyBlue;
			this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEnter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnEnter.ForeColor = System.Drawing.Color.Navy;
			this.btnEnter.Location = new System.Drawing.Point(89, 65);
			this.btnEnter.Margin = new System.Windows.Forms.Padding(4);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new System.Drawing.Size(87, 60);
			this.btnEnter.TabIndex = 905;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n決定";
			this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = false;
			this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
			// 
			// lblShohinNmTo
			// 
			this.lblShohinNmTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblShohinNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinNmTo.ForeColor = System.Drawing.Color.Black;
			this.lblShohinNmTo.Location = new System.Drawing.Point(655, 4);
			this.lblShohinNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinNmTo.Name = "lblShohinNmTo";
			this.lblShohinNmTo.Size = new System.Drawing.Size(284, 24);
			this.lblShohinNmTo.TabIndex = 35;
			this.lblShohinNmTo.Tag = "DISPNAME";
			this.lblShohinNmTo.Text = "最　後";
			this.lblShohinNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinCdTo
			// 
			this.txtShohinCdTo.AutoSizeFromLength = true;
			this.txtShohinCdTo.DisplayLength = null;
			this.txtShohinCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShohinCdTo.Location = new System.Drawing.Point(524, 4);
			this.txtShohinCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinCdTo.MaxLength = 13;
			this.txtShohinCdTo.Name = "txtShohinCdTo";
			this.txtShohinCdTo.Size = new System.Drawing.Size(128, 23);
			this.txtShohinCdTo.TabIndex = 34;
			this.txtShohinCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdTo_Validating);
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(491, 5);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 27);
			this.label2.TabIndex = 33;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "～";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohinNmFr
			// 
			this.lblShohinNmFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblShohinNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinNmFr.ForeColor = System.Drawing.Color.Black;
			this.lblShohinNmFr.Location = new System.Drawing.Point(213, 4);
			this.lblShohinNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinNmFr.Name = "lblShohinNmFr";
			this.lblShohinNmFr.Size = new System.Drawing.Size(275, 24);
			this.lblShohinNmFr.TabIndex = 32;
			this.lblShohinNmFr.Tag = "DISPNAME";
			this.lblShohinNmFr.Text = "先　頭";
			this.lblShohinNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinCdFr
			// 
			this.txtShohinCdFr.AutoSizeFromLength = true;
			this.txtShohinCdFr.DisplayLength = null;
			this.txtShohinCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtShohinCdFr.Location = new System.Drawing.Point(84, 4);
			this.txtShohinCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinCdFr.MaxLength = 13;
			this.txtShohinCdFr.Name = "txtShohinCdFr";
			this.txtShohinCdFr.Size = new System.Drawing.Size(128, 23);
			this.txtShohinCdFr.TabIndex = 31;
			this.txtShohinCdFr.Tag = "CHANGE";
			this.txtShohinCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdFr_Validating);
			// 
			// lblShohinCd
			// 
			this.lblShohinCd.BackColor = System.Drawing.Color.Silver;
			this.lblShohinCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohinCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinCd.Location = new System.Drawing.Point(0, 0);
			this.lblShohinCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinCd.Name = "lblShohinCd";
			this.lblShohinCd.Size = new System.Drawing.Size(1017, 32);
			this.lblShohinCd.TabIndex = 30;
			this.lblShohinCd.Tag = "CHANGE";
			this.lblShohinCd.Text = "商品CD";
			this.lblShohinCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(13, 8);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 4;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1025, 154);
			this.fsiTableLayoutPanel1.TabIndex = 909;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lblShohinNmTo);
			this.fsiPanel4.Controls.Add(this.txtShohinCdFr);
			this.fsiPanel4.Controls.Add(this.txtShohinCdTo);
			this.fsiPanel4.Controls.Add(this.lblShohinNmFr);
			this.fsiPanel4.Controls.Add(this.label2);
			this.fsiPanel4.Controls.Add(this.lblShohinCd);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 118);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(1017, 32);
			this.fsiPanel4.TabIndex = 3;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtTantoshaCdFr);
			this.fsiPanel3.Controls.Add(this.lblTantoshaNmFr);
			this.fsiPanel3.Controls.Add(this.lblDateBet3);
			this.fsiPanel3.Controls.Add(this.txtTantoshaCdTo);
			this.fsiPanel3.Controls.Add(this.lblTantoshaNmTo);
			this.fsiPanel3.Controls.Add(this.lblSearchCode);
			this.fsiPanel3.Controls.Add(this.txtSearchCode);
			this.fsiPanel3.Controls.Add(this.lblTantoshaCd);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 80);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(1017, 31);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtFunanushiCdFr);
			this.fsiPanel2.Controls.Add(this.lblFunanushiNmFr);
			this.fsiPanel2.Controls.Add(this.lblDateBet2);
			this.fsiPanel2.Controls.Add(this.txtFunanushiCdTo);
			this.fsiPanel2.Controls.Add(this.lblFunanushiNmTo);
			this.fsiPanel2.Controls.Add(this.lblFunanushiCd);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 42);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(1017, 31);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.lblGengoFr);
			this.fsiPanel1.Controls.Add(this.txtGengoYearFr);
			this.fsiPanel1.Controls.Add(this.txtMonthFr);
			this.fsiPanel1.Controls.Add(this.lblYearFr);
			this.fsiPanel1.Controls.Add(this.txtDayFr);
			this.fsiPanel1.Controls.Add(this.lblMonthFr);
			this.fsiPanel1.Controls.Add(this.lblDayFr);
			this.fsiPanel1.Controls.Add(this.lblDateBet1);
			this.fsiPanel1.Controls.Add(this.lblGengoTo);
			this.fsiPanel1.Controls.Add(this.txtGengoYearTo);
			this.fsiPanel1.Controls.Add(this.txtMonthTo);
			this.fsiPanel1.Controls.Add(this.lblDayTo);
			this.fsiPanel1.Controls.Add(this.lblYearTo);
			this.fsiPanel1.Controls.Add(this.lblMonthTo);
			this.fsiPanel1.Controls.Add(this.txtDayTo);
			this.fsiPanel1.Controls.Add(this.lblDenpyoDate);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(1017, 31);
			this.fsiPanel1.TabIndex = 0;
			// 
			// KBDE1012
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1052, 605);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "KBDE1012";
			this.ShowFButton = true;
			this.Text = "売上伝票検索";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

		}

		// Token: 0x04000005 RID: 5
		private global::System.ComponentModel.IContainer components = null;

		// Token: 0x04000006 RID: 6
		private global::System.Windows.Forms.Label lblTantoshaNmFr;

		// Token: 0x04000007 RID: 7
		private global::jp.co.fsi.common.controls.FsiTextBox txtTantoshaCdFr;

		// Token: 0x04000008 RID: 8
		private global::System.Windows.Forms.Label lblTantoshaCd;

		// Token: 0x04000009 RID: 9
		private global::System.Windows.Forms.Label lblFunanushiNmFr;

		// Token: 0x0400000A RID: 10
		private global::jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdFr;

		// Token: 0x0400000B RID: 11
		private global::System.Windows.Forms.Label lblFunanushiCd;

		// Token: 0x0400000C RID: 12
		private global::jp.co.fsi.common.controls.FsiTextBox txtSearchCode;

		// Token: 0x0400000D RID: 13
		private global::System.Windows.Forms.Label lblSearchCode;

		// Token: 0x0400000E RID: 14
		private global::System.Windows.Forms.Label lblDayFr;

		// Token: 0x0400000F RID: 15
		private global::System.Windows.Forms.Label lblMonthFr;

		// Token: 0x04000010 RID: 16
		private global::jp.co.fsi.common.controls.FsiTextBox txtDayFr;

		// Token: 0x04000011 RID: 17
		private global::System.Windows.Forms.Label lblYearFr;

		// Token: 0x04000012 RID: 18
		private global::jp.co.fsi.common.controls.FsiTextBox txtMonthFr;

		// Token: 0x04000013 RID: 19
		private global::jp.co.fsi.common.controls.FsiTextBox txtGengoYearFr;

		// Token: 0x04000014 RID: 20
		private global::System.Windows.Forms.Label lblGengoFr;

		// Token: 0x04000016 RID: 22
		private global::System.Windows.Forms.Label lblDenpyoDate;

		// Token: 0x04000017 RID: 23
		private global::System.Windows.Forms.DataGridView dgvList;

		// Token: 0x04000018 RID: 24
		private global::System.Windows.Forms.Label lblDateBet1;

		// Token: 0x04000019 RID: 25
		private global::System.Windows.Forms.Label lblDayTo;

		// Token: 0x0400001A RID: 26
		private global::System.Windows.Forms.Label lblMonthTo;

		// Token: 0x0400001B RID: 27
		private global::jp.co.fsi.common.controls.FsiTextBox txtDayTo;

		// Token: 0x0400001C RID: 28
		private global::System.Windows.Forms.Label lblYearTo;

		// Token: 0x0400001D RID: 29
		private global::jp.co.fsi.common.controls.FsiTextBox txtMonthTo;

		// Token: 0x0400001E RID: 30
		private global::jp.co.fsi.common.controls.FsiTextBox txtGengoYearTo;

		// Token: 0x0400001F RID: 31
		private global::System.Windows.Forms.Label lblGengoTo;

		// Token: 0x04000021 RID: 33
		private global::System.Windows.Forms.Label lblDateBet2;

		// Token: 0x04000022 RID: 34
		private global::System.Windows.Forms.Label lblFunanushiNmTo;

		// Token: 0x04000023 RID: 35
		private global::jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdTo;

		// Token: 0x04000024 RID: 36
		private global::System.Windows.Forms.Label lblDateBet3;

		// Token: 0x04000025 RID: 37
		private global::System.Windows.Forms.Label lblTantoshaNmTo;

		// Token: 0x04000026 RID: 38
		private global::jp.co.fsi.common.controls.FsiTextBox txtTantoshaCdTo;

		// Token: 0x04000027 RID: 39
		protected global::System.Windows.Forms.Button btnEnter;

		// Token: 0x04000028 RID: 40
		private global::System.Windows.Forms.Label lblShohinNmTo;

		// Token: 0x04000029 RID: 41
		private global::jp.co.fsi.common.controls.FsiTextBox txtShohinCdTo;

		// Token: 0x0400002A RID: 42
		private global::System.Windows.Forms.Label label2;

		// Token: 0x0400002B RID: 43
		private global::System.Windows.Forms.Label lblShohinNmFr;

		// Token: 0x0400002C RID: 44
		private global::jp.co.fsi.common.controls.FsiTextBox txtShohinCdFr;

		// Token: 0x0400002D RID: 45
		private global::System.Windows.Forms.Label lblShohinCd;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}
