﻿namespace jp.co.fsi.kb.kbyr1021
{
    /// <summary>
    /// KBYR10211R の概要の説明です。
    /// </summary>
    partial class KBYR10211R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBYR10211R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateFr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBet = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupList = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnSFtr = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKomoku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKomoku01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKomoku02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKomoku03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKomoku04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKomoku05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKomoku06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKomoku07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox3,
            this.txtCompanyName,
            this.txtTitleName,
            this.txtDateFr,
            this.txtDateTo,
            this.txtToday,
            this.lblPage,
            this.lblBet,
            this.txtPageCount,
            this.txtGroupList,
            this.txtTitle,
            this.txtTitle01,
            this.txtTitle02,
            this.txtTitle03,
            this.txtTitle04,
            this.txtTitle05,
            this.txtTitle06,
            this.txtTitle07,
            this.lnSFtr,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line9,
            this.line10,
            this.line11,
            this.line8,
            this.line20});
            this.pageHeader.Height = 0.864567F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM14";
            this.textBox3.Height = 0.1968504F;
            this.textBox3.Left = 6.90748F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.textBox3.Text = "計";
            this.textBox3.Top = 0.6574803F;
            this.textBox3.Width = 0.7618111F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtCompanyName.Text = "ＮＮＮＮＮＮＮＮＮＮＮＮＮＮＮ";
            this.txtCompanyName.Top = 0.09055119F;
            this.txtCompanyName.Width = 1.531102F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.DataField = "ITEM02";
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 2.288189F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ ゴシック; font-size: 15.75pt; font-weight: bold; text-align: center; " +
    "text-decoration: underline; vertical-align: middle";
            this.txtTitleName.Text = "ＮＮＮＮＮＮＮＮＮＮＮＮＮＮＮ";
            this.txtTitleName.Top = 0F;
            this.txtTitleName.Width = 2.833465F;
            // 
            // txtDateFr
            // 
            this.txtDateFr.DataField = "ITEM03";
            this.txtDateFr.Height = 0.1968504F;
            this.txtDateFr.Left = 0F;
            this.txtDateFr.MultiLine = false;
            this.txtDateFr.Name = "txtDateFr";
            this.txtDateFr.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtDateFr.Text = "ＮＮＮＮＮＮＮＮＮＮＮＮＮＮＮ";
            this.txtDateFr.Top = 0.3779528F;
            this.txtDateFr.Width = 1.166535F;
            // 
            // txtDateTo
            // 
            this.txtDateTo.DataField = "ITEM04";
            this.txtDateTo.Height = 0.1968504F;
            this.txtDateTo.Left = 1.409843F;
            this.txtDateTo.MultiLine = false;
            this.txtDateTo.Name = "txtDateTo";
            this.txtDateTo.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtDateTo.Text = "ＮＮＮＮＮＮＮＮＮＮＮＮＮＮＮ";
            this.txtDateTo.Top = 0.3779528F;
            this.txtDateTo.Width = 1.166535F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM26";
            this.txtToday.Height = 0.1968504F;
            this.txtToday.Left = 5.6F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtToday.Text = "yyyy/MM/dd";
            this.txtToday.Top = 0.09055119F;
            this.txtToday.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.128347F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.09055119F;
            this.lblPage.Width = 0.1590552F;
            // 
            // lblBet
            // 
            this.lblBet.Height = 0.1968504F;
            this.lblBet.HyperLink = null;
            this.lblBet.Left = 1.179134F;
            this.lblBet.Name = "lblBet";
            this.lblBet.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center; vertical-align: midd" +
    "le";
            this.lblBet.Text = "～";
            this.lblBet.Top = 0.3818898F;
            this.lblBet.Width = 0.2110236F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 6.812599F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtPageCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.09055119F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // txtGroupList
            // 
            this.txtGroupList.DataField = "ITEM05";
            this.txtGroupList.Height = 0.1968504F;
            this.txtGroupList.Left = 2.735433F;
            this.txtGroupList.MultiLine = false;
            this.txtGroupList.Name = "txtGroupList";
            this.txtGroupList.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtGroupList.Text = "ＮＮＮＮＮＮＮＮＮＮＮＮＮＮＮ";
            this.txtGroupList.Top = 0.3818898F;
            this.txtGroupList.Width = 4.551969F;
            // 
            // txtTitle
            // 
            this.txtTitle.DataField = "ITEM06";
            this.txtTitle.Height = 0.1968504F;
            this.txtTitle.Left = 0F;
            this.txtTitle.MultiLine = false;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.txtTitle.Text = "Title";
            this.txtTitle.Top = 0.6574804F;
            this.txtTitle.Width = 1.562992F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.DataField = "ITEM07";
            this.txtTitle01.Height = 0.1968504F;
            this.txtTitle01.Left = 1.574803F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.txtTitle01.Text = "Title01";
            this.txtTitle01.Top = 0.6574804F;
            this.txtTitle01.Width = 0.761811F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.DataField = "ITEM08";
            this.txtTitle02.Height = 0.1968504F;
            this.txtTitle02.Left = 2.336614F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.txtTitle02.Text = "Title02";
            this.txtTitle02.Top = 0.6574804F;
            this.txtTitle02.Width = 0.7618113F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.DataField = "ITEM09";
            this.txtTitle03.Height = 0.1968504F;
            this.txtTitle03.Left = 3.098425F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.txtTitle03.Text = "Title03";
            this.txtTitle03.Top = 0.6614174F;
            this.txtTitle03.Width = 0.7618115F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.DataField = "ITEM10";
            this.txtTitle04.Height = 0.1968504F;
            this.txtTitle04.Left = 3.860236F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.txtTitle04.Text = "Title04";
            this.txtTitle04.Top = 0.6574804F;
            this.txtTitle04.Width = 0.7618113F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.DataField = "ITEM11";
            this.txtTitle05.Height = 0.1968504F;
            this.txtTitle05.Left = 4.622047F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.txtTitle05.Text = "Title05";
            this.txtTitle05.Top = 0.6574804F;
            this.txtTitle05.Width = 0.7618113F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.DataField = "ITEM12";
            this.txtTitle06.Height = 0.1968504F;
            this.txtTitle06.Left = 5.383858F;
            this.txtTitle06.MultiLine = false;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.txtTitle06.Text = "Title06";
            this.txtTitle06.Top = 0.6614174F;
            this.txtTitle06.Width = 0.7618113F;
            // 
            // txtTitle07
            // 
            this.txtTitle07.DataField = "ITEM13";
            this.txtTitle07.Height = 0.1968504F;
            this.txtTitle07.Left = 6.145669F;
            this.txtTitle07.MultiLine = false;
            this.txtTitle07.Name = "txtTitle07";
            this.txtTitle07.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.txtTitle07.Text = "Title07";
            this.txtTitle07.Top = 0.6574804F;
            this.txtTitle07.Width = 0.7618113F;
            // 
            // lnSFtr
            // 
            this.lnSFtr.Height = 0F;
            this.lnSFtr.Left = 0F;
            this.lnSFtr.LineWeight = 1F;
            this.lnSFtr.Name = "lnSFtr";
            this.lnSFtr.Top = 0.8582678F;
            this.lnSFtr.Width = 7.669291F;
            this.lnSFtr.X1 = 0F;
            this.lnSFtr.X2 = 7.669291F;
            this.lnSFtr.Y1 = 0.8582678F;
            this.lnSFtr.Y2 = 0.8582678F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.6574804F;
            this.line1.Width = 7.669291F;
            this.line1.X1 = 0F;
            this.line1.X2 = 7.669291F;
            this.line1.Y1 = 0.6574804F;
            this.line1.Y2 = 0.6574804F;
            // 
            // line2
            // 
            this.line2.Height = 0.1968503F;
            this.line2.Left = 1.574803F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.6574804F;
            this.line2.Width = 0F;
            this.line2.X1 = 1.574803F;
            this.line2.X2 = 1.574803F;
            this.line2.Y1 = 0.6574804F;
            this.line2.Y2 = 0.8543307F;
            // 
            // line3
            // 
            this.line3.Height = 0.1968501F;
            this.line3.Left = 2.336614F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.6614174F;
            this.line3.Width = 0F;
            this.line3.X1 = 2.336614F;
            this.line3.X2 = 2.336614F;
            this.line3.Y1 = 0.6614174F;
            this.line3.Y2 = 0.8582675F;
            // 
            // line4
            // 
            this.line4.Height = 0.1968501F;
            this.line4.Left = 3.098425F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.6614174F;
            this.line4.Width = 0F;
            this.line4.X1 = 3.098425F;
            this.line4.X2 = 3.098425F;
            this.line4.Y1 = 0.6614174F;
            this.line4.Y2 = 0.8582675F;
            // 
            // line5
            // 
            this.line5.Height = 0.1968501F;
            this.line5.Left = 3.860236F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.6614174F;
            this.line5.Width = 0F;
            this.line5.X1 = 3.860236F;
            this.line5.X2 = 3.860236F;
            this.line5.Y1 = 0.6614174F;
            this.line5.Y2 = 0.8582675F;
            // 
            // line6
            // 
            this.line6.Height = 0.1968501F;
            this.line6.Left = 4.622047F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.6574804F;
            this.line6.Width = 0F;
            this.line6.X1 = 4.622047F;
            this.line6.X2 = 4.622047F;
            this.line6.Y1 = 0.6574804F;
            this.line6.Y2 = 0.8543305F;
            // 
            // line7
            // 
            this.line7.Height = 0.1968501F;
            this.line7.Left = 5.383858F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.6614174F;
            this.line7.Width = 0F;
            this.line7.X1 = 5.383858F;
            this.line7.X2 = 5.383858F;
            this.line7.Y1 = 0.6614174F;
            this.line7.Y2 = 0.8582675F;
            // 
            // line9
            // 
            this.line9.Height = 0.1968501F;
            this.line9.Left = 6.145669F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0.6614174F;
            this.line9.Width = 0F;
            this.line9.X1 = 6.145669F;
            this.line9.X2 = 6.145669F;
            this.line9.Y1 = 0.6614174F;
            this.line9.Y2 = 0.8582675F;
            // 
            // line10
            // 
            this.line10.Height = 0.1968502F;
            this.line10.Left = 7.669291F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0.6574804F;
            this.line10.Width = 0F;
            this.line10.X1 = 7.669291F;
            this.line10.X2 = 7.669291F;
            this.line10.Y1 = 0.6574804F;
            this.line10.Y2 = 0.8543306F;
            // 
            // line11
            // 
            this.line11.Height = 0.1968502F;
            this.line11.Left = 0F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0.6574804F;
            this.line11.Width = 0F;
            this.line11.X1 = 0F;
            this.line11.X2 = 0F;
            this.line11.Y1 = 0.6574804F;
            this.line11.Y2 = 0.8543306F;
            // 
            // line8
            // 
            this.line8.Height = 0.1968501F;
            this.line8.Left = 0F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.6614174F;
            this.line8.Width = 0F;
            this.line8.X1 = 0F;
            this.line8.X2 = 0F;
            this.line8.Y1 = 0.6614174F;
            this.line8.Y2 = 0.8582675F;
            // 
            // line20
            // 
            this.line20.Height = 0.1968501F;
            this.line20.Left = 6.90748F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.6677166F;
            this.line20.Width = 0F;
            this.line20.X1 = 6.90748F;
            this.line20.X2 = 6.90748F;
            this.line20.Y1 = 0.6677166F;
            this.line20.Y2 = 0.8645667F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM25";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox2,
            this.textBox1,
            this.txtKomoku,
            this.txtKomoku01,
            this.txtKomoku02,
            this.txtKomoku03,
            this.txtKomoku04,
            this.txtKomoku05,
            this.txtKomoku06,
            this.txtKomoku07,
            this.line12,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line21,
            this.line13,
            this.line22,
            this.line23});
            this.detail.Height = 0.2027559F;
            this.detail.Name = "detail";
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM24";
            this.textBox2.Height = 0.1968504F;
            this.textBox2.Left = 6.90748F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: right; vertical-align: middle";
            this.textBox2.Text = null;
            this.textBox2.Top = 0F;
            this.textBox2.Width = 0.7618111F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM16";
            this.textBox1.Height = 0.1968504F;
            this.textBox1.Left = 0.3228347F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.textBox1.Text = "あいうえおかきくけこ";
            this.textBox1.Top = 0F;
            this.textBox1.Width = 1.208268F;
            // 
            // txtKomoku
            // 
            this.txtKomoku.DataField = "ITEM15";
            this.txtKomoku.Height = 0.1968504F;
            this.txtKomoku.Left = 0.03937008F;
            this.txtKomoku.MultiLine = false;
            this.txtKomoku.Name = "txtKomoku";
            this.txtKomoku.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: left; vertical-align: middle";
            this.txtKomoku.Text = "9999";
            this.txtKomoku.Top = 0F;
            this.txtKomoku.Width = 0.2834646F;
            // 
            // txtKomoku01
            // 
            this.txtKomoku01.DataField = "ITEM17";
            this.txtKomoku01.Height = 0.1968504F;
            this.txtKomoku01.Left = 1.574803F;
            this.txtKomoku01.MultiLine = false;
            this.txtKomoku01.Name = "txtKomoku01";
            this.txtKomoku01.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKomoku01.Text = "321,234,567";
            this.txtKomoku01.Top = 0F;
            this.txtKomoku01.Width = 0.761811F;
            // 
            // txtKomoku02
            // 
            this.txtKomoku02.DataField = "ITEM18";
            this.txtKomoku02.Height = 0.1968504F;
            this.txtKomoku02.Left = 2.336614F;
            this.txtKomoku02.MultiLine = false;
            this.txtKomoku02.Name = "txtKomoku02";
            this.txtKomoku02.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKomoku02.Text = null;
            this.txtKomoku02.Top = 0F;
            this.txtKomoku02.Width = 0.761811F;
            // 
            // txtKomoku03
            // 
            this.txtKomoku03.DataField = "ITEM19";
            this.txtKomoku03.Height = 0.1968504F;
            this.txtKomoku03.Left = 3.098425F;
            this.txtKomoku03.MultiLine = false;
            this.txtKomoku03.Name = "txtKomoku03";
            this.txtKomoku03.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKomoku03.Text = null;
            this.txtKomoku03.Top = 0.003937006F;
            this.txtKomoku03.Width = 0.761811F;
            // 
            // txtKomoku04
            // 
            this.txtKomoku04.DataField = "ITEM20";
            this.txtKomoku04.Height = 0.1968504F;
            this.txtKomoku04.Left = 3.860236F;
            this.txtKomoku04.MultiLine = false;
            this.txtKomoku04.Name = "txtKomoku04";
            this.txtKomoku04.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKomoku04.Text = null;
            this.txtKomoku04.Top = 0F;
            this.txtKomoku04.Width = 0.761811F;
            // 
            // txtKomoku05
            // 
            this.txtKomoku05.DataField = "ITEM21";
            this.txtKomoku05.Height = 0.1968504F;
            this.txtKomoku05.Left = 4.622047F;
            this.txtKomoku05.MultiLine = false;
            this.txtKomoku05.Name = "txtKomoku05";
            this.txtKomoku05.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKomoku05.Text = null;
            this.txtKomoku05.Top = 2.328306E-09F;
            this.txtKomoku05.Width = 0.761811F;
            // 
            // txtKomoku06
            // 
            this.txtKomoku06.DataField = "ITEM22";
            this.txtKomoku06.Height = 0.1968504F;
            this.txtKomoku06.Left = 5.383858F;
            this.txtKomoku06.MultiLine = false;
            this.txtKomoku06.Name = "txtKomoku06";
            this.txtKomoku06.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKomoku06.Text = null;
            this.txtKomoku06.Top = 0.003937008F;
            this.txtKomoku06.Width = 0.7618113F;
            // 
            // txtKomoku07
            // 
            this.txtKomoku07.DataField = "ITEM23";
            this.txtKomoku07.Height = 0.1968504F;
            this.txtKomoku07.Left = 6.145669F;
            this.txtKomoku07.MultiLine = false;
            this.txtKomoku07.Name = "txtKomoku07";
            this.txtKomoku07.Style = "font-family: ＭＳ ゴシック; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKomoku07.Text = null;
            this.txtKomoku07.Top = 0F;
            this.txtKomoku07.Width = 0.7618113F;
            // 
            // line12
            // 
            this.line12.Height = 0F;
            this.line12.Left = 0F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.2007878F;
            this.line12.Width = 7.669291F;
            this.line12.X1 = 0F;
            this.line12.X2 = 7.669291F;
            this.line12.Y1 = 0.2007878F;
            this.line12.Y2 = 0.2007878F;
            // 
            // line14
            // 
            this.line14.Height = 0.1968502F;
            this.line14.Left = 1.574803F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 1.574803F;
            this.line14.X2 = 1.574803F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.1968502F;
            // 
            // line15
            // 
            this.line15.Height = 0.19685F;
            this.line15.Left = 2.336614F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0.003937006F;
            this.line15.Width = 0F;
            this.line15.X1 = 2.336614F;
            this.line15.X2 = 2.336614F;
            this.line15.Y1 = 0.003937006F;
            this.line15.Y2 = 0.200787F;
            // 
            // line16
            // 
            this.line16.Height = 0.19685F;
            this.line16.Left = 3.098425F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.003937006F;
            this.line16.Width = 0F;
            this.line16.X1 = 3.098425F;
            this.line16.X2 = 3.098425F;
            this.line16.Y1 = 0.003937006F;
            this.line16.Y2 = 0.200787F;
            // 
            // line17
            // 
            this.line17.Height = 0.19685F;
            this.line17.Left = 3.860236F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0.003937006F;
            this.line17.Width = 0F;
            this.line17.X1 = 3.860236F;
            this.line17.X2 = 3.860236F;
            this.line17.Y1 = 0.003937006F;
            this.line17.Y2 = 0.200787F;
            // 
            // line18
            // 
            this.line18.Height = 0.1968502F;
            this.line18.Left = 4.622047F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 4.622047F;
            this.line18.X2 = 4.622047F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.1968502F;
            // 
            // line19
            // 
            this.line19.Height = 0.19685F;
            this.line19.Left = 5.383858F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.003937008F;
            this.line19.Width = 0F;
            this.line19.X1 = 5.383858F;
            this.line19.X2 = 5.383858F;
            this.line19.Y1 = 0.003937008F;
            this.line19.Y2 = 0.200787F;
            // 
            // line21
            // 
            this.line21.Height = 0.19685F;
            this.line21.Left = 6.145669F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.003937006F;
            this.line21.Width = 0F;
            this.line21.X1 = 6.145669F;
            this.line21.X2 = 6.145669F;
            this.line21.Y1 = 0.003937006F;
            this.line21.Y2 = 0.200787F;
            // 
            // line13
            // 
            this.line13.Height = 0.1968502F;
            this.line13.Left = 0F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 0F;
            this.line13.X2 = 0F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.1968502F;
            // 
            // line22
            // 
            this.line22.Height = 0.1968502F;
            this.line22.Left = 6.90748F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.003937006F;
            this.line22.Width = 0F;
            this.line22.X1 = 6.90748F;
            this.line22.X2 = 6.90748F;
            this.line22.Y1 = 0.003937006F;
            this.line22.Y2 = 0.2007872F;
            // 
            // line23
            // 
            this.line23.Height = 0.1968504F;
            this.line23.Left = 7.669291F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.003937008F;
            this.line23.Width = 0F;
            this.line23.X1 = 7.669291F;
            this.line23.X2 = 7.669291F;
            this.line23.Y1 = 0.003937008F;
            this.line23.Y2 = 0.2007874F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0.2235072F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KBYR10211R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.2905512F;
            this.PageSettings.Margins.Right = 0.2937008F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.677166F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKomoku07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateFr;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateTo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBet;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupList;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnSFtr;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKomoku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKomoku01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKomoku02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKomoku03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKomoku04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKomoku05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKomoku06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKomoku07;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
    }
}
