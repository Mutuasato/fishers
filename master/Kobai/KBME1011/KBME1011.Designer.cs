﻿namespace jp.co.fsi.kb.kbme1011
{
    partial class KBME1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDay = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGengoYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.lblDifferent = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblShohinKbn1 = new System.Windows.Forms.Label();
            this.txtTanabanCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateBet1 = new System.Windows.Forms.Label();
            this.lblTanabanNmTo = new System.Windows.Forms.Label();
            this.lblTanabanNmFr = new System.Windows.Forms.Label();
            this.txtTanabanCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCd = new System.Windows.Forms.Label();
            this.lblShohinKbn1Nm = new System.Windows.Forms.Label();
            this.txtShohinKbn1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.lblWaitMsg = new System.Windows.Forms.Label();
            this.pnlWait = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.pnlWait.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 708);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(2130, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(2119, 41);
            this.lblTitle.Text = "棚卸入力";
            // 
            // lblDay
            // 
            this.lblDay.AutoSize = true;
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDay.Location = new System.Drawing.Point(392, 14);
            this.lblDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(24, 16);
            this.lblDay.TabIndex = 9;
            this.lblDay.Tag = "CHANGE";
            this.lblDay.Text = "日";
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.Location = new System.Drawing.Point(300, 14);
            this.lblMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(24, 16);
            this.lblMonth.TabIndex = 7;
            this.lblMonth.Tag = "CHANGE";
            this.lblMonth.Text = "月";
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = false;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDay.Location = new System.Drawing.Point(331, 7);
            this.txtDay.Margin = new System.Windows.Forms.Padding(4);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(52, 23);
            this.txtDay.TabIndex = 8;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(211, 14);
            this.lblYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(24, 16);
            this.lblYear.TabIndex = 5;
            this.lblYear.Tag = "CHANGE";
            this.lblYear.Text = "年";
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.Location = new System.Drawing.Point(242, 7);
            this.txtMonth.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(52, 23);
            this.txtMonth.TabIndex = 6;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // txtGengoYear
            // 
            this.txtGengoYear.AutoSizeFromLength = false;
            this.txtGengoYear.DisplayLength = null;
            this.txtGengoYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYear.Location = new System.Drawing.Point(152, 7);
            this.txtGengoYear.Margin = new System.Windows.Forms.Padding(4);
            this.txtGengoYear.MaxLength = 2;
            this.txtGengoYear.Name = "txtGengoYear";
            this.txtGengoYear.Size = new System.Drawing.Size(52, 23);
            this.txtGengoYear.TabIndex = 4;
            this.txtGengoYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYear_Validating);
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengo.Location = new System.Drawing.Point(92, 6);
            this.lblGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(53, 24);
            this.lblGengo.TabIndex = 2;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mtbList
            // 
            this.mtbList.BackColor = System.Drawing.Color.White;
            this.mtbList.FixedCols = 0;
            this.mtbList.FocusField = null;
            this.mtbList.ForeColor = System.Drawing.SystemColors.ControlText;
            this.mtbList.Location = new System.Drawing.Point(16, 156);
            this.mtbList.Margin = new System.Windows.Forms.Padding(4);
            this.mtbList.Name = "mtbList";
            this.mtbList.NotSelectableCols = 0;
            this.mtbList.SelectRange = null;
            this.mtbList.Size = new System.Drawing.Size(1065, 518);
            this.mtbList.TabIndex = 902;
            this.mtbList.Text = "sosMultiTable1";
            this.mtbList.UndoBufferEnabled = false;
            this.mtbList.FieldValidating += new systembase.table.UTable.FieldValidatingEventHandler(this.mtbList_FieldValidating);
            this.mtbList.FieldEnter += new systembase.table.UTable.FieldEnterEventHandler(this.mtbList_FieldEnter);
            this.mtbList.EditStart += new systembase.table.UTable.EditStartEventHandler(this.mtbList_EditStart);
            this.mtbList.InitializeEditor += new systembase.table.UTable.InitializeEditorEventHandler(this.mtbList_InitializeEditor);
            this.mtbList.Leave += new System.EventHandler(this.mtbList_Leave);
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(501, 7);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 0;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(549, 6);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(311, 24);
            this.lblMizuageShishoNm.TabIndex = 907;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.AutoSize = true;
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShisho.Location = new System.Drawing.Point(441, 6);
            this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShisho.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(40, 24);
            this.lblMizuageShisho.TabIndex = 906;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDifferent
            // 
            this.lblDifferent.AutoSize = true;
            this.lblDifferent.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDifferent.Location = new System.Drawing.Point(751, 684);
            this.lblDifferent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDifferent.Name = "lblDifferent";
            this.lblDifferent.Size = new System.Drawing.Size(0, 15);
            this.lblDifferent.TabIndex = 908;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(11, 49);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 2;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1070, 91);
            this.fsiTableLayoutPanel1.TabIndex = 910;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.lblGengo);
            this.fsiPanel1.Controls.Add(this.txtGengoYear);
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.txtMonth);
            this.fsiPanel1.Controls.Add(this.lblYear);
            this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
            this.fsiPanel1.Controls.Add(this.txtDay);
            this.fsiPanel1.Controls.Add(this.lblMonth);
            this.fsiPanel1.Controls.Add(this.lblDay);
            this.fsiPanel1.Controls.Add(this.label2);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(1062, 38);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1062, 38);
            this.label2.TabIndex = 2;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "棚卸日付";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbn1
            // 
            this.lblShohinKbn1.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbn1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKbn1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShohinKbn1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn1.Location = new System.Drawing.Point(0, 0);
            this.lblShohinKbn1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKbn1.Name = "lblShohinKbn1";
            this.lblShohinKbn1.Size = new System.Drawing.Size(1062, 38);
            this.lblShohinKbn1.TabIndex = 10;
            this.lblShohinKbn1.Tag = "CHANGE";
            this.lblShohinKbn1.Text = "商品区分１";
            this.lblShohinKbn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTanabanCdTo
            // 
            this.txtTanabanCdTo.AutoSizeFromLength = true;
            this.txtTanabanCdTo.DisplayLength = null;
            this.txtTanabanCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTanabanCdTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTanabanCdTo.Location = new System.Drawing.Point(699, 6);
            this.txtTanabanCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTanabanCdTo.MaxLength = 8;
            this.txtTanabanCdTo.Name = "txtTanabanCdTo";
            this.txtTanabanCdTo.Size = new System.Drawing.Size(63, 23);
            this.txtTanabanCdTo.TabIndex = 17;
            this.txtTanabanCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTanabanCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTanabanCdTo_Validating);
            // 
            // lblDateBet1
            // 
            this.lblDateBet1.BackColor = System.Drawing.Color.Silver;
            this.lblDateBet1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateBet1.Location = new System.Drawing.Point(670, 5);
            this.lblDateBet1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateBet1.Name = "lblDateBet1";
            this.lblDateBet1.Size = new System.Drawing.Size(24, 24);
            this.lblDateBet1.TabIndex = 16;
            this.lblDateBet1.Tag = "CHANGE";
            this.lblDateBet1.Text = "～";
            this.lblDateBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTanabanNmTo
            // 
            this.lblTanabanNmTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblTanabanNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTanabanNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTanabanNmTo.Location = new System.Drawing.Point(765, 5);
            this.lblTanabanNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTanabanNmTo.Name = "lblTanabanNmTo";
            this.lblTanabanNmTo.Size = new System.Drawing.Size(95, 24);
            this.lblTanabanNmTo.TabIndex = 18;
            this.lblTanabanNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTanabanNmFr
            // 
            this.lblTanabanNmFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblTanabanNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTanabanNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTanabanNmFr.Location = new System.Drawing.Point(568, 5);
            this.lblTanabanNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTanabanNmFr.Name = "lblTanabanNmFr";
            this.lblTanabanNmFr.Size = new System.Drawing.Size(95, 24);
            this.lblTanabanNmFr.TabIndex = 15;
            this.lblTanabanNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTanabanCdFr
            // 
            this.txtTanabanCdFr.AutoSizeFromLength = true;
            this.txtTanabanCdFr.DisplayLength = null;
            this.txtTanabanCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTanabanCdFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTanabanCdFr.Location = new System.Drawing.Point(501, 6);
            this.txtTanabanCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtTanabanCdFr.MaxLength = 8;
            this.txtTanabanCdFr.Name = "txtTanabanCdFr";
            this.txtTanabanCdFr.Size = new System.Drawing.Size(63, 23);
            this.txtTanabanCdFr.TabIndex = 14;
            this.txtTanabanCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTanabanCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTanabanCdFr_Validating);
            // 
            // lblFunanushiCd
            // 
            this.lblFunanushiCd.AutoSize = true;
            this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCd.Location = new System.Drawing.Point(441, 5);
            this.lblFunanushiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanushiCd.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblFunanushiCd.Name = "lblFunanushiCd";
            this.lblFunanushiCd.Size = new System.Drawing.Size(56, 24);
            this.lblFunanushiCd.TabIndex = 13;
            this.lblFunanushiCd.Tag = "CHANGE";
            this.lblFunanushiCd.Text = "棚　番";
            this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbn1Nm
            // 
            this.lblShohinKbn1Nm.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinKbn1Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKbn1Nm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn1Nm.Location = new System.Drawing.Point(140, 5);
            this.lblShohinKbn1Nm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKbn1Nm.Name = "lblShohinKbn1Nm";
            this.lblShohinKbn1Nm.Size = new System.Drawing.Size(295, 24);
            this.lblShohinKbn1Nm.TabIndex = 12;
            this.lblShohinKbn1Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKbn1
            // 
            this.txtShohinKbn1.AutoSizeFromLength = true;
            this.txtShohinKbn1.DisplayLength = null;
            this.txtShohinKbn1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKbn1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohinKbn1.Location = new System.Drawing.Point(92, 6);
            this.txtShohinKbn1.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohinKbn1.MaxLength = 4;
            this.txtShohinKbn1.Name = "txtShohinKbn1";
            this.txtShohinKbn1.Size = new System.Drawing.Size(44, 23);
            this.txtShohinKbn1.TabIndex = 11;
            this.txtShohinKbn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKbn1.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbn1_Validating);
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtShohinKbn1);
            this.fsiPanel2.Controls.Add(this.lblShohinKbn1Nm);
            this.fsiPanel2.Controls.Add(this.lblFunanushiCd);
            this.fsiPanel2.Controls.Add(this.txtTanabanCdFr);
            this.fsiPanel2.Controls.Add(this.lblTanabanNmFr);
            this.fsiPanel2.Controls.Add(this.lblTanabanNmTo);
            this.fsiPanel2.Controls.Add(this.lblDateBet1);
            this.fsiPanel2.Controls.Add(this.txtTanabanCdTo);
            this.fsiPanel2.Controls.Add(this.lblShohinKbn1);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 49);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(1062, 38);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // lblWaitMsg
            // 
            this.lblWaitMsg.BackColor = System.Drawing.Color.Transparent;
            this.lblWaitMsg.Font = new System.Drawing.Font("ＭＳ ゴシック", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblWaitMsg.Location = new System.Drawing.Point(4, 84);
            this.lblWaitMsg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWaitMsg.Name = "lblWaitMsg";
            this.lblWaitMsg.Size = new System.Drawing.Size(605, 33);
            this.lblWaitMsg.TabIndex = 14;
            this.lblWaitMsg.Text = "しばらくお待ちください...";
            this.lblWaitMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlWait
            // 
            this.pnlWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pnlWait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlWait.Controls.Add(this.lblWaitMsg);
            this.pnlWait.Font = new System.Drawing.Font("ＭＳ 明朝", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlWait.Location = new System.Drawing.Point(231, 235);
            this.pnlWait.Margin = new System.Windows.Forms.Padding(4);
            this.pnlWait.Name = "pnlWait";
            this.pnlWait.Size = new System.Drawing.Size(615, 198);
            this.pnlWait.TabIndex = 903;
            this.pnlWait.Visible = false;
            // 
            // KBME1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2119, 845);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.lblDifferent);
            this.Controls.Add(this.pnlWait);
            this.Controls.Add(this.mtbList);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBME1011";
            this.Text = "棚卸入力";
            this.Controls.SetChildIndex(this.mtbList, 0);
            this.Controls.SetChildIndex(this.pnlWait, 0);
            this.Controls.SetChildIndex(this.lblDifferent, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.pnlWait.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private common.controls.FsiTextBox txtDay;
        private System.Windows.Forms.Label lblYear;
        private common.controls.FsiTextBox txtMonth;
        private common.controls.FsiTextBox txtGengoYear;
        private System.Windows.Forms.Label lblGengo;
        private common.controls.SjMultiTable mtbList;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label lblDifferent;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel2;
        private common.controls.FsiTextBox txtShohinKbn1;
        private System.Windows.Forms.Label lblShohinKbn1Nm;
        private System.Windows.Forms.Label lblFunanushiCd;
        private common.controls.FsiTextBox txtTanabanCdFr;
        private System.Windows.Forms.Label lblTanabanNmFr;
        private System.Windows.Forms.Label lblTanabanNmTo;
        private System.Windows.Forms.Label lblDateBet1;
        private common.controls.FsiTextBox txtTanabanCdTo;
        private System.Windows.Forms.Label lblShohinKbn1;
        private System.Windows.Forms.Label lblWaitMsg;
        private common.FsiPanel pnlWait;
    }
}