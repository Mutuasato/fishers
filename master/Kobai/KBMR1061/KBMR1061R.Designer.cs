﻿namespace jp.co.fsi.kb.kbmr1061
{
    /// <summary>
    /// KBMR1061R の概要の説明です。
    /// </summary>
    partial class KBMR1061R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBMR1061R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル229 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル229)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label9,
            this.label15,
            this.label13,
            this.label16,
            this.label11,
            this.label12,
            this.label10,
            this.ラベル229,
            this.label17,
            this.label14,
            this.shape4,
            this.shape3,
            this.shape2,
            this.shape1,
            this.lblDate,
            this.txtToday,
            this.txtPage,
            this.lblPage,
            this.lblSuryo01,
            this.lblKingaku01,
            this.textBox1,
            this.lblTitle01,
            this.label1,
            this.shape5,
            this.shape6,
            this.shape7,
            this.shape8,
            this.shape9,
            this.shape11,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.textBox11,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25});
            this.pageHeader.Height = 0.9506068F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // label9
            // 
            this.label9.Height = 0.26F;
            this.label9.HyperLink = null;
            this.label9.Left = 1.104F;
            this.label9.Name = "label9";
            this.label9.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label9.Tag = "";
            this.label9.Text = "";
            this.label9.Top = 0.6850001F;
            this.label9.Width = 2.392F;
            // 
            // label15
            // 
            this.label15.Height = 0.26F;
            this.label15.HyperLink = null;
            this.label15.Left = 7.439F;
            this.label15.Name = "label15";
            this.label15.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label15.Tag = "";
            this.label15.Text = "";
            this.label15.Top = 0.6850001F;
            this.label15.Width = 0.9570006F;
            // 
            // label13
            // 
            this.label13.Height = 0.26F;
            this.label13.HyperLink = null;
            this.label13.Left = 6.026F;
            this.label13.Name = "label13";
            this.label13.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label13.Tag = "";
            this.label13.Text = "";
            this.label13.Top = 0.6850001F;
            this.label13.Width = 0.4449996F;
            // 
            // label16
            // 
            this.label16.Height = 0.26F;
            this.label16.HyperLink = null;
            this.label16.Left = 8.396001F;
            this.label16.Name = "label16";
            this.label16.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label16.Tag = "";
            this.label16.Text = "";
            this.label16.Top = 0.6850001F;
            this.label16.Width = 0.9579991F;
            // 
            // label11
            // 
            this.label11.Height = 0.26F;
            this.label11.HyperLink = null;
            this.label11.Left = 4.347F;
            this.label11.Name = "label11";
            this.label11.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label11.Tag = "";
            this.label11.Text = "";
            this.label11.Top = 0.6850001F;
            this.label11.Width = 0.8510002F;
            // 
            // label12
            // 
            this.label12.Height = 0.26F;
            this.label12.HyperLink = null;
            this.label12.Left = 5.198F;
            this.label12.Name = "label12";
            this.label12.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label12.Tag = "";
            this.label12.Text = "";
            this.label12.Top = 0.6850001F;
            this.label12.Width = 0.8279999F;
            // 
            // label10
            // 
            this.label10.Height = 0.26F;
            this.label10.HyperLink = null;
            this.label10.Left = 3.496F;
            this.label10.Name = "label10";
            this.label10.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label10.Tag = "";
            this.label10.Text = "";
            this.label10.Top = 0.6850001F;
            this.label10.Width = 0.8510002F;
            // 
            // ラベル229
            // 
            this.ラベル229.Height = 0.26F;
            this.ラベル229.HyperLink = null;
            this.ラベル229.Left = 0F;
            this.ラベル229.Name = "ラベル229";
            this.ラベル229.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.ラベル229.Tag = "";
            this.ラベル229.Text = "";
            this.ラベル229.Top = 0.6850001F;
            this.ラベル229.Width = 1.104F;
            // 
            // label17
            // 
            this.label17.Height = 0.26F;
            this.label17.HyperLink = null;
            this.label17.Left = 9.354F;
            this.label17.Name = "label17";
            this.label17.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label17.Tag = "";
            this.label17.Text = "";
            this.label17.Top = 0.6850001F;
            this.label17.Width = 1.030001F;
            // 
            // label14
            // 
            this.label14.Height = 0.26F;
            this.label14.HyperLink = null;
            this.label14.Left = 6.471F;
            this.label14.Name = "label14";
            this.label14.Style = "background-color: #AAFFFF; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; fo" +
    "nt-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.label14.Tag = "";
            this.label14.Text = "";
            this.label14.Top = 0.6850001F;
            this.label14.Width = 0.9679998F;
            // 
            // shape4
            // 
            this.shape4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape4.Height = 0.2604167F;
            this.shape4.Left = 4.347F;
            this.shape4.Name = "shape4";
            this.shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape4.Top = 1.33F;
            this.shape4.Visible = false;
            this.shape4.Width = 0.8507875F;
            // 
            // shape3
            // 
            this.shape3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape3.Height = 0.2604167F;
            this.shape3.Left = 3.496F;
            this.shape3.Name = "shape3";
            this.shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape3.Top = 1.33F;
            this.shape3.Visible = false;
            this.shape3.Width = 0.8507878F;
            // 
            // shape2
            // 
            this.shape2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape2.Height = 0.2604167F;
            this.shape2.Left = 1.103F;
            this.shape2.LineWeight = 0F;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape2.Top = 1.33F;
            this.shape2.Visible = false;
            this.shape2.Width = 2.39252F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.Aqua;
            this.shape1.Height = 0.2604167F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape1.Top = 1.33F;
            this.shape1.Visible = false;
            this.shape1.Width = 1.103937F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.2F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 7.927953F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.lblDate.Text = "作 成 日";
            this.lblDate.Top = 0.1212599F;
            this.lblDate.Width = 0.6299214F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM16";
            this.txtToday.Height = 0.2F;
            this.txtToday.Left = 8.636615F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.txtToday.Text = "gy年MM月dd日";
            this.txtToday.Top = 0.1212599F;
            this.txtToday.Width = 1.181103F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.2F;
            this.txtPage.Left = 9.842914F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.1212599F;
            this.txtPage.Width = 0.3748035F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.21772F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.1212599F;
            this.lblPage.Width = 0.1666665F;
            // 
            // lblSuryo01
            // 
            this.lblSuryo01.Height = 0.1574803F;
            this.lblSuryo01.HyperLink = null;
            this.lblSuryo01.Left = 0.063F;
            this.lblSuryo01.Name = "lblSuryo01";
            this.lblSuryo01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblSuryo01.Text = "商品コード";
            this.lblSuryo01.Top = 0.74F;
            this.lblSuryo01.Width = 0.9791338F;
            // 
            // lblKingaku01
            // 
            this.lblKingaku01.Height = 0.1574803F;
            this.lblKingaku01.HyperLink = null;
            this.lblKingaku01.Left = 1.135F;
            this.lblKingaku01.Name = "lblKingaku01";
            this.lblKingaku01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblKingaku01.Text = "商　　品　　名";
            this.lblKingaku01.Top = 0.74F;
            this.lblKingaku01.Width = 2.333504F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM01";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 0.07874016F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-style: normal; font-weight: normal; d" +
    "do-char-set: 128";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.07874016F;
            this.textBox1.Width = 2.926378F;
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.2625984F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 3.710629F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold; ddo-char-set: 1";
            this.lblTitle01.Text = "＊＊＊ 商品管理日報 ＊＊＊";
            this.lblTitle01.Top = 0.1212599F;
            this.lblTitle01.Width = 3.208661F;
            // 
            // label1
            // 
            this.label1.Height = 0.1574803F;
            this.label1.HyperLink = null;
            this.label1.Left = 3.556F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label1.Text = "入庫数";
            this.label1.Top = 0.74F;
            this.label1.Width = 0.7515748F;
            // 
            // shape5
            // 
            this.shape5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape5.Height = 0.2604167F;
            this.shape5.Left = 5.198F;
            this.shape5.Name = "shape5";
            this.shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape5.Top = 1.33F;
            this.shape5.Visible = false;
            this.shape5.Width = 0.8279527F;
            // 
            // shape6
            // 
            this.shape6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape6.Height = 0.2604167F;
            this.shape6.Left = 6.026F;
            this.shape6.Name = "shape6";
            this.shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape6.Top = 1.33F;
            this.shape6.Visible = false;
            this.shape6.Width = 0.4448818F;
            // 
            // shape7
            // 
            this.shape7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape7.Height = 0.2604167F;
            this.shape7.Left = 6.471F;
            this.shape7.Name = "shape7";
            this.shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape7.Top = 1.33F;
            this.shape7.Visible = false;
            this.shape7.Width = 0.9685038F;
            // 
            // shape8
            // 
            this.shape8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape8.Height = 0.2604167F;
            this.shape8.Left = 7.439F;
            this.shape8.Name = "shape8";
            this.shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape8.Top = 1.33F;
            this.shape8.Visible = false;
            this.shape8.Width = 0.9570864F;
            // 
            // shape9
            // 
            this.shape9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape9.Height = 0.2604167F;
            this.shape9.Left = 8.397F;
            this.shape9.Name = "shape9";
            this.shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape9.Top = 1.33F;
            this.shape9.Visible = false;
            this.shape9.Width = 0.9570864F;
            // 
            // shape11
            // 
            this.shape11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape11.Height = 0.2604167F;
            this.shape11.Left = 9.354F;
            this.shape11.Name = "shape11";
            this.shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(9.999999F, null, null, null, null);
            this.shape11.Top = 1.33F;
            this.shape11.Visible = false;
            this.shape11.Width = 1.030709F;
            // 
            // label2
            // 
            this.label2.Height = 0.1574803F;
            this.label2.HyperLink = null;
            this.label2.Left = 4.383F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label2.Text = "出庫数";
            this.label2.Top = 0.74F;
            this.label2.Width = 0.7874016F;
            // 
            // label3
            // 
            this.label3.Height = 0.1574803F;
            this.label3.HyperLink = null;
            this.label3.Left = 5.231F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label3.Text = "在庫数";
            this.label3.Top = 0.74F;
            this.label3.Width = 0.7515749F;
            // 
            // label4
            // 
            this.label4.Height = 0.1574803F;
            this.label4.HyperLink = null;
            this.label4.Left = 6.064F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label4.Text = "単位";
            this.label4.Top = 0.74F;
            this.label4.Width = 0.3722982F;
            // 
            // label5
            // 
            this.label5.Height = 0.1574803F;
            this.label5.HyperLink = null;
            this.label5.Left = 6.51F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label5.Text = "数量(ｹｰｽ)";
            this.label5.Top = 0.74F;
            this.label5.Width = 0.8976378F;
            // 
            // label6
            // 
            this.label6.Height = 0.1574803F;
            this.label6.HyperLink = null;
            this.label6.Left = 7.459001F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label6.Text = "数量(ﾊﾞﾗ)";
            this.label6.Top = 0.74F;
            this.label6.Width = 0.8979998F;
            // 
            // label7
            // 
            this.label7.Height = 0.1574803F;
            this.label7.HyperLink = null;
            this.label7.Left = 8.44252F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label7.Text = "原単価";
            this.label7.Top = 0.7397639F;
            this.label7.Width = 0.889481F;
            // 
            // label8
            // 
            this.label8.Height = 0.1574803F;
            this.label8.HyperLink = null;
            this.label8.Left = 9.392F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label8.Text = "原価金額";
            this.label8.Top = 0.74F;
            this.label8.Width = 0.9568195F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM02";
            this.textBox11.Height = 0.2F;
            this.textBox11.Left = 3.851771F;
            this.textBox11.MultiLine = false;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-style: normal; font-weight: normal; d" +
    "do-char-set: 128";
            this.textBox11.Text = null;
            this.textBox11.Top = 0.403937F;
            this.textBox11.Width = 2.926378F;
            // 
            // line13
            // 
            this.line13.Height = 0.0002363324F;
            this.line13.Left = 0F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0.9447638F;
            this.line13.Width = 10.38425F;
            this.line13.X1 = 0F;
            this.line13.X2 = 10.38425F;
            this.line13.Y1 = 0.9450001F;
            this.line13.Y2 = 0.9447638F;
            // 
            // line14
            // 
            this.line14.Height = 0.2597638F;
            this.line14.Left = 1.104F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0.6850001F;
            this.line14.Width = 0F;
            this.line14.X1 = 1.104F;
            this.line14.X2 = 1.104F;
            this.line14.Y1 = 0.6850001F;
            this.line14.Y2 = 0.9447639F;
            // 
            // line15
            // 
            this.line15.Height = 0.00045681F;
            this.line15.Left = 0F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0.6845433F;
            this.line15.Width = 10.38425F;
            this.line15.X1 = 0F;
            this.line15.X2 = 10.38425F;
            this.line15.Y1 = 0.6850001F;
            this.line15.Y2 = 0.6845433F;
            // 
            // line16
            // 
            this.line16.Height = 0.2597637F;
            this.line16.Left = 0F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.6850001F;
            this.line16.Width = 0F;
            this.line16.X1 = 0F;
            this.line16.X2 = 0F;
            this.line16.Y1 = 0.6850001F;
            this.line16.Y2 = 0.9447638F;
            // 
            // line17
            // 
            this.line17.Height = 0.2597638F;
            this.line17.Left = 3.496F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0.6850001F;
            this.line17.Width = 0F;
            this.line17.X1 = 3.496F;
            this.line17.X2 = 3.496F;
            this.line17.Y1 = 0.6850001F;
            this.line17.Y2 = 0.9447639F;
            // 
            // line18
            // 
            this.line18.Height = 0.2597638F;
            this.line18.Left = 4.347F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0.6850001F;
            this.line18.Width = 0F;
            this.line18.X1 = 4.347F;
            this.line18.X2 = 4.347F;
            this.line18.Y1 = 0.6850001F;
            this.line18.Y2 = 0.9447639F;
            // 
            // line19
            // 
            this.line19.Height = 0.2597638F;
            this.line19.Left = 5.198F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.6850001F;
            this.line19.Width = 0F;
            this.line19.X1 = 5.198F;
            this.line19.X2 = 5.198F;
            this.line19.Y1 = 0.6850001F;
            this.line19.Y2 = 0.9447639F;
            // 
            // line20
            // 
            this.line20.Height = 0.2597638F;
            this.line20.Left = 6.026F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.6850001F;
            this.line20.Width = 0F;
            this.line20.X1 = 6.026F;
            this.line20.X2 = 6.026F;
            this.line20.Y1 = 0.6850001F;
            this.line20.Y2 = 0.9447639F;
            // 
            // line21
            // 
            this.line21.Height = 0.2597638F;
            this.line21.Left = 6.471F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.6850001F;
            this.line21.Width = 0F;
            this.line21.X1 = 6.471F;
            this.line21.X2 = 6.471F;
            this.line21.Y1 = 0.6850001F;
            this.line21.Y2 = 0.9447639F;
            // 
            // line22
            // 
            this.line22.Height = 0.2597638F;
            this.line22.Left = 7.439F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.6850001F;
            this.line22.Width = 0F;
            this.line22.X1 = 7.439F;
            this.line22.X2 = 7.439F;
            this.line22.Y1 = 0.6850001F;
            this.line22.Y2 = 0.9447639F;
            // 
            // line23
            // 
            this.line23.Height = 0.2597638F;
            this.line23.Left = 8.396001F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.6850001F;
            this.line23.Width = 0F;
            this.line23.X1 = 8.396001F;
            this.line23.X2 = 8.396001F;
            this.line23.Y1 = 0.6850001F;
            this.line23.Y2 = 0.9447639F;
            // 
            // line24
            // 
            this.line24.Height = 0.2597638F;
            this.line24.Left = 9.353999F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.6850001F;
            this.line24.Width = 0F;
            this.line24.X1 = 9.353999F;
            this.line24.X2 = 9.353999F;
            this.line24.Y1 = 0.6850001F;
            this.line24.Y2 = 0.9447639F;
            // 
            // line25
            // 
            this.line25.Height = 0.2597638F;
            this.line25.Left = 10.384F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.6850001F;
            this.line25.Width = 0F;
            this.line25.X1 = 10.384F;
            this.line25.X2 = 10.384F;
            this.line25.Y1 = 0.6850001F;
            this.line25.Y2 = 0.9447639F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox12,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12});
            this.detail.Height = 0.2440946F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.RepeatToFill = true;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM03";
            this.textBox2.Height = 0.1665355F;
            this.textBox2.Left = 0.06259843F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox2.Text = "1234567890123";
            this.textBox2.Top = 0.04173229F;
            this.textBox2.Width = 0.9791667F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM04";
            this.textBox3.Height = 0.1665355F;
            this.textBox3.Left = 1.135F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; white-space: nowrap; ddo" +
    "-wrap-mode: nowrap";
            this.textBox3.Text = "１２３４５６７８９０１２３４５６７８９０";
            this.textBox3.Top = 0.04173229F;
            this.textBox3.Width = 2.334F;
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.DataField = "ITEM05";
            this.textBox4.Height = 0.1665355F;
            this.textBox4.Left = 3.555906F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox4.Text = "999,999,999";
            this.textBox4.Top = 0.04173229F;
            this.textBox4.Width = 0.7755906F;
            // 
            // textBox5
            // 
            this.textBox5.CanGrow = false;
            this.textBox5.DataField = "ITEM06";
            this.textBox5.Height = 0.1665355F;
            this.textBox5.Left = 4.383071F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox5.Text = "999,999,999";
            this.textBox5.Top = 0.04173229F;
            this.textBox5.Width = 0.7874016F;
            // 
            // textBox6
            // 
            this.textBox6.CanGrow = false;
            this.textBox6.DataField = "ITEM07";
            this.textBox6.Height = 0.1665355F;
            this.textBox6.Left = 5.222835F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox6.Text = "999,999,999";
            this.textBox6.Top = 0.04173229F;
            this.textBox6.Width = 0.7755908F;
            // 
            // textBox7
            // 
            this.textBox7.CanGrow = false;
            this.textBox7.DataField = "ITEM08";
            this.textBox7.Height = 0.1665355F;
            this.textBox7.Left = 6.064F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center";
            this.textBox7.Text = "9999";
            this.textBox7.Top = 0.042F;
            this.textBox7.Width = 0.3722992F;
            // 
            // textBox8
            // 
            this.textBox8.CanGrow = false;
            this.textBox8.DataField = "ITEM09";
            this.textBox8.Height = 0.1665355F;
            this.textBox8.Left = 6.509843F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox8.Text = "999,999,999";
            this.textBox8.Top = 0.04173229F;
            this.textBox8.Width = 0.8976378F;
            // 
            // textBox9
            // 
            this.textBox9.CanGrow = false;
            this.textBox9.DataField = "ITEM10";
            this.textBox9.Height = 0.1665355F;
            this.textBox9.Left = 7.459056F;
            this.textBox9.MultiLine = false;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox9.Text = "999,999,999";
            this.textBox9.Top = 0.04173229F;
            this.textBox9.Width = 0.8976378F;
            // 
            // textBox10
            // 
            this.textBox10.CanGrow = false;
            this.textBox10.DataField = "ITEM11";
            this.textBox10.Height = 0.1665355F;
            this.textBox10.Left = 8.44252F;
            this.textBox10.MultiLine = false;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox10.Text = "999,999,999";
            this.textBox10.Top = 0.04173229F;
            this.textBox10.Width = 0.8894805F;
            // 
            // textBox12
            // 
            this.textBox12.CanGrow = false;
            this.textBox12.DataField = "ITEM12";
            this.textBox12.Height = 0.1665355F;
            this.textBox12.Left = 9.392F;
            this.textBox12.MultiLine = false;
            this.textBox12.Name = "textBox12";
            this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox12.Text = "9,999,999,999";
            this.textBox12.Top = 0.042F;
            this.textBox12.Width = 0.9570008F;
            // 
            // line1
            // 
            this.line1.Height = 0.2397638F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0F;
            this.line1.Width = 0F;
            this.line1.X1 = 0F;
            this.line1.X2 = 0F;
            this.line1.Y1 = 0F;
            this.line1.Y2 = 0.2397638F;
            // 
            // line2
            // 
            this.line2.Height = 0.003543302F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.2362205F;
            this.line2.Width = 10.38425F;
            this.line2.X1 = 0F;
            this.line2.X2 = 10.38425F;
            this.line2.Y1 = 0.2362205F;
            this.line2.Y2 = 0.2397638F;
            // 
            // line3
            // 
            this.line3.Height = 0.2397638F;
            this.line3.Left = 1.103937F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0F;
            this.line3.Width = 0F;
            this.line3.X1 = 1.103937F;
            this.line3.X2 = 1.103937F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0.2397638F;
            // 
            // line4
            // 
            this.line4.Height = 0.2397638F;
            this.line4.Left = 3.496457F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 3.496457F;
            this.line4.X2 = 3.496457F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0.2397638F;
            // 
            // line5
            // 
            this.line5.Height = 0.2397638F;
            this.line5.Left = 4.347244F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 4.347244F;
            this.line5.X2 = 4.347244F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0.2397638F;
            // 
            // line6
            // 
            this.line6.Height = 0.2397638F;
            this.line6.Left = 5.198032F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 5.198032F;
            this.line6.X2 = 5.198032F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 0.2397638F;
            // 
            // line7
            // 
            this.line7.Height = 0.2397638F;
            this.line7.Left = 6.025985F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 6.025985F;
            this.line7.X2 = 6.025985F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.2397638F;
            // 
            // line8
            // 
            this.line8.Height = 0.2397638F;
            this.line8.Left = 6.470867F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 6.470867F;
            this.line8.X2 = 6.470867F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.2397638F;
            // 
            // line9
            // 
            this.line9.Height = 0.2397638F;
            this.line9.Left = 7.439371F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 7.439371F;
            this.line9.X2 = 7.439371F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.2397638F;
            // 
            // line10
            // 
            this.line10.Height = 0.2397638F;
            this.line10.Left = 8.396458F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 8.396458F;
            this.line10.X2 = 8.396458F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.2397638F;
            // 
            // line11
            // 
            this.line11.Height = 0.2397638F;
            this.line11.Left = 9.353544F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 9.353544F;
            this.line11.X2 = 9.353544F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.2397638F;
            // 
            // line12
            // 
            this.line12.Height = 0.2397638F;
            this.line12.Left = 10.38425F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 10.38425F;
            this.line12.X2 = 10.38425F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.2397638F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Height = 0F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // KBMR1061R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.2755905F;
            this.PageSettings.Margins.Left = 0.5275591F;
            this.PageSettings.Margins.Right = 0.5275591F;
            this.PageSettings.Margins.Top = 0.2755905F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 10.62992F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル229)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル229;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape4;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape3;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape6;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape7;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape8;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape9;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape11;
    }
}
