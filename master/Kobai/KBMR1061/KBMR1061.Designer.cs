﻿namespace jp.co.fsi.kb.kbmr1061
{
    partial class KBMR1061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdoZeroAri = new System.Windows.Forms.RadioButton();
            this.rdoZeroNashi = new System.Windows.Forms.RadioButton();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lblDateBet = new System.Windows.Forms.Label();
            this.lblDateDayTo = new System.Windows.Forms.Label();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.labelDateYearTo = new System.Windows.Forms.Label();
            this.labelDateYearFr = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.lblFunanishiCdBet = new System.Windows.Forms.Label();
            this.lblShohinNmFr = new System.Windows.Forms.Label();
            this.lblShohinNmTo = new System.Windows.Forms.Label();
            this.txtShohinCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohinCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 708);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1124, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1113, 41);
            this.lblTitle.Text = "";
            // 
            // rdoZeroAri
            // 
            this.rdoZeroAri.AutoSize = true;
            this.rdoZeroAri.BackColor = System.Drawing.Color.Silver;
            this.rdoZeroAri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeroAri.Location = new System.Drawing.Point(171, 5);
            this.rdoZeroAri.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeroAri.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeroAri.Name = "rdoZeroAri";
            this.rdoZeroAri.Size = new System.Drawing.Size(58, 24);
            this.rdoZeroAri.TabIndex = 1;
            this.rdoZeroAri.TabStop = true;
            this.rdoZeroAri.Tag = "CHANGE";
            this.rdoZeroAri.Text = "あり";
            this.rdoZeroAri.UseVisualStyleBackColor = false;
            // 
            // rdoZeroNashi
            // 
            this.rdoZeroNashi.AutoSize = true;
            this.rdoZeroNashi.BackColor = System.Drawing.Color.Silver;
            this.rdoZeroNashi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeroNashi.Location = new System.Drawing.Point(105, 5);
            this.rdoZeroNashi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeroNashi.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeroNashi.Name = "rdoZeroNashi";
            this.rdoZeroNashi.Size = new System.Drawing.Size(58, 24);
            this.rdoZeroNashi.TabIndex = 0;
            this.rdoZeroNashi.TabStop = true;
            this.rdoZeroNashi.Tag = "CHANGE";
            this.rdoZeroNashi.Text = "なし";
            this.rdoZeroNashi.UseVisualStyleBackColor = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(105, 4);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 908;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(151, 3);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 910;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.SystemColors.Window;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0: 全ての商品",
            "1: 取引有りの商品のみ",
            "2: 取引中止の商品のみ"});
            this.comboBox1.Location = new System.Drawing.Point(105, 4);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(295, 24);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.TabStop = false;
            // 
            // lblDateBet
            // 
            this.lblDateBet.BackColor = System.Drawing.Color.Silver;
            this.lblDateBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateBet.Location = new System.Drawing.Point(369, 4);
            this.lblDateBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateBet.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDateBet.Name = "lblDateBet";
            this.lblDateBet.Size = new System.Drawing.Size(23, 24);
            this.lblDateBet.TabIndex = 8;
            this.lblDateBet.Tag = "CHANGE";
            this.lblDateBet.Text = "～";
            this.lblDateBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayTo
            // 
            this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayTo.Location = new System.Drawing.Point(636, 4);
            this.lblDateDayTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateDayTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDateDayTo.Name = "lblDateDayTo";
            this.lblDateDayTo.Size = new System.Drawing.Size(20, 24);
            this.lblDateDayTo.TabIndex = 15;
            this.lblDateDayTo.Tag = "CHANGE";
            this.lblDateDayTo.Text = "日";
            this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayFr.Location = new System.Drawing.Point(337, 4);
            this.lblDateDayFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(20, 24);
            this.lblDateDayFr.TabIndex = 7;
            this.lblDateDayFr.Tag = "CHANGE";
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthTo.Location = new System.Drawing.Point(568, 4);
            this.lblDateMonthTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(20, 24);
            this.lblDateMonthTo.TabIndex = 13;
            this.lblDateMonthTo.Tag = "CHANGE";
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthFr.Location = new System.Drawing.Point(270, 4);
            this.lblDateMonthFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(20, 24);
            this.lblDateMonthFr.TabIndex = 5;
            this.lblDateMonthFr.Tag = "CHANGE";
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYearTo
            // 
            this.labelDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYearTo.Location = new System.Drawing.Point(502, 4);
            this.labelDateYearTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDateYearTo.Name = "labelDateYearTo";
            this.labelDateYearTo.Size = new System.Drawing.Size(20, 24);
            this.labelDateYearTo.TabIndex = 11;
            this.labelDateYearTo.Tag = "CHANGE";
            this.labelDateYearTo.Text = "年";
            this.labelDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYearFr
            // 
            this.labelDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYearFr.Location = new System.Drawing.Point(204, 4);
            this.labelDateYearFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDateYearFr.Name = "labelDateYearFr";
            this.labelDateYearFr.Size = new System.Drawing.Size(20, 24);
            this.labelDateYearFr.TabIndex = 3;
            this.labelDateYearFr.Tag = "CHANGE";
            this.labelDateYearFr.Text = "年";
            this.labelDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayTo.Location = new System.Drawing.Point(593, 5);
            this.txtDateDayTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(39, 23);
            this.txtDateDayTo.TabIndex = 14;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayFr.Location = new System.Drawing.Point(294, 5);
            this.txtDateDayFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(39, 23);
            this.txtDateDayFr.TabIndex = 6;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearTo.Location = new System.Drawing.Point(460, 5);
            this.txtDateYearTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(39, 23);
            this.txtDateYearTo.TabIndex = 10;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearFr.Location = new System.Drawing.Point(162, 5);
            this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(39, 23);
            this.txtDateYearFr.TabIndex = 2;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthTo.Location = new System.Drawing.Point(526, 5);
            this.txtDateMonthTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(39, 23);
            this.txtDateMonthTo.TabIndex = 12;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthFr.Location = new System.Drawing.Point(228, 5);
            this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(39, 23);
            this.txtDateMonthFr.TabIndex = 4;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoTo.Location = new System.Drawing.Point(402, 4);
            this.lblDateGengoTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateGengoTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(55, 24);
            this.lblDateGengoTo.TabIndex = 9;
            this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoFr.Location = new System.Drawing.Point(105, 4);
            this.lblDateGengoFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateGengoFr.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(55, 24);
            this.lblDateGengoFr.TabIndex = 1;
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanishiCdBet
            // 
            this.lblFunanishiCdBet.BackColor = System.Drawing.Color.Silver;
            this.lblFunanishiCdBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanishiCdBet.Location = new System.Drawing.Point(454, 5);
            this.lblFunanishiCdBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanishiCdBet.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblFunanishiCdBet.Name = "lblFunanishiCdBet";
            this.lblFunanishiCdBet.Size = new System.Drawing.Size(23, 24);
            this.lblFunanishiCdBet.TabIndex = 3;
            this.lblFunanishiCdBet.Tag = "CHANGE";
            this.lblFunanishiCdBet.Text = "～";
            this.lblFunanishiCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShohinNmFr
            // 
            this.lblShohinNmFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNmFr.Location = new System.Drawing.Point(245, 5);
            this.lblShohinNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinNmFr.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblShohinNmFr.Name = "lblShohinNmFr";
            this.lblShohinNmFr.Size = new System.Drawing.Size(204, 24);
            this.lblShohinNmFr.TabIndex = 2;
            this.lblShohinNmFr.Text = "先　頭";
            this.lblShohinNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinNmTo
            // 
            this.lblShohinNmTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNmTo.Location = new System.Drawing.Point(622, 5);
            this.lblShohinNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinNmTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblShohinNmTo.Name = "lblShohinNmTo";
            this.lblShohinNmTo.Size = new System.Drawing.Size(204, 24);
            this.lblShohinNmTo.TabIndex = 5;
            this.lblShohinNmTo.Text = "最　後";
            this.lblShohinNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdTo
            // 
            this.txtShohinCdTo.AutoSizeFromLength = false;
            this.txtShohinCdTo.DisplayLength = null;
            this.txtShohinCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdTo.Location = new System.Drawing.Point(482, 5);
            this.txtShohinCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohinCdTo.MaxLength = 13;
            this.txtShohinCdTo.Name = "txtShohinCdTo";
            this.txtShohinCdTo.Size = new System.Drawing.Size(135, 23);
            this.txtShohinCdTo.TabIndex = 4;
            this.txtShohinCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohinCdTo_KeyDown);
            this.txtShohinCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinTo_Validating);
            // 
            // txtShohinCdFr
            // 
            this.txtShohinCdFr.AutoSizeFromLength = false;
            this.txtShohinCdFr.DisplayLength = null;
            this.txtShohinCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdFr.Location = new System.Drawing.Point(105, 5);
            this.txtShohinCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohinCdFr.MaxLength = 13;
            this.txtShohinCdFr.Name = "txtShohinCdFr";
            this.txtShohinCdFr.Size = new System.Drawing.Size(135, 23);
            this.txtShohinCdFr.TabIndex = 1;
            this.txtShohinCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinFr_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.MinimumSize = new System.Drawing.Size(0, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(871, 33);
            this.label1.TabIndex = 909;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "支所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(871, 33);
            this.label2.TabIndex = 909;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "日付範囲";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(871, 33);
            this.label3.TabIndex = 909;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "在庫ゼロ表示";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(871, 33);
            this.label4.TabIndex = 909;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "商品CD範囲";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.MinimumSize = new System.Drawing.Size(0, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(871, 34);
            this.label5.TabIndex = 909;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "中止区分";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 44);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 5;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(879, 202);
            this.fsiTableLayoutPanel1.TabIndex = 911;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.comboBox1);
            this.fsiPanel5.Controls.Add(this.label5);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 164);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(871, 34);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblFunanishiCdBet);
            this.fsiPanel4.Controls.Add(this.txtShohinCdFr);
            this.fsiPanel4.Controls.Add(this.lblShohinNmFr);
            this.fsiPanel4.Controls.Add(this.txtShohinCdTo);
            this.fsiPanel4.Controls.Add(this.lblShohinNmTo);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 124);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(871, 33);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.rdoZeroNashi);
            this.fsiPanel3.Controls.Add(this.rdoZeroAri);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 84);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(871, 33);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblDateGengoFr);
            this.fsiPanel2.Controls.Add(this.lblDateBet);
            this.fsiPanel2.Controls.Add(this.lblDateGengoTo);
            this.fsiPanel2.Controls.Add(this.txtDateMonthFr);
            this.fsiPanel2.Controls.Add(this.txtDateMonthTo);
            this.fsiPanel2.Controls.Add(this.txtDateYearFr);
            this.fsiPanel2.Controls.Add(this.txtDateYearTo);
            this.fsiPanel2.Controls.Add(this.lblDateDayTo);
            this.fsiPanel2.Controls.Add(this.txtDateDayFr);
            this.fsiPanel2.Controls.Add(this.txtDateDayTo);
            this.fsiPanel2.Controls.Add(this.labelDateYearFr);
            this.fsiPanel2.Controls.Add(this.lblDateDayFr);
            this.fsiPanel2.Controls.Add(this.labelDateYearTo);
            this.fsiPanel2.Controls.Add(this.lblDateMonthFr);
            this.fsiPanel2.Controls.Add(this.lblDateMonthTo);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 44);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(871, 33);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(871, 33);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // KBMR1061
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 745);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBMR1061";
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton rdoZeroAri;
        private System.Windows.Forms.RadioButton rdoZeroNashi;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lblDateBet;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label labelDateYearTo;
        private System.Windows.Forms.Label labelDateYearFr;
        private common.controls.FsiTextBox txtDateDayTo;
        private common.controls.FsiTextBox txtDateDayFr;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateYearFr;
        private common.controls.FsiTextBox txtDateMonthTo;
        private common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblFunanishiCdBet;
        private System.Windows.Forms.Label lblShohinNmFr;
        private System.Windows.Forms.Label lblShohinNmTo;
        private common.controls.FsiTextBox txtShohinCdTo;
        private common.controls.FsiTextBox txtShohinCdFr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}