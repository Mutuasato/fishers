﻿using System.Data;
using System;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1011
{
    /// <summary>
    /// KBMR1011R の帳票
    /// </summary>
    public partial class KBMR1011R : BaseReport
    {

        public KBMR1011R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            InitializeComponent();
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            ////和暦でDataTimeを文字列に変換する
            //System.Globalization.CultureInfo ci =
            //    new System.Globalization.CultureInfo("ja-JP", false);
            //ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            //this.txtToday.Text = DateTime.Now.ToString("gy年MM月dd日", ci);
        }

        private void reportFooter1_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            this.txtTotal01.Text = Util.FormatNum(this.txtTotal01.Text);
            this.txtTotal02.Text = Util.FormatNum(this.txtTotal02.Text);
            this.txtTotal03.Text = Util.FormatNum(this.txtTotal03.Text);
            this.txtTotal04.Text = Util.FormatNum(this.txtTotal04.Text);
            this.txtTotal05.Text = Util.FormatNum(this.txtTotal05.Text);
            this.txtTotal06.Text = Util.FormatNum(this.txtTotal06.Text);
            this.txtTotal07.Text = Util.FormatNum(this.txtTotal07.Text);
            this.txtTotal08.Text = Util.FormatNum(this.txtTotal08.Text);
            this.txtTotal09.Text = Util.FormatNum(this.txtTotal09.Text);
            this.txtTotal10.Text = Util.FormatNum(this.txtTotal10.Text);
        }

    }
}
