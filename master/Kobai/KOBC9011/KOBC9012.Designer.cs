﻿namespace jp.co.fsi.kob.kobc9011
{
    partial class KOBC9012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShiireCd = new jp.co.fsi.common.controls.SosTextBox();
            this.lblShiireCd = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKaishuBi = new jp.co.fsi.common.controls.SosTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKaishuTsuki = new jp.co.fsi.common.controls.SosTextBox();
            this.lblKaishuTsuki = new System.Windows.Forms.Label();
            this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
            this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.SosTextBox();
            this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
            this.lblShzHsSrMemo = new System.Windows.Forms.Label();
            this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.SosTextBox();
            this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
            this.lblSkshKskMemo = new System.Windows.Forms.Label();
            this.txtSeikyushoKeishiki = new jp.co.fsi.common.controls.SosTextBox();
            this.lblSeikyushoKeishiki = new System.Windows.Forms.Label();
            this.lblSkshHkMemo = new System.Windows.Forms.Label();
            this.txtSeikyushoHakko = new jp.co.fsi.common.controls.SosTextBox();
            this.lblSeikyushoHakko = new System.Windows.Forms.Label();
            this.lblShimebiMemo = new System.Windows.Forms.Label();
            this.txtShimebi = new jp.co.fsi.common.controls.SosTextBox();
            this.lblShimebi = new System.Windows.Forms.Label();
            this.lblSeikyusakiNm = new System.Windows.Forms.Label();
            this.txtSeikyusakiCd = new jp.co.fsi.common.controls.SosTextBox();
            this.lblSeikyusakiCd = new System.Windows.Forms.Label();
            this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
            this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.SosTextBox();
            this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
            this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
            this.txtKingakuHasuShori = new jp.co.fsi.common.controls.SosTextBox();
            this.lblKingakuHasuShori = new System.Windows.Forms.Label();
            this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
            this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.SosTextBox();
            this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.SosTextBox();
            this.lblTantoshaCd = new System.Windows.Forms.Label();
            this.txtFaxBango = new jp.co.fsi.common.controls.SosTextBox();
            this.lblFaxBango = new System.Windows.Forms.Label();
            this.txtDenwaBango = new jp.co.fsi.common.controls.SosTextBox();
            this.lblDenwaBango = new System.Windows.Forms.Label();
            this.txtJusho2 = new jp.co.fsi.common.controls.SosTextBox();
            this.lblJusho2 = new System.Windows.Forms.Label();
            this.txtJusho1 = new jp.co.fsi.common.controls.SosTextBox();
            this.lblJusho1 = new System.Windows.Forms.Label();
            this.txtYubinBango2 = new jp.co.fsi.common.controls.SosTextBox();
            this.lblYubinBangoHyphen = new System.Windows.Forms.Label();
            this.txtYubinBango1 = new jp.co.fsi.common.controls.SosTextBox();
            this.lblYubinBango = new System.Windows.Forms.Label();
            this.txtShiireKanaNm = new jp.co.fsi.common.controls.SosTextBox();
            this.lblShiireKanaNm = new System.Windows.Forms.Label();
            this.txtShiireNm = new jp.co.fsi.common.controls.SosTextBox();
            this.lblShiireNm = new System.Windows.Forms.Label();
            this.txthyoji = new jp.co.fsi.common.controls.SosTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(792, 23);
            this.lblTitle.Text = "仕入先の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 259);
            this.pnlDebug.Size = new System.Drawing.Size(825, 100);
            // 
            // txtShiireCd
            // 
            this.txtShiireCd.AutoSizeFromLength = true;
            this.txtShiireCd.DisplayLength = null;
            this.txtShiireCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiireCd.Location = new System.Drawing.Point(132, 13);
            this.txtShiireCd.MaxLength = 4;
            this.txtShiireCd.Name = "txtShiireCd";
            this.txtShiireCd.Size = new System.Drawing.Size(34, 20);
            this.txtShiireCd.TabIndex = 2;
            this.txtShiireCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiireCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireCd_Validating);
            // 
            // lblShiireCd
            // 
            this.lblShiireCd.BackColor = System.Drawing.Color.Silver;
            this.lblShiireCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireCd.Location = new System.Drawing.Point(17, 13);
            this.lblShiireCd.Name = "lblShiireCd";
            this.lblShiireCd.Size = new System.Drawing.Size(115, 20);
            this.lblShiireCd.TabIndex = 1;
            this.lblShiireCd.Text = "仕入先コード";
            this.lblShiireCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.txthyoji);
            this.pnlMain.Controls.Add(this.label4);
            this.pnlMain.Controls.Add(this.label5);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.txtKaishuBi);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.txtKaishuTsuki);
            this.pnlMain.Controls.Add(this.lblKaishuTsuki);
            this.pnlMain.Controls.Add(this.lblShzTnkHohoMemo);
            this.pnlMain.Controls.Add(this.txtShohizeiTenkaHoho);
            this.pnlMain.Controls.Add(this.lblShohizeiTenkaHoho);
            this.pnlMain.Controls.Add(this.lblShzHsSrMemo);
            this.pnlMain.Controls.Add(this.txtShohizeiHasuShori);
            this.pnlMain.Controls.Add(this.lblShohizeiHasuShori);
            this.pnlMain.Controls.Add(this.lblSkshKskMemo);
            this.pnlMain.Controls.Add(this.txtSeikyushoKeishiki);
            this.pnlMain.Controls.Add(this.lblSeikyushoKeishiki);
            this.pnlMain.Controls.Add(this.lblSkshHkMemo);
            this.pnlMain.Controls.Add(this.txtSeikyushoHakko);
            this.pnlMain.Controls.Add(this.lblSeikyushoHakko);
            this.pnlMain.Controls.Add(this.lblShimebiMemo);
            this.pnlMain.Controls.Add(this.txtShimebi);
            this.pnlMain.Controls.Add(this.lblShimebi);
            this.pnlMain.Controls.Add(this.lblSeikyusakiNm);
            this.pnlMain.Controls.Add(this.txtSeikyusakiCd);
            this.pnlMain.Controls.Add(this.lblSeikyusakiCd);
            this.pnlMain.Controls.Add(this.lblShzNrkHohoNm);
            this.pnlMain.Controls.Add(this.txtShohizeiNyuryokuHoho);
            this.pnlMain.Controls.Add(this.lblShohizeiNyuryokuHoho);
            this.pnlMain.Controls.Add(this.lblKgkHsShrMemo);
            this.pnlMain.Controls.Add(this.txtKingakuHasuShori);
            this.pnlMain.Controls.Add(this.lblKingakuHasuShori);
            this.pnlMain.Controls.Add(this.lblTnkStkHohoMemo);
            this.pnlMain.Controls.Add(this.txtTankaShutokuHoho);
            this.pnlMain.Controls.Add(this.lblTankaShutokuHoho);
            this.pnlMain.Controls.Add(this.lblTantoshaNm);
            this.pnlMain.Controls.Add(this.txtTantoshaCd);
            this.pnlMain.Controls.Add(this.lblTantoshaCd);
            this.pnlMain.Controls.Add(this.txtFaxBango);
            this.pnlMain.Controls.Add(this.lblFaxBango);
            this.pnlMain.Controls.Add(this.txtDenwaBango);
            this.pnlMain.Controls.Add(this.lblDenwaBango);
            this.pnlMain.Controls.Add(this.txtJusho2);
            this.pnlMain.Controls.Add(this.lblJusho2);
            this.pnlMain.Controls.Add(this.txtJusho1);
            this.pnlMain.Controls.Add(this.lblJusho1);
            this.pnlMain.Controls.Add(this.txtYubinBango2);
            this.pnlMain.Controls.Add(this.lblYubinBangoHyphen);
            this.pnlMain.Controls.Add(this.txtYubinBango1);
            this.pnlMain.Controls.Add(this.lblYubinBango);
            this.pnlMain.Controls.Add(this.txtShiireKanaNm);
            this.pnlMain.Controls.Add(this.lblShiireKanaNm);
            this.pnlMain.Controls.Add(this.txtShiireNm);
            this.pnlMain.Controls.Add(this.lblShiireNm);
            this.pnlMain.Location = new System.Drawing.Point(12, 39);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(794, 260);
            this.pnlMain.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(767, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 20);
            this.label2.TabIndex = 50;
            this.label2.Text = "日";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKaishuBi
            // 
            this.txtKaishuBi.AutoSizeFromLength = true;
            this.txtKaishuBi.DisplayLength = null;
            this.txtKaishuBi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaishuBi.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKaishuBi.Location = new System.Drawing.Point(746, 141);
            this.txtKaishuBi.MaxLength = 2;
            this.txtKaishuBi.Name = "txtKaishuBi";
            this.txtKaishuBi.Size = new System.Drawing.Size(20, 20);
            this.txtKaishuBi.TabIndex = 49;
            this.txtKaishuBi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKaishuBi.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuBi_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(539, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 20);
            this.label1.TabIndex = 48;
            this.label1.Text = "0:当月 1:翌月 2:翌々月 …";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKaishuTsuki
            // 
            this.txtKaishuTsuki.AutoSizeFromLength = true;
            this.txtKaishuTsuki.DisplayLength = null;
            this.txtKaishuTsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaishuTsuki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKaishuTsuki.Location = new System.Drawing.Point(518, 141);
            this.txtKaishuTsuki.MaxLength = 2;
            this.txtKaishuTsuki.Name = "txtKaishuTsuki";
            this.txtKaishuTsuki.Size = new System.Drawing.Size(20, 20);
            this.txtKaishuTsuki.TabIndex = 47;
            this.txtKaishuTsuki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKaishuTsuki.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuTsuki_Validating);
            // 
            // lblKaishuTsuki
            // 
            this.lblKaishuTsuki.BackColor = System.Drawing.Color.Silver;
            this.lblKaishuTsuki.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKaishuTsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaishuTsuki.Location = new System.Drawing.Point(403, 141);
            this.lblKaishuTsuki.Name = "lblKaishuTsuki";
            this.lblKaishuTsuki.Size = new System.Drawing.Size(115, 20);
            this.lblKaishuTsuki.TabIndex = 46;
            this.lblKaishuTsuki.Text = "回収日";
            this.lblKaishuTsuki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShzTnkHohoMemo
            // 
            this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzTnkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(539, 118);
            this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
            this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(248, 20);
            this.lblShzTnkHohoMemo.TabIndex = 45;
            this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
            this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiTenkaHoho
            // 
            this.txtShohizeiTenkaHoho.AutoSizeFromLength = true;
            this.txtShohizeiTenkaHoho.DisplayLength = 2;
            this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiTenkaHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(518, 118);
            this.txtShohizeiTenkaHoho.MaxLength = 1;
            this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
            this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(20, 20);
            this.txtShohizeiTenkaHoho.TabIndex = 44;
            this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
            // 
            // lblShohizeiTenkaHoho
            // 
            this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiTenkaHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(403, 118);
            this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
            this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(115, 20);
            this.lblShohizeiTenkaHoho.TabIndex = 43;
            this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
            this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShzHsSrMemo
            // 
            this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzHsSrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzHsSrMemo.Location = new System.Drawing.Point(539, 95);
            this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
            this.lblShzHsSrMemo.Size = new System.Drawing.Size(248, 20);
            this.lblShzHsSrMemo.TabIndex = 42;
            this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
            this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiHasuShori
            // 
            this.txtShohizeiHasuShori.AutoSizeFromLength = true;
            this.txtShohizeiHasuShori.DisplayLength = 2;
            this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohizeiHasuShori.Location = new System.Drawing.Point(518, 95);
            this.txtShohizeiHasuShori.MaxLength = 1;
            this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
            this.txtShohizeiHasuShori.Size = new System.Drawing.Size(20, 20);
            this.txtShohizeiHasuShori.TabIndex = 41;
            this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
            // 
            // lblShohizeiHasuShori
            // 
            this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiHasuShori.Location = new System.Drawing.Point(403, 95);
            this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
            this.lblShohizeiHasuShori.Size = new System.Drawing.Size(115, 20);
            this.lblShohizeiHasuShori.TabIndex = 40;
            this.lblShohizeiHasuShori.Text = "消費税端数処理";
            this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSkshKskMemo
            // 
            this.lblSkshKskMemo.BackColor = System.Drawing.Color.Silver;
            this.lblSkshKskMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSkshKskMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSkshKskMemo.Location = new System.Drawing.Point(539, 72);
            this.lblSkshKskMemo.Name = "lblSkshKskMemo";
            this.lblSkshKskMemo.Size = new System.Drawing.Size(248, 20);
            this.lblSkshKskMemo.TabIndex = 39;
            this.lblSkshKskMemo.Text = "1:合計型 2:明細型";
            this.lblSkshKskMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeikyushoKeishiki
            // 
            this.txtSeikyushoKeishiki.AutoSizeFromLength = true;
            this.txtSeikyushoKeishiki.DisplayLength = 2;
            this.txtSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeikyushoKeishiki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSeikyushoKeishiki.Location = new System.Drawing.Point(518, 72);
            this.txtSeikyushoKeishiki.MaxLength = 1;
            this.txtSeikyushoKeishiki.Name = "txtSeikyushoKeishiki";
            this.txtSeikyushoKeishiki.Size = new System.Drawing.Size(20, 20);
            this.txtSeikyushoKeishiki.TabIndex = 38;
            this.txtSeikyushoKeishiki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSeikyushoKeishiki
            // 
            this.lblSeikyushoKeishiki.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoKeishiki.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeikyushoKeishiki.Location = new System.Drawing.Point(403, 72);
            this.lblSeikyushoKeishiki.Name = "lblSeikyushoKeishiki";
            this.lblSeikyushoKeishiki.Size = new System.Drawing.Size(115, 20);
            this.lblSeikyushoKeishiki.TabIndex = 37;
            this.lblSeikyushoKeishiki.Text = "請求書形式";
            this.lblSeikyushoKeishiki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSkshHkMemo
            // 
            this.lblSkshHkMemo.BackColor = System.Drawing.Color.Silver;
            this.lblSkshHkMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSkshHkMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSkshHkMemo.Location = new System.Drawing.Point(539, 49);
            this.lblSkshHkMemo.Name = "lblSkshHkMemo";
            this.lblSkshHkMemo.Size = new System.Drawing.Size(248, 20);
            this.lblSkshHkMemo.TabIndex = 36;
            this.lblSkshHkMemo.Text = "1:する 2:しない";
            this.lblSkshHkMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeikyushoHakko
            // 
            this.txtSeikyushoHakko.AutoSizeFromLength = true;
            this.txtSeikyushoHakko.DisplayLength = 2;
            this.txtSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeikyushoHakko.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSeikyushoHakko.Location = new System.Drawing.Point(518, 49);
            this.txtSeikyushoHakko.MaxLength = 1;
            this.txtSeikyushoHakko.Name = "txtSeikyushoHakko";
            this.txtSeikyushoHakko.Size = new System.Drawing.Size(20, 20);
            this.txtSeikyushoHakko.TabIndex = 35;
            this.txtSeikyushoHakko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSeikyushoHakko
            // 
            this.lblSeikyushoHakko.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoHakko.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeikyushoHakko.Location = new System.Drawing.Point(403, 49);
            this.lblSeikyushoHakko.Name = "lblSeikyushoHakko";
            this.lblSeikyushoHakko.Size = new System.Drawing.Size(115, 20);
            this.lblSeikyushoHakko.TabIndex = 34;
            this.lblSeikyushoHakko.Text = "請求書発行";
            this.lblSeikyushoHakko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShimebiMemo
            // 
            this.lblShimebiMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShimebiMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebiMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShimebiMemo.Location = new System.Drawing.Point(539, 26);
            this.lblShimebiMemo.Name = "lblShimebiMemo";
            this.lblShimebiMemo.Size = new System.Drawing.Size(248, 20);
            this.lblShimebiMemo.TabIndex = 33;
            this.lblShimebiMemo.Text = "1～28 ※末締めは99";
            this.lblShimebiMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShimebi
            // 
            this.txtShimebi.AutoSizeFromLength = true;
            this.txtShimebi.DisplayLength = null;
            this.txtShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShimebi.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShimebi.Location = new System.Drawing.Point(518, 26);
            this.txtShimebi.MaxLength = 2;
            this.txtShimebi.Name = "txtShimebi";
            this.txtShimebi.Size = new System.Drawing.Size(20, 20);
            this.txtShimebi.TabIndex = 32;
            this.txtShimebi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShimebi.Validating += new System.ComponentModel.CancelEventHandler(this.txtShimebi_Validating);
            // 
            // lblShimebi
            // 
            this.lblShimebi.BackColor = System.Drawing.Color.Silver;
            this.lblShimebi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShimebi.Location = new System.Drawing.Point(403, 26);
            this.lblShimebi.Name = "lblShimebi";
            this.lblShimebi.Size = new System.Drawing.Size(115, 20);
            this.lblShimebi.TabIndex = 31;
            this.lblShimebi.Text = "締日";
            this.lblShimebi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyusakiNm
            // 
            this.lblSeikyusakiNm.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyusakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyusakiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeikyusakiNm.Location = new System.Drawing.Point(553, 3);
            this.lblSeikyusakiNm.Name = "lblSeikyusakiNm";
            this.lblSeikyusakiNm.Size = new System.Drawing.Size(234, 20);
            this.lblSeikyusakiNm.TabIndex = 30;
            this.lblSeikyusakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeikyusakiCd
            // 
            this.txtSeikyusakiCd.AutoSizeFromLength = true;
            this.txtSeikyusakiCd.DisplayLength = null;
            this.txtSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeikyusakiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSeikyusakiCd.Location = new System.Drawing.Point(518, 3);
            this.txtSeikyusakiCd.MaxLength = 4;
            this.txtSeikyusakiCd.Name = "txtSeikyusakiCd";
            this.txtSeikyusakiCd.Size = new System.Drawing.Size(34, 20);
            this.txtSeikyusakiCd.TabIndex = 29;
            this.txtSeikyusakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSeikyusakiCd
            // 
            this.lblSeikyusakiCd.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyusakiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeikyusakiCd.Location = new System.Drawing.Point(403, 3);
            this.lblSeikyusakiCd.Name = "lblSeikyusakiCd";
            this.lblSeikyusakiCd.Size = new System.Drawing.Size(115, 20);
            this.lblSeikyusakiCd.TabIndex = 28;
            this.lblSeikyusakiCd.Text = "請求先コード";
            this.lblSeikyusakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShzNrkHohoNm
            // 
            this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShzNrkHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzNrkHohoNm.Location = new System.Drawing.Point(139, 233);
            this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
            this.lblShzNrkHohoNm.Size = new System.Drawing.Size(248, 20);
            this.lblShzNrkHohoNm.TabIndex = 27;
            this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiNyuryokuHoho
            // 
            this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = true;
            this.txtShohizeiNyuryokuHoho.DisplayLength = 2;
            this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiNyuryokuHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(118, 233);
            this.txtShohizeiNyuryokuHoho.MaxLength = 1;
            this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
            this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(20, 20);
            this.txtShohizeiNyuryokuHoho.TabIndex = 26;
            this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiNyuryokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiNyuryokuHoho_Validating);
            // 
            // lblShohizeiNyuryokuHoho
            // 
            this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(3, 233);
            this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
            this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(115, 20);
            this.lblShohizeiNyuryokuHoho.TabIndex = 25;
            this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
            this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKgkHsShrMemo
            // 
            this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblKgkHsShrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKgkHsShrMemo.Location = new System.Drawing.Point(139, 210);
            this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
            this.lblKgkHsShrMemo.Size = new System.Drawing.Size(248, 20);
            this.lblKgkHsShrMemo.TabIndex = 24;
            this.lblKgkHsShrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
            this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKingakuHasuShori
            // 
            this.txtKingakuHasuShori.AutoSizeFromLength = true;
            this.txtKingakuHasuShori.DisplayLength = 2;
            this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingakuHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKingakuHasuShori.Location = new System.Drawing.Point(118, 210);
            this.txtKingakuHasuShori.MaxLength = 1;
            this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
            this.txtKingakuHasuShori.Size = new System.Drawing.Size(20, 20);
            this.txtKingakuHasuShori.TabIndex = 23;
            this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
            // 
            // lblKingakuHasuShori
            // 
            this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblKingakuHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKingakuHasuShori.Location = new System.Drawing.Point(3, 210);
            this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
            this.lblKingakuHasuShori.Size = new System.Drawing.Size(115, 20);
            this.lblKingakuHasuShori.TabIndex = 22;
            this.lblKingakuHasuShori.Text = "金額端数処理";
            this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTnkStkHohoMemo
            // 
            this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblTnkStkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(139, 187);
            this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
            this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(248, 20);
            this.lblTnkStkHohoMemo.TabIndex = 21;
            this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
            this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTankaShutokuHoho
            // 
            this.txtTankaShutokuHoho.AutoSizeFromLength = true;
            this.txtTankaShutokuHoho.DisplayLength = 2;
            this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTankaShutokuHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTankaShutokuHoho.Location = new System.Drawing.Point(118, 187);
            this.txtTankaShutokuHoho.MaxLength = 1;
            this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
            this.txtTankaShutokuHoho.Size = new System.Drawing.Size(20, 20);
            this.txtTankaShutokuHoho.TabIndex = 20;
            this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
            // 
            // lblTankaShutokuHoho
            // 
            this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblTankaShutokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTankaShutokuHoho.Location = new System.Drawing.Point(3, 187);
            this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
            this.lblTankaShutokuHoho.Size = new System.Drawing.Size(115, 20);
            this.lblTankaShutokuHoho.TabIndex = 19;
            this.lblTankaShutokuHoho.Text = "単価取得方法";
            this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNm.Location = new System.Drawing.Point(153, 164);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(234, 20);
            this.lblTantoshaNm.TabIndex = 18;
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = true;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoshaCd.Location = new System.Drawing.Point(118, 164);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.Size = new System.Drawing.Size(34, 20);
            this.txtTantoshaCd.TabIndex = 17;
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
            // 
            // lblTantoshaCd
            // 
            this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaCd.Location = new System.Drawing.Point(3, 164);
            this.lblTantoshaCd.Name = "lblTantoshaCd";
            this.lblTantoshaCd.Size = new System.Drawing.Size(115, 20);
            this.lblTantoshaCd.TabIndex = 16;
            this.lblTantoshaCd.Text = "担当者コード";
            this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFaxBango
            // 
            this.txtFaxBango.AutoSizeFromLength = true;
            this.txtFaxBango.DisplayLength = null;
            this.txtFaxBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFaxBango.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFaxBango.Location = new System.Drawing.Point(118, 141);
            this.txtFaxBango.MaxLength = 15;
            this.txtFaxBango.Name = "txtFaxBango";
            this.txtFaxBango.Size = new System.Drawing.Size(111, 20);
            this.txtFaxBango.TabIndex = 15;
            // 
            // lblFaxBango
            // 
            this.lblFaxBango.BackColor = System.Drawing.Color.Silver;
            this.lblFaxBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFaxBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFaxBango.Location = new System.Drawing.Point(3, 141);
            this.lblFaxBango.Name = "lblFaxBango";
            this.lblFaxBango.Size = new System.Drawing.Size(115, 20);
            this.lblFaxBango.TabIndex = 14;
            this.lblFaxBango.Text = "ＦＡＸ番号";
            this.lblFaxBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDenwaBango
            // 
            this.txtDenwaBango.AutoSizeFromLength = true;
            this.txtDenwaBango.DisplayLength = null;
            this.txtDenwaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenwaBango.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDenwaBango.Location = new System.Drawing.Point(118, 118);
            this.txtDenwaBango.MaxLength = 15;
            this.txtDenwaBango.Name = "txtDenwaBango";
            this.txtDenwaBango.Size = new System.Drawing.Size(111, 20);
            this.txtDenwaBango.TabIndex = 13;
            // 
            // lblDenwaBango
            // 
            this.lblDenwaBango.BackColor = System.Drawing.Color.Silver;
            this.lblDenwaBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenwaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenwaBango.Location = new System.Drawing.Point(3, 118);
            this.lblDenwaBango.Name = "lblDenwaBango";
            this.lblDenwaBango.Size = new System.Drawing.Size(115, 20);
            this.lblDenwaBango.TabIndex = 12;
            this.lblDenwaBango.Text = "電話番号";
            this.lblDenwaBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJusho2
            // 
            this.txtJusho2.AutoSizeFromLength = false;
            this.txtJusho2.DisplayLength = null;
            this.txtJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho2.Location = new System.Drawing.Point(118, 95);
            this.txtJusho2.MaxLength = 30;
            this.txtJusho2.Name = "txtJusho2";
            this.txtJusho2.Size = new System.Drawing.Size(269, 20);
            this.txtJusho2.TabIndex = 11;
            this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho2_Validating);
            // 
            // lblJusho2
            // 
            this.lblJusho2.BackColor = System.Drawing.Color.Silver;
            this.lblJusho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho2.Location = new System.Drawing.Point(3, 95);
            this.lblJusho2.Name = "lblJusho2";
            this.lblJusho2.Size = new System.Drawing.Size(115, 20);
            this.lblJusho2.TabIndex = 10;
            this.lblJusho2.Text = "住所２";
            this.lblJusho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJusho1
            // 
            this.txtJusho1.AutoSizeFromLength = false;
            this.txtJusho1.DisplayLength = null;
            this.txtJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho1.Location = new System.Drawing.Point(118, 72);
            this.txtJusho1.MaxLength = 30;
            this.txtJusho1.Name = "txtJusho1";
            this.txtJusho1.Size = new System.Drawing.Size(269, 20);
            this.txtJusho1.TabIndex = 9;
            this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho1_Validating);
            // 
            // lblJusho1
            // 
            this.lblJusho1.BackColor = System.Drawing.Color.Silver;
            this.lblJusho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho1.Location = new System.Drawing.Point(3, 72);
            this.lblJusho1.Name = "lblJusho1";
            this.lblJusho1.Size = new System.Drawing.Size(115, 20);
            this.lblJusho1.TabIndex = 8;
            this.lblJusho1.Text = "住所１";
            this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYubinBango2
            // 
            this.txtYubinBango2.AutoSizeFromLength = true;
            this.txtYubinBango2.DisplayLength = null;
            this.txtYubinBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYubinBango2.Location = new System.Drawing.Point(158, 49);
            this.txtYubinBango2.MaxLength = 4;
            this.txtYubinBango2.Name = "txtYubinBango2";
            this.txtYubinBango2.Size = new System.Drawing.Size(34, 20);
            this.txtYubinBango2.TabIndex = 7;
            this.txtYubinBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango2_Validating);
            // 
            // lblYubinBangoHyphen
            // 
            this.lblYubinBangoHyphen.BackColor = System.Drawing.Color.Silver;
            this.lblYubinBangoHyphen.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYubinBangoHyphen.Location = new System.Drawing.Point(146, 49);
            this.lblYubinBangoHyphen.Name = "lblYubinBangoHyphen";
            this.lblYubinBangoHyphen.Size = new System.Drawing.Size(11, 20);
            this.lblYubinBangoHyphen.TabIndex = 6;
            this.lblYubinBangoHyphen.Text = "-";
            this.lblYubinBangoHyphen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYubinBango1
            // 
            this.txtYubinBango1.AutoSizeFromLength = true;
            this.txtYubinBango1.DisplayLength = null;
            this.txtYubinBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYubinBango1.Location = new System.Drawing.Point(118, 49);
            this.txtYubinBango1.MaxLength = 3;
            this.txtYubinBango1.Name = "txtYubinBango1";
            this.txtYubinBango1.Size = new System.Drawing.Size(27, 20);
            this.txtYubinBango1.TabIndex = 5;
            this.txtYubinBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango1_Validating);
            // 
            // lblYubinBango
            // 
            this.lblYubinBango.BackColor = System.Drawing.Color.Silver;
            this.lblYubinBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYubinBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYubinBango.Location = new System.Drawing.Point(3, 49);
            this.lblYubinBango.Name = "lblYubinBango";
            this.lblYubinBango.Size = new System.Drawing.Size(115, 20);
            this.lblYubinBango.TabIndex = 4;
            this.lblYubinBango.Text = "郵便番号";
            this.lblYubinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiireKanaNm
            // 
            this.txtShiireKanaNm.AutoSizeFromLength = false;
            this.txtShiireKanaNm.DisplayLength = null;
            this.txtShiireKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtShiireKanaNm.Location = new System.Drawing.Point(118, 26);
            this.txtShiireKanaNm.MaxLength = 30;
            this.txtShiireKanaNm.Name = "txtShiireKanaNm";
            this.txtShiireKanaNm.Size = new System.Drawing.Size(269, 20);
            this.txtShiireKanaNm.TabIndex = 3;
            this.txtShiireKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireKanaNm_Validating);
            // 
            // lblShiireKanaNm
            // 
            this.lblShiireKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblShiireKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireKanaNm.Location = new System.Drawing.Point(3, 26);
            this.lblShiireKanaNm.Name = "lblShiireKanaNm";
            this.lblShiireKanaNm.Size = new System.Drawing.Size(115, 20);
            this.lblShiireKanaNm.TabIndex = 2;
            this.lblShiireKanaNm.Text = "仕入先カナ名";
            this.lblShiireKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiireNm
            // 
            this.txtShiireNm.AutoSizeFromLength = false;
            this.txtShiireNm.DisplayLength = null;
            this.txtShiireNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtShiireNm.Location = new System.Drawing.Point(118, 3);
            this.txtShiireNm.MaxLength = 40;
            this.txtShiireNm.Name = "txtShiireNm";
            this.txtShiireNm.Size = new System.Drawing.Size(269, 20);
            this.txtShiireNm.TabIndex = 1;
            this.txtShiireNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireNm_Validating);
            // 
            // lblShiireNm
            // 
            this.lblShiireNm.BackColor = System.Drawing.Color.Silver;
            this.lblShiireNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireNm.Location = new System.Drawing.Point(3, 3);
            this.lblShiireNm.Name = "lblShiireNm";
            this.lblShiireNm.Size = new System.Drawing.Size(115, 20);
            this.lblShiireNm.TabIndex = 0;
            this.lblShiireNm.Text = "仕入先名";
            this.lblShiireNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txthyoji
            // 
            this.txthyoji.AutoSizeFromLength = false;
            this.txthyoji.BackColor = System.Drawing.Color.White;
            this.txthyoji.DisplayLength = null;
            this.txthyoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txthyoji.Location = new System.Drawing.Point(518, 164);
            this.txthyoji.MaxLength = 1;
            this.txthyoji.Name = "txthyoji";
            this.txthyoji.Size = new System.Drawing.Size(20, 20);
            this.txthyoji.TabIndex = 976;
            this.txthyoji.Text = "0";
            this.txthyoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txthyoji.Validating += new System.ComponentModel.CancelEventHandler(this.txthyoji_Validating);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(539, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 21);
            this.label4.TabIndex = 975;
            this.label4.Text = "0:表示する 1:表示しない ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(403, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 21);
            this.label5.TabIndex = 974;
            this.label5.Text = "一覧表示";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KOBC9012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 362);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtShiireCd);
            this.Controls.Add(this.lblShiireCd);
            this.Name = "KOBC9012";
            this.ShowFButton = true;
            this.Text = "仕入先の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblShiireCd, 0);
            this.Controls.SetChildIndex(this.txtShiireCd, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.SosTextBox txtShiireCd;
        private System.Windows.Forms.Label lblShiireCd;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private jp.co.fsi.common.controls.SosTextBox txtShiireNm;
        private System.Windows.Forms.Label lblShiireNm;
        private System.Windows.Forms.Label lblShiireKanaNm;
        private jp.co.fsi.common.controls.SosTextBox txtShiireKanaNm;
        private System.Windows.Forms.Label lblYubinBango;
        private jp.co.fsi.common.controls.SosTextBox txtYubinBango1;
        private System.Windows.Forms.Label lblYubinBangoHyphen;
        private jp.co.fsi.common.controls.SosTextBox txtYubinBango2;
        private jp.co.fsi.common.controls.SosTextBox txtJusho1;
        private System.Windows.Forms.Label lblJusho1;
        private jp.co.fsi.common.controls.SosTextBox txtJusho2;
        private System.Windows.Forms.Label lblJusho2;
        private System.Windows.Forms.Label lblDenwaBango;
        private jp.co.fsi.common.controls.SosTextBox txtDenwaBango;
        private jp.co.fsi.common.controls.SosTextBox txtFaxBango;
        private System.Windows.Forms.Label lblFaxBango;
        private System.Windows.Forms.Label lblTantoshaCd;
        private jp.co.fsi.common.controls.SosTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblTantoshaNm;
        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private jp.co.fsi.common.controls.SosTextBox txtTankaShutokuHoho;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private jp.co.fsi.common.controls.SosTextBox txtKingakuHasuShori;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private jp.co.fsi.common.controls.SosTextBox txtShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblSeikyusakiNm;
        private jp.co.fsi.common.controls.SosTextBox txtSeikyusakiCd;
        private System.Windows.Forms.Label lblSeikyusakiCd;
        private System.Windows.Forms.Label lblShimebi;
        private jp.co.fsi.common.controls.SosTextBox txtShimebi;
        private System.Windows.Forms.Label lblShimebiMemo;
        private System.Windows.Forms.Label lblSkshHkMemo;
        private jp.co.fsi.common.controls.SosTextBox txtSeikyushoHakko;
        private System.Windows.Forms.Label lblSeikyushoHakko;
        private System.Windows.Forms.Label lblSkshKskMemo;
        private jp.co.fsi.common.controls.SosTextBox txtSeikyushoKeishiki;
        private System.Windows.Forms.Label lblSeikyushoKeishiki;
        private System.Windows.Forms.Label lblShzHsSrMemo;
        private jp.co.fsi.common.controls.SosTextBox txtShohizeiHasuShori;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
        private jp.co.fsi.common.controls.SosTextBox txtShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private System.Windows.Forms.Label label1;
        private jp.co.fsi.common.controls.SosTextBox txtKaishuTsuki;
        private System.Windows.Forms.Label lblKaishuTsuki;
        private jp.co.fsi.common.controls.SosTextBox txtKaishuBi;
        private System.Windows.Forms.Label label2;
        private common.controls.SosTextBox txthyoji;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}