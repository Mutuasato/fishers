﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbdr1041
{
    /// <summary>
    /// KBDR1041R の帳票
    /// </summary>
    public partial class KBDR1041R : BaseReport
    {
        public KBDR1041R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
