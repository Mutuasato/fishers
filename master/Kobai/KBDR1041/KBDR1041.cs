﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.kb.kbdr1041
{
    #region 構造体
    /// <summary>
    /// 印刷ワークテーブルのデータ構造体
    /// </summary>
    struct NumVals
    {
        public decimal ZENGETU_KURIKOSHI_KINGAKU;
        public decimal URIAGE_KINGAKU;
        public decimal SYOUHIZEI_GAKU;
        public decimal HENPIN_KINGAKU;
        public decimal NYUKIN_KINGAKU;

        public void Clear()
        {
            ZENGETU_KURIKOSHI_KINGAKU = 0;
            URIAGE_KINGAKU = 0;
            SYOUHIZEI_GAKU = 0;
            HENPIN_KINGAKU = 0;
            NYUKIN_KINGAKU = 0;
        }
    }
    #endregion

    /// <summary>
    /// 購買未収金一覧表(KBDR1041)
    /// </summary>
    public partial class KBDR1041 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols = 23;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        private decimal MOTOCHO_KUBUN = 1;
        private decimal GENKIN;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDR1041()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 開始
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = "1";
            // 終了
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            MOTOCHO_KUBUN = (Util.ToDecimal(this.Par1) == 0) ? MOTOCHO_KUBUN : Util.ToDecimal(this.Par1);
            GENKIN = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "GENKIN"));

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年と取引先コードに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtChikuCdFr":
                case "txtChikuCdTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtChikuCdFr":
                    #region 地区CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1021.HNCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtChikuCdFr.Text = outData[0];
                                this.lblChikuCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtChikuCdTo":
                    #region 地区CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1021.HNCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtChikuCdTo.Text = outData[0];
                                this.lblChikuCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdFr":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdTo":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearFr":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearTo":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBDR1041R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 地区コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChikuCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidChikuCdFr())
            {
                e.Cancel = true;
                this.txtChikuCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 地区コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChikuCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidChikuCdTo())
            {
                e.Cancel = true;
                this.txtChikuCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 地区コード(自)の入力チェック
        /// </summary>
        private bool IsValidChikuCdFr()
        {
            if (ValChk.IsEmpty(this.txtChikuCdFr.Text))
            {
                this.lblChikuCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtChikuCdFr.Text))
                {
                    Msg.Error("地区コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblChikuCdFr.Text = this.Dba.GetName(this.UInfo, "TB_HN_CHIKU_MST", this.txtMizuageShishoCd.Text, this.txtChikuCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 地区コード(至)の入力チェック
        /// </summary>
        private bool IsValidChikuCdTo()
        {
            if (ValChk.IsEmpty(this.txtChikuCdTo.Text))
            {
                this.lblChikuCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtChikuCdTo.Text))
                {
                    Msg.Error("地区コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblChikuCdTo.Text = this.Dba.GetName(this.UInfo, "TB_HN_CHIKU_MST", this.txtMizuageShishoCd.Text, this.txtChikuCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            // 地区コード(自)の入力チェック
            if (!IsValidChikuCdFr())
            {
                this.txtChikuCdFr.Focus();
                this.txtChikuCdFr.SelectAll();
                return false;
            }
            // 地区コード(至)の入力チェック
            if (!IsValidChikuCdTo())
            {
                this.txtChikuCdTo.Focus();
                this.txtChikuCdTo.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }



        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");
                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBDR1041R rpt = new KBDR1041R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Info("該当データがありません。");
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {

            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpjpDateTo2 = Util.ConvJpDate(tmpDateTo2, this.Dba);

            // 表示する日付を設定する
            string hyojiDate; // 表示用日付
            string dateFrChk = string.Format("{0}{1}{2}{3}", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4]);
            string dateToChk = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            string dateFrChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], "1");
            string dateToChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo2[4]);
            if (dateFrChk == dateFrChk2 && dateToChk == dateToChk2)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // 取引先コード設定
            string TORIHIKISAKI_CD_FR;
            string TORIHIKISAKI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                TORIHIKISAKI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                TORIHIKISAKI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                TORIHIKISAKI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                TORIHIKISAKI_CD_TO = "9999";
            }
            string shishoCd = this.txtMizuageShishoCd.Text;

            StringBuilder Sql;
            int i; // ループ用カウント変数

            #region メインループデータ取得準備
            // Zam.TB_会社情報(TB_ZM_KAISHA_JOHO)
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            DataTable dtZM_KAISHA_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_ZM_KAISHA_JOHO",
                "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                "KAISHA_CD,KESSANKI",
                dpc);
            if (dtZM_KAISHA_JOHO.Rows.Count == 0)
            {
                return false;
            }

            // 月計 支払金額=税込み金額
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     B.HOJO_KAMOKU_CD    AS TORIHIKISAKI_CD,");
            //Sql.Append("     B.ZEIKOMI_KINGAKU     AS ZEIKOMI_KINGAKU");
            Sql.Append("     CASE WHEN B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN THEN B.ZEIKOMI_KINGAKU ELSE -1 * B.ZEIKOMI_KINGAKU END    AS ZEIKOMI_KINGAKU");
            Sql.Append(" FROM");
            Sql.Append("     TB_HN_NYUKIN_KAMOKU AS A  ");
            Sql.Append(" LEFT OUTER JOIN  TB_ZM_SHIWAKE_MEISAI  AS B");
            Sql.Append(" ON A.KAISHA_CD     = B.KAISHA_CD   AND ");
            Sql.Append("     A.SHISHO_CD    = B.SHISHO_CD   AND ");
            Sql.Append("     A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND ");
            //Sql.Append("     B.KAIKEI_NENDO = @KAIKEI_NENDO  AND");
            Sql.Append("     1                = B.DENPYO_KUBUN  ");
            Sql.Append(" LEFT OUTER JOIN  TB_ZM_KANJO_KAMOKU  AS C");
            Sql.Append(" ON  B.KAISHA_CD        = C.KAISHA_CD   AND ");
            Sql.Append("     B.KANJO_KAMOKU_CD  = C.KANJO_KAMOKU_CD  AND");
            Sql.Append("     B.KAIKEI_NENDO     = C.KAIKEI_NENDO");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD     = @SHISHO_CD AND");
            Sql.Append("     A.MOTOCHO_KUBUN       = @MOTOCHO_KUBUN AND");
            Sql.Append("     B.HOJO_KAMOKU_CD Between @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO AND");
            Sql.Append("     B.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND");
            //Sql.Append("     B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN AND");
            Sql.Append("     (B.SHIWAKE_SAKUSEI_KUBUN IS NULL OR B.SHIWAKE_SAKUSEI_KUBUN =9)");

            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.VarChar, 6, MOTOCHO_KUBUN);

            DataTable dtM_NYUKIN_KINGAKU = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 月別 売上金額,消費税額,返品金額
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     A.DENPYO_DATE   AS DENPYO_DATE,");
            Sql.Append("     A.TOKUISAKI_CD  AS TORIHIKISAKI_CD,");
            Sql.Append("     A.ZEI_RITSU     AS ZEI_RITSU,");
            Sql.Append("     A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE A.BARA_SOSU END )  AS URIAGE_SURYO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE A.BARA_SOSU END )  AS HENPIN_SURYO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.ZEINUKI_KINGAKU  ELSE 0 END END )  AS ZEINUKI_HENPINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.ZEINUKI_KINGAKU  ELSE 0 END END )  AS ZEINUKI_KINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.SHOHIZEI ELSE A.SHOHIZEI END END )  AS URIAGE_SHOHIZEI,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.SHOHIZEI ELSE A.SHOHIZEI END END )  AS HENPIN_SHOHIZEI,");
            Sql.Append("     0  AS SHOHIZEI,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO <> 3 THEN A.BAIKA_KINGAKU  ELSE 0 END END )  AS URIAGE_KINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO <> 3 THEN A.BAIKA_KINGAKU  ELSE 0 END END )  AS HENPIN_KINGAKU");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_TORIHIKI_MEISAI AS A  ");
            Sql.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B ");
            Sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD AND");
            Sql.Append("     A.TOKUISAKI_CD = B.TORIHIKISAKI_CD");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD     = @SHISHO_CD AND");
            Sql.Append("     A.DENPYO_KUBUN       = 1 AND");
            Sql.Append("     A.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND");
            Sql.Append("     (B.TORIHIKISAKI_CD BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO)  AND");
            //Sql.Append("     TORIHIKI_KUBUN1 = 1");
            Sql.Append("     TORIHIKI_KUBUN1 <> @TORIHIKI_KUBUN1 ");
            Sql.Append(" GROUP BY");
            Sql.Append("     A.DENPYO_DATE,");
            Sql.Append("     A.TOKUISAKI_CD,");
            Sql.Append("     A.ZEI_RITSU,");
            Sql.Append("     A.SHOHIZEI_NYURYOKU_HOHO");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.TOKUISAKI_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@TORIHIKI_KUBUN1", SqlDbType.VarChar, 6, GENKIN);
            
            DataTable dtM_TORIHIKI_MEISAI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 累計 入金金額=SUM(税込み金額)
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     B.HOJO_KAMOKU_CD    AS TORIHIKISAKI_CD,");
            //Sql.Append("     B.ZEIKOMI_KINGAKU     AS ZEIKOMI_KINGAKU");
            Sql.Append("     CASE WHEN B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN THEN B.ZEIKOMI_KINGAKU ELSE -1 * B.ZEIKOMI_KINGAKU END    AS ZEIKOMI_KINGAKU");
            Sql.Append(" FROM");
            Sql.Append("     TB_HN_NYUKIN_KAMOKU AS A  ");
            Sql.Append(" LEFT OUTER JOIN  TB_ZM_SHIWAKE_MEISAI  AS B");
            Sql.Append(" ON A.KAISHA_CD     = B.KAISHA_CD   AND ");
            Sql.Append("    A.SHISHO_CD     = B.SHISHO_CD   AND ");
            Sql.Append("    A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND ");
            //Sql.Append("    B.KAIKEI_NENDO = @KAIKEI_NENDO  AND");
            Sql.Append("    1                = B.DENPYO_KUBUN ");
            Sql.Append(" LEFT OUTER JOIN  TB_ZM_KANJO_KAMOKU  AS C");
            Sql.Append(" ON B.KAISHA_CD         = C.KAISHA_CD   AND ");
            Sql.Append("    B.KANJO_KAMOKU_CD   = C.KANJO_KAMOKU_CD  AND");
            Sql.Append("    B.KAIKEI_NENDO      = C.KAIKEI_NENDO");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD     = @SHISHO_CD AND");
            Sql.Append("     A.MOTOCHO_KUBUN       = @MOTOCHO_KUBUN AND");
            Sql.Append("     B.HOJO_KAMOKU_CD Between @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO AND");
            Sql.Append("     B.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND");
            //Sql.Append("     B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN AND");
            Sql.Append("     (B.SHIWAKE_SAKUSEI_KUBUN IS NULL OR B.SHIWAKE_SAKUSEI_KUBUN =9)");

            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, Util.ToDateStr(dtZM_KAISHA_JOHO.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.VarChar, 6, MOTOCHO_KUBUN);

            DataTable dtR_NYUKIN_KINGAKU = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 累計 売上金額,消費税額,返品金額
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     A.DENPYO_DATE   AS DENPYO_DATE,");
            Sql.Append("     A.TOKUISAKI_CD   AS TORIHIKISAKI_CD,");
            Sql.Append("     A.ZEI_RITSU       AS ZEI_RITSU,");
            Sql.Append("     A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE A.BARA_SOSU END )  AS URIAGE_SURYO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE A.BARA_SOSU END )  AS HENPIN_SURYO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.ZEINUKI_KINGAKU  ELSE 0 END END )  AS ZEINUKI_HENPINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.ZEINUKI_KINGAKU  ELSE 0 END END )  AS ZEINUKI_KINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.SHOHIZEI ELSE A.SHOHIZEI END END )  AS URIAGE_SHOHIZEI,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.SHOHIZEI ELSE A.SHOHIZEI END END )  AS HENPIN_SHOHIZEI,");
            Sql.Append("     0  AS SHOHIZEI,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO <> 3 THEN A.BAIKA_KINGAKU  ELSE 0 END END )  AS URIAGE_KINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO <> 3 THEN A.BAIKA_KINGAKU  ELSE 0 END END )  AS HENPIN_KINGAKU");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_TORIHIKI_MEISAI AS A  ");
            Sql.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B ");
            Sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD AND");
            Sql.Append("     A.TOKUISAKI_CD = B.TORIHIKISAKI_CD");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD     = @SHISHO_CD AND");
            Sql.Append("     A.DENPYO_KUBUN       = 1 AND");
            Sql.Append("     A.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND");
            Sql.Append("     (B.TORIHIKISAKI_CD BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO)  AND");
            //Sql.Append("     TORIHIKI_KUBUN1 = 1");
            Sql.Append("     TORIHIKI_KUBUN1 <> @TORIHIKI_KUBUN1 ");
            Sql.Append(" GROUP BY");
            Sql.Append("     A.DENPYO_DATE,");
            Sql.Append("     A.TOKUISAKI_CD,");
            Sql.Append("     A.ZEI_RITSU,");
            Sql.Append("     A.SHOHIZEI_NYURYOKU_HOHO");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.TOKUISAKI_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, Util.ToDateStr(dtZM_KAISHA_JOHO.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@TORIHIKI_KUBUN1", SqlDbType.VarChar, 6, GENKIN);

            DataTable dtR_TORIHIKI_MEISAI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 累計 地区コード,会員番号,会員名称,前月繰越金額=期首残,繰越残高=残高
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append(" T.TORIHIKISAKI_CD AS TOKUISAKI_CD,");
            Sql.Append(" T.TORIHIKISAKI_NM,");
            Sql.Append(" T.CHIKU_CD,");
            Sql.Append(" T.CHIKU_NM,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN ");
            Sql.Append(" FROM (");
            Sql.Append(" 	SELECT ");
            Sql.Append(" 		A.KAISHA_CD,");
            Sql.Append(" 		A.TORIHIKISAKI_CD,");
            Sql.Append(" 		A.TORIHIKISAKI_NM,");
            Sql.Append(" 		A.CHIKU_CD,");
            Sql.Append(" 		C.CHIKU_NM");
            Sql.Append(" 	FROM TB_CM_TORIHIKISAKI A");
            Sql.Append(" 	LEFT JOIN TB_HN_TORIHIKISAKI_JOHO B");
            Sql.Append(" 		ON	A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" 		AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD");
            Sql.Append(" 	LEFT JOIN TB_HN_CHIKU_MST C");
            Sql.Append(" 		ON	A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append(" 		AND A.CHIKU_CD	= C.CHIKU_CD");
            Sql.Append(" 	WHERE");
            Sql.Append(" 		B.TORIHIKISAKI_KUBUN1 = 1");
            Sql.Append(" ) T");
            Sql.Append(" LEFT JOIN (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		D.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 		D.SHISHO_CD,");
            Sql.Append(" 		D.HOJO_KAMOKU_CD,");
            Sql.Append(" 		SUM(CASE WHEN D.TAISHAKU_KUBUN = F.TAISHAKU_KUBUN ");
            Sql.Append(" 				 THEN D.ZEIKOMI_KINGAKU ");
            Sql.Append(" 				 ELSE (D.ZEIKOMI_KINGAKU * -1) ");
            Sql.Append(" 				 END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN D.DENPYO_DATE < @DATE_FR  ");
            Sql.Append(" 				 THEN (");
            Sql.Append(" 						CASE WHEN D.TAISHAKU_KUBUN = F.TAISHAKU_KUBUN ");
            Sql.Append(" 							 THEN D.ZEIKOMI_KINGAKU ");
            Sql.Append(" 							 ELSE (D.ZEIKOMI_KINGAKU * -1) ");
            Sql.Append(" 						END)     ");
            Sql.Append(" 				 ELSE 0 END)     AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI D");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU E");
            Sql.Append(" 		ON	D.KAISHA_CD = E.KAISHA_CD");
            Sql.Append(" 		AND D.SHISHO_CD = E.SHISHO_CD");
            Sql.Append(" 		AND D.KANJO_KAMOKU_CD = E.KANJO_KAMOKU_CD");
            Sql.Append(" 		AND E.MOTOCHO_KUBUN = @MOTOCHO_KUBUN");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU F");
            Sql.Append(" 		ON	D.KAISHA_CD = F.KAISHA_CD");
            Sql.Append(" 		AND D.KANJO_KAMOKU_CD = F.KANJO_KAMOKU_CD");
            Sql.Append(" 		AND D.KAIKEI_NENDO = F.KAIKEI_NENDO");
            Sql.Append(" 	WHERE");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append(" 		D.DENPYO_DATE <= @DATE_TO");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 		D.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 		D.SHISHO_CD,");
            Sql.Append(" 		D.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z ");
            Sql.Append(" ON	T.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND T.TORIHIKISAKI_CD = Z.HOJO_KAMOKU_CD");
            Sql.Append(" WHERE");
            Sql.Append(" 	T.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" 	ISNULL(Z.SHISHO_CD, @SHISHO_CD) = @SHISHO_CD AND ");
            Sql.Append(" 	T.TORIHIKISAKI_CD >= @TORIHIKISAKI_CD_FR AND ");
            Sql.Append(" 	T.TORIHIKISAKI_CD <= @TORIHIKISAKI_CD_TO");
            Sql.Append(" ORDER BY");
            Sql.Append(" 	CHIKU_CD,");
            Sql.Append(" 	TORIHIKISAKI_CD");

            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, Util.ToDateStr(dtZM_KAISHA_JOHO.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KESSANKI", SqlDbType.VarChar, 5, dtZM_KAISHA_JOHO.Rows[0]["KESSANKI"]);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.VarChar, 6, MOTOCHO_KUBUN);

            DataTable dtR_MainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            // 月計 地区コード,会員番号,会員名称,前月繰越金額=期首残,繰越残高=残高
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append(" T.TORIHIKISAKI_CD AS TOKUISAKI_CD,");
            Sql.Append(" T.TORIHIKISAKI_NM,");
            Sql.Append(" T.CHIKU_CD,");
            Sql.Append(" T.CHIKU_NM,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN ");
            Sql.Append(" FROM (");
            Sql.Append(" 	SELECT ");
            Sql.Append(" 		A.KAISHA_CD,");
            Sql.Append(" 		A.TORIHIKISAKI_CD,");
            Sql.Append(" 		A.TORIHIKISAKI_NM,");
            Sql.Append(" 		A.CHIKU_CD,");
            Sql.Append(" 		C.CHIKU_NM");
            Sql.Append(" 	FROM TB_CM_TORIHIKISAKI A");
            Sql.Append(" 	LEFT JOIN TB_HN_TORIHIKISAKI_JOHO B");
            Sql.Append(" 		ON	A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" 		AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD");
            Sql.Append(" 	LEFT JOIN TB_HN_CHIKU_MST C");
            Sql.Append(" 		ON	A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append(" 		AND A.CHIKU_CD	= C.CHIKU_CD");
            Sql.Append(" 	WHERE");
            Sql.Append(" 		B.TORIHIKISAKI_KUBUN1 = 1");
            Sql.Append(" ) T");
            Sql.Append(" LEFT JOIN (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		D.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 		D.SHISHO_CD,");
            Sql.Append(" 		D.HOJO_KAMOKU_CD,");
            Sql.Append(" 		SUM(CASE WHEN D.TAISHAKU_KUBUN = F.TAISHAKU_KUBUN ");
            Sql.Append(" 				 THEN D.ZEIKOMI_KINGAKU ");
            Sql.Append(" 				 ELSE (D.ZEIKOMI_KINGAKU * -1) ");
            Sql.Append(" 				 END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN D.DENPYO_DATE < @DATE_FR  ");
            Sql.Append(" 				 THEN (");
            Sql.Append(" 						CASE WHEN D.TAISHAKU_KUBUN = F.TAISHAKU_KUBUN ");
            Sql.Append(" 							 THEN D.ZEIKOMI_KINGAKU ");
            Sql.Append(" 							 ELSE (D.ZEIKOMI_KINGAKU * -1) ");
            Sql.Append(" 						END)     ");
            Sql.Append(" 				 ELSE 0 END)     AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI D");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU E");
            Sql.Append(" 		ON	D.KAISHA_CD = E.KAISHA_CD");
            Sql.Append(" 		AND D.SHISHO_CD = E.SHISHO_CD");
            Sql.Append(" 		AND D.KANJO_KAMOKU_CD = E.KANJO_KAMOKU_CD");
            Sql.Append(" 		AND E.MOTOCHO_KUBUN = @MOTOCHO_KUBUN");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU F");
            Sql.Append(" 		ON	D.KAISHA_CD = F.KAISHA_CD");
            Sql.Append(" 		AND D.KANJO_KAMOKU_CD = F.KANJO_KAMOKU_CD");
            Sql.Append(" 		AND D.KAIKEI_NENDO = F.KAIKEI_NENDO");
            Sql.Append(" 	WHERE");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append(" 		D.DENPYO_DATE <= @DATE_TO");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 		D.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 		D.SHISHO_CD,");
            Sql.Append(" 		D.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z ");
            Sql.Append(" ON	T.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND T.TORIHIKISAKI_CD = Z.HOJO_KAMOKU_CD");
            Sql.Append(" WHERE");
            Sql.Append(" 	T.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" 	ISNULL(Z.SHISHO_CD, @SHISHO_CD) = @SHISHO_CD AND ");
            Sql.Append(" 	T.TORIHIKISAKI_CD >= @TORIHIKISAKI_CD_FR AND ");
            Sql.Append(" 	T.TORIHIKISAKI_CD <= @TORIHIKISAKI_CD_TO");
            Sql.Append(" ORDER BY");
            Sql.Append(" 	CHIKU_CD,");
            Sql.Append(" 	TORIHIKISAKI_CD");

            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KESSANKI", SqlDbType.VarChar, 5, dtZM_KAISHA_JOHO.Rows[0]["KESSANKI"]);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.VarChar, 6, MOTOCHO_KUBUN);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                NumVals GChikuNum = new NumVals(); // 地区月計
                NumVals RChikuNum = new NumVals(); // 地区累計
                NumVals MonthNum = new NumVals(); // 月計
                NumVals TMonthNum = new NumVals(); // 月計合計
                NumVals RuikeiNum = new NumVals(); // 累計
                NumVals TRuikeiNum = new NumVals(); // 累計合計

                i = 0;
                int j;
                int dbSORT = 1;
                string chikuCd = "";
                string chikuNm = "";
                bool nyukinFlg = false;
                bool uriageFlg = false;
                // 月計合計リセット
                TMonthNum.Clear();
                // 累計合計リセット
                TRuikeiNum.Clear();

                while (dtMainLoop.Rows.Count > i)
                {
                    // 月計をリセット
                    MonthNum.Clear();
                    // 累計をリセット
                    RuikeiNum.Clear();

                    // 入金があればtrue
                    foreach (DataRow dr in dtM_NYUKIN_KINGAKU.Rows)
                    {
                        if (Util.ToDecimal(dr["TORIHIKISAKI_CD"]) == Util.ToDecimal(dtMainLoop.Rows[i]["TOKUISAKI_CD"]))
                        {
                            nyukinFlg = true;
                            break;
                        }
                        else
                        {
                            nyukinFlg = false;
                        }
                    }
                    // 取引があればtrue
                    foreach (DataRow dr in dtM_TORIHIKI_MEISAI.Rows)
                    {
                        if (Util.ToDecimal(dr["TORIHIKISAKI_CD"]) == Util.ToDecimal(dtMainLoop.Rows[i]["TOKUISAKI_CD"]))
                        {
                            uriageFlg = true;
                            break;
                        }
                        else
                        {
                            uriageFlg = false;
                        }
                    }
                    // 登録するデータを判別
                    if (String.IsNullOrEmpty(Util.ToString(dtMainLoop.Rows[i]["ZANDAKA"])) == false && (nyukinFlg || uriageFlg || Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]) > 0 || Util.ToDecimal(dtMainLoop.Rows[i]["KISHUZAN"]) > 0))
                    {
                        // 地区コードが変わった場合実行
                        if (!ValChk.IsEmpty(chikuCd) && !Util.ToString(dtMainLoop.Rows[i]["CHIKU_CD"]).Equals(chikuCd))
                        {
                            #region 印刷ワークテーブルに地区合計登録
                            dpc = new DbParamCollection();
                            Sql = new StringBuilder();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(") ");

                            // 空行登録 ２つ
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "購買未収金一覧表");
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hyojiDate);
                            dbSORT++;
                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM09");
                            Sql.Append(" ,ITEM10");
                            Sql.Append(" ,ITEM11");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM13");
                            Sql.Append(" ,ITEM14");
                            Sql.Append(" ,ITEM15");
                            Sql.Append(" ,ITEM16");
                            Sql.Append(" ,ITEM17");
                            Sql.Append(" ,ITEM18");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM09");
                            Sql.Append(" ,@ITEM10");
                            Sql.Append(" ,@ITEM11");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM13");
                            Sql.Append(" ,@ITEM14");
                            Sql.Append(" ,@ITEM15");
                            Sql.Append(" ,@ITEM16");
                            Sql.Append(" ,@ITEM17");
                            Sql.Append(" ,@ITEM18");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            // 月計登録
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "購買未収金一覧表");
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hyojiDate);

                            // 月計登録を設定
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 4, chikuCd); // ｺｰﾄﾞ
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, chikuNm + "\r\n【　地　区　合　計　】"); // 仲買人名
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "月　計"); // 月計
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.ZENGETU_KURIKOSHI_KINGAKU)); // 前月繰越金額
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.URIAGE_KINGAKU)); // 売上金額
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.SYOUHIZEI_GAKU)); // 消費税額
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.URIAGE_KINGAKU + GChikuNum.SYOUHIZEI_GAKU)); // 税込金額
                            dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.HENPIN_KINGAKU)); // 売上返品
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.NYUKIN_KINGAKU)); // 入金金額
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200,
                                Util.FormatNum(
                                    (GChikuNum.ZENGETU_KURIKOSHI_KINGAKU + GChikuNum.URIAGE_KINGAKU + GChikuNum.SYOUHIZEI_GAKU) - GChikuNum.NYUKIN_KINGAKU)); // 繰越残高

                            // 累計登録を設定
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "累　計"); // 累計
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.ZENGETU_KURIKOSHI_KINGAKU)); // 前月繰越金額
                            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.URIAGE_KINGAKU)); // 売上金額
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.SYOUHIZEI_GAKU)); // 消費税額
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.URIAGE_KINGAKU + RChikuNum.SYOUHIZEI_GAKU)); // 税込金額
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.HENPIN_KINGAKU)); // 売上返品
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.NYUKIN_KINGAKU)); // 入金金額
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200,
                                Util.FormatNum(
                                    (RChikuNum.ZENGETU_KURIKOSHI_KINGAKU + RChikuNum.URIAGE_KINGAKU + RChikuNum.SYOUHIZEI_GAKU) - RChikuNum.NYUKIN_KINGAKU)); // 繰越残高
                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            GChikuNum.Clear();
                            RChikuNum.Clear();
                            dbSORT++;

                            dpc = new DbParamCollection();
                            Sql = new StringBuilder();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(") ");

                            // 空行登録 ２つ
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "購買未収金一覧表");
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hyojiDate);
                            dbSORT++;
                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            #endregion
                        }

                        // 月計 前月繰越金額
                        MonthNum.ZENGETU_KURIKOSHI_KINGAKU = Util.ToDecimal(dtMainLoop.Rows[i]["KISHUZAN"]);

                        // 月計 売上金額,消費税額,返品金額
                        j = 0;
                        while (dtM_TORIHIKI_MEISAI.Rows.Count > j)
                        {
                            // 取引先コードを比較
                            if (Util.ToDecimal(dtMainLoop.Rows[i]["TOKUISAKI_CD"]) == Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["TORIHIKISAKI_CD"]))
                            {
                                //if (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["SHOHIZEI_NYURYOKU_HOHO"]) == 3)
                                if (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["SHOHIZEI_NYURYOKU_HOHO"]) == 3)
                                {
                                    // 売上金額
                                    MonthNum.URIAGE_KINGAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_KINGAKU"]) - Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_HENPINGAKU"]));
                                    // 返品金額
                                    MonthNum.HENPIN_KINGAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_HENPINGAKU"]) + Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));
                                }
                                else
                                {
                                    // 売上金額
                                    MonthNum.URIAGE_KINGAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["URIAGE_KINGAKU"]) - Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_KINGAKU"]));
                                    // 返品金額
                                    MonthNum.HENPIN_KINGAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_KINGAKU"]) + Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));
                                }
                                // 消費税額
                                MonthNum.SYOUHIZEI_GAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["URIAGE_SHOHIZEI"]) - Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));
                            }
                            j++;
                        }

                        // 月計 入金金額=SUM(税込み金額)
                        j = 0;
                        while (dtM_NYUKIN_KINGAKU.Rows.Count > j)
                        {
                            // 取引先コードを比較
                            if (Util.ToDecimal(dtMainLoop.Rows[i]["TOKUISAKI_CD"]) == Util.ToDecimal(dtM_NYUKIN_KINGAKU.Rows[j]["TORIHIKISAKI_CD"]))
                            {
                                // 入金金額
                                MonthNum.NYUKIN_KINGAKU += Util.ToDecimal(dtM_NYUKIN_KINGAKU.Rows[j]["ZEIKOMI_KINGAKU"]);
                            }
                            j++;
                        }

                        // 累計 前月繰越金額
                        j = 0;
                        while (dtR_MainLoop.Rows.Count > j)
                        {
                            // 取引先コードを比較
                            if (Util.ToDecimal(dtMainLoop.Rows[i]["TOKUISAKI_CD"]) == Util.ToDecimal(dtR_MainLoop.Rows[j]["TOKUISAKI_CD"]))
                            {
                                RuikeiNum.ZENGETU_KURIKOSHI_KINGAKU += Util.ToDecimal(dtR_MainLoop.Rows[i]["KISHUZAN"]);
                            }
                            j++;
                        }

                        // 累計 売上金額,消費税額,返品金額
                        j = 0;
                        while (dtR_TORIHIKI_MEISAI.Rows.Count > j)
                        {
                            // 取引先コードを比較
                            if (Util.ToDecimal(dtMainLoop.Rows[i]["TOKUISAKI_CD"]) == Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["TORIHIKISAKI_CD"]))
                            {
                                if (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["SHOHIZEI_NYURYOKU_HOHO"]) == 3)
                                {
                                    // 売上金額
                                    RuikeiNum.URIAGE_KINGAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_KINGAKU"]) - Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_HENPINGAKU"]));
                                    // 返品金額
                                    RuikeiNum.HENPIN_KINGAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_HENPINGAKU"]) + Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));
                                }
                                else
                                {
                                    // 売上金額
                                    RuikeiNum.URIAGE_KINGAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["URIAGE_KINGAKU"]) - Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_KINGAKU"]));
                                    // 返品金額
                                    RuikeiNum.HENPIN_KINGAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_KINGAKU"]) + Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));
                                }
                                // 消費税額
                                RuikeiNum.SYOUHIZEI_GAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["URIAGE_SHOHIZEI"]) - Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));

                            }
                            j++;
                        }

                        // 累計 入金金額=SUM(税込み金額)
                        j = 0;
                        while (dtR_NYUKIN_KINGAKU.Rows.Count > j)
                        {
                            // 取引先コードを比較
                            if (Util.ToDecimal(dtMainLoop.Rows[i]["TOKUISAKI_CD"]) == Util.ToDecimal(dtR_NYUKIN_KINGAKU.Rows[j]["TORIHIKISAKI_CD"]))
                            {
                                // 入金金額
                                RuikeiNum.NYUKIN_KINGAKU += Util.ToDecimal(dtR_NYUKIN_KINGAKU.Rows[j]["ZEIKOMI_KINGAKU"]);
                            }
                            j++;
                        }


                        #region 印刷ワークテーブルに登録
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                        Sql.Append(") ");

                        // 月計登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "購買未収金一覧表");
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hyojiDate);

                        // 月計登録を設定
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 4, dtMainLoop.Rows[i]["TOKUISAKI_CD"]); // ｺｰﾄﾞ
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_NM"]); // 仲買人名
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "月　計"); // 月計
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.ZENGETU_KURIKOSHI_KINGAKU)); // 前月繰越金額
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.URIAGE_KINGAKU)); // 売上金額
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.SYOUHIZEI_GAKU)); // 消費税額
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.URIAGE_KINGAKU + MonthNum.SYOUHIZEI_GAKU)); // 税込金額
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.HENPIN_KINGAKU)); // 売上返品
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.NYUKIN_KINGAKU)); // 入金金額
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200,
                            Util.FormatNum(
                                (MonthNum.ZENGETU_KURIKOSHI_KINGAKU + MonthNum.URIAGE_KINGAKU + MonthNum.SYOUHIZEI_GAKU) - MonthNum.NYUKIN_KINGAKU)); // 繰越残高

                        // 累計登録を設定
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "累　計"); // 累計
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.ZENGETU_KURIKOSHI_KINGAKU)); // 前月繰越金額
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.URIAGE_KINGAKU)); // 売上金額
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.SYOUHIZEI_GAKU)); // 消費税額
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.URIAGE_KINGAKU + RuikeiNum.SYOUHIZEI_GAKU)); // 税込金額
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.HENPIN_KINGAKU)); // 売上返品
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.NYUKIN_KINGAKU)); // 入金金額
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200,
                            Util.FormatNum(
                                (RuikeiNum.ZENGETU_KURIKOSHI_KINGAKU + RuikeiNum.URIAGE_KINGAKU + RuikeiNum.SYOUHIZEI_GAKU) - RuikeiNum.NYUKIN_KINGAKU)); // 繰越残高
                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                        // 月計を加算
                        TMonthNum.ZENGETU_KURIKOSHI_KINGAKU += MonthNum.ZENGETU_KURIKOSHI_KINGAKU;
                        TMonthNum.URIAGE_KINGAKU += MonthNum.URIAGE_KINGAKU;
                        TMonthNum.SYOUHIZEI_GAKU += MonthNum.SYOUHIZEI_GAKU;
                        TMonthNum.NYUKIN_KINGAKU += MonthNum.NYUKIN_KINGAKU;
                        // 累計を加算
                        TRuikeiNum.ZENGETU_KURIKOSHI_KINGAKU += RuikeiNum.ZENGETU_KURIKOSHI_KINGAKU;
                        TRuikeiNum.URIAGE_KINGAKU += RuikeiNum.URIAGE_KINGAKU;
                        TRuikeiNum.SYOUHIZEI_GAKU += RuikeiNum.SYOUHIZEI_GAKU;
                        TRuikeiNum.NYUKIN_KINGAKU += RuikeiNum.NYUKIN_KINGAKU;
                        // 地区月計を加算
                        GChikuNum.ZENGETU_KURIKOSHI_KINGAKU += MonthNum.ZENGETU_KURIKOSHI_KINGAKU;
                        GChikuNum.URIAGE_KINGAKU += MonthNum.URIAGE_KINGAKU;
                        GChikuNum.SYOUHIZEI_GAKU += MonthNum.SYOUHIZEI_GAKU;
                        GChikuNum.NYUKIN_KINGAKU += MonthNum.NYUKIN_KINGAKU;
                        // 地区累計を加算
                        RChikuNum.ZENGETU_KURIKOSHI_KINGAKU += RuikeiNum.ZENGETU_KURIKOSHI_KINGAKU;
                        RChikuNum.URIAGE_KINGAKU += RuikeiNum.URIAGE_KINGAKU;
                        RChikuNum.SYOUHIZEI_GAKU += RuikeiNum.SYOUHIZEI_GAKU;
                        RChikuNum.NYUKIN_KINGAKU += RuikeiNum.NYUKIN_KINGAKU;

                        chikuNm = Util.ToString(dtMainLoop.Rows[i]["CHIKU_NM"]);
                        chikuCd = Util.ToString(dtMainLoop.Rows[i]["CHIKU_CD"]);
                        dbSORT++;
                        #endregion
                    }
                    i++;
                }

                #region 印刷ワークテーブルに地区合計登録(ループ終了後)
                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(") ");

                // 空行登録 ２つ
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "購買未収金一覧表");
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hyojiDate);
                dbSORT++;
                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                Sql.Append(") ");
                // 月計登録
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "購買未収金一覧表");
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hyojiDate);

                // 月計登録を設定
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 4, chikuCd); // ｺｰﾄﾞ
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, chikuNm + "\r\n【　地　区　合　計　】"); // 仲買人名
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "月　計"); // 月計
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.ZENGETU_KURIKOSHI_KINGAKU)); // 前月繰越金額
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.URIAGE_KINGAKU)); // 売上金額
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.SYOUHIZEI_GAKU)); // 消費税額
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.URIAGE_KINGAKU + GChikuNum.SYOUHIZEI_GAKU)); // 税込金額
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.HENPIN_KINGAKU)); // 売上返品
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(GChikuNum.NYUKIN_KINGAKU)); // 入金金額
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200,
                    Util.FormatNum(
                        (GChikuNum.ZENGETU_KURIKOSHI_KINGAKU + GChikuNum.URIAGE_KINGAKU + GChikuNum.SYOUHIZEI_GAKU) - GChikuNum.NYUKIN_KINGAKU)); // 繰越残高

                // 累計登録を設定
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "累　計"); // 累計
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.ZENGETU_KURIKOSHI_KINGAKU)); // 前月繰越金額
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.URIAGE_KINGAKU)); // 売上金額
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.SYOUHIZEI_GAKU)); // 消費税額
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.URIAGE_KINGAKU + RChikuNum.SYOUHIZEI_GAKU)); // 税込金額
                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.HENPIN_KINGAKU)); // 売上返品
                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(RChikuNum.NYUKIN_KINGAKU)); // 入金金額
                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200,
                    Util.FormatNum(
                        (RChikuNum.ZENGETU_KURIKOSHI_KINGAKU + RChikuNum.URIAGE_KINGAKU + RChikuNum.SYOUHIZEI_GAKU) - RChikuNum.NYUKIN_KINGAKU)); // 繰越残高
                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                #endregion

                #region 印刷ワークテーブルに合計登録
                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ," + Util.ColsArray(5, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(5, "@"));
                Sql.Append(") ");

                // 空行登録 ２つ
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT + 1);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "購買未収金一覧表");
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hyojiDate);
                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                Sql.Append(") ");
                // 月計登録
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT + 2);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "購買未収金一覧表");
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hyojiDate);

                // 月計登録の設定
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 4, ""); // ｺｰﾄﾞ
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "　　【　合　計　】"); // 仕入先名
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "月　計"); // 月計
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.ZENGETU_KURIKOSHI_KINGAKU)); // 前月繰越金額
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.URIAGE_KINGAKU)); // 売上金額
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.SYOUHIZEI_GAKU)); // 消費税額
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.URIAGE_KINGAKU + TMonthNum.SYOUHIZEI_GAKU)); // 税込金額
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.HENPIN_KINGAKU)); // 売上返品
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.NYUKIN_KINGAKU)); // 支払金額
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200,
                    Util.FormatNum(
                        (TMonthNum.ZENGETU_KURIKOSHI_KINGAKU + TMonthNum.URIAGE_KINGAKU + TMonthNum.SYOUHIZEI_GAKU) - TMonthNum.NYUKIN_KINGAKU)); // 繰越残高

                // 累計登録を設定
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "累　計"); // 累計
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.ZENGETU_KURIKOSHI_KINGAKU)); // 前月繰越金額
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.URIAGE_KINGAKU)); // 仕入金額
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.SYOUHIZEI_GAKU)); // 消費税額
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.URIAGE_KINGAKU + TRuikeiNum.SYOUHIZEI_GAKU)); // 税込金額
                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.HENPIN_KINGAKU)); // 売上返品
                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.NYUKIN_KINGAKU)); // 支払金額
                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200,
                    Util.FormatNum(
                        (TRuikeiNum.ZENGETU_KURIKOSHI_KINGAKU + TRuikeiNum.URIAGE_KINGAKU + TRuikeiNum.SYOUHIZEI_GAKU) - TRuikeiNum.NYUKIN_KINGAKU)); // 繰越残高

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;

                // 空データ時の対応
                dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                    "SORT",
                    "PR_HN_TBL",
                    "GUID = @GUID AND LTRIM(RTRIM(ITEM06)) <> ''",
                    dpc);
                if (tmpdtPR_HN_TBL.Rows.Count == 0)
                {
                    dataFlag = false;
                }
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
