﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000007 RID: 7
	public partial class KBDB1013 : BasePgForm
	{
		// Token: 0x0600006F RID: 111 RVA: 0x0000F793 File Offset: 0x0000D993
		public KBDB1013(KBDB1011 frm, ConfigLoader config)
		{
			this._pForm = frm;
			this._config = config;
			this.InitializeComponent();
			base.BindGotFocusEvent();
		}

		// Token: 0x06000070 RID: 112 RVA: 0x0000F7B8 File Offset: 0x0000D9B8
		protected override void InitForm()
		{
			string text = " ";
			string[] array = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DpyDtFr"]), base.Dba);
			text += array[5];
			array = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DpyDtTo"]), base.Dba);
			text = text + " ～ " + array[5];
			this.lblTerm.Text = text;
			DataTable dataSource = this.EditDataList(this._pForm.SwkTgtData);
			this.dgvList.DataSource = dataSource;
			foreach (object obj in this.dgvList.Columns)
			{
				((DataGridViewColumn)obj).SortMode = DataGridViewColumnSortMode.NotSortable;
			}
//			this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//			this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9f);
			this.dgvList.Columns[0].Width = 187;
			this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
			this.dgvList.Columns[1].Width = 120;
			this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvList.Columns[2].Width = 120;
			this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvList.Columns[3].Width = 120;
			this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvList.Columns[4].Width = 120;
			this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvList.Columns[5].Width = 120;
			this.dgvList.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.lblGenkinUriage.Text = Util.FormatNum(this._sumTotalInfo.genkinUriage);
			this.lblGenkinZei.Text = Util.FormatNum(this._sumTotalInfo.genkinZei);
			this.lblKakeUriage.Text = Util.FormatNum(this._sumTotalInfo.kakeUriage);
			this.lblKakeZei.Text = Util.FormatNum(this._sumTotalInfo.kakeZei);
			this.lblKeiUriage.Text = Util.FormatNum(this._sumTotalInfo.keiUriage);
			if (base.InData != null && !ValChk.IsEmpty(base.InData) && ((string)base.InData).Length != 0)
			{
				this.btnF6.Enabled = false;
			}
		}

		// Token: 0x06000071 RID: 113 RVA: 0x0000FB14 File Offset: 0x0000DD14
		public override void PressF1()
		{
			using (KBDB1014 kbdb = new KBDB1014(this._pForm, this))
			{
				kbdb.ShowDialog(this);
			}
		}

		// Token: 0x06000072 RID: 114 RVA: 0x0000FB54 File Offset: 0x0000DD54
		public override void PressF6()
		{
			if (!this.btnF6.Enabled)
			{
				return;
			}
			if (Msg.ConfYesNo(((this._pForm.Mode == 2) ? "更新" : "登録") + "しますか？") == DialogResult.No)
			{
				return;
			}
			KBDB1016 kbdb = new KBDB1016();
			kbdb.Show();
			kbdb.Refresh();
			bool flag = false;
			try
			{
				base.Dba.BeginTransaction();
				flag = new KBDB1011DA(base.UInfo, base.Dba, base.Config).MakeSwkData(this._pForm.Mode, this._pForm.PackDpyNo, this._pForm.Condition, this._pForm.TaishakuData);
				kbdb.Close();
				if (flag)
				{
					base.Dba.Commit();
				}
				else
				{
					base.Dba.Rollback();
					Msg.Error("更新に失敗しました。" + Environment.NewLine + "もう一度やり直して下さい。");
				}
			}
			finally
			{
				base.Dba.Rollback();
			}
			if (flag)
			{
				base.DialogResult = DialogResult.OK;
				base.Close();
			}
		}

		// Token: 0x06000073 RID: 115 RVA: 0x0000FC70 File Offset: 0x0000DE70
		private void KBDB1013_Shown(object sender, EventArgs e)
		{
			if (this.dgvList.RowCount == 0)
			{
				Msg.Info("該当データがありません。");
				base.DialogResult = DialogResult.Cancel;
				base.Close();
			}
		}

		// Token: 0x06000074 RID: 116 RVA: 0x0000FC98 File Offset: 0x0000DE98
		private DataTable EditDataList(DataTable dtData)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("請求先名", typeof(string));
			dataTable.Columns.Add("現金売上額", typeof(string));
			dataTable.Columns.Add("現金消費税", typeof(string));
			dataTable.Columns.Add("掛売上", typeof(string));
			dataTable.Columns.Add("掛消費税", typeof(string));
			dataTable.Columns.Add("合計売上", typeof(string));
			string text = string.Empty;
			string value = string.Empty;
			this._sumSeikyuInfo.Clear();
			this._sumTotalInfo.Clear();
			int num = 2;
			try
			{
				num = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenkinTorihikiKbn"));
			}
			catch (Exception)
			{
				num = 2;
			}
			for (int i = 0; i < dtData.Rows.Count; i++)
			{
				if (!ValChk.IsEmpty(text) && !Util.ToString(dtData.Rows[i]["会員番号"]).Equals(text))
				{
					DataRow dataRow = dataTable.NewRow();
					dataRow["請求先名"] = value;
					dataRow["現金売上額"] = Util.FormatNum(this._sumSeikyuInfo.genkinUriage);
					dataRow["現金消費税"] = Util.FormatNum(this._sumSeikyuInfo.genkinZei);
					dataRow["掛売上"] = Util.FormatNum(this._sumSeikyuInfo.kakeUriage);
					dataRow["掛消費税"] = Util.FormatNum(this._sumSeikyuInfo.kakeZei);
					dataRow["合計売上"] = Util.FormatNum(this._sumSeikyuInfo.keiUriage);
					dataTable.Rows.Add(dataRow);
					this._sumSeikyuInfo.Clear();
				}
				int value2 = (Util.ToInt(dtData.Rows[i]["取引区分２"]) == 2) ? -1 : 1;
				if (Util.ToInt(dtData.Rows[i]["取引区分１"]) == num)
				{
					this._sumSeikyuInfo.genkinUriage = this._sumSeikyuInfo.genkinUriage + Util.ToDecimal(dtData.Rows[i]["売上金額"]) * value2;
					this._sumSeikyuInfo.genkinZei = this._sumSeikyuInfo.genkinZei + Util.ToDecimal(dtData.Rows[i]["消費税"]) * value2;
					this._sumTotalInfo.genkinUriage = this._sumTotalInfo.genkinUriage + Util.ToDecimal(dtData.Rows[i]["売上金額"]) * value2;
					this._sumTotalInfo.genkinZei = this._sumTotalInfo.genkinZei + Util.ToDecimal(dtData.Rows[i]["消費税"]) * value2;
				}
				else
				{
					this._sumSeikyuInfo.kakeUriage = this._sumSeikyuInfo.kakeUriage + Util.ToDecimal(dtData.Rows[i]["売上金額"]) * value2;
					this._sumSeikyuInfo.kakeZei = this._sumSeikyuInfo.kakeZei + Util.ToDecimal(dtData.Rows[i]["消費税"]) * value2;
					this._sumTotalInfo.kakeUriage = this._sumTotalInfo.kakeUriage + Util.ToDecimal(dtData.Rows[i]["売上金額"]) * value2;
					this._sumTotalInfo.kakeZei = this._sumTotalInfo.kakeZei + Util.ToDecimal(dtData.Rows[i]["消費税"]) * value2;
				}
				this._sumSeikyuInfo.keiUriage = this._sumSeikyuInfo.keiUriage + (Util.ToDecimal(dtData.Rows[i]["売上金額"]) + Util.ToDecimal(dtData.Rows[i]["消費税"])) * value2;
				this._sumTotalInfo.keiUriage = this._sumTotalInfo.keiUriage + (Util.ToDecimal(dtData.Rows[i]["売上金額"]) + Util.ToDecimal(dtData.Rows[i]["消費税"])) * value2;
				text = Util.ToString(dtData.Rows[i]["会員番号"]);
				value = Util.ToString(dtData.Rows[i]["会員名称"]);
			}
			if (dtData.Rows.Count > 0)
			{
				DataRow dataRow = dataTable.NewRow();
				dataRow["請求先名"] = value;
				dataRow["現金売上額"] = Util.FormatNum(this._sumSeikyuInfo.genkinUriage);
				dataRow["現金消費税"] = Util.FormatNum(this._sumSeikyuInfo.genkinZei);
				dataRow["掛売上"] = Util.FormatNum(this._sumSeikyuInfo.kakeUriage);
				dataRow["掛消費税"] = Util.FormatNum(this._sumSeikyuInfo.kakeZei);
				dataRow["合計売上"] = Util.FormatNum(this._sumSeikyuInfo.keiUriage);
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		// Token: 0x04000048 RID: 72
		private KBDB1013.Summary _sumSeikyuInfo;

		// Token: 0x04000049 RID: 73
		private KBDB1013.Summary _sumTotalInfo;

		// Token: 0x0400004A RID: 74
		private KBDB1011 _pForm;

		// Token: 0x0400004B RID: 75
		private ConfigLoader _config;

		// Token: 0x0200000C RID: 12
		private struct Summary
		{
			// Token: 0x06000095 RID: 149 RVA: 0x00014BCC File Offset: 0x00012DCC
			public void Clear()
			{
				this.genkinUriage = 0m;
				this.genkinZei = 0m;
				this.kakeUriage = 0m;
				this.kakeZei = 0m;
				this.keiUriage = 0m;
			}

			// Token: 0x0400008A RID: 138
			public decimal genkinUriage;

			// Token: 0x0400008B RID: 139
			public decimal genkinZei;

			// Token: 0x0400008C RID: 140
			public decimal kakeUriage;

			// Token: 0x0400008D RID: 141
			public decimal kakeZei;

			// Token: 0x0400008E RID: 142
			public decimal keiUriage;
		}
	}
}
