﻿namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000009 RID: 9
	public partial class KBDB1014 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x06000092 RID: 146 RVA: 0x00013C83 File Offset: 0x00011E83
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00013CA4 File Offset: 0x00011EA4
		private void InitializeComponent()
		{
            this.lblKarikataSum = new System.Windows.Forms.Label();
            this.lblKashikataSum = new System.Windows.Forms.Label();
            this.lblDpyDtDay = new System.Windows.Forms.Label();
            this.txtDpyDtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDtMonth = new System.Windows.Forms.Label();
            this.txtDpyDtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDtJpYear = new System.Windows.Forms.Label();
            this.txtDpyDtJpYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDt = new System.Windows.Forms.Label();
            this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
            this.lblKariAmount = new System.Windows.Forms.Label();
            this.lblKariZei = new System.Windows.Forms.Label();
            this.lblKashiZei = new System.Windows.Forms.Label();
            this.lblKashiAmount = new System.Windows.Forms.Label();
            this.txtDpyDtGengo = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(4, 65);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF1
            // 
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(89, 65);
            this.btnF7.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF6
            // 
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(175, 65);
            this.btnF8.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 420);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(888, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(899, 31);
            this.lblTitle.Text = "仕訳データ参照";
            // 
            // lblKarikataSum
            // 
            this.lblKarikataSum.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblKarikataSum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKarikataSum.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKarikataSum.ForeColor = System.Drawing.Color.White;
            this.lblKarikataSum.Location = new System.Drawing.Point(17, 423);
            this.lblKarikataSum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKarikataSum.Name = "lblKarikataSum";
            this.lblKarikataSum.Size = new System.Drawing.Size(198, 50);
            this.lblKarikataSum.TabIndex = 10;
            this.lblKarikataSum.Tag = "CHANGE";
            this.lblKarikataSum.Text = "[借方合計] ";
            this.lblKarikataSum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashikataSum
            // 
            this.lblKashikataSum.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblKashikataSum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashikataSum.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKashikataSum.ForeColor = System.Drawing.Color.White;
            this.lblKashikataSum.Location = new System.Drawing.Point(348, 423);
            this.lblKashikataSum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKashikataSum.Name = "lblKashikataSum";
            this.lblKashikataSum.Size = new System.Drawing.Size(411, 50);
            this.lblKashikataSum.TabIndex = 13;
            this.lblKashikataSum.Tag = "CHANGE";
            this.lblKashikataSum.Text = "[貸方合計] ";
            this.lblKashikataSum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDpyDtDay
            // 
            this.lblDpyDtDay.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtDay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtDay.Location = new System.Drawing.Point(305, 21);
            this.lblDpyDtDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDpyDtDay.Name = "lblDpyDtDay";
            this.lblDpyDtDay.Size = new System.Drawing.Size(24, 23);
            this.lblDpyDtDay.TabIndex = 8;
            this.lblDpyDtDay.Tag = "CHANGE";
            this.lblDpyDtDay.Text = "日";
            this.lblDpyDtDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtDay
            // 
            this.txtDpyDtDay.AutoSizeFromLength = true;
            this.txtDpyDtDay.DisplayLength = null;
            this.txtDpyDtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtDay.Location = new System.Drawing.Point(276, 21);
            this.txtDpyDtDay.Margin = new System.Windows.Forms.Padding(4);
            this.txtDpyDtDay.MaxLength = 2;
            this.txtDpyDtDay.Name = "txtDpyDtDay";
            this.txtDpyDtDay.ReadOnly = true;
            this.txtDpyDtDay.Size = new System.Drawing.Size(25, 23);
            this.txtDpyDtDay.TabIndex = 7;
            this.txtDpyDtDay.TabStop = false;
            this.txtDpyDtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDpyDtMonth
            // 
            this.lblDpyDtMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtMonth.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtMonth.Location = new System.Drawing.Point(251, 21);
            this.lblDpyDtMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDpyDtMonth.Name = "lblDpyDtMonth";
            this.lblDpyDtMonth.Size = new System.Drawing.Size(24, 23);
            this.lblDpyDtMonth.TabIndex = 6;
            this.lblDpyDtMonth.Tag = "CHANGE";
            this.lblDpyDtMonth.Text = "月";
            this.lblDpyDtMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtMonth
            // 
            this.txtDpyDtMonth.AutoSizeFromLength = true;
            this.txtDpyDtMonth.DisplayLength = null;
            this.txtDpyDtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtMonth.Location = new System.Drawing.Point(221, 21);
            this.txtDpyDtMonth.Margin = new System.Windows.Forms.Padding(4);
            this.txtDpyDtMonth.MaxLength = 2;
            this.txtDpyDtMonth.Name = "txtDpyDtMonth";
            this.txtDpyDtMonth.ReadOnly = true;
            this.txtDpyDtMonth.Size = new System.Drawing.Size(25, 23);
            this.txtDpyDtMonth.TabIndex = 5;
            this.txtDpyDtMonth.TabStop = false;
            this.txtDpyDtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDpyDtJpYear
            // 
            this.lblDpyDtJpYear.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtJpYear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDtJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtJpYear.Location = new System.Drawing.Point(196, 21);
            this.lblDpyDtJpYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDpyDtJpYear.Name = "lblDpyDtJpYear";
            this.lblDpyDtJpYear.Size = new System.Drawing.Size(24, 23);
            this.lblDpyDtJpYear.TabIndex = 4;
            this.lblDpyDtJpYear.Tag = "CHANGE";
            this.lblDpyDtJpYear.Text = "年";
            this.lblDpyDtJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtJpYear
            // 
            this.txtDpyDtJpYear.AutoSizeFromLength = true;
            this.txtDpyDtJpYear.DisplayLength = null;
            this.txtDpyDtJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtJpYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtJpYear.Location = new System.Drawing.Point(167, 21);
            this.txtDpyDtJpYear.Margin = new System.Windows.Forms.Padding(4);
            this.txtDpyDtJpYear.MaxLength = 2;
            this.txtDpyDtJpYear.Name = "txtDpyDtJpYear";
            this.txtDpyDtJpYear.ReadOnly = true;
            this.txtDpyDtJpYear.Size = new System.Drawing.Size(25, 23);
            this.txtDpyDtJpYear.TabIndex = 3;
            this.txtDpyDtJpYear.TabStop = false;
            this.txtDpyDtJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDpyDt
            // 
            this.lblDpyDt.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDt.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDt.Location = new System.Drawing.Point(17, 20);
            this.lblDpyDt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDpyDt.Name = "lblDpyDt";
            this.lblDpyDt.Size = new System.Drawing.Size(99, 24);
            this.lblDpyDt.TabIndex = 1;
            this.lblDpyDt.Tag = "CHANGE";
            this.lblDpyDt.Text = "伝 票 日 付";
            this.lblDpyDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mtbList
            // 
            this.mtbList.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.mtbList.FixedCols = 0;
            this.mtbList.FocusField = null;
            this.mtbList.Location = new System.Drawing.Point(17, 48);
            this.mtbList.Margin = new System.Windows.Forms.Padding(4);
            this.mtbList.Name = "mtbList";
            this.mtbList.NotSelectableCols = 0;
            this.mtbList.SelectRange = null;
            this.mtbList.Size = new System.Drawing.Size(876, 375);
            this.mtbList.TabIndex = 9;
            this.mtbList.Text = "sosMultiTable1";
            this.mtbList.UndoBufferEnabled = false;
            // 
            // lblKariAmount
            // 
            this.lblKariAmount.BackColor = System.Drawing.Color.White;
            this.lblKariAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKariAmount.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKariAmount.ForeColor = System.Drawing.Color.Black;
            this.lblKariAmount.Location = new System.Drawing.Point(215, 423);
            this.lblKariAmount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKariAmount.Name = "lblKariAmount";
            this.lblKariAmount.Size = new System.Drawing.Size(134, 26);
            this.lblKariAmount.TabIndex = 11;
            this.lblKariAmount.Text = "-99,999,999,999";
            this.lblKariAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKariZei
            // 
            this.lblKariZei.BackColor = System.Drawing.Color.White;
            this.lblKariZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKariZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKariZei.ForeColor = System.Drawing.Color.Black;
            this.lblKariZei.Location = new System.Drawing.Point(215, 447);
            this.lblKariZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKariZei.Name = "lblKariZei";
            this.lblKariZei.Size = new System.Drawing.Size(134, 26);
            this.lblKariZei.TabIndex = 12;
            this.lblKariZei.Text = "-99,999,999,999";
            this.lblKariZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashiZei
            // 
            this.lblKashiZei.BackColor = System.Drawing.Color.White;
            this.lblKashiZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashiZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKashiZei.ForeColor = System.Drawing.Color.Black;
            this.lblKashiZei.Location = new System.Drawing.Point(759, 447);
            this.lblKashiZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKashiZei.Name = "lblKashiZei";
            this.lblKashiZei.Size = new System.Drawing.Size(134, 26);
            this.lblKashiZei.TabIndex = 15;
            this.lblKashiZei.Text = "-99,999,999,999";
            this.lblKashiZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashiAmount
            // 
            this.lblKashiAmount.BackColor = System.Drawing.Color.White;
            this.lblKashiAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashiAmount.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKashiAmount.ForeColor = System.Drawing.Color.Black;
            this.lblKashiAmount.Location = new System.Drawing.Point(759, 423);
            this.lblKashiAmount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKashiAmount.Name = "lblKashiAmount";
            this.lblKashiAmount.Size = new System.Drawing.Size(134, 26);
            this.lblKashiAmount.TabIndex = 14;
            this.lblKashiAmount.Text = "-99,999,999,999";
            this.lblKashiAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDpyDtGengo
            // 
            this.txtDpyDtGengo.BackColor = System.Drawing.Color.Silver;
            this.txtDpyDtGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtDpyDtGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtGengo.Location = new System.Drawing.Point(119, 20);
            this.txtDpyDtGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtDpyDtGengo.Name = "txtDpyDtGengo";
            this.txtDpyDtGengo.Size = new System.Drawing.Size(44, 24);
            this.txtDpyDtGengo.TabIndex = 902;
            this.txtDpyDtGengo.Tag = "CHANGE";
            this.txtDpyDtGengo.Text = "平成";
            this.txtDpyDtGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDB1014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 557);
            this.Controls.Add(this.txtDpyDtGengo);
            this.Controls.Add(this.lblKashiZei);
            this.Controls.Add(this.lblKashiAmount);
            this.Controls.Add(this.lblKariZei);
            this.Controls.Add(this.lblKariAmount);
            this.Controls.Add(this.mtbList);
            this.Controls.Add(this.lblDpyDt);
            this.Controls.Add(this.lblDpyDtDay);
            this.Controls.Add(this.txtDpyDtDay);
            this.Controls.Add(this.lblDpyDtMonth);
            this.Controls.Add(this.txtDpyDtMonth);
            this.Controls.Add(this.lblDpyDtJpYear);
            this.Controls.Add(this.txtDpyDtJpYear);
            this.Controls.Add(this.lblKashikataSum);
            this.Controls.Add(this.lblKarikataSum);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBDB1014";
            this.ShowFButton = true;
            this.ShowTitle = false;
            this.Text = "仕訳データ参照";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblKarikataSum, 0);
            this.Controls.SetChildIndex(this.lblKashikataSum, 0);
            this.Controls.SetChildIndex(this.txtDpyDtJpYear, 0);
            this.Controls.SetChildIndex(this.lblDpyDtJpYear, 0);
            this.Controls.SetChildIndex(this.txtDpyDtMonth, 0);
            this.Controls.SetChildIndex(this.lblDpyDtMonth, 0);
            this.Controls.SetChildIndex(this.txtDpyDtDay, 0);
            this.Controls.SetChildIndex(this.lblDpyDtDay, 0);
            this.Controls.SetChildIndex(this.lblDpyDt, 0);
            this.Controls.SetChildIndex(this.mtbList, 0);
            this.Controls.SetChildIndex(this.lblKariAmount, 0);
            this.Controls.SetChildIndex(this.lblKariZei, 0);
            this.Controls.SetChildIndex(this.lblKashiAmount, 0);
            this.Controls.SetChildIndex(this.lblKashiZei, 0);
            this.Controls.SetChildIndex(this.txtDpyDtGengo, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		// Token: 0x04000074 RID: 116
		private global::System.ComponentModel.IContainer components = null;

		// Token: 0x04000075 RID: 117
		private global::System.Windows.Forms.Label lblKarikataSum;

		// Token: 0x04000076 RID: 118
		private global::System.Windows.Forms.Label lblKashikataSum;

		// Token: 0x04000077 RID: 119
		private global::System.Windows.Forms.Label lblDpyDtDay;

		// Token: 0x04000078 RID: 120
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtDay;

		// Token: 0x04000079 RID: 121
		private global::System.Windows.Forms.Label lblDpyDtMonth;

		// Token: 0x0400007A RID: 122
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtMonth;

		// Token: 0x0400007B RID: 123
		private global::System.Windows.Forms.Label lblDpyDtJpYear;

		// Token: 0x0400007C RID: 124
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtJpYear;

		// Token: 0x0400007E RID: 126
		private global::System.Windows.Forms.Label lblDpyDt;

		// Token: 0x0400007F RID: 127
		private global::jp.co.fsi.common.controls.SjMultiTable mtbList;

		// Token: 0x04000080 RID: 128
		private global::System.Windows.Forms.Label lblKariAmount;

		// Token: 0x04000081 RID: 129
		private global::System.Windows.Forms.Label lblKariZei;

		// Token: 0x04000082 RID: 130
		private global::System.Windows.Forms.Label lblKashiZei;

		// Token: 0x04000083 RID: 131
		private global::System.Windows.Forms.Label lblKashiAmount;
        private System.Windows.Forms.Label txtDpyDtGengo;
    }
}
