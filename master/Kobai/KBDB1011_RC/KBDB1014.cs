﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using jp.co.fsi.common.controls;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using systembase.table;

namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000009 RID: 9
	public partial class KBDB1014 : BasePgForm
	{
		// Token: 0x0600008B RID: 139 RVA: 0x00012E74 File Offset: 0x00011074
		public KBDB1014(KBDB1011 frm1, KBDB1013 frm3)
		{
			this._pForm1 = frm1;
			this._pForm3 = frm3;
			this.InitializeComponent();
		}

		// Token: 0x0600008C RID: 140 RVA: 0x00012E90 File Offset: 0x00011090
		protected override void InitForm()
		{
			this._curDataIdx = 0;
			this.InitDetailArea();
			this.DispData();
			string[] array = Util.ConvJpDate(Util.ToDate(this._pForm1.Condition["SwkDpyDt"]), base.Dba);
			this.txtDpyDtGengo.Text = array[0];
			this.txtDpyDtJpYear.Text = array[2];
			this.txtDpyDtMonth.Text = array[3];
			this.txtDpyDtDay.Text = array[4];
			this.ControlPrevNext();
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00012F14 File Offset: 0x00011114
		public override void PressF7()
		{
			if (!this.btnF7.Enabled)
			{
				return;
			}
			this._curDataIdx--;
			this.ControlPrevNext();
			this.DispData();
		}

		// Token: 0x0600008E RID: 142 RVA: 0x00012F3E File Offset: 0x0001113E
		public override void PressF8()
		{
			if (!this.btnF8.Enabled)
			{
				return;
			}
			this._curDataIdx++;
			this.ControlPrevNext();
			this.DispData();
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00012F68 File Offset: 0x00011168
		private void InitDetailArea()
		{
			UTable.CRecordProvider crecordProvider = new UTable.CRecordProvider();
			CLayoutBuilder clayoutBuilder = new CLayoutBuilder(CLayoutBuilder.EOrientation.ROW);
			clayoutBuilder.Ascend();
			UTable.CFieldDesc cfieldDesc = crecordProvider.AddField("借方勘定科目コード", new CTextFieldProvider("科目"), clayoutBuilder.Next());
			cfieldDesc.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc2 = crecordProvider.AddField("借方補助科目コード", new CTextFieldProvider("補助"), clayoutBuilder.Next());
			cfieldDesc2.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc2.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc2.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc3 = crecordProvider.AddField("借方部門コード", new CTextFieldProvider("部門"), clayoutBuilder.Next());
			cfieldDesc3.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc3.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc3.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc4 = crecordProvider.AddField("借方税区分", new CTextFieldProvider("税"), clayoutBuilder.Next());
			cfieldDesc4.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc4.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc4.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc5 = crecordProvider.AddField("借方事業区分", new CTextFieldProvider("業"), clayoutBuilder.Next());
			cfieldDesc5.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc5.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc5.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			clayoutBuilder.Descend();
			UTable.CFieldDesc cfieldDesc6 = crecordProvider.AddField("借方科目名", new CTextFieldProvider("借\u3000方\u3000科\u3000目"), clayoutBuilder.Next(1, 5));
			cfieldDesc6.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
			cfieldDesc6.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc6.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			clayoutBuilder.Break(5);
			UTable.CFieldDesc cfieldDesc7 = crecordProvider.AddField("借方金額", new CTextFieldProvider("金\u3000額"), clayoutBuilder.Next());
			cfieldDesc7.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc7.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc7.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc8 = crecordProvider.AddField("借方消費税", new CTextFieldProvider("消費税"), clayoutBuilder.Next());
			cfieldDesc8.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc8.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc8.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			clayoutBuilder.Break();
			UTable.CFieldDesc cfieldDesc9 = crecordProvider.AddField("未設定", new CTextFieldProvider(), clayoutBuilder.Next());
			cfieldDesc9.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
			cfieldDesc9.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc9.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc10 = crecordProvider.AddField("摘要", new CTextFieldProvider("摘\u3000\u3000要"), clayoutBuilder.Next());
			cfieldDesc10.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
			cfieldDesc10.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc10.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			clayoutBuilder.Break();
			clayoutBuilder.Ascend();
			UTable.CFieldDesc cfieldDesc11 = crecordProvider.AddField("貸方勘定科目コード", new CTextFieldProvider("科目"), clayoutBuilder.Next());
			cfieldDesc11.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc11.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc11.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc12 = crecordProvider.AddField("貸方補助科目コード", new CTextFieldProvider("補助"), clayoutBuilder.Next());
			cfieldDesc12.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc12.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc12.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc13 = crecordProvider.AddField("貸方部門コード", new CTextFieldProvider("部門"), clayoutBuilder.Next());
			cfieldDesc13.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc13.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc13.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc14 = crecordProvider.AddField("貸方税区分", new CTextFieldProvider("税"), clayoutBuilder.Next());
			cfieldDesc14.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc14.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc14.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc15 = crecordProvider.AddField("貸方事業区分", new CTextFieldProvider("業"), clayoutBuilder.Next());
			cfieldDesc15.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc15.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc15.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			clayoutBuilder.Descend();
			UTable.CFieldDesc cfieldDesc16 = crecordProvider.AddField("貸方科目名", new CTextFieldProvider("貸\u3000方\u3000科\u3000目"), clayoutBuilder.Next(1, 5));
			cfieldDesc16.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
			cfieldDesc16.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc16.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			clayoutBuilder.Break(5);
			UTable.CFieldDesc cfieldDesc17 = crecordProvider.AddField("貸方金額", new CTextFieldProvider("金\u3000額"), clayoutBuilder.Next());
			cfieldDesc17.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc17.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc17.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			UTable.CFieldDesc cfieldDesc18 = crecordProvider.AddField("貸方消費税", new CTextFieldProvider("消費税"), clayoutBuilder.Next());
			cfieldDesc18.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
			cfieldDesc18.Setting.Editable = UTable.EAllow.DISABLE;
			cfieldDesc18.Setting.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			this.mtbList.Content.SetRecordProvider(crecordProvider);
			this.mtbList.CreateCaption(UTable.EHAlign.MIDDLE);
			this.mtbList.Cols.SetSize(new int[]
			{
				36,
				36,
				36,
				20,
				20,
				100,
				160,
				36,
				36,
				36,
				20,
				20,
				100
			});
			this.mtbList.Setting.ContentBackColor = SystemColors.AppWorkspace;
		}

		// Token: 0x06000090 RID: 144 RVA: 0x0001357C File Offset: 0x0001177C
		private void DispData()
		{
			DataTable dataTable = this._pForm1.TaishakuData.Tables[this._curDataIdx];
			StringBuilder stringBuilder = new StringBuilder();
			string text = string.Empty;
			int num = Util.ToInt(dataTable.Rows[dataTable.Rows.Count - 1]["GYO_BANGO"]);
			this.mtbList.Content.ClearRecord();
			KBDB1014.Summary summary = default(KBDB1014.Summary);
			int num2 = 0;
			int num3 = 0;
			for (int i = 1; i <= num; i++)
			{
				UTable.CRecord crecord = this.mtbList.Content.AddRecord();
				stringBuilder = new StringBuilder();
				stringBuilder.Append("GYO_BANGO = " + Util.ToString(i));
				stringBuilder.Append(" AND TAISHAKU_KUBUN = 1");
				stringBuilder.Append(" AND MEISAI_KUBUN = 0");
				DataRow[] array = dataTable.Select(stringBuilder.ToString());
				if (array.Length != 0)
				{
					crecord.Fields["借方勘定科目コード"].Value = array[0]["KANJO_KAMOKU_CD"];
					crecord.Fields["借方補助科目コード"].Value = array[0]["HOJO_KAMOKU_CD"];
					crecord.Fields["借方部門コード"].Value = array[0]["BUMON_CD"];
					crecord.Fields["借方税区分"].Value = array[0]["ZEI_KUBUN"];
					crecord.Fields["借方事業区分"].Value = array[0]["JIGYO_KUBUN"];
					text = Util.ToString(array[0]["KANJO_KAMOKU_NM"]);
					if (!ValChk.IsEmpty(array[0]["HOJO_KAMOKU_NM"]))
					{
						text = text + " " + Util.ToString(array[0]["HOJO_KAMOKU_NM"]);
					}
					crecord.Fields["借方科目名"].Value = text;
					crecord.Fields["借方金額"].Value = Util.FormatNum(Util.ToDecimal(array[0]["ZEIKOMI_KINGAKU"]));
					summary.kariAmount += Util.ToDecimal(array[0]["ZEIKOMI_KINGAKU"]);
					if (Util.ToInt(array[0]["KAZEI_KUBUN"]) == 1 && Util.ToDecimal(array[0]["SHOHIZEI_KINGAKU"]) > 0m)
					{
						crecord.Fields["借方消費税"].Value = Util.FormatNum(Util.ToDecimal(array[0]["SHOHIZEI_KINGAKU"]));
						summary.kariZei += Util.ToDecimal(array[0]["SHOHIZEI_KINGAKU"]);
					}
					else
					{
						crecord.Fields["借方消費税"].Value = string.Empty;
					}
					crecord.Fields["摘要"].Value = Util.ToString(array[0]["TEKIYO"]);
				}
				else
				{
					num2 = 1;
				}
				stringBuilder = new StringBuilder();
				stringBuilder.Append("GYO_BANGO = " + Util.ToString(i));
				stringBuilder.Append(" AND TAISHAKU_KUBUN = 2");
				stringBuilder.Append(" AND MEISAI_KUBUN = 0");
				array = dataTable.Select(stringBuilder.ToString());
				if (array.Length != 0)
				{
					crecord.Fields["貸方勘定科目コード"].Value = array[0]["KANJO_KAMOKU_CD"];
					crecord.Fields["貸方補助科目コード"].Value = array[0]["HOJO_KAMOKU_CD"];
					crecord.Fields["貸方部門コード"].Value = array[0]["BUMON_CD"];
					crecord.Fields["貸方税区分"].Value = array[0]["ZEI_KUBUN"];
					crecord.Fields["貸方事業区分"].Value = array[0]["JIGYO_KUBUN"];
					text = Util.ToString(array[0]["KANJO_KAMOKU_NM"]);
					if (!ValChk.IsEmpty(array[0]["HOJO_KAMOKU_NM"]))
					{
						text = text + " " + Util.ToString(array[0]["HOJO_KAMOKU_NM"]);
					}
					crecord.Fields["貸方科目名"].Value = text;
					crecord.Fields["貸方金額"].Value = Util.FormatNum(Util.ToDecimal(array[0]["ZEIKOMI_KINGAKU"]));
					summary.kashiAmount += Util.ToDecimal(array[0]["ZEIKOMI_KINGAKU"]);
					if (Util.ToInt(array[0]["KAZEI_KUBUN"]) == 1 && Util.ToDecimal(array[0]["SHOHIZEI_KINGAKU"]) > 0m)
					{
						crecord.Fields["貸方消費税"].Value = Util.FormatNum(Util.ToDecimal(array[0]["SHOHIZEI_KINGAKU"]));
						summary.kashiZei += Util.ToDecimal(array[0]["SHOHIZEI_KINGAKU"]);
					}
					else
					{
						crecord.Fields["貸方消費税"].Value = string.Empty;
					}
					crecord.Fields["摘要"].Value = Util.ToString(array[0]["TEKIYO"]);
				}
				else
				{
					num3 = 1;
				}
				if (num2 == 1 && num3 == 1)
				{
					this.mtbList.Content.LastAddedRecord.Visible = false;
				}
				num2 = 0;
				num3 = 0;
			}
			this.lblKariAmount.Text = Util.FormatNum(summary.kariAmount);
			this.lblKariZei.Text = Util.FormatNum(summary.kariZei);
			this.lblKashiAmount.Text = Util.FormatNum(summary.kashiAmount);
			this.lblKashiZei.Text = Util.FormatNum(summary.kashiZei);
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00013BD4 File Offset: 0x00011DD4
		private void ControlPrevNext()
		{
			if (this._pForm1.TaishakuData.Tables.Count <= 1)
			{
				this.btnF7.Enabled = false;
				this.btnF8.Enabled = false;
				return;
			}
			if (this._curDataIdx == 0)
			{
				this.btnF7.Enabled = false;
				this.btnF8.Enabled = true;
				return;
			}
			if (this._curDataIdx == this._pForm1.TaishakuData.Tables.Count - 1)
			{
				this.btnF7.Enabled = true;
				this.btnF8.Enabled = false;
				return;
			}
			this.btnF7.Enabled = true;
			this.btnF8.Enabled = true;
		}

		// Token: 0x04000071 RID: 113
		private KBDB1011 _pForm1;

		// Token: 0x04000072 RID: 114
		private KBDB1013 _pForm3;

		// Token: 0x04000073 RID: 115
		private int _curDataIdx;

		// Token: 0x0200000D RID: 13
		private struct Summary
		{
			// Token: 0x06000096 RID: 150 RVA: 0x00014C0A File Offset: 0x00012E0A
			public void Clear()
			{
				this.kariAmount = 0m;
				this.kariZei = 0m;
				this.kashiAmount = 0m;
				this.kashiZei = 0m;
			}

			// Token: 0x0400008F RID: 143
			public decimal kariAmount;

			// Token: 0x04000090 RID: 144
			public decimal kariZei;

			// Token: 0x04000091 RID: 145
			public decimal kashiAmount;

			// Token: 0x04000092 RID: 146
			public decimal kashiZei;
		}
	}
}
