﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000006 RID: 6
	public partial class KBDB1012 : BasePgForm
	{
		// Token: 0x06000063 RID: 99 RVA: 0x0000E0A9 File Offset: 0x0000C2A9
		public KBDB1012()
		{
			this.InitializeComponent();
			base.BindGotFocusEvent();
		}

		// Token: 0x06000064 RID: 100 RVA: 0x0000E0C0 File Offset: 0x0000C2C0
		protected override void InitForm()
		{
			DataTable dtData = new DataTable();
			try
			{
				dtData = this.GetTB_HN_ZIDO_SHIWAKE_RIREKI();
			}
			catch (Exception ex)
			{
				Msg.Error(ex.Message);
				return;
			}
			DataTable dataSource = this.EditDataList(dtData);
			this.dgvList.DataSource = dataSource;
			foreach (object obj in this.dgvList.Columns)
			{
				((DataGridViewColumn)obj).SortMode = DataGridViewColumnSortMode.NotSortable;
			}
//			this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9f, FontStyle.Regular);
			this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			//			this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9f);

			//this.dgvList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells; //列幅自動調整

			this.dgvList.Columns[0].Width = 110;
			this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvList.Columns[1].Width = 150;
			this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvList.Columns[2].Width = 130;
			this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvList.Columns[3].Width = 150;
			this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvList.Columns[4].Width = 280;
			this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dgvList.Columns[5].Width = 240;
			this.dgvList.Columns[6].Width = 100;
			//this.dgvList.Columns[7].Width = 0;
			//this.dgvList.Columns[8].Width = 0;
			//this.dgvList.Columns[9].Width = 0;
			//this.dgvList.Columns[10].Width = 0;
			//this.dgvList.Columns[11].Width = 0;
			//this.dgvList.Columns[12].Width = 0;
			//this.dgvList.Columns[13].Width = 0;
			//this.dgvList.Columns[14].Width = 0;
			//this.dgvList.Columns[15].Width = 0;
			//this.dgvList.Columns[16].Width = 0;
			//this.dgvList.Columns[17].Width = 0;
			//this.dgvList.Columns[18].Width = 0;
			for (int i = 7; i < 19; i++)
			{
				this.dgvList.Columns[i].Visible = false;
			}
			this.dgvList.Columns[7].HeaderText = string.Empty;
			this.dgvList.Columns[8].HeaderText = string.Empty;
			this.dgvList.Columns[9].HeaderText = string.Empty;
			this.dgvList.Columns[10].HeaderText = string.Empty;
			this.dgvList.Columns[11].HeaderText = string.Empty;
			this.dgvList.Columns[12].HeaderText = string.Empty;
			this.dgvList.Columns[13].HeaderText = string.Empty;
			this.dgvList.Columns[14].HeaderText = string.Empty;
			this.dgvList.Columns[15].HeaderText = string.Empty;
			this.dgvList.Columns[16].HeaderText = string.Empty;
			this.dgvList.Columns[17].HeaderText = string.Empty;
			this.dgvList.Columns[18].HeaderText = string.Empty;
		}

		// Token: 0x06000065 RID: 101 RVA: 0x0000E580 File Offset: 0x0000C780
		public override void PressEsc()
		{
			base.DialogResult = DialogResult.Cancel;
			base.PressEsc();
		}

		// Token: 0x06000066 RID: 102 RVA: 0x0000E58F File Offset: 0x0000C78F
		private void dgvList_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				this.ReturnVal();
			}
		}

		// Token: 0x06000067 RID: 103 RVA: 0x0000E5A1 File Offset: 0x0000C7A1
		private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			this.ReturnVal();
		}

		// Token: 0x06000068 RID: 104 RVA: 0x0000E5A1 File Offset: 0x0000C7A1
		private void btnEnter_Click(object sender, EventArgs e)
		{
			this.ReturnVal();
		}

		// Token: 0x06000069 RID: 105 RVA: 0x0000E5A9 File Offset: 0x0000C7A9
		private void KBDB1012_Shown(object sender, EventArgs e)
		{
			if (this.dgvList.RowCount == 0)
			{
				Msg.Info("該当データがありません。");
				base.DialogResult = DialogResult.Cancel;
				base.Close();
			}
		}

		// Token: 0x0600006A RID: 106 RVA: 0x0000E5D0 File Offset: 0x0000C7D0
		private DataTable GetTB_HN_ZIDO_SHIWAKE_RIREKI()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  A.DENPYO_BANGO              AS 伝票番号 ");
			stringBuilder.Append(" ,A.DENPYO_DATE               AS 伝票日付 ");
			stringBuilder.Append(" ,MIN(A.SHIWAKE_DENPYO_BANGO) AS 仕訳伝票番号 ");
			stringBuilder.Append(" ,MAX(A.SHORI_KUBUN)          AS 処理区分 ");
			stringBuilder.Append(" ,MAX(CASE WHEN A.SHORI_KUBUN = 1 THEN '現金' ");
			stringBuilder.Append("           ELSE CASE WHEN A.SHORI_KUBUN = 2 THEN '現金、掛' ");
			stringBuilder.Append("                     ELSE CASE WHEN A.SHORI_KUBUN = 3 THEN '締日基準' ");
			stringBuilder.Append("                               ELSE '' ");
			stringBuilder.Append("                          END ");
			stringBuilder.Append("                END ");
			stringBuilder.Append("      END) AS 処理区分名称 ");
			stringBuilder.Append(" ,MAX(A.SHIMEBI)              AS 締日 ");
			stringBuilder.Append(" ,MAX(A.KAISHI_DENPYO_DATE)   AS 開始伝票日付 ");
			stringBuilder.Append(" ,MAX(A.SHURYO_DENPYO_DATE)   AS 終了伝票日付 ");
			stringBuilder.Append(" ,MAX(A.KAISHI_SEIKYUSAKI_CD) AS 開始請求先コード ");
			stringBuilder.Append(" ,MAX(CASE WHEN ISNULL(A.KAISHI_SEIKYUSAKI_CD, '') = '' THEN '先\u3000頭' ELSE B.TORIHIKISAKI_NM END) AS 開始請求先名 ");
			stringBuilder.Append(" ,MAX(A.SHURYO_SEIKYUSAKI_CD) AS 終了請求先コード ");
			stringBuilder.Append(" ,MAX(CASE WHEN ISNULL(A.SHURYO_SEIKYUSAKI_CD, '') = '' THEN '最\u3000後' ELSE C.TORIHIKISAKI_NM END) AS 終了請求先名 ");
			stringBuilder.Append(" ,MAX(A.TEKIYO_CD)            AS 摘要コード ");
			stringBuilder.Append(" ,MAX(A.TEKIYO)               AS 摘要名 ");
			stringBuilder.Append(" ,MAX(A.TANTOSHA_CD)          AS 担当者コード ");
			stringBuilder.Append(" ,MAX(D.TANTOSHA_NM)          AS 担当者名 ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("    TB_HN_ZIDO_SHIWAKE_RIREKI AS A ");
			stringBuilder.Append("LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B ");
			stringBuilder.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
			stringBuilder.Append("AND A.KAISHI_SEIKYUSAKI_CD = B.TORIHIKISAKI_CD ");
			stringBuilder.Append("LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS C ");
			stringBuilder.Append("ON A.KAISHA_CD = C.KAISHA_CD ");
			stringBuilder.Append("AND A.SHURYO_SEIKYUSAKI_CD = C.TORIHIKISAKI_CD ");
			stringBuilder.Append("LEFT OUTER JOIN TB_CM_TANTOSHA AS D ");
			stringBuilder.Append("ON A.KAISHA_CD = D.KAISHA_CD ");
			stringBuilder.Append("AND A.TANTOSHA_CD = D.TANTOSHA_CD ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    A.KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND A.SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND A.DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND A.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO ");
			stringBuilder.Append("GROUP BY ");
			stringBuilder.Append("  A.DENPYO_DATE ");
			stringBuilder.Append(" ,A.DENPYO_BANGO ");
			stringBuilder.Append(" ,A.SHORI_KUBUN ");
			stringBuilder.Append("ORDER BY ");
			stringBuilder.Append("  A.DENPYO_DATE DESC ");
			stringBuilder.Append(" ,A.DENPYO_BANGO DESC ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, base.UInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(base.UInfo.ShishoCd));
			dbParamCollection.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, base.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
			dbParamCollection.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, base.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]);
			return base.Dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x0600006B RID: 107 RVA: 0x0000E8A0 File Offset: 0x0000CAA0
		private DataTable EditDataList(DataTable dtData)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("仕訳伝票№", typeof(int));
			dataTable.Columns.Add("日 付", typeof(string));
			dataTable.Columns.Add("作成区分", typeof(string));
			dataTable.Columns.Add("締日", typeof(string));
			dataTable.Columns.Add("取引伝票日付範囲", typeof(string));
			dataTable.Columns.Add("請求先範囲", typeof(string));
			dataTable.Columns.Add("ﾃﾞｰﾀ担当者", typeof(string));
			dataTable.Columns.Add("返却用_伝票番号", typeof(string));
			dataTable.Columns.Add("返却用_伝票日付", typeof(string));
			dataTable.Columns.Add("返却用_処理区分", typeof(string));
			dataTable.Columns.Add("返却用_締日", typeof(string));
			dataTable.Columns.Add("返却用_開始伝票日付", typeof(string));
			dataTable.Columns.Add("返却用_終了伝票日付", typeof(string));
			dataTable.Columns.Add("返却用_開始請求先コード", typeof(string));
			dataTable.Columns.Add("返却用_終了請求先コード", typeof(string));
			dataTable.Columns.Add("返却用_摘要コード", typeof(string));
			dataTable.Columns.Add("返却用_摘要", typeof(string));
			dataTable.Columns.Add("返却用_担当者コード", typeof(string));
			dataTable.Columns.Add("返却用_担当者名", typeof(string));
			for (int i = 0; i < dtData.Rows.Count; i++)
			{
				DataRow dataRow = dataTable.NewRow();
				dataRow["仕訳伝票№"] = dtData.Rows[i]["仕訳伝票番号"];
				string[] array = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["伝票日付"]), base.Dba);
				string text = string.Concat(new string[]
				{
					//Util.ToInt(array[2]).ToString("00"),
					//"/",
					//array[3].PadLeft(2, ' '),
					//"/",
					//array[4].PadLeft(2, ' ')
					
					array[0], //元号表示
                    Util.ToInt(array[2]).ToString("00"),
					"年",
					array[3].PadLeft(2, ' '),
					"月",
					array[4].PadLeft(2, ' '),
					"日"
				});
				dataRow["日 付"] = text;
				dataRow["作成区分"] = dtData.Rows[i]["処理区分名称"];
				dataRow["締日"] = dtData.Rows[i]["締日"];
				array = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["開始伝票日付"]), base.Dba);
				text = string.Concat(new string[]
				{
					//Util.ToInt(array[2]).ToString("00"),
					//"/",
					//array[3].PadLeft(2, ' '),
					//"/",
					//array[4].PadLeft(2, ' ')

					array[0], //元号表示
                    Util.ToInt(array[2]).ToString("00"),
					"年",
					array[3].PadLeft(2, ' '),
					"月",
					array[4].PadLeft(2, ' '),
					"日"

				});
				array = Util.ConvJpDate(Util.ToDate(dtData.Rows[i]["終了伝票日付"]), base.Dba);
				text = string.Concat(new string[]
				{
					text,
					"～",
					//Util.ToInt(array[2]).ToString("00"),
					//"/",
					//array[3].PadLeft(2, ' '),
					//"/",
					//array[4].PadLeft(2, ' ')
					array[0], //元号表示
                    Util.ToInt(array[2]).ToString("00"),
					"年",
					array[3].PadLeft(2, ' '),
					"月",
					array[4].PadLeft(2, ' '),
					"日"
				});
				dataRow["取引伝票日付範囲"] = text;
				string text2;
				if (ValChk.IsEmpty(dtData.Rows[i]["開始請求先コード"]))
				{
					text2 = "    " + Util.ToString(dtData.Rows[i]["開始請求先名"]) + "          ";
				}
				else
				{
					text2 = Util.ToString(dtData.Rows[i]["開始請求先名"]) + new string(' ', 20 - Util.GetByteLength(dtData.Rows[i]["開始請求先名"].ToString()));
				}
				text2 = text2 + "～" + Util.ToString(dtData.Rows[i]["終了請求先名"]);
				dataRow["請求先範囲"] = text2;
				text2 = Util.ToString(dtData.Rows[i]["担当者コード"]) + " " + Util.ToString(dtData.Rows[i]["担当者名"]);
				dataRow["ﾃﾞｰﾀ担当者"] = text2;
				dataRow["返却用_伝票番号"] = Util.ToString(dtData.Rows[i]["伝票番号"]);
				dataRow["返却用_伝票日付"] = Util.ToDateStr(dtData.Rows[i]["伝票日付"]);
				dataRow["返却用_処理区分"] = Util.ToString(dtData.Rows[i]["処理区分"]);
				dataRow["返却用_締日"] = Util.ToString(dtData.Rows[i]["締日"]);
				dataRow["返却用_開始伝票日付"] = Util.ToDateStr(dtData.Rows[i]["開始伝票日付"]);
				dataRow["返却用_終了伝票日付"] = Util.ToDateStr(dtData.Rows[i]["終了伝票日付"]);
				dataRow["返却用_開始請求先コード"] = Util.ToString(dtData.Rows[i]["開始請求先コード"]);
				dataRow["返却用_終了請求先コード"] = Util.ToString(dtData.Rows[i]["終了請求先コード"]);
				dataRow["返却用_摘要コード"] = ((Util.ToInt(dtData.Rows[i]["摘要コード"]) == 0) ? string.Empty : Util.ToString(dtData.Rows[i]["摘要コード"]));
				dataRow["返却用_摘要"] = Util.ToString(dtData.Rows[i]["摘要名"]);
				dataRow["返却用_担当者コード"] = ((Util.ToInt(dtData.Rows[i]["担当者コード"]) == 0) ? string.Empty : Util.ToString(dtData.Rows[i]["担当者コード"]));
				dataRow["返却用_担当者名"] = Util.ToString(dtData.Rows[i]["担当者名"]);
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		// Token: 0x0600006C RID: 108 RVA: 0x0000F020 File Offset: 0x0000D220
		private void ReturnVal()
		{
			base.OutData = new string[]
			{
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_伝票番号"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_伝票日付"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_処理区分"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_締日"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_開始伝票日付"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_終了伝票日付"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_開始請求先コード"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_終了請求先コード"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_摘要コード"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_摘要"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_担当者コード"].Value),
				Util.ToString(this.dgvList.SelectedRows[0].Cells["返却用_担当者名"].Value)
			};
			base.DialogResult = DialogResult.OK;
			base.Close();
		}
	}
}
