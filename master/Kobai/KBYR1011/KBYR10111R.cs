﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbyr1011
{
    /// <summary>
    /// KBYR1011R の概要の説明です。
    /// </summary>
    public partial class KBYR10111R : BaseReport
    {

        public KBYR10111R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
