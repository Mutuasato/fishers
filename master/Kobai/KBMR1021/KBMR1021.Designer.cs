﻿namespace jp.co.fsi.kb.kbmr1021
{
    partial class KBMR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdoOutput2 = new System.Windows.Forms.RadioButton();
            this.rdoOutput1 = new System.Windows.Forms.RadioButton();
            this.txtTanabanTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTanabanFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet3 = new System.Windows.Forms.Label();
            this.rdoPaper2 = new System.Windows.Forms.RadioButton();
            this.rdoPaper1 = new System.Windows.Forms.RadioButton();
            this.rdoZeroOutput2 = new System.Windows.Forms.RadioButton();
            this.rdoZeroOutput1 = new System.Windows.Forms.RadioButton();
            this.rdoOutputRank3 = new System.Windows.Forms.RadioButton();
            this.rdoOutputRank2 = new System.Windows.Forms.RadioButton();
            this.rdoOutputRank1 = new System.Windows.Forms.RadioButton();
            this.lblTanabanNmTo = new System.Windows.Forms.Label();
            this.lblTanabanNmFr = new System.Windows.Forms.Label();
            this.lblTanabanCap = new System.Windows.Forms.Label();
            this.lblShohinNmTo = new System.Windows.Forms.Label();
            this.txtShohinCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet2 = new System.Windows.Forms.Label();
            this.lblShohinNmFr = new System.Windows.Forms.Label();
            this.txtShohinCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinCdCap = new System.Windows.Forms.Label();
            this.lblShohinKbnTo = new System.Windows.Forms.Label();
            this.txtShohinKbnTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.lblShohinKbnFr = new System.Windows.Forms.Label();
            this.txtShohinKbnFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinKbnCap = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDay = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel7 = new jp.co.fsi.common.FsiPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel9 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel11 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel10 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel7.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel9.SuspendLayout();
            this.fsiPanel11.SuspendLayout();
            this.fsiPanel10.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(7, 840);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1279, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1268, 41);
            this.lblTitle.Text = "棚卸表";
            // 
            // rdoOutput2
            // 
            this.rdoOutput2.AutoSize = true;
            this.rdoOutput2.BackColor = System.Drawing.Color.Silver;
            this.rdoOutput2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutput2.Location = new System.Drawing.Point(209, 3);
            this.rdoOutput2.Margin = new System.Windows.Forms.Padding(4);
            this.rdoOutput2.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoOutput2.Name = "rdoOutput2";
            this.rdoOutput2.Size = new System.Drawing.Size(74, 24);
            this.rdoOutput2.TabIndex = 1;
            this.rdoOutput2.Tag = "CHANGE";
            this.rdoOutput2.Text = "記入表";
            this.rdoOutput2.UseVisualStyleBackColor = false;
            // 
            // rdoOutput1
            // 
            this.rdoOutput1.AutoSize = true;
            this.rdoOutput1.BackColor = System.Drawing.Color.Silver;
            this.rdoOutput1.Checked = true;
            this.rdoOutput1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutput1.Location = new System.Drawing.Point(103, 3);
            this.rdoOutput1.Margin = new System.Windows.Forms.Padding(4);
            this.rdoOutput1.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoOutput1.Name = "rdoOutput1";
            this.rdoOutput1.Size = new System.Drawing.Size(74, 24);
            this.rdoOutput1.TabIndex = 0;
            this.rdoOutput1.TabStop = true;
            this.rdoOutput1.Tag = "CHANGE";
            this.rdoOutput1.Text = "棚卸表";
            this.rdoOutput1.UseVisualStyleBackColor = false;
            this.rdoOutput1.CheckedChanged += new System.EventHandler(this.rdoOutput_CheckedChanged);
            // 
            // txtTanabanTo
            // 
            this.txtTanabanTo.AutoSizeFromLength = true;
            this.txtTanabanTo.DisplayLength = null;
            this.txtTanabanTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTanabanTo.Location = new System.Drawing.Point(396, 6);
            this.txtTanabanTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtTanabanTo.MaxLength = 6;
            this.txtTanabanTo.Name = "txtTanabanTo";
            this.txtTanabanTo.Size = new System.Drawing.Size(63, 23);
            this.txtTanabanTo.TabIndex = 16;
            this.txtTanabanTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTanabanTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTanabanTo_KeyDown);
            this.txtTanabanTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTanabanTo_Validating);
            // 
            // txtTanabanFr
            // 
            this.txtTanabanFr.AutoSizeFromLength = true;
            this.txtTanabanFr.DisplayLength = null;
            this.txtTanabanFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTanabanFr.Location = new System.Drawing.Point(103, 6);
            this.txtTanabanFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtTanabanFr.MaxLength = 6;
            this.txtTanabanFr.Name = "txtTanabanFr";
            this.txtTanabanFr.Size = new System.Drawing.Size(63, 23);
            this.txtTanabanFr.TabIndex = 13;
            this.txtTanabanFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTanabanFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTanabanFr_Validating);
            // 
            // lblCodeBet3
            // 
            this.lblCodeBet3.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet3.Location = new System.Drawing.Point(367, 5);
            this.lblCodeBet3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet3.Name = "lblCodeBet3";
            this.lblCodeBet3.Size = new System.Drawing.Size(20, 24);
            this.lblCodeBet3.TabIndex = 15;
            this.lblCodeBet3.Tag = "CHANGE";
            this.lblCodeBet3.Text = "～";
            this.lblCodeBet3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoPaper2
            // 
            this.rdoPaper2.AutoSize = true;
            this.rdoPaper2.BackColor = System.Drawing.Color.Silver;
            this.rdoPaper2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoPaper2.Location = new System.Drawing.Point(209, 6);
            this.rdoPaper2.Margin = new System.Windows.Forms.Padding(4);
            this.rdoPaper2.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoPaper2.Name = "rdoPaper2";
            this.rdoPaper2.Size = new System.Drawing.Size(58, 24);
            this.rdoPaper2.TabIndex = 1;
            this.rdoPaper2.Tag = "CHANGE";
            this.rdoPaper2.Text = "よこ";
            this.rdoPaper2.UseVisualStyleBackColor = false;
            // 
            // rdoPaper1
            // 
            this.rdoPaper1.AutoSize = true;
            this.rdoPaper1.BackColor = System.Drawing.Color.Silver;
            this.rdoPaper1.Checked = true;
            this.rdoPaper1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoPaper1.Location = new System.Drawing.Point(103, 5);
            this.rdoPaper1.Margin = new System.Windows.Forms.Padding(4);
            this.rdoPaper1.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoPaper1.Name = "rdoPaper1";
            this.rdoPaper1.Size = new System.Drawing.Size(58, 24);
            this.rdoPaper1.TabIndex = 0;
            this.rdoPaper1.TabStop = true;
            this.rdoPaper1.Tag = "CHANGE";
            this.rdoPaper1.Text = "たて";
            this.rdoPaper1.UseVisualStyleBackColor = false;
            // 
            // rdoZeroOutput2
            // 
            this.rdoZeroOutput2.AutoSize = true;
            this.rdoZeroOutput2.BackColor = System.Drawing.Color.Silver;
            this.rdoZeroOutput2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeroOutput2.Location = new System.Drawing.Point(209, 6);
            this.rdoZeroOutput2.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeroOutput2.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeroOutput2.Name = "rdoZeroOutput2";
            this.rdoZeroOutput2.Size = new System.Drawing.Size(74, 24);
            this.rdoZeroOutput2.TabIndex = 1;
            this.rdoZeroOutput2.Tag = "CHANGE";
            this.rdoZeroOutput2.Text = "しない";
            this.rdoZeroOutput2.UseVisualStyleBackColor = false;
            // 
            // rdoZeroOutput1
            // 
            this.rdoZeroOutput1.AutoSize = true;
            this.rdoZeroOutput1.BackColor = System.Drawing.Color.Silver;
            this.rdoZeroOutput1.Checked = true;
            this.rdoZeroOutput1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeroOutput1.Location = new System.Drawing.Point(103, 5);
            this.rdoZeroOutput1.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeroOutput1.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeroOutput1.Name = "rdoZeroOutput1";
            this.rdoZeroOutput1.Size = new System.Drawing.Size(58, 24);
            this.rdoZeroOutput1.TabIndex = 0;
            this.rdoZeroOutput1.TabStop = true;
            this.rdoZeroOutput1.Tag = "CHANGE";
            this.rdoZeroOutput1.Text = "する";
            this.rdoZeroOutput1.UseVisualStyleBackColor = false;
            // 
            // rdoOutputRank3
            // 
            this.rdoOutputRank3.AutoSize = true;
            this.rdoOutputRank3.BackColor = System.Drawing.Color.Silver;
            this.rdoOutputRank3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutputRank3.Location = new System.Drawing.Point(363, 5);
            this.rdoOutputRank3.Margin = new System.Windows.Forms.Padding(4);
            this.rdoOutputRank3.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoOutputRank3.Name = "rdoOutputRank3";
            this.rdoOutputRank3.Size = new System.Drawing.Size(90, 24);
            this.rdoOutputRank3.TabIndex = 2;
            this.rdoOutputRank3.Tag = "CHANGE";
            this.rdoOutputRank3.Text = "商品合計";
            this.rdoOutputRank3.UseVisualStyleBackColor = false;
            this.rdoOutputRank3.Visible = false;
            // 
            // rdoOutputRank2
            // 
            this.rdoOutputRank2.AutoSize = true;
            this.rdoOutputRank2.BackColor = System.Drawing.Color.Silver;
            this.rdoOutputRank2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutputRank2.Location = new System.Drawing.Point(246, 5);
            this.rdoOutputRank2.Margin = new System.Windows.Forms.Padding(4);
            this.rdoOutputRank2.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoOutputRank2.Name = "rdoOutputRank2";
            this.rdoOutputRank2.Size = new System.Drawing.Size(106, 24);
            this.rdoOutputRank2.TabIndex = 1;
            this.rdoOutputRank2.Tag = "CHANGE";
            this.rdoOutputRank2.Text = "倉庫商品順";
            this.rdoOutputRank2.UseVisualStyleBackColor = false;
            this.rdoOutputRank2.Visible = false;
            // 
            // rdoOutputRank1
            // 
            this.rdoOutputRank1.AutoSize = true;
            this.rdoOutputRank1.BackColor = System.Drawing.Color.Silver;
            this.rdoOutputRank1.Checked = true;
            this.rdoOutputRank1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutputRank1.Location = new System.Drawing.Point(100, 5);
            this.rdoOutputRank1.Margin = new System.Windows.Forms.Padding(4);
            this.rdoOutputRank1.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoOutputRank1.Name = "rdoOutputRank1";
            this.rdoOutputRank1.Size = new System.Drawing.Size(106, 24);
            this.rdoOutputRank1.TabIndex = 0;
            this.rdoOutputRank1.TabStop = true;
            this.rdoOutputRank1.Tag = "CHANGE";
            this.rdoOutputRank1.Text = "商品倉庫順";
            this.rdoOutputRank1.UseVisualStyleBackColor = false;
            this.rdoOutputRank1.Visible = false;
            // 
            // lblTanabanNmTo
            // 
            this.lblTanabanNmTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblTanabanNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTanabanNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTanabanNmTo.Location = new System.Drawing.Point(466, 5);
            this.lblTanabanNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTanabanNmTo.Name = "lblTanabanNmTo";
            this.lblTanabanNmTo.Size = new System.Drawing.Size(211, 24);
            this.lblTanabanNmTo.TabIndex = 17;
            this.lblTanabanNmTo.Tag = "DISPNAME";
            this.lblTanabanNmTo.Text = "最　後";
            this.lblTanabanNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTanabanNmFr
            // 
            this.lblTanabanNmFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblTanabanNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTanabanNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTanabanNmFr.Location = new System.Drawing.Point(169, 5);
            this.lblTanabanNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTanabanNmFr.Name = "lblTanabanNmFr";
            this.lblTanabanNmFr.Size = new System.Drawing.Size(195, 24);
            this.lblTanabanNmFr.TabIndex = 14;
            this.lblTanabanNmFr.Tag = "DISPNAME";
            this.lblTanabanNmFr.Text = "先　頭";
            this.lblTanabanNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTanabanCap
            // 
            this.lblTanabanCap.BackColor = System.Drawing.Color.Silver;
            this.lblTanabanCap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTanabanCap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTanabanCap.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTanabanCap.Location = new System.Drawing.Point(0, 0);
            this.lblTanabanCap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTanabanCap.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblTanabanCap.Name = "lblTanabanCap";
            this.lblTanabanCap.Size = new System.Drawing.Size(816, 34);
            this.lblTanabanCap.TabIndex = 12;
            this.lblTanabanCap.Tag = "CHANGE";
            this.lblTanabanCap.Text = "棚　　　番";
            this.lblTanabanCap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinNmTo
            // 
            this.lblShohinNmTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNmTo.Location = new System.Drawing.Point(584, 4);
            this.lblShohinNmTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinNmTo.Name = "lblShohinNmTo";
            this.lblShohinNmTo.Size = new System.Drawing.Size(213, 24);
            this.lblShohinNmTo.TabIndex = 11;
            this.lblShohinNmTo.Tag = "DISPNAME";
            this.lblShohinNmTo.Text = "最　後";
            this.lblShohinNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdTo
            // 
            this.txtShohinCdTo.AutoSizeFromLength = true;
            this.txtShohinCdTo.DisplayLength = null;
            this.txtShohinCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdTo.Location = new System.Drawing.Point(465, 5);
            this.txtShohinCdTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtShohinCdTo.MaxLength = 13;
            this.txtShohinCdTo.Name = "txtShohinCdTo";
            this.txtShohinCdTo.Size = new System.Drawing.Size(110, 23);
            this.txtShohinCdTo.TabIndex = 10;
            this.txtShohinCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdTo_Validating);
            // 
            // lblCodeBet2
            // 
            this.lblCodeBet2.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet2.Location = new System.Drawing.Point(436, 4);
            this.lblCodeBet2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet2.Name = "lblCodeBet2";
            this.lblCodeBet2.Size = new System.Drawing.Size(20, 24);
            this.lblCodeBet2.TabIndex = 9;
            this.lblCodeBet2.Tag = "CHANGE";
            this.lblCodeBet2.Text = "～";
            this.lblCodeBet2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinNmFr
            // 
            this.lblShohinNmFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNmFr.Location = new System.Drawing.Point(216, 4);
            this.lblShohinNmFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinNmFr.Name = "lblShohinNmFr";
            this.lblShohinNmFr.Size = new System.Drawing.Size(213, 24);
            this.lblShohinNmFr.TabIndex = 8;
            this.lblShohinNmFr.Tag = "DISPNAME";
            this.lblShohinNmFr.Text = "先　頭";
            this.lblShohinNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdFr
            // 
            this.txtShohinCdFr.AutoSizeFromLength = true;
            this.txtShohinCdFr.DisplayLength = null;
            this.txtShohinCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdFr.Location = new System.Drawing.Point(103, 5);
            this.txtShohinCdFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtShohinCdFr.MaxLength = 13;
            this.txtShohinCdFr.Name = "txtShohinCdFr";
            this.txtShohinCdFr.Size = new System.Drawing.Size(110, 23);
            this.txtShohinCdFr.TabIndex = 7;
            this.txtShohinCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdFr_Validating);
            // 
            // lblShohinCdCap
            // 
            this.lblShohinCdCap.BackColor = System.Drawing.Color.Silver;
            this.lblShohinCdCap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinCdCap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShohinCdCap.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinCdCap.Location = new System.Drawing.Point(0, 0);
            this.lblShohinCdCap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinCdCap.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblShohinCdCap.Name = "lblShohinCdCap";
            this.lblShohinCdCap.Size = new System.Drawing.Size(816, 34);
            this.lblShohinCdCap.TabIndex = 6;
            this.lblShohinCdCap.Tag = "CHANGE";
            this.lblShohinCdCap.Text = "商品コード";
            this.lblShohinCdCap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbnTo
            // 
            this.lblShohinKbnTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinKbnTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKbnTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbnTo.Location = new System.Drawing.Point(446, 4);
            this.lblShohinKbnTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKbnTo.Name = "lblShohinKbnTo";
            this.lblShohinKbnTo.Size = new System.Drawing.Size(213, 24);
            this.lblShohinKbnTo.TabIndex = 5;
            this.lblShohinKbnTo.Tag = "DISPNAME";
            this.lblShohinKbnTo.Text = "最　後";
            this.lblShohinKbnTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKbnTo
            // 
            this.txtShohinKbnTo.AutoSizeFromLength = true;
            this.txtShohinKbnTo.DisplayLength = null;
            this.txtShohinKbnTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKbnTo.Location = new System.Drawing.Point(399, 5);
            this.txtShohinKbnTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtShohinKbnTo.MaxLength = 4;
            this.txtShohinKbnTo.Name = "txtShohinKbnTo";
            this.txtShohinKbnTo.Size = new System.Drawing.Size(44, 23);
            this.txtShohinKbnTo.TabIndex = 4;
            this.txtShohinKbnTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKbnTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbnTo_Validating);
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet1.Location = new System.Drawing.Point(367, 4);
            this.lblCodeBet1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet1.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(20, 24);
            this.lblCodeBet1.TabIndex = 3;
            this.lblCodeBet1.Tag = "CHANGE";
            this.lblCodeBet1.Text = "～";
            this.lblCodeBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbnFr
            // 
            this.lblShohinKbnFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinKbnFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKbnFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbnFr.Location = new System.Drawing.Point(150, 4);
            this.lblShohinKbnFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKbnFr.Name = "lblShohinKbnFr";
            this.lblShohinKbnFr.Size = new System.Drawing.Size(213, 24);
            this.lblShohinKbnFr.TabIndex = 2;
            this.lblShohinKbnFr.Tag = "DISPNAME";
            this.lblShohinKbnFr.Text = "先　頭";
            this.lblShohinKbnFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKbnFr
            // 
            this.txtShohinKbnFr.AutoSizeFromLength = true;
            this.txtShohinKbnFr.DisplayLength = null;
            this.txtShohinKbnFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKbnFr.Location = new System.Drawing.Point(103, 5);
            this.txtShohinKbnFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtShohinKbnFr.MaxLength = 4;
            this.txtShohinKbnFr.Name = "txtShohinKbnFr";
            this.txtShohinKbnFr.Size = new System.Drawing.Size(44, 23);
            this.txtShohinKbnFr.TabIndex = 1;
            this.txtShohinKbnFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKbnFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbnFr_Validating);
            // 
            // lblShohinKbnCap
            // 
            this.lblShohinKbnCap.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbnCap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKbnCap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShohinKbnCap.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbnCap.Location = new System.Drawing.Point(0, 0);
            this.lblShohinKbnCap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKbnCap.Name = "lblShohinKbnCap";
            this.lblShohinKbnCap.Size = new System.Drawing.Size(816, 34);
            this.lblShohinKbnCap.TabIndex = 0;
            this.lblShohinKbnCap.Tag = "CHANGE";
            this.lblShohinKbnCap.Text = "商品区分";
            this.lblShohinKbnCap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonth
            // 
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.Location = new System.Drawing.Point(246, 5);
            this.lblMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonth.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(27, 24);
            this.lblMonth.TabIndex = 4;
            this.lblMonth.Tag = "CHANGE";
            this.lblMonth.Text = "月";
            this.lblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblYear
            // 
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(186, 5);
            this.lblYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYear.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(27, 24);
            this.lblYear.TabIndex = 2;
            this.lblYear.Tag = "CHANGE";
            this.lblYear.Text = "年";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = true;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.Location = new System.Drawing.Point(155, 6);
            this.txtDateYear.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(28, 23);
            this.txtDateYear.TabIndex = 1;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.TextChanged += new System.EventHandler(this.txtDateYear_TextChanged);
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.LightCyan;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.Location = new System.Drawing.Point(103, 5);
            this.lblDateGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(51, 24);
            this.lblDateGengo.TabIndex = 0;
            this.lblDateGengo.Tag = "DISPNAME";
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = true;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonth.Location = new System.Drawing.Point(214, 6);
            this.txtDateMonth.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(28, 23);
            this.txtDateMonth.TabIndex = 3;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // txtDateDay
            // 
            this.txtDateDay.AutoSizeFromLength = true;
            this.txtDateDay.DisplayLength = null;
            this.txtDateDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDay.Location = new System.Drawing.Point(274, 6);
            this.txtDateDay.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateDay.MaxLength = 2;
            this.txtDateDay.Name = "txtDateDay";
            this.txtDateDay.Size = new System.Drawing.Size(28, 23);
            this.txtDateDay.TabIndex = 5;
            this.txtDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // lblDay
            // 
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDay.Location = new System.Drawing.Point(305, 5);
            this.lblDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDay.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(27, 24);
            this.lblDay.TabIndex = 6;
            this.lblDay.Tag = "CHANGE";
            this.lblDay.Text = "日";
            this.lblDay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(103, 4);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(149, 3);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0: 全ての商品",
            "1: 取引有りの商品のみ",
            "2: 取引中止の商品のみ"});
            this.comboBox1.Location = new System.Drawing.Point(103, 6);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(295, 24);
            this.comboBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(816, 34);
            this.label1.TabIndex = 904;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "支所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(816, 34);
            this.label2.TabIndex = 904;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "出力帳票";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(816, 34);
            this.label3.TabIndex = 904;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "用紙";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(816, 34);
            this.label4.TabIndex = 904;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "差ｾﾞﾛ出力";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(816, 34);
            this.label5.TabIndex = 904;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "棚卸日付";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(816, 34);
            this.label8.TabIndex = 904;
            this.label8.Tag = "CHANGE";
            this.label8.Text = "中止区分";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel7, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel8, 0, 6);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel9, 0, 7);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel11, 0, 9);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel10, 0, 8);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 10;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(826, 431);
            this.fsiTableLayoutPanel1.TabIndex = 905;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblDay);
            this.fsiPanel5.Controls.Add(this.lblDateGengo);
            this.fsiPanel5.Controls.Add(this.lblMonth);
            this.fsiPanel5.Controls.Add(this.txtDateDay);
            this.fsiPanel5.Controls.Add(this.lblYear);
            this.fsiPanel5.Controls.Add(this.txtDateYear);
            this.fsiPanel5.Controls.Add(this.txtDateMonth);
            this.fsiPanel5.Controls.Add(this.label5);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(5, 177);
            this.fsiPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.rdoZeroOutput1);
            this.fsiPanel4.Controls.Add(this.rdoZeroOutput2);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 134);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.rdoPaper1);
            this.fsiPanel3.Controls.Add(this.rdoPaper2);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 91);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.rdoOutput1);
            this.fsiPanel2.Controls.Add(this.rdoOutput2);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 48);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // fsiPanel7
            // 
            this.fsiPanel7.Controls.Add(this.label9);
            this.fsiPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel7.Location = new System.Drawing.Point(5, 220);
            this.fsiPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel7.Name = "fsiPanel7";
            this.fsiPanel7.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel7.TabIndex = 6;
            this.fsiPanel7.Tag = "CHANGE";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Silver;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(816, 34);
            this.label9.TabIndex = 906;
            this.label9.Tag = "CHANGE";
            this.label9.Text = "抽出範囲";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.Controls.Add(this.txtShohinKbnFr);
            this.fsiPanel8.Controls.Add(this.lblShohinKbnTo);
            this.fsiPanel8.Controls.Add(this.txtShohinKbnTo);
            this.fsiPanel8.Controls.Add(this.lblCodeBet1);
            this.fsiPanel8.Controls.Add(this.lblShohinKbnFr);
            this.fsiPanel8.Controls.Add(this.lblShohinKbnCap);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel8.Location = new System.Drawing.Point(5, 263);
            this.fsiPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel8.TabIndex = 7;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // fsiPanel9
            // 
            this.fsiPanel9.Controls.Add(this.txtShohinCdFr);
            this.fsiPanel9.Controls.Add(this.lblCodeBet2);
            this.fsiPanel9.Controls.Add(this.txtShohinCdTo);
            this.fsiPanel9.Controls.Add(this.lblShohinNmFr);
            this.fsiPanel9.Controls.Add(this.lblShohinNmTo);
            this.fsiPanel9.Controls.Add(this.lblShohinCdCap);
            this.fsiPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel9.Location = new System.Drawing.Point(5, 306);
            this.fsiPanel9.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel9.Name = "fsiPanel9";
            this.fsiPanel9.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel9.TabIndex = 6;
            this.fsiPanel9.Tag = "CHANGE";
            // 
            // fsiPanel11
            // 
            this.fsiPanel11.Controls.Add(this.comboBox1);
            this.fsiPanel11.Controls.Add(this.label8);
            this.fsiPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel11.Location = new System.Drawing.Point(5, 392);
            this.fsiPanel11.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel11.Name = "fsiPanel11";
            this.fsiPanel11.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel11.TabIndex = 6;
            this.fsiPanel11.Tag = "CHANGE";
            // 
            // fsiPanel10
            // 
            this.fsiPanel10.Controls.Add(this.txtTanabanFr);
            this.fsiPanel10.Controls.Add(this.lblTanabanNmTo);
            this.fsiPanel10.Controls.Add(this.lblCodeBet3);
            this.fsiPanel10.Controls.Add(this.txtTanabanTo);
            this.fsiPanel10.Controls.Add(this.lblTanabanNmFr);
            this.fsiPanel10.Controls.Add(this.lblTanabanCap);
            this.fsiPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel10.Location = new System.Drawing.Point(5, 349);
            this.fsiPanel10.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel10.Name = "fsiPanel10";
            this.fsiPanel10.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel10.TabIndex = 6;
            this.fsiPanel10.Tag = "CHANGE";
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.rdoOutputRank2);
            this.fsiPanel6.Controls.Add(this.rdoOutputRank1);
            this.fsiPanel6.Controls.Add(this.rdoOutputRank3);
            this.fsiPanel6.Controls.Add(this.label6);
            this.fsiPanel6.Location = new System.Drawing.Point(10, 501);
            this.fsiPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(816, 34);
            this.fsiPanel6.TabIndex = 5;
            this.fsiPanel6.Tag = "CHANGE";
            this.fsiPanel6.Visible = false;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(816, 34);
            this.label6.TabIndex = 904;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "出力順位";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Visible = false;
            // 
            // KBMR1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 977);
            this.Controls.Add(this.fsiPanel6);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBMR1021";
            this.Text = "棚卸表";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.fsiPanel6, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel7.ResumeLayout(false);
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.fsiPanel9.ResumeLayout(false);
            this.fsiPanel9.PerformLayout();
            this.fsiPanel11.ResumeLayout(false);
            this.fsiPanel10.ResumeLayout(false);
            this.fsiPanel10.PerformLayout();
            this.fsiPanel6.ResumeLayout(false);
            this.fsiPanel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton rdoOutput2;
        private System.Windows.Forms.RadioButton rdoOutput1;
        private jp.co.fsi.common.controls.FsiTextBox txtTanabanTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTanabanFr;
        private System.Windows.Forms.Label lblCodeBet3;
        private System.Windows.Forms.RadioButton rdoPaper2;
        private System.Windows.Forms.RadioButton rdoPaper1;
        private System.Windows.Forms.RadioButton rdoZeroOutput2;
        private System.Windows.Forms.RadioButton rdoZeroOutput1;
        private System.Windows.Forms.RadioButton rdoOutputRank3;
        private System.Windows.Forms.RadioButton rdoOutputRank2;
        private System.Windows.Forms.RadioButton rdoOutputRank1;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbnFr;
        private System.Windows.Forms.Label lblShohinKbnCap;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.Label lblShohinKbnFr;
        private System.Windows.Forms.Label lblShohinKbnTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbnTo;
        private System.Windows.Forms.Label lblCodeBet2;
        private System.Windows.Forms.Label lblShohinNmFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdFr;
        private System.Windows.Forms.Label lblShohinCdCap;
        private System.Windows.Forms.Label lblTanabanNmTo;
        private System.Windows.Forms.Label lblTanabanNmFr;
        private System.Windows.Forms.Label lblTanabanCap;
        private System.Windows.Forms.Label lblShohinNmTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdTo;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Label lblYear;
        private common.controls.FsiTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private common.controls.FsiTextBox txtDateMonth;
        private common.controls.FsiTextBox txtDateDay;
        private System.Windows.Forms.Label lblDay;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel8;
        private common.FsiPanel fsiPanel7;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label9;
        private common.FsiPanel fsiPanel9;
        private common.FsiPanel fsiPanel11;
        private common.FsiPanel fsiPanel10;
        private System.Windows.Forms.Label label6;
    }
}