﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;


namespace jp.co.fsi.kb.kbmr1021
{
    /// <summary>
    /// KOBR3032R の帳票
    /// </summary>
    public partial class KBMR10212R : BaseReport
    {
        //件数カウンタ
        //private int intRowNumber;

        public KBMR10212R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, EventArgs e)
        {
            // 合計金額が0の場合、空表示とする
            if (this.txtIrisu.Text == "0")
            {
                this.txtIrisu.Text = "";
            }
            if (this.txtSouko.Text == "0")
            {
                this.txtSouko.Text = "";
            }
            if (this.txtKesusu01.Text == "0.00")
            {
                this.txtKesusu01.Text = "";
            }
            if (this.txtBarasu01.Text == "0.00")
            {
                this.txtBarasu01.Text = "";
            }
            if (this.txtTyoboKingaku.Text == "0")
            {
                this.txtTyoboKingaku.Text = "";
            }
            if (this.txtKesusu02.Text == "0.00")
            {
                this.txtKesusu02.Text = "";
            }
            if (this.txtBarasu02.Text == "0.00")
            {
                this.txtBarasu02.Text = "";
            }
            if (this.txtKesusu03.Text == "0.00")
            {
                this.txtKesusu03.Text = "";
            }
            if (this.txtBarasu03.Text == "0.00")
            {
                this.txtBarasu03.Text = "";
            }
            if (this.txtSaiKingaku.Text == "0")
            {
                this.txtSaiKingaku.Text = "";
            }
            if (this.txtZaikoKingaku.Text == "0")
            {
                this.txtZaikoKingaku.Text = "";
            }

            //this.line14.Visible = false;
            //intRowNumber = intRowNumber + 1;
            //if (intRowNumber % 17 == 0)
            //{
            //    this.line14.Visible = true;
            //}

        }

        private void reportFooter1_Format(object sender, EventArgs e)
        {
            // 合計金額が0の場合、空表示とする
            if (this.txtTotalIrisu.Text == "0")
            {
                this.txtTotalIrisu.Text = "";
            }
            if (this.txtTotalSouko.Text == "0")
            {
                this.txtTotalSouko.Text = "";
            }
            if (this.txtTotalKesusu01.Text == "0")
            {
                this.txtTotalKesusu01.Text = "";
            }
            if (this.txtTotalBarasu01.Text == "0.00")
            {
                this.txtTotalBarasu01.Text = "";
            }
            if (this.txtTotalTyoboKingaku.Text == "0")
            {
                this.txtTotalTyoboKingaku.Text = "";
            }
            if (this.txtTotalKesusu02.Text == "0")
            {
                this.txtTotalKesusu02.Text = "";
            }
            if (this.txtTotalBarasu02.Text == "0.00")
            {
                this.txtTotalBarasu02.Text = "";
            }
            if (this.txtTotalKesusu03.Text == "0")
            {
                this.txtTotalKesusu03.Text = "";
            }
            if (this.txtTotalBarasu03.Text == "0.00")
            {
                this.txtTotalBarasu03.Text = "";
            }
            if (this.txtTotalSaiKingaku.Text == "0.0")
            {
                this.txtTotalSaiKingaku.Text = "";
            }
            if (this.txtTotalZaikoKingaku.Text == "0")
            {
                this.txtTotalZaikoKingaku.Text = "";
            }

        }

        private void pageHeader_Format(object sender, EventArgs e)
        {

        }

        private void pageFooter_Format(object sender, EventArgs e)
        {

        }

        private void KBMR10212R_ReportStart(object sender, EventArgs e)
        {

        }
    }
}
