﻿namespace jp.co.fsi.kob.kobb3041
{
    partial class KOBB3041
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbTnorsDt = new System.Windows.Forms.GroupBox();
            this.lblTnorsDtDay = new System.Windows.Forms.Label();
            this.txtTnorsDtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTnorsDtMonth = new System.Windows.Forms.Label();
            this.txtTnorsDtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTnorsDtJpYear = new System.Windows.Forms.Label();
            this.txtTnorsDtJpYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTnorsDtGengo = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gpbTnorsDt.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "棚卸更新";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 531);
            this.pnlDebug.Size = new System.Drawing.Size(843, 100);
            // 
            // gpbTnorsDt
            // 
            this.gpbTnorsDt.Controls.Add(this.lblTnorsDtGengo);
            this.gpbTnorsDt.Controls.Add(this.lblTnorsDtDay);
            this.gpbTnorsDt.Controls.Add(this.txtTnorsDtDay);
            this.gpbTnorsDt.Controls.Add(this.lblTnorsDtMonth);
            this.gpbTnorsDt.Controls.Add(this.txtTnorsDtMonth);
            this.gpbTnorsDt.Controls.Add(this.lblTnorsDtJpYear);
            this.gpbTnorsDt.Controls.Add(this.txtTnorsDtJpYear);
            this.gpbTnorsDt.Controls.Add(this.lblDate);
            this.gpbTnorsDt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbTnorsDt.Location = new System.Drawing.Point(12, 52);
            this.gpbTnorsDt.Name = "gpbTnorsDt";
            this.gpbTnorsDt.Size = new System.Drawing.Size(218, 62);
            this.gpbTnorsDt.TabIndex = 1;
            this.gpbTnorsDt.TabStop = false;
            this.gpbTnorsDt.Text = "棚卸日付";
            // 
            // lblTnorsDtDay
            // 
            this.lblTnorsDtDay.BackColor = System.Drawing.Color.Silver;
            this.lblTnorsDtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnorsDtDay.Location = new System.Drawing.Point(170, 24);
            this.lblTnorsDtDay.Name = "lblTnorsDtDay";
            this.lblTnorsDtDay.Size = new System.Drawing.Size(18, 20);
            this.lblTnorsDtDay.TabIndex = 6;
            this.lblTnorsDtDay.Text = "日";
            this.lblTnorsDtDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTnorsDtDay
            // 
            this.txtTnorsDtDay.AutoSizeFromLength = true;
            this.txtTnorsDtDay.DisplayLength = null;
            this.txtTnorsDtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTnorsDtDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTnorsDtDay.Location = new System.Drawing.Point(147, 24);
            this.txtTnorsDtDay.MaxLength = 2;
            this.txtTnorsDtDay.Name = "txtTnorsDtDay";
            this.txtTnorsDtDay.Size = new System.Drawing.Size(20, 20);
            this.txtTnorsDtDay.TabIndex = 5;
            this.txtTnorsDtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTnorsDtDay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTnorsDtDay_KeyDown);
            this.txtTnorsDtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtDay_Validating);
            // 
            // lblTnorsDtMonth
            // 
            this.lblTnorsDtMonth.BackColor = System.Drawing.Color.Silver;
            this.lblTnorsDtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnorsDtMonth.Location = new System.Drawing.Point(128, 24);
            this.lblTnorsDtMonth.Name = "lblTnorsDtMonth";
            this.lblTnorsDtMonth.Size = new System.Drawing.Size(18, 20);
            this.lblTnorsDtMonth.TabIndex = 4;
            this.lblTnorsDtMonth.Text = "月";
            this.lblTnorsDtMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTnorsDtMonth
            // 
            this.txtTnorsDtMonth.AutoSizeFromLength = true;
            this.txtTnorsDtMonth.DisplayLength = null;
            this.txtTnorsDtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTnorsDtMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTnorsDtMonth.Location = new System.Drawing.Point(106, 24);
            this.txtTnorsDtMonth.MaxLength = 2;
            this.txtTnorsDtMonth.Name = "txtTnorsDtMonth";
            this.txtTnorsDtMonth.Size = new System.Drawing.Size(20, 20);
            this.txtTnorsDtMonth.TabIndex = 3;
            this.txtTnorsDtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTnorsDtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtMonth_Validating);
            // 
            // lblTnorsDtJpYear
            // 
            this.lblTnorsDtJpYear.BackColor = System.Drawing.Color.Silver;
            this.lblTnorsDtJpYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnorsDtJpYear.Location = new System.Drawing.Point(87, 24);
            this.lblTnorsDtJpYear.Name = "lblTnorsDtJpYear";
            this.lblTnorsDtJpYear.Size = new System.Drawing.Size(18, 20);
            this.lblTnorsDtJpYear.TabIndex = 2;
            this.lblTnorsDtJpYear.Text = "年";
            this.lblTnorsDtJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTnorsDtJpYear
            // 
            this.txtTnorsDtJpYear.AutoSizeFromLength = true;
            this.txtTnorsDtJpYear.DisplayLength = null;
            this.txtTnorsDtJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTnorsDtJpYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTnorsDtJpYear.Location = new System.Drawing.Point(65, 24);
            this.txtTnorsDtJpYear.MaxLength = 2;
            this.txtTnorsDtJpYear.Name = "txtTnorsDtJpYear";
            this.txtTnorsDtJpYear.Size = new System.Drawing.Size(20, 20);
            this.txtTnorsDtJpYear.TabIndex = 1;
            this.txtTnorsDtJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTnorsDtJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtJpYear_Validating);
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.Location = new System.Drawing.Point(22, 21);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(166, 26);
            this.lblDate.TabIndex = 8;
            // 
            // lblTnorsDtGengo
            // 
            this.lblTnorsDtGengo.BackColor = System.Drawing.Color.Silver;
            this.lblTnorsDtGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTnorsDtGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnorsDtGengo.Location = new System.Drawing.Point(25, 24);
            this.lblTnorsDtGengo.Name = "lblTnorsDtGengo";
            this.lblTnorsDtGengo.Size = new System.Drawing.Size(38, 20);
            this.lblTnorsDtGengo.TabIndex = 902;
            this.lblTnorsDtGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KOBB3041
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 634);
            this.Controls.Add(this.gpbTnorsDt);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "KOBB3041";
            this.Text = "棚卸更新";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gpbTnorsDt, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gpbTnorsDt.ResumeLayout(false);
            this.gpbTnorsDt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbTnorsDt;
        private System.Windows.Forms.Label lblTnorsDtDay;
        private common.controls.FsiTextBox txtTnorsDtDay;
        private System.Windows.Forms.Label lblTnorsDtMonth;
        private common.controls.FsiTextBox txtTnorsDtMonth;
        private System.Windows.Forms.Label lblTnorsDtJpYear;
        private common.controls.FsiTextBox txtTnorsDtJpYear;
        private System.Windows.Forms.Label lblTnorsDtGengo;
        private System.Windows.Forms.Label lblDate;


    }
}