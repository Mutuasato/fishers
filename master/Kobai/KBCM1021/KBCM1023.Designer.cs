﻿namespace jp.co.fsi.kb.kbcm1021
{
    partial class KBCM1023
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblShohinCdTo = new System.Windows.Forms.Label();
			this.lblShohinCdFr = new System.Windows.Forms.Label();
			this.txtShohinCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtShohinCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShishoCdTo = new System.Windows.Forms.Label();
			this.lblShishoCdFr = new System.Windows.Forms.Label();
			this.txtShishoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtShishoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnF4
			// 
			this.btnF4.Text = "F4\r\n\r\nﾌﾟﾚﾋﾞｭｰ";
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 106);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1011, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1000, 31);
			this.lblTitle.Text = "";
			// 
			// lblShohinCdTo
			// 
			this.lblShohinCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblShohinCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinCdTo.Location = new System.Drawing.Point(736, 4);
			this.lblShohinCdTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShohinCdTo.Name = "lblShohinCdTo";
			this.lblShohinCdTo.Size = new System.Drawing.Size(205, 24);
			this.lblShohinCdTo.TabIndex = 6;
			this.lblShohinCdTo.Tag = "DISPNAME";
			this.lblShohinCdTo.Text = "最  後";
			this.lblShohinCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohinCdFr
			// 
			this.lblShohinCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblShohinCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinCdFr.Location = new System.Drawing.Point(333, 4);
			this.lblShohinCdFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShohinCdFr.Name = "lblShohinCdFr";
			this.lblShohinCdFr.Size = new System.Drawing.Size(205, 24);
			this.lblShohinCdFr.TabIndex = 3;
			this.lblShohinCdFr.Tag = "DISPNAME";
			this.lblShohinCdFr.Text = "先  頭";
			this.lblShohinCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohinCdTo
			// 
			this.txtShohinCdTo.AutoSizeFromLength = false;
			this.txtShohinCdTo.DisplayLength = null;
			this.txtShohinCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinCdTo.Location = new System.Drawing.Point(584, 5);
			this.txtShohinCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinCdTo.MaxLength = 13;
			this.txtShohinCdTo.Name = "txtShohinCdTo";
			this.txtShohinCdTo.Size = new System.Drawing.Size(143, 23);
			this.txtShohinCdTo.TabIndex = 5;
			this.txtShohinCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdTo_Validating);
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Silver;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(547, 4);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(29, 27);
			this.label1.TabIndex = 4;
			this.label1.Tag = "CHANGE";
			this.label1.Text = "～";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtShohinCdFr
			// 
			this.txtShohinCdFr.AutoSizeFromLength = false;
			this.txtShohinCdFr.DisplayLength = null;
			this.txtShohinCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohinCdFr.Location = new System.Drawing.Point(181, 5);
			this.txtShohinCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohinCdFr.MaxLength = 13;
			this.txtShohinCdFr.Name = "txtShohinCdFr";
			this.txtShohinCdFr.Size = new System.Drawing.Size(143, 23);
			this.txtShohinCdFr.TabIndex = 4;
			this.txtShohinCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdFr_Validating);
			// 
			// lblShishoCdTo
			// 
			this.lblShishoCdTo.BackColor = System.Drawing.Color.LightCyan;
			this.lblShishoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShishoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShishoCdTo.Location = new System.Drawing.Point(736, 3);
			this.lblShishoCdTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShishoCdTo.Name = "lblShishoCdTo";
			this.lblShishoCdTo.Size = new System.Drawing.Size(205, 24);
			this.lblShishoCdTo.TabIndex = 6;
			this.lblShishoCdTo.Tag = "DISPNAME";
			this.lblShishoCdTo.Text = "最  後";
			this.lblShishoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShishoCdFr
			// 
			this.lblShishoCdFr.BackColor = System.Drawing.Color.LightCyan;
			this.lblShishoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShishoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShishoCdFr.Location = new System.Drawing.Point(333, 3);
			this.lblShishoCdFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShishoCdFr.Name = "lblShishoCdFr";
			this.lblShishoCdFr.Size = new System.Drawing.Size(205, 24);
			this.lblShishoCdFr.TabIndex = 3;
			this.lblShishoCdFr.Tag = "DISPNAME";
			this.lblShishoCdFr.Text = "先  頭";
			this.lblShishoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShishoCdTo
			// 
			this.txtShishoCdTo.AutoSizeFromLength = false;
			this.txtShishoCdTo.DisplayLength = null;
			this.txtShishoCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShishoCdTo.Location = new System.Drawing.Point(584, 4);
			this.txtShishoCdTo.Margin = new System.Windows.Forms.Padding(4);
			this.txtShishoCdTo.MaxLength = 4;
			this.txtShishoCdTo.Name = "txtShishoCdTo";
			this.txtShishoCdTo.Size = new System.Drawing.Size(143, 23);
			this.txtShishoCdTo.TabIndex = 2;
			this.txtShishoCdTo.TabStop = false;
			this.txtShishoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShishoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCdTo_Validating);
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Silver;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label4.Location = new System.Drawing.Point(547, 4);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(29, 27);
			this.label4.TabIndex = 4;
			this.label4.Tag = "CHANGE";
			this.label4.Text = "～";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtShishoCdFr
			// 
			this.txtShishoCdFr.AutoSizeFromLength = false;
			this.txtShishoCdFr.DisplayLength = null;
			this.txtShishoCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShishoCdFr.Location = new System.Drawing.Point(181, 4);
			this.txtShishoCdFr.Margin = new System.Windows.Forms.Padding(4);
			this.txtShishoCdFr.MaxLength = 4;
			this.txtShishoCdFr.Name = "txtShishoCdFr";
			this.txtShishoCdFr.Size = new System.Drawing.Size(143, 23);
			this.txtShishoCdFr.TabIndex = 1;
			this.txtShishoCdFr.TabStop = false;
			this.txtShishoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShishoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCdFr_Validating);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 16);
			this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(991, 86);
			this.fsiTableLayoutPanel1.TabIndex = 903;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.lblShohinCdTo);
			this.fsiPanel2.Controls.Add(this.lblShohinCdFr);
			this.fsiPanel2.Controls.Add(this.txtShohinCdFr);
			this.fsiPanel2.Controls.Add(this.txtShohinCdTo);
			this.fsiPanel2.Controls.Add(this.label1);
			this.fsiPanel2.Controls.Add(this.label3);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(5, 47);
			this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(981, 34);
			this.fsiPanel2.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Silver;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(981, 34);
			this.label3.TabIndex = 1;
			this.label3.Tag = "CHANGE";
			this.label3.Text = "商品コード範囲";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.lblShishoCdTo);
			this.fsiPanel1.Controls.Add(this.lblShishoCdFr);
			this.fsiPanel1.Controls.Add(this.txtShishoCdFr);
			this.fsiPanel1.Controls.Add(this.txtShishoCdTo);
			this.fsiPanel1.Controls.Add(this.label4);
			this.fsiPanel1.Controls.Add(this.label2);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
			this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(981, 33);
			this.fsiPanel1.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Silver;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(981, 33);
			this.label2.TabIndex = 0;
			this.label2.Tag = "CHANGE";
			this.label2.Text = "支所コード範囲";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// KBCM1023
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1000, 243);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "KBCM1023";
			this.Text = "商品の登録";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblShohinCdTo;
        private System.Windows.Forms.Label lblShohinCdFr;
        private System.Windows.Forms.Label lblShishoCdTo;
        private System.Windows.Forms.Label lblShishoCdFr;
        private common.controls.FsiTextBox txtShishoCdTo;
        private System.Windows.Forms.Label label4;
        private common.controls.FsiTextBox txtShishoCdFr;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label3;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label2;
    }
}