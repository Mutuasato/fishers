﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbcm1021
{
    /// <summary>
    /// 商品の登録(KBCM1021)
    /// </summary>
    public partial class KBCM1021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "商品の検索";
        #endregion

        #region private変数
        // InitForm実行時フラグ
        private int _initFormFlg = 0;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBCM1021()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            this._initFormFlg = 1;
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // フォームの配置を上へ移動する
                //this.lblShisho.Location = new System.Drawing.Point(13, 13);
                //this.txtShishoCd.Location = new System.Drawing.Point(91, 15);
                //this.lblShishoNm.Location = new System.Drawing.Point(126, 15);

                //this.lblKanaNm.Location = new System.Drawing.Point(13, 43);
                //this.txtKanaNm.Location = new System.Drawing.Point(88, 45);
                //this.grpSearchCheck.Location = new System.Drawing.Point(510, 43);
                //this.dgvList.Location = new System.Drawing.Point(13, 80);
                // サイズを縮める
                this.Size = new Size(876, 533);
                //this.dgvList.Size = new Size(681, 279);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnEnter.Visible = true;
                this.btnEnter.Location = this.btnF2.Location;
                this.btnF1.Location = this.btnF3.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            //本所以外は支所コードは訂正不可
#if DEBUG
            txtShishoCd.Text = "1";
            this.UInfo.ShishoCd = txtShishoCd.Text;
#else
            txtShishoCd.Text = this.UInfo.ShishoCd;
#endif
            this.txtShishoCd.Enabled = (txtShishoCd.Text == "1") ? true : false;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtShishoCd.Text, this.txtShishoCd.Text);

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // 一旦あいまい検索チェックにフォーカスする
            this.chkLikeSearch.Focus();
            if (!MODE_CD_SRC.Equals(this.Par1))
            {
                // 一旦あいまい検索チェックのグループを非表示にする、念のためチェックはオフ
                this.chkLikeSearch.Checked = false;
//                this.grpSearchCheck.Visible = false;
            }

            // カナ名にフォーカス
            this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaNm.Focus();

            this._initFormFlg = 0;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付,船主CDに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShishoCd":
                case "dgvList":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            //String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShishoCd.Text = outData[0];
                                this.lblShishoNm.Text = outData[1];
                            }
                        }
                    }
                    break;
            }

        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ商品登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 商品登録画面の起動
                EditShohin(string.Empty);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // メンテ機能で立ち上げている場合のみ商品の印刷画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                KBCM1023 frmKBCM1023;
                // 商品マスタ一覧画面を起動
                frmKBCM1023 = new KBCM1023();
                frmKBCM1023.Par2 = this.txtShishoCd.Text;

                DialogResult result = frmKBCM1023.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    // Gridに再度フォーカスをセット
                    this.ActiveControl = this.dgvList;
                    this.dgvList.Focus();
                }
            }
        }

        public override void PressF9()
        {
            ShowConfig();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtShishoCd.Text, this.lblShishoNm.Text, this.txtShishoCd.MaxLength) || !IsValidShishoCd())
            {
                e.Cancel = true;
                this.txtShishoCd.SelectAll();
                this.txtShishoCd.Focus();
            }
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaNm_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない
            // InitFormメソッド実行中はキャンセルする
            if (this._initFormFlg.Equals(1))
            {
                e.Cancel = true;
                return;
            }
            // 入力された情報を元に検索する
            SearchData(false);
        }

        private void txtShishoCd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                Assembly asm;
                Type t;
                string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
                // アセンブリのロード
                asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                // フォーム作成
                t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                if (t != null)
                {
                    Object obj = Activator.CreateInstance(t);
                    if (obj != null)
                    {
                        BasePgForm frm = (BasePgForm)obj;
                        frm.Par1 = "1";
                        frm.ShowDialog(this);

                        if (frm.DialogResult == DialogResult.OK)
                        {
                            string[] outData = (string[])frm.OutData;
                            this.txtShishoCd.Text = outData[0];
                            this.lblShishoNm.Text = outData[1];
                        }
                    }
                }

            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditShohin(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
                    e.Handled = true;
                }
            }

            if (e.KeyCode == Keys.F1)
            {
                // カナ名にフォーカスを戻す
                this.txtKanaNm.Focus();
                this.txtKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditShohin(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
            }
        }

#endregion

#region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtShishoCd.Text) || Equals(this.txtShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtShishoCd.Text = "0";
                this.lblShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            string shishoCd = this.txtShishoCd.Text;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);

            if (ValChk.IsEmpty(this.lblShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            string shishoCd = txtShishoCd.Text;

            // 商品テーブルからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND SHOHIN_KUBUN5 <> 1");
            if (this.Par3 == "1")
            {
                where.Append(" AND ISNULL(CHUSHI_KUBUN, 0) = 1");
            }
            //where.Append(" AND BARCODE1 <> 999"); //魚種はテーブルを分けたので不要
            if (isInitial)
            {
                // 初期処理の場合、検索結果がヒットしないようにあり得ない検索条件を設定する
                //where.Append(" AND SHOHIN_CD = -1");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaNm.Text))
                {
                    where.Append(" AND SHOHIN_KANA_NM LIKE @SHOHIN_KANA_NM");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    // 呼び出しパラメータかつ、あいまい検索チェックがオフの時は前方検索
                    if (MODE_CD_SRC.Equals(this.Par1) && !this.chkLikeSearch.Checked)
                    {
                        dpc.SetParam("@SHOHIN_KANA_NM", SqlDbType.VarChar, 30, this.txtKanaNm.Text + "%");
                    }
                    else
                    {
                        dpc.SetParam("@SHOHIN_KANA_NM", SqlDbType.VarChar, 30, "%" + this.txtKanaNm.Text + "%");
                    }
                }
            }
            string cols = "SHOHIN_CD AS コード";
            cols += ", SHOHIN_NM AS '商　品　名'";
            cols += ", KIKAKU AS '規　格'";
            // 呼び出しパラメータ時ｹｰｽ数とﾊﾞﾗ数
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                cols += ", '' AS ｹｰｽ数";
                cols += ", '' AS ﾊﾞﾗ数";
            }
            else
            {
                cols += ", SHOHIN_KANA_NM AS 'カ　ナ　名'";
                cols += ", TANABAN AS '棚　番'";
                cols += ", IRISU AS 入数";
                cols += ", TANI AS 単位";
                cols += ", CHUSHI_KUBUN_NM AS 中止区分";
            }

            string from = "VI_HN_SHOHIN";

            DataTable dtShohin =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "SHOHIN_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtShohin.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtShohin.Rows.Add(dtShohin.NewRow());
            }

            this.dgvList.DataSource = dtShohin;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 80;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            // 呼び出しパラメータ時
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                this.dgvList.Columns[1].Width = 230;
                this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dgvList.Columns[2].Width = 150;
                this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dgvList.Columns[3].Width = 100;
                this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvList.Columns[4].Width = 100;
                this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            else
            {
                this.dgvList.Columns[1].Width = 200;
                this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dgvList.Columns[2].Width = 90;
                this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dgvList.Columns[3].Width = 150;
                this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dgvList.Columns[4].Width = 80;
                this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvList.Columns[5].Width = 50;
                this.dgvList.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvList.Columns[6].Width = 60;
                this.dgvList.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvList.Columns[7].Width = 85;
                this.dgvList.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
        }

        /// <summary>
        /// 商品を追加編集する
        /// </summary>
        /// <param name="code">商品コード(空：新規登録、以外：編集)</param>
        private void EditShohin(string code)
        {
            KBCM1022 frmKBCM1032;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmKBCM1032 = new KBCM1022("1");
                frmKBCM1032.Par1 = "1";
                frmKBCM1032.Par2 = txtShishoCd.Text;
            }
            else
            {
                // 編集モードで登録画面を起動
                frmKBCM1032 = new KBCM1022("2");
                frmKBCM1032.Par1 = "2";
                frmKBCM1032.Par2 = txtShishoCd.Text;
                frmKBCM1032.InData = code;
            }

            DialogResult result = frmKBCM1032.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["商　品　名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["規　格"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 商品マスタ初期設定画面を表示する
        /// </summary>
        private void ShowConfig()
        {
            KBCM1024 frmKBCM1024;

            // 新規登録モードで登録画面を起動
            frmKBCM1024 = new KBCM1024();
            frmKBCM1024.Par1 = this.txtShishoCd.Text;

            DialogResult result = frmKBCM1024.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }
        #endregion

    }
}
