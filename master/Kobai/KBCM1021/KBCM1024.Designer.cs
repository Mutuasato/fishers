﻿namespace jp.co.fsi.kb.kbcm1021
{
    partial class KBCM1024
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.txtShohizeiKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohiZeiKbn = new System.Windows.Forms.Label();
			this.lblShiireShiwakeCdResults = new System.Windows.Forms.Label();
			this.lblUriageShiwakeCdResults = new System.Windows.Forms.Label();
			this.txtUriageShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblUriageShiwakeCd = new System.Windows.Forms.Label();
			this.txtShiireShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShiireShiwakeCd = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 112);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(650, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(639, 31);
			this.lblTitle.Text = "";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.LightCyan;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.Location = new System.Drawing.Point(187, 3);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(312, 24);
			this.label1.TabIndex = 917;
			this.label1.Tag = "CHANGE";
			this.label1.Text = " 0:外税　1:内税";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiKbn
			// 
			this.txtShohizeiKbn.AllowDrop = true;
			this.txtShohizeiKbn.AutoSizeFromLength = false;
			this.txtShohizeiKbn.DisplayLength = null;
			this.txtShohizeiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiKbn.Location = new System.Drawing.Point(132, 3);
			this.txtShohizeiKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtShohizeiKbn.MaxLength = 4;
			this.txtShohizeiKbn.Name = "txtShohizeiKbn";
			this.txtShohizeiKbn.Size = new System.Drawing.Size(51, 23);
			this.txtShohizeiKbn.TabIndex = 3;
			this.txtShohizeiKbn.Text = "0";
			this.txtShohizeiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohizeiKbn_KeyDown);
			this.txtShohizeiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiKbn_Validating);
			// 
			// lblShohiZeiKbn
			// 
			this.lblShohiZeiKbn.BackColor = System.Drawing.Color.Silver;
			this.lblShohiZeiKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohiZeiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohiZeiKbn.Location = new System.Drawing.Point(0, 0);
			this.lblShohiZeiKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohiZeiKbn.Name = "lblShohiZeiKbn";
			this.lblShohiZeiKbn.Size = new System.Drawing.Size(617, 30);
			this.lblShohiZeiKbn.TabIndex = 915;
			this.lblShohiZeiKbn.Tag = "CHANGE";
			this.lblShohiZeiKbn.Text = "消 費 税 区 分";
			this.lblShohiZeiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShiireShiwakeCdResults
			// 
			this.lblShiireShiwakeCdResults.BackColor = System.Drawing.Color.LightCyan;
			this.lblShiireShiwakeCdResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShiireShiwakeCdResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiireShiwakeCdResults.Location = new System.Drawing.Point(187, 2);
			this.lblShiireShiwakeCdResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiireShiwakeCdResults.Name = "lblShiireShiwakeCdResults";
			this.lblShiireShiwakeCdResults.Size = new System.Drawing.Size(312, 24);
			this.lblShiireShiwakeCdResults.TabIndex = 914;
			this.lblShiireShiwakeCdResults.Tag = "DISPNAME";
			this.lblShiireShiwakeCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblUriageShiwakeCdResults
			// 
			this.lblUriageShiwakeCdResults.BackColor = System.Drawing.Color.LightCyan;
			this.lblUriageShiwakeCdResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblUriageShiwakeCdResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblUriageShiwakeCdResults.Location = new System.Drawing.Point(187, 2);
			this.lblUriageShiwakeCdResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblUriageShiwakeCdResults.Name = "lblUriageShiwakeCdResults";
			this.lblUriageShiwakeCdResults.Size = new System.Drawing.Size(312, 24);
			this.lblUriageShiwakeCdResults.TabIndex = 911;
			this.lblUriageShiwakeCdResults.Tag = "DISPNAME";
			this.lblUriageShiwakeCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtUriageShiwakeCd
			// 
			this.txtUriageShiwakeCd.AllowDrop = true;
			this.txtUriageShiwakeCd.AutoSizeFromLength = false;
			this.txtUriageShiwakeCd.DisplayLength = null;
			this.txtUriageShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtUriageShiwakeCd.Location = new System.Drawing.Point(132, 2);
			this.txtUriageShiwakeCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtUriageShiwakeCd.MaxLength = 4;
			this.txtUriageShiwakeCd.Name = "txtUriageShiwakeCd";
			this.txtUriageShiwakeCd.Size = new System.Drawing.Size(51, 23);
			this.txtUriageShiwakeCd.TabIndex = 1;
			this.txtUriageShiwakeCd.Text = "0";
			this.txtUriageShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtUriageShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtUriageShiwakeCd_Validating);
			// 
			// lblUriageShiwakeCd
			// 
			this.lblUriageShiwakeCd.BackColor = System.Drawing.Color.Silver;
			this.lblUriageShiwakeCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblUriageShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblUriageShiwakeCd.Location = new System.Drawing.Point(0, 0);
			this.lblUriageShiwakeCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblUriageShiwakeCd.Name = "lblUriageShiwakeCd";
			this.lblUriageShiwakeCd.Size = new System.Drawing.Size(617, 29);
			this.lblUriageShiwakeCd.TabIndex = 909;
			this.lblUriageShiwakeCd.Tag = "CHANGE";
			this.lblUriageShiwakeCd.Text = "売上仕訳コード";
			this.lblUriageShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShiireShiwakeCd
			// 
			this.txtShiireShiwakeCd.AllowDrop = true;
			this.txtShiireShiwakeCd.AutoSizeFromLength = false;
			this.txtShiireShiwakeCd.DisplayLength = null;
			this.txtShiireShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShiireShiwakeCd.Location = new System.Drawing.Point(132, 3);
			this.txtShiireShiwakeCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtShiireShiwakeCd.MaxLength = 4;
			this.txtShiireShiwakeCd.Name = "txtShiireShiwakeCd";
			this.txtShiireShiwakeCd.Size = new System.Drawing.Size(51, 23);
			this.txtShiireShiwakeCd.TabIndex = 2;
			this.txtShiireShiwakeCd.Text = "0";
			this.txtShiireShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShiireShiwakeCd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShiireShiwakeCd_KeyDown);
			this.txtShiireShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireShiwakeCd_Validating);
			// 
			// lblShiireShiwakeCd
			// 
			this.lblShiireShiwakeCd.BackColor = System.Drawing.Color.Silver;
			this.lblShiireShiwakeCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShiireShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShiireShiwakeCd.Location = new System.Drawing.Point(0, 0);
			this.lblShiireShiwakeCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShiireShiwakeCd.Name = "lblShiireShiwakeCd";
			this.lblShiireShiwakeCd.Size = new System.Drawing.Size(617, 29);
			this.lblShiireShiwakeCd.TabIndex = 912;
			this.lblShiireShiwakeCd.Tag = "CHANGE";
			this.lblShiireShiwakeCd.Text = "仕入仕訳コード";
			this.lblShiireShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 10);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 3;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(625, 110);
			this.fsiTableLayoutPanel1.TabIndex = 903;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.label1);
			this.fsiPanel3.Controls.Add(this.txtShohizeiKbn);
			this.fsiPanel3.Controls.Add(this.lblShohiZeiKbn);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 76);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(617, 30);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtShiireShiwakeCd);
			this.fsiPanel2.Controls.Add(this.lblShiireShiwakeCdResults);
			this.fsiPanel2.Controls.Add(this.lblShiireShiwakeCd);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 40);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(617, 29);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtUriageShiwakeCd);
			this.fsiPanel1.Controls.Add(this.lblUriageShiwakeCdResults);
			this.fsiPanel1.Controls.Add(this.lblUriageShiwakeCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(617, 29);
			this.fsiPanel1.TabIndex = 0;
			// 
			// KBCM1024
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(639, 249);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "KBCM1024";
			this.ShowFButton = true;
			this.Text = "商品マスタ初期設定";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblShiireShiwakeCdResults;
        private System.Windows.Forms.Label lblUriageShiwakeCdResults;
        private common.controls.FsiTextBox txtUriageShiwakeCd;
        private System.Windows.Forms.Label lblUriageShiwakeCd;
        private common.controls.FsiTextBox txtShiireShiwakeCd;
        private System.Windows.Forms.Label lblShiireShiwakeCd;
        private System.Windows.Forms.Label label1;
        private common.controls.FsiTextBox txtShohizeiKbn;
        private System.Windows.Forms.Label lblShohiZeiKbn;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}