﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

//using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbcm1021
{
    /// <summary>
    /// 商品の登録_印刷条件(KBCM1023)
    /// </summary>
    public partial class KBCM1023 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBCM1023()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();

        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string shishoCd = this.Par2;

            this.txtShishoCdFr.Text = shishoCd;
            this.txtShishoCdTo.Text = shishoCd;
            this.lblShishoCdFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);
            this.lblShishoCdTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);
            this.txtShishoCdFr.Enabled = (txtShishoCdFr.Text == "1") ? true : false;
            this.txtShishoCdTo.Enabled = (txtShishoCdTo.Text == "1") ? true : false;

            // タイトルは非表示
            this.lblTitle.Visible = false;

            // ボタンを設定
            this.ShowFButton = true;
            this.btnEsc.Visible = true;
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = true;
            this.btnF5.Visible = true;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false; //非表示に変更
            this.btnF12.Visible = false;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;

            // フォーカス設定
            this.txtShohinCdFr.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtShishoCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShishoCdFr.Text = outData[0];
                                this.lblShishoCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShishoCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShishoCdTo.Text = outData[0];
                                this.lblShishoCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShohinCdFr":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShohinCdFr.Text = outData[0];
                            }
                        }
                    }
                    break;

                case "txtShohinCdTo":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShohinCdTo.Text = outData[0];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 支所コード(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShishoCdFr())
            {
                e.Cancel = true;
                this.txtShishoCdFr.SelectAll();
            }

            this.txtShishoCdTo.Focus();

        }

        /// <summary>
        /// 仕入先コード(他)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShishoCdTo())
            {
                e.Cancel = true;
                this.txtShishoCdTo.SelectAll();
            }

            txtShohinCdFr.Focus();
        }

        /// <summary>
        /// 仕入先コード(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCdFr())
            {
                e.Cancel = true;
                this.txtShohinCdFr.SelectAll();
            }
            txtShohinCdTo.Focus();
        }

        /// <summary>
        /// 仕入先コード(他)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCdTo())
            {
                e.Cancel = true;
                this.txtShohinCdTo.SelectAll();
            }
        }

        private void DoEnter()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinCdFr.Text))
            {
                Msg.Notice("商品コード(自)は数値のみで入力してください。");
                this.txtShohinCdFr.SelectAll();
                //e.Cancel = true;
                return;
            }

        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 支所コードの入力チェック(先頭）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShishoCdFr()
        {
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.txtShishoCdFr.Text))
            {
                this.lblShishoCdFr.Text = "先　頭";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShishoCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string shishoCd = this.txtShishoCdFr.Text;
            string name = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, this.txtShishoCdFr.Text);
            this.lblShishoCdFr.Text = name;
            return true;
        }
        /// <summary>
        /// 支所コードの入力チェック(最後）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShishoCdTo()
        {
            // 未入力の場合「最後」を表示
            if (ValChk.IsEmpty(this.txtShishoCdTo.Text))
            {
                this.lblShishoCdTo.Text = "最　後";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShishoCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string shishoCd = this.txtShishoCdTo.Text;
            string name = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);
            this.lblShishoCdTo.Text = name;
            return true;
        }

        /// <summary>
        /// 仕入先コードの入力チェック(先頭）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinCdFr()
        {
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.txtShohinCdFr.Text))
            {
                this.lblShohinCdFr.Text = "先　頭";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShohinCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string shishoCd = this.txtShishoCdTo.Text;
            string name = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN", shishoCd, this.txtShohinCdFr.Text);
            this.lblShohinCdFr.Text = name;
            return true;
        }

        /// <summary>
        /// 仕入先コードの入力チェック(最後）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtShohinCdTo.Text))
            {
                this.lblShohinCdTo.Text = "最　後";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShohinCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string shishoCd = this.txtShishoCdTo.Text;
            string name = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN", shishoCd, this.txtShohinCdTo.Text);
            this.lblShohinCdTo.Text = name;
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            //支所コード(先頭)のチェック
            if (!IsValidShishoCdFr())
            {
                this.txtShishoCdFr.Focus();
                this.txtShishoCdFr.SelectAll();
                return false;
            }
            //支所コード(最後)のチェック
            if (!IsValidShishoCdTo())
            {
                this.txtShishoCdTo.Focus();
                this.txtShishoCdTo.SelectAll();
                return false;
            }

            // 仕入先コードのチェック
            if (!IsValidShohinCdFr())
            {
                this.txtShohinCdFr.Focus();
                this.txtShohinCdFr.SelectAll();
                return false;
            }

            // 仕入先コードのチェック
            if (!IsValidShohinCdTo())
            {
                this.txtShohinCdTo.Focus();
                this.txtShohinCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    KBCM1021R rpt = new KBCM1021R(dtOutput);
                    if (isPreview)
                    {
                        // 帳票オブジェクトをインスタンス化
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.Show();
                    }
                    else
                    {
                        // 帳票オブジェクトをインスタンス化
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }
        
        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            //支所コードの設定
            string shishoCdFr;
            string shishoCdTo;
            if (Util.ToDecimal(txtShishoCdFr.Text) > 0)
            {
                shishoCdFr = txtShishoCdFr.Text;
            }
            else
            {
                shishoCdFr = "0";
            }
            if (Util.ToDecimal(txtShishoCdTo.Text) > 0)
            {
                shishoCdTo = txtShishoCdTo.Text;
            }
            else
            {
                shishoCdTo = "9999"; 
            }

            // 仕入先コード設定
            string shohinCdFr;
            string shohinCdTo;
            if (Util.ToDecimal(txtShohinCdFr.Text) > 0)
            {
                shohinCdFr = txtShohinCdFr.Text;
            }
            else
            {
                shohinCdFr = "0";
            }
            if (Util.ToDecimal(txtShohinCdTo.Text) > 0)
            {
                shohinCdTo = txtShohinCdTo.Text;
            }
            else
            {
                shohinCdTo = "99999"; //9999→99999に変更
            }
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            //VI_HN_SHOHINからデータ取得
            sql.Append("SELECT");
            sql.Append(" KAISHA_CD,");

            sql.Append(" SHISHO_CD,");
            sql.Append(" SHISHO_NM,");

            sql.Append(" SHOHIN_CD,");
            sql.Append(" SHOHIN_NM,");
            sql.Append(" KIKAKU,");
            sql.Append(" SHOHIN_KANA_NM,");
            sql.Append(" BARCODE1,");
            sql.Append(" IRISU,");
            sql.Append(" TANI,");
            sql.Append(" SHIIRE_TANKA,");
            sql.Append(" OROSHI_TANKA,");
            sql.Append(" KORI_TANKA,");
            sql.Append(" TANABAN,");
            sql.Append(" ZAIKO_KANRI_KUBUN_NM,");
            sql.Append(" URIAGE_SHIWAKE_CD,");
            sql.Append(" URIAGE_SHIWAKE_NM,");
            sql.Append(" SHIIRE_SHIWAKE_CD,");
            sql.Append(" SHIIRE_SHIWAKE_NM,");
            sql.Append(" SHOHIN_KUBUN1,");
            sql.Append(" SHOHIN_KUBUN_NM1,");
            sql.Append(" SHOHIN_KUBUN2,");
            sql.Append(" SHOHIN_KUBUN_NM2,");
            sql.Append(" SHOHIN_KUBUN3,");
            sql.Append(" SHOHIN_KUBUN_NM3,");
            sql.Append(" SHOHIN_KUBUN4,");
            sql.Append(" SHOHIN_KUBUN_NM4,");
            sql.Append(" SHOHIN_KUBUN5,");
            sql.Append(" SHOHIN_KUBUN_NM5 ");
            sql.Append("FROM");
            sql.Append(" VI_HN_SHOHIN ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO ");
            sql.Append(" AND SHISHO_CD BETWEEN @SHISHO_CD_FR AND @SHISHO_CD_TO ");
            sql.Append("ORDER BY ");
            sql.Append(" KAISHA_CD,");
            sql.Append(" SHISHO_CD,");
            sql.Append(" SHOHIN_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 6, shohinCdFr);
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 6, shohinCdTo);

            dpc.SetParam("@SHISHO_CD_FR", SqlDbType.Decimal, 4, shishoCdFr);
            dpc.SetParam("@SHISHO_CD_TO", SqlDbType.Decimal, 4, shishoCdTo);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion
        
            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("INSERT INTO PR_HN_TBL(");
                    sql.Append("  GUID");
                    sql.Append(" ,SORT");
                    sql.Append(" ,ITEM01");
                    sql.Append(" ,ITEM02");
                    sql.Append(" ,ITEM03");
                    sql.Append(" ,ITEM04");
                    sql.Append(" ,ITEM05");
                    sql.Append(" ,ITEM06");
                    sql.Append(" ,ITEM07");
                    sql.Append(" ,ITEM08");
                    sql.Append(" ,ITEM09");
                    sql.Append(" ,ITEM10");
                    sql.Append(" ,ITEM11");
                    sql.Append(" ,ITEM12");
                    sql.Append(" ,ITEM13");
                    sql.Append(" ,ITEM14");
                    sql.Append(" ,ITEM15");
                    sql.Append(" ,ITEM16");
                    sql.Append(" ,ITEM17");
                    sql.Append(" ,ITEM18");
                    sql.Append(" ,ITEM19");
                    sql.Append(" ,ITEM20");
                    sql.Append(" ,ITEM21");
                    sql.Append(" ,ITEM22");
                    sql.Append(" ,ITEM23");
                    sql.Append(" ,ITEM24");
                    sql.Append(" ,ITEM25");
                    sql.Append(" ,ITEM26");
                    sql.Append(" ,ITEM27");
                    sql.Append(") ");
                    sql.Append("VALUES(");
                    sql.Append("  @GUID");
                    sql.Append(" ,@SORT");
                    sql.Append(" ,@ITEM01");
                    sql.Append(" ,@ITEM02");
                    sql.Append(" ,@ITEM03");
                    sql.Append(" ,@ITEM04");
                    sql.Append(" ,@ITEM05");
                    sql.Append(" ,@ITEM06");
                    sql.Append(" ,@ITEM07");
                    sql.Append(" ,@ITEM08");
                    sql.Append(" ,@ITEM09");
                    sql.Append(" ,@ITEM10");
                    sql.Append(" ,@ITEM11");
                    sql.Append(" ,@ITEM12");
                    sql.Append(" ,@ITEM13");
                    sql.Append(" ,@ITEM14");
                    sql.Append(" ,@ITEM15");
                    sql.Append(" ,@ITEM16");
                    sql.Append(" ,@ITEM17");
                    sql.Append(" ,@ITEM18");
                    sql.Append(" ,@ITEM19");
                    sql.Append(" ,@ITEM20");
                    sql.Append(" ,@ITEM21");
                    sql.Append(" ,@ITEM22");
                    sql.Append(" ,@ITEM23");
                    sql.Append(" ,@ITEM24");
                    sql.Append(" ,@ITEM25");
                    sql.Append(" ,@ITEM26");
                    sql.Append(" ,@ITEM27");
                    sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_CD"]); // 商品CD
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_NM"]); // 商品名
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KIKAKU"]); // 規格
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KANA_NM"]);// 商品カナ名
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["BARCODE1"]);// JANコード
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["IRISU"]); // 入数
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TANI"]); // 単位
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHIIRE_TANKA"]); // 仕入単価
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["OROSHI_TANKA"]); // 卸単価
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KORI_TANKA"]); // 小売単価
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TANABAN"]); // 棚番
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["ZAIKO_KANRI_KUBUN_NM"]); // 在庫管理区分名
                    // 売上仕訳CD
                    if (dtMainLoop.Rows[i]["URIAGE_SHIWAKE_CD"].ToString() == "0")
                    {
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, "");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["URIAGE_SHIWAKE_CD"]);
                    }

                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["URIAGE_SHIWAKE_NM"]); // 売上仕訳名
                    // 仕入仕訳CD
                    if (dtMainLoop.Rows[i]["SHIIRE_SHIWAKE_CD"].ToString() == "0")
                    {
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHIIRE_SHIWAKE_CD"]);
                    }

                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHIIRE_SHIWAKE_NM"]); // 仕入仕訳名
                    // 商品区分1
                    if (dtMainLoop.Rows[i]["SHOHIN_KUBUN1"].ToString() == "0")
                    {
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN1"]);
                    }

                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN_NM1"]); // 商品区分名1
                    // 商品区分2
                    if (dtMainLoop.Rows[i]["SHOHIN_KUBUN2"].ToString() == "0")
                    {
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, "");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN2"]);
                    }

                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN_NM2"]); // 商品区分名2
                    // 商品区分3
                    if (dtMainLoop.Rows[i]["SHOHIN_KUBUN3"].ToString() == "0")
                    {
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, ""); 
                    }
                    else
                    {
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN3"]); 
                    }

                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN_NM3"]); // 商品区分名3
                    // 商品区分4
                    if (dtMainLoop.Rows[i]["SHOHIN_KUBUN4"].ToString() == "0")
                    {
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, "");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN4"]);
                    }

                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN_NM4"]); // 商品区分名4
                    // 商品区分5
                    if (dtMainLoop.Rows[i]["SHOHIN_KUBUN5"].ToString() == "0")
                    {
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, ""); 
                    }
                    else
                    {
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN5"]);
                    }

                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KUBUN_NM5"]); // 商品区分名5
                    
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion
                    
                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        #endregion

    }
}
