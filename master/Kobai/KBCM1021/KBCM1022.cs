﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbcm1021
{
    /// <summary>
    /// 商品の登録(KBCM1022)
    /// </summary>
    public partial class KBCM1022 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBCM1022()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

                /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KBCM1022(string par1) : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string shishoCd = Util.ToString(this.Par2);
            this.txtShishoCd.Text = shishoCd;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);
            txtShishoCd.Enabled = (txtShishoCd.Text == "1") ? true : false;

            // タイトルは非表示
            this.lblTitle.Visible = false;

            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 消費税区分の操作
            try
            {
                // １設定時は操作可
                string ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShohizeiKbnEnable"));
                if (ret == "1")
                    this.txtShohizeiKbn.Enabled = true;
                else
                    this.txtShohizeiKbn.Enabled = false;
            }
            catch (Exception)
            {
                // 設定無しは通常
                this.txtShohizeiKbn.Enabled = false;
            }

            // 引数：Par1／モード(1:新規、2:変更)、InData：担当者コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                // 新規登録時はF3削除を非活性化
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 売上仕訳コード,仕入仕訳コード,商品区分１～５にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShishoCd":
                case "txtUriageShiwakeCd":
                case "txtShiireShiwakeCd":
                case "txtShohinKbn1":
                case "txtShohinKbn2":
                case "txtShohinKbn3":
                case "txtShohinKbn4":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            System.Reflection.Assembly asm;
            // フォーム作成
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShishoCd.Text = outData[0];
                                this.lblShishoNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                case "txtShohinKbn1":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "VI_HN_SHOHIN_KBN1";
                            frm.InData = this.txtShohinKbn1.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtShohinKbn1.Text = result[0];
                                this.lblShohinKbn1Results.Text = result[1];

                                // 次の項目(商品区分２)にフォーカス
                                this.txtShohinKbn2.Focus();
                            }
                        }
                    }
                    break;

                case "txtShohinKbn2":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "VI_HN_SHOHIN_KBN2";
                            frm.InData = this.txtShohinKbn2.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtShohinKbn2.Text = result[0];
                                this.lblShohinKbn2Results.Text = result[1];

                                // 次の項目(商品区分３)にフォーカス
                                this.txtShohinKbn3.Focus();
                            }
                        }
                    }
                    break;

                case "txtShohinKbn3":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "VI_HN_SHOHIN_KBN3";
                            frm.InData = this.txtShohinKbn3.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtShohinKbn3.Text = result[0];
                                this.lblShohinKbn3Results.Text = result[1];

                                // 次の項目(商品区分４)にフォーカス
                                this.txtShohinKbn4.Focus();
                            }
                        }
                    }
                    break;

                case "txtShohinKbn4":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "VI_HN_SHOHIN_KBN4";
                            frm.InData = this.txtShohinKbn4.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtShohinKbn4.Text = result[0];
                                this.lblShohinKbn4Results.Text = result[1];

                                // 次の項目(ﾊﾞｰｺｰﾄﾞ(ｹｰｽ))にフォーカス
                                this.txtBarcodeCase.Focus();
                            }
                        }
                    }
                    break;

                case "txtUriageShiwakeCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2051.CMCM2053");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.Par2 = this.txtShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtUriageShiwakeCd.Text = outData[0];
                                this.lblUriageShiwakeCdResults.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShiireShiwakeCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2051.CMCM2053");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.Par2 = this.txtShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShiireShiwakeCd.Text = outData[0];
                                this.lblShiireShiwakeCdResults.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            // 新規登録の場合は処理しない
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                StringBuilder Sql;
                DbParamCollection dpc;
                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append(" DELETE ");
                Sql.Append(" FROM ");
                Sql.Append("     TB_HN_SHOHIN ");
                Sql.Append(" WHERE ");
                Sql.Append("     KAISHA_CD = @KAISHA_CD AND ");
                Sql.Append("     SHOHIN_CD = @SHOHIN_CD");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToDecimal(txtShohinCd.Text));

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";

            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmShohin = SetCmShohinParams();

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 販売.商品
                    this.Dba.Insert("TB_HN_SHOHIN", (DbParamCollection)alParamsCmShohin[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 販売.商品
                    this.Dba.Update("TB_HN_SHOHIN",
                        (DbParamCollection)alParamsCmShohin[1],
                        "KAISHA_CD = @KAISHA_CD  AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD",
                        (DbParamCollection)alParamsCmShohin[0]);

                }
                
                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("登録しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtShishoCd.Text, this.lblShishoNm.Text, this.txtShishoCd.MaxLength) || !IsValidShishoCd())
            {
                e.Cancel = true;
                this.txtShishoCd.SelectAll();
                this.txtShishoCd.Focus();
            }
        }

        /// <summary>
        /// 商品コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCd())
            {
                e.Cancel = true;
                this.txtShohinCd.SelectAll();
            }
        }

        /// <summary>
        /// 商品名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinNm())
            {
                e.Cancel = true;
                this.txtShohinNm.SelectAll();
            }
        }

        /// <summary>
        /// 商品カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKanaNm())
            {
                e.Cancel = true;
                this.txtShohinKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 商品規格の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKikaku_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKikaku())
            {
                e.Cancel = true;
                this.txtShohinKikaku.SelectAll();
            }
        }

        /// <summary>
        /// 棚番の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTanaban_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTanaban())
            {
                e.Cancel = true;
                this.txtTanaban.SelectAll();
            }
        }

        /// <summary>
        /// 在庫管理区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZaikoKanriKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZaikoKanriKbn())
            {
                e.Cancel = true;
                this.txtZaikoKanriKbn.SelectAll();
            }
        }

        /// <summary>
        /// 売上仕訳コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUriageShiwakeCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidUriageShiwakeCd())
            {
                e.Cancel = true;
                this.txtUriageShiwakeCd.SelectAll();
            }
        }

        /// <summary>
        /// 仕入仕訳コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireShiwakeCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireShiwakeCd())
            {
                e.Cancel = true;
                this.txtShiireShiwakeCd.SelectAll();
            }
        }

        /// <summary>
        /// 入数の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIrisu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidIrisu())
            {
                e.Cancel = true;
                this.txtIrisu.SelectAll();
            }
        }

        /// <summary>
        /// 単位の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTani_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTani())
            {
                e.Cancel = true;
                this.txtTani.SelectAll();
            }
        }

        /// <summary>
        /// 仕入単価の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireTanka_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireTanka())
            {
                e.Cancel = true;
                this.txtShiireTanka.SelectAll();
            }
        }

        /// <summary>
        /// 卸単価の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtOroshiTanka_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidOroshiTanka())
            {
                e.Cancel = true;
                this.txtOroshiTanka.SelectAll();
            }
        }

        /// <summary>
        /// 小売単価の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKouriTanka_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKouriTanka())
            {
                e.Cancel = true;
                this.txtKouriTanka.SelectAll();
            }
        }

        /// <summary>
        /// 商品区分１の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKbn1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKbn1())
            {
                e.Cancel = true;
                this.txtShohinKbn1.SelectAll();
            }
        }

        /// <summary>
        /// 商品区分２の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKbn2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKbn2())
            {
                e.Cancel = true;
                this.txtShohinKbn2.SelectAll();
            }
        }

        /// <summary>
        /// 商品区分３の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKbn3_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKbn3())
            {
                e.Cancel = true;
                this.txtShohinKbn3.SelectAll();
            }
        }

        /// <summary>
        /// 商品区分４の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKbn4_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKbn4())
            {
                e.Cancel = true;
                this.txtShohinKbn4.SelectAll();
            }
        }

        /// <summary>
        /// ﾊﾞｰｺｰﾄﾞ(ｹｰｽ)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBarcodeCase_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBarcodeCase())
            {
                e.Cancel = true;
                this.txtBarcodeCase.SelectAll();
            }
        }

        /// <summary>
        /// ﾊﾞｰｺｰﾄﾞ(ﾊﾞﾗ)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBarcodeBara_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBarcodeBara())
            {
                e.Cancel = true;
                this.txtBarcodeBara.SelectAll();
            }
        }

        /// <summary>
        /// 適正数の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiseisu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiseisu())
            {
                e.Cancel = true;
                this.txtTekiseisu.SelectAll();
            }
        }

        /// <summary>
        /// 消費税区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiKbn())
            {
                e.Cancel = true;
                this.txtShohizeiKbn.SelectAll();
            }
        }

        /// <summary>
        /// 中止区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChushiKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidChushiKbn())
            {
                e.Cancel = true;
                this.txtChushiKbn.SelectAll();
            }
        }

        private void txtChushiKbn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 商品コードの初期値を取得
            // 商品の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
            DataTable dtMaxShohin =
                this.Dba.GetDataTableByConditionWithParams("MAX(SHOHIN_CD) AS MAX_CD",
                    "TB_HN_SHOHIN", Util.ToString(where), dpc);
            if (dtMaxShohin.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxShohin.Rows[0]["MAX_CD"]))
            {
                this.txtShohinCd.Text = Util.ToString(Util.ToInt(dtMaxShohin.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtShohinCd.Text = "1";
            }

            // 各項目の初期値を設定
            // 在庫管理区分
            this.txtZaikoKanriKbn.Text = "0";
            //// 売上仕訳コード
            //this.txtUriageShiwakeCd.Text = "0";
            //// 仕入仕訳コード
            //this.txtShiireShiwakeCd.Text = "0";
            string ret = "";
            // 売上仕訳コード
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "UriageShiwakeCd"));
            this.txtUriageShiwakeCd.Text = ret;
            this.lblUriageShiwakeCdResults.Text = this.Dba.GetName(this.UInfo, "VI_HN_URIAGE_SHIWAKE", this.Par1, this.txtUriageShiwakeCd.Text);
            // 仕入仕訳コード
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShiireShiwakeCd"));
            this.txtShiireShiwakeCd.Text = ret;
            this.lblShiireShiwakeCdResults.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRE_SHIWAKE", this.Par1, this.txtShiireShiwakeCd.Text);
            // 入数
            this.txtIrisu.Text = "0";
            // 仕入単価
            this.txtShiireTanka.Text = "0.00";
            // 卸単価
            this.txtOroshiTanka.Text = "0.00";
            // 小売単価
            this.txtKouriTanka.Text = "0.00";
            // 商品区分１
            this.txtShohinKbn1.Text = "0";
            // 商品区分２
            this.txtShohinKbn2.Text = "0";
            // 商品区分３
            this.txtShohinKbn3.Text = "0";
            // 商品区分４
            this.txtShohinKbn4.Text = "0";
            // 適正数
            this.txtTekiseisu.Text = "0";
            // 消費税区分
            //this.txtShohizeiKbn.Text = "1";
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShohizeiKbn"));
            this.txtShohizeiKbn.Text = ret;
            // 中止区分
            this.txtChushiKbn.Text = "1";

            // 担当者名に初期フォーカス
            this.ActiveControl = this.txtShohinNm;
            this.txtShohinNm.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            string shishoCd = this.txtShishoCd.Text;

            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append(" ,SHOHIN_CD");
            cols.Append(" ,SHOHIN_NM");
            cols.Append(" ,SHOHIN_KANA_NM");
            cols.Append(" ,KIKAKU");
            cols.Append(" ,TANABAN");
            cols.Append(" ,ZAIKO_KANRI_KUBUN");
            cols.Append(" ,URIAGE_SHIWAKE_CD");
            cols.Append(" ,SHIIRE_SHIWAKE_CD");
            cols.Append(" ,IRISU");
            cols.Append(" ,TANI");
            cols.Append(" ,SHIIRE_TANKA");
            cols.Append(" ,OROSHI_TANKA");
            cols.Append(" ,KORI_TANKA");
            cols.Append(" ,SHOHIN_KUBUN1");
            cols.Append(" ,SHOHIN_KUBUN2");
            cols.Append(" ,SHOHIN_KUBUN3");
            cols.Append(" ,SHOHIN_KUBUN4");
            cols.Append(" ,SHOHIN_KUBUN5");
            cols.Append(" ,BARCODE1");
            cols.Append(" ,BARCODE2");
            cols.Append(" ,TEKISEISU");
            cols.Append(" ,SET_SHOHIN");
            cols.Append(" ,SHISHO_CD");
            cols.Append(" ,SHOHIZEI_KUBUN");
            cols.Append(" ,CHUSHI_KUBUN");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_SHOHIN");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToString(this.InData));
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtShohinCd.Text = Util.ToString(drDispData["SHOHIN_CD"]);
            this.txtShohinNm.Text = Util.ToString(drDispData["SHOHIN_NM"]);
            this.txtShohinKanaNm.Text = Util.ToString(drDispData["SHOHIN_KANA_NM"]);
            this.txtShohinKikaku.Text = Util.ToString(drDispData["KIKAKU"]);
            this.txtTanaban.Text = Util.ToString(drDispData["TANABAN"]);
            this.txtZaikoKanriKbn.Text = Util.ToString(drDispData["ZAIKO_KANRI_KUBUN"]);
            this.txtUriageShiwakeCd.Text = Util.ToString(drDispData["URIAGE_SHIWAKE_CD"]);
            this.lblUriageShiwakeCdResults.Text =
                this.Dba.GetName(this.UInfo, "VI_HN_URIAGE_SHIWAKE", shishoCd, this.txtUriageShiwakeCd.Text);
            this.txtShiireShiwakeCd.Text = Util.ToString(drDispData["SHIIRE_SHIWAKE_CD"]);
            this.lblShiireShiwakeCdResults.Text =
                this.Dba.GetName(this.UInfo, "VI_HN_SHIIRE_SHIWAKE", shishoCd, this.txtShiireShiwakeCd.Text);
            this.txtIrisu.Text = Util.ToString(drDispData["IRISU"]);
            this.txtTani.Text = Util.ToString(drDispData["TANI"]);
            this.txtShiireTanka.Text = Util.ToString(drDispData["SHIIRE_TANKA"]);
            // 数値のフォーマット
            this.txtShiireTanka.Text = Util.FormatNum(this.txtShiireTanka.Text, 2);
            this.txtOroshiTanka.Text = Util.ToString(drDispData["OROSHI_TANKA"]);
            // 数値のフォーマット
            this.txtOroshiTanka.Text = Util.FormatNum(this.txtOroshiTanka.Text, 2);
            this.txtKouriTanka.Text = Util.ToString(drDispData["KORI_TANKA"]);
            // 数値のフォーマット
            this.txtKouriTanka.Text = Util.FormatNum(this.txtKouriTanka.Text, 2);
            this.txtShohinKbn1.Text = Util.ToString(drDispData["SHOHIN_KUBUN1"]);
            if (this.txtShohinKbn1.Text != "")
                this.lblShohinKbn1Results.Text =
                    this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN1", shishoCd, this.txtShohinKbn1.Text);

            this.txtShohinKbn2.Text = Util.ToString(drDispData["SHOHIN_KUBUN2"]);
            if (this.txtShohinKbn2.Text != "")
                this.lblShohinKbn2Results.Text =
                this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN2", shishoCd, this.txtShohinKbn2.Text);
            this.txtShohinKbn3.Text = Util.ToString(drDispData["SHOHIN_KUBUN3"]);
            if (this.txtShohinKbn3.Text != "")
                this.lblShohinKbn3Results.Text =
                this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN3", shishoCd, this.txtShohinKbn3.Text);
            this.txtShohinKbn4.Text = Util.ToString(drDispData["SHOHIN_KUBUN4"]);
            if (this.txtShohinKbn4.Text != "")
                this.lblShohinKbn4Results.Text =
                this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN4", shishoCd, this.txtShohinKbn4.Text);
            this.txtBarcodeCase.Text = Util.ToString(drDispData["BARCODE1"]);
            this.txtBarcodeBara.Text = Util.ToString(drDispData["BARCODE2"]);
            this.txtTekiseisu.Text = Util.ToString(drDispData["TEKISEISU"]);

            this.txtShishoCd.Text = Util.ToString(drDispData["SHISHO_CD"]);
            //this.lblShishoNm.Text = Util.ToString(drDispData["SHISHO_NM"]);
            // 消費税区分は値が文字列ゼロなら表示は１を設定
            //this.txtShohizeiKbn.Text = Util.ToString(drDispData["SHOHIZEI_KUBUN"]);
            string ret = "";
            ret = Util.ToString(drDispData["SHOHIZEI_KUBUN"]);
            if (ret == "0")
                this.txtShohizeiKbn.Text = "1";
            else
                this.txtShohizeiKbn.Text = "0";

            this.txtChushiKbn.Text = Util.ToString(drDispData["CHUSHI_KUBUN"]);


            // 商品コードは入力不可
            this.txtShohinCd.Enabled = false;
        }

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtShishoCd.Text) || Equals(this.txtShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtShishoCd.Text = "0";
                this.lblShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            string shishoCd = this.txtShishoCd.Text;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);

            if (ValChk.IsEmpty(this.lblShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }


        /// <summary>
        /// 商品コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinCd()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtShohinCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShohinCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, this.txtShohinCd.Text);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND SHOHIN_CD = @SHOHIN_CD");
            DataTable dtTanto =
                this.Dba.GetDataTableByConditionWithParams("SHOHIN_CD",
                    "TB_HN_SHOHIN", Util.ToString(where), dpc);
            if (dtTanto.Rows.Count > 0)
            {
                Msg.Error("既に存在する商品コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 商品名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShohinNm.Text, this.txtShohinNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 商品カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinKanaNm()
        {
            // 15バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShohinKanaNm.Text, this.txtShohinKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 商品規格の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinKikaku()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShohinKikaku.Text, this.txtShohinKikaku.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 棚番の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTanaban()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTanaban.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 在庫管理区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidZaikoKanriKbn()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtZaikoKanriKbn.Text))
            {
                this.txtZaikoKanriKbn.Text = "0";
            }

            // 0,1のみ入力を許可
            if (!ValChk.IsNumber(this.txtZaikoKanriKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtZaikoKanriKbn.Text);
            if (intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 売上仕訳コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidUriageShiwakeCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtUriageShiwakeCd.Text))
            {
                this.txtUriageShiwakeCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtUriageShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtUriageShiwakeCd.Text))
            {
                this.lblUriageShiwakeCdResults.Text = string.Empty;
            }
            else
            {
                string shishoCd = this.txtShishoCd.Text;
                string name = this.Dba.GetName(this.UInfo, "VI_HN_URIAGE_SHIWAKE", shishoCd, this.txtUriageShiwakeCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblUriageShiwakeCdResults.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 仕入仕訳コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireShiwakeCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShiireShiwakeCd.Text))
            {
                this.txtShiireShiwakeCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiireShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtShiireShiwakeCd.Text))
            {
                this.lblShiireShiwakeCdResults.Text = string.Empty;
            }
            else
            {
                string shishoCd = this.txtShishoCd.Text;
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRE_SHIWAKE", shishoCd, this.txtShiireShiwakeCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblShiireShiwakeCdResults.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 入数の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidIrisu()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtIrisu.Text))
            {
                this.txtIrisu.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtIrisu.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            if (Util.ToDecimal(this.txtIrisu.Text) == 1)
            {
                Msg.Error("入数に１は使用できません、２以上を設定してください。");
                this.txtIrisu.Text = "0";
                return false;
            }
            return true;
        }

        /// <summary>
        /// 単位の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTani()
        {
            // 4バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTani.Text, this.txtTani.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仕入単価の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireTanka()
        {
            // 未入力は0.00とする
            if (ValChk.IsEmpty(this.txtShiireTanka.Text))
            {
                this.txtShiireTanka.Text = "0.00";
            }

                if (!ValChk.IsDecNumWithinLength(this.txtShiireTanka.Text,9,2))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値のフォーマット
            this.txtShiireTanka.Text = Util.FormatNum(this.txtShiireTanka.Text, 2);

            return true;
        }

        /// <summary>
        /// 卸単価の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidOroshiTanka()
        {
            // 未入力は0.00とする
            if (ValChk.IsEmpty(this.txtOroshiTanka.Text))
            {
                this.txtOroshiTanka.Text = "0.00";
            }

            if (!ValChk.IsDecNumWithinLength(this.txtOroshiTanka.Text, 9, 2))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値のフォーマット
            this.txtOroshiTanka.Text = Util.FormatNum(this.txtOroshiTanka.Text, 2);

            return true;
        }

        /// <summary>
        /// 小売単価の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKouriTanka()
        {
            // 未入力は0.00とする
            if (ValChk.IsEmpty(this.txtKouriTanka.Text))
            {
                this.txtKouriTanka.Text = "0.00";
            }

            if (!ValChk.IsDecNumWithinLength(this.txtKouriTanka.Text, 9, 2))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値のフォーマット
            this.txtKouriTanka.Text = Util.FormatNum(this.txtKouriTanka.Text, 2);

            return true;
        }

        /// <summary>
        /// 商品区分１の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinKbn1()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShohinKbn1.Text))
            {
                this.txtShohinKbn1.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShohinKbn1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtShohinKbn1.Text))
            {
                this.lblShohinKbn1Results.Text = string.Empty;
            }
            else
            {
                string shishoCd = this.txtShishoCd.Text;
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN1", shishoCd, this.txtShohinKbn1.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblShohinKbn1Results.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 商品区分２の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinKbn2()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShohinKbn2.Text))
            {
                this.txtShohinKbn2.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShohinKbn2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtShohinKbn2.Text))
            {
                this.lblShohinKbn2Results.Text = string.Empty;
            }
            else
            {
                string shishoCd = this.txtShishoCd.Text;
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN2", shishoCd, this.txtShohinKbn2.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblShohinKbn2Results.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 商品区分３の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinKbn3()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShohinKbn3.Text))
            {
                this.txtShohinKbn3.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShohinKbn3.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtShohinKbn3.Text))
            {
                this.lblShohinKbn3Results.Text = string.Empty;
            }
            else
            {
                string shishoCd = this.txtShishoCd.Text;
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN3", shishoCd, this.txtShohinKbn3.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblShohinKbn3Results.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 商品区分４の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohinKbn4()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShohinKbn4.Text))
            {
                this.txtShohinKbn4.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShohinKbn4.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtShohinKbn4.Text))
            {
                this.lblShohinKbn4Results.Text = string.Empty;
            }
            else
            {
                string shishoCd = this.txtShishoCd.Text;
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN4", shishoCd, this.txtShohinKbn4.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblShohinKbn4Results.Text = name;
            }

            return true;
        }

        /// <summary>
        /// ﾊﾞｰｺｰﾄﾞ(ｹｰｽ)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBarcodeCase()
        {
            //// 未入力は0とする
            //if (ValChk.IsEmpty(this.txtBarcodeCase.Text))
            //{
            //    this.txtBarcodeCase.Text = "0";
            //}

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBarcodeCase.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 13バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtBarcodeCase.Text, this.txtBarcodeCase.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// ﾊﾞｰｺｰﾄﾞ(ﾊﾞﾗ)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBarcodeBara()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBarcodeBara.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 13バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtBarcodeBara.Text, this.txtBarcodeBara.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 適正数の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiseisu()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTekiseisu.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiKbn()
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtShohizeiKbn.Text))
            {
                this.txtShohizeiKbn.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShohizeiKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 1バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShohizeiKbn.Text, this.txtShohizeiKbn.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 1より大きい場合は、1として処理
            int shohizeiKbn = Util.ToInt(this.txtShohizeiKbn.Text);
            if (shohizeiKbn > 1)
            {
                this.txtShohizeiKbn.Text = "1";
            }

            return true;
        }

        /// <summary>
        /// 中止区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidChushiKbn()
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtChushiKbn.Text))
            {
                this.txtChushiKbn.Text = "1";
            }
            else if (this.txtChushiKbn.Text == "0")
            {
                this.txtChushiKbn.Text = "1";
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtChushiKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 1バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtChushiKbn.Text, this.txtChushiKbn.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 2より大きい場合は、2として処理
            int chushiKbn = Util.ToInt(this.txtChushiKbn.Text);
            if (chushiKbn > 2)
            {
                this.txtChushiKbn.Text = "2";
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 商品コードのチェック
                if (!IsValidShohinCd())
                {
                    this.txtShohinCd.Focus();
                    return false;
                }
            }

            // 商品名のチェック
            if (!IsValidShohinNm())
            {
                this.txtShohinNm.Focus();
                return false;
            }

            // 商品カナ名のチェック
            if (!IsValidShohinNm())
            {
                this.txtShohinNm.Focus();
                return false;
            }

            // 商品企画のチェック
            if (!IsValidShohinKikaku())
            {
                this.txtShohinKikaku.Focus();
                return false;
            }

            // 棚番のチェック
            if (!IsValidTanaban())
            {
                this.txtTanaban.Focus();
                return false;
            }

            // 在庫管理区分のチェック
            if (!IsValidZaikoKanriKbn())
            {
                this.txtZaikoKanriKbn.Focus();
                return false;
            }

            // 売上仕訳コードのチェック
            if (!IsValidUriageShiwakeCd())
            {
                this.txtUriageShiwakeCd.Focus();
                return false;
            }

            // 仕入仕訳コードのチェック
            if (!IsValidShiireShiwakeCd())
            {
                this.txtShiireShiwakeCd.Focus();
                return false;
            }

            // 入数のチェック
            if (!IsValidIrisu())
            {
                this.txtIrisu.Focus();
                return false;
            }

            // 単位のチェック
            if (!IsValidTani())
            {
                this.txtTani.Focus();
                return false;
            }

            // 仕入単価のチェック
            if (!IsValidShiireTanka())
            {
                this.txtShiireTanka.Focus();
                return false;
            }

            // 卸単価のチェック
            if (!IsValidOroshiTanka())
            {
                this.txtOroshiTanka.Focus();
                return false;
            }

            // 小売単価のチェック
            if (!IsValidKouriTanka())
            {
                this.txtKouriTanka.Focus();
                return false;
            }

            // 商品区分１のチェック
            if (!IsValidShohinKbn1())
            {
                this.txtShohinKbn1.Focus();
                return false;
            }

            // 商品区分２のチェック
            if (!IsValidShohinKbn2())
            {
                this.txtShohinKbn2.Focus();
                return false;
            }

            // 商品区分３のチェック
            if (!IsValidShohinKbn3())
            {
                this.txtShohinKbn3.Focus();
                return false;
            }

            // 商品区分４のチェック
            if (!IsValidShohinKbn4())
            {
                this.txtShohinKbn4.Focus();
                return false;
            }

            // ﾊﾞｰｺｰﾄﾞ(ｹｰｽ)のチェック
            if (!IsValidBarcodeCase())
            {
                this.txtBarcodeCase.Focus();
                return false;
            }

            // ﾊﾞｰｺｰﾄﾞ(ﾊﾞﾗ)のチェック
            if (!IsValidBarcodeBara())
            {
                this.txtBarcodeBara.Focus();
                return false;
            }

            // 適正数のチェック
            if (!IsValidTekiseisu())
            {
                this.txtTekiseisu.Focus();
                return false;
            }

            if (!IsValidShohizeiKbn())
            {
                this.txtShohizeiKbn.SelectAll();
                return false;
            }


            if (!IsValidChushiKbn())
            {
                this.txtChushiKbn.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_SHOHINに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmShohinParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと商品コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, this.txtShohinCd.Text);
                //// ﾊﾞｰｺｰﾄﾞ(ｹｰｽ)
                //updParam.SetParam("@BARCODE1", SqlDbType.VarChar, 13, this.txtBarcodeCase.Text);
                //支所コード
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);

                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと商品コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, this.txtShohinCd.Text);
                //// ﾊﾞｰｺｰﾄﾞ(ｹｰｽ)
                //whereParam.SetParam("@BARCODE1", SqlDbType.VarChar, 13, this.txtBarcodeCase.Text);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
                alParams.Add(whereParam);
            }

            // 商品名
            updParam.SetParam("@SHOHIN_NM", SqlDbType.VarChar, 40, this.txtShohinNm.Text);
            // 商品カナ名
            updParam.SetParam("@SHOHIN_KANA_NM", SqlDbType.VarChar, 15, this.txtShohinKanaNm.Text);
            // 商品規格
            updParam.SetParam("@KIKAKU", SqlDbType.VarChar, 30, this.txtShohinKikaku.Text);
            // 棚番
            updParam.SetParam("@TANABAN", SqlDbType.VarChar, 30, this.txtTanaban.Text);
            // 在庫管理区分
            updParam.SetParam("@ZAIKO_KANRI_KUBUN", SqlDbType.Decimal, 1, this.txtZaikoKanriKbn.Text);
            // 売上仕訳コード
            updParam.SetParam("@URIAGE_SHIWAKE_CD", SqlDbType.Decimal, 4, this.txtUriageShiwakeCd.Text);
            // 仕入仕訳コード
            updParam.SetParam("@SHIIRE_SHIWAKE_CD", SqlDbType.Decimal, 4, this.txtShiireShiwakeCd.Text);
            // 入数
            updParam.SetParam("@IRISU", SqlDbType.Decimal, 4, this.txtIrisu.Text);
            // 単位
            updParam.SetParam("@TANI", SqlDbType.VarChar, 4, this.txtTani.Text);
            // 仕入単価
            updParam.SetParam("@SHIIRE_TANKA", SqlDbType.Decimal, 11, this.txtShiireTanka.Text.Replace(",",""));
            // 卸単価
            updParam.SetParam("@OROSHI_TANKA", SqlDbType.Decimal, 11, this.txtOroshiTanka.Text.Replace(",", ""));
            // 小売単価
            updParam.SetParam("@KORI_TANKA", SqlDbType.Decimal, 11, this.txtKouriTanka.Text.Replace(",", ""));
            // 商品区分１
            updParam.SetParam("@SHOHIN_KUBUN1", SqlDbType.Decimal, 4, this.txtShohinKbn1.Text);
            // 商品区分２
            updParam.SetParam("@SHOHIN_KUBUN2", SqlDbType.Decimal, 4, this.txtShohinKbn2.Text);
            // 商品区分３
            updParam.SetParam("@SHOHIN_KUBUN3", SqlDbType.Decimal, 4, this.txtShohinKbn3.Text);
            // 商品区分４
            updParam.SetParam("@SHOHIN_KUBUN4", SqlDbType.Decimal, 4, this.txtShohinKbn4.Text);
            // 商品区分５
            updParam.SetParam("@SHOHIN_KUBUN5", SqlDbType.Decimal, 4, 0); // 現状固定値
            // ﾊﾞｰｺｰﾄﾞ(ﾊﾞﾗ)
            updParam.SetParam("@BARCODE2", SqlDbType.VarChar, 13, this.txtBarcodeBara.Text);
            // 適正数
            updParam.SetParam("@TEKISEISU", SqlDbType.Decimal, 12, this.txtTekiseisu.Text);
            // 消費税区分
            updParam.SetParam("@SHOHIZEI_KUBUN", SqlDbType.Decimal, 12, this.txtShohizeiKbn.Text == "1"? 0:1);
            // 中止区分
            updParam.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 12, this.txtChushiKbn.Text);

            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

            alParams.Add(updParam);

            return alParams;
        }
        #endregion
    }
}
