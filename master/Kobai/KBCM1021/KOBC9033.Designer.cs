﻿namespace jp.co.fsi.kob.kobc9031
{
    partial class KOBC9033
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShiwakeCd = new jp.co.fsi.common.controls.SosTextBox();
            this.lblShiwakeCd = new System.Windows.Forms.Label();
            this.gbxJoken = new System.Windows.Forms.GroupBox();
            this.txtShiwakeNm = new jp.co.fsi.common.controls.SosTextBox();
            this.lblShiwakeNm = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCd = new jp.co.fsi.common.controls.SosTextBox();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.txtHojoKamokuCd = new jp.co.fsi.common.controls.SosTextBox();
            this.lblHojoKamokuCd = new System.Windows.Forms.Label();
            this.txtBumonCd = new jp.co.fsi.common.controls.SosTextBox();
            this.lblBumonCd = new System.Windows.Forms.Label();
            this.txtZeiKubun = new jp.co.fsi.common.controls.SosTextBox();
            this.lblZeiKubun = new System.Windows.Forms.Label();
            this.txtJigyoKbn = new jp.co.fsi.common.controls.SosTextBox();
            this.lblJigyoKbn = new System.Windows.Forms.Label();
            this.txtTaishakuKbn = new jp.co.fsi.common.controls.SosTextBox();
            this.lblTaishakuKbn = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCdResults = new System.Windows.Forms.Label();
            this.lblHojoKamokuCdResults = new System.Windows.Forms.Label();
            this.lblBumonCdResults = new System.Windows.Forms.Label();
            this.lblZeiKubunResults = new System.Windows.Forms.Label();
            this.lblJigyoKbnResults = new System.Windows.Forms.Label();
            this.lblTaishakuKbnResults = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxJoken.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "商品の登録";
            // 
            // txtShiwakeCd
            // 
            this.txtShiwakeCd.AllowDrop = true;
            this.txtShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiwakeCd.Location = new System.Drawing.Point(102, 51);
            this.txtShiwakeCd.MaxLength = 4;
            this.txtShiwakeCd.Name = "txtShiwakeCd";
            this.txtShiwakeCd.Size = new System.Drawing.Size(39, 20);
            this.txtShiwakeCd.TabIndex = 903;
            this.txtShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblShiwakeCd
            // 
            this.lblShiwakeCd.BackColor = System.Drawing.Color.Silver;
            this.lblShiwakeCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiwakeCd.Location = new System.Drawing.Point(12, 48);
            this.lblShiwakeCd.Name = "lblShiwakeCd";
            this.lblShiwakeCd.Size = new System.Drawing.Size(132, 25);
            this.lblShiwakeCd.TabIndex = 902;
            this.lblShiwakeCd.Text = "仕 訳 コ ー ド";
            this.lblShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxJoken
            // 
            this.gbxJoken.Controls.Add(this.lblTaishakuKbnResults);
            this.gbxJoken.Controls.Add(this.lblJigyoKbnResults);
            this.gbxJoken.Controls.Add(this.lblZeiKubunResults);
            this.gbxJoken.Controls.Add(this.lblBumonCdResults);
            this.gbxJoken.Controls.Add(this.lblHojoKamokuCdResults);
            this.gbxJoken.Controls.Add(this.lblKanjoKamokuCdResults);
            this.gbxJoken.Controls.Add(this.txtTaishakuKbn);
            this.gbxJoken.Controls.Add(this.lblTaishakuKbn);
            this.gbxJoken.Controls.Add(this.txtJigyoKbn);
            this.gbxJoken.Controls.Add(this.lblJigyoKbn);
            this.gbxJoken.Controls.Add(this.txtZeiKubun);
            this.gbxJoken.Controls.Add(this.lblZeiKubun);
            this.gbxJoken.Controls.Add(this.txtBumonCd);
            this.gbxJoken.Controls.Add(this.txtHojoKamokuCd);
            this.gbxJoken.Controls.Add(this.lblBumonCd);
            this.gbxJoken.Controls.Add(this.lblHojoKamokuCd);
            this.gbxJoken.Controls.Add(this.txtKanjoKamokuCd);
            this.gbxJoken.Controls.Add(this.lblKanjoKamokuCd);
            this.gbxJoken.Controls.Add(this.txtShiwakeNm);
            this.gbxJoken.Controls.Add(this.lblShiwakeNm);
            this.gbxJoken.Location = new System.Drawing.Point(8, 77);
            this.gbxJoken.Name = "gbxJoken";
            this.gbxJoken.Size = new System.Drawing.Size(313, 268);
            this.gbxJoken.TabIndex = 904;
            this.gbxJoken.TabStop = false;
            // 
            // txtShiwakeNm
            // 
            this.txtShiwakeNm.AllowDrop = true;
            this.txtShiwakeNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiwakeNm.Location = new System.Drawing.Point(94, 18);
            this.txtShiwakeNm.MaxLength = 4;
            this.txtShiwakeNm.Name = "txtShiwakeNm";
            this.txtShiwakeNm.Size = new System.Drawing.Size(207, 20);
            this.txtShiwakeNm.TabIndex = 905;
            this.txtShiwakeNm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblShiwakeNm
            // 
            this.lblShiwakeNm.BackColor = System.Drawing.Color.Silver;
            this.lblShiwakeNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiwakeNm.Location = new System.Drawing.Point(4, 15);
            this.lblShiwakeNm.Name = "lblShiwakeNm";
            this.lblShiwakeNm.Size = new System.Drawing.Size(300, 25);
            this.lblShiwakeNm.TabIndex = 904;
            this.lblShiwakeNm.Text = "仕  訳  名  称";
            this.lblShiwakeNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCd
            // 
            this.txtKanjoKamokuCd.AllowDrop = true;
            this.txtKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCd.Location = new System.Drawing.Point(94, 53);
            this.txtKanjoKamokuCd.MaxLength = 4;
            this.txtKanjoKamokuCd.Name = "txtKanjoKamokuCd";
            this.txtKanjoKamokuCd.Size = new System.Drawing.Size(39, 20);
            this.txtKanjoKamokuCd.TabIndex = 907;
            this.txtKanjoKamokuCd.Text = "0";
            this.txtKanjoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(4, 50);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(132, 25);
            this.lblKanjoKamokuCd.TabIndex = 906;
            this.lblKanjoKamokuCd.Text = "勘定科目コード";
            this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoKamokuCd
            // 
            this.txtHojoKamokuCd.AllowDrop = true;
            this.txtHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoKamokuCd.Location = new System.Drawing.Point(94, 88);
            this.txtHojoKamokuCd.MaxLength = 4;
            this.txtHojoKamokuCd.Name = "txtHojoKamokuCd";
            this.txtHojoKamokuCd.Size = new System.Drawing.Size(39, 20);
            this.txtHojoKamokuCd.TabIndex = 909;
            this.txtHojoKamokuCd.Text = "0";
            this.txtHojoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblHojoKamokuCd
            // 
            this.lblHojoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuCd.Location = new System.Drawing.Point(4, 85);
            this.lblHojoKamokuCd.Name = "lblHojoKamokuCd";
            this.lblHojoKamokuCd.Size = new System.Drawing.Size(132, 25);
            this.lblHojoKamokuCd.TabIndex = 908;
            this.lblHojoKamokuCd.Text = "補助科目コード";
            this.lblHojoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AllowDrop = true;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCd.Location = new System.Drawing.Point(94, 123);
            this.txtBumonCd.MaxLength = 4;
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.Size = new System.Drawing.Size(39, 20);
            this.txtBumonCd.TabIndex = 909;
            this.txtBumonCd.Text = "0";
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBumonCd
            // 
            this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCd.Location = new System.Drawing.Point(4, 120);
            this.lblBumonCd.Name = "lblBumonCd";
            this.lblBumonCd.Size = new System.Drawing.Size(132, 25);
            this.lblBumonCd.TabIndex = 908;
            this.lblBumonCd.Text = "部 門 コ ー ド";
            this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZeiKubun
            // 
            this.txtZeiKubun.AllowDrop = true;
            this.txtZeiKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiKubun.Location = new System.Drawing.Point(94, 158);
            this.txtZeiKubun.MaxLength = 4;
            this.txtZeiKubun.Name = "txtZeiKubun";
            this.txtZeiKubun.Size = new System.Drawing.Size(39, 20);
            this.txtZeiKubun.TabIndex = 911;
            this.txtZeiKubun.Text = "0";
            this.txtZeiKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblZeiKubun
            // 
            this.lblZeiKubun.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKubun.Location = new System.Drawing.Point(4, 155);
            this.lblZeiKubun.Name = "lblZeiKubun";
            this.lblZeiKubun.Size = new System.Drawing.Size(132, 25);
            this.lblZeiKubun.TabIndex = 910;
            this.lblZeiKubun.Text = "税    区    分";
            this.lblZeiKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJigyoKbn
            // 
            this.txtJigyoKbn.AllowDrop = true;
            this.txtJigyoKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJigyoKbn.Location = new System.Drawing.Point(94, 194);
            this.txtJigyoKbn.MaxLength = 4;
            this.txtJigyoKbn.Name = "txtJigyoKbn";
            this.txtJigyoKbn.Size = new System.Drawing.Size(39, 20);
            this.txtJigyoKbn.TabIndex = 913;
            this.txtJigyoKbn.Text = "0";
            this.txtJigyoKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblJigyoKbn
            // 
            this.lblJigyoKbn.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJigyoKbn.Location = new System.Drawing.Point(4, 191);
            this.lblJigyoKbn.Name = "lblJigyoKbn";
            this.lblJigyoKbn.Size = new System.Drawing.Size(132, 25);
            this.lblJigyoKbn.TabIndex = 912;
            this.lblJigyoKbn.Text = "事  業  区  分";
            this.lblJigyoKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTaishakuKbn
            // 
            this.txtTaishakuKbn.AllowDrop = true;
            this.txtTaishakuKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuKbn.Location = new System.Drawing.Point(94, 229);
            this.txtTaishakuKbn.MaxLength = 4;
            this.txtTaishakuKbn.Name = "txtTaishakuKbn";
            this.txtTaishakuKbn.Size = new System.Drawing.Size(39, 20);
            this.txtTaishakuKbn.TabIndex = 915;
            this.txtTaishakuKbn.Text = "1";
            this.txtTaishakuKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTaishakuKbn
            // 
            this.lblTaishakuKbn.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakuKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTaishakuKbn.Location = new System.Drawing.Point(4, 226);
            this.lblTaishakuKbn.Name = "lblTaishakuKbn";
            this.lblTaishakuKbn.Size = new System.Drawing.Size(132, 25);
            this.lblTaishakuKbn.TabIndex = 914;
            this.lblTaishakuKbn.Text = "貸  借  区  分";
            this.lblTaishakuKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCdResults
            // 
            this.lblKanjoKamokuCdResults.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCdResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCdResults.Location = new System.Drawing.Point(139, 50);
            this.lblKanjoKamokuCdResults.Name = "lblKanjoKamokuCdResults";
            this.lblKanjoKamokuCdResults.Size = new System.Drawing.Size(161, 25);
            this.lblKanjoKamokuCdResults.TabIndex = 916;
            this.lblKanjoKamokuCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHojoKamokuCdResults
            // 
            this.lblHojoKamokuCdResults.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuCdResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuCdResults.Location = new System.Drawing.Point(139, 85);
            this.lblHojoKamokuCdResults.Name = "lblHojoKamokuCdResults";
            this.lblHojoKamokuCdResults.Size = new System.Drawing.Size(161, 25);
            this.lblHojoKamokuCdResults.TabIndex = 917;
            this.lblHojoKamokuCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCdResults
            // 
            this.lblBumonCdResults.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdResults.Location = new System.Drawing.Point(139, 120);
            this.lblBumonCdResults.Name = "lblBumonCdResults";
            this.lblBumonCdResults.Size = new System.Drawing.Size(161, 25);
            this.lblBumonCdResults.TabIndex = 918;
            this.lblBumonCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZeiKubunResults
            // 
            this.lblZeiKubunResults.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKubunResults.Location = new System.Drawing.Point(139, 155);
            this.lblZeiKubunResults.Name = "lblZeiKubunResults";
            this.lblZeiKubunResults.Size = new System.Drawing.Size(161, 25);
            this.lblZeiKubunResults.TabIndex = 919;
            this.lblZeiKubunResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJigyoKbnResults
            // 
            this.lblJigyoKbnResults.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKbnResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJigyoKbnResults.Location = new System.Drawing.Point(139, 192);
            this.lblJigyoKbnResults.Name = "lblJigyoKbnResults";
            this.lblJigyoKbnResults.Size = new System.Drawing.Size(161, 25);
            this.lblJigyoKbnResults.TabIndex = 920;
            this.lblJigyoKbnResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaishakuKbnResults
            // 
            this.lblTaishakuKbnResults.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakuKbnResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTaishakuKbnResults.Location = new System.Drawing.Point(139, 227);
            this.lblTaishakuKbnResults.Name = "lblTaishakuKbnResults";
            this.lblTaishakuKbnResults.Size = new System.Drawing.Size(161, 25);
            this.lblTaishakuKbnResults.TabIndex = 921;
            this.lblTaishakuKbnResults.Text = "1:借方  2:貸方";
            this.lblTaishakuKbnResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KOBC9033
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 638);
            this.Controls.Add(this.gbxJoken);
            this.Controls.Add(this.txtShiwakeCd);
            this.Controls.Add(this.lblShiwakeCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KOBC9033";
            this.Text = "商品の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblShiwakeCd, 0);
            this.Controls.SetChildIndex(this.txtShiwakeCd, 0);
            this.Controls.SetChildIndex(this.gbxJoken, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxJoken.ResumeLayout(false);
            this.gbxJoken.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.SosTextBox txtShiwakeCd;
        private System.Windows.Forms.Label lblShiwakeCd;
        private System.Windows.Forms.GroupBox gbxJoken;
        private jp.co.fsi.common.controls.SosTextBox txtShiwakeNm;
        private System.Windows.Forms.Label lblShiwakeNm;
        private jp.co.fsi.common.controls.SosTextBox txtKanjoKamokuCd;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private System.Windows.Forms.Label lblTaishakuKbnResults;
        private System.Windows.Forms.Label lblJigyoKbnResults;
        private System.Windows.Forms.Label lblZeiKubunResults;
        private System.Windows.Forms.Label lblBumonCdResults;
        private System.Windows.Forms.Label lblHojoKamokuCdResults;
        private System.Windows.Forms.Label lblKanjoKamokuCdResults;
        private jp.co.fsi.common.controls.SosTextBox txtTaishakuKbn;
        private System.Windows.Forms.Label lblTaishakuKbn;
        private jp.co.fsi.common.controls.SosTextBox txtJigyoKbn;
        private System.Windows.Forms.Label lblJigyoKbn;
        private jp.co.fsi.common.controls.SosTextBox txtZeiKubun;
        private System.Windows.Forms.Label lblZeiKubun;
        private jp.co.fsi.common.controls.SosTextBox txtBumonCd;
        private jp.co.fsi.common.controls.SosTextBox txtHojoKamokuCd;
        private System.Windows.Forms.Label lblBumonCd;
        private System.Windows.Forms.Label lblHojoKamokuCd;


    }
}