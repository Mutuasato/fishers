﻿namespace jp.co.fsi.kb.kbdr2031
{
    partial class KBDR2031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShiiresakiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiCdFr = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblShiiresakiCdTo = new System.Windows.Forms.Label();
            this.txtShiiresakiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpFr = new System.Windows.Forms.Label();
            this.lblJpTo = new System.Windows.Forms.Label();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpDayTo = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblJpDayFr = new System.Windows.Forms.Label();
            this.lblJpMonthTo = new System.Windows.Forms.Label();
            this.lblJpMonthFr = new System.Windows.Forms.Label();
            this.lblJpYearTo = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpearFr = new System.Windows.Forms.Label();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.rdo2 = new System.Windows.Forms.RadioButton();
            this.rdo1 = new System.Windows.Forms.RadioButton();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "買掛元帳問合せ";
            // 
            // txtShiiresakiCdFr
            // 
            this.txtShiiresakiCdFr.AutoSizeFromLength = false;
            this.txtShiiresakiCdFr.DisplayLength = null;
            this.txtShiiresakiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtShiiresakiCdFr.Location = new System.Drawing.Point(180, 8);
            this.txtShiiresakiCdFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtShiiresakiCdFr.MaxLength = 4;
            this.txtShiiresakiCdFr.Name = "txtShiiresakiCdFr";
            this.txtShiiresakiCdFr.Size = new System.Drawing.Size(52, 23);
            this.txtShiiresakiCdFr.TabIndex = 7;
            this.txtShiiresakiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblShiiresakiCdFr
            // 
            this.lblShiiresakiCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblShiiresakiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblShiiresakiCdFr.Location = new System.Drawing.Point(241, 7);
            this.lblShiiresakiCdFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblShiiresakiCdFr.Name = "lblShiiresakiCdFr";
            this.lblShiiresakiCdFr.Size = new System.Drawing.Size(272, 24);
            this.lblShiiresakiCdFr.TabIndex = 1;
            this.lblShiiresakiCdFr.Tag = "DISPNAME";
            this.lblShiiresakiCdFr.Text = "先　頭";
            this.lblShiiresakiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblCodeBet.Location = new System.Drawing.Point(528, 7);
            this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(24, 32);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Tag = "CHANGE";
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresakiCdTo
            // 
            this.lblShiiresakiCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblShiiresakiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblShiiresakiCdTo.Location = new System.Drawing.Point(633, 7);
            this.lblShiiresakiCdTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblShiiresakiCdTo.Name = "lblShiiresakiCdTo";
            this.lblShiiresakiCdTo.Size = new System.Drawing.Size(272, 23);
            this.lblShiiresakiCdTo.TabIndex = 4;
            this.lblShiiresakiCdTo.Tag = "DISPNAME";
            this.lblShiiresakiCdTo.Text = "最　後";
            this.lblShiiresakiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCdTo
            // 
            this.txtShiiresakiCdTo.AutoSizeFromLength = false;
            this.txtShiiresakiCdTo.DisplayLength = null;
            this.txtShiiresakiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtShiiresakiCdTo.Location = new System.Drawing.Point(572, 8);
            this.txtShiiresakiCdTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtShiiresakiCdTo.MaxLength = 4;
            this.txtShiiresakiCdTo.Name = "txtShiiresakiCdTo";
            this.txtShiiresakiCdTo.Size = new System.Drawing.Size(52, 23);
            this.txtShiiresakiCdTo.TabIndex = 8;
            this.txtShiiresakiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShiiresakiCdTo_KeyDown);
            this.txtShiiresakiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // lblJpFr
            // 
            this.lblJpFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblJpFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblJpFr.Location = new System.Drawing.Point(180, 5);
            this.lblJpFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblJpFr.Name = "lblJpFr";
            this.lblJpFr.Size = new System.Drawing.Size(53, 24);
            this.lblJpFr.TabIndex = 0;
            this.lblJpFr.Tag = "DISPNAME";
            this.lblJpFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJpTo
            // 
            this.lblJpTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblJpTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblJpTo.Location = new System.Drawing.Point(572, 5);
            this.lblJpTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblJpTo.Name = "lblJpTo";
            this.lblJpTo.Size = new System.Drawing.Size(53, 24);
            this.lblJpTo.TabIndex = 8;
            this.lblJpTo.Tag = "DISPNAME";
            this.lblJpTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDateYearFr.Location = new System.Drawing.Point(240, 7);
            this.txtDateYearFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(52, 23);
            this.txtDateYearFr.TabIndex = 1;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDateYearTo.Location = new System.Drawing.Point(632, 7);
            this.txtDateYearTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(52, 23);
            this.txtDateYearTo.TabIndex = 4;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // lblJpDayTo
            // 
            this.lblJpDayTo.AutoSize = true;
            this.lblJpDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblJpDayTo.Location = new System.Drawing.Point(885, 5);
            this.lblJpDayTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblJpDayTo.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpDayTo.Name = "lblJpDayTo";
            this.lblJpDayTo.Size = new System.Drawing.Size(24, 32);
            this.lblJpDayTo.TabIndex = 14;
            this.lblJpDayTo.Tag = "CHANGE";
            this.lblJpDayTo.Text = "日";
            this.lblJpDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblCodeBetDate.Location = new System.Drawing.Point(536, 5);
            this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblCodeBetDate.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(24, 32);
            this.lblCodeBetDate.TabIndex = 7;
            this.lblCodeBetDate.Tag = "CHANGE";
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpDayFr
            // 
            this.lblJpDayFr.AutoSize = true;
            this.lblJpDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblJpDayFr.Location = new System.Drawing.Point(488, 5);
            this.lblJpDayFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblJpDayFr.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpDayFr.Name = "lblJpDayFr";
            this.lblJpDayFr.Size = new System.Drawing.Size(24, 32);
            this.lblJpDayFr.TabIndex = 6;
            this.lblJpDayFr.Tag = "CHANGE";
            this.lblJpDayFr.Text = "日";
            this.lblJpDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpMonthTo
            // 
            this.lblJpMonthTo.AutoSize = true;
            this.lblJpMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblJpMonthTo.Location = new System.Drawing.Point(789, 5);
            this.lblJpMonthTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblJpMonthTo.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpMonthTo.Name = "lblJpMonthTo";
            this.lblJpMonthTo.Size = new System.Drawing.Size(24, 32);
            this.lblJpMonthTo.TabIndex = 12;
            this.lblJpMonthTo.Tag = "CHANGE";
            this.lblJpMonthTo.Text = "月";
            this.lblJpMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpMonthFr
            // 
            this.lblJpMonthFr.AutoSize = true;
            this.lblJpMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblJpMonthFr.Location = new System.Drawing.Point(392, 5);
            this.lblJpMonthFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblJpMonthFr.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpMonthFr.Name = "lblJpMonthFr";
            this.lblJpMonthFr.Size = new System.Drawing.Size(24, 32);
            this.lblJpMonthFr.TabIndex = 4;
            this.lblJpMonthFr.Tag = "CHANGE";
            this.lblJpMonthFr.Text = "月";
            this.lblJpMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpYearTo
            // 
            this.lblJpYearTo.AutoSize = true;
            this.lblJpYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblJpYearTo.Location = new System.Drawing.Point(693, 5);
            this.lblJpYearTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblJpYearTo.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpYearTo.Name = "lblJpYearTo";
            this.lblJpYearTo.Size = new System.Drawing.Size(24, 32);
            this.lblJpYearTo.TabIndex = 10;
            this.lblJpYearTo.Tag = "CHANGE";
            this.lblJpYearTo.Text = "年";
            this.lblJpYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDateDayTo.Location = new System.Drawing.Point(824, 7);
            this.txtDateDayTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(52, 23);
            this.txtDateDayTo.TabIndex = 6;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDateDayFr.Location = new System.Drawing.Point(427, 7);
            this.txtDateDayFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(52, 23);
            this.txtDateDayFr.TabIndex = 3;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblJpearFr
            // 
            this.lblJpearFr.AutoSize = true;
            this.lblJpearFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblJpearFr.Location = new System.Drawing.Point(299, 5);
            this.lblJpearFr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblJpearFr.MinimumSize = new System.Drawing.Size(0, 32);
            this.lblJpearFr.Name = "lblJpearFr";
            this.lblJpearFr.Size = new System.Drawing.Size(24, 32);
            this.lblJpearFr.TabIndex = 2;
            this.lblJpearFr.Tag = "CHANGE";
            this.lblJpearFr.Text = "年";
            this.lblJpearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDateMonthTo.Location = new System.Drawing.Point(728, 7);
            this.txtDateMonthTo.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(52, 23);
            this.txtDateMonthTo.TabIndex = 5;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtDateMonthFr.Location = new System.Drawing.Point(333, 7);
            this.txtDateMonthFr.Margin = new System.Windows.Forms.Padding(5);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(52, 23);
            this.txtDateMonthFr.TabIndex = 2;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // rdo2
            // 
            this.rdo2.AutoSize = true;
            this.rdo2.BackColor = System.Drawing.Color.Silver;
            this.rdo2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdo2.Location = new System.Drawing.Point(308, 5);
            this.rdo2.Margin = new System.Windows.Forms.Padding(5);
            this.rdo2.MinimumSize = new System.Drawing.Size(0, 32);
            this.rdo2.Name = "rdo2";
            this.rdo2.Size = new System.Drawing.Size(90, 32);
            this.rdo2.TabIndex = 10;
            this.rdo2.Tag = "CHANGE";
            this.rdo2.Text = "伝票合計";
            this.rdo2.UseVisualStyleBackColor = false;
            // 
            // rdo1
            // 
            this.rdo1.AutoSize = true;
            this.rdo1.BackColor = System.Drawing.Color.Silver;
            this.rdo1.Checked = true;
            this.rdo1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.rdo1.Location = new System.Drawing.Point(180, 5);
            this.rdo1.Margin = new System.Windows.Forms.Padding(5);
            this.rdo1.MinimumSize = new System.Drawing.Size(0, 32);
            this.rdo1.Name = "rdo1";
            this.rdo1.Size = new System.Drawing.Size(90, 32);
            this.rdo1.TabIndex = 9;
            this.rdo1.TabStop = true;
            this.rdo1.Tag = "CHANGE";
            this.rdo1.Text = "伝票明細";
            this.rdo1.UseVisualStyleBackColor = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(180, 7);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(5);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(228, 5);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(959, 35);
            this.label1.TabIndex = 902;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "支所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(959, 35);
            this.label2.TabIndex = 903;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "日付範囲";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(959, 35);
            this.label3.TabIndex = 904;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "表示方法";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(959, 36);
            this.label4.TabIndex = 905;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "仕入先コード範囲";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 4;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(969, 178);
            this.fsiTableLayoutPanel1.TabIndex = 906;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtShiiresakiCdFr);
            this.fsiPanel4.Controls.Add(this.lblShiiresakiCdTo);
            this.fsiPanel4.Controls.Add(this.txtShiiresakiCdTo);
            this.fsiPanel4.Controls.Add(this.lblShiiresakiCdFr);
            this.fsiPanel4.Controls.Add(this.lblCodeBet);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 137);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(959, 36);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.rdo1);
            this.fsiPanel3.Controls.Add(this.rdo2);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 93);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(959, 35);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblJpFr);
            this.fsiPanel2.Controls.Add(this.lblJpTo);
            this.fsiPanel2.Controls.Add(this.lblJpDayTo);
            this.fsiPanel2.Controls.Add(this.txtDateYearFr);
            this.fsiPanel2.Controls.Add(this.txtDateYearTo);
            this.fsiPanel2.Controls.Add(this.txtDateMonthFr);
            this.fsiPanel2.Controls.Add(this.lblCodeBetDate);
            this.fsiPanel2.Controls.Add(this.txtDateMonthTo);
            this.fsiPanel2.Controls.Add(this.lblJpearFr);
            this.fsiPanel2.Controls.Add(this.lblJpDayFr);
            this.fsiPanel2.Controls.Add(this.txtDateDayFr);
            this.fsiPanel2.Controls.Add(this.txtDateDayTo);
            this.fsiPanel2.Controls.Add(this.lblJpMonthTo);
            this.fsiPanel2.Controls.Add(this.lblJpYearTo);
            this.fsiPanel2.Controls.Add(this.lblJpMonthFr);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 49);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(959, 35);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(959, 35);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // KBDR2031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBDR2031";
            this.Par1 = "2";
            this.Text = "買掛元帳問合せ";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtShiiresakiCdFr;
        private System.Windows.Forms.Label lblShiiresakiCdFr;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblShiiresakiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShiiresakiCdTo;
        private System.Windows.Forms.Label lblJpFr;
        private System.Windows.Forms.Label lblJpTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblJpMonthTo;
        private System.Windows.Forms.Label lblJpMonthFr;
        private System.Windows.Forms.Label lblJpYearTo;
        private System.Windows.Forms.Label lblJpearFr;
        private System.Windows.Forms.Label lblJpDayFr;
        private System.Windows.Forms.Label lblJpDayTo;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.RadioButton rdo2;
        private System.Windows.Forms.RadioButton rdo1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}