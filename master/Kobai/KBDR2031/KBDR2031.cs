﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.kb.kbdr2031
{
    #region 構造体
    /// <summary>
    /// 印刷ワークテーブルのデータ構造体
    /// </summary>
    struct NumVals
    {
        public decimal SHIIREGAKU;
        public decimal SHOHIZEIGAKU;
        public decimal SHIHARAIGAKU;
        public decimal ZANDAKA;

        public void Clear()
        {
            SHIIREGAKU = 0;
            SHOHIZEIGAKU = 0;
            SHIHARAIGAKU = 0;
            ZANDAKA = 0;
        }
    }
    #endregion

    /// <summary>
    /// 買掛元帳問合せ(KBDR2031)
    /// </summary>
    public partial class KBDR2031 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷テーブル使用列数
        /// </summary>
        public const int prtCols = 21;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        // 元帳区分設定変数
        private decimal MOTOCHO_KUBUN = 12; //2から12へ
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDR2031()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 現在の年号を取得する
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            //今月の最初の日を取得する
            DateTime dtMonthFr = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            string[] jpMontFr = Util.ConvJpDate(dtMonthFr, this.Dba);
            // 日付範囲(自)
            this.lblJpFr.Text = Util.ToString(jpMontFr[0]);
            this.txtDateYearFr.Text = jpMontFr[2];
            this.txtDateMonthFr.Text = jpMontFr[3];
            this.txtDateDayFr.Text = jpMontFr[4];
            // 日付範囲(至)
            this.lblJpTo.Text = Util.ToString(jpDate[0]);
            this.txtDateYearTo.Text = jpDate[2];
            this.txtDateMonthTo.Text = jpDate[3];
            this.txtDateDayTo.Text = jpDate[4];

            // 元帳区分取得
            MOTOCHO_KUBUN = (Util.ToDecimal(this.Par1) == 0) ? MOTOCHO_KUBUN : Util.ToDecimal(this.Par1);
            // フォーカス設定
            this.txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 年度1・2、仕入先コード1・2に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtShiiresakiCdFr":
                case "txtShiiresakiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShiiresakiCdFr":
                    #region 仕入先CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShiiresakiCdFr.Text = outData[0];
                                this.lblShiiresakiCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShiiresakiCdTo":
                    #region 仕入先CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShiiresakiCdTo.Text = outData[0];
                                this.lblShiiresakiCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearFr":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblJpFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblJpFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblJpFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblJpFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearTo":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblJpTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblJpTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblJpTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblJpTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[2] { "KBDR2031R","KOBR1032R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckJpDateFr();
                SetJpDateFr();
            }
        }
        /// <summary>
        /// 月(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckJpDateFr();
                SetJpDateFr();
            }
        }
        /// <summary>
        /// 日(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckJpDateFr();
                SetJpDateFr();
            }
        }
        /// <summary>
        /// 年(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckJpDateTo();
                SetJpDateTo();
            }
        }
        /// <summary>
        /// 月(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckJpDateTo();
                SetJpDateTo();
            }
        }
        /// <summary>
        /// 日(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckJpDateTo();
                SetJpDateTo();
            }
        }
        /// <summary>
        /// 仕入先コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtShiiresakiCdFr.SelectAll();
            }
        }
        /// <summary>
        /// 仕入先コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtShiiresakiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 仕入先CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiiresakiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtShiiresakiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblJpFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblJpFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }
        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblJpTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblJpTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 仕入先コード(自)の入力チェック
        /// </summary>
        private bool IsValidCodeFr()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtShiiresakiCdFr.Text);
            DataTable dtVI_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "TORIHIKISAKI_NM", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN3 = 3", dpc);

            if (ValChk.IsEmpty(this.txtShiiresakiCdFr.Text))
            {
                this.lblShiiresakiCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShiiresakiCdFr.Text))
            {
                Msg.Notice("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                //this.lblShiiresakiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", this.txtMizuageShishoCd.Text, this.txtShiiresakiCdFr.Text);
                this.lblShiiresakiCdFr.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"]);
            }

            return true;
        }

        /// <summary>
        /// 仕入先コード(至)の入力チェック
        /// </summary>
        private bool IsValidCodeTo()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtShiiresakiCdTo.Text);
            DataTable dtVI_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "TORIHIKISAKI_NM", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN3 = 3", dpc);

            if (ValChk.IsEmpty(this.txtShiiresakiCdTo.Text))
            {
                this.lblShiiresakiCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShiiresakiCdTo.Text))
            {
                Msg.Notice("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                //this.lblShiiresakiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", this.txtMizuageShishoCd.Text, this.txtShiiresakiCdTo.Text);
                this.lblShiiresakiCdTo.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"]);
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpDateTo();

            // 仕入先コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtShiiresakiCdFr.Focus();
                this.txtShiiresakiCdFr.SelectAll();
                return false;
            }
            // 仕入先コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtShiiresakiCdTo.Focus();
                this.txtShiiresakiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblJpFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblJpTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                if (rdo1.Checked) //明細選択時
                {
                    dataFlag = MakeWkData01();
                }
                else //合計選択時
                {
                    dataFlag = MakeWkData02();
                }

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");
                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    jp.co.fsi.common.report.BaseReport rpt;
                    string subTitle;
                    if (rdo1.Checked) //明細選択時
                    {
                        rpt = new KBDR2031R(dtOutput);
                        subTitle = " (伝票明細)";
                    }
                    else
                    {
                        rpt = new KBDR2032R(dtOutput);
                        subTitle = " (伝票合計)";
                    }

                    rpt.Document.Name = this.lblTitle.Text + subTitle;
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text + subTitle;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Rollback();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData01()
        {
            #region データ取得準備
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblJpTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            //今月の最後の日
            DateTime tmpLastDay = Util.ConvAdDate(this.lblJpTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(Util.ToInt(this.txtDateYearTo.Text), Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);
            
            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 日付表示形式を判断
            int dataHyojiFlag = 0; // 表示日付用変数
            // 入力開始日付が1日でない場合
            if (tmpDateFr.Day != 1)
            {
                dataHyojiFlag = 1;
            }
            // 入力終了日付が、その月の最終日でない場合
            else if (tmpDateTo != tmpLastDay)
            {
                dataHyojiFlag = 1;
            }
            // 入力開始日付と入力終了日付の年又は月が一致でない場合
            if (tmpDateFr.Year != tmpDateTo.Year || tmpDateFr.Month != tmpDateTo.Month)
            {
                dataHyojiFlag = 1;
            }

            string hyojiDate; // 表示用日付            
            if (dataHyojiFlag == 0)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // 仕入先コード設定
            string SHIIRESAKI_CD_FR;
            string SHIIRESAKI_CD_TO;
            if (Util.ToDecimal(txtShiiresakiCdFr.Text) > 0)
            {
                SHIIRESAKI_CD_FR = txtShiiresakiCdFr.Text;
            }
            else
            {
                SHIIRESAKI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtShiiresakiCdTo.Text) > 0)
            {
                SHIIRESAKI_CD_TO = txtShiiresakiCdTo.Text;
            }
            else
            {
                SHIIRESAKI_CD_TO = "9999";
            }
            // 会計年度設定
            int kaikeiNendo;
            if (tmpDateFr.Month <= 3)
            {
                kaikeiNendo = tmpDateFr.Year - 1;
            }
            else
            {
                kaikeiNendo = tmpDateFr.Year;
            }
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;

            StringBuilder Sql = new StringBuilder();
            #endregion

            #region 各業者の期首算を取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.TORIHIKISAKI_CD,");
            Sql.Append(" A.TORIHIKISAKI_NM,");
            Sql.Append(" A.DENWA_BANGO,");
            Sql.Append(" A.FAX_BANGO,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN");
            Sql.Append(" FROM TB_CM_TORIHIKISAKI AS A ");
            Sql.Append(" INNER JOIN TB_HN_TORIHIKISAKI_JOHO AS A1");
            Sql.Append(" 	ON	A.KAISHA_CD = A1.KAISHA_CD");
            Sql.Append(" 	AND A.TORIHIKISAKI_CD = A1.TORIHIKISAKI_CD");
            Sql.Append(" 	AND A1.TORIHIKISAKI_KUBUN3 = 3  ");
            Sql.Append(" LEFT JOIN ");
            Sql.Append(" (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		C.KAISHA_CD,");
            Sql.Append(" 		C.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 		SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN C.DENPYO_DATE < @DATE_FR THEN (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) ELSE 0 END) AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI AS C ");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B ");
            Sql.Append(" 		ON  B.KAISHA_CD = C.KAISHA_CD  ");
            Sql.Append(" 		AND B.SHISHO_CD = C.SHISHO_CD  ");
            Sql.Append(" 		AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD  ");
            Sql.Append(" 		AND @MOTOCHO_KUBUN = B.MOTOCHO_KUBUN ");
            Sql.Append(" 		AND C.DENPYO_DATE < @DATE_TO ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS D ");
            Sql.Append(" 		ON C.KAISHA_CD = D.KAISHA_CD  ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = D.KAIKEI_NENDO");
            Sql.Append(" 		AND C.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD  ");
            Sql.Append(" 	WHERE   ");
            Sql.Append(" 		C.KAISHA_CD = @KAISHA_CD AND  ");
            if (shishoCd != "0")
                Sql.Append(" 		C.SHISHO_CD = @SHISHO_CD AND  ");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND  ");
            Sql.Append(" 		C.HOJO_KAMOKU_CD BETWEEN @SHIIRESAKI_CD_FR AND @SHIIRESAKI_CD_TO ");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 	C.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 	C.SHISHO_CD,");
            Sql.Append(" 	C.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z");
            Sql.Append(" ON	A.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND A.TORIHIKISAKI_CD = Z.TORIHIKISAKI_CD");
            Sql.Append(" ORDER BY  ");
            Sql.Append(" A.TORIHIKISAKI_CD ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHIIRESAKI_CD_FR", SqlDbType.VarChar, 6, SHIIRESAKI_CD_FR);
            dpc.SetParam("@SHIIRESAKI_CD_TO", SqlDbType.VarChar, 6, SHIIRESAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);
            
            DataTable dtKishu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region メインデータを取得
            // han.VI_取引元帳(VI_HN_TORIHIKI_MOTOCHO)とhan.TB_商品(TB_HN_SHOHIN)の日付範囲から発生している取引先データを取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append(" 1 AS RANK,");
            Sql.Append(" A.TOKUISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" A.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" A.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" A.GYO_BANGO AS GYO_BANGO,");
            Sql.Append(" A.TORIHIKI_KUBUN1 AS TORIHIKI_KUBUN,");
            Sql.Append(" A.TORIHIKI_KUBUN_NM AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" A.SHOHIN_CD AS SHOHIN_CD,");
            Sql.Append(" A.SHOHIN_NM AS SHOHIN_NM,");
            Sql.Append(" A.IRISU AS IRISU,");
            Sql.Append(" (A.SURYO * A.ENZAN_HOHO) AS SURYO,");
            Sql.Append(" A.TANKA AS TANKA,");
            Sql.Append(" (A.URIAGE_KINGAKU * A.ENZAN_HOHO) AS URIAGE_KINGAKU,");
            Sql.Append(" (A.SHOHIZEI * A.ENZAN_HOHO) AS SHOHIZEIGAKU,");
            Sql.Append(" A.GOKEI_SHOHIZEI AS GOKEI_SHOHIZEI,");
            Sql.Append(" C.SHOHIZEI_TENKA_HOHO AS SHOHIZEI_TENKA_HOHO,");
            Sql.Append(" B.KIKAKU AS TEKIYO,");
            Sql.Append(" 0 AS SHIHARAIGAKU,");
            Sql.Append(" 0 AS TAISHAKU_KUBUN1,");
            Sql.Append(" 0 AS TAISHAKU_KUBUN2");
            Sql.Append(" FROM VI_HN_TORIHIKI_MOTOCHO AS A");
            Sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B");
            Sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD ");
            Sql.Append(" AND A.SHOHIN_CD = B.SHOHIN_CD ");

            Sql.Append(" LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS C ");
            Sql.Append(" 	ON	A.KAISHA_CD = C.KAISHA_CD     ");
            Sql.Append(" 	AND A.TOKUISAKI_CD = C.TORIHIKISAKI_CD ");

            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" A.DENPYO_KUBUN = 2 AND");
            Sql.Append(" A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            Sql.Append(" A.TOKUISAKI_CD BETWEEN @SHIIRESAKI_CD_FR AND @SHIIRESAKI_CD_TO");
            Sql.Append(" UNION");
            Sql.Append(" SELECT");
            Sql.Append(" 2 AS RANK,");
            Sql.Append(" B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" B.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" B.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" B.GYO_BANGO AS GYO_BANGO,");
            Sql.Append(" '9' AS TORIHIKI_KUBUN,");
            Sql.Append(" '' AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" 0 AS SHOHIN_CD,");
            Sql.Append(" '' AS SHOHIN_NM,");
            Sql.Append(" 0 AS IRISU,");
            Sql.Append(" 0 AS SURYO,");
            Sql.Append(" 0 AS TANKA,");
            Sql.Append(" 0 AS URIAGE_KINGAKU,");
            Sql.Append(" 0 AS SHOHIZEIGAKU,");
            Sql.Append(" 0 AS GOKEI_SHOHIZEI,");

            Sql.Append(" 0 AS SHOHIZEI_TENKA_HOHO,");

            Sql.Append(" B.TEKIYO AS TEKIYO,");
            Sql.Append(" B.ZEIKOMI_KINGAKU AS SHIHARAIGAKU,");
            Sql.Append(" B.TAISHAKU_KUBUN AS TAISHAKU_KUBUN1,");
            Sql.Append(" C.TAISHAKU_KUBUN AS TAISHAKU_KUBUN2");
            Sql.Append(" FROM TB_HN_NYUKIN_KAMOKU AS A");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B");
            Sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD ");
            Sql.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            Sql.Append(" AND 1 = B.DENPYO_KUBUN ");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C");
            Sql.Append(" ON  B.KAISHA_CD = C.KAISHA_CD ");
            Sql.Append(" AND B.KAIKEI_NENDO = C.KAIKEI_NENDO");
            Sql.Append(" AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
            Sql.Append(" AND C.KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append(" B.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" A.MOTOCHO_KUBUN = @MOTOCHO_KUBUN AND");
            Sql.Append(" B.HOJO_KAMOKU_CD BETWEEN @SHIIRESAKI_CD_FR AND @SHIIRESAKI_CD_TO AND");
            Sql.Append(" B.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            Sql.Append(" B.SHIWAKE_SAKUSEI_KUBUN IS NULL");
            Sql.Append(" ORDER BY ");
            Sql.Append(" TORIHIKISAKI_CD, DENPYO_DATE, RANK, DENPYO_BANGO, GYO_BANGO");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHIIRESAKI_CD_FR", SqlDbType.VarChar, 6, SHIIRESAKI_CD_FR);
            dpc.SetParam("@SHIIRESAKI_CD_TO", SqlDbType.VarChar, 6, SHIIRESAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                int i = 0; // ループ用カウント変数
                int flag = 0; // 表示フラグ
                List<int> tirihikisakiCd = new List<int>(); // 取引先番号格納用変数
                while (dtKishu.Rows.Count > i)
                {
                    if (Util.ToInt(dtKishu.Rows[i]["KISHUZAN"]) > 0)
                    {
                        tirihikisakiCd.Add(i);
                        flag = 1;
                    }
                    i++;
                }
                if (flag != 0)
                {
                    i = 0;
                    int dbSORT = 1;
                    while (tirihikisakiCd.Count > i)
                    {
                        #region 前月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM20");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM20");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 空行の登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 次月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM20");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM20");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        i++;
                    }
                }
                else
                {
                    Msg.Info("該当データがありません。");
                    return false;
                }
            }
            else
            {
                NumVals DENPYO = new NumVals(); // 伝票グループ
                NumVals MONTH = new NumVals(); // 月グループ
                NumVals TORIHIKISAKI = new NumVals(); // 取引先グループ

                #region メインループ準備処理
                String tmpNen = null;// 年
                String tmpNextNen = null;// 比較用年
                String tmpBeforeNen = null;// 比較用年
                String tmpGetsu = null;// 月
                String tmpNextGetsu = null;// 比較用月
                String tmpBeforeGetsu = null;// 比較用月
                int tmpDenpyo = 0;// 伝票
                int tmpNextDenpyo = 0;// 比較用伝票
                int tmpBeforeDenpyo = 0;// 比較用伝票
                int tmpTorihikisakiCd = 0;// 取引先コード
                int tmpNextTorihikisakiCd = 0;// 比較用取引先コード
                int tmpBeforeTorihikisakiCd = 0;// 比較用取引先コード
                int flag = 0;// 空データ表示フラグ

                String DenpyoDate = "";
                String DenpyoNo = "";
                String TorihikiKubunn = "";
                String Zandaka = "";

                String tmpDenwaBango = "";//電話番号変数
                String tmpFaxBango = "";//FAX番号変数
                String tmpTorihikisakiNm = null;// 取引先名
                string tekiyo; // 摘要

                int i = 0; // ループ用カウント変数
                int j = 1; // 1件後のデータとの比較用カウンタ変数
                int k = -1; // 1件前のデータとの比較用カウンタ変数
                int l = 0; // ループ用カウント変数
                int Multiple = 33; // 指定行での改ページ用変数
                int Count = 1; // 指定行での改ページ用カウンタ変数
                int z = 0; // 期首残・電話番号・FAX番号・取引先名の取得用カウンタ変数
                int dbSORT = 1;

                DataRow[] dr; // メインループ用データ変数
                #endregion

                while (dtKishu.Rows.Count > l)
                {
                    dr = dtMainLoop.Select("TORIHIKISAKI_CD = " + Util.ToString(dtKishu.Rows[l]["TORIHIKISAKI_CD"]));

                    if ((Util.ToDecimal(dtKishu.Rows[l]["KISHUZAN"]) > 0 || Util.ToDecimal(dtKishu.Rows[l]["ZANDAKA"]) > 0) && dr.Length == 0)
                    {
                        #region 前月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM20");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM20");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(dtKishu.Rows[l]["KISHUZAN"])); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        Count++; // 改ページ用カウンタ
                        #endregion

                        #region 空データの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, Util.ToString(dtKishu.Rows[l]["TORIHIKISAKI_CD"])); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                        flag = 1;
                        Count++; // 改ページ用カウンタ
                        #endregion

                        #region 次月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM20");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM20");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(dtKishu.Rows[l]["KISHUZAN"])); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        flag = 0;// 空データ表示フラグ
                        Count++; // 改ページ用カウンタ
                        #endregion
                    }
                    else if (dr.Length > 0)
                    {
                        while (dr.Length > i)
                        {
                            #region データの準備
                            // 伝票日付を和暦に変換
                            DateTime d = Util.ToDate(dr[i]["DENPYO_DATE"]);
                            string[] denpyo_date = Util.ConvJpDate(d, this.Dba);
                            if (denpyo_date[3].Length == 1)
                            {
                                denpyo_date[3] = " " + denpyo_date[3];
                            }
                            if (denpyo_date[4].Length == 1)
                            {
                                denpyo_date[4] = " " + denpyo_date[4];
                            }
                            String wareki_denpyo_date = denpyo_date[2] + "/" + denpyo_date[3] + "/" + denpyo_date[4];

                            // 比較用データがnullの場合(取引先コードが変わった時)、期首残・電話番号・FAX番号・取引先名を取得
                            if (tmpNextGetsu == null)
                            {
                                while (dtKishu.Rows.Count > z)
                                {
                                    if (Util.ToInt(dtKishu.Rows[z]["TORIHIKISAKI_CD"]) == Util.ToInt(dr[i]["TORIHIKISAKI_CD"]))
                                    {
                                        tmpTorihikisakiNm = Util.ToString(dtKishu.Rows[z]["TORIHIKISAKI_NM"]);
                                        TORIHIKISAKI.ZANDAKA += Util.ToInt(dtKishu.Rows[z]["KISHUZAN"]);
                                        tmpDenwaBango = Util.ToString(dtKishu.Rows[z]["DENWA_BANGO"]);
                                        tmpFaxBango = Util.ToString(dtKishu.Rows[z]["FAX_BANGO"]);
                                        break;
                                    }
                                    z++;
                                }
                            }

                            // 現在の伝票日付の年・月、伝票番号、取引先コードを取得
                            tmpNen = denpyo_date[2];
                            tmpGetsu = denpyo_date[3];
                            tmpDenpyo = Util.ToInt(dr[i]["DENPYO_BANGO"]);
                            tmpTorihikisakiCd = Util.ToInt(dr[i]["TORIHIKISAKI_CD"]);

                            // 比較用の伝票日付の年・月、伝票番号を取得
                            if (j < dr.Length)
                            {
                                // 伝票日付を和暦に変換
                                DateTime tmpD = Util.ToDate(dr[j]["DENPYO_DATE"]);
                                string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                                if (tmpDenpyoDate[3].Length == 1)
                                {
                                    tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                                }
                                tmpNextNen = tmpDenpyoDate[2];
                                tmpNextGetsu = tmpDenpyoDate[3];
                                tmpNextDenpyo = Util.ToInt(dr[j]["DENPYO_BANGO"]);
                                tmpNextTorihikisakiCd = Util.ToInt(dr[j]["TORIHIKISAKI_CD"]);
                            }
                            if (k != -1)
                            {
                                // 伝票日付を和暦に変換
                                DateTime tmpD = Util.ToDate(dr[k]["DENPYO_DATE"]);
                                string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                                if (tmpDenpyoDate[3].Length == 1)
                                {
                                    tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                                }
                                tmpBeforeNen = tmpDenpyoDate[2];
                                tmpBeforeGetsu = tmpDenpyoDate[3];
                                tmpBeforeDenpyo = Util.ToInt(dr[k]["DENPYO_BANGO"]);
                                tmpBeforeTorihikisakiCd = Util.ToInt(dr[k]["TORIHIKISAKI_CD"]);
                            }
                            #endregion

                            #region 印刷ワークテーブルに登録
                            // 登録するデータを判別
                            if (Util.ToInt(dr[i]["DENPYO_BANGO"]) != 0)
                            {
                                // 現在の取引先コードと前回の取引先コードが違っていれば実行 // 前回の年と現在の年が不一致の場合実行 // 前回の月と現在の月が不一致の場合実行
                                if (tmpTorihikisakiCd != tmpBeforeTorihikisakiCd || tmpNen != tmpBeforeNen || tmpGetsu != tmpBeforeGetsu)
                                {
                                    #region 前月繰越テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 期首残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが指定した倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 常に実行
                                #region 実データ登録
                                #region インサートテーブル
                                Sql = new StringBuilder();
                                dpc = new DbParamCollection();
                                Sql.Append("INSERT INTO PR_HN_TBL(");
                                Sql.Append("  GUID");
                                Sql.Append(" ,SORT");
                                Sql.Append(" ,ITEM01");
                                Sql.Append(" ,ITEM02");
                                Sql.Append(" ,ITEM03");
                                Sql.Append(" ,ITEM04");
                                Sql.Append(" ,ITEM05");
                                Sql.Append(" ,ITEM06");
                                Sql.Append(" ,ITEM07");
                                Sql.Append(" ,ITEM08");
                                Sql.Append(" ,ITEM09");
                                Sql.Append(" ,ITEM10");
                                Sql.Append(" ,ITEM11");
                                Sql.Append(" ,ITEM12");
                                Sql.Append(" ,ITEM13");
                                Sql.Append(" ,ITEM14");
                                Sql.Append(" ,ITEM15");
                                Sql.Append(" ,ITEM16");
                                Sql.Append(" ,ITEM17");
                                Sql.Append(" ,ITEM18");
                                Sql.Append(" ,ITEM19");
                                Sql.Append(" ,ITEM20");
                                Sql.Append(" ,ITEM21");
                                Sql.Append(") ");
                                Sql.Append("VALUES(");
                                Sql.Append("  @GUID");
                                Sql.Append(" ,@SORT");
                                Sql.Append(" ,@ITEM01");
                                Sql.Append(" ,@ITEM02");
                                Sql.Append(" ,@ITEM03");
                                Sql.Append(" ,@ITEM04");
                                Sql.Append(" ,@ITEM05");
                                Sql.Append(" ,@ITEM06");
                                Sql.Append(" ,@ITEM07");
                                Sql.Append(" ,@ITEM08");
                                Sql.Append(" ,@ITEM09");
                                Sql.Append(" ,@ITEM10");
                                Sql.Append(" ,@ITEM11");
                                Sql.Append(" ,@ITEM12");
                                Sql.Append(" ,@ITEM13");
                                Sql.Append(" ,@ITEM14");
                                Sql.Append(" ,@ITEM15");
                                Sql.Append(" ,@ITEM16");
                                Sql.Append(" ,@ITEM17");
                                Sql.Append(" ,@ITEM18");
                                Sql.Append(" ,@ITEM19");
                                Sql.Append(" ,@ITEM20");
                                Sql.Append(" ,@ITEM21");
                                Sql.Append(") ");
                                #endregion

                                #region ページヘッダーデータを設定
                                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                dbSORT++;
                                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                #endregion

                                if (tmpDenpyo == tmpBeforeDenpyo)
                                {
                                    DenpyoDate = "";
                                    DenpyoNo = "";
                                    TorihikiKubunn = "";
                                    Zandaka = "";
                                }
                                else
                                {
                                    DenpyoDate = wareki_denpyo_date;
                                    DenpyoNo = Util.ToString(dr[i]["DENPYO_BANGO"]);
                                    TorihikiKubunn = Util.ToString(dr[i]["TORIHIKI_KUBUN_NM"]);
                                    Zandaka = Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA));
                                }

                                //仕入時
                                if (Util.ToInt(dr[i]["TORIHIKI_KUBUN"]) != 9)
                                {
                                    tekiyo = Util.ToString(dr[i]["SHOHIN_NM"]) + "     " + Util.ToString(dr[i]["TEKIYO"]);
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, DenpyoDate); // 伝票日付
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, DenpyoNo); // 伝票No
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, TorihikiKubunn); // 取引区分
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, ""); // 文字列データ
                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, dr[i]["SHOHIN_CD"]); // 商品コード
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, tekiyo); // 商品名
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, dr[i]["IRISU"]); // 入数
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(dr[i]["SURYO"], 1)); // 数量
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["TANKA"]), 2)); // 単価
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]))); // 仕入金額
                                    //dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, ""); // 消費税額
                                    if (Util.ToString(dr[i]["SHOHIZEI_TENKA_HOHO"]) == "1")
                                    {
                                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]))); // 消費税
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, ""); // 消費税
                                    }
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, ""); // 支払額
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, ""); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 伝票グループ
                                    DENPYO.SHIIREGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 合計用仕入金額
                                    DENPYO.SHIHARAIGAKU += 0; // 合計用支払額
                                    // 月グループ
                                    MONTH.SHIIREGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 合計用仕入金額
                                    if (TorihikiKubunn == "現金仕入")
                                    {
                                        MONTH.SHIHARAIGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["GOKEI_SHOHIZEI"]); // 合計用支払額
                                    }

                                }
                                //支払時
                                else
                                {
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, DenpyoDate); // 伝票日付
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, DenpyoNo); // 伝票No
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, ""); // 取引区分
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, ""); // 文字列データ
                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 商品コード
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, dr[i]["TEKIYO"]); // 支払時摘要
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, ""); // 入数
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, ""); // 数量
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, ""); // 単価
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, ""); // 仕入金額
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, ""); // 消費税額
                                    if (Util.ToDecimal(dr[i]["TAISHAKU_KUBUN1"]) == Util.ToDecimal(dr[i]["TAISHAKU_KUBUN2"]))
                                    {
                                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, "-" + Util.FormatNum(Util.ToDecimal(dr[i]["SHIHARAIGAKU"]))); // 支払額
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["SHIHARAIGAKU"]))); // 支払額
                                    }

                                    if (TorihikiKubunn != "現金仕入")
                                    {
                                        if (Util.ToDecimal(dr[i]["TAISHAKU_KUBUN1"]) == Util.ToDecimal(dr[i]["TAISHAKU_KUBUN2"]))
                                        {
                                            TORIHIKISAKI.ZANDAKA += Util.ToDecimal(dr[i]["SHIHARAIGAKU"]); // 伝票合計用残高
                                        }
                                        else
                                        {
                                            TORIHIKISAKI.ZANDAKA -= Util.ToDecimal(dr[i]["SHIHARAIGAKU"]); // 伝票合計用残高
                                        }
                                    }
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 伝票グループ
                                    DENPYO.SHIIREGAKU += 0; // 合計用仕入金額
                                    DENPYO.SHIHARAIGAKU += Util.ToDecimal(dr[i]["GOKEI_SHOHIZEI"]); // 合計用支払額
                                    // 月グループ
                                    MONTH.SHIIREGAKU += 0; // 合計用仕入金額
                                    MONTH.SHOHIZEIGAKU += 0; // 合計用消費税額
                                    MONTH.SHIHARAIGAKU += Util.ToDecimal(dr[i]["GOKEI_SHOHIZEI"]); // 合計用支払額
                                }
                                Count++; // 改ページ用カウンタ
                                #endregion

                                // 改ページ用カウンタが指定した倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の伝票と次回の伝票が違っていれば実行 // 取引区分が1の時に実行
                                if ((dr.Length == j || tmpDenpyo != tmpNextDenpyo) && Util.ToInt(dr[i]["TORIHIKI_KUBUN"]) != 9)
                                {
                                    #region 伝票合計テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, "【伝票合計】"); // 伝票合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(DENPYO.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["GOKEI_SHOHIZEI"]))); // 消費税額合計
                                    if (TorihikiKubunn != "現金仕入")
                                    {
                                        TORIHIKISAKI.ZANDAKA += Util.ToDecimal(DENPYO.SHIIREGAKU) + Util.ToDecimal(dr[i]["GOKEI_SHOHIZEI"]); // 伝票合計用残高
                                    }
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    MONTH.SHOHIZEIGAKU += Util.ToDecimal(dr[i]["GOKEI_SHOHIZEI"]); // 合計用消費税額

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 伝票合計をクリア
                                    DENPYO.Clear();
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが指定した倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の伝票と次回の伝票が違っていれば実行
                                if (dr.Length == j || tmpDenpyo != tmpNextDenpyo)
                                {
                                    #region 空データの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    flag = 1;
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが指定した倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の年と次回の年が違っていれば実行 
                                // 現在の月と次回の月が違っていれば実行  // 現在の取引先コードと次回の取引先コードが違っていれば実行
                                if (dr.Length == j || tmpNen != tmpNextNen || tmpGetsu != tmpNextGetsu || tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                                {
                                    #region 月合計テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "【　　" + tmpGetsu + "月度 合計】"); // 月度合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    if (MONTH.SHOHIZEIGAKU == 0)
                                    {
                                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, ""); // 消費税合計
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    }
                                    if (MONTH.SHIHARAIGAKU == 0)
                                    {
                                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, ""); // 支払額合計
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    }
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 月合計をクリア
                                    MONTH.Clear();
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが指定した倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の年と次回の年が違っていれば実行 
                                // 現在の月と次回の月が違っていれば実行 // 現在の取引先コードと次回の取引先コードが違っていれば実行
                                if (dr.Length == j || tmpNen != tmpNextNen || tmpGetsu != tmpNextGetsu || tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                                {
                                    #region 次月繰越テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 期首残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    flag = 0;// 空データ表示フラグ
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが指定した倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 現在の伝票と次回の伝票が違っていれば実行 // flagが0の(空データ連続でない)時に実行
                                if (tmpDenpyo != tmpNextDenpyo && flag == 0)
                                {
                                    #region 空データの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    flag = 0;
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが指定した倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM11");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM11");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }
                            }

                            #region データの準備
                            // 比較用の取引先コードを取得
                            if (j == dr.Length)
                            {
                                if (l + 1 < dtKishu.Rows.Count)
                                {
                                    tmpNextTorihikisakiCd = Util.ToInt(dtKishu.Rows[l + 1]["TORIHIKISAKI_CD"]);
                                }

                            }

                            // 現在と次回の取引先コードが不一致の場合 // 比較用月データを初期化 // 改ページ用カウンタを初期化 // 残高をクリア
                            if (tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                            {
                                tmpNextGetsu = null;
                                Count = 1;
                                TORIHIKISAKI.Clear();
                            }

                            i++;
                            j++;
                            k++;
                            #endregion

                            #endregion
                        }
                    }
                    i = 0;
                    j = 1;
                    k = -1;
                    l++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);
            
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        
        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData02()
        {
            #region データ取得準備
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblJpTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            //今月の最後の日
            DateTime tmpLastDay = Util.ConvAdDate(this.lblJpTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(Util.ToInt(this.txtDateYearTo.Text), Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);
            
            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 日付表示形式を判断
            int dataHyojiFlag = 0; // 表示日付用変数
            // 入力開始日付が1日でない場合
            if (tmpDateFr.Day != 1)
            {
                dataHyojiFlag = 1;
            }
            // 入力終了日付が、その月の最終日でない場合
            else if (tmpDateTo != tmpLastDay)
            {
                dataHyojiFlag = 1;
            }
            // 入力開始日付と入力終了日付の年又は月が一致でない場合
            if (tmpDateFr.Year != tmpDateTo.Year || tmpDateFr.Month != tmpDateTo.Month)
            {
                dataHyojiFlag = 1;
            }

            string hyojiDate; // 表示用日付            
            if (dataHyojiFlag == 0)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // 仕入先コード設定
            string SHIIRESAKI_CD_FR;
            string SHIIRESAKI_CD_TO;
            if (Util.ToDecimal(txtShiiresakiCdFr.Text) > 0)
            {
                SHIIRESAKI_CD_FR = txtShiiresakiCdFr.Text;
            }
            else
            {
                SHIIRESAKI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtShiiresakiCdTo.Text) > 0)
            {
                SHIIRESAKI_CD_TO = txtShiiresakiCdTo.Text;
            }
            else
            {
                SHIIRESAKI_CD_TO = "9999";
            }
            // 会計年度設定
            int kaikeiNendo;
            if (tmpDateFr.Month <= 3)
            {
                kaikeiNendo = tmpDateFr.Year - 1;
            }
            else
            {
                kaikeiNendo = tmpDateFr.Year;
            }
            string shishoCd = this.txtMizuageShishoCd.Text;

            StringBuilder Sql = new StringBuilder();
            #endregion

            #region 各業者の期首算を取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.TORIHIKISAKI_CD,");
            Sql.Append(" A.TORIHIKISAKI_NM,");
            Sql.Append(" A.DENWA_BANGO,");
            Sql.Append(" A.FAX_BANGO,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN");
            Sql.Append(" FROM TB_CM_TORIHIKISAKI AS A ");
            Sql.Append(" INNER JOIN TB_HN_TORIHIKISAKI_JOHO AS A1");
            Sql.Append(" 	ON	A.KAISHA_CD = A1.KAISHA_CD");
            Sql.Append(" 	AND A.TORIHIKISAKI_CD = A1.TORIHIKISAKI_CD");
            Sql.Append(" 	AND A1.TORIHIKISAKI_KUBUN3 = 3  ");
            Sql.Append(" LEFT JOIN ");
            Sql.Append(" (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		C.KAISHA_CD,");
            Sql.Append(" 		C.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 		SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN C.DENPYO_DATE < @DATE_FR THEN (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) ELSE 0 END) AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI AS C ");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B ");
            Sql.Append(" 		ON  B.KAISHA_CD = C.KAISHA_CD  ");
            Sql.Append(" 		AND B.SHISHO_CD = C.SHISHO_CD  ");
            Sql.Append(" 		AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD  ");
            Sql.Append(" 		AND @MOTOCHO_KUBUN = B.MOTOCHO_KUBUN ");
            Sql.Append(" 		AND C.DENPYO_DATE < @DATE_TO ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS D ");
            Sql.Append(" 		ON C.KAISHA_CD = D.KAISHA_CD  ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = D.KAIKEI_NENDO");
            Sql.Append(" 		AND C.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD  ");
            Sql.Append(" 	WHERE   ");
            Sql.Append(" 		C.KAISHA_CD = @KAISHA_CD AND  ");
            if (shishoCd != "0")
                Sql.Append(" 		C.SHISHO_CD = @SHISHO_CD AND  ");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND  ");
            Sql.Append(" 		C.HOJO_KAMOKU_CD BETWEEN @SHIIRESAKI_CD_FR AND @SHIIRESAKI_CD_TO ");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 	C.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 	C.SHISHO_CD,");
            Sql.Append(" 	C.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z");
            Sql.Append(" ON	A.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND A.TORIHIKISAKI_CD = Z.TORIHIKISAKI_CD");
            Sql.Append(" ORDER BY  ");
            Sql.Append(" A.TORIHIKISAKI_CD ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHIIRESAKI_CD_FR", SqlDbType.VarChar, 6, SHIIRESAKI_CD_FR);
            dpc.SetParam("@SHIIRESAKI_CD_TO", SqlDbType.VarChar, 6, SHIIRESAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);
            
            DataTable dtKishu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region メインデータを取得
            // han.VI_取引伝票(VI_HN_TORIHIKI_DENPYO)とHan.TB_入金科目(TB_HN_NYUKIN_KAMOKU)とHan.VI_取引先情報(VI_HN_TORIHIKI_DENPYO)
            // の日付範囲から発生している取引先データを取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.AppendLine(" SELECT");
            Sql.AppendLine(" A.TOKUISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.AppendLine(" A.TOKUISAKI_NM AS TORIHIKISAKI_NM,");
            Sql.AppendLine(" A.DENPYO_DATE AS DENPYO_DATE,");
            Sql.AppendLine(" A.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.AppendLine(" MIN(A.TORIHIKI_KUBUN1) AS TORIHIKI_KUBUN,");
            Sql.AppendLine(" MIN(A.TORIHIKI_KUBUN_NM) AS TORIHIKI_KUBUN_NM,");
            Sql.AppendLine(" MIN(A.TEKIYO_NM) AS TEKIYO,");
            Sql.AppendLine(" SUM(A.URIAGE_KINGAKU * A.ENZAN_HOHO) AS URIAGE_KINGAKU,");
            Sql.AppendLine(" SUM(A.SHOHIZEI * A.ENZAN_HOHO) AS SHOHIZEIGAKU,");
            Sql.AppendLine(" 0 AS TAISHAKU_KUBUN1,");
            Sql.AppendLine(" 0 AS TAISHAKU_KUBUN2");
            Sql.AppendLine(" FROM VI_HN_TORIHIKI_MOTOCHO AS A");
            Sql.AppendLine(" WHERE ");
            Sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.AppendLine(" A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.AppendLine(" A.DENPYO_KUBUN = 2 AND ");
            Sql.AppendLine(" A.DENPYO_DATE Between @DATE_FR and @DATE_TO AND ");
            Sql.AppendLine(" A.TOKUISAKI_CD BETWEEN @SHIIRESAKI_CD_FR AND @SHIIRESAKI_CD_TO");
            Sql.AppendLine(" GROUP BY ");
            Sql.AppendLine(" A.TOKUISAKI_CD, A.DENPYO_DATE, ");
            Sql.AppendLine(" A.DENPYO_BANGO, A.ZEI_RITSU, ");
            Sql.AppendLine(" A.SHOHIZEI_NYURYOKU_HOHO, A.TOKUISAKI_NM");
            Sql.AppendLine(" ");
            Sql.AppendLine(" UNION");
            Sql.AppendLine(" ");
            Sql.AppendLine(" SELECT");
            Sql.AppendLine(" B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.AppendLine(" '' AS TORIHIKISAKI_NM,");
            Sql.AppendLine(" B.DENPYO_DATE AS DENPYO_DATE,");
            Sql.AppendLine(" B.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.AppendLine(" '9' AS TORIHIKI_KUBUN,");
            Sql.AppendLine(" '' AS TORIHIKI_KUBUN_NM,");
            Sql.AppendLine(" B.TEKIYO AS TEKIYO,");
            Sql.AppendLine(" B.ZEIKOMI_KINGAKU AS URIAGE_KINGAKU,");
            Sql.AppendLine(" 0 AS SHOHIZEIGAKU,");
            Sql.AppendLine(" B.TAISHAKU_KUBUN AS TAISHAKU_KUBUN1,");
            Sql.AppendLine(" C.TAISHAKU_KUBUN AS TAISHAKU_KUBUN2");
            Sql.AppendLine(" FROM  TB_HN_NYUKIN_KAMOKU AS A");
            Sql.AppendLine(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B");
            Sql.AppendLine(" ON  A.KAISHA_CD = B.KAISHA_CD ");
            Sql.AppendLine(" AND A.SHISHO_CD = B.SHISHO_CD ");
            Sql.AppendLine(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            Sql.AppendLine(" AND 1 = B.DENPYO_KUBUN ");
            Sql.AppendLine(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C");
            Sql.AppendLine(" ON  B.KAISHA_CD = C.KAISHA_CD ");
            Sql.AppendLine(" AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
            Sql.AppendLine(" AND B.KAIKEI_NENDO = C.KAIKEI_NENDO ");
            Sql.AppendLine(" WHERE ");
            Sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.AppendLine(" B.SHISHO_CD = @SHISHO_CD AND ");
            Sql.AppendLine(" B.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.AppendLine(" A.MOTOCHO_KUBUN = @MOTOCHO_KUBUN AND ");
            Sql.AppendLine(" B.HOJO_KAMOKU_CD BETWEEN @SHIIRESAKI_CD_FR AND @SHIIRESAKI_CD_TO AND ");
            Sql.AppendLine(" B.DENPYO_DATE BETWEEN @DATE_FR and @DATE_TO AND ");
            Sql.AppendLine(" B.SHIWAKE_SAKUSEI_KUBUN IS NULL");
            Sql.AppendLine(" ORDER BY TORIHIKISAKI_CD ASC, DENPYO_DATE ASC, TORIHIKI_KUBUN ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHIIRESAKI_CD_FR", SqlDbType.VarChar, 6, SHIIRESAKI_CD_FR);
            dpc.SetParam("@SHIIRESAKI_CD_TO", SqlDbType.VarChar, 6, SHIIRESAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);
            
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                int i = 0; // ループ用カウント変数
                int flag = 0; // 表示フラグ
                List<int> tirihikisakiCd = new List<int>(); // 取引先番号格納用変数
                while (dtKishu.Rows.Count > i)
                {
                    if (Util.ToInt(dtKishu.Rows[i]["KISHUZAN"]) > 0)
                    {
                        tirihikisakiCd.Add(i);
                        flag = 1;
                    }
                    i++;
                }
                if (flag != 0)
                {
                    i = 0;
                    int dbSORT = 1;
                    while (tirihikisakiCd.Count > i)
                    {
                        #region 前月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM16");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM16");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 空行の登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        #endregion

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 次月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM16");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM16");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate); // 年月日
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        i++;
                    }
                }
                else
                {
                    Msg.Info("該当データがありません。");
                    return false;
                }
            }
            else
            {
                NumVals MONTH = new NumVals(); // 月グループ
                NumVals TORIHIKISAKI = new NumVals(); // 取引先グループ

                #region メインループ準備処理
                String tmpNen = null;// 年
                String tmpNextNen = null;// 比較用年
                String tmpBeforeNen = null;// 比較用年
                String tmpGetsu = null;// 月
                String tmpNextGetsu = null;// 比較用月
                String tmpBeforeGetsu = null;// 比較用月
                int tmpDenpyo = 0;// 伝票
                int tmpNextDenpyo = 0;// 比較用伝票
                int tmpBeforeDenpyo = 0;// 比較用伝票
                int tmpTorihikisakiCd = 0;// 取引先コード
                int tmpNextTorihikisakiCd = 0;// 比較用取引先コード
                int tmpBeforeTorihikisakiCd = 0;// 比較用取引先コード

                String tmpDenwaBango = "";//電話番号変数
                String tmpFaxBango = "";//FAX番号変数
                String tmpTorihikisakiNm = null;// 取引先名

                int i = 0; // ループ用カウント変数
                int j = 1; // 1件後のデータとの比較用カウンタ変数
                int k = -1; // 1件前のデータとの比較用カウンタ変数
                int l = 0; // ループ用カウント変数
                int Multiple = 50; // 指定行での改ページ用変数
                int Count = 1; // 指定行での改ページ用カウンタ変数
                int z = 0; // 期首残・電話番号・FAX番号・取引先名の取得用カウンタ変数
                int dbSORT = 1;

                DataRow[] dr; // ループ用データ
                #endregion

                while (dtKishu.Rows.Count > l)
                {
                    dr = dtMainLoop.Select("TORIHIKISAKI_CD = " + Util.ToString(dtKishu.Rows[l]["TORIHIKISAKI_CD"]));
                    if ((Util.ToDecimal(dtKishu.Rows[l]["KISHUZAN"]) > 0 || Util.ToDecimal(dtKishu.Rows[l]["ZANDAKA"]) > 0) && dr.Length == 0)
                    {
                        #region 前月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM16");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM16");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(dtKishu.Rows[l]["KISHUZAN"])); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        Count++; // 改ページ用カウンタ
                        #endregion

                        #region 次月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM16");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM16");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(dtKishu.Rows[l]["KISHUZAN"])); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        Count++; // 改ページ用カウンタ
                        #endregion
                    }
                    else if (dr.Length > 0)
                    {
                        while (dr.Length > i)
                        {
                            #region データの準備
                            // 伝票日付を和暦に変換
                            DateTime d = Util.ToDate(dr[i]["DENPYO_DATE"]);
                            string[] denpyo_date = Util.ConvJpDate(d, this.Dba);
                            if (denpyo_date[3].Length == 1)
                            {
                                denpyo_date[3] = " " + denpyo_date[3];
                            }
                            if (denpyo_date[4].Length == 1)
                            {
                                denpyo_date[4] = " " + denpyo_date[4];
                            }
                            String wareki_denpyo_date = denpyo_date[2] + "/" + denpyo_date[3] + "/" + denpyo_date[4];

                            // 比較用データがnullの場合(取引先コードが変わった時)、期首残・電話番号・FAX番号・取引先名を取得
                            if (tmpNextGetsu == null)
                            {
                                while (dtKishu.Rows.Count > z)
                                {
                                    if (Util.ToInt(dtKishu.Rows[z]["TORIHIKISAKI_CD"]) == Util.ToInt(dr[i]["TORIHIKISAKI_CD"]))
                                    {
                                        tmpTorihikisakiNm = Util.ToString(dtKishu.Rows[z]["TORIHIKISAKI_NM"]);
                                        TORIHIKISAKI.ZANDAKA += Util.ToInt(dtKishu.Rows[z]["KISHUZAN"]);
                                        tmpDenwaBango = Util.ToString(dtKishu.Rows[z]["DENWA_BANGO"]);
                                        tmpFaxBango = Util.ToString(dtKishu.Rows[z]["FAX_BANGO"]);
                                        break;
                                    }
                                    z++;
                                }
                            }

                            // 現在の伝票日付の年・月、伝票番号、取引先コードを取得
                            tmpNen = denpyo_date[2];
                            tmpGetsu = denpyo_date[3];
                            tmpDenpyo = Util.ToInt(dr[i]["DENPYO_BANGO"]);
                            tmpTorihikisakiCd = Util.ToInt(dr[i]["TORIHIKISAKI_CD"]);

                            // 比較用の伝票日付の年・月、伝票番号を取得
                            if (j < dr.Length)
                            {
                                // 伝票日付を和暦に変換
                                DateTime tmpD = Util.ToDate(dr[j]["DENPYO_DATE"]);
                                string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                                if (tmpDenpyoDate[3].Length == 1)
                                {
                                    tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                                }
                                tmpNextNen = tmpDenpyoDate[2];
                                tmpNextGetsu = tmpDenpyoDate[3];
                                tmpNextDenpyo = Util.ToInt(dr[j]["DENPYO_BANGO"]);
                                tmpNextTorihikisakiCd = Util.ToInt(dr[j]["TORIHIKISAKI_CD"]);
                            }
                            if (k != -1)
                            {
                                // 伝票日付を和暦に変換
                                DateTime tmpD = Util.ToDate(dr[k]["DENPYO_DATE"]);
                                string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                                if (tmpDenpyoDate[3].Length == 1)
                                {
                                    tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                                }
                                tmpBeforeNen = tmpDenpyoDate[2];
                                tmpBeforeGetsu = tmpDenpyoDate[3];
                                tmpBeforeDenpyo = Util.ToInt(dr[k]["DENPYO_BANGO"]);
                                tmpBeforeTorihikisakiCd = Util.ToInt(dr[k]["TORIHIKISAKI_CD"]);
                            }
                            #endregion

                            #region 印刷ワークテーブルに登録
                            // 登録するデータを判別
                            if (Util.ToInt(dr[i]["DENPYO_BANGO"]) != 0)
                            {
                                // 現在の取引先コードと前回の取引先コードが違っていれば実行 // 前回の年と現在が不一致の場合実行 // 前回の月と現在が不一致の場合実行
                                if (tmpTorihikisakiCd != tmpBeforeTorihikisakiCd || tmpNen != tmpBeforeNen || tmpGetsu != tmpBeforeGetsu)
                                {
                                    #region 前月繰越テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 期首残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが34の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 常に実行
                                #region 実データ登録
                                #region インサートテーブル
                                Sql = new StringBuilder();
                                dpc = new DbParamCollection();
                                Sql.Append("INSERT INTO PR_HN_TBL(");
                                Sql.Append("  GUID");
                                Sql.Append(" ,SORT");
                                Sql.Append(" ,ITEM01");
                                Sql.Append(" ,ITEM02");
                                Sql.Append(" ,ITEM03");
                                Sql.Append(" ,ITEM04");
                                Sql.Append(" ,ITEM05");
                                Sql.Append(" ,ITEM06");
                                Sql.Append(" ,ITEM07");
                                Sql.Append(" ,ITEM08");
                                Sql.Append(" ,ITEM09");
                                Sql.Append(" ,ITEM10");
                                Sql.Append(" ,ITEM11");
                                Sql.Append(" ,ITEM13");
                                Sql.Append(" ,ITEM14");
                                Sql.Append(" ,ITEM15");
                                Sql.Append(" ,ITEM16");
                                Sql.Append(" ,ITEM17");
                                Sql.Append(") ");
                                Sql.Append("VALUES(");
                                Sql.Append("  @GUID");
                                Sql.Append(" ,@SORT");
                                Sql.Append(" ,@ITEM01");
                                Sql.Append(" ,@ITEM02");
                                Sql.Append(" ,@ITEM03");
                                Sql.Append(" ,@ITEM04");
                                Sql.Append(" ,@ITEM05");
                                Sql.Append(" ,@ITEM06");
                                Sql.Append(" ,@ITEM07");
                                Sql.Append(" ,@ITEM08");
                                Sql.Append(" ,@ITEM09");
                                Sql.Append(" ,@ITEM10");
                                Sql.Append(" ,@ITEM11");
                                Sql.Append(" ,@ITEM13");
                                Sql.Append(" ,@ITEM14");
                                Sql.Append(" ,@ITEM15");
                                Sql.Append(" ,@ITEM16");
                                Sql.Append(" ,@ITEM17");
                                Sql.Append(") ");
                                #endregion

                                #region ページヘッダーデータを設定
                                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                dbSORT++;
                                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                #endregion

                                //仕入時
                                if (Util.ToInt(dr[i]["TORIHIKI_KUBUN"]) != 9)
                                {
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, wareki_denpyo_date); // 伝票日付
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, Util.ToString(dr[i]["DENPYO_BANGO"])); // 伝票No
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, Util.ToString(dr[i]["TORIHIKI_KUBUN_NM"])); // 取引区分
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, ""); // 文字列データ
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]))); // 仕入金額
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]))); // 消費税額
                                    if (Util.ToString(dr[i]["TORIHIKI_KUBUN_NM"]) != "現金仕入")
                                    {
                                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, ""); // 支払額
                                        TORIHIKISAKI.ZANDAKA += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 伝票合計用残高
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]))); // 支払額
                                    }
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    // 月グループ
                                    MONTH.SHIIREGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 合計用仕入金額
                                    MONTH.SHOHIZEIGAKU += Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 合計用消費税額
                                    if (Util.ToString(dr[i]["TORIHIKI_KUBUN_NM"]) == "現金仕入")
                                    {
                                        MONTH.SHIHARAIGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 合計用支払額
                                    }

                                }
                                //支払時
                                else
                                {
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, wareki_denpyo_date); // 伝票日付
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, Util.ToString(dr[i]["DENPYO_BANGO"])); // 伝票No
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, ""); // 取引区分
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, Util.ToString(dr[i]["TEKIYO"])); // 文字列データ
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, ""); // 仕入金額
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, ""); // 消費税額
                                    if (Util.ToDecimal(dr[i]["TAISHAKU_KUBUN1"]) == Util.ToDecimal(dr[i]["TAISHAKU_KUBUN2"]))
                                    {
                                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, "-" + Util.FormatNum(Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]))); // 支払額
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]))); // 支払額
                                    }
                                    if (Util.ToString(dr[i]["TORIHIKI_KUBUN_NM"]) != "現金仕入")
                                    {
                                        if (Util.ToDecimal(dr[i]["TAISHAKU_KUBUN1"]) == Util.ToDecimal(dr[i]["TAISHAKU_KUBUN2"]))
                                        {
                                            TORIHIKISAKI.ZANDAKA += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 伝票合計用残高
                                        }
                                        else
                                        {
                                            TORIHIKISAKI.ZANDAKA -= Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 伝票合計用残高
                                        }
                                    } 
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 月グループ
                                    MONTH.SHIIREGAKU += 0; // 合計用仕入金額
                                    MONTH.SHOHIZEIGAKU += 0; // 合計用消費税額
                                    MONTH.SHIHARAIGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 合計用支払額
                                }
                                Count++; // 改ページ用カウンタ
                                #endregion

                                // 改ページ用カウンタが34の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが34の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の年と次回の年が違っていれば実行
                                // 現在の月と次回の月が違っていれば実行 // 現在の取引先コードと次回の取引先コードが違っていれば実行
                                if (dr.Length == j || tmpNen != tmpNextNen || tmpGetsu != tmpNextGetsu || tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                                {
                                    #region 月合計テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "【　　" + tmpGetsu + "月度 合計】"); // 月度合計
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 月合計をクリア
                                    MONTH.Clear();
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが34の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の年と次回の年が違っていれば実行
                                // 現在の月と次回の月が違っていれば実行 // 現在の取引先コードと次回の取引先コードが違っていれば実行
                                if (dr.Length == j || tmpNen != tmpNextNen || tmpGetsu != tmpNextGetsu || tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                                {
                                    #region 次月繰越テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 期首残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが34の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 現在の年と次回の年が違っていれば実行　// 現在の月と次回の月が違っていれば実行 // flagが0の(空データ連続でない)時に実行
                                if (tmpNen != tmpNextNen || tmpGetsu != tmpNextGetsu)
                                {
                                    #region 空データの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが34の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM13");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM13");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr[i]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIIREGAKU))); // 仕入金額合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHIHARAIGAKU))); // 支払額合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }
                            }

                            #region データの準備
                            // 比較用の取引先コードを取得
                            if (j == dr.Length)
                            {
                                if (l + 1 < dtKishu.Rows.Count)
                                {
                                    tmpNextTorihikisakiCd = Util.ToInt(dtKishu.Rows[l + 1]["TORIHIKISAKI_CD"]);
                                }
                            }

                            // 現在と次回の取引先コードが不一致の場合 // 比較用月データを初期化 // 改ページ用カウンタを初期化 // 残高をクリア
                            if (tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                            {
                                tmpNextGetsu = null;
                                Count = 1;
                                TORIHIKISAKI.Clear();
                            }

                            i++;
                            j++;
                            k++;
                            #endregion

                            #endregion
                        }
                    }
                    i = 0;
                    j = 1;
                    k = -1;
                    l++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);
            
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
