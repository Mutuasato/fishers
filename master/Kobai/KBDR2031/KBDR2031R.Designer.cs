﻿namespace jp.co.fsi.kb.kbdr2031
{
    /// <summary>
    /// KBDR2031R の概要の説明です。
    /// </summary>
    partial class KBDR2031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR2031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtGrpPages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.ラベル1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト255 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト256 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト324 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト253 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線101 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.LineR = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.LineL = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.直線197 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト255)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト256)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト324)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト253)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGrpPages,
            this.ラベル21,
            this.直線68,
            this.テキスト188,
            this.ラベル71,
            this.reportInfo1,
            this.ラベル1,
            this.ラベル52,
            this.ラベル53,
            this.ラベル54,
            this.ラベル56,
            this.ラベル57,
            this.ラベル58,
            this.ラベル74,
            this.ラベル24,
            this.ラベル25,
            this.ラベル27,
            this.テキスト31,
            this.テキスト34,
            this.テキスト255,
            this.テキスト256,
            this.テキスト324,
            this.label1,
            this.label2});
            this.pageHeader.Height = 0.8165354F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtGrpPages
            // 
            this.txtGrpPages.Height = 0.1875F;
            this.txtGrpPages.Left = 10.58348F;
            this.txtGrpPages.Name = "txtGrpPages";
            this.txtGrpPages.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.txtGrpPages.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGrpPages.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtGrpPages.Tag = "";
            this.txtGrpPages.Text = null;
            this.txtGrpPages.Top = 0.08333334F;
            this.txtGrpPages.Width = 0.375F;
            // 
            // ラベル21
            // 
            this.ラベル21.Height = 0.28125F;
            this.ラベル21.HyperLink = null;
            this.ラベル21.Left = 4.479168F;
            this.ラベル21.Name = "ラベル21";
            this.ラベル21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル21.Tag = "";
            this.ラベル21.Text = "買掛元帳問合せ";
            this.ラベル21.Top = 0F;
            this.ラベル21.Width = 2.270833F;
            // 
            // 直線68
            // 
            this.直線68.Height = 0F;
            this.直線68.Left = 4.377779F;
            this.直線68.LineWeight = 1F;
            this.直線68.Name = "直線68";
            this.直線68.Tag = "";
            this.直線68.Top = 0.3125F;
            this.直線68.Width = 2.473611F;
            this.直線68.X1 = 4.377779F;
            this.直線68.X2 = 6.85139F;
            this.直線68.Y1 = 0.3125F;
            this.直線68.Y2 = 0.3125F;
            // 
            // テキスト188
            // 
            this.テキスト188.DataField = "=IIf([ITEM07] Is Null,\"(\" & [ITEM05] & \" ～ \" & [ITEM06] & \")\",[ITEM07])";
            this.テキスト188.Height = 0.1770833F;
            this.テキスト188.Left = 3.891668F;
            this.テキスト188.Name = "テキスト188";
            this.テキスト188.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト188.Tag = "";
            this.テキスト188.Text = "=IIf([ITEM07] Is Null,\"(\" & [ITEM05] & \" ～ \" & [ITEM06] & \")\",[ITEM07])";
            this.テキスト188.Top = 0.3541666F;
            this.テキスト188.Width = 3.445833F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.1972222F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 10.96404F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: #4C535C; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 1";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "頁";
            this.ラベル71.Top = 0.07847223F;
            this.ラベル71.Width = 0.2027778F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1925197F;
            this.reportInfo1.Left = 9.283465F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.reportInfo1.Top = 0.07834646F;
            this.reportInfo1.Width = 1.3F;
            // 
            // ラベル1
            // 
            this.ラベル1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル1.Height = 0.1972222F;
            this.ラベル1.HyperLink = null;
            this.ラベル1.Left = 0F;
            this.ラベル1.Name = "ラベル1";
            this.ラベル1.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル1.Tag = "";
            this.ラベル1.Text = "伝票日付";
            this.ラベル1.Top = 0.6169292F;
            this.ラベル1.Width = 0.7251968F;
            // 
            // ラベル52
            // 
            this.ラベル52.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル52.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル52.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル52.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル52.Height = 0.1972222F;
            this.ラベル52.HyperLink = null;
            this.ラベル52.Left = 0.7291338F;
            this.ラベル52.Name = "ラベル52";
            this.ラベル52.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル52.Tag = "";
            this.ラベル52.Text = "伝票No.";
            this.ラベル52.Top = 0.6169292F;
            this.ラベル52.Width = 0.5311024F;
            // 
            // ラベル53
            // 
            this.ラベル53.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル53.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル53.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル53.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル53.Height = 0.1972222F;
            this.ラベル53.HyperLink = null;
            this.ラベル53.Left = 1.260236F;
            this.ラベル53.Name = "ラベル53";
            this.ラベル53.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル53.Tag = "";
            this.ラベル53.Text = "取引区分";
            this.ラベル53.Top = 0.6169292F;
            this.ラベル53.Width = 0.6770833F;
            // 
            // ラベル54
            // 
            this.ラベル54.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル54.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル54.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル54.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル54.Height = 0.1972222F;
            this.ラベル54.HyperLink = null;
            this.ラベル54.Left = 1.937402F;
            this.ラベル54.Name = "ラベル54";
            this.ラベル54.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル54.Tag = "";
            this.ラベル54.Text = "商　品　名　・　規　格";
            this.ラベル54.Top = 0.6169292F;
            this.ラベル54.Width = 3.489583F;
            // 
            // ラベル56
            // 
            this.ラベル56.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル56.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル56.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル56.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル56.Height = 0.1972222F;
            this.ラベル56.HyperLink = null;
            this.ラベル56.Left = 5.895833F;
            this.ラベル56.Name = "ラベル56";
            this.ラベル56.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル56.Tag = "";
            this.ラベル56.Text = "数　量";
            this.ラベル56.Top = 0.6169292F;
            this.ラベル56.Width = 0.875F;
            // 
            // ラベル57
            // 
            this.ラベル57.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル57.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル57.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル57.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル57.Height = 0.1972222F;
            this.ラベル57.HyperLink = null;
            this.ラベル57.Left = 9.322919F;
            this.ラベル57.Name = "ラベル57";
            this.ラベル57.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル57.Tag = "";
            this.ラベル57.Text = "支 払 額";
            this.ラベル57.Top = 0.6169292F;
            this.ラベル57.Width = 0.8333333F;
            // 
            // ラベル58
            // 
            this.ラベル58.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル58.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル58.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル58.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル58.Height = 0.1972222F;
            this.ラベル58.HyperLink = null;
            this.ラベル58.Left = 10.15625F;
            this.ラベル58.Name = "ラベル58";
            this.ラベル58.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル58.Tag = "";
            this.ラベル58.Text = "残　高";
            this.ラベル58.Top = 0.6169292F;
            this.ラベル58.Width = 1.020833F;
            // 
            // ラベル74
            // 
            this.ラベル74.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル74.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル74.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル74.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル74.Height = 0.1972222F;
            this.ラベル74.HyperLink = null;
            this.ラベル74.Left = 5.427083F;
            this.ラベル74.Name = "ラベル74";
            this.ラベル74.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル74.Tag = "";
            this.ラベル74.Text = "入数";
            this.ラベル74.Top = 0.6169292F;
            this.ラベル74.Width = 0.46875F;
            // 
            // ラベル24
            // 
            this.ラベル24.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル24.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル24.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル24.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル24.Height = 0.1972222F;
            this.ラベル24.HyperLink = null;
            this.ラベル24.Left = 7.637499F;
            this.ラベル24.Name = "ラベル24";
            this.ラベル24.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル24.Tag = "";
            this.ラベル24.Text = "仕入金額";
            this.ラベル24.Top = 0.6169292F;
            this.ラベル24.Width = 0.8645833F;
            // 
            // ラベル25
            // 
            this.ラベル25.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル25.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル25.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル25.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル25.Height = 0.1972222F;
            this.ラベル25.HyperLink = null;
            this.ラベル25.Left = 6.770833F;
            this.ラベル25.Name = "ラベル25";
            this.ラベル25.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル25.Tag = "";
            this.ラベル25.Text = "単　価";
            this.ラベル25.Top = 0.6169292F;
            this.ラベル25.Width = 0.875F;
            // 
            // ラベル27
            // 
            this.ラベル27.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル27.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル27.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル27.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル27.Height = 0.1972222F;
            this.ラベル27.HyperLink = null;
            this.ラベル27.Left = 8.500001F;
            this.ラベル27.Name = "ラベル27";
            this.ラベル27.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル27.Tag = "";
            this.ラベル27.Text = "消費税額";
            this.ラベル27.Top = 0.6169292F;
            this.ラベル27.Width = 0.8229167F;
            // 
            // テキスト31
            // 
            this.テキスト31.DataField = "ITEM01";
            this.テキスト31.Height = 0.2006944F;
            this.テキスト31.Left = 0.2322835F;
            this.テキスト31.Name = "テキスト31";
            this.テキスト31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.テキスト31.Tag = "";
            this.テキスト31.Text = "ITEM01";
            this.テキスト31.Top = 0.1181102F;
            this.テキスト31.Width = 0.6583334F;
            // 
            // テキスト34
            // 
            this.テキスト34.DataField = "ITEM02";
            this.テキスト34.Height = 0.2007874F;
            this.テキスト34.Left = 0.9405512F;
            this.テキスト34.Name = "テキスト34";
            this.テキスト34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.テキスト34.Tag = "";
            this.テキスト34.Text = "ITEM02";
            this.テキスト34.Top = 0.1181102F;
            this.テキスト34.Width = 2.168898F;
            // 
            // テキスト255
            // 
            this.テキスト255.DataField = "ITEM03";
            this.テキスト255.Height = 0.2007874F;
            this.テキスト255.Left = 0.9011811F;
            this.テキスト255.Name = "テキスト255";
            this.テキスト255.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト255.Tag = "";
            this.テキスト255.Text = "ITEM03";
            this.テキスト255.Top = 0.3543307F;
            this.テキスト255.Width = 0.9862205F;
            // 
            // テキスト256
            // 
            this.テキスト256.DataField = "ITEM04";
            this.テキスト256.Height = 0.2007874F;
            this.テキスト256.Left = 2.251181F;
            this.テキスト256.Name = "テキスト256";
            this.テキスト256.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト256.Tag = "";
            this.テキスト256.Text = "ITEM04";
            this.テキスト256.Top = 0.3543307F;
            this.テキスト256.Width = 1.179134F;
            // 
            // テキスト324
            // 
            this.テキスト324.DataField = "ITEM21";
            this.テキスト324.Height = 0.2007874F;
            this.テキスト324.Left = 8.29685F;
            this.テキスト324.Name = "テキスト324";
            this.テキスト324.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.テキスト324.Tag = "";
            this.テキスト324.Text = "ITEM21";
            this.テキスト324.Top = 0.3543307F;
            this.テキスト324.Width = 2.877165F;
            // 
            // label1
            // 
            this.label1.Height = 0.1976378F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.5267717F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label1.Text = "TEL:";
            this.label1.Top = 0.3543307F;
            this.label1.Width = 0.3637795F;
            // 
            // label2
            // 
            this.label2.Height = 0.1976378F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.887402F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label2.Text = "FAX:";
            this.label2.Top = 0.3543307F;
            this.label2.Width = 0.3637795F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト72,
            this.テキスト253,
            this.テキスト32,
            this.テキスト33,
            this.テキスト35,
            this.テキスト36,
            this.テキスト37,
            this.テキスト38,
            this.テキスト39,
            this.テキスト40,
            this.テキスト41,
            this.テキスト42,
            this.ITEM20,
            this.直線89,
            this.直線91,
            this.直線92,
            this.直線93,
            this.直線94,
            this.直線95,
            this.直線96,
            this.直線97,
            this.直線98,
            this.直線99,
            this.直線100,
            this.直線101,
            this.直線102,
            this.LineR,
            this.LineL,
            this.txtCount});
            this.detail.Height = 0.1979166F;
            this.detail.Name = "detail";
            // 
            // テキスト72
            // 
            this.テキスト72.DataField = "ITEM13";
            this.テキスト72.Height = 0.1784722F;
            this.テキスト72.Left = 2.479167F;
            this.テキスト72.Name = "テキスト72";
            this.テキスト72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.テキスト72.Tag = "";
            this.テキスト72.Text = "ITEM13";
            this.テキスト72.Top = 0F;
            this.テキスト72.Width = 2.95F;
            // 
            // テキスト253
            // 
            this.テキスト253.DataField = "ITEM11";
            this.テキスト253.Height = 0.1784722F;
            this.テキスト253.Left = 1.937402F;
            this.テキスト253.Name = "テキスト253";
            this.テキスト253.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト253.Tag = "";
            this.テキスト253.Text = "ITEM11";
            this.テキスト253.Top = 0F;
            this.テキスト253.Width = 3.493154F;
            // 
            // テキスト32
            // 
            this.テキスト32.DataField = "ITEM08";
            this.テキスト32.Height = 0.1784722F;
            this.テキスト32.Left = 0.02083334F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "ITEM08";
            this.テキスト32.Top = 0F;
            this.テキスト32.Width = 0.6423611F;
            // 
            // テキスト33
            // 
            this.テキスト33.DataField = "ITEM09";
            this.テキスト33.Height = 0.1784722F;
            this.テキスト33.Left = 0.7515748F;
            this.テキスト33.Name = "テキスト33";
            this.テキスト33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト33.Tag = "";
            this.テキスト33.Text = "ITEM09";
            this.テキスト33.Top = 0F;
            this.テキスト33.Width = 0.4901575F;
            // 
            // テキスト35
            // 
            this.テキスト35.DataField = "ITEM10";
            this.テキスト35.Height = 0.1784722F;
            this.テキスト35.Left = 1.302083F;
            this.テキスト35.MultiLine = false;
            this.テキスト35.Name = "テキスト35";
            this.テキスト35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト35.Tag = "";
            this.テキスト35.Text = "ITEM10";
            this.テキスト35.Top = 0F;
            this.テキスト35.Width = 0.5854167F;
            // 
            // テキスト36
            // 
            this.テキスト36.DataField = "ITEM12";
            this.テキスト36.Height = 0.1784722F;
            this.テキスト36.Left = 1.96875F;
            this.テキスト36.Name = "テキスト36";
            this.テキスト36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト36.Tag = "";
            this.テキスト36.Text = "ITEM12";
            this.テキスト36.Top = 0F;
            this.テキスト36.Width = 0.4395833F;
            // 
            // テキスト37
            // 
            this.テキスト37.DataField = "ITEM14";
            this.テキスト37.Height = 0.1784722F;
            this.テキスト37.Left = 5.4375F;
            this.テキスト37.Name = "テキスト37";
            this.テキスト37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト37.Tag = "";
            this.テキスト37.Text = "ITEM14";
            this.テキスト37.Top = 0F;
            this.テキスト37.Width = 0.4395833F;
            // 
            // テキスト38
            // 
            this.テキスト38.DataField = "ITEM15";
            this.テキスト38.Height = 0.1784722F;
            this.テキスト38.Left = 5.944445F;
            this.テキスト38.Name = "テキスト38";
            this.テキスト38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト38.Tag = "";
            this.テキスト38.Text = "ITEM15";
            this.テキスト38.Top = 0F;
            this.テキスト38.Width = 0.79375F;
            // 
            // テキスト39
            // 
            this.テキスト39.DataField = "ITEM16";
            this.テキスト39.Height = 0.1784722F;
            this.テキスト39.Left = 6.78125F;
            this.テキスト39.Name = "テキスト39";
            this.テキスト39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト39.Tag = "";
            this.テキスト39.Text = "ITEM16";
            this.テキスト39.Top = 0F;
            this.テキスト39.Width = 0.8458334F;
            // 
            // テキスト40
            // 
            this.テキスト40.DataField = "ITEM17";
            this.テキスト40.Height = 0.1784722F;
            this.テキスト40.Left = 7.666667F;
            this.テキスト40.Name = "テキスト40";
            this.テキスト40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト40.Tag = "";
            this.テキスト40.Text = "ITEM17";
            this.テキスト40.Top = 0F;
            this.テキスト40.Width = 0.8041667F;
            // 
            // テキスト41
            // 
            this.テキスト41.DataField = "ITEM18";
            this.テキスト41.Height = 0.1784722F;
            this.テキスト41.Left = 8.510419F;
            this.テキスト41.Name = "テキスト41";
            this.テキスト41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト41.Tag = "";
            this.テキスト41.Text = "ITEM18";
            this.テキスト41.Top = 0F;
            this.テキスト41.Width = 0.7729167F;
            // 
            // テキスト42
            // 
            this.テキスト42.DataField = "ITEM19";
            this.テキスト42.Height = 0.1784722F;
            this.テキスト42.Left = 9.354169F;
            this.テキスト42.Name = "テキスト42";
            this.テキスト42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト42.Tag = "";
            this.テキスト42.Text = "ITEM19";
            this.テキスト42.Top = 0F;
            this.テキスト42.Width = 0.7729167F;
            // 
            // ITEM20
            // 
            this.ITEM20.DataField = "ITEM20";
            this.ITEM20.Height = 0.1784722F;
            this.ITEM20.Left = 10.25F;
            this.ITEM20.Name = "ITEM20";
            this.ITEM20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.ITEM20.Tag = "";
            this.ITEM20.Text = "ITEM20";
            this.ITEM20.Top = 0F;
            this.ITEM20.Width = 0.8770834F;
            // 
            // 直線89
            // 
            this.直線89.Height = 0F;
            this.直線89.Left = 0F;
            this.直線89.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線89.LineWeight = 0F;
            this.直線89.Name = "直線89";
            this.直線89.Tag = "";
            this.直線89.Top = 0.1965278F;
            this.直線89.Width = 11.17361F;
            this.直線89.X1 = 0F;
            this.直線89.X2 = 11.17361F;
            this.直線89.Y1 = 0.1965278F;
            this.直線89.Y2 = 0.1965278F;
            // 
            // 直線91
            // 
            this.直線91.Height = 0.1784722F;
            this.直線91.Left = 0.7299213F;
            this.直線91.LineWeight = 0F;
            this.直線91.Name = "直線91";
            this.直線91.Tag = "";
            this.直線91.Top = 0F;
            this.直線91.Width = 0F;
            this.直線91.X1 = 0.7299213F;
            this.直線91.X2 = 0.7299213F;
            this.直線91.Y1 = 0F;
            this.直線91.Y2 = 0.1784722F;
            // 
            // 直線92
            // 
            this.直線92.Height = 0.1784722F;
            this.直線92.Left = 1.260417F;
            this.直線92.LineWeight = 0F;
            this.直線92.Name = "直線92";
            this.直線92.Tag = "";
            this.直線92.Top = 0F;
            this.直線92.Width = 0F;
            this.直線92.X1 = 1.260417F;
            this.直線92.X2 = 1.260417F;
            this.直線92.Y1 = 0F;
            this.直線92.Y2 = 0.1784722F;
            // 
            // 直線93
            // 
            this.直線93.Height = 0.1784722F;
            this.直線93.Left = 1.9375F;
            this.直線93.LineWeight = 0F;
            this.直線93.Name = "直線93";
            this.直線93.Tag = "";
            this.直線93.Top = 0F;
            this.直線93.Width = 0F;
            this.直線93.X1 = 1.9375F;
            this.直線93.X2 = 1.9375F;
            this.直線93.Y1 = 0F;
            this.直線93.Y2 = 0.1784722F;
            // 
            // 直線94
            // 
            this.直線94.Height = 0.1784722F;
            this.直線94.Left = 5.427083F;
            this.直線94.LineWeight = 0F;
            this.直線94.Name = "直線94";
            this.直線94.Tag = "";
            this.直線94.Top = 0F;
            this.直線94.Width = 0F;
            this.直線94.X1 = 5.427083F;
            this.直線94.X2 = 5.427083F;
            this.直線94.Y1 = 0F;
            this.直線94.Y2 = 0.1784722F;
            // 
            // 直線95
            // 
            this.直線95.Height = 0.1784722F;
            this.直線95.Left = 5.895833F;
            this.直線95.LineWeight = 0F;
            this.直線95.Name = "直線95";
            this.直線95.Tag = "";
            this.直線95.Top = 0F;
            this.直線95.Width = 0F;
            this.直線95.X1 = 5.895833F;
            this.直線95.X2 = 5.895833F;
            this.直線95.Y1 = 0F;
            this.直線95.Y2 = 0.1784722F;
            // 
            // 直線96
            // 
            this.直線96.Height = 0.1784722F;
            this.直線96.Left = 6.770833F;
            this.直線96.LineWeight = 0F;
            this.直線96.Name = "直線96";
            this.直線96.Tag = "";
            this.直線96.Top = 0F;
            this.直線96.Width = 0F;
            this.直線96.X1 = 6.770833F;
            this.直線96.X2 = 6.770833F;
            this.直線96.Y1 = 0F;
            this.直線96.Y2 = 0.1784722F;
            // 
            // 直線97
            // 
            this.直線97.Height = 0.1784722F;
            this.直線97.Left = 11.17708F;
            this.直線97.LineWeight = 0F;
            this.直線97.Name = "直線97";
            this.直線97.Tag = "";
            this.直線97.Top = 0F;
            this.直線97.Width = 0F;
            this.直線97.X1 = 11.17708F;
            this.直線97.X2 = 11.17708F;
            this.直線97.Y1 = 0F;
            this.直線97.Y2 = 0.1784722F;
            // 
            // 直線98
            // 
            this.直線98.Height = 0.1784722F;
            this.直線98.Left = 10.15625F;
            this.直線98.LineWeight = 0F;
            this.直線98.Name = "直線98";
            this.直線98.Tag = "";
            this.直線98.Top = 0F;
            this.直線98.Width = 0F;
            this.直線98.X1 = 10.15625F;
            this.直線98.X2 = 10.15625F;
            this.直線98.Y1 = 0F;
            this.直線98.Y2 = 0.1784722F;
            // 
            // 直線99
            // 
            this.直線99.Height = 0.1784722F;
            this.直線99.Left = 9.322919F;
            this.直線99.LineWeight = 0F;
            this.直線99.Name = "直線99";
            this.直線99.Tag = "";
            this.直線99.Top = 0F;
            this.直線99.Width = 0F;
            this.直線99.X1 = 9.322919F;
            this.直線99.X2 = 9.322919F;
            this.直線99.Y1 = 0F;
            this.直線99.Y2 = 0.1784722F;
            // 
            // 直線100
            // 
            this.直線100.Height = 0.1784722F;
            this.直線100.Left = 8.500001F;
            this.直線100.LineWeight = 0F;
            this.直線100.Name = "直線100";
            this.直線100.Tag = "";
            this.直線100.Top = 0F;
            this.直線100.Width = 0F;
            this.直線100.X1 = 8.500001F;
            this.直線100.X2 = 8.500001F;
            this.直線100.Y1 = 0F;
            this.直線100.Y2 = 0.1784722F;
            // 
            // 直線101
            // 
            this.直線101.Height = 0.1784722F;
            this.直線101.Left = 7.645833F;
            this.直線101.LineWeight = 0F;
            this.直線101.Name = "直線101";
            this.直線101.Tag = "";
            this.直線101.Top = 0F;
            this.直線101.Width = 0F;
            this.直線101.X1 = 7.645833F;
            this.直線101.X2 = 7.645833F;
            this.直線101.Y1 = 0F;
            this.直線101.Y2 = 0.1784722F;
            // 
            // 直線102
            // 
            this.直線102.Height = 0.1784722F;
            this.直線102.Left = 0F;
            this.直線102.LineWeight = 0F;
            this.直線102.Name = "直線102";
            this.直線102.Tag = "";
            this.直線102.Top = 0F;
            this.直線102.Width = 0F;
            this.直線102.X1 = 0F;
            this.直線102.X2 = 0F;
            this.直線102.Y1 = 0F;
            this.直線102.Y2 = 0.1784722F;
            // 
            // LineR
            // 
            this.LineR.Height = 0.1902778F;
            this.LineR.HyperLink = null;
            this.LineR.Left = 11.10417F;
            this.LineR.Name = "LineR";
            this.LineR.Style = "color: #4C535C; font-family: ＭＳ 明朝; font-weight: bold; text-align: right; ddo-cha" +
    "r-set: 1";
            this.LineR.Tag = "";
            this.LineR.Text = "罫線右";
            this.LineR.Top = 0.01041669F;
            this.LineR.Visible = false;
            this.LineR.Width = 0.04652778F;
            // 
            // LineL
            // 
            this.LineL.Height = 0.1895833F;
            this.LineL.HyperLink = null;
            this.LineL.Left = 0F;
            this.LineL.Name = "LineL";
            this.LineL.Style = "color: #4C535C; font-family: ＭＳ Ｐゴシック; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
            this.LineL.Tag = "";
            this.LineL.Text = "罫線左";
            this.LineL.Top = 0F;
            this.LineL.Visible = false;
            this.LineL.Width = 0.03611111F;
            // 
            // txtCount
            // 
            this.txtCount.Height = 0.1784722F;
            this.txtCount.Left = 4.60625F;
            this.txtCount.Name = "txtCount";
            this.txtCount.Style = "color: Black; font-family: ＭＳ 明朝; font-weight: bold; text-align: center; ddo-char" +
    "-set: 1";
            this.txtCount.Tag = "";
            this.txtCount.Text = null;
            this.txtCount.Top = 0F;
            this.txtCount.Visible = false;
            this.txtCount.Width = 0.4395833F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM01";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線197});
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // 直線197
            // 
            this.直線197.Height = 0F;
            this.直線197.Left = 0.02086614F;
            this.直線197.LineWeight = 1F;
            this.直線197.Name = "直線197";
            this.直線197.Tag = "";
            this.直線197.Top = 0F;
            this.直線197.Width = 11.17361F;
            this.直線197.X1 = 0.02086614F;
            this.直線197.X2 = 11.19448F;
            this.直線197.Y1 = 0F;
            this.直線197.Y2 = 0F;
            // 
            // KBDR2031R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1456693F;
            this.PageSettings.Margins.Left = 0.1456693F;
            this.PageSettings.Margins.Right = 0.1456693F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.22917F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト255)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト256)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト324)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト253)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpPages;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル21;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト188;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト72;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線89;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線91;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線92;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線93;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線94;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線95;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線96;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線97;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線98;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線99;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線100;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線101;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト253;
        private GrapeCity.ActiveReports.SectionReportModel.Label LineR;
        private GrapeCity.ActiveReports.SectionReportModel.Label LineL;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線197;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル52;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル53;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル54;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル56;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル57;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル58;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル74;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル24;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル25;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト255;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト256;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト324;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;


    }
}
