﻿namespace jp.co.fsi.kb.kbdr2031
{
    /// <summary>
    /// KOBR1032R の概要の説明です。
    /// </summary>
    partial class KBDR2032R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR2032R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ラベル21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.txtGrpPages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル181 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル183 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル184 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル185 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル186 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル187 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト189 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト190 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト255 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト256 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト194 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線106 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト193 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.直線191 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル180)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル181)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル182)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル183)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル184)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル185)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル186)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル187)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト189)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト190)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト255)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト256)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト194)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト193)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル21,
            this.直線68,
            this.テキスト188,
            this.ラベル71,
            this.reportInfo1,
            this.txtGrpPages,
            this.ラベル180,
            this.ラベル181,
            this.ラベル182,
            this.ラベル183,
            this.ラベル184,
            this.ラベル185,
            this.ラベル186,
            this.ラベル187,
            this.テキスト189,
            this.テキスト190,
            this.テキスト255,
            this.テキスト256,
            this.テキスト194,
            this.label1,
            this.label2});
            this.pageHeader.Height = 1.158661F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ラベル21
            // 
            this.ラベル21.Height = 0.28125F;
            this.ラベル21.HyperLink = null;
            this.ラベル21.Left = 2.807292F;
            this.ラベル21.Name = "ラベル21";
            this.ラベル21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル21.Tag = "";
            this.ラベル21.Text = "買掛元帳問合せ";
            this.ラベル21.Top = 0F;
            this.ラベル21.Width = 2.270833F;
            // 
            // 直線68
            // 
            this.直線68.Height = 0F;
            this.直線68.Left = 2.705903F;
            this.直線68.LineWeight = 1F;
            this.直線68.Name = "直線68";
            this.直線68.Tag = "";
            this.直線68.Top = 0.3125F;
            this.直線68.Width = 2.473611F;
            this.直線68.X1 = 2.705903F;
            this.直線68.X2 = 5.179514F;
            this.直線68.Y1 = 0.3125F;
            this.直線68.Y2 = 0.3125F;
            // 
            // テキスト188
            // 
            this.テキスト188.DataField = "=IIf([ITEM07] Is Null,\"(\" & [ITEM05] & \" ～ \" & [ITEM06] & \")\",[ITEM07])";
            this.テキスト188.Height = 0.1770833F;
            this.テキスト188.Left = 2.219792F;
            this.テキスト188.Name = "テキスト188";
            this.テキスト188.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 11.25pt; font-weight: normal; text" +
    "-align: center; ddo-char-set: 128";
            this.テキスト188.Tag = "";
            this.テキスト188.Text = "=IIf([ITEM07] Is Null,\"(\" & [ITEM05] & \" ～ \" & [ITEM06] & \")\",[ITEM07])";
            this.テキスト188.Top = 0.3541667F;
            this.テキスト188.Width = 3.445833F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.1972222F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 7.526323F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: #4C535C; font-family: ＭＳ 明朝; font-weight: bold; text-align: right; ddo-cha" +
    "r-set: 1";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "頁";
            this.ラベル71.Top = 0F;
            this.ラベル71.Width = 0.2027778F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1925197F;
            this.reportInfo1.Left = 5.84567F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt";
            this.reportInfo1.Top = 0F;
            this.reportInfo1.Width = 1.3F;
            // 
            // txtGrpPages
            // 
            this.txtGrpPages.Height = 0.1875F;
            this.txtGrpPages.Left = 7.120079F;
            this.txtGrpPages.Name = "txtGrpPages";
            this.txtGrpPages.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.txtGrpPages.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGrpPages.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtGrpPages.Tag = "";
            this.txtGrpPages.Text = null;
            this.txtGrpPages.Top = 0F;
            this.txtGrpPages.Width = 0.375F;
            // 
            // ラベル180
            // 
            this.ラベル180.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル180.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル180.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル180.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル180.Height = 0.1979167F;
            this.ラベル180.HyperLink = null;
            this.ラベル180.Left = 0F;
            this.ラベル180.Name = "ラベル180";
            this.ラベル180.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル180.Tag = "";
            this.ラベル180.Text = "伝票日付";
            this.ラベル180.Top = 0.9606299F;
            this.ラベル180.Width = 0.7916667F;
            // 
            // ラベル181
            // 
            this.ラベル181.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル181.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル181.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル181.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル181.Height = 0.1979167F;
            this.ラベル181.HyperLink = null;
            this.ラベル181.Left = 0.7917323F;
            this.ラベル181.Name = "ラベル181";
            this.ラベル181.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル181.Tag = "";
            this.ラベル181.Text = "伝票No.";
            this.ラベル181.Top = 0.9606299F;
            this.ラベル181.Width = 0.5416667F;
            // 
            // ラベル182
            // 
            this.ラベル182.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル182.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル182.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル182.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル182.Height = 0.1979167F;
            this.ラベル182.HyperLink = null;
            this.ラベル182.Left = 1.309055F;
            this.ラベル182.Name = "ラベル182";
            this.ラベル182.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル182.Tag = "";
            this.ラベル182.Text = "取引区分";
            this.ラベル182.Top = 0.9606299F;
            this.ラベル182.Width = 0.6770833F;
            // 
            // ラベル183
            // 
            this.ラベル183.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル183.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル183.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル183.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル183.Height = 0.1979167F;
            this.ラベル183.HyperLink = null;
            this.ラベル183.Left = 1.989764F;
            this.ラベル183.Name = "ラベル183";
            this.ラベル183.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル183.Tag = "";
            this.ラベル183.Text = "摘　　　要";
            this.ラベル183.Top = 0.9606299F;
            this.ラベル183.Width = 2.208333F;
            // 
            // ラベル184
            // 
            this.ラベル184.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル184.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル184.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル184.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル184.Height = 0.1979167F;
            this.ラベル184.HyperLink = null;
            this.ラベル184.Left = 5.099213F;
            this.ラベル184.Name = "ラベル184";
            this.ラベル184.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル184.Tag = "";
            this.ラベル184.Text = "消費税額";
            this.ラベル184.Top = 0.9606299F;
            this.ラベル184.Width = 0.8229167F;
            // 
            // ラベル185
            // 
            this.ラベル185.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル185.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル185.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル185.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル185.Height = 0.1979167F;
            this.ラベル185.HyperLink = null;
            this.ラベル185.Left = 5.916535F;
            this.ラベル185.Name = "ラベル185";
            this.ラベル185.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル185.Tag = "";
            this.ラベル185.Text = "支　払　額";
            this.ラベル185.Top = 0.9606299F;
            this.ラベル185.Width = 0.8333333F;
            // 
            // ラベル186
            // 
            this.ラベル186.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル186.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル186.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル186.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル186.Height = 0.1979167F;
            this.ラベル186.HyperLink = null;
            this.ラベル186.Left = 6.739764F;
            this.ラベル186.Name = "ラベル186";
            this.ラベル186.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル186.Tag = "";
            this.ラベル186.Text = "残　高";
            this.ラベル186.Top = 0.9606299F;
            this.ラベル186.Width = 0.9893701F;
            // 
            // ラベル187
            // 
            this.ラベル187.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル187.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル187.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル187.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル187.Height = 0.1979167F;
            this.ラベル187.HyperLink = null;
            this.ラベル187.Left = 4.198031F;
            this.ラベル187.Name = "ラベル187";
            this.ラベル187.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 12" +
    "8";
            this.ラベル187.Tag = "";
            this.ラベル187.Text = "仕入金額";
            this.ラベル187.Top = 0.9606299F;
            this.ラベル187.Width = 0.9166667F;
            // 
            // テキスト189
            // 
            this.テキスト189.DataField = "ITEM01";
            this.テキスト189.Height = 0.2006944F;
            this.テキスト189.Left = 0F;
            this.テキスト189.Name = "テキスト189";
            this.テキスト189.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.テキスト189.Tag = "";
            this.テキスト189.Text = "ITEM01";
            this.テキスト189.Top = 0.4889764F;
            this.テキスト189.Width = 0.6583334F;
            // 
            // テキスト190
            // 
            this.テキスト190.DataField = "ITEM02";
            this.テキスト190.Height = 0.2006944F;
            this.テキスト190.Left = 0.7082677F;
            this.テキスト190.Name = "テキスト190";
            this.テキスト190.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.テキスト190.Tag = "";
            this.テキスト190.Text = "ITEM02";
            this.テキスト190.Top = 0.4889764F;
            this.テキスト190.Width = 1.73125F;
            // 
            // テキスト255
            // 
            this.テキスト255.DataField = "ITEM03";
            this.テキスト255.Height = 0.2006944F;
            this.テキスト255.Left = 0.3929134F;
            this.テキスト255.Name = "テキスト255";
            this.テキスト255.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト255.Tag = "";
            this.テキスト255.Text = "ITM03";
            this.テキスト255.Top = 0.7251968F;
            this.テキスト255.Width = 1.081103F;
            // 
            // テキスト256
            // 
            this.テキスト256.DataField = "ITEM04";
            this.テキスト256.Height = 0.2006944F;
            this.テキスト256.Left = 1.989764F;
            this.テキスト256.Name = "テキスト256";
            this.テキスト256.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.テキスト256.Tag = "";
            this.テキスト256.Text = "ITEM04";
            this.テキスト256.Top = 0.7251968F;
            this.テキスト256.Width = 1.106299F;
            // 
            // テキスト194
            // 
            this.テキスト194.DataField = "ITEM17";
            this.テキスト194.Height = 0.2007874F;
            this.テキスト194.Left = 4.423228F;
            this.テキスト194.Name = "テキスト194";
            this.テキスト194.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 128";
            this.テキスト194.Tag = "";
            this.テキスト194.Text = "ITEM17";
            this.テキスト194.Top = 0.6854331F;
            this.テキスト194.Width = 3.288583F;
            // 
            // label1
            // 
            this.label1.Height = 0.2007874F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.08346457F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.label1.Text = "TEL:";
            this.label1.Top = 0.7251968F;
            this.label1.Width = 0.3645833F;
            // 
            // label2
            // 
            this.label2.Height = 0.2007874F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.661417F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.label2.Text = "FAX:";
            this.label2.Top = 0.7244095F;
            this.label2.Width = 0.3645833F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト32,
            this.テキスト33,
            this.テキスト35,
            this.テキスト36,
            this.テキスト37,
            this.テキスト38,
            this.テキスト39,
            this.テキスト49,
            this.直線16,
            this.直線18,
            this.直線21,
            this.直線22,
            this.直線23,
            this.直線24,
            this.直線25,
            this.直線26,
            this.直線27,
            this.直線106,
            this.テキスト193});
            this.detail.Height = 0.1979167F;
            this.detail.Name = "detail";
            // 
            // テキスト32
            // 
            this.テキスト32.DataField = "ITEM08";
            this.テキスト32.Height = 0.1972222F;
            this.テキスト32.Left = 0.03125F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "ITEM08";
            this.テキスト32.Top = 0F;
            this.テキスト32.Width = 0.75F;
            // 
            // テキスト33
            // 
            this.テキスト33.DataField = "ITEM09";
            this.テキスト33.Height = 0.1972222F;
            this.テキスト33.Left = 0.7868056F;
            this.テキスト33.Name = "テキスト33";
            this.テキスト33.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト33.Tag = "";
            this.テキスト33.Text = "ITEM09";
            this.テキスト33.Top = 0F;
            this.テキスト33.Width = 0.5229167F;
            // 
            // テキスト35
            // 
            this.テキスト35.DataField = "ITEM10";
            this.テキスト35.Height = 0.1972222F;
            this.テキスト35.Left = 1.3125F;
            this.テキスト35.MultiLine = false;
            this.テキスト35.Name = "テキスト35";
            this.テキスト35.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト35.Tag = "";
            this.テキスト35.Text = "ITEM10";
            this.テキスト35.Top = 0F;
            this.テキスト35.Width = 0.6583334F;
            // 
            // テキスト36
            // 
            this.テキスト36.DataField = "ITEM11";
            this.テキスト36.Height = 0.1972222F;
            this.テキスト36.Left = 1.968056F;
            this.テキスト36.Name = "テキスト36";
            this.テキスト36.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: left; ddo-char-set: 128";
            this.テキスト36.Tag = "";
            this.テキスト36.Text = "ITEM11";
            this.テキスト36.Top = 0F;
            this.テキスト36.Width = 2.241667F;
            // 
            // テキスト37
            // 
            this.テキスト37.DataField = "ITEM13";
            this.テキスト37.Height = 0.1972222F;
            this.テキスト37.Left = 4.212501F;
            this.テキスト37.Name = "テキスト37";
            this.テキスト37.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト37.Tag = "";
            this.テキスト37.Text = "ITEM13";
            this.テキスト37.Top = 0F;
            this.テキスト37.Width = 0.8354167F;
            // 
            // テキスト38
            // 
            this.テキスト38.DataField = "ITEM15";
            this.テキスト38.Height = 0.1972222F;
            this.テキスト38.Left = 5.904861F;
            this.テキスト38.Name = "テキスト38";
            this.テキスト38.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト38.Tag = "";
            this.テキスト38.Text = "ITEM15";
            this.テキスト38.Top = 0F;
            this.テキスト38.Width = 0.79375F;
            // 
            // テキスト39
            // 
            this.テキスト39.DataField = "ITEM16";
            this.テキスト39.Height = 0.1972222F;
            this.テキスト39.Left = 6.735417F;
            this.テキスト39.Name = "テキスト39";
            this.テキスト39.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト39.Tag = "";
            this.テキスト39.Text = "ITEM16";
            this.テキスト39.Top = 0F;
            this.テキスト39.Width = 0.9291667F;
            // 
            // テキスト49
            // 
            this.テキスト49.DataField = "ITEM14";
            this.テキスト49.Height = 0.1972222F;
            this.テキスト49.Left = 5.118055F;
            this.テキスト49.Name = "テキスト49";
            this.テキスト49.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 128";
            this.テキスト49.Tag = "";
            this.テキスト49.Text = "ITEM14";
            this.テキスト49.Top = 0F;
            this.テキスト49.Width = 0.7520834F;
            // 
            // 直線16
            // 
            this.直線16.Height = 0F;
            this.直線16.Left = 0F;
            this.直線16.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線16.LineWeight = 0F;
            this.直線16.Name = "直線16";
            this.直線16.Tag = "";
            this.直線16.Top = 0.1965278F;
            this.直線16.Width = 7.736112F;
            this.直線16.X1 = 0F;
            this.直線16.X2 = 7.736112F;
            this.直線16.Y1 = 0.1965278F;
            this.直線16.Y2 = 0.1965278F;
            // 
            // 直線18
            // 
            this.直線18.Height = 0.1972222F;
            this.直線18.Left = 0F;
            this.直線18.LineWeight = 0F;
            this.直線18.Name = "直線18";
            this.直線18.Tag = "";
            this.直線18.Top = 0F;
            this.直線18.Width = 0F;
            this.直線18.X1 = 0F;
            this.直線18.X2 = 0F;
            this.直線18.Y1 = 0F;
            this.直線18.Y2 = 0.1972222F;
            // 
            // 直線21
            // 
            this.直線21.Height = 0.1972222F;
            this.直線21.Left = 0.7916667F;
            this.直線21.LineWeight = 0F;
            this.直線21.Name = "直線21";
            this.直線21.Tag = "";
            this.直線21.Top = 0F;
            this.直線21.Width = 0F;
            this.直線21.X1 = 0.7916667F;
            this.直線21.X2 = 0.7916667F;
            this.直線21.Y1 = 0F;
            this.直線21.Y2 = 0.1972222F;
            // 
            // 直線22
            // 
            this.直線22.Height = 0.1972222F;
            this.直線22.Left = 1.3125F;
            this.直線22.LineWeight = 0F;
            this.直線22.Name = "直線22";
            this.直線22.Tag = "";
            this.直線22.Top = 0F;
            this.直線22.Width = 0F;
            this.直線22.X1 = 1.3125F;
            this.直線22.X2 = 1.3125F;
            this.直線22.Y1 = 0F;
            this.直線22.Y2 = 0.1972222F;
            // 
            // 直線23
            // 
            this.直線23.Height = 0.1972222F;
            this.直線23.Left = 1.989583F;
            this.直線23.LineWeight = 0F;
            this.直線23.Name = "直線23";
            this.直線23.Tag = "";
            this.直線23.Top = 0F;
            this.直線23.Width = 0F;
            this.直線23.X1 = 1.989583F;
            this.直線23.X2 = 1.989583F;
            this.直線23.Y1 = 0F;
            this.直線23.Y2 = 0.1972222F;
            // 
            // 直線24
            // 
            this.直線24.Height = 0.1972222F;
            this.直線24.Left = 4.197917F;
            this.直線24.LineWeight = 0F;
            this.直線24.Name = "直線24";
            this.直線24.Tag = "";
            this.直線24.Top = 0F;
            this.直線24.Width = 0F;
            this.直線24.X1 = 4.197917F;
            this.直線24.X2 = 4.197917F;
            this.直線24.Y1 = 0F;
            this.直線24.Y2 = 0.1972222F;
            // 
            // 直線25
            // 
            this.直線25.Height = 0.1972222F;
            this.直線25.Left = 5.114584F;
            this.直線25.LineWeight = 0F;
            this.直線25.Name = "直線25";
            this.直線25.Tag = "";
            this.直線25.Top = 0F;
            this.直線25.Width = 0F;
            this.直線25.X1 = 5.114584F;
            this.直線25.X2 = 5.114584F;
            this.直線25.Y1 = 0F;
            this.直線25.Y2 = 0.1972222F;
            // 
            // 直線26
            // 
            this.直線26.Height = 0.1972222F;
            this.直線26.Left = 5.916667F;
            this.直線26.LineWeight = 0F;
            this.直線26.Name = "直線26";
            this.直線26.Tag = "";
            this.直線26.Top = 0F;
            this.直線26.Width = 0F;
            this.直線26.X1 = 5.916667F;
            this.直線26.X2 = 5.916667F;
            this.直線26.Y1 = 0F;
            this.直線26.Y2 = 0.1972222F;
            // 
            // 直線27
            // 
            this.直線27.Height = 0.1972222F;
            this.直線27.Left = 6.739764F;
            this.直線27.LineWeight = 0F;
            this.直線27.Name = "直線27";
            this.直線27.Tag = "";
            this.直線27.Top = 0F;
            this.直線27.Width = 0F;
            this.直線27.X1 = 6.739764F;
            this.直線27.X2 = 6.739764F;
            this.直線27.Y1 = 0F;
            this.直線27.Y2 = 0.1972222F;
            // 
            // 直線106
            // 
            this.直線106.Height = 0.1972222F;
            this.直線106.Left = 7.729167F;
            this.直線106.LineWeight = 0F;
            this.直線106.Name = "直線106";
            this.直線106.Tag = "";
            this.直線106.Top = 0F;
            this.直線106.Width = 0F;
            this.直線106.X1 = 7.729167F;
            this.直線106.X2 = 7.729167F;
            this.直線106.Y1 = 0F;
            this.直線106.Y2 = 0.1972222F;
            // 
            // テキスト193
            // 
            this.テキスト193.DataField = "ITEM12";
            this.テキスト193.Height = 0.1972222F;
            this.テキスト193.Left = 1.986221F;
            this.テキスト193.MultiLine = false;
            this.テキスト193.Name = "テキスト193";
            this.テキスト193.Style = "color: Black; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-" +
    "align: center; ddo-char-set: 128";
            this.テキスト193.Tag = "";
            this.テキスト193.Text = "ITEM12";
            this.テキスト193.Top = 0F;
            this.テキスト193.Width = 2.223502F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM01";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線191});
            this.groupFooter1.Height = 0.05208333F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // 直線191
            // 
            this.直線191.Height = 0F;
            this.直線191.Left = 0.003543307F;
            this.直線191.LineWeight = 0F;
            this.直線191.Name = "直線191";
            this.直線191.Tag = "";
            this.直線191.Top = 0F;
            this.直線191.Width = 7.725695F;
            this.直線191.X1 = 0.003543307F;
            this.直線191.X2 = 7.729238F;
            this.直線191.Y1 = 0F;
            this.直線191.Y2 = 0F;
            // 
            // KBDR2032R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.885417F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.KOBR1032R_FetchData);
            this.PageEnd += new System.EventHandler(this.KOBR1032R_PageEnd);
            ((System.ComponentModel.ISupportInitialize)(this.ラベル21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル180)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル181)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル182)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル183)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル184)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル185)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル186)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル187)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト189)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト190)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト255)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト256)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト194)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト193)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル21;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト188;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト49;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線16;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線18;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線21;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線22;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線23;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線24;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線25;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線26;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線27;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト193;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpPages;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル180;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル181;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル182;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル183;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル184;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル185;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル186;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル187;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト189;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト190;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト255;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト256;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト194;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線191;
    }
}
