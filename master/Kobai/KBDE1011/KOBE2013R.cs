﻿using System.Data;

using jp.co.sosok.erp.common.report;

namespace jp.co.sosok.erp.kob.kobe2011
{
    /// <summary>
    /// KOBE2013R の概要の説明です。
    /// </summary>
    public partial class KOBE2013R : BaseReport
    {

        public KOBE2013R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
