﻿namespace jp.co.fsi.kb.kbde1011
{
    partial class KBDE1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblMode = new System.Windows.Forms.Label();
			this.lblTantoshaNm = new System.Windows.Forms.Label();
			this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTantoshaCd = new System.Windows.Forms.Label();
			this.lblFunanushiNm = new System.Windows.Forms.Label();
			this.txtFunanushiCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiCd = new System.Windows.Forms.Label();
			this.txtDenpyoNo = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblDenpyoNo = new System.Windows.Forms.Label();
			this.lblDay = new System.Windows.Forms.Label();
			this.lblMonth = new System.Windows.Forms.Label();
			this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblYear = new System.Windows.Forms.Label();
			this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtGengoYear = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengo = new System.Windows.Forms.Label();
			this.lblJp = new System.Windows.Forms.Label();
			this.lblTorihikiKubunNm = new System.Windows.Forms.Label();
			this.txtTorihikiKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTorihikiKubunCd = new System.Windows.Forms.Label();
			this.pnlDenpyoSyukei = new System.Windows.Forms.Panel();
			this.txtTotalGaku = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSyohiZei = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtSyokei = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblTotalGaku = new System.Windows.Forms.Label();
			this.lblSyohiZei = new System.Windows.Forms.Label();
			this.lblSyokei = new System.Windows.Forms.Label();
			this.dgvInputList = new System.Windows.Forms.DataGridView();
			this.txtGridEdit = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblBeforeDenpyoNo = new System.Windows.Forms.Label();
			this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new System.Windows.Forms.Label();
			this.lblMizuageShisho = new System.Windows.Forms.Label();
			this.pnlShohizei = new System.Windows.Forms.Panel();
			this.lblShohizeiTenka2 = new System.Windows.Forms.Label();
			this.lblShohizeiInputHoho2 = new System.Windows.Forms.Label();
			this.lblShohizeiTenka1 = new System.Windows.Forms.Label();
			this.lblShohizeiInputHoho1 = new System.Windows.Forms.Label();
			this.lblTaxInfo = new System.Windows.Forms.Label();
			this.txtDummy = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinNm = new System.Windows.Forms.Label();
			this.lblZaikoSu = new System.Windows.Forms.Label();
			this.lblAutoData = new System.Windows.Forms.Label();
			this.GYO_BANGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SHOHIN_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SHOHIN_NM = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.IRISU = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TANI = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KESUSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BARA_SOSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.URI_TANKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BAIKA_KINGAKU = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SHOHIZEI = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ZEI_RITSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SHIWAKE_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BUMON_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.JIGYO_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KUBUN_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.GEN_TANKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.KAZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.pnlDebug.SuspendLayout();
			this.pnlDenpyoSyukei.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
			this.pnlShohizei.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 609);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(1119, 41);
			this.lblTitle.Text = "購買売上入力";
			// 
			// lblMode
			// 
			this.lblMode.BackColor = System.Drawing.Color.Transparent;
			this.lblMode.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMode.Location = new System.Drawing.Point(16, 57);
			this.lblMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMode.Name = "lblMode";
			this.lblMode.Size = new System.Drawing.Size(111, 27);
			this.lblMode.TabIndex = 1;
			this.lblMode.Text = "【登録】";
			this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTantoshaNm
			// 
			this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
			this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoshaNm.Location = new System.Drawing.Point(747, 133);
			this.lblTantoshaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaNm.Name = "lblTantoshaNm";
			this.lblTantoshaNm.Size = new System.Drawing.Size(312, 27);
			this.lblTantoshaNm.TabIndex = 24;
			this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTantoshaCd
			// 
			this.txtTantoshaCd.AutoSizeFromLength = true;
			this.txtTantoshaCd.DisplayLength = null;
			this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTantoshaCd.Location = new System.Drawing.Point(700, 133);
			this.txtTantoshaCd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtTantoshaCd.MaxLength = 4;
			this.txtTantoshaCd.Name = "txtTantoshaCd";
			this.txtTantoshaCd.Size = new System.Drawing.Size(44, 20);
			this.txtTantoshaCd.TabIndex = 23;
			this.txtTantoshaCd.Text = "0";
			this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTantoshaCd_KeyDown);
			this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
			// 
			// lblTantoshaCd
			// 
			this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
			this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTantoshaCd.Location = new System.Drawing.Point(608, 131);
			this.lblTantoshaCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTantoshaCd.Name = "lblTantoshaCd";
			this.lblTantoshaCd.Size = new System.Drawing.Size(457, 33);
			this.lblTantoshaCd.TabIndex = 22;
			this.lblTantoshaCd.Text = "担 当 者";
			this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFunanushiNm
			// 
			this.lblFunanushiNm.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiNm.Location = new System.Drawing.Point(181, 133);
			this.lblFunanushiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiNm.Name = "lblFunanushiNm";
			this.lblFunanushiNm.Size = new System.Drawing.Size(420, 27);
			this.lblFunanushiNm.TabIndex = 18;
			this.lblFunanushiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtFunanushiCd
			// 
			this.txtFunanushiCd.AutoSizeFromLength = true;
			this.txtFunanushiCd.DisplayLength = null;
			this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCd.Location = new System.Drawing.Point(112, 133);
			this.txtFunanushiCd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtFunanushiCd.MaxLength = 4;
			this.txtFunanushiCd.Name = "txtFunanushiCd";
			this.txtFunanushiCd.Size = new System.Drawing.Size(63, 20);
			this.txtFunanushiCd.TabIndex = 17;
			this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCd_Validating);
			// 
			// lblFunanushiCd
			// 
			this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
			this.lblFunanushiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblFunanushiCd.Location = new System.Drawing.Point(16, 131);
			this.lblFunanushiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFunanushiCd.Name = "lblFunanushiCd";
			this.lblFunanushiCd.Size = new System.Drawing.Size(589, 33);
			this.lblFunanushiCd.TabIndex = 16;
			this.lblFunanushiCd.Text = "船主CD";
			this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtDenpyoNo
			// 
			this.txtDenpyoNo.AutoSizeFromLength = true;
			this.txtDenpyoNo.DisplayLength = null;
			this.txtDenpyoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDenpyoNo.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDenpyoNo.Location = new System.Drawing.Point(112, 95);
			this.txtDenpyoNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtDenpyoNo.MaxLength = 6;
			this.txtDenpyoNo.Name = "txtDenpyoNo";
			this.txtDenpyoNo.Size = new System.Drawing.Size(63, 20);
			this.txtDenpyoNo.TabIndex = 3;
			this.txtDenpyoNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDenpyoNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenpyoNo_Validating);
			// 
			// lblDenpyoNo
			// 
			this.lblDenpyoNo.BackColor = System.Drawing.Color.Silver;
			this.lblDenpyoNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDenpyoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDenpyoNo.Location = new System.Drawing.Point(16, 92);
			this.lblDenpyoNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDenpyoNo.Name = "lblDenpyoNo";
			this.lblDenpyoNo.Size = new System.Drawing.Size(164, 33);
			this.lblDenpyoNo.TabIndex = 2;
			this.lblDenpyoNo.Text = "伝票番号";
			this.lblDenpyoNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDay
			// 
			this.lblDay.AutoSize = true;
			this.lblDay.BackColor = System.Drawing.Color.Silver;
			this.lblDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblDay.Location = new System.Drawing.Point(576, 101);
			this.lblDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDay.Name = "lblDay";
			this.lblDay.Size = new System.Drawing.Size(21, 13);
			this.lblDay.TabIndex = 12;
			this.lblDay.Text = "日";
			// 
			// lblMonth
			// 
			this.lblMonth.AutoSize = true;
			this.lblMonth.BackColor = System.Drawing.Color.Silver;
			this.lblMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMonth.Location = new System.Drawing.Point(484, 101);
			this.lblMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMonth.Name = "lblMonth";
			this.lblMonth.Size = new System.Drawing.Size(21, 13);
			this.lblMonth.TabIndex = 10;
			this.lblMonth.Text = "月";
			// 
			// txtDay
			// 
			this.txtDay.AutoSizeFromLength = false;
			this.txtDay.DisplayLength = null;
			this.txtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDay.Location = new System.Drawing.Point(515, 96);
			this.txtDay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtDay.MaxLength = 2;
			this.txtDay.Name = "txtDay";
			this.txtDay.Size = new System.Drawing.Size(52, 20);
			this.txtDay.TabIndex = 11;
			this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
			// 
			// lblYear
			// 
			this.lblYear.AutoSize = true;
			this.lblYear.BackColor = System.Drawing.Color.Silver;
			this.lblYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYear.Location = new System.Drawing.Point(395, 101);
			this.lblYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(21, 13);
			this.lblYear.TabIndex = 8;
			this.lblYear.Text = "年";
			// 
			// txtMonth
			// 
			this.txtMonth.AutoSizeFromLength = false;
			this.txtMonth.DisplayLength = null;
			this.txtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMonth.Location = new System.Drawing.Point(425, 96);
			this.txtMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtMonth.MaxLength = 2;
			this.txtMonth.Name = "txtMonth";
			this.txtMonth.Size = new System.Drawing.Size(52, 20);
			this.txtMonth.TabIndex = 9;
			this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
			// 
			// txtGengoYear
			// 
			this.txtGengoYear.AutoSizeFromLength = false;
			this.txtGengoYear.DisplayLength = null;
			this.txtGengoYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGengoYear.Location = new System.Drawing.Point(336, 96);
			this.txtGengoYear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtGengoYear.MaxLength = 2;
			this.txtGengoYear.Name = "txtGengoYear";
			this.txtGengoYear.Size = new System.Drawing.Size(52, 20);
			this.txtGengoYear.TabIndex = 7;
			this.txtGengoYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYear_Validating);
			// 
			// lblGengo
			// 
			this.lblGengo.BackColor = System.Drawing.Color.Silver;
			this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblGengo.Location = new System.Drawing.Point(276, 96);
			this.lblGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblGengo.Name = "lblGengo";
			this.lblGengo.Size = new System.Drawing.Size(53, 27);
			this.lblGengo.TabIndex = 5;
			this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblJp
			// 
			this.lblJp.BackColor = System.Drawing.Color.Silver;
			this.lblJp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJp.Location = new System.Drawing.Point(183, 92);
			this.lblJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJp.Name = "lblJp";
			this.lblJp.Size = new System.Drawing.Size(423, 33);
			this.lblJp.TabIndex = 6;
			this.lblJp.Text = " 伝票日付";
			this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTorihikiKubunNm
			// 
			this.lblTorihikiKubunNm.BackColor = System.Drawing.Color.Silver;
			this.lblTorihikiKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTorihikiKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTorihikiKubunNm.Location = new System.Drawing.Point(747, 96);
			this.lblTorihikiKubunNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTorihikiKubunNm.Name = "lblTorihikiKubunNm";
			this.lblTorihikiKubunNm.Size = new System.Drawing.Size(169, 27);
			this.lblTorihikiKubunNm.TabIndex = 15;
			this.lblTorihikiKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTorihikiKubunCd
			// 
			this.txtTorihikiKubunCd.AutoSizeFromLength = true;
			this.txtTorihikiKubunCd.DisplayLength = null;
			this.txtTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTorihikiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTorihikiKubunCd.Location = new System.Drawing.Point(700, 96);
			this.txtTorihikiKubunCd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtTorihikiKubunCd.MaxLength = 2;
			this.txtTorihikiKubunCd.Name = "txtTorihikiKubunCd";
			this.txtTorihikiKubunCd.Size = new System.Drawing.Size(44, 20);
			this.txtTorihikiKubunCd.TabIndex = 14;
			this.txtTorihikiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTorihikiKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikiKubunCd_Validating);
			// 
			// lblTorihikiKubunCd
			// 
			this.lblTorihikiKubunCd.BackColor = System.Drawing.Color.Silver;
			this.lblTorihikiKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTorihikiKubunCd.Location = new System.Drawing.Point(608, 92);
			this.lblTorihikiKubunCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTorihikiKubunCd.Name = "lblTorihikiKubunCd";
			this.lblTorihikiKubunCd.Size = new System.Drawing.Size(316, 33);
			this.lblTorihikiKubunCd.TabIndex = 13;
			this.lblTorihikiKubunCd.Text = "取引区分";
			this.lblTorihikiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pnlDenpyoSyukei
			// 
			this.pnlDenpyoSyukei.BackColor = System.Drawing.Color.Silver;
			this.pnlDenpyoSyukei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlDenpyoSyukei.Controls.Add(this.txtTotalGaku);
			this.pnlDenpyoSyukei.Controls.Add(this.txtSyohiZei);
			this.pnlDenpyoSyukei.Controls.Add(this.txtSyokei);
			this.pnlDenpyoSyukei.Controls.Add(this.lblTotalGaku);
			this.pnlDenpyoSyukei.Controls.Add(this.lblSyohiZei);
			this.pnlDenpyoSyukei.Controls.Add(this.lblSyokei);
			this.pnlDenpyoSyukei.Location = new System.Drawing.Point(832, 589);
			this.pnlDenpyoSyukei.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pnlDenpyoSyukei.Name = "pnlDenpyoSyukei";
			this.pnlDenpyoSyukei.Size = new System.Drawing.Size(270, 115);
			this.pnlDenpyoSyukei.TabIndex = 27;
			// 
			// txtTotalGaku
			// 
			this.txtTotalGaku.AutoSizeFromLength = true;
			this.txtTotalGaku.DisplayLength = null;
			this.txtTotalGaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTotalGaku.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtTotalGaku.Location = new System.Drawing.Point(141, 75);
			this.txtTotalGaku.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtTotalGaku.MaxLength = 12;
			this.txtTotalGaku.Name = "txtTotalGaku";
			this.txtTotalGaku.ReadOnly = true;
			this.txtTotalGaku.Size = new System.Drawing.Size(119, 20);
			this.txtTotalGaku.TabIndex = 5;
			this.txtTotalGaku.TabStop = false;
			this.txtTotalGaku.Text = "0";
			this.txtTotalGaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtSyohiZei
			// 
			this.txtSyohiZei.AutoSizeFromLength = true;
			this.txtSyohiZei.DisplayLength = null;
			this.txtSyohiZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSyohiZei.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSyohiZei.Location = new System.Drawing.Point(141, 41);
			this.txtSyohiZei.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtSyohiZei.MaxLength = 12;
			this.txtSyohiZei.Name = "txtSyohiZei";
			this.txtSyohiZei.ReadOnly = true;
			this.txtSyohiZei.Size = new System.Drawing.Size(119, 20);
			this.txtSyohiZei.TabIndex = 3;
			this.txtSyohiZei.TabStop = false;
			this.txtSyohiZei.Text = "0";
			this.txtSyohiZei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtSyokei
			// 
			this.txtSyokei.AutoSizeFromLength = true;
			this.txtSyokei.DisplayLength = null;
			this.txtSyokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtSyokei.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtSyokei.Location = new System.Drawing.Point(141, 8);
			this.txtSyokei.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtSyokei.MaxLength = 12;
			this.txtSyokei.Name = "txtSyokei";
			this.txtSyokei.ReadOnly = true;
			this.txtSyokei.Size = new System.Drawing.Size(119, 20);
			this.txtSyokei.TabIndex = 1;
			this.txtSyokei.Text = "0";
			this.txtSyokei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblTotalGaku
			// 
			this.lblTotalGaku.BackColor = System.Drawing.Color.Silver;
			this.lblTotalGaku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTotalGaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTotalGaku.Location = new System.Drawing.Point(4, 75);
			this.lblTotalGaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTotalGaku.Name = "lblTotalGaku";
			this.lblTotalGaku.Size = new System.Drawing.Size(136, 27);
			this.lblTotalGaku.TabIndex = 4;
			this.lblTotalGaku.Text = "合　計　額";
			this.lblTotalGaku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSyohiZei
			// 
			this.lblSyohiZei.BackColor = System.Drawing.Color.Silver;
			this.lblSyohiZei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSyohiZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSyohiZei.Location = new System.Drawing.Point(4, 41);
			this.lblSyohiZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSyohiZei.Name = "lblSyohiZei";
			this.lblSyohiZei.Size = new System.Drawing.Size(136, 27);
			this.lblSyohiZei.TabIndex = 2;
			this.lblSyohiZei.Text = "消　費　税";
			this.lblSyohiZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSyokei
			// 
			this.lblSyokei.BackColor = System.Drawing.Color.Silver;
			this.lblSyokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSyokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblSyokei.Location = new System.Drawing.Point(4, 8);
			this.lblSyokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSyokei.Name = "lblSyokei";
			this.lblSyokei.Size = new System.Drawing.Size(136, 27);
			this.lblSyokei.TabIndex = 0;
			this.lblSyokei.Text = "小　　　計";
			this.lblSyokei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dgvInputList
			// 
			this.dgvInputList.AllowUserToAddRows = false;
			this.dgvInputList.AllowUserToDeleteRows = false;
			this.dgvInputList.AllowUserToResizeColumns = false;
			this.dgvInputList.AllowUserToResizeRows = false;
			this.dgvInputList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvInputList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GYO_BANGO,
            this.KUBUN,
            this.SHOHIN_CD,
            this.SHOHIN_NM,
            this.IRISU,
            this.TANI,
            this.KESUSU,
            this.BARA_SOSU,
            this.URI_TANKA,
            this.BAIKA_KINGAKU,
            this.SHOHIZEI,
            this.ZEI_RITSU,
            this.SHIWAKE_CD,
            this.BUMON_CD,
            this.ZEI_KUBUN,
            this.JIGYO_KUBUN,
            this.KUBUN_CD,
            this.GEN_TANKA,
            this.KAZEI_KUBUN});
			dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle14.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
			dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle14;
			this.dgvInputList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvInputList.EnableHeadersVisualStyles = false;
			this.dgvInputList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvInputList.Location = new System.Drawing.Point(8, 175);
			this.dgvInputList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.dgvInputList.MultiSelect = false;
			this.dgvInputList.Name = "dgvInputList";
			this.dgvInputList.RowHeadersVisible = false;
			this.dgvInputList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dgvInputList.RowTemplate.Height = 21;
			this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.dgvInputList.Size = new System.Drawing.Size(1101, 408);
			this.dgvInputList.TabIndex = 25;
			this.dgvInputList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellEnter);
			this.dgvInputList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvInputList_CellFormatting);
			this.dgvInputList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvInputList_CellMouseDown);
			this.dgvInputList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvInputList_Scroll);
			this.dgvInputList.Enter += new System.EventHandler(this.dgvInputList_Enter);
			// 
			// txtGridEdit
			// 
			this.txtGridEdit.AutoSizeFromLength = true;
			this.txtGridEdit.BackColor = System.Drawing.Color.White;
			this.txtGridEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtGridEdit.DisplayLength = null;
			this.txtGridEdit.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtGridEdit.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtGridEdit.Location = new System.Drawing.Point(525, 412);
			this.txtGridEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtGridEdit.MaxLength = 10;
			this.txtGridEdit.Name = "txtGridEdit";
			this.txtGridEdit.Size = new System.Drawing.Size(101, 20);
			this.txtGridEdit.TabIndex = 26;
			this.txtGridEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGridEdit.Enter += new System.EventHandler(this.txtGridEdit_Enter);
			this.txtGridEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGridEdit_KeyDown);
			// 
			// lblBeforeDenpyoNo
			// 
			this.lblBeforeDenpyoNo.BackColor = System.Drawing.Color.White;
			this.lblBeforeDenpyoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBeforeDenpyoNo.Location = new System.Drawing.Point(92, 57);
			this.lblBeforeDenpyoNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBeforeDenpyoNo.Name = "lblBeforeDenpyoNo";
			this.lblBeforeDenpyoNo.Size = new System.Drawing.Size(315, 27);
			this.lblBeforeDenpyoNo.TabIndex = 905;
			this.lblBeforeDenpyoNo.Text = "【前回登録伝票番号：】";
			this.lblBeforeDenpyoNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblBeforeDenpyoNo.Visible = false;
			// 
			// txtMizuageShishoCd
			// 
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new System.Drawing.Point(700, 56);
			this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtMizuageShishoCd.MaxLength = 5;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 20);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			// 
			// lblMizuageShishoNm
			// 
			this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMizuageShishoNm.Location = new System.Drawing.Point(747, 56);
			this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new System.Drawing.Size(312, 27);
			this.lblMizuageShishoNm.TabIndex = 908;
			this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMizuageShisho
			// 
			this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
			this.lblMizuageShisho.Location = new System.Drawing.Point(608, 53);
			this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new System.Drawing.Size(457, 33);
			this.lblMizuageShisho.TabIndex = 906;
			this.lblMizuageShisho.Text = "支　　所 ";
			this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pnlShohizei
			// 
			this.pnlShohizei.BackColor = System.Drawing.Color.Silver;
			this.pnlShohizei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlShohizei.Controls.Add(this.lblShohizeiTenka2);
			this.pnlShohizei.Controls.Add(this.lblShohizeiInputHoho2);
			this.pnlShohizei.Controls.Add(this.lblShohizeiTenka1);
			this.pnlShohizei.Controls.Add(this.lblShohizeiInputHoho1);
			this.pnlShohizei.Location = new System.Drawing.Point(523, 589);
			this.pnlShohizei.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pnlShohizei.Name = "pnlShohizei";
			this.pnlShohizei.Size = new System.Drawing.Size(303, 115);
			this.pnlShohizei.TabIndex = 909;
			// 
			// lblShohizeiTenka2
			// 
			this.lblShohizeiTenka2.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiTenka2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohizeiTenka2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiTenka2.Location = new System.Drawing.Point(4, 83);
			this.lblShohizeiTenka2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiTenka2.Name = "lblShohizeiTenka2";
			this.lblShohizeiTenka2.Size = new System.Drawing.Size(293, 27);
			this.lblShohizeiTenka2.TabIndex = 3;
			this.lblShohizeiTenka2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiInputHoho2
			// 
			this.lblShohizeiInputHoho2.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiInputHoho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohizeiInputHoho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiInputHoho2.Location = new System.Drawing.Point(4, 32);
			this.lblShohizeiInputHoho2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiInputHoho2.Name = "lblShohizeiInputHoho2";
			this.lblShohizeiInputHoho2.Size = new System.Drawing.Size(293, 27);
			this.lblShohizeiInputHoho2.TabIndex = 1;
			this.lblShohizeiInputHoho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiTenka1
			// 
			this.lblShohizeiTenka1.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiTenka1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
			this.lblShohizeiTenka1.Location = new System.Drawing.Point(4, 56);
			this.lblShohizeiTenka1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiTenka1.Name = "lblShohizeiTenka1";
			this.lblShohizeiTenka1.Size = new System.Drawing.Size(260, 27);
			this.lblShohizeiTenka1.TabIndex = 2;
			this.lblShohizeiTenka1.Text = "消費税転嫁方法";
			this.lblShohizeiTenka1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiInputHoho1
			// 
			this.lblShohizeiInputHoho1.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiInputHoho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
			this.lblShohizeiInputHoho1.Location = new System.Drawing.Point(4, 4);
			this.lblShohizeiInputHoho1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohizeiInputHoho1.Name = "lblShohizeiInputHoho1";
			this.lblShohizeiInputHoho1.Size = new System.Drawing.Size(260, 27);
			this.lblShohizeiInputHoho1.TabIndex = 0;
			this.lblShohizeiInputHoho1.Text = "消費税入力方法";
			this.lblShohizeiInputHoho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTaxInfo
			// 
			this.lblTaxInfo.BackColor = System.Drawing.Color.White;
			this.lblTaxInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTaxInfo.Location = new System.Drawing.Point(20, 589);
			this.lblTaxInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTaxInfo.Name = "lblTaxInfo";
			this.lblTaxInfo.Size = new System.Drawing.Size(481, 120);
			this.lblTaxInfo.TabIndex = 65542;
			this.lblTaxInfo.Text = "消費税内訳情報";
			// 
			// txtDummy
			// 
			this.txtDummy.AutoSizeFromLength = true;
			this.txtDummy.BackColor = System.Drawing.Color.White;
			this.txtDummy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtDummy.DisplayLength = null;
			this.txtDummy.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtDummy.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtDummy.Location = new System.Drawing.Point(975, 97);
			this.txtDummy.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtDummy.MaxLength = 10;
			this.txtDummy.Name = "txtDummy";
			this.txtDummy.Size = new System.Drawing.Size(0, 20);
			this.txtDummy.TabIndex = 65543;
			this.txtDummy.TabStop = false;
			this.txtDummy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblShohinNm
			// 
			this.lblShohinNm.BackColor = System.Drawing.Color.Silver;
			this.lblShohinNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohinNm.Location = new System.Drawing.Point(523, 713);
			this.lblShohinNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShohinNm.Name = "lblShohinNm";
			this.lblShohinNm.Size = new System.Drawing.Size(421, 27);
			this.lblShohinNm.TabIndex = 65544;
			this.lblShohinNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblShohinNm.Visible = false;
			// 
			// lblZaikoSu
			// 
			this.lblZaikoSu.BackColor = System.Drawing.Color.Silver;
			this.lblZaikoSu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblZaikoSu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblZaikoSu.Location = new System.Drawing.Point(945, 713);
			this.lblZaikoSu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblZaikoSu.Name = "lblZaikoSu";
			this.lblZaikoSu.Size = new System.Drawing.Size(157, 27);
			this.lblZaikoSu.TabIndex = 65545;
			this.lblZaikoSu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblZaikoSu.Visible = false;
			// 
			// lblAutoData
			// 
			this.lblAutoData.BackColor = System.Drawing.Color.White;
			this.lblAutoData.Font = new System.Drawing.Font("ＭＳ ゴシック", 10.5F, System.Drawing.FontStyle.Bold);
			this.lblAutoData.ForeColor = System.Drawing.Color.Blue;
			this.lblAutoData.Location = new System.Drawing.Point(409, 57);
			this.lblAutoData.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblAutoData.Name = "lblAutoData";
			this.lblAutoData.Size = new System.Drawing.Size(196, 27);
			this.lblAutoData.TabIndex = 65546;
			this.lblAutoData.Text = "財務へ連携済みです";
			this.lblAutoData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblAutoData.Visible = false;
			// 
			// GYO_BANGO
			// 
			dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
			this.GYO_BANGO.DefaultCellStyle = dataGridViewCellStyle2;
			this.GYO_BANGO.HeaderText = "行";
			this.GYO_BANGO.Name = "GYO_BANGO";
			this.GYO_BANGO.ReadOnly = true;
			this.GYO_BANGO.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.GYO_BANGO.Width = 25;
			// 
			// KUBUN
			// 
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
			this.KUBUN.DefaultCellStyle = dataGridViewCellStyle3;
			this.KUBUN.HeaderText = "区";
			this.KUBUN.Name = "KUBUN";
			this.KUBUN.ReadOnly = true;
			this.KUBUN.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.KUBUN.Width = 25;
			// 
			// SHOHIN_CD
			// 
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
			this.SHOHIN_CD.DefaultCellStyle = dataGridViewCellStyle4;
			this.SHOHIN_CD.HeaderText = "ｺｰﾄﾞ";
			this.SHOHIN_CD.Name = "SHOHIN_CD";
			this.SHOHIN_CD.ReadOnly = true;
			this.SHOHIN_CD.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.SHOHIN_CD.Width = 85;
			// 
			// SHOHIN_NM
			// 
			dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
			this.SHOHIN_NM.DefaultCellStyle = dataGridViewCellStyle5;
			this.SHOHIN_NM.HeaderText = "商品名";
			this.SHOHIN_NM.Name = "SHOHIN_NM";
			this.SHOHIN_NM.ReadOnly = true;
			this.SHOHIN_NM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.SHOHIN_NM.Width = 160;
			// 
			// IRISU
			// 
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
			this.IRISU.DefaultCellStyle = dataGridViewCellStyle6;
			this.IRISU.HeaderText = "入数";
			this.IRISU.Name = "IRISU";
			this.IRISU.ReadOnly = true;
			this.IRISU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.IRISU.Width = 80;
			// 
			// TANI
			// 
			dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
			this.TANI.DefaultCellStyle = dataGridViewCellStyle7;
			this.TANI.HeaderText = "単位";
			this.TANI.Name = "TANI";
			this.TANI.ReadOnly = true;
			this.TANI.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.TANI.Width = 80;
			// 
			// KESUSU
			// 
			dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
			this.KESUSU.DefaultCellStyle = dataGridViewCellStyle8;
			this.KESUSU.HeaderText = "ｹｰｽ数";
			this.KESUSU.Name = "KESUSU";
			this.KESUSU.ReadOnly = true;
			this.KESUSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.KESUSU.Width = 80;
			// 
			// BARA_SOSU
			// 
			dataGridViewCellStyle9.NullValue = null;
			dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
			this.BARA_SOSU.DefaultCellStyle = dataGridViewCellStyle9;
			this.BARA_SOSU.HeaderText = "ﾊﾞﾗ数";
			this.BARA_SOSU.Name = "BARA_SOSU";
			this.BARA_SOSU.ReadOnly = true;
			this.BARA_SOSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.BARA_SOSU.Width = 80;
			// 
			// URI_TANKA
			// 
			dataGridViewCellStyle10.NullValue = null;
			dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
			this.URI_TANKA.DefaultCellStyle = dataGridViewCellStyle10;
			this.URI_TANKA.HeaderText = "単価";
			this.URI_TANKA.Name = "URI_TANKA";
			this.URI_TANKA.ReadOnly = true;
			this.URI_TANKA.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.URI_TANKA.Width = 80;
			// 
			// BAIKA_KINGAKU
			// 
			dataGridViewCellStyle11.NullValue = null;
			dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
			this.BAIKA_KINGAKU.DefaultCellStyle = dataGridViewCellStyle11;
			this.BAIKA_KINGAKU.HeaderText = "金額";
			this.BAIKA_KINGAKU.Name = "BAIKA_KINGAKU";
			this.BAIKA_KINGAKU.ReadOnly = true;
			this.BAIKA_KINGAKU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			// 
			// SHOHIZEI
			// 
			dataGridViewCellStyle12.NullValue = null;
			dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
			this.SHOHIZEI.DefaultCellStyle = dataGridViewCellStyle12;
			this.SHOHIZEI.HeaderText = "消費税";
			this.SHOHIZEI.Name = "SHOHIZEI";
			this.SHOHIZEI.ReadOnly = true;
			this.SHOHIZEI.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.SHOHIZEI.Width = 90;
			// 
			// ZEI_RITSU
			// 
			dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
			this.ZEI_RITSU.DefaultCellStyle = dataGridViewCellStyle13;
			this.ZEI_RITSU.HeaderText = "率";
			this.ZEI_RITSU.Name = "ZEI_RITSU";
			this.ZEI_RITSU.ReadOnly = true;
			this.ZEI_RITSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.ZEI_RITSU.Width = 39;
			// 
			// SHIWAKE_CD
			// 
			this.SHIWAKE_CD.HeaderText = "仕訳コード";
			this.SHIWAKE_CD.Name = "SHIWAKE_CD";
			this.SHIWAKE_CD.Visible = false;
			// 
			// BUMON_CD
			// 
			this.BUMON_CD.HeaderText = "部門コード";
			this.BUMON_CD.Name = "BUMON_CD";
			this.BUMON_CD.Visible = false;
			// 
			// ZEI_KUBUN
			// 
			this.ZEI_KUBUN.HeaderText = "税区分";
			this.ZEI_KUBUN.Name = "ZEI_KUBUN";
			this.ZEI_KUBUN.Visible = false;
			// 
			// JIGYO_KUBUN
			// 
			this.JIGYO_KUBUN.HeaderText = "事業区分";
			this.JIGYO_KUBUN.Name = "JIGYO_KUBUN";
			this.JIGYO_KUBUN.Visible = false;
			// 
			// KUBUN_CD
			// 
			this.KUBUN_CD.HeaderText = "区ｺｰﾄﾞ";
			this.KUBUN_CD.Name = "KUBUN_CD";
			this.KUBUN_CD.Visible = false;
			// 
			// GEN_TANKA
			// 
			this.GEN_TANKA.HeaderText = "原単価";
			this.GEN_TANKA.Name = "GEN_TANKA";
			this.GEN_TANKA.Visible = false;
			// 
			// KAZEI_KUBUN
			// 
			this.KAZEI_KUBUN.HeaderText = "課税区分";
			this.KAZEI_KUBUN.Name = "KAZEI_KUBUN";
			this.KAZEI_KUBUN.Visible = false;
			// 
			// KBDE1011
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1119, 745);
			this.Controls.Add(this.lblAutoData);
			this.Controls.Add(this.lblZaikoSu);
			this.Controls.Add(this.txtDummy);
			this.Controls.Add(this.lblShohinNm);
			this.Controls.Add(this.lblTaxInfo);
			this.Controls.Add(this.pnlShohizei);
			this.Controls.Add(this.txtMizuageShishoCd);
			this.Controls.Add(this.lblMizuageShishoNm);
			this.Controls.Add(this.lblMizuageShisho);
			this.Controls.Add(this.lblBeforeDenpyoNo);
			this.Controls.Add(this.txtGridEdit);
			this.Controls.Add(this.pnlDenpyoSyukei);
			this.Controls.Add(this.dgvInputList);
			this.Controls.Add(this.lblTorihikiKubunNm);
			this.Controls.Add(this.txtTorihikiKubunCd);
			this.Controls.Add(this.lblTorihikiKubunCd);
			this.Controls.Add(this.lblDay);
			this.Controls.Add(this.lblMonth);
			this.Controls.Add(this.txtDay);
			this.Controls.Add(this.lblYear);
			this.Controls.Add(this.txtMonth);
			this.Controls.Add(this.txtGengoYear);
			this.Controls.Add(this.lblGengo);
			this.Controls.Add(this.lblJp);
			this.Controls.Add(this.txtDenpyoNo);
			this.Controls.Add(this.lblDenpyoNo);
			this.Controls.Add(this.lblFunanushiNm);
			this.Controls.Add(this.txtFunanushiCd);
			this.Controls.Add(this.lblFunanushiCd);
			this.Controls.Add(this.lblTantoshaNm);
			this.Controls.Add(this.txtTantoshaCd);
			this.Controls.Add(this.lblTantoshaCd);
			this.Controls.Add(this.lblMode);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
			this.Name = "KBDE1011";
			this.Text = "購買売上入力";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.KBDE1011_FormClosing);
			this.Controls.SetChildIndex(this.lblMode, 0);
			this.Controls.SetChildIndex(this.lblTantoshaCd, 0);
			this.Controls.SetChildIndex(this.txtTantoshaCd, 0);
			this.Controls.SetChildIndex(this.lblTantoshaNm, 0);
			this.Controls.SetChildIndex(this.lblFunanushiCd, 0);
			this.Controls.SetChildIndex(this.txtFunanushiCd, 0);
			this.Controls.SetChildIndex(this.lblFunanushiNm, 0);
			this.Controls.SetChildIndex(this.lblDenpyoNo, 0);
			this.Controls.SetChildIndex(this.txtDenpyoNo, 0);
			this.Controls.SetChildIndex(this.lblJp, 0);
			this.Controls.SetChildIndex(this.lblGengo, 0);
			this.Controls.SetChildIndex(this.txtGengoYear, 0);
			this.Controls.SetChildIndex(this.txtMonth, 0);
			this.Controls.SetChildIndex(this.lblYear, 0);
			this.Controls.SetChildIndex(this.txtDay, 0);
			this.Controls.SetChildIndex(this.lblMonth, 0);
			this.Controls.SetChildIndex(this.lblDay, 0);
			this.Controls.SetChildIndex(this.lblTorihikiKubunCd, 0);
			this.Controls.SetChildIndex(this.txtTorihikiKubunCd, 0);
			this.Controls.SetChildIndex(this.lblTorihikiKubunNm, 0);
			this.Controls.SetChildIndex(this.dgvInputList, 0);
			this.Controls.SetChildIndex(this.pnlDenpyoSyukei, 0);
			this.Controls.SetChildIndex(this.txtGridEdit, 0);
			this.Controls.SetChildIndex(this.lblBeforeDenpyoNo, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.lblMizuageShisho, 0);
			this.Controls.SetChildIndex(this.lblMizuageShishoNm, 0);
			this.Controls.SetChildIndex(this.txtMizuageShishoCd, 0);
			this.Controls.SetChildIndex(this.pnlShohizei, 0);
			this.Controls.SetChildIndex(this.lblTaxInfo, 0);
			this.Controls.SetChildIndex(this.lblShohinNm, 0);
			this.Controls.SetChildIndex(this.txtDummy, 0);
			this.Controls.SetChildIndex(this.lblZaikoSu, 0);
			this.Controls.SetChildIndex(this.lblAutoData, 0);
			this.pnlDebug.ResumeLayout(false);
			this.pnlDenpyoSyukei.ResumeLayout(false);
			this.pnlDenpyoSyukei.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
			this.pnlShohizei.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblTantoshaCd;
        private System.Windows.Forms.Label lblFunanushiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtDenpyoNo;
        private System.Windows.Forms.Label lblDenpyoNo;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDay;
        private System.Windows.Forms.Label lblYear;
        private jp.co.fsi.common.controls.FsiTextBox txtMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYear;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblTorihikiKubunNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTorihikiKubunCd;
        private System.Windows.Forms.Label lblTorihikiKubunCd;
        private System.Windows.Forms.DataGridView dgvInputList;
        private System.Windows.Forms.Panel pnlDenpyoSyukei;
        private jp.co.fsi.common.controls.FsiTextBox txtTotalGaku;
        private jp.co.fsi.common.controls.FsiTextBox txtSyohiZei;
        private jp.co.fsi.common.controls.FsiTextBox txtSyokei;
        private System.Windows.Forms.Label lblTotalGaku;
        private System.Windows.Forms.Label lblSyohiZei;
        private System.Windows.Forms.Label lblSyokei;
        private jp.co.fsi.common.controls.FsiTextBox txtGridEdit;
        private System.Windows.Forms.Label lblBeforeDenpyoNo;
        private jp.co.fsi.common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Panel pnlShohizei;
        private System.Windows.Forms.Label lblShohizeiTenka2;
        private System.Windows.Forms.Label lblShohizeiInputHoho2;
        private System.Windows.Forms.Label lblShohizeiTenka1;
        private System.Windows.Forms.Label lblShohizeiInputHoho1;
        private System.Windows.Forms.Label lblTaxInfo;
        private jp.co.fsi.common.controls.FsiTextBox txtDummy;
        private System.Windows.Forms.Label lblZaikoSu;
        private System.Windows.Forms.Label lblShohinNm;
        private System.Windows.Forms.Label lblAutoData;
		private System.Windows.Forms.DataGridViewTextBoxColumn GYO_BANGO;
		private System.Windows.Forms.DataGridViewTextBoxColumn KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIN_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIN_NM;
		private System.Windows.Forms.DataGridViewTextBoxColumn IRISU;
		private System.Windows.Forms.DataGridViewTextBoxColumn TANI;
		private System.Windows.Forms.DataGridViewTextBoxColumn KESUSU;
		private System.Windows.Forms.DataGridViewTextBoxColumn BARA_SOSU;
		private System.Windows.Forms.DataGridViewTextBoxColumn URI_TANKA;
		private System.Windows.Forms.DataGridViewTextBoxColumn BAIKA_KINGAKU;
		private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIZEI;
		private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_RITSU;
		private System.Windows.Forms.DataGridViewTextBoxColumn SHIWAKE_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn BUMON_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn JIGYO_KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn KUBUN_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn GEN_TANKA;
		private System.Windows.Forms.DataGridViewTextBoxColumn KAZEI_KUBUN;
	}
}