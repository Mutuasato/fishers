﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbde1011
{
    /// <summary>
    /// KBDE1013R の概要の説明です。
    /// </summary>
    public partial class KBDE10134R : BaseReport
    {

        public KBDE10134R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
