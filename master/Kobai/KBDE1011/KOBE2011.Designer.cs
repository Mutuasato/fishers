﻿namespace jp.co.sosok.erp.kob.kobe2011
{
    partial class KOBE2011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblMode = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.txtTantoshaCd = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.lblTantoshaCd = new System.Windows.Forms.Label();
            this.lblFunanushiNm = new System.Windows.Forms.Label();
            this.txtFunanushiCd = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.lblFunanushiCd = new System.Windows.Forms.Label();
            this.txtDenpyoNo = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.lblDenpyoNo = new System.Windows.Forms.Label();
            this.lblDay = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtMonth = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.txtGengoYear = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.lblEraBackFr = new System.Windows.Forms.Label();
            this.lblDenpyoDate = new System.Windows.Forms.Label();
            this.lblTorihikiKubunNm = new System.Windows.Forms.Label();
            this.txtTorihikiKubunCd = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.lblTorihikiKubunCd = new System.Windows.Forms.Label();
            this.pnlDenpyoSyukei = new System.Windows.Forms.Panel();
            this.txtTotalGaku = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.txtSyohiZei = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.txtSyokei = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.lblTotalGaku = new System.Windows.Forms.Label();
            this.lblSyohiZei = new System.Windows.Forms.Label();
            this.lblSyokei = new System.Windows.Forms.Label();
            this.dgvInputList = new System.Windows.Forms.DataGridView();
            this.txtGridEdit = new jp.co.sosok.erp.common.controls.SosTextBox();
            this.lblBeforeDenpyoNo = new System.Windows.Forms.Label();
            this.GYO_BANGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHOHIN_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHOHIN_NM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IRISU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TANI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KESUSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BARA_SOSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.URI_TANKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BAIKA_KINGAKU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHOHIZEI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHIWAKE_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BUMON_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JIGYO_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZEI_RITSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KUBUN_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GEN_TANKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KAZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDebug.SuspendLayout();
            this.pnlDenpyoSyukei.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "購買売上入力";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblMode
            // 
            this.lblMode.BackColor = System.Drawing.Color.Transparent;
            this.lblMode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMode.Location = new System.Drawing.Point(1, 45);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(83, 20);
            this.lblMode.TabIndex = 1;
            this.lblMode.Text = "【登録】";
            this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNm.Location = new System.Drawing.Point(476, 95);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(234, 20);
            this.lblTantoshaNm.TabIndex = 24;
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = true;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoshaCd.Location = new System.Drawing.Point(441, 95);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.Size = new System.Drawing.Size(34, 20);
            this.txtTantoshaCd.TabIndex = 23;
            this.txtTantoshaCd.Text = "0";
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
            // 
            // lblTantoshaCd
            // 
            this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaCd.Location = new System.Drawing.Point(369, 95);
            this.lblTantoshaCd.Name = "lblTantoshaCd";
            this.lblTantoshaCd.Size = new System.Drawing.Size(72, 20);
            this.lblTantoshaCd.TabIndex = 22;
            this.lblTantoshaCd.Text = "担 当 者";
            this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiNm
            // 
            this.lblFunanushiNm.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiNm.Location = new System.Drawing.Point(133, 95);
            this.lblFunanushiNm.Name = "lblFunanushiNm";
            this.lblFunanushiNm.Size = new System.Drawing.Size(234, 20);
            this.lblFunanushiNm.TabIndex = 18;
            this.lblFunanushiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCd
            // 
            this.txtFunanushiCd.AutoSizeFromLength = true;
            this.txtFunanushiCd.DisplayLength = null;
            this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFunanushiCd.Location = new System.Drawing.Point(84, 95);
            this.txtFunanushiCd.MaxLength = 4;
            this.txtFunanushiCd.Name = "txtFunanushiCd";
            this.txtFunanushiCd.Size = new System.Drawing.Size(48, 20);
            this.txtFunanushiCd.TabIndex = 17;
            this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCd_Validating);
            // 
            // lblFunanushiCd
            // 
            this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCd.Location = new System.Drawing.Point(12, 95);
            this.lblFunanushiCd.Name = "lblFunanushiCd";
            this.lblFunanushiCd.Size = new System.Drawing.Size(72, 20);
            this.lblFunanushiCd.TabIndex = 16;
            this.lblFunanushiCd.Text = "船主CD";
            this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDenpyoNo
            // 
            this.txtDenpyoNo.AutoSizeFromLength = true;
            this.txtDenpyoNo.DisplayLength = null;
            this.txtDenpyoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenpyoNo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDenpyoNo.Location = new System.Drawing.Point(84, 73);
            this.txtDenpyoNo.MaxLength = 8;
            this.txtDenpyoNo.Name = "txtDenpyoNo";
            this.txtDenpyoNo.Size = new System.Drawing.Size(48, 20);
            this.txtDenpyoNo.TabIndex = 3;
            this.txtDenpyoNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDenpyoNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenpyoNo_Validating);
            // 
            // lblDenpyoNo
            // 
            this.lblDenpyoNo.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoNo.Location = new System.Drawing.Point(12, 71);
            this.lblDenpyoNo.Name = "lblDenpyoNo";
            this.lblDenpyoNo.Size = new System.Drawing.Size(72, 24);
            this.lblDenpyoNo.TabIndex = 2;
            this.lblDenpyoNo.Text = "伝票番号";
            this.lblDenpyoNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDay
            // 
            this.lblDay.AutoSize = true;
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDay.Location = new System.Drawing.Point(432, 76);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(21, 13);
            this.lblDay.TabIndex = 12;
            this.lblDay.Text = "日";
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.Location = new System.Drawing.Point(363, 76);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(21, 13);
            this.lblMonth.TabIndex = 10;
            this.lblMonth.Text = "月";
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = false;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDay.Location = new System.Drawing.Point(386, 72);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(40, 20);
            this.txtDay.TabIndex = 11;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(296, 76);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(21, 13);
            this.lblYear.TabIndex = 8;
            this.lblYear.Text = "年";
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.Location = new System.Drawing.Point(319, 72);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(40, 20);
            this.txtMonth.TabIndex = 9;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // txtGengoYear
            // 
            this.txtGengoYear.AutoSizeFromLength = false;
            this.txtGengoYear.DisplayLength = null;
            this.txtGengoYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYear.Location = new System.Drawing.Point(252, 72);
            this.txtGengoYear.MaxLength = 2;
            this.txtGengoYear.Name = "txtGengoYear";
            this.txtGengoYear.Size = new System.Drawing.Size(40, 20);
            this.txtGengoYear.TabIndex = 7;
            this.txtGengoYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYear_Validating);
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.Silver;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengo.Location = new System.Drawing.Point(207, 72);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(40, 20);
            this.lblGengo.TabIndex = 5;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblEraBackFr
            // 
            this.lblEraBackFr.BackColor = System.Drawing.Color.Silver;
            this.lblEraBackFr.Location = new System.Drawing.Point(204, 70);
            this.lblEraBackFr.Name = "lblEraBackFr";
            this.lblEraBackFr.Size = new System.Drawing.Size(271, 24);
            this.lblEraBackFr.TabIndex = 6;
            this.lblEraBackFr.Text = " ";
            // 
            // lblDenpyoDate
            // 
            this.lblDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDate.Location = new System.Drawing.Point(133, 71);
            this.lblDenpyoDate.Name = "lblDenpyoDate";
            this.lblDenpyoDate.Size = new System.Drawing.Size(72, 24);
            this.lblDenpyoDate.TabIndex = 4;
            this.lblDenpyoDate.Text = "伝票日付";
            this.lblDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTorihikiKubunNm
            // 
            this.lblTorihikiKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikiKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikiKubunNm.Location = new System.Drawing.Point(583, 70);
            this.lblTorihikiKubunNm.Name = "lblTorihikiKubunNm";
            this.lblTorihikiKubunNm.Size = new System.Drawing.Size(127, 24);
            this.lblTorihikiKubunNm.TabIndex = 15;
            this.lblTorihikiKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTorihikiKubunCd
            // 
            this.txtTorihikiKubunCd.AutoSizeFromLength = true;
            this.txtTorihikiKubunCd.DisplayLength = null;
            this.txtTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTorihikiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTorihikiKubunCd.Location = new System.Drawing.Point(548, 72);
            this.txtTorihikiKubunCd.MaxLength = 4;
            this.txtTorihikiKubunCd.Name = "txtTorihikiKubunCd";
            this.txtTorihikiKubunCd.Size = new System.Drawing.Size(34, 20);
            this.txtTorihikiKubunCd.TabIndex = 14;
            this.txtTorihikiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikiKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikiKubunCd_Validating);
            // 
            // lblTorihikiKubunCd
            // 
            this.lblTorihikiKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikiKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikiKubunCd.Location = new System.Drawing.Point(476, 70);
            this.lblTorihikiKubunCd.Name = "lblTorihikiKubunCd";
            this.lblTorihikiKubunCd.Size = new System.Drawing.Size(72, 25);
            this.lblTorihikiKubunCd.TabIndex = 13;
            this.lblTorihikiKubunCd.Text = "取引区分";
            this.lblTorihikiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlDenpyoSyukei
            // 
            this.pnlDenpyoSyukei.BackColor = System.Drawing.Color.Silver;
            this.pnlDenpyoSyukei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlDenpyoSyukei.Controls.Add(this.txtTotalGaku);
            this.pnlDenpyoSyukei.Controls.Add(this.txtSyohiZei);
            this.pnlDenpyoSyukei.Controls.Add(this.txtSyokei);
            this.pnlDenpyoSyukei.Controls.Add(this.lblTotalGaku);
            this.pnlDenpyoSyukei.Controls.Add(this.lblSyohiZei);
            this.pnlDenpyoSyukei.Controls.Add(this.lblSyokei);
            this.pnlDenpyoSyukei.Location = new System.Drawing.Point(624, 442);
            this.pnlDenpyoSyukei.Name = "pnlDenpyoSyukei";
            this.pnlDenpyoSyukei.Size = new System.Drawing.Size(203, 87);
            this.pnlDenpyoSyukei.TabIndex = 27;
            // 
            // txtTotalGaku
            // 
            this.txtTotalGaku.AutoSizeFromLength = true;
            this.txtTotalGaku.DisplayLength = null;
            this.txtTotalGaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTotalGaku.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTotalGaku.Location = new System.Drawing.Point(106, 56);
            this.txtTotalGaku.MaxLength = 12;
            this.txtTotalGaku.Name = "txtTotalGaku";
            this.txtTotalGaku.ReadOnly = true;
            this.txtTotalGaku.Size = new System.Drawing.Size(90, 20);
            this.txtTotalGaku.TabIndex = 5;
            this.txtTotalGaku.TabStop = false;
            this.txtTotalGaku.Text = "0";
            this.txtTotalGaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSyohiZei
            // 
            this.txtSyohiZei.AutoSizeFromLength = true;
            this.txtSyohiZei.DisplayLength = null;
            this.txtSyohiZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSyohiZei.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSyohiZei.Location = new System.Drawing.Point(106, 31);
            this.txtSyohiZei.MaxLength = 12;
            this.txtSyohiZei.Name = "txtSyohiZei";
            this.txtSyohiZei.ReadOnly = true;
            this.txtSyohiZei.Size = new System.Drawing.Size(90, 20);
            this.txtSyohiZei.TabIndex = 3;
            this.txtSyohiZei.TabStop = false;
            this.txtSyohiZei.Text = "0";
            this.txtSyohiZei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSyokei
            // 
            this.txtSyokei.AutoSizeFromLength = true;
            this.txtSyokei.DisplayLength = null;
            this.txtSyokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSyokei.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSyokei.Location = new System.Drawing.Point(106, 6);
            this.txtSyokei.MaxLength = 12;
            this.txtSyokei.Name = "txtSyokei";
            this.txtSyokei.ReadOnly = true;
            this.txtSyokei.Size = new System.Drawing.Size(90, 20);
            this.txtSyokei.TabIndex = 1;
            this.txtSyokei.Text = "0";
            this.txtSyokei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalGaku
            // 
            this.lblTotalGaku.BackColor = System.Drawing.Color.Silver;
            this.lblTotalGaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTotalGaku.Location = new System.Drawing.Point(3, 56);
            this.lblTotalGaku.Name = "lblTotalGaku";
            this.lblTotalGaku.Size = new System.Drawing.Size(102, 22);
            this.lblTotalGaku.TabIndex = 4;
            this.lblTotalGaku.Text = "合　計　額";
            this.lblTotalGaku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSyohiZei
            // 
            this.lblSyohiZei.BackColor = System.Drawing.Color.Silver;
            this.lblSyohiZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSyohiZei.Location = new System.Drawing.Point(3, 28);
            this.lblSyohiZei.Name = "lblSyohiZei";
            this.lblSyohiZei.Size = new System.Drawing.Size(102, 26);
            this.lblSyohiZei.TabIndex = 2;
            this.lblSyohiZei.Text = "消　費　税";
            this.lblSyohiZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSyokei
            // 
            this.lblSyokei.BackColor = System.Drawing.Color.Silver;
            this.lblSyokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSyokei.Location = new System.Drawing.Point(3, 5);
            this.lblSyokei.Name = "lblSyokei";
            this.lblSyokei.Size = new System.Drawing.Size(102, 22);
            this.lblSyokei.TabIndex = 0;
            this.lblSyokei.Text = "小　　　計";
            this.lblSyokei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvInputList
            // 
            this.dgvInputList.AllowUserToAddRows = false;
            this.dgvInputList.AllowUserToDeleteRows = false;
            this.dgvInputList.AllowUserToResizeColumns = false;
            this.dgvInputList.AllowUserToResizeRows = false;
            this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GYO_BANGO,
            this.KUBUN,
            this.SHOHIN_CD,
            this.SHOHIN_NM,
            this.IRISU,
            this.TANI,
            this.KESUSU,
            this.BARA_SOSU,
            this.URI_TANKA,
            this.BAIKA_KINGAKU,
            this.SHOHIZEI,
            this.SHIWAKE_CD,
            this.BUMON_CD,
            this.ZEI_KUBUN,
            this.JIGYO_KUBUN,
            this.ZEI_RITSU,
            this.KUBUN_CD,
            this.GEN_TANKA,
            this.KAZEI_KUBUN});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvInputList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvInputList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvInputList.Location = new System.Drawing.Point(12, 131);
            this.dgvInputList.MultiSelect = false;
            this.dgvInputList.Name = "dgvInputList";
            this.dgvInputList.RowHeadersVisible = false;
            this.dgvInputList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInputList.RowTemplate.Height = 21;
            this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInputList.Size = new System.Drawing.Size(815, 306);
            this.dgvInputList.TabIndex = 25;
            this.dgvInputList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellEnter);
            this.dgvInputList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvInputList_Scroll);
            this.dgvInputList.Enter += new System.EventHandler(this.dgvInputList_Enter);
            // 
            // txtGridEdit
            // 
            this.txtGridEdit.AutoSizeFromLength = true;
            this.txtGridEdit.BackColor = System.Drawing.Color.White;
            this.txtGridEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGridEdit.DisplayLength = null;
            this.txtGridEdit.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGridEdit.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGridEdit.Location = new System.Drawing.Point(394, 309);
            this.txtGridEdit.MaxLength = 10;
            this.txtGridEdit.Name = "txtGridEdit";
            this.txtGridEdit.Size = new System.Drawing.Size(76, 20);
            this.txtGridEdit.TabIndex = 26;
            this.txtGridEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGridEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGridEdit_KeyDown);
            // 
            // lblBeforeDenpyoNo
            // 
            this.lblBeforeDenpyoNo.BackColor = System.Drawing.Color.White;
            this.lblBeforeDenpyoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBeforeDenpyoNo.Location = new System.Drawing.Point(90, 45);
            this.lblBeforeDenpyoNo.Name = "lblBeforeDenpyoNo";
            this.lblBeforeDenpyoNo.Size = new System.Drawing.Size(342, 20);
            this.lblBeforeDenpyoNo.TabIndex = 905;
            this.lblBeforeDenpyoNo.Text = "【前回登録伝票番号：】";
            this.lblBeforeDenpyoNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBeforeDenpyoNo.Visible = false;
            // 
            // GYO_BANGO
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.GYO_BANGO.DefaultCellStyle = dataGridViewCellStyle1;
            this.GYO_BANGO.HeaderText = "行";
            this.GYO_BANGO.Name = "GYO_BANGO";
            this.GYO_BANGO.ReadOnly = true;
            this.GYO_BANGO.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.GYO_BANGO.Width = 25;
            // 
            // KUBUN
            // 
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.KUBUN.DefaultCellStyle = dataGridViewCellStyle2;
            this.KUBUN.HeaderText = "区";
            this.KUBUN.Name = "KUBUN";
            this.KUBUN.ReadOnly = true;
            this.KUBUN.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KUBUN.Width = 25;
            // 
            // SHOHIN_CD
            // 
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.SHOHIN_CD.DefaultCellStyle = dataGridViewCellStyle3;
            this.SHOHIN_CD.HeaderText = "ｺｰﾄﾞ";
            this.SHOHIN_CD.Name = "SHOHIN_CD";
            this.SHOHIN_CD.ReadOnly = true;
            this.SHOHIN_CD.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SHOHIN_CD.Width = 90;
            // 
            // SHOHIN_NM
            // 
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.SHOHIN_NM.DefaultCellStyle = dataGridViewCellStyle4;
            this.SHOHIN_NM.HeaderText = "商品名";
            this.SHOHIN_NM.Name = "SHOHIN_NM";
            this.SHOHIN_NM.ReadOnly = true;
            this.SHOHIN_NM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SHOHIN_NM.Width = 165;
            // 
            // IRISU
            // 
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.IRISU.DefaultCellStyle = dataGridViewCellStyle5;
            this.IRISU.HeaderText = "入数";
            this.IRISU.Name = "IRISU";
            this.IRISU.ReadOnly = true;
            this.IRISU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IRISU.Width = 55;
            // 
            // TANI
            // 
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.TANI.DefaultCellStyle = dataGridViewCellStyle6;
            this.TANI.HeaderText = "単位";
            this.TANI.Name = "TANI";
            this.TANI.ReadOnly = true;
            this.TANI.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TANI.Width = 55;
            // 
            // KESUSU
            // 
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.KESUSU.DefaultCellStyle = dataGridViewCellStyle7;
            this.KESUSU.HeaderText = "ｹｰｽ数";
            this.KESUSU.Name = "KESUSU";
            this.KESUSU.ReadOnly = true;
            this.KESUSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KESUSU.Width = 63;
            // 
            // BARA_SOSU
            // 
            dataGridViewCellStyle8.NullValue = null;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.BARA_SOSU.DefaultCellStyle = dataGridViewCellStyle8;
            this.BARA_SOSU.HeaderText = "ﾊﾞﾗ数";
            this.BARA_SOSU.Name = "BARA_SOSU";
            this.BARA_SOSU.ReadOnly = true;
            this.BARA_SOSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BARA_SOSU.Width = 75;
            // 
            // URI_TANKA
            // 
            dataGridViewCellStyle9.NullValue = null;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.URI_TANKA.DefaultCellStyle = dataGridViewCellStyle9;
            this.URI_TANKA.HeaderText = "単価";
            this.URI_TANKA.Name = "URI_TANKA";
            this.URI_TANKA.ReadOnly = true;
            this.URI_TANKA.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.URI_TANKA.Width = 80;
            // 
            // BAIKA_KINGAKU
            // 
            dataGridViewCellStyle10.NullValue = null;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.BAIKA_KINGAKU.DefaultCellStyle = dataGridViewCellStyle10;
            this.BAIKA_KINGAKU.HeaderText = "金額";
            this.BAIKA_KINGAKU.Name = "BAIKA_KINGAKU";
            this.BAIKA_KINGAKU.ReadOnly = true;
            this.BAIKA_KINGAKU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BAIKA_KINGAKU.Width = 87;
            // 
            // SHOHIZEI
            // 
            dataGridViewCellStyle11.NullValue = null;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.SHOHIZEI.DefaultCellStyle = dataGridViewCellStyle11;
            this.SHOHIZEI.HeaderText = "消費税";
            this.SHOHIZEI.Name = "SHOHIZEI";
            this.SHOHIZEI.ReadOnly = true;
            this.SHOHIZEI.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SHOHIZEI.Width = 85;
            // 
            // SHIWAKE_CD
            // 
            this.SHIWAKE_CD.HeaderText = "仕訳コード";
            this.SHIWAKE_CD.Name = "SHIWAKE_CD";
            this.SHIWAKE_CD.Visible = false;
            // 
            // BUMON_CD
            // 
            this.BUMON_CD.HeaderText = "部門コード";
            this.BUMON_CD.Name = "BUMON_CD";
            this.BUMON_CD.Visible = false;
            // 
            // ZEI_KUBUN
            // 
            this.ZEI_KUBUN.HeaderText = "税区分";
            this.ZEI_KUBUN.Name = "ZEI_KUBUN";
            this.ZEI_KUBUN.Visible = false;
            // 
            // JIGYO_KUBUN
            // 
            this.JIGYO_KUBUN.HeaderText = "事業区分";
            this.JIGYO_KUBUN.Name = "JIGYO_KUBUN";
            this.JIGYO_KUBUN.Visible = false;
            // 
            // ZEI_RITSU
            // 
            this.ZEI_RITSU.HeaderText = "税率";
            this.ZEI_RITSU.Name = "ZEI_RITSU";
            this.ZEI_RITSU.Visible = false;
            // 
            // KUBUN_CD
            // 
            this.KUBUN_CD.HeaderText = "区ｺｰﾄﾞ";
            this.KUBUN_CD.Name = "KUBUN_CD";
            this.KUBUN_CD.Visible = false;
            // 
            // GEN_TANKA
            // 
            this.GEN_TANKA.HeaderText = "原単価";
            this.GEN_TANKA.Name = "GEN_TANKA";
            this.GEN_TANKA.Visible = false;
            // 
            // KAZEI_KUBUN
            // 
            this.KAZEI_KUBUN.HeaderText = "課税区分";
            this.KAZEI_KUBUN.Name = "KAZEI_KUBUN";
            this.KAZEI_KUBUN.Visible = false;
            // 
            // KOBE2011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblBeforeDenpyoNo);
            this.Controls.Add(this.txtGridEdit);
            this.Controls.Add(this.pnlDenpyoSyukei);
            this.Controls.Add(this.dgvInputList);
            this.Controls.Add(this.lblTorihikiKubunNm);
            this.Controls.Add(this.txtTorihikiKubunCd);
            this.Controls.Add(this.lblTorihikiKubunCd);
            this.Controls.Add(this.lblDenpyoDate);
            this.Controls.Add(this.lblDay);
            this.Controls.Add(this.lblMonth);
            this.Controls.Add(this.txtDay);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.txtMonth);
            this.Controls.Add(this.txtGengoYear);
            this.Controls.Add(this.lblGengo);
            this.Controls.Add(this.lblEraBackFr);
            this.Controls.Add(this.txtDenpyoNo);
            this.Controls.Add(this.lblDenpyoNo);
            this.Controls.Add(this.lblFunanushiNm);
            this.Controls.Add(this.txtFunanushiCd);
            this.Controls.Add(this.lblFunanushiCd);
            this.Controls.Add(this.lblTantoshaNm);
            this.Controls.Add(this.txtTantoshaCd);
            this.Controls.Add(this.lblTantoshaCd);
            this.Controls.Add(this.lblMode);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KOBE2011";
            this.Text = "購買売上入力";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.KOBE2011_FormClosing);
            this.Controls.SetChildIndex(this.lblMode, 0);
            this.Controls.SetChildIndex(this.lblTantoshaCd, 0);
            this.Controls.SetChildIndex(this.txtTantoshaCd, 0);
            this.Controls.SetChildIndex(this.lblTantoshaNm, 0);
            this.Controls.SetChildIndex(this.lblFunanushiCd, 0);
            this.Controls.SetChildIndex(this.txtFunanushiCd, 0);
            this.Controls.SetChildIndex(this.lblFunanushiNm, 0);
            this.Controls.SetChildIndex(this.lblDenpyoNo, 0);
            this.Controls.SetChildIndex(this.txtDenpyoNo, 0);
            this.Controls.SetChildIndex(this.lblEraBackFr, 0);
            this.Controls.SetChildIndex(this.lblGengo, 0);
            this.Controls.SetChildIndex(this.txtGengoYear, 0);
            this.Controls.SetChildIndex(this.txtMonth, 0);
            this.Controls.SetChildIndex(this.lblYear, 0);
            this.Controls.SetChildIndex(this.txtDay, 0);
            this.Controls.SetChildIndex(this.lblMonth, 0);
            this.Controls.SetChildIndex(this.lblDay, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblTorihikiKubunCd, 0);
            this.Controls.SetChildIndex(this.txtTorihikiKubunCd, 0);
            this.Controls.SetChildIndex(this.lblTorihikiKubunNm, 0);
            this.Controls.SetChildIndex(this.dgvInputList, 0);
            this.Controls.SetChildIndex(this.pnlDenpyoSyukei, 0);
            this.Controls.SetChildIndex(this.txtGridEdit, 0);
            this.Controls.SetChildIndex(this.lblBeforeDenpyoNo, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlDenpyoSyukei.ResumeLayout(false);
            this.pnlDenpyoSyukei.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label lblTantoshaNm;
        private common.controls.SosTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblTantoshaCd;
        private System.Windows.Forms.Label lblFunanushiNm;
        private common.controls.SosTextBox txtFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCd;
        private common.controls.SosTextBox txtDenpyoNo;
        private System.Windows.Forms.Label lblDenpyoNo;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private common.controls.SosTextBox txtDay;
        private System.Windows.Forms.Label lblYear;
        private common.controls.SosTextBox txtMonth;
        private common.controls.SosTextBox txtGengoYear;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.Label lblEraBackFr;
        private System.Windows.Forms.Label lblDenpyoDate;
        private System.Windows.Forms.Label lblTorihikiKubunNm;
        private common.controls.SosTextBox txtTorihikiKubunCd;
        private System.Windows.Forms.Label lblTorihikiKubunCd;
        private System.Windows.Forms.DataGridView dgvInputList;
        private System.Windows.Forms.Panel pnlDenpyoSyukei;
        private common.controls.SosTextBox txtTotalGaku;
        private common.controls.SosTextBox txtSyohiZei;
        private common.controls.SosTextBox txtSyokei;
        private System.Windows.Forms.Label lblTotalGaku;
        private System.Windows.Forms.Label lblSyohiZei;
        private System.Windows.Forms.Label lblSyokei;
        private common.controls.SosTextBox txtGridEdit;
        private System.Windows.Forms.Label lblBeforeDenpyoNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn GYO_BANGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn KUBUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIN_CD;
        private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIN_NM;
        private System.Windows.Forms.DataGridViewTextBoxColumn IRISU;
        private System.Windows.Forms.DataGridViewTextBoxColumn TANI;
        private System.Windows.Forms.DataGridViewTextBoxColumn KESUSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn BARA_SOSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn URI_TANKA;
        private System.Windows.Forms.DataGridViewTextBoxColumn BAIKA_KINGAKU;
        private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIZEI;
        private System.Windows.Forms.DataGridViewTextBoxColumn SHIWAKE_CD;
        private System.Windows.Forms.DataGridViewTextBoxColumn BUMON_CD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_KUBUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn JIGYO_KUBUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_RITSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn KUBUN_CD;
        private System.Windows.Forms.DataGridViewTextBoxColumn GEN_TANKA;
        private System.Windows.Forms.DataGridViewTextBoxColumn KAZEI_KUBUN;

    }
}