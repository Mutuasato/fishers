﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.kb.kbmr1041
{
    /// <summary>
    /// KBDR1061R の帳票
    /// </summary>
    public partial class KBMR1041R : BaseReport
    {
        public KBMR1041R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

       private void detail_Format(object sender, EventArgs e)
        {
            int Count = Util.ToInt(this.txtCount.Text);
            Count++;
            this.txtCount.Text = Count.ToString();

            if (Count % 45 == 0 || Count == Util.ToInt(this.txtDetailAllCount.Text))
            {
                this.line24.Visible = true;
                this.line22.Visible = false;
            } 
            else
            {
                this.line24.Visible = false;
                this.line22.Visible = true;
            }
        }
    }
}
