﻿namespace jp.co.fsi.kb.kbmr1041
{
    partial class KBMR1041
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUriRankFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtUriRankTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblEra = new System.Windows.Forms.Label();
            this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonth = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.rdoDateview2 = new System.Windows.Forms.RadioButton();
            this.rdoDateview1 = new System.Windows.Forms.RadioButton();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(9, 812);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1129, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1119, 41);
            this.lblTitle.Text = "商品売上順位表";
            // 
            // txtUriRankFr
            // 
            this.txtUriRankFr.AutoSizeFromLength = false;
            this.txtUriRankFr.DisplayLength = null;
            this.txtUriRankFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUriRankFr.Location = new System.Drawing.Point(139, 8);
            this.txtUriRankFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtUriRankFr.MaxLength = 5;
            this.txtUriRankFr.Name = "txtUriRankFr";
            this.txtUriRankFr.Size = new System.Drawing.Size(65, 23);
            this.txtUriRankFr.TabIndex = 0;
            this.txtUriRankFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtUriRankFr_Validating);
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.BackColor = System.Drawing.Color.Silver;
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(212, 7);
            this.lblCodeBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(20, 24);
            this.lblCodeBet.TabIndex = 1;
            this.lblCodeBet.Tag = "CHANGE";
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUriRankTo
            // 
            this.txtUriRankTo.AutoSizeFromLength = false;
            this.txtUriRankTo.DisplayLength = null;
            this.txtUriRankTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUriRankTo.Location = new System.Drawing.Point(240, 8);
            this.txtUriRankTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtUriRankTo.MaxLength = 5;
            this.txtUriRankTo.Name = "txtUriRankTo";
            this.txtUriRankTo.Size = new System.Drawing.Size(65, 23);
            this.txtUriRankTo.TabIndex = 2;
            this.txtUriRankTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUriRankTo_KeyDown);
            this.txtUriRankTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtUriRankTo_Validating);
            // 
            // lblEra
            // 
            this.lblEra.BackColor = System.Drawing.Color.LightCyan;
            this.lblEra.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEra.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEra.Location = new System.Drawing.Point(139, 7);
            this.lblEra.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEra.Name = "lblEra";
            this.lblEra.Size = new System.Drawing.Size(53, 24);
            this.lblEra.TabIndex = 0;
            this.lblEra.Tag = "DISPNAME";
            this.lblEra.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtYear
            // 
            this.txtYear.AutoSizeFromLength = false;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYear.Location = new System.Drawing.Point(199, 8);
            this.txtYear.Margin = new System.Windows.Forms.Padding(4);
            this.txtYear.MaxLength = 2;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(39, 23);
            this.txtYear.TabIndex = 1;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.Location = new System.Drawing.Point(324, 7);
            this.lblMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonth.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(24, 24);
            this.lblMonth.TabIndex = 4;
            this.lblMonth.Tag = "CHANGE";
            this.lblMonth.Text = "月";
            this.lblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(245, 7);
            this.lblYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYear.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(24, 24);
            this.lblYear.TabIndex = 2;
            this.lblYear.Tag = "CHANGE";
            this.lblYear.Text = "年";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.Location = new System.Drawing.Point(277, 8);
            this.txtMonth.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(39, 23);
            this.txtMonth.TabIndex = 3;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // rdoDateview2
            // 
            this.rdoDateview2.AutoSize = true;
            this.rdoDateview2.BackColor = System.Drawing.Color.Silver;
            this.rdoDateview2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoDateview2.Location = new System.Drawing.Point(237, 8);
            this.rdoDateview2.Margin = new System.Windows.Forms.Padding(4);
            this.rdoDateview2.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoDateview2.Name = "rdoDateview2";
            this.rdoDateview2.Size = new System.Drawing.Size(106, 24);
            this.rdoDateview2.TabIndex = 4;
            this.rdoDateview2.TabStop = true;
            this.rdoDateview2.Tag = "CHANGE";
            this.rdoDateview2.Text = "表示しない";
            this.rdoDateview2.UseVisualStyleBackColor = false;
            // 
            // rdoDateview1
            // 
            this.rdoDateview1.AutoSize = true;
            this.rdoDateview1.BackColor = System.Drawing.Color.Silver;
            this.rdoDateview1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoDateview1.Location = new System.Drawing.Point(139, 8);
            this.rdoDateview1.Margin = new System.Windows.Forms.Padding(4);
            this.rdoDateview1.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoDateview1.Name = "rdoDateview1";
            this.rdoDateview1.Size = new System.Drawing.Size(90, 24);
            this.rdoDateview1.TabIndex = 3;
            this.rdoDateview1.TabStop = true;
            this.rdoDateview1.Tag = "CHANGE";
            this.rdoDateview1.Text = "表示する";
            this.rdoDateview1.UseVisualStyleBackColor = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(139, 9);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(53, 23);
            this.txtMizuageShishoCd.TabIndex = 908;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(199, 8);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(266, 24);
            this.lblMizuageShishoNm.TabIndex = 910;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.MinimumSize = new System.Drawing.Size(0, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(476, 39);
            this.label1.TabIndex = 911;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "支所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(476, 39);
            this.label2.TabIndex = 911;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "日付範囲";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(476, 39);
            this.label3.TabIndex = 911;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "売上順位範囲指定";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(476, 40);
            this.label4.TabIndex = 911;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "日付表示";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 45);
            this.fsiTableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 4;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(486, 194);
            this.fsiTableLayoutPanel1.TabIndex = 912;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.rdoDateview1);
            this.fsiPanel4.Controls.Add(this.rdoDateview2);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(5, 149);
            this.fsiPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(476, 40);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtUriRankFr);
            this.fsiPanel3.Controls.Add(this.txtUriRankTo);
            this.fsiPanel3.Controls.Add(this.lblCodeBet);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(5, 101);
            this.fsiPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(476, 39);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblEra);
            this.fsiPanel2.Controls.Add(this.txtYear);
            this.fsiPanel2.Controls.Add(this.txtMonth);
            this.fsiPanel2.Controls.Add(this.lblYear);
            this.fsiPanel2.Controls.Add(this.lblMonth);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(5, 53);
            this.fsiPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(476, 39);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(5, 5);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(476, 39);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // KBMR1041
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBMR1041";
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtUriRankFr;
        private System.Windows.Forms.Label lblCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtUriRankTo;
        private System.Windows.Forms.Label lblEra;
        private jp.co.fsi.common.controls.FsiTextBox txtYear;
        private jp.co.fsi.common.controls.FsiTextBox txtMonth;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.RadioButton rdoDateview2;
        private System.Windows.Forms.RadioButton rdoDateview1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}