﻿namespace jp.co.fsi.kb.kbmr1051
{
    partial class KBMR1052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShiiresakiCd6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm6 = new System.Windows.Forms.Label();
            this.lblShiiresaki6 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm5 = new System.Windows.Forms.Label();
            this.lblShiiresaki5 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm4 = new System.Windows.Forms.Label();
            this.lblShiiresaki4 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm3 = new System.Windows.Forms.Label();
            this.lblShiiresaki3 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm2 = new System.Windows.Forms.Label();
            this.lblShiiresaki2 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm1 = new System.Windows.Forms.Label();
            this.lblShiiresaki1 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 223);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(580, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(588, 31);
            this.lblTitle.Visible = false;
            // 
            // txtShiiresakiCd6
            // 
            this.txtShiiresakiCd6.AutoSizeFromLength = true;
            this.txtShiiresakiCd6.DisplayLength = null;
            this.txtShiiresakiCd6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd6.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd6.Location = new System.Drawing.Point(744, 266);
            this.txtShiiresakiCd6.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiiresakiCd6.MaxLength = 5;
            this.txtShiiresakiCd6.Name = "txtShiiresakiCd6";
            this.txtShiiresakiCd6.Size = new System.Drawing.Size(59, 20);
            this.txtShiiresakiCd6.TabIndex = 926;
            this.txtShiiresakiCd6.TabStop = false;
            this.txtShiiresakiCd6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd6.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd6_Validating);
            // 
            // lblShiiresakiNm6
            // 
            this.lblShiiresakiNm6.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresakiNm6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm6.Location = new System.Drawing.Point(808, 266);
            this.lblShiiresakiNm6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresakiNm6.Name = "lblShiiresakiNm6";
            this.lblShiiresakiNm6.Size = new System.Drawing.Size(324, 27);
            this.lblShiiresakiNm6.TabIndex = 928;
            this.lblShiiresakiNm6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki6
            // 
            this.lblShiiresaki6.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiiresaki6.Location = new System.Drawing.Point(608, 262);
            this.lblShiiresaki6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresaki6.Name = "lblShiiresaki6";
            this.lblShiiresaki6.Size = new System.Drawing.Size(532, 33);
            this.lblShiiresaki6.TabIndex = 927;
            this.lblShiiresaki6.Text = "仕入先コード6";
            this.lblShiiresaki6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd5
            // 
            this.txtShiiresakiCd5.AutoSizeFromLength = true;
            this.txtShiiresakiCd5.DisplayLength = null;
            this.txtShiiresakiCd5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd5.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd5.Location = new System.Drawing.Point(116, 4);
            this.txtShiiresakiCd5.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiiresakiCd5.MaxLength = 5;
            this.txtShiiresakiCd5.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtShiiresakiCd5.Name = "txtShiiresakiCd5";
            this.txtShiiresakiCd5.Size = new System.Drawing.Size(59, 23);
            this.txtShiiresakiCd5.TabIndex = 923;
            this.txtShiiresakiCd5.TabStop = false;
            this.txtShiiresakiCd5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd5.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd5_Validating);
            // 
            // lblShiiresakiNm5
            // 
            this.lblShiiresakiNm5.BackColor = System.Drawing.Color.LightCyan;
            this.lblShiiresakiNm5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm5.Location = new System.Drawing.Point(180, 3);
            this.lblShiiresakiNm5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresakiNm5.Name = "lblShiiresakiNm5";
            this.lblShiiresakiNm5.Size = new System.Drawing.Size(324, 24);
            this.lblShiiresakiNm5.TabIndex = 925;
            this.lblShiiresakiNm5.Tag = "DISPNAME";
            this.lblShiiresakiNm5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki5
            // 
            this.lblShiiresaki5.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShiiresaki5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresaki5.Location = new System.Drawing.Point(0, 0);
            this.lblShiiresaki5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresaki5.Name = "lblShiiresaki5";
            this.lblShiiresaki5.Size = new System.Drawing.Size(513, 32);
            this.lblShiiresaki5.TabIndex = 924;
            this.lblShiiresaki5.Tag = "CHANGE";
            this.lblShiiresaki5.Text = "仕入先コード5";
            this.lblShiiresaki5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd4
            // 
            this.txtShiiresakiCd4.AutoSizeFromLength = true;
            this.txtShiiresakiCd4.DisplayLength = null;
            this.txtShiiresakiCd4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd4.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd4.Location = new System.Drawing.Point(116, 3);
            this.txtShiiresakiCd4.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiiresakiCd4.MaxLength = 5;
            this.txtShiiresakiCd4.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtShiiresakiCd4.Name = "txtShiiresakiCd4";
            this.txtShiiresakiCd4.Size = new System.Drawing.Size(59, 23);
            this.txtShiiresakiCd4.TabIndex = 920;
            this.txtShiiresakiCd4.TabStop = false;
            this.txtShiiresakiCd4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd4.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd4_Validating);
            // 
            // lblShiiresakiNm4
            // 
            this.lblShiiresakiNm4.BackColor = System.Drawing.Color.LightCyan;
            this.lblShiiresakiNm4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm4.Location = new System.Drawing.Point(180, 2);
            this.lblShiiresakiNm4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresakiNm4.Name = "lblShiiresakiNm4";
            this.lblShiiresakiNm4.Size = new System.Drawing.Size(324, 24);
            this.lblShiiresakiNm4.TabIndex = 922;
            this.lblShiiresakiNm4.Tag = "DISPNAME";
            this.lblShiiresakiNm4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki4
            // 
            this.lblShiiresaki4.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShiiresaki4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresaki4.Location = new System.Drawing.Point(0, 0);
            this.lblShiiresaki4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresaki4.Name = "lblShiiresaki4";
            this.lblShiiresaki4.Size = new System.Drawing.Size(513, 29);
            this.lblShiiresaki4.TabIndex = 921;
            this.lblShiiresaki4.Tag = "CHANGE";
            this.lblShiiresaki4.Text = "仕入先コード4";
            this.lblShiiresaki4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd3
            // 
            this.txtShiiresakiCd3.AutoSizeFromLength = true;
            this.txtShiiresakiCd3.DisplayLength = null;
            this.txtShiiresakiCd3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd3.Location = new System.Drawing.Point(116, 2);
            this.txtShiiresakiCd3.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiiresakiCd3.MaxLength = 5;
            this.txtShiiresakiCd3.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtShiiresakiCd3.Name = "txtShiiresakiCd3";
            this.txtShiiresakiCd3.Size = new System.Drawing.Size(59, 23);
            this.txtShiiresakiCd3.TabIndex = 917;
            this.txtShiiresakiCd3.TabStop = false;
            this.txtShiiresakiCd3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd3.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd3_Validating);
            // 
            // lblShiiresakiNm3
            // 
            this.lblShiiresakiNm3.BackColor = System.Drawing.Color.LightCyan;
            this.lblShiiresakiNm3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm3.Location = new System.Drawing.Point(180, 1);
            this.lblShiiresakiNm3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresakiNm3.Name = "lblShiiresakiNm3";
            this.lblShiiresakiNm3.Size = new System.Drawing.Size(324, 24);
            this.lblShiiresakiNm3.TabIndex = 919;
            this.lblShiiresakiNm3.Tag = "DISPNAME";
            this.lblShiiresakiNm3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki3
            // 
            this.lblShiiresaki3.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShiiresaki3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresaki3.Location = new System.Drawing.Point(0, 0);
            this.lblShiiresaki3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresaki3.Name = "lblShiiresaki3";
            this.lblShiiresaki3.Size = new System.Drawing.Size(513, 29);
            this.lblShiiresaki3.TabIndex = 918;
            this.lblShiiresaki3.Tag = "CHANGE";
            this.lblShiiresaki3.Text = "仕入先コード3";
            this.lblShiiresaki3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd2
            // 
            this.txtShiiresakiCd2.AutoSizeFromLength = true;
            this.txtShiiresakiCd2.DisplayLength = null;
            this.txtShiiresakiCd2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd2.Location = new System.Drawing.Point(116, 3);
            this.txtShiiresakiCd2.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiiresakiCd2.MaxLength = 5;
            this.txtShiiresakiCd2.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtShiiresakiCd2.Name = "txtShiiresakiCd2";
            this.txtShiiresakiCd2.Size = new System.Drawing.Size(59, 23);
            this.txtShiiresakiCd2.TabIndex = 914;
            this.txtShiiresakiCd2.TabStop = false;
            this.txtShiiresakiCd2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd2.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd2_Validating);
            // 
            // lblShiiresakiNm2
            // 
            this.lblShiiresakiNm2.BackColor = System.Drawing.Color.LightCyan;
            this.lblShiiresakiNm2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm2.Location = new System.Drawing.Point(180, 2);
            this.lblShiiresakiNm2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresakiNm2.Name = "lblShiiresakiNm2";
            this.lblShiiresakiNm2.Size = new System.Drawing.Size(324, 24);
            this.lblShiiresakiNm2.TabIndex = 916;
            this.lblShiiresakiNm2.Tag = "DISPNAME";
            this.lblShiiresakiNm2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki2
            // 
            this.lblShiiresaki2.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShiiresaki2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresaki2.Location = new System.Drawing.Point(0, 0);
            this.lblShiiresaki2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresaki2.Name = "lblShiiresaki2";
            this.lblShiiresaki2.Size = new System.Drawing.Size(513, 29);
            this.lblShiiresaki2.TabIndex = 915;
            this.lblShiiresaki2.Tag = "CHANGE";
            this.lblShiiresaki2.Text = "仕入先コード2";
            this.lblShiiresaki2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd1
            // 
            this.txtShiiresakiCd1.AutoSizeFromLength = true;
            this.txtShiiresakiCd1.DisplayLength = null;
            this.txtShiiresakiCd1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd1.Location = new System.Drawing.Point(116, 3);
            this.txtShiiresakiCd1.Margin = new System.Windows.Forms.Padding(4);
            this.txtShiiresakiCd1.MaxLength = 5;
            this.txtShiiresakiCd1.MinimumSize = new System.Drawing.Size(4, 23);
            this.txtShiiresakiCd1.Name = "txtShiiresakiCd1";
            this.txtShiiresakiCd1.Size = new System.Drawing.Size(59, 23);
            this.txtShiiresakiCd1.TabIndex = 911;
            this.txtShiiresakiCd1.TabStop = false;
            this.txtShiiresakiCd1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd1.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd1_Validating);
            // 
            // lblShiiresakiNm1
            // 
            this.lblShiiresakiNm1.BackColor = System.Drawing.Color.LightCyan;
            this.lblShiiresakiNm1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm1.Location = new System.Drawing.Point(180, 2);
            this.lblShiiresakiNm1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresakiNm1.Name = "lblShiiresakiNm1";
            this.lblShiiresakiNm1.Size = new System.Drawing.Size(324, 24);
            this.lblShiiresakiNm1.TabIndex = 913;
            this.lblShiiresakiNm1.Tag = "DISPNAME";
            this.lblShiiresakiNm1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki1
            // 
            this.lblShiiresaki1.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShiiresaki1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresaki1.Location = new System.Drawing.Point(0, 0);
            this.lblShiiresaki1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiiresaki1.Name = "lblShiiresaki1";
            this.lblShiiresaki1.Size = new System.Drawing.Size(513, 29);
            this.lblShiiresaki1.TabIndex = 912;
            this.lblShiiresaki1.Tag = "CHANGE";
            this.lblShiiresaki1.Text = "仕入先コード1";
            this.lblShiiresaki1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 34);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 5;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(521, 184);
            this.fsiTableLayoutPanel1.TabIndex = 929;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtShiiresakiCd5);
            this.fsiPanel5.Controls.Add(this.lblShiiresakiNm5);
            this.fsiPanel5.Controls.Add(this.lblShiiresaki5);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 148);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(513, 32);
            this.fsiPanel5.TabIndex = 4;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.txtShiiresakiCd4);
            this.fsiPanel4.Controls.Add(this.lblShiiresakiNm4);
            this.fsiPanel4.Controls.Add(this.lblShiiresaki4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 112);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(513, 29);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtShiiresakiCd3);
            this.fsiPanel3.Controls.Add(this.lblShiiresakiNm3);
            this.fsiPanel3.Controls.Add(this.lblShiiresaki3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 76);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(513, 29);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.txtShiiresakiCd2);
            this.fsiPanel2.Controls.Add(this.lblShiiresakiNm2);
            this.fsiPanel2.Controls.Add(this.lblShiiresaki2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 40);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(513, 29);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtShiiresakiCd1);
            this.fsiPanel1.Controls.Add(this.lblShiiresakiNm1);
            this.fsiPanel1.Controls.Add(this.lblShiiresaki1);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(513, 29);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // KBMR1052
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 362);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.txtShiiresakiCd6);
            this.Controls.Add(this.lblShiiresakiNm6);
            this.Controls.Add(this.lblShiiresaki6);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBMR1052";
            this.Text = "県漁連コード設定";
            this.Controls.SetChildIndex(this.lblShiiresaki6, 0);
            this.Controls.SetChildIndex(this.lblShiiresakiNm6, 0);
            this.Controls.SetChildIndex(this.txtShiiresakiCd6, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private common.controls.FsiTextBox txtShiiresakiCd6;
        private System.Windows.Forms.Label lblShiiresakiNm6;
        private System.Windows.Forms.Label lblShiiresaki6;
        private common.controls.FsiTextBox txtShiiresakiCd5;
        private System.Windows.Forms.Label lblShiiresakiNm5;
        private System.Windows.Forms.Label lblShiiresaki5;
        private common.controls.FsiTextBox txtShiiresakiCd4;
        private System.Windows.Forms.Label lblShiiresakiNm4;
        private System.Windows.Forms.Label lblShiiresaki4;
        private common.controls.FsiTextBox txtShiiresakiCd3;
        private System.Windows.Forms.Label lblShiiresakiNm3;
        private System.Windows.Forms.Label lblShiiresaki3;
        private common.controls.FsiTextBox txtShiiresakiCd2;
        private System.Windows.Forms.Label lblShiiresakiNm2;
        private System.Windows.Forms.Label lblShiiresaki2;
        private common.controls.FsiTextBox txtShiiresakiCd1;
        private System.Windows.Forms.Label lblShiiresakiNm1;
        private System.Windows.Forms.Label lblShiiresaki1;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}