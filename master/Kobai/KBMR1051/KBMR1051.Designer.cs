﻿namespace jp.co.fsi.kb.kbmr1051
{
    partial class KBMR1051
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtJpYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpTo = new System.Windows.Forms.Label();
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.lblDateYear = new System.Windows.Forms.Label();
            this.txtJpYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpFr = new System.Windows.Forms.Label();
            this.lblShohinKubunBet = new System.Windows.Forms.Label();
            this.lblShohinKubunFr = new System.Windows.Forms.Label();
            this.lblShohinKubunTo = new System.Windows.Forms.Label();
            this.txtShohinKubunTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohinKubunFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.rdoAll = new System.Windows.Forms.RadioButton();
            this.rdoMonth = new System.Windows.Forms.RadioButton();
            this.chkMode = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel8 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel8.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 708);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1124, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1113, 41);
            this.lblTitle.Text = "";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(352, 5);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.MinimumSize = new System.Drawing.Size(0, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 24);
            this.label5.TabIndex = 12;
            this.label5.Tag = "CHANGE";
            this.label5.Text = "～";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(571, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.MinimumSize = new System.Drawing.Size(0, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 24);
            this.label1.TabIndex = 11;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "月";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(500, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 24);
            this.label2.TabIndex = 9;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "年";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJpYearTo
            // 
            this.txtJpYearTo.AutoSizeFromLength = false;
            this.txtJpYearTo.DisplayLength = null;
            this.txtJpYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearTo.Location = new System.Drawing.Point(457, 6);
            this.txtJpYearTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpYearTo.MaxLength = 2;
            this.txtJpYearTo.Name = "txtJpYearTo";
            this.txtJpYearTo.Size = new System.Drawing.Size(39, 23);
            this.txtJpYearTo.TabIndex = 8;
            this.txtJpYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpYearTo_Validating);
            // 
            // txtJpMonthTo
            // 
            this.txtJpMonthTo.AutoSizeFromLength = false;
            this.txtJpMonthTo.DisplayLength = null;
            this.txtJpMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthTo.Location = new System.Drawing.Point(527, 6);
            this.txtJpMonthTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpMonthTo.MaxLength = 2;
            this.txtJpMonthTo.Name = "txtJpMonthTo";
            this.txtJpMonthTo.Size = new System.Drawing.Size(39, 23);
            this.txtJpMonthTo.TabIndex = 10;
            this.txtJpMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // lblJpTo
            // 
            this.lblJpTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblJpTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpTo.Location = new System.Drawing.Point(400, 5);
            this.lblJpTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblJpTo.Name = "lblJpTo";
            this.lblJpTo.Size = new System.Drawing.Size(51, 24);
            this.lblJpTo.TabIndex = 6;
            this.lblJpTo.Tag = "DISPNAME";
            this.lblJpTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonth.Location = new System.Drawing.Point(307, 5);
            this.lblDateMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateMonth.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(20, 24);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Tag = "CHANGE";
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYear
            // 
            this.lblDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblDateYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYear.Location = new System.Drawing.Point(236, 5);
            this.lblDateYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateYear.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblDateYear.Name = "lblDateYear";
            this.lblDateYear.Size = new System.Drawing.Size(23, 24);
            this.lblDateYear.TabIndex = 3;
            this.lblDateYear.Tag = "CHANGE";
            this.lblDateYear.Text = "年";
            this.lblDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJpYearFr
            // 
            this.txtJpYearFr.AutoSizeFromLength = false;
            this.txtJpYearFr.DisplayLength = null;
            this.txtJpYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearFr.Location = new System.Drawing.Point(193, 6);
            this.txtJpYearFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpYearFr.MaxLength = 2;
            this.txtJpYearFr.Name = "txtJpYearFr";
            this.txtJpYearFr.Size = new System.Drawing.Size(39, 23);
            this.txtJpYearFr.TabIndex = 2;
            this.txtJpYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpYearFr_Validating);
            // 
            // txtJpMonthFr
            // 
            this.txtJpMonthFr.AutoSizeFromLength = false;
            this.txtJpMonthFr.DisplayLength = null;
            this.txtJpMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthFr.Location = new System.Drawing.Point(263, 6);
            this.txtJpMonthFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtJpMonthFr.MaxLength = 2;
            this.txtJpMonthFr.Name = "txtJpMonthFr";
            this.txtJpMonthFr.Size = new System.Drawing.Size(39, 23);
            this.txtJpMonthFr.TabIndex = 4;
            this.txtJpMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblJpFr
            // 
            this.lblJpFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblJpFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpFr.Location = new System.Drawing.Point(134, 5);
            this.lblJpFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJpFr.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblJpFr.Name = "lblJpFr";
            this.lblJpFr.Size = new System.Drawing.Size(51, 24);
            this.lblJpFr.TabIndex = 1;
            this.lblJpFr.Tag = "DISPNAME";
            this.lblJpFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKubunBet
            // 
            this.lblShohinKubunBet.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKubunBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunBet.Location = new System.Drawing.Point(410, 3);
            this.lblShohinKubunBet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKubunBet.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblShohinKubunBet.Name = "lblShohinKubunBet";
            this.lblShohinKubunBet.Size = new System.Drawing.Size(23, 24);
            this.lblShohinKubunBet.TabIndex = 2;
            this.lblShohinKubunBet.Tag = "CHANGE";
            this.lblShohinKubunBet.Text = "～";
            this.lblShohinKubunBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShohinKubunFr
            // 
            this.lblShohinKubunFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinKubunFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKubunFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunFr.Location = new System.Drawing.Point(201, 3);
            this.lblShohinKubunFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKubunFr.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblShohinKubunFr.Name = "lblShohinKubunFr";
            this.lblShohinKubunFr.Size = new System.Drawing.Size(204, 24);
            this.lblShohinKubunFr.TabIndex = 1;
            this.lblShohinKubunFr.Tag = "DISPNAME";
            this.lblShohinKubunFr.Text = "先　頭";
            this.lblShohinKubunFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKubunTo
            // 
            this.lblShohinKubunTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohinKubunTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKubunTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunTo.Location = new System.Drawing.Point(507, 3);
            this.lblShohinKubunTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinKubunTo.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblShohinKubunTo.Name = "lblShohinKubunTo";
            this.lblShohinKubunTo.Size = new System.Drawing.Size(204, 24);
            this.lblShohinKubunTo.TabIndex = 4;
            this.lblShohinKubunTo.Tag = "DISPNAME";
            this.lblShohinKubunTo.Text = "最　後";
            this.lblShohinKubunTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKubunTo
            // 
            this.txtShohinKubunTo.AutoSizeFromLength = false;
            this.txtShohinKubunTo.DisplayLength = null;
            this.txtShohinKubunTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKubunTo.Location = new System.Drawing.Point(441, 4);
            this.txtShohinKubunTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohinKubunTo.MaxLength = 4;
            this.txtShohinKubunTo.Name = "txtShohinKubunTo";
            this.txtShohinKubunTo.Size = new System.Drawing.Size(65, 23);
            this.txtShohinKubunTo.TabIndex = 3;
            this.txtShohinKubunTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKubunTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohinKubunTo_KeyDown);
            this.txtShohinKubunTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKubunTo_Validating);
            // 
            // txtShohinKubunFr
            // 
            this.txtShohinKubunFr.AutoSizeFromLength = false;
            this.txtShohinKubunFr.DisplayLength = null;
            this.txtShohinKubunFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKubunFr.Location = new System.Drawing.Point(134, 4);
            this.txtShohinKubunFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohinKubunFr.MaxLength = 4;
            this.txtShohinKubunFr.Name = "txtShohinKubunFr";
            this.txtShohinKubunFr.Size = new System.Drawing.Size(65, 23);
            this.txtShohinKubunFr.TabIndex = 0;
            this.txtShohinKubunFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKubunFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKubunFr_Validating);
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.BackColor = System.Drawing.Color.Silver;
            this.rdoZeikomi.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeikomi.Location = new System.Drawing.Point(70, 5);
            this.rdoZeikomi.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeikomi.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(58, 24);
            this.rdoZeikomi.TabIndex = 1;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Tag = "CHANGE";
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = false;
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.BackColor = System.Drawing.Color.Silver;
            this.rdoZeinuki.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeinuki.Location = new System.Drawing.Point(4, 5);
            this.rdoZeinuki.Margin = new System.Windows.Forms.Padding(4);
            this.rdoZeinuki.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(58, 24);
            this.rdoZeinuki.TabIndex = 0;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Tag = "CHANGE";
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(131, 6);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 908;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(177, 5);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.MinimumSize = new System.Drawing.Size(0, 24);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblMizuageShishoNm.TabIndex = 910;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0: 全ての商品",
            "1: 取引有りの商品のみ",
            "2: 取引中止の商品のみ"});
            this.comboBox1.Location = new System.Drawing.Point(134, 6);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(295, 24);
            this.comboBox1.TabIndex = 0;
            // 
            // rdoAll
            // 
            this.rdoAll.AutoSize = true;
            this.rdoAll.BackColor = System.Drawing.Color.Silver;
            this.rdoAll.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoAll.Location = new System.Drawing.Point(70, 6);
            this.rdoAll.Margin = new System.Windows.Forms.Padding(4);
            this.rdoAll.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoAll.Name = "rdoAll";
            this.rdoAll.Size = new System.Drawing.Size(58, 24);
            this.rdoAll.TabIndex = 1;
            this.rdoAll.TabStop = true;
            this.rdoAll.Tag = "CHANGE";
            this.rdoAll.Text = "累計";
            this.rdoAll.UseVisualStyleBackColor = false;
            this.rdoAll.CheckedChanged += new System.EventHandler(this.rdoAll_CheckedChanged);
            // 
            // rdoMonth
            // 
            this.rdoMonth.AutoSize = true;
            this.rdoMonth.BackColor = System.Drawing.Color.Silver;
            this.rdoMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoMonth.Location = new System.Drawing.Point(4, 6);
            this.rdoMonth.Margin = new System.Windows.Forms.Padding(4);
            this.rdoMonth.MinimumSize = new System.Drawing.Size(0, 24);
            this.rdoMonth.Name = "rdoMonth";
            this.rdoMonth.Size = new System.Drawing.Size(58, 24);
            this.rdoMonth.TabIndex = 0;
            this.rdoMonth.TabStop = true;
            this.rdoMonth.Tag = "CHANGE";
            this.rdoMonth.Text = "月計";
            this.rdoMonth.UseVisualStyleBackColor = false;
            this.rdoMonth.CheckedChanged += new System.EventHandler(this.rdoMonth_CheckedChanged);
            // 
            // chkMode
            // 
            this.chkMode.AutoSize = true;
            this.chkMode.BackColor = System.Drawing.Color.Silver;
            this.chkMode.Checked = true;
            this.chkMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMode.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkMode.Location = new System.Drawing.Point(452, 6);
            this.chkMode.Margin = new System.Windows.Forms.Padding(4);
            this.chkMode.MinimumSize = new System.Drawing.Size(0, 24);
            this.chkMode.Name = "chkMode";
            this.chkMode.Size = new System.Drawing.Size(171, 24);
            this.chkMode.TabIndex = 904;
            this.chkMode.Tag = "CHANGE";
            this.chkMode.Text = "商品明細を表示する";
            this.chkMode.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(865, 34);
            this.label3.TabIndex = 911;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "支所";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(865, 34);
            this.label6.TabIndex = 911;
            this.label6.Tag = "CHANGE";
            this.label6.Text = "出力月";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(865, 34);
            this.label7.TabIndex = 911;
            this.label7.Tag = "CHANGE";
            this.label7.Text = "商品区分１の検索";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(865, 34);
            this.label8.TabIndex = 911;
            this.label8.Tag = "CHANGE";
            this.label8.Text = "中止区分";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 44);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 4;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(873, 165);
            this.fsiTableLayoutPanel1.TabIndex = 912;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.comboBox1);
            this.fsiPanel4.Controls.Add(this.chkMode);
            this.fsiPanel4.Controls.Add(this.label8);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 127);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(865, 34);
            this.fsiPanel4.TabIndex = 3;
            this.fsiPanel4.Tag = "CHANGE";
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.txtShohinKubunFr);
            this.fsiPanel3.Controls.Add(this.fsiPanel8);
            this.fsiPanel3.Controls.Add(this.lblShohinKubunBet);
            this.fsiPanel3.Controls.Add(this.txtShohinKubunTo);
            this.fsiPanel3.Controls.Add(this.lblShohinKubunTo);
            this.fsiPanel3.Controls.Add(this.lblShohinKubunFr);
            this.fsiPanel3.Controls.Add(this.label7);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 86);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(865, 34);
            this.fsiPanel3.TabIndex = 2;
            this.fsiPanel3.Tag = "CHANGE";
            // 
            // fsiPanel8
            // 
            this.fsiPanel8.BackColor = System.Drawing.Color.Silver;
            this.fsiPanel8.Controls.Add(this.rdoZeinuki);
            this.fsiPanel8.Controls.Add(this.rdoZeikomi);
            this.fsiPanel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel8.Location = new System.Drawing.Point(727, 0);
            this.fsiPanel8.Name = "fsiPanel8";
            this.fsiPanel8.Size = new System.Drawing.Size(138, 34);
            this.fsiPanel8.TabIndex = 912;
            this.fsiPanel8.Tag = "CHANGE";
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.lblJpFr);
            this.fsiPanel2.Controls.Add(this.txtJpMonthFr);
            this.fsiPanel2.Controls.Add(this.fsiPanel5);
            this.fsiPanel2.Controls.Add(this.txtJpYearFr);
            this.fsiPanel2.Controls.Add(this.lblDateYear);
            this.fsiPanel2.Controls.Add(this.lblDateMonth);
            this.fsiPanel2.Controls.Add(this.lblJpTo);
            this.fsiPanel2.Controls.Add(this.txtJpMonthTo);
            this.fsiPanel2.Controls.Add(this.txtJpYearTo);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Controls.Add(this.label5);
            this.fsiPanel2.Controls.Add(this.label1);
            this.fsiPanel2.Controls.Add(this.label6);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 45);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(865, 34);
            this.fsiPanel2.TabIndex = 1;
            this.fsiPanel2.Tag = "CHANGE";
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.BackColor = System.Drawing.Color.Silver;
            this.fsiPanel5.Controls.Add(this.rdoAll);
            this.fsiPanel5.Controls.Add(this.rdoMonth);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.fsiPanel5.Location = new System.Drawing.Point(727, 0);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(138, 34);
            this.fsiPanel5.TabIndex = 912;
            this.fsiPanel5.Tag = "CHANGE";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.label3);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(865, 34);
            this.fsiPanel1.TabIndex = 0;
            this.fsiPanel1.Tag = "CHANGE";
            // 
            // KBMR1051
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 745);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBMR1051";
            this.Tag = "CHANGE";
            this.Text = "";
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel8.ResumeLayout(false);
            this.fsiPanel8.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtJpYearFr;
        private System.Windows.Forms.Label lblJpFr;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label lblDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtJpMonthFr;
        private System.Windows.Forms.Label lblShohinKubunFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKubunFr;
        private System.Windows.Forms.Label lblShohinKubunBet;
        private System.Windows.Forms.Label lblShohinKubunTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKubunTo;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private common.controls.FsiTextBox txtJpYearTo;
        private common.controls.FsiTextBox txtJpMonthTo;
        private System.Windows.Forms.Label lblJpTo;
        private System.Windows.Forms.RadioButton rdoAll;
        private System.Windows.Forms.RadioButton rdoMonth;
        private System.Windows.Forms.CheckBox chkMode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel8;
    }
}