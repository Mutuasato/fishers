﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1051
{
    #region 棚卸設定構造体
    // 棚卸設定構造体
    public struct Settei
    {
        public int kbn;
        public decimal tanka;
        public decimal gokei;
        public int sort;
        public void Clear()
        {
            kbn = 0;
            tanka = 0;
            gokei = 0;
            sort = 0;
        }
    }
    #endregion

    /// <summary>
    /// 事業集計一覧表(KBMR1051)
    /// </summary>
    public partial class KBMR1051 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols = 30;
        #endregion

        /// <summary>
        /// 棚卸設定退避用変数
        /// </summary>
        public Settei gSettei;

        public decimal[] kenCode = new decimal[6];

        public string gParam;

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBMR1051()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblJpFr.Text = jpDate[0];
            txtJpYearFr.Text = jpDate[2];
            txtJpMonthFr.Text = jpDate[3];
            lblJpTo.Text = jpDate[0];
            txtJpYearTo.Text = jpDate[2];
            txtJpMonthTo.Text = jpDate[3];

            rdoZeinuki.Checked = true;
            rdoMonth.Checked = true;

            this.comboBox1.SelectedIndex = 1;

            GetSettei();

            // 初期フォーカス
            txtJpYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、商品区分に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtJpYearFr":
                case "txtJpYearTo":
                case "txtShohinKubunFr":
                case "txtShohinKubunTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinKubunFr":
                    #region 商品区分１
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "VI_HN_SHOHIN_KBN1";
                            frm.InData = this.txtShohinKubunFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShohinKubunFr.Text = result[0];
                                this.lblShohinKubunFr.Text = result[1];

                                // 次の項目(商品区分２)にフォーカス
                                this.txtShohinKubunTo.Focus();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinKubunTo":
                    #region 商品区分１
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "VI_HN_SHOHIN_KBN1";
                            frm.InData = this.txtShohinKubunTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShohinKubunTo.Text = result[0];
                                this.lblShohinKubunTo.Text = result[1];

                            }
                        }
                    }
                    #endregion
                    break;

                case "txtJpYearFr":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblJpFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblJpFr.Text,
                                        this.txtJpYearFr.Text,
                                        this.txtJpMonthFr.Text,
                                        "1",
                                        this.Dba);
                                this.lblJpFr.Text = arrJpDate[0];
                                this.txtJpYearFr.Text = arrJpDate[2];
                                this.txtJpMonthFr.Text = arrJpDate[3];
                            }
                        }
                    }
                    break;
                #endregion
                case "txtJpYearTo":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblJpTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrToDate =
                                    Util.FixJpDate(this.lblJpTo.Text,
                                        this.txtJpYearTo.Text,
                                        this.txtJpMonthTo.Text,
                                        "1",
                                        this.Dba);
                                this.lblJpTo.Text = arrToDate[0];
                                this.txtJpYearTo.Text = arrToDate[2];
                                this.txtJpMonthTo.Text = arrToDate[3];
                            }
                        }
                    }
                    break;
                    #endregion

            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        public override void PressF11()
        {
            KBMR1052 frmKBMR1052 = new KBMR1052();
            if (frmKBMR1052.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                this.Config.ReloadConfig();
                // 設定内容の取得
                GetSettei();
            }
            frmKBMR1052.Dispose();
        }
        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[2] { "KBMR1051R", "KBMR1052R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidMizuageShishoCd())
            {
                e.Cancel = true;

                this.lblMizuageShishoNm.Text = "";
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 商品区分１(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKubunFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKubunFr())
            {
                e.Cancel = true;
                this.txtShohinKubunFr.SelectAll();
            }
        }

        /// <summary>
        /// 商品区分１(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKubunTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKubunTo())
            {
                e.Cancel = true;
                this.txtShohinKubunTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJpYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearFr())
            {
                e.Cancel = true;
                this.txtJpYearFr.SelectAll();
            }
            else
            {
                SetJpDate();
            }
        }
        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJpYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearTo())
            {
                e.Cancel = true;
                this.txtJpYearTo.SelectAll();
            }
            else
            {
                SetToDate();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthFr())
            {
                e.Cancel = true;
                this.txtJpMonthFr.SelectAll();
            }
            else
            {
                SetJpDate();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthTo())
            {
                e.Cancel = true;
                this.txtJpMonthTo.SelectAll();
            }
            else
            {
                SetToDate();
            }
        }

        /// <summary>
        /// 累計ラジオボタンクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoAll_CheckedChanged(object sender, EventArgs e)
        {
            /// 対象期間を会計期の開始月から終了年月に設定する。
            string[] jpDate = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
            lblJpFr.Text = jpDate[0];
            txtJpYearFr.Text = jpDate[2];
            txtJpMonthFr.Text = jpDate[3];
            jpDate = Util.ConvJpDate(Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);
            lblJpTo.Text = jpDate[0];
            txtJpYearTo.Text = jpDate[2];
            txtJpMonthTo.Text = jpDate[3];
        }

        /// <summary>
        /// 月計ラジオボタンクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoMonth_CheckedChanged(object sender, EventArgs e)
        {
            /// 対象期間を当月に変更する。
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblJpFr.Text = jpDate[0];
            txtJpYearFr.Text = jpDate[2];
            txtJpMonthFr.Text = jpDate[3];
            lblJpTo.Text = jpDate[0];
            txtJpYearTo.Text = jpDate[2];
            txtJpMonthTo.Text = jpDate[3];
        }

        /// <summary>
        /// 商品区分(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKubunTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtShohinKubunTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0入力の場合
            if (Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpYearFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtJpYearFr.Text))
            {
                this.txtJpYearFr.Text = "0";
            }

            return true;
        }
        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpYearTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtJpYearTo.Text))
            {
                this.txtJpYearTo.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpMonthFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtJpMonthFr.Text))
            {
                this.txtJpMonthFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtJpMonthFr.Text) > 12)
                {
                    this.txtJpMonthFr.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtJpMonthFr.Text) < 1)
                {
                    this.txtJpMonthFr.Text = "1";
                }
            }
            return true;
        }
        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpMonthTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtJpMonthTo.Text))
            {
                this.txtJpMonthTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtJpMonthTo.Text) > 12)
                {
                    this.txtJpMonthTo.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtJpMonthTo.Text) < 1)
                {
                    this.txtJpMonthTo.Text = "1";
                }
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                this.txtJpMonthFr.Text, "1", this.Dba));

        }
        private void SetToDate()
        {
            SetToDate(Util.FixJpDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                this.txtJpMonthTo.Text, "1", this.Dba));
        }

        /// <summary>
        /// 商品区分１(自)の入力チェック
        /// </summary>
        private bool IsValidShohinKubunFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinKubunFr.Text))
            {
                Msg.Error("商品区分１は数値のみで入力してください。");
                return false;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShohinKubunFr.Text.Length > 0)
            {
                this.lblShohinKubunFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN1", this.txtMizuageShishoCd.Text, this.txtShohinKubunFr.Text);
            }
            else
            {
                this.lblShohinKubunFr.Text = "先　頭";
            }

            return true;
        }

        /// <summary>
        /// 商品区分１(至)の入力チェック
        /// </summary>
        private bool IsValidShohinKubunTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinKubunTo.Text))
            {
                Msg.Error("商品区分１は数値のみで入力してください。");
                return false;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShohinKubunTo.Text.Length > 0)
            {
                this.lblShohinKubunTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN1", this.txtMizuageShishoCd.Text, this.txtShohinKubunTo.Text);
            }
            else
            {
                this.lblShohinKubunTo.Text = "最　後";
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年(自)のチェック
            if (!IsValidDateYearFr())
            {
                this.txtJpYearFr.Focus();
                this.txtJpYearFr.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValidDateYearTo())
            {
                this.txtJpYearTo.Focus();
                this.txtJpYearTo.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonthFr())
            {
                this.txtJpMonthFr.Focus();
                this.txtJpMonthFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonthTo())
            {
                this.txtJpMonthTo.Focus();
                this.txtJpMonthTo.SelectAll();
                return false;
            }
            SetJpDate();
            SetToDate();

            // 商品区分１(自)の入力チェック
            if (!IsValidShohinKubunFr())
            {
                this.txtShohinKubunFr.Focus();
                this.txtShohinKubunFr.SelectAll();
                return false;
            }
            // 商品区分１(至)の入力チェック
            if (!IsValidShohinKubunTo())
            {
                this.txtShohinKubunTo.Focus();
                this.txtShohinKubunTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblJpFr.Text = arrJpDate[0];
            this.txtJpYearFr.Text = arrJpDate[2];
            this.txtJpMonthFr.Text = arrJpDate[3];
        }

        private void SetToDate(string[] arrToDate)
        {
            this.lblJpTo.Text = arrToDate[0];
            this.txtJpYearTo.Text = arrToDate[2];
            this.txtJpMonthTo.Text = arrToDate[3];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;
                
                this.Dba.BeginTransaction();

                // 日付を西暦にして取得
                DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                        this.txtJpMonthFr.Text, "1", this.Dba);

                // 前回棚卸日を取得
                DateTime zenkaiTnorsDt = new DateTime();
                DbParamCollection dpc = new DbParamCollection();
                StringBuilder sql = new StringBuilder();
                sql.Append(" SELECT");
                sql.Append("     MAX(TANAOROSHI_DATE) AS 最終棚卸日付");
                sql.Append(" FROM");
                sql.Append("     TB_HN_TANAOROSHI AS A ");
                sql.Append("     LEFT OUTER JOIN TB_HN_SHOHIN AS B ");
                sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD ");
                sql.Append("     AND A.SHISHO_CD = B.SHISHO_CD ");
                sql.Append("     AND A.SHOHIN_CD = B.SHOHIN_CD ");
                sql.Append(" WHERE");
                sql.Append("     A.KAISHA_CD = @KAISHA_CD AND ");
                if (this.txtMizuageShishoCd.Text != "0")
                {
                    sql.Append("     A.SHISHO_CD = @SHISHO_CD AND ");
                }
                sql.Append("     A.TANAOROSHI_DATE < @TANAOROSHI_DATE AND ");
                sql.Append("     B.SHOHIN_KUBUN5 <> 1");

                sql.Append(" UNION");

                sql.Append(" SELECT");
                sql.Append("     MAX(TANAOROSHI_DATE) AS 最終棚卸日付");
                sql.Append(" FROM");
                sql.Append("     TB_HN_TANAOROSHI2 AS A ");
                sql.Append("     LEFT OUTER JOIN TB_HN_SHOHIN AS B ");
                sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD ");
                sql.Append("     AND A.SHISHO_CD = B.SHISHO_CD ");
                sql.Append("     AND A.SHOHIN_CD = B.SHOHIN_CD ");
                sql.Append(" WHERE");
                sql.Append("     A.KAISHA_CD = @KAISHA_CD AND ");
                if (this.txtMizuageShishoCd.Text != "0")
                {
                    sql.Append("     A.SHISHO_CD = @SHISHO_CD AND ");
                }
                sql.Append("     A.TANAOROSHI_DATE < @TANAOROSHI_DATE AND ");
                sql.Append("     B.SHOHIN_KUBUN5 <> 1");

                sql.Append("     ORDER BY 最終棚卸日付 DESC");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, 10, tmpDateFr);

                DataTable dtTnorsBkup = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

                if (!ValChk.IsEmpty(dtTnorsBkup.Rows[0]["最終棚卸日付"]))
                {
                    zenkaiTnorsDt = Util.ToDate(dtTnorsBkup.Rows[0]["最終棚卸日付"]);
                }
                else
                {
                    // 棚卸が未実行の場合はシステムが稼働した期の期首からを累計とする
                    Msg.Error("棚卸ﾃﾞｰﾀがありません！");

                    return;
                }

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData(zenkaiTnorsDt);

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    sql = new StringBuilder();
                    sql.Append(" SELECT");
                    sql.Append(" *");
                    sql.Append(" FROM PR_HN_TBL");
                    sql.Append(" WHERE");
                    sql.Append(" GUID = @GUID");
                    sql.Append(" AND (ITEM06 != '0.00' OR ITEM07 != '0' OR ITEM08 != '0.00' OR ITEM09 != '0' OR ITEM10 != '0.00' OR ITEM11 != '0' OR ITEM12 != '0.00' OR ITEM13 != '0' OR ITEM16 != '0.00' OR ITEM17 != '0')");
                    sql.Append(" ORDER BY SORT ");

                    // データの取得
                    //DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                    //    Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "ITEM23 ASC , ITEM26 ASC", dpc);
                    DataTable dtOutput = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

                    jp.co.fsi.common.report.BaseReport rpt;
                    if (this.chkMode.Checked)
                    {
                        rpt = new KBMR1051R(dtOutput);
                    }
                    else
                    {
                        rpt = new KBMR1052R(dtOutput);
                    }
                    // 帳票オブジェクトをインスタンス化
                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Error("データがありません。");
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        /// <param name="zenkaiTnorsDt">前回棚卸日</param>
        private bool MakeWkData(DateTime zenkaiTnorsDt)
        {
            DbParamCollection dpc = new DbParamCollection();
            //DataRow drShohin;
            // 日付を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text, this.txtJpMonthFr.Text, "1", this.Dba);
            // 月の最終日を取得
            DateTime tmpDateTo = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text, this.txtJpMonthTo.Text, "1", this.Dba);
            //今月の最後の日
            DateTime tmpLastDay = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, Util.ToString(DateTime.DaysInMonth(Util.ToInt(this.txtJpYearTo.Text), Util.ToInt(this.txtJpMonthTo.Text))), this.Dba);

            DateTime tmpZenLastDay = tmpDateFr.AddDays(-1);
            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 日付表示形式を判断
            int dataHyojiFlag = 0; // 表示日付用変数
            // 入力開始日付が1日でない場合
            if (tmpDateFr.Day != 1)
            {
                dataHyojiFlag = 1;
            }
            // 入力終了日付が、その月の最終日でない場合
            else if (tmpDateTo != tmpLastDay)
            {
                dataHyojiFlag = 1;
            }
            // 入力開始日付と入力終了日付の年又は月が一致でない場合
            if (tmpDateFr.Year != tmpDateTo.Year || tmpDateFr.Month != tmpDateTo.Month)
            {
                dataHyojiFlag = 1;
            }

            string hyojiDate; // 表示用日付            
            if (dataHyojiFlag == 0)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3])
                          + "～"
                          + string.Format("{0}{1}年{2}月", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3]);
            }
            int i; // ループ用カウント変数

            // 商品区分設定
            string shohinKbnFr;
            string shohinKbnTo;
            if (Util.ToDecimal(txtShohinKubunFr.Text) > 0)
            {
                shohinKbnFr = txtShohinKubunFr.Text;
            }
            else
            {
                shohinKbnFr = "1";
            }
            if (Util.ToDecimal(txtShohinKubunTo.Text) > 0)
            {
                shohinKbnTo = txtShohinKubunTo.Text;
            }
            else
            {
                shohinKbnTo = "9999";
            }
            // 税抜,税込設定
            string zeiKbn;
            if (rdoZeinuki.Checked)
            {
                zeiKbn = "税抜き";
            }
            else
            {
                zeiKbn = "税込み";
            }
            string shishoCd = this.txtMizuageShishoCd.Text;
            // 出力日付設定
            string[] nowDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            string outputDate = nowDate[5];

            StringBuilder sql;
           
            i = 1;
            //中止区分
            int Chushikubun = this.comboBox1.SelectedIndex;
            #region データインサート,アップデート処理
            // 商品情報取得
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            //sql.Append("     A.KAISHA_CD,");
            //sql.Append("     A.SHOHIN_CD,");
            //sql.Append("     A.SHOHIN_NM,");
            //sql.Append("     A.SHIIRE_TANKA");
            sql.Append(" *");
            sql.Append(" FROM");
            sql.Append("     VI_HN_SHOHIN AS A");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
            {
                sql.Append("  A.SHISHO_CD    = @SHISHO_CD AND ");
            }
            sql.Append("     A.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND ");
            sql.Append("     A.SHOHIN_KUBUN5 <> 1 AND ");
            if (Chushikubun != 0)
            {
                sql.Append("     ISNULL(A.CHUSHI_KUBUN, 0) = @CHUSHI_KUBUN AND");
            }
            sql.Append("     A.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_KUBUN1, A.SHOHIN_KUBUN2, A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, shohinKbnFr);
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, shohinKbnTo);
            dpc.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 1, Chushikubun);

            DataTable dt_SHOHIN = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if(dt_SHOHIN is null)
            {
                return false;
            }
            else
            {
                foreach (DataRow dr in dt_SHOHIN.Rows)
                {
                    #region インサートテーブル
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("INSERT INTO PR_HN_TBL(");
                    sql.Append("  GUID");
                    sql.Append(" ,SORT");
                    sql.Append(" ," + Util.ColsArray(prtCols, ""));
                    sql.Append(") ");
                    sql.Append("VALUES(");
                    sql.Append("  @GUID");
                    sql.Append(" ,@SORT");
                    sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                    sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.Int, i);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, zeiKbn);
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyojiDate);
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);

                    //データを設定
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["SHOHIN_NM"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "0.00");            //繰越数量
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "0");               //繰越金額
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "0.00");            //受入数量
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "0");               //受入金額
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "0.00");            //系統数量
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, "0");               //系統金額
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "0.00");            //棚卸数量
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, "0");               //棚卸金額
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, "0.00");            //供給原価数量
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, "0");               //供給原価金額
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "0.00");            //供給高数量
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "0");               //供給高金額
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "0.00");            //受入消費税
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, "0");               //系統消費税
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, "0");               //供給高消費税
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, "0");               //在庫金額？
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, "消費税");               //消費税タイトル(内税・外税)
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, dr["SHOHIN_KUBUN1"]);
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dr["SHOHIN_KUBUN_NM1"]);
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, dr["SHOHIN_KUBUN2"]);
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, dr["SHOHIN_KUBUN_NM2"]);
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, dr["KAISHA_CD"]);
                    dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, dr["SHISHO_CD"]);
                    dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, dr["SHISHO_NM"]);
                    dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, outputDate);
                
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    i++;
                    #endregion
                }
            }

            #region 繰越棚卸データを取得する
            ///     開始日付以前の最終棚卸日付
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.KAISHA_CD AS KAISHA_CD,");
            sql.Append("     A.TANAOROSHI_DATE AS TANAOROSHI_DATE,");
            sql.Append("     A.SHOHIN_CD AS SHOHIN_CD,");
            sql.Append("     CASE WHEN A.IRISU <> 0 THEN ((A.IRISU * A.JITSU_SURYO1) + JITSU_SURYO2) ELSE JITSU_SURYO2 END AS SURYO2,");
            sql.Append("     A.KINGAKU AS KINGAKU ");
            sql.Append(" FROM");
            sql.Append("     TB_HN_TANAOROSHI AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B");
            sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" AND A.SHOHIN_CD = B.SHOHIN_CD");
            sql.Append(" AND B.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD ");
            if (shishoCd != "0")
            {
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD ");
            }
            sql.Append(" AND A.TANAOROSHI_DATE = @TANAOROSHI_DATE ");
            sql.Append(" AND B.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO ");
            sql.Append(" AND B.SHOHIN_KUBUN5 <> 1");

            sql.Append(" UNION ");

            sql.Append(" SELECT");
            sql.Append("     A.KAISHA_CD AS KAISHA_CD,");
            sql.Append("     A.TANAOROSHI_DATE AS TANAOROSHI_DATE,");
            sql.Append("     A.SHOHIN_CD AS SHOHIN_CD,");
            sql.Append("     CASE WHEN A.IRISU <> 0 THEN ((A.IRISU * A.JITSU_SURYO1) + JITSU_SURYO2) ELSE JITSU_SURYO2 END AS SURYO2,");
            sql.Append("     A.KINGAKU AS KINGAKU ");
            sql.Append(" FROM");
            sql.Append("     TB_HN_TANAOROSHI2 AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B");
            sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" AND A.SHOHIN_CD = B.SHOHIN_CD");
            sql.Append(" AND B.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD ");
            if (shishoCd != "0")
            {
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD ");
            }
            sql.Append(" AND A.TANAOROSHI_DATE = @TANAOROSHI_DATE ");
            sql.Append(" AND B.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO ");
            sql.Append(" AND B.SHOHIN_KUBUN5 <> 1");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, 10, zenkaiTnorsDt);
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, shohinKbnFr);
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, shohinKbnTo);

            DataTable dtR_KURIKOSHI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            if (dtR_KURIKOSHI.Rows.Count > 0)
            {
                decimal suryo = 0;
                decimal kin = 0;
                foreach (DataRow dr in dtR_KURIKOSHI.Rows)
                {
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("UPDATE PR_HN_TBL SET");
                    sql.Append("  ITEM06 = @ITEM06");
                    sql.Append(" ,ITEM07 = @ITEM07");
                    sql.Append(" WHERE");
                    sql.Append("  GUID = @GUID");
                    sql.Append(" AND");
                    sql.Append("  ITEM04 = @ITEM04");

                    suryo = Util.ToDecimal(dr["SURYO2"]);
                    kin = Util.ToDecimal(dr["KINGAKU"]);

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);

                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(suryo, 2));
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(kin));

                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                }
            }

            // 払出数量、払出金額、受入数量、受入金額の更新
            // 取得した結果をそのまま更新
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     B.SHOHIN_CD AS SHOHIN_CD,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (((B.SURYO1 * B.IRISU) + B.SURYO2) * -1) ELSE ((B.SURYO1 * B.IRISU) + B.SURYO2) END) ELSE 0 END) AS URIAGE_SURYO,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 ");
            sql.Append(" 			 THEN (");
            sql.Append(" 				CASE WHEN A.TORIHIKI_KUBUN2 = 2 ");
            sql.Append(" 					 THEN (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END) * -1 ");
            sql.Append(" 					 ELSE (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END)");
            sql.Append(" 				END) ");
            sql.Append(" 			 ELSE 0 ");
            sql.Append(" 		END) AS URIAGE_KINGAKU,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN B.SHOHIZEI * -1 ELSE B.SHOHIZEI END) ELSE 0 END) AS URIAGE_SHOHIZEI,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (((B.SURYO1 * B.IRISU) + B.SURYO2) * -1) ELSE ((B.SURYO1 * B.IRISU) + B.SURYO2) END) ELSE 0 END) AS SHIIRE_SURYO,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 ");
            sql.Append(" 			 THEN (");
            sql.Append(" 				CASE WHEN A.TORIHIKI_KUBUN2 = 2 ");
            sql.Append(" 					 THEN (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END) * -1 ");
            sql.Append(" 					 ELSE (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END)");
            sql.Append(" 				END) ");
            sql.Append(" 			 ELSE 0 ");
            sql.Append(" 		END) AS  SHIIRE_KINGAKU,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN B.SHOHIZEI * -1 ELSE B.SHOHIZEI END) ELSE 0 END) AS SHIIRE_SHOHIZEI");
            sql.Append(" FROM TB_HN_TORIHIKI_DENPYO AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKI_MEISAI AS B");
            sql.Append(" 	ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" 	AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" 	AND A.DENPYO_KUBUN = B.DENPYO_KUBUN");
            sql.Append(" 	AND A.DENPYO_BANGO = B.DENPYO_BANGO");
            sql.Append(" LEFT OUTER JOIN  TB_HN_SHOHIN AS C");
            sql.Append(" 	ON  B.KAISHA_CD = C.KAISHA_CD");
            sql.Append(" 	AND B.SHISHO_CD = C.SHISHO_CD");
            sql.Append(" 	AND B.SHOHIN_CD = C.SHOHIN_CD");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
            {
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD ");
            }
            sql.Append(" AND A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO");
            sql.Append(" AND C.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO ");
            sql.Append(" AND C.SHOHIN_KUBUN5 <> 1");
            sql.Append(" AND C.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" GROUP BY");
            sql.Append("     B.SHOHIN_CD");
            sql.Append(" ORDER BY");
            sql.Append("     B.SHOHIN_CD");

            // 前回棚卸日の翌日から当月末までの累計を取る
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, tmpDateFr);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, tmpLastDay);
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, shohinKbnFr);
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, shohinKbnTo);

            DataTable dtR_UKE_HARAI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if (dtR_UKE_HARAI.Rows.Count > 0 )
            {
                decimal sirKin = 0;
                decimal uragKin = 0;
                decimal sirSuryo = 0;
                decimal uragSuryo = 0;
                decimal uriShohizei = 0;
                decimal sirShohizei = 0;

                foreach (DataRow dr in dtR_UKE_HARAI.Rows)
                {
                    if (zeiKbn == "税抜き")
                    {
                        sirKin = Util.ToDecimal(dr["SHIIRE_KINGAKU"]);
                        sirShohizei = Util.ToDecimal(dr["SHIIRE_SHOHIZEI"]);
                        uragKin = Util.ToDecimal(dr["URIAGE_KINGAKU"]);
                        uriShohizei = Util.ToDecimal(dr["URIAGE_SHOHIZEI"]);
                    }
                    else
                    {
                        sirKin = Util.ToDecimal(dr["SHIIRE_KINGAKU"]) + Util.ToDecimal(dr["SHIIRE_SHOHIZEI"]);
                        sirShohizei = Util.ToDecimal(dr["SHIIRE_SHOHIZEI"]);
                        uragKin = Util.ToDecimal(dr["URIAGE_KINGAKU"]) + Util.ToDecimal(dr["URIAGE_SHOHIZEI"]);
                        uriShohizei = Util.ToDecimal(dr["URIAGE_SHOHIZEI"]);
                    }
                    // 四捨五入処理
                    uragSuryo = Util.Round(Util.ToDecimal(dr["URIAGE_SURYO"]), 2);
                    sirSuryo = Util.Round(Util.ToDecimal(dr["SHIIRE_SURYO"]), 2);

                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("UPDATE PR_HN_TBL SET");
                    sql.Append("  ITEM08 = @ITEM08");
                    sql.Append(" ,ITEM09 = @ITEM09");
                    sql.Append(" ,ITEM16 = @ITEM16");
                    sql.Append(" ,ITEM17 = @ITEM17");
                    sql.Append(" ,ITEM18 = @ITEM18");
                    sql.Append(" ,ITEM20 = @ITEM20");
                    sql.Append(" WHERE");
                    sql.Append("  GUID = @GUID");
                    sql.Append(" AND");
                    sql.Append("  ITEM04 = @ITEM04");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);

                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(sirSuryo, 2));
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(sirKin));
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(uragSuryo, 2));
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(uragKin));
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, sirShohizei);
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, uriShohizei);

                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                }
            }

            // 系統データ取得
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT ");
            sql.Append("  B.SHOHIN_CD      AS SHOHIN_CD,");
            sql.Append("  SUM( CASE WHEN A.DENPYO_KUBUN = 2 THEN ");
            sql.Append("          (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (((B.SURYO1 * B.IRISU) + B.SURYO2) * -1) ELSE ((B.SURYO１ * B.IRISU) + B.SURYO2) END)");
            sql.Append("       ELSE 0 END )        AS  SHIIRE_SURYO,");
            if (rdoZeinuki.Checked)
            {

                sql.Append("  SUM( CASE WHEN A.DENPYO_KUBUN = 2 THEN ");
                sql.Append("          (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN ");
                sql.Append("                (CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 THEN B.BAIKA_KINGAKU - B.SHOHIZEI ELSE B.BAIKA_KINGAKU END) * -1 ");
                sql.Append("           ELSE ");
                sql.Append("                CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 THEN B.BAIKA_KINGAKU - B.SHOHIZEI ELSE B.BAIKA_KINGAKU END ");
                sql.Append("           END)");
                sql.Append("       ELSE 0 END )");
            }
            else
            {
                sql.Append("  SUM( CASE WHEN A.DENPYO_KUBUN = 2 THEN ");
                sql.Append("          (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN ");
                sql.Append("                (CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 THEN B.BAIKA_KINGAKU - B.SHOHIZEI ELSE B.BAIKA_KINGAKU END) * -1 ");
                sql.Append("           ELSE ");
                sql.Append("                CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO=3 THEN B.BAIKA_KINGAKU - B.SHOHIZEI ELSE B.BAIKA_KINGAKU END ");
                sql.Append("           END)");
                sql.Append("       ELSE 0 END ) +");
                sql.Append("  SUM( CASE WHEN A.DENPYO_KUBUN = 2 THEN ");
                sql.Append("          (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN B.SHOHIZEI * -1 ELSE B.SHOHIZEI END)");
                sql.Append("       ELSE 0 END )");
            }
            sql.Append("     AS  SHIIRE_KINGAKU,");
            sql.Append("  SUM( CASE WHEN A.DENPYO_KUBUN = 2 THEN ");
            sql.Append("          (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN B.SHOHIZEI * -1 ELSE B.SHOHIZEI END)");
            sql.Append("       ELSE 0 END )        AS  SHIIRE_SHOHIZEI ");
            sql.Append(" FROM TB_HN_TORIHIKI_DENPYO AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKI_MEISAI AS B");
            sql.Append(" 	ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" 	AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" 	AND A.DENPYO_KUBUN = B.DENPYO_KUBUN");
            sql.Append(" 	AND A.DENPYO_BANGO = B.DENPYO_BANGO");
            sql.Append("  LEFT OUTER JOIN  TB_HN_SHOHIN  AS C ");
            sql.Append("    ON B.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("   AND B.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append(" WHERE ");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
            {
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD ");
            }
            sql.Append(" AND A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO");
            sql.Append(" AND A.SEIKYUSAKI_CD IN (" + gParam + ") ");
            //sql.Append(" AND A.SEIKYUSAKI_CD IN (@SEIKYUSAKI_CD) ");
            sql.Append(" AND C.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO ");
            sql.Append(" AND C.SHOHIN_KUBUN5 <> 1");
            sql.Append(" AND C.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" GROUP BY ");
            sql.Append("  B.SHOHIN_CD ");

            // 前回棚卸日の翌日から当月末までの累計を取る
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            //dpc.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 4, gParam);
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, tmpDateFr);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, tmpLastDay);
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, shohinKbnFr);
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, shohinKbnTo);

            DataTable dtR_KEITO_SHIIRE = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dtR_KEITO_SHIIRE.Rows.Count > 0)
            {
                decimal sirKin = 0;
                decimal sirSuryo = 0;
                decimal sirShohizei = 0;
                foreach (DataRow dr in dtR_KEITO_SHIIRE.Rows)
                {
                    if (zeiKbn == "税抜き")
                    {
                        sirKin = Util.ToDecimal(dr["SHIIRE_KINGAKU"]);
                        sirShohizei = Util.ToDecimal(dr["SHIIRE_SHOHIZEI"]);
                    }
                    else
                    {
                        sirKin = Util.ToDecimal(dr["SHIIRE_KINGAKU"]) + Util.ToDecimal(dr["SHIIRE_SHOHIZEI"]);
                        sirShohizei = Util.ToDecimal(dr["SHIIRE_SHOHIZEI"]);
                    }
                    // 四捨五入処理
                    sirSuryo = Util.Round(Util.ToDecimal(dr["SHIIRE_SURYO"]), 2);

                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("UPDATE PR_HN_TBL SET");
                    sql.Append("  ITEM10 = @ITEM10");
                    sql.Append(" ,ITEM11 = @ITEM11");
                    sql.Append(" ,ITEM19 = @ITEM19");
                    sql.Append(" WHERE");
                    sql.Append("  GUID = @GUID");
                    sql.Append(" AND");
                    sql.Append("  ITEM04 = @ITEM04");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(sirSuryo, 2));
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(sirKin));
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(sirShohizei));

                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                }
            }

            // 対象期間（会計期間最後の？）棚卸データ
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.KAISHA_CD AS KAISHA_CD,");
            sql.Append("     A.TANAOROSHI_DATE AS TANAOROSHI_DATE,");
            sql.Append("     A.SHOHIN_CD AS SHOHIN_CD,");
            sql.Append("     CASE WHEN A.IRISU <> 0 THEN ((A.IRISU * A.JITSU_SURYO1) + A.JITSU_SURYO2) ELSE A.JITSU_SURYO2 END AS SURYO2,");
            sql.Append("     A.KINGAKU AS KINGAKU ");
            sql.Append(" FROM");
            sql.Append("     TB_HN_TANAOROSHI AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B");
            sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" AND A.SHOHIN_CD = B.SHOHIN_CD");
            sql.Append(" AND B.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
            {
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD ");
            }
            sql.Append(" AND A.TANAOROSHI_DATE = @TANAOROSHI_DATE");
            sql.Append(" AND B.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO ");
            sql.Append(" AND B.SHOHIN_KUBUN5 <> 1");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, 10, tmpLastDay);
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, shohinKbnFr);
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, shohinKbnTo);

            DataTable dtZAIKO = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            /// 棚卸データがあれば実行する
            if (dtZAIKO.Rows.Count > 0)
            {
                decimal suryo = 0;
                decimal kin = 0;
                foreach (DataRow dr in dtZAIKO.Rows)
                {
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("UPDATE PR_HN_TBL SET");
                    sql.Append("  ITEM12 = @ITEM12");
                    sql.Append(" ,ITEM13 = @ITEM13");
                    sql.Append(" WHERE");
                    sql.Append("  GUID = @GUID");
                    sql.Append(" AND");
                    sql.Append("  ITEM04 = @ITEM04");

                    suryo = Util.ToDecimal(dr["SURYO2"]);
                    kin = Util.ToDecimal(dr["KINGAKU"]);

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);

                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(suryo, 2));
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(kin));

                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                }
            }

            #endregion

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        private void GetSettei()
        {
            // 使用する商品区分取得
            gSettei.kbn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "ShohinKbn"));
            // 在庫金額計算用単価
            gSettei.tanka = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "UseTanka"));
            // 棚卸表・記入票の合計印字設定
            gSettei.gokei = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "PrintGokei"));
            // 棚卸入力ソート順
            gSettei.sort = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "Sort"));

            //県漁連コード
            kenCode[0] = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI1"));
            kenCode[1] = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI2"));
            kenCode[2] = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI3"));
            kenCode[3] = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI4"));
            kenCode[4] = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI5"));
            kenCode[5] = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "SHIIRESAKI6"));

            for (int i=0; i< kenCode.Length; i++)
            {
                if(kenCode[i] != 0)
                {
                    if (i > 0) gParam += ",";
                    gParam += kenCode[i].ToString();
                }
            }
        }

        /// <summary>
        /// 棚卸日付以前の最終仕入単価取得
        /// </summary>
        /// <param name="tanaoroshiDate"></param>
        /// <param name="drShohin"></param>
        /// <returns></returns>
        private DataRow getSaishuSiireTanka(DateTime tanaoroshiDate, DataRow drShohin)
        {
            #region 最終仕入単価選択時 取得用SQL
            // の検索日付に発生しているデータを取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            Sql.Append(" EXEC SP_SAISHU_SHIIRE_TANKA @KAISHA_CD, @SHISHO_CD, @SHOHIN_CD, @TANAOROSHI_DATE");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            // 検索する棚卸日付をセット
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.VarChar, 10, tanaoroshiDate);
            // 検索する商品コードをセット
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 6, drShohin["SHOHIN_CD"]);
            DataTable dtSaishushiireTanka = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 最終仕入単価を取得
            DataRow drSaishushiireTanka;
            if (dtSaishushiireTanka.Rows.Count != 0)
            {
                drSaishushiireTanka = dtSaishushiireTanka.Rows[0];
            }
            else
            {
                drSaishushiireTanka = drShohin;
            }
            #endregion
            return drSaishushiireTanka;
        }

        #endregion
    }
}
