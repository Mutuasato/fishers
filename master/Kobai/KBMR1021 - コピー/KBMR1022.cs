﻿using jp.co.fsi.common.constants;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.kb.kbmr1021
{
    /// <summary>
    /// 棚卸初期設定(KOBR3032)
    /// </summary>
    public partial class KBMR1022 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// KOBR3032(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        KBMR1021 _pForm;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBMR1022()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }

        ///// <summary>
        ///// コンストラクタ
        ///// </summary>
        //public KBMR1022(KBMR1021 frm)
        //{
        //    this._pForm = frm;

        //    InitializeComponent();
        //    BindGotFocusEvent();
        //}
        #endregion

        #region protectedメソッド
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            this.pnlDebug.Visible = true;
            // タイトルは非表示
            this.lblTitle.Visible = false;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnEsc.Visible = true;
            this.btnF6.Visible = true;
            this.btnEsc.Enabled = true;
            this.btnF6.Enabled = true;

            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            // 画面の初期表示
            InitDisp();

            // 初期フォーカス
            this.rdoShohinKubun1.Focus();
            this.lblTitle.Visible = false;
            this.btnEsc.Visible = true;
            this.btnF6.Visible = true;
        }
        
        /// <summary>
        /// F6押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            if (Msg.ConfYesNo("更新しますか？") == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            // 更新処理
            SaveSettings();

            // 画面を閉じる
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 初期情報の表示
        /// </summary>
        private void InitDisp()
        {
            // 商品区分絞込み条件            
            int kbn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "ShohinKbn"));
            //switch (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBMR1021", "Setting", "ShohinKubun")))
            switch (kbn)
            {
                case 1:
                    this.rdoShohinKubun1.Checked = true;
                    break;

                case 2:
                    this.rdoShohinKubun2.Checked = true;
                    break;

                case 3:
                    this.rdoShohinBunrui3.Checked = true;
                    break;
            }
            // 棚卸金額計算単価         
            int tanka = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "UseTanka"));
            //switch (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBMR1021", "Setting", "TanaoroshiKingakuTanka")))
            switch (tanka)
            {
                case 0:
                    this.rdoShohinMasutaSireTanka.Checked = true;
                    break;

                case 1:
                    this.rdoSaishuSireTanka.Checked = true;
                    break;
            }
            // 棚卸表数量合計印字         
            int gokei = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "PrintGokei"));
            //switch (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBMR1021", "Setting", "TanaoroshiGoukeiInji")))
            switch (gokei)
            {
                case 0:
                    this.rdoInjiAri.Checked = true;
                    break;

                case 1:
                    this.rdoInjiNashi.Checked = true;
                    break;
            }
            // 棚卸入力ソート順
            int sort = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "Sort"));
            switch (sort)
            {
                case 1:
                    this.rdoSort1.Checked = true;
                    break;
                case 2:
                    this.rdoSort2.Checked = true;
                    break;
                case 3:
                    this.rdoSort3.Checked = true;
                    break;
                case 4:
                    this.rdoSort4.Checked = true;
                    break;
                default:
                    this.rdoSort1.Checked = true;
                    break;
            }

        }

        /// <summary>
        /// 設定を保存する
        /// </summary>
        private void SaveSettings()
        {
            // 商品区分絞込み条件            
            string shohinKubun = "";
            if (this.rdoShohinKubun1.Checked)
            {
                shohinKubun = "1";
            }
            else if (this.rdoShohinKubun2.Checked)
            {
                shohinKubun = "2";
            }
            else 
            {
                shohinKubun = "3";
            }
            //this.Config.SetPgConfig(Constants.SubSys.Kob, "KBMR1021", "Setting", "ShohinKubun", shohinKubun);
            this.Config.SetPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "ShohinKbn", shohinKubun);

            // 棚卸金額計算単価         
            string tanaoroshiTanka = "";
            if (this.rdoShohinMasutaSireTanka.Checked)
            {
                tanaoroshiTanka = "0";
            }
            else 
            {
                tanaoroshiTanka = "1";
            }
            //this.Config.SetPgConfig(Constants.SubSys.Kob, "KBMR1021", "Setting", "TanaoroshiKingakuTanka", tanaoroshiTanka);
            this.Config.SetPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "UseTanka", tanaoroshiTanka);

            // 棚卸表数量合計印字         
            string tanaoroshiInji = "";
            if (this.rdoInjiAri.Checked)
            {
                tanaoroshiInji = "0";
            }
            else
            {
                tanaoroshiInji = "1";
            }
            //this.Config.SetPgConfig(Constants.SubSys.Kob, "KBMR1021", "Setting", "TanaoroshiGoukeiInji", tanaoroshiInji);
            this.Config.SetPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "PrintGokei", tanaoroshiInji);

            // 棚卸入力ソート順設定保存
            string sort = "";
            if (this.rdoSort1.Checked)
            {
                sort = "1";
            }
            else if (this.rdoSort2.Checked)
            {
                sort = "2";
            }
            else if (this.rdoSort3.Checked)
            {
                sort = "3";
            }
            else if (this.rdoSort4.Checked)
            {
                sort = "4";
            }
            this.Config.SetPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "Sort", sort);

            // 設定を保存する
            this.Config.SaveConfig();
        }
        #endregion

    }
}
