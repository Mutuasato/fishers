﻿namespace jp.co.fsi.kb.kbmr1021
{
    /// <summary>
    /// KOBR3032R の概要の説明です。
    /// </summary>
    partial class KBMR1022R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBMR1022R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtJuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHyojiDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTanaban = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShohinmei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblIrisu = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSoko = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTyoboSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTanaorosiSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSai = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblZaikoKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKesusu01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBarasu01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKesusu02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBarasu02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKesusu03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBarasu03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblKikaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSaiKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTyoboKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.crossSectionBox1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox();
            this.crossSectionLine1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine2 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine3 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine4 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine5 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine6 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine7 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine8 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine9 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine10 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine11 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine12 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine13 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtTanaban = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIrisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSouko = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBarasu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBarasu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKikaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBarasu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZaikoKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyoboKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSaiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTotalIrisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalSouko = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalKesusu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalBarasu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalKesusu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalBarasu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalKesusu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalBarasu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalZaikoKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalTyoboKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalSaiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanaban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohinmei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIrisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSoko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyoboSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanaorosiSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZaikoKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKikaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSaiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyoboKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIrisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKikaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyoboKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIrisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSouko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalZaikoKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTyoboKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSaiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtCompanyName,
            this.lblTitle,
            this.label1,
            this.txtJuni,
            this.txtHyojiDate,
            this.txtTitle,
            this.lblTanaban,
            this.lblShohinmei,
            this.lblIrisu,
            this.lblSoko,
            this.lblTyoboSuryo,
            this.lblTanaorosiSuryo,
            this.lblSai,
            this.lblZaikoKingaku,
            this.lblKesusu01,
            this.lblBarasu01,
            this.lblKesusu02,
            this.lblBarasu02,
            this.lblKesusu03,
            this.lblBarasu03,
            this.line1,
            this.textBox2,
            this.line3,
            this.line13,
            this.lblKikaku,
            this.lblSaiKingaku,
            this.lblTyoboKingaku,
            this.crossSectionBox1,
            this.crossSectionLine1,
            this.crossSectionLine2,
            this.crossSectionLine3,
            this.crossSectionLine4,
            this.crossSectionLine5,
            this.crossSectionLine6,
            this.crossSectionLine7,
            this.crossSectionLine8,
            this.crossSectionLine9,
            this.crossSectionLine10,
            this.crossSectionLine11,
            this.crossSectionLine12,
            this.crossSectionLine13,
            this.reportInfo1});
            this.pageHeader.Height = 1.257087F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.2070866F;
            this.txtToday.Left = 8.161418F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtToday.Text = "ggyy年M月d日";
            this.txtToday.Top = 0.2192914F;
            this.txtToday.Visible = false;
            this.txtToday.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2070866F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.75118F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.2192914F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.2070866F;
            this.txtPageCount.Left = 10.50787F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.2192914F;
            this.txtPageCount.Width = 0.2433071F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM02";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0.04645669F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
            this.txtCompanyName.Text = "2";
            this.txtCompanyName.Top = 0.2295276F;
            this.txtCompanyName.Width = 4.036221F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.3968504F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 0.01181103F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: nor" +
    "mal; text-align: center";
            this.lblTitle.Text = "";
            this.lblTitle.Top = 0.8559958F;
            this.lblTitle.Width = 11.04331F;
            // 
            // label1
            // 
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 5.964174F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.label1.Text = "現在";
            this.label1.Top = 0.426378F;
            this.label1.Width = 0.3255906F;
            // 
            // txtJuni
            // 
            this.txtJuni.DataField = "ITEM04";
            this.txtJuni.Height = 0.2F;
            this.txtJuni.Left = 9.805119F;
            this.txtJuni.MultiLine = false;
            this.txtJuni.Name = "txtJuni";
            this.txtJuni.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.txtJuni.Text = "04";
            this.txtJuni.Top = 0.488189F;
            this.txtJuni.Width = 1.157086F;
            // 
            // txtHyojiDate
            // 
            this.txtHyojiDate.DataField = "ITEM03";
            this.txtHyojiDate.Height = 0.2F;
            this.txtHyojiDate.Left = 4.798819F;
            this.txtHyojiDate.Name = "txtHyojiDate";
            this.txtHyojiDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtHyojiDate.Text = "平成27年15月14日";
            this.txtHyojiDate.Top = 0.426378F;
            this.txtHyojiDate.Width = 1.165354F;
            // 
            // txtTitle
            // 
            this.txtTitle.DataField = "ITEM01";
            this.txtTitle.Height = 0.2464567F;
            this.txtTitle.Left = 4.645669F;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center";
            this.txtTitle.Text = "01";
            this.txtTitle.Top = 0F;
            this.txtTitle.Width = 1.764172F;
            // 
            // lblTanaban
            // 
            this.lblTanaban.Height = 0.2000002F;
            this.lblTanaban.HyperLink = null;
            this.lblTanaban.Left = 0.01181102F;
            this.lblTanaban.LineSpacing = 1F;
            this.lblTanaban.MultiLine = false;
            this.lblTanaban.Name = "lblTanaban";
            this.lblTanaban.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.lblTanaban.Text = "棚番";
            this.lblTanaban.Top = 0.9673229F;
            this.lblTanaban.Width = 0.3822835F;
            // 
            // lblShohinmei
            // 
            this.lblShohinmei.Height = 0.2000002F;
            this.lblShohinmei.HyperLink = null;
            this.lblShohinmei.Left = 0.3940945F;
            this.lblShohinmei.LineSpacing = 1F;
            this.lblShohinmei.MultiLine = false;
            this.lblShohinmei.Name = "lblShohinmei";
            this.lblShohinmei.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.lblShohinmei.Text = "商  品  名";
            this.lblShohinmei.Top = 0.9673229F;
            this.lblShohinmei.Width = 2.329529F;
            // 
            // lblIrisu
            // 
            this.lblIrisu.Height = 0.2F;
            this.lblIrisu.HyperLink = null;
            this.lblIrisu.Left = 4.38189F;
            this.lblIrisu.LineSpacing = 1F;
            this.lblIrisu.MultiLine = false;
            this.lblIrisu.Name = "lblIrisu";
            this.lblIrisu.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.lblIrisu.Text = "入数";
            this.lblIrisu.Top = 0.9673229F;
            this.lblIrisu.Width = 0.3114176F;
            // 
            // lblSoko
            // 
            this.lblSoko.Height = 0.2000002F;
            this.lblSoko.HyperLink = null;
            this.lblSoko.Left = 4.693307F;
            this.lblSoko.LineSpacing = 1F;
            this.lblSoko.MultiLine = false;
            this.lblSoko.Name = "lblSoko";
            this.lblSoko.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.lblSoko.Text = "倉庫";
            this.lblSoko.Top = 0.9673229F;
            this.lblSoko.Width = 0.3456693F;
            // 
            // lblTyoboSuryo
            // 
            this.lblTyoboSuryo.Height = 0.1968504F;
            this.lblTyoboSuryo.HyperLink = null;
            this.lblTyoboSuryo.Left = 5.039371F;
            this.lblTyoboSuryo.LineSpacing = 1F;
            this.lblTyoboSuryo.MultiLine = false;
            this.lblTyoboSuryo.Name = "lblTyoboSuryo";
            this.lblTyoboSuryo.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; text-justify: auto; vertical-align: bottom";
            this.lblTyoboSuryo.Text = "帳簿数量";
            this.lblTyoboSuryo.Top = 0.8559056F;
            this.lblTyoboSuryo.Width = 1.206299F;
            // 
            // lblTanaorosiSuryo
            // 
            this.lblTanaorosiSuryo.Height = 0.1968504F;
            this.lblTanaorosiSuryo.HyperLink = null;
            this.lblTanaorosiSuryo.Left = 6.991339F;
            this.lblTanaorosiSuryo.LineSpacing = 1F;
            this.lblTanaorosiSuryo.MultiLine = false;
            this.lblTanaorosiSuryo.Name = "lblTanaorosiSuryo";
            this.lblTanaorosiSuryo.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblTanaorosiSuryo.Text = "棚卸数量";
            this.lblTanaorosiSuryo.Top = 0.8559056F;
            this.lblTanaorosiSuryo.Width = 1.205905F;
            // 
            // lblSai
            // 
            this.lblSai.Height = 0.1968504F;
            this.lblSai.HyperLink = null;
            this.lblSai.Left = 8.225985F;
            this.lblSai.LineSpacing = 1F;
            this.lblSai.MultiLine = false;
            this.lblSai.Name = "lblSai";
            this.lblSai.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblSai.Text = "差    異";
            this.lblSai.Top = 0.8559056F;
            this.lblSai.Width = 1.246457F;
            // 
            // lblZaikoKingaku
            // 
            this.lblZaikoKingaku.Height = 0.2F;
            this.lblZaikoKingaku.HyperLink = null;
            this.lblZaikoKingaku.Left = 10.24449F;
            this.lblZaikoKingaku.LineSpacing = 1F;
            this.lblZaikoKingaku.MultiLine = false;
            this.lblZaikoKingaku.Name = "lblZaikoKingaku";
            this.lblZaikoKingaku.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.lblZaikoKingaku.Text = "在庫金額";
            this.lblZaikoKingaku.Top = 0.9673229F;
            this.lblZaikoKingaku.Width = 0.8106298F;
            // 
            // lblKesusu01
            // 
            this.lblKesusu01.Height = 0.2F;
            this.lblKesusu01.HyperLink = null;
            this.lblKesusu01.Left = 5.038977F;
            this.lblKesusu01.LineSpacing = 1F;
            this.lblKesusu01.MultiLine = false;
            this.lblKesusu01.Name = "lblKesusu01";
            this.lblKesusu01.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblKesusu01.Text = "ケース数";
            this.lblKesusu01.Top = 1.052756F;
            this.lblKesusu01.Width = 0.5708662F;
            // 
            // lblBarasu01
            // 
            this.lblBarasu01.Height = 0.2F;
            this.lblBarasu01.HyperLink = null;
            this.lblBarasu01.Left = 5.609843F;
            this.lblBarasu01.LineSpacing = 1F;
            this.lblBarasu01.MultiLine = false;
            this.lblBarasu01.Name = "lblBarasu01";
            this.lblBarasu01.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblBarasu01.Text = "バラ数";
            this.lblBarasu01.Top = 1.052756F;
            this.lblBarasu01.Width = 0.6413385F;
            // 
            // lblKesusu02
            // 
            this.lblKesusu02.Height = 0.2F;
            this.lblKesusu02.HyperLink = null;
            this.lblKesusu02.Left = 6.98504F;
            this.lblKesusu02.LineSpacing = 1F;
            this.lblKesusu02.MultiLine = false;
            this.lblKesusu02.Name = "lblKesusu02";
            this.lblKesusu02.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblKesusu02.Text = "ケース数";
            this.lblKesusu02.Top = 1.052756F;
            this.lblKesusu02.Width = 0.5708662F;
            // 
            // lblBarasu02
            // 
            this.lblBarasu02.Height = 0.2F;
            this.lblBarasu02.HyperLink = null;
            this.lblBarasu02.Left = 7.555906F;
            this.lblBarasu02.LineSpacing = 1F;
            this.lblBarasu02.MultiLine = false;
            this.lblBarasu02.Name = "lblBarasu02";
            this.lblBarasu02.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblBarasu02.Text = "バラ数";
            this.lblBarasu02.Top = 1.052756F;
            this.lblBarasu02.Width = 0.6622052F;
            // 
            // lblKesusu03
            // 
            this.lblKesusu03.Height = 0.2F;
            this.lblKesusu03.HyperLink = null;
            this.lblKesusu03.Left = 8.218111F;
            this.lblKesusu03.LineSpacing = 1F;
            this.lblKesusu03.MultiLine = false;
            this.lblKesusu03.Name = "lblKesusu03";
            this.lblKesusu03.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblKesusu03.Text = "ケース数";
            this.lblKesusu03.Top = 1.052756F;
            this.lblKesusu03.Width = 0.5708662F;
            // 
            // lblBarasu03
            // 
            this.lblBarasu03.Height = 0.2F;
            this.lblBarasu03.HyperLink = null;
            this.lblBarasu03.Left = 8.788977F;
            this.lblBarasu03.LineSpacing = 1F;
            this.lblBarasu03.MultiLine = false;
            this.lblBarasu03.Name = "lblBarasu03";
            this.lblBarasu03.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblBarasu03.Text = "バラ数";
            this.lblBarasu03.Top = 1.052756F;
            this.lblBarasu03.Width = 0.683465F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 4.645669F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2464567F;
            this.line1.Width = 1.764174F;
            this.line1.X1 = 4.645669F;
            this.line1.X2 = 6.409843F;
            this.line1.Y1 = 0.2464567F;
            this.line1.Y2 = 0.2464567F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM04";
            this.textBox2.Height = 0.2070866F;
            this.textBox2.Left = 7.556694F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ ゴシック; font-size: 11.25pt";
            this.textBox2.Text = "123456";
            this.textBox2.Top = 0.2192914F;
            this.textBox2.Visible = false;
            this.textBox2.Width = 0.6047244F;
            // 
            // line3
            // 
            this.line3.Height = 9.536743E-07F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 1.252756F;
            this.line3.Width = 11.05512F;
            this.line3.X1 = 0F;
            this.line3.X2 = 11.05512F;
            this.line3.Y1 = 1.252757F;
            this.line3.Y2 = 1.252756F;
            // 
            // line13
            // 
            this.line13.Height = 0F;
            this.line13.Left = 5.039371F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 1.052756F;
            this.line13.Width = 4.433071F;
            this.line13.X1 = 5.039371F;
            this.line13.X2 = 9.472442F;
            this.line13.Y1 = 1.052756F;
            this.line13.Y2 = 1.052756F;
            // 
            // lblKikaku
            // 
            this.lblKikaku.Height = 0.2000002F;
            this.lblKikaku.HyperLink = null;
            this.lblKikaku.Left = 2.723622F;
            this.lblKikaku.LineSpacing = 1F;
            this.lblKikaku.MultiLine = false;
            this.lblKikaku.Name = "lblKikaku";
            this.lblKikaku.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.lblKikaku.Text = "規 格";
            this.lblKikaku.Top = 0.9673229F;
            this.lblKikaku.Width = 1.646457F;
            // 
            // lblSaiKingaku
            // 
            this.lblSaiKingaku.Height = 0.2F;
            this.lblSaiKingaku.HyperLink = null;
            this.lblSaiKingaku.Left = 9.472442F;
            this.lblSaiKingaku.LineSpacing = 1F;
            this.lblSaiKingaku.MultiLine = false;
            this.lblSaiKingaku.Name = "lblSaiKingaku";
            this.lblSaiKingaku.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.lblSaiKingaku.Text = "差異金額";
            this.lblSaiKingaku.Top = 0.9673229F;
            this.lblSaiKingaku.Width = 0.7720461F;
            // 
            // lblTyoboKingaku
            // 
            this.lblTyoboKingaku.Height = 0.2F;
            this.lblTyoboKingaku.HyperLink = null;
            this.lblTyoboKingaku.Left = 6.251575F;
            this.lblTyoboKingaku.LineSpacing = 1F;
            this.lblTyoboKingaku.MultiLine = false;
            this.lblTyoboKingaku.Name = "lblTyoboKingaku";
            this.lblTyoboKingaku.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.lblTyoboKingaku.Text = "帳簿金額";
            this.lblTyoboKingaku.Top = 0.9673229F;
            this.lblTyoboKingaku.Width = 0.7397637F;
            // 
            // crossSectionBox1
            // 
            this.crossSectionBox1.Bottom = 0F;
            this.crossSectionBox1.Left = 0F;
            this.crossSectionBox1.LineWeight = 1F;
            this.crossSectionBox1.Name = "crossSectionBox1";
            this.crossSectionBox1.Right = 11.05906F;
            this.crossSectionBox1.Top = 0.8559058F;
            // 
            // crossSectionLine1
            // 
            this.crossSectionLine1.Bottom = 0.1788223F;
            this.crossSectionLine1.Left = 0.4259843F;
            this.crossSectionLine1.LineWeight = 1F;
            this.crossSectionLine1.Name = "crossSectionLine1";
            this.crossSectionLine1.Top = 0.8559056F;
            // 
            // crossSectionLine2
            // 
            this.crossSectionLine2.Bottom = 0.1163223F;
            this.crossSectionLine2.Left = 10.24449F;
            this.crossSectionLine2.LineWeight = 1F;
            this.crossSectionLine2.Name = "crossSectionLine2";
            this.crossSectionLine2.Top = 0.8559056F;
            // 
            // crossSectionLine3
            // 
            this.crossSectionLine3.Bottom = 0.1788223F;
            this.crossSectionLine3.Left = 2.723622F;
            this.crossSectionLine3.LineWeight = 1F;
            this.crossSectionLine3.Name = "crossSectionLine3";
            this.crossSectionLine3.Top = 0.8559056F;
            // 
            // crossSectionLine4
            // 
            this.crossSectionLine4.Bottom = 0.1788223F;
            this.crossSectionLine4.Left = 9.472442F;
            this.crossSectionLine4.LineWeight = 1F;
            this.crossSectionLine4.Name = "crossSectionLine4";
            this.crossSectionLine4.Top = 0.8559056F;
            // 
            // crossSectionLine5
            // 
            this.crossSectionLine5.Bottom = 0.3378773F;
            this.crossSectionLine5.Left = 8.788977F;
            this.crossSectionLine5.LineWeight = 1F;
            this.crossSectionLine5.Name = "crossSectionLine5";
            this.crossSectionLine5.Top = 1.052756F;
            // 
            // crossSectionLine6
            // 
            this.crossSectionLine6.Bottom = 0.1882711F;
            this.crossSectionLine6.Left = 8.218111F;
            this.crossSectionLine6.LineWeight = 1F;
            this.crossSectionLine6.Name = "crossSectionLine6";
            this.crossSectionLine6.Top = 0.8653544F;
            // 
            // crossSectionLine7
            // 
            this.crossSectionLine7.Bottom = 0.2902396F;
            this.crossSectionLine7.Left = 7.555906F;
            this.crossSectionLine7.LineWeight = 1F;
            this.crossSectionLine7.Name = "crossSectionLine7";
            this.crossSectionLine7.Top = 1.052756F;
            // 
            // crossSectionLine8
            // 
            this.crossSectionLine8.Bottom = 0.1788223F;
            this.crossSectionLine8.Left = 6.98504F;
            this.crossSectionLine8.LineWeight = 1F;
            this.crossSectionLine8.Name = "crossSectionLine8";
            this.crossSectionLine8.Top = 0.8559056F;
            // 
            // crossSectionLine9
            // 
            this.crossSectionLine9.Bottom = 0.1788223F;
            this.crossSectionLine9.Left = 6.251575F;
            this.crossSectionLine9.LineWeight = 1F;
            this.crossSectionLine9.Name = "crossSectionLine9";
            this.crossSectionLine9.Top = 0.8559056F;
            // 
            // crossSectionLine10
            // 
            this.crossSectionLine10.Bottom = 0.2902396F;
            this.crossSectionLine10.Left = 5.609843F;
            this.crossSectionLine10.LineWeight = 1F;
            this.crossSectionLine10.Name = "crossSectionLine10";
            this.crossSectionLine10.Top = 1.052756F;
            // 
            // crossSectionLine11
            // 
            this.crossSectionLine11.Bottom = 0.1882711F;
            this.crossSectionLine11.Left = 4.693307F;
            this.crossSectionLine11.LineWeight = 1F;
            this.crossSectionLine11.Name = "crossSectionLine11";
            this.crossSectionLine11.Top = 0.8653544F;
            // 
            // crossSectionLine12
            // 
            this.crossSectionLine12.Bottom = 0.1788223F;
            this.crossSectionLine12.Left = 4.38189F;
            this.crossSectionLine12.LineWeight = 1F;
            this.crossSectionLine12.Name = "crossSectionLine12";
            this.crossSectionLine12.Top = 0.8559056F;
            // 
            // crossSectionLine13
            // 
            this.crossSectionLine13.Bottom = 0.1788223F;
            this.crossSectionLine13.Left = 5.038977F;
            this.crossSectionLine13.LineWeight = 1F;
            this.crossSectionLine13.Name = "crossSectionLine13";
            this.crossSectionLine13.Top = 0.8559056F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.2070866F;
            this.reportInfo1.Left = 9.342521F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
            this.reportInfo1.Top = 0.2192914F;
            this.reportInfo1.Width = 1.165354F;
            // 
            // line6
            // 
            this.line6.Height = 0.2992126F;
            this.line6.Left = 9.472442F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 9.472442F;
            this.line6.X2 = 9.472442F;
            this.line6.Y1 = 0.2992126F;
            this.line6.Y2 = 0F;
            // 
            // line8
            // 
            this.line8.Height = 0.299212F;
            this.line8.Left = 7.555906F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 5.662441E-07F;
            this.line8.Width = 0F;
            this.line8.X1 = 7.555906F;
            this.line8.X2 = 7.555906F;
            this.line8.Y1 = 0.2992126F;
            this.line8.Y2 = 5.662441E-07F;
            // 
            // line9
            // 
            this.line9.Height = 0.2992126F;
            this.line9.Left = 6.98504F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 6.98504F;
            this.line9.X2 = 6.98504F;
            this.line9.Y1 = 0.2992126F;
            this.line9.Y2 = 0F;
            // 
            // line11
            // 
            this.line11.Height = 0.2992126F;
            this.line11.Left = 4.38189F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 4.38189F;
            this.line11.X2 = 4.38189F;
            this.line11.Y1 = 0.2992126F;
            this.line11.Y2 = 0F;
            // 
            // line16
            // 
            this.line16.Height = 0.2992126F;
            this.line16.Left = 10.24449F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 10.24449F;
            this.line16.X2 = 10.24449F;
            this.line16.Y1 = 0.2992126F;
            this.line16.Y2 = 0F;
            // 
            // line10
            // 
            this.line10.Height = 0.2992126F;
            this.line10.Left = 6.251575F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 6.251575F;
            this.line10.X2 = 6.251575F;
            this.line10.Y1 = 0.2992126F;
            this.line10.Y2 = 0F;
            // 
            // line7
            // 
            this.line7.Height = 0.2992126F;
            this.line7.Left = 8.218111F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 8.218111F;
            this.line7.X2 = 8.218111F;
            this.line7.Y1 = 0.2992126F;
            this.line7.Y2 = 0F;
            // 
            // line17
            // 
            this.line17.Height = 0.2992126F;
            this.line17.Left = 8.788977F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0F;
            this.line17.Width = 0F;
            this.line17.X1 = 8.788977F;
            this.line17.X2 = 8.788977F;
            this.line17.Y1 = 0.2992126F;
            this.line17.Y2 = 0F;
            // 
            // line18
            // 
            this.line18.Height = 0.2992126F;
            this.line18.Left = 5.609843F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 5.609843F;
            this.line18.X2 = 5.609843F;
            this.line18.Y1 = 0.2992126F;
            this.line18.Y2 = 0F;
            // 
            // line19
            // 
            this.line19.Height = 0.2992126F;
            this.line19.Left = 4.693307F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0F;
            this.line19.Width = 0F;
            this.line19.X1 = 4.693307F;
            this.line19.X2 = 4.693307F;
            this.line19.Y1 = 0.2992126F;
            this.line19.Y2 = 0F;
            // 
            // line12
            // 
            this.line12.Height = 0.2992126F;
            this.line12.Left = 5.039371F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 5.039371F;
            this.line12.X2 = 5.039371F;
            this.line12.Y1 = 0.2992126F;
            this.line12.Y2 = 0F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTanaban,
            this.txtShohinCd,
            this.txtIrisu,
            this.txtSouko,
            this.txtKesusu01,
            this.txtBarasu01,
            this.txtKesusu02,
            this.txtBarasu02,
            this.txtShohinNm,
            this.txtKikaku,
            this.txtBarasu03,
            this.txtKesusu03,
            this.txtZaikoKingaku,
            this.txtTyoboKingaku,
            this.txtSaiKingaku,
            this.line25,
            this.line14});
            this.detail.Height = 0.3188976F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // txtTanaban
            // 
            this.txtTanaban.DataField = "ITEM05";
            this.txtTanaban.Height = 0.1688976F;
            this.txtTanaban.Left = 0.03464484F;
            this.txtTanaban.MultiLine = false;
            this.txtTanaban.Name = "txtTanaban";
            this.txtTanaban.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left";
            this.txtTanaban.Text = "5";
            this.txtTanaban.Top = 0.08346456F;
            this.txtTanaban.Width = 0.3913386F;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.DataField = "ITEM06";
            this.txtShohinCd.Height = 0.1688976F;
            this.txtShohinCd.Left = 0.5094488F;
            this.txtShohinCd.MultiLine = false;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.txtShohinCd.Text = "6";
            this.txtShohinCd.Top = 0F;
            this.txtShohinCd.Width = 0.5366142F;
            // 
            // txtIrisu
            // 
            this.txtIrisu.DataField = "ITEM09";
            this.txtIrisu.Height = 0.1688976F;
            this.txtIrisu.Left = 4.433858F;
            this.txtIrisu.MultiLine = false;
            this.txtIrisu.Name = "txtIrisu";
            this.txtIrisu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtIrisu.Text = "9";
            this.txtIrisu.Top = 0.08346457F;
            this.txtIrisu.Width = 0.259449F;
            // 
            // txtSouko
            // 
            this.txtSouko.DataField = "ITEM10";
            this.txtSouko.Height = 0.1688976F;
            this.txtSouko.Left = 4.693701F;
            this.txtSouko.Name = "txtSouko";
            this.txtSouko.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtSouko.Text = "10";
            this.txtSouko.Top = 0.08346457F;
            this.txtSouko.Width = 0.3456693F;
            // 
            // txtKesusu01
            // 
            this.txtKesusu01.DataField = "ITEM11";
            this.txtKesusu01.Height = 0.1688976F;
            this.txtKesusu01.Left = 5.039371F;
            this.txtKesusu01.MultiLine = false;
            this.txtKesusu01.Name = "txtKesusu01";
            this.txtKesusu01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtKesusu01.Text = "11";
            this.txtKesusu01.Top = 0.08346457F;
            this.txtKesusu01.Width = 0.5318899F;
            // 
            // txtBarasu01
            // 
            this.txtBarasu01.DataField = "ITEM12";
            this.txtBarasu01.Height = 0.1688976F;
            this.txtBarasu01.Left = 5.609843F;
            this.txtBarasu01.Name = "txtBarasu01";
            this.txtBarasu01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtBarasu01.Text = "12,000.00";
            this.txtBarasu01.Top = 0.08346457F;
            this.txtBarasu01.Width = 0.6417327F;
            // 
            // txtKesusu02
            // 
            this.txtKesusu02.DataField = "ITEM14";
            this.txtKesusu02.Height = 0.1688976F;
            this.txtKesusu02.Left = 6.991339F;
            this.txtKesusu02.Name = "txtKesusu02";
            this.txtKesusu02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtKesusu02.Text = "14";
            this.txtKesusu02.Top = 0.08346457F;
            this.txtKesusu02.Width = 0.5110235F;
            // 
            // txtBarasu02
            // 
            this.txtBarasu02.DataField = "ITEM15";
            this.txtBarasu02.Height = 0.1688976F;
            this.txtBarasu02.Left = 7.555906F;
            this.txtBarasu02.Name = "txtBarasu02";
            this.txtBarasu02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtBarasu02.Text = "15,000.00";
            this.txtBarasu02.Top = 0.08346457F;
            this.txtBarasu02.Width = 0.6413388F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.DataField = "ITEM07";
            this.txtShohinNm.Height = 0.1460631F;
            this.txtShohinNm.Left = 0.5094488F;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left";
            this.txtShohinNm.Text = "7";
            this.txtShohinNm.Top = 0.1688976F;
            this.txtShohinNm.Width = 2.141339F;
            // 
            // txtKikaku
            // 
            this.txtKikaku.DataField = "ITEM08";
            this.txtKikaku.Height = 0.1688976F;
            this.txtKikaku.Left = 2.775591F;
            this.txtKikaku.Name = "txtKikaku";
            this.txtKikaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left";
            this.txtKikaku.Text = "8";
            this.txtKikaku.Top = 0.08346457F;
            this.txtKikaku.Width = 1.594488F;
            // 
            // txtBarasu03
            // 
            this.txtBarasu03.DataField = "ITEM17";
            this.txtBarasu03.Height = 0.1688976F;
            this.txtBarasu03.Left = 8.788977F;
            this.txtBarasu03.Name = "txtBarasu03";
            this.txtBarasu03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtBarasu03.Text = "17.000.00";
            this.txtBarasu03.Top = 0.08346457F;
            this.txtBarasu03.Width = 0.6523628F;
            // 
            // txtKesusu03
            // 
            this.txtKesusu03.DataField = "ITEM16";
            this.txtKesusu03.Height = 0.1688976F;
            this.txtKesusu03.Left = 8.246457F;
            this.txtKesusu03.Name = "txtKesusu03";
            this.txtKesusu03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtKesusu03.Text = "16";
            this.txtKesusu03.Top = 0.08346457F;
            this.txtKesusu03.Width = 0.5216541F;
            // 
            // txtZaikoKingaku
            // 
            this.txtZaikoKingaku.DataField = "ITEM19";
            this.txtZaikoKingaku.Height = 0.1688976F;
            this.txtZaikoKingaku.Left = 10.24449F;
            this.txtZaikoKingaku.Name = "txtZaikoKingaku";
            this.txtZaikoKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtZaikoKingaku.Text = "19,123,000";
            this.txtZaikoKingaku.Top = 0.08346457F;
            this.txtZaikoKingaku.Width = 0.7610235F;
            // 
            // txtTyoboKingaku
            // 
            this.txtTyoboKingaku.DataField = "ITEM13";
            this.txtTyoboKingaku.Height = 0.1688976F;
            this.txtTyoboKingaku.Left = 6.272441F;
            this.txtTyoboKingaku.Name = "txtTyoboKingaku";
            this.txtTyoboKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.txtTyoboKingaku.Text = "13,000,000";
            this.txtTyoboKingaku.Top = 0.08346457F;
            this.txtTyoboKingaku.Width = 0.7188978F;
            // 
            // txtSaiKingaku
            // 
            this.txtSaiKingaku.DataField = "ITEM18";
            this.txtSaiKingaku.Height = 0.1688976F;
            this.txtSaiKingaku.Left = 9.472442F;
            this.txtSaiKingaku.Name = "txtSaiKingaku";
            this.txtSaiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.txtSaiKingaku.Text = "18,123,000";
            this.txtSaiKingaku.Top = 0.08346457F;
            this.txtSaiKingaku.Width = 0.7409449F;
            // 
            // line25
            // 
            this.line25.Height = 8.940697E-08F;
            this.line25.Left = 0F;
            this.line25.LineWeight = 2F;
            this.line25.Name = "line25";
            this.line25.Top = 0.3149606F;
            this.line25.Width = 11.05512F;
            this.line25.X1 = 0F;
            this.line25.X2 = 11.05512F;
            this.line25.Y1 = 0.3149606F;
            this.line25.Y2 = 0.3149607F;
            // 
            // line14
            // 
            this.line14.Height = 0F;
            this.line14.Left = 0F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0.3149607F;
            this.line14.Visible = false;
            this.line14.Width = 11.05512F;
            this.line14.X1 = 0F;
            this.line14.X2 = 11.05512F;
            this.line14.Y1 = 0.3149607F;
            this.line14.Y2 = 0.3149607F;
            // 
            // pageFooter
            // 
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line23});
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // line23
            // 
            this.line23.Height = 0.3377953F;
            this.line23.Left = 10.50787F;
            this.line23.LineWeight = 3F;
            this.line23.Name = "line23";
            this.line23.Top = 0F;
            this.line23.Width = 0F;
            this.line23.X1 = 10.50787F;
            this.line23.X2 = 10.50787F;
            this.line23.Y1 = 0F;
            this.line23.Y2 = 0.3377953F;
            // 
            // txtTotalIrisu
            // 
            this.txtTotalIrisu.DataField = "ITEM20";
            this.txtTotalIrisu.Height = 0.1688976F;
            this.txtTotalIrisu.Left = 4.433858F;
            this.txtTotalIrisu.Name = "txtTotalIrisu";
            this.txtTotalIrisu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtTotalIrisu.Text = "20";
            this.txtTotalIrisu.Top = 0.07716536F;
            this.txtTotalIrisu.Width = 0.2594485F;
            // 
            // txtTotalSouko
            // 
            this.txtTotalSouko.DataField = "ITEM21";
            this.txtTotalSouko.Height = 0.1688976F;
            this.txtTotalSouko.Left = 4.693701F;
            this.txtTotalSouko.Name = "txtTotalSouko";
            this.txtTotalSouko.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtTotalSouko.Text = "21";
            this.txtTotalSouko.Top = 0.07716536F;
            this.txtTotalSouko.Width = 0.3456692F;
            // 
            // txtTotalKesusu01
            // 
            this.txtTotalKesusu01.DataField = "ITEM22";
            this.txtTotalKesusu01.Height = 0.1688976F;
            this.txtTotalKesusu01.Left = 5.039371F;
            this.txtTotalKesusu01.Name = "txtTotalKesusu01";
            this.txtTotalKesusu01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtTotalKesusu01.Text = "22";
            this.txtTotalKesusu01.Top = 0.07716536F;
            this.txtTotalKesusu01.Width = 0.5318899F;
            // 
            // txtTotalBarasu01
            // 
            this.txtTotalBarasu01.DataField = "ITEM23";
            this.txtTotalBarasu01.Height = 0.1688976F;
            this.txtTotalBarasu01.Left = 5.609843F;
            this.txtTotalBarasu01.MultiLine = false;
            this.txtTotalBarasu01.Name = "txtTotalBarasu01";
            this.txtTotalBarasu01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtTotalBarasu01.Text = "23";
            this.txtTotalBarasu01.Top = 0.07716536F;
            this.txtTotalBarasu01.Width = 0.6417322F;
            // 
            // txtTotalKesusu02
            // 
            this.txtTotalKesusu02.DataField = "ITEM25";
            this.txtTotalKesusu02.Height = 0.1688976F;
            this.txtTotalKesusu02.Left = 6.991339F;
            this.txtTotalKesusu02.MultiLine = false;
            this.txtTotalKesusu02.Name = "txtTotalKesusu02";
            this.txtTotalKesusu02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtTotalKesusu02.Text = "25";
            this.txtTotalKesusu02.Top = 0.07716536F;
            this.txtTotalKesusu02.Width = 0.5110235F;
            // 
            // txtTotalBarasu02
            // 
            this.txtTotalBarasu02.DataField = "ITEM26";
            this.txtTotalBarasu02.Height = 0.1688976F;
            this.txtTotalBarasu02.Left = 7.555906F;
            this.txtTotalBarasu02.MultiLine = false;
            this.txtTotalBarasu02.Name = "txtTotalBarasu02";
            this.txtTotalBarasu02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtTotalBarasu02.Text = "26";
            this.txtTotalBarasu02.Top = 0.07716536F;
            this.txtTotalBarasu02.Width = 0.6413388F;
            // 
            // txtTotalKesusu03
            // 
            this.txtTotalKesusu03.DataField = "ITEM27";
            this.txtTotalKesusu03.Height = 0.1688976F;
            this.txtTotalKesusu03.Left = 8.246457F;
            this.txtTotalKesusu03.MultiLine = false;
            this.txtTotalKesusu03.Name = "txtTotalKesusu03";
            this.txtTotalKesusu03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtTotalKesusu03.Text = "27";
            this.txtTotalKesusu03.Top = 0.07716536F;
            this.txtTotalKesusu03.Width = 0.5216551F;
            // 
            // txtTotalBarasu03
            // 
            this.txtTotalBarasu03.DataField = "ITEM28";
            this.txtTotalBarasu03.Height = 0.1688976F;
            this.txtTotalBarasu03.Left = 8.788977F;
            this.txtTotalBarasu03.MultiLine = false;
            this.txtTotalBarasu03.Name = "txtTotalBarasu03";
            this.txtTotalBarasu03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtTotalBarasu03.Text = "28";
            this.txtTotalBarasu03.Top = 0.07716536F;
            this.txtTotalBarasu03.Width = 0.6523628F;
            // 
            // txtTotalZaikoKingaku
            // 
            this.txtTotalZaikoKingaku.DataField = "ITEM30";
            this.txtTotalZaikoKingaku.Height = 0.1688976F;
            this.txtTotalZaikoKingaku.Left = 10.24449F;
            this.txtTotalZaikoKingaku.MultiLine = false;
            this.txtTotalZaikoKingaku.Name = "txtTotalZaikoKingaku";
            this.txtTotalZaikoKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtTotalZaikoKingaku.Text = "30";
            this.txtTotalZaikoKingaku.Top = 0.07716536F;
            this.txtTotalZaikoKingaku.Width = 0.7610235F;
            // 
            // txtTotalTyoboKingaku
            // 
            this.txtTotalTyoboKingaku.DataField = "ITEM24";
            this.txtTotalTyoboKingaku.Height = 0.1688976F;
            this.txtTotalTyoboKingaku.Left = 6.272441F;
            this.txtTotalTyoboKingaku.MultiLine = false;
            this.txtTotalTyoboKingaku.Name = "txtTotalTyoboKingaku";
            this.txtTotalTyoboKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.txtTotalTyoboKingaku.Text = "24";
            this.txtTotalTyoboKingaku.Top = 0.07716536F;
            this.txtTotalTyoboKingaku.Width = 0.7125983F;
            // 
            // txtTotalSaiKingaku
            // 
            this.txtTotalSaiKingaku.DataField = "ITEM29";
            this.txtTotalSaiKingaku.Height = 0.1688976F;
            this.txtTotalSaiKingaku.Left = 9.472442F;
            this.txtTotalSaiKingaku.MultiLine = false;
            this.txtTotalSaiKingaku.Name = "txtTotalSaiKingaku";
            this.txtTotalSaiKingaku.OutputFormat = resources.GetString("txtTotalSaiKingaku.OutputFormat");
            this.txtTotalSaiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.txtTotalSaiKingaku.Text = "29";
            this.txtTotalSaiKingaku.Top = 0.07716536F;
            this.txtTotalSaiKingaku.Width = 0.7409449F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1688976F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 3.280709F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: right; vertical-align: bottom" +
    "";
            this.lblTotal.Text = "【合    計】";
            this.lblTotal.Top = 0.07716536F;
            this.lblTotal.Width = 1.101181F;
            // 
            // line22
            // 
            this.line22.Height = 0.3149606F;
            this.line22.Left = 0.007874016F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0F;
            this.line22.Width = 0F;
            this.line22.X1 = 0.007874016F;
            this.line22.X2 = 0.007874016F;
            this.line22.Y1 = 0F;
            this.line22.Y2 = 0.3149606F;
            // 
            // line24
            // 
            this.line24.Height = 0F;
            this.line24.Left = 0F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.3149607F;
            this.line24.Width = 11.03898F;
            this.line24.X1 = 0F;
            this.line24.X2 = 11.03898F;
            this.line24.Y1 = 0.3149607F;
            this.line24.Y2 = 0.3149607F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotalIrisu,
            this.txtTotalSouko,
            this.txtTotalKesusu01,
            this.txtTotalBarasu01,
            this.txtTotalKesusu02,
            this.txtTotalBarasu02,
            this.txtTotalKesusu03,
            this.txtTotalBarasu03,
            this.txtTotalZaikoKingaku,
            this.txtTotalTyoboKingaku,
            this.txtTotalSaiKingaku,
            this.lblTotal,
            this.line22,
            this.line24,
            this.line2,
            this.line12,
            this.line19,
            this.line11,
            this.line18,
            this.line10,
            this.line9,
            this.line8,
            this.line7,
            this.line17,
            this.line6,
            this.line16,
            this.line4,
            this.line5});
            this.reportFooter1.Height = 0.3188976F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // line2
            // 
            this.line2.Height = 0.2992126F;
            this.line2.Left = 11.05118F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0F;
            this.line2.Width = 0F;
            this.line2.X1 = 11.05118F;
            this.line2.X2 = 11.05118F;
            this.line2.Y1 = 0F;
            this.line2.Y2 = 0.2992126F;
            // 
            // line4
            // 
            this.line4.Height = 0.2992126F;
            this.line4.Left = 2.723622F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 2.723622F;
            this.line4.X2 = 2.723622F;
            this.line4.Y1 = 0.2992126F;
            this.line4.Y2 = 0F;
            // 
            // line5
            // 
            this.line5.Height = 0.2992126F;
            this.line5.Left = 0.4259843F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 0.4259843F;
            this.line5.X2 = 0.4259843F;
            this.line5.Y1 = 0.2992126F;
            this.line5.Y2 = 0F;
            // 
            // KOBR3032R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.6692914F;
            this.PageSettings.Margins.Left = 0.3149606F;
            this.PageSettings.Margins.Right = 0.3149606F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.05906F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanaban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohinmei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIrisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSoko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyoboSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanaorosiSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZaikoKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKikaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSaiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyoboKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIrisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKikaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyoboKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIrisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSouko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalZaikoKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTyoboKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSaiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJuni;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanaban;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShohinmei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblIrisu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSoko;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTyoboSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanaorosiSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSai;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblZaikoKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKesusu01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBarasu01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKesusu02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBarasu02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKesusu03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBarasu03;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyoboKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTyoboKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalSaiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKikaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSaiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTyoboKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanaban;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIrisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSouko;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBarasu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBarasu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKikaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBarasu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZaikoKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalIrisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalSouko;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalKesusu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBarasu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalKesusu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBarasu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalKesusu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBarasu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalZaikoKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox crossSectionBox1;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine1;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine2;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine3;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine4;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine5;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine6;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine7;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine8;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine9;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine10;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine11;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine12;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine13;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
    }
}
