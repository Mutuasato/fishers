﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.kb.kbde1021
{
    /// <summary>
    /// 仕入単価参照(KOBE1013)
    /// </summary>
    public partial class KBDE1023 : BasePgForm
    {

        #region private変数
        /// <summary>
        /// KOBE1013(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        Decimal _pTOKUISAKI_CD;
        Decimal _pSEIKYUSAKI_CD;
        Decimal _pSHOHIN_CD;
        // 支所コード
        private int ShishoCode;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDE1023(Decimal TOKUISAKI_CD, Decimal SEIKYUSAKI_CD, Decimal SHOHIN_CD)
        {
            this._pTOKUISAKI_CD = TOKUISAKI_CD;
            this._pSEIKYUSAKI_CD = SEIKYUSAKI_CD;
            this._pSHOHIN_CD = SHOHIN_CD;

            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();

        }

        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            // タイトルは非表示
            this.lblTitle.Visible = false;

            this.btnF2.Visible = false;
            this.btnF3.Enabled = true;
            this.btnF3.Visible = true;
            this.btnF3.Location = this.btnF2.Location;
            this.btnF3.Text = this.btnF2.Text;
            this.btnF3.Text = this.btnF3.Text.Replace("F2", "F3");
            this.btnF3.Enabled = false;

            // 商品名取得
            this.lblShohinNm.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_SHIIRE", this.ShishoCode.ToString(), this._pSHOHIN_CD.ToString());

            // データ取得のSQLを発行してGridに反映
            DataTable dtList = new DataTable();
            try
            {
                dtList = this.GetTB_HN_SHIIRE_TANKA_RIREKI();
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                return;
            }

            // 取得したデータを表示用に編集
            DataTable dtDsp = this.EditDataList(dtList);

            this.dgvList.DataSource = dtDsp;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 9F);

            // 列幅を設定する
            this.dgvList.Columns[0].Visible = false;
            this.dgvList.Columns[1].Width = 100;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[2].Width = 148;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                default:
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // 未使用
            return;
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
                return;

            // 以下は不要？
            //// 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            //if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            //{
            //    Msg.Error("この会計年度は凍結されています。");
            //    return;
            //}

            // 仕入単価削除
            this.DeleteData();
            this.InitForm();
        }

        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.ReturnVal();
        }

        /// <summary>
        /// グリッドの行フォーカス時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (Util.ToDecimal(this.dgvList[0, e.RowIndex].Value) > 2)
            {
                this.btnF3.Enabled = true;
            }
            else
            {
                this.btnF3.Enabled = false;
            }
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            this.ReturnVal();
        }
        
        #endregion

        #region privateメソッド
        /// <summary>
        /// 仕入単価のデータを取得
        /// </summary>
        /// <returns>仕入単価の取得したデータ</returns>
        private DataTable GetTB_HN_SHIIRE_TANKA_RIREKI()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("    CAST((YEAR(A.SAISHU_TORIHIKI_DATE) + 10000) + (MONTH(A.SAISHU_TORIHIKI_DATE) + 100) + (DAY(A.SAISHU_TORIHIKI_DATE)) AS DECIMAL(8)) AS 順位,");
            sql.Append("    CONVERT(VARCHAR(8), A.SAISHU_TORIHIKI_DATE, 11) AS 取引日付,");
            sql.Append("    A.TANKA AS 単価 ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_SHIIRE_TANKA_RIREKI AS A ");
            sql.Append("WHERE ");
            sql.Append("        A.KAISHA_CD     = @KAISHA_CD ");
            sql.Append("    AND A.SHISHO_CD     = @SHISHO_CD ");
            //sql.Append("    AND A.SEIKYUSAKI_CD = 0 ");
            sql.Append("    AND A.SEIKYUSAKI_CD = @SEIKYUSAKI_CD ");
            sql.Append("    AND A.TOKUISAKI_CD  = @TOKUISAKI_CD ");
            sql.Append("    AND A.SHOHIN_CD     = @SHOHIN_CD ");
            //sql.Append("    AND A.KAIKEI_NENDO  = @KAIKEI_NENDO ");
            sql.Append("UNION ALL ");
            sql.Append("SELECT ");
            sql.Append("    CAST(1  AS DECIMAL(8))         AS 順位,");
            sql.Append("    CAST('仕入単価' AS VARCHAR(8)) AS 取引日付,");
            sql.Append("    C.SHIIRE_TANKA                     AS 単価 ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_SHOHIN  AS C ");
            sql.Append("WHERE ");
            sql.Append("        C.KAISHA_CD = @KAISHA_CD ");
            sql.Append("    AND C.SHISHO_CD = @SHISHO_CD ");
            sql.Append("    AND C.SHOHIN_CD = @SHOHIN_CD ");
            sql.Append("ORDER BY ");
            sql.Append("    1 DESC ");
            
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            // 取得の際は会計年度を無視しておく
            //dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, this._pTOKUISAKI_CD);
            dpc.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, this._pSEIKYUSAKI_CD);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 8, this._pSHOHIN_CD);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// DBから取得したDataTableを元に表示用のデータを取得
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private DataTable EditDataList(DataTable dtData)
        {
            // 返却するDataTable
            DataTable dtResult = new DataTable();
            DataRow drResult;

            // 列の定義を作成
            dtResult.Columns.Add("順位", typeof(string));
            dtResult.Columns.Add("取引日付", typeof(string));
            dtResult.Columns.Add("単価", typeof(string));

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();

                drResult["順位"] = dtData.Rows[i]["順位"];
                drResult["取引日付"] = dtData.Rows[i]["取引日付"];
                drResult["単価"] = Util.FormatNum(dtData.Rows[i]["単価"], 2);
                
                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }

        /// <summary>
        /// 削除処理
        /// </summary>
        private void DeleteData()
        {
            DbParamCollection whereParam;

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 仕入単価削除
                // Han.TB_仕入単価履歴(TB_HN_SHIIRE_TANKA_RIREKI)
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
                whereParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, this._pSEIKYUSAKI_CD);
                whereParam.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, this._pTOKUISAKI_CD);
                whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 8, this._pSHOHIN_CD);
                whereParam.SetParam("@TANKA", SqlDbType.Decimal, 13, this.dgvList[2, this.dgvList.CurrentCell.RowIndex].Value);
                this.Dba.Delete("TB_HN_SHIIRE_TANKA_RIREKI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD AND TANKA = @TANKA",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[1] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["単価"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
                                                                                                        
    }    
    
}
