﻿namespace jp.co.fsi.kb.kbde1021
{
    partial class KBDE1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblMode = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaCd = new System.Windows.Forms.Label();
            this.lblSiiresakiNm = new System.Windows.Forms.Label();
            this.txtSiiresakiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSiiresakiCd = new System.Windows.Forms.Label();
            this.lblSiharaisakiNm = new System.Windows.Forms.Label();
            this.txtSiharaisakiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSiharaisakiCd = new System.Windows.Forms.Label();
            this.txtDenpyoNo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoNo = new System.Windows.Forms.Label();
            this.lblDay = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGengoYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.lblDenpyoDate = new System.Windows.Forms.Label();
            this.lblTorihikiKubunNm = new System.Windows.Forms.Label();
            this.txtTorihikiKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTorihikiKubunCd = new System.Windows.Forms.Label();
            this.pnlDenpyoSyukei = new jp.co.fsi.common.FsiPanel();
            this.txtTotalGaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSyohiZei = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSyokei = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTotalGaku = new System.Windows.Forms.Label();
            this.lblSyohiZei = new System.Windows.Forms.Label();
            this.lblSyokei = new System.Windows.Forms.Label();
            this.dgvInputList = new System.Windows.Forms.DataGridView();
            this.GYO_BANGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHOHIN_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHOHIN_NM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IRISU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TANI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KESUSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BARA_SOSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.URI_TANKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BAIKA_KINGAKU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHOHIZEI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZEI_RITSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHIWAKE_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BUMON_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JIGYO_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KUBUN_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KAZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZAIKO_KANRI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KEI_FLG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtGridEdit = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBeforeDenpyoNo = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.pnlShohizei = new jp.co.fsi.common.FsiPanel();
            this.lblShohizeiTenka2 = new System.Windows.Forms.Label();
            this.lblShohizeiInputHoho2 = new System.Windows.Forms.Label();
            this.lblShohizeiTenka1 = new System.Windows.Forms.Label();
            this.lblShohizeiInputHoho1 = new System.Windows.Forms.Label();
            this.lblTaxInfo = new System.Windows.Forms.Label();
            this.txtDummy = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZaikoSu = new System.Windows.Forms.Label();
            this.lblShohinNm = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiDates = new jp.co.fsi.ClientCommon.FsiDate();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.pnlDenpyoSyukei.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
            this.pnlShohizei.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.lblShohinNm);
            this.pnlDebug.Controls.Add(this.lblZaikoSu);
            this.pnlDebug.Location = new System.Drawing.Point(3, 486);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1090, 134);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.lblZaikoSu, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.lblShohinNm, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1080, 41);
            this.lblTitle.Text = "仕入伝票入力";
            // 
            // lblMode
            // 
            this.lblMode.BackColor = System.Drawing.Color.Transparent;
            this.lblMode.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMode.Location = new System.Drawing.Point(4, 4);
            this.lblMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(96, 27);
            this.lblMode.TabIndex = 1;
            this.lblMode.Text = "【登録】";
            this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNm.Location = new System.Drawing.Point(695, 3);
            this.lblTantoshaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(312, 24);
            this.lblTantoshaNm.TabIndex = 24;
            this.lblTantoshaNm.Tag = "DISPNAME";
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = true;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoshaCd.Location = new System.Drawing.Point(648, 4);
            this.txtTantoshaCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.Size = new System.Drawing.Size(44, 23);
            this.txtTantoshaCd.TabIndex = 23;
            this.txtTantoshaCd.Text = "0";
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTantoshaCd_KeyDown);
            this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
            // 
            // lblTantoshaCd
            // 
            this.lblTantoshaCd.AutoSize = true;
            this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaCd.Location = new System.Drawing.Point(572, 7);
            this.lblTantoshaCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoshaCd.Name = "lblTantoshaCd";
            this.lblTantoshaCd.Size = new System.Drawing.Size(72, 16);
            this.lblTantoshaCd.TabIndex = 22;
            this.lblTantoshaCd.Tag = "CHANGE";
            this.lblTantoshaCd.Text = "担 当 者";
            this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSiiresakiNm
            // 
            this.lblSiiresakiNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblSiiresakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSiiresakiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSiiresakiNm.Location = new System.Drawing.Point(146, 3);
            this.lblSiiresakiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSiiresakiNm.Name = "lblSiiresakiNm";
            this.lblSiiresakiNm.Size = new System.Drawing.Size(420, 24);
            this.lblSiiresakiNm.TabIndex = 18;
            this.lblSiiresakiNm.Tag = "DISPNAME";
            this.lblSiiresakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSiiresakiCd
            // 
            this.txtSiiresakiCd.AutoSizeFromLength = true;
            this.txtSiiresakiCd.DisplayLength = null;
            this.txtSiiresakiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSiiresakiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSiiresakiCd.Location = new System.Drawing.Point(77, 3);
            this.txtSiiresakiCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtSiiresakiCd.MaxLength = 4;
            this.txtSiiresakiCd.Name = "txtSiiresakiCd";
            this.txtSiiresakiCd.Size = new System.Drawing.Size(63, 23);
            this.txtSiiresakiCd.TabIndex = 17;
            this.txtSiiresakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSiiresakiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtSiiresakiCd_Validating);
            // 
            // lblSiiresakiCd
            // 
            this.lblSiiresakiCd.BackColor = System.Drawing.Color.Silver;
            this.lblSiiresakiCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSiiresakiCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSiiresakiCd.Location = new System.Drawing.Point(0, 0);
            this.lblSiiresakiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSiiresakiCd.Name = "lblSiiresakiCd";
            this.lblSiiresakiCd.Size = new System.Drawing.Size(1054, 31);
            this.lblSiiresakiCd.TabIndex = 16;
            this.lblSiiresakiCd.Tag = "CHANGE";
            this.lblSiiresakiCd.Text = "仕 入 先";
            this.lblSiiresakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSiharaisakiNm
            // 
            this.lblSiharaisakiNm.BackColor = System.Drawing.Color.Silver;
            this.lblSiharaisakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSiharaisakiNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSiharaisakiNm.Location = new System.Drawing.Point(257, 527);
            this.lblSiharaisakiNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSiharaisakiNm.Name = "lblSiharaisakiNm";
            this.lblSiharaisakiNm.Size = new System.Drawing.Size(420, 27);
            this.lblSiharaisakiNm.TabIndex = 21;
            this.lblSiharaisakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblSiharaisakiNm.Visible = false;
            // 
            // txtSiharaisakiCd
            // 
            this.txtSiharaisakiCd.AutoSizeFromLength = true;
            this.txtSiharaisakiCd.DisplayLength = null;
            this.txtSiharaisakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSiharaisakiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSiharaisakiCd.Location = new System.Drawing.Point(210, 542);
            this.txtSiharaisakiCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtSiharaisakiCd.MaxLength = 6;
            this.txtSiharaisakiCd.Name = "txtSiharaisakiCd";
            this.txtSiharaisakiCd.Size = new System.Drawing.Size(63, 20);
            this.txtSiharaisakiCd.TabIndex = 20;
            this.txtSiharaisakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSiharaisakiCd.Visible = false;
            // 
            // lblSiharaisakiCd
            // 
            this.lblSiharaisakiCd.BackColor = System.Drawing.Color.Silver;
            this.lblSiharaisakiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSiharaisakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSiharaisakiCd.Location = new System.Drawing.Point(115, 527);
            this.lblSiharaisakiCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSiharaisakiCd.Name = "lblSiharaisakiCd";
            this.lblSiharaisakiCd.Size = new System.Drawing.Size(96, 27);
            this.lblSiharaisakiCd.TabIndex = 19;
            this.lblSiharaisakiCd.Text = "支 払 先";
            this.lblSiharaisakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblSiharaisakiCd.Visible = false;
            // 
            // txtDenpyoNo
            // 
            this.txtDenpyoNo.AutoSizeFromLength = true;
            this.txtDenpyoNo.DisplayLength = null;
            this.txtDenpyoNo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenpyoNo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDenpyoNo.Location = new System.Drawing.Point(77, 3);
            this.txtDenpyoNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDenpyoNo.MaxLength = 6;
            this.txtDenpyoNo.Name = "txtDenpyoNo";
            this.txtDenpyoNo.Size = new System.Drawing.Size(63, 23);
            this.txtDenpyoNo.TabIndex = 3;
            this.txtDenpyoNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDenpyoNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenpyoNo_Validating);
            // 
            // lblDenpyoNo
            // 
            this.lblDenpyoNo.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenpyoNo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoNo.Location = new System.Drawing.Point(0, 0);
            this.lblDenpyoNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDenpyoNo.Name = "lblDenpyoNo";
            this.lblDenpyoNo.Size = new System.Drawing.Size(1054, 29);
            this.lblDenpyoNo.TabIndex = 2;
            this.lblDenpyoNo.Tag = "CHANGE";
            this.lblDenpyoNo.Text = "伝票番号";
            this.lblDenpyoNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDay
            // 
            this.lblDay.AutoSize = true;
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDay.Location = new System.Drawing.Point(528, 8);
            this.lblDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(24, 16);
            this.lblDay.TabIndex = 12;
            this.lblDay.Tag = "CHANGE";
            this.lblDay.Text = "日";
            this.lblDay.Visible = false;
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.Location = new System.Drawing.Point(436, 8);
            this.lblMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(24, 16);
            this.lblMonth.TabIndex = 10;
            this.lblMonth.Tag = "CHANGE";
            this.lblMonth.Text = "月";
            this.lblMonth.Visible = false;
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = false;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDay.Location = new System.Drawing.Point(467, 3);
            this.txtDay.Margin = new System.Windows.Forms.Padding(4);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(52, 23);
            this.txtDay.TabIndex = 11;
            this.txtDay.TabStop = false;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.Visible = false;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(347, 8);
            this.lblYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(24, 16);
            this.lblYear.TabIndex = 8;
            this.lblYear.Tag = "CHANGE";
            this.lblYear.Text = "年";
            this.lblYear.Visible = false;
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.Location = new System.Drawing.Point(377, 3);
            this.txtMonth.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(52, 23);
            this.txtMonth.TabIndex = 9;
            this.txtMonth.TabStop = false;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Visible = false;
            // 
            // txtGengoYear
            // 
            this.txtGengoYear.AutoSizeFromLength = false;
            this.txtGengoYear.DisplayLength = null;
            this.txtGengoYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYear.Location = new System.Drawing.Point(288, 3);
            this.txtGengoYear.Margin = new System.Windows.Forms.Padding(4);
            this.txtGengoYear.MaxLength = 2;
            this.txtGengoYear.Name = "txtGengoYear";
            this.txtGengoYear.Size = new System.Drawing.Size(52, 23);
            this.txtGengoYear.TabIndex = 7;
            this.txtGengoYear.TabStop = false;
            this.txtGengoYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYear.Visible = false;
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengo.Location = new System.Drawing.Point(228, 2);
            this.lblGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(53, 24);
            this.lblGengo.TabIndex = 5;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblGengo.Visible = false;
            // 
            // lblDenpyoDate
            // 
            this.lblDenpyoDate.AutoSize = true;
            this.lblDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDate.Location = new System.Drawing.Point(148, 7);
            this.lblDenpyoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDenpyoDate.Name = "lblDenpyoDate";
            this.lblDenpyoDate.Size = new System.Drawing.Size(72, 16);
            this.lblDenpyoDate.TabIndex = 4;
            this.lblDenpyoDate.Tag = "CHANGE";
            this.lblDenpyoDate.Text = "伝票日付";
            this.lblDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTorihikiKubunNm
            // 
            this.lblTorihikiKubunNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblTorihikiKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKubunNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikiKubunNm.Location = new System.Drawing.Point(695, 3);
            this.lblTorihikiKubunNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTorihikiKubunNm.Name = "lblTorihikiKubunNm";
            this.lblTorihikiKubunNm.Size = new System.Drawing.Size(312, 24);
            this.lblTorihikiKubunNm.TabIndex = 15;
            this.lblTorihikiKubunNm.Tag = "DISPNAME";
            this.lblTorihikiKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTorihikiKubunCd
            // 
            this.txtTorihikiKubunCd.AutoSizeFromLength = true;
            this.txtTorihikiKubunCd.DisplayLength = null;
            this.txtTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTorihikiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTorihikiKubunCd.Location = new System.Drawing.Point(648, 3);
            this.txtTorihikiKubunCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTorihikiKubunCd.MaxLength = 2;
            this.txtTorihikiKubunCd.Name = "txtTorihikiKubunCd";
            this.txtTorihikiKubunCd.Size = new System.Drawing.Size(44, 23);
            this.txtTorihikiKubunCd.TabIndex = 14;
            this.txtTorihikiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikiKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikiKubunCd_Validating);
            // 
            // lblTorihikiKubunCd
            // 
            this.lblTorihikiKubunCd.AutoSize = true;
            this.lblTorihikiKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikiKubunCd.Location = new System.Drawing.Point(569, 8);
            this.lblTorihikiKubunCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTorihikiKubunCd.Name = "lblTorihikiKubunCd";
            this.lblTorihikiKubunCd.Size = new System.Drawing.Size(72, 16);
            this.lblTorihikiKubunCd.TabIndex = 13;
            this.lblTorihikiKubunCd.Tag = "CHANGE";
            this.lblTorihikiKubunCd.Text = "取引区分";
            this.lblTorihikiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlDenpyoSyukei
            // 
            this.pnlDenpyoSyukei.BackColor = System.Drawing.Color.Transparent;
            this.pnlDenpyoSyukei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDenpyoSyukei.Controls.Add(this.txtTotalGaku);
            this.pnlDenpyoSyukei.Controls.Add(this.txtSyohiZei);
            this.pnlDenpyoSyukei.Controls.Add(this.txtSyokei);
            this.pnlDenpyoSyukei.Controls.Add(this.lblTotalGaku);
            this.pnlDenpyoSyukei.Controls.Add(this.lblSyohiZei);
            this.pnlDenpyoSyukei.Controls.Add(this.lblSyokei);
            this.pnlDenpyoSyukei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDenpyoSyukei.Location = new System.Drawing.Point(801, 379);
            this.pnlDenpyoSyukei.Margin = new System.Windows.Forms.Padding(4);
            this.pnlDenpyoSyukei.Name = "pnlDenpyoSyukei";
            this.pnlDenpyoSyukei.Size = new System.Drawing.Size(270, 115);
            this.pnlDenpyoSyukei.TabIndex = 27;
            this.pnlDenpyoSyukei.Tag = "CHANGE";
            // 
            // txtTotalGaku
            // 
            this.txtTotalGaku.AutoSizeFromLength = true;
            this.txtTotalGaku.DisplayLength = null;
            this.txtTotalGaku.Enabled = false;
            this.txtTotalGaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTotalGaku.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTotalGaku.Location = new System.Drawing.Point(141, 75);
            this.txtTotalGaku.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalGaku.MaxLength = 12;
            this.txtTotalGaku.Name = "txtTotalGaku";
            this.txtTotalGaku.ReadOnly = true;
            this.txtTotalGaku.Size = new System.Drawing.Size(119, 23);
            this.txtTotalGaku.TabIndex = 5;
            this.txtTotalGaku.TabStop = false;
            this.txtTotalGaku.Text = "0";
            this.txtTotalGaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSyohiZei
            // 
            this.txtSyohiZei.AutoSizeFromLength = true;
            this.txtSyohiZei.DisplayLength = null;
            this.txtSyohiZei.Enabled = false;
            this.txtSyohiZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSyohiZei.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSyohiZei.Location = new System.Drawing.Point(141, 41);
            this.txtSyohiZei.Margin = new System.Windows.Forms.Padding(4);
            this.txtSyohiZei.MaxLength = 12;
            this.txtSyohiZei.Name = "txtSyohiZei";
            this.txtSyohiZei.ReadOnly = true;
            this.txtSyohiZei.Size = new System.Drawing.Size(119, 23);
            this.txtSyohiZei.TabIndex = 3;
            this.txtSyohiZei.TabStop = false;
            this.txtSyohiZei.Text = "0";
            this.txtSyohiZei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSyokei
            // 
            this.txtSyokei.AutoSizeFromLength = true;
            this.txtSyokei.DisplayLength = null;
            this.txtSyokei.Enabled = false;
            this.txtSyokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSyokei.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSyokei.Location = new System.Drawing.Point(141, 8);
            this.txtSyokei.Margin = new System.Windows.Forms.Padding(4);
            this.txtSyokei.MaxLength = 12;
            this.txtSyokei.Name = "txtSyokei";
            this.txtSyokei.ReadOnly = true;
            this.txtSyokei.Size = new System.Drawing.Size(119, 23);
            this.txtSyokei.TabIndex = 1;
            this.txtSyokei.Text = "0";
            this.txtSyokei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalGaku
            // 
            this.lblTotalGaku.BackColor = System.Drawing.Color.Silver;
            this.lblTotalGaku.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTotalGaku.Location = new System.Drawing.Point(4, 75);
            this.lblTotalGaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalGaku.Name = "lblTotalGaku";
            this.lblTotalGaku.Size = new System.Drawing.Size(136, 27);
            this.lblTotalGaku.TabIndex = 4;
            this.lblTotalGaku.Tag = "CHANGE";
            this.lblTotalGaku.Text = "合　計　額";
            this.lblTotalGaku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSyohiZei
            // 
            this.lblSyohiZei.BackColor = System.Drawing.Color.Silver;
            this.lblSyohiZei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSyohiZei.Location = new System.Drawing.Point(4, 41);
            this.lblSyohiZei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSyohiZei.Name = "lblSyohiZei";
            this.lblSyohiZei.Size = new System.Drawing.Size(136, 27);
            this.lblSyohiZei.TabIndex = 2;
            this.lblSyohiZei.Tag = "CHANGE";
            this.lblSyohiZei.Text = "消費税（外）";
            this.lblSyohiZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSyokei
            // 
            this.lblSyokei.BackColor = System.Drawing.Color.Silver;
            this.lblSyokei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSyokei.Location = new System.Drawing.Point(4, 8);
            this.lblSyokei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSyokei.Name = "lblSyokei";
            this.lblSyokei.Size = new System.Drawing.Size(136, 27);
            this.lblSyokei.TabIndex = 0;
            this.lblSyokei.Tag = "CHANGE";
            this.lblSyokei.Text = "小　　　計";
            this.lblSyokei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvInputList
            // 
            this.dgvInputList.AllowUserToAddRows = false;
            this.dgvInputList.AllowUserToDeleteRows = false;
            this.dgvInputList.AllowUserToResizeColumns = false;
            this.dgvInputList.AllowUserToResizeRows = false;
            this.dgvInputList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInputList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GYO_BANGO,
            this.KUBUN,
            this.SHOHIN_CD,
            this.SHOHIN_NM,
            this.IRISU,
            this.TANI,
            this.KESUSU,
            this.BARA_SOSU,
            this.URI_TANKA,
            this.BAIKA_KINGAKU,
            this.SHOHIZEI,
            this.ZEI_RITSU,
            this.SHIWAKE_CD,
            this.BUMON_CD,
            this.ZEI_KUBUN,
            this.JIGYO_KUBUN,
            this.KUBUN_CD,
            this.KAZEI_KUBUN,
            this.ZAIKO_KANRI_KUBUN,
            this.KEI_FLG});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvInputList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvInputList.EnableHeadersVisualStyles = false;
            this.dgvInputList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvInputList.Location = new System.Drawing.Point(8, 158);
            this.dgvInputList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvInputList.MultiSelect = false;
            this.dgvInputList.Name = "dgvInputList";
            this.dgvInputList.RowHeadersVisible = false;
            this.dgvInputList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInputList.RowTemplate.Height = 21;
            this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInputList.Size = new System.Drawing.Size(1061, 217);
            this.dgvInputList.TabIndex = 25;
            this.dgvInputList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellEnter);
            this.dgvInputList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvInputList_CellFormatting);
            this.dgvInputList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvInputList_CellMouseDown);
            this.dgvInputList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvInputList_Scroll);
            this.dgvInputList.Enter += new System.EventHandler(this.dgvInputList_Enter);
            // 
            // GYO_BANGO
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.GYO_BANGO.DefaultCellStyle = dataGridViewCellStyle2;
            this.GYO_BANGO.HeaderText = "行";
            this.GYO_BANGO.Name = "GYO_BANGO";
            this.GYO_BANGO.ReadOnly = true;
            this.GYO_BANGO.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.GYO_BANGO.Width = 25;
            // 
            // KUBUN
            // 
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.KUBUN.DefaultCellStyle = dataGridViewCellStyle3;
            this.KUBUN.HeaderText = "区";
            this.KUBUN.Name = "KUBUN";
            this.KUBUN.ReadOnly = true;
            this.KUBUN.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KUBUN.Width = 25;
            // 
            // SHOHIN_CD
            // 
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.SHOHIN_CD.DefaultCellStyle = dataGridViewCellStyle4;
            this.SHOHIN_CD.HeaderText = "ｺｰﾄﾞ";
            this.SHOHIN_CD.Name = "SHOHIN_CD";
            this.SHOHIN_CD.ReadOnly = true;
            this.SHOHIN_CD.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SHOHIN_CD.Width = 85;
            // 
            // SHOHIN_NM
            // 
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.SHOHIN_NM.DefaultCellStyle = dataGridViewCellStyle5;
            this.SHOHIN_NM.HeaderText = "商品名";
            this.SHOHIN_NM.Name = "SHOHIN_NM";
            this.SHOHIN_NM.ReadOnly = true;
            this.SHOHIN_NM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SHOHIN_NM.Width = 180;
            // 
            // IRISU
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.IRISU.DefaultCellStyle = dataGridViewCellStyle6;
            this.IRISU.HeaderText = "入数";
            this.IRISU.Name = "IRISU";
            this.IRISU.ReadOnly = true;
            this.IRISU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IRISU.Width = 80;
            // 
            // TANI
            // 
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.TANI.DefaultCellStyle = dataGridViewCellStyle7;
            this.TANI.HeaderText = "単位";
            this.TANI.Name = "TANI";
            this.TANI.ReadOnly = true;
            this.TANI.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TANI.Width = 80;
            // 
            // KESUSU
            // 
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.KESUSU.DefaultCellStyle = dataGridViewCellStyle8;
            this.KESUSU.HeaderText = "ｹｰｽ数";
            this.KESUSU.Name = "KESUSU";
            this.KESUSU.ReadOnly = true;
            this.KESUSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KESUSU.Width = 80;
            // 
            // BARA_SOSU
            // 
            dataGridViewCellStyle9.NullValue = null;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.BARA_SOSU.DefaultCellStyle = dataGridViewCellStyle9;
            this.BARA_SOSU.HeaderText = "ﾊﾞﾗ数";
            this.BARA_SOSU.Name = "BARA_SOSU";
            this.BARA_SOSU.ReadOnly = true;
            this.BARA_SOSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BARA_SOSU.Width = 80;
            // 
            // URI_TANKA
            // 
            dataGridViewCellStyle10.NullValue = null;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.URI_TANKA.DefaultCellStyle = dataGridViewCellStyle10;
            this.URI_TANKA.HeaderText = "単価";
            this.URI_TANKA.Name = "URI_TANKA";
            this.URI_TANKA.ReadOnly = true;
            this.URI_TANKA.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.URI_TANKA.Width = 80;
            // 
            // BAIKA_KINGAKU
            // 
            dataGridViewCellStyle11.NullValue = null;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.BAIKA_KINGAKU.DefaultCellStyle = dataGridViewCellStyle11;
            this.BAIKA_KINGAKU.HeaderText = "金額";
            this.BAIKA_KINGAKU.Name = "BAIKA_KINGAKU";
            this.BAIKA_KINGAKU.ReadOnly = true;
            this.BAIKA_KINGAKU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // SHOHIZEI
            // 
            dataGridViewCellStyle12.NullValue = null;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.SHOHIZEI.DefaultCellStyle = dataGridViewCellStyle12;
            this.SHOHIZEI.HeaderText = "消費税";
            this.SHOHIZEI.Name = "SHOHIZEI";
            this.SHOHIZEI.ReadOnly = true;
            this.SHOHIZEI.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SHOHIZEI.Width = 90;
            // 
            // ZEI_RITSU
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.Format = "#,##0";
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.ZEI_RITSU.DefaultCellStyle = dataGridViewCellStyle13;
            this.ZEI_RITSU.HeaderText = "率";
            this.ZEI_RITSU.Name = "ZEI_RITSU";
            this.ZEI_RITSU.ReadOnly = true;
            this.ZEI_RITSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ZEI_RITSU.Width = 50;
            // 
            // SHIWAKE_CD
            // 
            this.SHIWAKE_CD.HeaderText = "仕訳コード";
            this.SHIWAKE_CD.Name = "SHIWAKE_CD";
            this.SHIWAKE_CD.Visible = false;
            // 
            // BUMON_CD
            // 
            this.BUMON_CD.HeaderText = "部門コード";
            this.BUMON_CD.Name = "BUMON_CD";
            this.BUMON_CD.Visible = false;
            // 
            // ZEI_KUBUN
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.Format = "#,##0";
            this.ZEI_KUBUN.DefaultCellStyle = dataGridViewCellStyle14;
            this.ZEI_KUBUN.HeaderText = "区";
            this.ZEI_KUBUN.Name = "ZEI_KUBUN";
            this.ZEI_KUBUN.Width = 50;
            // 
            // JIGYO_KUBUN
            // 
            this.JIGYO_KUBUN.HeaderText = "事業区分";
            this.JIGYO_KUBUN.Name = "JIGYO_KUBUN";
            this.JIGYO_KUBUN.Visible = false;
            // 
            // KUBUN_CD
            // 
            this.KUBUN_CD.HeaderText = "区ｺｰﾄﾞ";
            this.KUBUN_CD.Name = "KUBUN_CD";
            this.KUBUN_CD.Visible = false;
            // 
            // KAZEI_KUBUN
            // 
            this.KAZEI_KUBUN.HeaderText = "課税区分";
            this.KAZEI_KUBUN.Name = "KAZEI_KUBUN";
            this.KAZEI_KUBUN.Visible = false;
            // 
            // ZAIKO_KANRI_KUBUN
            // 
            this.ZAIKO_KANRI_KUBUN.HeaderText = "在庫管理区分";
            this.ZAIKO_KANRI_KUBUN.Name = "ZAIKO_KANRI_KUBUN";
            this.ZAIKO_KANRI_KUBUN.Visible = false;
            // 
            // KEI_FLG
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.KEI_FLG.DefaultCellStyle = dataGridViewCellStyle15;
            this.KEI_FLG.HeaderText = "軽";
            this.KEI_FLG.Name = "KEI_FLG";
            this.KEI_FLG.Width = 50;
            // 
            // txtGridEdit
            // 
            this.txtGridEdit.AutoSizeFromLength = true;
            this.txtGridEdit.BackColor = System.Drawing.Color.White;
            this.txtGridEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGridEdit.DisplayLength = null;
            this.txtGridEdit.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGridEdit.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGridEdit.Location = new System.Drawing.Point(525, 268);
            this.txtGridEdit.Margin = new System.Windows.Forms.Padding(4);
            this.txtGridEdit.MaxLength = 10;
            this.txtGridEdit.Name = "txtGridEdit";
            this.txtGridEdit.Size = new System.Drawing.Size(101, 23);
            this.txtGridEdit.TabIndex = 26;
            this.txtGridEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGridEdit.TextChanged += new System.EventHandler(this.txtGridEdit_TextChanged);
            this.txtGridEdit.Enter += new System.EventHandler(this.txtGridEdit_Enter);
            this.txtGridEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGridEdit_KeyDown);
            // 
            // lblBeforeDenpyoNo
            // 
            this.lblBeforeDenpyoNo.BackColor = System.Drawing.Color.White;
            this.lblBeforeDenpyoNo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBeforeDenpyoNo.Location = new System.Drawing.Point(108, 4);
            this.lblBeforeDenpyoNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBeforeDenpyoNo.Name = "lblBeforeDenpyoNo";
            this.lblBeforeDenpyoNo.Size = new System.Drawing.Size(456, 27);
            this.lblBeforeDenpyoNo.TabIndex = 905;
            this.lblBeforeDenpyoNo.Text = "【前回登録伝票番号：】";
            this.lblBeforeDenpyoNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBeforeDenpyoNo.Visible = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(80, 5);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 5;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtMizuageShishoCd.TabIndex = 909;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(127, 5);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(312, 24);
            this.lblMizuageShishoNm.TabIndex = 911;
            this.lblMizuageShishoNm.Tag = "DISPNAME";
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlShohizei
            // 
            this.pnlShohizei.BackColor = System.Drawing.Color.Transparent;
            this.pnlShohizei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlShohizei.Controls.Add(this.lblShohizeiTenka2);
            this.pnlShohizei.Controls.Add(this.lblShohizeiInputHoho2);
            this.pnlShohizei.Controls.Add(this.lblShohizeiTenka1);
            this.pnlShohizei.Controls.Add(this.lblShohizeiInputHoho1);
            this.pnlShohizei.Location = new System.Drawing.Point(522, 379);
            this.pnlShohizei.Margin = new System.Windows.Forms.Padding(4);
            this.pnlShohizei.Name = "pnlShohizei";
            this.pnlShohizei.Size = new System.Drawing.Size(270, 115);
            this.pnlShohizei.TabIndex = 912;
            this.pnlShohizei.Tag = "CHANGE";
            // 
            // lblShohizeiTenka2
            // 
            this.lblShohizeiTenka2.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohizeiTenka2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiTenka2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiTenka2.Location = new System.Drawing.Point(4, 83);
            this.lblShohizeiTenka2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohizeiTenka2.Name = "lblShohizeiTenka2";
            this.lblShohizeiTenka2.Size = new System.Drawing.Size(260, 27);
            this.lblShohizeiTenka2.TabIndex = 3;
            this.lblShohizeiTenka2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiInputHoho2
            // 
            this.lblShohizeiInputHoho2.BackColor = System.Drawing.Color.LightCyan;
            this.lblShohizeiInputHoho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiInputHoho2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiInputHoho2.Location = new System.Drawing.Point(4, 32);
            this.lblShohizeiInputHoho2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohizeiInputHoho2.Name = "lblShohizeiInputHoho2";
            this.lblShohizeiInputHoho2.Size = new System.Drawing.Size(260, 27);
            this.lblShohizeiInputHoho2.TabIndex = 1;
            this.lblShohizeiInputHoho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiTenka1
            // 
            this.lblShohizeiTenka1.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiTenka1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiTenka1.Location = new System.Drawing.Point(4, 56);
            this.lblShohizeiTenka1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohizeiTenka1.Name = "lblShohizeiTenka1";
            this.lblShohizeiTenka1.Size = new System.Drawing.Size(260, 27);
            this.lblShohizeiTenka1.TabIndex = 2;
            this.lblShohizeiTenka1.Tag = "CHANGE";
            this.lblShohizeiTenka1.Text = "消費税転嫁方法";
            this.lblShohizeiTenka1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiInputHoho1
            // 
            this.lblShohizeiInputHoho1.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiInputHoho1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiInputHoho1.Location = new System.Drawing.Point(4, 4);
            this.lblShohizeiInputHoho1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohizeiInputHoho1.Name = "lblShohizeiInputHoho1";
            this.lblShohizeiInputHoho1.Size = new System.Drawing.Size(260, 27);
            this.lblShohizeiInputHoho1.TabIndex = 0;
            this.lblShohizeiInputHoho1.Tag = "CHANGE";
            this.lblShohizeiInputHoho1.Text = "消費税入力方法";
            this.lblShohizeiInputHoho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaxInfo
            // 
            this.lblTaxInfo.BackColor = System.Drawing.Color.White;
            this.lblTaxInfo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTaxInfo.Location = new System.Drawing.Point(20, 379);
            this.lblTaxInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTaxInfo.Name = "lblTaxInfo";
            this.lblTaxInfo.Size = new System.Drawing.Size(436, 120);
            this.lblTaxInfo.TabIndex = 65543;
            this.lblTaxInfo.Tag = "CHANGE";
            this.lblTaxInfo.Text = "消費税内訳情報";
            // 
            // txtDummy
            // 
            this.txtDummy.AutoSizeFromLength = true;
            this.txtDummy.BackColor = System.Drawing.Color.White;
            this.txtDummy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDummy.DisplayLength = null;
            this.txtDummy.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDummy.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDummy.Location = new System.Drawing.Point(975, 97);
            this.txtDummy.Margin = new System.Windows.Forms.Padding(4);
            this.txtDummy.MaxLength = 10;
            this.txtDummy.Name = "txtDummy";
            this.txtDummy.Size = new System.Drawing.Size(0, 20);
            this.txtDummy.TabIndex = 65544;
            this.txtDummy.TabStop = false;
            this.txtDummy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblZaikoSu
            // 
            this.lblZaikoSu.BackColor = System.Drawing.Color.Silver;
            this.lblZaikoSu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZaikoSu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZaikoSu.Location = new System.Drawing.Point(798, 12);
            this.lblZaikoSu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZaikoSu.Name = "lblZaikoSu";
            this.lblZaikoSu.Size = new System.Drawing.Size(270, 27);
            this.lblZaikoSu.TabIndex = 65548;
            this.lblZaikoSu.Tag = "CHANGE";
            this.lblZaikoSu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblZaikoSu.Visible = false;
            // 
            // lblShohinNm
            // 
            this.lblShohinNm.BackColor = System.Drawing.Color.Silver;
            this.lblShohinNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNm.Location = new System.Drawing.Point(524, 12);
            this.lblShohinNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShohinNm.Name = "lblShohinNm";
            this.lblShohinNm.Size = new System.Drawing.Size(265, 27);
            this.lblShohinNm.TabIndex = 65547;
            this.lblShohinNm.Tag = "CHANGE";
            this.lblShohinNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblShohinNm.Visible = false;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(7, 44);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 3;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(1062, 111);
            this.fsiTableLayoutPanel1.TabIndex = 65549;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.lblSiiresakiNm);
            this.fsiPanel3.Controls.Add(this.txtSiiresakiCd);
            this.fsiPanel3.Controls.Add(this.lblTantoshaCd);
            this.fsiPanel3.Controls.Add(this.txtTantoshaCd);
            this.fsiPanel3.Controls.Add(this.lblTantoshaNm);
            this.fsiPanel3.Controls.Add(this.lblSiiresakiCd);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 76);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(1054, 31);
            this.fsiPanel3.TabIndex = 2;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.fsiDates);
            this.fsiPanel2.Controls.Add(this.txtDenpyoNo);
            this.fsiPanel2.Controls.Add(this.lblDenpyoDate);
            this.fsiPanel2.Controls.Add(this.lblGengo);
            this.fsiPanel2.Controls.Add(this.txtGengoYear);
            this.fsiPanel2.Controls.Add(this.txtMonth);
            this.fsiPanel2.Controls.Add(this.lblYear);
            this.fsiPanel2.Controls.Add(this.txtDay);
            this.fsiPanel2.Controls.Add(this.lblMonth);
            this.fsiPanel2.Controls.Add(this.lblDay);
            this.fsiPanel2.Controls.Add(this.lblTorihikiKubunCd);
            this.fsiPanel2.Controls.Add(this.lblTorihikiKubunNm);
            this.fsiPanel2.Controls.Add(this.txtTorihikiKubunCd);
            this.fsiPanel2.Controls.Add(this.lblDenpyoNo);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 40);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(1054, 29);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiDates
            // 
            this.fsiDates.Day = "";
            this.fsiDates.DbConnect = null;
            this.fsiDates.DispFormat = jp.co.fsi.ClientCommon.DispDateFormat.年月日;
            this.fsiDates.ExecBtn = null;
            this.fsiDates.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.fsiDates.Gengo = "";
            this.fsiDates.JpDate = null;
            this.fsiDates.Location = new System.Drawing.Point(222, 0);
            this.fsiDates.Month = "";
            this.fsiDates.Name = "fsiDates";
            this.fsiDates.OpenDialogExe = null;
            this.fsiDates.SeirekiYmd = null;
            this.fsiDates.Size = new System.Drawing.Size(241, 29);
            this.fsiDates.SYear = "";
            this.fsiDates.TabIndex = 6;
            this.fsiDates.TabStop = true;
            this.fsiDates.Tag = "CHANGE";
            this.fsiDates.WYear = "";
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.fsiPanel5);
            this.fsiPanel1.Controls.Add(this.fsiPanel4);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(1, 1);
            this.fsiPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(1060, 35);
            this.fsiPanel1.TabIndex = 0;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel5.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel5.Controls.Add(this.label1);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(571, 0);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Padding = new System.Windows.Forms.Padding(3);
            this.fsiPanel5.Size = new System.Drawing.Size(489, 35);
            this.fsiPanel5.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(483, 29);
            this.label1.TabIndex = 911;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "支　　所";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblMode);
            this.fsiPanel4.Controls.Add(this.lblBeforeDenpyoNo);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.fsiPanel4.Location = new System.Drawing.Point(0, 0);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(571, 35);
            this.fsiPanel4.TabIndex = 2;
            // 
            // KBDE1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1080, 618);
            this.Controls.Add(this.lblTaxInfo);
            this.Controls.Add(this.pnlShohizei);
            this.Controls.Add(this.pnlDenpyoSyukei);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Controls.Add(this.txtDummy);
            this.Controls.Add(this.txtGridEdit);
            this.Controls.Add(this.dgvInputList);
            this.Controls.Add(this.lblSiharaisakiNm);
            this.Controls.Add(this.txtSiharaisakiCd);
            this.Controls.Add(this.lblSiharaisakiCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "KBDE1021";
            this.Text = "仕入伝票入力";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.KBDE1021_FormClosing);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblSiharaisakiCd, 0);
            this.Controls.SetChildIndex(this.txtSiharaisakiCd, 0);
            this.Controls.SetChildIndex(this.lblSiharaisakiNm, 0);
            this.Controls.SetChildIndex(this.dgvInputList, 0);
            this.Controls.SetChildIndex(this.txtGridEdit, 0);
            this.Controls.SetChildIndex(this.txtDummy, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDenpyoSyukei, 0);
            this.Controls.SetChildIndex(this.pnlShohizei, 0);
            this.Controls.SetChildIndex(this.lblTaxInfo, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlDenpyoSyukei.ResumeLayout(false);
            this.pnlDenpyoSyukei.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
            this.pnlShohizei.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel5.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblTantoshaCd;
        private System.Windows.Forms.Label lblSiiresakiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtSiiresakiCd;
        private System.Windows.Forms.Label lblSiiresakiCd;
        private System.Windows.Forms.Label lblSiharaisakiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtSiharaisakiCd;
        private System.Windows.Forms.Label lblSiharaisakiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtDenpyoNo;
        private System.Windows.Forms.Label lblDenpyoNo;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDay;
        private System.Windows.Forms.Label lblYear;
        private jp.co.fsi.common.controls.FsiTextBox txtMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYear;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.Label lblDenpyoDate;
        private System.Windows.Forms.Label lblTorihikiKubunNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTorihikiKubunCd;
        private System.Windows.Forms.Label lblTorihikiKubunCd;
        private System.Windows.Forms.DataGridView dgvInputList;
        private jp.co.fsi.common.FsiPanel pnlDenpyoSyukei;
        private jp.co.fsi.common.controls.FsiTextBox txtTotalGaku;
        private jp.co.fsi.common.controls.FsiTextBox txtSyohiZei;
        private jp.co.fsi.common.controls.FsiTextBox txtSyokei;
        private System.Windows.Forms.Label lblTotalGaku;
        private System.Windows.Forms.Label lblSyohiZei;
        private System.Windows.Forms.Label lblSyokei;
        private jp.co.fsi.common.controls.FsiTextBox txtGridEdit;
        private System.Windows.Forms.Label lblBeforeDenpyoNo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private jp.co.fsi.common.FsiPanel pnlShohizei;
        private System.Windows.Forms.Label lblShohizeiTenka2;
        private System.Windows.Forms.Label lblShohizeiInputHoho2;
        private System.Windows.Forms.Label lblShohizeiTenka1;
        private System.Windows.Forms.Label lblShohizeiInputHoho1;
        private System.Windows.Forms.Label lblTaxInfo;
        private common.controls.FsiTextBox txtDummy;
        private System.Windows.Forms.Label lblZaikoSu;
        private System.Windows.Forms.Label lblShohinNm;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
        private common.FsiPanel fsiPanel5;
        private System.Windows.Forms.Label label1;
        private common.FsiPanel fsiPanel4;
		private System.Windows.Forms.DataGridViewTextBoxColumn GYO_BANGO;
		private System.Windows.Forms.DataGridViewTextBoxColumn KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIN_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIN_NM;
		private System.Windows.Forms.DataGridViewTextBoxColumn IRISU;
		private System.Windows.Forms.DataGridViewTextBoxColumn TANI;
		private System.Windows.Forms.DataGridViewTextBoxColumn KESUSU;
		private System.Windows.Forms.DataGridViewTextBoxColumn BARA_SOSU;
		private System.Windows.Forms.DataGridViewTextBoxColumn URI_TANKA;
		private System.Windows.Forms.DataGridViewTextBoxColumn BAIKA_KINGAKU;
		private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIZEI;
		private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_RITSU;
		private System.Windows.Forms.DataGridViewTextBoxColumn SHIWAKE_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn BUMON_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn JIGYO_KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn KUBUN_CD;
		private System.Windows.Forms.DataGridViewTextBoxColumn KAZEI_KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn ZAIKO_KANRI_KUBUN;
		private System.Windows.Forms.DataGridViewTextBoxColumn KEI_FLG;
		private ClientCommon.FsiDate fsiDates;
	}
}