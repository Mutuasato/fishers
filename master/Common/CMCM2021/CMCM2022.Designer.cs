﻿namespace jp.co.fsi.cm.cmcm2021
{
    partial class CMCM2022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTantoshaCd = new System.Windows.Forms.Label();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTantoshaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.txtTantoshaKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaKanaNm = new System.Windows.Forms.Label();
            this.txtBumon = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumon = new System.Windows.Forms.Label();
            this.lblBumonNm = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.txtUserId = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtPassWord = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblpassword = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBumonnCd = new System.Windows.Forms.Label();
            this.lblHojoKamokuCd = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.lblKamokbn = new System.Windows.Forms.Label();
            this.lblZeiKbnn = new System.Windows.Forms.Label();
            this.txtKyuyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyo = new System.Windows.Forms.Label();
            this.txtZaimu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZaimu = new System.Windows.Forms.Label();
            this.txtShokudo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShokudo = new System.Windows.Forms.Label();
            this.txtKobai = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKobai = new System.Windows.Forms.Label();
            this.txtSeri = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeri = new System.Windows.Forms.Label();
            this.txtUserKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblUserKubun = new System.Windows.Forms.Label();
            this.txtShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.shosai = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.panel13 = new jp.co.fsi.common.FsiPanel();
            this.panel12 = new jp.co.fsi.common.FsiPanel();
            this.panel11 = new jp.co.fsi.common.FsiPanel();
            this.panel10 = new jp.co.fsi.common.FsiPanel();
            this.panel9 = new jp.co.fsi.common.FsiPanel();
            this.panel8 = new jp.co.fsi.common.FsiPanel();
            this.tableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.panel7 = new jp.co.fsi.common.FsiPanel();
            this.panel6 = new jp.co.fsi.common.FsiPanel();
            this.panel5 = new jp.co.fsi.common.FsiPanel();
            this.panel4 = new jp.co.fsi.common.FsiPanel();
            this.panel3 = new jp.co.fsi.common.FsiPanel();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.panel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.shosai.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 540);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1351, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1340, 31);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // lblTantoshaCd
            // 
            this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaCd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaCd.Location = new System.Drawing.Point(0, 0);
            this.lblTantoshaCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoshaCd.Name = "lblTantoshaCd";
            this.lblTantoshaCd.Size = new System.Drawing.Size(534, 37);
            this.lblTantoshaCd.TabIndex = 3;
            this.lblTantoshaCd.Tag = "CHANGE";
            this.lblTantoshaCd.Text = "担当者コード";
            this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = false;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCd.Location = new System.Drawing.Point(121, 6);
            this.txtTantoshaCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.Size = new System.Drawing.Size(69, 23);
            this.txtTantoshaCd.TabIndex = 4;
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
            // 
            // txtTantoshaNm
            // 
            this.txtTantoshaNm.AutoSizeFromLength = false;
            this.txtTantoshaNm.DisplayLength = null;
            this.txtTantoshaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTantoshaNm.Location = new System.Drawing.Point(121, 7);
            this.txtTantoshaNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtTantoshaNm.MaxLength = 40;
            this.txtTantoshaNm.Name = "txtTantoshaNm";
            this.txtTantoshaNm.Size = new System.Drawing.Size(376, 23);
            this.txtTantoshaNm.TabIndex = 6;
            this.txtTantoshaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaNm_Validating);
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNm.Location = new System.Drawing.Point(0, 0);
            this.lblTantoshaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(534, 37);
            this.lblTantoshaNm.TabIndex = 5;
            this.lblTantoshaNm.Tag = "CHANGE";
            this.lblTantoshaNm.Text = "担 当 者 名";
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaKanaNm
            // 
            this.txtTantoshaKanaNm.AutoSizeFromLength = false;
            this.txtTantoshaKanaNm.DisplayLength = null;
            this.txtTantoshaKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtTantoshaKanaNm.Location = new System.Drawing.Point(121, 6);
            this.txtTantoshaKanaNm.Margin = new System.Windows.Forms.Padding(4);
            this.txtTantoshaKanaNm.MaxLength = 30;
            this.txtTantoshaKanaNm.Name = "txtTantoshaKanaNm";
            this.txtTantoshaKanaNm.Size = new System.Drawing.Size(376, 23);
            this.txtTantoshaKanaNm.TabIndex = 8;
            this.txtTantoshaKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaKanaNm_Validating);
            // 
            // lblTantoshaKanaNm
            // 
            this.lblTantoshaKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTantoshaKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaKanaNm.Location = new System.Drawing.Point(0, 0);
            this.lblTantoshaKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoshaKanaNm.Name = "lblTantoshaKanaNm";
            this.lblTantoshaKanaNm.Size = new System.Drawing.Size(534, 37);
            this.lblTantoshaKanaNm.TabIndex = 7;
            this.lblTantoshaKanaNm.Tag = "CHANGE";
            this.lblTantoshaKanaNm.Text = "担当者カナ名";
            this.lblTantoshaKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumon
            // 
            this.txtBumon.AutoSizeFromLength = false;
            this.txtBumon.DisplayLength = null;
            this.txtBumon.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumon.Location = new System.Drawing.Point(121, 6);
            this.txtBumon.Margin = new System.Windows.Forms.Padding(4);
            this.txtBumon.MaxLength = 4;
            this.txtBumon.Name = "txtBumon";
            this.txtBumon.Size = new System.Drawing.Size(69, 23);
            this.txtBumon.TabIndex = 10;
            this.txtBumon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumon.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumon_Validating);
            // 
            // lblBumon
            // 
            this.lblBumon.BackColor = System.Drawing.Color.Silver;
            this.lblBumon.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBumon.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumon.Location = new System.Drawing.Point(0, 0);
            this.lblBumon.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumon.Name = "lblBumon";
            this.lblBumon.Size = new System.Drawing.Size(534, 37);
            this.lblBumon.TabIndex = 9;
            this.lblBumon.Tag = "CHANGE";
            this.lblBumon.Text = "部       門";
            this.lblBumon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonNm
            // 
            this.lblBumonNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonNm.Location = new System.Drawing.Point(196, 6);
            this.lblBumonNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonNm.Name = "lblBumonNm";
            this.lblBumonNm.Size = new System.Drawing.Size(301, 24);
            this.lblBumonNm.TabIndex = 11;
            this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUserId
            // 
            this.lblUserId.BackColor = System.Drawing.Color.Silver;
            this.lblUserId.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUserId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUserId.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblUserId.Location = new System.Drawing.Point(0, 0);
            this.lblUserId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(534, 37);
            this.lblUserId.TabIndex = 12;
            this.lblUserId.Tag = "CHANGE";
            this.lblUserId.Text = "ユーザID";
            this.lblUserId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUserId
            // 
            this.txtUserId.AutoSizeFromLength = false;
            this.txtUserId.DisplayLength = null;
            this.txtUserId.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUserId.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtUserId.Location = new System.Drawing.Point(121, 6);
            this.txtUserId.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserId.MaxLength = 30;
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(299, 23);
            this.txtUserId.TabIndex = 13;
            this.txtUserId.Validating += new System.ComponentModel.CancelEventHandler(this.txtUserId_Validating);
            // 
            // txtPassWord
            // 
            this.txtPassWord.AutoSizeFromLength = false;
            this.txtPassWord.DisplayLength = null;
            this.txtPassWord.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPassWord.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtPassWord.Location = new System.Drawing.Point(121, 8);
            this.txtPassWord.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassWord.MaxLength = 30;
            this.txtPassWord.Name = "txtPassWord";
            this.txtPassWord.Size = new System.Drawing.Size(299, 23);
            this.txtPassWord.TabIndex = 15;
            this.txtPassWord.Validating += new System.ComponentModel.CancelEventHandler(this.txtPassword_Validating);
            // 
            // lblpassword
            // 
            this.lblpassword.BackColor = System.Drawing.Color.Silver;
            this.lblpassword.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblpassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblpassword.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblpassword.Location = new System.Drawing.Point(0, 0);
            this.lblpassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblpassword.Name = "lblpassword";
            this.lblpassword.Size = new System.Drawing.Size(534, 44);
            this.lblpassword.TabIndex = 14;
            this.lblpassword.Tag = "CHANGE";
            this.lblpassword.Text = "パスワード";
            this.lblpassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightCyan;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(139, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "1：管理者　　０：一般";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonnCd
            // 
            this.lblBumonnCd.BackColor = System.Drawing.Color.LightCyan;
            this.lblBumonnCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonnCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonnCd.Location = new System.Drawing.Point(139, 5);
            this.lblBumonnCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBumonnCd.Name = "lblBumonnCd";
            this.lblBumonnCd.Size = new System.Drawing.Size(283, 24);
            this.lblBumonnCd.TabIndex = 14;
            this.lblBumonnCd.Text = "1：表示する　０：表示しない";
            this.lblBumonnCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHojoKamokuCd
            // 
            this.lblHojoKamokuCd.BackColor = System.Drawing.Color.LightCyan;
            this.lblHojoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuCd.Location = new System.Drawing.Point(139, 5);
            this.lblHojoKamokuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHojoKamokuCd.Name = "lblHojoKamokuCd";
            this.lblHojoKamokuCd.Size = new System.Drawing.Size(283, 24);
            this.lblHojoKamokuCd.TabIndex = 11;
            this.lblHojoKamokuCd.Text = "1：表示する　０：表示しない";
            this.lblHojoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.LightCyan;
            this.lblKanjoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(139, 6);
            this.lblKanjoKamokuCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(283, 24);
            this.lblKanjoKamokuCd.TabIndex = 8;
            this.lblKanjoKamokuCd.Text = "1：表示する　０：表示しない";
            this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKamokbn
            // 
            this.lblKamokbn.BackColor = System.Drawing.Color.LightCyan;
            this.lblKamokbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKamokbn.Location = new System.Drawing.Point(139, 6);
            this.lblKamokbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKamokbn.Name = "lblKamokbn";
            this.lblKamokbn.Size = new System.Drawing.Size(283, 24);
            this.lblKamokbn.TabIndex = 5;
            this.lblKamokbn.Text = "1：表示する　０：表示しない";
            this.lblKamokbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZeiKbnn
            // 
            this.lblZeiKbnn.BackColor = System.Drawing.Color.LightCyan;
            this.lblZeiKbnn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKbnn.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKbnn.Location = new System.Drawing.Point(139, 5);
            this.lblZeiKbnn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZeiKbnn.Name = "lblZeiKbnn";
            this.lblZeiKbnn.Size = new System.Drawing.Size(283, 24);
            this.lblZeiKbnn.TabIndex = 17;
            this.lblZeiKbnn.Text = "1：表示する　０：表示しない";
            this.lblZeiKbnn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyo
            // 
            this.txtKyuyo.AutoSizeFromLength = true;
            this.txtKyuyo.DisplayLength = null;
            this.txtKyuyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKyuyo.Location = new System.Drawing.Point(100, 6);
            this.txtKyuyo.Margin = new System.Windows.Forms.Padding(4);
            this.txtKyuyo.MaxLength = 4;
            this.txtKyuyo.Name = "txtKyuyo";
            this.txtKyuyo.Size = new System.Drawing.Size(29, 23);
            this.txtKyuyo.TabIndex = 16;
            this.txtKyuyo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKyuyo_KeyDown);
            this.txtKyuyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyo_Validating);
            // 
            // lblKyuyo
            // 
            this.lblKyuyo.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKyuyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyo.Location = new System.Drawing.Point(0, 0);
            this.lblKyuyo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKyuyo.Name = "lblKyuyo";
            this.lblKyuyo.Size = new System.Drawing.Size(547, 37);
            this.lblKyuyo.TabIndex = 15;
            this.lblKyuyo.Tag = "CHANGE";
            this.lblKyuyo.Text = "給　　与";
            this.lblKyuyo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZaimu
            // 
            this.txtZaimu.AutoSizeFromLength = true;
            this.txtZaimu.DisplayLength = null;
            this.txtZaimu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZaimu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtZaimu.Location = new System.Drawing.Point(100, 6);
            this.txtZaimu.Margin = new System.Windows.Forms.Padding(4);
            this.txtZaimu.MaxLength = 7;
            this.txtZaimu.Name = "txtZaimu";
            this.txtZaimu.Size = new System.Drawing.Size(29, 23);
            this.txtZaimu.TabIndex = 13;
            this.txtZaimu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZaimu.Validating += new System.ComponentModel.CancelEventHandler(this.txtZaimu_Validating);
            // 
            // lblZaimu
            // 
            this.lblZaimu.BackColor = System.Drawing.Color.Silver;
            this.lblZaimu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZaimu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZaimu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZaimu.Location = new System.Drawing.Point(0, 0);
            this.lblZaimu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZaimu.Name = "lblZaimu";
            this.lblZaimu.Size = new System.Drawing.Size(547, 37);
            this.lblZaimu.TabIndex = 12;
            this.lblZaimu.Tag = "CHANGE";
            this.lblZaimu.Text = "財　　務";
            this.lblZaimu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShokudo
            // 
            this.txtShokudo.AutoSizeFromLength = true;
            this.txtShokudo.DisplayLength = null;
            this.txtShokudo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShokudo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShokudo.Location = new System.Drawing.Point(100, 6);
            this.txtShokudo.Margin = new System.Windows.Forms.Padding(4);
            this.txtShokudo.MaxLength = 7;
            this.txtShokudo.Name = "txtShokudo";
            this.txtShokudo.Size = new System.Drawing.Size(29, 23);
            this.txtShokudo.TabIndex = 10;
            this.txtShokudo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShokudo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShokudo_Validating);
            // 
            // lblShokudo
            // 
            this.lblShokudo.BackColor = System.Drawing.Color.Silver;
            this.lblShokudo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShokudo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShokudo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShokudo.Location = new System.Drawing.Point(0, 0);
            this.lblShokudo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShokudo.Name = "lblShokudo";
            this.lblShokudo.Size = new System.Drawing.Size(547, 37);
            this.lblShokudo.TabIndex = 9;
            this.lblShokudo.Tag = "CHANGE";
            this.lblShokudo.Text = "食　　堂";
            this.lblShokudo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKobai
            // 
            this.txtKobai.AutoSizeFromLength = false;
            this.txtKobai.DisplayLength = null;
            this.txtKobai.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKobai.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKobai.Location = new System.Drawing.Point(100, 7);
            this.txtKobai.Margin = new System.Windows.Forms.Padding(4);
            this.txtKobai.MaxLength = 7;
            this.txtKobai.Name = "txtKobai";
            this.txtKobai.Size = new System.Drawing.Size(29, 23);
            this.txtKobai.TabIndex = 7;
            this.txtKobai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKobai.Validating += new System.ComponentModel.CancelEventHandler(this.txtKobai_Validating);
            // 
            // lblKobai
            // 
            this.lblKobai.BackColor = System.Drawing.Color.Silver;
            this.lblKobai.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKobai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKobai.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKobai.Location = new System.Drawing.Point(0, 0);
            this.lblKobai.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKobai.Name = "lblKobai";
            this.lblKobai.Size = new System.Drawing.Size(547, 37);
            this.lblKobai.TabIndex = 6;
            this.lblKobai.Tag = "CHANGE";
            this.lblKobai.Text = "購　　買";
            this.lblKobai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeri
            // 
            this.txtSeri.AutoSizeFromLength = false;
            this.txtSeri.DisplayLength = null;
            this.txtSeri.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeri.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSeri.Location = new System.Drawing.Point(100, 7);
            this.txtSeri.Margin = new System.Windows.Forms.Padding(4);
            this.txtSeri.MaxLength = 1;
            this.txtSeri.Name = "txtSeri";
            this.txtSeri.Size = new System.Drawing.Size(29, 23);
            this.txtSeri.TabIndex = 4;
            this.txtSeri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeri.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeri_Validating);
            // 
            // lblSeri
            // 
            this.lblSeri.BackColor = System.Drawing.Color.Silver;
            this.lblSeri.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSeri.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeri.Location = new System.Drawing.Point(0, 0);
            this.lblSeri.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSeri.Name = "lblSeri";
            this.lblSeri.Size = new System.Drawing.Size(547, 37);
            this.lblSeri.TabIndex = 3;
            this.lblSeri.Tag = "CHANGE";
            this.lblSeri.Text = "セリ販売";
            this.lblSeri.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUserKubun
            // 
            this.txtUserKubun.AutoSizeFromLength = true;
            this.txtUserKubun.DisplayLength = null;
            this.txtUserKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUserKubun.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtUserKubun.Location = new System.Drawing.Point(100, 7);
            this.txtUserKubun.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserKubun.MaxLength = 15;
            this.txtUserKubun.Name = "txtUserKubun";
            this.txtUserKubun.Size = new System.Drawing.Size(29, 23);
            this.txtUserKubun.TabIndex = 1;
            this.txtUserKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUserKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtUserKubun_Validating);
            // 
            // lblUserKubun
            // 
            this.lblUserKubun.BackColor = System.Drawing.Color.Silver;
            this.lblUserKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUserKubun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUserKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblUserKubun.Location = new System.Drawing.Point(0, 0);
            this.lblUserKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserKubun.Name = "lblUserKubun";
            this.lblUserKubun.Size = new System.Drawing.Size(547, 37);
            this.lblUserKubun.TabIndex = 0;
            this.lblUserKubun.Tag = "CHANGE";
            this.lblUserKubun.Text = "権　　限";
            this.lblUserKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShishoCd
            // 
            this.txtShishoCd.AutoSizeFromLength = true;
            this.txtShishoCd.DisplayLength = null;
            this.txtShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShishoCd.Location = new System.Drawing.Point(121, 6);
            this.txtShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtShishoCd.MaxLength = 5;
            this.txtShishoCd.Name = "txtShishoCd";
            this.txtShishoCd.Size = new System.Drawing.Size(67, 23);
            this.txtShishoCd.TabIndex = 1;
            this.txtShishoCd.TabStop = false;
            this.txtShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblShishoNm
            // 
            this.lblShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoNm.Location = new System.Drawing.Point(196, 5);
            this.lblShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShishoNm.Name = "lblShishoNm";
            this.lblShishoNm.Size = new System.Drawing.Size(301, 24);
            this.lblShishoNm.TabIndex = 2;
            this.lblShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
            this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(534, 37);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "支        所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // shosai
            // 
            this.shosai.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.shosai.ColumnCount = 1;
            this.shosai.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.shosai.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.shosai.Controls.Add(this.panel13, 0, 5);
            this.shosai.Controls.Add(this.panel12, 0, 4);
            this.shosai.Controls.Add(this.panel11, 0, 3);
            this.shosai.Controls.Add(this.panel10, 0, 2);
            this.shosai.Controls.Add(this.panel9, 0, 1);
            this.shosai.Controls.Add(this.panel8, 0, 0);
            this.shosai.Location = new System.Drawing.Point(571, 45);
            this.shosai.Name = "shosai";
            this.shosai.RowCount = 7;
            this.shosai.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.shosai.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.shosai.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.shosai.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.shosai.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.shosai.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.shosai.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.shosai.Size = new System.Drawing.Size(555, 316);
            this.shosai.TabIndex = 1000;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.lblZeiKbnn);
            this.panel13.Controls.Add(this.txtKyuyo);
            this.panel13.Controls.Add(this.lblKyuyo);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(4, 224);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(547, 37);
            this.panel13.TabIndex = 5;
            this.panel13.Visible = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.lblBumonnCd);
            this.panel12.Controls.Add(this.txtZaimu);
            this.panel12.Controls.Add(this.lblZaimu);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(4, 180);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(547, 37);
            this.panel12.TabIndex = 4;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.lblHojoKamokuCd);
            this.panel11.Controls.Add(this.txtShokudo);
            this.panel11.Controls.Add(this.lblShokudo);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(4, 136);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(547, 37);
            this.panel11.TabIndex = 3;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.txtKobai);
            this.panel10.Controls.Add(this.lblKanjoKamokuCd);
            this.panel10.Controls.Add(this.lblKobai);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(4, 92);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(547, 37);
            this.panel10.TabIndex = 2;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtSeri);
            this.panel9.Controls.Add(this.lblKamokbn);
            this.panel9.Controls.Add(this.lblSeri);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(4, 48);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(547, 37);
            this.panel9.TabIndex = 1;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.txtUserKubun);
            this.panel8.Controls.Add(this.lblUserKubun);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(4, 4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(547, 37);
            this.panel8.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(11, 45);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(542, 316);
            this.tableLayoutPanel1.TabIndex = 1000;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtPassWord);
            this.panel7.Controls.Add(this.lblpassword);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(4, 268);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(534, 44);
            this.panel7.TabIndex = 6;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.txtUserId);
            this.panel6.Controls.Add(this.lblUserId);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(4, 224);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(534, 37);
            this.panel6.TabIndex = 5;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblBumonNm);
            this.panel5.Controls.Add(this.txtBumon);
            this.panel5.Controls.Add(this.lblBumon);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(4, 180);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(534, 37);
            this.panel5.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtTantoshaKanaNm);
            this.panel4.Controls.Add(this.lblTantoshaKanaNm);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(4, 136);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(534, 37);
            this.panel4.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtTantoshaNm);
            this.panel3.Controls.Add(this.lblTantoshaNm);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 92);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(534, 37);
            this.panel3.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtTantoshaCd);
            this.panel2.Controls.Add(this.lblTantoshaCd);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(4, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(534, 37);
            this.panel2.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblShishoNm);
            this.panel1.Controls.Add(this.txtShishoCd);
            this.panel1.Controls.Add(this.lblMizuageShisho);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(534, 37);
            this.panel1.TabIndex = 0;
            // 
            // CMCM2022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 678);
            this.Controls.Add(this.shosai);
            this.Controls.Add(this.tableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "CMCM2022";
            this.ShowFButton = true;
            this.Text = "担当者の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.shosai, 0);
            this.pnlDebug.ResumeLayout(false);
            this.shosai.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Label lblTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaNm;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaKanaNm;
        private System.Windows.Forms.Label lblTantoshaKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtBumon;
        private System.Windows.Forms.Label lblBumon;
        private System.Windows.Forms.Label lblBumonNm;
        private System.Windows.Forms.Label lblUserId;
        private common.controls.FsiTextBox txtUserId;
        private common.controls.FsiTextBox txtPassWord;
        private System.Windows.Forms.Label lblpassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblBumonnCd;
        private System.Windows.Forms.Label lblHojoKamokuCd;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private System.Windows.Forms.Label lblKamokbn;
        private System.Windows.Forms.Label lblZeiKbnn;
        private common.controls.FsiTextBox txtKyuyo;
        private System.Windows.Forms.Label lblKyuyo;
        private common.controls.FsiTextBox txtZaimu;
        private System.Windows.Forms.Label lblZaimu;
        private common.controls.FsiTextBox txtShokudo;
        private System.Windows.Forms.Label lblShokudo;
        private common.controls.FsiTextBox txtKobai;
        private System.Windows.Forms.Label lblKobai;
        private common.controls.FsiTextBox txtSeri;
        private System.Windows.Forms.Label lblSeri;
        private common.controls.FsiTextBox txtUserKubun;
        private System.Windows.Forms.Label lblUserKubun;
        private common.controls.FsiTextBox txtShishoCd;
        private System.Windows.Forms.Label lblShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private jp.co.fsi.common.FsiTableLayoutPanel shosai;
        private jp.co.fsi.common.FsiPanel panel13;
        private jp.co.fsi.common.FsiPanel panel12;
        private jp.co.fsi.common.FsiPanel panel11;
        private jp.co.fsi.common.FsiPanel panel10;
        private jp.co.fsi.common.FsiPanel panel9;
        private jp.co.fsi.common.FsiPanel panel8;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel1;
        private jp.co.fsi.common.FsiPanel panel7;
        private jp.co.fsi.common.FsiPanel panel6;
        private jp.co.fsi.common.FsiPanel panel5;
        private jp.co.fsi.common.FsiPanel panel4;
        private jp.co.fsi.common.FsiPanel panel3;
        private jp.co.fsi.common.FsiPanel panel2;
        private jp.co.fsi.common.FsiPanel panel1;
    }
}
