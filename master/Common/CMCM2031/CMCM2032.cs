﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Security.Cryptography;
using jp.co.fsi.common.mynumber;

namespace jp.co.fsi.cm.cmcm2031
{
    /// <summary>
    /// 支所マスタ登録(KOBC9022)
    /// </summary>
    public partial class CMCM2032 : BasePgForm
    {
        private bool _lastControlCheck = false;         // 

        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";

        /// <summary>
        /// モード(購買)
        /// </summary>
        private const string MODE_KOBAI = "1";

        /// <summary>
        /// モード(販売)
        /// </summary>
        private const string MODE_HANBAI = "2";

        //パスワードに使用する文字
        private static readonly string passwordChars = "0123456789abcdefghijklmnopqrstuvwxyz";
        #endregion

        #region プロパティ
        /// <summary>
        /// 公開フラグ用変数
        /// </summary>
        private bool _numberFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._numberFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2032()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        /// <param name="par2">引数2</param>
        public CMCM2032(string par1, string par2)
            : base(par1, par2)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {

            // タイトルは非表示
            this.lblTitle.Visible = false;

            //ボタン表示
            this.ShowFButton = true;

            // 引数：Par1／モード(1:新規、2:変更)、InData：仕入先コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                //F3非活性化
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
                ////F3活性化
                //this.btnF3.Enabled = true;
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // ボタンの配置を調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;

            // ボタンを調整
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // フォーカス設定
            this.txtShishoNm.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 郵便番号1・2、住所1・2、担当者コード、消費税入力方法、生年月日の年、加入日の年、脱退日の年、
            // 船名CD、漁法CD、地区CDに、
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtYubinBango1":
                case "txtYubinBango2":
                case "txtJusho1":
                case "txtJusho2":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtYubinBango1":
                case "txtYubinBango2":
                case "txtJusho1":
                case "txtJusho2":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1031.CMCM1031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.ShowDialog();

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtYubinBango1.Text = outData[0];
                                this.txtYubinBango2.Text = outData[1];
                                this.txtJusho1.Text = outData[2];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モード時
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 6, this.txtShishoCd.Text);
            try
            {
                this.Dba.BeginTransaction();

                // 支所
                this.Dba.Delete("TB_CM_SHISHO",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD",
                    (DbParamCollection)delParams[0]);
                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTori = SetCmToriParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.支所マスタ
                    this.Dba.Insert("TB_CM_SHISHO", (DbParamCollection)alParamsCmTori[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.支所マスタ
                    this.Dba.Update("TB_CM_SHISHO",
                        (DbParamCollection)alParamsCmTori[1],
                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD",
                        (DbParamCollection)alParamsCmTori[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 支所コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShishoCd())
            {
                e.Cancel = true;
                this.txtShishoCd.SelectAll();
            }
        }

        /// <summary>
        /// 正式支所名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShishoNm())
            {
                e.Cancel = true;
                this.txtShishoNm.SelectAll();
            }
        }

        /// <summary>
        /// 支所カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShishoKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShishoKanaNm())
            {
                e.Cancel = true;
                this.txtShishoKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 郵便番号(上3桁)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango1())
            {
                e.Cancel = true;
                this.txtYubinBango1.SelectAll();
            }
        }

        /// <summary>
        /// 郵便番号(下4桁)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango2())
            {
                e.Cancel = true;
                this.txtYubinBango2.SelectAll();
            }
        }

        /// <summary>
        /// 住所１の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho1())
            {
                e.Cancel = true;
                this.txtJusho1.SelectAll();
            }
        }

        /// <summary>
        /// 住所２の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho2())
            {
                e.Cancel = true;
                this.txtJusho2.SelectAll();
            }
        }

        /// <summary>
        /// 電話番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTel_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTel())
            {
                e.Cancel = true;
                this.txtTel.SelectAll();
            }
        }

        /// <summary>
        /// FAX番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFax_Validating(object sender, CancelEventArgs e)
        {
            this._lastControlCheck = false;
            if (!IsValidFax())
            {
                e.Cancel = true;
                this.txtFax.SelectAll();
                return;
            }
            this._lastControlCheck = true;
        }


        private void txtFax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this._lastControlCheck)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 支所コードの初期値を取得
            // 支所の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            DataTable dtMaxFnns =
                this.Dba.GetDataTableByConditionWithParams("MAX(SHISHO_CD) AS MAX_CD",
                    "TB_CM_SHISHO", Util.ToString(where), dpc);
            if (dtMaxFnns.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxFnns.Rows[0]["MAX_CD"]))
            {
                this.txtShishoCd.Text = Util.ToString(Util.ToInt(dtMaxFnns.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtShishoCd.Text = "1";
            }
            
            // 各項目の初期値を設定

            // 正式支所名に初期フォーカス
            this.ActiveControl = this.txtShishoNm;
            this.txtShishoNm.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append(" ,SHISHO_CD"); // 支所コード
            cols.Append(" ,SHISHO_NM"); // 正式支所名
            cols.Append(" ,SHISHO_KANA_NM"); // 支所カナ名
            cols.Append(" ,YUBIN_BANGO1"); // 郵便番号1
            cols.Append(" ,YUBIN_BANGO2"); // 郵便番号2
            cols.Append(" ,JUSHO1"); // 住所1
            cols.Append(" ,JUSHO2"); // 住所2
            cols.Append(" ,DENWA_BANGO"); // 電話番号
            cols.Append(" ,FAX_BANGO"); // FAX番号

            StringBuilder from = new StringBuilder();
            from.Append("TB_CM_SHISHO AS A");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.SHISHO_CD = @SHISHO_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtShishoCd.Text = Util.ToString(drDispData["SHISHO_CD"]); // 支所コード
            this.txtShishoNm.Text = Util.ToString(drDispData["SHISHO_NM"]); // 正式支所名
            this.txtShishoKanaNm.Text = Util.ToString(drDispData["SHISHO_KANA_NM"]); // 支所カナ名
            this.txtYubinBango1.Text = Util.ToString(drDispData["YUBIN_BANGO1"]); // 郵便番号1
            this.txtYubinBango2.Text = Util.ToString(drDispData["YUBIN_BANGO2"]); // 郵便番号2
            this.txtJusho1.Text = Util.ToString(drDispData["JUSHO1"]); // 住所1
            this.txtJusho2.Text = Util.ToString(drDispData["JUSHO2"]); // 住所2
            this.txtTel.Text = Util.ToString(drDispData["DENWA_BANGO"]); // 電話番号
            this.txtFax.Text = Util.ToString(drDispData["FAX_BANGO"]); // FAX番号

            this.txtShishoCd.Enabled = false;

            // 削除ボタン制御を実装
            //// TB_ZM_SHIWAKE_MEISAIのデータを取得
            //DbParamCollection dpcDenpyo = new DbParamCollection();
            //StringBuilder Sql = new StringBuilder();
            //Sql.Append(" SELECT");
            //Sql.Append(" DENPYO_BANGO");
            //Sql.Append(" FROM");
            //Sql.Append(" TB_HN_TORIHIKI_DENPYO");
            //Sql.Append(" WHERE");
            //Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            //Sql.Append(" SHISHO_CD = @SHISHO_CD ");
            //dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            //dpcDenpyo.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, this.txtShishoCd.Text);

            //DataTable dtDenpyoUmu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpcDenpyo);
            //if (dtDenpyoUmu.Rows.Count == 0)
            //{
            //    this.btnF3.Enabled = true;
            //}
            //else
            //{
            //    this.btnF3.Enabled = false;
            //}
            this.btnF3.Enabled = IsValidShishoCd(Util.ToDecimal(this.txtShishoCd.Text));
        }

        /// <summary>
        /// 支所コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShishoCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShishoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 4バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShishoCd.Text, this.txtShishoCd.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            DataTable dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("SHISHO_CD",
                    "TB_CM_SHISHO", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                Msg.Error("既に存在する支所コードと重複しています。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 支所コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShishoCd(decimal Code)
        {

            // 選択された支所コードが使用されているかチェックする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);

            //仕切データチェック
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            DataTable dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("SHISHO_CD",
                    "TB_HN_SHIKIRI_DATA", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                return false;
            }
            //取引伝票チェック
            where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("SHISHO_CD",
                    "TB_HN_TORIHIKI_DENPYO", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                return false;
            }
            //仕訳伝票チェック
            where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("SHISHO_CD",
                    "TB_ZM_SHIWAKE_DENPYO", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 正式支所名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShishoNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShishoNm.Text, this.txtShishoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 支所カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShishoKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShishoKanaNm.Text, this.txtShishoKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(上3桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango1()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 郵便番号(下4桁)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango2()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所１の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho1()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho1.Text, this.txtJusho1.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 住所２の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho2()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtJusho2.Text, this.txtJusho2.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 電話番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTel()
        {
            //// 15バイトを超えていたらエラー
            //if (!ValChk.IsWithinLength(this.txtTel.Text, this.txtTel.MaxLength))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}

            //return true;
            if (!string.IsNullOrEmpty(this.txtTel.Text))
            {
                if (!IsPhoneNumber(this.txtTel.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// FAX番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFax()
        {
            //// 15バイトを超えていたらエラー
            //if (!ValChk.IsWithinLength(this.txtFax.Text, this.txtFax.MaxLength))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}

            //return true;
            if (!string.IsNullOrEmpty(this.txtFax.Text))
            {
                if (!IsPhoneNumber(this.txtFax.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        private bool IsPhoneNumber(string number)
        {
            return Regex.IsMatch(number, @"^0\d{1,4}-\d{1,4}-\d{4}$");
            //return Regex.IsMatch(number, @"\A0\d{1,4}-\d{1,4}-\d{4}\z");
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 支所コードのチェック
                if (!IsValidShishoCd())
                {
                    this.txtShishoCd.Focus();
                    return false;
                }
            }

            // 正式支所名のチェック
            if (!IsValidShishoNm())
            {
                this.txtShishoNm.Focus();
                return false;
            }
            // 登録jは名称の入力チェック
            if (this.txtShishoNm.Text.Length == 0)
            {
                Msg.Error("入力に誤りがあります。");
                this.txtShishoNm.Focus();
                return false;
            }

            // 支所カナ名のチェック
            if (!IsValidShishoKanaNm())
            {
                this.txtShishoKanaNm.Focus();
                return false;
            }


            // 郵便番号１のチェック
            if (!IsValidYubinBango1())
            {
                this.txtYubinBango1.Focus();
                return false;
            }

            // 郵便番号２のチェック
            if (!IsValidYubinBango2())
            {
                this.txtYubinBango2.Focus();
                return false;
            }

            // 郵便番号２だけの入力はエラー
            if (ValChk.IsEmpty(this.txtYubinBango1.Text) &&
                !ValChk.IsEmpty(this.txtYubinBango2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                this.txtYubinBango1.Focus();
                return false;
            }

            // 住所１のチェック
            if (!IsValidJusho1())
            {
                this.txtJusho1.Focus();
                return false;
            }

            // 住所２のチェック
            if (!IsValidJusho2())
            {
                this.txtJusho2.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_CM_SHISHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと支所コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと支所コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
                alParams.Add(whereParam);
            }

            // 支所名
            updParam.SetParam("@SHISHO_NM", SqlDbType.VarChar, 40, this.txtShishoNm.Text);
            // 支所カナ名
            updParam.SetParam("@SHISHO_KANA_NM", SqlDbType.VarChar, 30, this.txtShishoKanaNm.Text);
            // 郵便番号1
            updParam.SetParam("@YUBIN_BANGO1", SqlDbType.VarChar, 3, this.txtYubinBango1.Text);
            // 郵便番号2
            updParam.SetParam("@YUBIN_BANGO2", SqlDbType.VarChar, 4, this.txtYubinBango2.Text);
            // 住所1
            updParam.SetParam("@JUSHO1", SqlDbType.VarChar, 30, this.txtJusho1.Text);
            // 住所2
            updParam.SetParam("@JUSHO2", SqlDbType.VarChar, 30, this.txtJusho2.Text);
            // 住所3
            updParam.SetParam("@JUSHO3", SqlDbType.VarChar, 30, "");
            // 電話番号
            updParam.SetParam("@DENWA_BANGO", SqlDbType.VarChar, 15, this.txtTel.Text);
            // FAX番号
            updParam.SetParam("@FAX_BANGO", SqlDbType.VarChar, 15, this.txtFax.Text);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_CM_SHISHOからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 会社コードと支所コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcDenpyo = new DbParamCollection();
            dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcDenpyo.SetParam("@SHISHO_CD", SqlDbType.VarChar, 4, this.txtShishoCd.Text);

            alParams.Add(dpcDenpyo);

            return alParams;
        }
        #endregion
    }
}
