﻿namespace jp.co.fsi.cm.cmcm2031
{
    partial class CMCM2031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtKanaName = new jp.co.fsi.common.controls.FsiTextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblKanaNm = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.panel3 = new jp.co.fsi.common.FsiPanel();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(5, 5);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            this.btnEsc.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(5, 87);
            this.btnF1.Margin = new System.Windows.Forms.Padding(5);
            this.btnF1.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF2
            // 
            this.btnF2.Location = new System.Drawing.Point(119, 87);
            this.btnF2.Margin = new System.Windows.Forms.Padding(5);
            this.btnF2.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF3
            // 
            this.btnF3.Location = new System.Drawing.Point(233, 87);
            this.btnF3.Margin = new System.Windows.Forms.Padding(5);
            this.btnF3.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF4
            // 
            this.btnF4.Location = new System.Drawing.Point(347, 87);
            this.btnF4.Margin = new System.Windows.Forms.Padding(5);
            this.btnF4.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF5
            // 
            this.btnF5.Location = new System.Drawing.Point(460, 87);
            this.btnF5.Margin = new System.Windows.Forms.Padding(5);
            this.btnF5.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(688, 87);
            this.btnF7.Margin = new System.Windows.Forms.Padding(5);
            this.btnF7.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(575, 87);
            this.btnF6.Margin = new System.Windows.Forms.Padding(5);
            this.btnF6.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(801, 87);
            this.btnF8.Margin = new System.Windows.Forms.Padding(5);
            this.btnF8.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF9
            // 
            this.btnF9.Location = new System.Drawing.Point(916, 87);
            this.btnF9.Margin = new System.Windows.Forms.Padding(5);
            this.btnF9.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF12
            // 
            this.btnF12.Location = new System.Drawing.Point(1257, 87);
            this.btnF12.Margin = new System.Windows.Forms.Padding(5);
            this.btnF12.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF11
            // 
            this.btnF11.Location = new System.Drawing.Point(1143, 87);
            this.btnF11.Margin = new System.Windows.Forms.Padding(5);
            this.btnF11.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF10
            // 
            this.btnF10.Location = new System.Drawing.Point(1029, 87);
            this.btnF10.Margin = new System.Windows.Forms.Padding(5);
            this.btnF10.Size = new System.Drawing.Size(116, 80);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(9, 778);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1340, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1340, 31);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "支所の登録";
            // 
            // txtKanaName
            // 
            this.txtKanaName.AllowDrop = true;
            this.txtKanaName.AutoSizeFromLength = false;
            this.txtKanaName.DisplayLength = null;
            this.txtKanaName.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Location = new System.Drawing.Point(127, 8);
            this.txtKanaName.Margin = new System.Windows.Forms.Padding(4);
            this.txtKanaName.MaxLength = 30;
            this.txtKanaName.Name = "txtKanaName";
            this.txtKanaName.Size = new System.Drawing.Size(357, 23);
            this.txtKanaName.TabIndex = 1;
            this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(0, 0);
            this.dgvList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(617, 411);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // lblKanaNm
            // 
            this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanaNm.Location = new System.Drawing.Point(0, 0);
            this.lblKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKanaNm.Name = "lblKanaNm";
            this.lblKanaNm.Size = new System.Drawing.Size(617, 40);
            this.lblKanaNm.TabIndex = 1000;
            this.lblKanaNm.Tag = "CHANGE";
            this.lblKanaNm.Text = "カ　ナ　名";
            this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 41);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(625, 466);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvList);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 51);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(617, 411);
            this.panel3.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtKanaName);
            this.panel2.Controls.Add(this.lblKanaNm);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(617, 40);
            this.panel2.TabIndex = 0;
            // 
            // CMCM2031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 678);
            this.Controls.Add(this.tableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "CMCM2031";
            this.Par1 = "1";
            this.Text = "支所の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblKanaNm;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel1;
        private jp.co.fsi.common.FsiPanel panel3;
        private jp.co.fsi.common.FsiPanel panel2;
    }
}