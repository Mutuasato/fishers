﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm1041
{
    /// <summary>
    /// 汎用コード検索(CMCM1041)
    /// </summary>
    public partial class CMCM1041 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// データの有無を判断するフラグ
        /// </summary>
        private bool _dtFrg = false;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM1041()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 引数Par1のテーブル(あるいはVIEW)からデータを取得して表示
            DataTable dtList =
                this.Dba.GetCdNmList(this.UInfo, this.Par1, "");
            this.dgvList.DataSource = dtList;

            this.btnF12.Enabled = false;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//           this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // コードは右寄せ
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            // 列幅を設定する
            // 区分によって変動させる。画面サイズも。
            //switch (this.Par1)
            //{
            //    case "TB_CM_BUMON":
            //        this.Size = new Size(320, 475);
            //        this.dgvList.Size = new Size(280, 353);
            //        this.dgvList.Columns[0].Width = 100;
            //        this.dgvList.Columns[1].Width = 177;
            //        break;

            //    case "TB_ZM_F_ZEI_KUBUN":
            //        this.Size = new Size(320, 525);
            //        this.dgvList.Size = new Size(280, 403);
            //        this.dgvList.Columns[0].Width = 50;
            //        this.dgvList.Columns[1].Width = 227;
            //        break;

            //    case "TB_ZM_F_JIGYO_KUBUN":
            //        this.Size = new Size(293, 319);
            //        this.dgvList.Size = new Size(253, 200);
            //        this.dgvList.Columns[0].Width = 50;
            //        this.dgvList.Columns[1].Width = 200;
            //        break;

            //    case "VI_HN_SHOHIN_KBN1":
            //    case "VI_HN_SHOHIN_KBN2":
            //    case "VI_HN_SHOHIN_KBN3":
            //    case "VI_HN_SHOHIN_KBN4":
            //    case "VI_HN_SHOHIN_KBN5":
            //    case "VI_HN_SHOHIN_KBN6":
            //    case "VI_HN_SHOHIN_KBN7":
            //    case "VI_HN_SHOHIN_KBN8":
            //        this.Size = new Size(293, 318);
            //        this.dgvList.Size = new Size(253, 200);
            //        this.dgvList.Columns[0].Width = 50;
            //        this.dgvList.Columns[1].Width = 200;
            //        this.btnF11.Visible = true;
            //        this.btnF11.Enabled = true;
            //        this.btnF11.Location = this.btnF3.Location;
            //        this.btnF11.Text = "F11\r\n\r\n追加";
            //        break;

            //    case "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO":
            //        this.Size = new Size(293, 228);
            //        this.dgvList.Size = new Size(253, 108);
            //        this.dgvList.Columns[0].Width = 50;
            //        this.dgvList.Columns[1].Width = 200;
            //        break;

            //    case "TB_HN_COMBO_DATA_SEISAN":
            //    case "TB_HN_COMBO_DATA_MIZUAGE":
            //    case "TB_HN_F_TORIHIKI_KUBUN1":
            //    case "TB_HN_F_TORIHIKI_KUBUN2":
            //    case "TB_HN_F_TORIHIKI_KUBUN8":
            //    case "VI_HN_HANBAI_KUBUN_NM":
            //    case "VI_HN_HANBAI_KUBUN_NM2":
            //        this.Size = new Size(293, 345);
            //        this.dgvList.Size = new Size(253, 228);
            //        this.dgvList.Columns[0].Width = 50;
            //        this.dgvList.Columns[1].Width = 200;
            //        break;

            //    case "TB_KY_F_KOYO_HOKEN_KEISAN_HOHO":
            //        this.Size = new Size(313, 226);
            //        this.dgvList.Size = new Size(273, 108);
            //        this.dgvList.Columns[0].Width = 70;
            //        this.dgvList.Columns[1].Width = 200;
            //        break;

            //    case "TB_KY_F_KOYO_HOKEN_GYOSHU":
            //    case "TB_KY_F_SHOTOKUZEI_KEISAN_HOHO":
            //    case "TB_KY_F_KYUYO_KEITAI":
            //        this.Size = new Size(263, 226);
            //        this.dgvList.Size = new Size(223, 108);
            //        this.dgvList.Columns[0].Width = 70;
            //        this.dgvList.Columns[1].Width = 150;
            //        break;

            //    case "TB_KY_F_HONNIN_KUBUN1":
            //    case "TB_KY_F_HONNIN_KUBUN2":
            //    case "TB_KY_F_HONNIN_KUBUN3":
            //    case "TB_KY_F_HAIGUSHA_KUBUN":
            //        this.Size = new Size(243, 226);
            //        this.dgvList.Size = new Size(203, 108);
            //        this.dgvList.Columns[0].Width = 50;
            //        this.dgvList.Columns[1].Width = 150;
            //        break;

            //    case "TB_ZM_F_KAMOKU_BUNRUI":
            //        this.Size = new Size(390, 475);
            //        this.dgvList.Size = new Size(350, 353);
            //        this.dgvList.Columns[0].Width = 100;
            //        this.dgvList.Columns[1].Width = 240;
            //        break;

            //    case "TB_ZM_F_KAMOKU_KUBUN":
            //        this.Size = new Size(293, 318);
            //        this.dgvList.Size = new Size(253, 200);
            //        this.dgvList.Columns[0].Width = 50;
            //        this.dgvList.Columns[1].Width = 200;
            //        break;

            //    default:
            //        this.dgvList.Columns[0].Width = 50;
            //        this.dgvList.Columns[1].Width = 97;
            //        break;
            //}


            this.dgvList.Columns[0].Width = 100;
            this.dgvList.Columns[1].Width = 200;


            // 引数で渡された値と一致する行を初期選択する
            if (dtList.Rows.Count > 0)
            {
                this.dgvList.Rows[0].Selected = true;
                if (!ValChk.IsEmpty(this.InData))
                {
                    for (int i = 0; i < this.dgvList.Rows.Count; i++)
                    {
                        if (Util.ToString(this.InData).Equals(
                            Util.ToString(this.dgvList.Rows[i].Cells[0].Value)))
                        {
                            this.dgvList.Rows[i].Selected = true;
                            break;
                        }
                    }
                    this.dgvList.FirstDisplayedScrollingRowIndex = this.dgvList.SelectedCells[0].RowIndex;
                }

                // Gridに初期フォーカス
                this.dgvList.Focus();
            }
            else
            {
                // データが無い時
                this._dtFrg = true;
            }
        }

        /// <summary>
        /// フォーム読み込み時処理
        /// </summary>
        private void CMCM1041_Shown(object sender, EventArgs e)
        {
            if (this._dtFrg)
            {
                switch (this.Par1)
                {
                    case "VI_HN_SHOHIN_KBN1":
                    case "VI_HN_SHOHIN_KBN2":
                    case "VI_HN_SHOHIN_KBN3":
                    case "VI_HN_SHOHIN_KBN4":
                    case "VI_HN_SHOHIN_KBN5":
                    case "VI_HN_SHOHIN_KBN6":
                    case "VI_HN_SHOHIN_KBN7":
                    case "VI_HN_SHOHIN_KBN8":
                        if (Msg.ConfYesNo("該当データがありません。登録しますか？") == DialogResult.No)
                        {
                            this.DialogResult = DialogResult.Cancel;
                            base.PressEsc();
                        }
                        else
                        {
                            // 登録画面の起動
                            this.PressF11();
                        }
                        break;
                    default:
                        Msg.Info("該当データがありません。");
                        this.DialogResult = DialogResult.Cancel;
                        base.PressEsc();
                        break;
                }
            }
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF11();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF11()
        {
            String subTitle = "";
            String kubunShubetsu = "VI_HN_SHOHIN_KBN1";
            switch (this.Par1)
            {
                case "VI_HN_SHOHIN_KBN1":
                    subTitle = "商品区分1";
                    kubunShubetsu = "1";
                    break;
                case "VI_HN_SHOHIN_KBN2":
                    subTitle = "商品区分2";
                    kubunShubetsu = "2";
                    break;
                case "VI_HN_SHOHIN_KBN3":
                    subTitle = "商品分類";
                    kubunShubetsu = "3";
                    break;
                case "VI_HN_SHOHIN_KBN4":
                    subTitle = "商品区分4";
                    kubunShubetsu = "4";
                    break;
                case "VI_HN_SHOHIN_KBN5":
                    subTitle = "商品区分5";
                    kubunShubetsu = "5";
                    break;
                case "VI_HN_SHOHIN_KBN6":
                    subTitle = "商品区分6";
                    kubunShubetsu = "6";
                    break;
                case "VI_HN_SHOHIN_KBN7":
                    subTitle = "商品区分7";
                    kubunShubetsu = "7";
                    break;
                case "VI_HN_SHOHIN_KBN8":
                    subTitle = "商品区分8";
                    kubunShubetsu = "8";
                    break;
                default:
                    return;
            }
            switch (this.Par1)
            {
                case "VI_HN_SHOHIN_KBN1":
                case "VI_HN_SHOHIN_KBN2":
                case "VI_HN_SHOHIN_KBN3":
                case "VI_HN_SHOHIN_KBN4":
                case "VI_HN_SHOHIN_KBN5":
                case "VI_HN_SHOHIN_KBN6":
                case "VI_HN_SHOHIN_KBN7":
                case "VI_HN_SHOHIN_KBN8":
                    // 新規登録モードで登録画面を起動
                    CMCM1042 frmCMCM1042 = new CMCM1042(subTitle, kubunShubetsu);
                    DialogResult result = frmCMCM1042.ShowDialog(this);

                    if (result == DialogResult.OK)
                    {
                        // データを再検索する
                        // 引数Par1のテーブル(あるいはVIEW)からデータを取得して表示
                        DataTable dtList =
                            this.Dba.GetCdNmList(this.UInfo, this.Par1, "");
                        this.dgvList.DataSource = dtList;
                        // Gridに初期フォーカス
                        this.dgvList.Focus();
                    }
                    else
                    {
                        if (this.dgvList.RowCount == 0)
                        {
                            this.DialogResult = DialogResult.Cancel;
                            base.PressEsc();
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[2] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells[0].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells[1].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
