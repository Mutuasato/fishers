﻿namespace jp.co.fsi.cm.cmcm2011
{
    partial class CMCM2012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFunanushiCd = new System.Windows.Forms.Label();
            this.txtFunanushiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxSenshuInfo = new System.Windows.Forms.GroupBox();
            this.txthyoji = new jp.co.fsi.common.controls.FsiTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtMyNumberHide = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMyNumber = new jp.co.fsi.common.controls.FsiTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDattaiYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanyuYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDattaiDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanyuDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDattaiMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanyuMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanyuJpYMD = new System.Windows.Forms.Label();
            this.lblDattaiJpYMD = new System.Windows.Forms.Label();
            this.txtJpDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpYMD = new System.Windows.Forms.Label();
            this.txtKaishuBi = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDattaiGengo = new System.Windows.Forms.Label();
            this.lblKanyuGengo = new System.Windows.Forms.Label();
            this.lblJpGengo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
            this.lblShzHsSrMemo = new System.Windows.Forms.Label();
            this.lblSkshKskMemo = new System.Windows.Forms.Label();
            this.lblSkshHkMemo = new System.Windows.Forms.Label();
            this.lblShimebiMemo = new System.Windows.Forms.Label();
            this.lblSeikyusakiNm = new System.Windows.Forms.Label();
            this.txtKaishuTsuki = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.textBox14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeikyushoKeishiki = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeikyushoHakko = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShimebi = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeikyusakiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
            this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
            this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.lblAddBar = new System.Windows.Forms.Label();
            this.txtYubinBango2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDattaiRiyu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKingakuHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFax = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTel = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYubinBango1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRyFunanushiNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFunanushiKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFunanushiNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDattaiRiyu = new System.Windows.Forms.Label();
            this.lblDattaiJp = new System.Windows.Forms.Label();
            this.lblKanyuJp = new System.Windows.Forms.Label();
            this.lblJp = new System.Windows.Forms.Label();
            this.lblKaishuTsuki = new System.Windows.Forms.Label();
            this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
            this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
            this.lblSeikyushoKeishiki = new System.Windows.Forms.Label();
            this.lblSeikyushoHakko = new System.Windows.Forms.Label();
            this.lblShimebi = new System.Windows.Forms.Label();
            this.lblSeikyusakiCd = new System.Windows.Forms.Label();
            this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
            this.lblKingakuHasuShori = new System.Windows.Forms.Label();
            this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
            this.lblTantoshaCd = new System.Windows.Forms.Label();
            this.lblFax = new System.Windows.Forms.Label();
            this.lblTel = new System.Windows.Forms.Label();
            this.lblJusho2 = new System.Windows.Forms.Label();
            this.lblJusho1 = new System.Windows.Forms.Label();
            this.lblYubinBango = new System.Windows.Forms.Label();
            this.lblRyFunanushiNm = new System.Windows.Forms.Label();
            this.lblFunanushiKanaNm = new System.Windows.Forms.Label();
            this.lblFunanushiNm = new System.Windows.Forms.Label();
            this.tbcKoza = new System.Windows.Forms.TabControl();
            this.futsu = new System.Windows.Forms.TabPage();
            this.gbxfutsu = new System.Windows.Forms.GroupBox();
            this.txtFutsuKozaKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFutsuKozaBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFutsuKozaBango = new System.Windows.Forms.Label();
            this.lblFutsuKozaKubunSelect = new System.Windows.Forms.Label();
            this.lblFutsuKozaKubun = new System.Windows.Forms.Label();
            this.tsumitate = new System.Windows.Forms.TabPage();
            this.gbxTsumitate = new System.Windows.Forms.GroupBox();
            this.txtTsumitateKozaKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTsumitateRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTsumitateKozaBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTsumitateRitsu = new System.Windows.Forms.Label();
            this.lblTsumitateKozaBango = new System.Windows.Forms.Label();
            this.lblTsumitateKozaKubun = new System.Windows.Forms.Label();
            this.lblTsumitateKozaKubunMemo = new System.Windows.Forms.Label();
            this.azukari = new System.Windows.Forms.TabPage();
            this.gbxAzukari = new System.Windows.Forms.GroupBox();
            this.txtKozaKubunAzukari = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTsumitateRitsuAzukari = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKozaBangoAzukari = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTsumitateRitsuAzukari = new System.Windows.Forms.Label();
            this.lblKozaBangoAzukari = new System.Windows.Forms.Label();
            this.lblKozaKubunAzukari = new System.Windows.Forms.Label();
            this.lblKozaKubunAzukariSelect = new System.Windows.Forms.Label();
            this.honnin = new System.Windows.Forms.TabPage();
            this.gbxHonnin = new System.Windows.Forms.GroupBox();
            this.txtKozaKubunHonnin = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTsumitateRitsuHonnin = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKozaBangoHonnin = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTsumitateRitsuHonnin = new System.Windows.Forms.Label();
            this.lblKozaBangoHonnin = new System.Windows.Forms.Label();
            this.lblKozaKubunHonnin = new System.Windows.Forms.Label();
            this.lblKozaKubunHonninSelect = new System.Windows.Forms.Label();
            this.gbxPersonInfo = new System.Windows.Forms.GroupBox();
            this.txtPayaoRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonotaRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFutsuRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFurikomisakiWariai = new System.Windows.Forms.Label();
            this.lblPayaoRitsu = new System.Windows.Forms.Label();
            this.lblSonotaRitsu = new System.Windows.Forms.Label();
            this.lblFutsuRitsu = new System.Windows.Forms.Label();
            this.lblShishoKubunMemo = new System.Windows.Forms.Label();
            this.lblSeijunKubunMemo = new System.Windows.Forms.Label();
            this.txtSeijunKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeijunKubun = new System.Windows.Forms.Label();
            this.txtShishoKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShishoKubun = new System.Windows.Forms.Label();
            this.lblShiharaiKubunMemo = new System.Windows.Forms.Label();
            this.txtShiharaiKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiharaiKubun = new System.Windows.Forms.Label();
            this.txtKumiaiTesuryoRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKumiaiTesuryoRitsu = new System.Windows.Forms.Label();
            this.lblChikuCdSub = new System.Windows.Forms.Label();
            this.lblGyohoCdSub = new System.Windows.Forms.Label();
            this.txtChikuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGyohoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuneNmCdSub = new System.Windows.Forms.Label();
            this.txtFuneNmCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblChikuCd = new System.Windows.Forms.Label();
            this.lblGyohoCd = new System.Windows.Forms.Label();
            this.lblFuneNmCd = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSenshuInfo.SuspendLayout();
            this.tbcKoza.SuspendLayout();
            this.futsu.SuspendLayout();
            this.gbxfutsu.SuspendLayout();
            this.tsumitate.SuspendLayout();
            this.gbxTsumitate.SuspendLayout();
            this.azukari.SuspendLayout();
            this.gbxAzukari.SuspendLayout();
            this.honnin.SuspendLayout();
            this.gbxHonnin.SuspendLayout();
            this.gbxPersonInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4";
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5";
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12";
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 534);
            this.pnlDebug.Size = new System.Drawing.Size(830, 131);
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(822, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            this.lblTitle.Visible = false;
            // 
            // lblFunanushiCd
            // 
            this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCd.Location = new System.Drawing.Point(17, 3);
            this.lblFunanushiCd.Name = "lblFunanushiCd";
            this.lblFunanushiCd.Size = new System.Drawing.Size(149, 25);
            this.lblFunanushiCd.TabIndex = 0;
            this.lblFunanushiCd.Text = "船　主　C　D";
            this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCd
            // 
            this.txtFunanushiCd.AutoSizeFromLength = false;
            this.txtFunanushiCd.DisplayLength = null;
            this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFunanushiCd.Location = new System.Drawing.Point(121, 5);
            this.txtFunanushiCd.MaxLength = 4;
            this.txtFunanushiCd.Name = "txtFunanushiCd";
            this.txtFunanushiCd.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCd.TabIndex = 1;
            this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCd_Validating);
            // 
            // gbxSenshuInfo
            // 
            this.gbxSenshuInfo.Controls.Add(this.txthyoji);
            this.gbxSenshuInfo.Controls.Add(this.label4);
            this.gbxSenshuInfo.Controls.Add(this.label5);
            this.gbxSenshuInfo.Controls.Add(this.button1);
            this.gbxSenshuInfo.Controls.Add(this.txtMyNumberHide);
            this.gbxSenshuInfo.Controls.Add(this.txtMyNumber);
            this.gbxSenshuInfo.Controls.Add(this.label3);
            this.gbxSenshuInfo.Controls.Add(this.txtDattaiYear);
            this.gbxSenshuInfo.Controls.Add(this.txtKanyuYear);
            this.gbxSenshuInfo.Controls.Add(this.txtJpYear);
            this.gbxSenshuInfo.Controls.Add(this.txtDattaiDay);
            this.gbxSenshuInfo.Controls.Add(this.txtKanyuDay);
            this.gbxSenshuInfo.Controls.Add(this.txtDattaiMonth);
            this.gbxSenshuInfo.Controls.Add(this.txtKanyuMonth);
            this.gbxSenshuInfo.Controls.Add(this.lblKanyuJpYMD);
            this.gbxSenshuInfo.Controls.Add(this.lblDattaiJpYMD);
            this.gbxSenshuInfo.Controls.Add(this.txtJpDay);
            this.gbxSenshuInfo.Controls.Add(this.txtJpMonth);
            this.gbxSenshuInfo.Controls.Add(this.lblJpYMD);
            this.gbxSenshuInfo.Controls.Add(this.txtKaishuBi);
            this.gbxSenshuInfo.Controls.Add(this.label2);
            this.gbxSenshuInfo.Controls.Add(this.lblDattaiGengo);
            this.gbxSenshuInfo.Controls.Add(this.lblKanyuGengo);
            this.gbxSenshuInfo.Controls.Add(this.lblJpGengo);
            this.gbxSenshuInfo.Controls.Add(this.label1);
            this.gbxSenshuInfo.Controls.Add(this.lblShzTnkHohoMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblShzHsSrMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblSkshKskMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblSkshHkMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblShimebiMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblSeikyusakiNm);
            this.gbxSenshuInfo.Controls.Add(this.txtKaishuTsuki);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiTenkaHoho);
            this.gbxSenshuInfo.Controls.Add(this.textBox14);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.txtSeikyushoKeishiki);
            this.gbxSenshuInfo.Controls.Add(this.txtSeikyushoHakko);
            this.gbxSenshuInfo.Controls.Add(this.txtShimebi);
            this.gbxSenshuInfo.Controls.Add(this.txtSeikyusakiCd);
            this.gbxSenshuInfo.Controls.Add(this.lblShzNrkHohoNm);
            this.gbxSenshuInfo.Controls.Add(this.lblKgkHsShrMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblTnkStkHohoMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblTantoshaNm);
            this.gbxSenshuInfo.Controls.Add(this.lblAddBar);
            this.gbxSenshuInfo.Controls.Add(this.txtYubinBango2);
            this.gbxSenshuInfo.Controls.Add(this.txtDattaiRiyu);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiNyuryokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.txtKingakuHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.txtTankaShutokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.txtTantoshaCd);
            this.gbxSenshuInfo.Controls.Add(this.txtFax);
            this.gbxSenshuInfo.Controls.Add(this.txtTel);
            this.gbxSenshuInfo.Controls.Add(this.txtJusho2);
            this.gbxSenshuInfo.Controls.Add(this.txtJusho1);
            this.gbxSenshuInfo.Controls.Add(this.txtYubinBango1);
            this.gbxSenshuInfo.Controls.Add(this.txtRyFunanushiNm);
            this.gbxSenshuInfo.Controls.Add(this.txtFunanushiKanaNm);
            this.gbxSenshuInfo.Controls.Add(this.txtFunanushiNm);
            this.gbxSenshuInfo.Controls.Add(this.lblDattaiRiyu);
            this.gbxSenshuInfo.Controls.Add(this.lblDattaiJp);
            this.gbxSenshuInfo.Controls.Add(this.lblKanyuJp);
            this.gbxSenshuInfo.Controls.Add(this.lblJp);
            this.gbxSenshuInfo.Controls.Add(this.lblKaishuTsuki);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiTenkaHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.lblSeikyushoKeishiki);
            this.gbxSenshuInfo.Controls.Add(this.lblSeikyushoHakko);
            this.gbxSenshuInfo.Controls.Add(this.lblShimebi);
            this.gbxSenshuInfo.Controls.Add(this.lblSeikyusakiCd);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiNyuryokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblKingakuHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.lblTankaShutokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblTantoshaCd);
            this.gbxSenshuInfo.Controls.Add(this.lblFax);
            this.gbxSenshuInfo.Controls.Add(this.lblTel);
            this.gbxSenshuInfo.Controls.Add(this.lblJusho2);
            this.gbxSenshuInfo.Controls.Add(this.lblJusho1);
            this.gbxSenshuInfo.Controls.Add(this.lblYubinBango);
            this.gbxSenshuInfo.Controls.Add(this.lblRyFunanushiNm);
            this.gbxSenshuInfo.Controls.Add(this.lblFunanushiKanaNm);
            this.gbxSenshuInfo.Controls.Add(this.lblFunanushiNm);
            this.gbxSenshuInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSenshuInfo.Location = new System.Drawing.Point(11, 26);
            this.gbxSenshuInfo.Name = "gbxSenshuInfo";
            this.gbxSenshuInfo.Size = new System.Drawing.Size(800, 335);
            this.gbxSenshuInfo.TabIndex = 5;
            this.gbxSenshuInfo.TabStop = false;
            // 
            // txthyoji
            // 
            this.txthyoji.AutoSizeFromLength = false;
            this.txthyoji.BackColor = System.Drawing.Color.White;
            this.txthyoji.DisplayLength = null;
            this.txthyoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txthyoji.Location = new System.Drawing.Point(462, 311);
            this.txthyoji.MaxLength = 1;
            this.txthyoji.Name = "txthyoji";
            this.txthyoji.Size = new System.Drawing.Size(23, 20);
            this.txthyoji.TabIndex = 77;
            this.txthyoji.Text = "0";
            this.txthyoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txthyoji.Validating += new System.ComponentModel.CancelEventHandler(this.txthyoji_Validating);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(490, 310);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 25);
            this.label4.TabIndex = 78;
            this.label4.Text = "0:表示する 1:表示しない ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(359, 310);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 25);
            this.label5.TabIndex = 76;
            this.label5.Text = "一覧表示";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(584, 287);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 75;
            this.button1.Text = "公開";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtMyNumberHide
            // 
            this.txtMyNumberHide.AutoSizeFromLength = false;
            this.txtMyNumberHide.DisplayLength = null;
            this.txtMyNumberHide.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMyNumberHide.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMyNumberHide.Location = new System.Drawing.Point(462, 286);
            this.txtMyNumberHide.MaxLength = 15;
            this.txtMyNumberHide.Name = "txtMyNumberHide";
            this.txtMyNumberHide.ReadOnly = true;
            this.txtMyNumberHide.Size = new System.Drawing.Size(116, 23);
            this.txtMyNumberHide.TabIndex = 73;
            this.txtMyNumberHide.Text = "************";
            this.txtMyNumberHide.Visible = false;
            // 
            // txtMyNumber
            // 
            this.txtMyNumber.AutoSizeFromLength = false;
            this.txtMyNumber.DisplayLength = null;
            this.txtMyNumber.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMyNumber.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMyNumber.Location = new System.Drawing.Point(462, 286);
            this.txtMyNumber.MaxLength = 12;
            this.txtMyNumber.Name = "txtMyNumber";
            this.txtMyNumber.ReadOnly = true;
            this.txtMyNumber.Size = new System.Drawing.Size(114, 23);
            this.txtMyNumber.TabIndex = 74;
            this.txtMyNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtMyNumber_Validating);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(359, 286);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(300, 24);
            this.label3.TabIndex = 72;
            this.label3.Text = "マイナンバー";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDattaiYear
            // 
            this.txtDattaiYear.AutoSizeFromLength = false;
            this.txtDattaiYear.BackColor = System.Drawing.Color.White;
            this.txtDattaiYear.DisplayLength = null;
            this.txtDattaiYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDattaiYear.Location = new System.Drawing.Point(519, 238);
            this.txtDattaiYear.MaxLength = 2;
            this.txtDattaiYear.Name = "txtDattaiYear";
            this.txtDattaiYear.Size = new System.Drawing.Size(32, 20);
            this.txtDattaiYear.TabIndex = 67;
            this.txtDattaiYear.Text = "0";
            this.txtDattaiYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDattaiYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDattaiYear_Validating);
            // 
            // txtKanyuYear
            // 
            this.txtKanyuYear.AutoSizeFromLength = false;
            this.txtKanyuYear.BackColor = System.Drawing.Color.White;
            this.txtKanyuYear.DisplayLength = null;
            this.txtKanyuYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanyuYear.Location = new System.Drawing.Point(519, 213);
            this.txtKanyuYear.MaxLength = 2;
            this.txtKanyuYear.Name = "txtKanyuYear";
            this.txtKanyuYear.Size = new System.Drawing.Size(32, 20);
            this.txtKanyuYear.TabIndex = 61;
            this.txtKanyuYear.Text = "0";
            this.txtKanyuYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanyuYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanyuYear_Validating);
            // 
            // txtJpYear
            // 
            this.txtJpYear.AutoSizeFromLength = false;
            this.txtJpYear.BackColor = System.Drawing.Color.White;
            this.txtJpYear.DisplayLength = null;
            this.txtJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYear.Location = new System.Drawing.Point(519, 188);
            this.txtJpYear.MaxLength = 2;
            this.txtJpYear.Name = "txtJpYear";
            this.txtJpYear.Size = new System.Drawing.Size(32, 20);
            this.txtJpYear.TabIndex = 55;
            this.txtJpYear.Text = "0";
            this.txtJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpYear_Validating);
            // 
            // txtDattaiDay
            // 
            this.txtDattaiDay.AutoSizeFromLength = false;
            this.txtDattaiDay.BackColor = System.Drawing.Color.White;
            this.txtDattaiDay.DisplayLength = null;
            this.txtDattaiDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDattaiDay.Location = new System.Drawing.Point(641, 238);
            this.txtDattaiDay.MaxLength = 2;
            this.txtDattaiDay.Name = "txtDattaiDay";
            this.txtDattaiDay.Size = new System.Drawing.Size(32, 20);
            this.txtDattaiDay.TabIndex = 69;
            this.txtDattaiDay.Text = "0";
            this.txtDattaiDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDattaiDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDattaiDay_Validating);
            // 
            // txtKanyuDay
            // 
            this.txtKanyuDay.AutoSizeFromLength = false;
            this.txtKanyuDay.BackColor = System.Drawing.Color.White;
            this.txtKanyuDay.DisplayLength = null;
            this.txtKanyuDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanyuDay.Location = new System.Drawing.Point(641, 213);
            this.txtKanyuDay.MaxLength = 2;
            this.txtKanyuDay.Name = "txtKanyuDay";
            this.txtKanyuDay.Size = new System.Drawing.Size(32, 20);
            this.txtKanyuDay.TabIndex = 63;
            this.txtKanyuDay.Text = "0";
            this.txtKanyuDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanyuDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanyuDay_Validating);
            // 
            // txtDattaiMonth
            // 
            this.txtDattaiMonth.AutoSizeFromLength = false;
            this.txtDattaiMonth.BackColor = System.Drawing.Color.White;
            this.txtDattaiMonth.DisplayLength = null;
            this.txtDattaiMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDattaiMonth.Location = new System.Drawing.Point(578, 238);
            this.txtDattaiMonth.MaxLength = 2;
            this.txtDattaiMonth.Name = "txtDattaiMonth";
            this.txtDattaiMonth.Size = new System.Drawing.Size(32, 20);
            this.txtDattaiMonth.TabIndex = 68;
            this.txtDattaiMonth.Text = "0";
            this.txtDattaiMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDattaiMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDattaiMonth_Validating);
            // 
            // txtKanyuMonth
            // 
            this.txtKanyuMonth.AutoSizeFromLength = false;
            this.txtKanyuMonth.BackColor = System.Drawing.Color.White;
            this.txtKanyuMonth.DisplayLength = null;
            this.txtKanyuMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanyuMonth.Location = new System.Drawing.Point(578, 213);
            this.txtKanyuMonth.MaxLength = 2;
            this.txtKanyuMonth.Name = "txtKanyuMonth";
            this.txtKanyuMonth.Size = new System.Drawing.Size(32, 20);
            this.txtKanyuMonth.TabIndex = 62;
            this.txtKanyuMonth.Text = "0";
            this.txtKanyuMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanyuMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanyuMonth_Validating);
            // 
            // lblKanyuJpYMD
            // 
            this.lblKanyuJpYMD.BackColor = System.Drawing.Color.Silver;
            this.lblKanyuJpYMD.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanyuJpYMD.Location = new System.Drawing.Point(549, 213);
            this.lblKanyuJpYMD.Name = "lblKanyuJpYMD";
            this.lblKanyuJpYMD.Size = new System.Drawing.Size(150, 20);
            this.lblKanyuJpYMD.TabIndex = 60;
            this.lblKanyuJpYMD.Text = "年　　　 月　　　 日";
            this.lblKanyuJpYMD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDattaiJpYMD
            // 
            this.lblDattaiJpYMD.BackColor = System.Drawing.Color.Silver;
            this.lblDattaiJpYMD.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDattaiJpYMD.Location = new System.Drawing.Point(549, 237);
            this.lblDattaiJpYMD.Name = "lblDattaiJpYMD";
            this.lblDattaiJpYMD.Size = new System.Drawing.Size(150, 20);
            this.lblDattaiJpYMD.TabIndex = 66;
            this.lblDattaiJpYMD.Text = "年　　　 月 　　　日";
            this.lblDattaiJpYMD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtJpDay
            // 
            this.txtJpDay.AutoSizeFromLength = false;
            this.txtJpDay.BackColor = System.Drawing.Color.White;
            this.txtJpDay.DisplayLength = null;
            this.txtJpDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpDay.Location = new System.Drawing.Point(641, 188);
            this.txtJpDay.MaxLength = 2;
            this.txtJpDay.Name = "txtJpDay";
            this.txtJpDay.Size = new System.Drawing.Size(32, 20);
            this.txtJpDay.TabIndex = 57;
            this.txtJpDay.Text = "0";
            this.txtJpDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpDay_Validating);
            // 
            // txtJpMonth
            // 
            this.txtJpMonth.AutoSizeFromLength = false;
            this.txtJpMonth.BackColor = System.Drawing.Color.White;
            this.txtJpMonth.DisplayLength = null;
            this.txtJpMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonth.Location = new System.Drawing.Point(578, 188);
            this.txtJpMonth.MaxLength = 2;
            this.txtJpMonth.Name = "txtJpMonth";
            this.txtJpMonth.Size = new System.Drawing.Size(32, 20);
            this.txtJpMonth.TabIndex = 56;
            this.txtJpMonth.Text = "0";
            this.txtJpMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpMonth_Validating);
            // 
            // lblJpYMD
            // 
            this.lblJpYMD.BackColor = System.Drawing.Color.Silver;
            this.lblJpYMD.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpYMD.Location = new System.Drawing.Point(549, 188);
            this.lblJpYMD.Name = "lblJpYMD";
            this.lblJpYMD.Size = new System.Drawing.Size(150, 20);
            this.lblJpYMD.TabIndex = 54;
            this.lblJpYMD.Text = "年　　　 月　　 　日";
            this.lblJpYMD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtKaishuBi
            // 
            this.txtKaishuBi.AutoSizeFromLength = false;
            this.txtKaishuBi.BackColor = System.Drawing.Color.White;
            this.txtKaishuBi.DisplayLength = null;
            this.txtKaishuBi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaishuBi.Location = new System.Drawing.Point(732, 163);
            this.txtKaishuBi.MaxLength = 2;
            this.txtKaishuBi.Name = "txtKaishuBi";
            this.txtKaishuBi.Size = new System.Drawing.Size(23, 20);
            this.txtKaishuBi.TabIndex = 51;
            this.txtKaishuBi.Text = "99";
            this.txtKaishuBi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKaishuBi.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuBi_Validating);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(730, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 25);
            this.label2.TabIndex = 959;
            this.label2.Text = "日";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDattaiGengo
            // 
            this.lblDattaiGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDattaiGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDattaiGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDattaiGengo.Location = new System.Drawing.Point(462, 238);
            this.lblDattaiGengo.Name = "lblDattaiGengo";
            this.lblDattaiGengo.Size = new System.Drawing.Size(53, 20);
            this.lblDattaiGengo.TabIndex = 65;
            this.lblDattaiGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanyuGengo
            // 
            this.lblKanyuGengo.BackColor = System.Drawing.Color.Silver;
            this.lblKanyuGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanyuGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanyuGengo.Location = new System.Drawing.Point(462, 213);
            this.lblKanyuGengo.Name = "lblKanyuGengo";
            this.lblKanyuGengo.Size = new System.Drawing.Size(53, 20);
            this.lblKanyuGengo.TabIndex = 59;
            this.lblKanyuGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpGengo
            // 
            this.lblJpGengo.BackColor = System.Drawing.Color.Silver;
            this.lblJpGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpGengo.Location = new System.Drawing.Point(462, 188);
            this.lblJpGengo.Name = "lblJpGengo";
            this.lblJpGengo.Size = new System.Drawing.Size(53, 20);
            this.lblJpGengo.TabIndex = 53;
            this.lblJpGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(490, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 25);
            this.label1.TabIndex = 50;
            this.label1.Text = "0:当月 1:翌月 2:翌々月…";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShzTnkHohoMemo
            // 
            this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzTnkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(490, 136);
            this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
            this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(238, 25);
            this.lblShzTnkHohoMemo.TabIndex = 47;
            this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
            this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShzHsSrMemo
            // 
            this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzHsSrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzHsSrMemo.Location = new System.Drawing.Point(490, 111);
            this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
            this.lblShzHsSrMemo.Size = new System.Drawing.Size(238, 25);
            this.lblShzHsSrMemo.TabIndex = 44;
            this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
            this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSkshKskMemo
            // 
            this.lblSkshKskMemo.BackColor = System.Drawing.Color.Silver;
            this.lblSkshKskMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSkshKskMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSkshKskMemo.Location = new System.Drawing.Point(490, 86);
            this.lblSkshKskMemo.Name = "lblSkshKskMemo";
            this.lblSkshKskMemo.Size = new System.Drawing.Size(154, 25);
            this.lblSkshKskMemo.TabIndex = 41;
            this.lblSkshKskMemo.Text = "1:合計型 2:明細型";
            this.lblSkshKskMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSkshHkMemo
            // 
            this.lblSkshHkMemo.BackColor = System.Drawing.Color.Silver;
            this.lblSkshHkMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSkshHkMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSkshHkMemo.Location = new System.Drawing.Point(490, 61);
            this.lblSkshHkMemo.Name = "lblSkshHkMemo";
            this.lblSkshHkMemo.Size = new System.Drawing.Size(154, 25);
            this.lblSkshHkMemo.TabIndex = 38;
            this.lblSkshHkMemo.Text = "1:する 2:しない";
            this.lblSkshHkMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShimebiMemo
            // 
            this.lblShimebiMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShimebiMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebiMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShimebiMemo.Location = new System.Drawing.Point(490, 36);
            this.lblShimebiMemo.Name = "lblShimebiMemo";
            this.lblShimebiMemo.Size = new System.Drawing.Size(154, 25);
            this.lblShimebiMemo.TabIndex = 35;
            this.lblShimebiMemo.Text = "1～28 ※末締めは99";
            this.lblShimebiMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyusakiNm
            // 
            this.lblSeikyusakiNm.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyusakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyusakiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeikyusakiNm.Location = new System.Drawing.Point(508, 11);
            this.lblSeikyusakiNm.Name = "lblSeikyusakiNm";
            this.lblSeikyusakiNm.Size = new System.Drawing.Size(286, 25);
            this.lblSeikyusakiNm.TabIndex = 32;
            this.lblSeikyusakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKaishuTsuki
            // 
            this.txtKaishuTsuki.AutoSizeFromLength = false;
            this.txtKaishuTsuki.BackColor = System.Drawing.Color.White;
            this.txtKaishuTsuki.DisplayLength = null;
            this.txtKaishuTsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaishuTsuki.Location = new System.Drawing.Point(462, 163);
            this.txtKaishuTsuki.MaxLength = 1;
            this.txtKaishuTsuki.Name = "txtKaishuTsuki";
            this.txtKaishuTsuki.Size = new System.Drawing.Size(23, 20);
            this.txtKaishuTsuki.TabIndex = 49;
            this.txtKaishuTsuki.Text = "0";
            this.txtKaishuTsuki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKaishuTsuki.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuTsuki_Validating);
            // 
            // txtShohizeiTenkaHoho
            // 
            this.txtShohizeiTenkaHoho.AutoSizeFromLength = false;
            this.txtShohizeiTenkaHoho.BackColor = System.Drawing.Color.White;
            this.txtShohizeiTenkaHoho.DisplayLength = null;
            this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(462, 138);
            this.txtShohizeiTenkaHoho.MaxLength = 1;
            this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
            this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(23, 20);
            this.txtShohizeiTenkaHoho.TabIndex = 46;
            this.txtShohizeiTenkaHoho.Text = "2";
            this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
            // 
            // textBox14
            // 
            this.textBox14.AllowDrop = true;
            this.textBox14.AutoSizeFromLength = false;
            this.textBox14.DisplayLength = null;
            this.textBox14.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox14.Location = new System.Drawing.Point(919, -149);
            this.textBox14.MaxLength = 4;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(50, 19);
            this.textBox14.TabIndex = 938;
            // 
            // txtShohizeiHasuShori
            // 
            this.txtShohizeiHasuShori.AutoSizeFromLength = false;
            this.txtShohizeiHasuShori.BackColor = System.Drawing.Color.White;
            this.txtShohizeiHasuShori.DisplayLength = null;
            this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiHasuShori.Location = new System.Drawing.Point(462, 113);
            this.txtShohizeiHasuShori.MaxLength = 1;
            this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
            this.txtShohizeiHasuShori.Size = new System.Drawing.Size(23, 20);
            this.txtShohizeiHasuShori.TabIndex = 43;
            this.txtShohizeiHasuShori.Text = "2";
            this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
            // 
            // txtSeikyushoKeishiki
            // 
            this.txtSeikyushoKeishiki.AutoSizeFromLength = false;
            this.txtSeikyushoKeishiki.BackColor = System.Drawing.Color.White;
            this.txtSeikyushoKeishiki.DisplayLength = null;
            this.txtSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeikyushoKeishiki.Location = new System.Drawing.Point(462, 88);
            this.txtSeikyushoKeishiki.MaxLength = 1;
            this.txtSeikyushoKeishiki.Name = "txtSeikyushoKeishiki";
            this.txtSeikyushoKeishiki.Size = new System.Drawing.Size(23, 20);
            this.txtSeikyushoKeishiki.TabIndex = 40;
            this.txtSeikyushoKeishiki.Text = "2";
            this.txtSeikyushoKeishiki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeikyushoKeishiki.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoKeishiki_Validating);
            // 
            // txtSeikyushoHakko
            // 
            this.txtSeikyushoHakko.AutoSizeFromLength = false;
            this.txtSeikyushoHakko.BackColor = System.Drawing.Color.White;
            this.txtSeikyushoHakko.DisplayLength = null;
            this.txtSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeikyushoHakko.Location = new System.Drawing.Point(462, 63);
            this.txtSeikyushoHakko.MaxLength = 1;
            this.txtSeikyushoHakko.Name = "txtSeikyushoHakko";
            this.txtSeikyushoHakko.Size = new System.Drawing.Size(23, 20);
            this.txtSeikyushoHakko.TabIndex = 37;
            this.txtSeikyushoHakko.Text = "1";
            this.txtSeikyushoHakko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtShimebi
            // 
            this.txtShimebi.AutoSizeFromLength = false;
            this.txtShimebi.BackColor = System.Drawing.Color.White;
            this.txtShimebi.DisplayLength = null;
            this.txtShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShimebi.Location = new System.Drawing.Point(462, 38);
            this.txtShimebi.MaxLength = 2;
            this.txtShimebi.Name = "txtShimebi";
            this.txtShimebi.Size = new System.Drawing.Size(23, 20);
            this.txtShimebi.TabIndex = 34;
            this.txtShimebi.Text = "99";
            this.txtShimebi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShimebi.Validating += new System.ComponentModel.CancelEventHandler(this.txtShimebi_Validating);
            // 
            // txtSeikyusakiCd
            // 
            this.txtSeikyusakiCd.AutoSizeFromLength = true;
            this.txtSeikyusakiCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtSeikyusakiCd.DisplayLength = null;
            this.txtSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeikyusakiCd.Location = new System.Drawing.Point(462, 13);
            this.txtSeikyusakiCd.MaxLength = 4;
            this.txtSeikyusakiCd.Name = "txtSeikyusakiCd";
            this.txtSeikyusakiCd.Size = new System.Drawing.Size(40, 20);
            this.txtSeikyusakiCd.TabIndex = 31;
            this.txtSeikyusakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblShzNrkHohoNm
            // 
            this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShzNrkHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzNrkHohoNm.Location = new System.Drawing.Point(129, 286);
            this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
            this.lblShzNrkHohoNm.Size = new System.Drawing.Size(223, 25);
            this.lblShzNrkHohoNm.TabIndex = 29;
            this.lblShzNrkHohoNm.Text = "税抜き入力（自動計算あり）";
            this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKgkHsShrMemo
            // 
            this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblKgkHsShrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKgkHsShrMemo.Location = new System.Drawing.Point(129, 261);
            this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
            this.lblKgkHsShrMemo.Size = new System.Drawing.Size(223, 25);
            this.lblKgkHsShrMemo.TabIndex = 26;
            this.lblKgkHsShrMemo.Text = "1:切捨て 2:四捨五入 3:切り上げ";
            this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTnkStkHohoMemo
            // 
            this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblTnkStkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(129, 236);
            this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
            this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(223, 25);
            this.lblTnkStkHohoMemo.TabIndex = 23;
            this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
            this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNm.Location = new System.Drawing.Point(154, 211);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(198, 25);
            this.lblTantoshaNm.TabIndex = 20;
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAddBar
            // 
            this.lblAddBar.AutoSize = true;
            this.lblAddBar.BackColor = System.Drawing.Color.Silver;
            this.lblAddBar.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblAddBar.Location = new System.Drawing.Point(154, 93);
            this.lblAddBar.Name = "lblAddBar";
            this.lblAddBar.Size = new System.Drawing.Size(14, 13);
            this.lblAddBar.TabIndex = 8;
            this.lblAddBar.Text = "-";
            // 
            // txtYubinBango2
            // 
            this.txtYubinBango2.AutoSizeFromLength = false;
            this.txtYubinBango2.BackColor = System.Drawing.Color.White;
            this.txtYubinBango2.DisplayLength = null;
            this.txtYubinBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango2.Location = new System.Drawing.Point(172, 88);
            this.txtYubinBango2.MaxLength = 4;
            this.txtYubinBango2.Name = "txtYubinBango2";
            this.txtYubinBango2.Size = new System.Drawing.Size(46, 20);
            this.txtYubinBango2.TabIndex = 9;
            this.txtYubinBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango2_Validating);
            // 
            // txtDattaiRiyu
            // 
            this.txtDattaiRiyu.AutoSizeFromLength = false;
            this.txtDattaiRiyu.BackColor = System.Drawing.Color.White;
            this.txtDattaiRiyu.DisplayLength = null;
            this.txtDattaiRiyu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDattaiRiyu.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtDattaiRiyu.Location = new System.Drawing.Point(462, 263);
            this.txtDattaiRiyu.MaxLength = 40;
            this.txtDattaiRiyu.Name = "txtDattaiRiyu";
            this.txtDattaiRiyu.Size = new System.Drawing.Size(328, 20);
            this.txtDattaiRiyu.TabIndex = 71;
            this.txtDattaiRiyu.Validating += new System.ComponentModel.CancelEventHandler(this.txtDattaiRiyu_Validating);
            // 
            // txtShohizeiNyuryokuHoho
            // 
            this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
            this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.White;
            this.txtShohizeiNyuryokuHoho.DisplayLength = null;
            this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(109, 288);
            this.txtShohizeiNyuryokuHoho.MaxLength = 1;
            this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
            this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(15, 20);
            this.txtShohizeiNyuryokuHoho.TabIndex = 28;
            this.txtShohizeiNyuryokuHoho.Text = "2";
            this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiNyuryokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiNyuryokuHoho_Validating);
            // 
            // txtKingakuHasuShori
            // 
            this.txtKingakuHasuShori.AutoSizeFromLength = false;
            this.txtKingakuHasuShori.BackColor = System.Drawing.Color.White;
            this.txtKingakuHasuShori.DisplayLength = null;
            this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingakuHasuShori.Location = new System.Drawing.Point(109, 263);
            this.txtKingakuHasuShori.MaxLength = 1;
            this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
            this.txtKingakuHasuShori.Size = new System.Drawing.Size(15, 20);
            this.txtKingakuHasuShori.TabIndex = 25;
            this.txtKingakuHasuShori.Text = "2";
            this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
            // 
            // txtTankaShutokuHoho
            // 
            this.txtTankaShutokuHoho.AutoSizeFromLength = false;
            this.txtTankaShutokuHoho.BackColor = System.Drawing.Color.White;
            this.txtTankaShutokuHoho.DisplayLength = null;
            this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTankaShutokuHoho.Location = new System.Drawing.Point(109, 238);
            this.txtTankaShutokuHoho.MaxLength = 1;
            this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
            this.txtTankaShutokuHoho.Size = new System.Drawing.Size(15, 20);
            this.txtTankaShutokuHoho.TabIndex = 22;
            this.txtTankaShutokuHoho.Text = "0";
            this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = false;
            this.txtTantoshaCd.BackColor = System.Drawing.Color.White;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCd.Location = new System.Drawing.Point(109, 213);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.Size = new System.Drawing.Size(40, 20);
            this.txtTantoshaCd.TabIndex = 19;
            this.txtTantoshaCd.Text = "0";
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
            // 
            // txtFax
            // 
            this.txtFax.AutoSizeFromLength = false;
            this.txtFax.BackColor = System.Drawing.Color.White;
            this.txtFax.DisplayLength = null;
            this.txtFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFax.Location = new System.Drawing.Point(109, 188);
            this.txtFax.MaxLength = 15;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(166, 20);
            this.txtFax.TabIndex = 17;
            this.txtFax.Validating += new System.ComponentModel.CancelEventHandler(this.txtFax_Validating);
            // 
            // txtTel
            // 
            this.txtTel.AutoSizeFromLength = false;
            this.txtTel.BackColor = System.Drawing.Color.White;
            this.txtTel.DisplayLength = null;
            this.txtTel.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTel.Location = new System.Drawing.Point(109, 163);
            this.txtTel.MaxLength = 15;
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(166, 20);
            this.txtTel.TabIndex = 15;
            this.txtTel.Validating += new System.ComponentModel.CancelEventHandler(this.txtTel_Validating);
            // 
            // txtJusho2
            // 
            this.txtJusho2.AutoSizeFromLength = false;
            this.txtJusho2.BackColor = System.Drawing.Color.White;
            this.txtJusho2.DisplayLength = null;
            this.txtJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho2.Location = new System.Drawing.Point(109, 138);
            this.txtJusho2.MaxLength = 30;
            this.txtJusho2.Name = "txtJusho2";
            this.txtJusho2.Size = new System.Drawing.Size(240, 20);
            this.txtJusho2.TabIndex = 13;
            this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho2_Validating);
            // 
            // txtJusho1
            // 
            this.txtJusho1.AutoSizeFromLength = false;
            this.txtJusho1.BackColor = System.Drawing.Color.White;
            this.txtJusho1.DisplayLength = null;
            this.txtJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho1.Location = new System.Drawing.Point(109, 113);
            this.txtJusho1.MaxLength = 30;
            this.txtJusho1.Name = "txtJusho1";
            this.txtJusho1.Size = new System.Drawing.Size(240, 20);
            this.txtJusho1.TabIndex = 11;
            this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho1_Validating);
            // 
            // txtYubinBango1
            // 
            this.txtYubinBango1.AutoSizeFromLength = false;
            this.txtYubinBango1.BackColor = System.Drawing.Color.White;
            this.txtYubinBango1.DisplayLength = null;
            this.txtYubinBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango1.Location = new System.Drawing.Point(109, 88);
            this.txtYubinBango1.MaxLength = 3;
            this.txtYubinBango1.Name = "txtYubinBango1";
            this.txtYubinBango1.Size = new System.Drawing.Size(39, 20);
            this.txtYubinBango1.TabIndex = 7;
            this.txtYubinBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango1_Validating);
            // 
            // txtRyFunanushiNm
            // 
            this.txtRyFunanushiNm.AutoSizeFromLength = false;
            this.txtRyFunanushiNm.BackColor = System.Drawing.Color.White;
            this.txtRyFunanushiNm.DisplayLength = null;
            this.txtRyFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRyFunanushiNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtRyFunanushiNm.Location = new System.Drawing.Point(109, 63);
            this.txtRyFunanushiNm.MaxLength = 20;
            this.txtRyFunanushiNm.Name = "txtRyFunanushiNm";
            this.txtRyFunanushiNm.Size = new System.Drawing.Size(240, 20);
            this.txtRyFunanushiNm.TabIndex = 5;
            this.txtRyFunanushiNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtRyFunanushiNm_Validating);
            // 
            // txtFunanushiKanaNm
            // 
            this.txtFunanushiKanaNm.AutoSizeFromLength = false;
            this.txtFunanushiKanaNm.BackColor = System.Drawing.Color.White;
            this.txtFunanushiKanaNm.DisplayLength = null;
            this.txtFunanushiKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFunanushiKanaNm.Location = new System.Drawing.Point(109, 38);
            this.txtFunanushiKanaNm.MaxLength = 30;
            this.txtFunanushiKanaNm.Name = "txtFunanushiKanaNm";
            this.txtFunanushiKanaNm.Size = new System.Drawing.Size(240, 20);
            this.txtFunanushiKanaNm.TabIndex = 3;
            this.txtFunanushiKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiKanaNm_Validating);
            // 
            // txtFunanushiNm
            // 
            this.txtFunanushiNm.AutoSizeFromLength = false;
            this.txtFunanushiNm.BackColor = System.Drawing.Color.White;
            this.txtFunanushiNm.DisplayLength = null;
            this.txtFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtFunanushiNm.Location = new System.Drawing.Point(109, 13);
            this.txtFunanushiNm.MaxLength = 40;
            this.txtFunanushiNm.Name = "txtFunanushiNm";
            this.txtFunanushiNm.Size = new System.Drawing.Size(240, 20);
            this.txtFunanushiNm.TabIndex = 1;
            this.txtFunanushiNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiNm_Validating);
            // 
            // lblDattaiRiyu
            // 
            this.lblDattaiRiyu.BackColor = System.Drawing.Color.Silver;
            this.lblDattaiRiyu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDattaiRiyu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDattaiRiyu.Location = new System.Drawing.Point(359, 261);
            this.lblDattaiRiyu.Name = "lblDattaiRiyu";
            this.lblDattaiRiyu.Size = new System.Drawing.Size(435, 25);
            this.lblDattaiRiyu.TabIndex = 70;
            this.lblDattaiRiyu.Text = "脱退理由";
            this.lblDattaiRiyu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDattaiJp
            // 
            this.lblDattaiJp.BackColor = System.Drawing.Color.Silver;
            this.lblDattaiJp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDattaiJp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDattaiJp.Location = new System.Drawing.Point(359, 236);
            this.lblDattaiJp.Name = "lblDattaiJp";
            this.lblDattaiJp.Size = new System.Drawing.Size(341, 25);
            this.lblDattaiJp.TabIndex = 64;
            this.lblDattaiJp.Text = "脱退日";
            this.lblDattaiJp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanyuJp
            // 
            this.lblKanyuJp.BackColor = System.Drawing.Color.Silver;
            this.lblKanyuJp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanyuJp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanyuJp.Location = new System.Drawing.Point(359, 211);
            this.lblKanyuJp.Name = "lblKanyuJp";
            this.lblKanyuJp.Size = new System.Drawing.Size(341, 25);
            this.lblKanyuJp.TabIndex = 58;
            this.lblKanyuJp.Text = "加入日";
            this.lblKanyuJp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJp
            // 
            this.lblJp.BackColor = System.Drawing.Color.Silver;
            this.lblJp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJp.Location = new System.Drawing.Point(359, 186);
            this.lblJp.Name = "lblJp";
            this.lblJp.Size = new System.Drawing.Size(341, 25);
            this.lblJp.TabIndex = 52;
            this.lblJp.Text = "生年月日";
            this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKaishuTsuki
            // 
            this.lblKaishuTsuki.BackColor = System.Drawing.Color.Silver;
            this.lblKaishuTsuki.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKaishuTsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaishuTsuki.Location = new System.Drawing.Point(359, 161);
            this.lblKaishuTsuki.Name = "lblKaishuTsuki";
            this.lblKaishuTsuki.Size = new System.Drawing.Size(129, 25);
            this.lblKaishuTsuki.TabIndex = 48;
            this.lblKaishuTsuki.Text = "回収日";
            this.lblKaishuTsuki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiTenkaHoho
            // 
            this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiTenkaHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(359, 136);
            this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
            this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(129, 25);
            this.lblShohizeiTenkaHoho.TabIndex = 45;
            this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
            this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiHasuShori
            // 
            this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiHasuShori.Location = new System.Drawing.Point(359, 111);
            this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
            this.lblShohizeiHasuShori.Size = new System.Drawing.Size(129, 25);
            this.lblShohizeiHasuShori.TabIndex = 42;
            this.lblShohizeiHasuShori.Text = "消費税端数処理";
            this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyushoKeishiki
            // 
            this.lblSeikyushoKeishiki.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoKeishiki.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeikyushoKeishiki.Location = new System.Drawing.Point(359, 86);
            this.lblSeikyushoKeishiki.Name = "lblSeikyushoKeishiki";
            this.lblSeikyushoKeishiki.Size = new System.Drawing.Size(129, 25);
            this.lblSeikyushoKeishiki.TabIndex = 39;
            this.lblSeikyushoKeishiki.Text = "請求書形式";
            this.lblSeikyushoKeishiki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyushoHakko
            // 
            this.lblSeikyushoHakko.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoHakko.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeikyushoHakko.Location = new System.Drawing.Point(359, 61);
            this.lblSeikyushoHakko.Name = "lblSeikyushoHakko";
            this.lblSeikyushoHakko.Size = new System.Drawing.Size(129, 25);
            this.lblSeikyushoHakko.TabIndex = 36;
            this.lblSeikyushoHakko.Text = "請求書発行";
            this.lblSeikyushoHakko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShimebi
            // 
            this.lblShimebi.BackColor = System.Drawing.Color.Silver;
            this.lblShimebi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShimebi.Location = new System.Drawing.Point(359, 36);
            this.lblShimebi.Name = "lblShimebi";
            this.lblShimebi.Size = new System.Drawing.Size(129, 25);
            this.lblShimebi.TabIndex = 33;
            this.lblShimebi.Text = "締日";
            this.lblShimebi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyusakiCd
            // 
            this.lblSeikyusakiCd.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyusakiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeikyusakiCd.Location = new System.Drawing.Point(359, 11);
            this.lblSeikyusakiCd.Name = "lblSeikyusakiCd";
            this.lblSeikyusakiCd.Size = new System.Drawing.Size(147, 25);
            this.lblSeikyusakiCd.TabIndex = 30;
            this.lblSeikyusakiCd.Text = "請求先コード";
            this.lblSeikyusakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiNyuryokuHoho
            // 
            this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(6, 286);
            this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
            this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(121, 25);
            this.lblShohizeiNyuryokuHoho.TabIndex = 27;
            this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
            this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKingakuHasuShori
            // 
            this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblKingakuHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKingakuHasuShori.Location = new System.Drawing.Point(6, 261);
            this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
            this.lblKingakuHasuShori.Size = new System.Drawing.Size(121, 25);
            this.lblKingakuHasuShori.TabIndex = 24;
            this.lblKingakuHasuShori.Text = "金額端数処理";
            this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTankaShutokuHoho
            // 
            this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblTankaShutokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTankaShutokuHoho.Location = new System.Drawing.Point(6, 236);
            this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
            this.lblTankaShutokuHoho.Size = new System.Drawing.Size(121, 25);
            this.lblTankaShutokuHoho.TabIndex = 21;
            this.lblTankaShutokuHoho.Text = "単価取得方法";
            this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoshaCd
            // 
            this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaCd.Location = new System.Drawing.Point(6, 211);
            this.lblTantoshaCd.Name = "lblTantoshaCd";
            this.lblTantoshaCd.Size = new System.Drawing.Size(146, 25);
            this.lblTantoshaCd.TabIndex = 18;
            this.lblTantoshaCd.Text = "担当者コード";
            this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFax
            // 
            this.lblFax.BackColor = System.Drawing.Color.Silver;
            this.lblFax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFax.Location = new System.Drawing.Point(6, 186);
            this.lblFax.Name = "lblFax";
            this.lblFax.Size = new System.Drawing.Size(272, 25);
            this.lblFax.TabIndex = 16;
            this.lblFax.Text = "ＦＡＸ番号";
            this.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTel
            // 
            this.lblTel.BackColor = System.Drawing.Color.Silver;
            this.lblTel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTel.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTel.Location = new System.Drawing.Point(6, 161);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(272, 25);
            this.lblTel.TabIndex = 14;
            this.lblTel.Text = "電話番号";
            this.lblTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho2
            // 
            this.lblJusho2.BackColor = System.Drawing.Color.Silver;
            this.lblJusho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho2.Location = new System.Drawing.Point(6, 136);
            this.lblJusho2.Name = "lblJusho2";
            this.lblJusho2.Size = new System.Drawing.Size(346, 25);
            this.lblJusho2.TabIndex = 12;
            this.lblJusho2.Text = "住所２";
            this.lblJusho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho1
            // 
            this.lblJusho1.BackColor = System.Drawing.Color.Silver;
            this.lblJusho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho1.Location = new System.Drawing.Point(6, 111);
            this.lblJusho1.Name = "lblJusho1";
            this.lblJusho1.Size = new System.Drawing.Size(346, 25);
            this.lblJusho1.TabIndex = 10;
            this.lblJusho1.Text = "住所１";
            this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYubinBango
            // 
            this.lblYubinBango.BackColor = System.Drawing.Color.Silver;
            this.lblYubinBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYubinBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYubinBango.Location = new System.Drawing.Point(6, 86);
            this.lblYubinBango.Name = "lblYubinBango";
            this.lblYubinBango.Size = new System.Drawing.Size(215, 25);
            this.lblYubinBango.TabIndex = 6;
            this.lblYubinBango.Text = "郵便番号";
            this.lblYubinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRyFunanushiNm
            // 
            this.lblRyFunanushiNm.BackColor = System.Drawing.Color.Silver;
            this.lblRyFunanushiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRyFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblRyFunanushiNm.Location = new System.Drawing.Point(6, 61);
            this.lblRyFunanushiNm.Name = "lblRyFunanushiNm";
            this.lblRyFunanushiNm.Size = new System.Drawing.Size(346, 25);
            this.lblRyFunanushiNm.TabIndex = 4;
            this.lblRyFunanushiNm.Text = "略称船主名";
            this.lblRyFunanushiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiKanaNm
            // 
            this.lblFunanushiKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiKanaNm.Location = new System.Drawing.Point(6, 36);
            this.lblFunanushiKanaNm.Name = "lblFunanushiKanaNm";
            this.lblFunanushiKanaNm.Size = new System.Drawing.Size(346, 25);
            this.lblFunanushiKanaNm.TabIndex = 2;
            this.lblFunanushiKanaNm.Text = "船主カナ名";
            this.lblFunanushiKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiNm
            // 
            this.lblFunanushiNm.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiNm.Location = new System.Drawing.Point(6, 11);
            this.lblFunanushiNm.Name = "lblFunanushiNm";
            this.lblFunanushiNm.Size = new System.Drawing.Size(346, 25);
            this.lblFunanushiNm.TabIndex = 0;
            this.lblFunanushiNm.Text = "正式船主名";
            this.lblFunanushiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbcKoza
            // 
            this.tbcKoza.Controls.Add(this.futsu);
            this.tbcKoza.Controls.Add(this.tsumitate);
            this.tbcKoza.Controls.Add(this.azukari);
            this.tbcKoza.Controls.Add(this.honnin);
            this.tbcKoza.ItemSize = new System.Drawing.Size(100, 40);
            this.tbcKoza.Location = new System.Drawing.Point(416, 40);
            this.tbcKoza.Margin = new System.Windows.Forms.Padding(0);
            this.tbcKoza.Name = "tbcKoza";
            this.tbcKoza.Padding = new System.Drawing.Point(10, 0);
            this.tbcKoza.SelectedIndex = 0;
            this.tbcKoza.Size = new System.Drawing.Size(324, 170);
            this.tbcKoza.TabIndex = 27;
            // 
            // futsu
            // 
            this.futsu.Controls.Add(this.gbxfutsu);
            this.futsu.Location = new System.Drawing.Point(4, 44);
            this.futsu.Name = "futsu";
            this.futsu.Padding = new System.Windows.Forms.Padding(3);
            this.futsu.Size = new System.Drawing.Size(316, 122);
            this.futsu.TabIndex = 0;
            this.futsu.Text = "普通口座";
            this.futsu.UseVisualStyleBackColor = true;
            // 
            // gbxfutsu
            // 
            this.gbxfutsu.Controls.Add(this.txtFutsuKozaKubun);
            this.gbxfutsu.Controls.Add(this.txtFutsuKozaBango);
            this.gbxfutsu.Controls.Add(this.lblFutsuKozaBango);
            this.gbxfutsu.Controls.Add(this.lblFutsuKozaKubunSelect);
            this.gbxfutsu.Controls.Add(this.lblFutsuKozaKubun);
            this.gbxfutsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxfutsu.ForeColor = System.Drawing.Color.Black;
            this.gbxfutsu.Location = new System.Drawing.Point(6, 6);
            this.gbxfutsu.Name = "gbxfutsu";
            this.gbxfutsu.Size = new System.Drawing.Size(250, 100);
            this.gbxfutsu.TabIndex = 0;
            this.gbxfutsu.TabStop = false;
            // 
            // txtFutsuKozaKubun
            // 
            this.txtFutsuKozaKubun.AutoSizeFromLength = false;
            this.txtFutsuKozaKubun.BackColor = System.Drawing.Color.White;
            this.txtFutsuKozaKubun.DisplayLength = null;
            this.txtFutsuKozaKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFutsuKozaKubun.Location = new System.Drawing.Point(91, 43);
            this.txtFutsuKozaKubun.MaxLength = 1;
            this.txtFutsuKozaKubun.Name = "txtFutsuKozaKubun";
            this.txtFutsuKozaKubun.Size = new System.Drawing.Size(15, 20);
            this.txtFutsuKozaKubun.TabIndex = 3;
            this.txtFutsuKozaKubun.Text = "0";
            this.txtFutsuKozaKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFutsuKozaKubun.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFutsuKozaKubun_KeyDown);
            this.txtFutsuKozaKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtFutsuKozaKubun_Validating);
            // 
            // txtFutsuKozaBango
            // 
            this.txtFutsuKozaBango.AutoSizeFromLength = false;
            this.txtFutsuKozaBango.BackColor = System.Drawing.Color.White;
            this.txtFutsuKozaBango.DisplayLength = null;
            this.txtFutsuKozaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFutsuKozaBango.Location = new System.Drawing.Point(91, 15);
            this.txtFutsuKozaBango.MaxLength = 7;
            this.txtFutsuKozaBango.Name = "txtFutsuKozaBango";
            this.txtFutsuKozaBango.Size = new System.Drawing.Size(55, 20);
            this.txtFutsuKozaBango.TabIndex = 1;
            this.txtFutsuKozaBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtFutsuKozaBango_Validating);
            // 
            // lblFutsuKozaBango
            // 
            this.lblFutsuKozaBango.BackColor = System.Drawing.Color.Silver;
            this.lblFutsuKozaBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFutsuKozaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFutsuKozaBango.Location = new System.Drawing.Point(6, 13);
            this.lblFutsuKozaBango.Name = "lblFutsuKozaBango";
            this.lblFutsuKozaBango.Size = new System.Drawing.Size(143, 25);
            this.lblFutsuKozaBango.TabIndex = 0;
            this.lblFutsuKozaBango.Text = "口 座 番 号";
            this.lblFutsuKozaBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFutsuKozaKubunSelect
            // 
            this.lblFutsuKozaKubunSelect.BackColor = System.Drawing.Color.Silver;
            this.lblFutsuKozaKubunSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFutsuKozaKubunSelect.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFutsuKozaKubunSelect.Location = new System.Drawing.Point(111, 41);
            this.lblFutsuKozaKubunSelect.Name = "lblFutsuKozaKubunSelect";
            this.lblFutsuKozaKubunSelect.Size = new System.Drawing.Size(101, 25);
            this.lblFutsuKozaKubunSelect.TabIndex = 4;
            this.lblFutsuKozaKubunSelect.Text = "1:普通 2:当座";
            this.lblFutsuKozaKubunSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFutsuKozaKubun
            // 
            this.lblFutsuKozaKubun.BackColor = System.Drawing.Color.Silver;
            this.lblFutsuKozaKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFutsuKozaKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFutsuKozaKubun.Location = new System.Drawing.Point(6, 41);
            this.lblFutsuKozaKubun.Name = "lblFutsuKozaKubun";
            this.lblFutsuKozaKubun.Size = new System.Drawing.Size(103, 25);
            this.lblFutsuKozaKubun.TabIndex = 2;
            this.lblFutsuKozaKubun.Text = "口 座 区 分";
            this.lblFutsuKozaKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsumitate
            // 
            this.tsumitate.Controls.Add(this.gbxTsumitate);
            this.tsumitate.Location = new System.Drawing.Point(4, 44);
            this.tsumitate.Name = "tsumitate";
            this.tsumitate.Padding = new System.Windows.Forms.Padding(3);
            this.tsumitate.Size = new System.Drawing.Size(316, 122);
            this.tsumitate.TabIndex = 1;
            this.tsumitate.Text = "積立口座";
            this.tsumitate.UseVisualStyleBackColor = true;
            // 
            // gbxTsumitate
            // 
            this.gbxTsumitate.Controls.Add(this.txtTsumitateKozaKubun);
            this.gbxTsumitate.Controls.Add(this.txtTsumitateRitsu);
            this.gbxTsumitate.Controls.Add(this.txtTsumitateKozaBango);
            this.gbxTsumitate.Controls.Add(this.lblTsumitateRitsu);
            this.gbxTsumitate.Controls.Add(this.lblTsumitateKozaBango);
            this.gbxTsumitate.Controls.Add(this.lblTsumitateKozaKubun);
            this.gbxTsumitate.Controls.Add(this.lblTsumitateKozaKubunMemo);
            this.gbxTsumitate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxTsumitate.ForeColor = System.Drawing.Color.Black;
            this.gbxTsumitate.Location = new System.Drawing.Point(6, 6);
            this.gbxTsumitate.Name = "gbxTsumitate";
            this.gbxTsumitate.Size = new System.Drawing.Size(250, 100);
            this.gbxTsumitate.TabIndex = 0;
            this.gbxTsumitate.TabStop = false;
            // 
            // txtTsumitateKozaKubun
            // 
            this.txtTsumitateKozaKubun.AutoSizeFromLength = false;
            this.txtTsumitateKozaKubun.BackColor = System.Drawing.Color.White;
            this.txtTsumitateKozaKubun.DisplayLength = null;
            this.txtTsumitateKozaKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTsumitateKozaKubun.Location = new System.Drawing.Point(91, 71);
            this.txtTsumitateKozaKubun.MaxLength = 1;
            this.txtTsumitateKozaKubun.Name = "txtTsumitateKozaKubun";
            this.txtTsumitateKozaKubun.Size = new System.Drawing.Size(15, 20);
            this.txtTsumitateKozaKubun.TabIndex = 5;
            this.txtTsumitateKozaKubun.Text = "0";
            this.txtTsumitateKozaKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTsumitateKozaKubun.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTsumitateKozaKubun_KeyDown);
            this.txtTsumitateKozaKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateKozaKubun_Validating);
            // 
            // txtTsumitateRitsu
            // 
            this.txtTsumitateRitsu.AutoSizeFromLength = false;
            this.txtTsumitateRitsu.BackColor = System.Drawing.Color.White;
            this.txtTsumitateRitsu.DisplayLength = null;
            this.txtTsumitateRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTsumitateRitsu.Location = new System.Drawing.Point(91, 15);
            this.txtTsumitateRitsu.MaxLength = 5;
            this.txtTsumitateRitsu.Name = "txtTsumitateRitsu";
            this.txtTsumitateRitsu.Size = new System.Drawing.Size(55, 20);
            this.txtTsumitateRitsu.TabIndex = 1;
            this.txtTsumitateRitsu.Text = "0.00";
            this.txtTsumitateRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTsumitateRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateRitsu_Validating);
            // 
            // txtTsumitateKozaBango
            // 
            this.txtTsumitateKozaBango.AutoSizeFromLength = false;
            this.txtTsumitateKozaBango.BackColor = System.Drawing.Color.White;
            this.txtTsumitateKozaBango.DisplayLength = null;
            this.txtTsumitateKozaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTsumitateKozaBango.Location = new System.Drawing.Point(91, 43);
            this.txtTsumitateKozaBango.MaxLength = 7;
            this.txtTsumitateKozaBango.Name = "txtTsumitateKozaBango";
            this.txtTsumitateKozaBango.Size = new System.Drawing.Size(55, 20);
            this.txtTsumitateKozaBango.TabIndex = 3;
            this.txtTsumitateKozaBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateKozaBango_Validating);
            // 
            // lblTsumitateRitsu
            // 
            this.lblTsumitateRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblTsumitateRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTsumitateRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTsumitateRitsu.Location = new System.Drawing.Point(6, 13);
            this.lblTsumitateRitsu.Name = "lblTsumitateRitsu";
            this.lblTsumitateRitsu.Size = new System.Drawing.Size(143, 25);
            this.lblTsumitateRitsu.TabIndex = 0;
            this.lblTsumitateRitsu.Text = "積　立　率";
            this.lblTsumitateRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTsumitateKozaBango
            // 
            this.lblTsumitateKozaBango.BackColor = System.Drawing.Color.Silver;
            this.lblTsumitateKozaBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTsumitateKozaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTsumitateKozaBango.Location = new System.Drawing.Point(6, 41);
            this.lblTsumitateKozaBango.Name = "lblTsumitateKozaBango";
            this.lblTsumitateKozaBango.Size = new System.Drawing.Size(143, 25);
            this.lblTsumitateKozaBango.TabIndex = 2;
            this.lblTsumitateKozaBango.Text = "口 座 番 号";
            this.lblTsumitateKozaBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTsumitateKozaKubun
            // 
            this.lblTsumitateKozaKubun.BackColor = System.Drawing.Color.Silver;
            this.lblTsumitateKozaKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTsumitateKozaKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTsumitateKozaKubun.Location = new System.Drawing.Point(6, 69);
            this.lblTsumitateKozaKubun.Name = "lblTsumitateKozaKubun";
            this.lblTsumitateKozaKubun.Size = new System.Drawing.Size(103, 25);
            this.lblTsumitateKozaKubun.TabIndex = 4;
            this.lblTsumitateKozaKubun.Text = "口 座 区 分";
            this.lblTsumitateKozaKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTsumitateKozaKubunMemo
            // 
            this.lblTsumitateKozaKubunMemo.BackColor = System.Drawing.Color.Silver;
            this.lblTsumitateKozaKubunMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTsumitateKozaKubunMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTsumitateKozaKubunMemo.Location = new System.Drawing.Point(111, 69);
            this.lblTsumitateKozaKubunMemo.Name = "lblTsumitateKozaKubunMemo";
            this.lblTsumitateKozaKubunMemo.Size = new System.Drawing.Size(100, 25);
            this.lblTsumitateKozaKubunMemo.TabIndex = 6;
            this.lblTsumitateKozaKubunMemo.Text = "1:普通 2:当座";
            this.lblTsumitateKozaKubunMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // azukari
            // 
            this.azukari.Controls.Add(this.gbxAzukari);
            this.azukari.Location = new System.Drawing.Point(4, 44);
            this.azukari.Name = "azukari";
            this.azukari.Size = new System.Drawing.Size(316, 122);
            this.azukari.TabIndex = 2;
            this.azukari.Text = "積立口座\r\n(預り金)";
            this.azukari.UseVisualStyleBackColor = true;
            // 
            // gbxAzukari
            // 
            this.gbxAzukari.Controls.Add(this.txtKozaKubunAzukari);
            this.gbxAzukari.Controls.Add(this.txtTsumitateRitsuAzukari);
            this.gbxAzukari.Controls.Add(this.txtKozaBangoAzukari);
            this.gbxAzukari.Controls.Add(this.lblTsumitateRitsuAzukari);
            this.gbxAzukari.Controls.Add(this.lblKozaBangoAzukari);
            this.gbxAzukari.Controls.Add(this.lblKozaKubunAzukari);
            this.gbxAzukari.Controls.Add(this.lblKozaKubunAzukariSelect);
            this.gbxAzukari.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxAzukari.ForeColor = System.Drawing.Color.Black;
            this.gbxAzukari.Location = new System.Drawing.Point(6, 6);
            this.gbxAzukari.Name = "gbxAzukari";
            this.gbxAzukari.Size = new System.Drawing.Size(250, 100);
            this.gbxAzukari.TabIndex = 0;
            this.gbxAzukari.TabStop = false;
            // 
            // txtKozaKubunAzukari
            // 
            this.txtKozaKubunAzukari.AutoSizeFromLength = false;
            this.txtKozaKubunAzukari.BackColor = System.Drawing.Color.White;
            this.txtKozaKubunAzukari.DisplayLength = null;
            this.txtKozaKubunAzukari.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKozaKubunAzukari.Location = new System.Drawing.Point(91, 71);
            this.txtKozaKubunAzukari.MaxLength = 1;
            this.txtKozaKubunAzukari.Name = "txtKozaKubunAzukari";
            this.txtKozaKubunAzukari.Size = new System.Drawing.Size(15, 20);
            this.txtKozaKubunAzukari.TabIndex = 5;
            this.txtKozaKubunAzukari.Text = "0";
            this.txtKozaKubunAzukari.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKozaKubunAzukari.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKozaKubunAzukari_KeyDown);
            this.txtKozaKubunAzukari.Validating += new System.ComponentModel.CancelEventHandler(this.txtKozaKubunAzukari_Validating);
            // 
            // txtTsumitateRitsuAzukari
            // 
            this.txtTsumitateRitsuAzukari.AutoSizeFromLength = false;
            this.txtTsumitateRitsuAzukari.BackColor = System.Drawing.Color.White;
            this.txtTsumitateRitsuAzukari.DisplayLength = null;
            this.txtTsumitateRitsuAzukari.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTsumitateRitsuAzukari.Location = new System.Drawing.Point(91, 15);
            this.txtTsumitateRitsuAzukari.MaxLength = 5;
            this.txtTsumitateRitsuAzukari.Name = "txtTsumitateRitsuAzukari";
            this.txtTsumitateRitsuAzukari.Size = new System.Drawing.Size(55, 20);
            this.txtTsumitateRitsuAzukari.TabIndex = 1;
            this.txtTsumitateRitsuAzukari.Text = "0.00";
            this.txtTsumitateRitsuAzukari.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTsumitateRitsuAzukari.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateRitsuAzukari_Validating);
            // 
            // txtKozaBangoAzukari
            // 
            this.txtKozaBangoAzukari.AutoSizeFromLength = false;
            this.txtKozaBangoAzukari.BackColor = System.Drawing.Color.White;
            this.txtKozaBangoAzukari.DisplayLength = null;
            this.txtKozaBangoAzukari.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKozaBangoAzukari.Location = new System.Drawing.Point(91, 43);
            this.txtKozaBangoAzukari.MaxLength = 7;
            this.txtKozaBangoAzukari.Name = "txtKozaBangoAzukari";
            this.txtKozaBangoAzukari.Size = new System.Drawing.Size(55, 20);
            this.txtKozaBangoAzukari.TabIndex = 3;
            this.txtKozaBangoAzukari.Validating += new System.ComponentModel.CancelEventHandler(this.txtKozaBangoAzukari_Validating);
            // 
            // lblTsumitateRitsuAzukari
            // 
            this.lblTsumitateRitsuAzukari.BackColor = System.Drawing.Color.Silver;
            this.lblTsumitateRitsuAzukari.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTsumitateRitsuAzukari.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTsumitateRitsuAzukari.Location = new System.Drawing.Point(6, 13);
            this.lblTsumitateRitsuAzukari.Name = "lblTsumitateRitsuAzukari";
            this.lblTsumitateRitsuAzukari.Size = new System.Drawing.Size(143, 25);
            this.lblTsumitateRitsuAzukari.TabIndex = 0;
            this.lblTsumitateRitsuAzukari.Text = "積　立　率";
            this.lblTsumitateRitsuAzukari.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKozaBangoAzukari
            // 
            this.lblKozaBangoAzukari.BackColor = System.Drawing.Color.Silver;
            this.lblKozaBangoAzukari.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKozaBangoAzukari.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKozaBangoAzukari.Location = new System.Drawing.Point(6, 41);
            this.lblKozaBangoAzukari.Name = "lblKozaBangoAzukari";
            this.lblKozaBangoAzukari.Size = new System.Drawing.Size(143, 25);
            this.lblKozaBangoAzukari.TabIndex = 2;
            this.lblKozaBangoAzukari.Text = "口 座 番 号";
            this.lblKozaBangoAzukari.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKozaKubunAzukari
            // 
            this.lblKozaKubunAzukari.BackColor = System.Drawing.Color.Silver;
            this.lblKozaKubunAzukari.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKozaKubunAzukari.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKozaKubunAzukari.Location = new System.Drawing.Point(6, 69);
            this.lblKozaKubunAzukari.Name = "lblKozaKubunAzukari";
            this.lblKozaKubunAzukari.Size = new System.Drawing.Size(103, 25);
            this.lblKozaKubunAzukari.TabIndex = 4;
            this.lblKozaKubunAzukari.Text = "口 座 区 分";
            this.lblKozaKubunAzukari.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKozaKubunAzukariSelect
            // 
            this.lblKozaKubunAzukariSelect.BackColor = System.Drawing.Color.Silver;
            this.lblKozaKubunAzukariSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKozaKubunAzukariSelect.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKozaKubunAzukariSelect.Location = new System.Drawing.Point(111, 69);
            this.lblKozaKubunAzukariSelect.Name = "lblKozaKubunAzukariSelect";
            this.lblKozaKubunAzukariSelect.Size = new System.Drawing.Size(100, 25);
            this.lblKozaKubunAzukariSelect.TabIndex = 6;
            this.lblKozaKubunAzukariSelect.Text = "1:普通 2:当座";
            this.lblKozaKubunAzukariSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // honnin
            // 
            this.honnin.BackColor = System.Drawing.Color.Transparent;
            this.honnin.Controls.Add(this.gbxHonnin);
            this.honnin.Location = new System.Drawing.Point(4, 44);
            this.honnin.Name = "honnin";
            this.honnin.Size = new System.Drawing.Size(316, 122);
            this.honnin.TabIndex = 3;
            this.honnin.Text = "積立口座\r\n (本人)";
            // 
            // gbxHonnin
            // 
            this.gbxHonnin.Controls.Add(this.txtKozaKubunHonnin);
            this.gbxHonnin.Controls.Add(this.txtTsumitateRitsuHonnin);
            this.gbxHonnin.Controls.Add(this.txtKozaBangoHonnin);
            this.gbxHonnin.Controls.Add(this.lblTsumitateRitsuHonnin);
            this.gbxHonnin.Controls.Add(this.lblKozaBangoHonnin);
            this.gbxHonnin.Controls.Add(this.lblKozaKubunHonnin);
            this.gbxHonnin.Controls.Add(this.lblKozaKubunHonninSelect);
            this.gbxHonnin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxHonnin.ForeColor = System.Drawing.Color.Black;
            this.gbxHonnin.Location = new System.Drawing.Point(6, 6);
            this.gbxHonnin.Name = "gbxHonnin";
            this.gbxHonnin.Size = new System.Drawing.Size(250, 100);
            this.gbxHonnin.TabIndex = 0;
            this.gbxHonnin.TabStop = false;
            // 
            // txtKozaKubunHonnin
            // 
            this.txtKozaKubunHonnin.AutoSizeFromLength = false;
            this.txtKozaKubunHonnin.BackColor = System.Drawing.Color.White;
            this.txtKozaKubunHonnin.DisplayLength = null;
            this.txtKozaKubunHonnin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKozaKubunHonnin.Location = new System.Drawing.Point(91, 71);
            this.txtKozaKubunHonnin.MaxLength = 1;
            this.txtKozaKubunHonnin.Name = "txtKozaKubunHonnin";
            this.txtKozaKubunHonnin.Size = new System.Drawing.Size(15, 20);
            this.txtKozaKubunHonnin.TabIndex = 5;
            this.txtKozaKubunHonnin.Text = "0";
            this.txtKozaKubunHonnin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKozaKubunHonnin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKozaKubunHonnin_KeyDown);
            this.txtKozaKubunHonnin.Validating += new System.ComponentModel.CancelEventHandler(this.txtKozaKubunHonnin_Validating);
            // 
            // txtTsumitateRitsuHonnin
            // 
            this.txtTsumitateRitsuHonnin.AutoSizeFromLength = false;
            this.txtTsumitateRitsuHonnin.BackColor = System.Drawing.Color.White;
            this.txtTsumitateRitsuHonnin.DisplayLength = null;
            this.txtTsumitateRitsuHonnin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTsumitateRitsuHonnin.Location = new System.Drawing.Point(91, 15);
            this.txtTsumitateRitsuHonnin.MaxLength = 5;
            this.txtTsumitateRitsuHonnin.Name = "txtTsumitateRitsuHonnin";
            this.txtTsumitateRitsuHonnin.Size = new System.Drawing.Size(55, 20);
            this.txtTsumitateRitsuHonnin.TabIndex = 1;
            this.txtTsumitateRitsuHonnin.Text = "0.00";
            this.txtTsumitateRitsuHonnin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTsumitateRitsuHonnin.Validating += new System.ComponentModel.CancelEventHandler(this.txtTsumitateRitsuHonnin_Validating);
            // 
            // txtKozaBangoHonnin
            // 
            this.txtKozaBangoHonnin.AutoSizeFromLength = false;
            this.txtKozaBangoHonnin.BackColor = System.Drawing.Color.White;
            this.txtKozaBangoHonnin.DisplayLength = null;
            this.txtKozaBangoHonnin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKozaBangoHonnin.Location = new System.Drawing.Point(91, 43);
            this.txtKozaBangoHonnin.MaxLength = 7;
            this.txtKozaBangoHonnin.Name = "txtKozaBangoHonnin";
            this.txtKozaBangoHonnin.Size = new System.Drawing.Size(55, 20);
            this.txtKozaBangoHonnin.TabIndex = 3;
            this.txtKozaBangoHonnin.Validating += new System.ComponentModel.CancelEventHandler(this.txtKozaBangoHonnin_Validating);
            // 
            // lblTsumitateRitsuHonnin
            // 
            this.lblTsumitateRitsuHonnin.BackColor = System.Drawing.Color.Silver;
            this.lblTsumitateRitsuHonnin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTsumitateRitsuHonnin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTsumitateRitsuHonnin.Location = new System.Drawing.Point(6, 13);
            this.lblTsumitateRitsuHonnin.Name = "lblTsumitateRitsuHonnin";
            this.lblTsumitateRitsuHonnin.Size = new System.Drawing.Size(143, 25);
            this.lblTsumitateRitsuHonnin.TabIndex = 0;
            this.lblTsumitateRitsuHonnin.Text = "積　立　率";
            this.lblTsumitateRitsuHonnin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKozaBangoHonnin
            // 
            this.lblKozaBangoHonnin.BackColor = System.Drawing.Color.Silver;
            this.lblKozaBangoHonnin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKozaBangoHonnin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKozaBangoHonnin.Location = new System.Drawing.Point(6, 41);
            this.lblKozaBangoHonnin.Name = "lblKozaBangoHonnin";
            this.lblKozaBangoHonnin.Size = new System.Drawing.Size(143, 25);
            this.lblKozaBangoHonnin.TabIndex = 2;
            this.lblKozaBangoHonnin.Text = "口 座 番 号";
            this.lblKozaBangoHonnin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKozaKubunHonnin
            // 
            this.lblKozaKubunHonnin.BackColor = System.Drawing.Color.Silver;
            this.lblKozaKubunHonnin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKozaKubunHonnin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKozaKubunHonnin.Location = new System.Drawing.Point(6, 69);
            this.lblKozaKubunHonnin.Name = "lblKozaKubunHonnin";
            this.lblKozaKubunHonnin.Size = new System.Drawing.Size(103, 25);
            this.lblKozaKubunHonnin.TabIndex = 4;
            this.lblKozaKubunHonnin.Text = "口 座 区 分";
            this.lblKozaKubunHonnin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKozaKubunHonninSelect
            // 
            this.lblKozaKubunHonninSelect.BackColor = System.Drawing.Color.Silver;
            this.lblKozaKubunHonninSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKozaKubunHonninSelect.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKozaKubunHonninSelect.Location = new System.Drawing.Point(111, 69);
            this.lblKozaKubunHonninSelect.Name = "lblKozaKubunHonninSelect";
            this.lblKozaKubunHonninSelect.Size = new System.Drawing.Size(100, 25);
            this.lblKozaKubunHonninSelect.TabIndex = 6;
            this.lblKozaKubunHonninSelect.Text = "1:普通 2:当座";
            this.lblKozaKubunHonninSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxPersonInfo
            // 
            this.gbxPersonInfo.Controls.Add(this.tbcKoza);
            this.gbxPersonInfo.Controls.Add(this.txtPayaoRitsu);
            this.gbxPersonInfo.Controls.Add(this.txtSonotaRitsu);
            this.gbxPersonInfo.Controls.Add(this.txtFutsuRitsu);
            this.gbxPersonInfo.Controls.Add(this.lblFurikomisakiWariai);
            this.gbxPersonInfo.Controls.Add(this.lblPayaoRitsu);
            this.gbxPersonInfo.Controls.Add(this.lblSonotaRitsu);
            this.gbxPersonInfo.Controls.Add(this.lblFutsuRitsu);
            this.gbxPersonInfo.Controls.Add(this.lblShishoKubunMemo);
            this.gbxPersonInfo.Controls.Add(this.lblSeijunKubunMemo);
            this.gbxPersonInfo.Controls.Add(this.txtSeijunKubun);
            this.gbxPersonInfo.Controls.Add(this.lblSeijunKubun);
            this.gbxPersonInfo.Controls.Add(this.txtShishoKubun);
            this.gbxPersonInfo.Controls.Add(this.lblShishoKubun);
            this.gbxPersonInfo.Controls.Add(this.lblShiharaiKubunMemo);
            this.gbxPersonInfo.Controls.Add(this.txtShiharaiKubun);
            this.gbxPersonInfo.Controls.Add(this.lblShiharaiKubun);
            this.gbxPersonInfo.Controls.Add(this.txtKumiaiTesuryoRitsu);
            this.gbxPersonInfo.Controls.Add(this.lblKumiaiTesuryoRitsu);
            this.gbxPersonInfo.Controls.Add(this.lblChikuCdSub);
            this.gbxPersonInfo.Controls.Add(this.lblGyohoCdSub);
            this.gbxPersonInfo.Controls.Add(this.txtChikuCd);
            this.gbxPersonInfo.Controls.Add(this.txtGyohoCd);
            this.gbxPersonInfo.Controls.Add(this.lblFuneNmCdSub);
            this.gbxPersonInfo.Controls.Add(this.txtFuneNmCd);
            this.gbxPersonInfo.Controls.Add(this.lblChikuCd);
            this.gbxPersonInfo.Controls.Add(this.lblGyohoCd);
            this.gbxPersonInfo.Controls.Add(this.lblFuneNmCd);
            this.gbxPersonInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxPersonInfo.ForeColor = System.Drawing.Color.Black;
            this.gbxPersonInfo.Location = new System.Drawing.Point(11, 359);
            this.gbxPersonInfo.Name = "gbxPersonInfo";
            this.gbxPersonInfo.Size = new System.Drawing.Size(799, 240);
            this.gbxPersonInfo.TabIndex = 6;
            this.gbxPersonInfo.TabStop = false;
            this.gbxPersonInfo.Text = "個人情報";
            // 
            // txtPayaoRitsu
            // 
            this.txtPayaoRitsu.AutoSizeFromLength = false;
            this.txtPayaoRitsu.BackColor = System.Drawing.Color.White;
            this.txtPayaoRitsu.DisplayLength = null;
            this.txtPayaoRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPayaoRitsu.Location = new System.Drawing.Point(239, 215);
            this.txtPayaoRitsu.MaxLength = 3;
            this.txtPayaoRitsu.Name = "txtPayaoRitsu";
            this.txtPayaoRitsu.Size = new System.Drawing.Size(55, 20);
            this.txtPayaoRitsu.TabIndex = 26;
            this.txtPayaoRitsu.Text = "0";
            this.txtPayaoRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPayaoRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtPayaoRitsu_Validating);
            // 
            // txtSonotaRitsu
            // 
            this.txtSonotaRitsu.AutoSizeFromLength = false;
            this.txtSonotaRitsu.BackColor = System.Drawing.Color.White;
            this.txtSonotaRitsu.DisplayLength = null;
            this.txtSonotaRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonotaRitsu.Location = new System.Drawing.Point(181, 215);
            this.txtSonotaRitsu.MaxLength = 3;
            this.txtSonotaRitsu.Name = "txtSonotaRitsu";
            this.txtSonotaRitsu.Size = new System.Drawing.Size(55, 20);
            this.txtSonotaRitsu.TabIndex = 24;
            this.txtSonotaRitsu.Text = "0";
            this.txtSonotaRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSonotaRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtSonotaRitsu_Validating);
            // 
            // txtFutsuRitsu
            // 
            this.txtFutsuRitsu.AutoSizeFromLength = false;
            this.txtFutsuRitsu.BackColor = System.Drawing.Color.White;
            this.txtFutsuRitsu.DisplayLength = null;
            this.txtFutsuRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFutsuRitsu.Location = new System.Drawing.Point(123, 215);
            this.txtFutsuRitsu.MaxLength = 3;
            this.txtFutsuRitsu.Name = "txtFutsuRitsu";
            this.txtFutsuRitsu.Size = new System.Drawing.Size(55, 20);
            this.txtFutsuRitsu.TabIndex = 22;
            this.txtFutsuRitsu.Text = "0";
            this.txtFutsuRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFutsuRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtFutsuRitsu_Validating);
            // 
            // lblFurikomisakiWariai
            // 
            this.lblFurikomisakiWariai.BackColor = System.Drawing.Color.Silver;
            this.lblFurikomisakiWariai.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFurikomisakiWariai.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFurikomisakiWariai.Location = new System.Drawing.Point(6, 213);
            this.lblFurikomisakiWariai.Name = "lblFurikomisakiWariai";
            this.lblFurikomisakiWariai.Size = new System.Drawing.Size(292, 25);
            this.lblFurikomisakiWariai.TabIndex = 20;
            this.lblFurikomisakiWariai.Text = "振込先割合";
            this.lblFurikomisakiWariai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPayaoRitsu
            // 
            this.lblPayaoRitsu.BackColor = System.Drawing.Color.White;
            this.lblPayaoRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPayaoRitsu.Location = new System.Drawing.Point(242, 191);
            this.lblPayaoRitsu.Name = "lblPayaoRitsu";
            this.lblPayaoRitsu.Size = new System.Drawing.Size(55, 25);
            this.lblPayaoRitsu.TabIndex = 25;
            this.lblPayaoRitsu.Text = "パヤオ";
            this.lblPayaoRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSonotaRitsu
            // 
            this.lblSonotaRitsu.BackColor = System.Drawing.Color.White;
            this.lblSonotaRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSonotaRitsu.Location = new System.Drawing.Point(183, 191);
            this.lblSonotaRitsu.Name = "lblSonotaRitsu";
            this.lblSonotaRitsu.Size = new System.Drawing.Size(55, 25);
            this.lblSonotaRitsu.TabIndex = 23;
            this.lblSonotaRitsu.Text = "その他";
            this.lblSonotaRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFutsuRitsu
            // 
            this.lblFutsuRitsu.BackColor = System.Drawing.Color.White;
            this.lblFutsuRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFutsuRitsu.Location = new System.Drawing.Point(127, 191);
            this.lblFutsuRitsu.Name = "lblFutsuRitsu";
            this.lblFutsuRitsu.Size = new System.Drawing.Size(55, 25);
            this.lblFutsuRitsu.TabIndex = 21;
            this.lblFutsuRitsu.Text = "普通";
            this.lblFutsuRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShishoKubunMemo
            // 
            this.lblShishoKubunMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShishoKubunMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShishoKubunMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoKubunMemo.Location = new System.Drawing.Point(111, 140);
            this.lblShishoKubunMemo.Name = "lblShishoKubunMemo";
            this.lblShishoKubunMemo.Size = new System.Drawing.Size(100, 25);
            this.lblShishoKubunMemo.TabIndex = 16;
            this.lblShishoKubunMemo.Text = "1:本社 2:支社";
            this.lblShishoKubunMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeijunKubunMemo
            // 
            this.lblSeijunKubunMemo.BackColor = System.Drawing.Color.Silver;
            this.lblSeijunKubunMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeijunKubunMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeijunKubunMemo.Location = new System.Drawing.Point(111, 165);
            this.lblSeijunKubunMemo.Name = "lblSeijunKubunMemo";
            this.lblSeijunKubunMemo.Size = new System.Drawing.Size(263, 25);
            this.lblSeijunKubunMemo.TabIndex = 19;
            this.lblSeijunKubunMemo.Text = "1:正組合員　2:準組合員　3:組合員以外";
            this.lblSeijunKubunMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeijunKubun
            // 
            this.txtSeijunKubun.AutoSizeFromLength = false;
            this.txtSeijunKubun.BackColor = System.Drawing.Color.White;
            this.txtSeijunKubun.DisplayLength = null;
            this.txtSeijunKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeijunKubun.Location = new System.Drawing.Point(92, 167);
            this.txtSeijunKubun.MaxLength = 1;
            this.txtSeijunKubun.Name = "txtSeijunKubun";
            this.txtSeijunKubun.Size = new System.Drawing.Size(15, 20);
            this.txtSeijunKubun.TabIndex = 18;
            this.txtSeijunKubun.Text = "0";
            this.txtSeijunKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSeijunKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeijunKubun_Validating);
            // 
            // lblSeijunKubun
            // 
            this.lblSeijunKubun.BackColor = System.Drawing.Color.Silver;
            this.lblSeijunKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeijunKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeijunKubun.Location = new System.Drawing.Point(6, 165);
            this.lblSeijunKubun.Name = "lblSeijunKubun";
            this.lblSeijunKubun.Size = new System.Drawing.Size(103, 25);
            this.lblSeijunKubun.TabIndex = 17;
            this.lblSeijunKubun.Text = "正準区分";
            this.lblSeijunKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShishoKubun
            // 
            this.txtShishoKubun.AutoSizeFromLength = false;
            this.txtShishoKubun.BackColor = System.Drawing.Color.White;
            this.txtShishoKubun.DisplayLength = null;
            this.txtShishoKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoKubun.Location = new System.Drawing.Point(92, 142);
            this.txtShishoKubun.MaxLength = 1;
            this.txtShishoKubun.Name = "txtShishoKubun";
            this.txtShishoKubun.Size = new System.Drawing.Size(15, 20);
            this.txtShishoKubun.TabIndex = 15;
            this.txtShishoKubun.Text = "0";
            this.txtShishoKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtShishoKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoKubun_Validating);
            // 
            // lblShishoKubun
            // 
            this.lblShishoKubun.BackColor = System.Drawing.Color.Silver;
            this.lblShishoKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShishoKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoKubun.Location = new System.Drawing.Point(6, 140);
            this.lblShishoKubun.Name = "lblShishoKubun";
            this.lblShishoKubun.Size = new System.Drawing.Size(103, 25);
            this.lblShishoKubun.TabIndex = 14;
            this.lblShishoKubun.Text = "支所区分";
            this.lblShishoKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiharaiKubunMemo
            // 
            this.lblShiharaiKubunMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiKubunMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiharaiKubunMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiharaiKubunMemo.Location = new System.Drawing.Point(111, 115);
            this.lblShiharaiKubunMemo.Name = "lblShiharaiKubunMemo";
            this.lblShiharaiKubunMemo.Size = new System.Drawing.Size(167, 25);
            this.lblShiharaiKubunMemo.TabIndex = 13;
            this.lblShiharaiKubunMemo.Text = "1:口座有り 2:口座無し";
            this.lblShiharaiKubunMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiharaiKubun
            // 
            this.txtShiharaiKubun.AutoSizeFromLength = false;
            this.txtShiharaiKubun.BackColor = System.Drawing.Color.White;
            this.txtShiharaiKubun.DisplayLength = null;
            this.txtShiharaiKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiharaiKubun.Location = new System.Drawing.Point(92, 117);
            this.txtShiharaiKubun.MaxLength = 1;
            this.txtShiharaiKubun.Name = "txtShiharaiKubun";
            this.txtShiharaiKubun.Size = new System.Drawing.Size(15, 20);
            this.txtShiharaiKubun.TabIndex = 12;
            this.txtShiharaiKubun.Text = "0";
            this.txtShiharaiKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtShiharaiKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiKubun_Validating);
            // 
            // lblShiharaiKubun
            // 
            this.lblShiharaiKubun.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiharaiKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiharaiKubun.Location = new System.Drawing.Point(6, 115);
            this.lblShiharaiKubun.Name = "lblShiharaiKubun";
            this.lblShiharaiKubun.Size = new System.Drawing.Size(103, 25);
            this.lblShiharaiKubun.TabIndex = 11;
            this.lblShiharaiKubun.Text = "支払区分";
            this.lblShiharaiKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKumiaiTesuryoRitsu
            // 
            this.txtKumiaiTesuryoRitsu.AutoSizeFromLength = false;
            this.txtKumiaiTesuryoRitsu.BackColor = System.Drawing.Color.White;
            this.txtKumiaiTesuryoRitsu.DisplayLength = null;
            this.txtKumiaiTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKumiaiTesuryoRitsu.Location = new System.Drawing.Point(109, 92);
            this.txtKumiaiTesuryoRitsu.MaxLength = 5;
            this.txtKumiaiTesuryoRitsu.Name = "txtKumiaiTesuryoRitsu";
            this.txtKumiaiTesuryoRitsu.Size = new System.Drawing.Size(56, 20);
            this.txtKumiaiTesuryoRitsu.TabIndex = 10;
            this.txtKumiaiTesuryoRitsu.Text = "0.00";
            this.txtKumiaiTesuryoRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKumiaiTesuryoRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtKumiaiTesuryoRitsu_Validating);
            // 
            // lblKumiaiTesuryoRitsu
            // 
            this.lblKumiaiTesuryoRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblKumiaiTesuryoRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKumiaiTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKumiaiTesuryoRitsu.Location = new System.Drawing.Point(6, 90);
            this.lblKumiaiTesuryoRitsu.Name = "lblKumiaiTesuryoRitsu";
            this.lblKumiaiTesuryoRitsu.Size = new System.Drawing.Size(163, 25);
            this.lblKumiaiTesuryoRitsu.TabIndex = 9;
            this.lblKumiaiTesuryoRitsu.Text = "組合手数料率";
            this.lblKumiaiTesuryoRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblChikuCdSub
            // 
            this.lblChikuCdSub.BackColor = System.Drawing.Color.Silver;
            this.lblChikuCdSub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChikuCdSub.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChikuCdSub.Location = new System.Drawing.Point(170, 65);
            this.lblChikuCdSub.Name = "lblChikuCdSub";
            this.lblChikuCdSub.Size = new System.Drawing.Size(186, 25);
            this.lblChikuCdSub.TabIndex = 8;
            this.lblChikuCdSub.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyohoCdSub
            // 
            this.lblGyohoCdSub.BackColor = System.Drawing.Color.Silver;
            this.lblGyohoCdSub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyohoCdSub.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyohoCdSub.Location = new System.Drawing.Point(170, 40);
            this.lblGyohoCdSub.Name = "lblGyohoCdSub";
            this.lblGyohoCdSub.Size = new System.Drawing.Size(186, 25);
            this.lblGyohoCdSub.TabIndex = 5;
            this.lblGyohoCdSub.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChikuCd
            // 
            this.txtChikuCd.AutoSizeFromLength = false;
            this.txtChikuCd.BackColor = System.Drawing.Color.White;
            this.txtChikuCd.DisplayLength = null;
            this.txtChikuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChikuCd.Location = new System.Drawing.Point(109, 67);
            this.txtChikuCd.MaxLength = 5;
            this.txtChikuCd.Name = "txtChikuCd";
            this.txtChikuCd.Size = new System.Drawing.Size(56, 20);
            this.txtChikuCd.TabIndex = 7;
            this.txtChikuCd.Text = "0";
            this.txtChikuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChikuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCd_Validating);
            // 
            // txtGyohoCd
            // 
            this.txtGyohoCd.AutoSizeFromLength = false;
            this.txtGyohoCd.BackColor = System.Drawing.Color.White;
            this.txtGyohoCd.DisplayLength = null;
            this.txtGyohoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyohoCd.Location = new System.Drawing.Point(109, 42);
            this.txtGyohoCd.MaxLength = 5;
            this.txtGyohoCd.Name = "txtGyohoCd";
            this.txtGyohoCd.Size = new System.Drawing.Size(56, 20);
            this.txtGyohoCd.TabIndex = 4;
            this.txtGyohoCd.Text = "0";
            this.txtGyohoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyohoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoCd_Validating);
            // 
            // lblFuneNmCdSub
            // 
            this.lblFuneNmCdSub.BackColor = System.Drawing.Color.Silver;
            this.lblFuneNmCdSub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuneNmCdSub.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuneNmCdSub.Location = new System.Drawing.Point(170, 15);
            this.lblFuneNmCdSub.Name = "lblFuneNmCdSub";
            this.lblFuneNmCdSub.Size = new System.Drawing.Size(186, 25);
            this.lblFuneNmCdSub.TabIndex = 2;
            this.lblFuneNmCdSub.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuneNmCd
            // 
            this.txtFuneNmCd.AutoSizeFromLength = false;
            this.txtFuneNmCd.BackColor = System.Drawing.Color.White;
            this.txtFuneNmCd.DisplayLength = null;
            this.txtFuneNmCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuneNmCd.Location = new System.Drawing.Point(109, 17);
            this.txtFuneNmCd.MaxLength = 5;
            this.txtFuneNmCd.Name = "txtFuneNmCd";
            this.txtFuneNmCd.Size = new System.Drawing.Size(56, 20);
            this.txtFuneNmCd.TabIndex = 1;
            this.txtFuneNmCd.Text = "0";
            this.txtFuneNmCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuneNmCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuneNmCd_Validating);
            // 
            // lblChikuCd
            // 
            this.lblChikuCd.BackColor = System.Drawing.Color.Silver;
            this.lblChikuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChikuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChikuCd.Location = new System.Drawing.Point(6, 65);
            this.lblChikuCd.Name = "lblChikuCd";
            this.lblChikuCd.Size = new System.Drawing.Size(162, 25);
            this.lblChikuCd.TabIndex = 6;
            this.lblChikuCd.Text = "地 区 C D";
            this.lblChikuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyohoCd
            // 
            this.lblGyohoCd.BackColor = System.Drawing.Color.Silver;
            this.lblGyohoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyohoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyohoCd.Location = new System.Drawing.Point(6, 40);
            this.lblGyohoCd.Name = "lblGyohoCd";
            this.lblGyohoCd.Size = new System.Drawing.Size(162, 25);
            this.lblGyohoCd.TabIndex = 3;
            this.lblGyohoCd.Text = "漁 法 C D";
            this.lblGyohoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFuneNmCd
            // 
            this.lblFuneNmCd.BackColor = System.Drawing.Color.Silver;
            this.lblFuneNmCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuneNmCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuneNmCd.Location = new System.Drawing.Point(6, 15);
            this.lblFuneNmCd.Name = "lblFuneNmCd";
            this.lblFuneNmCd.Size = new System.Drawing.Size(162, 25);
            this.lblFuneNmCd.TabIndex = 0;
            this.lblFuneNmCd.Text = "船 名 C D";
            this.lblFuneNmCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CMCM2012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 660);
            this.Controls.Add(this.txtFunanushiCd);
            this.Controls.Add(this.lblFunanushiCd);
            this.Controls.Add(this.gbxPersonInfo);
            this.Controls.Add(this.gbxSenshuInfo);
            this.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "CMCM2012";
            this.Text = "船主マスタ登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxSenshuInfo, 0);
            this.Controls.SetChildIndex(this.gbxPersonInfo, 0);
            this.Controls.SetChildIndex(this.lblFunanushiCd, 0);
            this.Controls.SetChildIndex(this.txtFunanushiCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSenshuInfo.ResumeLayout(false);
            this.gbxSenshuInfo.PerformLayout();
            this.tbcKoza.ResumeLayout(false);
            this.futsu.ResumeLayout(false);
            this.gbxfutsu.ResumeLayout(false);
            this.gbxfutsu.PerformLayout();
            this.tsumitate.ResumeLayout(false);
            this.gbxTsumitate.ResumeLayout(false);
            this.gbxTsumitate.PerformLayout();
            this.azukari.ResumeLayout(false);
            this.gbxAzukari.ResumeLayout(false);
            this.gbxAzukari.PerformLayout();
            this.honnin.ResumeLayout(false);
            this.gbxHonnin.ResumeLayout(false);
            this.gbxHonnin.PerformLayout();
            this.gbxPersonInfo.ResumeLayout(false);
            this.gbxPersonInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFunanushiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCd;
        private System.Windows.Forms.GroupBox gbxSenshuInfo;
        private System.Windows.Forms.Label lblFax;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.Label lblJusho2;
        private System.Windows.Forms.Label lblJusho1;
        private System.Windows.Forms.Label lblYubinBango;
        private System.Windows.Forms.Label lblRyFunanushiNm;
        private System.Windows.Forms.Label lblFunanushiKanaNm;
        private System.Windows.Forms.Label lblFunanushiNm;
        private System.Windows.Forms.Label lblDattaiRiyu;
        private System.Windows.Forms.Label lblDattaiJp;
        private System.Windows.Forms.Label lblKanyuJp;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblKaishuTsuki;
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblSeikyushoKeishiki;
        private System.Windows.Forms.Label lblSeikyushoHakko;
        private System.Windows.Forms.Label lblShimebi;
        private System.Windows.Forms.Label lblSeikyusakiCd;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private System.Windows.Forms.Label lblTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtDattaiRiyu;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtKingakuHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtTankaShutokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFax;
        private jp.co.fsi.common.controls.FsiTextBox txtTel;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho2;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho1;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango1;
        private jp.co.fsi.common.controls.FsiTextBox txtRyFunanushiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiKanaNm;
        private System.Windows.Forms.Label lblAddBar;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango2;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuTsuki;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiTenkaHoho;
        private jp.co.fsi.common.controls.FsiTextBox textBox14;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoKeishiki;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoHakko;
        private jp.co.fsi.common.controls.FsiTextBox txtShimebi;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyusakiCd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
        private System.Windows.Forms.Label lblShzHsSrMemo;
        private System.Windows.Forms.Label lblSkshKskMemo;
        private System.Windows.Forms.Label lblSkshHkMemo;
        private System.Windows.Forms.Label lblShimebiMemo;
        private System.Windows.Forms.Label lblSeikyusakiNm;
        private System.Windows.Forms.GroupBox gbxPersonInfo;
        private System.Windows.Forms.Label lblFutsuKozaKubun;
        private System.Windows.Forms.Label lblFutsuKozaBango;
        private System.Windows.Forms.Label lblChikuCd;
        private System.Windows.Forms.Label lblGyohoCd;
        private System.Windows.Forms.Label lblFuneNmCd;
        private System.Windows.Forms.Label lblDattaiGengo;
        private System.Windows.Forms.Label lblKanyuGengo;
        private System.Windows.Forms.Label lblJpGengo;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuBi;
        private System.Windows.Forms.Label label2;
        private jp.co.fsi.common.controls.FsiTextBox txtJpDay;
        private jp.co.fsi.common.controls.FsiTextBox txtJpMonth;
        private System.Windows.Forms.Label lblJpYMD;
        private jp.co.fsi.common.controls.FsiTextBox txtDattaiDay;
        private jp.co.fsi.common.controls.FsiTextBox txtKanyuDay;
        private jp.co.fsi.common.controls.FsiTextBox txtDattaiMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtKanyuMonth;
        private System.Windows.Forms.Label lblKanyuJpYMD;
        private System.Windows.Forms.Label lblDattaiJpYMD;
        private System.Windows.Forms.Label lblChikuCdSub;
        private System.Windows.Forms.Label lblGyohoCdSub;
        private jp.co.fsi.common.controls.FsiTextBox txtChikuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoCd;
        private System.Windows.Forms.Label lblFuneNmCdSub;
        private jp.co.fsi.common.controls.FsiTextBox txtFuneNmCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFutsuKozaKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtFutsuKozaBango;
        private System.Windows.Forms.Label lblFutsuKozaKubunSelect;
        private jp.co.fsi.common.controls.FsiTextBox txtKumiaiTesuryoRitsu;
        private System.Windows.Forms.Label lblKumiaiTesuryoRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtTsumitateKozaKubun;
        private System.Windows.Forms.Label lblTsumitateKozaKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtTsumitateKozaBango;
        private System.Windows.Forms.Label lblTsumitateKozaBango;
        private jp.co.fsi.common.controls.FsiTextBox txtTsumitateRitsu;
        private System.Windows.Forms.Label lblTsumitateRitsu;
        private System.Windows.Forms.Label lblShiharaiKubunMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtShiharaiKubun;
        private System.Windows.Forms.Label lblShiharaiKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtSeijunKubun;
        private System.Windows.Forms.Label lblSeijunKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtShishoKubun;
        private System.Windows.Forms.Label lblShishoKubun;
        private System.Windows.Forms.Label lblSeijunKubunMemo;
        private System.Windows.Forms.Label lblTsumitateKozaKubunMemo;
        private System.Windows.Forms.Label lblShishoKubunMemo;
        private System.Windows.Forms.Label lblPayaoRitsu;
        private System.Windows.Forms.Label lblSonotaRitsu;
        private System.Windows.Forms.Label lblFutsuRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtSonotaRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtFutsuRitsu;
        private System.Windows.Forms.Label lblFurikomisakiWariai;
        private jp.co.fsi.common.controls.FsiTextBox txtKozaBangoAzukari;
        private System.Windows.Forms.Label lblKozaBangoAzukari;
        private jp.co.fsi.common.controls.FsiTextBox txtTsumitateRitsuAzukari;
        private System.Windows.Forms.Label lblTsumitateRitsuAzukari;
        private jp.co.fsi.common.controls.FsiTextBox txtDattaiYear;
        private jp.co.fsi.common.controls.FsiTextBox txtKanyuYear;
        private jp.co.fsi.common.controls.FsiTextBox txtJpYear;
        private jp.co.fsi.common.controls.FsiTextBox txtKozaKubunAzukari;
        private System.Windows.Forms.Label lblKozaKubunAzukari;
        private System.Windows.Forms.Label lblKozaKubunAzukariSelect;
        private System.Windows.Forms.TabControl tbcKoza;
        private System.Windows.Forms.TabPage futsu;
        private System.Windows.Forms.TabPage tsumitate;
        private System.Windows.Forms.TabPage azukari;
        private System.Windows.Forms.TabPage honnin;
        private System.Windows.Forms.GroupBox gbxfutsu;
        private System.Windows.Forms.GroupBox gbxTsumitate;
        private System.Windows.Forms.GroupBox gbxAzukari;
        private System.Windows.Forms.GroupBox gbxHonnin;
        private common.controls.FsiTextBox txtKozaKubunHonnin;
        private common.controls.FsiTextBox txtTsumitateRitsuHonnin;
        private common.controls.FsiTextBox txtKozaBangoHonnin;
        private System.Windows.Forms.Label lblTsumitateRitsuHonnin;
        private System.Windows.Forms.Label lblKozaBangoHonnin;
        private System.Windows.Forms.Label lblKozaKubunHonnin;
        private System.Windows.Forms.Label lblKozaKubunHonninSelect;
        private common.controls.FsiTextBox txtPayaoRitsu;
        private System.Windows.Forms.Button button1;
        private common.controls.FsiTextBox txtMyNumberHide;
        private common.controls.FsiTextBox txtMyNumber;
        private System.Windows.Forms.Label label3;
        private common.controls.FsiTextBox txthyoji;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;

    }
}