﻿namespace jp.co.fsi.cm.cmcm1031
{
    partial class CMCM1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.btnEnter = new System.Windows.Forms.Button();
			this.lblYubin = new System.Windows.Forms.Label();
			this.txtYubin02 = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtYubin01 = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblYubinHaihun = new System.Windows.Forms.Label();
			this.lblJushoKana = new System.Windows.Forms.Label();
			this.txtJushoKana = new jp.co.fsi.common.controls.FsiTextBox();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(4, 65);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Visible = false;
			// 
			// btnF2
			// 
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Visible = false;
			// 
			// btnF4
			// 
			this.btnF4.Visible = false;
			// 
			// btnF5
			// 
			this.btnF5.Visible = false;
			// 
			// btnF7
			// 
			this.btnF7.Visible = false;
			// 
			// btnF6
			// 
			this.btnF6.Visible = false;
			// 
			// btnF8
			// 
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Visible = false;
			// 
			// btnF12
			// 
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Location = new System.Drawing.Point(16, 384);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1337, 133);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(601, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.Location = new System.Drawing.Point(16, 120);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(569, 300);
			this.dgvList.TabIndex = 6;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// btnEnter
			// 
			this.btnEnter.BackColor = System.Drawing.Color.SkyBlue;
			this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEnter.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.btnEnter.ForeColor = System.Drawing.Color.Navy;
			this.btnEnter.Location = new System.Drawing.Point(89, 65);
			this.btnEnter.Margin = new System.Windows.Forms.Padding(4);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new System.Drawing.Size(87, 60);
			this.btnEnter.TabIndex = 903;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n決定";
			this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = false;
			this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
			// 
			// lblYubin
			// 
			this.lblYubin.BackColor = System.Drawing.Color.Silver;
			this.lblYubin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblYubin.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblYubin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYubin.Location = new System.Drawing.Point(0, 0);
			this.lblYubin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYubin.Name = "lblYubin";
			this.lblYubin.Padding = new System.Windows.Forms.Padding(27, 0, 0, 0);
			this.lblYubin.Size = new System.Drawing.Size(565, 31);
			this.lblYubin.TabIndex = 0;
			this.lblYubin.Tag = "CHANGE";
			this.lblYubin.Text = "〒";
			this.lblYubin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtYubin02
			// 
			this.txtYubin02.AutoSizeFromLength = false;
			this.txtYubin02.DisplayLength = null;
			this.txtYubin02.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYubin02.ImeMode = System.Windows.Forms.ImeMode.Alpha;
			this.txtYubin02.Location = new System.Drawing.Point(165, 3);
			this.txtYubin02.Margin = new System.Windows.Forms.Padding(4);
			this.txtYubin02.MaxLength = 4;
			this.txtYubin02.Name = "txtYubin02";
			this.txtYubin02.Size = new System.Drawing.Size(80, 23);
			this.txtYubin02.TabIndex = 3;
			this.txtYubin02.Text = "9999";
			this.txtYubin02.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtYubin01
			// 
			this.txtYubin01.AutoSizeFromLength = false;
			this.txtYubin01.DisplayLength = null;
			this.txtYubin01.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtYubin01.ImeMode = System.Windows.Forms.ImeMode.Alpha;
			this.txtYubin01.Location = new System.Drawing.Point(81, 3);
			this.txtYubin01.Margin = new System.Windows.Forms.Padding(4);
			this.txtYubin01.MaxLength = 3;
			this.txtYubin01.Name = "txtYubin01";
			this.txtYubin01.Size = new System.Drawing.Size(52, 23);
			this.txtYubin01.TabIndex = 1;
			this.txtYubin01.Text = "999";
			this.txtYubin01.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblYubinHaihun
			// 
			this.lblYubinHaihun.AutoSize = true;
			this.lblYubinHaihun.BackColor = System.Drawing.Color.Silver;
			this.lblYubinHaihun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblYubinHaihun.Location = new System.Drawing.Point(142, 7);
			this.lblYubinHaihun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblYubinHaihun.Name = "lblYubinHaihun";
			this.lblYubinHaihun.Size = new System.Drawing.Size(16, 16);
			this.lblYubinHaihun.TabIndex = 2;
			this.lblYubinHaihun.Tag = "CHANGE";
			this.lblYubinHaihun.Text = "-";
			// 
			// lblJushoKana
			// 
			this.lblJushoKana.BackColor = System.Drawing.Color.Silver;
			this.lblJushoKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJushoKana.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJushoKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJushoKana.Location = new System.Drawing.Point(0, 0);
			this.lblJushoKana.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJushoKana.Name = "lblJushoKana";
			this.lblJushoKana.Size = new System.Drawing.Size(565, 32);
			this.lblJushoKana.TabIndex = 4;
			this.lblJushoKana.Tag = "CHANGE";
			this.lblJushoKana.Text = "住所カナ";
			this.lblJushoKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtJushoKana
			// 
			this.txtJushoKana.AutoSizeFromLength = false;
			this.txtJushoKana.DisplayLength = null;
			this.txtJushoKana.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtJushoKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtJushoKana.Location = new System.Drawing.Point(81, 3);
			this.txtJushoKana.Margin = new System.Windows.Forms.Padding(4);
			this.txtJushoKana.MaxLength = 30;
			this.txtJushoKana.Name = "txtJushoKana";
			this.txtJushoKana.Size = new System.Drawing.Size(241, 23);
			this.txtJushoKana.TabIndex = 5;
			this.txtJushoKana.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtJushoKana_KeyDown);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(12, 38);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 2;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(573, 78);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtJushoKana);
			this.fsiPanel2.Controls.Add(this.lblJushoKana);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 42);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(565, 32);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtYubin01);
			this.fsiPanel1.Controls.Add(this.lblYubinHaihun);
			this.fsiPanel1.Controls.Add(this.txtYubin02);
			this.fsiPanel1.Controls.Add(this.lblYubin);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(565, 31);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// CMCM1031
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(601, 521);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.MinimizeBox = false;
			this.Name = "CMCM1031";
			this.ShowFButton = true;
			this.ShowTitle = false;
			this.Text = "住所検索";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        protected System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.Label lblYubin;
        private common.controls.FsiTextBox txtYubin02;
        private common.controls.FsiTextBox txtYubin01;
        private System.Windows.Forms.Label lblYubinHaihun;
        private System.Windows.Forms.Label lblJushoKana;
        private common.controls.FsiTextBox txtJushoKana;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel2;
        private common.FsiPanel fsiPanel1;
    }
}
