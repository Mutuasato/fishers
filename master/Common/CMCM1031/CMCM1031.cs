﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm1031
{
    /// <summary>
    /// 住所検索(CMCM1031)
    /// </summary>
    public partial class CMCM1031 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM1031()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // データを表示
            this.getListData(true);
            
            // 郵便番号に初期フォーカス
            this.txtYubin01.Focus();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // 住所カナにフォーカスを戻す
            this.txtJushoKana.Focus();
            this.txtJushoKana.SelectAll();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }

        /// <summary>
        /// 住所ｶﾅでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJushoKana_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.getListData(false);
            }
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データ取得してグリッドに設定
        /// </summary>
        private void getListData(bool isInitial)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder where = new StringBuilder();
            // 入力された郵便番号から検索する
            if (!ValChk.IsEmpty(this.txtYubin01.Text))
            {
                where.Append(" SUBSTRING(SHIN_YUBIN_BANGO, 1, 3) LIKE @YUBIN01 AND");
                dpc.SetParam("@YUBIN01", SqlDbType.VarChar, 32, "%" + this.txtYubin01.Text + "%");
            }
            if (!ValChk.IsEmpty(this.txtYubin02.Text))
            {
                where.Append(" SUBSTRING(SHIN_YUBIN_BANGO, 4, 4) LIKE @YUBIN02 AND");
                dpc.SetParam("@YUBIN02", SqlDbType.VarChar, 32, "%" + this.txtYubin02.Text + "%");
            }
            // 入力された魚種分類カナ名から検索する
            if (!ValChk.IsEmpty(this.txtJushoKana.Text))
            {
                where.Append(" (TODOFUKEN_KANA_NM LIKE @TODOFUKEN_KANA_NM");
                where.Append(" OR SHIKUCHOSON_KANA_NM LIKE @SHIKUCHOSON_KANA_NM");
                where.Append(" OR CHOIKI_KANA_NM LIKE @CHOIKI_KANA_NM) AND");
                // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                dpc.SetParam("@TODOFUKEN_KANA_NM", SqlDbType.VarChar, 32, "%" + this.txtJushoKana.Text + "%");
                dpc.SetParam("@SHIKUCHOSON_KANA_NM", SqlDbType.VarChar, 64, "%" + this.txtJushoKana.Text + "%");
                dpc.SetParam("@CHOIKI_KANA_NM", SqlDbType.VarChar, 64, "%" + this.txtJushoKana.Text + "%");
            }
            where.Append(" 1 = 1");

            string cols = "SHIN_YUBIN_BANGO AS 郵便番号,";
            cols += " TODOFUKEN_NM + SHIKUCHOSON_NM + CHOIKI_NM AS 住所";
            string from = "TB_CM_JUSHO";

            DataTable dtJusho =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "ID", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtJusho.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtJusho.Rows.Add(dtJusho.NewRow());
            }

            this.dgvList.DataSource = dtJusho;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
//            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 75;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 350;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            this.dgvList.Rows[0].Selected = true;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            if (!ValChk.IsEmpty(this.dgvList.SelectedRows[0].Cells["郵便番号"].Value))
            {
                string yubin01 = this.dgvList.SelectedRows[0].Cells["郵便番号"].Value.ToString().Substring(0, 3);
                string yubin02 = this.dgvList.SelectedRows[0].Cells["郵便番号"].Value.ToString().Substring(3);

                this.OutData = new string[3] {
                    yubin01,
                    yubin02,
                    Util.ToString(this.dgvList.SelectedRows[0].Cells["住所"].Value)
                };
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.Cancel;
            }

            this.Close();
        }
        #endregion
    }
}
