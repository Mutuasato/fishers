﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.cm.cmcm2041
{
    /// <summary>
    /// 部門の登録(CMCM2041)
    /// </summary>
    public partial class CMCM2041 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "部門の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2041()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(700, 430);
                this.dgvList.Size = new Size(660, 259);
                // フォームの配置を上へ移動する
                this.lblKanaName.Location = new System.Drawing.Point(13, 12);
                this.txtKanaName.Location = new System.Drawing.Point(132, 14);
                this.dgvList.Location = new System.Drawing.Point(13, 45);

                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }
            // 勘定科目にフォーカス
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Focus();

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }
        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ部門登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 部門登録画面の起動
                Editbumon(string.Empty);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData(false);
        }
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    Editbumon(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                Editbumon(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 部門ビューからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);

            StringBuilder where = new StringBuilder("ZM.KAISHA_CD = @KAISHA_CD");

            if (!isInitial)
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaName.Text))
                {
                    where.Append(" AND ZM.BUMON_KANA_NM LIKE @BUMON_KANA_NM");

                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@BUMON_KANA_NM", SqlDbType.VarChar, 32, "%" + this.txtKanaName.Text + "%");
                }
            }
            string cols = "ZM.BUMON_CD AS コード";
            cols += ", ZM.BUMON_NM AS 部門名";
            cols += ", ZM.BUMON_KANA_NM AS カナ名";
            cols += ", ZM.JIGYO_KUBUN AS 事業区分";
            cols += ", ZM.JIGYO_KUBUN_NM AS 事業区分名称";
            cols += ", ZM.HAIFU_KUBUN AS 配賦区分";

            string from = "VI_ZM_BUMON AS ZM";

            DataTable dtbumon =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "ZM.BUMON_CD", dpc);
   
            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtbumon.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtbumon.Rows.Add(dtbumon.NewRow());
            }

            this.dgvList.DataSource = dtbumon;

            
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 60;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 170;
            this.dgvList.Columns[2].Width = 160;
            this.dgvList.Columns[3].Width = 80;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[4].Width = 110;
            this.dgvList.Columns[5].Width = 80;
        }

        /// <summary>
        /// 部門を追加編集する
        /// </summary>
        /// <param name="code">部門コード(空：新規登録、以外：編集)</param>
        private void Editbumon(string code)
        {
            CMCM2042 frmCMCM2042;

            if (ValChk.IsEmpty(code))
            {
                 // 新規登録モードで登録画面を起動
                 frmCMCM2042 = new CMCM2042("1");
             }
             else
             {
                // 編集モードで登録画面を起動
                frmCMCM2042 = new CMCM2042("2");
                frmCMCM2042.InData = code;
             }

             DialogResult result = frmCMCM2042.ShowDialog(this);

             if (result == DialogResult.OK)
             {
                 // データを再検索する
                 SearchData(false);
                 // 元々選択していたデータを選択
                 for (int i = 0; i < this.dgvList.Rows.Count; i++)
                 {
                     if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["コード"].Value)))
                     {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                     }
                 }
                 // Gridに再度フォーカスをセット
                 this.ActiveControl = this.dgvList;
                 this.dgvList.Focus();
             }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[6] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["部門名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["カナ名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["事業区分"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["事業区分名称"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["配賦区分"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
