﻿using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm2051
{
    /// <summary>
    /// 自動仕訳(発生)検索(CMCM2053)
    /// </summary>
    public partial class CMCM2053 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2053()
        {
            InitializeComponent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public CMCM2053(string Par1) : base(Par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // コード検索画面としての挙動をする
            // タイトルは非表示
            this.lblTitle.Visible = false;
            // サイズを縮める
            this.Size = new Size(454, 441);
            this.dgvList.Size = new Size(413, 324);
            // EscapeとF1のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Location = this.btnF2.Location;
            this.btnF12.Visible = false;

            // 初期状態を作り出す
            SearchData();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // ダイアログとしての処理結果を返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            // 登録画面の起動
            EditShiwake(string.Empty);
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData()
        {
            string shishoCd = this.Par2;
            // 商品テーブルからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, this.Par1);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");

            string cols = "SHIWAKE_CD AS コード";
            cols += ", SHIWAKE_NM AS 仕訳名称";
            cols += ", TAISHAKU_KUBUN_NM AS 貸借";
            string from = "VI_HN_ZIDO_SHIWAKE_SETTEI_B";

            DataTable dtShiwake =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "DENPYO_KUBUN", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtShiwake.Rows.Count == 0)
            {
                dtShiwake.Rows.Add(dtShiwake.NewRow());
            }

            this.dgvList.DataSource = dtShiwake;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ 明朝", 12F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 60;
            this.dgvList.Columns[1].Width = 220;
            this.dgvList.Columns[2].Width = 60;
        }

        /// <summary>
        /// 仕訳コードを追加
        /// </summary>
        /// <param name="code">商品コード(空：新規登録、以外：編集)</param>
        private void EditShiwake(string code)
        {
            CMCM2052 frmCMCM2052;
            if ("1".Equals(this.Par1))
            {
                // 売上仕訳登録モードで登録画面を起動
                frmCMCM2052 = new CMCM2052("1", "1", "2");
            }
            else if ("2".Equals(this.Par1))
            {
                // 仕入仕訳登録モードで登録画面を起動
                frmCMCM2052 = new CMCM2052("1", "2", "2");
            }
            else
            {
                // セリ仕訳登録モードで登録画面を起動
                frmCMCM2052 = new CMCM2052("1", this.Par1, "2");
            }
            frmCMCM2052.SHISHO_CD = this.Par2;

            DialogResult result = frmCMCM2052.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[2] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["仕訳名称"].Value),
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
