﻿namespace jp.co.fsi.cm.cmcm2051
{
    partial class CMCM2051
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTorihikiKbn = new System.Windows.Forms.Label();
            this.txtTorihikiKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblTorihikiKbnResults = new System.Windows.Forms.Label();
            this.txtSetteiKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSetteiKbn = new System.Windows.Forms.Label();
            this.lblSetteiKbnResults = new System.Windows.Forms.Label();
            this.txtShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShishoNm = new System.Windows.Forms.Label();
            this.lblShisho = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.panel3 = new jp.co.fsi.common.FsiPanel();
            this.panel6 = new jp.co.fsi.common.FsiPanel();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.tableLayoutPanel2 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.panel5 = new jp.co.fsi.common.FsiPanel();
            this.panel4 = new jp.co.fsi.common.FsiPanel();
            this.panel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(5, 5);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            this.btnEsc.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(5, 87);
            this.btnF1.Margin = new System.Windows.Forms.Padding(5);
            this.btnF1.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF2
            // 
            this.btnF2.Location = new System.Drawing.Point(119, 87);
            this.btnF2.Margin = new System.Windows.Forms.Padding(5);
            this.btnF2.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF3
            // 
            this.btnF3.Location = new System.Drawing.Point(233, 87);
            this.btnF3.Margin = new System.Windows.Forms.Padding(5);
            this.btnF3.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF4
            // 
            this.btnF4.Location = new System.Drawing.Point(347, 87);
            this.btnF4.Margin = new System.Windows.Forms.Padding(5);
            this.btnF4.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF5
            // 
            this.btnF5.Location = new System.Drawing.Point(460, 87);
            this.btnF5.Margin = new System.Windows.Forms.Padding(5);
            this.btnF5.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(688, 87);
            this.btnF7.Margin = new System.Windows.Forms.Padding(5);
            this.btnF7.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(575, 87);
            this.btnF6.Margin = new System.Windows.Forms.Padding(5);
            this.btnF6.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(801, 87);
            this.btnF8.Margin = new System.Windows.Forms.Padding(5);
            this.btnF8.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF9
            // 
            this.btnF9.Location = new System.Drawing.Point(916, 87);
            this.btnF9.Margin = new System.Windows.Forms.Padding(5);
            this.btnF9.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF12
            // 
            this.btnF12.Location = new System.Drawing.Point(1257, 87);
            this.btnF12.Margin = new System.Windows.Forms.Padding(5);
            this.btnF12.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF11
            // 
            this.btnF11.Location = new System.Drawing.Point(1143, 87);
            this.btnF11.Margin = new System.Windows.Forms.Padding(5);
            this.btnF11.Size = new System.Drawing.Size(116, 80);
            // 
            // btnF10
            // 
            this.btnF10.Location = new System.Drawing.Point(1029, 87);
            this.btnF10.Margin = new System.Windows.Forms.Padding(5);
            this.btnF10.Size = new System.Drawing.Size(116, 80);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(9, 778);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1350, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1340, 41);
            this.lblTitle.Text = "";
            // 
            // lblTorihikiKbn
            // 
            this.lblTorihikiKbn.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikiKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKbn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTorihikiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikiKbn.Location = new System.Drawing.Point(0, 0);
            this.lblTorihikiKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTorihikiKbn.Name = "lblTorihikiKbn";
            this.lblTorihikiKbn.Size = new System.Drawing.Size(541, 30);
            this.lblTorihikiKbn.TabIndex = 3;
            this.lblTorihikiKbn.Tag = "CHANGE";
            this.lblTorihikiKbn.Text = "取 引 区 分";
            this.lblTorihikiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTorihikiKbn
            // 
            this.txtTorihikiKbn.AllowDrop = true;
            this.txtTorihikiKbn.AutoSizeFromLength = false;
            this.txtTorihikiKbn.DisplayLength = null;
            this.txtTorihikiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTorihikiKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTorihikiKbn.Location = new System.Drawing.Point(102, 3);
            this.txtTorihikiKbn.Margin = new System.Windows.Forms.Padding(4);
            this.txtTorihikiKbn.MaxLength = 1;
            this.txtTorihikiKbn.Name = "txtTorihikiKbn";
            this.txtTorihikiKbn.Size = new System.Drawing.Size(33, 23);
            this.txtTorihikiKbn.TabIndex = 4;
            this.txtTorihikiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikiKbn_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvList.Location = new System.Drawing.Point(0, 0);
            this.dgvList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(906, 491);
            this.dgvList.TabIndex = 10;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // lblTorihikiKbnResults
            // 
            this.lblTorihikiKbnResults.BackColor = System.Drawing.Color.LightCyan;
            this.lblTorihikiKbnResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKbnResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikiKbnResults.Location = new System.Drawing.Point(139, 2);
            this.lblTorihikiKbnResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTorihikiKbnResults.Name = "lblTorihikiKbnResults";
            this.lblTorihikiKbnResults.Size = new System.Drawing.Size(388, 24);
            this.lblTorihikiKbnResults.TabIndex = 5;
            this.lblTorihikiKbnResults.Tag = "CHANGE";
            this.lblTorihikiKbnResults.Text = "1:売上　2:仕入　3:セリ　4:控除　5:支払";
            this.lblTorihikiKbnResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSetteiKbn
            // 
            this.txtSetteiKbn.AllowDrop = true;
            this.txtSetteiKbn.AutoSizeFromLength = false;
            this.txtSetteiKbn.DisplayLength = null;
            this.txtSetteiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSetteiKbn.Location = new System.Drawing.Point(118, 3);
            this.txtSetteiKbn.Margin = new System.Windows.Forms.Padding(4);
            this.txtSetteiKbn.MaxLength = 1;
            this.txtSetteiKbn.Name = "txtSetteiKbn";
            this.txtSetteiKbn.Size = new System.Drawing.Size(33, 23);
            this.txtSetteiKbn.TabIndex = 7;
            this.txtSetteiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSetteiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSetteiKbn_Validating);
            // 
            // lblSetteiKbn
            // 
            this.lblSetteiKbn.BackColor = System.Drawing.Color.Silver;
            this.lblSetteiKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSetteiKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSetteiKbn.Location = new System.Drawing.Point(4, 0);
            this.lblSetteiKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSetteiKbn.Name = "lblSetteiKbn";
            this.lblSetteiKbn.Size = new System.Drawing.Size(156, 33);
            this.lblSetteiKbn.TabIndex = 6;
            this.lblSetteiKbn.Tag = "CHANGE";
            this.lblSetteiKbn.Text = "設 定 区 分";
            this.lblSetteiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSetteiKbnResults
            // 
            this.lblSetteiKbnResults.BackColor = System.Drawing.Color.LightCyan;
            this.lblSetteiKbnResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSetteiKbnResults.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblSetteiKbnResults.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSetteiKbnResults.Location = new System.Drawing.Point(159, 0);
            this.lblSetteiKbnResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSetteiKbnResults.Name = "lblSetteiKbnResults";
            this.lblSetteiKbnResults.Size = new System.Drawing.Size(203, 30);
            this.lblSetteiKbnResults.TabIndex = 9;
            this.lblSetteiKbnResults.Tag = "CHANGE";
            this.lblSetteiKbnResults.Text = "1:相手　2:発生";
            this.lblSetteiKbnResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShishoCd
            // 
            this.txtShishoCd.AutoSizeFromLength = true;
            this.txtShishoCd.DisplayLength = null;
            this.txtShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShishoCd.Location = new System.Drawing.Point(69, 4);
            this.txtShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtShishoCd.MaxLength = 4;
            this.txtShishoCd.Name = "txtShishoCd";
            this.txtShishoCd.Size = new System.Drawing.Size(44, 23);
            this.txtShishoCd.TabIndex = 1;
            this.txtShishoCd.TabStop = false;
            this.txtShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblShishoNm
            // 
            this.lblShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoNm.Location = new System.Drawing.Point(117, 3);
            this.lblShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShishoNm.Name = "lblShishoNm";
            this.lblShishoNm.Size = new System.Drawing.Size(283, 24);
            this.lblShishoNm.TabIndex = 2;
            this.lblShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShisho
            // 
            this.lblShisho.BackColor = System.Drawing.Color.Silver;
            this.lblShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShisho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShisho.Location = new System.Drawing.Point(0, 0);
            this.lblShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShisho.Name = "lblShisho";
            this.lblShisho.Size = new System.Drawing.Size(906, 33);
            this.lblShisho.TabIndex = 907;
            this.lblShisho.Tag = "CHANGE";
            this.lblShisho.Text = "支   所";
            this.lblShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 53);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.920415F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.759099F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.30849F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(914, 578);
            this.tableLayoutPanel1.TabIndex = 908;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 83);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(906, 491);
            this.panel3.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dgvList);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(906, 491);
            this.panel6.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(4, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(906, 32);
            this.panel2.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.Controls.Add(this.panel5, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(906, 32);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.txtSetteiKbn);
            this.panel5.Controls.Add(this.lblSetteiKbn);
            this.panel5.Controls.Add(this.lblSetteiKbnResults);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(543, 1);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(362, 30);
            this.panel5.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblTorihikiKbnResults);
            this.panel4.Controls.Add(this.txtTorihikiKbn);
            this.panel4.Controls.Add(this.lblTorihikiKbn);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(1, 1);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(541, 30);
            this.panel4.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtShishoCd);
            this.panel1.Controls.Add(this.lblShishoNm);
            this.panel1.Controls.Add(this.lblShisho);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(906, 33);
            this.panel1.TabIndex = 0;
            // 
            // CMCM2051
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 678);
            this.Controls.Add(this.tableLayoutPanel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "CMCM2051";
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTorihikiKbn;
        private jp.co.fsi.common.controls.FsiTextBox txtTorihikiKbn;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblTorihikiKbnResults;
        private common.controls.FsiTextBox txtSetteiKbn;
        private System.Windows.Forms.Label lblSetteiKbn;
        private System.Windows.Forms.Label lblSetteiKbnResults;
        private common.controls.FsiTextBox txtShishoCd;
        private System.Windows.Forms.Label lblShishoNm;
        private System.Windows.Forms.Label lblShisho;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel1;
        private jp.co.fsi.common.FsiPanel panel3;
        private jp.co.fsi.common.FsiPanel panel2;
        private jp.co.fsi.common.FsiPanel panel1;
        private jp.co.fsi.common.FsiPanel panel6;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel2;
        private jp.co.fsi.common.FsiPanel panel5;
        private jp.co.fsi.common.FsiPanel panel4;
    }
}