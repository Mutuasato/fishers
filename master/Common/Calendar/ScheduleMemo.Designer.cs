﻿namespace jp.co.fsi.com.calendar
{
    partial class ScheduleMemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblDateCap = new System.Windows.Forms.Label();
            this.txtMemo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMemo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblSort = new System.Windows.Forms.Label();
            this.txtSort = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblScope = new System.Windows.Forms.Label();
            this.gpbScope = new System.Windows.Forms.GroupBox();
            this.rdbPersonal = new System.Windows.Forms.RadioButton();
            this.rdbPublic = new System.Windows.Forms.RadioButton();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.gpbScope.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(4, 65);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
            // 
            // btnF1
            // 
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Text = "F7\r\n\r\n削除";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(16, 380);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1440, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(704, 31);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.EnableHeadersVisualStyles = false;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.dgvList.Location = new System.Drawing.Point(16, 52);
            this.dgvList.Margin = new System.Windows.Forms.Padding(4);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(672, 172);
            this.dgvList.TabIndex = 2;
            this.dgvList.CurrentCellChanged += new System.EventHandler(this.dgvList_CurrentCellChanged);
            // 
            // lblDateCap
            // 
            this.lblDateCap.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateCap.Location = new System.Drawing.Point(17, 19);
            this.lblDateCap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateCap.Name = "lblDateCap";
            this.lblDateCap.Size = new System.Drawing.Size(75, 27);
            this.lblDateCap.TabIndex = 0;
            this.lblDateCap.Text = "日付：";
            this.lblDateCap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMemo
            // 
            this.txtMemo.AutoSizeFromLength = false;
            this.txtMemo.DisplayLength = null;
            this.txtMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMemo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtMemo.Location = new System.Drawing.Point(16, 271);
            this.txtMemo.Margin = new System.Windows.Forms.Padding(4);
            this.txtMemo.Multiline = true;
            this.txtMemo.Name = "txtMemo";
            this.txtMemo.Size = new System.Drawing.Size(671, 60);
            this.txtMemo.TabIndex = 4;
            this.txtMemo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMemo_Validating);
            // 
            // lblMemo
            // 
            this.lblMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMemo.Location = new System.Drawing.Point(17, 240);
            this.lblMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMemo.Name = "lblMemo";
            this.lblMemo.Size = new System.Drawing.Size(89, 27);
            this.lblMemo.TabIndex = 3;
            this.lblMemo.Text = "[メモ]";
            this.lblMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDate
            // 
            this.lblDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDate.Location = new System.Drawing.Point(83, 19);
            this.lblDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(185, 27);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "平成26年 4月30日";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSort
            // 
            this.lblSort.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSort.Location = new System.Drawing.Point(17, 345);
            this.lblSort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(104, 27);
            this.lblSort.TabIndex = 5;
            this.lblSort.Text = "[並び順]";
            this.lblSort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSort
            // 
            this.txtSort.AutoSizeFromLength = true;
            this.txtSort.DisplayLength = null;
            this.txtSort.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSort.Location = new System.Drawing.Point(129, 347);
            this.txtSort.Margin = new System.Windows.Forms.Padding(4);
            this.txtSort.MaxLength = 3;
            this.txtSort.Name = "txtSort";
            this.txtSort.Size = new System.Drawing.Size(35, 23);
            this.txtSort.TabIndex = 6;
            this.txtSort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSort.Validating += new System.ComponentModel.CancelEventHandler(this.txtSort_Validating);
            // 
            // lblScope
            // 
            this.lblScope.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblScope.Location = new System.Drawing.Point(204, 345);
            this.lblScope.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblScope.Name = "lblScope";
            this.lblScope.Size = new System.Drawing.Size(125, 27);
            this.lblScope.TabIndex = 7;
            this.lblScope.Text = "[公開範囲]";
            this.lblScope.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gpbScope
            // 
            this.gpbScope.Controls.Add(this.rdbPersonal);
            this.gpbScope.Controls.Add(this.rdbPublic);
            this.gpbScope.Location = new System.Drawing.Point(324, 336);
            this.gpbScope.Margin = new System.Windows.Forms.Padding(4);
            this.gpbScope.Name = "gpbScope";
            this.gpbScope.Padding = new System.Windows.Forms.Padding(4);
            this.gpbScope.Size = new System.Drawing.Size(160, 40);
            this.gpbScope.TabIndex = 8;
            this.gpbScope.TabStop = false;
            // 
            // rdbPersonal
            // 
            this.rdbPersonal.AutoSize = true;
            this.rdbPersonal.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdbPersonal.Location = new System.Drawing.Point(79, 13);
            this.rdbPersonal.Margin = new System.Windows.Forms.Padding(4);
            this.rdbPersonal.Name = "rdbPersonal";
            this.rdbPersonal.Size = new System.Drawing.Size(58, 20);
            this.rdbPersonal.TabIndex = 1;
            this.rdbPersonal.TabStop = true;
            this.rdbPersonal.Text = "個人";
            this.rdbPersonal.UseVisualStyleBackColor = true;
            // 
            // rdbPublic
            // 
            this.rdbPublic.AutoSize = true;
            this.rdbPublic.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdbPublic.Location = new System.Drawing.Point(8, 13);
            this.rdbPublic.Margin = new System.Windows.Forms.Padding(4);
            this.rdbPublic.Name = "rdbPublic";
            this.rdbPublic.Size = new System.Drawing.Size(58, 20);
            this.rdbPublic.TabIndex = 0;
            this.rdbPublic.TabStop = true;
            this.rdbPublic.Text = "全社";
            this.rdbPublic.UseVisualStyleBackColor = true;
            // 
            // ScheduleMemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(704, 517);
            this.Controls.Add(this.gpbScope);
            this.Controls.Add(this.lblScope);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.txtSort);
            this.Controls.Add(this.lblMemo);
            this.Controls.Add(this.lblSort);
            this.Controls.Add(this.txtMemo);
            this.Controls.Add(this.lblDateCap);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.MinimizeBox = false;
            this.Name = "ScheduleMemo";
            this.ShowFButton = true;
            this.ShowTitle = false;
            this.Text = "メモ登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblDateCap, 0);
            this.Controls.SetChildIndex(this.txtMemo, 0);
            this.Controls.SetChildIndex(this.lblSort, 0);
            this.Controls.SetChildIndex(this.lblMemo, 0);
            this.Controls.SetChildIndex(this.txtSort, 0);
            this.Controls.SetChildIndex(this.lblDate, 0);
            this.Controls.SetChildIndex(this.lblScope, 0);
            this.Controls.SetChildIndex(this.gpbScope, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.gpbScope.ResumeLayout(false);
            this.gpbScope.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblDateCap;
        private common.controls.FsiTextBox txtMemo;
        private System.Windows.Forms.Label lblMemo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblSort;
        private common.controls.FsiTextBox txtSort;
        private System.Windows.Forms.Label lblScope;
        private System.Windows.Forms.GroupBox gpbScope;
        private System.Windows.Forms.RadioButton rdbPublic;
        private System.Windows.Forms.RadioButton rdbPersonal;

    }
}