﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm2031
{
    /// <summary>
    /// 支所の登録(cmcm2031)
    /// </summary>
    public partial class CMCM2031 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "支所の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2031()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // サイズを縮める
                this.Size = new Size(664, 652);
                //タイトルを非表示
                this.lblTitle.Visible = false;
                // フォームの配置を上へ移動する
                //this.lblKanaNm.Location = new System.Drawing.Point(14, 14);
                //this.txtKanaName.Location = new System.Drawing.Point(95, 17);
                //this.ckboxAllHyoji.Location = new System.Drawing.Point(451, 14);
                //this.dgvList.Location = new System.Drawing.Point(12, 52);
                // Escapeのみ表示とF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // フォーカス設定
            this.txtKanaName.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }

            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ支所登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 支所登録画面の起動
                EditFunanushi(string.Empty);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        /*public override void PressF5()
        {
            // メンテ機能で立ち上げている場合のみ支所マスタ一覧を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 支所マスタ一覧の起動
                ItiranFunanushi();
            }
        }*/
        #endregion

        #region イベント
        /// <summary>
        /// 表示ボタンの変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoBashoNm01_CheckedChanged(object sender, System.EventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない
            if (txtKanaName.Text != "")
            {
                // 入力された情報を元に検索する
                SearchData(false);
            }
            else
            {
                SearchData(true);
            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditFunanushi(Util.ToString(this.dgvList.SelectedRows[0].Cells["支所コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditFunanushi(Util.ToString(this.dgvList.SelectedRows[0].Cells["支所コード"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 支所マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD"); ;
            if (isInitial)
            {
                // 初期処理の場合　表示フラグが1の支所は表示しない
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaName.Text))
                {
                    where.Append(" AND SHISHO_KANA_NM LIKE '%" + this.txtKanaName.Text + "%'");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    //dpc.SetParam("@SHISHO_KANA_NM", SqlDbType.VarChar, 30, "'%" + this.txtKanaName.Text+"%'");
                }
            }
            string cols = "SHISHO_CD AS 支所コード";
            cols += ", SHISHO_NM AS 支所名";
            cols += ", SHISHO_KANA_NM AS 支所カナ名";
            string from = "TB_CM_SHISHO ";

            DataTable dtShisho =
                this.Dba.GetDataTableByConditionWithParams(cols, from, Util.ToString(where), dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtShisho.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtShisho.Rows.Add(dtShisho.NewRow());
            }

            this.dgvList.DataSource = dtShisho;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ 明朝", 12F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ 明朝", 12F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 270;
            this.dgvList.Columns[2].Width = 250;
        }

        /// <summary>
        /// 支所を追加編集する
        /// </summary>
        /// <param name="code">支所コード(空：新規登録、以外：編集)</param>
        private void EditFunanushi(string code)
        {
            CMCM2032 frmCMCM1022;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmCMCM1022 = new CMCM2032("1", this.Par2);
            }
            else
            {
                // 編集モードで登録画面を起動
                frmCMCM1022 = new CMCM2032("2", this.Par2);
                frmCMCM1022.InData = code;
            }

            DialogResult result = frmCMCM1022.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["支所コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 支所マスタ一覧画面を起動する
        /// </summary>
        /// <param name="code">支所コード(空：新規登録、以外：編集)</param>
        private void ItiranFunanushi()
        {
            KOBC9023 frmCMC1023;
            // 支所マスタ一覧画面を起動
            frmCMC1023 = new KOBC9023();
            
            DialogResult result = frmCMC1023.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["支所コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["支所名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["支所カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
