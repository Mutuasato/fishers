﻿namespace jp.co.fsi.cm.cmcm2011
{
    partial class CMCM2013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
			this.lblShzHsSrMemo = new System.Windows.Forms.Label();
			this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
			this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
			this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
			this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtKingakuHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
			this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
			this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
			this.lblKingakuHasuShori = new System.Windows.Forms.Label();
			this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel5.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEsc
			// 
			this.btnEsc.Location = new System.Drawing.Point(5, 5);
			this.btnEsc.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF1
			// 
			this.btnF1.Location = new System.Drawing.Point(5, 64);
			this.btnF1.Margin = new System.Windows.Forms.Padding(5);
			// 
			// btnF2
			// 
			this.btnF2.Location = new System.Drawing.Point(91, 64);
			this.btnF2.Margin = new System.Windows.Forms.Padding(5);
			this.btnF2.Text = "F2";
			this.btnF2.Visible = false;
			// 
			// btnF3
			// 
			this.btnF3.Location = new System.Drawing.Point(177, 64);
			this.btnF3.Margin = new System.Windows.Forms.Padding(5);
			this.btnF3.Text = "F3\r\n\r\n削除";
			// 
			// btnF4
			// 
			this.btnF4.Location = new System.Drawing.Point(263, 64);
			this.btnF4.Margin = new System.Windows.Forms.Padding(5);
			this.btnF4.Text = "F4";
			this.btnF4.Visible = false;
			// 
			// btnF5
			// 
			this.btnF5.Location = new System.Drawing.Point(349, 64);
			this.btnF5.Margin = new System.Windows.Forms.Padding(5);
			this.btnF5.Text = "F5";
			this.btnF5.Visible = false;
			// 
			// btnF7
			// 
			this.btnF7.Location = new System.Drawing.Point(521, 64);
			this.btnF7.Margin = new System.Windows.Forms.Padding(5);
			this.btnF7.Visible = false;
			// 
			// btnF6
			// 
			this.btnF6.Location = new System.Drawing.Point(435, 64);
			this.btnF6.Margin = new System.Windows.Forms.Padding(5);
			this.btnF6.Text = "F6\r\n\r\n登録";
			// 
			// btnF8
			// 
			this.btnF8.Location = new System.Drawing.Point(607, 64);
			this.btnF8.Margin = new System.Windows.Forms.Padding(5);
			this.btnF8.Visible = false;
			// 
			// btnF9
			// 
			this.btnF9.Location = new System.Drawing.Point(693, 64);
			this.btnF9.Margin = new System.Windows.Forms.Padding(5);
			this.btnF9.Visible = false;
			// 
			// btnF12
			// 
			this.btnF12.Location = new System.Drawing.Point(951, 64);
			this.btnF12.Margin = new System.Windows.Forms.Padding(5);
			this.btnF12.Text = "F12";
			this.btnF12.Visible = false;
			// 
			// btnF11
			// 
			this.btnF11.Location = new System.Drawing.Point(865, 64);
			this.btnF11.Margin = new System.Windows.Forms.Padding(5);
			this.btnF11.Visible = false;
			// 
			// btnF10
			// 
			this.btnF10.Location = new System.Drawing.Point(779, 64);
			this.btnF10.Margin = new System.Windows.Forms.Padding(5);
			this.btnF10.Visible = false;
			// 
			// pnlDebug
			// 
			this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.pnlDebug.Location = new System.Drawing.Point(7, 178);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(1045, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(539, 31);
			this.lblTitle.TabIndex = 999;
			this.lblTitle.Text = "";
			this.lblTitle.Visible = false;
			// 
			// lblShzTnkHohoMemo
			// 
			this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(182, 2);
			this.lblShzTnkHohoMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
			this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(272, 24);
			this.lblShzTnkHohoMemo.TabIndex = 47;
			this.lblShzTnkHohoMemo.Tag = "CHANGE";
			this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
			this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShzHsSrMemo
			// 
			this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzHsSrMemo.Location = new System.Drawing.Point(182, 3);
			this.lblShzHsSrMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
			this.lblShzHsSrMemo.Size = new System.Drawing.Size(272, 24);
			this.lblShzHsSrMemo.TabIndex = 44;
			this.lblShzHsSrMemo.Tag = "CHANGE";
			this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
			this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiTenkaHoho
			// 
			this.txtShohizeiTenkaHoho.AutoSizeFromLength = false;
			this.txtShohizeiTenkaHoho.BackColor = System.Drawing.Color.White;
			this.txtShohizeiTenkaHoho.DisplayLength = null;
			this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(129, 3);
			this.txtShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(5);
			this.txtShohizeiTenkaHoho.MaxLength = 1;
			this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
			this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(29, 23);
			this.txtShohizeiTenkaHoho.TabIndex = 46;
			this.txtShohizeiTenkaHoho.Text = "2";
			this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiTenkaHoho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohizeiTenkaHoho_KeyDown);
			this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
			// 
			// txtShohizeiHasuShori
			// 
			this.txtShohizeiHasuShori.AutoSizeFromLength = false;
			this.txtShohizeiHasuShori.BackColor = System.Drawing.Color.White;
			this.txtShohizeiHasuShori.DisplayLength = null;
			this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiHasuShori.Location = new System.Drawing.Point(129, 3);
			this.txtShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(5);
			this.txtShohizeiHasuShori.MaxLength = 1;
			this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
			this.txtShohizeiHasuShori.Size = new System.Drawing.Size(29, 23);
			this.txtShohizeiHasuShori.TabIndex = 43;
			this.txtShohizeiHasuShori.Text = "2";
			this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
			// 
			// lblShzNrkHohoNm
			// 
			this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.Silver;
			this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShzNrkHohoNm.Location = new System.Drawing.Point(182, 2);
			this.lblShzNrkHohoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
			this.lblShzNrkHohoNm.Size = new System.Drawing.Size(272, 24);
			this.lblShzNrkHohoNm.TabIndex = 29;
			this.lblShzNrkHohoNm.Tag = "CHANGE";
			this.lblShzNrkHohoNm.Text = "税抜き入力（自動計算あり）";
			this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKgkHsShrMemo
			// 
			this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
			this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKgkHsShrMemo.Location = new System.Drawing.Point(182, 2);
			this.lblKgkHsShrMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
			this.lblKgkHsShrMemo.Size = new System.Drawing.Size(272, 24);
			this.lblKgkHsShrMemo.TabIndex = 26;
			this.lblKgkHsShrMemo.Tag = "CHANGE";
			this.lblKgkHsShrMemo.Text = "1:切捨て 2:四捨五入 3:切り上げ";
			this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTnkStkHohoMemo
			// 
			this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
			this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(182, 2);
			this.lblTnkStkHohoMemo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
			this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(272, 24);
			this.lblTnkStkHohoMemo.TabIndex = 23;
			this.lblTnkStkHohoMemo.Tag = "CHANGE";
			this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
			this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtShohizeiNyuryokuHoho
			// 
			this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
			this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.White;
			this.txtShohizeiNyuryokuHoho.DisplayLength = null;
			this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(129, 3);
			this.txtShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(5);
			this.txtShohizeiNyuryokuHoho.MaxLength = 1;
			this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
			this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(29, 23);
			this.txtShohizeiNyuryokuHoho.TabIndex = 28;
			this.txtShohizeiNyuryokuHoho.Text = "2";
			this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohizeiNyuryokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiNyuryokuHoho_Validating);
			// 
			// txtKingakuHasuShori
			// 
			this.txtKingakuHasuShori.AutoSizeFromLength = false;
			this.txtKingakuHasuShori.BackColor = System.Drawing.Color.White;
			this.txtKingakuHasuShori.DisplayLength = null;
			this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtKingakuHasuShori.Location = new System.Drawing.Point(129, 3);
			this.txtKingakuHasuShori.Margin = new System.Windows.Forms.Padding(5);
			this.txtKingakuHasuShori.MaxLength = 1;
			this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
			this.txtKingakuHasuShori.Size = new System.Drawing.Size(29, 23);
			this.txtKingakuHasuShori.TabIndex = 25;
			this.txtKingakuHasuShori.Text = "2";
			this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
			// 
			// txtTankaShutokuHoho
			// 
			this.txtTankaShutokuHoho.AutoSizeFromLength = false;
			this.txtTankaShutokuHoho.BackColor = System.Drawing.Color.White;
			this.txtTankaShutokuHoho.DisplayLength = null;
			this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtTankaShutokuHoho.Location = new System.Drawing.Point(129, 2);
			this.txtTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(5);
			this.txtTankaShutokuHoho.MaxLength = 1;
			this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
			this.txtTankaShutokuHoho.Size = new System.Drawing.Size(29, 23);
			this.txtTankaShutokuHoho.TabIndex = 22;
			this.txtTankaShutokuHoho.Text = "0";
			this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
			// 
			// lblShohizeiTenkaHoho
			// 
			this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiTenkaHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohizeiTenkaHoho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(0, 0);
			this.lblShohizeiTenkaHoho.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
			this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(510, 29);
			this.lblShohizeiTenkaHoho.TabIndex = 45;
			this.lblShohizeiTenkaHoho.Tag = "CHANGE";
			this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
			this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiHasuShori
			// 
			this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohizeiHasuShori.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiHasuShori.Location = new System.Drawing.Point(0, 0);
			this.lblShohizeiHasuShori.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
			this.lblShohizeiHasuShori.Size = new System.Drawing.Size(510, 29);
			this.lblShohizeiHasuShori.TabIndex = 42;
			this.lblShohizeiHasuShori.Tag = "CHANGE";
			this.lblShohizeiHasuShori.Text = "消費税端数処理";
			this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblShohizeiNyuryokuHoho
			// 
			this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohizeiNyuryokuHoho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(0, 0);
			this.lblShohizeiNyuryokuHoho.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
			this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(510, 29);
			this.lblShohizeiNyuryokuHoho.TabIndex = 27;
			this.lblShohizeiNyuryokuHoho.Tag = "CHANGE";
			this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
			this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblKingakuHasuShori
			// 
			this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
			this.lblKingakuHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKingakuHasuShori.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKingakuHasuShori.Location = new System.Drawing.Point(0, 0);
			this.lblKingakuHasuShori.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
			this.lblKingakuHasuShori.Size = new System.Drawing.Size(510, 29);
			this.lblKingakuHasuShori.TabIndex = 24;
			this.lblKingakuHasuShori.Tag = "CHANGE";
			this.lblKingakuHasuShori.Text = "金額端数処理";
			this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTankaShutokuHoho
			// 
			this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
			this.lblTankaShutokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTankaShutokuHoho.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblTankaShutokuHoho.Location = new System.Drawing.Point(0, 0);
			this.lblTankaShutokuHoho.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
			this.lblTankaShutokuHoho.Size = new System.Drawing.Size(510, 29);
			this.lblTankaShutokuHoho.TabIndex = 21;
			this.lblTankaShutokuHoho.Tag = "CHANGE";
			this.lblTankaShutokuHoho.Text = "単価取得方法";
			this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(12, 45);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 5;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(518, 181);
			this.fsiTableLayoutPanel1.TabIndex = 1000;
			// 
			// fsiPanel5
			// 
			this.fsiPanel5.Controls.Add(this.lblShzTnkHohoMemo);
			this.fsiPanel5.Controls.Add(this.txtShohizeiTenkaHoho);
			this.fsiPanel5.Controls.Add(this.lblShohizeiTenkaHoho);
			this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel5.Location = new System.Drawing.Point(4, 148);
			this.fsiPanel5.Name = "fsiPanel5";
			this.fsiPanel5.Size = new System.Drawing.Size(510, 29);
			this.fsiPanel5.TabIndex = 4;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.txtShohizeiHasuShori);
			this.fsiPanel4.Controls.Add(this.lblShzHsSrMemo);
			this.fsiPanel4.Controls.Add(this.lblShohizeiHasuShori);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 112);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(510, 29);
			this.fsiPanel4.TabIndex = 3;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtShohizeiNyuryokuHoho);
			this.fsiPanel3.Controls.Add(this.lblShzNrkHohoNm);
			this.fsiPanel3.Controls.Add(this.lblShohizeiNyuryokuHoho);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 76);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(510, 29);
			this.fsiPanel3.TabIndex = 2;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtKingakuHasuShori);
			this.fsiPanel2.Controls.Add(this.lblKgkHsShrMemo);
			this.fsiPanel2.Controls.Add(this.lblKingakuHasuShori);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 40);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(510, 29);
			this.fsiPanel2.TabIndex = 1;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtTankaShutokuHoho);
			this.fsiPanel1.Controls.Add(this.lblTnkStkHohoMemo);
			this.fsiPanel1.Controls.Add(this.lblTankaShutokuHoho);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(510, 29);
			this.fsiPanel1.TabIndex = 0;
			// 
			// CMCM2013
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(539, 317);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "CMCM2013";
			this.Text = "船主マスタ初期設定";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel5.ResumeLayout(false);
			this.fsiPanel5.PerformLayout();
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtKingakuHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtTankaShutokuHoho;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiTenkaHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiHasuShori;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
        private System.Windows.Forms.Label lblShzHsSrMemo;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel5;
		private common.FsiPanel fsiPanel4;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
	}
}