﻿namespace jp.co.fsi.cm.cmcm2041
{
    partial class CMCM2042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblBumonCd = new System.Windows.Forms.Label();
			this.lblJigyoKubun = new System.Windows.Forms.Label();
			this.txtJigyoKbn = new jp.co.fsi.common.controls.FsiTextBox();
			this.txtBumonKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblBumonKanaNm = new System.Windows.Forms.Label();
			this.lblJigyoKbn = new System.Windows.Forms.Label();
			this.txtBumonNm = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblBumonNm = new System.Windows.Forms.Label();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel4.SuspendLayout();
			this.fsiPanel3.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 173);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(508, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(519, 32);
			this.lblTitle.Text = "部門の登録";
			// 
			// txtBumonCd
			// 
			this.txtBumonCd.AutoSizeFromLength = true;
			this.txtBumonCd.DisplayLength = null;
			this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtBumonCd.Location = new System.Drawing.Point(100, 2);
			this.txtBumonCd.Margin = new System.Windows.Forms.Padding(4);
			this.txtBumonCd.MaxLength = 6;
			this.txtBumonCd.Name = "txtBumonCd";
			this.txtBumonCd.Size = new System.Drawing.Size(63, 23);
			this.txtBumonCd.TabIndex = 2;
			this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblBumonCd
			// 
			this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
			this.lblBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBumonCd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBumonCd.Location = new System.Drawing.Point(0, 0);
			this.lblBumonCd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBumonCd.Name = "lblBumonCd";
			this.lblBumonCd.Size = new System.Drawing.Size(500, 28);
			this.lblBumonCd.TabIndex = 1;
			this.lblBumonCd.Tag = "CHANGE";
			this.lblBumonCd.Text = "部門コード";
			this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJigyoKubun
			// 
			this.lblJigyoKubun.BackColor = System.Drawing.Color.LightCyan;
			this.lblJigyoKubun.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblJigyoKubun.Location = new System.Drawing.Point(182, 2);
			this.lblJigyoKubun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJigyoKubun.Name = "lblJigyoKubun";
			this.lblJigyoKubun.Size = new System.Drawing.Size(207, 24);
			this.lblJigyoKubun.TabIndex = 6;
			this.lblJigyoKubun.Tag = "DISPNAME";
			this.lblJigyoKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtJigyoKbn
			// 
			this.txtJigyoKbn.AutoSizeFromLength = true;
			this.txtJigyoKbn.DisplayLength = null;
			this.txtJigyoKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtJigyoKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.txtJigyoKbn.Location = new System.Drawing.Point(100, 2);
			this.txtJigyoKbn.Margin = new System.Windows.Forms.Padding(4);
			this.txtJigyoKbn.MaxLength = 3;
			this.txtJigyoKbn.Name = "txtJigyoKbn";
			this.txtJigyoKbn.Size = new System.Drawing.Size(63, 23);
			this.txtJigyoKbn.TabIndex = 5;
			this.txtJigyoKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtJigyoKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtJigyoKbn_KeyDown);
			this.txtJigyoKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtJigyoKbn_Validating);
			// 
			// txtBumonKanaNm
			// 
			this.txtBumonKanaNm.AutoSizeFromLength = false;
			this.txtBumonKanaNm.DisplayLength = null;
			this.txtBumonKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtBumonKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtBumonKanaNm.Location = new System.Drawing.Point(100, 2);
			this.txtBumonKanaNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtBumonKanaNm.MaxLength = 30;
			this.txtBumonKanaNm.Name = "txtBumonKanaNm";
			this.txtBumonKanaNm.Size = new System.Drawing.Size(357, 23);
			this.txtBumonKanaNm.TabIndex = 3;
			// 
			// lblBumonKanaNm
			// 
			this.lblBumonKanaNm.BackColor = System.Drawing.Color.Silver;
			this.lblBumonKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBumonKanaNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblBumonKanaNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBumonKanaNm.Location = new System.Drawing.Point(0, 0);
			this.lblBumonKanaNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBumonKanaNm.Name = "lblBumonKanaNm";
			this.lblBumonKanaNm.Size = new System.Drawing.Size(500, 28);
			this.lblBumonKanaNm.TabIndex = 2;
			this.lblBumonKanaNm.Tag = "CHANGE";
			this.lblBumonKanaNm.Text = "部門カナ名";
			this.lblBumonKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJigyoKbn
			// 
			this.lblJigyoKbn.BackColor = System.Drawing.Color.Silver;
			this.lblJigyoKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJigyoKbn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblJigyoKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblJigyoKbn.Location = new System.Drawing.Point(0, 0);
			this.lblJigyoKbn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJigyoKbn.Name = "lblJigyoKbn";
			this.lblJigyoKbn.Size = new System.Drawing.Size(500, 29);
			this.lblJigyoKbn.TabIndex = 4;
			this.lblJigyoKbn.Tag = "CHANGE";
			this.lblJigyoKbn.Text = "事業区分";
			this.lblJigyoKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBumonNm
			// 
			this.txtBumonNm.AutoSizeFromLength = false;
			this.txtBumonNm.DisplayLength = null;
			this.txtBumonNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtBumonNm.ImeMode = System.Windows.Forms.ImeMode.On;
			this.txtBumonNm.Location = new System.Drawing.Point(100, 2);
			this.txtBumonNm.Margin = new System.Windows.Forms.Padding(4);
			this.txtBumonNm.MaxLength = 40;
			this.txtBumonNm.Name = "txtBumonNm";
			this.txtBumonNm.Size = new System.Drawing.Size(357, 23);
			this.txtBumonNm.TabIndex = 1;
			// 
			// lblBumonNm
			// 
			this.lblBumonNm.BackColor = System.Drawing.Color.Silver;
			this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBumonNm.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblBumonNm.Location = new System.Drawing.Point(0, 0);
			this.lblBumonNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBumonNm.Name = "lblBumonNm";
			this.lblBumonNm.Size = new System.Drawing.Size(500, 28);
			this.lblBumonNm.TabIndex = 0;
			this.lblBumonNm.Tag = "CHANGE";
			this.lblBumonNm.Text = "部　門　名";
			this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(5, 37);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 4;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(508, 142);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			// 
			// fsiPanel4
			// 
			this.fsiPanel4.Controls.Add(this.lblJigyoKubun);
			this.fsiPanel4.Controls.Add(this.txtJigyoKbn);
			this.fsiPanel4.Controls.Add(this.lblJigyoKbn);
			this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel4.Location = new System.Drawing.Point(4, 109);
			this.fsiPanel4.Name = "fsiPanel4";
			this.fsiPanel4.Size = new System.Drawing.Size(500, 29);
			this.fsiPanel4.TabIndex = 3;
			this.fsiPanel4.Tag = "CHANGE";
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.Controls.Add(this.txtBumonKanaNm);
			this.fsiPanel3.Controls.Add(this.lblBumonKanaNm);
			this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel3.Location = new System.Drawing.Point(4, 74);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(500, 28);
			this.fsiPanel3.TabIndex = 2;
			this.fsiPanel3.Tag = "CHANGE";
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.txtBumonNm);
			this.fsiPanel2.Controls.Add(this.lblBumonNm);
			this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel2.Location = new System.Drawing.Point(4, 39);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(500, 28);
			this.fsiPanel2.TabIndex = 1;
			this.fsiPanel2.Tag = "CHANGE";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtBumonCd);
			this.fsiPanel1.Controls.Add(this.lblBumonCd);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(500, 28);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "CHANGE";
			// 
			// CMCM2042
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(519, 311);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "CMCM2042";
			this.ShowFButton = true;
			this.Text = "部門の登録";
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel4.ResumeLayout(false);
			this.fsiPanel4.PerformLayout();
			this.fsiPanel3.ResumeLayout(false);
			this.fsiPanel3.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblBumonCd;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonNm;
        private System.Windows.Forms.Label lblBumonNm;
        private System.Windows.Forms.Label lblBumonKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonKanaNm;
        private System.Windows.Forms.Label lblJigyoKbn;
        private System.Windows.Forms.Label lblJigyoKubun;
        private common.controls.FsiTextBox txtJigyoKbn;
		private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
		private common.FsiPanel fsiPanel4;
		private common.FsiPanel fsiPanel3;
		private common.FsiPanel fsiPanel2;
		private common.FsiPanel fsiPanel1;
	}
}