﻿namespace jp.co.fsi.cm.cmcm2041
{
    partial class CMCM2041
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this.lblKanaName = new System.Windows.Forms.Label();
			this.txtKanaName = new System.Windows.Forms.TextBox();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.pnlDebug.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.fsiTableLayoutPanel1.SuspendLayout();
			this.fsiPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlDebug
			// 
			this.pnlDebug.Location = new System.Drawing.Point(7, 531);
			this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
			this.pnlDebug.Size = new System.Drawing.Size(714, 133);
			// 
			// lblTitle
			// 
			this.lblTitle.ForeColor = System.Drawing.Color.Black;
			this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.lblTitle.Size = new System.Drawing.Size(704, 32);
			this.lblTitle.Text = "部門の登録";
			// 
			// lblKanaName
			// 
			this.lblKanaName.BackColor = System.Drawing.Color.Silver;
			this.lblKanaName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblKanaName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblKanaName.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.lblKanaName.Location = new System.Drawing.Point(0, 0);
			this.lblKanaName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKanaName.Name = "lblKanaName";
			this.lblKanaName.Size = new System.Drawing.Size(687, 30);
			this.lblKanaName.TabIndex = 1;
			this.lblKanaName.Tag = "CHANGE";
			this.lblKanaName.Text = "カ　　ナ　　名";
			this.lblKanaName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtKanaName
			// 
			this.txtKanaName.Font = new System.Drawing.Font("ＭＳ 明朝", 12F);
			this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.txtKanaName.Location = new System.Drawing.Point(123, 3);
			this.txtKanaName.Margin = new System.Windows.Forms.Padding(4);
			this.txtKanaName.MaxLength = 30;
			this.txtKanaName.Name = "txtKanaName";
			this.txtKanaName.Size = new System.Drawing.Size(239, 23);
			this.txtKanaName.TabIndex = 1;
			this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new System.Drawing.Point(4, 90);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(695, 479);
			this.dgvList.TabIndex = 3;
			this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			// 
			// fsiTableLayoutPanel1
			// 
			this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.fsiTableLayoutPanel1.ColumnCount = 1;
			this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
			this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(4, 45);
			this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
			this.fsiTableLayoutPanel1.RowCount = 1;
			this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(695, 38);
			this.fsiTableLayoutPanel1.TabIndex = 902;
			this.fsiTableLayoutPanel1.Tag = "";
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Controls.Add(this.txtKanaName);
			this.fsiPanel1.Controls.Add(this.lblKanaName);
			this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(687, 30);
			this.fsiPanel1.TabIndex = 0;
			this.fsiPanel1.Tag = "";
			// 
			// CMCM2041
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(704, 669);
			this.Controls.Add(this.fsiTableLayoutPanel1);
			this.Controls.Add(this.dgvList);
			this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
			this.Margin = new System.Windows.Forms.Padding(7);
			this.Name = "CMCM2041";
			this.Par1 = "";
			this.Text = "部門 の登録";
			this.Controls.SetChildIndex(this.pnlDebug, 0);
			this.Controls.SetChildIndex(this.lblTitle, 0);
			this.Controls.SetChildIndex(this.dgvList, 0);
			this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
			this.pnlDebug.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.fsiTableLayoutPanel1.ResumeLayout(false);
			this.fsiPanel1.ResumeLayout(false);
			this.fsiPanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKanaName;
        private System.Windows.Forms.TextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel1;
    }
}