﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm2061
{
    /// <summary>
    /// 摘要の登録(変更・追加)(CMCM2062)
    /// </summary>
    public partial class CMCM2062 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2062()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public CMCM2062(string par1) : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 初期値、入力制御を実装
            this.txtMizuageShishoCd.Text = this.Par2;
            this.lblMizuageShishoNm.Text = this.Dba.GetName(UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：摘要コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;

            // EscapeとF1のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                default:
                    {
                        this.txtTekiyoCd.Focus();
                        this.txtTekiyoCd.SelectAll();
                        break;
                    }
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (MODE_NEW.Equals(Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                StringBuilder Sql;
                DbParamCollection dpc;
                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append(" DELETE ");
                Sql.Append(" FROM ");
                Sql.Append("     TB_HN_TEKIYO ");
                Sql.Append(" WHERE ");
                Sql.Append("     KAISHA_CD = @KAISHA_CD AND ");
                Sql.Append("     SHISHO_CD = @SHISHO_CD AND ");
                Sql.Append("     TEKIYO_CD = @TEKIYO_CD");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 6, Util.ToDecimal(txtTekiyoCd.Text));

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTanto = SetCmTekiyoParams();

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.摘要マスタ
                    this.Dba.Insert("TB_HN_TEKIYO", (DbParamCollection)alParamsCmTanto[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.摘要マスタ
                    this.Dba.Update("TB_HN_TEKIYO",
                        (DbParamCollection)alParamsCmTanto[1],
                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND TEKIYO_CD = @TEKIYO_CD",
                        (DbParamCollection)alParamsCmTanto[0]);

                }

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("登録しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 摘要コードの値変更時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoCd_TextChanged(object sender, EventArgs e)
        {
            // 既に存在するコードを入力した場合は表示する
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append(" ,TEKIYO_CD");
            cols.Append(" ,TEKIYO_NM");
            cols.Append(" ,TEKIYO_KANA_NM");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_TEKIYO");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtTekiyoCd.Text));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND TEKIYO_CD = @TEKIYO_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                this.txtTekiyoNm.Text = "";
                this.txtTekiyoKanaNm.Text = "";
                this.btnF3.Enabled = false;
                this.Par1 = "1";
            }
            else
            {
                // 取得した内容を表示
                DataRow drDispData = dtDispData.Rows[0];
                this.txtTekiyoCd.Text = Util.ToString(drDispData["TEKIYO_CD"]);
                this.txtTekiyoNm.Text = Util.ToString(drDispData["TEKIYO_NM"]);
                this.txtTekiyoKanaNm.Text = Util.ToString(drDispData["TEKIYO_KANA_NM"]);
                this.btnF3.Enabled = true;
                this.Par1 = "2";
            }
        }

        /// <summary>
        /// 摘要コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyoCd())
            {
                e.Cancel = true;
                this.txtTekiyoCd.SelectAll();
            }
        }

        /// <summary>
        /// 摘要名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyoNm())
            {
                e.Cancel = true;
                this.txtTekiyoNm.SelectAll();
            }
        }

        /// <summary>
        /// 摘要カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyoKanaNm())
            {
                e.Cancel = true;
                this.txtTekiyoKanaNm.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 摘要カナ名のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyoKanaNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // 確認メッセージを表示
                string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
                if (Msg.ConfYesNo(msg) == DialogResult.No)
                {
                    // 「いいえ」を押されたら処理終了
                    return;
                }

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                // 入力値をバインドパラメータとしてセットする
                ArrayList alParamsCmTanto = SetCmTekiyoParams();

                try
                {
                    // トランザクションの開始
                    this.Dba.BeginTransaction();

                    if (MODE_NEW.Equals(this.Par1))
                    {
                        // データ登録
                        // 共通.摘要マスタ
                        this.Dba.Insert("TB_HN_TEKIYO", (DbParamCollection)alParamsCmTanto[0]);
                    }
                    else if (MODE_EDIT.Equals(this.Par1))
                    {
                        // データ更新
                        // 共通.摘要マスタ
                        this.Dba.Update("TB_HN_TEKIYO",
                            (DbParamCollection)alParamsCmTanto[1],
                            "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND TEKIYO_CD = @TEKIYO_CD",
                            (DbParamCollection)alParamsCmTanto[0]);

                    }

                    // トランザクションをコミット
                    this.Dba.Commit();

                    Msg.Info("登録しました。");
                }
                finally
                {
                    // ロールバック
                    this.Dba.Rollback();
                }

                // DialogResultに「OK」をセットし結果を返却
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 更新処理時は必須
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 摘要コードの初期値を取得
            this.txtTekiyoCd.Text = "0";

            // 摘要名に初期フォーカス
            this.ActiveControl = this.txtTekiyoCd;
            this.txtTekiyoCd.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append(" ,TEKIYO_CD");
            cols.Append(" ,TEKIYO_NM");
            cols.Append(" ,TEKIYO_KANA_NM");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_TEKIYO");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND TEKIYO_CD = @TEKIYO_CD",
                    dpc);

            //if (dtDispData.Rows.Count == 0)
            //{
                //Msg.Error("不正な起動です。終了します。");
                //this.Close();
            //}

            // 取得した内容を表示
            if (dtDispData.Rows.Count > 0)
            {
                DataRow drDispData = dtDispData.Rows[0];
                this.txtTekiyoCd.Text = Util.ToString(drDispData["TEKIYO_CD"]);
                this.txtTekiyoNm.Text = Util.ToString(drDispData["TEKIYO_NM"]);
                this.txtTekiyoKanaNm.Text = Util.ToString(drDispData["TEKIYO_KANA_NM"]);
            }
        }

        /// <summary>
        /// 摘要コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoCd()
        {
            // 摘要コードが0の場合はエラーとする
            if ((Util.ToInt(this.txtTekiyoCd.Text)) == 0)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtTekiyoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTekiyoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 摘要名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTekiyoNm.Text, this.txtTekiyoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 摘要カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyoKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTekiyoKanaNm.Text, this.txtTekiyoKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 摘要コードのチェック
                if (!IsValidTekiyoCd())
                {
                    this.txtTekiyoCd.Focus();
                    return false;
                }
            }

            // 摘要名のチェック
            if (!IsValidTekiyoNm())
            {
                this.txtTekiyoNm.Focus();
                return false;
            }

            // 摘要カナ名のチェック
            if (!IsValidTekiyoKanaNm())
            {
                this.txtTekiyoKanaNm.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_TEKIYOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmTekiyoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと摘要コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                updParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと摘要コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                whereParam.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, this.txtTekiyoCd.Text);
                alParams.Add(whereParam);
            }

            // 摘要名
            updParam.SetParam("@TEKIYO_NM", SqlDbType.VarChar, 40, this.txtTekiyoNm.Text);
            // 摘要カナ名
            updParam.SetParam("@TEKIYO_KANA_NM", SqlDbType.VarChar, 30, this.txtTekiyoKanaNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }
        #endregion
    }
}
