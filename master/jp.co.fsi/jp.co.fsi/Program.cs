﻿using jp.co.fsi.ClientCommon;
using jp.co.fsi.common;
using jp.co.fsi.common.CommonFrm;
using jp.co.fsi.common.util;
using jp.co.fsi.Login;
using jp.co.fsi.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jp.co.fsi
{
#pragma warning disable 3270
#pragma warning disable CS0219

	static class Program
	{


		#region ログ
		readonly static log4net.ILog log =
			log4net.LogManager.GetLogger(
				System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		#endregion

		/// <summary>
		/// アプリケーションのメイン エントリ ポイントです。
		/// </summary>
		[STAThread]
		static void Main()
		{
            //管理者として自分自身を起動する
            System.Diagnostics.ProcessStartInfo psi =
                new System.Diagnostics.ProcessStartInfo();
            //動詞に「runas」をつける
            psi.Verb = "runas";

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			try
			{

				frmWarrning frmWarn = null;

				// プログラム利用許諾認証チェック
				switch (SecureUtils.ChkUserLicense())
				{
					case 0:
					case -9999:
						log.Info("利用許諾チェックで失敗しました。");
						break;
					case -4000:

#if DEBUG==false
						log.Info("利用期限切れです。");
						frmWarn = new frmWarrning();

						frmWarn.StartPosition = FormStartPosition.CenterScreen;

						frmWarn.STR_MESSAGE = "完　　了";

						frmWarn.ShowDialog();

						Application.Exit();
#endif
						break;

					case -4010:
#if DEBUG==false
						log.Info("利用期限切れ間近。");
						frmWarn = new frmWarrning();

						frmWarn.STR_MESSAGE = "完了間近";

						frmWarn.STR_DATE = SecureUtils.STR_ENDDATE;

						frmWarn.StartPosition = FormStartPosition.CenterScreen;

						frmWarn.ShowDialog();
#endif
						break;

				}

				// コネクションストリングの取得とセット
				// 漁協毎に接続データベースを切り替えるため
				if (!SecureUtils.SettingConnectionString())
				{
					MessageBox.Show("Config.Xml設定時にエラーが発生しました。プログラムを終了します。",
						"エラー",MessageBoxButtons.OK,MessageBoxIcon.Error);

				}

				ConfigLoader cLdr = new ConfigLoader();

				string msgs = SSHConnection.SSHConnect();

				log.Info("画面起動開始");

				// Config.xmlの[UserInfo][doLogin]のフラグで起動画面を切り替える。
				if ("1" == cLdr.LoadCommonConfig("UserInfo", "dologin"))
				{
					log.Info("ログイン画面起動");
					Application.Run(new FrmLogin());
				}
				else
				{
					Application.Run(new FrmMenu());
				}
			}
			catch (Exception ex)
			{
				log.Error("プログラム起動時のエラー：" + Environment.NewLine + ex.Message);
				MessageBox.Show("起動時のエラー:" + ex.Message);
			}
			finally
			{
				// SSH開放
				SSHConnection.SSHDisConnect();
				// 共有メモリ開放
				//SecureUtils.CloseShareMem();
			}
		}
	}
}
