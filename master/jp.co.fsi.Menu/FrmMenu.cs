﻿using jp.co.fsi.ClientCommon;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Deployment.Application;

namespace jp.co.fsi.Menu
{
    /// <summary>
    /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
    /// <para>■ 機能説明： メニュー画面</para>
    /// <para>■ 機能概要： メニュー画面。</para>
    /// <para>■</para>
    /// <para>■ 作成者：   FSI</para>
    /// <para>■ 作成日：   2019-11</para>
    /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
    /// </summary>
    public partial class FrmMenu : BaseForm
    {


        #region 業務状態
        private Dictionary<string, bool> dicGyumuflags;
        #endregion

        #region ツリービューメニューデータ
        private DataTable dtDataTreeView;
        #endregion

        #region メニューデータ格納
        private DataTable dtMenu;
        #endregion

        #region 起動プログラム格納変数
        private BasePgForm execForms;
        #endregion

        private string nl2 = Environment.NewLine + Environment.NewLine;
        string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

        #region 子フォーム
        private BasePgForm childForm;
        /// <summary>
        /// <para>□□□□□□□□□□□□□□□□□□□□□□□□□□□</para>
        /// <para>□ 子フォーム</para>
        /// <para>□□□□□□□□□□□□□□□□□□□□□□□□□□□</para>
        /// </summary>
        public BasePgForm ChildForm
        {
            get
            {
                return childForm;
            }

            set
            {
                childForm = value;
            }
        }
        #endregion

        #region 起動機能ＩＤ
        /// <summary>
        /// <para>□□□□□□□□□□□□□□□□□□□□□□□□□□□</para>
        /// <para>□ 起動機能ＩＤ</para>
        /// <para>□□□□□□□□□□□□□□□□□□□□□□□□□□□</para>
        /// </summary>
        private int runningSubSysIdx;
        #endregion

        #region 業務一覧情報格納DataTable
        /// <summary>
        /// <para>□□□□□□□□□□□□□□□□□□□□□□□□□□□</para>
        /// <para>□ 業務一覧情報格納DataTable</para>
        /// <para>□□□□□□□□□□□□□□□□□□□□□□□□□□□</para>
        /// </summary>
        private DataTable dtGyoumu;
        #endregion

        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： コンストラクタ</para>
        /// <para>■ 機能概要： コンストラクタ。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public FrmMenu()
        {
            InitializeComponent();
        }

        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： 画面起動処理</para>
        /// <para>■ 機能概要： 画面起動時の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        protected override void InitForm()
        {

			this.Text = this.Text + " Ver" + GetVersion();
			dicGyumuflags = new Dictionary<string, bool>();

            string[] arrJpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // ユーザー名の表示
            tsLabelLoginUser.Text = this.UInfo.KaishaNm + " " + UInfo.UserNm + " 様"; ;
            // 現在日付の表示
            tsLabelTime.Text = arrJpDate[5];
            // 会計期の表示
            tsLabelKi.Text = this.UInfo.KessanKiDsp;

            // ツリービューデータ
            dtDataTreeView = new DataTable();

            // 使用可否区分によってボタンの表示を制御する
            dicGyumuflags["dicHan"] = this.Config.LoadCommonConfig("UserInfo", "usehn").Equals("1");
            dicGyumuflags["dicKou"] = this.Config.LoadCommonConfig("UserInfo", "usekb").Equals("1");
            dicGyumuflags["dicZam"] = this.Config.LoadCommonConfig("UserInfo", "usezm").Equals("1");
            dicGyumuflags["dicKyu"] = this.Config.LoadCommonConfig("UserInfo", "useky").Equals("1");
            dicGyumuflags["dicShoku"] = this.Config.LoadCommonConfig("UserInfo", "usesk").Equals("1");

            // メニュー用データテーブル初期化
            InitMenuDataTable();

            // ボタンの初期化
            ClearForm();

            // ツリーメニューの設定
            SetTree();

            // タスクバーを有効にする。
            this.MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;

			this.treeMenu.Focus();
		}


#region 継承イベント

        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： Esc押下処理</para>
        /// <para>■ 機能概要： Escキーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressEsc()
        {


			this.lstMenu.Focus();

			if (null != execForms)
            {
				if (execForms.OrgnDialogResult)
				{
					splitContainer1.Panel1Collapsed = true;
					execForms.Focus();
				}
				else
				{
                    splitContainer1.Panel1Collapsed = false;

                    splitContainer1.FixedPanel = FixedPanel.Panel1;
                    ClearFunckeys();
				}
			}
            else
            {
                splitContainer1.Panel1Collapsed = false;

                splitContainer1.FixedPanel = FixedPanel.Panel1;
                ClearFunckeys();

            }
        }

		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能説明： F1押下処理</para>
		/// <para>■ 機能概要： F1キーが押された場合の処理</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		public override void PressF1()
        {

        }

        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F2押下処理</para>
        /// <para>■ 機能概要： F2キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF2()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F3押下処理</para>
        /// <para>■ 機能概要： F3キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF3()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F4押下処理</para>
        /// <para>■ 機能概要： F4キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF4()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F5押下処理</para>
        /// <para>■ 機能概要： F5キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF5()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F6押下処理</para>
        /// <para>■ 機能概要： F6キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF6()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F7押下処理</para>
        /// <para>■ 機能概要： F7キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF7()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F8押下処理</para>
        /// <para>■ 機能概要： F8キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF8()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F9押下処理</para>
        /// <para>■ 機能概要： F9キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF9()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F10押下処理</para>
        /// <para>■ 機能概要： F10キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF10()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F11押下処理</para>
        /// <para>■ 機能概要： F11キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF11()
        {

        }
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： F12押下処理</para>
        /// <para>■ 機能概要： F12キーが押された場合の処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        public override void PressF12()
        {

        }


#endregion

#region ツリーノードクリック
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ツリーノードクリック処理</para>
        /// <para>■ 機能概要： ツリーノードクリック処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void treeMenu_AfterSelect(object sender, TreeViewEventArgs e)
        {

            if (execForms != null)
            {
                execForms.Close();
                execForms.FuncBtnEnableChanged -= Frm_FuncBtnEnableChanged;
                execForms = null;
            }

            // ツリーノードが選択されたときの処理
            ExecTreeMenu(e);

        }


#endregion

#region 機能メニューダブルクリック

        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： 機能メニューダブルクリック</para>
        /// <para>■ 機能概要： 機能メニューダブルクリック</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void lstMenu_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (execForms != null)
            {
                execForms.Close();
				execForms.Dispose();
				execForms.FuncBtnEnableChanged -= Frm_FuncBtnEnableChanged;
                execForms = null;

            }
            ExecListmenu();
        }

#endregion

#region 機能メニュー選択時の処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： 機能メニュー選択時の処理</para>
        /// <para>■ 機能概要： 機能メニュー選択時の処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void lstMenu_SelectedIndexChanged(object sender, EventArgs e)
        {

            ExecChoiceItem();

        }
#endregion


#region ヘッダーボタンクリック
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ヘッダーボタン押下処理</para>
        /// <para>■ 機能概要： ヘッダーボタンが押された場合の処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void tsBtn_Click(object sender, EventArgs e)
        {

            ExecHeaderBtn(sender);
        }
#endregion

#region ファンクションボタンクリック処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ファンクションボタンクリック処理</para>
        /// <para>■ 機能概要： ファンクションボタンクリック処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void tsBBtnF_Click(object sender, EventArgs e)
        {

            this.Cursor = Cursors.WaitCursor;

            ExecFuncKeys(sender);

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region 画面リサイズ処理

        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： 画面リサイズ処理</para>
        /// <para>■ 機能概要： 画面リサイズ処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void FrmMenu_Resize(object sender, EventArgs e)
        {

            // 画面サイズがかわったら子フォームのサイズも変更する。

            if (null != execForms)
            {
                execForms.TopLevel = false;
                execForms.WindowState = FormWindowState.Minimized;

                execForms.FormBorderStyle = FormBorderStyle.None;
                execForms.WindowState = FormWindowState.Normal;

                execForms.Width = pnlBody.Width;
                execForms.Height = pnlBody.Height;

            }

        }
#endregion

#region プライベートメソッド

#region ＩＤ取得処理
        /// <summary>
        /// サブシステムIDを取得する
        /// </summary>
        /// <returns>サブシステムID</returns>
        private string GetSubSysId()
        {
            string subSysId = string.Empty;
            switch (this.runningSubSysIdx)
            {
                case 1:
                    subSysId = "HAN";
                    break;

                case 2:
                    subSysId = "KOB";
                    break;

                case 3:
                    subSysId = "ZAM";
                    break;

                case 4:
                    subSysId = "KYU";
                    break;

                case 5:
                    subSysId = "SKD";
                    break;
            }

            return subSysId;
        }

#endregion

#region ファンクションキー表示処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ファンクションキー表示処理</para>
        /// <para>■ 機能概要： メニュー画面のファンクションキーの表示処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void DispFunctionKey()
        {
            Button btnTmp;

            string subSysId = GetSubSysId();
            DataRow drSubSysSetting = this.MInfo.LoadSubSysConf(subSysId);

            // ファンクションキーの表示
            if (drSubSysSetting != null)
            {
                for (int i = 1; i <= 12; i++)
                {
                    btnTmp = (Button)this.Controls.Find("btnF" + Util.ToString(i), true)[0];
                    btnTmp.Text = "F" + Util.ToString(i) + nl2 + Util.ToString(drSubSysSetting["CapF" + Util.ToString(i)]);

                    // キャプションが空ならDisable
                    btnTmp.Enabled = !ValChk.IsEmpty(drSubSysSetting["CapF" + Util.ToString(i)]);
                }
            }
            else
            {
                for (int i = 1; i <= 12; i++)
                {
                    btnTmp = (Button)this.Controls.Find("btnF" + Util.ToString(i), true)[0];
                    btnTmp.Text = "F" + Util.ToString(i) + nl2;

                    // キャプションが空ならDisable
                    btnTmp.Enabled = false;
                }
            }
        }
#endregion

#region カレンダーボタン押下処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： カレンダーボタン押下処理</para>
        /// <para>■ 機能概要： カレンダーボタン押下処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void Calendar_Click()
        {
            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "Calendar.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.com.calendar.Calendar");
            if (t != null)
            {
                Object obj = System.Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.InData = DateTime.Now;
                    frm.ShowDialog(this);

                    frm.Dispose();
                }
            }
        }
#endregion

#region ログアウト処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ログアウト処理</para>
        /// <para>■ 機能概要： ログアウト処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void btnLogout_Click()
        {
            if (Msg.ConfOKCancel("ログアウトしますが、宜しいですか？") == System.Windows.Forms.DialogResult.OK)
            {
                this.Visible = false;
                // アセンブリのロード
                Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\" + "Login.exe");
                // フォーム作成
                Type t = asm.GetType("jp.co.fsi.Login.FrmLogin");
                if (t != null)
                {
                    Object obj = System.Activator.CreateInstance(t);
                    if (obj != null)
                    {
                        BaseForm frm = (BaseForm)obj;
                        frm.Height = 282;
                        frm.ShowDialog(this);
                        frm.Dispose();

						frm = null;
                    }
                }
                // 画面を閉じる
                this.Close();
            }

            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
        }

#endregion

#region ツリーメニュー設定

        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ツリーメニュー処理</para>
        /// <para>■ 機能概要： ツリーメニュー処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void SetTree()
        {

            treeMenu.AllClear();

            DataView dv = new DataView(dtDataTreeView);

            treeMenu.AutoBuildTree = false;

            treeMenu.AddGroup("TITLE", "TITLECD", "TITLE", "TITLECD", 0, 1);
            treeMenu.AddGroup("CATE01", "CATECD01", "CATE01", "CATECD01", 0, 1);
            treeMenu.SetLeafData("CA02", "CATE02", "CATECD02", "CHOICE", 0, 1);
            treeMenu.DataSource = dv;

            treeMenu.BuildTree();

            treeMenu.ExpandAll();

			// 先頭ノードの取得
			if (0<treeMenu.Nodes.Count)
			{

				TreeNode tops = treeMenu.Nodes[0];

				treeMenu.SelectedNode = tops;
			}
        }


#endregion

#region 会計期間クリック処理
        private void lblAccYear_Click()
        {
            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1011.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.cm.cmcm1011.CMCM1011");
            if (t != null)
            {
                Object obj = System.Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.InData = this.UInfo.KessanKi;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] outData = (string[])frm.OutData;
                        this.UInfo.KaishaCd = outData[0];
                        this.UInfo.KaishaNm = outData[1];
                        this.UInfo.KessanKi = Util.ToInt(outData[2]);
                        this.Config.SetCommonConfig("UserInfo", "kaishacd", outData[0]);
                        this.Config.SetCommonConfig("UserInfo", "kaishanm", outData[1]);
                        this.Config.SetCommonConfig("UserInfo", "kaikeinendo", Util.ToString(this.UInfo.KaikeiNendo));
                        this.Config.SetCommonConfig("UserInfo", "kessanki", Util.ToString(this.UInfo.KessanKi));
                        this.Config.SetCommonConfig("UserInfo", "kessankiDsp", Util.ToString(this.UInfo.KessanKiDsp));
                        this.Config.SaveConfig();
                        this.tsLabelKi.Text = this.UInfo.KessanKiDsp;
                    }

					frm.Dispose();

					frm = null;
                }
            }
        }
#endregion


#region クリア処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： 画面クリア処理</para>
        /// <para>■ 機能概要： 画面クリア処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void ClearForm()
        {

            // サブ機能ＩＤの初期化
            runningSubSysIdx = 0;

            ClearFunckeys();

            string strCateCd01 = string.Empty;
            string strCate01 = string.Empty;

            foreach (string key in dicGyumuflags.Keys)
            {
                switch (key)
                {
                    case "dicHan":
                        if (dicGyumuflags[key])
                        {
                            SetGyoumuData("セリ販売", "0101", "HAN");
                        }

                        break;
                    case "dicKou":
                        if (dicGyumuflags[key])
                        {
                            SetGyoumuData("購　　買", "0201", "KOB");
                        }

                        break;
                    case "dicZam":
                        if (dicGyumuflags[key])
                        {
                            SetGyoumuData("財　　務", "0301", "ZAM");
                        }

                        break;
                    case "dicKyu":
                        if (dicGyumuflags[key])
                        {
                            SetGyoumuData("給　　与", "0401", "KYU");
                        }

                        break;
                    case "dicShoku":
                        if (dicGyumuflags[key])
                        {
                            SetGyoumuData("食堂販売", "0501", "SKD");
                        }

                        break;
                }
            }

        }

#endregion

#region ファンクションボタンクリア
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ファンクションボタンクリア</para>
        /// <para>■ 機能概要： ファンクションボタンクリア</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void ClearFunckeys()
        {
            int intFKeyNum = 1;
            // ファンクションキーのクリア
            foreach (object b in bottmTool.Items)
            {
                if (b is ToolStripButton)
                {
                    if ("Esc" != ((ToolStripButton)b).Text)
                    {
                        ((ToolStripButton)b).Text = "F" + intFKeyNum.ToString() + Environment.NewLine;
                        ((ToolStripButton)b).Enabled = false;
                        intFKeyNum++;
                    }
                    else
                    {
                        ((ToolStripButton)b).Text = "Esc" + Environment.NewLine;
                        ((ToolStripButton)b).Enabled = false;
                    }
                }
            }
            tsLabelKi.Enabled = true;
        }
#endregion

#region メニューデータテーブル列設定
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： メニューデータテーブル列設定処理</para>
        /// <para>■ 機能概要： メニューデータテーブル列設定処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void InitMenuDataTable()
        {

            dtDataTreeView = new DataTable();

            DataColumn col01 = new DataColumn("TITLECD");
            DataColumn col02 = new DataColumn("TITLE");
            DataColumn col03 = new DataColumn("CATECD01");
            DataColumn col04 = new DataColumn("CATE01");
            DataColumn col05 = new DataColumn("CATECD02");
            DataColumn col06 = new DataColumn("CATE02");
            DataColumn col07 = new DataColumn("CHOICE");

            dtDataTreeView.Columns.AddRange(new DataColumn[] {
                col01,
                col02,
                col03,
                col04,
                col05,
                col06,
                col07 });

        }

#endregion

#region メニュー用データテーブルセット

        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： メニューデータテーブルデータ設定処理</para>
        /// <para>■ 機能概要： メニューデータテーブルデータ設定処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void SetMenuDataTable(string strCate01,
                                      string strCateCd01,
                                      string strCate02,
                                      string strCateCd02)
        {

            DataRow dr = dtDataTreeView.NewRow();

            dr["TITLE"] = "統合システムメニュー";
            dr["TITLECD"] = "01";
            dr["CATE01"] = strCate01;
            dr["CATECD01"] = strCateCd01;
            dr["CATE02"] = strCate02;
            dr["CATECD02"] = strCateCd02;
            dr["CHOICE"] = "0";

            dtDataTreeView.Rows.Add(dr);
        }

#endregion

#region 業務セット

        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： 業務セット処理</para>
        /// <para>■ 機能概要： 業務セット処理</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void SetGyoumuData(string strCate01, string strCateCd01, string strDFlags)
        {

            this.dtGyoumu = this.MInfo.GetGyomuList(strDFlags);

            // メニュー格納用
            DataRow[] arrGyomuSetting = null;


            for (int i = 1; i <= 10; i++)
            {
                arrGyomuSetting = this.dtGyoumu.Select("Pos = " + Util.ToString(i));

                if (arrGyomuSetting.Length > 0)
                {
                    SetMenuDataTable(strCate01, strCateCd01,
                        Util.ToString(arrGyomuSetting[0]["PrgNm"]),
                        strCateCd01 + i.ToString().PadLeft(2, '0'));
                }
            }
        }

#endregion

#region ツリーメニュークリック処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ツリーメニュークリック処理</para>
        /// <para>■ 機能概要： ツリーメニュークリック処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void ExecTreeMenu(TreeViewEventArgs e)
        {
            string strGyoumuId = string.Empty;

            // まだデータがセットされていない場合は以降の処理を行わない。
            if (null == e.Node)
            {
                return;
            }
            if (null == e.Node.Parent)
            {
                return;
            }

            // 機能メニューのクリア
            lstMenu.Items.Clear();

            lstMenu.LargeImageList = null;
            lstMenu.StateImageList = null;
            lstMenu.SmallImageList = null;

            // 親の名前を取得
            string strCate01 = e.Node.Parent.Text;
            // 選択されているノード名の取得
            string strCate02 = e.Node.Text;

            DataView dvv = (DataView)treeMenu.DataSource;

            dvv.RowFilter = "CATE01 = '" + strCate01 + "' AND CATE02 = '" + strCate02 + "'";

            // カテゴリコードの取得
            if (0 < dvv.Count)
            {

                ImageList iLstimg = new ImageList();

                ClientUtil.SetListThumbnail("",lstMenu, iLstimg);

                runningSubSysIdx = Util.ToInt(dvv[0]["CATECD02"].ToString().Substring(0, 2));

                string subSysId = GetSubSysId();

                // 業務メニューデータの取得
                dtGyoumu = this.MInfo.GetGyomuList(subSysId);

                int idx = Util.ToInt(dvv[0]["CATECD02"].ToString().Substring(4, 2));

                // 業務番号の取得
                strGyoumuId = Util.ToString(dtGyoumu.Select("Pos = " + Util.ToString(idx))[0]["GyomuId"]);

                dtMenu = this.MInfo.GetMenuList(subSysId, strGyoumuId);

                DataRow[] arrMenuSetting;

                int lstCount = 0;

                // 機能メニューをセットする。
                for (int i = 1; i <= 16; i++)
                {

                    arrMenuSetting = dtMenu.Select("Pos = " + Util.ToString(i));

                    if (0 < arrMenuSetting.Length)
                    {
                        lstMenu.Items.Add(arrMenuSetting[0]["PrgNm"].ToString());

                        lstMenu.Items[lstCount].ImageIndex = 0;
                        if (arrMenuSetting[0]["PrgId"].ToString().ToString().IndexOf("R")>0)
                        {
                            lstMenu.Items[lstCount].ImageIndex = 1;
                        }

                        // リストビューオブジェクトのSubにセットしているのは
                        // 1 ネームスペース
                        // 2 プログラムID
                        // 3 パラメータ１
                        // 4 パラメータ２
                        // 5 パラメータ３
                        // 6 プログラム名

                        lstMenu.Items[lstCount].SubItems.Add(
                            arrMenuSetting[0]["Namespace"].ToString() + ":" +
                            arrMenuSetting[0]["PrgId"].ToString() + ":" +
                            arrMenuSetting[0]["Par1"].ToString() + ":" +
                            arrMenuSetting[0]["Par2"].ToString() + ":" +
                            arrMenuSetting[0]["Par3"].ToString() + ":" +
                            arrMenuSetting[0]["PrgNm"].ToString());

                        lstCount++;

                        lstMenu.Refresh();

                    }
                }
            }
        }
#endregion

#region ヘッダボタンクリック処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ヘッダーボタン押下処理</para>
        /// <para>■ 機能概要： ヘッダーボタンが押された場合の処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void ExecHeaderBtn(object sender)
        {
            if (sender is ToolStripButton)
            {
                switch (((ToolStripButton)sender).Name)
                {
                    case "tsBtnCalendar":
                        Calendar_Click();
                        break;

                    case "tsBtnLogout":
                        btnLogout_Click();
                        break;

                    case "tsBtnEnd":

                        this.Close();
                        break;
                }
            }
            else
            {
                // 会計期間がクリックされた場合
                switch (((ToolStripLabel)sender).Name)
                {
                    case "tsLabelKi":

                        lblAccYear_Click();

                        break;
                }
            }
        }

#endregion

#region 機能メニュークリック処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： 機能メニュークリック処理</para>
        /// <para>■ 機能概要： 機能メニュークリック処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void ExecListmenu()
        {

            // リストビューに表示されたアイコンがダブルクリックされると
            // 本メソッド内で起動するEXEファイルをメモリ上にロードし
            // クラス化して、本メニュー画面スプリットコンテナーPanel2へ画面を表示する。
            //　起動する際に必要なEXE名やネームスペース、パラメータ等はListViewのSubItem内に
            //  格納している。
            try
            {

                if (0 < lstMenu.SelectedIndices.Count)
                {

					if ((null != ChildForm )&&(ChildForm.IsDisposed))
					{
						ChildForm = null;
					}

                    var proc = new System.Diagnostics.Process();
                    //string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

                    string kinouAttr = lstMenu.SelectedItems[0].SubItems[1].Text;

                    string[] kinouattrs = kinouAttr.Split(':');

                    // プログラムＩＤでロード
                    Assembly asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + kinouattrs[1] + ".exe");// EXE名 Menu.csv

                    // 名前でインスタンス
                    Type t = asm.GetType(kinouattrs[0] + "." + kinouattrs[1]);// 名前空間とEXE名 Menu.csv

                    if (null != t)
                    {
                        dynamic obj = System.Activator.CreateInstance(t);

                        if (null != obj)
                        {

                            // 業務メニューを消す。
                            splitContainer1.Panel1Collapsed = true;

                            BasePgForm frm = (BasePgForm)obj;
                            //frm.BackColor = System.Drawing.Color.Blue;
                            //frm.ForeColor = System.Drawing.Color.White;

                            frm.TopLevel = false;
                            frm.FormBorderStyle = FormBorderStyle.None;
                            frm.WindowState = FormWindowState.Maximized;
                            frm.MenuFrm = this;
                            frm.Par1 = kinouattrs[2];// 機能名 Menu.csv
                            frm.Par2 = kinouattrs[3];// 機能名 Menu.csv
                            frm.Par3 = kinouattrs[4];// 機能名 Menu.csv
                            frm.Text = kinouattrs[5];// 機能名 Menu.csv

                            frm.ShowFButton = false;
                            // 右上終了ボタンは消す。
                            frm.ControlBox = false;

                            //frm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                            //    System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));

                            frm.AutoScaleMode = AutoScaleMode.Dpi;
                            //frm.AutoScaleDimensions = null;

                            pnlBody.Controls.Add(frm);

                            frm.Show();

                            frm.BringToFront();

                            tsBtnEsc.Text = frm.btnEsc.Text.Replace(nl2, Environment.NewLine);
                            tsBtnEsc.Enabled = frm.btnEsc.Enabled;

                            tsBtnF01.Text = frm.btnF1.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF01.Enabled = frm.btnF1.Enabled;

                            tsBtnF02.Text = frm.btnF2.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF02.Enabled = frm.btnF2.Enabled;

                            tsBtnF03.Text = frm.btnF3.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF03.Enabled = frm.btnF3.Enabled;

                            tsBtnF04.Text = frm.btnF4.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF04.Enabled = frm.btnF4.Enabled;

                            tsBtnF05.Text = frm.btnF5.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF05.Enabled = frm.btnF5.Enabled;

                            tsBtnF06.Text = frm.btnF6.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF06.Enabled = frm.btnF6.Enabled;

                            tsBtnF07.Text = frm.btnF7.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF07.Enabled = frm.btnF7.Enabled;

                            tsBtnF08.Text = frm.btnF8.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF08.Enabled = frm.btnF8.Enabled;

                            tsBtnF09.Text = frm.btnF9.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF09.Enabled = frm.btnF9.Enabled;

                            tsBtnF10.Text = frm.btnF10.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF10.Enabled = frm.btnF10.Enabled;

                            tsBtnF11.Text = frm.btnF11.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF11.Enabled = frm.btnF11.Enabled;

                            tsBtnF12.Text = frm.btnF12.Text.Replace(nl2, Environment.NewLine);
                            tsBtnF12.Enabled = frm.btnF12.Enabled;

                            frm.AutoScroll = true;
                            frm.Width = pnlBody.Width;
                            frm.Height = pnlBody.Height;

                            execForms = frm;
                            execForms.Refresh();
                            execForms.FuncBtnEnableChanged += this.Frm_FuncBtnEnableChanged;
                            tsLabelKi.Enabled = false;
                            ChildForm = frm;
                        }
                    }
                }
                else
                {
                    Msg.Error("起動するプログラムが見つかりません。"
                        + Environment.NewLine + "実行環境をご確認ください。");
                }
            }
            catch (Exception ex)
            {

                Msg.Error("起動するプログラムが見つかりません。"
                    + Environment.NewLine + "実行環境をご確認ください。" + Environment.NewLine + ex.Message);
            }
        }

#endregion

#region 機能メニューITEM選択時の処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： 機能メニューITEM選択時の処理</para>
        /// <para>■ 機能概要： 機能メニューITEM選択時の処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void ExecChoiceItem()
        {

            foreach (ListViewItem items in lstMenu.Items)
            {

                items.ImageIndex = 0;

                string[] s = items.SubItems[1].ToString().Split(':');

                if (s.Length < 3) return;

                if (s[2].IndexOf("R") > 0)
                {
                    items.ImageIndex = 1;
                }

            }

            if (0 < lstMenu.SelectedIndices.Count)
            {
                string[] s = lstMenu.SelectedItems[0].SubItems[1].ToString().Split(':');

                if (s.Length < 3) return;

                lstMenu.SelectedItems[0].ImageIndex = 2;
                if (s[2].ToString().IndexOf("R")>0)
                {
                    lstMenu.SelectedItems[0].ImageIndex = 3;
                }
            }

        }
#endregion

#region ファンクションボタンクリック処理
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： ファンクションボタンクリック処理</para>
        /// <para>■ 機能概要： ファンクションボタンクリック処理。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void ExecFuncKeys(object sender)
        {

            // メニューファンクションボタンを押下すると現在開いている
            switch (((ToolStripButton)sender).Name)
            {
                case "tsBtnEsc":
                    
                    if (execForms != null)
                    {
                        execForms.PressEsc();
                        PressEsc();
                    }
                    else
                    {
                        PressEsc();
                    }

					this.lstMenu.Focus();

                    break;

                case "tsBtnF01":

                    if (execForms != null)
                    {
                        execForms.PressF1();
                    }

                    break;
                case "tsBtnF02":

                    if (execForms != null)
                    {
                        execForms.PressF2();
                    }

                    break;
                case "tsBtnF03":

                    if (execForms != null)
                    {
                        execForms.PressF3();
                    }

                    break;
                case "tsBtnF04":

                    if (execForms != null)
                    {
                        execForms.PressF4();
                    }

                    break;
                case "tsBtnF05":

                    if (execForms != null)
                    {
                        execForms.PressF5();
                    }

                    break;
                case "tsBtnF06":

                    if (execForms != null)
                    {
                        execForms.PressF6();
                    }

                    break;
                case "tsBtnF07":

                    if (execForms != null)
                    {
                        execForms.PressF7();
                    }

                    break;
                case "tsBtnF08":

                    if (execForms != null)
                    {
                        execForms.PressF8();
                    }

                    break;
                case "tsBtnF09":

                    if (execForms != null)
                    {
                        execForms.PressF9();
                    }

                    break;
                case "tsBtnF10":

                    if (execForms != null)
                    {
                        execForms.PressF10();
                    }

                    break;
                case "tsBtnF11":

                    if (execForms != null)
                    {
                        execForms.PressF11();
                    }

                    break;
                case "tsBtnF12":

                    if (execForms != null)
                    {
                        execForms.PressF12();
                    }

                    break;


            }

        }

#endregion

#region コントロールの色合いチェック
        private void AllCtlCheck(Control ctl)
        {
            foreach (Control ctrl in ctl.Controls)
            {

                if (ctrl is Panel)
                {
                    AllCtlCheck(ctl);
                }
                if (ctrl is GroupBox)
                {
                    AllCtlCheck(ctl);
                }

                if (ctrl is Label)
                {
                    ctrl.BackColor = System.Drawing.Color.Green;
                    ctrl.ForeColor = System.Drawing.Color.Black;
                }

            }
        }
#endregion

#region 子画面からのファンクションボタン制御受け取り
        /// <summary>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// <para>■ 機能説明： 子画面からのファンクションボタン制御受け取り</para>
        /// <para>■ 機能概要： 子画面からのファンクションボタン制御受け取り。</para>
        /// <para>■</para>
        /// <para>■ 作成者：   FSI</para>
        /// <para>■ 作成日：   2019-11</para>
        /// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
        /// </summary>
        private void Frm_FuncBtnEnableChanged(object sender, string btnName)
        {


            switch (btnName)
            {
                case "ESC":
                    tsBtnEsc.Enabled = ((Button)sender).Enabled;
                    break;
                case "F1":
                    tsBtnF01.Enabled = ((Button)sender).Enabled;

                    base.isBtnF1Enable = ((Button)sender).Enabled;
                    break;
                case "F2":
                    tsBtnF02.Enabled = ((Button)sender).Enabled;
                    base.isBtnF2Enable = ((Button)sender).Enabled;
                    break;
                case "F3":
                    tsBtnF03.Enabled = ((Button)sender).Enabled;
                    base.isBtnF3Enable = ((Button)sender).Enabled;
                    break;
                case "F4":
                    tsBtnF04.Enabled = ((Button)sender).Enabled;
                    base.isBtnF4Enable = ((Button)sender).Enabled;
                    break;
                case "F5":
                    tsBtnF05.Enabled = ((Button)sender).Enabled;
                    base.isBtnF5Enable = ((Button)sender).Enabled;
                    break;
                case "F6":
                    tsBtnF06.Enabled = ((Button)sender).Enabled;
                    base.isBtnF6Enable = ((Button)sender).Enabled;
                    break;
                case "F7":
                    tsBtnF07.Enabled = ((Button)sender).Enabled;
                    base.isBtnF7Enable = ((Button)sender).Enabled;
                    break;
                case "F8":
                    tsBtnF08.Enabled = ((Button)sender).Enabled;
                    base.isBtnF8Enable = ((Button)sender).Enabled;
                    break;
                case "F9":
                    tsBtnF09.Enabled = ((Button)sender).Enabled;
                    base.isBtnF9Enable = ((Button)sender).Enabled;
                    break;
                case "F10":
                    tsBtnF10.Enabled = ((Button)sender).Enabled;
                    base.isBtnF10Enable = ((Button)sender).Enabled;
                    break;
                case "F11":
                    tsBtnF11.Enabled = ((Button)sender).Enabled;
                    base.isBtnF11Enable = ((Button)sender).Enabled;
                    break;
                case "F12":
                    tsBtnF12.Enabled = ((Button)sender).Enabled;
                    base.isBtnF12Enable = ((Button)sender).Enabled;
                    break;

            }

        }

		#endregion

		#endregion

		private string GetVersion()
		{
			if (!ApplicationDeployment.IsNetworkDeployed) return String.Empty;

			var version = ApplicationDeployment.CurrentDeployment.CurrentVersion;
			return (
			version.Major.ToString() + "." +
			version.Minor.ToString() + "." +
			version.Build.ToString() + "." +
			version.Revision.ToString()
　　　　　　);
		}

		#region リストビューエンターキー
		private void lstMenu_KeyDown(object sender, KeyEventArgs e)
		{

			if (e.KeyCode == Keys.Left)
			{

				if (lstMenu.SelectedItems == null)
					return;

				// 左→キーを押された場合
				if (lstMenu.SelectedItems[0].Index == 0)
				{

					this.treeMenu.Focus();

				}

			}
			else if (e.KeyCode == Keys.Enter)
			{
				if (execForms != null)
				{
					execForms.Close();
					execForms.FuncBtnEnableChanged -= Frm_FuncBtnEnableChanged;
					execForms = null;

				}
				ExecListmenu();
			}

		}
		#endregion

		#region TreeView キーダウン操作
		private void treeMenu_KeyDown(object sender, KeyEventArgs e)
		{

			try
			{
				if (e.KeyCode == Keys.Right)
				{
					if (treeMenu.SelectedNode == null)
						return;

					if (treeMenu.SelectedNode.Level == 2)
					{
						this.lstMenu.Focus();

						if (this.lstMenu.Items.Count > 0)
						{
							this.lstMenu.Items[0].Selected = true;
						}
					}
				}

			}
			catch (Exception ex)
			{

				Msg.Error("矢印キー操作時のエラー" + Environment.NewLine + ex.Message);

			}
		}
		#endregion

	}
}
