using System.Drawing;
using System.Windows.Forms;

namespace systembase.table
{
	public interface IFieldProvider
	{
		string Caption
		{
			get;
			set;
		}

		int TabOrder
		{
			get;
			set;
		}

		string Clipboard
		{
			get;
			set;
		}

		void FieldInitialize(UTable.CField field);

		void EditorInitialize(UTable.CField field, IEditor editor);

		void Render(Graphics g, UTable.CField field, Rectangle rect, bool alter);

		void SetBorder(UTable.CField field, UTable.CBorder border, bool merged);

		void ToggleValue(UTable.CField field, object value);

		UTable.CSetting Setting();

		UTable.CField CreateField();

		IEditor CreateEditor();

		bool Focusable();

		bool UndoEnabled();

		ImeMode ImeMode();

		Size GetAdjustSize(Graphics g, UTable.CField field);

		object ValueRegularize(object value, IEditor editor);

		UTable.CFieldModify CreateModifyField(UTable.CField field);

		bool Draggable();
	}
}
