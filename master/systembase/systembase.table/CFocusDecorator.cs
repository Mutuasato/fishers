using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace systembase.table
{
	public class CFocusDecorator : IFieldDecorator
	{
		[DebuggerNonUserCode]
		public CFocusDecorator()
		{
		}

		public void RenderBackground(Graphics g, UTable.CField field, Rectangle rect)
		{
		}

		void IFieldDecorator.RenderBackground(Graphics g, UTable.CField field, Rectangle rect)
		{
			//ILSpy generated this explicit interface implementation from .override directive in RenderBackground
			this.RenderBackground(g, field, rect);
		}

		public void RenderForeground(Graphics g, UTable.CField field, Rectangle rect)
		{
			Rectangle rectangle = checked(new Rectangle(rect.Left + 1, rect.Top + 1, rect.Width - 1, rect.Height - 1));
			ControlPaint.DrawFocusRectangle(g, rectangle);
		}

		void IFieldDecorator.RenderForeground(Graphics g, UTable.CField field, Rectangle rect)
		{
			//ILSpy generated this explicit interface implementation from .override directive in RenderForeground
			this.RenderForeground(g, field, rect);
		}
	}
}
