using System.Collections.Generic;

namespace systembase.table
{
	public class CLayoutBuilder
	{
		public enum EOrientation
		{
			COL,
			ROW
		}

		public class CLayer
		{
			public EOrientation Orientation;

			public int Row;

			public int Col;

			public CLayer(EOrientation orientation)
			{
				Row = 0;
				Col = 0;
				Orientation = orientation;
			}

			public CLayer(EOrientation orientation, CLayer layer)
				: this(orientation)
			{
				Row = layer.Row;
				Col = layer.Col;
			}
		}

		private Stack<CLayer> layers;

		public CLayoutBuilder()
			: this(EOrientation.COL)
		{
		}

		public CLayoutBuilder(EOrientation orientation)
		{
			layers = new Stack<CLayer>();
			layers.Push(new CLayer(orientation));
		}

		public CLayoutBuilder Ascend()
		{
			return Ascend(EOrientation.COL);
		}

		public CLayoutBuilder Ascend(EOrientation orientation)
		{
			layers.Push(new CLayer(orientation, layers.Peek()));
			return this;
		}

		public CLayoutBuilder Descend()
		{
			CLayer cLayer = layers.Pop();
			CLayer cLayer2 = layers.Peek();
			checked
			{
				switch (cLayer2.Orientation)
				{
				case EOrientation.COL:
					cLayer2.Col = cLayer.Col + 1;
					break;
				case EOrientation.ROW:
					cLayer2.Row++;
					break;
				}
				cLayer2 = null;
				return this;
			}
		}

		public CLayoutBuilder Set(int row, int col)
		{
			CLayer cLayer = layers.Peek();
			cLayer.Row = row;
			cLayer.Col = col;
			cLayer = null;
			return this;
		}

		public UTable.CGrid.CRegion Next()
		{
			return Next(1, 1);
		}

		public UTable.CGrid.CRegion Next(int rows, int cols)
		{
			CLayer cLayer = layers.Peek();
			UTable.CGrid.CRegion result = new UTable.CGrid.CRegion(cLayer.Row, cLayer.Col, rows, cols);
			switch (cLayer.Orientation)
			{
			case EOrientation.COL:
				Skip(cols);
				break;
			case EOrientation.ROW:
				Skip(rows);
				break;
			}
			cLayer = null;
			return result;
		}

		public CLayoutBuilder Skip()
		{
			return Skip(1);
		}

		public CLayoutBuilder Skip(int step)
		{
			CLayer cLayer = layers.Peek();
			checked
			{
				switch (cLayer.Orientation)
				{
				case EOrientation.COL:
				{
					CLayer cLayer2 = cLayer;
					cLayer2.Col += step;
					break;
				}
				case EOrientation.ROW:
				{
					CLayer cLayer2 = cLayer;
					cLayer2.Row += step;
					break;
				}
				}
				cLayer = null;
				return this;
			}
		}

		public CLayoutBuilder Break()
		{
			return Break(1);
		}

		public CLayoutBuilder Break(int step)
		{
			CLayer cLayer = layers.Peek();
			checked
			{
				switch (cLayer.Orientation)
				{
				case EOrientation.COL:
				{
					cLayer.Col = 0;
					CLayer cLayer2 = cLayer;
					cLayer2.Row += step;
					break;
				}
				case EOrientation.ROW:
				{
					cLayer.Row = 0;
					CLayer cLayer2 = cLayer;
					cLayer2.Col += step;
					break;
				}
				}
				cLayer = null;
				return this;
			}
		}
	}
}
