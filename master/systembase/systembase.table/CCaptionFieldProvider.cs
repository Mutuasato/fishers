using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace systembase.table
{
	public class CCaptionFieldProvider : CFieldProvider
	{
		public class CCaptionField : UTable.CField
		{
			[DebuggerNonUserCode]
			public CCaptionField()
			{
				base.MouseDown += CCaptionField_MouseDown;
				base.DoubleClick += CCaptionField_DoubleClick;
			}

			public override void Clear()
			{
			}

			private void CCaptionField_DoubleClick(UTable.CField field, Point location, MouseEventArgs e)
			{
				if (TopLevelContent() == Table().HeaderContent)
				{
					UTable.EAllow userSortable = Table().Setting.UserSortable;
					CCaptionFieldProvider cCaptionFieldProvider = (CCaptionFieldProvider)Desc.Provider;
					if (cCaptionFieldProvider.UserSortable != 0)
					{
						userSortable = cCaptionFieldProvider.UserSortable;
					}
					cCaptionFieldProvider = null;
					if (userSortable == UTable.EAllow.ALLOW)
					{
						using (Table().RenderBlock())
						{
							UTable.CSortState.EOrder order = UTable.CSortState.EOrder.ASCEND;
							if ((Table().SortState.Field == this) & (Table().SortState.Order == UTable.CSortState.EOrder.ASCEND))
							{
								order = UTable.CSortState.EOrder.DESCEND;
							}
							object objectValue = RuntimeHelpers.GetObjectValue(Key);
							if (objectValue is UTable.CCaptionKey)
							{
								objectValue = RuntimeHelpers.GetObjectValue(((UTable.CCaptionKey)objectValue).key);
							}
							Table().Content.Sort(RuntimeHelpers.GetObjectValue(objectValue), order);
							UTable.CSortState sortState = Table().SortState;
							sortState.Field = this;
							sortState.Order = order;
							sortState = null;
						}
					}
				}
			}

			private void CCaptionField_MouseDown(UTable.CField field, Point location, MouseEventArgs e)
			{
				using (Table().RenderBlock())
				{
					if (TopLevelContent() == Table().Content)
					{
						if (Table().Setting.RecordSelectableByCaptionClick && Table().Setting.UserRangeSelectable)
						{
							UTable.CField cField = Record.DefaultField();
							if (cField != null)
							{
								Table().FocusField = cField;
								Table().SelectRange = (UTable.CRange)Table().CreateRange(Record);
							}
						}
						else if (Table().Setting.DefaultFieldSelectableByCaptionClick)
						{
							UTable.CField cField2 = Record.DefaultField();
							if (cField2 != null)
							{
								Table().FocusField = cField2;
							}
						}
					}
					else if (TopLevelContent() == Table().HeaderContent && Table().Setting.UserRangeSelectable)
					{
						UTable.CRecord topRecord = Table().Content.LayoutCache.TopRecord;
						if (topRecord != null)
						{
							object objectValue = RuntimeHelpers.GetObjectValue(Key);
							if (objectValue is UTable.CCaptionKey)
							{
								objectValue = RuntimeHelpers.GetObjectValue(((UTable.CCaptionKey)objectValue).key);
							}
							if (topRecord.Fields.ContainsKey(RuntimeHelpers.GetObjectValue(objectValue)))
							{
								UTable.CField cField3 = topRecord.Fields[RuntimeHelpers.GetObjectValue(objectValue)];
								int num = Table().LayoutCache.SelectableColsRev[LeftCol().Index()];
								if (num >= 0)
								{
									if (Table().Setting.ColSelectableByCaptionClick && cField3.Focusable())
									{
										Table().FocusField = cField3;
										Table().SelectRange = (UTable.CRange)Table().CreateRange(Table().Content, num);
									}
								}
								else if (Table().Setting.AllSelectableByCaptionClick)
								{
									Table().SelectRange = (UTable.CRange)Table().CreateRange(Table().Content);
								}
							}
						}
					}
				}
			}
		}

		private UTable.CSetting _Setting;

		private bool _Draggable;

		public UTable.EAllow UserSortable;

		public CCaptionFieldProvider()
			: this(null, draggable: false)
		{
		}

		public CCaptionFieldProvider(string caption)
			: this(caption, draggable: false)
		{
		}

		public CCaptionFieldProvider(string caption, bool draggable)
			: base(caption)
		{
			_Setting = null;
			UserSortable = UTable.EAllow.DEFAULT;
			_Draggable = draggable;
		}

		internal CCaptionFieldProvider(UTable.CFieldDesc fieldDesc)
			: this(fieldDesc.Provider.Caption)
		{
			UserSortable = fieldDesc.UserSortable;
			if (fieldDesc.Provider is CFieldProvider)
			{
				BorderLine = ((CFieldProvider)fieldDesc.Provider).BorderLine;
			}
			if (fieldDesc.HasSetting())
			{
				_Setting = (UTable.CSetting)fieldDesc.Setting.Clone();
			}
		}

		public override UTable.CField CreateField()
		{
			return new CCaptionField();
		}

		public override UTable.CSetting Setting()
		{
			return _Setting;
		}

		public override void FieldInitialize(UTable.CField field)
		{
			field.Value = Caption;
		}

		public override IEditor CreateEditor()
		{
			return null;
		}

		protected override void renderBackground(Graphics g, UTable.CField field, UTable.CDynamicSetting s, Rectangle rect, bool alter)
		{
			bool drag = field.Record == field.Table().DraggingRecord;
			CFieldProvider.RenderCaptionBackgroudRect(g, rect, CFieldProvider.CaptionBackColor(field, s, drag), s.CaptionStyle());
		}

		protected override void renderForeground(Graphics g, UTable.CField field, UTable.CDynamicSetting s, Rectangle rect, bool alter)
		{
			bool drag = field.Record == field.Table().DraggingRecord;
			CFieldProvider.RenderCaptionValue(g, field, field.Table().SortState, CFieldProvider.CreateValueRect(rect, s, field.TopLevelContent().RenderingRect), formatValue(RuntimeHelpers.GetObjectValue(field.Value)), CFieldProvider.CaptionForeColor(field, s, drag), s.CaptionFont(), GetBoundedStringFormat(g, field, s, rect));
		}

		public override void SetBorder(UTable.CField field, UTable.CBorder border, bool merged)
		{
			CFieldProvider.SetCaptionBorder(field, border, merged, BorderLine);
		}

		public override bool Focusable()
		{
			return false;
		}

		public override bool UndoEnabled()
		{
			return false;
		}

		public override Size GetAdjustSize(Graphics g, UTable.CField field)
		{
			UTable.CDynamicSetting cDynamicSetting = field.DynamicSetting();
			return g.MeasureString(formatValue(RuntimeHelpers.GetObjectValue(field.Value)), cDynamicSetting.CaptionFont(), 100000, cDynamicSetting.GetStringFormat()).ToSize();
		}

		public override bool Draggable()
		{
			return _Draggable;
		}
	}
}
