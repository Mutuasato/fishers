using Microsoft.VisualBasic.CompilerServices;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace systembase.table
{
	public class CKeyboardOperation
	{
		public enum EBehavior
		{
			NONE,
			NEXT_FIELD,
			NEXT_EDITABLE_FIELD,
			NEXT_RECORD,
			NEXT_CONTROL
		}

		public EBehavior EnterBehavior;

		public EBehavior TabBehavior;

		public Keys StartEditKey;

		public bool LeaveFromLastField;

		public bool ClearByDelete;

		public bool ClipboardPaste;

		public bool ClipboardPasteAddRecord;

		public bool ClipboardCopy;

		public bool ClipboardCut;

		public bool Undo;

		public bool Redo;

		public bool ClipboardEnabled
		{
			set
			{
				ClipboardCopy = value;
				ClipboardCut = value;
				ClipboardPaste = value;
			}
		}

		public bool UndoRedoEnabled
		{
			set
			{
				Undo = value;
				Redo = value;
			}
		}

		public CKeyboardOperation()
		{
			EnterBehavior = EBehavior.NEXT_RECORD;
			TabBehavior = EBehavior.NEXT_FIELD;
			StartEditKey = Keys.F2;
			LeaveFromLastField = true;
			ClearByDelete = true;
			ClipboardPaste = true;
			ClipboardPasteAddRecord = false;
			ClipboardCopy = true;
			ClipboardCut = true;
			Undo = true;
			Redo = true;
		}

		public virtual bool IsInputKey(Keys key, UTable table, bool defaultValue)
		{
			switch (key)
			{
			case Keys.LButton | Keys.MButton | Keys.Space | Keys.Shift:
			case Keys.RButton | Keys.MButton | Keys.Space | Keys.Shift:
			case Keys.LButton | Keys.RButton | Keys.MButton | Keys.Space | Keys.Shift:
			case Keys.Back | Keys.Space | Keys.Shift:
				if (table.SelectRange != null)
				{
					return false;
				}
				break;
			}
			return defaultValue;
		}

		public virtual bool ProcessDialogKey(Keys key, UTable table)
		{
			if (processFieldSelected(key, table))
			{
				return true;
			}
			if (processStartEdit(key, table))
			{
				return true;
			}
			return processKey(key, table);
		}

		public virtual void LeaveEdit(string direction, UTable table)
		{
			if (Operators.CompareString(direction, "UP", TextCompare: false) == 0)
			{
				prevRow(table);
			}
			else if (Operators.CompareString(direction, "DOWN", TextCompare: false) == 0)
			{
				nextRow(table);
			}
			else if (Operators.CompareString(direction, "LEFT", TextCompare: false) == 0)
			{
				prevCol(table);
			}
			else if (Operators.CompareString(direction, "RIGHT", TextCompare: false) == 0)
			{
				nextCol(table);
			}
			else if (Operators.CompareString(direction, "ENTER", TextCompare: false) == 0)
			{
				tabEnter(table, EnterBehavior);
			}
			else if (Operators.CompareString(direction, "ENTER_PREV", TextCompare: false) == 0)
			{
				tabEnterPrev(table, EnterBehavior);
			}
			else if (Operators.CompareString(direction, "TAB", TextCompare: false) == 0)
			{
				tabEnter(table, TabBehavior);
			}
			else if (Operators.CompareString(direction, "TAB_PREV", TextCompare: false) == 0)
			{
				tabEnterPrev(table, TabBehavior);
			}
		}

		protected virtual bool processFieldSelected(Keys key, UTable table)
		{
			bool handled = false;
			if (key.Equals(Keys.Return) & (table.FocusField != null))
			{
				table.RaiseFieldSelected(table.FocusField, ref handled);
			}
			return handled;
		}

		protected virtual bool processStartEdit(Keys key, UTable table)
		{
			if (key.Equals(StartEditKey) && table.StartEdit() != null)
			{
				return true;
			}
			return false;
		}

		protected virtual bool processKey(Keys key, UTable table)
		{
			switch (key)
			{
			case Keys.Left:
			case Keys.Up:
			case Keys.Right:
			case Keys.Down:
				if (table.FocusField == null)
				{
					table.FocusField = table.Content.DefaultField();
				}
				else
				{
					switch (key)
					{
					case Keys.Up:
						prevRow(table);
						break;
					case Keys.Down:
						nextRow(table);
						break;
					case Keys.Left:
						prevCol(table);
						break;
					case Keys.Right:
						nextCol(table);
						break;
					}
				}
				return true;
			case Keys.End:
			case Keys.Home:
				if (table.FocusField != null)
				{
					switch (key)
					{
					case Keys.Home:
						startCol(table);
						break;
					case Keys.End:
						endCol(table);
						break;
					}
				}
				return true;
			case Keys.Next:
				pageDown(table);
				return true;
			case Keys.Prior:
				pageUp(table);
				return true;
			case Keys.MButton | Keys.Space | Keys.Control:
				startContent(table);
				return true;
			case Keys.LButton | Keys.RButton | Keys.Space | Keys.Control:
				endContent(table);
				return true;
			case Keys.Return:
				tabEnter(table, EnterBehavior);
				return true;
			case Keys.LButton | Keys.MButton | Keys.Back | Keys.Shift:
				tabEnterPrev(table, EnterBehavior);
				return true;
			case Keys.Tab:
				tabEnter(table, TabBehavior);
				return true;
			case Keys.LButton | Keys.Back | Keys.Shift:
				tabEnterPrev(table, TabBehavior);
				return true;
			case Keys.Delete:
				if (ClearByDelete)
				{
					if (table.SelectRange != null)
					{
						table.SelectRange.FieldsClear();
					}
					else if (table.FocusField != null && table.FocusField.Editable().Equals(UTable.EAllow.ALLOW))
					{
						table.FocusField.Clear();
					}
					return true;
				}
				break;
			case Keys.LButton | Keys.Space | Keys.Shift:
			case Keys.RButton | Keys.Space | Keys.Shift:
			case Keys.LButton | Keys.RButton | Keys.Space | Keys.Shift:
			case Keys.MButton | Keys.Space | Keys.Shift:
			case Keys.LButton | Keys.MButton | Keys.Space | Keys.Shift:
			case Keys.RButton | Keys.MButton | Keys.Space | Keys.Shift:
			case Keys.LButton | Keys.RButton | Keys.MButton | Keys.Space | Keys.Shift:
			case Keys.Back | Keys.Space | Keys.Shift:
			case (Keys)131137:
			case Keys.LButton | Keys.RButton | Keys.Space | Keys.Shift | Keys.Control:
			case Keys.MButton | Keys.Space | Keys.Shift | Keys.Control:
				if (table.FocusField != null && table.Setting.UserRangeSelectable)
				{
					switch (key)
					{
					case Keys.RButton | Keys.MButton | Keys.Space | Keys.Shift:
						rangeUp(table);
						return true;
					case Keys.Back | Keys.Space | Keys.Shift:
						rangeDown(table);
						return true;
					case Keys.LButton | Keys.MButton | Keys.Space | Keys.Shift:
						rangeLeft(table);
						return true;
					case Keys.LButton | Keys.RButton | Keys.MButton | Keys.Space | Keys.Shift:
						rangeRight(table);
						return true;
					case Keys.MButton | Keys.Space | Keys.Shift:
						rangeStartCol(table);
						return true;
					case Keys.LButton | Keys.RButton | Keys.Space | Keys.Shift:
						rangeEndCol(table);
						return true;
					case Keys.LButton | Keys.Space | Keys.Shift:
						rangePageUp(table);
						return true;
					case Keys.RButton | Keys.Space | Keys.Shift:
						rangePageDown(table);
						return true;
					case (Keys)131137:
						rangeAll(table);
						return true;
					}
				}
				break;
			case (Keys)131158:
				if (ClipboardPaste)
				{
					table.ClipboardPaste(ClipboardPasteAddRecord);
					return true;
				}
				break;
			case (Keys)131139:
				if (ClipboardCopy)
				{
					table.ClipboardCopy();
					return true;
				}
				break;
			case (Keys)131160:
				if (ClipboardCut)
				{
					table.ClipboardCopy();
					if (table.SelectRange != null)
					{
						table.SelectRange.FieldsClear();
					}
					else if (table.FocusField != null && table.FocusField.Editable() == UTable.EAllow.ALLOW)
					{
						table.FocusField.Clear();
					}
					return true;
				}
				break;
			case (Keys)131162:
				if (Undo)
				{
					table.Undo();
					return true;
				}
				break;
			case (Keys)131161:
				if (Redo)
				{
					table.Redo();
					return true;
				}
				break;
			}
			return false;
		}

		protected virtual void nextRow(UTable table)
		{
			table.FocusGrid.Row = table.FocusField.Desc.Layout.BottomRow();
			UTable.CField cField = nextRowField(table.FocusGrid, table.FocusRecord);
			if (cField != null)
			{
				table.MoveFocusField(cField);
			}
		}

		protected virtual void prevRow(UTable table)
		{
			table.FocusGrid.Row = table.FocusField.Desc.Layout.Row;
			UTable.CField cField = prevRowField(table.FocusGrid, table.FocusRecord);
			if (cField != null)
			{
				table.MoveFocusField(cField);
			}
		}

		protected virtual void nextCol(UTable table)
		{
			table.FocusGrid.Col = table.FocusField.Desc.Layout.RightCol();
			UTable.CField cField = nextColField(table.FocusGrid, table.FocusRecord);
			if (cField != null)
			{
				table.MoveFocusField(cField);
			}
		}

		protected virtual void prevCol(UTable table)
		{
			table.FocusGrid.Col = table.FocusField.Desc.Layout.Col;
			UTable.CField cField = prevColField(table.FocusGrid, table.FocusRecord);
			if (cField != null)
			{
				table.MoveFocusField(cField);
			}
		}

		protected virtual void startCol(UTable table)
		{
			table.FocusGrid.Col = table.FocusField.Desc.Layout.Col;
			UTable.CField cField = nextColField(new UTable.CGrid.CPoint(table.FocusGrid.Row, -1), table.FocusRecord);
			if (cField != null)
			{
				table.MoveFocusField(cField);
			}
		}

		protected virtual void endCol(UTable table)
		{
			table.FocusGrid.Col = table.FocusField.Desc.Layout.Col;
			UTable.CField cField = prevColField(new UTable.CGrid.CPoint(table.FocusGrid.Row, table.Cols.Count), table.FocusRecord);
			if (cField != null)
			{
				table.MoveFocusField(cField);
			}
		}

		protected virtual void pageDown(UTable table)
		{
			if (table.FocusField != null && table.FocusField.TopLevelContent() == table.Content)
			{
				table.SetVScrollValue(checked(table.VScrollBar.Value + table.VScrollBar.LargeChange));
			}
		}

		protected virtual void pageUp(UTable table)
		{
			if (table.FocusField != null && table.FocusField.TopLevelContent() == table.Content)
			{
				table.SetVScrollValue(checked(table.VScrollBar.Value - table.VScrollBar.LargeChange));
			}
		}

		protected virtual void startContent(UTable table)
		{
			UTable.CRecord topRecord = table.FocusField.TopLevelContent().LayoutCache.TopRecord;
			if (topRecord != null)
			{
				UTable.CField cField = topRecord.DefaultField();
				if (cField != null)
				{
					table.FocusField = cField;
				}
			}
		}

		protected virtual void endContent(UTable table)
		{
			UTable.CRecord lastRecord = table.FocusField.TopLevelContent().LayoutCache.LastRecord;
			if (lastRecord != null)
			{
				UTable.CField cField = lastRecord.LastField();
				if (cField != null)
				{
					table.FocusField = cField;
				}
			}
		}

		protected void tabEnter(UTable table, EBehavior behavior)
		{
			if (table.FocusField == null)
			{
				table.FocusField = table.Content.DefaultField();
			}
			if (table.FocusField == null)
			{
				doNextControl(table);
				return;
			}
			switch (behavior)
			{
			case EBehavior.NEXT_FIELD:
				doNextField(table, editableOnly: false);
				break;
			case EBehavior.NEXT_EDITABLE_FIELD:
				doNextField(table, editableOnly: true);
				break;
			case EBehavior.NEXT_RECORD:
				nextRecord(table);
				break;
			case EBehavior.NEXT_CONTROL:
				doNextControl(table);
				break;
			}
		}

		protected void tabEnterPrev(UTable table, EBehavior behavior)
		{
			if (table.FocusField == null)
			{
				table.FocusField = table.Content.DefaultField();
			}
			if (table.FocusField == null)
			{
				doPrevControl(table);
				return;
			}
			switch (behavior)
			{
			case EBehavior.NEXT_FIELD:
				doPrevField(table, editableOnly: false);
				break;
			case EBehavior.NEXT_EDITABLE_FIELD:
				doPrevField(table, editableOnly: true);
				break;
			case EBehavior.NEXT_RECORD:
				prevRecord(table);
				break;
			case EBehavior.NEXT_CONTROL:
				doPrevControl(table);
				break;
			}
		}

		protected void doNextField(UTable table, bool editableOnly)
		{
			bool handled = false;
			table.raiseFocusNextField(table.FocusField, forward: true, ref handled);
			if (!handled)
			{
				nextField(table, editableOnly);
			}
		}

		protected void doPrevField(UTable table, bool editableOnly)
		{
			bool handled = false;
			table.raiseFocusNextField(table.FocusField, forward: false, ref handled);
			if (!handled)
			{
				prevField(table, editableOnly);
			}
		}

		protected virtual void nextField(UTable table, bool editableOnly)
		{
			UTable.CField cField = (!editableOnly) ? table.FocusField.NextField() : table.FocusField.NextEditableField();
			if (cField == null)
			{
				if (table.FocusField.TopLevelContent() == table.HeaderContent)
				{
					if (table.Content.Enabled)
					{
						cField = table.Content.DefaultField();
					}
				}
				else if (table.FocusField.TopLevelContent() == table.Content && table.FooterContent.Enabled)
				{
					cField = table.FooterContent.DefaultField();
				}
			}
			if (cField != null)
			{
				table.FocusField = cField;
			}
			else if (LeaveFromLastField)
			{
				doNextControl(table);
			}
		}

		protected virtual void prevField(UTable table, bool editableOnly)
		{
			UTable.CField cField = (!editableOnly) ? table.FocusField.PrevField() : table.FocusField.PrevEditableField();
			if (cField == null)
			{
				if (table.FocusField.TopLevelContent() == table.Content)
				{
					if (table.HeaderContent.Enabled)
					{
						cField = table.HeaderContent.LastField();
					}
				}
				else if (table.FocusField.TopLevelContent() == table.FooterContent && table.Content.Enabled)
				{
					cField = table.Content.LastField();
				}
			}
			if (cField != null)
			{
				table.FocusField = cField;
			}
			else if (LeaveFromLastField)
			{
				doPrevControl(table);
			}
		}

		protected virtual void nextRecord(UTable table)
		{
			UTable.CRecord nextRecord = table.FocusRecord.LayoutCache.NextRecord;
			if (nextRecord != null)
			{
				if (nextRecord.Fields.ContainsKey(RuntimeHelpers.GetObjectValue(table.FocusField.Key)))
				{
					table.FocusField = nextRecord.Fields[RuntimeHelpers.GetObjectValue(table.FocusField.Key)];
				}
				else
				{
					table.FocusField = nextRecord.DefaultField();
				}
			}
		}

		protected virtual void prevRecord(UTable table)
		{
			UTable.CRecord prevRecord = table.FocusRecord.LayoutCache.PrevRecord;
			if (prevRecord != null)
			{
				if (prevRecord.Fields.ContainsKey(RuntimeHelpers.GetObjectValue(table.FocusField.Key)))
				{
					table.FocusField = prevRecord.Fields[RuntimeHelpers.GetObjectValue(table.FocusField.Key)];
				}
				else
				{
					table.FocusField = prevRecord.DefaultField();
				}
			}
		}

		protected void doNextControl(UTable table)
		{
			bool handled = false;
			if (table.FocusField != null)
			{
				table.raiseFocusNextControl(table.FocusField, forward: true, ref handled);
			}
			if (!handled)
			{
				nextControl(table);
			}
		}

		protected void doPrevControl(UTable table)
		{
			bool handled = false;
			if (table.FocusField != null)
			{
				table.raiseFocusNextControl(table.FocusField, forward: false, ref handled);
			}
			if (!handled)
			{
				prevControl(table);
			}
		}

		protected virtual void nextControl(UTable table)
		{
			if (table.TopLevelControl != null)
			{
				table.TopLevelControl.SelectNextControl(table, forward: true, tabStopOnly: true, nested: true, wrap: true);
			}
		}

		protected virtual void prevControl(UTable table)
		{
			if (table.TopLevelControl != null)
			{
				table.TopLevelControl.SelectNextControl(table, forward: false, tabStopOnly: true, nested: true, wrap: true);
			}
		}

		protected UTable.CField findFocusableField(UTable.CRecord record, UTable.CGrid.CPoint point)
		{
			if (((point.Row >= 0) & (point.Row < record.Rows.Count) & (point.Col >= 0) & (point.Col < record.Table().Cols.Count)) && (record.LayoutCache.Visible & record.Rows[point.Row].Visible & record.Table().Cols[point.Col].Visible))
			{
				UTable.CField cField = record.FindField(point);
				if (cField != null && cField.Focusable())
				{
					return cField;
				}
			}
			return null;
		}

		protected UTable.CField nextColField(UTable.CGrid.CPoint point, UTable.CRecord record)
		{
			checked
			{
				UTable.CGrid.CPoint cPoint = new UTable.CGrid.CPoint(point.Row, point.Col + 1);
				while (cPoint.Col < record.Table().Cols.Count)
				{
					UTable.CField cField = findFocusableField(record, cPoint);
					if (cField != null)
					{
						return cField;
					}
					UTable.CGrid.CPoint cPoint2 = cPoint;
					cPoint2.Col++;
				}
				return null;
			}
		}

		protected UTable.CField prevColField(UTable.CGrid.CPoint point, UTable.CRecord record)
		{
			checked
			{
				UTable.CGrid.CPoint cPoint = new UTable.CGrid.CPoint(point.Row, point.Col - 1);
				while (cPoint.Col >= 0)
				{
					UTable.CField cField = findFocusableField(record, cPoint);
					if (cField != null)
					{
						return cField;
					}
					UTable.CGrid.CPoint cPoint2 = cPoint;
					cPoint2.Col--;
				}
				return null;
			}
		}

		protected UTable.CField nextRowField(UTable.CGrid.CPoint point, UTable.CRecord record)
		{
			checked
			{
				UTable.CGrid.CPoint cPoint = new UTable.CGrid.CPoint(point.Row + 1, point.Col);
				while (cPoint.Row < record.Rows.Count)
				{
					UTable.CField cField = findFocusableField(record, cPoint);
					if (cField != null)
					{
						return cField;
					}
					cField = nextColField(cPoint, record);
					if (cField != null)
					{
						return cField;
					}
					cField = prevColField(cPoint, record);
					if (cField != null)
					{
						return cField;
					}
					UTable.CGrid.CPoint cPoint2 = cPoint;
					cPoint2.Row++;
				}
				if (record.LayoutCache.NextRecord != null)
				{
					UTable.CRecord nextRecord = record.LayoutCache.NextRecord;
					return nextRowField(new UTable.CGrid.CPoint(-1, point.Col), nextRecord);
				}
				return null;
			}
		}

		protected UTable.CField prevRowField(UTable.CGrid.CPoint point, UTable.CRecord record)
		{
			checked
			{
				UTable.CGrid.CPoint cPoint = new UTable.CGrid.CPoint(point.Row - 1, point.Col);
				while (cPoint.Row >= 0)
				{
					UTable.CField cField = findFocusableField(record, cPoint);
					if (cField != null)
					{
						return cField;
					}
					cField = nextColField(cPoint, record);
					if (cField != null)
					{
						return cField;
					}
					cField = prevColField(cPoint, record);
					if (cField != null)
					{
						return cField;
					}
					UTable.CGrid.CPoint cPoint2 = cPoint;
					cPoint2.Row--;
				}
				if (record.LayoutCache.PrevRecord != null)
				{
					UTable.CRecord prevRecord = record.LayoutCache.PrevRecord;
					return prevRowField(new UTable.CGrid.CPoint(prevRecord.Rows.Count, point.Col), prevRecord);
				}
				return null;
			}
		}

		protected virtual void rangeUp(UTable table)
		{
			using (table.RenderBlock())
			{
				if (table.SelectRange == null)
				{
					table.SelectRange = (UTable.CRange)table.CreateRange(table.FocusField);
				}
				if (table.SelectRange != null)
				{
					int num = table.SelectRange.EndRow;
					if (num > 0)
					{
						num = checked(num - 1);
					}
					UTable.CRange selectRange = table.SelectRange;
					table.SelectRange = (UTable.CRange)table.CreateRange(selectRange.TopContent, selectRange.StartRow, selectRange.StartCol, num, selectRange.EndCol);
					table.SelectRange.ScrollToEnd();
					selectRange = null;
				}
			}
		}

		protected virtual void rangeDown(UTable table)
		{
			checked
			{
				using (table.RenderBlock())
				{
					if (table.SelectRange == null)
					{
						table.SelectRange = (UTable.CRange)table.CreateRange(table.FocusField);
					}
					if (table.SelectRange != null)
					{
						int num = table.SelectRange.EndRow;
						if (num < table.SelectRange.TopContent.LayoutCache.SelectableRows.Count - 1)
						{
							num++;
						}
						UTable.CRange selectRange = table.SelectRange;
						table.SelectRange = (UTable.CRange)table.CreateRange(selectRange.TopContent, selectRange.StartRow, selectRange.StartCol, num, selectRange.EndCol);
						table.SelectRange.ScrollToEnd();
						selectRange = null;
					}
				}
			}
		}

		protected virtual void rangeLeft(UTable table)
		{
			using (table.RenderBlock())
			{
				if (table.SelectRange == null)
				{
					table.SelectRange = (UTable.CRange)table.CreateRange(table.FocusField);
				}
				if (table.SelectRange != null)
				{
					int num = table.SelectRange.EndCol;
					if (num > 0)
					{
						num = checked(num - 1);
					}
					UTable.CRange selectRange = table.SelectRange;
					table.SelectRange = (UTable.CRange)table.CreateRange(selectRange.TopContent, selectRange.StartRow, selectRange.StartCol, selectRange.EndRow, num);
					table.SelectRange.ScrollToEnd();
					selectRange = null;
				}
			}
		}

		protected virtual void rangeRight(UTable table)
		{
			checked
			{
				using (table.RenderBlock())
				{
					if (table.SelectRange == null)
					{
						table.SelectRange = (UTable.CRange)table.CreateRange(table.FocusField);
					}
					if (table.SelectRange != null)
					{
						int num = table.SelectRange.EndCol;
						if (num < table.SelectRange.TopContent.Table.LayoutCache.SelectableCols.Count - 1)
						{
							num++;
						}
						UTable.CRange selectRange = table.SelectRange;
						table.SelectRange = (UTable.CRange)table.CreateRange(selectRange.TopContent, selectRange.StartRow, selectRange.StartCol, selectRange.EndRow, num);
						table.SelectRange.ScrollToEnd();
						selectRange = null;
					}
				}
			}
		}

		protected virtual void rangeStartCol(UTable table)
		{
			using (table.RenderBlock())
			{
				if (table.SelectRange == null)
				{
					table.SelectRange = (UTable.CRange)table.CreateRange(table.FocusField);
				}
				if (table.SelectRange != null)
				{
					int endCol = 0;
					UTable.CRange selectRange = table.SelectRange;
					table.SelectRange = (UTable.CRange)table.CreateRange(selectRange.TopContent, selectRange.StartRow, selectRange.StartCol, selectRange.EndRow, endCol);
					table.SelectRange.ScrollToEnd();
					selectRange = null;
				}
			}
		}

		protected virtual void rangeEndCol(UTable table)
		{
			using (table.RenderBlock())
			{
				if (table.SelectRange == null)
				{
					table.SelectRange = (UTable.CRange)table.CreateRange(table.FocusField);
				}
				if (table.SelectRange != null)
				{
					int endCol = checked(table.SelectRange.TopContent.Table.LayoutCache.SelectableCols.Count - 1);
					UTable.CRange selectRange = table.SelectRange;
					table.SelectRange = (UTable.CRange)table.CreateRange(selectRange.TopContent, selectRange.StartRow, selectRange.StartCol, selectRange.EndRow, endCol);
					table.SelectRange.ScrollToEnd();
					selectRange = null;
				}
			}
		}

		protected virtual void rangePageUp(UTable table)
		{
			checked
			{
				using (table.RenderBlock())
				{
					if (table.FocusField.TopLevelContent() == table.Content)
					{
						if (table.SelectRange == null)
						{
							table.SelectRange = (UTable.CRange)table.CreateRange(table.FocusField);
						}
						if (table.SelectRange != null)
						{
							int num = table.SelectRange.EndRect().Top - table.VScrollBar.LargeChange;
							int num2 = table.SelectRange.TopRow();
							while (num2 > 0 && num < table.SelectRange.TopContent.LayoutCache.SelectableRowsPos[num2])
							{
								num2--;
							}
							UTable.CRange selectRange = table.SelectRange;
							table.SelectRange = (UTable.CRange)table.CreateRange(selectRange.TopContent, selectRange.StartRow, selectRange.StartCol, num2, selectRange.EndCol);
							table.SelectRange.ScrollToEnd();
							selectRange = null;
						}
					}
				}
			}
		}

		protected virtual void rangePageDown(UTable table)
		{
			checked
			{
				using (table.RenderBlock())
				{
					if (table.FocusField.TopLevelContent() == table.Content)
					{
						if (table.SelectRange == null)
						{
							table.SelectRange = (UTable.CRange)table.CreateRange(table.FocusField);
						}
						if (table.SelectRange != null)
						{
							int num = table.SelectRange.EndRect().Bottom + table.VScrollBar.LargeChange;
							int i;
							for (i = table.SelectRange.TopRow(); i < table.SelectRange.TopContent.LayoutCache.SelectableRows.Count - 1 && num > table.SelectRange.TopContent.LayoutCache.SelectableRowsPos[i]; i++)
							{
							}
							UTable.CRange selectRange = table.SelectRange;
							table.SelectRange = (UTable.CRange)table.CreateRange(selectRange.TopContent, selectRange.StartRow, selectRange.StartCol, i, selectRange.EndCol);
							table.SelectRange.ScrollToEnd();
							selectRange = null;
						}
					}
				}
			}
		}

		protected virtual void rangeAll(UTable table)
		{
			using (table.RenderBlock())
			{
				UTable uTable = table;
				UTable.CContent topContent = table.FocusField.TopLevelContent();
				uTable.SelectRange = (UTable.CRange)uTable.CreateRange(topContent);
				uTable = null;
			}
		}
	}
}
