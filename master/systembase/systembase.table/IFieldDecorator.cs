using System.Drawing;

namespace systembase.table
{
	public interface IFieldDecorator
	{
		void RenderBackground(Graphics g, UTable.CField field, Rectangle rect);

		void RenderForeground(Graphics g, UTable.CField field, Rectangle rect);
	}
}
