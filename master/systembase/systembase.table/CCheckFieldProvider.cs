using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Drawing;

namespace systembase.table
{
	public class CCheckFieldProvider : CFieldProvider
	{
		public override string Clipboard
		{
			get
			{
				if (Conversions.ToBoolean(field.Value))
				{
					return "1";
				}
				return "0";
			}
			set
			{
				try
				{
					field.Value = Conversions.ToBoolean(value);
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					ProjectData.ClearProjectError();
				}
			}
		}

		public CCheckFieldProvider()
			: this(null)
		{
		}

		public CCheckFieldProvider(string caption)
			: base(caption)
		{
		}

		public override void FieldInitialize(UTable.CField field)
		{
			field.Value = false;
			SetToggle(field);
		}

		protected override void renderForeground(Graphics g, UTable.CField field, UTable.CDynamicSetting s, Rectangle rect, bool alter)
		{
			CFieldProvider.RenderCheckBox(g, rect, Conversions.ToBoolean(field.Value));
		}

		public override Size GetAdjustSize(Graphics g, UTable.CField field)
		{
			return checked(new Size(CFieldProvider.TOGGLERECT_SIZE + 8, CFieldProvider.TOGGLERECT_SIZE + 8));
		}
	}
}
