using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace systembase.table
{
	public class CNarrowTreeFieldProvider : CNarrowChildFieldProvider
	{
		public CNarrowTreeFieldProvider()
			: this(null)
		{
		}

		public CNarrowTreeFieldProvider(string caption)
			: base(caption)
		{
		}

		public override void SetBorder(UTable.CField field, UTable.CBorder border, bool merged)
		{
			int num = 12;
			CFieldProvider.SetFieldBorder(field, border, merged, BorderLine & num);
		}

		protected override void renderForeground(Graphics g, UTable.CField field, UTable.CDynamicSetting s, Rectangle rect, bool alter)
		{
			//Discarded unreachable code: IL_01a8
			int levelMax = getLevelMax(field);
			int num = field.Content().Level();
			checked
			{
				int num2 = (int)Math.Round((double)rect.Width / (double)levelMax);
				if (num < levelMax - 1)
				{
					CFieldProvider.RenderNarrowChild(g, getToggleRect(field, rect, 0), Conversions.ToBoolean(field.Value), (field.Record.Child != null && field.Record.Child.Records.Count > 0) ? true : false);
				}
				int num3 = levelMax - 2;
				for (int i = 0; i <= num3; i++)
				{
					if (i < num - 1)
					{
						if (!isLastRecord(field, i))
						{
							int num4 = rect.Left + num2 * (i + 1);
							using (Pen pen = new Pen(Color.Gray))
							{
								pen.DashStyle = DashStyle.Dot;
								g.DrawLine(pen, num4, rect.Top, num4, rect.Bottom);
							}
						}
					}
					else if (i == num - 1)
					{
						int num5 = rect.Left + num2 * (i + 1);
						int x = (int)Math.Round((double)(num5 + num2) - (double)CFieldProvider.TOGGLERECT_SIZE / 2.0);
						int num6 = (int)Math.Round((double)rect.Top + (double)rect.Height / 2.0);
						using (Pen pen2 = new Pen(Color.Gray))
						{
							pen2.DashStyle = DashStyle.Dot;
							g.DrawLine(pen2, num5, num6, x, num6);
							if (!isLastRecord(field, i))
							{
								g.DrawLine(pen2, num5, rect.Top, num5, rect.Bottom);
							}
							else
							{
								g.DrawLine(pen2, num5, rect.Top, num5, num6);
							}
						}
					}
					else if (i == num)
					{
						using (Pen pen3 = new Pen(Color.Gray))
						{
							pen3.DashStyle = DashStyle.Dot;
							object obj = null;
							int num7 = (int)Math.Round((double)rect.Top + (double)rect.Height / 2.0);
							int x2 = (int)Math.Round((double)(rect.Left + num2 * (i + 1)) + (double)CFieldProvider.TOGGLERECT_SIZE / 2.0);
							int x3 = rect.Right - 4;
							g.DrawLine(pen3, x2, num7, x3, num7);
							obj = null;
							if (Conversions.ToBoolean(field.Value))
							{
								int num8 = rect.Left + num2 * (i + 1);
								int y = (int)Math.Round((double)rect.Top + (double)rect.Height / 2.0 + (double)CFieldProvider.TOGGLERECT_SIZE / 2.0);
								int bottom = rect.Bottom;
								g.DrawLine(pen3, num8, y, num8, bottom);
							}
						}
					}
				}
			}
		}

		protected override Rectangle getToggleRect(UTable.CField field, Rectangle rect, int w)
		{
			int levelMax = getLevelMax(field);
			int num = field.Content().Level();
			checked
			{
				if (levelMax > num)
				{
					int num2 = (int)Math.Round((double)rect.Width / (double)levelMax * (double)(num + 1));
					int num3 = CFieldProvider.TOGGLERECT_SIZE + w;
					return new Rectangle((int)Math.Round((double)(rect.Left + num2) - (double)num3 / 2.0), (int)Math.Round((double)rect.Top + (double)rect.Height / 2.0 - (double)num3 / 2.0), num3, num3);
				}
				return new Rectangle(0, 0, 0, 0);
			}
		}

		private static int getLevelMax(UTable.CField field)
		{
			if (field.TopLevelContent().RecordProvider == null)
			{
				return 1;
			}
			int num = 0;
			for (UTable.CRecordProvider cRecordProvider = field.TopLevelContent().RecordProvider; cRecordProvider != null; cRecordProvider = cRecordProvider.ChildRecordProvider)
			{
				num = checked(num + 1);
			}
			return num;
		}

		private static bool isLastRecord(UTable.CField field, int l)
		{
			UTable.CRecord cRecord = field.Record;
			int num = cRecord.Content.Level();
			checked
			{
				while (l + 1 < num)
				{
					num--;
					cRecord = cRecord.Content.ParentRecord;
				}
				return cRecord.Index() == cRecord.Content.Records.Count - 1;
			}
		}

		public override Size GetAdjustSize(Graphics g, UTable.CField field)
		{
			int levelMax = getLevelMax(field);
			return checked(new Size(CFieldProvider.TOGGLERECT_SIZE * levelMax + 8, CFieldProvider.TOGGLERECT_SIZE + 8));
		}
	}
}
