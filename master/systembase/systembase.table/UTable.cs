using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

namespace systembase.table
{
	public class UTable : Control
	{
		public class CField
		{
			public delegate void ValidatingEventHandler(CField field, CancelEventArgs e);

			public delegate void ValidatedEventHandler(CField field);

			public delegate void EnteringEventHandler(CField field, ref bool cancel);

			public delegate void EnterEventHandler(CField field);

			public delegate void LeaveEventHandler(CField field);

			public delegate void MouseDownEventHandler(CField field, Point location, MouseEventArgs e);

			public delegate void MouseMoveEventHandler(CField field, Point location, MouseEventArgs e);

			public delegate void MouseUpEventHandler(CField field, Point location, MouseEventArgs e);

			public delegate void ClickEventHandler(CField field, Point location, MouseEventArgs e);

			public delegate void DoubleClickEventHandler(CField field, Point location, MouseEventArgs e);

			public delegate void KeyDownEventHandler(CField field, KeyEventArgs e);

			public delegate void KeyUpEventHandler(CField field, KeyEventArgs e);

			public delegate void KeyPressEventHandler(CField field, KeyPressEventArgs e);

			public delegate void ValueChangedEventHandler(CField field);

			public delegate void EditorValueChangedEventHandler(CField field);

			public delegate void EditStartingEventHandler(CField field, ref EAllow editable);

			public delegate void EditStartEventHandler(CField field, IEditor editor);

			public delegate void EditFinishedEventHandler(CField field);

			public delegate void ButtonClickEventHandler(CField field);

			public delegate void SelectedEventHandler(CField field, ref bool handled);

			public delegate void FocusNextFieldEventHandler(CField field, bool forward, ref bool handled);

			public delegate void FocusNextControlEventHandler(CField field, bool forward, ref bool handled);

			private class _CValueBuffer : IDisposable
			{
				public CField field;

				public object Value;

				public bool Cancel;

				private bool disposedValue;

				public _CValueBuffer(CField field, object value)
				{
					Cancel = false;
					disposedValue = false;
					this.field = field;
					Value = RuntimeHelpers.GetObjectValue(value);
				}

				protected virtual void Dispose(bool disposing)
				{
					if (!disposedValue)
					{
						field.valueBuffer = null;
						if (!Cancel)
						{
							field.ValueCommit(RuntimeHelpers.GetObjectValue(Value));
						}
					}
					disposedValue = true;
				}

				public void Dispose()
				{
					Dispose(disposing: true);
					GC.SuppressFinalize(this);
				}

				void IDisposable.Dispose()
				{
					//ILSpy generated this explicit interface implementation from .override directive in Dispose
					this.Dispose();
				}
			}

			public object Key;

			public CFieldDesc Desc;

			public CRecord Record;

			public IFieldDecorator Decorator;

			private ValidatingEventHandler ValidatingEvent;

			private ValidatedEventHandler ValidatedEvent;

			private EnteringEventHandler EnteringEvent;

			private EnterEventHandler EnterEvent;

			private LeaveEventHandler LeaveEvent;

			private MouseDownEventHandler MouseDownEvent;

			private MouseMoveEventHandler MouseMoveEvent;

			private MouseUpEventHandler MouseUpEvent;

			private ClickEventHandler ClickEvent;

			private DoubleClickEventHandler DoubleClickEvent;

			private KeyDownEventHandler KeyDownEvent;

			private KeyUpEventHandler KeyUpEvent;

			private KeyPressEventHandler KeyPressEvent;

			private ValueChangedEventHandler ValueChangedEvent;

			private EditorValueChangedEventHandler EditorValueChangedEvent;

			private EditStartingEventHandler EditStartingEvent;

			private EditStartEventHandler EditStartEvent;

			private EditFinishedEventHandler EditFinishedEvent;

			private ButtonClickEventHandler ButtonClickEvent;

			private SelectedEventHandler SelectedEvent;

			private FocusNextFieldEventHandler FocusNextFieldEvent;

			private FocusNextControlEventHandler FocusNextControlEvent;

			private object _Value;

			private CSetting _Setting;

			private _CValueBuffer valueBuffer;

			public CSetting Setting
			{
				get
				{
					if (_Setting == null)
					{
						_Setting = new CSetting();
					}
					return _Setting;
				}
				set
				{
					_Setting = value;
				}
			}

			public virtual object Value
			{
				get
				{
					if (Editor() != null)
					{
						return Editor().Value;
					}
					if (valueBuffer != null)
					{
						return valueBuffer.Value;
					}
					return rawValue;
				}
				set
				{
					using (Table().RenderBlock())
					{
						object objectValue = RuntimeHelpers.GetObjectValue(Desc.Provider.ValueRegularize(RuntimeHelpers.GetObjectValue(value), Editor()));
						if (Editor() != null)
						{
							Editor().Value = RuntimeHelpers.GetObjectValue(objectValue);
						}
						else if (valueBuffer != null)
						{
							valueBuffer.Value = RuntimeHelpers.GetObjectValue(objectValue);
						}
						else
						{
							ValueCommit(RuntimeHelpers.GetObjectValue(objectValue));
						}
					}
				}
			}

			protected virtual object rawValue
			{
				get
				{
					return _Value;
				}
				set
				{
					_Value = RuntimeHelpers.GetObjectValue(value);
				}
			}

			public event ValidatingEventHandler Validating
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					ValidatingEvent = (ValidatingEventHandler)Delegate.Combine(ValidatingEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					ValidatingEvent = (ValidatingEventHandler)Delegate.Remove(ValidatingEvent, value);
				}
			}

			public event ValidatedEventHandler Validated
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					ValidatedEvent = (ValidatedEventHandler)Delegate.Combine(ValidatedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					ValidatedEvent = (ValidatedEventHandler)Delegate.Remove(ValidatedEvent, value);
				}
			}

			public event EnteringEventHandler Entering
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					EnteringEvent = (EnteringEventHandler)Delegate.Combine(EnteringEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					EnteringEvent = (EnteringEventHandler)Delegate.Remove(EnteringEvent, value);
				}
			}

			public event EnterEventHandler Enter
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					EnterEvent = (EnterEventHandler)Delegate.Combine(EnterEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					EnterEvent = (EnterEventHandler)Delegate.Remove(EnterEvent, value);
				}
			}

			public event LeaveEventHandler Leave
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					LeaveEvent = (LeaveEventHandler)Delegate.Combine(LeaveEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					LeaveEvent = (LeaveEventHandler)Delegate.Remove(LeaveEvent, value);
				}
			}

			public event MouseDownEventHandler MouseDown
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					MouseDownEvent = (MouseDownEventHandler)Delegate.Combine(MouseDownEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					MouseDownEvent = (MouseDownEventHandler)Delegate.Remove(MouseDownEvent, value);
				}
			}

			public event MouseMoveEventHandler MouseMove
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					MouseMoveEvent = (MouseMoveEventHandler)Delegate.Combine(MouseMoveEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					MouseMoveEvent = (MouseMoveEventHandler)Delegate.Remove(MouseMoveEvent, value);
				}
			}

			public event MouseUpEventHandler MouseUp
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					MouseUpEvent = (MouseUpEventHandler)Delegate.Combine(MouseUpEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					MouseUpEvent = (MouseUpEventHandler)Delegate.Remove(MouseUpEvent, value);
				}
			}

			public event ClickEventHandler Click
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					ClickEvent = (ClickEventHandler)Delegate.Combine(ClickEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					ClickEvent = (ClickEventHandler)Delegate.Remove(ClickEvent, value);
				}
			}

			public event DoubleClickEventHandler DoubleClick
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					DoubleClickEvent = (DoubleClickEventHandler)Delegate.Combine(DoubleClickEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					DoubleClickEvent = (DoubleClickEventHandler)Delegate.Remove(DoubleClickEvent, value);
				}
			}

			public event KeyDownEventHandler KeyDown
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					KeyDownEvent = (KeyDownEventHandler)Delegate.Combine(KeyDownEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					KeyDownEvent = (KeyDownEventHandler)Delegate.Remove(KeyDownEvent, value);
				}
			}

			public event KeyUpEventHandler KeyUp
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					KeyUpEvent = (KeyUpEventHandler)Delegate.Combine(KeyUpEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					KeyUpEvent = (KeyUpEventHandler)Delegate.Remove(KeyUpEvent, value);
				}
			}

			public event KeyPressEventHandler KeyPress
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					KeyPressEvent = (KeyPressEventHandler)Delegate.Combine(KeyPressEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					KeyPressEvent = (KeyPressEventHandler)Delegate.Remove(KeyPressEvent, value);
				}
			}

			public event ValueChangedEventHandler ValueChanged
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					ValueChangedEvent = (ValueChangedEventHandler)Delegate.Combine(ValueChangedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					ValueChangedEvent = (ValueChangedEventHandler)Delegate.Remove(ValueChangedEvent, value);
				}
			}

			public event EditorValueChangedEventHandler EditorValueChanged
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					EditorValueChangedEvent = (EditorValueChangedEventHandler)Delegate.Combine(EditorValueChangedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					EditorValueChangedEvent = (EditorValueChangedEventHandler)Delegate.Remove(EditorValueChangedEvent, value);
				}
			}

			public event EditStartingEventHandler EditStarting
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					EditStartingEvent = (EditStartingEventHandler)Delegate.Combine(EditStartingEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					EditStartingEvent = (EditStartingEventHandler)Delegate.Remove(EditStartingEvent, value);
				}
			}

			public event EditStartEventHandler EditStart
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					EditStartEvent = (EditStartEventHandler)Delegate.Combine(EditStartEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					EditStartEvent = (EditStartEventHandler)Delegate.Remove(EditStartEvent, value);
				}
			}

			public event EditFinishedEventHandler EditFinished
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					EditFinishedEvent = (EditFinishedEventHandler)Delegate.Combine(EditFinishedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					EditFinishedEvent = (EditFinishedEventHandler)Delegate.Remove(EditFinishedEvent, value);
				}
			}

			public event ButtonClickEventHandler ButtonClick
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					ButtonClickEvent = (ButtonClickEventHandler)Delegate.Combine(ButtonClickEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					ButtonClickEvent = (ButtonClickEventHandler)Delegate.Remove(ButtonClickEvent, value);
				}
			}

			public event SelectedEventHandler Selected
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					SelectedEvent = (SelectedEventHandler)Delegate.Combine(SelectedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					SelectedEvent = (SelectedEventHandler)Delegate.Remove(SelectedEvent, value);
				}
			}

			public event FocusNextFieldEventHandler FocusNextField
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					FocusNextFieldEvent = (FocusNextFieldEventHandler)Delegate.Combine(FocusNextFieldEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					FocusNextFieldEvent = (FocusNextFieldEventHandler)Delegate.Remove(FocusNextFieldEvent, value);
				}
			}

			public event FocusNextControlEventHandler FocusNextControl
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					FocusNextControlEvent = (FocusNextControlEventHandler)Delegate.Combine(FocusNextControlEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					FocusNextControlEvent = (FocusNextControlEventHandler)Delegate.Remove(FocusNextControlEvent, value);
				}
			}

			public CField()
			{
				Decorator = null;
				_Setting = null;
				valueBuffer = null;
			}

			public CGrid.CSize GetAdjustSize()
			{
				CGrid.CRegion layout = Desc.Layout;
				return GetAdjustSize(checked(new CGrid.CPoint(layout.Row + layout.Rows - 1, layout.Col + layout.Cols - 1)));
			}

			public CGrid.CSize GetAdjustSize(CGrid.CPoint point)
			{
				if (Table().BackBuffer == null)
				{
					return new CGrid.CSize();
				}
				CFieldDesc desc = Desc;
				Size adjustSize = desc.Provider.GetAdjustSize(Table().BackBuffer.Graphics, this);
				return new CGrid.CSize(Record.Rows.GetSizeByEntireSize(desc.Layout.Row, desc.Layout.Rows, point.Row, adjustSize.Height), Table().Cols.GetSizeByEntireSize(desc.Layout.Col, desc.Layout.Cols, point.Col, adjustSize.Width));
			}

			private void extend_on_setValue()
			{
				if (!((Desc.Layout != null) & (Table().AutoExtend.Col | Table().AutoExtend.Row)))
				{
					return;
				}
				CGrid.CSize adjustSize = GetAdjustSize();
				if (Table().AutoExtend.Row)
				{
					CGrid cGrid = BottomRow();
					if (cGrid != null && cGrid.Size < adjustSize.Row)
					{
						cGrid.Size = adjustSize.Row;
						TopLevelContent().LayoutCacheInvalid = true;
					}
				}
				if (Table().AutoExtend.Col)
				{
					CGrid cGrid2 = RightCol();
					if (cGrid2 != null && cGrid2.Size < adjustSize.Col)
					{
						cGrid2.Size = adjustSize.Col;
						Table().LayoutCacheInvalid = true;
					}
				}
				adjustSize = null;
			}

			internal void Init(object key, CRecord record, CFieldDesc fieldDesc)
			{
				Key = RuntimeHelpers.GetObjectValue(key);
				Record = record;
				Desc = fieldDesc;
			}

			public bool HasSetting()
			{
				return _Setting != null;
			}

			public void ClearSetting()
			{
				Setting = null;
			}

			public void ValueCommit(object v)
			{
				ValueCommit(RuntimeHelpers.GetObjectValue(v), null);
			}

			internal void ValueCommit(object v, CFieldModify fieldModify)
			{
				CFieldModify cFieldModify = fieldModify;
				if (cFieldModify == null)
				{
					cFieldModify = Desc.Provider.CreateModifyField(this);
				}
				rawValue = RuntimeHelpers.GetObjectValue(v);
				if (cFieldModify.Modified(this))
				{
					Table().raiseFieldValueChanged(this);
					if (Table().UndoBufferEnabled && Desc.Provider.UndoEnabled())
					{
						cFieldModify.SetUp(this);
						Content().addModify(cFieldModify);
					}
					extend_on_setValue();
				}
			}

			public object CommittedValue()
			{
				return rawValue;
			}

			public CGrid LeftCol()
			{
				checked
				{
					int num = Desc.Layout.Cols - 1;
					for (int i = 0; i <= num; i++)
					{
						CGrid cGrid = Table().Cols[Desc.Layout.Col + i];
						if (cGrid.Visible)
						{
							return cGrid;
						}
					}
					return null;
				}
			}

			public CGrid RightCol()
			{
				checked
				{
					for (int i = Desc.Layout.Cols - 1; i >= 0; i += -1)
					{
						CGrid cGrid = Table().Cols[Desc.Layout.Col + i];
						if (cGrid.Visible)
						{
							return cGrid;
						}
					}
					return null;
				}
			}

			public CGrid TopRow()
			{
				checked
				{
					int num = Desc.Layout.Rows - 1;
					for (int i = 0; i <= num; i++)
					{
						CGrid cGrid = Record.Rows[Desc.Layout.Row + i];
						if (cGrid.Visible)
						{
							return cGrid;
						}
					}
					return null;
				}
			}

			public CGrid BottomRow()
			{
				checked
				{
					for (int i = Desc.Layout.Rows - 1; i >= 0; i += -1)
					{
						CGrid cGrid = Record.Rows[Desc.Layout.Row + i];
						if (cGrid.Visible)
						{
							return cGrid;
						}
					}
					return null;
				}
			}

			public CContent Content()
			{
				return Record.Content;
			}

			public UTable Table()
			{
				return Record.Table();
			}

			public CContent TopLevelContent()
			{
				return Record.TopLevelContent();
			}

			public CRecord TopLevelRecord()
			{
				return Record.TopLevelRecord();
			}

			public EAllow Editable()
			{
				EAllow editable = DynamicSetting().Editable();
				Table().raiseEditStarting(this, ref editable);
				return editable;
			}

			public void Visualize()
			{
				Record.Visualize();
			}

			public bool Focusable()
			{
				if (Desc.Layout == null || !Desc.Provider.Focusable())
				{
					return false;
				}
				CContent cContent = Content();
				while (true)
				{
					if (!cContent.Enabled)
					{
						return false;
					}
					if (cContent.ParentRecord == null)
					{
						break;
					}
					cContent = cContent.ParentRecord.Content;
				}
				for (CRecord cRecord = Record; cRecord != null; cRecord = cRecord.Content.ParentRecord)
				{
					if (!cRecord.Visible || !cRecord.Content.Visible)
					{
						return false;
					}
				}
				bool flag = false;
				checked
				{
					int num = Desc.Layout.Cols - 1;
					for (int i = 0; i <= num; i++)
					{
						if (Table().Cols[Desc.Layout.Col + i].Visible)
						{
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						return false;
					}
					int num2 = Desc.Layout.Rows - 1;
					for (int j = 0; j <= num2; j++)
					{
						if (Record.Rows[Desc.Layout.Row + j].Visible)
						{
							return true;
						}
					}
					bool result = default(bool);
					return result;
				}
			}

			public void Focus()
			{
				Table().FocusField = this;
			}

			public bool Focused()
			{
				return this == Table().FocusField;
			}

			public IEditor Editor()
			{
				if (Focused())
				{
					return Table().Editor();
				}
				return null;
			}

			public CDynamicSetting DynamicSetting()
			{
				return new CDynamicSetting(this);
			}

			internal void raiseValidating(CancelEventArgs e)
			{
				ValidatingEvent?.Invoke(this, e);
			}

			internal void raiseValidated()
			{
				ValidatedEvent?.Invoke(this);
			}

			internal void raiseValueChanged()
			{
				ValueChangedEvent?.Invoke(this);
			}

			internal void raiseEditorValueChanged()
			{
				EditorValueChangedEvent?.Invoke(this);
			}

			internal void raiseEntering(ref bool cancel)
			{
				EnteringEvent?.Invoke(this, ref cancel);
			}

			internal void raiseEnter()
			{
				EnterEvent?.Invoke(this);
			}

			internal void raiseLeave()
			{
				LeaveEvent?.Invoke(this);
			}

			internal void raiseEditStarting(ref EAllow editable)
			{
				EditStartingEvent?.Invoke(this, ref editable);
			}

			internal void raiseEditStart(IEditor editor)
			{
				EditStartEvent?.Invoke(this, editor);
			}

			internal void raiseEditFinished()
			{
				EditFinishedEvent?.Invoke(this);
			}

			internal void raiseMouseDown(Point location, MouseEventArgs e)
			{
				MouseDownEvent?.Invoke(this, location, e);
			}

			internal void raiseMouseUp(Point location, MouseEventArgs e)
			{
				MouseUpEvent?.Invoke(this, location, e);
			}

			internal void raiseMouseMove(Point location, MouseEventArgs e)
			{
				MouseMoveEvent?.Invoke(this, location, e);
			}

			internal void raiseClick(Point location, MouseEventArgs e)
			{
				ClickEvent?.Invoke(this, location, e);
			}

			internal void raiseDoubleClick(Point location, MouseEventArgs e)
			{
				DoubleClickEvent?.Invoke(this, location, e);
			}

			internal void raiseKeyDown(KeyEventArgs e)
			{
				KeyDownEvent?.Invoke(this, e);
			}

			internal void raiseKeyUp(KeyEventArgs e)
			{
				KeyUpEvent?.Invoke(this, e);
			}

			internal void raiseKeyPress(KeyPressEventArgs e)
			{
				KeyPressEvent?.Invoke(this, e);
			}

			internal void raiseButtonClick()
			{
				ButtonClickEvent?.Invoke(this);
			}

			internal void raiseSelected(ref bool handled)
			{
				SelectedEvent?.Invoke(this, ref handled);
			}

			internal void raiseFocusNextField(bool forward, ref bool handled)
			{
				FocusNextFieldEvent?.Invoke(this, forward, ref handled);
			}

			internal void raiseFocusNextControl(bool forward, ref bool handled)
			{
				FocusNextControlEvent?.Invoke(this, forward, ref handled);
			}

			private _CValueBuffer valueBufferBlock(object value)
			{
				valueBuffer = new _CValueBuffer(this, RuntimeHelpers.GetObjectValue(value));
				return valueBuffer;
			}

			public bool SetValueIfValidated(object value)
			{
				//Discarded unreachable code: IL_005f
				using (Table().RenderBlock())
				{
					using (_CValueBuffer cValueBuffer = valueBufferBlock(RuntimeHelpers.GetObjectValue(value)))
					{
						CancelEventArgs cancelEventArgs = new CancelEventArgs();
						Table().raiseFieldValidating(this, cancelEventArgs);
						if (cancelEventArgs.Cancel)
						{
							cValueBuffer.Cancel = true;
						}
						else
						{
							Table().raiseFieldValidated(this);
						}
						return !cValueBuffer.Cancel;
					}
				}
			}

			public virtual void Clear()
			{
				SetValueIfValidated(null);
			}

			public Rectangle GetRectangle()
			{
				return GetRectangle(Record.LayoutCache.Top, 0);
			}

			internal Rectangle GetRectangle(int top_row, int top_col)
			{
				return CGrid.GetRectangle(Table().Cols, Record.Rows, Desc.Layout, top_row, top_col);
			}

			public CField NextField()
			{
				List<CField> list = Record.tabOrderdList();
				checked
				{
					for (int i = list.IndexOf(this) + 1; i < list.Count; i++)
					{
						if (list[i].Focusable() && list[i].DynamicSetting().TabStop() == ETabStop.STOP)
						{
							return list[i];
						}
					}
					for (CRecord nextRecord = Record.LayoutCache.NextRecord; nextRecord != null; nextRecord = nextRecord.LayoutCache.NextRecord)
					{
						CField cField = nextRecord.DefaultField();
						if (cField != null)
						{
							return cField;
						}
					}
					return null;
				}
			}

			public CField PrevField()
			{
				List<CField> list = Record.tabOrderdList();
				checked
				{
					for (int num = list.IndexOf(this) - 1; num >= 0; num--)
					{
						if (list[num].Focusable() && list[num].DynamicSetting().TabStop() == ETabStop.STOP)
						{
							return list[num];
						}
					}
					for (CRecord prevRecord = Record.LayoutCache.PrevRecord; prevRecord != null; prevRecord = prevRecord.LayoutCache.PrevRecord)
					{
						CField cField = prevRecord.LastField();
						if (cField != null)
						{
							return cField;
						}
					}
					return null;
				}
			}

			public CField NextEditableField()
			{
				for (CField cField = NextField(); cField != null; cField = cField.NextField())
				{
					if (cField.Editable().Equals(EAllow.ALLOW))
					{
						return cField;
					}
				}
				return null;
			}

			public CField PrevEditableField()
			{
				for (CField cField = PrevField(); cField != null; cField = cField.PrevField())
				{
					if (cField.Editable().Equals(EAllow.ALLOW))
					{
						return cField;
					}
				}
				return null;
			}

			public void ClipboardCopy()
			{
				
//				string text = _SanitizeClipboard(Desc.Provider.get_Clipboard(this)) + "\r\n";
				string text = _SanitizeClipboard(Desc.Provider.Clipboard) + "\r\n";
				Clipboard.SetText(text);
			}

			public void ClipboardPaste()
			{
				if (Clipboard.ContainsText() && Focusable() && Editable().Equals(EAllow.ALLOW))
				{
					List<List<string>> list = _ParseClipboard(Clipboard.GetText());
					if (list.Count > 0 && list[0].Count > 0)
					{
						Desc.Provider.Clipboard=list[0][0];
					}
				}
			}
		}

		public class CBorder
		{
			public enum EBorderType
			{
				DEFAULT,
				FIELD,
				RECORD,
				CAPTION,
				NONE
			}

			public List<int> VisibleCols;

			public List<int> VisibleRows;

			public List<int> VisibleColsMap;

			public List<int> VisibleRowsMap;

			public List<int> VerticalMap;

			public List<int> HorizontalMap;

			public List<List<EBorderType>> Vertical;

			public List<List<EBorderType>> Horizontal;

			public int RowCount;

			public const int _T = 1;

			public const int _B = 2;

			public const int _L = 4;

			public const int _R = 8;

			public CBorder(CRecord record, CBorder previousBorder)
			{
				VisibleCols = new List<int>();
				VisibleRows = new List<int>();
				VisibleColsMap = new List<int>();
				VisibleRowsMap = new List<int>();
				VerticalMap = new List<int>();
				HorizontalMap = new List<int>();
				Vertical = new List<List<EBorderType>>();
				Horizontal = new List<List<EBorderType>>();
				checked
				{
					if (record.Table().Cols.Count > 0)
					{
						int num = 0;
						int num2 = -1;
						int num3 = -1;
						foreach (CGrid col in record.Table().Cols)
						{
							if ((num3 == -1) & col.Visible)
							{
								num3 = 0;
							}
							VerticalMap.Add(num3);
							if (col.Visible)
							{
								num2++;
								num3++;
								VisibleCols.Add(num);
							}
							VisibleColsMap.Add(num2);
							num++;
						}
						VerticalMap.Add(num3);
					}
					if (record.Rows.Count > 0)
					{
						int num4 = 0;
						int num5 = -1;
						int num6 = -1;
						foreach (CGrid row in record.Rows)
						{
							if ((num6 == -1) & row.Visible)
							{
								num6 = 0;
							}
							HorizontalMap.Add(num6);
							if (row.Visible)
							{
								num5++;
								num6++;
								VisibleRows.Add(num4);
							}
							VisibleRowsMap.Add(num5);
							num4++;
						}
						HorizontalMap.Add(num6);
					}
					RowCount = record.Rows.Count;
					int num7 = VisibleRows.Count - 1;
					for (int i = 0; i <= num7; i++)
					{
						List<EBorderType> list = new List<EBorderType>();
						int count = VisibleCols.Count;
						for (int j = 0; j <= count; j++)
						{
							list.Add(EBorderType.DEFAULT);
						}
						Vertical.Add(list);
					}
					int count2 = VisibleRows.Count;
					for (int k = 0; k <= count2; k++)
					{
						List<EBorderType> list2 = new List<EBorderType>();
						int num8 = VisibleCols.Count - 1;
						for (int l = 0; l <= num8; l++)
						{
							list2.Add(EBorderType.DEFAULT);
						}
						Horizontal.Add(list2);
					}
					int num9 = VisibleCols.Count - 1;
					for (int m = 0; m <= num9; m++)
					{
						if (previousBorder != null && previousBorder.Horizontal[previousBorder.RowCount][m] > EBorderType.RECORD)
						{
							Horizontal[0][m] = previousBorder.Horizontal[previousBorder.RowCount][m];
						}
						else
						{
							Horizontal[0][m] = EBorderType.RECORD;
						}
					}
				}
			}

			public void SetVertical(int row, int col, EBorderType type)
			{
				int num = VisibleRowsMap[row];
				int num2 = VerticalMap[col];
				if (num >= 0 && num2 >= 0 && Vertical[num][num2] < type)
				{
					Vertical[num][num2] = type;
				}
			}

			public void SetHorizontal(int row, int col, EBorderType type)
			{
				int num = HorizontalMap[row];
				int num2 = VisibleColsMap[col];
				if (num >= 0 && num2 >= 0 && Horizontal[num][num2] < type)
				{
					Horizontal[num][num2] = type;
				}
			}

			public void Fill(CGrid.CRegion layout, EBorderType type, int borderLine)
			{
				EBorderType type2 = ((borderLine & 1) == 0) ? EBorderType.NONE : type;
				checked
				{
					int num = layout.Cols - 1;
					for (int i = 0; i <= num; i++)
					{
						SetHorizontal(layout.Row, layout.Col + i, type2);
					}
					type2 = (((borderLine & 2) == 0) ? EBorderType.NONE : type);
					int num2 = layout.Cols - 1;
					for (int j = 0; j <= num2; j++)
					{
						SetHorizontal(layout.Row + layout.Rows, layout.Col + j, type2);
					}
					type2 = (((borderLine & 4) == 0) ? EBorderType.NONE : type);
					int num3 = layout.Rows - 1;
					for (int k = 0; k <= num3; k++)
					{
						SetVertical(layout.Row + k, layout.Col, type2);
					}
					type2 = (((borderLine & 8) == 0) ? EBorderType.NONE : type);
					int num4 = layout.Rows - 1;
					for (int l = 0; l <= num4; l++)
					{
						SetVertical(layout.Row + l, layout.Col + layout.Cols, type2);
					}
				}
			}

			public void Render(CRecord record, Graphics g, int top_row, int top_col, bool @fixed)
			{
				Dictionary<EBorderType, Pen> dictionary = new Dictionary<EBorderType, Pen>();
				checked
				{
					try
					{
						CDynamicBorderSetting cDynamicBorderSetting = record.DynamicBorderSetting();
						Pen pen = (Pen)createPen(cDynamicBorderSetting.BorderColor(), cDynamicBorderSetting.BorderStyle());
						if (pen != null)
						{
							dictionary.Add(EBorderType.FIELD, pen);
						}
						pen = (Pen)createPen(cDynamicBorderSetting.CaptionBorderColor(), cDynamicBorderSetting.CaptionBorderStyle());
						if (pen != null)
						{
							dictionary.Add(EBorderType.CAPTION, pen);
						}
						pen = (Pen)createPen(cDynamicBorderSetting.RecordBorderColor(), cDynamicBorderSetting.RecordBorderStyle());
						if (pen != null)
						{
							dictionary.Add(EBorderType.RECORD, pen);
						}
						cDynamicBorderSetting = null;
						int num = top_col;
						int count = VisibleCols.Count;
						int num2;
						for (int i = 0; i <= count && (!@fixed || i >= VisibleCols.Count || VisibleCols[i] <= record.Table().FixedCols); i++)
						{
							num2 = top_row + record.LayoutCache.Top;
							int num3 = VisibleRows.Count - 1;
							for (int j = 0; j <= num3; j++)
							{
								EBorderType key = Vertical[j][i];
								int num4 = num2 + record.Rows[VisibleRows[j]].Size;
								if (dictionary.ContainsKey(key))
								{
									g.DrawLine(dictionary[key], num, num2, num, num4);
								}
								num2 = num4;
							}
							if (i < VisibleCols.Count)
							{
								num += record.Table().Cols[VisibleCols[i]].Size;
							}
						}
						num2 = top_row + record.LayoutCache.Top;
						int count2 = VisibleRows.Count;
						for (int k = 0; k <= count2; k++)
						{
							num = top_col;
							int num5 = VisibleCols.Count - 1;
							for (int l = 0; l <= num5 && (!@fixed || VisibleCols[l] < record.Table().FixedCols); l++)
							{
								EBorderType key2 = Horizontal[k][l];
								int num6 = num + record.Table().Cols[VisibleCols[l]].Size;
								if (dictionary.ContainsKey(key2))
								{
									g.DrawLine(dictionary[key2], num, num2, num6, num2);
								}
								num = num6;
							}
							if (k < VisibleRows.Count)
							{
								num2 += record.Rows[VisibleRows[k]].Size;
							}
						}
					}
					finally
					{
						foreach (Pen value in dictionary.Values)
						{
							value.Dispose();
						}
					}
				}
			}
		}

		public class CResizing
		{
			public int Size;

			public int Col;

			[DebuggerNonUserCode]
			public CResizing()
			{
			}
		}

		public class CContent
		{
			public delegate void ValidatedEventHandler(CContent content, object key);

			public delegate void EnterEventHandler(CContent content);

			public delegate void SortedEventHandler(CContent content, object key);

			public class CLayoutCache
			{
				public int RowsSize;

				public CRecord TopRecord;

				public CRecord LastRecord;

				public List<CGrid> SelectableRows;

				public List<int> SelectableRowsPos;

				[DebuggerNonUserCode]
				public CLayoutCache()
				{
				}
			}

			private ValidatedEventHandler ValidatedEvent;

			private EnterEventHandler EnterEvent;

			private SortedEventHandler SortedEvent;

			public CRecordProvider RecordProvider;

			public List<CRecord> Records;

			public CRecord ParentRecord;

			public UTable Table;

			public bool LayoutCacheInvalid;

			public CLayoutCache LayoutCache;

			public CRecord LastAddedRecord;

			public bool Enabled;

			public int MaxRecordCount;

			public Rectangle RenderingRect;

			public bool LimitIncludeFocusColor;

			private CSetting _Setting;

			private CBorderSetting _BorderSetting;

			private bool _Visible;

			public CSetting Setting
			{
				get
				{
					if (_Setting == null)
					{
						_Setting = new CSetting();
					}
					return _Setting;
				}
				set
				{
					_Setting = value;
				}
			}

			public CBorderSetting BorderSetting
			{
				get
				{
					if (_BorderSetting == null)
					{
						_BorderSetting = new CBorderSetting();
					}
					return _BorderSetting;
				}
				set
				{
					_BorderSetting = value;
				}
			}

			public bool Visible
			{
				get
				{
					return _Visible;
				}
				set
				{
					using (Table.RenderBlock())
					{
						if (!value)
						{
							focusEscape();
						}
						_Visible = value;
						TopLevelContent().LayoutCacheInvalid = true;
					}
				}
			}

			public event ValidatedEventHandler Validated
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					ValidatedEvent = (ValidatedEventHandler)Delegate.Combine(ValidatedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					ValidatedEvent = (ValidatedEventHandler)Delegate.Remove(ValidatedEvent, value);
				}
			}

			public event EnterEventHandler Enter
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					EnterEvent = (EnterEventHandler)Delegate.Combine(EnterEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					EnterEvent = (EnterEventHandler)Delegate.Remove(EnterEvent, value);
				}
			}

			public event SortedEventHandler Sorted
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					SortedEvent = (SortedEventHandler)Delegate.Combine(SortedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					SortedEvent = (SortedEventHandler)Delegate.Remove(SortedEvent, value);
				}
			}

			public CContent()
			{
				Records = new List<CRecord>();
				ParentRecord = null;
				LayoutCacheInvalid = false;
				LayoutCache = new CLayoutCache();
				LastAddedRecord = null;
				Enabled = true;
				MaxRecordCount = 0;
				LimitIncludeFocusColor = false;
				_Setting = null;
				_BorderSetting = null;
				_Visible = true;
			}

			public CContent TopLevelContent()
			{
				CContent cContent = this;
				while (cContent.ParentRecord != null)
				{
					cContent = cContent.ParentRecord.Content;
				}
				return cContent;
			}

			public CRecord TopLevelRecord()
			{
				if (ParentRecord != null)
				{
					CRecord parentRecord = ParentRecord;
					while (parentRecord.Content.ParentRecord != null)
					{
						parentRecord = parentRecord.Content.ParentRecord;
					}
					return parentRecord;
				}
				return null;
			}

			public int Level()
			{
				CContent cContent = this;
				int num = 0;
				while (cContent.ParentRecord != null)
				{
					cContent = cContent.ParentRecord.Content;
					num = checked(num + 1);
				}
				return num;
			}

			public bool HasSetting()
			{
				return _Setting != null;
			}

			public void ClearSetting()
			{
				Setting = null;
			}

			public bool HasBorderSetting()
			{
				return _BorderSetting != null;
			}

			public void ClearBorderSetting()
			{
				_BorderSetting = null;
			}

			public void Visualize()
			{
				using (Table.RenderBlock())
				{
					Visible = true;
					if (ParentRecord != null)
					{
						ParentRecord.Visualize();
					}
				}
			}

			public void SetRecordProvider(CRecordProvider recordProvider)
			{
				RecordProvider = recordProvider;
				MaxRecordCount = recordProvider.MaxRecordCount;
				LimitIncludeFocusColor = recordProvider.LimitIncludeFocusColor;
				Table.CreateCols(RecordProvider.ColsCount);
			}

			public CRecord CreateRecord()
			{
				return CreateRecord(RecordProvider);
			}

			public CRecord CreateRecord(CRecordProvider recordProvider)
			{
				CRecord cRecord = new CRecord(this, recordProvider);
				cRecord.raiseCreated();
				return cRecord;
			}

			public CRecord AddRecord()
			{
				return AddRecord(RecordProvider);
			}

			public CRecord AddRecord(CRecordProvider recordProvider)
			{
				return AddRecord(CreateRecord(recordProvider));
			}

			public CRecord AddRecord(CRecord record)
			{
				//Discarded unreachable code: IL_006c
				if (MaxRecordCount == 0 || Records.Count < MaxRecordCount)
				{
					if (Table.UndoBufferEnabled)
					{
						addModify(new CAddRecordModify(Records.Count));
					}
					using (Table.RenderBlock())
					{
						Records.Add(record);
						_afterAddRecord(record);
						return record;
					}
				}
				return null;
			}

			public CRecord InsertRecord(int i)
			{
				return InsertRecord(i, RecordProvider);
			}

			public CRecord InsertRecord(int i, CRecordProvider recordProvider)
			{
				return InsertRecord(i, CreateRecord(recordProvider));
			}

			public CRecord InsertRecord(int i, CRecord record)
			{
				if (Table.UndoBufferEnabled)
				{
					addModify(new CAddRecordModify(i));
				}
				using (Table.RenderBlock())
				{
					Records.Insert(i, record);
					_afterAddRecord(record);
					return record;
				}
			}

			private void _afterAddRecord(CRecord record)
			{
				TopLevelContent().LayoutCacheInvalid = true;
				record.raiseAdded();
				LastAddedRecord = record;
			}

			public void RemoveRecord(CRecord record)
			{
				RemoveRecord(record.Index());
			}

			public void RemoveRecord(int i)
			{
				CRecord record = Records[i];
				if (Table.UndoBufferEnabled)
				{
					addModify(new CRemoveRecordModify(record));
				}
				using (Table.RenderBlock())
				{
					_beforeRemoveRecord(record);
					Records.RemoveAt(i);
					_afterRemoveRecord(record);
				}
			}

			private void _beforeRemoveRecord(CRecord record)
			{
				if (Table.Editor() != null)
				{
					Table.FinishEdit();
				}
				record.focusEscape();
			}

			private void _afterRemoveRecord(CRecord record)
			{
				TopLevelContent().LayoutCacheInvalid = true;
				record.raiseRemoved();
			}

			internal void render(Graphics g, int top_row, int top_col, int max_row, bool @fixed)
			{
				bool flag = false;
				CRecord cRecord = LayoutCache.TopRecord;
				CContent cContent = null;
				CVerticalMerge cVerticalMerge = new CVerticalMerge();
				CBorder cBorder = null;
				checked
				{
					while (cRecord != null)
					{
						if (!Table.Setting.AlterConsistent & (cRecord.Content != cContent))
						{
							flag = false;
						}
						cVerticalMerge.Update(cRecord, flag);
						int num = cRecord.LayoutCache.Top + top_row;
						if (num <= max_row)
						{
							cContent = cRecord.Content;
							if (num + cRecord.LayoutCache.RowsSize > 0)
							{
								cBorder = new CBorder(cRecord, cBorder);
								cRecord.render(g, top_row, top_col, flag, @fixed, cBorder, cVerticalMerge);
								cBorder.Render(cRecord, g, top_row, top_col, @fixed);
							}
							flag = !flag;
							cRecord = cRecord.LayoutCache.NextRecord;
							continue;
						}
						break;
					}
				}
			}

			internal CRecord updateLayoutCache()
			{
				int top = 0;
				CRecord prevRecord = null;
				int height = 0;
				return updateLayoutCache(ref top, ref prevRecord, visible: true, ref height);
			}

			internal CRecord updateLayoutCache(ref int top, ref CRecord prevRecord, bool visible, ref int height)
			{
				LayoutCache.RowsSize = 0;
				LayoutCache.TopRecord = null;
				LayoutCache.LastRecord = null;
				LayoutCache.SelectableRows = new List<CGrid>();
				LayoutCache.SelectableRowsPos = new List<int>();
				bool visible2 = (visible && Visible) ? true : false;
				int num = 0;
				checked
				{
					foreach (CRecord record in Records)
					{
						record.updateCache(ref top, ref prevRecord, visible2, num, ref height);
						CLayoutCache layoutCache = LayoutCache;
						layoutCache.RowsSize += record.LayoutCache.EntireRowsSize;
						num++;
					}
					return prevRecord;
				}
			}

			public CGrid Col(object key)
			{
				return Table.Cols[RecordProvider.FieldDescs[RuntimeHelpers.GetObjectValue(key)].Layout.RightCol()];
			}

			public CContent ParentContent()
			{
				if (ParentRecord != null)
				{
					return ParentRecord.Content;
				}
				return null;
			}

			public void ClearRecord()
			{
				if (Table.UndoBufferEnabled)
				{
					addModify(new CClearRecordModify(this));
				}
				using (Table.RenderBlock())
				{
					if (Table.Editor() != null)
					{
						Table.FinishEdit();
					}
					if (Table.FocusField != null && IsChild(Table.FocusField))
					{
						Table.FocusField = null;
					}
					LastAddedRecord = null;
					Records.Clear();
					if (Table.Content == this)
					{
						Table.SortState.Field = null;
					}
					TopLevelContent().LayoutCacheInvalid = true;
				}
			}

			public void MoveRecord(int i1, int i2)
			{
				MoveRecord(Records[i1], i2);
			}

			public void MoveRecord(CRecord r, int i)
			{
				if (Table.UndoBufferEnabled)
				{
					addModify(new CMoveRecordModify(r, i));
				}
				using (Table.RenderBlock())
				{
					if (i < Records.Count)
					{
						Records.Remove(r);
						Records.Insert(i, r);
					}
					else
					{
						Records.Remove(r);
						Records.Add(r);
					}
					TopLevelContent().LayoutCacheInvalid = true;
					r.raiseMoved();
				}
			}

			public bool IsChild(CField field)
			{
				if (field.Content() == this)
				{
					return true;
				}
				return IsChild(field.Record);
			}

			public bool IsChild(CRecord record)
			{
				if (record.Content == this)
				{
					return true;
				}
				return IsChild(record.Content);
			}

			public bool IsChild(CContent content)
			{
				for (CContent cContent = content.ParentContent(); cContent != null; cContent = cContent.ParentContent())
				{
					if (cContent == this)
					{
						return true;
					}
				}
				return false;
			}

			public void Sort(object key)
			{
				Sort(RuntimeHelpers.GetObjectValue(key), CSortState.EOrder.ASCEND);
			}

			public void Sort(object key, CSortState.EOrder Order)
			{
				CComparer comparer = Table.Comparer;
				if (RecordProvider != null && RecordProvider.FieldDescs.ContainsKey(RuntimeHelpers.GetObjectValue(key)) && RecordProvider.FieldDescs[RuntimeHelpers.GetObjectValue(key)].Comparer != null)
				{
					comparer = RecordProvider.FieldDescs[RuntimeHelpers.GetObjectValue(key)].Comparer;
				}
				Sort(RuntimeHelpers.GetObjectValue(key), Order, comparer);
			}

			public void Sort(object key, CSortState.EOrder Order, CComparer comparer)
			{
				List<int> list = new List<int>(Records.Count);
				checked
				{
					int num = Records.Count - 1;
					for (int i = 0; i <= num; i++)
					{
						list.Add(i);
					}
					comparer.Init(Records, RuntimeHelpers.GetObjectValue(key), Order);
					list.Sort(comparer);
					if (Table.UndoBufferEnabled)
					{
						addModify(new CSortModify(list));
					}
					using (Table.RenderBlock())
					{
						List<CRecord> records = Records;
						Records = new List<CRecord>();
						int num2 = list.Count - 1;
						for (int j = 0; j <= num2; j++)
						{
							Records.Add(records[list[j]]);
						}
						raiseSorted(RuntimeHelpers.GetObjectValue(key));
						TopLevelContent().LayoutCacheInvalid = true;
					}
				}
			}

			public CField DefaultField()
			{
				for (CRecord cRecord = LayoutCache.TopRecord; cRecord != null; cRecord = cRecord.LayoutCache.NextRecord)
				{
					CField cField = cRecord.DefaultField();
					if (cField != null)
					{
						return cField;
					}
				}
				return null;
			}

			public CField LastField()
			{
				CRecord cRecord = LayoutCache.LastRecord;
				if (cRecord != null)
				{
					while (cRecord.Child != null && cRecord.Child.LayoutCache.LastRecord != null)
					{
						cRecord = cRecord.Child.LayoutCache.LastRecord;
					}
					while (cRecord != null)
					{
						CField cField = cRecord.LastField();
						if (cField != null)
						{
							return cField;
						}
						cRecord = cRecord.LayoutCache.PrevRecord;
					}
				}
				return null;
			}

			internal void focusEscape()
			{
				if (Table.FocusField == null || !IsChild(Table.FocusField))
				{
					return;
				}
				if (ParentRecord != null)
				{
					CField cField = ParentRecord.DefaultField();
					if (cField != null)
					{
						Table.FocusField = cField;
						return;
					}
				}
				Table.FocusField = null;
			}

			public void Clear()
			{
				Clear(recursive: true);
			}

			public void Clear(bool recursive)
			{
				//Discarded unreachable code: IL_0042
				using (Table.RenderBlock())
				{
					foreach (CRecord record in Records)
					{
						record.Clear(recursive);
					}
				}
			}

			internal void raiseValidated(object key)
			{
				ValidatedEvent?.Invoke(this, RuntimeHelpers.GetObjectValue(key));
				Table.raiseContentValidated(this, RuntimeHelpers.GetObjectValue(key));
			}

			internal void raiseEnter()
			{
				EnterEvent?.Invoke(this);
				Table.raiseContentEnter(this);
			}

			internal void raiseSorted(object key)
			{
				SortedEvent?.Invoke(this, RuntimeHelpers.GetObjectValue(key));
				Table.raiseSorted(this, RuntimeHelpers.GetObjectValue(key));
			}

			internal void addModify(IModify modify)
			{
				if (ParentRecord != null)
				{
					ParentContent().addModify(new CChildModify(ParentRecord, modify));
				}
				else
				{
					Table.addModify(this, modify);
				}
			}
		}

		public enum ECompareNullRule
		{
			ASCEND,
			DESCEND,
			ASCEND_ALWAYS,
			DESCEND_ALWAYS
		}

		public class CComparer : IComparer<int>
		{
			public ECompareNullRule NullRule;

			protected List<CRecord> records;

			protected object key;

			protected CSortState.EOrder order;

			public CComparer()
			{
				NullRule = ECompareNullRule.ASCEND;
			}

			public void Init(List<CRecord> records, object key, CSortState.EOrder order)
			{
				this.records = records;
				this.key = RuntimeHelpers.GetObjectValue(key);
				this.order = order;
			}

			public virtual int Compare(int x, int y)
			{
				if (x == y)
				{
					return 0;
				}
				CRecord cRecord = records[x];
				CRecord cRecord2 = records[y];
				if (!cRecord.Fields.ContainsKey(RuntimeHelpers.GetObjectValue(key)) | !cRecord2.Fields.ContainsKey(RuntimeHelpers.GetObjectValue(key)))
				{
					return -1;
				}
				object obj = RuntimeHelpers.GetObjectValue(records[x].Fields[RuntimeHelpers.GetObjectValue(key)].Value);
				object obj2 = RuntimeHelpers.GetObjectValue(records[y].Fields[RuntimeHelpers.GetObjectValue(key)].Value);
				if (obj is string && string.IsNullOrEmpty(Conversions.ToString(obj)))
				{
					obj = null;
				}
				if (obj2 is string && string.IsNullOrEmpty(Conversions.ToString(obj2)))
				{
					obj2 = null;
				}
				if (Operators.ConditionalCompareObjectEqual(obj, obj2, TextCompare: false))
				{
					return 0 - ((x < y) ? 1 : 0);
				}
				if (obj == null && obj2 != null)
				{
					return 0 - (nullResult() ? 1 : 0);
				}
				if (obj != null && obj2 == null)
				{
					return 0 - ((!nullResult()) ? 1 : 0);
				}
				if (obj is string)
				{
					switch (order)
					{
					case CSortState.EOrder.ASCEND:
						return Conversions.ToString(obj).CompareTo(RuntimeHelpers.GetObjectValue(obj2));
					case CSortState.EOrder.DESCEND:
						return checked(-Conversions.ToString(obj).CompareTo(RuntimeHelpers.GetObjectValue(obj2)));
					}
				}
				else
				{
					switch (order)
					{
					case CSortState.EOrder.ASCEND:
						return Conversions.ToInteger(Operators.CompareObjectLess(obj, obj2, TextCompare: false));
					case CSortState.EOrder.DESCEND:
						return Conversions.ToInteger(Operators.CompareObjectGreater(obj, obj2, TextCompare: false));
					}
				}
				int result = default(int);
				return result;
			}

			int IComparer<int>.Compare(int x, int y)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Compare
				return this.Compare(x, y);
			}

			private bool nullResult()
			{
				switch (NullRule)
				{
				case ECompareNullRule.ASCEND:
					switch (order)
					{
					case CSortState.EOrder.ASCEND:
						return true;
					case CSortState.EOrder.DESCEND:
						return false;
					}
					break;
				case ECompareNullRule.DESCEND:
					switch (order)
					{
					case CSortState.EOrder.ASCEND:
						return false;
					case CSortState.EOrder.DESCEND:
						return true;
					}
					break;
				case ECompareNullRule.ASCEND_ALWAYS:
					return true;
				case ECompareNullRule.DESCEND_ALWAYS:
					return false;
				}
				bool result = default(bool);
				return result;
			}
		}

		public class CVerticalMerge
		{
			public class CVerticalMergeEntry
			{
				public CField TopField;

				public bool TopAlter;

				public CField BottomField;

				public CField DispField;

				public bool Rendered;

				public object MergeKey;

				public CVerticalMergeEntry()
				{
					TopField = null;
					BottomField = null;
					DispField = null;
					Rendered = false;
					MergeKey = null;
				}

				public void init(CField field, bool alter)
				{
					TopField = field;
					TopAlter = alter;
					BottomField = field;
					DispField = field;
					Rendered = false;
				}

				public void clear()
				{
					TopField = null;
					BottomField = null;
					DispField = null;
					Rendered = false;
				}
			}

			public class CTranslateResult
			{
				public CField Field;

				public Rectangle Rect;

				public bool Alter;

				public bool Merged;

				public CTranslateResult(CField field, Rectangle rect, bool alter, bool merged)
				{
					Field = field;
					Rect = rect;
					Alter = alter;
					Merged = merged;
				}
			}

			public Dictionary<object, CVerticalMergeEntry> Fields;

			private CRecordProvider lastrp;

			public CVerticalMerge()
			{
				Fields = new Dictionary<object, CVerticalMergeEntry>();
				lastrp = null;
			}

			public void Update(CRecord record, bool alter)
			{
				addFieldEntry(record.Content);
				foreach (object key in Fields.Keys)
				{
					object objectValue = RuntimeHelpers.GetObjectValue(key);
					CField cField = record.Fields[RuntimeHelpers.GetObjectValue(objectValue)];
					CVerticalMergeEntry cVerticalMergeEntry = Fields[RuntimeHelpers.GetObjectValue(objectValue)];
					if (cVerticalMergeEntry.TopField == null || isBreak(cField, cVerticalMergeEntry.TopField))
					{
						cVerticalMergeEntry.init(cField, alter);
						CField cField2 = cField;
						while (cField2 != null && !isBreak(cField, cField2))
						{
							if (cField2.Table().FocusRecord == cField2.Record)
							{
								cVerticalMergeEntry.DispField = cField2;
							}
							cVerticalMergeEntry.BottomField = cField2;
							cField2 = nextField(cField2);
						}
					}
					cVerticalMergeEntry = null;
				}
			}

			private void addFieldEntry(CContent c)
			{
				if (c.RecordProvider != null && c.RecordProvider != lastrp)
				{
					Dictionary<object, CVerticalMergeEntry> fields = Fields;
					Fields = new Dictionary<object, CVerticalMergeEntry>();
					foreach (object key in c.RecordProvider.FieldDescs.Keys)
					{
						object objectValue = RuntimeHelpers.GetObjectValue(key);
						if (c.RecordProvider.FieldDescs[RuntimeHelpers.GetObjectValue(objectValue)].VerticalMerge)
						{
							if (fields.ContainsKey(RuntimeHelpers.GetObjectValue(objectValue)))
							{
								Fields.Add(RuntimeHelpers.GetObjectValue(objectValue), fields[RuntimeHelpers.GetObjectValue(objectValue)]);
							}
							else
							{
								Fields.Add(RuntimeHelpers.GetObjectValue(objectValue), new CVerticalMergeEntry());
							}
						}
					}
					lastrp = c.RecordProvider;
				}
			}

			private bool isBreak(CField f1, CField f2)
			{
				bool @break = !f2.Desc.VerticalMerge || f1.Value == null || !f1.Value.Equals(RuntimeHelpers.GetObjectValue(f2.Value));
				f1.Table().raiseVerticalMergingBreak(f1, f2, ref @break);
				return @break;
			}

			public CTranslateResult Translate(CField field, Rectangle rect, bool alter, int top_row, int top_col)
			{
				if (Fields.ContainsKey(RuntimeHelpers.GetObjectValue(field.Key)))
				{
					CVerticalMergeEntry cVerticalMergeEntry = Fields[RuntimeHelpers.GetObjectValue(field.Key)];
					if (!cVerticalMergeEntry.Rendered)
					{
						cVerticalMergeEntry.Rendered = true;
						return new CTranslateResult(cVerticalMergeEntry.DispField, checked(Rectangle.Union(cVerticalMergeEntry.TopField.GetRectangle(top_row + cVerticalMergeEntry.TopField.Record.LayoutCache.Top, top_col), cVerticalMergeEntry.BottomField.GetRectangle(top_row + cVerticalMergeEntry.BottomField.Record.LayoutCache.Top, top_col))), cVerticalMergeEntry.TopAlter, field != cVerticalMergeEntry.BottomField);
					}
					Rectangle rect2 = new Rectangle(0, 0, 0, 0);
					return new CTranslateResult(field, rect2, alter, field != cVerticalMergeEntry.BottomField);
				}
				return new CTranslateResult(field, rect, alter, merged: false);
			}

			private CField nextField(CField field)
			{
				if (field.Record.LayoutCache.NextRecord == null)
				{
					return null;
				}
				CRecord nextRecord = field.Record.LayoutCache.NextRecord;
				if (!nextRecord.Fields.ContainsKey(RuntimeHelpers.GetObjectValue(field.Key)))
				{
					return null;
				}
				return nextRecord.Fields[RuntimeHelpers.GetObjectValue(field.Key)];
			}
		}

		public class CEditorContainer
		{
			public UTable Table;

			public CFieldModify FieldModify;

			[AccessedThroughProperty("Editor")]
			private IEditor _Editor;

			[AccessedThroughProperty("Control")]
			private Control _Control;

			private bool editor_validating_flg;

			private bool editor_validated_flg;

			public virtual IEditor Editor
			{
				[DebuggerNonUserCode]
				get
				{
					return _Editor;
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				set
				{
					IEditor.ValueChangedEventHandler obj = _Editor_ValueChanged;
					IEditor.LeaveEventHandler obj2 = Editor_Leave;
					if (_Editor != null)
					{
						_Editor.ValueChanged -= obj;
						_Editor.Leave -= obj2;
					}
					_Editor = value;
					if (_Editor != null)
					{
						_Editor.ValueChanged += obj;
						_Editor.Leave += obj2;
					}
				}
			}

			public virtual Control Control
			{
				[DebuggerNonUserCode]
				get
				{
					return _Control;
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				set
				{
					EventHandler value2 = Editor_Validated;
					CancelEventHandler value3 = Editor_Validating;
					if (_Control != null)
					{
						_Control.Validated -= value2;
						_Control.Validating -= value3;
					}
					_Control = value;
					if (_Control != null)
					{
						_Control.Validated += value2;
						_Control.Validating += value3;
					}
				}
			}

			public CEditorContainer(UTable table, IEditor editor)
			{
				editor_validating_flg = false;
				editor_validated_flg = false;
				Table = table;
				Editor = editor;
				Control = editor.Control();
				FieldModify = table.FocusField.Desc.Provider.CreateModifyField(table.FocusField);
				FieldModify.SetUp(table.FocusField);
			}

			private void Editor_Leave(object sender, string direction)
			{
				Table.LeaveEdit(direction);
			}

			private void _Editor_ValueChanged(object sender)
			{
				Table.raiseEditorValueChanged(Table.FocusField);
			}

			private void Editor_Validating(object sender, CancelEventArgs e)
			{
				if (!Table.finishEdit_flg)
				{
					try
					{
						if (!editor_validating_flg)
						{
							editor_validating_flg = true;
							if (Table.Editor() == Editor)
							{
								Table.raiseFieldValidating(Table.FocusField, e);
							}
						}
					}
					finally
					{
						editor_validating_flg = false;
					}
				}
			}

			private void Editor_Validated(object sender, EventArgs e)
			{
				try
				{
					if (!editor_validated_flg)
					{
						editor_validated_flg = true;
						if (Table.Editor() == Editor)
						{
							Table.raiseFieldValidated(Table.FocusField);
							Table.FinishEdit(valueCommit: true);
						}
					}
				}
				finally
				{
					editor_validated_flg = false;
				}
			}
		}

		private class CDraggingScrollMove
		{
			public int dx;

			public int dy;

			private const int THRESHOLD = 80;

			public CDraggingScrollMove()
			{
				dx = 0;
				dy = 0;
			}

			public void Update(UTable table, Point location)
			{
				checked
				{
					if (location.X < -80)
					{
						dx = -10;
					}
					else if (location.X < 0)
					{
						dx = -1;
					}
					else if (location.X > table.Width + 80)
					{
						dx = 10;
					}
					else if (location.X > table.Width)
					{
						dx = 1;
					}
					else
					{
						dx = 0;
					}
					if (location.Y < table.HeaderContent.LayoutCache.RowsSize - 80)
					{
						dy = -10;
					}
					else if (location.Y < table.HeaderContent.LayoutCache.RowsSize)
					{
						dy = -1;
					}
					else if (location.Y > table.Height - table.FooterContent.LayoutCache.RowsSize + 80)
					{
						dy = 10;
					}
					else if (location.Y > table.Height - table.FooterContent.LayoutCache.RowsSize)
					{
						dy = 1;
					}
					else
					{
						dy = 0;
					}
				}
			}
		}

		public class CFieldDesc
		{
			public bool CreateCaption;

			public EAllow UserSortable;

			public object MergeCaption;

			public CComparer Comparer;

			public CGrid.CRegion Layout;

			public IFieldProvider Provider;

			public bool VerticalMerge;

			private CSetting _Setting;

			private CSetting _Setting2;

			public CSetting Setting
			{
				get
				{
					if (_Setting == null)
					{
						_Setting = new CSetting();
					}
					return _Setting;
				}
				set
				{
					_Setting = value;
				}
			}

			public CSetting Setting2
			{
				get
				{
					if (_Setting2 == null)
					{
						_Setting2 = new CSetting();
					}
					return _Setting2;
				}
				set
				{
					_Setting2 = value;
				}
			}

			public CFieldDesc()
			{
				CreateCaption = true;
				UserSortable = EAllow.DEFAULT;
				MergeCaption = null;
				Comparer = null;
				VerticalMerge = false;
			}

			public CFieldDesc(IFieldProvider provider, CGrid.CRegion layout)
			{
				CreateCaption = true;
				UserSortable = EAllow.DEFAULT;
				MergeCaption = null;
				Comparer = null;
				VerticalMerge = false;
				Initialize(provider, layout);
			}

			public void Initialize(IFieldProvider provider, CGrid.CRegion layout)
			{
				Provider = provider;
				Layout = layout;
				Setting = provider.Setting();
			}

			public bool HasSetting()
			{
				return _Setting != null;
			}

			public void ClearSetting()
			{
				Setting = null;
			}

			public bool HasSetting2()
			{
				return _Setting2 != null;
			}

			public void ClearSetting2()
			{
				Setting2 = null;
			}
		}

		public class CGrid
		{
			public class CPoint
			{
				public int Row;

				public int Col;

				public CPoint()
					: this(0, 0)
				{
				}

				public CPoint(int row, int col)
				{
					Row = row;
					Col = col;
				}
			}

			public class CSize
			{
				public int Row;

				public int Col;

				public CSize()
					: this(0, 0)
				{
				}

				public CSize(int row, int col)
				{
					Row = row;
					Col = col;
				}
			}

			public class CRegion
			{
				public int Row;

				public int Col;

				public int Rows;

				public int Cols;

				public CRegion()
					: this(0, 0)
				{
				}

				public CRegion(int row, int col)
					: this(row, col, 1, 1)
				{
				}

				public CRegion Clone()
				{
					CRegion cRegion = new CRegion();
					cRegion.Row = Row;
					cRegion.Col = Col;
					cRegion.Rows = Rows;
					cRegion.Cols = Cols;
					return cRegion;
				}

				public CRegion(int row, int col, int rows, int cols)
				{
					Row = row;
					Col = col;
					Rows = rows;
					Cols = cols;
				}

				public bool Contains(CPoint point)
				{
					return (point.Row >= Row) & (point.Row <= BottomRow()) & (point.Col >= Col) & (point.Col <= RightCol());
				}

				public CPoint Point()
				{
					return new CPoint(Row, Col);
				}

				public int BottomRow()
				{
					return checked(Row + Rows - 1);
				}

				public int RightCol()
				{
					return checked(Col + Cols - 1);
				}

				public CRegion Union(CRegion r)
				{
					int num = 0;
					int num2 = 0;
					int num3 = 0;
					int num4 = 0;
					num = ((Row >= r.Row) ? r.Row : Row);
					num2 = ((Col >= r.Col) ? r.Col : Col);
					checked
					{
						num3 = ((Row + Rows <= r.Row + r.Rows) ? (r.Row + r.Rows - num) : (Row + Rows - num));
						num4 = ((Col + Cols <= r.Col + r.Cols) ? (r.Col + r.Cols - num2) : (Col + Cols - num2));
						return new CRegion(num, num2, num3, num4);
					}
				}
			}

			public UTable Table;

			public CRecord Record;

			public CContent Content;

			public int SizeMax;

			public int SizeMin;

			public EAllow UserResizable;

			public EAllow UserAdjustable;

			private bool _Visible;

			private int _Size;

			public int Size
			{
				get
				{
					if (_Size == 0)
					{
						if (Table != null)
						{
							return Table.DefaultGridSize.Col;
						}
						if (Content != null)
						{
							return Content.Table.DefaultGridSize.Row;
						}
						return 0;
					}
					return _Size;
				}
				set
				{
					int num = Regularize(value);
					if (num > 0)
					{
						_Size = num;
						update();
					}
				}
			}

			public bool Visible
			{
				get
				{
					return _Visible;
				}
				set
				{
					_Visible = value;
					update();
				}
			}

			public CGrid()
			{
				Table = null;
				Record = null;
				Content = null;
				SizeMax = 0;
				SizeMin = 0;
				UserResizable = EAllow.DEFAULT;
				UserAdjustable = EAllow.DEFAULT;
				_Visible = true;
				_Size = 0;
			}

			public CGrid(UTable table)
			{
				Table = null;
				Record = null;
				Content = null;
				SizeMax = 0;
				SizeMin = 0;
				UserResizable = EAllow.DEFAULT;
				UserAdjustable = EAllow.DEFAULT;
				_Visible = true;
				_Size = 0;
				Table = table;
				Record = null;
				Content = null;
			}

			public CGrid(CRecord record)
			{
				Table = null;
				Record = null;
				Content = null;
				SizeMax = 0;
				SizeMin = 0;
				UserResizable = EAllow.DEFAULT;
				UserAdjustable = EAllow.DEFAULT;
				_Visible = true;
				_Size = 0;
				Table = null;
				Record = record;
				Content = record.TopLevelContent();
			}

			public CGrid(CGrid i, CRecord record)
			{
				Table = null;
				Record = null;
				Content = null;
				SizeMax = 0;
				SizeMin = 0;
				UserResizable = EAllow.DEFAULT;
				UserAdjustable = EAllow.DEFAULT;
				_Visible = true;
				_Size = 0;
				Table = null;
				Record = record;
				Content = record.TopLevelContent();
				SizeMin = i.SizeMin;
				SizeMax = i.SizeMax;
				UserResizable = i.UserResizable;
				UserAdjustable = i.UserAdjustable;
				Visible = i.Visible;
				Size = i.Size;
			}

			public static Rectangle GetRectangle(CGrids cols, CGrids rows, CRegion region, int top_row, int top_col)
			{
				int size = rows.GetSize(region.Row, region.Rows);
				Rectangle result = default(Rectangle);
				if (size == 0)
				{
					return result;
				}
				int size2 = cols.GetSize(region.Col, region.Cols);
				if (size2 == 0)
				{
					return result;
				}
				checked
				{
					int y = top_row + rows.GetLocation(region.Row);
					int x = top_col + cols.GetLocation(region.Col);
					return new Rectangle(x, y, size2, size);
				}
			}

			public int Regularize(int size)
			{
				if (SizeMin > 0 && size < SizeMin)
				{
					return SizeMin;
				}
				if (SizeMax > 0 && size > SizeMax)
				{
					return SizeMax;
				}
				return size;
			}

			private void update()
			{
				//Discarded unreachable code: IL_002c
				if (Table != null)
				{
					using (Table.RenderBlock())
					{
						Table.LayoutCacheInvalid = true;
					}
				}
				else if (Content != null)
				{
					using (Content.Table.RenderBlock())
					{
						Content.TopLevelContent().LayoutCacheInvalid = true;
					}
				}
			}

			public int Index()
			{
				if (Table != null)
				{
					return Table.Cols.IndexOf(this);
				}
				return Record.Rows.IndexOf(this);
			}
		}

		public class CGrids : List<CGrid>
		{
			public CGrids()
			{
			}

			public CGrids(CGrids grids, CRecord record)
			{
				foreach (CGrid grid in grids)
				{
					Add(new CGrid(grid, record));
				}
			}

			public int GetLocation(int pos)
			{
				int num = 0;
				checked
				{
					int num2 = pos - 1;
					for (int i = 0; i <= num2; i++)
					{
						if (this[i].Visible)
						{
							num += this[i].Size;
						}
					}
					return num;
				}
			}

			public int GetSize(int pos, int size)
			{
				int num = 0;
				checked
				{
					int num2 = size - 1;
					for (int i = 0; i <= num2; i++)
					{
						if (this[pos + i].Visible)
						{
							num += this[pos + i].Size;
						}
					}
					return num;
				}
			}

			public void SetSize(params int[] v)
			{
				checked
				{
					int num = v.Length - 1;
					for (int i = 0; i <= num; i++)
					{
						if (i < Count)
						{
							this[i].Size = v[i];
						}
					}
				}
			}

			public int GetSizeByEntireSize(int region_start, int region_size, int position, int entireSize)
			{
				return checked(entireSize - (GetSize(region_start, region_size) - GetSize(position, 1)) + 2);
			}
		}

		public class CRange
		{
			public CContent TopContent;

			public int StartCol;

			public int StartRow;

			public int EndCol;

			public int EndRow;

			private List<List<CField>> _FieldMatrix;

			public CRange(CContent topContent, int startRow, int startCol, int endRow, int endCol)
			{
				_FieldMatrix = null;
				TopContent = topContent;
				StartCol = startCol;
				StartRow = startRow;
				EndCol = endCol;
				EndRow = endRow;
			}

			public int LeftCol()
			{
				if (StartCol < EndCol)
				{
					return StartCol;
				}
				return EndCol;
			}

			public int RightCol()
			{
				if (StartCol > EndCol)
				{
					return StartCol;
				}
				return EndCol;
			}

			public int TopRow()
			{
				if (StartRow < EndRow)
				{
					return StartRow;
				}
				return EndRow;
			}

			public int BottomRow()
			{
				if (StartRow > EndRow)
				{
					return StartRow;
				}
				return EndRow;
			}

			public int Cols()
			{
				return checked(Math.Abs(EndCol - StartCol) + 1);
			}

			public int Rows()
			{
				return checked(Math.Abs(EndRow - StartRow) + 1);
			}

			public bool IsInclude(CField field)
			{
				if (field.TopLevelContent() != TopContent || field.Desc.Layout == null)
				{
					return false;
				}
				int num = field.Table().LayoutCache.SelectableColsRev[field.Desc.Layout.Col];
				if ((num < LeftCol()) | (num > RightCol()))
				{
					return false;
				}
				int num2 = field.Record.LayoutCache.SelectableRowsRev[field.Desc.Layout.Row];
				if ((num2 < TopRow()) | (num2 > BottomRow()))
				{
					return false;
				}
				return true;
			}

			public List<List<CField>> FieldMatrix()
			{
				checked
				{
					if (_FieldMatrix == null)
					{
						_FieldMatrix = new List<List<CField>>();
						int num = TopRow();
						int num2 = BottomRow();
						for (int i = num; i <= num2; i++)
						{
							List<CField> list = new List<CField>();
							int num3 = LeftCol();
							int num4 = RightCol();
							for (int j = num3; j <= num4; j++)
							{
								list.Add(null);
							}
							CGrid cGrid = TopContent.LayoutCache.SelectableRows[i];
							int num5 = cGrid.Index();
							foreach (CField value in cGrid.Record.Fields.Values)
							{
								if (value.Desc.Layout != null && value.TopRow().Index() == num5 && TopContent.Table.Cols[value.Desc.Layout.Col].Visible)
								{
									int num6 = TopContent.Table.LayoutCache.SelectableColsRev[value.LeftCol().Index()] - LeftCol();
									if ((num6 >= 0) & (num6 < list.Count))
									{
										list[num6] = value;
									}
								}
							}
							cGrid = null;
							_FieldMatrix.Add(list);
						}
					}
					return _FieldMatrix;
				}
			}

			public Rectangle EndRect()
			{
				CLayoutCache layoutCache = TopContent.Table.LayoutCache;
				int num = layoutCache.SelectableColsPos[EndCol];
				int size = layoutCache.SelectableCols[EndCol].Size;
				layoutCache = null;
				CContent.CLayoutCache layoutCache2 = TopContent.LayoutCache;
				int num2 = layoutCache2.SelectableRowsPos[EndRow];
				int size2 = layoutCache2.SelectableRows[EndRow].Size;
				layoutCache2 = null;
				return checked(new Rectangle(num - size, num2 - size2, size, size2));
			}

			public void ScrollToEnd()
			{
				if (TopContent == TopContent.Table.Content)
				{
					TopContent.Table.ScrollTo(EndRect());
				}
			}

			public void FieldsClear()
			{
				//Discarded unreachable code: IL_00a6, IL_00b2
				using (TopContent.Table.RenderBlock())
				{
					using (TopContent.Table.EditBlock())
					{
						foreach (List<CField> item in FieldMatrix())
						{
							foreach (CField item2 in item)
							{
								if (item2 != null && item2.Focusable() && item2.Editable().Equals(EAllow.ALLOW))
								{
									item2.Clear();
								}
							}
						}
					}
				}
			}

			public void ClipboardCopy()
			{
				StringBuilder stringBuilder = new StringBuilder();
				foreach (List<CField> item in FieldMatrix())
				{
					bool flag = false;
					foreach (CField item2 in item)
					{
						if (flag)
						{
							stringBuilder.Append("\t");
						}
						if (item2 != null)
						{
							stringBuilder.Append(_SanitizeClipboard(item2.Desc.Provider.Clipboard));
						}
						flag = true;
					}
					stringBuilder.Append("\r\n");
				}
				string text = stringBuilder.ToString();
				if (string.IsNullOrEmpty(text))
				{
					Clipboard.Clear();
				}
				else
				{
					Clipboard.SetText(text);
				}
			}
		}

		public class CRecord
		{
			public delegate void CreatedEventHandler(CRecord record);

			public delegate void AddedEventHandler(CRecord record);

			public delegate void MovedEventHandler(CRecord record);

			public delegate void RemovedEventHandler(CRecord record);

			public delegate void EnterEventHandler(CRecord record);

			public class CLayoutCache
			{
				public int Index;

				public bool Visible;

				public int Top;

				public int RowsSize;

				public int EntireRowsSize;

				public CRecord PrevRecord;

				public CRecord NextRecord;

				public List<int> SelectableRowsRev;

				[DebuggerNonUserCode]
				public CLayoutCache()
				{
				}
			}

			private class _CTabOrderComparer : IComparer<CField>
			{
				[DebuggerNonUserCode]
				public _CTabOrderComparer()
				{
				}

				public int Compare(CField x, CField y)
				{
					if (x.Desc.Provider.TabOrder != y.Desc.Provider.TabOrder)
					{
						return 0 - ((x.Desc.Provider.TabOrder < y.Desc.Provider.TabOrder) ? 1 : 0);
					}
					return 0 - ((Operators.CompareString(x.Key.ToString(), y.Key.ToString(), TextCompare: false) < 0) ? 1 : 0);
				}

				int IComparer<CField>.Compare(CField x, CField y)
				{
					//ILSpy generated this explicit interface implementation from .override directive in Compare
					return this.Compare(x, y);
				}
			}

			private CreatedEventHandler CreatedEvent;

			private AddedEventHandler AddedEvent;

			private MovedEventHandler MovedEvent;

			private RemovedEventHandler RemovedEvent;

			private EnterEventHandler EnterEvent;

			public CGrids Rows;

			public Dictionary<object, CField> Fields;

			public CContent Content;

			public CContent Child;

			public CLayoutCache LayoutCache;

			private CSetting _Setting;

			private CBorderSetting _BorderSetting;

			private bool _Visible;

			public CSetting Setting
			{
				get
				{
					if (_Setting == null)
					{
						_Setting = new CSetting();
					}
					return _Setting;
				}
				set
				{
					using (Table().RenderBlock())
					{
						_Setting = value;
					}
				}
			}

			public CBorderSetting BorderSetting
			{
				get
				{
					if (_BorderSetting == null)
					{
						_BorderSetting = new CBorderSetting();
					}
					return _BorderSetting;
				}
				set
				{
					_BorderSetting = value;
				}
			}

			public bool Visible
			{
				get
				{
					return _Visible;
				}
				set
				{
					using (Table().RenderBlock())
					{
						if (!value)
						{
							focusEscape();
						}
						_Visible = value;
						TopLevelContent().LayoutCacheInvalid = true;
					}
				}
			}

			public event CreatedEventHandler Created
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					CreatedEvent = (CreatedEventHandler)Delegate.Combine(CreatedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					CreatedEvent = (CreatedEventHandler)Delegate.Remove(CreatedEvent, value);
				}
			}

			public event AddedEventHandler Added
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					AddedEvent = (AddedEventHandler)Delegate.Combine(AddedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					AddedEvent = (AddedEventHandler)Delegate.Remove(AddedEvent, value);
				}
			}

			public event MovedEventHandler Moved
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					MovedEvent = (MovedEventHandler)Delegate.Combine(MovedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					MovedEvent = (MovedEventHandler)Delegate.Remove(MovedEvent, value);
				}
			}

			public event RemovedEventHandler Removed
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					RemovedEvent = (RemovedEventHandler)Delegate.Combine(RemovedEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					RemovedEvent = (RemovedEventHandler)Delegate.Remove(RemovedEvent, value);
				}
			}

			public event EnterEventHandler Enter
			{
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				add
				{
					EnterEvent = (EnterEventHandler)Delegate.Combine(EnterEvent, value);
				}
				[MethodImpl(MethodImplOptions.Synchronized)]
				[DebuggerNonUserCode]
				remove
				{
					EnterEvent = (EnterEventHandler)Delegate.Remove(EnterEvent, value);
				}
			}

			internal CRecord(CContent content, CRecordProvider recordProvider)
			{
				Rows = new CGrids();
				Fields = new Dictionary<object, CField>();
				LayoutCache = new CLayoutCache();
				_Setting = null;
				_BorderSetting = null;
				_Visible = true;
				using (content.Table.RenderBlock())
				{
					Content = content;
					if (recordProvider != null)
					{
						CRecordProvider cRecordProvider = recordProvider;
						Rows.AddRange(new CGrids(cRecordProvider.Rows, this));
						foreach (object key in cRecordProvider.FieldDescs.Keys)
						{
							object objectValue = RuntimeHelpers.GetObjectValue(key);
							_addField(RuntimeHelpers.GetObjectValue(objectValue), cRecordProvider.FieldDescs[RuntimeHelpers.GetObjectValue(objectValue)]);
						}
						foreach (object key2 in cRecordProvider.FieldDescs.Keys)
						{
							object objectValue2 = RuntimeHelpers.GetObjectValue(key2);
							Fields[RuntimeHelpers.GetObjectValue(objectValue2)].Desc.Provider.FieldInitialize(Fields[RuntimeHelpers.GetObjectValue(objectValue2)]);
						}
						if (cRecordProvider.ChildRecordProvider != null)
						{
							CreateChild(cRecordProvider.ChildRecordProvider);
						}
						cRecordProvider = null;
					}
				}
			}

			public bool HasSetting()
			{
				return _Setting != null;
			}

			public void ClearSetting()
			{
				Setting = null;
			}

			public bool HasBorderSetting()
			{
				return _BorderSetting != null;
			}

			public void ClearBorderSetting()
			{
				_BorderSetting = null;
			}

			private bool isRectAppeared(Rectangle rect)
			{
				if (rect.Height <= 0 || rect.Width <= 0 || rect.Left >= Table().Width || rect.Top >= Table().Height || rect.Right < 0 || rect.Bottom < 0)
				{
					return false;
				}
				return true;
			}

			private void createRenderCache(CField field, Rectangle rect)
			{
				CRenderCache.CField cField = new CRenderCache.CField();
				cField.Rect = rect;
				cField.Field = field;
				Table().RenderCache.Fields.Add(cField);
				if (field == Table().FocusField)
				{
					Table().RenderCache.FocusField = cField;
				}
			}

			internal void render(Graphics g, int top_row, int top_col, bool alter, bool @fixed, CBorder border, CVerticalMerge verticalMerge)
			{
				checked
				{
					foreach (CField value in Fields.Values)
					{
						if (value.Desc.Layout != null && ((@fixed & (value.Desc.Layout.Col < Table().FixedCols)) | (!@fixed & (value.Desc.Layout.Col + value.Desc.Layout.Cols > Table().FixedCols))))
						{
							Rectangle rectangle = value.GetRectangle(top_row + LayoutCache.Top, top_col);
							CVerticalMerge.CTranslateResult cTranslateResult = verticalMerge.Translate(value, rectangle, alter, top_row, top_col);
							if (isRectAppeared(cTranslateResult.Rect))
							{
								cTranslateResult.Field.Desc.Provider.Render(g, cTranslateResult.Field, cTranslateResult.Rect, cTranslateResult.Alter);
							}
							if (isRectAppeared(rectangle))
							{
								value.Desc.Provider.SetBorder(value, border, cTranslateResult.Merged);
								createRenderCache(value, rectangle);
							}
							cTranslateResult = null;
						}
					}
				}
			}

			public CContent CreateChild()
			{
				return CreateChild(null);
			}

			public CContent CreateChild(CRecordProvider recordProvider)
			{
				Child = new CContent();
				Child.ParentRecord = this;
				Child.Table = Child.ParentRecord.Content.Table;
				if (recordProvider != null)
				{
					Child.SetRecordProvider(recordProvider);
				}
				return Child;
			}

			public void RemoveChild()
			{
				using (Table().RenderBlock())
				{
					Child = null;
					TopLevelContent().LayoutCacheInvalid = true;
				}
			}

			public void CreateRows(int rows_count)
			{
				while (Rows.Count < rows_count)
				{
					using (Table().RenderBlock())
					{
						Rows.Add(new CGrid(this));
						Content.TopLevelContent().LayoutCacheInvalid = true;
					}
				}
			}

			public CField AddField(object key, IFieldProvider fieldProvider)
			{
				return AddField(RuntimeHelpers.GetObjectValue(key), fieldProvider, null);
			}

			public CField AddField(object key, IFieldProvider fieldProvider, CGrid.CRegion layout)
			{
				return AddField(RuntimeHelpers.GetObjectValue(key), new CFieldDesc(fieldProvider, layout));
			}

			public CField AddField(object key, CFieldDesc fieldDesc)
			{
				using (Table().RenderBlock())
				{
					if ((fieldDesc.Layout != null) & fieldDesc.Provider.Focusable())
					{
						int num = -1;
						foreach (CField value in Fields.Values)
						{
							if (num < value.Desc.Provider.TabOrder)
							{
								num = value.Desc.Provider.TabOrder;
							}
						}
						fieldDesc.Provider.TabOrder = checked(num + 1);
					}
					CField cField = _addField(RuntimeHelpers.GetObjectValue(key), fieldDesc);
					cField?.Desc.Provider.FieldInitialize(cField);
					return cField;
				}
			}

			private CField _addField(object key, CFieldDesc fieldDesc)
			{
				checked
				{
					if (fieldDesc.Layout != null)
					{
						Table().CreateCols(fieldDesc.Layout.Col + fieldDesc.Layout.Cols);
						CreateRows(fieldDesc.Layout.Row + fieldDesc.Layout.Rows);
					}
					CField cField = fieldDesc.Provider.CreateField();
					cField.Init(RuntimeHelpers.GetObjectValue(key), this, fieldDesc);
					if (cField != null)
					{
						Fields.Add(RuntimeHelpers.GetObjectValue(key), cField);
					}
					return cField;
				}
			}

			public void RemoveField(object key)
			{
				using (Table().RenderBlock())
				{
					Fields.Remove(RuntimeHelpers.GetObjectValue(key));
				}
			}

			internal void updateCache(ref int top, ref CRecord prevRecord, bool visible, int index, ref int height)
			{
				LayoutCache.Index = index;
				LayoutCache.Visible = visible;
				LayoutCache.Top = top;
				LayoutCache.RowsSize = 0;
				LayoutCache.EntireRowsSize = 0;
				LayoutCache.PrevRecord = null;
				LayoutCache.NextRecord = null;
				LayoutCache.SelectableRowsRev = new List<int>();
				bool flag = (visible && Visible) ? true : false;
				checked
				{
					if (flag)
					{
						if (prevRecord != null)
						{
							LayoutCache.PrevRecord = prevRecord;
							prevRecord.LayoutCache.NextRecord = this;
						}
						prevRecord = this;
						foreach (CGrid row in Rows)
						{
							CContent.CLayoutCache layoutCache = TopLevelContent().LayoutCache;
							if (row.Visible)
							{
								CLayoutCache layoutCache2 = LayoutCache;
								layoutCache2.RowsSize += row.Size;
								height += row.Size;
								layoutCache.SelectableRows.Add(row);
								layoutCache.SelectableRowsPos.Add(height);
							}
							LayoutCache.SelectableRowsRev.Add(layoutCache.SelectableRows.Count - 1);
							layoutCache = null;
						}
						LayoutCache.EntireRowsSize = LayoutCache.RowsSize;
						top += LayoutCache.RowsSize;
						if (Content.LayoutCache.TopRecord == null)
						{
							Content.LayoutCache.TopRecord = this;
						}
						Content.LayoutCache.LastRecord = this;
					}
					if (Child != null)
					{
						Child.updateLayoutCache(ref top, ref prevRecord, flag, ref height);
						CLayoutCache layoutCache2 = LayoutCache;
						layoutCache2.EntireRowsSize += Child.LayoutCache.RowsSize;
					}
				}
			}

			public bool IsChild(CField field)
			{
				if (field.Record == this)
				{
					return true;
				}
				return IsChild(field.Record);
			}

			public bool IsChild(CContent content)
			{
				if (content.ParentRecord == this)
				{
					return true;
				}
				return IsChild(content.ParentRecord);
			}

			public bool IsChild(CRecord record)
			{
				for (CRecord parentRecord = record.Content.ParentRecord; parentRecord != null; parentRecord = parentRecord.Content.ParentRecord)
				{
					if (parentRecord == this)
					{
						return true;
					}
				}
				return false;
			}

			public CGrid Row(object key)
			{
				return Rows[Fields[RuntimeHelpers.GetObjectValue(key)].Desc.Layout.BottomRow()];
			}

			public CGrid Col(object key)
			{
				return Table().Cols[Fields[RuntimeHelpers.GetObjectValue(key)].Desc.Layout.RightCol()];
			}

			public CField FindField(int row, int col)
			{
				return FindField(new CGrid.CPoint(row, col));
			}

			public CField FindField(CGrid.CPoint point)
			{
				foreach (CField value in Fields.Values)
				{
					if (value.Desc.Layout != null && value.Desc.Layout.Contains(point))
					{
						return value;
					}
				}
				return null;
			}

			public UTable Table()
			{
				return Content.Table;
			}

			public CContent TopLevelContent()
			{
				return Content.TopLevelContent();
			}

			public CRecord TopLevelRecord()
			{
				CRecord cRecord = Content.TopLevelRecord();
				if (cRecord != null)
				{
					return cRecord;
				}
				return this;
			}

			public void FlatBorder(object key1, object key2)
			{
				Dictionary<object, CFieldDesc> dictionary = new Dictionary<object, CFieldDesc>();
				foreach (object key3 in Fields.Keys)
				{
					object objectValue = RuntimeHelpers.GetObjectValue(key3);
					dictionary.Add(RuntimeHelpers.GetObjectValue(objectValue), Fields[RuntimeHelpers.GetObjectValue(objectValue)].Desc);
				}
				CFieldProvider.FlatBorder(dictionary, RuntimeHelpers.GetObjectValue(key1), RuntimeHelpers.GetObjectValue(key2));
			}

			public Rectangle GetRectangle()
			{
				return new Rectangle(0, LayoutCache.Top, Table().LayoutCache.ColsSize, LayoutCache.RowsSize);
			}

			public void Visualize()
			{
				using (Table().RenderBlock())
				{
					Visible = true;
					Content.Visualize();
				}
			}

			internal List<CField> tabOrderdList()
			{
				List<CField> list = new List<CField>();
				foreach (CField value in Fields.Values)
				{
					if (value.Desc.Provider.Focusable())
					{
						list.Add(value);
					}
				}
				list.Sort(new _CTabOrderComparer());
				return list;
			}

			public CField DefaultField()
			{
				foreach (CField item in tabOrderdList())
				{
					if (item.Focusable() && item.DynamicSetting().TabStop() == ETabStop.STOP)
					{
						return item;
					}
				}
				return null;
			}

			public CField LastField()
			{
				List<CField> list = tabOrderdList();
				checked
				{
					for (int i = list.Count - 1; i >= 0; i += -1)
					{
						if (list[i].Focusable() && list[i].DynamicSetting().TabStop() == ETabStop.STOP)
						{
							return list[i];
						}
					}
					return null;
				}
			}

			internal void focusEscape()
			{
				if (Table().FocusField == null)
				{
					return;
				}
				if (Table().FocusField.Record == this)
				{
					for (CRecord prevRecord = LayoutCache.PrevRecord; prevRecord != null; prevRecord = prevRecord.LayoutCache.PrevRecord)
					{
						CField cField = prevRecord.DefaultField();
						if (cField != null)
						{
							Table().FocusField = cField;
							return;
						}
					}
					for (CRecord prevRecord = LayoutCache.NextRecord; prevRecord != null; prevRecord = prevRecord.LayoutCache.NextRecord)
					{
						CField cField2 = prevRecord.DefaultField();
						if (cField2 != null)
						{
							Table().FocusField = cField2;
							return;
						}
					}
					Table().FocusField = null;
				}
				else if (IsChild(Table().FocusField))
				{
					if (Child != null)
					{
						Child.focusEscape();
					}
					else
					{
						Table().FocusField = null;
					}
				}
			}

			public int Index()
			{
				return Content.Records.IndexOf(this);
			}

			public void Clear()
			{
				Clear(recursive: true);
			}

			public void Clear(bool recursive)
			{
				using (Table().RenderBlock())
				{
					foreach (CField value in Fields.Values)
					{
						value.Clear();
					}
					if (recursive & (Child != null))
					{
						Child.Clear(recursive);
					}
				}
			}

			public CDynamicBorderSetting DynamicBorderSetting()
			{
				return new CDynamicBorderSetting(this);
			}

			internal void raiseCreated()
			{
				CreatedEvent?.Invoke(this);
				Table().raiseRecordCreated(this);
			}

			internal void raiseAdded()
			{
				AddedEvent?.Invoke(this);
				Table().raiseRecordAdded(this);
			}

			internal void raiseMoved()
			{
				MovedEvent?.Invoke(this);
				Table().raiseRecordMoved(this);
			}

			public void raiseRemoved()
			{
				RemovedEvent?.Invoke(this);
				Table().raiseRecordRemoved(this);
			}

			public void raiseEnter()
			{
				EnterEvent?.Invoke(this);
				Table().raiseRecordEnter(this);
			}
		}

		public class CRecordProvider
		{
			public Dictionary<object, CFieldDesc> FieldDescs;

			public CGrids Rows;

			public CRecordProvider ChildRecordProvider;

			public int ColsCount;

			public int LastTabOrder;

			public int MaxRecordCount;

			public bool LimitIncludeFocusColor;

			private CSetting _Setting;

			private CBorderSetting _BorderSetting;

			public CSetting Setting
			{
				get
				{
					if (_Setting == null)
					{
						_Setting = new CSetting();
					}
					return _Setting;
				}
				set
				{
					_Setting = value;
				}
			}

			public CBorderSetting BorderSetting
			{
				get
				{
					if (_BorderSetting == null)
					{
						_BorderSetting = new CBorderSetting();
					}
					return _BorderSetting;
				}
				set
				{
					_BorderSetting = value;
				}
			}

			public CRecordProvider()
			{
				FieldDescs = new Dictionary<object, CFieldDesc>();
				Rows = new CGrids();
				ChildRecordProvider = null;
				ColsCount = 0;
				LastTabOrder = -1;
				MaxRecordCount = 0;
				LimitIncludeFocusColor = false;
				_Setting = null;
				_BorderSetting = null;
			}

			public bool HasSetting()
			{
				return _Setting != null;
			}

			public void ClearSetting()
			{
				Setting = null;
			}

			public bool HasBorderSetting()
			{
				return _BorderSetting != null;
			}

			public void ClearBorderSetting()
			{
				_BorderSetting = null;
			}

			public CFieldDesc AddField(object key, IFieldProvider provider)
			{
				return AddField(RuntimeHelpers.GetObjectValue(key), provider, null);
			}

			public CFieldDesc AddField(object key, IFieldProvider provider, CGrid.CRegion layout)
			{
				return _AddField<CFieldDesc>(RuntimeHelpers.GetObjectValue(key), provider, layout);
			}

			protected TDesc _AddField<TDesc>(object key, IFieldProvider provider, CGrid.CRegion layout) where TDesc : CFieldDesc, new()
			{
				TDesc val = new TDesc();
				val.Initialize(provider, layout);
				checked
				{
					if (layout != null)
					{
						while (Rows.Count < layout.Row + layout.Rows)
						{
							Rows.Add(new CGrid());
						}
						if (ColsCount < layout.Col + layout.Cols)
						{
							ColsCount = layout.Col + layout.Cols;
						}
						if (provider.Focusable())
						{
							LastTabOrder++;
							provider.TabOrder = LastTabOrder;
						}
					}
					FieldDescs.Add(RuntimeHelpers.GetObjectValue(key), val);
					return val;
				}
			}

			public void SetChildRecordProvider(CRecordProvider recordProvider)
			{
				ChildRecordProvider = recordProvider;
			}

			public CGrid.CRegion Layout(object key)
			{
				return FieldDescs[RuntimeHelpers.GetObjectValue(key)].Layout;
			}

			public CGrid.CRegion Layout(object key1, object key2)
			{
				return FieldDescs[RuntimeHelpers.GetObjectValue(key1)].Layout.Union(FieldDescs[RuntimeHelpers.GetObjectValue(key2)].Layout);
			}

			public void FlatBorder(object key1, object key2)
			{
				CFieldProvider.FlatBorder(FieldDescs, RuntimeHelpers.GetObjectValue(key1), RuntimeHelpers.GetObjectValue(key2));
			}
		}

		public class CRenderCache
		{
			public class CCol
			{
				public int From;

				public int To;

				public bool Appeared;

				[DebuggerNonUserCode]
				public CCol()
				{
				}
			}

			public class CField
			{
				public Rectangle Rect;

				public UTable.CField Field;

				[DebuggerNonUserCode]
				public CField()
				{
				}
			}

			public CField FocusField;

			public List<CCol> Cols;

			public List<CField> Fields;

			public CRenderCache()
			{
				Cols = new List<CCol>();
				Fields = new List<CField>();
			}

			public CField FindField(Point location)
			{
				checked
				{
					for (int i = Fields.Count - 1; i >= 0; i += -1)
					{
						CField cField = Fields[i];
						if (cField.Rect.Contains(location))
						{
							return cField;
						}
					}
					return null;
				}
			}

			public CField FindField(UTable.CField field)
			{
				foreach (CField field2 in Fields)
				{
					if (field2.Field == field)
					{
						return field2;
					}
				}
				return null;
			}
		}

		public class CRenderBlock : IDisposable
		{
			public UTable table;

			private bool disposedValue;

			public CRenderBlock(UTable table)
			{
				disposedValue = false;
				this.table = table;
			}

			protected virtual void Dispose(bool disposing)
			{
				if (!disposedValue)
				{
					table._RenderBlock = null;
					table.Render();
				}
				disposedValue = true;
			}

			public void Dispose()
			{
				Dispose(disposing: true);
				GC.SuppressFinalize(this);
			}

			void IDisposable.Dispose()
			{
				//ILSpy generated this explicit interface implementation from .override directive in Dispose
				this.Dispose();
			}
		}

		private class CHScrollBar : HScrollBar
		{
			public CHScrollBar()
			{
				SetStyle(ControlStyles.Selectable, value: false);
			}
		}

		private class CVScrollBar : VScrollBar
		{
			public CVScrollBar()
			{
				SetStyle(ControlStyles.Selectable, value: false);
			}
		}

		public enum EVAlign
		{
			DEFAULT,
			TOP,
			MIDDLE,
			BOTTOM
		}

		public enum EHAlign
		{
			DEFAULT,
			LEFT,
			MIDDLE,
			RIGHT
		}

		public enum EAllow
		{
			DEFAULT,
			ALLOW,
			DISABLE
		}

		public enum ETabStop
		{
			DEFAULT,
			STOP,
			NOTSTOP
		}

		public enum ETextWrap
		{
			DEFAULT,
			WRAP,
			NOWRAP
		}

		public enum EScrollUnit
		{
			FIELD,
			RECORD
		}

		public enum ERangeUnit
		{
			FIELD,
			RECORD
		}

		public enum ECaptionStyle
		{
			DEFAULT,
			METALLIC,
			GRADIENT,
			FLAT
		}

		public enum EStay
		{
			DEFAULT,
			NOT_STAY,
			STAY
		}

		public enum EBound
		{
			DEFAULT,
			NOT_BOUND,
			BOUND
		}

		public enum EBorderStyle
		{
			DEFAULT,
			SOLID,
			BOLD,
			DOT,
			DASH,
			DASHDOT,
			DASHDOTDOT
		}

		public class CSetting : ICloneable
		{
			public Font Font;

			public Font CaptionFont;

			public Color ForeColor;

			public Color BackColor;

			public Color AlterBackColor;

			public Color CaptionForeColor;

			public Color CaptionBackColor;

			public Color FocusForeColor;

			public Color FocusBackColor;

			public Color FocusRecordForeColor;

			public Color FocusRecordBackColor;

			public Color FocusCaptionForeColor;

			public Color FocusCaptionBackColor;

			public Color FocusRecordCaptionForeColor;

			public Color FocusRecordCaptionBackColor;

			public Color IncludeFocusRecordCaptionForeColor;

			public Color IncludeFocusRecordCaptionBackColor;

			public Color RangedForeColor;

			public Color RangedBackColor;

			public Color DraggingForeColor;

			public Color DraggingBackColor;

			public Color ButtonBackColor;

			public EHAlign HorizontalAlignment;

			public EVAlign VerticalAlignment;

			public EBound LeftBound;

			public EBound TopBound;

			public EAllow Editable;

			public ETabStop TabStop;

			public ETextWrap TextWrap;

			public ECaptionStyle CaptionStyle;

			public EStay StayVertical;

			public EStay StayHorizontal;

			public StringFormatFlags StringFormatFlags;

			public Cursor Cursor;

			public CSetting()
			{
				Font = null;
				CaptionFont = null;
				ForeColor = Color.Empty;
				BackColor = Color.Empty;
				AlterBackColor = Color.Empty;
				CaptionForeColor = Color.Empty;
				CaptionBackColor = Color.Empty;
				FocusForeColor = Color.Empty;
				FocusBackColor = Color.Empty;
				FocusRecordForeColor = Color.Empty;
				FocusRecordBackColor = Color.Empty;
				FocusCaptionForeColor = Color.Empty;
				FocusCaptionBackColor = Color.Empty;
				FocusRecordCaptionForeColor = Color.Empty;
				FocusRecordCaptionBackColor = Color.Empty;
				IncludeFocusRecordCaptionForeColor = Color.Empty;
				IncludeFocusRecordCaptionBackColor = Color.Empty;
				RangedForeColor = Color.Empty;
				RangedBackColor = Color.Empty;
				DraggingForeColor = Color.Empty;
				DraggingBackColor = Color.Empty;
				ButtonBackColor = Color.Empty;
				HorizontalAlignment = EHAlign.DEFAULT;
				VerticalAlignment = EVAlign.DEFAULT;
				LeftBound = EBound.DEFAULT;
				TopBound = EBound.DEFAULT;
				Editable = EAllow.DEFAULT;
				TabStop = ETabStop.DEFAULT;
				TextWrap = ETextWrap.DEFAULT;
				CaptionStyle = ECaptionStyle.DEFAULT;
				StayVertical = EStay.DEFAULT;
				StayHorizontal = EStay.DEFAULT;
				StringFormatFlags = (StringFormatFlags)0;
				Cursor = null;
			}

			public object Clone()
			{
				CSetting cSetting = new CSetting();
				cSetting.Font = Font;
				cSetting.CaptionFont = CaptionFont;
				cSetting.ForeColor = ForeColor;
				cSetting.BackColor = BackColor;
				cSetting.AlterBackColor = AlterBackColor;
				cSetting.CaptionForeColor = CaptionForeColor;
				cSetting.CaptionBackColor = CaptionBackColor;
				cSetting.FocusForeColor = FocusForeColor;
				cSetting.FocusBackColor = FocusBackColor;
				cSetting.FocusRecordForeColor = FocusRecordForeColor;
				cSetting.FocusRecordBackColor = FocusRecordBackColor;
				cSetting.FocusCaptionForeColor = FocusCaptionForeColor;
				cSetting.FocusCaptionBackColor = FocusCaptionBackColor;
				cSetting.IncludeFocusRecordCaptionForeColor = IncludeFocusRecordCaptionForeColor;
				cSetting.IncludeFocusRecordCaptionBackColor = IncludeFocusRecordCaptionBackColor;
				cSetting.FocusRecordCaptionForeColor = FocusRecordCaptionForeColor;
				cSetting.FocusRecordCaptionBackColor = FocusRecordCaptionBackColor;
				cSetting.RangedForeColor = RangedForeColor;
				cSetting.RangedBackColor = RangedBackColor;
				cSetting.ButtonBackColor = ButtonBackColor;
				cSetting.HorizontalAlignment = HorizontalAlignment;
				cSetting.VerticalAlignment = VerticalAlignment;
				cSetting.LeftBound = LeftBound;
				cSetting.TopBound = TopBound;
				cSetting.Editable = Editable;
				cSetting.TabStop = TabStop;
				cSetting.TextWrap = TextWrap;
				cSetting.StayVertical = StayVertical;
				cSetting.StayHorizontal = StayHorizontal;
				cSetting.CaptionStyle = CaptionStyle;
				cSetting.StringFormatFlags = StringFormatFlags;
				cSetting.Cursor = Cursor;
				return cSetting;
			}

			object ICloneable.Clone()
			{
				//ILSpy generated this explicit interface implementation from .override directive in Clone
				return this.Clone();
			}
		}

		public class CBorderSetting
		{
			public Color BorderColor;

			public EBorderStyle BorderStyle;

			public Color CaptionBorderColor;

			public EBorderStyle CaptionBorderStyle;

			public Color RecordBorderColor;

			public EBorderStyle RecordBorderStyle;

			public CBorderSetting()
			{
				BorderColor = Color.Empty;
				BorderStyle = EBorderStyle.DEFAULT;
				CaptionBorderColor = Color.Empty;
				CaptionBorderStyle = EBorderStyle.DEFAULT;
				RecordBorderColor = Color.Empty;
				RecordBorderStyle = EBorderStyle.DEFAULT;
			}
		}

		public class CGlobalSetting
		{
			public Color HeaderContentBackColor;

			public Color FooterContentBackColor;

			public Color ContentBackColor;

			public Color ResizingLineColor;

			public Font Font;

			public Font CaptionFont;

			public Color ForeColor;

			public Color BackColor;

			public Color AlterBackColor;

			public Color FocusForeColor;

			public Color FocusBackColor;

			public Color FocusRecordForeColor;

			public Color FocusRecordBackColor;

			public Color CaptionForeColor;

			public Color CaptionBackColor;

			public Color FocusCaptionForeColor;

			public Color FocusCaptionBackColor;

			public Color FocusRecordCaptionForeColor;

			public Color FocusRecordCaptionBackColor;

			public Color IncludeFocusRecordCaptionForeColor;

			public Color IncludeFocusRecordCaptionBackColor;

			public Color RangedForeColor;

			public Color RangedBackColor;

			public Color DraggingForeColor;

			public Color DraggingBackColor;

			public Color ButtonBackColor;

			public Color BorderColor;

			public EBorderStyle BorderStyle;

			public Color CaptionBorderColor;

			public EBorderStyle CaptionBorderStyle;

			public Color RecordBorderColor;

			public EBorderStyle RecordBorderStyle;

			public Color FixedColBorderColor;

			public EBorderStyle FixedColBorderStyle;

			public EHAlign HorizontalAlignment;

			public EVAlign VerticalAlignment;

			public EBound LeftBound;

			public EBound TopBound;

			public EAllow Editable;

			public EAllow UserColAdjustable;

			public EAllow UserColResizable;

			public EAllow UserSortable;

			public bool UserRangeSelectable;

			public ERangeUnit UserRangeSelectUnit;

			public bool DefaultFieldSelectableByCaptionClick;

			public bool RecordSelectableByCaptionClick;

			public bool ColSelectableByCaptionClick;

			public bool AllSelectableByCaptionClick;

			public bool RangeToggleValue;

			public ETextWrap TextWrap;

			public ECaptionStyle CaptionStyle;

			public EScrollUnit ScrollUnit;

			public EStay StayVertical;

			public EStay StayHorizontal;

			public CContent EnterContent;

			public bool FocusColorAlways;

			public bool AlterConsistent;

			public bool VScrollBarAlways;

			public bool HScrollBarAlways;

			public bool VScrollBarDisable;

			public bool HScrollBarDisable;

			public bool HScrollToEdge;

			public bool VScrollToEdge;

			public int VScrollToEdgeLevel;

			public int MouseWheelScrollRows;

			public StringFormatFlags StringFormatFlags;

			public Cursor Cursor;

			public CGlobalSetting()
			{
				HeaderContentBackColor = Color.DarkGray;
				FooterContentBackColor = Color.DarkGray;
				ContentBackColor = Color.DimGray;
				ResizingLineColor = Color.DarkGreen;
				Font = SystemFonts.DefaultFont;
				CaptionFont = SystemFonts.DefaultFont;
				ForeColor = Color.Black;
				BackColor = Color.White;
				AlterBackColor = Color.Beige;
				FocusForeColor = Color.Transparent;
				FocusBackColor = Color.LightCyan;
				FocusRecordForeColor = Color.Transparent;
				FocusRecordBackColor = Color.PeachPuff;
				CaptionForeColor = Color.Black;
				CaptionBackColor = Color.Silver;
				FocusCaptionForeColor = Color.Transparent;
				FocusCaptionBackColor = Color.CornflowerBlue;
				FocusRecordCaptionForeColor = Color.Transparent;
				FocusRecordCaptionBackColor = Color.CornflowerBlue;
				IncludeFocusRecordCaptionForeColor = Color.Transparent;
				IncludeFocusRecordCaptionBackColor = Color.Transparent;
				RangedForeColor = Color.Transparent;
				RangedBackColor = Color.LightGreen;
				DraggingForeColor = Color.Transparent;
				DraggingBackColor = Color.LightGreen;
				ButtonBackColor = Color.LightGray;
				BorderColor = Color.Gray;
				BorderStyle = EBorderStyle.DOT;
				CaptionBorderColor = Color.Gray;
				CaptionBorderStyle = EBorderStyle.SOLID;
				RecordBorderColor = Color.Gray;
				RecordBorderStyle = EBorderStyle.SOLID;
				FixedColBorderColor = Color.DimGray;
				FixedColBorderStyle = EBorderStyle.BOLD;
				HorizontalAlignment = EHAlign.LEFT;
				VerticalAlignment = EVAlign.MIDDLE;
				LeftBound = EBound.NOT_BOUND;
				TopBound = EBound.NOT_BOUND;
				Editable = EAllow.ALLOW;
				UserColAdjustable = EAllow.ALLOW;
				UserColResizable = EAllow.ALLOW;
				UserSortable = EAllow.DISABLE;
				UserRangeSelectable = false;
				UserRangeSelectUnit = ERangeUnit.FIELD;
				DefaultFieldSelectableByCaptionClick = true;
				RecordSelectableByCaptionClick = true;
				ColSelectableByCaptionClick = true;
				AllSelectableByCaptionClick = true;
				RangeToggleValue = true;
				TextWrap = ETextWrap.NOWRAP;
				CaptionStyle = ECaptionStyle.METALLIC;
				ScrollUnit = EScrollUnit.FIELD;
				StayVertical = EStay.NOT_STAY;
				StayHorizontal = EStay.NOT_STAY;
				FocusColorAlways = true;
				AlterConsistent = false;
				VScrollBarAlways = false;
				HScrollBarAlways = false;
				VScrollBarDisable = false;
				HScrollBarDisable = false;
				HScrollToEdge = false;
				VScrollToEdge = false;
				VScrollToEdgeLevel = 0;
				MouseWheelScrollRows = 1;
				StringFormatFlags = (StringFormatFlags)0;
				Cursor = Cursors.Default;
			}
		}

		public class CDynamicSetting
		{
			public CGlobalSetting GlobalSetting;

			public List<CSetting> Settings;

			public CDynamicSetting(CField field)
			{
				Settings = new List<CSetting>();
				if (field.HasSetting())
				{
					Settings.Add(field.Setting);
				}
				if (field.Desc.HasSetting2())
				{
					Settings.Add(field.Desc.Setting2);
				}
				if (field.Record.HasSetting())
				{
					Settings.Add(field.Record.Setting);
				}
				if (field.Desc.HasSetting())
				{
					Settings.Add(field.Desc.Setting);
				}
				if (field.Content().HasSetting())
				{
					Settings.Add(field.Content().Setting);
				}
				if (field.Content().RecordProvider != null && field.Content().RecordProvider.HasSetting())
				{
					Settings.Add(field.Content().RecordProvider.Setting);
				}
				GlobalSetting = field.Table().Setting;
			}

			public CDynamicSetting(CDynamicSetting @base, CSetting s)
			{
				Settings = new List<CSetting>();
				GlobalSetting = @base.GlobalSetting;
				Settings.AddRange(@base.Settings);
				Settings.Add(s);
			}

			public Font Font()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.Font != null)
					{
						return setting.Font;
					}
				}
				return GlobalSetting.Font;
			}

			public Font CaptionFont()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.CaptionFont != null)
					{
						return setting.CaptionFont;
					}
				}
				return GlobalSetting.CaptionFont;
			}

			public Color ForeColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.ForeColor != Color.Empty)
					{
						return setting.ForeColor;
					}
				}
				return GlobalSetting.ForeColor;
			}

			public Color BackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.BackColor != Color.Empty)
					{
						return setting.BackColor;
					}
				}
				return GlobalSetting.BackColor;
			}

			public Color AlterBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.AlterBackColor != Color.Empty)
					{
						return setting.AlterBackColor;
					}
				}
				return GlobalSetting.AlterBackColor;
			}

			public Color CaptionForeColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.CaptionForeColor != Color.Empty)
					{
						return setting.CaptionForeColor;
					}
				}
				return GlobalSetting.CaptionForeColor;
			}

			public Color CaptionBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.CaptionBackColor != Color.Empty)
					{
						return setting.CaptionBackColor;
					}
				}
				return GlobalSetting.CaptionBackColor;
			}

			public Color FocusForeColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.FocusForeColor != Color.Empty)
					{
						return setting.FocusForeColor;
					}
				}
				return GlobalSetting.FocusForeColor;
			}

			public Color FocusBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.FocusBackColor != Color.Empty)
					{
						return setting.FocusBackColor;
					}
				}
				return GlobalSetting.FocusBackColor;
			}

			public Color FocusRecordForeColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.FocusRecordForeColor != Color.Empty)
					{
						return setting.FocusRecordForeColor;
					}
				}
				return GlobalSetting.FocusRecordForeColor;
			}

			public Color FocusRecordBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.FocusRecordBackColor != Color.Empty)
					{
						return setting.FocusRecordBackColor;
					}
				}
				return GlobalSetting.FocusRecordBackColor;
			}

			public Color FocusCaptionForeColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.FocusCaptionForeColor != Color.Empty)
					{
						return setting.FocusCaptionForeColor;
					}
				}
				return GlobalSetting.FocusCaptionForeColor;
			}

			public Color FocusCaptionBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.FocusCaptionBackColor != Color.Empty)
					{
						return setting.FocusCaptionBackColor;
					}
				}
				return GlobalSetting.FocusCaptionBackColor;
			}

			public Color FocusRecordCaptionForeColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.FocusRecordCaptionForeColor != Color.Empty)
					{
						return setting.FocusRecordCaptionForeColor;
					}
				}
				return GlobalSetting.FocusRecordCaptionForeColor;
			}

			public Color FocusRecordCaptionBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.FocusRecordCaptionBackColor != Color.Empty)
					{
						return setting.FocusRecordCaptionBackColor;
					}
				}
				return GlobalSetting.FocusRecordCaptionBackColor;
			}

			public Color IncludeFocusRecordCaptionForeColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.IncludeFocusRecordCaptionForeColor != Color.Empty)
					{
						return setting.IncludeFocusRecordCaptionForeColor;
					}
				}
				return GlobalSetting.IncludeFocusRecordCaptionForeColor;
			}

			public Color IncludeFocusRecordCaptionBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.IncludeFocusRecordCaptionBackColor != Color.Empty)
					{
						return setting.IncludeFocusRecordCaptionBackColor;
					}
				}
				return GlobalSetting.IncludeFocusRecordCaptionBackColor;
			}

			public Color RangedForeColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.RangedForeColor != Color.Empty)
					{
						return setting.RangedForeColor;
					}
				}
				return GlobalSetting.RangedForeColor;
			}

			public Color RangedBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.RangedBackColor != Color.Empty)
					{
						return setting.RangedBackColor;
					}
				}
				return GlobalSetting.RangedBackColor;
			}

			public Color DraggingForeColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.DraggingForeColor != Color.Empty)
					{
						return setting.DraggingForeColor;
					}
				}
				return GlobalSetting.DraggingForeColor;
			}

			public Color DraggingBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.DraggingBackColor != Color.Empty)
					{
						return setting.DraggingBackColor;
					}
				}
				return GlobalSetting.DraggingBackColor;
			}

			public Color ButtonBackColor()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.ButtonBackColor != Color.Empty)
					{
						return setting.ButtonBackColor;
					}
				}
				return GlobalSetting.ButtonBackColor;
			}

			public EHAlign HorizontalAlignment()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.HorizontalAlignment != 0)
					{
						return setting.HorizontalAlignment;
					}
				}
				return GlobalSetting.HorizontalAlignment;
			}

			public EVAlign VerticalAlignment()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.VerticalAlignment != 0)
					{
						return setting.VerticalAlignment;
					}
				}
				return GlobalSetting.VerticalAlignment;
			}

			public EBound LeftBound()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.LeftBound != 0)
					{
						return setting.LeftBound;
					}
				}
				return GlobalSetting.LeftBound;
			}

			public EBound TopBound()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.TopBound != 0)
					{
						return setting.TopBound;
					}
				}
				return GlobalSetting.TopBound;
			}

			public EAllow Editable()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.Editable != 0)
					{
						return setting.Editable;
					}
				}
				return GlobalSetting.Editable;
			}

			public ETabStop TabStop()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.TabStop != 0)
					{
						return setting.TabStop;
					}
				}
				return ETabStop.STOP;
			}

			public ETextWrap TextWrap()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.TextWrap != 0)
					{
						return setting.TextWrap;
					}
				}
				return GlobalSetting.TextWrap;
			}

			public EStay StayVertical()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.StayVertical != 0)
					{
						return setting.StayVertical;
					}
				}
				return GlobalSetting.StayVertical;
			}

			public EStay StayHorizontal()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.StayHorizontal != 0)
					{
						return setting.StayHorizontal;
					}
				}
				return GlobalSetting.StayHorizontal;
			}

			public ECaptionStyle CaptionStyle()
			{
				foreach (CSetting setting in Settings)
				{
					if (setting.CaptionStyle != 0)
					{
						return setting.CaptionStyle;
					}
				}
				return GlobalSetting.CaptionStyle;
			}

			public Cursor Cursor()
			{
				foreach (CSetting setting in Settings)
				{
					if ((object)setting.Cursor != null)
					{
						return setting.Cursor;
					}
				}
				return GlobalSetting.Cursor;
			}

			public StringFormat GetStringFormat()
			{
				StringFormat stringFormat = new StringFormat();
				if (TextWrap() == ETextWrap.NOWRAP)
				{
					stringFormat.FormatFlags = StringFormatFlags.NoWrap;
				}
				switch (HorizontalAlignment())
				{
				case EHAlign.LEFT:
					stringFormat.Alignment = StringAlignment.Near;
					break;
				case EHAlign.MIDDLE:
					stringFormat.Alignment = StringAlignment.Center;
					break;
				case EHAlign.RIGHT:
					stringFormat.Alignment = StringAlignment.Far;
					break;
				}
				switch (VerticalAlignment())
				{
				case EVAlign.TOP:
					stringFormat.LineAlignment = StringAlignment.Near;
					break;
				case EVAlign.MIDDLE:
					stringFormat.LineAlignment = StringAlignment.Center;
					break;
				case EVAlign.BOTTOM:
					stringFormat.LineAlignment = StringAlignment.Far;
					break;
				}
				stringFormat.FormatFlags |= GlobalSetting.StringFormatFlags;
				foreach (CSetting setting in Settings)
				{
					stringFormat.FormatFlags |= setting.StringFormatFlags;
				}
				return stringFormat;
			}

			public StringFormat GetButtonStringFormat()
			{
				StringFormat stringFormat = new StringFormat();
				stringFormat.FormatFlags = StringFormatFlags.NoWrap;
				stringFormat.Alignment = StringAlignment.Near;
				switch (VerticalAlignment())
				{
				case EVAlign.TOP:
					stringFormat.LineAlignment = StringAlignment.Near;
					break;
				case EVAlign.MIDDLE:
					stringFormat.LineAlignment = StringAlignment.Center;
					break;
				case EVAlign.BOTTOM:
					stringFormat.LineAlignment = StringAlignment.Far;
					break;
				}
				return stringFormat;
			}
		}

		public class CDynamicBorderSetting
		{
			public CGlobalSetting GlobalSetting;

			public List<CBorderSetting> Settings;

			public CDynamicBorderSetting(CRecord record)
			{
				Settings = new List<CBorderSetting>();
				if (record.HasBorderSetting())
				{
					Settings.Add(record.BorderSetting);
				}
				if (record.Content.HasBorderSetting())
				{
					Settings.Add(record.Content.BorderSetting);
				}
				if (record.Content.RecordProvider != null && record.Content.RecordProvider.HasBorderSetting())
				{
					Settings.Add(record.Content.RecordProvider.BorderSetting);
				}
				GlobalSetting = record.Table().Setting;
			}

			public Color BorderColor()
			{
				foreach (CBorderSetting setting in Settings)
				{
					if (setting.BorderColor != Color.Empty)
					{
						return setting.BorderColor;
					}
				}
				return GlobalSetting.BorderColor;
			}

			public EBorderStyle BorderStyle()
			{
				foreach (CBorderSetting setting in Settings)
				{
					if (setting.BorderStyle != 0)
					{
						return setting.BorderStyle;
					}
				}
				return GlobalSetting.BorderStyle;
			}

			public Color CaptionBorderColor()
			{
				foreach (CBorderSetting setting in Settings)
				{
					if (setting.CaptionBorderColor != Color.Empty)
					{
						return setting.CaptionBorderColor;
					}
				}
				return GlobalSetting.CaptionBorderColor;
			}

			public EBorderStyle CaptionBorderStyle()
			{
				foreach (CBorderSetting setting in Settings)
				{
					if (setting.CaptionBorderStyle != 0)
					{
						return setting.CaptionBorderStyle;
					}
				}
				return GlobalSetting.CaptionBorderStyle;
			}

			public Color RecordBorderColor()
			{
				foreach (CBorderSetting setting in Settings)
				{
					if (setting.RecordBorderColor != Color.Empty)
					{
						return setting.RecordBorderColor;
					}
				}
				return GlobalSetting.RecordBorderColor;
			}

			public EBorderStyle RecordBorderStyle()
			{
				foreach (CBorderSetting setting in Settings)
				{
					if (setting.RecordBorderStyle != 0)
					{
						return setting.RecordBorderStyle;
					}
				}
				return GlobalSetting.RecordBorderStyle;
			}
		}

		public class CSortState
		{
			public enum EOrder
			{
				ASCEND,
				DESCEND
			}

			public CField Field;

			public EOrder Order;

			public CSortState()
			{
				Field = null;
				Order = EOrder.ASCEND;
			}
		}

		public class CLayoutCache
		{
			public int ColsSize;

			public int FixedColsSize;

			public List<CGrid> SelectableCols;

			public List<int> SelectableColsPos;

			public List<int> SelectableColsRev;

			[DebuggerNonUserCode]
			public CLayoutCache()
			{
			}
		}

		public class CAutoExtend
		{
			public bool Row;

			public bool Col;

			[DebuggerNonUserCode]
			public CAutoExtend()
			{
			}
		}

		public delegate void RecordCreatedEventHandler(CRecord record);

		public delegate void RecordAddedEventHandler(CRecord record);

		public delegate void RecordMovedEventHandler(CRecord record);

		public delegate void RecordRemovedEventHandler(CRecord record);

		public delegate void RecordEnterEventHandler(CRecord record);

		public delegate void ContentValidatedEventHandler(CContent content, object key);

		public delegate void ContentEnterEventHandler(CContent content);

		public delegate void SortedEventHandler(CContent content, object key);

		public delegate void FieldValidatingEventHandler(CField field, CancelEventArgs e);

		public delegate void FieldValidatedEventHandler(CField field);

		public delegate void FieldValueChangedEventHandler(CField field);

		public delegate void FieldEnteringEventHandler(CField field, ref bool cancel);

		public delegate void FieldEnterEventHandler(CField field);

		public delegate void FieldLeaveEventHandler(CField field);

		public delegate void FieldMouseDownEventHandler(CField field, Point location, MouseEventArgs e);

		public delegate void FieldMouseUpEventHandler(CField field, Point location, MouseEventArgs e);

		public delegate void FieldMouseMoveEventHandler(CField field, Point location, MouseEventArgs e);

		public delegate void FieldClickEventHandler(CField field, Point location, MouseEventArgs e);

		public delegate void FieldDoubleClickEventHandler(CField field, Point location, MouseEventArgs e);

		public delegate void FieldKeyDownEventHandler(CField field, KeyEventArgs e);

		public delegate void FieldKeyPressEventHandler(CField field, KeyPressEventArgs e);

		public delegate void FieldKeyUpEventHandler(CField field, KeyEventArgs e);

		public delegate void FieldSelectedEventHandler(CField field, ref bool handled);

		public delegate void FieldButtonClickEventHandler(CField field);

		public delegate void EditStartingEventHandler(CField field, ref EAllow editable);

		public delegate void EditStartEventHandler(CField field, IEditor editor);

		public delegate void EditLeavingEventHandler(CField field, string direction, ref bool cancel);

		public delegate void EditFinishedEventHandler(CField field);

		public delegate void EditorValueChangedEventHandler(CField field);

		public delegate void InitializeEditorEventHandler(CField field, IEditor editor);

		public delegate void LayoutUpdatingEventHandler(UTable table);

		public delegate void DecideScrollRectEventHandler(CField field, ref Rectangle rect);

		public delegate void FocusNextFieldEventHandler(CField field, bool forward, ref bool handled);

		public delegate void FocusNextControlEventHandler(CField field, bool forward, ref bool handled);

		public delegate void CreatingCaptionEventHandler(object key, CFieldDesc captionDesc, ref bool cancel);

		public delegate void DraggingScrollEventHandler(UTable table, int dx, int dy);

		public delegate void VerticalMergingBreakEventHandler(CField field1, CField field2, ref bool @break);

		public delegate void RecordDragStartingEventHandler(CRecord record, ref bool cancel);

		public delegate void RecordDraggingEventHandler(CRecord record, int i, ref bool cancel);

		public delegate void RecordDragFinishedEventHandler(CRecord record);

		public delegate void UndoFinishedEventHandler(UTable table, List<CModifyContainer> ucs);

		public delegate void RedoFinishedEventHandler(UTable table, List<CModifyContainer> ucs);

		public class CCaptionKey
		{
			public object key;

			public CCaptionKey(object key)
			{
				this.key = RuntimeHelpers.GetObjectValue(key);
			}

			public override bool Equals(object obj)
			{
				if (obj == this)
				{
					return true;
				}
				if (!(obj is CCaptionKey))
				{
					return false;
				}
				return key.Equals(RuntimeHelpers.GetObjectValue(((CCaptionKey)obj).key));
			}

			public override int GetHashCode()
			{
				return key.GetHashCode();
			}
		}

		public class CEditBlock : IDisposable
		{
			public UTable Table;

			public Dictionary<CContent, List<object>> EditedContents;

			public List<CModifyContainer> ModifyContainers;

			public bool Disposing;

			public CEditBlock(UTable table)
			{
				EditedContents = new Dictionary<CContent, List<object>>();
				ModifyContainers = new List<CModifyContainer>();
				Disposing = false;
				Table = table;
			}

			public void AddEditedField(CField field)
			{
				if (!EditedContents.ContainsKey(field.Content()))
				{
					EditedContents.Add(field.Content(), new List<object>());
				}
				List<object> list = EditedContents[field.Content()];
				if (!list.Contains(RuntimeHelpers.GetObjectValue(field.Key)))
				{
					list.Add(RuntimeHelpers.GetObjectValue(field.Key));
				}
				list = null;
			}

			protected virtual void onDispose()
			{
			}

			protected virtual void Dispose(bool disposing)
			{
				if (!Disposing)
				{
					Disposing = true;
					foreach (CContent key in EditedContents.Keys)
					{
						foreach (object item in EditedContents[key])
						{
							object objectValue = RuntimeHelpers.GetObjectValue(item);
							key.raiseValidated(RuntimeHelpers.GetObjectValue(objectValue));
						}
					}
					onDispose();
					if (ModifyContainers.Count > 0)
					{
						Table.addModify(ModifyContainers);
					}
					Table._EditBlock = null;
				}
			}

			public void Dispose()
			{
				Dispose(disposing: true);
				GC.SuppressFinalize(this);
			}

			void IDisposable.Dispose()
			{
				//ILSpy generated this explicit interface implementation from .override directive in Dispose
				this.Dispose();
			}
		}

		public interface IModify
		{
			void Undo(CContent content);

			void Redo(CContent content);
		}

		public class CChildModify : IModify
		{
			public int RecordIndex;

			public IModify ChildModify;

			public CChildModify(CRecord record, IModify childModify)
			{
				RecordIndex = record.Index();
				ChildModify = childModify;
			}

			public void Redo(CContent content)
			{
				ChildModify.Redo(content.Records[RecordIndex].Child);
			}

			void IModify.Redo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Redo
				this.Redo(content);
			}

			public void Undo(CContent content)
			{
				ChildModify.Undo(content.Records[RecordIndex].Child);
			}

			void IModify.Undo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Undo
				this.Undo(content);
			}
		}

		public class CFieldModify : IModify
		{
			public int RecordIndex;

			public object Key;

			public object BakValue;

			public object NewValue;

			public CFieldModify(CField field)
			{
				BakValue = RuntimeHelpers.GetObjectValue(field.Value);
			}

			public virtual bool Modified(CField field)
			{
				NewValue = RuntimeHelpers.GetObjectValue(field.Value);
				if (BakValue == null)
				{
					return NewValue != null;
				}
				return !BakValue.Equals(RuntimeHelpers.GetObjectValue(NewValue));
			}

			public void SetUp(CField field)
			{
				RecordIndex = field.Record.Index();
				Key = RuntimeHelpers.GetObjectValue(field.Key);
			}

			public virtual void Redo(CContent content)
			{
				CField field = getField(content);
				if (field != null)
				{
					field.Value = RuntimeHelpers.GetObjectValue(NewValue);
				}
			}

			void IModify.Redo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Redo
				this.Redo(content);
			}

			public virtual void Undo(CContent content)
			{
				CField field = getField(content);
				if (field != null)
				{
					field.Value = RuntimeHelpers.GetObjectValue(BakValue);
				}
			}

			void IModify.Undo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Undo
				this.Undo(content);
			}

			protected CField getField(CContent content)
			{
				if (RecordIndex >= content.Records.Count)
				{
					return null;
				}
				CRecord cRecord = content.Records[RecordIndex];
				if (!cRecord.Fields.ContainsKey(RuntimeHelpers.GetObjectValue(Key)))
				{
					return null;
				}
				return cRecord.Fields[RuntimeHelpers.GetObjectValue(Key)];
			}
		}

		public class CAddRecordModify : IModify
		{
			public int RecordIndex;

			public CAddRecordModify(int recordIndex)
			{
				RecordIndex = recordIndex;
			}

			public void Redo(CContent content)
			{
				if (RecordIndex <= content.Records.Count)
				{
					content.Records.Insert(RecordIndex, content.CreateRecord());
					content.TopLevelContent().LayoutCacheInvalid = true;
				}
			}

			void IModify.Redo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Redo
				this.Redo(content);
			}

			public void Undo(CContent content)
			{
				if (RecordIndex < content.Records.Count)
				{
					content.Records.RemoveAt(RecordIndex);
					content.TopLevelContent().LayoutCacheInvalid = true;
				}
			}

			void IModify.Undo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Undo
				this.Undo(content);
			}
		}

		public class CRemoveRecordModify : IModify
		{
			public int RecordIndex;

			public List<CFieldModify> Fields;

			public CContentModifyBuffer ChildBuffer;

			public CRemoveRecordModify(CRecord record)
			{
				Fields = new List<CFieldModify>();
				ChildBuffer = null;
				RecordIndex = record.Index();
				foreach (object key in record.Fields.Keys)
				{
					object objectValue = RuntimeHelpers.GetObjectValue(key);
					CField cField = record.Fields[RuntimeHelpers.GetObjectValue(objectValue)];
					if (cField.Desc.Provider.UndoEnabled())
					{
						CFieldModify cFieldModify = cField.Desc.Provider.CreateModifyField(cField);
						cFieldModify.SetUp(cField);
						Fields.Add(cFieldModify);
					}
				}
				if (record.Child != null)
				{
					ChildBuffer = new CContentModifyBuffer(record.Child);
				}
			}

			public void Redo(CContent content)
			{
				if (RecordIndex < content.Records.Count)
				{
					content.Records.RemoveAt(RecordIndex);
					content.TopLevelContent().LayoutCacheInvalid = true;
				}
			}

			void IModify.Redo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Redo
				this.Redo(content);
			}

			public void Undo(CContent content)
			{
				if (RecordIndex <= content.Records.Count)
				{
					CRecord cRecord = content.CreateRecord();
					content.Records.Insert(RecordIndex, cRecord);
					foreach (CFieldModify field in Fields)
					{
						field.Undo(content);
					}
					if (ChildBuffer != null && cRecord.Child != null)
					{
						ChildBuffer.Undo(cRecord.Child);
					}
					content.TopLevelContent().LayoutCacheInvalid = true;
				}
			}

			void IModify.Undo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Undo
				this.Undo(content);
			}
		}

		public class CMoveRecordModify : IModify
		{
			public int RecordIndex;

			public int BakRecordIndex;

			public CMoveRecordModify(CRecord record, int recordIndex)
				: this(record.Index(), recordIndex)
			{
			}

			public CMoveRecordModify(int bakRecordIndex, int recordIndex)
			{
				BakRecordIndex = bakRecordIndex;
				RecordIndex = recordIndex;
			}

			public void Redo(CContent content)
			{
				if (RecordIndex < content.Records.Count && BakRecordIndex < content.Records.Count)
				{
					CRecord item = content.Records[BakRecordIndex];
					content.Records.RemoveAt(BakRecordIndex);
					content.Records.Insert(RecordIndex, item);
					content.TopLevelContent().LayoutCacheInvalid = true;
				}
			}

			void IModify.Redo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Redo
				this.Redo(content);
			}

			public void Undo(CContent content)
			{
				if (RecordIndex < content.Records.Count && BakRecordIndex < content.Records.Count)
				{
					CRecord item = content.Records[RecordIndex];
					content.Records.RemoveAt(RecordIndex);
					content.Records.Insert(BakRecordIndex, item);
					content.TopLevelContent().LayoutCacheInvalid = true;
				}
			}

			void IModify.Undo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Undo
				this.Undo(content);
			}
		}

		public class CClearRecordModify : IModify
		{
			public CContentModifyBuffer Buffer;

			public CClearRecordModify(CContent content)
			{
				Buffer = new CContentModifyBuffer(content);
			}

			public void Redo(CContent content)
			{
				content.ClearRecord();
			}

			void IModify.Redo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Redo
				this.Redo(content);
			}

			public void Undo(CContent content)
			{
				Buffer.Undo(content);
				content.TopLevelContent().LayoutCacheInvalid = true;
			}

			void IModify.Undo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Undo
				this.Undo(content);
			}
		}

		public class CSortModify : IModify
		{
			public List<int> RecordIndexss;

			public CSortModify(List<int> recordIndexes)
			{
				RecordIndexss = recordIndexes;
			}

			public void Redo(CContent content)
			{
				if (RecordIndexss.Count == content.Records.Count)
				{
					List<CRecord> records = content.Records;
					content.Records = new List<CRecord>();
					foreach (int item in RecordIndexss)
					{
						content.Records.Add(records[item]);
					}
					content.Table.SortState.Field = null;
					content.TopLevelContent().LayoutCacheInvalid = true;
				}
			}

			void IModify.Redo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Redo
				this.Redo(content);
			}

			public void Undo(CContent content)
			{
				checked
				{
					if (RecordIndexss.Count == content.Records.Count)
					{
						Dictionary<int, int> dictionary = new Dictionary<int, int>();
						int num = RecordIndexss.Count - 1;
						for (int i = 0; i <= num; i++)
						{
							dictionary.Add(RecordIndexss[i], i);
						}
						List<CRecord> records = content.Records;
						content.Records = new List<CRecord>();
						int num2 = RecordIndexss.Count - 1;
						for (int j = 0; j <= num2; j++)
						{
							content.Records.Add(records[dictionary[j]]);
						}
						content.Table.SortState.Field = null;
						content.TopLevelContent().LayoutCacheInvalid = true;
					}
				}
			}

			void IModify.Undo(CContent content)
			{
				//ILSpy generated this explicit interface implementation from .override directive in Undo
				this.Undo(content);
			}
		}

		public class CContentModifyBuffer
		{
			public List<CFieldModify> Fields;

			public List<CContentModifyBuffer> Records;

			public CContentModifyBuffer(CContent content)
			{
				Fields = new List<CFieldModify>();
				Records = new List<CContentModifyBuffer>();
				foreach (CRecord record in content.Records)
				{
					foreach (object key in record.Fields.Keys)
					{
						object objectValue = RuntimeHelpers.GetObjectValue(key);
						CField cField = record.Fields[RuntimeHelpers.GetObjectValue(objectValue)];
						if (cField.Desc.Provider.UndoEnabled())
						{
							CFieldModify cFieldModify = cField.Desc.Provider.CreateModifyField(cField);
							cFieldModify.SetUp(cField);
							Fields.Add(cFieldModify);
						}
					}
				}
				foreach (CRecord record2 in content.Records)
				{
					if (record2.Child != null)
					{
						Records.Add(new CContentModifyBuffer(record2.Child));
					}
					else
					{
						Records.Add(null);
					}
				}
			}

			public void Undo(CContent content)
			{
				foreach (CContentModifyBuffer record in Records)
				{
					CRecord cRecord = content.CreateRecord();
					content.Records.Add(cRecord);
					if ((record != null) & (cRecord.Child != null))
					{
						record.Undo(cRecord.Child);
					}
				}
				foreach (CFieldModify field in Fields)
				{
					field.Undo(content);
				}
			}
		}

		public class CModifyContainer
		{
			public CContent Content;

			public IModify Modify;

			public CModifyContainer(CContent content, IModify modify)
			{
				Content = content;
				Modify = modify;
			}
		}

		private CResizing cursor_resizing;

		private CResizing resizing;

		private CEditorContainer _EditorContainer;

		private bool finishEdit_flg;

		public string LeavingDirection;

		public CField MouseDownField;

		public bool MouseDoubleClicking;

		private CDraggingScrollMove draggingScrollMove;

		[AccessedThroughProperty("draggingScrollTimer")]
		private Timer _draggingScrollTimer;

		public CGrid.CPoint FocusGrid;

		private CField _FocusField;

		private bool imeSetUpFlag;

		private CRange _SelectRange;

		private bool _RangeDragging;

		public CRecord DraggingRecord;

		private CEditBlock draggingEditBlock;

		protected CRenderBlock _RenderBlock;

		private bool render_flg;

		public int ScrollBarWidth;

		private RecordCreatedEventHandler RecordCreatedEvent;

		private RecordAddedEventHandler RecordAddedEvent;

		private RecordMovedEventHandler RecordMovedEvent;

		private RecordRemovedEventHandler RecordRemovedEvent;

		private RecordEnterEventHandler RecordEnterEvent;

		private ContentValidatedEventHandler ContentValidatedEvent;

		private ContentEnterEventHandler ContentEnterEvent;

		private SortedEventHandler SortedEvent;

		private FieldValidatingEventHandler FieldValidatingEvent;

		private FieldValidatedEventHandler FieldValidatedEvent;

		private FieldValueChangedEventHandler FieldValueChangedEvent;

		private FieldEnteringEventHandler FieldEnteringEvent;

		private FieldEnterEventHandler FieldEnterEvent;

		private FieldLeaveEventHandler FieldLeaveEvent;

		private FieldMouseDownEventHandler FieldMouseDownEvent;

		private FieldMouseUpEventHandler FieldMouseUpEvent;

		private FieldMouseMoveEventHandler FieldMouseMoveEvent;

		private FieldClickEventHandler FieldClickEvent;

		private FieldDoubleClickEventHandler FieldDoubleClickEvent;

		private FieldKeyDownEventHandler FieldKeyDownEvent;

		private FieldKeyPressEventHandler FieldKeyPressEvent;

		private FieldKeyUpEventHandler FieldKeyUpEvent;

		private FieldSelectedEventHandler FieldSelectedEvent;

		private FieldButtonClickEventHandler FieldButtonClickEvent;

		private EditStartingEventHandler EditStartingEvent;

		private EditStartEventHandler EditStartEvent;

		private EditLeavingEventHandler EditLeavingEvent;

		private EditFinishedEventHandler EditFinishedEvent;

		private EditorValueChangedEventHandler EditorValueChangedEvent;

		private InitializeEditorEventHandler InitializeEditorEvent;

		private LayoutUpdatingEventHandler LayoutUpdatingEvent;

		private DecideScrollRectEventHandler DecideScrollRectEvent;

		private FocusNextFieldEventHandler FocusNextFieldEvent;

		private FocusNextControlEventHandler FocusNextControlEvent;

		private CreatingCaptionEventHandler CreatingCaptionEvent;

		private DraggingScrollEventHandler DraggingScrollEvent;

		private VerticalMergingBreakEventHandler VerticalMergingBreakEvent;

		private RecordDragStartingEventHandler RecordDragStartingEvent;

		private RecordDraggingEventHandler RecordDraggingEvent;

		private RecordDragFinishedEventHandler RecordDragFinishedEvent;

		private UndoFinishedEventHandler UndoFinishedEvent;

		private RedoFinishedEventHandler RedoFinishedEvent;

		public CGrids Cols;

		public CContent HeaderContent;

		public CContent FooterContent;

		public CContent Content;

		public CGlobalSetting Setting;

		public CGrid.CSize DefaultGridSize;

		public bool LayoutCacheInvalid;

		public CLayoutCache LayoutCache;

		public CRenderCache RenderCache;

		public BufferedGraphics BackBuffer;

		public CAutoExtend AutoExtend;

		public CKeyboardOperation KeyboardOperation;

		[AccessedThroughProperty("VScrollBar")]
		private ScrollBar _VScrollBar;

		[AccessedThroughProperty("HScrollBar")]
		private ScrollBar _HScrollBar;

		public CSortState SortState;

		public CComparer Comparer;

		public IFieldDecorator FocusFieldDecorator;

		public bool StrictMouseLocation;

		public List<List<CModifyContainer>> UndoBuffer;

		public List<List<CModifyContainer>> RedoBuffer;

		public int UndoBufferMax;

		private bool _UndoBufferEnabled;

		private int _FixedCols;

		private int _NotSelectableCols;

		private Panel scrollPlaceHolderPanel;

		protected CEditBlock _EditBlock;

		private bool _ignoreUndoBufferEnabled;

		public int FixedCols
		{
			get
			{
				return _FixedCols;
			}
			set
			{
				using (RenderBlock())
				{
					_FixedCols = value;
					LayoutCacheInvalid = true;
				}
			}
		}

		public int NotSelectableCols
		{
			get
			{
				return _NotSelectableCols;
			}
			set
			{
				_NotSelectableCols = value;
				LayoutCacheInvalid = true;
			}
		}

		public virtual Timer draggingScrollTimer
		{
			[DebuggerNonUserCode]
			get
			{
				return _draggingScrollTimer;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			set
			{
				EventHandler value2 = draggingScrollTimer_Tick;
				if (_draggingScrollTimer != null)
				{
					_draggingScrollTimer.Tick -= value2;
				}
				_draggingScrollTimer = value;
				if (_draggingScrollTimer != null)
				{
					_draggingScrollTimer.Tick += value2;
				}
			}
		}

		public CField FocusField
		{
			get
			{
				return _FocusField;
			}
			set
			{
				using (RenderBlock())
				{
					setFocusField(value);
					if (FocusField != null && FocusField == value)
					{
						FocusGrid.Row = FocusField.Desc.Layout.Row;
						FocusGrid.Col = FocusField.Desc.Layout.Col;
					}
				}
			}
		}

		public CRecord FocusRecord
		{
			get
			{
				if (FocusField != null)
				{
					return FocusField.Record;
				}
				return null;
			}
		}

		public CRange SelectRange
		{
			get
			{
				return _SelectRange;
			}
			set
			{
				using (RenderBlock())
				{
					_SelectRange = value;
				}
			}
		}

		public virtual ScrollBar VScrollBar
		{
			[DebuggerNonUserCode]
			get
			{
				return _VScrollBar;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			set
			{
				EventHandler value2 = ScrollBar_MouseEnter;
				EventHandler value3 = VScrollBar_ValueChanged;
				if (_VScrollBar != null)
				{
					_VScrollBar.MouseEnter -= value2;
					_VScrollBar.ValueChanged -= value3;
				}
				_VScrollBar = value;
				if (_VScrollBar != null)
				{
					_VScrollBar.MouseEnter += value2;
					_VScrollBar.ValueChanged += value3;
				}
			}
		}

		public virtual ScrollBar HScrollBar
		{
			[DebuggerNonUserCode]
			get
			{
				return _HScrollBar;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			set
			{
				EventHandler value2 = HScrollBar_ValueChanged;
				EventHandler value3 = ScrollBar_MouseEnter;
				if (_HScrollBar != null)
				{
					_HScrollBar.ValueChanged -= value2;
					_HScrollBar.MouseEnter -= value3;
				}
				_HScrollBar = value;
				if (_HScrollBar != null)
				{
					_HScrollBar.ValueChanged += value2;
					_HScrollBar.MouseEnter += value3;
				}
			}
		}

		public bool UndoBufferEnabled
		{
			get
			{
				return _UndoBufferEnabled;
			}
			set
			{
				_UndoBufferEnabled = value;
				if (UndoBufferEnabled)
				{
					if (UndoBuffer == null)
					{
						UndoBuffer = new List<List<CModifyContainer>>();
					}
					if (RedoBuffer == null)
					{
						RedoBuffer = new List<List<CModifyContainer>>();
					}
				}
				else
				{
					UndoBuffer = null;
					RedoBuffer = null;
				}
			}
		}

		public event RecordCreatedEventHandler RecordCreated
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				RecordCreatedEvent = (RecordCreatedEventHandler)Delegate.Combine(RecordCreatedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				RecordCreatedEvent = (RecordCreatedEventHandler)Delegate.Remove(RecordCreatedEvent, value);
			}
		}

		public event RecordAddedEventHandler RecordAdded
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				RecordAddedEvent = (RecordAddedEventHandler)Delegate.Combine(RecordAddedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				RecordAddedEvent = (RecordAddedEventHandler)Delegate.Remove(RecordAddedEvent, value);
			}
		}

		public event RecordMovedEventHandler RecordMoved
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				RecordMovedEvent = (RecordMovedEventHandler)Delegate.Combine(RecordMovedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				RecordMovedEvent = (RecordMovedEventHandler)Delegate.Remove(RecordMovedEvent, value);
			}
		}

		public event RecordRemovedEventHandler RecordRemoved
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				RecordRemovedEvent = (RecordRemovedEventHandler)Delegate.Combine(RecordRemovedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				RecordRemovedEvent = (RecordRemovedEventHandler)Delegate.Remove(RecordRemovedEvent, value);
			}
		}

		public event RecordEnterEventHandler RecordEnter
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				RecordEnterEvent = (RecordEnterEventHandler)Delegate.Combine(RecordEnterEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				RecordEnterEvent = (RecordEnterEventHandler)Delegate.Remove(RecordEnterEvent, value);
			}
		}

		public event ContentValidatedEventHandler ContentValidated
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				ContentValidatedEvent = (ContentValidatedEventHandler)Delegate.Combine(ContentValidatedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				ContentValidatedEvent = (ContentValidatedEventHandler)Delegate.Remove(ContentValidatedEvent, value);
			}
		}

		public event ContentEnterEventHandler ContentEnter
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				ContentEnterEvent = (ContentEnterEventHandler)Delegate.Combine(ContentEnterEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				ContentEnterEvent = (ContentEnterEventHandler)Delegate.Remove(ContentEnterEvent, value);
			}
		}

		public event SortedEventHandler Sorted
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				SortedEvent = (SortedEventHandler)Delegate.Combine(SortedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				SortedEvent = (SortedEventHandler)Delegate.Remove(SortedEvent, value);
			}
		}

		public event FieldValidatingEventHandler FieldValidating
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldValidatingEvent = (FieldValidatingEventHandler)Delegate.Combine(FieldValidatingEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldValidatingEvent = (FieldValidatingEventHandler)Delegate.Remove(FieldValidatingEvent, value);
			}
		}

		public event FieldValidatedEventHandler FieldValidated
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldValidatedEvent = (FieldValidatedEventHandler)Delegate.Combine(FieldValidatedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldValidatedEvent = (FieldValidatedEventHandler)Delegate.Remove(FieldValidatedEvent, value);
			}
		}

		public event FieldValueChangedEventHandler FieldValueChanged
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldValueChangedEvent = (FieldValueChangedEventHandler)Delegate.Combine(FieldValueChangedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldValueChangedEvent = (FieldValueChangedEventHandler)Delegate.Remove(FieldValueChangedEvent, value);
			}
		}

		public event FieldEnteringEventHandler FieldEntering
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldEnteringEvent = (FieldEnteringEventHandler)Delegate.Combine(FieldEnteringEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldEnteringEvent = (FieldEnteringEventHandler)Delegate.Remove(FieldEnteringEvent, value);
			}
		}

		public event FieldEnterEventHandler FieldEnter
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldEnterEvent = (FieldEnterEventHandler)Delegate.Combine(FieldEnterEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldEnterEvent = (FieldEnterEventHandler)Delegate.Remove(FieldEnterEvent, value);
			}
		}

		public event FieldLeaveEventHandler FieldLeave
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldLeaveEvent = (FieldLeaveEventHandler)Delegate.Combine(FieldLeaveEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldLeaveEvent = (FieldLeaveEventHandler)Delegate.Remove(FieldLeaveEvent, value);
			}
		}

		public event FieldMouseDownEventHandler FieldMouseDown
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldMouseDownEvent = (FieldMouseDownEventHandler)Delegate.Combine(FieldMouseDownEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldMouseDownEvent = (FieldMouseDownEventHandler)Delegate.Remove(FieldMouseDownEvent, value);
			}
		}

		public event FieldMouseUpEventHandler FieldMouseUp
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldMouseUpEvent = (FieldMouseUpEventHandler)Delegate.Combine(FieldMouseUpEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldMouseUpEvent = (FieldMouseUpEventHandler)Delegate.Remove(FieldMouseUpEvent, value);
			}
		}

		public event FieldMouseMoveEventHandler FieldMouseMove
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldMouseMoveEvent = (FieldMouseMoveEventHandler)Delegate.Combine(FieldMouseMoveEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldMouseMoveEvent = (FieldMouseMoveEventHandler)Delegate.Remove(FieldMouseMoveEvent, value);
			}
		}

		public event FieldClickEventHandler FieldClick
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldClickEvent = (FieldClickEventHandler)Delegate.Combine(FieldClickEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldClickEvent = (FieldClickEventHandler)Delegate.Remove(FieldClickEvent, value);
			}
		}

		public event FieldDoubleClickEventHandler FieldDoubleClick
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldDoubleClickEvent = (FieldDoubleClickEventHandler)Delegate.Combine(FieldDoubleClickEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldDoubleClickEvent = (FieldDoubleClickEventHandler)Delegate.Remove(FieldDoubleClickEvent, value);
			}
		}

		public event FieldKeyDownEventHandler FieldKeyDown
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldKeyDownEvent = (FieldKeyDownEventHandler)Delegate.Combine(FieldKeyDownEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldKeyDownEvent = (FieldKeyDownEventHandler)Delegate.Remove(FieldKeyDownEvent, value);
			}
		}

		public event FieldKeyPressEventHandler FieldKeyPress
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldKeyPressEvent = (FieldKeyPressEventHandler)Delegate.Combine(FieldKeyPressEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldKeyPressEvent = (FieldKeyPressEventHandler)Delegate.Remove(FieldKeyPressEvent, value);
			}
		}

		public event FieldKeyUpEventHandler FieldKeyUp
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldKeyUpEvent = (FieldKeyUpEventHandler)Delegate.Combine(FieldKeyUpEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldKeyUpEvent = (FieldKeyUpEventHandler)Delegate.Remove(FieldKeyUpEvent, value);
			}
		}

		public event FieldSelectedEventHandler FieldSelected
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldSelectedEvent = (FieldSelectedEventHandler)Delegate.Combine(FieldSelectedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldSelectedEvent = (FieldSelectedEventHandler)Delegate.Remove(FieldSelectedEvent, value);
			}
		}

		public event FieldButtonClickEventHandler FieldButtonClick
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FieldButtonClickEvent = (FieldButtonClickEventHandler)Delegate.Combine(FieldButtonClickEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FieldButtonClickEvent = (FieldButtonClickEventHandler)Delegate.Remove(FieldButtonClickEvent, value);
			}
		}

		public event EditStartingEventHandler EditStarting
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				EditStartingEvent = (EditStartingEventHandler)Delegate.Combine(EditStartingEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				EditStartingEvent = (EditStartingEventHandler)Delegate.Remove(EditStartingEvent, value);
			}
		}

		public event EditStartEventHandler EditStart
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				EditStartEvent = (EditStartEventHandler)Delegate.Combine(EditStartEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				EditStartEvent = (EditStartEventHandler)Delegate.Remove(EditStartEvent, value);
			}
		}

		public event EditLeavingEventHandler EditLeaving
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				EditLeavingEvent = (EditLeavingEventHandler)Delegate.Combine(EditLeavingEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				EditLeavingEvent = (EditLeavingEventHandler)Delegate.Remove(EditLeavingEvent, value);
			}
		}

		public event EditFinishedEventHandler EditFinished
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				EditFinishedEvent = (EditFinishedEventHandler)Delegate.Combine(EditFinishedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				EditFinishedEvent = (EditFinishedEventHandler)Delegate.Remove(EditFinishedEvent, value);
			}
		}

		public event EditorValueChangedEventHandler EditorValueChanged
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				EditorValueChangedEvent = (EditorValueChangedEventHandler)Delegate.Combine(EditorValueChangedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				EditorValueChangedEvent = (EditorValueChangedEventHandler)Delegate.Remove(EditorValueChangedEvent, value);
			}
		}

		public event InitializeEditorEventHandler InitializeEditor
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				InitializeEditorEvent = (InitializeEditorEventHandler)Delegate.Combine(InitializeEditorEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				InitializeEditorEvent = (InitializeEditorEventHandler)Delegate.Remove(InitializeEditorEvent, value);
			}
		}

		public event LayoutUpdatingEventHandler LayoutUpdating
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				LayoutUpdatingEvent = (LayoutUpdatingEventHandler)Delegate.Combine(LayoutUpdatingEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				LayoutUpdatingEvent = (LayoutUpdatingEventHandler)Delegate.Remove(LayoutUpdatingEvent, value);
			}
		}

		public event DecideScrollRectEventHandler DecideScrollRect
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				DecideScrollRectEvent = (DecideScrollRectEventHandler)Delegate.Combine(DecideScrollRectEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				DecideScrollRectEvent = (DecideScrollRectEventHandler)Delegate.Remove(DecideScrollRectEvent, value);
			}
		}

		public event FocusNextFieldEventHandler FocusNextField
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FocusNextFieldEvent = (FocusNextFieldEventHandler)Delegate.Combine(FocusNextFieldEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FocusNextFieldEvent = (FocusNextFieldEventHandler)Delegate.Remove(FocusNextFieldEvent, value);
			}
		}

		public event FocusNextControlEventHandler FocusNextControl
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				FocusNextControlEvent = (FocusNextControlEventHandler)Delegate.Combine(FocusNextControlEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				FocusNextControlEvent = (FocusNextControlEventHandler)Delegate.Remove(FocusNextControlEvent, value);
			}
		}

		public event CreatingCaptionEventHandler CreatingCaption
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				CreatingCaptionEvent = (CreatingCaptionEventHandler)Delegate.Combine(CreatingCaptionEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				CreatingCaptionEvent = (CreatingCaptionEventHandler)Delegate.Remove(CreatingCaptionEvent, value);
			}
		}

		public event DraggingScrollEventHandler DraggingScroll
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				DraggingScrollEvent = (DraggingScrollEventHandler)Delegate.Combine(DraggingScrollEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				DraggingScrollEvent = (DraggingScrollEventHandler)Delegate.Remove(DraggingScrollEvent, value);
			}
		}

		public event VerticalMergingBreakEventHandler VerticalMergingBreak
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				VerticalMergingBreakEvent = (VerticalMergingBreakEventHandler)Delegate.Combine(VerticalMergingBreakEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				VerticalMergingBreakEvent = (VerticalMergingBreakEventHandler)Delegate.Remove(VerticalMergingBreakEvent, value);
			}
		}

		public event RecordDragStartingEventHandler RecordDragStarting
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				RecordDragStartingEvent = (RecordDragStartingEventHandler)Delegate.Combine(RecordDragStartingEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				RecordDragStartingEvent = (RecordDragStartingEventHandler)Delegate.Remove(RecordDragStartingEvent, value);
			}
		}

		public event RecordDraggingEventHandler RecordDragging
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				RecordDraggingEvent = (RecordDraggingEventHandler)Delegate.Combine(RecordDraggingEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				RecordDraggingEvent = (RecordDraggingEventHandler)Delegate.Remove(RecordDraggingEvent, value);
			}
		}

		public event RecordDragFinishedEventHandler RecordDragFinished
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				RecordDragFinishedEvent = (RecordDragFinishedEventHandler)Delegate.Combine(RecordDragFinishedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				RecordDragFinishedEvent = (RecordDragFinishedEventHandler)Delegate.Remove(RecordDragFinishedEvent, value);
			}
		}

		public event UndoFinishedEventHandler UndoFinished
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				UndoFinishedEvent = (UndoFinishedEventHandler)Delegate.Combine(UndoFinishedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				UndoFinishedEvent = (UndoFinishedEventHandler)Delegate.Remove(UndoFinishedEvent, value);
			}
		}

		public event RedoFinishedEventHandler RedoFinished
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			add
			{
				RedoFinishedEvent = (RedoFinishedEventHandler)Delegate.Combine(RedoFinishedEvent, value);
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[DebuggerNonUserCode]
			remove
			{
				RedoFinishedEvent = (RedoFinishedEventHandler)Delegate.Remove(RedoFinishedEvent, value);
			}
		}

		public void AdjustSize()
		{
			checked
			{
				using (RenderBlock())
				{
					int num = Cols.Count - 1;
					for (int i = 0; i <= num; i++)
					{
						AdjustSize(i);
					}
				}
			}
		}

		public void AdjustSize(int col)
		{
			using (RenderBlock())
			{
				int num = 1;
				int adjustColSize = getAdjustColSize(col, HeaderContent);
				if (adjustColSize > num)
				{
					num = adjustColSize;
				}
				adjustColSize = getAdjustColSize(col, Content);
				if (adjustColSize > num)
				{
					num = adjustColSize;
				}
				adjustColSize = getAdjustColSize(col, FooterContent);
				if (adjustColSize > num)
				{
					num = adjustColSize;
				}
				Cols[col].Size = num;
			}
		}

		public void ExtendSize()
		{
			checked
			{
				using (RenderBlock())
				{
					int num = Cols.Count - 1;
					for (int i = 0; i <= num; i++)
					{
						ExtendSize(i);
					}
				}
			}
		}

		public void ExtendSize(int col)
		{
			using (RenderBlock())
			{
				int num = 1;
				int adjustColSize = getAdjustColSize(col, HeaderContent);
				if (adjustColSize > num)
				{
					num = adjustColSize;
				}
				adjustColSize = getAdjustColSize(col, Content);
				if (adjustColSize > num)
				{
					num = adjustColSize;
				}
				adjustColSize = getAdjustColSize(col, FooterContent);
				if (adjustColSize > num)
				{
					num = adjustColSize;
				}
				Cols[col].Size = Math.Max(Cols[col].Size, num);
			}
		}

		private int getAdjustColSize(int col, CContent content)
		{
			int num = 0;
			UpdateLayout();
			checked
			{
				foreach (CRecord record in content.Records)
				{
					if (record.LayoutCache.Visible)
					{
						int num2 = record.Rows.Count - 1;
						for (int i = 0; i <= num2; i++)
						{
							CField cField = record.FindField(i, col);
							if (cField != null)
							{
								int col2 = cField.GetAdjustSize(new CGrid.CPoint(i, col)).Col;
								if (col2 > num)
								{
									num = col2;
								}
							}
						}
						if (record.Child != null)
						{
							int adjustColSize = getAdjustColSize(col, record.Child);
							if (adjustColSize > num)
							{
								num = adjustColSize;
							}
						}
					}
				}
				return num;
			}
		}

		public void CreateCols(int cols_count)
		{
			while (Cols.Count < cols_count)
			{
				Cols.Add(new CGrid(this));
				LayoutCacheInvalid = true;
			}
		}

		private bool cols_MouseDown(MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left && cursor_resizing != null)
			{
				resizing = cursor_resizing;
				resizing.Size = Cols[resizing.Col].Size;
				return true;
			}
			return false;
		}

		private bool cols_MouseDoubleClick(MouseEventArgs e)
		{
			using (RenderBlock())
			{
				if (cursor_resizing != null)
				{
					int col = cursor_resizing.Col;
					EAllow eAllow = Setting.UserColAdjustable;
					if (Cols[col].UserAdjustable != 0)
					{
						eAllow = Cols[col].UserAdjustable;
					}
					if (eAllow == EAllow.ALLOW)
					{
						AdjustSize(col);
						resizing = null;
						cursor_resizing = null;
						return true;
					}
				}
			}
			return false;
		}

		private bool cols_MouseMove(MouseEventArgs e)
		{
			return resize_col(e.Location, Cursors.VSplit);
		}

		private bool cols_MouseUp(MouseEventArgs e)
		{
			if (resizing != null)
			{
				using (RenderBlock())
				{
					Cols[resizing.Col].Size = resizing.Size;
					resizing = null;
					cursor_resizing = null;
					LayoutCacheInvalid = true;
				}
				return true;
			}
			return false;
		}

		private bool resize_col(Point cursorPos, Cursor resizableCorsor)
		{
			object obj = null;
			CField cField = null;
			if (RenderCache != null)
			{
				CRenderCache.CField cField2 = RenderCache.FindField(cursorPos);
				if (cField2 != null)
				{
					cField = cField2.Field;
				}
			}
			if (cField != null)
			{
				Cursor = cField.DynamicSetting().Cursor();
			}
			else
			{
				Cursor = Setting.Cursor;
			}
			obj = null;
			if (Editor() != null)
			{
				return false;
			}
			checked
			{
				if (resizing == null)
				{
					cursor_resizing = null;
					if (cursorPos.Y <= HeaderContent.LayoutCache.RowsSize)
					{
						int num = RenderCache.Cols.Count - 1;
						for (int i = 0; i <= num; i++)
						{
							if (RenderCache.Cols[i].Appeared && Math.Abs(cursorPos.X - RenderCache.Cols[i].To) <= 3)
							{
								EAllow eAllow = Setting.UserColResizable;
								if (Cols[i].UserResizable != 0)
								{
									eAllow = Cols[i].UserResizable;
								}
								if (eAllow == EAllow.ALLOW)
								{
									Cursor = resizableCorsor;
									cursor_resizing = new CResizing();
									cursor_resizing.Col = i;
								}
							}
						}
					}
					return false;
				}
				Cursor = resizableCorsor;
				int size = Math.Max(cursorPos.X - RenderCache.Cols[resizing.Col].From, 1);
				size = Cols[resizing.Col].Regularize(size);
				if (isSizeValid(size))
				{
					using (RenderBlock())
					{
						resizing.Size = size;
					}
				}
				return true;
			}
		}

		private bool isSizeValid(int size)
		{
			if (resizing.Col < FixedCols)
			{
				int size2 = Cols.GetSize(0, FixedCols);
				if (checked(Width - ScrollBarWidth < size2 - Cols[resizing.Col].Size + size))
				{
					return false;
				}
			}
			return true;
		}

		private bool edit_KeyPress(KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar))
			{
				_StartEdit(e.KeyChar, clear: false);
			}
			return Editor() != null;
		}

		private bool edit_KeyDown(KeyEventArgs e)
		{
			if (e.KeyCode == Keys.ProcessKey)
			{
				ScrollTo(FocusField);
				_StartEdit('\0', clear: true);
				return true;
			}
			return false;
		}

		private bool edit_MouseDoubleClick(MouseEventArgs e)
		{
			if (RenderCache.FocusField != null && RenderCache.FocusField.Rect.Contains(e.Location))
			{
				bool handled = false;
				if (FocusField != null)
				{
					RaiseFieldSelected(FocusField, ref handled);
				}
				if (handled)
				{
					return true;
				}
				return StartEdit() != null;
			}
			return false;
		}

		public IEditor StartEdit()
		{
			return _StartEdit('\0', clear: false);
		}

		private IEditor _StartEdit(char key, bool clear)
		{
			if (Editor() != null)
			{
				return Editor();
			}
			if (!Focused)
			{
				return null;
			}
			if (DraggingRecord != null)
			{
				return null;
			}
			checked
			{
				if (FocusField != null)
				{
					using (RenderBlock())
					{
						EAllow eAllow = FocusField.Editable();
						if (eAllow == EAllow.ALLOW)
						{
							ScrollTo(FocusField);
							Render();
							if (RenderCache.FocusField != null)
							{
								IEditor editor = FocusField.Desc.Provider.CreateEditor();
								if (editor != null)
								{
									SelectRange = null;
									editor.Initialize(FocusField);
									FocusField.Desc.Provider.EditorInitialize(FocusField, editor);
									InitializeEditorEvent?.Invoke(FocusField, editor);
									CRenderCache.CField focusField = RenderCache.FocusField;
									Size size2 = editor.Control().Size = new Size(focusField.Rect.Width - 1, focusField.Rect.Height - 1);
									int num = focusField.Rect.Height - editor.Control().Size.Height;
									int num2 = 0;
									switch (FocusField.DynamicSetting().VerticalAlignment())
									{
									case EVAlign.TOP:
										num2 = focusField.Rect.Top;
										break;
									case EVAlign.MIDDLE:
										num2 = (int)Math.Round((double)focusField.Rect.Top + (double)num / 2.0);
										break;
									case EVAlign.BOTTOM:
										num2 = focusField.Rect.Top + num;
										break;
									}
									if (num2 == focusField.Rect.Top)
									{
										num2++;
									}
									Point point2 = editor.Control().Location = new Point(focusField.Rect.Left + 1, num2);
									Controls.Add((Control)editor);
									if (!clear)
									{
										editor.Value = RuntimeHelpers.GetObjectValue(FocusField.CommittedValue());
									}
									_EditorContainer = new CEditorContainer(this, editor);
									VScrollBar.Enabled = false;
									HScrollBar.Enabled = false;
									raiseEditStart(FocusField, Editor());
								}
							}
						}
					}
					if (Editor() != null)
					{
						Editor().Control().Focus();
						Editor().EditEnter(key);
					}
				}
				return Editor();
			}
		}

		public void FinishEdit()
		{
			FinishEdit(valueCommit: false);
		}

		public void FinishEdit(bool valueCommit)
		{
			//Discarded unreachable code: IL_00f0
			try
			{
				if (!finishEdit_flg)
				{
					finishEdit_flg = true;
					using (RenderBlock())
					{
						if (Editor() != null)
						{
							CFieldModify fieldModify = _EditorContainer.FieldModify;
							object objectValue = RuntimeHelpers.GetObjectValue(Editor().Value);
							Controls.Remove((Control)Editor());
							_EditorContainer = null;
							if (valueCommit)
							{
								FocusField.ValueCommit(RuntimeHelpers.GetObjectValue(FocusField.Desc.Provider.ValueRegularize(RuntimeHelpers.GetObjectValue(objectValue), null)), fieldModify);
							}
							if (VScrollBar.Maximum > VScrollBar.Minimum)
							{
								VScrollBar.Enabled = true;
							}
							if (HScrollBar.Maximum > HScrollBar.Minimum)
							{
								HScrollBar.Enabled = true;
							}
							raiseEditFinished(FocusField);
						}
					}
				}
			}
			finally
			{
				finishEdit_flg = false;
			}
		}

		public bool LeaveEdit()
		{
			return LeaveEdit(null);
		}

		public bool LeaveEdit(string direction)
		{
			//Discarded unreachable code: IL_0080, IL_008c
			bool cancel = false;
			EditLeavingEvent?.Invoke(FocusField, direction, ref cancel);
			if (cancel)
			{
				return false;
			}
			try
			{
				LeavingDirection = direction;
				using (RenderBlock())
				{
					using (EditBlock())
					{
						if (Editor() != null)
						{
							Editor().EditLeave();
							Focus();
							if (Focused && LeavingDirection != null)
							{
								KeyboardOperation.LeaveEdit(LeavingDirection, this);
							}
						}
					}
				}
			}
			finally
			{
				LeavingDirection = null;
			}
			return Editor() == null;
		}

		public IEditor Editor()
		{
			if (_EditorContainer != null)
			{
				return _EditorContainer.Editor;
			}
			return null;
		}

		private void UTable_Click(object sender, EventArgs e)
		{
			Focus();
		}

		private void UTable_Enter(object sender, EventArgs e)
		{
			Focus();
		}

		protected override bool IsInputChar(char charCode)
		{
			return true;
		}

		protected override bool IsInputKey(Keys keyData)
		{
			if (Editor() == null)
			{
				return KeyboardOperation.IsInputKey(keyData, this, base.IsInputKey(keyData));
			}
			return base.IsInputKey(keyData);
		}

		protected override bool ProcessDialogKey(Keys keyData)
		{
			if (Editor() == null && KeyboardOperation.ProcessDialogKey(keyData, this))
			{
				return true;
			}
			return base.ProcessDialogKey(keyData);
		}

		private void UTable_MouseDown(object sender, MouseEventArgs e)
		{
			if ((!StrictMouseLocation || PointToClient(Cursor.Position).Equals(e.Location)) && (Editor() == null || LeaveEdit()) && !cols_MouseDown(e) && !focus_MouseDown(e) && Editor() == null)
			{
				CRenderCache.CField cField = RenderCache.FindField(e.Location);
				if (cField != null)
				{
					MouseDownField = cField.Field;
					Point location = checked(new Point(e.X - cField.Rect.Left, e.Y - cField.Rect.Top));
					recordDrag_MouseDown(cField.Field, location, e);
					cField.Field.raiseMouseDown(location, e);
					FieldMouseDownEvent?.Invoke(cField.Field, location, e);
				}
			}
		}

		private void UTable_MouseUp(object sender, MouseEventArgs e)
		{
			if (!cols_MouseUp(e) && !range_MouseUp(e) && Editor() == null)
			{
				recordDrag_MouseUp(e);
				if (MouseDownField != null)
				{
					CRenderCache.CField cField = RenderCache.FindField(e.Location);
					Point location = (cField != null && MouseDownField == cField.Field) ? checked(new Point(e.X - cField.Rect.Left, e.Y - cField.Rect.Top)) : new Point(-1, -1);
					CField mouseDownField = MouseDownField;
					MouseDownField = null;
					mouseDownField.raiseMouseUp(location, e);
					FieldMouseUpEvent?.Invoke(mouseDownField, location, e);
				}
			}
		}

		private void UTable_MouseMove(object sender, MouseEventArgs e)
		{
			if ((!StrictMouseLocation || PointToClient(Cursor.Position).Equals(e.Location)) && !cols_MouseMove(e) && !drag_MouseMove(e) && !range_MouseMove(e) && Editor() == null)
			{
				recordDrag_MouseMove(e);
				CRenderCache.CField cField = RenderCache.FindField(e.Location);
				if (cField != null)
				{
					Point location = checked(new Point(e.X - cField.Rect.Left, e.Y - cField.Rect.Top));
					cField.Field.raiseMouseMove(location, e);
					FieldMouseMoveEvent?.Invoke(cField.Field, location, e);
				}
			}
		}

		private void UTable_MouseClick(object sender, MouseEventArgs e)
		{
			if ((!StrictMouseLocation || PointToClient(Cursor.Position).Equals(e.Location)) && Editor() == null)
			{
				CRenderCache.CField cField = RenderCache.FindField(e.Location);
				if (cField != null)
				{
					Point location = checked(new Point(e.X - cField.Rect.Left, e.Y - cField.Rect.Top));
					cField.Field.raiseClick(location, e);
					FieldClickEvent?.Invoke(cField.Field, location, e);
				}
			}
		}

		private void UTable_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			MouseDoubleClicking = true;
			try
			{
				if ((!StrictMouseLocation || PointToClient(Cursor.Position).Equals(e.Location)) && !cols_MouseDoubleClick(e) && !edit_MouseDoubleClick(e) && Editor() == null)
				{
					CRenderCache.CField cField = RenderCache.FindField(e.Location);
					if (cField != null)
					{
						Point location = checked(new Point(e.X - cField.Rect.Left, e.Y - cField.Rect.Top));
						cField.Field.raiseDoubleClick(location, e);
						FieldDoubleClickEvent?.Invoke(cField.Field, location, e);
					}
				}
			}
			finally
			{
				MouseDoubleClicking = false;
			}
		}

		private void UTable_KeyDown(object sender, KeyEventArgs e)
		{
			if (!edit_KeyDown(e) && Editor() == null && FocusField != null)
			{
				FocusField.raiseKeyDown(e);
				FieldKeyDownEvent?.Invoke(FocusField, e);
			}
		}

		private void UTable_KeyUp(object sender, KeyEventArgs e)
		{
			if (Editor() == null && FocusField != null)
			{
				FocusField.raiseKeyUp(e);
				FieldKeyUpEvent?.Invoke(FocusField, e);
			}
		}

		private void UTable_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!imeSetUpFlag && !edit_KeyPress(e) && Editor() == null && FocusField != null)
			{
				FocusField.raiseKeyPress(e);
				FieldKeyPressEvent?.Invoke(FocusField, e);
			}
		}

		private void UTable_GotFocus(object sender, EventArgs e)
		{
			using (RenderBlock())
			{
				if (Editor() != null)
				{
					Editor().Control().Focus();
				}
				if (FocusField != null)
				{
					imeSetUp(FocusField);
				}
				if (Setting.EnterContent != null && FocusField == null)
				{
					FocusField = Setting.EnterContent.DefaultField();
				}
			}
		}

		private void UTable_LostFocus(object sender, EventArgs e)
		{
			range_LostFocus(e);
			Render();
		}

		private void UTable_Resize(object sender, EventArgs e)
		{
			//Discarded unreachable code: IL_004e
			BufferedGraphicsContext current = BufferedGraphicsManager.Current;
			try
			{
				using (RenderBlock())
				{
					if (Editor() != null && !LeaveEdit())
					{
						FinishEdit();
					}
					BackBuffer = current.Allocate(CreateGraphics(), ClientRectangle);
					LayoutCacheInvalid = true;
				}
			}
			catch (Exception ex)
			{
				ProjectData.SetProjectError(ex);
				Exception ex2 = ex;
				ProjectData.ClearProjectError();
			}
		}

		private void UTable_DraggingScroll(UTable table, int dx, int dy)
		{
			if (!range_DraggingOverflow(dx, dy))
			{
				recordDrag_DraggingOverflow(dx, dy);
			}
		}

		private void draggingScrollTimer_Tick(object sender, EventArgs e)
		{
			CDraggingScrollMove cDraggingScrollMove = draggingScrollMove;
			if ((cDraggingScrollMove.dx != 0) | (cDraggingScrollMove.dy != 0))
			{
				DraggingScrollEvent?.Invoke(this, cDraggingScrollMove.dx, cDraggingScrollMove.dy);
			}
			cDraggingScrollMove = null;
		}

		private bool drag_MouseMove(MouseEventArgs e)
		{
			draggingScrollMove.Update(this, e.Location);
			return false;
		}

		private void UTable_Validating(object sender, CancelEventArgs e)
		{
			if (Editor() != null)
			{
				e.Cancel = true;
			}
		}

		protected static string _SanitizeClipboard(string v)
		{
			string text = v;
			if (text == null)
			{
				text = "";
			}
			if (text.Contains("\r"))
			{
				text = text.Replace("\r", "");
				text = text.Replace("\"", "\"\"");
				text = "\"" + text + "\"";
			}
			return text;
		}

		protected static List<List<string>> _ParseClipboard(string s)
		{
			List<List<string>> list = new List<List<string>>();
			string text = s;
			checked
			{
				if (s.EndsWith("\r\n"))
				{
					text = s.Substring(0, s.Length - 2);
				}
				string[] array = text.Split('\r');
				for (int i = 0; i < array.Length; i++)
				{
					string text2 = array[i];
					if (text2.StartsWith("\n"))
					{
						text2 = text2.Substring(1);
					}
					List<string> list2 = new List<string>();
					string[] array2 = text2.Split('\t');
					for (int j = 0; j < array2.Length; j++)
					{
						string text3 = array2[j];
						if (text3.Contains("\n"))
						{
							text3 = text3.Replace("\n", "\r\n");
							if (text3.StartsWith("\"") & (text3.Length >= 2))
							{
								text3 = text3.Substring(1, text3.Length - 2);
							}
							text3 = text3.Replace("\"\"", "\"");
						}
						list2.Add(text3);
					}
					list.Add(list2);
				}
				return list;
			}
		}

		public void MoveFocusField(CField field)
		{
			using (RenderBlock())
			{
				setFocusField(field);
				if (FocusField != null)
				{
					CFieldDesc desc = FocusField.Desc;
					if (FocusGrid.Row < desc.Layout.Row)
					{
						FocusGrid.Row = desc.Layout.Row;
					}
					else if (FocusGrid.Row >= desc.Layout.BottomRow())
					{
						FocusGrid.Row = desc.Layout.BottomRow();
					}
					if (FocusGrid.Col < desc.Layout.Col)
					{
						FocusGrid.Col = desc.Layout.Col;
					}
					else if (FocusGrid.Col >= desc.Layout.RightCol())
					{
						FocusGrid.Col = desc.Layout.RightCol();
					}
					desc = null;
				}
			}
		}

		private void setFocusField(CField field)
		{
			if ((Editor() != null && !LeaveEdit()) || (field != null && !field.Focusable()))
			{
				return;
			}
			CField focusField = FocusField;
			bool cancel = false;
			if (field != null)
			{
				field.raiseEntering(ref cancel);
				FieldEnteringEvent?.Invoke(field, ref cancel);
			}
			if (cancel)
			{
				return;
			}
			imeSetUp(field);
			_FocusField = field;
			SelectRange = null;
			if (focusField != null && focusField != field)
			{
				focusField.raiseLeave();
				FieldLeaveEvent?.Invoke(focusField);
			}
			if (FocusField != null)
			{
				ScrollTo(FocusField);
				if (field != focusField)
				{
					field.raiseEnter();
					FieldEnterEvent?.Invoke(field);
				}
				if (!Focused || focusField == null || focusField.Record != field.Record)
				{
					field.Record.raiseEnter();
				}
				if (!Focused || focusField == null || focusField.Content() != field.Content())
				{
					field.Content().raiseEnter();
				}
			}
		}

		private bool focus_MouseDown(MouseEventArgs e)
		{
			CRenderCache.CField cField = RenderCache.FindField(e.Location);
			if (cField != null)
			{
				if (e.Button == MouseButtons.Left)
				{
					FocusField = cField.Field;
					if (Setting.UserRangeSelectable & (FocusField == cField.Field))
					{
						_RangeDragging = true;
					}
				}
				else if (e.Button == MouseButtons.Right && (SelectRange == null || !SelectRange.IsInclude(cField.Field)))
				{
					FocusField = cField.Field;
				}
			}
			return false;
		}

		public int PosToCol(int x)
		{
			checked
			{
				if (x < LayoutCache.FixedColsSize)
				{
					int num = 0;
					int num2 = FixedCols - 1;
					for (int i = 0; i <= num2; i++)
					{
						if (Cols[i].Visible)
						{
							num += Cols[i].Size;
						}
						if (x < num)
						{
							return i;
						}
					}
				}
				else
				{
					int num3 = x + HScrollBar.Value - LayoutCache.FixedColsSize;
					int num4 = 0;
					int fixedCols = FixedCols;
					int num5 = Cols.Count - 1;
					for (int j = fixedCols; j <= num5; j++)
					{
						if (Cols[j].Visible)
						{
							num4 += Cols[j].Size;
						}
						if (num3 < num4)
						{
							return j;
						}
					}
				}
				return -1;
			}
		}

		private void imeSetUp(CField field)
		{
			if (field != null)
			{
				ImeMode imeMode = field.Desc.Provider.ImeMode();
				if (field.DynamicSetting().Editable() != EAllow.ALLOW)
				{
					imeMode = ImeMode.Disable;
				}
				try
				{
					imeSetUpFlag = true;
					ImeMode = imeMode;
				}
				finally
				{
					imeSetUpFlag = false;
				}
			}
		}

		public object CreateRange(CContent topContent, int startRow, int startCol, int endRow, int endCol)
		{
			if ((startRow >= 0 && startCol >= 0) & (endRow >= 0) & (endCol >= 0))
			{
				return new CRange(topContent, startRow, startCol, endRow, endCol);
			}
			return null;
		}

		public CRange CreateRange(CField startField, int endRow, int endCol)
		{
			return (CRange)CreateRange(startField.TopLevelContent(), startField.Record.LayoutCache.SelectableRowsRev[startField.TopRow().Index()], startField.Table().LayoutCache.SelectableColsRev[startField.LeftCol().Index()], endRow, endCol);
		}

		public CRange CreateRange(CField startField, CField endField)
		{
			return CreateRange(startField, endField.Record.LayoutCache.SelectableRowsRev[endField.TopRow().Index()], endField.Table().LayoutCache.SelectableColsRev[endField.LeftCol().Index()]);
		}

		public object CreateRange(CContent topContent)
		{
			return checked(CreateRange(topContent, 0, 0, topContent.LayoutCache.SelectableRows.Count - 1, topContent.Table.LayoutCache.SelectableCols.Count - 1));
		}

		public object CreateRange(CRecord record)
		{
			checked
			{
				int num = record.Rows.Count - 1;
				int startRow = default(int);
				for (int i = 0; i <= num; i++)
				{
					if (record.Rows[i].Visible)
					{
						startRow = record.LayoutCache.SelectableRowsRev[i];
						break;
					}
				}
				int endRow = default(int);
				for (int j = record.Rows.Count - 1; j >= 0; j += -1)
				{
					if (record.Rows[j].Visible)
					{
						endRow = record.LayoutCache.SelectableRowsRev[j];
						break;
					}
				}
				return CreateRange(record.TopLevelContent(), startRow, 0, endRow, record.Table().LayoutCache.SelectableCols.Count - 1);
			}
		}

		public object CreateRange(CRecord record1, CRecord record2)
		{
			checked
			{
				int num = record1.Rows.Count - 1;
				int val = default(int);
				for (int i = 0; i <= num; i++)
				{
					if (record1.Rows[i].Visible)
					{
						val = record1.LayoutCache.SelectableRowsRev[i];
						break;
					}
				}
				int val2 = default(int);
				for (int j = record1.Rows.Count - 1; j >= 0; j += -1)
				{
					if (record1.Rows[j].Visible)
					{
						val2 = record1.LayoutCache.SelectableRowsRev[j];
						break;
					}
				}
				int num2 = record2.Rows.Count - 1;
				int val3 = default(int);
				for (int k = 0; k <= num2; k++)
				{
					if (record2.Rows[k].Visible)
					{
						val3 = record2.LayoutCache.SelectableRowsRev[k];
						break;
					}
				}
				int val4 = default(int);
				for (int l = record2.Rows.Count - 1; l >= 0; l += -1)
				{
					if (record2.Rows[l].Visible)
					{
						val4 = record2.LayoutCache.SelectableRowsRev[l];
						break;
					}
				}
				return CreateRange(record1.TopLevelContent(), Math.Min(val, val3), 0, Math.Max(val2, val4), record1.Table().LayoutCache.SelectableCols.Count - 1);
			}
		}

		public object CreateRange(CContent topContent, int col)
		{
			return CreateRange(topContent, 0, col, checked(topContent.LayoutCache.SelectableRows.Count - 1), col);
		}

		public object CreateRange(CField field)
		{
			return CreateRange(field, field);
		}

		private bool range_MouseMove(MouseEventArgs e)
		{
			checked
			{
				if ((FocusField != null) & _RangeDragging)
				{
					if (SelectRange == null)
					{
						SelectRange = (CRange)CreateRange(FocusField);
					}
					if (SelectRange != null)
					{
						int num = -1;
						int num2 = -1;
						int x = e.Location.X;
						int num3 = e.Location.Y;
						if (SelectRange.TopContent == Content)
						{
							num3 -= HeaderContent.LayoutCache.RowsSize - VScrollBar.Value;
						}
						else if (SelectRange.TopContent == FooterContent)
						{
							num3 -= Height - FooterContent.LayoutCache.RowsSize;
							if (HScrollBar.Visible)
							{
								num3 += HScrollBar.Height;
							}
						}
						int num4 = LayoutCache.SelectableColsPos.Count - 1;
						for (int i = 0; i <= num4; i++)
						{
							if (LayoutCache.SelectableCols[i].Index() < FixedCols)
							{
								if (x < LayoutCache.SelectableColsPos[i])
								{
									num2 = i;
									break;
								}
							}
							else if (x < LayoutCache.SelectableColsPos[i] - HScrollBar.Value)
							{
								num2 = i;
								break;
							}
						}
						int num5 = SelectRange.TopContent.LayoutCache.SelectableRowsPos.Count - 1;
						for (int j = 0; j <= num5; j++)
						{
							if (num3 < SelectRange.TopContent.LayoutCache.SelectableRowsPos[j])
							{
								num = j;
								break;
							}
						}
						if (num >= 0 && num2 >= 0)
						{
							using (RenderBlock())
							{
								switch (FocusField.Table().Setting.UserRangeSelectUnit)
								{
								case ERangeUnit.FIELD:
									SelectRange = CreateRange(FocusField, num, num2);
									break;
								case ERangeUnit.RECORD:
									SelectRange = (CRange)CreateRange(FocusField.Record, FocusField.TopLevelContent().LayoutCache.SelectableRows[num].Record);
									break;
								}
							}
						}
					}
				}
				return false;
			}
		}

		private void range_LostFocus(EventArgs e)
		{
			_RangeDragging = false;
		}

		private bool range_MouseUp(MouseEventArgs e)
		{
			_RangeDragging = false;
			return false;
		}

		private bool range_DraggingOverflow(int dx, int dy)
		{
			checked
			{
				if (SelectRange != null && _RangeDragging)
				{
					if (SelectRange.TopContent == Content && dy != 0)
					{
						SetVScrollValue(VScrollBar.Value + VScrollBar.SmallChange * dy);
					}
					if (dx != 0)
					{
						SetHScrollValue(HScrollBar.Value + HScrollBar.SmallChange * dx);
					}
				}
				return false;
			}
		}

		public void ClipboardCopy()
		{
			if (SelectRange != null)
			{
				SelectRange.ClipboardCopy();
			}
			else if (FocusField != null)
			{
				FocusField.ClipboardCopy();
			}
		}

		public void ClipboardPaste()
		{
			ClipboardPaste(addRecord: false);
		}

		public void ClipboardPaste(bool addRecord)
		{
			//Discarded unreachable code: IL_00b6
			if (Clipboard.ContainsText() && FocusField != null)
			{
				using (RenderBlock())
				{
					using (EditBlock())
					{
						List<List<string>> list = _ParseClipboard(Clipboard.GetText());
						if (SelectRange != null && SelectRange.Rows() % list.Count == 0 && SelectRange.Cols() % list[0].Count == 0)
						{
							_ClipboardPaste_toRangeRepeat(list);
						}
						else if (list.Count == 1 && list[0].Count == 1)
						{
							_ClipboardPaste_toField(FocusField, list[0][0]);
						}
						else
						{
							_ClipboardPaste_toRange(list, addRecord);
						}
					}
				}
			}
		}

		private void _ClipboardPaste_toField(CField field, string v)
		{
			if (field != null && field.Focusable() && field.Editable().Equals(EAllow.ALLOW))
			{
				field.Desc.Provider.Clipboard = v;
			}
		}

		private void _ClipboardPaste_toRange(List<List<string>> v, bool addRecord)
		{
			if (addRecord)
			{
				_ClipboardPaste_AddRecord(v.Count);
			}
			checked
			{
				CRange cRange = CreateRange(FocusField, FocusField.TopLevelContent().LayoutCache.SelectableRows.Count - 1, LayoutCache.SelectableCols.Count - 1);
				if (cRange == null)
				{
					return;
				}
				int num = v.Count - 1;
				for (int i = 0; i <= num && i < cRange.FieldMatrix().Count; i++)
				{
					int num2 = v[i].Count - 1;
					for (int j = 0; j <= num2 && j < cRange.FieldMatrix()[i].Count; j++)
					{
						_ClipboardPaste_toField(cRange.FieldMatrix()[i][j], v[i][j]);
					}
				}
			}
		}

		private void _ClipboardPaste_toRangeRepeat(List<List<string>> v)
		{
			int count = v.Count;
			int count2 = v[0].Count;
			checked
			{
				int num = SelectRange.FieldMatrix().Count - 1;
				for (int i = 0; i <= num; i++)
				{
					int index = unchecked(i % count);
					int num2 = SelectRange.FieldMatrix()[i].Count - 1;
					for (int j = 0; j <= num2; j++)
					{
						int index2 = unchecked(j % count2);
						_ClipboardPaste_toField(SelectRange.FieldMatrix()[i][j], v[index][index2]);
					}
				}
			}
		}

		private void _ClipboardPaste_AddRecord(int count)
		{
			checked
			{
				int num = FocusField.TopLevelContent().LayoutCache.SelectableRows.Count - FocusField.Record.LayoutCache.SelectableRowsRev[FocusField.TopRow().Index()];
				List<CGrid> selectableRows = FocusField.TopLevelContent().LayoutCache.SelectableRows;
				CContent content = selectableRows[selectableRows.Count - 1].Record.Content;
				selectableRows = null;
				if (content.RecordProvider == null)
				{
					return;
				}
				while (num < count)
				{
					CRecord cRecord = content.AddRecord();
					if (cRecord == null)
					{
						break;
					}
					foreach (CGrid row in cRecord.Rows)
					{
						if (row.Visible)
						{
							num++;
						}
					}
				}
				UpdateLayout();
			}
		}

		private void recordDrag_MouseDown(CField field, Point location, MouseEventArgs e)
		{
			if (field.TopLevelContent() == field.Table().Content && field.Desc.Provider.Draggable() && e.Button == MouseButtons.Left)
			{
				bool cancel = false;
				RecordDragStartingEvent?.Invoke(field.Record, ref cancel);
				if (!cancel)
				{
					DraggingRecord = field.Record;
					draggingEditBlock = EditBlock();
					field.Table().Render();
				}
			}
		}

		private void recordDrag_MouseMove(MouseEventArgs e)
		{
			if (DraggingRecord != null)
			{
				CRenderCache.CField cField = RenderCache.FindField(e.Location);
				if (cField != null)
				{
					recordDragMove(cField.Field);
				}
			}
		}

		private void recordDrag_MouseUp(MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left && DraggingRecord != null)
			{
				RecordDragFinishedEvent?.Invoke(DraggingRecord);
				DraggingRecord = null;
				if (draggingEditBlock != null)
				{
					draggingEditBlock.Dispose();
					draggingEditBlock = null;
				}
				Render();
			}
		}

		private void recordDrag_DraggingOverflow(int dx, int dy)
		{
			if (DraggingRecord != null && dy != 0)
			{
				SetVScrollValue(checked(VScrollBar.Value + VScrollBar.SmallChange * dy));
			}
		}

		private void recordDragMove(CField field)
		{
			if (DraggingRecord.Content == field.Content() && DraggingRecord != field.Record)
			{
				using (RenderBlock())
				{
					CContent content = DraggingRecord.Content;
					int num = content.Records.IndexOf(DraggingRecord);
					int num2 = content.Records.IndexOf(field.Record);
					if (num2 >= 0 && num != num2)
					{
						bool cancel = false;
						RecordDraggingEvent?.Invoke(DraggingRecord, num2, ref cancel);
						if (!cancel)
						{
							content.MoveRecord(DraggingRecord, num2);
						}
					}
					content = null;
				}
			}
		}

		public void UpdateLayout()
		{
			LayoutUpdatingEvent?.Invoke(this);
			updateScrollBars(updateLayoutCaches());
		}

		public void Render()
		{
			checked
			{
				if (!render_flg)
				{
					try
					{
						render_flg = true;
						if (BackBuffer != null)
						{
							UpdateLayout();
							BackBuffer.Graphics.ResetClip();
							using (SolidBrush brush = new SolidBrush(Setting.ContentBackColor))
							{
								BackBuffer.Graphics.FillRectangle(brush, ClientRectangle);
							}
							int top_row = HeaderContent.LayoutCache.RowsSize - VScrollBar.Value;
							updateRenderCache(-HScrollBar.Value);
							Content.RenderingRect = new Rectangle(LayoutCache.FixedColsSize, HeaderContent.LayoutCache.RowsSize, Width - LayoutCache.FixedColsSize, Height - (HeaderContent.LayoutCache.RowsSize + FooterContent.LayoutCache.RowsSize));
							BackBuffer.Graphics.Clip = new Region(Content.RenderingRect);
							Content.render(BackBuffer.Graphics, top_row, -HScrollBar.Value, Height, @fixed: false);
							RenderContent();
							BackBuffer.Graphics.ResetClip();
							Content.RenderingRect = new Rectangle(0, HeaderContent.LayoutCache.RowsSize, Width, Height - (HeaderContent.LayoutCache.RowsSize + FooterContent.LayoutCache.RowsSize));
							Content.render(BackBuffer.Graphics, top_row, 0, Height, @fixed: true);
							RenderFixedContent();
							using (SolidBrush brush2 = new SolidBrush(Setting.HeaderContentBackColor))
							{
								Graphics graphics = BackBuffer.Graphics;
								Rectangle rect = new Rectangle(0, 0, Width, HeaderContent.LayoutCache.RowsSize + 1);
								graphics.FillRectangle(brush2, rect);
							}
							HeaderContent.RenderingRect = new Rectangle(LayoutCache.FixedColsSize, 0, Width - LayoutCache.FixedColsSize, HeaderContent.LayoutCache.RowsSize + 1);
							BackBuffer.Graphics.Clip = new Region(HeaderContent.RenderingRect);
							HeaderContent.render(BackBuffer.Graphics, 0, -HScrollBar.Value, Height, @fixed: false);
							RenderHeaderContent();
							BackBuffer.Graphics.ResetClip();
							HeaderContent.RenderingRect = new Rectangle(0, 0, Width, HeaderContent.LayoutCache.RowsSize + 1);
							HeaderContent.render(BackBuffer.Graphics, 0, 0, Height, @fixed: true);
							RenderFixedHeaderContent();
							int num = Height - FooterContent.LayoutCache.RowsSize;
							if (HScrollBar.Visible)
							{
								num -= HScrollBar.Height;
							}
							using (SolidBrush brush3 = new SolidBrush(Setting.FooterContentBackColor))
							{
								Graphics graphics2 = BackBuffer.Graphics;
								Rectangle rect = new Rectangle(0, num, Width, FooterContent.LayoutCache.RowsSize + 1);
								graphics2.FillRectangle(brush3, rect);
							}
							FooterContent.RenderingRect = new Rectangle(LayoutCache.FixedColsSize, num, Width - LayoutCache.FixedColsSize, FooterContent.LayoutCache.RowsSize + 1);
							BackBuffer.Graphics.Clip = new Region(FooterContent.RenderingRect);
							FooterContent.render(BackBuffer.Graphics, num, -HScrollBar.Value, Height, @fixed: false);
							RenderFooterContent();
							BackBuffer.Graphics.ResetClip();
							FooterContent.RenderingRect = new Rectangle(0, num, Width, FooterContent.LayoutCache.RowsSize + 1);
							BackBuffer.Graphics.Clip = new Region(FooterContent.RenderingRect);
							FooterContent.render(BackBuffer.Graphics, num, 0, Height, @fixed: true);
							RenderFixedFooterContent();
							if (FixedCols > 0 && !Setting.FixedColBorderColor.Equals(Color.Transparent))
							{
								using (Pen pen = (Pen)createPen(Setting.FixedColBorderColor, Setting.FixedColBorderStyle))
								{
									if (pen != null)
									{
										BackBuffer.Graphics.DrawLine(pen, LayoutCache.FixedColsSize, 0, LayoutCache.FixedColsSize, Height);
									}
								}
							}
							if (resizing != null)
							{
								BackBuffer.Graphics.ResetClip();
								using (SolidBrush brush4 = new SolidBrush(Setting.ResizingLineColor))
								{
									CResizing cResizing = resizing;
									BackBuffer.Graphics.FillRectangle(brush4, RenderCache.Cols[cResizing.Col].From + cResizing.Size - 1, 0, 2, Height);
									cResizing = null;
								}
							}
							RenderFin();
							Refresh();
						}
					}
					finally
					{
						render_flg = false;
					}
				}
			}
		}

		protected static object createPen(Color color, EBorderStyle style)
		{
			if (color.Equals(Color.Transparent))
			{
				return null;
			}
			Pen pen = new Pen(color);
			if (style.Equals(EBorderStyle.BOLD))
			{
				pen.Width = 2f;
			}
			switch (style)
			{
			case EBorderStyle.DOT:
				pen.DashStyle = DashStyle.Dot;
				break;
			case EBorderStyle.DASH:
				pen.DashStyle = DashStyle.Dash;
				break;
			case EBorderStyle.DASHDOT:
				pen.DashStyle = DashStyle.DashDot;
				break;
			case EBorderStyle.DASHDOTDOT:
				pen.DashStyle = DashStyle.DashDotDot;
				break;
			}
			return pen;
		}

		protected virtual void RenderContent()
		{
		}

		protected virtual void RenderFixedContent()
		{
		}

		protected virtual void RenderHeaderContent()
		{
		}

		protected virtual void RenderFixedHeaderContent()
		{
		}

		protected virtual void RenderFooterContent()
		{
		}

		protected virtual void RenderFixedFooterContent()
		{
		}

		protected virtual void RenderFin()
		{
		}

		private bool updateLayoutCaches()
		{
			bool flag = false;
			if (LayoutCacheInvalid)
			{
				updateLayoutCache();
				LayoutCacheInvalid = false;
				flag = true;
			}
			if (HeaderContent.LayoutCacheInvalid)
			{
				HeaderContent.updateLayoutCache();
				HeaderContent.LayoutCacheInvalid = false;
				flag = true;
			}
			if (FooterContent.LayoutCacheInvalid)
			{
				FooterContent.updateLayoutCache();
				FooterContent.LayoutCacheInvalid = false;
				flag = true;
			}
			if (Content.LayoutCacheInvalid)
			{
				Content.updateLayoutCache();
				Content.LayoutCacheInvalid = false;
				flag = true;
			}
			if (flag)
			{
				SelectRange = null;
			}
			return flag;
		}

		private void updateRenderCache(int top_col)
		{
			int num = 0;
			RenderCache.FocusField = null;
			RenderCache.Cols.Clear();
			RenderCache.Fields.Clear();
			checked
			{
				int num2 = FixedCols - 1;
				for (int i = 0; i <= num2; i++)
				{
					CRenderCache.CCol cCol = new CRenderCache.CCol();
					cCol.From = num;
					cCol.To = cCol.From + Cols.GetSize(i, 1);
					cCol.Appeared = Cols[i].Visible;
					RenderCache.Cols.Add(cCol);
					num = cCol.To;
				}
				num += top_col;
				int fixedCols = FixedCols;
				int num3 = Cols.Count - 1;
				for (int j = fixedCols; j <= num3; j++)
				{
					CRenderCache.CCol cCol2 = new CRenderCache.CCol();
					cCol2.From = num;
					cCol2.To = cCol2.From + Cols.GetSize(j, 1);
					if (Cols[j].Visible & (LayoutCache.FixedColsSize < cCol2.To))
					{
						cCol2.Appeared = true;
					}
					else
					{
						cCol2.Appeared = false;
					}
					RenderCache.Cols.Add(cCol2);
					num = cCol2.To;
				}
			}
		}

		private void UTable_Paint(object sender, PaintEventArgs e)
		{
			if (!DesignMode & (BackBuffer != null))
			{
				BackBuffer.Render(e.Graphics);
			}
		}

		public CRenderBlock RenderBlock()
		{
			if (_RenderBlock == null)
			{
				return _RenderBlock = createRenderBlock();
			}
			return null;
		}

		protected virtual CRenderBlock createRenderBlock()
		{
			return new CRenderBlock(this);
		}

		private void updateScrollBars(bool needUpdate)
		{
			checked
			{
				if (needUpdate)
				{
					int scrollValue = 0;
					int scrollValue2 = 0;
					if (VScrollBar.Visible)
					{
						scrollValue = VScrollBar.Value;
					}
					if (HScrollBar.Visible)
					{
						scrollValue2 = HScrollBar.Value;
					}
					int num = getLastColSize() + LayoutCache.FixedColsSize;
					int lastRowSize = getLastRowSize();
					int num2 = 0;
					int num3 = HeaderContent.LayoutCache.RowsSize + FooterContent.LayoutCache.RowsSize;
					showScrollBar(VScrollBar, Conversions.ToInteger(Operators.AddObject(Content.LayoutCache.RowsSize, Interaction.IIf(Setting.VScrollToEdge, Math.Max(Height - num3 - lastRowSize, 0), 0))), Height - num3, Setting.VScrollBarAlways, Setting.VScrollBarDisable);
					num2 = (VScrollBar.Visible ? ScrollBarWidth : 0);
					showScrollBar(HScrollBar, Conversions.ToInteger(Operators.AddObject(LayoutCache.ColsSize, Interaction.IIf(Setting.HScrollToEdge, Math.Max(Width - num - num2, 0), 0))), Width - num2, Setting.HScrollBarAlways, Setting.HScrollBarDisable);
					if (HScrollBar.Visible)
					{
						showScrollBar(VScrollBar, Conversions.ToInteger(Operators.AddObject(Content.LayoutCache.RowsSize, Interaction.IIf(Setting.VScrollToEdge, Math.Max(Height - num3 - ScrollBarWidth - lastRowSize, 0), 0))), Height - num3 - ScrollBarWidth, Setting.VScrollBarAlways, Setting.VScrollBarDisable);
					}
					if ((num2 == 0) & VScrollBar.Visible)
					{
						showScrollBar(HScrollBar, Conversions.ToInteger(Operators.AddObject(LayoutCache.ColsSize, Interaction.IIf(Setting.HScrollToEdge, Math.Max(Width - num - ScrollBarWidth, 0), 0))), Width - ScrollBarWidth, Setting.HScrollBarAlways, Setting.HScrollBarDisable);
					}
					if (VScrollBar.Visible)
					{
						setScrollValue(VScrollBar, scrollValue);
					}
					if (HScrollBar.Visible)
					{
						setScrollValue(HScrollBar, scrollValue2);
					}
				}
				_updateScrollbars();
			}
		}

		private int getLastColSize()
		{
			checked
			{
				for (int i = Cols.Count - 1; i >= 0; i += -1)
				{
					if (Cols[i].Visible)
					{
						return Cols[i].Size;
					}
				}
				return 0;
			}
		}

		private int getLastRowSize()
		{
			return Conversions.ToInteger(getLastRowSize_aux(Content, 0));
		}

		private object getLastRowSize_aux(CContent content, int level)
		{
			CRecord lastRecord = content.LayoutCache.LastRecord;
			if (lastRecord != null)
			{
				if (lastRecord.Child != null && lastRecord.Child.LayoutCache.LastRecord != null && Setting.VScrollToEdgeLevel > level)
				{
					return getLastRowSize_aux(lastRecord.Child, checked(level + 1));
				}
				return lastRecord.LayoutCache.EntireRowsSize;
			}
			return 0;
		}

		private void UTable_VisibleChanged(object sender, EventArgs e)
		{
			updateScrollBars(needUpdate: true);
		}

		private void _updateScrollbars()
		{
			checked
			{
				if (VScrollBar.Visible & HScrollBar.Visible)
				{
					Size size2 = VScrollBar.Size = new Size(ScrollBarWidth, Height - ScrollBarWidth);
					Point point2 = VScrollBar.Location = new Point(Width - ScrollBarWidth, 0);
					size2 = (HScrollBar.Size = new Size(Width - ScrollBarWidth, ScrollBarWidth));
					point2 = (HScrollBar.Location = new Point(0, Height - ScrollBarWidth));
					size2 = (scrollPlaceHolderPanel.Size = new Size(ScrollBarWidth, ScrollBarWidth));
					point2 = (scrollPlaceHolderPanel.Location = new Point(Width - ScrollBarWidth, Height - ScrollBarWidth));
					scrollPlaceHolderPanel.Visible = true;
					scrollPlaceHolderPanel.BackColor = SystemColors.Control;
				}
				else if (VScrollBar.Visible)
				{
					Size size2 = VScrollBar.Size = new Size(ScrollBarWidth, Height);
					Point point2 = VScrollBar.Location = new Point(Width - ScrollBarWidth, 0);
					scrollPlaceHolderPanel.Visible = false;
				}
				else if (HScrollBar.Visible)
				{
					Size size2 = HScrollBar.Size = new Size(Width, ScrollBarWidth);
					Point point2 = HScrollBar.Location = new Point(0, Height - ScrollBarWidth);
					scrollPlaceHolderPanel.Visible = false;
				}
				else
				{
					scrollPlaceHolderPanel.Visible = false;
				}
			}
		}

		private void showScrollBar(ScrollBar scrollBar, int entireSize, int displaySize, bool always, bool disable)
		{
			if (disable)
			{
				scrollBar.Minimum = 0;
				scrollBar.Maximum = 0;
				scrollBar.Value = scrollBar.Minimum;
				scrollBar.Visible = false;
				scrollBar.Enabled = false;
				return;
			}
			bool flag = false;
			if (entireSize > displaySize && displaySize > 0)
			{
				int num = 0;
				ScrollBar scrollBar2 = scrollBar;
				if (scrollBar2.Visible)
				{
					num = scrollBar2.Value;
				}
				scrollBar2.Minimum = 0;
				scrollBar2.Maximum = entireSize;
				scrollBar2.LargeChange = displaySize;
				scrollBar2.SmallChange = DefaultGridSize.Row;
				flag = true;
				scrollBar2 = null;
			}
			else
			{
				scrollBar.Minimum = 0;
				scrollBar.Maximum = 0;
				scrollBar.Value = scrollBar.Minimum;
			}
			scrollBar.Visible = (always | flag);
			scrollBar.Enabled = (flag & (Editor() == null));
		}

		private void VScrollBar_ValueChanged(object sender, EventArgs e)
		{
			if (_RenderBlock == null)
			{
				Render();
			}
		}

		private void ScrollBar_MouseEnter(object sender, EventArgs e)
		{
			Cursor = Cursors.Default;
		}

		private void HScrollBar_ValueChanged(object sender, EventArgs e)
		{
			if (_RenderBlock == null)
			{
				Render();
			}
		}

		private void UTable_MouseWheel(object sender, MouseEventArgs e)
		{
			if (VScrollBar.Visible & VScrollBar.Enabled)
			{
				wheelScroll(VScrollBar, e.Delta);
			}
			else if (HScrollBar.Visible & HScrollBar.Enabled)
			{
				wheelScroll(HScrollBar, e.Delta);
			}
		}

		private void wheelScroll(ScrollBar scrollBar, int delta)
		{
			int value = scrollBar.Value;
			value = checked((delta <= 0) ? (value + scrollBar.SmallChange * Setting.MouseWheelScrollRows) : (value - scrollBar.SmallChange * Setting.MouseWheelScrollRows));
			setScrollValue(scrollBar, value);
		}

		private int contentRowSize()
		{
			int num = 0;
			if (HScrollBar.Visible)
			{
				num = HScrollBar.Height;
			}
			return checked(Height - (HeaderContent.LayoutCache.RowsSize + FooterContent.LayoutCache.RowsSize) - num);
		}

		private int contentColSize()
		{
			int num = 0;
			if (VScrollBar.Visible)
			{
				num = VScrollBar.Width;
			}
			return checked(Width - num);
		}

		public void SetVScrollValue(int scrollValue)
		{
			setScrollValue(VScrollBar, scrollValue);
		}

		public void SetHScrollValue(int scrollValue)
		{
			setScrollValue(HScrollBar, scrollValue);
		}

		private void setScrollValue(ScrollBar scrollBar, int scrollValue)
		{
			checked
			{
				if (scrollBar.Visible & scrollBar.Enabled)
				{
					if (scrollValue < scrollBar.Minimum)
					{
						scrollBar.Value = scrollBar.Minimum;
					}
					else if (scrollValue > scrollBar.Maximum - scrollBar.LargeChange)
					{
						scrollBar.Value = scrollBar.Maximum - scrollBar.LargeChange;
					}
					else
					{
						scrollBar.Value = scrollValue;
					}
				}
			}
		}

		public void ScrollTo(CField field)
		{
			if (field != null)
			{
				UpdateLayout();
				Rectangle rect = field.GetRectangle();
				if (Setting.ScrollUnit == EScrollUnit.RECORD)
				{
					Rectangle rectangle = field.Record.GetRectangle();
					rect.Y = rectangle.Y;
					rect.Height = rectangle.Height;
				}
				DecideScrollRectEvent?.Invoke(field, ref rect);
				if (field.TopLevelContent() != Content)
				{
					HScrollTo(rect);
				}
				else
				{
					ScrollTo(rect);
				}
			}
		}

		public void ScrollTo(Rectangle rect)
		{
			VScrollTo(rect);
			HScrollTo(rect);
		}

		public void HScrollTo(Rectangle rect)
		{
			if (rect.Left < LayoutCache.FixedColsSize)
			{
				return;
			}
			checked
			{
				int num = rect.Left - LayoutCache.FixedColsSize;
				int num2 = rect.Right - contentColSize() + 1;
				if (HScrollBar.Value > num)
				{
					setScrollValue(HScrollBar, num);
				}
				else if (HScrollBar.Value < num2)
				{
					if (contentColSize() - LayoutCache.FixedColsSize >= rect.Width)
					{
						setScrollValue(HScrollBar, num2);
					}
					else
					{
						setScrollValue(HScrollBar, num);
					}
				}
			}
		}

		public void VScrollTo(Rectangle rect)
		{
			int num = checked(rect.Bottom - contentRowSize() + 1);
			if (VScrollBar.Value > rect.Top)
			{
				setScrollValue(VScrollBar, rect.Top);
			}
			else if (VScrollBar.Value < num)
			{
				if (contentRowSize() >= rect.Height)
				{
					setScrollValue(VScrollBar, num);
				}
				else
				{
					setScrollValue(VScrollBar, rect.Top);
				}
			}
		}

		public UTable()
			: this(new CKeyboardOperation())
		{
		}

		public UTable(CKeyboardOperation keyboardOperation)
		{
			DraggingScroll += UTable_DraggingScroll;
			base.KeyUp += UTable_KeyUp;
			base.Enter += UTable_Enter;
			base.MouseMove += UTable_MouseMove;
			base.KeyPress += UTable_KeyPress;
			base.MouseDown += UTable_MouseDown;
			base.GotFocus += UTable_GotFocus;
			base.MouseClick += UTable_MouseClick;
			base.LostFocus += UTable_LostFocus;
			base.Validating += UTable_Validating;
			base.MouseDoubleClick += UTable_MouseDoubleClick;
			base.Resize += UTable_Resize;
			base.KeyDown += UTable_KeyDown;
			base.Click += UTable_Click;
			base.MouseUp += UTable_MouseUp;
			base.Paint += UTable_Paint;
			base.MouseWheel += UTable_MouseWheel;
			base.VisibleChanged += UTable_VisibleChanged;
			base.HandleDestroyed += UTable_HandleDestroyed;
			cursor_resizing = null;
			resizing = null;
			_EditorContainer = null;
			finishEdit_flg = false;
			LeavingDirection = null;
			MouseDownField = null;
			MouseDoubleClicking = false;
			draggingScrollMove = new CDraggingScrollMove();
			draggingScrollTimer = new Timer();
			FocusGrid = new CGrid.CPoint();
			_FocusField = null;
			imeSetUpFlag = false;
			_SelectRange = null;
			_RangeDragging = false;
			DraggingRecord = null;
			draggingEditBlock = null;
			_RenderBlock = null;
			render_flg = false;
			ScrollBarWidth = 16;
			Setting = new CGlobalSetting();
			DefaultGridSize = new CGrid.CSize();
			LayoutCacheInvalid = false;
			BackBuffer = null;
			AutoExtend = new CAutoExtend();
			VScrollBar = new CVScrollBar();
			HScrollBar = new CHScrollBar();
			SortState = new CSortState();
			Comparer = new CComparer();
			FocusFieldDecorator = new CFocusDecorator();
			StrictMouseLocation = true;
			UndoBuffer = null;
			RedoBuffer = null;
			UndoBufferMax = 100;
			_UndoBufferEnabled = false;
			_FixedCols = 0;
			_NotSelectableCols = 0;
			scrollPlaceHolderPanel = new Panel();
			_EditBlock = null;
			_ignoreUndoBufferEnabled = false;
			ResetAll();
			DefaultGridSize.Row = 20;
			DefaultGridSize.Col = 100;
			AutoExtend.Row = false;
			AutoExtend.Col = false;
			BackColor = Color.Black;
			KeyboardOperation = keyboardOperation;
			VScrollBar.TabStop = false;
			HScrollBar.TabStop = false;
			scrollPlaceHolderPanel.Visible = false;
			Controls.Add(VScrollBar);
			Controls.Add(HScrollBar);
			Controls.Add(scrollPlaceHolderPanel);
			DoubleBuffered = true;
			Setting.EnterContent = Content;
			draggingScrollTimer.Interval = 50;
			draggingScrollTimer.Enabled = true;
		}

		private void UTable_HandleDestroyed(object sender, EventArgs e)
		{
			if (draggingScrollTimer != null)
			{
				draggingScrollTimer.Enabled = false;
				draggingScrollTimer.Dispose();
				draggingScrollTimer = null;
			}
		}

		public void ResetAll()
		{
			using (RenderBlock())
			{
				FinishEdit();
				FocusField = null;
				FixedCols = 0;
				ClearUndoBuffer();
				object obj = null;
				bool flag = Setting.EnterContent == HeaderContent;
				HeaderContent = new CContent();
				HeaderContent.Table = this;
				if (flag)
				{
					Setting.EnterContent = HeaderContent;
				}
				obj = null;
				object obj2 = null;
				bool flag2 = Setting.EnterContent == FooterContent;
				FooterContent = new CContent();
				FooterContent.Table = this;
				if (flag2)
				{
					Setting.EnterContent = FooterContent;
				}
				obj2 = null;
				object obj3 = null;
				bool flag3 = Setting.EnterContent == Content;
				Content = new CContent();
				Content.Table = this;
				if (flag3)
				{
					Setting.EnterContent = Content;
				}
				obj3 = null;
				Cols = new CGrids();
				LayoutCache = new CLayoutCache();
				RenderCache = new CRenderCache();
				DraggingRecord = null;
				draggingEditBlock = null;
				VScrollBar.Visible = false;
				HScrollBar.Visible = false;
			}
		}

		private void updateLayoutCache()
		{
			LayoutCache.ColsSize = 0;
			LayoutCache.FixedColsSize = 0;
			LayoutCache.SelectableCols = new List<CGrid>();
			LayoutCache.SelectableColsPos = new List<int>();
			LayoutCache.SelectableColsRev = new List<int>();
			int num = 0;
			checked
			{
				int num2 = Cols.Count - 1;
				for (int i = 0; i <= num2; i++)
				{
					if (Cols[i].Visible)
					{
						CLayoutCache layoutCache;
						if (i < FixedCols)
						{
							layoutCache = LayoutCache;
							layoutCache.FixedColsSize += Cols[i].Size;
						}
						num += Cols[i].Size;
						layoutCache = LayoutCache;
						layoutCache.ColsSize += Cols[i].Size;
						if (i >= NotSelectableCols)
						{
							LayoutCache.SelectableCols.Add(Cols[i]);
							LayoutCache.SelectableColsPos.Add(num);
						}
					}
					LayoutCache.SelectableColsRev.Add(LayoutCache.SelectableCols.Count - 1);
				}
			}
		}

		public void CreateCaption()
		{
			CreateCaption(EHAlign.DEFAULT);
		}

		public void CreateCaption(EHAlign halign)
		{
			List<CRecordProvider> list = new List<CRecordProvider>();
			for (CRecordProvider cRecordProvider = Content.RecordProvider; cRecordProvider != null; cRecordProvider = cRecordProvider.ChildRecordProvider)
			{
				list.Add(cRecordProvider);
			}
			CreateCaption(halign, list.ToArray());
		}

		public void CreateCaption(EHAlign halign, params CRecordProvider[] recordProviders)
		{
			checked
			{
				using (RenderBlock())
				{
					CContent cContent = HeaderContent;
					int num = recordProviders.Length - 1;
					for (int i = 0; i <= num; i++)
					{
						createCaption_aux(cContent, recordProviders[i], halign);
						cContent = cContent.Records[0].CreateChild();
					}
					HeaderContent.LayoutCacheInvalid = true;
				}
			}
		}

		private void createCaption_aux(CContent caption_content, CRecordProvider recordProvider, EHAlign halign)
		{
			CRecord cRecord = caption_content.AddRecord();
			foreach (object key in recordProvider.FieldDescs.Keys)
			{
				object objectValue = RuntimeHelpers.GetObjectValue(key);
				if (recordProvider.FieldDescs[RuntimeHelpers.GetObjectValue(objectValue)].CreateCaption)
				{
					CGrid.CRegion cRegion = (recordProvider.FieldDescs[RuntimeHelpers.GetObjectValue(objectValue)].MergeCaption != null) ? recordProvider.Layout(RuntimeHelpers.GetObjectValue(objectValue), RuntimeHelpers.GetObjectValue(recordProvider.FieldDescs[RuntimeHelpers.GetObjectValue(objectValue)].MergeCaption)) : recordProvider.Layout(RuntimeHelpers.GetObjectValue(objectValue));
					if (cRegion != null)
					{
						cRegion = cRegion.Clone();
					}
					CFieldDesc cFieldDesc = new CFieldDesc(new CCaptionFieldProvider(recordProvider.FieldDescs[RuntimeHelpers.GetObjectValue(objectValue)]), cRegion);
					if (halign != 0)
					{
						cFieldDesc.Setting.HorizontalAlignment = halign;
					}
					bool cancel = false;
					CreatingCaptionEvent?.Invoke(RuntimeHelpers.GetObjectValue(objectValue), cFieldDesc, ref cancel);
					if (!cancel)
					{
						cRecord.AddField(RuntimeHelpers.GetObjectValue(CaptionKey(RuntimeHelpers.GetObjectValue(objectValue))), cFieldDesc);
					}
				}
			}
		}

		public static object CaptionKey(object key)
		{
			return new CCaptionKey(RuntimeHelpers.GetObjectValue(key));
		}

		public void Clear()
		{
			Clear(recursive: true);
		}

		public void Clear(bool recursive)
		{
			using (RenderBlock())
			{
				HeaderContent.Clear(recursive);
				Content.Clear(recursive);
				FooterContent.Clear(recursive);
			}
		}

		public CRecord CaptionRecord()
		{
			if (HeaderContent.Records.Count == 0)
			{
				HeaderContent.AddRecord();
			}
			return HeaderContent.Records[0];
		}

		public void RaiseFieldSelected(CField field, ref bool handled)
		{
			field.raiseSelected(ref handled);
			FieldSelectedEvent?.Invoke(field, ref handled);
		}

		internal void raiseFieldValidating(CField field, CancelEventArgs e)
		{
			field.raiseValidating(e);
			FieldValidatingEvent?.Invoke(field, e);
		}

		internal void raiseFieldValidated(CField field)
		{
			field.raiseValidated();
			FieldValidatedEvent?.Invoke(field);
			if (_EditBlock != null && !_EditBlock.Disposing)
			{
				_EditBlock.AddEditedField(field);
			}
			else
			{
				field.Content().raiseValidated(RuntimeHelpers.GetObjectValue(field.Key));
			}
		}

		internal void raiseFieldValueChanged(CField field)
		{
			field.raiseValueChanged();
			FieldValueChangedEvent?.Invoke(field);
		}

		internal void raiseEditorValueChanged(CField field)
		{
			field.raiseEditorValueChanged();
			EditorValueChangedEvent?.Invoke(field);
		}

		internal void raiseFieldButtonClick(CField field)
		{
			field.raiseButtonClick();
			FieldButtonClickEvent?.Invoke(field);
		}

		internal void raiseEditStarting(CField field, ref EAllow editable)
		{
			field.raiseEditStarting(ref editable);
			EditStartingEvent?.Invoke(field, ref editable);
		}

		internal void raiseEditStart(CField field, IEditor editor)
		{
			field.raiseEditStart(editor);
			EditStartEvent?.Invoke(field, editor);
		}

		internal void raiseEditFinished(CField field)
		{
			field.raiseEditFinished();
			EditFinishedEvent?.Invoke(field);
		}

		internal void raiseRecordCreated(CRecord record)
		{
			RecordCreatedEvent?.Invoke(record);
		}

		internal void raiseRecordAdded(CRecord record)
		{
			RecordAddedEvent?.Invoke(record);
		}

		internal void raiseRecordMoved(CRecord record)
		{
			RecordMovedEvent?.Invoke(record);
		}

		internal void raiseRecordRemoved(CRecord record)
		{
			RecordRemovedEvent?.Invoke(record);
		}

		internal void raiseRecordEnter(CRecord record)
		{
			RecordEnterEvent?.Invoke(record);
		}

		internal void raiseContentValidated(CContent content, object key)
		{
			ContentValidatedEvent?.Invoke(content, RuntimeHelpers.GetObjectValue(key));
		}

		internal void raiseContentEnter(CContent content)
		{
			ContentEnterEvent?.Invoke(content);
		}

		internal void raiseSorted(CContent content, object key)
		{
			SortedEvent?.Invoke(content, RuntimeHelpers.GetObjectValue(key));
		}

		internal void raiseFocusNextField(CField field, bool forward, ref bool handled)
		{
			field.raiseFocusNextField(forward, ref handled);
			FocusNextFieldEvent?.Invoke(field, forward, ref handled);
		}

		internal void raiseFocusNextControl(CField field, bool forward, ref bool handled)
		{
			field.raiseFocusNextControl(forward, ref handled);
			FocusNextControlEvent?.Invoke(field, forward, ref handled);
		}

		internal void raiseVerticalMergingBreak(CField field1, CField field2, ref bool @break)
		{
			VerticalMergingBreakEvent?.Invoke(field1, field2, ref @break);
		}

		public CEditBlock EditBlock()
		{
			if (_EditBlock == null)
			{
				return _EditBlock = createEditBlock();
			}
			return null;
		}

		protected virtual CEditBlock createEditBlock()
		{
			return new CEditBlock(this);
		}

		public void ClearUndoBuffer()
		{
			if (UndoBuffer != null)
			{
				UndoBuffer.Clear();
			}
			if (RedoBuffer != null)
			{
				RedoBuffer.Clear();
			}
		}

		internal void addModify(CContent content, IModify modify)
		{
			if (!(_ignoreUndoBufferEnabled | (UndoBuffer == null)))
			{
				if (_EditBlock != null)
				{
					_EditBlock.ModifyContainers.Add(new CModifyContainer(content, modify));
					return;
				}
				List<CModifyContainer> list = new List<CModifyContainer>();
				list.Add(new CModifyContainer(content, modify));
				addModify(list);
			}
		}

		internal void addModify(List<CModifyContainer> modifyContainers)
		{
			if (!(_ignoreUndoBufferEnabled | (UndoBuffer == null)))
			{
				UndoBuffer.Add(modifyContainers);
				RedoBuffer.Clear();
				if (UndoBuffer.Count > UndoBufferMax)
				{
					UndoBuffer.RemoveRange(0, checked(UndoBuffer.Count - UndoBufferMax));
				}
			}
		}

		public void Undo()
		{
			checked
			{
				if (UndoBuffer != null && UndoBuffer.Count != 0)
				{
					_ignoreUndoBufferEnabled = true;
					try
					{
						int index = UndoBuffer.Count - 1;
						List<CModifyContainer> list = UndoBuffer[index];
						using (RenderBlock())
						{
							for (int i = list.Count - 1; i >= 0; i += -1)
							{
								CModifyContainer cModifyContainer = list[i];
								cModifyContainer.Modify.Undo(cModifyContainer.Content);
							}
							RedoBuffer.Add(list);
							UndoBuffer.RemoveAt(index);
						}
						UndoFinishedEvent?.Invoke(this, list);
					}
					finally
					{
						_ignoreUndoBufferEnabled = false;
					}
				}
			}
		}

		public void Redo()
		{
			checked
			{
				if (RedoBuffer != null && RedoBuffer.Count != 0)
				{
					_ignoreUndoBufferEnabled = true;
					try
					{
						int index = RedoBuffer.Count - 1;
						List<CModifyContainer> list = RedoBuffer[index];
						using (RenderBlock())
						{
							int num = list.Count - 1;
							for (int i = 0; i <= num; i++)
							{
								CModifyContainer cModifyContainer = list[i];
								cModifyContainer.Modify.Redo(cModifyContainer.Content);
							}
							UndoBuffer.Add(list);
							RedoBuffer.RemoveAt(index);
						}
						RedoFinishedEvent?.Invoke(this, list);
					}
					finally
					{
						_ignoreUndoBufferEnabled = false;
					}
				}
			}
		}
	}
}
