﻿using jp.co.fsi.common.dataaccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.fsi.Logics
{
	public static class UtilDataCheck
	{

		readonly static log4net.ILog log =
			log4net.LogManager.GetLogger(
				System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		/// <summary>
		/// 同じ名前が存在すると帳票出力時のPIVOTでエラーとなるため
		/// 回避するためのチェック
		/// </summary>
		/// <param name="dba"></param>
		/// <param name="strKaissha"></param>
		/// <param name="strRptType"></param>
		/// <param name="strRptNo"></param>
		/// <param name="strRetuNo"></param>
		/// <param name="strRetuName"></param>
		/// <returns></returns>
		public static bool IsChohyoKbnName(DbAccess dba,
			                           string strKaissha,
									   string strRptType,
									   string strRptNo,
									   string strRetuName)
		{

			bool blRet = false;

			try
			{

				dba.BeginTransaction();

				string strSql = @"SELECT
                                       RETSU_NO
                                  FROM
                                      TB_HN_RPT_RETSU_KBN
                                  WHERE
                                       KAISHA_CD = @KAISHA_CD
                                    AND
                                       RETSU_NM  = @RETSU_NM
                                    AND
                                       RPT_TYPE  = @RPT_TYPE
                                    AND
                                       RPT_NO    = @RPT_NO
                                 ";

				DbParamCollection whereParam = new DbParamCollection();

				whereParam.SetParam("@RETSU_NM", SqlDbType.VarChar, 20, strRetuName);
				whereParam.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, strRptType); 
				whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, strKaissha);
				whereParam.SetParam("@RPT_NO", SqlDbType.Decimal, 1, strRptNo);

				DataTable ldt = dba.GetDataTableFromSqlWithParams(strSql, whereParam);

				if (null != ldt)
				{
					if (0 < ldt.Rows.Count)
					{
						blRet = true;
					}
				}

			}
			catch (Exception ex)
			{

				log.Error("帳票タイトル名称マスタアクセス時にエラーが発生しました。"
					+ Environment.NewLine + ex.Message);
			}
			finally
			{
				dba.Rollback();
			}
			return blRet;
		}

	}
}
