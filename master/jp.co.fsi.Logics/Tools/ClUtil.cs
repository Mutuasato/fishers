﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace jp.co.fsi.Logics
{
    /// <summary>
    /// ユーティリティクラス
    /// </summary>
    public static class ClUtil
    {
        #region ログ
        readonly static log4net.ILog log =
            log4net.LogManager.GetLogger(
                System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region publicメソッド
        /// <summary>
        /// 文字列への変換
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の文字列</returns>
        public static string ToString(object val)
        {
            if (val == null) return string.Empty;

            return val.ToString();
        }

        /// <summary>
        /// 数値(int)への変換
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の数値(int)</returns>
        public static int ToInt(object val)
        {
            int result;
            if (!Int32.TryParse(val.ToString(), out result))
            {
                result = 0;
            }
            return result;
        }

        /// <summary>
        /// 数値(long)への変換
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の数値(long)</returns>
        public static long ToLong(object val)
        {
            long result;
            if (!Int64.TryParse(val.ToString(), out result))
            {
                result = 0;
            }
            return result;
        }

        /// <summary>
        /// 数値(decimal)への変換
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の数値(decimal)</returns>
        public static decimal ToDecimal(object val)
        {
            decimal result;
            if (!Decimal.TryParse(val.ToString(), out result))
            {
                result = Decimal.Parse("0");
            }
            return result;
        }

        /// <summary>
        /// 引数で指定したデータを日付に変換します。
        /// </summary>
        /// <param name="val">変換する値</param>
        /// <returns>日付オブジェクト</returns>
        public static DateTime ToDate(object val)
        {
            try
            {
                //2019-11-12 吉村 キャストミス修正
                //return (DateTime)val;
                return DateTime.Parse(val.ToString());
            }
            catch (Exception e)
            {
                DateTime tmpDat;
                if (!DateTime.TryParse(ClUtil.ToString(val), out tmpDat))
                {
                    return new DateTime();
                }

                log.Error("日付変換時にエラーが発生しました。"+e.Message, e);

                return tmpDat;
            }
        }

        /// <summary>
        /// 引数で指定した日付データをyyyy/MM/dd形式の文字列に変換します。
        /// </summary>
        /// <param name="val">変換する値</param>
        /// <returns>文字列(yyyy/MM/dd形式)</returns>
        public static string ToDateStr(object val)
        {
            return ClUtil.ToDate(val).ToString("yyyy/MM/dd");
        }

        /// <summary>
        /// 引数で指定した日付データを引数で指定したフォーマットの文字列に変換します。
        /// </summary>
        /// <param name="val">変換する値</param>
        /// <param name="format">書式文字列</param>
        /// <returns>文字列(yyyy/MM/dd形式)</returns>
        /// <remarks>
        /// 引数formatに指定する文字列はMicrosoftの.Netフレームワークが
        /// サポートする書式なら何でもOKです。
        /// 詳細はググr…MicrosoftのWebを参照して下さい。
        /// </remarks>
        public static string ToDateStr(object val, string format)
        {
            return ClUtil.ToDate(val).ToString(format);
        }

        /// <summary>
        /// ゼロ埋めをする(引数が整数Ver.)
        /// </summary>
        /// <param name="num">フォーマットする数値</param>
        /// <param name="length">全体の文字長</param>
        /// <returns>フォーマットされた文字列</returns>
        public static string PadZero(int num, int length)
        {
            string result = string.Empty;
            result = String.Format("{0:D" + ClUtil.ToString(length) + "}", num);

            return result;
        }

        /// <summary>
        /// 数値をカンマ区切りでフォーマットする(整数のみ)
        /// </summary>
        /// <param name="val">フォーマットする値(数値変換できる値)</param>
        /// <returns>フォーマットされた文字列</returns>
        public static string FormatNum(object val)
        {
            return ClUtil.FormatNum(val, 0);
        }

        /// <summary>
        /// 数値をカンマ区切りでフォーマットする(小数部含む)
        /// </summary>
        /// <param name="val">フォーマットする値(数値変換できる値)</param>
        /// <param name="decLen">小数以下の桁数</param>
        /// <returns>フォーマットされた文字列</returns>
        public static string FormatNum(object val, int decLen)
        {
            decimal decVal = 0;
            if (!Decimal.TryParse(val.ToString(), out decVal))
            {
                return null;
            }

            string formatStr = "#,0";
            if (decLen > 0)
            {
                formatStr += "." + new String('0', decLen);
            }

            return decVal.ToString(formatStr);
        }


        /// <summary>
        /// 文字列のバイト長を取得します。
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static int GetByteLength(string val)
        {
            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            return sjisEnc.GetByteCount(val);
        }

        /// <summary>
        /// null値をDBNullに変換します。
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の値</returns>
        public static object ConvDBNull(object val)
        {
            if (val == null)
            {
                return DBNull.Value;
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// 10進値を、指定した小数部の桁数に丸めます。 
        /// </summary>
        /// <param name="d">丸め対象の10進数。</param>
        /// <param name="decimals">戻り値の小数部の桁数。</param>
        /// <returns></returns>
        public static decimal Round(decimal d, int decimals)
        {
            return Math.Round(d, decimals, MidpointRounding.AwayFromZero);
        }


        /// <summary>
        /// exeが配置されているPathを取得します。
        /// </summary>
        /// <returns>exeが配置されているPath</returns>
        public static string GetPath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        }

        /// <summary>
        /// 印刷用使用列名文字配列の作成
        /// </summary>
        /// <param name="colsCnt">列数</param>
        /// <param name="Prefix">接頭詞</param>
        /// <returns>印刷テーブル使用列名文字列</returns>
        public static StringBuilder ColsArray(int colsCnt, string Prefix)
        {
            StringBuilder cols = new StringBuilder();
            string itemNm = "ITEM";
            string wk;
            for (int i = 1; i < colsCnt + 1; i++)
            {
                if (i == 1)
                {
                    wk = "  " + Prefix + itemNm + ClUtil.PadZero(i, 2);
                }
                else
                {
                    wk = " ," + Prefix + itemNm + ClUtil.PadZero(i, 2);
                }
                cols.Append(wk);
            }
            return cols;
        }



        /// <summary>
        /// 指定したファイルパスをエクスプローラで開きます。
        /// </summary>
        /// <param name="path"></param>
        public static void OpenFolder(string path)
        {
            System.Diagnostics.Process.Start("EXPLORER.EXE", @"/select," + path);
        }

        #endregion
    }
}
