﻿using jp.co.fsi.Logics.constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace jp.co.fsi.Logics
{
    /// <summary>
    /// 設定ファイル(xml形式)を読み込むクラスです。
    /// </summary>
    public class ConfigLoader
    {
        #region 定数
        /// <summary>
        /// 設定ファイルのファイル名
        /// </summary>
        private const string CONFIG_FILE_NM = "config.xml";

        /// <summary>
        /// 共通設定のノード
        /// </summary>
        private const string COMMON_NODE = "Common";
        #endregion

        #region private変数
        /// <summary>
        /// XmlDocumentオブジェクト
        /// </summary>
        XmlDocument _xmlDoc;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ConfigLoader()
        {
            // XmlDocumentオブジェクトのインスタンスの生成
            this._xmlDoc = new XmlDocument();
            try
            {
                // 現在のアセンブリが存在するディレクトリを取得
                string path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                //MessageBox.Show(path,"path");
                this._xmlDoc.Load(Path.Combine(path, CONFIG_FILE_NM));
            }
            catch (Exception)
            {
                // 何もしない(デザイナへの影響を考慮)
            }
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 共通の設定情報を読み込みます。
        /// </summary>
        /// <param name="pnode">Common要素配下の親ノード名</param>
        /// <param name="cnode">ノードの中の子ノード名</param>
        /// <returns>設定値</returns>
        public string LoadCommonConfig(string pnode, string cnode)
        {
            // XMLノードの選択
            XmlNode xNode = this._xmlDoc.SelectSingleNode("/Config/Common/" + pnode + "/" + cnode);
            return xNode.InnerText;
        }

        /// <summary>
        /// 共通の設定情報に書き込みます。
        /// </summary>
        /// <param name="pnode">Common要素配下の親ノード名</param>
        /// <param name="cnode">ノードの中の子ノード名</param>
        /// <param name="value">書き込む値</param>
        public void SetCommonConfig(string pnode, string cnode, string value)
        {
            // XMLノードの選択
            XmlNode xNode = this._xmlDoc.SelectSingleNode("/Config/Common/" + pnode + "/" + cnode);

            xNode.InnerText = value;
        }

        /// <summary>
        /// プログラム個別の設定情報を読み込みます。
        /// </summary>
        /// <param name="subSys">サブシステム</param>
        /// <param name="pgId">プログラムID</param>
        /// <param name="pnode">プログラムID配下の親ノード名</param>
        /// <param name="cnode">pnodeの配下のノード名</param>
        /// <returns>設定値</returns>
        public string LoadPgConfig(Constants.SubSys subSys, string pgId, string pnode, string cnode)
        {
            // XMLノードの選択
            XmlNode xNode = this._xmlDoc.SelectSingleNode("/Config/" + Util.ToString(subSys) + "/" + pgId + "/" + pnode + "/" + cnode);

            return xNode.InnerText;
        }

        /// <summary>
        /// プログラム個別の設定情報に書き込みます。
        /// </summary>
        /// <param name="subSys">サブシステム</param>
        /// <param name="pgId">プログラムID</param>
        /// <param name="pnode">プログラムID配下の親ノード名</param>
        /// <param name="cnode">pnodeの配下のノード名</param>
        /// <param name="value">書き込む値</param>
        public void SetPgConfig(Constants.SubSys subSys, string pgId, string pnode, string cnode, string value)
        {
            // XMLノードの選択
            XmlNode xNode = this._xmlDoc.SelectSingleNode("/Config/" + Util.ToString(subSys) + "/" + pgId + "/" + pnode + "/" + cnode);

            xNode.InnerText = value;
        }

        /// <summary>
        /// サブシステム毎の帳票mdbファイルのパスを読み込みます。
        /// </summary>
        /// <param name="subSys">サブシステム</param>
        /// <remarks>TODO:恐らく未使用。</remarks>
        /// <returns>設定値</returns>
        public string LoadReportPath(Constants.SubSys subSys)
        {
            // XMLノードの選択
            XmlNode xNode = this._xmlDoc.SelectSingleNode("/Config/" + Util.ToString(subSys) + "/Report/mdbpath");

            return xNode.InnerText;
        }

        public string LoadSSHConfig(string pnode, string cnode)
        {
            // XMLノードの選択
            XmlNode xNode = this._xmlDoc.SelectSingleNode("/Config/" + pnode + "/" + cnode);

            return xNode.InnerText;
        }


        /// <summary>
        /// 書き込んだ設定を保存する
        /// </summary>
        public void SaveConfig()
        {
            // 現在のアセンブリが存在するディレクトリを取得
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            this._xmlDoc.Save(Path.Combine(path, CONFIG_FILE_NM));
        }

        /// <summary>
        /// 書き込んだ設定を読み直す
        /// </summary>
        public void ReloadConfig()
        {
            // 現在のアセンブリが存在するディレクトリを取得
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            this._xmlDoc.Load(Path.Combine(path, CONFIG_FILE_NM));
        }
        #endregion
    }
}
