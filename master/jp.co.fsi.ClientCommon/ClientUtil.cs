﻿using jp.co.fsi.ClientCommon.WsvCustomers;
using jp.co.fsi.common.util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;

namespace jp.co.fsi.ClientCommon
{
    public static class ClientUtil
    {

        #region ログ
        static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(
                System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： Windowフォーム端のまるめ処理</para>
        /// <para>◇ 処理概要： Windowフォーム端のまるめ処理</para>
        /// <para>◇ </para>
        /// <para>◇ 作成日: 2020/01/05</para>
        /// <para>◇ 作成者: FSI</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        public static void CircleForm(Form fm,int radi)
        {

            // フォームの境界線、タイトルバーを無しに設定
            fm.FormBorderStyle = FormBorderStyle.None;

            int radius = radi;
            int diameter = radius * 2;
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();

            // 左上
            gp.AddPie(0, 0, diameter, diameter, 180, 90);
            // 右上
            gp.AddPie(fm.Width - diameter, 0, diameter, diameter, 270, 90);
            // 左下
            gp.AddPie(0, fm.Height - diameter, diameter, diameter, 90, 90);
            // 右下
            gp.AddPie(fm.Width - diameter, fm.Height - diameter, diameter, diameter, 0, 90);
            // 中央
            gp.AddRectangle(new Rectangle(radius, 0, fm.Width - diameter, fm.Height));
            // 左
            gp.AddRectangle(new Rectangle(0, radius, radius, fm.Height - diameter));
            // 右
            gp.AddRectangle(new Rectangle(fm.Width - radius, radius, radius, fm.Height - diameter));

            fm.Region = new Region(gp);

        }

        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： 画像サイズ設定</para>
        /// <para>◇ 処理概要： メニューで利用している画像サイズの調整</para>
        /// <para>◇ </para>
        /// <para>◇ 作成日: 2020/01/05</para>
        /// <para>◇ 作成者: FSI</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        public static void SetListThumbnail(string strPath,ListView ls,ImageList limg)
        {

            int ww = 72;
            int hh = 72;

            limg.Images.Clear();

            limg.ImageSize = new Size(ww, hh);

            ls.LargeImageList = limg;
            //ls.StateImageList = limg;

            Image orginInp = null;
            Image thumbInp = null;
            Image orginRep = null;
            Image thumbRep = null;

            Image orginInpc = null;
            Image thumbInpc = null;
            Image orginRepc = null;
            Image thumbRepc = null;

            try
            {
                orginInp = Bitmap.FromFile(@".\imgs\Keyborad.png");
                orginRep = Bitmap.FromFile(@".\imgs\printer.png");
                orginInpc = Bitmap.FromFile(@".\imgs\Keyboradc.png");
                orginRepc = Bitmap.FromFile(@".\imgs\printerc.png");

                thumbInp = CreateThumbnail(orginInp,ww,hh);
                thumbRep = CreateThumbnail(orginRep, ww, hh);
                thumbInpc = CreateThumbnail(orginInpc, ww, hh);
                thumbRepc = CreateThumbnail(orginRepc, ww, hh);

                limg.Images.Add(thumbInp);
                limg.Images.Add(thumbRep);
                limg.Images.Add(thumbInpc);
                limg.Images.Add(thumbRepc);

            }
            catch (Exception ex)
            {
                log.Error("メニュー画像調整時のエラー" + ex.Message,ex);
            }

        }


        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： 画像サイズ設定</para>
        /// <para>◇ 処理概要： メニューで利用している画像サイズの調整</para>
        /// <para>◇ </para>
        /// <para>◇ 作成日: 2020/01/05</para>
        /// <para>◇ 作成者: FSI</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        private static Image CreateThumbnail(Image img,int w,int h)
        {

            Bitmap canvas = new Bitmap(w, h);

            Graphics g = Graphics.FromImage(canvas);

            try
            {

                g.FillRectangle(new SolidBrush(Color.Transparent), 0, 0, w, h);

                decimal fw = decimal.Parse(w.ToString()) / decimal.Parse(h.ToString());

                decimal fh = decimal.Parse(w.ToString()) / decimal.Parse(h.ToString());

                decimal scale = Math.Min(fw, fh);

                fw = img.Width * scale;
                fh = img.Height * scale;

                g.DrawImage(img, float.Parse(((w - fw) / 2).ToString()), float.Parse(((h - fh) / 2).ToString()), (float)fw, (float)fh);


            }
            catch(Exception ex)
            {
                log.Error("メニュー画像調整時のエラー" + ex.Message, ex);
            }
            finally
            {
                g.Dispose();
            }

            return canvas;

        }


    }


}
