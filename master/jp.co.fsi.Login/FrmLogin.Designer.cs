﻿namespace jp.co.fsi.Login
{
	partial class FrmLogin
	{
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnLogin = new System.Windows.Forms.Button();
			this.txtPassWord = new jp.co.fsi.common.controls.FsiTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtUserId = new jp.co.fsi.common.controls.FsiTextBox();
			this.lblUsers = new System.Windows.Forms.Label();
			this.panel1 = new jp.co.fsi.common.FsiPanel();
			this.chkLogin = new System.Windows.Forms.CheckBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lblLoginTitle = new System.Windows.Forms.Label();
			this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
			this.dtShisho = new System.Windows.Forms.DataGridView();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.dgvList = new System.Windows.Forms.DataGridView();
			this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.panel1.SuspendLayout();
			this.fsiPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dtShisho)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.LightSkyBlue;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.ForeColor = System.Drawing.Color.Navy;
			this.btnCancel.Location = new System.Drawing.Point(437, 199);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(132, 48);
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "キャンセル";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btn_Click);
			// 
			// btnLogin
			// 
			this.btnLogin.BackColor = System.Drawing.Color.LightSkyBlue;
			this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLogin.ForeColor = System.Drawing.Color.Navy;
			this.btnLogin.Location = new System.Drawing.Point(437, 113);
			this.btnLogin.Name = "btnLogin";
			this.btnLogin.Size = new System.Drawing.Size(132, 50);
			this.btnLogin.TabIndex = 5;
			this.btnLogin.Text = "ログイン";
			this.btnLogin.UseVisualStyleBackColor = false;
			this.btnLogin.Click += new System.EventHandler(this.btn_Click);
			// 
			// txtPassWord
			// 
			this.txtPassWord.AutoSizeFromLength = false;
			this.txtPassWord.DisplayLength = null;
			this.txtPassWord.Font = new System.Drawing.Font("ＭＳ 明朝", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtPassWord.Location = new System.Drawing.Point(164, 71);
			this.txtPassWord.MaxLength = 10;
			this.txtPassWord.Multiline = true;
			this.txtPassWord.Name = "txtPassWord";
			this.txtPassWord.PasswordChar = '●';
			this.txtPassWord.Size = new System.Drawing.Size(193, 31);
			this.txtPassWord.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.ForeColor = System.Drawing.Color.Navy;
			this.label1.Location = new System.Drawing.Point(7, 69);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(156, 33);
			this.label1.TabIndex = 1;
			this.label1.Text = "パスワード:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtUserId
			// 
			this.txtUserId.AutoSizeFromLength = false;
			this.txtUserId.DisplayLength = null;
			this.txtUserId.Font = new System.Drawing.Font("ＭＳ 明朝", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtUserId.Location = new System.Drawing.Point(164, 32);
			this.txtUserId.MaxLength = 10;
			this.txtUserId.Multiline = true;
			this.txtUserId.Name = "txtUserId";
			this.txtUserId.Size = new System.Drawing.Size(193, 33);
			this.txtUserId.TabIndex = 1;
			this.txtUserId.Text = "12345";
			// 
			// lblUsers
			// 
			this.lblUsers.Font = new System.Drawing.Font("ＭＳ 明朝", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblUsers.ForeColor = System.Drawing.Color.Navy;
			this.lblUsers.Location = new System.Drawing.Point(7, 30);
			this.lblUsers.Name = "lblUsers";
			this.lblUsers.Size = new System.Drawing.Size(156, 39);
			this.lblUsers.TabIndex = 0;
			this.lblUsers.Text = "ユーザーID:";
			this.lblUsers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.DeepSkyBlue;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.chkLogin);
			this.panel1.Controls.Add(this.lblUsers);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.txtPassWord);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.txtUserId);
			this.panel1.Location = new System.Drawing.Point(53, 113);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(376, 138);
			this.panel1.TabIndex = 4;
			// 
			// chkLogin
			// 
			this.chkLogin.AutoSize = true;
			this.chkLogin.ForeColor = System.Drawing.Color.Navy;
			this.chkLogin.Location = new System.Drawing.Point(20, 109);
			this.chkLogin.Name = "chkLogin";
			this.chkLogin.Size = new System.Drawing.Size(219, 20);
			this.chkLogin.TabIndex = 3;
			this.chkLogin.TabStop = false;
			this.chkLogin.Text = "次回からはログイン不要。";
			this.chkLogin.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.ForeColor = System.Drawing.Color.Navy;
			this.label2.Location = new System.Drawing.Point(12, 80);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(628, 19);
			this.label2.TabIndex = 0;
			this.label2.Text = "ユーザーログイン";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.ForeColor = System.Drawing.Color.Navy;
			this.label3.Location = new System.Drawing.Point(102, 6);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(187, 19);
			this.label3.TabIndex = 5;
			this.label3.Text = "漁協統合システム";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblLoginTitle
			// 
			this.lblLoginTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lblLoginTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblLoginTitle.ForeColor = System.Drawing.Color.Navy;
			this.lblLoginTitle.Location = new System.Drawing.Point(12, 41);
			this.lblLoginTitle.Name = "lblLoginTitle";
			this.lblLoginTitle.Size = new System.Drawing.Size(628, 19);
			this.lblLoginTitle.TabIndex = 0;
			this.lblLoginTitle.Text = "ユーザーログイン";
			this.lblLoginTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// fsiPanel2
			// 
			this.fsiPanel2.Controls.Add(this.dtShisho);
			this.fsiPanel2.Controls.Add(this.label5);
			this.fsiPanel2.Controls.Add(this.label4);
			this.fsiPanel2.Controls.Add(this.dgvList);
			this.fsiPanel2.Location = new System.Drawing.Point(72, 288);
			this.fsiPanel2.Name = "fsiPanel2";
			this.fsiPanel2.Size = new System.Drawing.Size(492, 337);
			this.fsiPanel2.TabIndex = 7;
			// 
			// dtShisho
			// 
			this.dtShisho.AllowUserToAddRows = false;
			this.dtShisho.AllowUserToDeleteRows = false;
			this.dtShisho.AllowUserToResizeColumns = false;
			this.dtShisho.AllowUserToResizeRows = false;
			dataGridViewCellStyle45.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle45.ForeColor = System.Drawing.Color.Black;
			this.dtShisho.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle45;
			this.dtShisho.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.dtShisho.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dtShisho.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle46.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle46.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle46.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle46.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle46.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dtShisho.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle46;
			this.dtShisho.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dtShisho.EnableHeadersVisualStyles = false;
			this.dtShisho.Location = new System.Drawing.Point(14, 221);
			this.dtShisho.Margin = new System.Windows.Forms.Padding(4);
			this.dtShisho.MultiSelect = false;
			this.dtShisho.Name = "dtShisho";
			this.dtShisho.ReadOnly = true;
			this.dtShisho.RowHeadersVisible = false;
			this.dtShisho.RowTemplate.Height = 21;
			this.dtShisho.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dtShisho.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dtShisho.Size = new System.Drawing.Size(462, 101);
			this.dtShisho.TabIndex = 8;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(19, 201);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 3;
			this.label5.Text = "支所選択";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(19, 13);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 16);
			this.label4.TabIndex = 2;
			this.label4.Text = "漁協選択";
			// 
			// dgvList
			// 
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			dataGridViewCellStyle47.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle47.ForeColor = System.Drawing.Color.Black;
			this.dgvList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle47;
			this.dgvList.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.dgvList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dgvList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle48.BackColor = System.Drawing.Color.LightSkyBlue;
			dataGridViewCellStyle48.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			dataGridViewCellStyle48.ForeColor = System.Drawing.Color.Navy;
			dataGridViewCellStyle48.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle48.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle48;
			this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.EnableHeadersVisualStyles = false;
			this.dgvList.Location = new System.Drawing.Point(14, 33);
			this.dgvList.Margin = new System.Windows.Forms.Padding(4);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new System.Drawing.Size(462, 159);
			this.dgvList.TabIndex = 7;
			// 
			// fsiPanel3
			// 
			this.fsiPanel3.BackgroundImage = global::jp.co.fsi.Login.Properties.Resources.サラリーマン;
			this.fsiPanel3.Cursor = System.Windows.Forms.Cursors.Hand;
			this.fsiPanel3.Location = new System.Drawing.Point(16, 9);
			this.fsiPanel3.Name = "fsiPanel3";
			this.fsiPanel3.Size = new System.Drawing.Size(33, 28);
			this.fsiPanel3.TabIndex = 0;
			this.toolTip1.SetToolTip(this.fsiPanel3, "漁協情報・支所の切り替え");
			this.fsiPanel3.Click += new System.EventHandler(this.fsiPanel3_Click);
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.fsiPanel1.BackColor = System.Drawing.Color.SkyBlue;
			this.fsiPanel1.BackgroundImage = global::jp.co.fsi.Login.Properties.Resources.電源のアイコン素材;
			this.fsiPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.fsiPanel1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.fsiPanel1.Location = new System.Drawing.Point(610, 9);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(33, 28);
			this.fsiPanel1.TabIndex = 0;
			this.toolTip1.SetToolTip(this.fsiPanel1, "閉じる");
			this.fsiPanel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.fsiPanel1_MouseDown);
			// 
			// FrmLogin
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.BackColor = System.Drawing.Color.SkyBlue;
			this.ClientSize = new System.Drawing.Size(655, 669);
			this.Controls.Add(this.fsiPanel3);
			this.Controls.Add(this.fsiPanel2);
			this.Controls.Add(this.fsiPanel1);
			this.Controls.Add(this.lblLoginTitle);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnLogin);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.MenuFrm = this;
			this.Name = "FrmLogin";
			this.Text = "ログイン";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLogin_KeyDown);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmLogin_MouseDown);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmLogin_MouseMove);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.fsiPanel2.ResumeLayout(false);
			this.fsiPanel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dtShisho)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Label lblUsers;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnLogin;
		private common.controls.FsiTextBox txtPassWord;
		private common.controls.FsiTextBox txtUserId;
		private jp.co.fsi.common.FsiPanel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLoginTitle;
		private common.FsiPanel fsiPanel1;
		private common.FsiPanel fsiPanel2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.DataGridView dgvList;
		private System.Windows.Forms.DataGridView dtShisho;
		private System.Windows.Forms.CheckBox chkLogin;
		private common.FsiPanel fsiPanel3;
		private System.Windows.Forms.ToolTip toolTip1;
	}
}

