﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jp.co.fsi.common.CommonFrm
{
	public partial class frmWarrning : Form
	{

		public string STR_MESSAGE;

		public string STR_DATE;

		public frmWarrning()
		{
			InitializeComponent();
		}

		private void fsiPanel1_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void frmWarrning_Load(object sender, EventArgs e)
		{
			if ("完　　了" == STR_MESSAGE)
			{
				lblKigenn.Visible = false;
				lblLastDate.Visible = false;
			}
			else
			{
				lblKigenn.Visible = true;
				lblLastDate.Visible = true;

				lblLastDate.Text = STR_DATE;
			}

		}
	}
}
