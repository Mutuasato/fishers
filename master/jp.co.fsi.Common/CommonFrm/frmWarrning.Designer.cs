﻿namespace jp.co.fsi.common.CommonFrm
{
	partial class frmWarrning
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWarrning));
			this.panel1 = new System.Windows.Forms.Panel();
			this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.lblMessage = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.lblKigenn = new System.Windows.Forms.Label();
			this.lblLastDate = new System.Windows.Forms.Label();
			this.btnOk = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.SkyBlue;
			this.panel1.Controls.Add(this.fsiPanel1);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(802, 44);
			this.panel1.TabIndex = 0;
			// 
			// fsiPanel1
			// 
			this.fsiPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.fsiPanel1.BackColor = System.Drawing.Color.SkyBlue;
			this.fsiPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fsiPanel1.BackgroundImage")));
			this.fsiPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.fsiPanel1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.fsiPanel1.Location = new System.Drawing.Point(757, 10);
			this.fsiPanel1.Name = "fsiPanel1";
			this.fsiPanel1.Size = new System.Drawing.Size(33, 28);
			this.fsiPanel1.TabIndex = 1;
			this.fsiPanel1.Click += new System.EventHandler(this.fsiPanel1_Click);
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label1.ForeColor = System.Drawing.Color.Navy;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(802, 44);
			this.label1.TabIndex = 0;
			this.label1.Text = "システムからの重要なお知らせ";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label2.Location = new System.Drawing.Point(76, 65);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(340, 21);
			this.label2.TabIndex = 1;
			this.label2.Text = "お客様のプログラム利用期限が【";
			// 
			// lblMessage
			// 
			this.lblMessage.AutoSize = true;
			this.lblMessage.Font = new System.Drawing.Font("ＭＳ 明朝", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblMessage.Location = new System.Drawing.Point(416, 65);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(102, 21);
			this.lblMessage.TabIndex = 2;
			this.lblMessage.Text = "完了間近";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label3.Location = new System.Drawing.Point(518, 65);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(186, 21);
			this.label3.TabIndex = 3;
			this.label3.Text = "】となりました。";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.label4.Location = new System.Drawing.Point(76, 103);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(538, 21);
			this.label4.TabIndex = 4;
			this.label4.Text = "契約したシステム会社への連絡をお願いいたします。";
			// 
			// lblKigenn
			// 
			this.lblKigenn.AutoSize = true;
			this.lblKigenn.Font = new System.Drawing.Font("ＭＳ 明朝", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblKigenn.Location = new System.Drawing.Point(120, 161);
			this.lblKigenn.Name = "lblKigenn";
			this.lblKigenn.Size = new System.Drawing.Size(571, 21);
			this.lblKigenn.TabIndex = 5;
			this.lblKigenn.Text = "------------------- 期限終了日 ------------------- ";
			this.lblKigenn.Visible = false;
			// 
			// lblLastDate
			// 
			this.lblLastDate.AutoSize = true;
			this.lblLastDate.Font = new System.Drawing.Font("ＭＳ 明朝", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.lblLastDate.Location = new System.Drawing.Point(314, 203);
			this.lblLastDate.Name = "lblLastDate";
			this.lblLastDate.Size = new System.Drawing.Size(164, 21);
			this.lblLastDate.TabIndex = 6;
			this.lblLastDate.Text = "2019念05月31日";
			this.lblLastDate.Visible = false;
			// 
			// btnOk
			// 
			this.btnOk.BackColor = System.Drawing.Color.SkyBlue;
			this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOk.Location = new System.Drawing.Point(354, 248);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(98, 42);
			this.btnOk.TabIndex = 7;
			this.btnOk.Text = "閉じる";
			this.btnOk.UseVisualStyleBackColor = false;
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// frmWarrning
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.ClientSize = new System.Drawing.Size(802, 311);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.lblLastDate);
			this.Controls.Add(this.lblKigenn);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.lblMessage);
			this.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "frmWarrning";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "frmWarrning";
			this.Load += new System.EventHandler(this.frmWarrning_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private FsiPanel fsiPanel1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblKigenn;
		private System.Windows.Forms.Label lblLastDate;
		private System.Windows.Forms.Button btnOk;
	}
}