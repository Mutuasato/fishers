﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace jp.co.fsi.common
{
    public class FsiTableLayoutPanel : TableLayoutPanel
    {
        // jp.co.fsi.common.FsiTableLayoutPanel
        public FsiTableLayoutPanel()
        {
            base.DoubleBuffered = true;
            base.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
        }
    }
}
