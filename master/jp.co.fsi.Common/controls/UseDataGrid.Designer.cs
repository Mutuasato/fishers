﻿namespace jp.co.fsi.common.controls
{
	partial class UseDataGrid
	{
		/// <summary> 
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region コンポーネント デザイナーで生成されたコード

		/// <summary> 
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlHeader = new jp.co.fsi.common.FsiPanel();
			this.pblBody = new jp.co.fsi.common.FsiPanel();
			this.pnlFooter = new jp.co.fsi.common.FsiPanel();
			this.headTbl = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.bodyTbl = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.footTbl = new jp.co.fsi.common.FsiTableLayoutPanel();
			this.pnlHeader.SuspendLayout();
			this.pblBody.SuspendLayout();
			this.pnlFooter.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlHeader
			// 
			this.pnlHeader.Controls.Add(this.headTbl);
			this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnlHeader.Location = new System.Drawing.Point(0, 0);
			this.pnlHeader.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pnlHeader.Name = "pnlHeader";
			this.pnlHeader.Size = new System.Drawing.Size(704, 37);
			this.pnlHeader.TabIndex = 0;
			// 
			// pblBody
			// 
			this.pblBody.AutoScroll = true;
			this.pblBody.Controls.Add(this.bodyTbl);
			this.pblBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pblBody.Location = new System.Drawing.Point(0, 37);
			this.pblBody.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pblBody.Name = "pblBody";
			this.pblBody.Size = new System.Drawing.Size(704, 316);
			this.pblBody.TabIndex = 1;
			// 
			// pnlFooter
			// 
			this.pnlFooter.Controls.Add(this.footTbl);
			this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlFooter.Location = new System.Drawing.Point(0, 353);
			this.pnlFooter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pnlFooter.Name = "pnlFooter";
			this.pnlFooter.Size = new System.Drawing.Size(704, 93);
			this.pnlFooter.TabIndex = 2;
			// 
			// headTbl
			// 
			this.headTbl.ColumnCount = 1;
			this.headTbl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.headTbl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.headTbl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.headTbl.Location = new System.Drawing.Point(0, 0);
			this.headTbl.Name = "headTbl";
			this.headTbl.RowCount = 1;
			this.headTbl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.headTbl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.headTbl.Size = new System.Drawing.Size(704, 37);
			this.headTbl.TabIndex = 0;
			// 
			// bodyTbl
			// 
			this.bodyTbl.ColumnCount = 1;
			this.bodyTbl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.bodyTbl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.bodyTbl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.bodyTbl.Location = new System.Drawing.Point(0, 0);
			this.bodyTbl.Name = "bodyTbl";
			this.bodyTbl.RowCount = 1;
			this.bodyTbl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.bodyTbl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.bodyTbl.Size = new System.Drawing.Size(704, 316);
			this.bodyTbl.TabIndex = 0;
			// 
			// footTbl
			// 
			this.footTbl.ColumnCount = 1;
			this.footTbl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.footTbl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.footTbl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.footTbl.Location = new System.Drawing.Point(0, 0);
			this.footTbl.Name = "footTbl";
			this.footTbl.RowCount = 1;
			this.footTbl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.footTbl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.footTbl.Size = new System.Drawing.Size(704, 93);
			this.footTbl.TabIndex = 1;
			// 
			// UseDataGrid
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.pblBody);
			this.Controls.Add(this.pnlFooter);
			this.Controls.Add(this.pnlHeader);
			this.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Name = "UseDataGrid";
			this.Size = new System.Drawing.Size(704, 446);
			this.pnlHeader.ResumeLayout(false);
			this.pblBody.ResumeLayout(false);
			this.pnlFooter.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		public jp.co.fsi.common.FsiPanel pnlHeader;
		public jp.co.fsi.common.FsiPanel pblBody;
		public jp.co.fsi.common.FsiPanel pnlFooter;
		private jp.co.fsi.common.FsiTableLayoutPanel headTbl;
		private jp.co.fsi.common.FsiTableLayoutPanel bodyTbl;
		private jp.co.fsi.common.FsiTableLayoutPanel footTbl;
	}
}
