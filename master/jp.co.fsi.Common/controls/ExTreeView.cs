﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace jp.co.fsi.Common
{
#pragma warning disable CS0252 // 認識できない #pragma ディレクティブです
#pragma warning disable CS0414 // 認識できない #pragma ディレクティブです
    public class ExTreeView : TreeView
    {
        #region コントロール用変数 Dispose処理を行う為の変数定義

        private System.ComponentModel.Container components;
        
        #endregion

        #region プログラム変数

        /// <summary>
        /// 自動ツリー構造作成フラグ
        /// </summary>
        private bool m_autoBuild = true;

        /// <summary>
        /// データバインディングで利用する変数
        /// </summary>
        private CurrencyManager m_currencyManager = null;

        /// <summary>
        /// データバインディングに必要な変数
        /// </summary>
        private string m_ValueMemer = string.Empty;

        /// <summary>
        /// データバインディング用変数
        /// </summary>
        private string m_displayMember = string.Empty;

        private object m_DataSource = null;

        private bool blLoopCheck = false;

        private ArrayList treeGroups = new ArrayList();

        private ImageList _ilStateImages = null;

        private bool _bUseTriState = false;

        private bool _bPreventCheckEvent = false;

        private bool _bCheckBoxesVisible = false;

        private bool bMouseUpCancel = false;

        private string strChechkBindingColumnName = string.Empty;

        #endregion

        #region プロパティ
        public new bool CheckBoxes
        {
            get 
            {
                return _bCheckBoxesVisible; 
            }
            set 
            {
                _bCheckBoxesVisible = value;
                base.CheckBoxes = _bCheckBoxesVisible;
                if (_bCheckBoxesVisible)
                {
                    this.StateImageList = _ilStateImages;
                }
            }
        }

        public bool CheckBoxesTriState
        {
            get 
            {
                return _bUseTriState; 
            }
            set 
            {
                _bUseTriState = value;
                this.CheckBoxes = value;
                this.StateImageList = _ilStateImages;   
            }
        }

        [Bindable(false)]
        public new ImageList StateImageList
        {
            get
            {
                return base.StateImageList;
            }
            set
            {
                base.StateImageList = value;
            }
        }

        public bool AutoBuildTree
        {
            get
            {
                return m_autoBuild;
            }
            set
            {
                m_autoBuild = value;
            }
        }

        public object DataSource
        {

            get
            {
                return m_DataSource;
            }
            set
            {

                if (null == value)
                {
                    this.m_currencyManager = null;
                    this.Nodes.Clear();
                    this.RemoveAllGroups();
                }
                else
                {

                    if ((value == typeof(IList)) || (this.m_DataSource == typeof(IListSource)))
                    {
                        throw new Exception("データソースの型が違います。");
                    }
                    else
                    {
                        if (value == typeof(IListSource))
                        {
                            IListSource myListSource = (IListSource)value;
                            if (myListSource.ContainsListCollection)
                            {
                                throw new Exception("データソースの型が違います。");
                            }
                        }

                        this.m_DataSource = value;
                        this.m_currencyManager = (CurrencyManager)this.BindingContext[this.m_DataSource];

                        if (this.AutoBuildTree)
                        {
                            BuildTree();
                        }
                    }
                }
            }
        }

        public string ValueMember
        {
            get
            {
                return this.m_ValueMemer;
            }
            set
            {
                this.m_ValueMemer = value;
            }
        }

        public string DisplayMember
        {
            get
            {
                return this.m_displayMember;
            }
            set
            {
                this.m_displayMember = value;
            }
        }

        public string CheckBoxBindingField
        {
            get
            {
                return strChechkBindingColumnName;
            }
            set
            {
                strChechkBindingColumnName = value;
            }
        }
        #endregion


        #region パブリックメソッド
        public object GetValue(int index)
        {
            IList innerList = (IList)this.m_currencyManager;
            if (null != innerList)
            {
                if ((!string.IsNullOrEmpty(this.ValueMember)) && (index > 0 && 0<innerList.Count))
                {
                    PropertyDescriptor pdValueMember = this.m_currencyManager.GetItemProperties()[this.ValueMember];
                    return pdValueMember.GetValue(innerList[index]);
                }
            }
            return null;
        }

        public object GetDisplay(int index)
        {
            IList innerList = (IList)this.m_currencyManager;
            if (null != innerList)
            {
                if ((!string.IsNullOrEmpty(this.DisplayMember)) && (index > 0 && 0 < innerList.Count))
                {
                    PropertyDescriptor pdValueMember = this.m_currencyManager.GetItemProperties()[this.DisplayMember];
                    return pdValueMember.GetValue(innerList[index]);
                }
            }
            return null;
        }

        public void AllClear()
        {

            this.m_currencyManager = null;

            this.Nodes.Clear();

            this.RemoveAllGroups();
            

        }


        public void BuildTree()
        {


            try
            {
                this.Nodes.Clear();

                if ((null != this.m_currencyManager) && (null != this.m_currencyManager.List))
                {

                    IList innerList = this.m_currencyManager.List;
                    TreeNodeCollection currNode = this.Nodes;
                    int currGroupIndex = 0;
                    int currListIndex = 0;

                    if (this.treeGroups.Count > currGroupIndex)
                    {
                        #region
                        OneCheckOnlyGroup currGroup = (OneCheckOnlyGroup)treeGroups[currGroupIndex];

                        ExOneCheckOnlyTreeNode myFirstNode = null;

                        PropertyDescriptor pdGroupBy = null;
                        PropertyDescriptor pdValue = null;
                        PropertyDescriptor pdDisplay = null;
                        pdGroupBy = this.m_currencyManager.GetItemProperties()[currGroup.GroupBy];
                        pdValue = this.m_currencyManager.GetItemProperties()[currGroup.ValueMember];
                        pdDisplay = this.m_currencyManager.GetItemProperties()[currGroup.DisplayMember];

                        string currGroupBy = null;

                        if (innerList.Count > currGroupIndex)
                        {
                            object currObject = null;

                            #region while
                            while (currListIndex < innerList.Count)
                            {
                                currObject = innerList[currListIndex];
                                if (pdGroupBy.GetValue(currObject).ToString() != currGroupBy)
                                {
                                    currGroupBy = pdGroupBy.GetValue(currObject).ToString();
                                    myFirstNode =
                                        new ExOneCheckOnlyTreeNode(currGroup.Name,
                                                 pdDisplay.GetValue(currObject).ToString(),
                                                 currObject,
                                                 pdValue.GetValue(innerList[currListIndex]),
                                                 currGroup.ImageIndex,
                                                 currGroup.SelectedImageIndex,
                                                 currListIndex
                                            );

                                    currNode.Add((TreeNode)myFirstNode);
                                }
                                else
                                {
                                    AddNodes(currGroupIndex, ref currListIndex, myFirstNode.Nodes, currGroup.GroupBy);
                                }
                            }
                            #endregion

                        }

                        #endregion
                    }
                    else
                    {
                        #region
                        while (currListIndex < innerList.Count)
                        {
                            AddNodes(currGroupIndex, ref currListIndex, this.Nodes, string.Empty);
                        }

                        #endregion
                    }
                }

                blLoopCheck = false;

                KaisouLoop(this.Nodes);

            }
            catch (Exception ex)
            {
                MessageBox.Show("階層表示エラー:" + ex.Message);
            }
        }
        #endregion

        #region 階層表示
        private void KaisouLoop(TreeNodeCollection r)
        {
            foreach (ExOneCheckOnlyTreeNode rChild in r)
            {
                if (0 < rChild.Nodes.Count)
                {
                    KaisouLoop(rChild.Nodes);
                }
                else
                {
                    SetTreeCheckBox(rChild);
                }

            }

        }
        #endregion

        #region ノード作成処理 メソッド名：AddNodes

        private void AddNodes(int currGroupIndex, 
                              ref int currentListIndex, 
                              TreeNodeCollection currNode, 
                              string prevGroupByField)
        {
            IList innerList = this.m_currencyManager.List;
            PropertyDescriptor pdPrevGroupBy = null;
            string prevGroupByValue = string.Empty;
            OneCheckOnlyGroup currGroup = null;

            if (!string.IsNullOrEmpty(prevGroupByField))
            {
                pdPrevGroupBy = this.m_currencyManager.GetItemProperties()[prevGroupByField];
            }
            currGroupIndex++;

            if (this.treeGroups.Count > currGroupIndex)
            {
                currGroup = (OneCheckOnlyGroup)this.treeGroups[currGroupIndex];
                PropertyDescriptor pdGroupBy = null;
                PropertyDescriptor pdValue = null;
                PropertyDescriptor pdDisplay = null;
                pdGroupBy = this.m_currencyManager.GetItemProperties()[currGroup.GroupBy];
                pdValue = this.m_currencyManager.GetItemProperties()[currGroup.ValueMember];
                pdDisplay = this.m_currencyManager.GetItemProperties()[currGroup.DisplayMember];
                string currGroupBy = null;
                if (innerList.Count > currentListIndex)
                {
                    if (null != pdPrevGroupBy)
                    {
                        prevGroupByValue = pdPrevGroupBy.GetValue(innerList[currentListIndex]).ToString();
                    }
                    ExOneCheckOnlyTreeNode myFirstNode = null;

                    object currObject = null;

                    while ((currentListIndex < innerList.Count) &&
                        (null != pdPrevGroupBy) &&
                        (pdPrevGroupBy.GetValue(innerList[currentListIndex]).ToString() == prevGroupByValue))
                    {
                        currObject = innerList[currentListIndex];
                        if (pdGroupBy.GetValue(currObject).ToString() != currGroupBy)
                        {

                            currGroupBy = pdGroupBy.GetValue(currObject).ToString();
                            
                            myFirstNode = new ExOneCheckOnlyTreeNode(
                                currGroup.Name,
                                pdDisplay.GetValue(currObject).ToString(),
                                currObject,
                                pdValue.GetValue(innerList[currentListIndex]),
                                currGroup.ImageIndex,
                                currGroup.SelectedImageIndex,
                                currentListIndex);

                            currNode.Add((TreeNode)myFirstNode);
                        }
                        else
                        {
                            AddNodes(currGroupIndex, ref currentListIndex, myFirstNode.Nodes, currGroup.GroupBy);
                        }
                    }
                }
            }
            else
            {
                ExOneCheckOnlyTreeNode myNewLeafNode = null;
                object currObject = this.m_currencyManager.List[currentListIndex];

                if ((null != this.DisplayMember) && (null != this.ValueMember) &&
                    !(string.IsNullOrEmpty(this.DisplayMember)) && !(string.IsNullOrEmpty(this.ValueMember)))
                {

                    PropertyDescriptor pdDisplayloc = this.m_currencyManager.GetItemProperties()[this.DisplayMember];
                    PropertyDescriptor pdValueloc = this.m_currencyManager.GetItemProperties()[this.ValueMember];
                    PropertyDescriptor pdCheckValueloc = this.m_currencyManager.GetItemProperties()[this.CheckBoxBindingField];
                    string strGroupName = string.Empty;

                    if (null == this.Tag)
                    {
                        strGroupName = string.Empty;
                    }
                    else
                    {
                        strGroupName = this.Tag.ToString();
                    }

                    myNewLeafNode = new ExOneCheckOnlyTreeNode(
                        strGroupName,
                        pdDisplayloc.GetValue(currObject).ToString(),
                        currObject,
                        pdValueloc.GetValue(currObject),
                        currentListIndex
                    );

                    myNewLeafNode.CheckBoxFieldName = this.CheckBoxBindingField;

                    if (pdCheckValueloc.GetValue(currObject).ToString() == "1")
                    {
                        myNewLeafNode.Checked = true;
                    }
                }
                else
                {
                    myNewLeafNode = new ExOneCheckOnlyTreeNode(
                                      string.Empty,
                                      currentListIndex.ToString(),
                                      currObject,
                                      currObject,
                                      this.ImageIndex,
                                      this.SelectedImageIndex,
                                      currentListIndex);
                }
                currNode.Add((TreeNode)myNewLeafNode);
                currentListIndex++;
            }
        }
        #endregion

        #region グループ削除
        public void RemoveGroup(object group)
        {
            if (this.treeGroups.Contains(group))
            {
                this.treeGroups.Remove(group);
                if (this.AutoBuildTree)
                {
                    BuildTree();
                }
            }
        }
        public void RemoveGroup(string groupName)
        {

            foreach (OneCheckOnlyGroup gg in this.treeGroups)
            {
                if (groupName == gg.Name)
                {
                    RemoveGroup(gg);
                }
            }
        }
        #endregion

        #region すべてのグループ削除
        public void RemoveAllGroups()
        {
            this.treeGroups.Clear();
        }
        #endregion

        #region グループ追加
        public void AddGroup(OneCheckOnlyGroup group)
        {

            try
            {

                treeGroups.Add(group);
                if (this.AutoBuildTree)
                {
                    BuildTree();
                }
            }
            catch (NotSupportedException exNots)
            {
                MessageBox.Show("NotSupportedException エラー:" + exNots.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void AddGroup(
            string name,
            string groupBy,
            string displayMember,
            string valueMember,
            int imageIndex,
            int selectedImageIndex            
        )
        {
            OneCheckOnlyGroup myNewGroup =
                new OneCheckOnlyGroup(
                    name,
                    groupBy,
                    displayMember,
                    valueMember,
                    imageIndex,
                    selectedImageIndex
                );

            this.AddGroup(myNewGroup);
        }
        #endregion

        #region グループの取得
        public OneCheckOnlyGroup[] GetGroups()
        {

            return (OneCheckOnlyGroup[])treeGroups.ToArray(typeof(Group));

        }
        #endregion

        #region 枝データセット
        public void SetLeafData(
            string name,
            string displayMember,
            string valueMember,
            string checkBindingColumnName,
            int imageIndex,
            int selectedImageIndex
        )
        {

            this.Tag = name;
            this.DisplayMember = displayMember;
            this.ValueMember = valueMember;
            this.ImageIndex = imageIndex;
            this.SelectedImageIndex = selectedImageIndex;
            this.CheckBoxBindingField = checkBindingColumnName;
        }
        #endregion

        #region 枝データセット状態確認
        public bool IsLeadNode(TreeNode node)
        {
            if (node.Nodes.Count == 0)
            {
                return true;    
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region ノード検索
        public TreeNode FindNodeByValue(object Value, TreeNodeCollection nodesToSearch)
        {
            int idx = 0;
            TreeNode currNode = null;
            ExOneCheckOnlyTreeNode leafNode = null;

            while (idx < nodesToSearch.Count)
            {
                currNode = nodesToSearch[idx];

                if (null == currNode.LastNode)
                {
                    leafNode = (ExOneCheckOnlyTreeNode)currNode;

                    if (leafNode.Value.ToString() == Value.ToString())
                    {
                        return currNode;
                    }
                }
                else
                {
                    currNode = FindNodeByValue(Value, currNode.Nodes);
                    if (null != currNode)
                    {
                        return currNode;
                    }
                }
            }
            return null;
        }
        #endregion

        #region ポジション検索
        private TreeNode FindNodeByPosition(int posIndex)
        {
            return FindNodeByPosition(posIndex,this.Nodes);
        }

        private TreeNode FindNodeByPosition(int posIndex, TreeNodeCollection nodeToSearch)
        {

            int i = 0;
            TreeNode currNode = null;

            ExOneCheckOnlyTreeNode leafNode = null;

            while (i < nodeToSearch.Count)
            {
                currNode = nodeToSearch[i];
                i++;
                if (leafNode.Position == posIndex)
                {
                    return currNode;
                }
                else
                {
                    currNode = FindNodeByPosition(posIndex, currNode.Nodes);

                    if (null != currNode)
                    {
                        return currNode;
                    }
                }
            }

            return null;
        }

        #endregion

        #region 継承イベント
        protected override void OnAfterSelect(TreeViewEventArgs e)
        {
            ExOneCheckOnlyTreeNode leafNode =
                (ExOneCheckOnlyTreeNode)e.Node;

            if (null != leafNode)
            {
                if (this.m_currencyManager.Position != leafNode.Position)
                {
                    this.m_currencyManager.Position = leafNode.Position;
                }
            }

            base.OnAfterSelect(e);
        }
        #endregion

        #region チェックステート関連メソッドアンドプロパティ
        private void SetCheckState()
        {
            CheckBoxState cbsState;
            Graphics gfxCheckBox = null;
            Bitmap bmpCheckBox  = null;

            _ilStateImages = new ImageList();

            cbsState = CheckBoxState.UncheckedNormal;

            for (int i = 0;i < 3;i++)
            {
                bmpCheckBox = new Bitmap(16, 16);
                gfxCheckBox = Graphics.FromImage(bmpCheckBox);

                switch (i)
                {
                    case 0:
                        cbsState = CheckBoxState.UncheckedNormal;
                        break;
                    case 1:
                        cbsState = CheckBoxState.CheckedNormal;
                        break;
                    case 2:
                        cbsState = CheckBoxState.MixedNormal;
                        break;
                }
                CheckBoxRenderer.DrawCheckBox(gfxCheckBox, new Point(2, 2), cbsState);
                gfxCheckBox.Save();
                _ilStateImages.Images.Add(bmpCheckBox);
                _bUseTriState = true;
            }
        }
        #endregion

        protected override void OnLayout(LayoutEventArgs levent)
        {
            base.OnLayout(levent);

            this.Refresh();

        }

        #region リフレッシュ
        public override void Refresh()
        {

            Stack<ExOneCheckOnlyTreeNode> stNodes = null;

            ExOneCheckOnlyTreeNode tnStacked = null;

            base.Refresh();

            if (!this.CheckBoxes)
            {
                return;
            }

            base.CheckBoxes = false;

            stNodes = new Stack<ExOneCheckOnlyTreeNode>(this.Nodes.Count);

            foreach (ExOneCheckOnlyTreeNode tnCurrent in this.Nodes)
            {
                stNodes.Push(tnCurrent);
            }

            while(0<stNodes.Count)
            {
                tnStacked = stNodes.Pop();

                if (tnStacked.StateImageIndex == -1)
                {
                    if (tnStacked.Checked)
                    {
                        tnStacked.StateImageIndex = 1;
                    }
                    else
                    {
                        tnStacked.StateImageIndex = 0;
                    }
                }

                for (int i = 0;i<tnStacked.Nodes.Count;i++)
                {
                    stNodes.Push((ExOneCheckOnlyTreeNode)tnStacked.Nodes[i]);
                }
            }
        }
        #endregion

        #region 展開後の処理
        protected override void OnAfterExpand(TreeViewEventArgs e)
        {
            base.OnAfterExpand(e);

            foreach (ExOneCheckOnlyTreeNode tnCurrent in e.Node.Nodes)
            {
                if (tnCurrent.Checked)
                {
                    tnCurrent.StateImageIndex = 1;
                }
                else
                {
                    tnCurrent.StateImageIndex = 0;
                }
            }
        }
        #endregion

        #region チェックボックスが押された後の処理

        protected override void OnAfterCheck(TreeViewEventArgs e)
        {
            base.OnAfterCheck(e);
            if (_bPreventCheckEvent)
            {
                return;
            }
            OnNodeMouseClick(new TreeNodeMouseClickEventArgs(e.Node, MouseButtons.None, 0, 0, 0));
        }

        #endregion

        #region ノードをクリックした際のイベント
        protected override void OnNodeMouseClick(TreeNodeMouseClickEventArgs e)
        {
            SetTreeCheckBox(e);
        }

        private void SetTreeCheckBox(TreeNodeMouseClickEventArgs e)
        {

            base.OnMouseClick(e);

            Stack<ExOneCheckOnlyTreeNode> stNodes = null;

            ExOneCheckOnlyTreeNode tnBuffer = null;

            bool bMixedState = false;

            int iSpacing = 0;

            int iIndex = 0;

            int i1 = 0;

            int i2 = 0;

            _bPreventCheckEvent = true;

            if (null != ImageList)
            {
                iSpacing = 18;
            }

            i1 = e.Node.Bounds.Left - iSpacing;
            i2 = e.Node.Bounds.Left - (iSpacing + 16);

            if (((e.X > i1) || (e.X < i2)) && (e.Button != System.Windows.Forms.MouseButtons.None))
            {
                return;
            }

            tnBuffer = (ExOneCheckOnlyTreeNode)e.Node;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                tnBuffer.Checked = !tnBuffer.Checked;
            }

            if (tnBuffer.Checked)
            {
                tnBuffer.StateImageIndex = 1;
            }
            else
            {
                tnBuffer.StateImageIndex = tnBuffer.StateImageIndex;
            }
            
            OnAfterCheck(new TreeViewEventArgs(tnBuffer, TreeViewAction.ByMouse));

            stNodes = new Stack<ExOneCheckOnlyTreeNode>(tnBuffer.Nodes.Count);

            stNodes.Push(tnBuffer);

            while (0 < stNodes.Count)
            {
                tnBuffer = stNodes.Pop();
                tnBuffer.Checked = e.Node.Checked;
                for (int i = 0; i < tnBuffer.Nodes.Count; i++)
                {
                    stNodes.Push((ExOneCheckOnlyTreeNode)tnBuffer.Nodes[i]);
                }
            }

            bMixedState = false;

            tnBuffer = (ExOneCheckOnlyTreeNode)e.Node;

            while (tnBuffer.Parent == null)
            {

                foreach (ExOneCheckOnlyTreeNode tnChild in tnBuffer.Parent.Nodes)
                {
                    if ((tnChild.Checked != tnBuffer.Checked) || (tnChild.StateImageIndex == 2))
                    {
                        bMixedState = true;
                    }
                }

                // ３２ビット、６４ビットＯＳ対応

                if (Environment.Is64BitOperatingSystem)
                {
                    iIndex = (int)Convert.ToUInt64(tnBuffer.Checked);
                }
                else
                {
                    iIndex = (int)Convert.ToUInt32(tnBuffer.Checked);
                }

                if (bMixedState || iIndex > 0)
                {
                    tnBuffer.Parent.Checked = true;
                }
                else
                {
                    tnBuffer.Parent.Checked = false;
                }

                if (bMixedState)
                {
                    if (this.CheckBoxesTriState)
                    {
                        tnBuffer.Parent.StateImageIndex = 2;
                    }
                    else
                    {
                        tnBuffer.Parent.StateImageIndex = 1;
                    }
                }
                else
                {
                    tnBuffer.Parent.StateImageIndex = iIndex;
                }

                tnBuffer = (ExOneCheckOnlyTreeNode)tnBuffer.Parent;
            }
            _bPreventCheckEvent = false;
        }

        #endregion

        #region チェックボックス生成
        private void SetTreeCheckBox(ExOneCheckOnlyTreeNode e)
        {

            Stack<ExOneCheckOnlyTreeNode> stNodes = null;
            ExOneCheckOnlyTreeNode tnBuffer = null;
            bool bMixedState = false;
//            int iSpacing = 0;
            int iIndex = 0;
//            int i1 = 0;
//            int i2 = 0;

            this._bPreventCheckEvent = true;
            tnBuffer = e;


            if (tnBuffer.Checked)
            {
                tnBuffer.StateImageIndex = 1;
            }
            else
            {
                tnBuffer.StateImageIndex = tnBuffer.StateImageIndex;
            }

            OnAfterCheck(new TreeViewEventArgs(tnBuffer, TreeViewAction.ByMouse));

            stNodes = new Stack<ExOneCheckOnlyTreeNode>();

            stNodes.Push(tnBuffer);

            while(stNodes.Count>0)
            {
                tnBuffer=stNodes.Pop();
                tnBuffer.Checked = e.Checked;
                for(int i = 0;i<tnBuffer.Nodes.Count;i++)
                {
                    stNodes.Push((ExOneCheckOnlyTreeNode)tnBuffer.Nodes[i]);
                }
            }

            bMixedState = false;

            tnBuffer = e;

            while(tnBuffer.Parent !=null)
            {
                foreach(ExOneCheckOnlyTreeNode tnChild in tnBuffer.Parent.Nodes)
                {
                    if ((tnChild.Checked = tnBuffer.Checked) || (tnChild.StateImageIndex == 2))
                    {
                        bMixedState = true;
                    }
                }

                if (Environment.Is64BitOperatingSystem)
                {
                    iIndex = (int)Convert.ToUInt64(((ExOneCheckOnlyTreeNode)tnBuffer).Checked);
                }
                else
                {
                    iIndex = (int)Convert.ToUInt32(((ExOneCheckOnlyTreeNode)tnBuffer).Checked);
                }

                if (bMixedState || (iIndex > 0))
                {
                    tnBuffer.Parent.Checked = true;
                }
                else
                {
                    tnBuffer.Parent.Checked = false;
                }

                if (bMixedState)
                {
                    if (this.CheckBoxesTriState)
                    {
                        tnBuffer.Parent.StateImageIndex = 2;
                    }
                    else
                    {
                        tnBuffer.Parent.StateImageIndex = 1;
                    }
                }
                else
                {
                    tnBuffer.Parent.StateImageIndex = iIndex;
                }

                tnBuffer = (ExOneCheckOnlyTreeNode)tnBuffer.Parent;
            }

            _bPreventCheckEvent = false;

        }

        #endregion

        #region コンストラクタ
        public ExTreeView()
        {
            InitializeComponent();

            //SetCheckState();

            this.CheckBoxes = false;

        }
        public ExTreeView(bool isCheckBox)
        {
            InitializeComponent();

            //SetCheckState();

            //this.CheckBoxes = isCheckBox;
            this.CheckBoxes = false;

        }
        #endregion

        #region 継承メソッド コンポーネント関連
        protected override void Dispose(bool disposing)
        {

            if (disposing)
            {
                if (null != components)
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new Container();
        }

        #endregion

    }

    public class ExTreeNode : TreeNode
    {
        #region プログラム変数
        
        private string m_groupName;

        private object m_value;

        private object m_item;

        private int m_position;

        private string strCheckBoxFieldName;

        #endregion

        [Bindable(true)]
        public string CheckBoxFieldName
        {
            get { return strCheckBoxFieldName; }
            set { strCheckBoxFieldName = value; }
        }

        public string GroupName
        {
            get { return m_groupName; }
            set { m_groupName = value; }
        }

        public new bool Checked
        {
            get { return base.Checked; }
            set 
            {
                base.Checked = value;

                if (null == strCheckBoxFieldName)
                {
                    return;
                }
                if (string.IsNullOrEmpty(strCheckBoxFieldName))
                {
                    return;
                }

                DataRowView loDrv = (DataRowView)m_item;
                DataView loDv = loDrv.DataView;
                DataTable loDt = loDv.ToTable();

                if (base.Checked)
                {
                    loDrv[strCheckBoxFieldName] = "1";
                }
                else
                {
                    loDrv[strCheckBoxFieldName] = "0";
                }

                loDt.AcceptChanges();
            }
        }

        public object Item
        {
            get { return this.m_item; }
            set { m_item = value; }
        }

        public object value
        {
            get { return m_value; }
            set { m_value = value; }
        }

        public int Position
        {
            get { return m_position; }
            set { m_position = value; }
        }
    }

    public class Group
    {

        private string groupName;
        private string groupByMember;
        private string groupByDisplayMember;
        private string groupByValueMember;
#pragma warning disable CS0169 // 認識できない #pragma ディレクティブです
        private string groupImageMember;
#pragma warning disable CS0169 // 認識できない #pragma ディレクティブです
        private int groupImageIndex;
        private int groupSelectedImageIndex;

        public string GroupBy
        {
            get { return groupByMember; }
            set { groupByMember = value; }
        }

        public string Name
        {
            get { return groupName; }
            set { groupName = value; }
        }

        public string ValueMember
        {
            get { return groupByValueMember; }
            set { groupByValueMember = value; }
        }

        public string DisplayMember
        {
            get { return groupByDisplayMember; }
            set { groupByDisplayMember = value; }
        }

        public int SelectedImageIndex
        {
            get { return groupSelectedImageIndex; }
            set { groupSelectedImageIndex = value; }
        }

        public int ImageIndex
        {
            get { return groupImageIndex; }
            set { groupImageIndex = value; }
        }


        public Group()
        {
        }

        public Group(
            string name,
            string groupBy,
            string displayMember,
            string valueMember,
            int imageIndex,
            int selectedImageIndex
        )
        {
            this.ImageIndex = imageIndex;
            this.Name = name;
            this.GroupBy = groupBy;
            this.DisplayMember = displayMember;
            this.ValueMember = valueMember;
            this.SelectedImageIndex = selectedImageIndex;
        }
        public Group(
            string name,
            string groupBy,
            string displayMember,
            string valueMember,
            int imageIndex
        )
        {
            this.ImageIndex = imageIndex;
            this.Name = name;
            this.GroupBy = groupBy;
            this.DisplayMember = displayMember;
            this.ValueMember = valueMember;
            this.SelectedImageIndex = imageIndex;
        }
        public Group(
            string name,
            string groupBy
        )
        {
            this.ImageIndex = -1;
            this.Name = name;
            this.GroupBy = groupBy;
            this.DisplayMember = groupBy;
            this.ValueMember = groupBy;
            this.SelectedImageIndex = -1;
        }

    }

    public class OneCheckOnlyGroup
    {
        private string groupName;
        private string groupByMember;
        private string groupByDisplayMember;
        private string groupByValueMember;
        private int groupImageIndex;
        private int groupSelectedImageIndex;

        public string GroupBy
        {
            get { return this.groupByMember; }
            set { this.groupByMember = value; }
        }
        public string Name
        {
            get { return this.groupName; }
            set { this.groupName = value; }
        }
        public string ValueMember
        {
            get { return this.groupByValueMember; }
            set { this.groupByValueMember = value; }
        }
        public string DisplayMember
        {
            get { return this.groupByDisplayMember; }
            set { this.groupByDisplayMember = value; }
        }
        public int SelectedImageIndex
        {
            get { return this.groupSelectedImageIndex; }
            set { this.groupSelectedImageIndex = value; }
        }
        public int ImageIndex
        {
            get { return this.groupImageIndex; }
            set { this.groupImageIndex = value; }
        }

        public OneCheckOnlyGroup()
        { }

        public OneCheckOnlyGroup(
            string name,
            string groupBy,
            string displayMember,
            string valueMember,
            int imageIndex,
            int selectedImageIndex
            )
        {
            this.ImageIndex = imageIndex;
            this.Name = name;
            this.GroupBy = groupBy;
            this.DisplayMember = displayMember;
            this.ValueMember = valueMember;
            this.SelectedImageIndex = selectedImageIndex;
        }

        public OneCheckOnlyGroup(
            string name,
            string groupBy,
            string displayMember,
            string valueMember,
            int imageIndex
            )
        {
            this.ImageIndex = imageIndex;
            this.Name = name;
            this.GroupBy = groupBy;
            this.DisplayMember = displayMember;
            this.ValueMember = valueMember;
            this.SelectedImageIndex = imageIndex;
        }

        public OneCheckOnlyGroup(
            string name,
            string groupBy
            )
        {
            this.ImageIndex = -1;
            this.Name = name;
            this.GroupBy = groupBy;
            this.DisplayMember = groupBy;
            this.ValueMember = groupBy;
            this.SelectedImageIndex = -1;
        }
    }

    public class ExOneCheckOnlyTreeNode : TreeNode
    {
        private string m_groupName;
        private object m_value;
        private object m_item;
        private int m_position;
        private string strCheckBoxFieldName;

        [Bindable(true)]
        public string CheckBoxFieldName
        {
            get { return strCheckBoxFieldName; }
            set { strCheckBoxFieldName = value; }
        }

        public string GroupName
        {
            get { return m_groupName; }
            set { m_groupName = value; }
        }

        public new bool Checked
        {
            get { return base.Checked; }
            set 
            {
                base.Checked = value;
                if (null == strCheckBoxFieldName) return;
                if (string.IsNullOrEmpty(strCheckBoxFieldName)) return;

                DataRowView loDrv = (DataRowView)m_item;
                DataView loDv = loDrv.DataView;
                DataTable loDt = loDv.ToTable();
                if (base.Checked)
                {
                    loDrv[strCheckBoxFieldName] = "1";
                }
                else
                {
                    loDrv[strCheckBoxFieldName] = "0";
                }

                loDt.AcceptChanges();
            }
        }

        public object Item
        {
            get { return m_item; }
            set { m_item = value; }
        }

        public object Value
        {
            get { return m_value; }
            set { m_value = value; }
        }

        public int Position
        {
            get { return m_position; }
            set { m_position = value; }

        }

        public ExOneCheckOnlyTreeNode()
        { }

        public ExOneCheckOnlyTreeNode(
            string groupName,
            string text,
            object item,
            object value,
            int position
        )
        {

            this.GroupName = groupName;
            this.Text = text;
            this.Item = item;
            this.Value = value;
            this.Position = position;
            this.ImageIndex = 0;
            this.StateImageIndex = 0;

        }
        public ExOneCheckOnlyTreeNode(
            string groupName,
            string text,
            object item,
            object value,
            int imageIndex,
            int selectedImgIndex,
            int position
        )
        {

            this.GroupName = groupName;
            this.Text = text;
            this.Item = item;
            this.Value = value;
            this.Position = position;
            this.ImageIndex = imageIndex;
            this.StateImageIndex = selectedImgIndex;

        }

    }
}
