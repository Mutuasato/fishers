﻿using jp.co.fsi.common.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Renci.SshNet;

namespace jp.co.fsi.common
{
    public static class SSHConnection
    {

        public static SshClient ssh = null;

        public static string SSHConnect()
        {

            string messages = string.Empty;

            ConfigLoader config = new ConfigLoader();

            string strSSHHost1 = config.LoadSSHConfig("SSH", "SSHHost");
            string strSSHPort1 = config.LoadSSHConfig("SSH", "SSHPort");
            string strSSHLocalHost1 = config.LoadSSHConfig("SSH", "SSHLocalHost");
            string strSSHLocalPort1 = config.LoadSSHConfig("SSH", "SSHLocalPort");
            string strSSHFwdPort1 = config.LoadSSHConfig("SSH", "SSHFwdPort");
            string strSSHUser1 = config.LoadSSHConfig("SSH", "SSHUser");
            string strSSHPassword1 = config.LoadSSHConfig("SSH", "SSHPassword");

            //// 複合化
            string strSSHHost = SecureUtils.Decrypt(strSSHHost1, config.LoadSSHConfig("SSH", "SSHHostKey"));
            string strSSHPort = SecureUtils.Decrypt(strSSHPort1, config.LoadSSHConfig("SSH", "SSHPortKey"));
            string strSSHLocalHost = SecureUtils.Decrypt(strSSHLocalHost1, config.LoadSSHConfig("SSH", "SSHLocalHostKey"));
            string strSSHLocalPort = SecureUtils.Decrypt(strSSHLocalPort1, config.LoadSSHConfig("SSH", "SSHLocalPortKey"));
            string strSSHFwdPort = SecureUtils.Decrypt(strSSHFwdPort1, config.LoadSSHConfig("SSH", "SSHFwdPortKey"));
            string strSSHUser = SecureUtils.Decrypt(strSSHUser1, config.LoadSSHConfig("SSH", "SSHUserKey"));
            string strSSHPassword = SecureUtils.Decrypt(strSSHPassword1, config.LoadSSHConfig("SSH", "SSHPasswordKey"));

            //string strSSHHost1 = SecureUtils.ReadShareMem("SSHHOST");
            //string strSSHHostKey1 = SecureUtils.ReadShareMem("SSHHOSTKEY");

            //string strSSHPort1 = SecureUtils.ReadShareMem("SSHPORT");
            //string strSSHPortKey1 = SecureUtils.ReadShareMem("SSHPORTKEY");

            //string strSSHLocalHost1 = SecureUtils.ReadShareMem("SSHLOCALHOST");
            //string strSSHLocalHostKey1 = SecureUtils.ReadShareMem("SSHLOCALHOSTKEY");

            //string strSSHLocalPort1 = SecureUtils.ReadShareMem("SSHLOCALPORT");
            //string strSSHLocalPortKey1 = SecureUtils.ReadShareMem("SSHLOCALPORTKEY");

            //string strSSHFwdPort1 = SecureUtils.ReadShareMem("SSHFWDPORT");
            //string strSSHFwdPortKey1 = SecureUtils.ReadShareMem("SSHFWDPORTKEY");

            //string strSSHUser1 = SecureUtils.ReadShareMem("SSHUSERID");
            //string strSSHUserKey1 = SecureUtils.ReadShareMem("SSHUSERIDKEY");

            //string strSSHPassword1 = SecureUtils.ReadShareMem("SSHPASSWORD");
            //string strSSHPasswordKey1 = SecureUtils.ReadShareMem("SSHPASSWORDKEY");

            ////            SecureUtils.ReadShareMem("SSHHOST"), SecureUtils.ReadShareMem("SSHHOSTKEY")
            //string strSSHHost = SecureUtils.Decrypt(strSSHHost1, strSSHHostKey1);
            //string strSSHPort = SecureUtils.Decrypt(strSSHPort1, strSSHPortKey1);
            //string strSSHLocalHost = SecureUtils.Decrypt(strSSHLocalHost1, strSSHLocalHostKey1);
            //string strSSHLocalPort = SecureUtils.Decrypt(strSSHLocalPort1, strSSHLocalPortKey1);
            //string strSSHFwdPort = SecureUtils.Decrypt(strSSHFwdPort1, strSSHFwdPortKey1);
            //string strSSHUser = SecureUtils.Decrypt(strSSHUser1, strSSHUserKey1);
            //string strSSHPassword = SecureUtils.Decrypt(strSSHPassword1, strSSHPasswordKey1);

            try
            {

                ConnectionInfo info = new ConnectionInfo(
                    strSSHHost,
                    int.Parse(strSSHPort),
                    strSSHUser,
                    new AuthenticationMethod[]
                    {
                        new PasswordAuthenticationMethod(
                        strSSHUser,
                        strSSHPassword)
                    }
                    );

                ssh = new SshClient(info);

                ssh.Connect();

                if (!ssh.IsConnected)
                {
                    messages = "SSH接続されていません。";
                }

                // ポート転送設定追加
                ForwardedPortLocal forwardSql =
                    new ForwardedPortLocal(
                        strSSHLocalHost, 
                        uint.Parse(strSSHLocalPort), 
                        strSSHHost, 
                        uint.Parse(strSSHFwdPort)); // SQLServer用

                ssh.AddForwardedPort(forwardSql);

                forwardSql.Start();


            }
            catch (Exception ex)
            {
                messages = "SSH接続時にエラーが発生しました。システム管理者へ問い合わせてください。"+Environment.NewLine + ex.Message;
                ssh.Dispose();

            }

            return messages;

        }

        public static string SSHDisConnect()
        {

            string messages = string.Empty;

            try
            {

                ssh.Disconnect();

                ssh.Dispose();

            }
            catch (Exception ex)
            {
                messages = "SSH切断時にエラーが発生しました。" + Environment.NewLine + ex.Message;
            }

            return messages;

        }


    }
}
