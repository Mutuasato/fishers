﻿using System;
using System.Text.RegularExpressions;

namespace jp.co.fsi.common.util
{
    /// <summary>
    /// 値チェッカークラス
    /// </summary>
    public static class ValChk
    {
        #region publicメソッド
        /// <summary>
        /// 引数に渡した値が空かどうかを判定します。
        /// </summary>
        /// <param name="val">判定する値</param>
        /// <returns>true:空、false:空でない</returns>
        public static bool IsEmpty(object val)
        {
            if (Util.ToString(val).Trim().Length == 0) return true;

            return false;
        }

        /// <summary>
        /// 引数に渡した値が半角文字かどうかを判定します。
        /// </summary>
        /// <param name="val">判定する値</param>
        /// <returns>true:半角、false:半角でない</returns>
        public static bool IsHalfChar(object val)
        {
            // 空はOKとする
            if (ValChk.IsEmpty(val)) return true;

            // 文字数とバイト数が一致していればtrue
            if (Util.GetByteLength(Util.ToString(val)) == Util.ToString(val).Length) return true;

            return false;
        }

        /// <summary>
        /// 引数に渡した値が数字のみかどうかを判定します。
        /// </summary>
        /// <param name="val">判定する値</param>
        /// <returns>true:数字、false:数字でない</returns>
        public static bool IsNumber(object val)
        {
            // 空はOKとする
            if (ValChk.IsEmpty(val)) return true;

            // 正規表現で数字のみセット
            return Regex.IsMatch(val.ToString(), "^[0-9]+$");
        }

        /// <summary>
        /// 引数に渡した値が指定した範囲内の数値かどうかを判定します。
        /// </summary>
        /// <param name="val">判定する値</param>
        /// <param name="intLen">整数部の桁数</param>
        /// <param name="decLen">小数部の桁数</param>
        /// <returns>true:指定した範囲内の数値、false:trueに該当しない</returns>
        public static bool IsDecNumWithinLength(object val, int intLen, int decLen)
        {
            return IsDecNumWithinLength(val, intLen, decLen, false);
        }

        /// <summary>
        /// 引数に渡した値が指定した範囲内の数値かどうかを判定します。
        /// </summary>
        /// <param name="val">判定する値</param>
        /// <param name="intLen">整数部の桁数</param>
        /// <param name="decLen">小数部の桁数</param>
        /// <param name="isDecOverError">小数部が超過時にエラーにするか(true:エラーとする)</param>
        /// <returns>true:指定した範囲内の数値、false:trueに該当しない</returns>
        public static bool IsDecNumWithinLength(object val, int intLen, int decLen, bool isDecOverError)
        {
            // 空はOKとする
            if (ValChk.IsEmpty(val)) return true;

            decimal decVal = 0;
            if (!Decimal.TryParse(val.ToString(), out decVal))
            {
                return false;
            }

            // 引数に渡された値を小数点の前後で分ける
            string tmpNum = decVal.ToString();
            string[] arrNum = tmpNum.Split('.');
            string tmpInt = "";
            string tmpDec = "";
            if (arrNum.Length == 1)
            {
                tmpInt = arrNum[0];
            }
            else if (arrNum.Length == 2)
            {
                tmpInt = arrNum[0];
                tmpDec = arrNum[1];
            }

            if (tmpInt.Replace("-", "").Length > intLen)
            {
                // 整数部が超過しているためエラー
                return false;
            }

            if (isDecOverError && (tmpDec.Length > decLen))
            {
                // 小数部が超過しているためエラー
                return false;
            }

            return true;
        }

        /// <summary>
        /// 値が指定した桁数以下かどうかを判定します。
        /// ※バイト数で判断します。
        /// </summary>
        /// <param name="val">チェックする値。</param>
        /// <param name="length">基準となる桁数</param>
        /// <returns>true:桁数以内、false:桁数を超えている</returns>
        public static bool IsWithinLength(object val, int length)
        {
            if (Util.GetByteLength(Util.ToString(val)) > length)
            {
                // 超えている
                return false;
            }

            // 超えていない
            return true;
        }
        #endregion
    }
}
