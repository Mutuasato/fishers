﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.util;
using jp.co.fsi.common.userinfo;
using System.Security.Cryptography;
using jp.co.fsi.common.riyoshalog;

namespace jp.co.fsi.common.mynumber
{
    /// <summary>
    /// ユーティリティクラス
    /// </summary>
    public static class Mynumber
    {
        #region 定数
        //パスワードに使用する文字
        private static readonly string passwordChars = "0123456789abcdefghijklmnopqrstuvwxyz";
        #endregion

        #region publicメソッド

        /// <summary>
        /// 文字列をAESで暗号化
        /// </summary>
        /// <param name="text">暗号化する文字</param>
        /// <param name="shain_cd">初期化ベクトル</param>
        /// <param name="shain_cd">パスワード</param>
        /// <returns>マイナンバー
        /// 参考URL　http://programmers.high-way.info/cs/aes.html
        public static string Encrypt(string text, string AesIV, string AesKey)
        {
            // AES暗号化サービスプロバイダ
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;// 16バイト
            aes.KeySize = 128;// 16バイト
            aes.IV = Encoding.UTF8.GetBytes(AesIV);
            aes.Key = Encoding.UTF8.GetBytes(AesKey);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // 文字列をバイト型配列に変換
            byte[] src = Encoding.Unicode.GetBytes(text);

            // 暗号化する
            using (ICryptoTransform encrypt = aes.CreateEncryptor())
            {
                byte[] dest = encrypt.TransformFinalBlock(src, 0, src.Length);

                // バイト型配列からBase64形式の文字列に変換
                return Convert.ToBase64String(dest);
            }
        }

        /// <summary>
        /// 文字列をAESで復号化
        /// </summary>
        /// <param name="text">暗号化する文字</param>
        /// <param name="shain_cd">初期化ベクトル</param>
        /// <param name="shain_cd">パスワード</param>
        /// 参考URL　http://programmers.high-way.info/cs/aes.html
        public static string Decrypt(string text, string AesIV, string AesKey)
        {
            // AES暗号化サービスプロバイダ
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 128;
            aes.IV = Encoding.UTF8.GetBytes(AesIV);
            aes.Key = Encoding.UTF8.GetBytes(AesKey);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // Base64形式の文字列からバイト型配列に変換
            byte[] src = System.Convert.FromBase64String(text);

            // 複号化する
            using (ICryptoTransform decrypt = aes.CreateDecryptor())
            {
                byte[] dest = decrypt.TransformFinalBlock(src, 0, src.Length);
                return Encoding.Unicode.GetString(dest);
            }
        }

        /// <summary>
        /// ランダムな文字列を生成する
        /// </summary>
        /// <param name="length">生成する文字列の長さ</param>
        /// <returns>生成された文字列</returns>
        public static string GeneratePassword(int length)
        {
            StringBuilder sb = new StringBuilder(length);
            Random r = new Random();

            for (int i = 0; i < length; i++)
            {
                //文字の位置をランダムに選択
                int pos = r.Next(passwordChars.Length);
                //選択された位置の文字を取得
                char c = passwordChars[pos];
                //パスワードに追加
                sb.Append(c);
            }
            return sb.ToString();
        }


        /// <summary>
        /// マイナンバーデータ を取得※法人用13桁
        /// </summary>
        /// <param name="kaisha_cd">会社コード</param>
        /// <returns>マイナンバー
        public static string GetmyNumber(int kaisha_cd, DbAccess dba)
        {
            ConfigLoader cLoader = new ConfigLoader();
            // 会社コード取得
            int kaishaCd = Util.ToInt(cLoader.LoadCommonConfig("UserInfo", "kaishacd"));
            string[] pattern; //パターン格納用変数
            string mynumber = "";// マイナンバー変数
            string data = "";// マイナンバー文字数
            #region メインデータ取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, kaishaCd);
            dpc.SetParam("@SEALA_NO", SqlDbType.Decimal, 5, kaisha_cd);
            dpc.SetParam("@KUBUN", SqlDbType.Decimal, 5, 2);//法人は固定で2

            sql.Append(" SELECT");
            sql.Append(" *");
            sql.Append(" FROM");
            sql.Append(" TB_CM_SEALA_NO");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SEALA_NO = @SEALA_NO AND");
            sql.Append(" KUBUN = @KUBUN ");
            DataTable dtmyNumber = dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            if (dtmyNumber.Rows.Count == 0)
            {
                return mynumber;
            }

            DbParamCollection dpc1 = new DbParamCollection();
            StringBuilder sql1 = new StringBuilder();
            dpc1.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, kaishaCd);
            dpc1.SetParam("@SEALA_NO", SqlDbType.Decimal, 5, kaisha_cd);
            dpc1.SetParam("@KUBUN", SqlDbType.Decimal, 5, 2);//法人は固定で2
            dpc1.SetParam("@KUBUN2", SqlDbType.Decimal, 5, dtmyNumber.Rows[0]["KUBUN2"]);
            sql1.Append(" SELECT");
            sql1.Append(" *");
            sql1.Append(" FROM");
            sql1.Append(" TB_CM_SEALA_NO1");
            sql1.Append(" WHERE");
            sql1.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql1.Append(" SEALA_NO = @SEALA_NO AND");
            sql1.Append(" KUBUN = @KUBUN AND");
            sql1.Append(" KUBUN2 = @KUBUN2");
            DataTable dtmyNumber1 = dba.GetDataTableFromSqlWithParams(sql1.ToString(), dpc1);

            if (dtmyNumber1.Rows.Count == 0)
            {
                return mynumber;
            }

            string AesIV = Util.ToString(dtmyNumber1.Rows[0]["PASSCODE"]);
            string AesKey = Util.ToString(dtmyNumber.Rows[0]["KEYCODE"]);

            #endregion

            foreach (DataRow dt in dtmyNumber.Rows)
            {
                switch (Util.ToInt(dt["KUBUN2"]))
                {
                    #region パターンを取得

                    case 0:
                        pattern = new string[13] { "7", "17", "11", "5", "2", "4", "13", "1", "18", "9", "16", "14", "20" };
                        break;
                    case 1:
                        pattern = new string[13] { "15", "6", "5", "10", "14", "3", "17", "20", "12", "4", "13", "2", "19" };
                        break;
                    case 2:
                        pattern = new string[13] { "2", "19", "16", "13", "10", "12", "7", "9", "15", "17", "18", "3", "1" };
                        break;
                    case 3:
                        pattern = new string[13] { "14", "10", "11", "8", "15", "12", "9", "5", "20", "18", "2", "13", "6" };
                        break;
                    case 4:
                        pattern = new string[13] { "19", "9", "12", "2", "6", "8", "17", "10", "3", "5", "14", "4", "20" };
                        break;
                    case 5:
                        pattern = new string[13] { "14", "20", "8", "19", "1", "10", "17", "18", "15", "12", "3", "2", "7" };
                        break;
                    case 6:
                        pattern = new string[13] { "20", "5", "8", "3", "10", "6", "19", "15", "2", "4", "11", "17", "9" };
                        break;
                    case 7:
                        pattern = new string[13] { "18", "9", "16", "10", "14", "5", "6", "12", "11", "13", "7", "15", "8" };
                        break;
                    case 8:
                        pattern = new string[13] { "3", "20", "5", "13", "8", "2", "1", "17", "9", "12", "4", "6", "18" };
                        break;
                    case 9:
                        pattern = new string[13] { "3", "2", "18", "16", "10", "6", "5", "1", "11", "7", "15", "14", "9" };
                        break;
                    case 10:
                        pattern = new string[13] { "20", "7", "8", "17", "1", "10", "6", "2", "18", "4", "16", "15", "9" };
                        break;
                    case 11:
                        pattern = new string[13] { "1", "13", "18", "7", "20", "10", "5", "9", "15", "16", "11", "2", "8" };
                        break;
                    case 12:
                        pattern = new string[13] { "15", "1", "18", "9", "2", "6", "3", "20", "17", "16", "10", "11", "19" };
                        break;
                    default:
                        pattern = new string[13] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "1", "2", "3" };
                        break;
                    #endregion
                }
                // マイナンバーを取得
                foreach (string dr in pattern)
                {
                    data = Util.ToString(dtmyNumber.Rows[0]["BANGO" + dr]);
                    AesKey = Util.ToString(dtmyNumber.Rows[0]["KEYCODE"]);
                    mynumber += Decrypt(data, AesIV, AesKey);
                    AesIV = data.Substring(0, 16);
                }
            }
            return mynumber;
        }

        /// <summary>
        /// マイナンバーデータ登録※法人用13桁
        /// </summary>
        /// <param name="set_no">登録番号(法人)</param>
        /// <param name="kaisha_cd">会社の番号（取引先コード）</param>
        /// <returns>マイナンバー
        public static void InsertNumber(string set_no, int kaisha_cd, DbAccess dba)
        {
            ConfigLoader cLoader = new ConfigLoader();
            // 会社コード取得
            int kaishaCd = Util.ToInt(cLoader.LoadCommonConfig("UserInfo", "kaishacd"));
            int[] mynumber = new int[13]; //マイナンバー配列
            int i = 0; //カウント変数
            int j = 0; //カウント変数
            int random = 0;//ランダム数字変数
            string dummy = "";// ダミー用変数
            string bango1 = "";// 番号１
            string bango2 = "";// 番号２
            string bango3 = "";// 番号３
            string bango4 = "";// 番号４
            string bango5 = "";// 番号５
            string bango6 = "";// 番号６
            string bango7 = "";// 番号７
            string bango8 = "";// 番号８
            string bango9 = "";// 番号９
            string bango10 = "";// 番号１０
            string bango11 = "";// 番号１１
            string bango12 = "";// 番号１２
            string bango13 = "";// 番号１３
            string bango14 = "";// 番号１４
            string bango15 = "";// 番号１５
            string bango16 = "";// 番号１６
            string bango17 = "";// 番号１７
            string bango18 = "";// 番号１８
            string bango19 = "";// 番号１９
            string bango20 = "";// 番号２０

            string code = GeneratePassword(16);// 暗号化の際の初期化ベクトルを設定
            string AesIV = code;
            string AesKey = GeneratePassword(16);//暗号化するパスワードを設定
            string[] pattern; //パターン格納用変数()

            // ランダムに数字を取得
            Random rnd = new Random();
            int kubun2 = rnd.Next(13);

            switch (kubun2)
            {
                #region パターンを取得

                case 0:
                    pattern = new string[13] { "7", "17", "11", "5", "2", "4", "13", "1", "18", "9", "16", "14", "20" };
                    break;
                case 1:
                    pattern = new string[13] { "15", "6", "5", "10", "14", "3", "17", "20", "12", "4", "13", "2", "19" };
                    break;
                case 2:
                    pattern = new string[13] { "2", "19", "16", "13", "10", "12", "7", "9", "15", "17", "18", "3", "1" };
                    break;
                case 3:
                    pattern = new string[13] { "14", "10", "11", "8", "15", "12", "9", "5", "20", "18", "2", "13", "6" };
                    break;
                case 4:
                    pattern = new string[13] { "19", "9", "12", "2", "6", "8", "17", "10", "3", "5", "14", "4", "20" };
                    break;
                case 5:
                    pattern = new string[13] { "14", "20", "8", "19", "1", "10", "17", "18", "15", "12", "3", "2", "7" };
                    break;
                case 6:
                    pattern = new string[13] { "20", "5", "8", "3", "10", "6", "19", "15", "2", "4", "11", "17", "9" };
                    break;
                case 7:
                    pattern = new string[13] { "18", "9", "16", "10", "14", "5", "6", "12", "11", "13", "7", "15", "8" };
                    break;
                case 8:
                    pattern = new string[13] { "3", "20", "5", "13", "8", "2", "1", "17", "9", "12", "4", "6", "18" };
                    break;
                case 9:
                    pattern = new string[13] { "3", "2", "18", "16", "10", "6", "5", "1", "11", "7", "15", "14", "9" };
                    break;
                case 10:
                    pattern = new string[13] { "20", "7", "8", "17", "1", "10", "6", "2", "18", "4", "16", "15", "9" };
                    break;
                case 11:
                    pattern = new string[13] { "1", "13", "18", "7", "20", "10", "5", "9", "15", "16", "11", "2", "8" };
                    break;
                case 12:
                    pattern = new string[13] { "15", "1", "18", "9", "2", "6", "3", "20", "17", "16", "10", "11", "19" };
                    break;
                default:
                    pattern = new string[13] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "1", "2", "3" };
                    break;
                #endregion
            }

            // マインバーを配列にセット
            foreach (char c in set_no)
            {
                mynumber[i] += Util.ToInt(c);
                i++;
            }

            foreach (string dr in pattern)
            {
                switch (dr)
                {
                    case "1":
                        bango1 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango1.Substring(0, 16);//暗号化された文字列を次の初期化ベクトルにセット16バイトになるよう切り取り
                        j++;
                        break;
                    case "2":
                        bango2 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango2.Substring(0, 16);
                        j++;
                        break;
                    case "3":
                        bango3 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango3.Substring(0, 16);
                        j++;
                        break;
                    case "4":
                        bango4 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango4.Substring(0, 16);
                        j++;
                        break;
                    case "5":
                        bango5 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango5.Substring(0, 16);
                        j++;
                        break;
                    case "6":
                        bango6 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango6.Substring(0, 16);
                        j++;
                        break;
                    case "7":
                        bango7 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango7.Substring(0, 16);
                        j++;
                        break;
                    case "8":
                        bango8 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango8.Substring(0, 16);
                        j++;
                        break;
                    case "9":
                        bango9 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango9.Substring(0, 16);
                        j++;
                        break;
                    case "10":
                        bango10 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango10.Substring(0, 16);
                        j++;
                        break;
                    case "11":
                        bango11 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango11.Substring(0, 16);
                        j++;
                        break;
                    case "12":
                        bango12 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango12.Substring(0, 16);
                        j++;
                        break;
                    case "13":
                        bango13 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango13.Substring(0, 16);
                        j++;
                        break;
                    case "14":
                        bango14 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango14.Substring(0, 16);
                        j++;
                        break;
                    case "15":
                        bango15 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango15.Substring(0, 16);
                        j++;
                        break;
                    case "16":
                        bango16 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango16.Substring(0, 16);
                        j++;
                        break;
                    case "17":
                        bango17 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango17.Substring(0, 16);
                        j++;
                        break;
                    case "18":
                        bango18 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango18.Substring(0, 16);
                        j++;
                        break;
                    case "19":
                        bango19 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango19.Substring(0, 16);
                        j++;
                        break;
                    case "20":
                        bango20 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango20.Substring(0, 16);
                        j++;
                        break;
                    default:
                        break;
                }
            }

            #region TB_CM_SEALA_NOテーブルへのインサート

            #region INSERTSQL
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append(" INSERT INTO TB_CM_SEALA_NO (");
            sql.Append(" KAISHA_CD,");
            sql.Append(" KUBUN,");
            sql.Append(" KUBUN2,");
            sql.Append(" SEALA_NO,");
            sql.Append(" FUYO_NO,");
            sql.Append(" BANGO1,");
            sql.Append(" BANGO2,");
            sql.Append(" BANGO3,");
            sql.Append(" BANGO4,");
            sql.Append(" BANGO5,");
            sql.Append(" BANGO6,");
            sql.Append(" BANGO7,");
            sql.Append(" BANGO8,");
            sql.Append(" BANGO9,");
            sql.Append(" BANGO10,");
            sql.Append(" BANGO11,");
            sql.Append(" BANGO12,");
            sql.Append(" BANGO13,");
            sql.Append(" BANGO14,");
            sql.Append(" BANGO15,");
            sql.Append(" BANGO16,");
            sql.Append(" BANGO17,");
            sql.Append(" BANGO18,");
            sql.Append(" BANGO19,");
            sql.Append(" BANGO20,");
            sql.Append(" KEYCODE,");
            sql.Append(" REGIST_DATE");
            sql.Append(" )");
            sql.Append(" VALUES (");
            sql.Append(" @KAISHA_CD,");
            sql.Append(" @KUBUN,");
            sql.Append(" @KUBUN2,");
            sql.Append(" @SEALA_NO,");
            sql.Append(" @FUYO_NO,");
            sql.Append(" @BANGO1,");
            sql.Append(" @BANGO2,");
            sql.Append(" @BANGO3,");
            sql.Append(" @BANGO4,");
            sql.Append(" @BANGO5,");
            sql.Append(" @BANGO6,");
            sql.Append(" @BANGO7,");
            sql.Append(" @BANGO8,");
            sql.Append(" @BANGO9,");
            sql.Append(" @BANGO10,");
            sql.Append(" @BANGO11,");
            sql.Append(" @BANGO12,");
            sql.Append(" @BANGO13,");
            sql.Append(" @BANGO14,");
            sql.Append(" @BANGO15,");
            sql.Append(" @BANGO16,");
            sql.Append(" @BANGO17,");
            sql.Append(" @BANGO18,");
            sql.Append(" @BANGO19,");
            sql.Append(" @BANGO20,");
            sql.Append(" @KEYCODE,");
            sql.Append(" @REGIST_DATE");
            sql.Append(" )");
            #endregion

            #region 登録パラメータの設定
            dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, kaishaCd);
            dpc.SetParam("@KUBUN", SqlDbType.Int, 4, 2);// 法人は区分2
            dpc.SetParam("@KUBUN2", SqlDbType.Int, 4, kubun2);
            dpc.SetParam("@SEALA_NO", SqlDbType.Int, 15, kaisha_cd);
            dpc.SetParam("@FUYO_NO", SqlDbType.Int, 15, 0);
            if (bango1.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO1", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO1", SqlDbType.VarChar, 24, bango1);
            }
            if (bango2.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO2", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO2", SqlDbType.VarChar, 24, bango2);
            }
            if (bango3.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO3", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO3", SqlDbType.VarChar, 24, bango3);
            }
            if (bango4.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO4", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO4", SqlDbType.VarChar, 24, bango4);
            }
            if (bango5.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO5", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO5", SqlDbType.VarChar, 24, bango5);
            }
            if (bango6.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO6", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO6", SqlDbType.VarChar, 24, bango6);
            }
            if (bango7.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO7", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO7", SqlDbType.VarChar, 24, bango7);
            }
            if (bango8.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO8", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO8", SqlDbType.VarChar, 24, bango8);
            }
            if (bango9.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO9", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO9", SqlDbType.VarChar, 24, bango9);
            }
            if (bango10.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO10", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO10", SqlDbType.VarChar, 24, bango10);
            } if (bango11.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO11", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO11", SqlDbType.VarChar, 24, bango11);
            }
            if (bango12.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO12", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO12", SqlDbType.VarChar, 24, bango12);
            }
            if (bango13.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO13", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO13", SqlDbType.VarChar, 24, bango13);
            }
            if (bango14.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO14", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO14", SqlDbType.VarChar, 24, bango14);
            }
            if (bango15.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO15", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO15", SqlDbType.VarChar, 24, bango15);
            }
            if (bango16.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO16", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO16", SqlDbType.VarChar, 24, bango16);
            }
            if (bango17.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO17", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO17", SqlDbType.VarChar, 24, bango17);
            }
            if (bango18.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO18", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO18", SqlDbType.VarChar, 24, bango18);
            }
            if (bango19.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO19", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO19", SqlDbType.VarChar, 24, bango19);
            }
            if (bango20.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO20", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO20", SqlDbType.VarChar, 24, bango20);
            }
            dpc.SetParam("@KEYCODE", SqlDbType.VarChar, 24, AesKey);
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now);
            #endregion

            dba.ModifyBySql(Util.ToString(sql), dpc);
            #endregion


            #region TB_CM_SEALA_NO1テーブルへのインサート

            #region INSERTSQL
            DbParamCollection dpc1 = new DbParamCollection();
            StringBuilder sql1 = new StringBuilder();
            sql1.Append(" INSERT INTO TB_CM_SEALA_NO1 (");
            sql1.Append(" KAISHA_CD,");
            sql1.Append(" KUBUN,");
            sql1.Append(" KUBUN2,");
            sql1.Append(" SEALA_NO,");
            sql1.Append(" FUYO_NO,");
            sql1.Append(" PASSCODE,");
            sql1.Append(" REGIST_DATE");
            sql1.Append(" )");
            sql1.Append(" VALUES (");
            sql1.Append(" @KAISHA_CD,");
            sql1.Append(" @KUBUN,");
            sql1.Append(" @KUBUN2,");
            sql1.Append(" @SEALA_NO,");
            sql1.Append(" @FUYO_NO,");
            sql1.Append(" @PASSCODE,");
            sql1.Append(" @REGIST_DATE");
            sql1.Append(" )");
            #endregion

            #region 登録パラメータの設定
            dpc1.SetParam("@KAISHA_CD", SqlDbType.Int, 4, kaishaCd);
            dpc1.SetParam("@KUBUN", SqlDbType.Int, 4, 2);// 法人は区分2
            dpc1.SetParam("@KUBUN2", SqlDbType.Int, 4, kubun2);
            dpc1.SetParam("@SEALA_NO", SqlDbType.Int, 15, kaisha_cd);
            dpc1.SetParam("@PASSCODE", SqlDbType.VarChar, 16, code);
            dpc1.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now);
            dpc1.SetParam("@FUYO_NO", SqlDbType.Int, 15, 0);
            #endregion
            dba.ModifyBySql(Util.ToString(sql1), dpc1);
            #endregion

        }


        /// <summary>
        /// マイナンバーデータ を取得
        /// </summary>
        /// <param name="cd">コード</param>
        /// <param name="kubun">区分</param>
        /// <returns>マイナンバー</returns>
        public static string GetmyNumber(int cd, int kubun, int huyo_no, DbAccess dba)
        {
            ConfigLoader cLoader = new ConfigLoader();
            // 会社コード取得
            int kaishaCd = Util.ToInt(cLoader.LoadCommonConfig("UserInfo", "kaishacd"));
            string[] pattern; //パターン格納用変数
            string mynumber = "";// マイナンバー変数
            string data = "";// マイナンバー文字数
            #region メインデータ取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 5, kaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 5, cd);
            dpc.SetParam("@KUBUN", SqlDbType.Decimal, 5, kubun);
            dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 5, huyo_no);

            sql.Append(" SELECT");
            sql.Append(" *");
            sql.Append(" FROM");
            sql.Append(" TB_CM_SEALA_NO");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SEALA_NO = @SHAIN_CD AND");
            sql.Append(" KUBUN = @KUBUN AND");
            sql.Append(" FUYO_NO = @FUYO_NO");
            DataTable dtmyNumber = dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            if (dtmyNumber.Rows.Count == 0)
            {
                return mynumber;
            }

            DbParamCollection dpc1 = new DbParamCollection();
            StringBuilder sql1 = new StringBuilder();
            dpc1.SetParam("@KAISHA_CD", SqlDbType.Decimal, 5, kaishaCd);
            dpc1.SetParam("@SHAIN_CD", SqlDbType.Decimal, 5, cd);
            dpc1.SetParam("@KUBUN", SqlDbType.Decimal, 5, kubun);
            dpc1.SetParam("@KUBUN2", SqlDbType.Decimal, 5, dtmyNumber.Rows[0]["KUBUN2"]);
            dpc1.SetParam("@FUYO_NO", SqlDbType.Decimal, 5, huyo_no);
            sql1.Append(" SELECT");
            sql1.Append(" *");
            sql1.Append(" FROM");
            sql1.Append(" TB_CM_SEALA_NO1");
            sql1.Append(" WHERE");
            sql1.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql1.Append(" SEALA_NO = @SHAIN_CD AND");
            sql1.Append(" KUBUN = @KUBUN AND");
            sql1.Append(" KUBUN2 = @KUBUN2 AND");
            sql1.Append(" FUYO_NO = @FUYO_NO");
            DataTable dtmyNumber1 = dba.GetDataTableFromSqlWithParams(sql1.ToString(), dpc1);

            if (dtmyNumber1.Rows.Count == 0)
            {
                return mynumber;
            }

            string AesIV = Util.ToString(dtmyNumber1.Rows[0]["PASSCODE"]);
            string AesKey = Util.ToString(dtmyNumber.Rows[0]["KEYCODE"]);

            #endregion

            try
            {
                foreach (DataRow dt in dtmyNumber.Rows)
                {
                    switch (Util.ToInt(dt["KUBUN2"]))
                    {
                        #region パターンを取得

                        case 0:
                            pattern = new string[12] { "6", "17", "4", "3", "1", "10", "12", "8", "9", "18", "5", "20" };
                            break;
                        case 1:
                            pattern = new string[12] { "15", "4", "6", "13", "16", "12", "18", "7", "17", "1", "3", "14" };
                            break;
                        case 2:
                            pattern = new string[12] { "17", "11", "3", "4", "19", "20", "12", "8", "9", "2", "14", "15" };
                            break;
                        case 3:
                            pattern = new string[12] { "5", "3", "18", "2", "17", "10", "16", "11", "4", "14", "9", "8" };
                            break;
                        case 4:
                            pattern = new string[12] { "4", "11", "2", "18", "14", "8", "20", "13", "17", "10", "1", "16" };
                            break;
                        case 5:
                            pattern = new string[12] { "12", "19", "4", "11", "15", "3", "17", "20", "2", "8", "7", "18" };
                            break;
                        case 6:
                            pattern = new string[12] { "11", "14", "19", "12", "15", "17", "7", "16", "6", "1", "4", "2" };
                            break;
                        case 7:
                            pattern = new string[12] { "12", "11", "7", "6", "4", "8", "9", "17", "20", "18", "16", "2" };
                            break;
                        case 8:
                            pattern = new string[12] { "1", "4", "18", "20", "13", "19", "11", "3", "8", "9", "14", "6" };
                            break;
                        case 9:
                            pattern = new string[12] { "15", "20", "18", "1", "9", "3", "7", "17", "19", "14", "6", "12" };
                            break;
                        case 10:
                            pattern = new string[12] { "13", "16", "12", "11", "19", "17", "1", "6", "4", "9", "3", "2" };
                            break;
                        case 11:
                            pattern = new string[12] { "18", "7", "8", "12", "2", "1", "10", "6", "14", "3", "15", "20" };
                            break;
                        case 12:
                            pattern = new string[12] { "20", "7", "14", "19", "13", "6", "15", "4", "16", "9", "12", "1" };
                            break;
                        default:
                            pattern = new string[12] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
                            break;
                            #endregion
                    }
                    // マイナンバーを取得
                    foreach (string dr in pattern)
                    {
                        data = Util.ToString(dtmyNumber.Rows[0]["BANGO" + dr]);
                        AesKey = Util.ToString(dtmyNumber.Rows[0]["KEYCODE"]);
                        mynumber += Decrypt(data, AesIV, AesKey);
                        AesIV = data.Substring(0, 16);
                    }
                }

            }
            catch (Exception)
            {

            }


            try
            {
                // トランザクション開始
                dba.BeginTransaction();
                string txtdata = Util.ToString(sql) + Util.ToString(sql1);
                // 利用者ログテーブルに実行時間等を登録
                Riyoshalog.Insertlog(kaishaCd, kubun, txtdata, dba);
                // トランザクションコミット
                dba.Commit();
            }
            catch(Exception)
            {
                dba.Rollback();
                Msg.Info("ログの吐き出しに失敗しました");
            }

            return mynumber;
        }

        /// <summary>
        /// マイナンバーデータ登録
        /// </summary>
        /// <param name="set_no">登録番号</param>
        /// <param name="kubun">区分</param>
        /// <param name="cd">番号</param>
        /// <param name="huyo_no">扶養コード</param>
        /// <returns>マイナンバー</returns>
        public static void InsertNumber(string set_no, int kubun, int cd, int huyo_no, DbAccess dba)
        {
            ConfigLoader cLoader = new ConfigLoader();
            // 会社コード取得
            int kaishaCd = Util.ToInt(cLoader.LoadCommonConfig("UserInfo", "kaishacd"));
            int[] mynumber = new int[12]; //マイナンバー配列
            int i = 0; //カウント変数
            int j = 0; //カウント変数
            int random = 0;//ランダム数字変数
            string dummy = "";// ダミー用変数
            string bango1 = "";// 番号１
            string bango2 = "";// 番号２
            string bango3 = "";// 番号３
            string bango4 = "";// 番号４
            string bango5 = "";// 番号５
            string bango6 = "";// 番号６
            string bango7 = "";// 番号７
            string bango8 = "";// 番号８
            string bango9 = "";// 番号９
            string bango10 = "";// 番号１０
            string bango11 = "";// 番号１１
            string bango12 = "";// 番号１２
            string bango13 = "";// 番号１３
            string bango14 = "";// 番号１４
            string bango15 = "";// 番号１５
            string bango16 = "";// 番号１６
            string bango17 = "";// 番号１７
            string bango18 = "";// 番号１８
            string bango19 = "";// 番号１９
            string bango20 = "";// 番号２０

            string code = GeneratePassword(16);// 暗号化の際の初期化ベクトルを設定
            string AesIV = code;
            string AesKey = GeneratePassword(16);//暗号化するパスワードを設定
            string[] pattern; //パターン格納用変数()

            // ランダムに数字を取得
            Random rnd = new Random();
            int kubun2 = rnd.Next(13);// 13パターンの中からランダムの数字取得

            switch (kubun2)
            {
                #region パターンを取得

                case 0:
                    pattern = new string[12] { "6", "17", "4", "3", "1", "10", "12", "8", "9", "18", "5", "20" };
                    break;
                case 1:
                    pattern = new string[12] { "15", "4", "6", "13", "16", "12", "18", "7", "17", "1", "3", "14" };
                    break;
                case 2:
                    pattern = new string[12] { "17", "11", "3", "4", "19", "20", "12", "8", "9", "2", "14", "15" };
                    break;
                case 3:
                    pattern = new string[12] { "5", "3", "18", "2", "17", "10", "16", "11", "4", "14", "9", "8" };
                    break;
                case 4:
                    pattern = new string[12] { "4", "11", "2", "18", "14", "8", "20", "13", "17", "10", "1", "16" };
                    break;
                case 5:
                    pattern = new string[12] { "12", "19", "4", "11", "15", "3", "17", "20", "2", "8", "7", "18" };
                    break;
                case 6:
                    pattern = new string[12] { "11", "14", "19", "12", "15", "17", "7", "16", "6", "1", "4", "2" };
                    break;
                case 7:
                    pattern = new string[12] { "12", "11", "7", "6", "4", "8", "9", "17", "20", "18", "16", "2" };
                    break;
                case 8:
                    pattern = new string[12] { "1", "4", "18", "20", "13", "19", "11", "3", "8", "9", "14", "6" };
                    break;
                case 9:
                    pattern = new string[12] { "15", "20", "18", "1", "9", "3", "7", "17", "19", "14", "6", "12" };
                    break;
                case 10:
                    pattern = new string[12] { "13", "16", "12", "11", "19", "17", "1", "6", "4", "9", "3", "2" };
                    break;
                case 11:
                    pattern = new string[12] { "18", "7", "8", "12", "2", "1", "10", "6", "14", "3", "15", "20" };
                    break;
                case 12:
                    pattern = new string[12] { "20", "7", "14", "19", "13", "6", "15", "4", "16", "9", "12", "1" };
                    break;
                default:
                    pattern = new string[12] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
                    break;
                #endregion
            }

            // マインバーを配列にセット
            foreach (char c in set_no)
            {
                mynumber[i] += Util.ToInt(c);
                i++;
            }

            foreach (string dr in pattern)
            {
                switch (dr)
                {
                    #region パターンを取得

                    case "1":
                        bango1 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango1.Substring(0, 16);//暗号化された文字列を次の初期化ベクトルにセット16バイトになるよう切り取り
                        j++;
                        break;
                    case "2":
                        bango2 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango2.Substring(0, 16);
                        j++;
                        break;
                    case "3":
                        bango3 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango3.Substring(0, 16);
                        j++;
                        break;
                    case "4":
                        bango4 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango4.Substring(0, 16);
                        j++;
                        break;
                    case "5":
                        bango5 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango5.Substring(0, 16);
                        j++;
                        break;
                    case "6":
                        bango6 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango6.Substring(0, 16);
                        j++;
                        break;
                    case "7":
                        bango7 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango7.Substring(0, 16);
                        j++;
                        break;
                    case "8":
                        bango8 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango8.Substring(0, 16);
                        j++;
                        break;
                    case "9":
                        bango9 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango9.Substring(0, 16);
                        j++;
                        break;
                    case "10":
                        bango10 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango10.Substring(0, 16);
                        j++;
                        break;
                    case "11":
                        bango11 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango11.Substring(0, 16);
                        j++;
                        break;
                    case "12":
                        bango12 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango12.Substring(0, 16);
                        j++;
                        break;
                    case "13":
                        bango13 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango13.Substring(0, 16);
                        j++;
                        break;
                    case "14":
                        bango14 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango14.Substring(0, 16);
                        j++;
                        break;
                    case "15":
                        bango15 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango15.Substring(0, 16);
                        j++;
                        break;
                    case "16":
                        bango16 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango16.Substring(0, 16);
                        j++;
                        break;
                    case "17":
                        bango17 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango17.Substring(0, 16);
                        j++;
                        break;
                    case "18":
                        bango18 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango18.Substring(0, 16);
                        j++;
                        break;
                    case "19":
                        bango19 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango19.Substring(0, 16);
                        j++;
                        break;
                    case "20":
                        bango20 = Encrypt(Util.ToString(mynumber[j]), AesIV, AesKey);
                        AesIV = bango20.Substring(0, 16);
                        j++;
                        break;
                    default:
                        break;
                    #endregion
                }
            }

            #region TB_CM_SEALA_NOテーブルへのインサート

            #region INSERTSQL
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append(" INSERT INTO TB_CM_SEALA_NO (");
            sql.Append(" KAISHA_CD,");
            sql.Append(" KUBUN,");
            sql.Append(" KUBUN2,");
            sql.Append(" SEALA_NO,");
            sql.Append(" FUYO_NO,");
            sql.Append(" BANGO1,");
            sql.Append(" BANGO2,");
            sql.Append(" BANGO3,");
            sql.Append(" BANGO4,");
            sql.Append(" BANGO5,");
            sql.Append(" BANGO6,");
            sql.Append(" BANGO7,");
            sql.Append(" BANGO8,");
            sql.Append(" BANGO9,");
            sql.Append(" BANGO10,");
            sql.Append(" BANGO11,");
            sql.Append(" BANGO12,");
            sql.Append(" BANGO13,");
            sql.Append(" BANGO14,");
            sql.Append(" BANGO15,");
            sql.Append(" BANGO16,");
            sql.Append(" BANGO17,");
            sql.Append(" BANGO18,");
            sql.Append(" BANGO19,");
            sql.Append(" BANGO20,");
            sql.Append(" KEYCODE,");
            sql.Append(" REGIST_DATE");
            sql.Append(" )");
            sql.Append(" VALUES (");
            sql.Append(" @KAISHA_CD,");
            sql.Append(" @KUBUN,");
            sql.Append(" @KUBUN2,");
            sql.Append(" @SEALA_NO,");
            sql.Append(" @FUYO_NO,");
            sql.Append(" @BANGO1,");
            sql.Append(" @BANGO2,");
            sql.Append(" @BANGO3,");
            sql.Append(" @BANGO4,");
            sql.Append(" @BANGO5,");
            sql.Append(" @BANGO6,");
            sql.Append(" @BANGO7,");
            sql.Append(" @BANGO8,");
            sql.Append(" @BANGO9,");
            sql.Append(" @BANGO10,");
            sql.Append(" @BANGO11,");
            sql.Append(" @BANGO12,");
            sql.Append(" @BANGO13,");
            sql.Append(" @BANGO14,");
            sql.Append(" @BANGO15,");
            sql.Append(" @BANGO16,");
            sql.Append(" @BANGO17,");
            sql.Append(" @BANGO18,");
            sql.Append(" @BANGO19,");
            sql.Append(" @BANGO20,");
            sql.Append(" @KEYCODE,");
            sql.Append(" @REGIST_DATE");
            sql.Append(" )");
            #endregion

            #region 登録パラメータの設定
            dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, kaishaCd);
            dpc.SetParam("@KUBUN", SqlDbType.Int, 4, kubun);
            dpc.SetParam("@KUBUN2", SqlDbType.Int, 4, kubun2);
            dpc.SetParam("@SEALA_NO", SqlDbType.Int, 15, cd);
            dpc.SetParam("@FUYO_NO", SqlDbType.Int, 15, huyo_no);
            if (bango1.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO1", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO1", SqlDbType.VarChar, 24, bango1);
            }
            if (bango2.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO2", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO2", SqlDbType.VarChar, 24, bango2);
            }
            if (bango3.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO3", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO3", SqlDbType.VarChar, 24, bango3);
            }
            if (bango4.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO4", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO4", SqlDbType.VarChar, 24, bango4);
            }
            if (bango5.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO5", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO5", SqlDbType.VarChar, 24, bango5);
            }
            if (bango6.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO6", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO6", SqlDbType.VarChar, 24, bango6);
            }
            if (bango7.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO7", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO7", SqlDbType.VarChar, 24, bango7);
            }
            if (bango8.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO8", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO8", SqlDbType.VarChar, 24, bango8);
            }
            if (bango9.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO9", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO9", SqlDbType.VarChar, 24, bango9);
            }
            if (bango10.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO10", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO10", SqlDbType.VarChar, 24, bango10);
            } if (bango11.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO11", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO11", SqlDbType.VarChar, 24, bango11);
            }
            if (bango12.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO12", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO12", SqlDbType.VarChar, 24, bango12);
            }
            if (bango13.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO13", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO13", SqlDbType.VarChar, 24, bango13);
            }
            if (bango14.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO14", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO14", SqlDbType.VarChar, 24, bango14);
            }
            if (bango15.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO15", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO15", SqlDbType.VarChar, 24, bango15);
            }
            if (bango16.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO16", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO16", SqlDbType.VarChar, 24, bango16);
            }
            if (bango17.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO17", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO17", SqlDbType.VarChar, 24, bango17);
            }
            if (bango18.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO18", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO18", SqlDbType.VarChar, 24, bango18);
            }
            if (bango19.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO19", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO19", SqlDbType.VarChar, 24, bango19);
            }
            if (bango20.Equals(""))
            {
                random = rnd.Next(10);
                dummy = Encrypt(Util.ToString(random), AesIV, AesKey);
                dpc.SetParam("@BANGO20", SqlDbType.VarChar, 24, dummy);
            }
            else
            {
                dpc.SetParam("@BANGO20", SqlDbType.VarChar, 24, bango20);
            }
            dpc.SetParam("@KEYCODE", SqlDbType.VarChar, 24, AesKey);
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now);
            #endregion

            dba.ModifyBySql(Util.ToString(sql), dpc);
            #endregion


            #region TB_CM_SEALA_NO1テーブルへのインサート

            #region INSERTSQL
            DbParamCollection dpc1 = new DbParamCollection();
            StringBuilder sql1 = new StringBuilder();
            sql1.Append(" INSERT INTO TB_CM_SEALA_NO1 (");
            sql1.Append(" KAISHA_CD,");
            sql1.Append(" KUBUN,");
            sql1.Append(" KUBUN2,");
            sql1.Append(" SEALA_NO,");
            sql1.Append(" FUYO_NO,");
            sql1.Append(" PASSCODE,");
            sql1.Append(" REGIST_DATE");
            sql1.Append(" )");
            sql1.Append(" VALUES (");
            sql1.Append(" @KAISHA_CD,");
            sql1.Append(" @KUBUN,");
            sql1.Append(" @KUBUN2,");
            sql1.Append(" @SEALA_NO,");
            sql1.Append(" @FUYO_NO,");
            sql1.Append(" @PASSCODE,");
            sql1.Append(" @REGIST_DATE");
            sql1.Append(" )");
            #endregion

            #region 登録パラメータの設定
            dpc1.SetParam("@KAISHA_CD", SqlDbType.Int, 4, kaishaCd);
            dpc1.SetParam("@KUBUN", SqlDbType.Int, 4, kubun);
            dpc1.SetParam("@KUBUN2", SqlDbType.Int, 4, kubun2);
            dpc1.SetParam("@SEALA_NO", SqlDbType.Int, 15, cd);
            dpc1.SetParam("@FUYO_NO", SqlDbType.Int, 15, huyo_no);
            dpc1.SetParam("@PASSCODE", SqlDbType.VarChar, 16, code);
            dpc1.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now);
            #endregion
            dba.ModifyBySql(Util.ToString(sql1), dpc1);
            #endregion

            // 利用者ログテーブルに実行時間等を登録
            string txtdata = Util.ToString(sql) + Util.ToString(sql1);
            Riyoshalog.Insertlog(kaishaCd, kubun, txtdata, dba);
        }

        #endregion
    }
}
