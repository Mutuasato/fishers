﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using jp.co.fsi.common.constants;

namespace jp.co.fsi.common.util
{
    public class Logger
    {
        #region 定数
        /// <summary>
        /// ログファイルの上限サイズ
        /// </summary>
        private const long MAX_FILE_BYTES = 1024 * 1024;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Logger()
        {
            bool newFileFlg = false;
            string logFilePath = Path.Combine(Util.GetPath(), Constants.LOG_DIR, Constants.ERR_LOG_NM + ".txt");

            if (!Directory.Exists(Path.Combine(Util.GetPath(), Constants.LOG_DIR)))
            {
                // Logディレクトリが無ければまず作成する
                Directory.CreateDirectory(Path.Combine(Util.GetPath(), Constants.LOG_DIR));

                newFileFlg = true;
            }
            else
            {
                if (File.Exists(logFilePath))
                {
                    // ログファイルを作成する前に、1MBを超えていたらリネームする
                    // (error_log_YYYYMMDDbk)
                    FileInfo fi = new FileInfo(logFilePath);
                    if (fi.Length > MAX_FILE_BYTES)
                    {
                        File.Move(logFilePath, Path.Combine(Util.GetPath(), Constants.LOG_DIR, Constants.ERR_LOG_NM + "_" + DateTime.Now.ToString("yyyyMMdd") + "bk.txt"));
                        newFileFlg = true;
                    }
                }
                else
                {
                    newFileFlg = true;
                }
            }

            if (newFileFlg)
            {
                // ログファイルを作成する
                File.Create(logFilePath);
            }
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// ログを出力する
        /// </summary>
        /// <param name="prgId">プログラムID</param>
        /// <param name="ex">例外オブジェクト</param>
        public void Output(string prgId, Exception ex)
        {
            string logFilePath = Path.Combine(Util.GetPath(), Constants.LOG_DIR, Constants.ERR_LOG_NM + ".txt");
            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            StreamWriter writer =
              new StreamWriter(logFilePath, true, sjisEnc);

            // 時刻、プログラムID、メッセージ
            writer.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " [" + prgId + "] " + ex.Message);
            // スタックトレース
            writer.WriteLine(ex.StackTrace);

            // ファイルをクローズ
            writer.Close();
        }
        #endregion
    }
}
