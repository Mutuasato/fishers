﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jp.co.fsi.Common
{
	public static class StaticUtils
	{

		#region ログ
		static readonly log4net.ILog log =
			log4net.LogManager.GetLogger(
				System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		#endregion

		/// <summary>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// <para>■ 機能名称： プログラムバージョン情報の取得。</para>
		/// <para>■ 機能概要： アセンブリ情報に設定されたプログラムバージョンを</para>
		/// <para>■　　　　　　読み込んで結果を返却する。</para>
		/// <para>■</para>
		/// <para>■ 作成者：   FSI</para>
		/// <para>■ 作成日：   2019-11</para>
		/// <para>■ 改修履歴： 新規作成</para>
		/// <para>■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■</para>
		/// </summary>
		/// <returns>バージョン情報の返却</returns>
		public static string GetAppVersion()
		{

			string strVer = string.Empty;

			System.Diagnostics.FileVersionInfo ver = null;

			try
			{


				ver =
					System.Diagnostics.FileVersionInfo.GetVersionInfo(
						System.Reflection.Assembly.GetExecutingAssembly().Location);

				strVer = "Ver." + ver.FileVersion;

			}
			catch (Exception ex)
			{

				log.Error("バージョン取得時のエラー:" + ex.Message);
			}

			return strVer;
		}
	}
}
