﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using jp.co.fsi.common.WsvCustomers;
using System.Data;
using jp.co.fsi.common.util;
using System.IO.MemoryMappedFiles;
using System.Net.NetworkInformation;
using System.Security.AccessControl;
using System.IO;

namespace jp.co.fsi.common
{

#pragma warning disable CS0414
    public static class SecureUtils
	{

		#region ログ
		readonly static log4net.ILog log =
			log4net.LogManager.GetLogger(
				System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        private static string STR_REGKEY = @"SOFTWARE\CommonFsiReg\Fishers";

        private static MemoryMappedFile mmf = null;

        public static string STR_ENDDATE = string.Empty;

        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： 暗号化処理</para>
        /// <para>◇ 処理概要： 暗号化処理</para>
        /// <para>◇</para>
        /// <para>◇ 作成日： 2020/5/14</para>
        /// <para>◇ 作成者： FSI</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        public static string Encrypt(string text)
		{
			string ret = string.Empty;


			try
			{
				using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
				{
					// 公開鍵、秘密鍵をXML形式で取得する
					string publickey = rsa.ToXmlString(false);

					using (RSACryptoServiceProvider rsa1 = new RSACryptoServiceProvider())
					{
						rsa1.FromXmlString(publickey);

						byte[] data = Encoding.UTF8.GetBytes(text);

						data = rsa1.Encrypt(data, false);

						ret = Convert.ToBase64String(data);
					}

				}

			}
			catch (Exception ex)
			{
				log.Error("暗号化処理でエラー" + Environment.NewLine + ex.Message);
			}

			return ret;
		}

        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： 複合化処理</para>
        /// <para>◇ 処理概要： 複合化処理</para>
        /// <para>◇</para>
        /// <para>◇ 作成日： 2020/5/14</para>
        /// <para>◇ 作成者： FSI</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
		public static string Decrypt(string cipher,string keys)
		{

			string ret = string.Empty;

			try
			{
				string privatekey = string.Empty;

				using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
				{

					rsa.FromXmlString(keys);

					byte[] data = Convert.FromBase64String(cipher);

					data = rsa.Decrypt(data, false);

					ret = Encoding.UTF8.GetString(data);
				}

			}
			catch (Exception ex)
			{
				log.Error("複合化処理でエラー" + Environment.NewLine + ex.Message);
			}

			return ret;
		}

        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： プログラム利用許諾チェック</para>
        /// <para>◇ 処理概要： 利用PC上で利用可能かをチェックする。</para>
        /// <para>◇ </para>
        /// <para>◇ 作成日: 2020/01/05</para>
        /// <para>◇ 作成者: FSI</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        public static int ChkUserLicense()
        {

            int intRet = 0;

            // レジストリの確認
            string strReg = GetReg();

            // MACアドレスの取得
            string strMac = GetMacAddr();

            WsvCustomersSoapClient services = new WsvCustomersSoapClient();

            // 利用許諾IDとMACアドレスで利用許可確認
            intRet = services.CertCheck(strReg, strMac);

            // 終了日付の取得
            STR_ENDDATE = services.GetEndDate(strReg, strMac);

            if (!string.IsNullOrEmpty(STR_ENDDATE))
            {
                STR_ENDDATE =
                    STR_ENDDATE.Substring(0, 4) + "年" +
                    STR_ENDDATE.Substring(4, 2) + "月" +
                    STR_ENDDATE.Substring(6, 2) + "日";
            }

            services.Close();

            return intRet;

        }

        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： DB、SSHコネクションストリング設定</para>
        /// <para>◇ 処理概要： DB、SSHコネクションストリング設定</para>
        /// <para>◇            共有メモリ</para>
        /// <para>◇</para>
        /// <para>◇ 作成日： 2020/5/14</para>
        /// <para>◇ 作成者： FSI</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        public static bool SettingConnectionString()
        {
            bool blRet = false;

            // コネクションストリングの取得
            WsvCustomersSoapClient services = new WsvCustomersSoapClient();

            DataSet ds = services.GetConnectionString(GetReg());

            ConfigLoader cldr = new ConfigLoader();

            try
            {

                // データセットがインスタンスされている
                if (null != ds)
                {
                    // データテーブルが存在する。
                    if (0 < ds.Tables.Count)
                    {
                        // データレコードが存在する。
                        if (0 < ds.Tables[0].Rows.Count)
                        {
                            // サーバー等の文字列はConfig.xmlへ
                            // 秘密キー情報は共有メモリへ保存する。

                            cldr.SetCommonConfig("DataAccess", "host", ds.Tables[0].Rows[0]["DBHOST"].ToString());
                            cldr.SetCommonConfig("DataAccess", "database", ds.Tables[0].Rows[0]["DBNAME"].ToString());
                            cldr.SetCommonConfig("DataAccess", "userid", ds.Tables[0].Rows[0]["DBUSERID"].ToString());
                            cldr.SetCommonConfig("DataAccess", "password", ds.Tables[0].Rows[0]["DBPASSWORD"].ToString());
                            cldr.SetCommonConfig("DataAccess", "hostkey", ds.Tables[0].Rows[0]["DBHOSTKEY"].ToString());
                            cldr.SetCommonConfig("DataAccess", "databasekey", ds.Tables[0].Rows[0]["DBNAMEKEY"].ToString());
                            cldr.SetCommonConfig("DataAccess", "useridkey", ds.Tables[0].Rows[0]["DBUSERIDKEY"].ToString());
                            cldr.SetCommonConfig("DataAccess", "passwordkey", ds.Tables[0].Rows[0]["DBPASSWORDKEY"].ToString());


                            cldr.SetConfig("SSH", "SSHHost", ds.Tables[0].Rows[0]["SSHHOST"].ToString());
                            cldr.SetConfig("SSH", "SSHHostKey", ds.Tables[0].Rows[0]["SSHHOSTKEY"].ToString());
                            cldr.SetConfig("SSH", "SSHPort", ds.Tables[0].Rows[0]["SSHPORT"].ToString());
                            cldr.SetConfig("SSH", "SSHPortKey", ds.Tables[0].Rows[0]["SSHPORTKEY"].ToString());
                            cldr.SetConfig("SSH", "SSHLocalHost", ds.Tables[0].Rows[0]["SSHLOCALHOST"].ToString());
                            cldr.SetConfig("SSH", "SSHLocalHostKey", ds.Tables[0].Rows[0]["SSHLOCALHOSTKEY"].ToString());
                            cldr.SetConfig("SSH", "SSHLocalPort", ds.Tables[0].Rows[0]["SSHLOCALPORT"].ToString());
                            cldr.SetConfig("SSH", "SSHLocalPortKey", ds.Tables[0].Rows[0]["SSHLOCALPORTKEY"].ToString());
                            cldr.SetConfig("SSH", "SSHFwdPort", ds.Tables[0].Rows[0]["SSHFWDPORT"].ToString());
                            cldr.SetConfig("SSH", "SSHFwdPortKey", ds.Tables[0].Rows[0]["SSHFWDPORTKEY"].ToString());
                            cldr.SetConfig("SSH", "SSHUser", ds.Tables[0].Rows[0]["SSHUSERID"].ToString());
                            cldr.SetConfig("SSH", "SSHUserKey", ds.Tables[0].Rows[0]["SSHUSERIDKEY"].ToString());
                            cldr.SetConfig("SSH", "SSHPassword", ds.Tables[0].Rows[0]["SSHPASSWORD"].ToString());
                            cldr.SetConfig("SSH", "SSHPasswordKey", ds.Tables[0].Rows[0]["SSHPASSWORDKEY"].ToString());

                            // 設定ファイル(Comfig.xml)へ保存
                            cldr.SaveConfig();

                        }
                    }
                }
                blRet = true;
            }
            catch (Exception ex)
            {
                log.Error("コネクションストリング設定中にエラーが発生しました。" + ex.Message, ex);
            }
            finally
            {
                services.Close();
                ds.Dispose();
            }

            return blRet;
        }

        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： 共有メモリへの保存</para>
        /// <para>◇ 処理概要： 共有メモリへの保存</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        //public static bool WriteSharedMem(string strShareName, string values)
        //{
        //    bool blRet = false;
        //    MemoryMappedViewAccessor accessor = null;
        //    try
        //    {
        //        MemoryMappedFileSecurity permission = mmf.GetAccessControl();

        //        permission.AddAccessRule(
        //            new AccessRule<MemoryMappedFileRights>("everyone",
        //            MemoryMappedFileRights.FullControl, AccessControlType.Allow));

        //        // mmf = MemoryMappedFile.CreateNew(@"Global\" + strShareName, 1024);
        //        mmf = MemoryMappedFile.CreateOrOpen(@"Global\" + strShareName, 1024,
        //            MemoryMappedFileAccess.ReadWrite,MemoryMappedFileOptions.None,
        //            permission,HandleInheritability.Inheritable);

        //        accessor = mmf.CreateViewAccessor();

        //        char[] data = values.ToCharArray();

        //        accessor.Write(0, data.Length);

        //        accessor.WriteArray<char>(sizeof(int), data, 0, data.Length);

        //        blRet = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("共有メモリへの保存に失敗しました。" + ex.Message, ex);
        //    }
        //    finally
        //    {
        //        accessor.Dispose();
        //    }

        //    return blRet;
        //}

        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： 共有メモリからの取得</para>
        /// <para>◇ 処理概要： 共有メモリからの取得</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        //public static string ReadShareMem(string strShareName)
        //{
        //    string strRet = string.Empty;
        //    MemoryMappedViewAccessor accessor = null;
        //    try
        //    {
        //        mmf = MemoryMappedFile.OpenExisting(@"Global\"+strShareName,MemoryMappedFileRights.FullControl);
        //        accessor = mmf.CreateViewAccessor();

        //        char[] data = new char[1024];

        //        int readSize = accessor.ReadArray<char>(sizeof(int), data, 0, data.Length);

        //        strRet = new string(data).Trim('\0');

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("共有メモリ情報取得時にエラーが発生しました。" + ex.Message, ex);
        //    }
        //    finally
        //    {
        //        if (null != accessor)
        //            accessor.Dispose();
        //    }

        //    return strRet;
        //}

        /// <summary>
        /// <para>◇ 処理名称： 共有メモリの開放</para>
        /// <para>◇ 処理概要： 共有メモリの開放</para>
        /// </summary>
        //public static bool CloseShareMem()
        //{

        //    try
        //    {
        //        // メモリ開放
        //        mmf.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("共有メモリ削除時にエラーが発生しました。" + ex.Message, ex);

        //        return false;
        //    }

        //    return true;

        //}


        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： MACADDRESSの取得</para>
        /// <para>◇ 処理概要： 本インストーラーを起動したPCのMACアドレスを取得する。</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        private static string GetMacAddr()
        {

            string strAddr = string.Empty;

            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface adapter in nics)
            {

                strAddr = adapter.GetPhysicalAddress().ToString();

                break;
            }

            return strAddr;

        }

        /// <summary>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// <para>◇ 処理名称： レジストリデータの取得</para>
        /// <para>◇ 処理概要： レジストリ内に利用許諾IDが登録されていないか確認する</para>
        /// <para>◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇</para>
        /// </summary>
        private static string GetReg()
        {

            string strReg = string.Empty;

#if DEBUG==false
            try
            {
                Microsoft.Win32.RegistryKey regKey =
                    Microsoft.Win32.Registry.CurrentUser.OpenSubKey(STR_REGKEY);

                if (null != regKey)
                {
                    strReg = regKey.GetValue("FSI_CERTID").ToString();
                }
                regKey.Close();
            }
            catch (Exception ex)
            {
                log.Error("レジストリデータ取得時のエラー" + ex.Message, ex);
            }
#else
            strReg = "A5DE6206-C692-4FBF-92C6-05FB2079E2AA";
#endif
            return strReg;

        }
    }

}
