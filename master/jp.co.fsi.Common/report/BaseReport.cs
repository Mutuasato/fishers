﻿using System;
using System.Data;

using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.util;

namespace jp.co.fsi.common.report
{
    /// <summary>
    /// 帳票基底クラス
    /// </summary>
    public class BaseReport : GrapeCity.ActiveReports.SectionReport
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public BaseReport()
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        public BaseReport(DataTable tgtData) : this()
        {
            // 帳票IDを取得(※目的として、それを元に帳票設定を取得したい)
            string repID = this.GetType().Name;

            // 設定情報を取得
            ReportPrintInfo repInfo = new ReportPrintInfo();
            DataRow drSetting = repInfo.GetPrintSetting(repID, false);

            if (drSetting == null)
            {
                // プリンタのジョブに表示されるドキュメント名を指定
                this.Document.Name = "(名称未設定)";
            }
            else
            {
                // プリンタのジョブに表示されるドキュメント名を指定
                this.Document.Name = Util.ToString(drSetting["ドキュメント名"]);

                // 出力先プリンタを指定
                if (!ValChk.IsEmpty(drSetting["出力プリンタ"]))
                {
                    this.Document.Printer.PrinterName = Util.ToString(drSetting["出力プリンタ"]);
                }

                // 手差しトレイを使う
                if (Util.ToString(drSetting["手差し"]).Equals("○"))
                {
                    foreach (System.Drawing.Printing.PaperSource ps in this.Document.Printer.PrinterSettings.PaperSources)
                    {
                        //「手差し」に設定します。
                        if (ps.Kind == System.Drawing.Printing.PaperSourceKind.Manual)
                        {
                            this.PageSettings.DefaultPaperSource = false;
                            this.PageSettings.PaperSource = ps.Kind;
                            this.Document.Printer.DefaultPageSettings.PaperSource = ps;
                            this.Document.Printer.PrinterSettings.DefaultPageSettings.PaperSource = ps;
                            break;
                        }
                    }
                }

                // カラー/モノクロ印刷
                if (Util.ToString(drSetting["カラー"]).Equals("○"))
                {
                    this.Document.Printer.DefaultPageSettings.Color = true;
                    this.Document.Printer.PrinterSettings.DefaultPageSettings.Color = true;
                }
                else if (Util.ToString(drSetting["カラー"]).Equals("×"))
                {
                    this.Document.Printer.DefaultPageSettings.Color = false;
                    this.Document.Printer.PrinterSettings.DefaultPageSettings.Color = false;
                }

                // 両面印刷
                if (Util.ToString(drSetting["両面印刷"]).Equals("○"))
                {
                    this.PageSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                    this.Document.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                }
            }

            this.DataSource = tgtData;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        /// <param name="repID">対象レポートID</param>
        public BaseReport(DataTable tgtData, string repID) : this()
        {
            // 帳票IDを取得(※目的として、それを元に帳票設定を取得したい)
            //if (repID.Equals(""))
            //{
            //    repID = this.GetType().Name;
            //}

            // 設定情報を取得
            ReportPrintInfo repInfo = new ReportPrintInfo();
            DataRow drSetting = repInfo.GetPrintSetting(repID, false);

            if (drSetting == null)
            {
                // プリンタのジョブに表示されるドキュメント名を指定
                this.Document.Name = "(名称未設定)";
            }
            else
            {
                // プリンタのジョブに表示されるドキュメント名を指定
                this.Document.Name = Util.ToString(drSetting["ドキュメント名"]);

                // 出力先プリンタを指定
                if (!ValChk.IsEmpty(drSetting["出力プリンタ"]))
                {
                    this.Document.Printer.PrinterName = Util.ToString(drSetting["出力プリンタ"]);
                }

                // 手差しトレイを使う
                if (Util.ToString(drSetting["手差し"]).Equals("○"))
                {
                    foreach (System.Drawing.Printing.PaperSource ps in this.Document.Printer.PrinterSettings.PaperSources)
                    {
                        //「手差し」に設定します。
                        if (ps.Kind == System.Drawing.Printing.PaperSourceKind.Manual)
                        {
                            this.PageSettings.DefaultPaperSource = false;
                            this.PageSettings.PaperSource = ps.Kind;
                            this.Document.Printer.DefaultPageSettings.PaperSource = ps;
                            this.Document.Printer.PrinterSettings.DefaultPageSettings.PaperSource = ps;
                            break;
                        }
                    }
                }

                // カラー/モノクロ印刷
                if (Util.ToString(drSetting["カラー"]).Equals("○"))
                {
                    this.Document.Printer.DefaultPageSettings.Color = true;
                    this.Document.Printer.PrinterSettings.DefaultPageSettings.Color = true;
                }
                else if (Util.ToString(drSetting["カラー"]).Equals("×"))
                {
                    this.Document.Printer.DefaultPageSettings.Color = false;
                    this.Document.Printer.PrinterSettings.DefaultPageSettings.Color = false;
                }

                // 両面印刷
                if (Util.ToString(drSetting["両面印刷"]).Equals("○"))
                {
                    this.PageSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                    this.Document.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                }
            }

            this.DataSource = tgtData;
        }

        #endregion
    }
}
