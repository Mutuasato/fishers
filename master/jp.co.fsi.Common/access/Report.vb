﻿Imports System.Runtime.InteropServices

'Namespace jp.co.fsi.common.access
''' <summary>
''' 帳票出力クラスです。
''' </summary>
''' <remarks></remarks>
<ComClass(Report.ClassId, Report.InterfaceId, Report.EventsId)>
    Public Class Report
#Region "GUID"
        Public Const ClassId As String = "EB8B2D2A-52B6-4A11-B49C-599D90AFBE44"
        Public Const InterfaceId As String = "EE98B230-7A5C-4510-85A1-291BBE2868C5"
        Public Const EventsId As String = "62F69658-9B39-405E-9903-F65232CA2ACF"
#End Region

#Region "private定数"
        ''' <summary>
        ''' 印刷
        ''' </summary>
        ''' <remarks></remarks>
        Private Const acViewNormal As Integer = 0

        ''' <summary>
        ''' プレビュー
        ''' </summary>
        ''' <remarks></remarks>
        Private Const acViewPreview As Integer = 2
#End Region

#Region "コンストラクタ"
        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
        End Sub
#End Region

#Region "publicメソッド"
        ''' <summary>
        ''' 帳票を出力します。
        ''' </summary>
        ''' <param name="mdbPath">mdbファイルのパス(フルパス)</param>
        ''' <param name="repName">レポートオブジェクトの名前</param>
        ''' <param name="guid">出力のためのユニークキー(GUID)</param>
        ''' <param name="doPreview">プレビュー出力するかどうか</param>
        ''' <remarks></remarks>
        Public Sub OutputReport(ByVal mdbPath As String, ByVal repName As String,
                                ByVal guid As String, Optional ByVal doPreview As Boolean = False)
            'Dim p As Process = Process.Start("MSAccess.exe", """" & mdbPath & """ /runtime")
            'p.WaitForInputIdle()

            ' TODO:試しに3秒待機
            'System.Threading.Thread.Sleep(3000)

            '大きな帳票を印刷しても「ActiveX コンポーネントを作成できません。」が出ないよう対策してみた。
            Dim p As Process = New Process()
            p.StartInfo.FileName = "MSAccess.exe"
            p.StartInfo.Arguments = """" & mdbPath & """ /runtime"
            p.Start()
            p.WaitForExit()

            Dim accApp As Object = GetObject(mdbPath)
            Dim accDoCmd As Object = accApp.DoCmd

            If doPreview Then
                accDoCmd.OpenReport(repName, acViewPreview, , "GUID = '" & guid & "'")
                MsgBox("印刷プレビューを表示します。" & Environment.NewLine & "「OK」を押すとプレビューを終了します。")
                Marshal.ReleaseComObject(accDoCmd)
                accDoCmd = Nothing
                accApp.Quit()
                Marshal.ReleaseComObject(accApp)
            Else
                accDoCmd.OpenReport(repName, acViewNormal, , "GUID = '" & guid & "'")
                Marshal.ReleaseComObject(accDoCmd)
                accDoCmd = Nothing
                accApp.Quit()
                Marshal.ReleaseComObject(accApp)
                MsgBox("印刷が完了しました。")
            End If
        End Sub
#End Region
    End Class
'End Namespace
