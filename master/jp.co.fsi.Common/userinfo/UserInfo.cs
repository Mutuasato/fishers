﻿using System;
using System.Collections;
using System.Data;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.util;

namespace jp.co.fsi.common.userinfo
{
    /// <summary>
    /// ユーザー情報管理クラス
    /// </summary>
    public class UserInfo
    {
        #region private変数
        /// <summary>
        /// DBAccessオブジェクト
        /// </summary>
        private DbAccess _dba;
        #endregion

        #region プロパティ
        private string _userId;
        /// <summary>
        /// ユーザーID
        /// </summary>
        public string UserId
        {
            get
            {
                return this._userId;
            }
        }

        private string _userNm;
        /// <summary>
        /// ユーザー名
        /// </summary>
        public string UserNm
        {
            get
            {
                return this._userNm;
            }
        }

	private string _userCd;
        /// <summary>
        /// ユーザーコード
        /// </summary>
        public string UserCd
        {
            get
            {
                return this._userCd;
            }
        }
        private string _kaishaCd;
        /// <summary>
        /// 会社コード
        /// </summary>
        public string KaishaCd
        {
            get
            {
                return this._kaishaCd;
            }
            set
            {
                this._kaishaCd = value;
            }
        }

        private string _kaishaNm;
        /// <summary>
        /// 会社名
        /// </summary>
        public string KaishaNm
        {
            get
            {
                return this._kaishaNm;
            }
            set
            {
                this._kaishaNm = value;
            }
        }

        private int _kaikeiNendo;
        /// <summary>
        /// 会計年度(西暦)
        /// </summary>
        public int KaikeiNendo
        {
            get
            {
                return this._kaikeiNendo;
            }
            set
            {
                this._kaikeiNendo = value;
            }
        }

        private int _kessanKi;
        /// <summary>
        /// 決算期(数値)
        /// </summary>
        public int KessanKi
        {
            get
            {
                return this._kessanKi;
            }
            set
            {
                this._kessanKi = value;
                OnChangeKessanKi();
            }
        }

        private string _kessanKiDsp;
        /// <summary>
        /// 決算期(表示用)
        /// </summary>
        public string KessanKiDsp
        {
            get
            {
                return this._kessanKiDsp;
            }
            set
            {
                this._kessanKiDsp = value;
            }
        }

        private int _hakotanka;
        /// <summary>
        /// 箱単価
        /// </summary>
        public int HakoTanka
        {
            get
            {
                return this._hakotanka;
            }
            set
            {
                this._hakotanka = value;
            }
        }

        /// <summary>
        /// 会計期間に関する設定情報
        /// </summary>
        public DataRow KaikeiSettings
        {
            get
            {
                return GetKaikeiSettings();
            }
        }

        private Hashtable _auth;
        /// <summary>
        /// 権限
        /// </summary>
        public Hashtable Auth
        {
            get
            {
                return this._auth;
            }
        }
        private string _shishoCd;
        /// <summary>
        /// 支所コード
        /// </summary>
        public string ShishoCd
        {
            get
            {
                return this._shishoCd;
            }
            set
            {
                this._shishoCd = value;
            }
        }

        private string _shishoNm;
        /// <summary>
        /// 支所名
        /// </summary>
        public string ShishoNm
        {
            get
            {
                return this._shishoNm;
            }
            set
            {
                this._shishoNm = value;
            }
        }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dba">DBアクセスオブジェクト</param>
        public UserInfo(DbAccess dba)
        {
            this._dba = dba;
            ConfigLoader cLoader = new ConfigLoader();

            //TODO:とりあえず現状はxmlファイルから読み込む
            //もしかしたらDBから読み込んだ方がいいかも
            try
            {
                this._userId = cLoader.LoadCommonConfig("UserInfo", "userid");
                this._userNm = cLoader.LoadCommonConfig("UserInfo", "usernm");
                this._userCd = cLoader.LoadCommonConfig("UserInfo", "usercd");
                this._kaishaCd = cLoader.LoadCommonConfig("UserInfo", "kaishacd");
                this._kaishaNm = cLoader.LoadCommonConfig("UserInfo", "kaishanm");
                this._kaikeiNendo = Util.ToInt(cLoader.LoadCommonConfig("UserInfo", "kaikeinendo"));
                this._kessanKi = Util.ToInt(cLoader.LoadCommonConfig("UserInfo", "kessanki"));
                this._kessanKiDsp = cLoader.LoadCommonConfig("UserInfo", "kessankiDsp");
                this._hakotanka = Util.ToInt(cLoader.LoadCommonConfig("UserInfo", "hakotanka"));
                // 20171003 TAKADA-Add 新ERP用カスタマイズ↓
                this._shishoCd = cLoader.LoadCommonConfig("UserInfo", "shishocd");
                this._shishoNm = cLoader.LoadCommonConfig("UserInfo", "shishonm");
                // 20171003 TAKADA-Add 新ERP用カスタマイズ↑
                this._auth = new Hashtable();
                this._auth.Add("HN",
                    cLoader.LoadCommonConfig("UserInfo", "usehn").Equals("1"));
                this._auth.Add("KB",
                    cLoader.LoadCommonConfig("UserInfo", "usekb").Equals("1"));
                this._auth.Add("ZM",
                    cLoader.LoadCommonConfig("UserInfo", "usezm").Equals("1"));
                this._auth.Add("KY",
                    cLoader.LoadCommonConfig("UserInfo", "useky").Equals("1"));
                // 20141209 kinjo-Add 名護漁協用カスタマイズ↓
                this._auth.Add("SK",
                    cLoader.LoadCommonConfig("UserInfo", "usesk").Equals("1"));
                // 20141209 kinjo-Add 名護漁協用カスタマイズ↑
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Print(ex.Message);
                //TODO:デバッグ用。固定なので開発環境に依存の恐れ
                this._userId = "test";
                this._userNm = "テスト 太郎";
                this._userCd = "1";
                this._kaishaCd = "1";
                this._kaishaNm = "テスト会社";
                this._kaikeiNendo = 2017;
                this._kessanKi = 29;
                this._kessanKiDsp = "第29期 (H29/ 4/ 1 ～ H29/ 3/31)";
                this._hakotanka = 60;
                this._auth = new Hashtable();
                this._auth.Add("HN", true);
                this._auth.Add("KB", true);
                this._auth.Add("ZM", true);
                this._auth.Add("KY", true);
                this._shishoCd = "0";
                this._shishoNm = "全て";
            }

            if (this._kessanKi == 0)
            {
                DateTime sysDate = DateTime.Now;
                if (sysDate.Month >= 1 && sysDate.Month <= 3)
                {
                    this._kaikeiNendo = sysDate.Year - 1;
                }
                else
                {
                    this._kaikeiNendo = sysDate.Year;
                }
                // 操作している年度に紐付く決算期を保持する
                try
                {

                    if (null == dba.GetKaikeiSettingsByDate(this._kaishaCd, sysDate)) return;

                    DataRow settings = dba.GetKaikeiSettingsByDate(this._kaishaCd, sysDate).Rows[0];
                    this._kessanKi = Util.ToInt(settings["KESSANKI"]);
                    string[] aryJpDateFr = Util.ConvJpDate(Util.ToDate(settings["KAIKEI_KIKAN_KAISHIBI"]), dba);
                    string[] aryJpDateTo = Util.ConvJpDate(Util.ToDate(settings["KAIKEI_KIKAN_SHURYOBI"]), dba);

                    this._kessanKiDsp = "第" + Util.ToString(this._kessanKi) + "期 ("
                        + aryJpDateFr[6] + " ～ " + aryJpDateTo[6] + ")";

                    // 設定した内容をconfig.xmlに書き込む
                    cLoader.SetCommonConfig("UserInfo", "kaikeinendo", Util.ToString(this._kaikeiNendo));
                    cLoader.SetCommonConfig("UserInfo", "kessanki", Util.ToString(this._kessanKi));
                    cLoader.SetCommonConfig("UserInfo", "kessankiDsp", this._kessanKiDsp);

                    // config.xmlを保存
                    cLoader.SaveConfig();
                }
                catch (Exception)
                {
                    this._kessanKi = 25;
                    this._kessanKiDsp = "第25期 (H25/ 4/ 1 ～ H26/ 3/31)";
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 決算期変更時の処理
        /// </summary>
        private void OnChangeKessanKi()
        {
            DataRow settings = GetKaikeiSettings();

            // 選択された決算期に紐付く決算期を保持する
            string[] aryJpDateFr = Util.ConvJpDate(Util.ToDate(settings["KAIKEI_KIKAN_KAISHIBI"]), this._dba);
            string[] aryJpDateTo = Util.ConvJpDate(Util.ToDate(settings["KAIKEI_KIKAN_SHURYOBI"]), this._dba);

            this._kessanKiDsp = "第" + Util.ToString(this._kessanKi) + "期 ("
                + aryJpDateFr[6] + " ～ " + aryJpDateTo[6] + ")";
            this._kaikeiNendo = Util.ToDate(settings["KAIKEI_KIKAN_KAISHIBI"]).Year;
        }

        /// <summary>
        /// 会計期間に関する設定を取得する
        /// </summary>
        /// <returns>会計期間に関する設定(DataRow)</returns>
        private DataRow GetKaikeiSettings()
        {
            return this._dba.GetKaikeiSettingsByKessanKi(this._kaishaCd, this._kessanKi).Rows[0];
        }
        #endregion
    }
}
