﻿using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.common.dataaccess
{
    /// <summary>
    /// データアクセスクラス
    /// </summary>
    public class DbAccess
    {

        #region ログ
        readonly log4net.ILog log =
            log4net.LogManager.GetLogger(
                System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion


        #region private変数
        /// <summary>
        /// SQL Serverへの接続情報
        /// </summary>
        private SqlConnection _conn;

        /// <summary>
        /// コマンド
        /// </summary>
        private SqlCommand _cmd;

        /// <summary>
        /// トランザクション
        /// </summary>
        private SqlTransaction _trans;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DbAccess()
        {
            // 接続情報をインスタンス化
            this._conn = new SqlConnection();
        }
        #endregion

        #region publicメソッド(基盤系)
        /// <summary>
        /// DB接続を開始する
        /// </summary>
        /// <returns>true:接続成功/false:接続失敗</returns>
        public bool Connect()
        {
            try
            {
                string connStr = "";

                try
                {
                    ConfigLoader cLoader = new ConfigLoader();

                    // config.xmlから設定内容を読み込む
                    string strKey01 = cLoader.LoadCommonConfig("DataAccess", "hostkey");
                    string strKey02 = cLoader.LoadCommonConfig("DataAccess", "databasekey");
                    string strKey03 = cLoader.LoadCommonConfig("DataAccess", "useridkey");
                    string strKey04 = cLoader.LoadCommonConfig("DataAccess", "passwordkey");

                    string strValue01 = cLoader.LoadCommonConfig("DataAccess", "host");
                    string strValue02 = cLoader.LoadCommonConfig("DataAccess", "database");
                    string strValue03 = cLoader.LoadCommonConfig("DataAccess", "userid");
                    string strValue04 = cLoader.LoadCommonConfig("DataAccess", "password");

                    connStr =
                        "Data Source=" + SecureUtils.Decrypt(strValue01, strKey01)
                        + ";Initial Catalog=" + SecureUtils.Decrypt(strValue02, strKey02)
                        + ";User Id=" + SecureUtils.Decrypt(strValue03, strKey03)
                        + ";Password=" + SecureUtils.Decrypt(strValue04, strKey04);
                }
                catch (Exception)
                {
                    // 開発用。ConfigLoaderの読み込みに失敗した場合、固定で接続先を設定
                    // TODO:固定なので、開発環境が変わればここ書き換えてリコンパイルして下さい。。。
                    connStr =
                        //"Data Source=192.168.51.150"
                        "Data Source=(local)"
                        + ";Initial Catalog=SosDb"
                        + ";User Id=sj"
                        + ";Password=6pLEcGn3xE";
                }

                this._conn.ConnectionString = connStr;
                //接続開始
                this._conn.Open();

                //トランザクション開始
                this._trans = this._conn.BeginTransaction();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 条件指定によってDataTable形式でデータを取得
        /// </summary>
        /// <param name="cols">SELECT句</param>
        /// <param name="table">FROM句</param>
        /// <remarks>並び順は保証しない</remarks>
        /// <returns>取得結果(DataTable形式)</returns>
        public DataTable GetDataTableByCondition(string cols, string table)
        {
            return GetDataTableByCondition(cols, table, null);
        }

        /// <summary>
        /// 条件指定によってDataTable形式でデータを取得
        /// </summary>
        /// <param name="cols">SELECT句</param>
        /// <param name="table">FROM句</param>
        /// <param name="where">WHERE句</param>
        /// <remarks>並び順は保証しない</remarks>
        /// <returns>取得結果(DataTable形式)</returns>
        public DataTable GetDataTableByCondition(string cols, string table, string where)
        {
            return GetDataTableByCondition(cols, table, where, null);
        }

        /// <summary>
        /// 条件指定によってDataTable形式でデータを取得
        /// </summary>
        /// <param name="cols">SELECT句</param>
        /// <param name="table">FROM句</param>
        /// <param name="where">WHERE句</param>
        /// <param name="order">ORDER BY句</param>
        /// <returns>取得結果(DataTable形式)</returns>
        public DataTable GetDataTableByCondition(string cols, string table, string where, string order)
        {
            return GetDataTableByCondition(cols, table, where, order, null);
        }

        /// <summary>
        /// 条件指定によってDataTable形式でデータを取得
        /// </summary>
        /// <param name="cols">SELECT句</param>
        /// <param name="table">FROM句</param>
        /// <param name="where">WHERE句</param>
        /// <param name="order">ORDER BY句</param>
        /// <param name="group">GROUP BY句</param>
        /// <returns>取得結果(DataTable形式)</returns>
        public DataTable GetDataTableByCondition(string cols, string table, string where, string order, string group)
        {
            return GetDataTableByConditionWithParams(cols, table, where, order, group, new DbParamCollection());
        }

        /// <summary>
        /// 条件指定によってDataTable形式でデータを取得
        /// </summary>
        /// <param name="cols">SELECT句</param>
        /// <param name="table">FROM句</param>
        /// <param name="where">WHERE句</param>
        /// <param name="dpc">バインドパラメータのコレクション</param>
        /// <returns>取得結果(DataTable形式)</returns>
        /// <remarks>
        /// 並び順は保証しない
        /// where句にバインドパラメータを"@Params"を指定することが出来ます
        /// ※where句でなくとも可能です…一応。
        /// </remarks>
        public DataTable GetDataTableByConditionWithParams(string cols, string table, string where, DbParamCollection dpc)
        {
            return GetDataTableByConditionWithParams(cols, table, where, null, dpc);
        }

        /// <summary>
        /// 条件指定によってDataTable形式でデータを取得
        /// </summary>
        /// <param name="cols">SELECT句</param>
        /// <param name="table">FROM句</param>
        /// <param name="where">WHERE句</param>
        /// <param name="order">ORDER BY句</param>
        /// <param name="dpc">バインドパラメータのコレクション</param>
        /// <returns>取得結果(DataTable形式)</returns>
        /// <remarks>
        /// where句にバインドパラメータを"@Params"を指定することが出来ます
        /// ※where句でなくとも可能です…一応。
        /// </remarks>
        public DataTable GetDataTableByConditionWithParams(string cols, string table, string where, string order, DbParamCollection dpc)
        {
            return GetDataTableByConditionWithParams(cols, table, where, order, null, dpc);
        }

        /// <summary>
        /// 条件指定によってDataTable形式でデータを取得
        /// </summary>
        /// <param name="cols">SELECT句</param>
        /// <param name="table">FROM句</param>
        /// <param name="where">WHERE句</param>
        /// <param name="order">ORDER BY句</param>
        /// <param name="group">GROUP BY句</param>
        /// <param name="dpc">バインドパラメータのコレクション</param>
        /// <returns>取得結果(DataTable形式)</returns>
        /// <remarks>
        /// where句にバインドパラメータを"@Params"を指定することが出来ます
        /// ※where句でなくとも可能です…一応。
        /// </remarks>
        public DataTable GetDataTableByConditionWithParams(string cols, string table, string where, string order, string group, DbParamCollection dpc)
        {
            //SQL文の組み立て
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append("SELECT " + cols);
            sbQuery.Append(" FROM " + table);

            if (!ValChk.IsEmpty(where))
            {
                sbQuery.Append(" WHERE " + where);
            }

            if (!ValChk.IsEmpty(group))
            {
                sbQuery.Append(" GROUP BY " + group);
            }

            if (!ValChk.IsEmpty(order))
            {
                sbQuery.Append(" ORDER BY " + order);
            }

            return GetDataTableFromSqlWithParams(sbQuery.ToString(), dpc);
        }

        /// <summary>
        /// SQL文によってDataTable形式でデータを取得
        /// </summary>
        /// <param name="sql">SQL文全体</param>
        /// <remarks>SELECT句、FROM句、WHERE句等々一通り含むSQL文を引数に指定してください。</remarks>
        /// <returns>取得結果(DataTable形式)</returns>
        public DataTable GetDataTableFromSql(string sql)
        {
            return GetDataTableFromSqlWithParams(sql, new DbParamCollection());
        }

        /// <summary>
        /// SQL文によってDataTable形式でデータを取得
        /// </summary>
        /// <param name="sql">SQL文全体</param>
        /// <remarks>SELECT句、FROM句、WHERE句等々一通り含むSQL文を引数に指定してください。</remarks>
        /// <returns>取得結果(DataTable形式)</returns>
        public DataTable GetDataTableFromSqlWithParams(string sql, DbParamCollection dpc)
        {
            this._cmd = new SqlCommand(sql, this._conn);
            this._cmd.Transaction = this._trans;
            this._cmd.CommandTimeout = 0;
            if (dpc.Params.Count > 0)
            {
                SqlParameter sPar = null;
                DbParam dPar = null;
                foreach (DictionaryEntry d in dpc.Params)
                {
                    dPar = (DbParam)d.Value;

                    switch (dPar.Type)
                    {
                        case SqlDbType.Char:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                            break;

                        case SqlDbType.VarChar:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                            break;

                        case SqlDbType.Decimal:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                            sPar.Precision = (byte)dPar.FullLength;
                            sPar.Scale = (byte)dPar.DecLength;
                            break;
     
                        case SqlDbType.DateTime:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                            break;

                        default:
                            // Char,VarChar, Decimal, DateTime以外の対応未定
                            // もし必要があれば
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                            break;
                    }

                    sPar.Value = dPar.Val;

                    // バインドパラメータをcommandに設定
                    this._cmd.Parameters.Add(sPar);
                }
            }

            SqlDataReader reader = null;
            try
            {
                reader = this._cmd.ExecuteReader();

                // 型付テーブルを作成します。
                DataTable dt = CreateSchemaDataTable(reader);
                DataRow row = null;

                // 結果を格納
                while (reader.Read())
                {
                    row = dt.NewRow();

                    

                    for (int i = 0; i < reader.FieldCount; i++)
                    {

                        row[i] = reader.GetValue(i);
                    }

                    dt.Rows.Add(row);
                }


                //結果を返す
                return dt;
            }
            catch (Exception ex)
            {
                log.Error("データ取得時のエラー" + ex.Message);

                return null;
            }
            finally
            {
                //クローズ処理をfinallyに移動 2020/02/19 吉村
                if (reader != null)
                {
                    // readerを破棄
                    reader.Close();
                    reader.Dispose();
                    //コマンドオブジェクトは使い捨て(変えるかも)
                    this._cmd.Dispose();
                }
            }
        }

        /// <summary>
        /// Insertを実行します。
        /// </summary>
        /// <param name="table">テーブル名</param>
        /// <param name="dpc">更新値のコレクション</param>
        /// <returns>更新された行数</returns>
        public int Insert(string table, DbParamCollection dpc)
        {
            // SQL文の組み立て
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbColNms = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();

            DbParam dPar = null;

            foreach (DictionaryEntry d in dpc.Params)
            {
                dPar = (DbParam)d.Value;
                if (!ValChk.IsEmpty(sbColNms))
                {
                    sbColNms.Append(", ");
                    sbValues.Append(", ");
                }
                //カラム名はキーの'@'を取り除く。VALUES句はそのまま。
                //但し、Valueに@NOWDATEが設定されていたら現在日付を、@NOWTIMEが設定されていたら現在日時を更新
                sbColNms.Append(Util.ToString(d.Key).Replace("@", ""));
                if (dPar.Val != null && dPar.Val.Equals("@NOWDATE"))
                {
                    sbValues.Append("CONVERT(varchar(10), GETDATE(), 111)");
                }
                else if (dPar.Val != null && dPar.Val.Equals("@NOWTIME"))
                {
                    sbValues.Append("GETDATE()");
                }
                else
                {
                    sbValues.Append(Util.ToString(d.Key));
                }
            }
            sbSql.Append("INSERT INTO " + table + "(");
            sbSql.Append(sbColNms.ToString());
            sbSql.Append(") VALUES (");
            sbSql.Append(sbValues.ToString());
            sbSql.Append(")");

            this._cmd = new SqlCommand(sbSql.ToString(), this._conn);
            this._cmd.Transaction = this._trans;

            // パラメータの設定
            SqlParameter sPar = null;
            foreach (DictionaryEntry d in dpc.Params)
            {
                dPar = (DbParam)d.Value;

                if (dPar.Val != null && (dPar.Val.Equals("@NOWDATE") || dPar.Val.Equals("@NOWTIME")))
                {
                    // Valueに@NOWDATEや@NOWTIMEが設定されている項目に関してはSQL組み立て時に反映済み
                    continue;
                }

                switch (dPar.Type)
                {
                    case SqlDbType.Char:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                        break;

                    case SqlDbType.VarChar:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                        break;

                    case SqlDbType.Decimal:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                        sPar.Precision = (byte)dPar.FullLength;
                        sPar.Scale = (byte)dPar.DecLength;
                        break;

                    case SqlDbType.DateTime:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                        break;

                    default:
                        // Char,VarChar, Decimal, DateTime以外の対応未定
                        // もし必要があれば
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                        break;
                }

                sPar.Value = dPar.Val == null ? DBNull.Value : dPar.Val;

                // バインドパラメータをcommandに設定
                this._cmd.Parameters.Add(sPar);
            }

            // 更新処理を実行
            int affectedRows = this._cmd.ExecuteNonQuery();

            // コマンドオブジェクトは使い捨て(変えるかも)
            this._cmd.Dispose();

            // 更新された行数を返す
            return affectedRows;
        }

        /// <summary>
        /// Updateを実行します。
        /// </summary>
        /// <param name="table">テーブル名</param>
        /// <param name="setDpc">更新値のコレクション</param>
        /// <param name="where">WHERE句</param>
        /// <param name="whereDpc">WHERE句のバインド変数のコレクション</param>
        /// <returns>更新された行数</returns>
        public int Update(string table, DbParamCollection setDpc, string where, DbParamCollection whereDpc)
        {
            // SQL文の組み立て
            StringBuilder sbSql = new StringBuilder();

            DbParam dPar = null;

            bool isStartCreateUpdVals = false;

            sbSql.Append("UPDATE " + table + " SET ");
            
            foreach (DictionaryEntry d in setDpc.Params)
            {
                if (isStartCreateUpdVals)
                {
                    sbSql.Append(", ");
                }
                isStartCreateUpdVals = true;

                //カラム名はキーの'@'を取り除く。VALUES句はそのまま。
                //但し、Valueに@NOWDATEが設定されていたら現在日付を、@NOWTIMEが設定されていたら現在日時を更新
                dPar = (DbParam)d.Value;

                if (dPar.Val != null && dPar.Val.Equals("@NOWDATE"))
                {
                    sbSql.Append(Util.ToString(d.Key).Replace("@", "") + " = CONVERT(varchar(10), GETDATE(), 111)");
                }
                else if (dPar.Val != null && dPar.Val.Equals("@NOWTIME"))
                {
                    sbSql.Append(Util.ToString(d.Key).Replace("@", "") + " = GETDATE()");
                }
                else
                {
                    sbSql.Append(Util.ToString(d.Key).Replace("@", "") + " = " + Util.ToString(d.Key));
                }
            }
            sbSql.Append(" WHERE " + where);

            this._cmd = new SqlCommand(sbSql.ToString(), this._conn);
            this._cmd.Transaction = this._trans;

            // パラメータの設定
            SqlParameter sPar = null;

            // SET句
            foreach (DictionaryEntry d in setDpc.Params)
            {
                dPar = (DbParam)d.Value;

                if (dPar.Val != null && (dPar.Val.Equals("@NOWDATE") || dPar.Val.Equals("@NOWTIME")))
                {
                    // Valueに@NOWDATEや@NOWTIMEが設定されている項目に関してはSQL組み立て時に反映済み
                    continue;
                }

                switch (dPar.Type)
                {
                    case SqlDbType.Char:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                        break;

                    case SqlDbType.VarChar:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                        break;

                    case SqlDbType.Decimal:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                        sPar.Precision = (byte)dPar.FullLength;
                        sPar.Scale = (byte)dPar.DecLength;
                        break;

                    case SqlDbType.DateTime:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                        break;

                    default:
                        // Char,VarChar, Decimal, DateTime以外の対応未定
                        // もし必要があれば
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                        break;
                }

                sPar.Value = dPar.Val == null ? DBNull.Value : dPar.Val;

                // バインドパラメータをcommandに設定
                this._cmd.Parameters.Add(sPar);
            }

            // WHERE句
            foreach (DictionaryEntry d in whereDpc.Params)
            {
                dPar = (DbParam)d.Value;

                switch (dPar.Type)
                {
                    case SqlDbType.Char:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                        break;

                    case SqlDbType.VarChar:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                        break;

                    case SqlDbType.Decimal:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                        sPar.Precision = (byte)dPar.FullLength;
                        sPar.Scale = (byte)dPar.DecLength;
                        break;

                    case SqlDbType.DateTime:
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                        break;

                    default:
                        // Char,VarChar, Decimal, DateTime以外の対応未定
                        // もし必要があれば
                        sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                        break;
                }

                sPar.Value = dPar.Val;

                // バインドパラメータをcommandに設定
                this._cmd.Parameters.Add(sPar);
            }

            // 更新処理を実行
            int affectedRows = this._cmd.ExecuteNonQuery();

            // コマンドオブジェクトは使い捨て(変えるかも)
            this._cmd.Dispose();

            // 更新された行数を返す
            return affectedRows;
        }

        /// <summary>
        /// Deleteを実行します。
        /// </summary>
        /// <param name="table">テーブル名</param>
        /// <param name="where">WHERE句</param>
        /// <param name="whereDpc">WHERE句のバインド変数のコレクション</param>
        /// <returns>更新された行数</returns>
        public int Delete(string table, string where, DbParamCollection whereDpc)
        {
            // SQL文の組み立て
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("DELETE FROM " + table);
            if (!ValChk.IsEmpty(where))
            {
                sbSql.Append(" WHERE " + where);
            }

            this._cmd = new SqlCommand(sbSql.ToString(), this._conn);
            this._cmd.Transaction = this._trans;

            if (whereDpc != null)
            {
                // パラメータの設定
                DbParam dPar = null;
                SqlParameter sPar = null;
                // WHERE句
                foreach (DictionaryEntry d in whereDpc.Params)
                {
                    dPar = (DbParam)d.Value;

                    switch (dPar.Type)
                    {
                        case SqlDbType.Char:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                            break;

                        case SqlDbType.VarChar:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                            break;

                        case SqlDbType.Decimal:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                            sPar.Precision = (byte)dPar.FullLength;
                            sPar.Scale = (byte)dPar.DecLength;
                            break;

                        case SqlDbType.DateTime:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                            break;

                        default:
                            // Char,VarChar, Decimal, DateTime以外の対応未定
                            // もし必要があれば
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                            break;
                    }

                    sPar.Value = dPar.Val;

                    // バインドパラメータをcommandに設定
                    this._cmd.Parameters.Add(sPar);
                }
            }

            // 更新処理を実行
            int affectedRows = this._cmd.ExecuteNonQuery();

            // コマンドオブジェクトは使い捨て(変えるかも)
            this._cmd.Dispose();

            // 更新された行数を返す
            return affectedRows;
        }

        /// <summary>
        /// SQLを用いて更新を実行します。
        /// </summary>
        /// <param name="sql">テーブル名</param>
        /// <param name="dpc">バインド変数のコレクション</param>
        /// <returns>更新された行数</returns>
        /// <remarks>Insert-Select等、複雑な更新のSQLを発行する際に使用してください。</remarks>
        public int ModifyBySql(string sql, DbParamCollection dpc)
        {
            this._cmd = new SqlCommand(sql, this._conn);
            this._cmd.Transaction = this._trans;

            if (dpc != null)
            {
                DbParam dPar = null;
                SqlParameter sPar = null;
                // WHERE句
                foreach (DictionaryEntry d in dpc.Params)
                {
                    dPar = (DbParam)d.Value;

                    switch (dPar.Type)
                    {
                        case SqlDbType.Char:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                            break;

                        case SqlDbType.VarChar:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                            break;

                        case SqlDbType.Decimal:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                            sPar.Precision = (byte)dPar.FullLength;
                            sPar.Scale = (byte)dPar.DecLength;
                            break;

                        case SqlDbType.DateTime:
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type);
                            break;

                        default:
                            // Char,VarChar, Decimal, DateTime以外の対応未定
                            // もし必要があれば
                            sPar = new SqlParameter(Util.ToString(d.Key), dPar.Type, dPar.FullLength);
                            break;
                    }

                    sPar.Value = dPar.Val == null ? DBNull.Value : dPar.Val;
                    // バインドパラメータをcommandに設定
                    this._cmd.Parameters.Add(sPar);
                }
            }

            // 更新処理を実行
            int affectedRows = 0;

            try
            {

                affectedRows = this._cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }

            // コマンドオブジェクトは使い捨て(変えるかも)
            this._cmd.Dispose();

            // 更新された行数を返す
            return affectedRows;
        }

        /// <summary>
        /// トランザクションを開始する
        /// </summary>
        public void BeginTransaction()
        {
            this._trans.Rollback();
            this._trans = this._conn.BeginTransaction();
        }

        /// <summary>
        /// コミットする
        /// </summary>
        public void Commit()
        {
            this._trans.Commit();
            this._trans = this._conn.BeginTransaction();
        }

        /// <summary>
        /// ロールバックする
        /// </summary>
        public void Rollback()
        {
            this._trans.Rollback();
            this._trans = this._conn.BeginTransaction();
        }

        /// <summary>
        /// DB接続を解除する
        /// </summary>
        /// <returns>true:接続成功/false:接続失敗</returns>
        public bool Disconnect()
        {
            try
            {
                //接続解除
                this._conn.Close();
                this._conn.Dispose();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region publicメソッド(ユーティリティ系)
        /// <summary>
        /// コードと名称のリストを取得します。
        /// </summary>
        /// <param name="uInfo">ログイン情報</param>
        /// <param name="tableNm">テーブル名</param>
        /// <param name="sCode">支所コード</param>
        /// <returns>コードと名称のリスト</returns>
        public DataTable GetCdNmList(UserInfo uInfo, string tableNm, string sCode)
        {
            DbParamCollection dpc = new DbParamCollection();
            string[] condition = GetMstCondition(uInfo, tableNm, ref dpc, false, sCode, null);

            // SQL組立
            string cols = condition[0] + " AS " + condition[1] + ", " + condition[2] + " AS " + condition[3];
            DataTable dtList = this.GetDataTableByConditionWithParams(
                cols, condition[6], condition[4], condition[5], dpc);

            return dtList;
        }

        ///// <summary>
        ///// コードを元に名称を取得します。
        ///// </summary>
        ///// <param name="uInfo">ログイン情報</param>
        ///// <param name="tableNm">テーブル名</param>
        ///// <param name="code">元となるコード</param>
        ///// <returns>コードと名称のリスト</returns>
        //public string GetName(UserInfo uInfo, string tableNm, string code)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    string[] condition = GetMstCondition(uInfo, tableNm, ref dpc, true, code);

        //    // SQL組立
        //    string cols = condition[2] + " AS " + condition[3];
        //    DataTable dtList = this.GetDataTableByConditionWithParams(
        //        cols, condition[6], condition[4], condition[5], dpc);

        //    if (dtList.Rows.Count > 0)
        //    {
        //        return Util.ToString(dtList.Rows[0][0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        ///// <summary>
        ///// コードを元に名称を取得します。
        ///// </summary>
        ///// <param name="uInfo">ログイン情報</param>
        ///// <param name="tableNm">テーブル名</param>
        ///// <param name="scode">支所コード</param>
        ///// <param name="code">元となるコード</param>
        ///// <returns>コードと名称のリスト</returns>
        //public string GetName(UserInfo uInfo, string tableNm, string scode, string code) 
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    string[] condition = GetMstCondition(uInfo, tableNm, ref dpc, true, scode, code);

        //    // SQL組立
        //    string cols = condition[2] + " AS " + condition[3];
        //    DataTable dtList = this.GetDataTableByConditionWithParams(
        //        cols, condition[6], condition[4], condition[5], dpc);

        //    if (dtList.Rows.Count > 0)
        //    {
        //        return Util.ToString(dtList.Rows[0][0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}
        /// <summary>
        /// コードを元に名称を取得します。
        /// </summary>
        /// <param name="uInfo">ログイン情報</param>
        /// <param name="tableNm">テーブル名</param>
        /// <param name="scode">支所コード</param>
        /// <param name="code">元となるコード</param>
        /// <param name="kbn">取引先、仕入先の種別区分</param>
        /// <returns>コードと名称のリスト</returns>
        public string GetName(UserInfo uInfo, string tableNm, string scode, string code)
        {
            return GetName(uInfo, tableNm, scode, code, -1);
        }
        public string GetName(UserInfo uInfo, string tableNm, string scode, string code, decimal kbn = -1)
        {
            DbParamCollection dpc = new DbParamCollection();
            //string[] condition = GetMstCondition(uInfo, tableNm, ref dpc, true, scode, code);
            string[] condition = GetMstCondition(uInfo, tableNm, ref dpc, true, scode, code, kbn);

            // SQL組立
            string cols = condition[2] + " AS " + condition[3];
            DataTable dtList = this.GetDataTableByConditionWithParams(
                cols, condition[6], condition[4], condition[5], dpc);

            if (dtList != null && dtList.Rows.Count > 0)
            {
                return Util.ToString(dtList.Rows[0][0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 勘定科目にぶら下がる補助科目名を取得します。
        /// </summary>
        /// <param name="uInfo">ログイン情報</param>
        /// <param name="kanjoKmkCd">勘定科目コード</param>
        /// <param name="code">元となるコード</param>
        /// <returns>コードと名称のリスト</returns>
        public string GetHojoKmkNm(UserInfo uInfo, decimal shishoCd,string kanjoKmkCd, string code)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKmkCd);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, code);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");

            DataTable dtName =
                GetDataTableByConditionWithParams(
                    "HOJO_KAMOKU_NM", "VI_ZM_HOJO_KAMOKU", where.ToString(), dpc);

            if (dtName.Rows.Count > 0)
            {
                name = Util.ToString(dtName.Rows[0]["HOJO_KAMOKU_NM"]);
            }

            return name;
        }

        /// <summary>
        /// 給与システムの部門にぶら下がる部課名を取得します。
        /// </summary>
        /// <param name="uInfo">ログイン情報</param>
        /// <param name="bumonCd">部門コード</param>
        /// <param name="code">元となるコード</param>
        /// <returns>コードと名称のリスト</returns>
        public string GetBukaNm(UserInfo uInfo, string bumonCd, string code)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, bumonCd);
            dpc.SetParam("@BUKA_CD", SqlDbType.Decimal, 4, code);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND BUMON_CD = @BUMON_CD");
            where.Append(" AND BUKA_CD = @BUKA_CD");

            DataTable dtName =
                GetDataTableByConditionWithParams(
                    "BUKA_NM", "TB_KY_BUKA", where.ToString(), dpc);

            if (dtName.Rows.Count > 0)
            {
                name = Util.ToString(dtName.Rows[0]["BUKA_NM"]);
            }

            return name;
        }

        /// <summary>
        /// 仕訳名称名を取得します。
        /// </summary>
        /// <param name="uInfo">ログイン情報</param>
        /// <param name="tekiyoCd">摘要コード</param>
        /// <param name="shiwakeCd">仕訳コード</param>
        /// <returns>仕訳名称</returns>
        public string GetShiwakeNm(UserInfo uInfo, decimal shishoCd, string tekiyoCd, string shiwakeCd)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, tekiyoCd);
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 4, shiwakeCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND TEKIYO_CD = @TEKIYO_CD");
            where.Append(" AND SHIWAKE_CD = @SHIWAKE_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");

            DataTable dtName =
                GetDataTableByConditionWithParams(
                    "SHIWAKE_NM", "VI_ZM_SHIWAKE_JIREI_NM", where.ToString(), dpc);

            if (dtName.Rows.Count > 0)
            {
                name = Util.ToString(dtName.Rows[0]["SHIWAKE_NM"]);
            }

            return name;
        }

        /// <summary>
        /// 銀行支店名を取得します。
        /// </summary>
        /// <param name="ginkoCd">銀行コード</param>
        /// <param name="shitenCd">支店コード</param>
        /// <returns>銀行支店名</returns>
        public string GetGinkoShitenNm(string ginkoCd, string shitenCd)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, ginkoCd);
            dpc.SetParam("@SHITEN_CD", SqlDbType.Decimal, 4, shitenCd);
            StringBuilder where = new StringBuilder();
            where.Append("GINKO_CD = @GINKO_CD");
            where.Append(" AND SHITEN_CD = @SHITEN_CD");

            DataTable dtName =
                GetDataTableByConditionWithParams(
                    "SHITEN_NM", "TB_KY_GINKO_SHITEN", where.ToString(), dpc);

            if (dtName.Rows.Count > 0)
            {
                name = Util.ToString(dtName.Rows[0]["SHITEN_NM"]);
            }

            return name;
        }

        /// <summary>
        /// 給与システムの区分名称を取得します。
        /// </summary>
        /// <param name="shubetsuCd">種別</param>
        /// <param name="kubun">区分</param>
        /// <returns>区分名称</returns>
        public string GetKyuyoKbnNm(string shubetsuCd, string kubun)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@SHUBETSU_CD", SqlDbType.Decimal, 2, shubetsuCd);
            dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, kubun);
            StringBuilder where = new StringBuilder();
            where.Append("SHUBETSU_CD = @SHUBETSU_CD");
            where.Append(" AND KUBUN = @KUBUN");

            DataTable dtName =
                GetDataTableByConditionWithParams(
                    "NM", "VI_KY_KUBUN_NM", where.ToString(), dpc);

            if (dtName.Rows.Count > 0)
            {
                name = Util.ToString(dtName.Rows[0]["NM"]);
            }

            return name;
        }

        /// <summary>
        /// 購買システムの棚番を取得します。
        /// </summary>
        /// <param name="uInfo">ログイン情報</param>
        /// <param name="code">元となるコード</param>
        /// <returns>コードと名称のリスト</returns>
        public string GetTanabanNm(UserInfo uInfo, string code)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
            dpc.SetParam("@TANABAN", SqlDbType.VarChar, 6, code);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND TANABAN IS NOT NULL");
            where.Append(" AND TANABAN = @TANABAN");

            DataTable dtName =
                GetDataTableByConditionWithParams(
                    "TANABAN", "TB_HN_SHOHIN", where.ToString(), dpc);

            if (dtName.Rows.Count > 0)
            {
                name = Util.ToString(dtName.Rows[0]["TANABAN"]);
            }

            return name;
        }

        /// <summary>
        /// 会計期間に紐付く設定を取得します。
        /// </summary>
        /// <param name="kaishaCd">会社コード</param>
        /// <param name="tgtDate">会計期間を取得する対象の日付</param>
        /// <returns>会社情報</returns>
        public DataTable GetKaikeiSettingsByDate(string kaishaCd, DateTime tgtDate)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, kaishaCd);
            dpc.SetParam("@TGT_DATE", SqlDbType.DateTime, tgtDate);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KAIKEI_KIKAN_KAISHIBI <= @TGT_DATE");
            where.Append(" AND KAIKEI_KIKAN_SHURYOBI >= @TGT_DATE");

            DataTable dtKaishaInfo =
                GetDataTableByConditionWithParams(
                    "*", "VI_ZM_KAISHA_JOHO", where.ToString(), dpc);

            return dtKaishaInfo;
        }

        /// <summary>
        /// 会計期間に紐付く設定を取得します。
        /// </summary>
        /// <param name="kaishaCd">会社コード</param>
        /// <param name="kessanKi">決算期</param>
        /// <returns>会社情報</returns>
        public DataTable GetKaikeiSettingsByKessanKi(string kaishaCd, int kessanKi)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, kaishaCd);
            dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 4, kessanKi);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KESSANKI = @KESSANKI");

            DataTable dtKaishaInfo =
                GetDataTableByConditionWithParams(
                    "*", "VI_ZM_KAISHA_JOHO", where.ToString(), dpc);

            return dtKaishaInfo;
        }

        /// <summary>
        /// 販売(購買)システムで管理されている伝票番号を取得する
        /// </summary>
        /// <param name="uInfo">ログイン情報</param>
        /// <param name="shishoCd">支所コード</param>
        /// <param name="denpyoKbn1">伝票区分1</param>
        /// <param name="denpyoKbn2">伝票区分2</param>
        /// <returns>最新の伝票番号</returns>
        public int GetHNDenpyoNo(UserInfo uInfo, decimal shishoCd, int denpyoKbn1, int denpyoKbn2)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  DENPYO_KUBUN1 ");
            sql.Append(" ,DENPYO_KUBUN2 ");
            sql.Append(" ,DENPYO_BANGO ");
            sql.Append(" ,BANGO_ZOBUN ");
            sql.Append(" ,BANGO_SAISHOCHI ");
            sql.Append(" ,BANGO_SAIDAICHI ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_F_DENPYO_BANGO ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD     = @KAISHA_CD ");
            sql.Append("AND KAIKEI_NENDO  = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_KUBUN1 = @DENPYO_KUBUN1 ");
            sql.Append("AND DENPYO_KUBUN2 = @DENPYO_KUBUN2 ");
            sql.Append("AND SHISHO_CD = @SHISHO_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
            // 会計年度の設定を抽出する際、旧システムから移行されたデータへの考慮
            if (uInfo.KaikeiNendo < Constants.SYSTEM_START_YEAR)
            {
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, (Constants.SYSTEM_START_YEAR - 1));
            }
            else
            {
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
            }
            dpc.SetParam("@DENPYO_KUBUN1", SqlDbType.Decimal, 4, denpyoKbn1);
            dpc.SetParam("@DENPYO_KUBUN2", SqlDbType.Decimal, 4, denpyoKbn2);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtResult = GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            int denpyoNo;
            if (dtResult.Rows.Count > 0)
            {
                denpyoNo = Util.ToInt(dtResult.Rows[0]["DENPYO_BANGO"]) + Util.ToInt(dtResult.Rows[0]["BANGO_ZOBUN"]);
            }
            else
            {
                // denpyoNo = Constants.DP_NO_MIN;
                denpyoNo = 0;
            }

            return denpyoNo;
        }

        /// <summary>
        /// 販売(購買)システムで管理されている伝票番号を更新する
        /// </summary>
        /// <param name="uInfo">ログイン情報</param>
        /// <param name="shishoCd">支所コード</param>
        /// <param name="denpyoKbn1">伝票区分1</param>
        /// <param name="denpyoKbn2">伝票区分2</param>
        /// <param name="denpyoNo">伝票番号</param>
        /// <returns></returns>
        public int UpdateHNDenpyoNo(UserInfo uInfo, decimal shishoCd, int denpyoKbn1, int denpyoKbn2, int denpyoNo)
        {
            if (denpyoNo == 0)
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                dpc.SetParam("@DENPYO_KUBUN1", SqlDbType.Decimal, 4, denpyoKbn1);
                dpc.SetParam("@DENPYO_KUBUN2", SqlDbType.Decimal, 4, denpyoKbn2);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                // 年度が変わっても伝票番号の最終番号を取得しないといけない(食堂の場合)
                // 現状 手作業で伝票番号の変更。
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, 1);
                dpc.SetParam("@BANGO_ZOBUN", SqlDbType.Decimal, 4, Constants.DP_NO_ZOBUN);
                dpc.SetParam("@BANGO_SAISHOCHI", SqlDbType.Decimal, 9, Constants.DP_NO_MIN);
                dpc.SetParam("@BANGO_SAIDAICHI", SqlDbType.Decimal, 9, Constants.DP_NO_MAX);
                dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
                dpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

                return Insert("TB_HN_F_DENPYO_BANGO", dpc);
            }
            else
            {
                StringBuilder where = new StringBuilder();
                where.Append("    KAISHA_CD = @KAISHA_CD ");
                where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
                where.Append("AND DENPYO_KUBUN1 = @DENPYO_KUBUN1 ");
                where.Append("AND DENPYO_KUBUN2 = @DENPYO_KUBUN2 ");
                where.Append("AND SHISHO_CD = @SHISHO_CD ");

                DbParamCollection updDpc = new DbParamCollection();
                updDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, denpyoNo);
                updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

                DbParamCollection whereDpc = new DbParamCollection();
                whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                // 会計年度の設定を抽出する際、旧システムから以降されたデータへの考慮
                if (uInfo.KaikeiNendo < Constants.SYSTEM_START_YEAR)
                {
                    whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, (Constants.SYSTEM_START_YEAR - 1));
                }
                else
                {
                    whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                }
                whereDpc.SetParam("@DENPYO_KUBUN1", SqlDbType.Decimal, 4, denpyoKbn1);
                whereDpc.SetParam("@DENPYO_KUBUN2", SqlDbType.Decimal, 4, denpyoKbn2);
                whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

                return Update("TB_HN_F_DENPYO_BANGO", updDpc, where.ToString(), whereDpc);
            }
        }

        /// <summary>
        /// 財務システムで管理されている伝票番号を更新する
        /// </summary>
        /// <param name="uInfo">ログイン情報</param>
        /// <param name="shishoCd">支所コード</param>
        /// <param name="denpyoKbn">伝票区分</param>
        /// <param name="denpyoNo">伝票番号</param>
        /// <returns>更新件数</returns>
        public int UpdateZMDenpyoNo(UserInfo uInfo, decimal shishoCd, int denpyoKbn, int denpyoNo)
        {
            // 現在の伝票番号の最大値を取得する
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  MAX(DENPYO_BANGO) AS MAX_DENPYO_BANGO ");
            sql.Append("FROM ");
            sql.Append("  TB_ZM_DENPYO_BANGO ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD    = @KAISHA_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_KUBUN = @DENPYO_KUBUN ");
            sql.Append("AND SHISHO_CD = @SHISHO_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, denpyoKbn);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtDenpyo = GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            int result = 0;

            // 「現状レコードがなければ」という条件で更新する
            // 但し、更新しようとする伝票番号が発行済みの伝票番号であれば更新はしない
            if (dtDenpyo.Rows.Count == 0 || ValChk.IsEmpty(dtDenpyo.Rows[0]["MAX_DENPYO_BANGO"]))
            {
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, denpyoKbn);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, denpyoNo);
                dpc.SetParam("@BANGO_ZOBUN", SqlDbType.Decimal, 4, Constants.DP_NO_ZOBUN);
                dpc.SetParam("@BANGO_SAISHOCHI", SqlDbType.Decimal, 9, Constants.DP_NO_MIN);
                dpc.SetParam("@BANGO_SAIDAICHI", SqlDbType.Decimal, 9, Constants.DP_NO_MAX);
                dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
                dpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

                result = Insert("TB_ZM_DENPYO_BANGO", dpc);
            }
            else if (Util.ToInt(dtDenpyo.Rows[0]["MAX_DENPYO_BANGO"]) < denpyoNo)
            {
                StringBuilder where = new StringBuilder();
                where.Append("    KAISHA_CD = @KAISHA_CD ");
                where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
                where.Append("AND DENPYO_KUBUN = @DENPYO_KUBUN ");
                where.Append("AND SHISHO_CD = @SHISHO_CD ");

                DbParamCollection updDpc = new DbParamCollection();
                updDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 9, denpyoNo);
                updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

                DbParamCollection whereDpc = new DbParamCollection();
                whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                whereDpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, denpyoKbn);
                whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

                result = Update("TB_ZM_DENPYO_BANGO", updDpc, where.ToString(), whereDpc);
            }

            return result;
        }

        public void DeleteWork(string tbNm, string GUID)
        {
            this.BeginTransaction();
            // 帳票出力用に作成したデータを削除
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, GUID);
            this.Delete(tbNm, "GUID = @GUID", dpc);
            this.Commit();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// SqlDataReaderで取得した構造を元にDataTableを作成します。
        /// </summary>
        /// <param name="reader">SqlDataReaderオブジェクト</param>
        /// <returns>DataTableオブジェクト</returns>
        private DataTable CreateSchemaDataTable(SqlDataReader reader)
        {
            if (reader == null) { return null; }
            if (reader.IsClosed) { return null; }

            DataTable schema = reader.GetSchemaTable();
            DataTable dt = new DataTable();

            foreach (DataRow row in schema.Rows)
            {
                // Column情報を追加してください。
                DataColumn col = new DataColumn();
                col.ColumnName = row["ColumnName"].ToString();
                col.DataType = Type.GetType(row["DataType"].ToString());

                if (col.DataType.Equals(typeof(string)))
                {
                    col.MaxLength = (int)row["ColumnSize"];
                }

                dt.Columns.Add(col);
            }
            return dt;
        }

        //        /// <summary>
        //        /// 名称を取得するためのマスタの条件を取得する
        //        /// </summary>
        //        /// <param name="uInfo">ユーザー情報</param>
        //        /// <param name="tableNm">テーブル名</param>
        //        /// <param name="dpc">バインドパラメータ</param>
        //        /// <param name="isToGetName">名称取得処理用か</param>
        //        /// <param name="code">名称取得処理の元となるコード</param>
        //        /// <returns>
        //        /// マスタの取得条件
        //        /// 0:コード項目のカラム名
        //        /// 1:コード項目のエイリアス名
        //        /// 2:名称項目のカラム名
        //        /// 3:名称項目のエイリアス名
        //        /// 4:抽出のためのWHERE句
        //        /// 5:ORDER BY句
        //        /// </returns>
        //        private string[] GetMstCondition(UserInfo uInfo, string tableNm, ref DbParamCollection dpc,
        //            bool isToGetName, string code)
        //        {
        //            string[] condition = new string[7];

        //            // 引数に指定するテーブル名と実際のテーブル名が異なるケースがあるため、
        //            // 結果の配列として返す(大抵のケースは一致している)
        //            condition[6] = tableNm;

        //            switch (tableNm)
        //            {
        //                #region "共通マスタ"
        //                case "TB_CM_SHISHO":    //Y.TAKADA 2017/10/03 ADD↓
        //                    condition[0] = "SHISHO_CD";
        //                    condition[1] = "支所コード";
        //                    condition[2] = "SHISHO_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;
        //                    //Y.TAKADA 2017/10/03 ADD↑

        //                case "TB_CM_BUMON":
        //                    condition[0] = "BUMON_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "BUMON_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_CM_TANTOSHA":
        //                    condition[0] = "TANTOSHA_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "TANTOSHA_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_CM_TORIHIKISAKI":
        //                    condition[0] = "TORIHIKISAKI_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "TORIHIKISAKI_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;
        //                #endregion
        //                #region "セリ・購買テーブル"
        //                case "TB_HN_GYOHO_MST":
        //                    condition[0] = "GYOHO_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "GYOHO_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                //case "TB_HN_CHIKU_MST":
        //                case "TB_HN_CHIKU_MST":
        //                    condition[0] = "CHIKU_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "CHIKU_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_HN_TEKIYO":
        //                    condition[0] = "TEKIYO_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "TEKIYO_NM";
        //                    condition[3] = "名称";
        //                    //Y.TAKADA EDIT 20171003 ↓
        //                    //condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND ";
        //                    condition[4] += "SHISHO_CD = @SHISHO_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, uInfo.ShishoCd);
        //                    //Y.TAKADA EDIT 20171003 ↑
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;
        //                case "TB_HN_F_TORIHIKI_KUBUN1":
        //                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
        //                    condition[1] = "区分";
        //                    condition[2] = "TORIHIKI_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "DENPYO_KUBUN = 1";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
        //                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
        //                    break;

        //                case "TB_HN_F_TORIHIKI_KUBUN2":
        //                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
        //                    condition[1] = "区分";
        //                    condition[2] = "TORIHIKI_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "DENPYO_KUBUN = 2";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
        //                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
        //                    break;

        //                case "TB_HN_F_TORIHIKI_KUBUN3":
        //                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
        //                    condition[1] = "区分";
        //                    condition[2] = "TORIHIKI_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "DENPYO_KUBUN = 3";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
        //                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
        //                    break;

        //                case "TB_HN_F_TORIHIKI_KUBUN4":
        //                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
        //                    condition[1] = "区分";
        //                    condition[2] = "TORIHIKI_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "DENPYO_KUBUN = 4";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
        //                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
        //                    break;

        //                case "TB_HN_F_TORIHIKI_KUBUN5":
        //                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
        //                    condition[1] = "区分";
        //                    condition[2] = "TORIHIKI_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "DENPYO_KUBUN = 5";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
        //                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
        //                    break;

        //                case "TB_HN_F_TORIHIKI_KUBUN7":
        //                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
        //                    condition[1] = "区分";
        //                    condition[2] = "TORIHIKI_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "DENPYO_KUBUN = 7";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
        //                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
        //                    break;

        //                case "TB_HN_F_TORIHIKI_KUBUN8":
        //                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
        //                    condition[1] = "区分";
        //                    condition[2] = "TORIHIKI_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "DENPYO_KUBUN = 8";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
        //                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
        //                    break;

        //                case "TB_HN_SHOHIN":
        //                    condition[0] = "SHOHIN_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "SHOHIN_NM";
        //                    condition[3] = "名称";
        //                    //Y.TAKADA EDIT 20171003 ↓
        //                    //condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    //dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND ";
        //                    condition[4] += "SHISHO_CD = @SHISHO_CD  ";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, uInfo.ShishoCd);
        //                    //Y.TAKADA EDIT 20171003 ↑
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_HN_GYOSHU":
        //                    condition[0] = "GYOSHU_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "GYOSHU_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND ";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_HN_COMBO_DATA_SEISAN":
        //                    condition[0] = "CD";
        //                    condition[1] = "区分";
        //                    condition[2] = "NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "CD >= 1 AND KUBUN = 24";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "TB_HN_COMBO_DATA";
        //                    break;

        //                case "TB_HN_COMBO_DATA_MIZUAGE":
        //                    condition[0] = "CD";
        //                    condition[1] = "区分";
        //                    condition[2] = "NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "CD >= 1 AND KUBUN = 25";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "TB_HN_COMBO_DATA";
        //                    break;

        //                case "TB_HN_TESURYO_RITSU_MST_NIUKE":
        //                    condition[0] = "URIBA_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "URIBA_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SEISAN_KUBUN = 2";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    //condition[6] = "TB_HN_TESURYO_RITSU_MST";
        //                    condition[6] = "TB_HN_TESURYO_RITSU_MST";
        //                    break;
        //                case "TB_HN_FUNE_NM_MST":
        //                    condition[0] = "FUNE_NM_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "FUNE_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                #endregion
        //                #region "セリ・購買View"
        //                case "VI_HN_FUNANUSHI":
        //                case "VI_HN_NAKAGAI":
        //                case "VI_HN_SHIIRESK":
        //                case "VI_HN_TORIHIKISAKI_JOHO":
        //                case "VI_HN_TOKUISAKI":
        //                    condition[0] = "TORIHIKISAKI_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "TORIHIKISAKI_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "VI_HN_URIAGE_SHIWAKE":
        //                case "VI_HN_SHIIRE_SHIWAKE":
        //                case "VI_HN_SHOKUDO_URIAGE":
        //                case "VI_HN_SHOKUDO_SHIIRE":
        //                    condition[0] = "SHIWAKE_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "SHIWAKE_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "VI_HN_SHOHIN_KBN1":
        //                case "VI_HN_SHOHIN_KBN2":
        //                case "VI_HN_SHOHIN_KBN3":
        //                case "VI_HN_SHOHIN_KBN4":
        //                case "VI_HN_SHOHIN_KBN5":
        //                    // 20150105 kinjo-Add:名護版カスタマイズ↓
        //                case "VI_HN_SHOHIN_KBN6":
        //                case "VI_HN_SHOHIN_KBN7":
        //                case "VI_HN_SHOHIN_KBN8":
        //                    // 20150105 kinjo-Add:名護版カスタマイズ↑

        //                    condition[0] = "SHOHIN_KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "SHOHIN_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;
        //                case "VI_HN_SHOHIN":
        //                    condition[0] = "SHOHIN_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "SHOHIN_NM";
        //                    condition[3] = "名称";
        //                    //Y.TAKADA EDIT 20171003 ↓
        //                    //condition[4] = "KAISHA_CD = @KAISHA_CD AND BARCODE1 = 0";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND ";
        //                    condition[4] += "SHISHO_CD = @SHISHO_CD ";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, uInfo.ShishoCd);
        //                    //Y.TAKADA EDIT 20171003 ↑
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "VI_HN_SHOHIN2":
        //                    condition[0] = "SHOHIN_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "SHOHIN_NM";
        //                    condition[3] = "名称";
        //                    //Y.TAKADA EDIT 20171003 ↓
        //                    //condition[4] = "KAISHA_CD = @KAISHA_CD AND SHOHIN_KUBUN5 <> 1 AND (BARCODE1 <> 999 AND BARCODE1 <> 888)";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND ";
        //                    condition[4] += "SHISHO_CD = @SHISHO_CD ";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "VI_HN_SHOHIN";
        //                    break;

        //                case "VI_HN_SHOHIN_SHIIRE":
        //                    condition[0] = "SHOHIN_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "SHOHIN_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND (BARCODE1 IS NULL OR BARCODE1 != 999)";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "VI_HN_SHOHIN";
        //                    break;

        //                case "VI_HN_SHOHIN_GYOSHU":
        //                    //condition[0] = "SHOHIN_CD";
        //                    condition[0] = "GYOSHU_CD";
        //                    condition[1] = "コード";
        //                    //condition[2] = "SHOHIN_NM";
        //                    condition[2] = "GYOSHU_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    //condition[6] = "VI_HN_SHOHIN";
        //                    condition[6] = "TB_HN_GYOSHU";
        //                    break;
        //                case "VI_HN_HANBAI_KUBUN_NM":
        //                    condition[0] = "KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 2";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "VI_HN_HANBAI_KUBUN_NM";
        //                    break;

        //                case "VI_HN_HANBAI_KUBUN_NM2":
        //                    condition[0] = "KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 1";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "VI_HN_HANBAI_KUBUN_NM";
        //                    break;

        //                case "VI_HN_HANBAI_KUBUN_NM3":
        //                    condition[0] = "KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 1 AND KUBUN_SHUBETSU = 7";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "VI_HN_HANBAI_KUBUN_NM";
        //                    break;

        //                case "VI_HN_HANBAI_KUBUN_NM4":
        //                    condition[0] = "KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 7";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    condition[6] = "VI_HN_HANBAI_KUBUN_NM";
        //                    break;


        //                #endregion
        //                #region "財務"
        //                case "TB_ZM_KANJO_KAMOKU":
        //                    condition[0] = "KANJO_KAMOKU_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "KANJO_KAMOKU_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
        //                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_F_ZEI_KUBUN":
        //                    condition[0] = "ZEI_KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "ZEI_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 2, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_F_JIGYO_KUBUN":
        //                    condition[0] = "JIGYO_KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "JIGYO_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO":
        //                    condition[0] = "SHOHIZEI_NYURYOKU_HOHO";
        //                    condition[1] = "区分";
        //                    condition[2] = "SHOHIZEI_NYURYOKU_HOHO_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_F_KAZEI_HOHO":
        //                    condition[0] = "KAZEI_HOHO";
        //                    condition[1] = "区分";
        //                    condition[2] = "KAZEI_HOHO_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_F_KOJO_HOHO":
        //                    condition[0] = "KOJO_HOHO";
        //                    condition[1] = "区分";
        //                    condition[2] = "KOJO_HOHO_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_F_TAISHAKU_KUBUN":
        //                    condition[0] = "TAISHAKU_KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "TAISHAKU_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_F_HASU_SHORI":
        //                    condition[0] = "HASU_SHORI";
        //                    condition[1] = "区分";
        //                    condition[2] = "HASU_SHORI_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_F_KAMOKU_KUBUN":
        //                    condition[0] = "KAMOKU_KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "KAMOKU_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;
        //                case "TB_ZM_TEKIYO":
        //                    condition[0] = "TEKIYO_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "TEKIYO_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
        //                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_KOJI":
        //                    condition[0] = "KOJI_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "KOJI_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
        //                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_ZM_KOSHU":
        //                    condition[0] = "KOSHU_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "KOSHU_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
        //                    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
        //                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;
        //                case "TB_ZM_F_KAMOKU_BUNRUI":
        //                    condition[0] = "KAMOKU_BUNRUI_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "KAMOKU_BUNRUI_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                #endregion
        //                #region "給与"
        //                case "TB_KY_SHAIN_JOHO":
        //                    condition[0] = "SHAIN_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "SHAIN_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 6, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_BUMON":
        //                    condition[0] = "BUMON_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "BUMON_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_YAKUSHOKU":
        //                    condition[0] = "YAKUSHOKU_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "YAKUSHOKU_NM";
        //                    condition[3] = "名称";
        //                    condition[4] = "KAISHA_CD = @KAISHA_CD";
        //                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
        //                    if (isToGetName)
        //                    {
        //                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_SHICHOSON":
        //                    condition[0] = "SHICHOSON_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "SHICHOSON_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 6, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_GINKO":
        //                    condition[0] = "GINKO_CD";
        //                    condition[1] = "コード";
        //                    condition[2] = "GINKO_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_F_KOYO_HOKEN_KEISAN_HOHO":
        //                    condition[0] = "KOYO_HOKEN_KEISAN_HOHO";
        //                    condition[1] = "コード";
        //                    condition[2] = "KOYO_HOKEN_KEISAN_HOHO_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_F_KOYO_HOKEN_GYOSHU":
        //                    condition[0] = "KOYO_HOKEN_GYOSHU";
        //                    condition[1] = "コード";
        //                    condition[2] = "KOYO_HOKEN_GYOSHU_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_F_SHOTOKUZEI_KEISAN_HOHO":
        //                    condition[0] = "SHOTOKUZEI_KEISAN_HOHO";
        //                    condition[1] = "コード";
        //                    condition[2] = "SHOTOKUZEI_KEISAN_HOHO_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_F_KYUYO_KEITAI":
        //                    condition[0] = "KYUYO_KEITAI";
        //                    condition[1] = "コード";
        //                    condition[2] = "KYUYO_KEITAI_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_F_HONNIN_KUBUN1":
        //                    condition[0] = "HONNIN_KUBUN1";
        //                    condition[1] = "区分";
        //                    condition[2] = "HONNIN_KUBUN1_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_F_HONNIN_KUBUN2":
        //                    condition[0] = "HONNIN_KUBUN2";
        //                    condition[1] = "区分";
        //                    condition[2] = "HONNIN_KUBUN2_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_F_HONNIN_KUBUN3":
        //                    condition[0] = "HONNIN_KUBUN3";
        //                    condition[1] = "区分";
        //                    condition[2] = "HONNIN_KUBUN3_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;

        //                case "TB_KY_F_HAIGUSHA_KUBUN":
        //                    condition[0] = "HAIGUSHA_KUBUN";
        //                    condition[1] = "区分";
        //                    condition[2] = "HAIGUSHA_KUBUN_NM";
        //                    condition[3] = "名称";
        //                    if (isToGetName)
        //                    {
        //                        condition[4] = condition[0] + " = @" + condition[0];
        //                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
        //                    }
        //                    condition[5] = condition[0];
        //                    break;
        //#endregion
        //                default:
        //                    break;
        //            }

        //            return condition;
        //        }

        /// <summary>
        /// 支所コードを指定して名称を取得するためのマスタの条件を取得する
        /// </summary>
        /// <param name="uInfo">ユーザー情報</param>
        /// <param name="tableNm">テーブル名</param>
        /// <param name="dpc">バインドパラメータ</param>
        /// <param name="isToGetName">名称取得処理用か</param>
        /// <param name="scode">支所コード</param>
        /// <param name="code">名称取得処理の元となるコード</param>
        /// <returns>
        /// マスタの取得条件
        /// 0:コード項目のカラム名
        /// 1:コード項目のエイリアス名
        /// 2:名称項目のカラム名
        /// 3:名称項目のエイリアス名
        /// 4:抽出のためのWHERE句
        /// 5:ORDER BY句
        /// </returns>
        //private string[] GetMstCondition(UserInfo uInfo, string tableNm, ref DbParamCollection dpc,
        //    bool isToGetName, string scode, string code)
        private string[] GetMstCondition(UserInfo uInfo, string tableNm, ref DbParamCollection dpc,
            bool isToGetName, string scode, string code)
        {
            return GetMstCondition(uInfo, tableNm, ref dpc, isToGetName, scode, code, -1);
        }
            private string[] GetMstCondition(UserInfo uInfo, string tableNm, ref DbParamCollection dpc,
            bool isToGetName, string scode, string code, decimal kbn = -1)
        {
            string[] condition = new string[7];

            // 引数に指定するテーブル名と実際のテーブル名が異なるケースがあるため、
            // 結果の配列として返す(大抵のケースは一致している)
            condition[6] = tableNm;

            switch (tableNm)
            {
                #region "共通マスタ"
                case "TB_CM_SHISHO":    //Y.TAKADA 2017/10/03 ADD↓
                    condition[0] = "SHISHO_CD";
                    condition[1] = "支所コード";
                    condition[2] = "SHISHO_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;
                //Y.TAKADA 2017/10/03 ADD↑

                case "TB_CM_BUMON":
                    condition[0] = "BUMON_CD";
                    condition[1] = "コード";
                    condition[2] = "BUMON_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_CM_TANTOSHA":
                    condition[0] = "TANTOSHA_CD";
                    condition[1] = "コード";
                    condition[2] = "TANTOSHA_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_CM_TORIHIKISAKI":
                    condition[0] = "TORIHIKISAKI_CD";
                    condition[1] = "コード";
                    condition[2] = "TORIHIKISAKI_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;
                #endregion
                #region "セリ・購買テーブル"
                case "TB_HN_GYOHO_MST":
                    condition[0] = "GYOHO_CD";
                    condition[1] = "コード";
                    condition[2] = "GYOHO_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_HN_CHIKU_MST":
                    condition[0] = "CHIKU_CD";
                    condition[1] = "コード";
                    condition[2] = "CHIKU_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_HN_TEKIYO":
                    condition[0] = "TEKIYO_CD";
                    condition[1] = "コード";
                    condition[2] = "TEKIYO_NM";
                    condition[3] = "名称";
                    //Y.TAKADA EDIT 20171003 ↓
                    //condition[4] = "KAISHA_CD = @KAISHA_CD";
                    condition[4] = "KAISHA_CD = @KAISHA_CD AND ";
                    condition[4] += "SHISHO_CD = @SHISHO_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                    //Y.TAKADA EDIT 20171003 ↑
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_HN_F_TORIHIKI_KUBUN1":
                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                    condition[1] = "区分";
                    condition[2] = "TORIHIKI_KUBUN_NM";
                    condition[3] = "名称";
                    condition[4] = "DENPYO_KUBUN = 1";
                    if (isToGetName)
                    {
                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                    break;

                case "TB_HN_F_TORIHIKI_KUBUN2":
                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                    condition[1] = "区分";
                    condition[2] = "TORIHIKI_KUBUN_NM";
                    condition[3] = "名称";
                    condition[4] = "DENPYO_KUBUN = 2";
                    if (isToGetName)
                    {
                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                    break;

                case "TB_HN_F_TORIHIKI_KUBUN3":
                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                    condition[1] = "区分";
                    condition[2] = "TORIHIKI_KUBUN_NM";
                    condition[3] = "名称";
                    condition[4] = "DENPYO_KUBUN = 3";
                    if (isToGetName)
                    {
                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                    break;

                case "TB_HN_F_TORIHIKI_KUBUN4":
                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                    condition[1] = "区分";
                    condition[2] = "TORIHIKI_KUBUN_NM";
                    condition[3] = "名称";
                    condition[4] = "DENPYO_KUBUN = 4";
                    if (isToGetName)
                    {
                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                    break;

                case "TB_HN_F_TORIHIKI_KUBUN5":
                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                    condition[1] = "区分";
                    condition[2] = "TORIHIKI_KUBUN_NM";
                    condition[3] = "名称";
                    condition[4] = "DENPYO_KUBUN = 5";
                    if (isToGetName)
                    {
                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                    break;

                case "TB_HN_F_TORIHIKI_KUBUN7":
                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                    condition[1] = "区分";
                    condition[2] = "TORIHIKI_KUBUN_NM";
                    condition[3] = "名称";
                    condition[4] = "DENPYO_KUBUN = 7";
                    if (isToGetName)
                    {
                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                    break;

                case "TB_HN_F_TORIHIKI_KUBUN8":
                    condition[0] = "CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4))";
                    condition[1] = "区分";
                    condition[2] = "TORIHIKI_KUBUN_NM";
                    condition[3] = "名称";
                    condition[4] = "DENPYO_KUBUN = 8";
                    if (isToGetName)
                    {
                        condition[4] += " AND CAST(TORIHIKI_KUBUN1 * 10 + TORIHIKI_KUBUN2 AS DECIMAL(4)) = @KUBUN";
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_F_TORIHIKI_KUBUN";
                    break;

                case "TB_HN_SHOHIN":
                    condition[0] = "SHOHIN_CD";
                    condition[1] = "コード";
                    condition[2] = "SHOHIN_NM";
                    condition[3] = "名称";
                    //Y.TAKADA EDIT 20171003 ↓
                    //condition[4] = "KAISHA_CD = @KAISHA_CD";
                    //dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    condition[4] = "KAISHA_CD = @KAISHA_CD AND ";
                    condition[4] += "SHISHO_CD = @SHISHO_CD  ";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                    //Y.TAKADA EDIT 20171003 ↑
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                    }
                    condition[5] = condition[0];
                    break;
                case "TB_HN_COMBO_DATA_SEISAN":
                    condition[0] = "CD";
                    condition[1] = "区分";
                    condition[2] = "NM";
                    condition[3] = "名称";
                    condition[4] = "CD >= 1 AND KUBUN = 24";
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_COMBO_DATA";
                    break;

                case "TB_HN_COMBO_DATA_MIZUAGE":
                    condition[0] = "CD";
                    condition[1] = "区分";
                    condition[2] = "NM";
                    condition[3] = "名称";
                    condition[4] = "CD >= 1 AND KUBUN = 25";
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_COMBO_DATA";
                    break;

                case "TB_HN_TESURYO_RITSU_MST_NIUKE":
                    condition[0] = "URIBA_CD";
                    condition[1] = "コード";
                    condition[2] = "URIBA_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SEISAN_KUBUN = 2";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    //condition[6] = "TB_HN_TESURYO_RITSU_MST";
                    condition[6] = "TB_HN_TESURYO_RITSU_MST";
                    break;
                case "TB_HN_FUNE_NM_MST":
                    condition[0] = "FUNE_NM_CD";
                    condition[1] = "コード";
                    condition[2] = "FUNE_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "TB_HN_FUNE_NM_MST";
                    break;

                #endregion
                #region "セリ・購買View"
                case "VI_HN_FUNANUSHI":
                case "VI_HN_NAKAGAI":
                case "VI_HN_SHIIRESK":
                //case "VI_HN_TORIHIKISAKI_JOHO":
                case "VI_HN_TOKUISAKI":
                    condition[0] = "TORIHIKISAKI_CD";
                    condition[1] = "コード";
                    condition[2] = "TORIHIKISAKI_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;
                case "VI_HN_TORIHIKISAKI_JOHO":
                    condition[0] = "TORIHIKISAKI_CD";
                    condition[1] = "コード";
                    condition[2] = "TORIHIKISAKI_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETU_KUBUN = @SHUBETU_KUBUN";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    dpc.SetParam("@SHUBETU_KUBUN", SqlDbType.Decimal, 4, kbn);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "VI_HN_URIAGE_SHIWAKE":
                case "VI_HN_SHIIRE_SHIWAKE":
                case "VI_HN_SHOKUDO_URIAGE":
                case "VI_HN_SHOKUDO_SHIIRE":
                    condition[0] = "SHIWAKE_CD";
                    condition[1] = "コード";
                    condition[2] = "SHIWAKE_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "VI_HN_SHOHIN_KBN1":
                case "VI_HN_SHOHIN_KBN2":
                case "VI_HN_SHOHIN_KBN3":
                case "VI_HN_SHOHIN_KBN4":
                case "VI_HN_SHOHIN_KBN5":
                // 20150105 kinjo-Add:名護版カスタマイズ↓
                case "VI_HN_SHOHIN_KBN6":
                case "VI_HN_SHOHIN_KBN7":
                case "VI_HN_SHOHIN_KBN8":
                    // 20150105 kinjo-Add:名護版カスタマイズ↑

                    condition[0] = "SHOHIN_KUBUN";
                    condition[1] = "区分";
                    condition[2] = "SHOHIN_KUBUN_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;
                case "VI_HN_SHOHIN":
                    condition[0] = "SHOHIN_CD";
                    condition[1] = "コード";
                    condition[2] = "SHOHIN_NM";
                    condition[3] = "名称";
                    //Y.TAKADA EDIT 20171003 ↓
                    //condition[4] = "KAISHA_CD = @KAISHA_CD AND BARCODE1 = 0";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    condition[4] += " AND SHISHO_CD = @SHISHO_CD ";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                    //Y.TAKADA EDIT 20171003 ↑
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "VI_HN_SHOHIN2":
                    condition[0] = "SHOHIN_CD";
                    condition[1] = "コード";
                    condition[2] = "SHOHIN_NM";
                    condition[3] = "名称";
                    //Y.TAKADA EDIT 20171003 ↓
                    //condition[4] = "KAISHA_CD = @KAISHA_CD AND SHOHIN_KUBUN5 <> 1 AND (BARCODE1 <> 999 AND BARCODE1 <> 888)";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    condition[4] += " AND SHISHO_CD = @SHISHO_CD ";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "VI_HN_SHOHIN";
                    break;

                case "VI_HN_SHOHIN_SHIIRE":
                    condition[0] = "SHOHIN_CD";
                    condition[1] = "コード";
                    condition[2] = "SHOHIN_NM";
                    condition[3] = "名称";
                    //condition[4] = "KAISHA_CD = @KAISHA_CD AND (BARCODE1 IS NULL OR BARCODE1 != 999)";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    condition[4]+= " AND SHISHO_CD = @SHISHO_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "VI_HN_SHOHIN";
                    break;

                case "VI_HN_GYOSHU":
                    condition[0] = "GYOSHU_CD";
                    condition[1] = "コード";
                    condition[2] = "GYOSHU_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    condition[4] += " AND SHISHO_CD = @SHISHO_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "VI_HN_GYOSHU";
                    break;
                case "VI_HN_HANBAI_KUBUN_NM":
                    condition[0] = "KUBUN";
                    condition[1] = "区分";
                    condition[2] = "NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 2";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "VI_HN_HANBAI_KUBUN_NM";
                    break;

                case "VI_HN_HANBAI_KUBUN_NM2":
                    condition[0] = "KUBUN";
                    condition[1] = "区分";
                    condition[2] = "NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 1";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "VI_HN_HANBAI_KUBUN_NM";
                    break;

                case "VI_HN_HANBAI_KUBUN_NM3":
                    condition[0] = "KUBUN";
                    condition[1] = "区分";
                    condition[2] = "NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 1 AND KUBUN_SHUBETSU = 7";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "VI_HN_HANBAI_KUBUN_NM";
                    break;

                case "VI_HN_HANBAI_KUBUN_NM4":
                    condition[0] = "KUBUN";
                    condition[1] = "区分";
                    condition[2] = "NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 7";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                    }
                    condition[5] = condition[0];
                    condition[6] = "VI_HN_HANBAI_KUBUN_NM";
                    break;


                #endregion
                #region "財務"
                case "TB_ZM_KANJO_KAMOKU":
                    condition[0] = "KANJO_KAMOKU_CD";
                    condition[1] = "コード";
                    condition[2] = "KANJO_KAMOKU_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 6, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_ZM_F_ZEI_KUBUN":
                    condition[0] = "ZEI_KUBUN";
                    condition[1] = "区分";
                    condition[2] = "ZEI_KUBUN_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 2, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_ZM_F_JIGYO_KUBUN":
                    condition[0] = "JIGYO_KUBUN";
                    condition[1] = "区分";
                    condition[2] = "JIGYO_KUBUN_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO":
                    condition[0] = "SHOHIZEI_NYURYOKU_HOHO";
                    condition[1] = "区分";
                    condition[2] = "SHOHIZEI_NYURYOKU_HOHO_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_ZM_F_KAZEI_HOHO":
                    condition[0] = "KAZEI_HOHO";
                    condition[1] = "区分";
                    condition[2] = "KAZEI_HOHO_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_ZM_F_KOJO_HOHO":
                    condition[0] = "KOJO_HOHO";
                    condition[1] = "区分";
                    condition[2] = "KOJO_HOHO_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_ZM_F_TAISHAKU_KUBUN":
                    condition[0] = "TAISHAKU_KUBUN";
                    condition[1] = "区分";
                    condition[2] = "TAISHAKU_KUBUN_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_ZM_F_HASU_SHORI":
                    condition[0] = "HASU_SHORI";
                    condition[1] = "区分";
                    condition[2] = "HASU_SHORI_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_ZM_F_KAMOKU_KUBUN":
                    condition[0] = "KAMOKU_KUBUN";
                    condition[1] = "区分";
                    condition[2] = "KAMOKU_KUBUN_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;
                case "TB_ZM_TEKIYO":
                    condition[0] = "TEKIYO_CD";
                    condition[1] = "コード";
                    condition[2] = "TEKIYO_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    condition[4] += " AND SHISHO_CD = @SHISHO_CD";
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, scode);
                    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                //case "TB_ZM_KOJI":
                //    condition[0] = "KOJI_CD";
                //    condition[1] = "コード";
                //    condition[2] = "KOJI_NM";
                //    condition[3] = "名称";
                //    condition[4] = "KAISHA_CD = @KAISHA_CD";
                //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                //    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                //    if (isToGetName)
                //    {
                //        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                //        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                //    }
                //    condition[5] = condition[0];
                //    break;

                //case "TB_ZM_KOSHU":
                //    condition[0] = "KOSHU_CD";
                //    condition[1] = "コード";
                //    condition[2] = "KOSHU_NM";
                //    condition[3] = "名称";
                //    condition[4] = "KAISHA_CD = @KAISHA_CD";
                //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                //    condition[4] += " AND KAIKEI_NENDO = @KAIKEI_NENDO";
                //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, uInfo.KaikeiNendo);
                //    if (isToGetName)
                //    {
                //        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                //        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                //    }
                //    condition[5] = condition[0];
                //    break;

                case "TB_ZM_F_KAMOKU_BUNRUI":
                    condition[0] = "KAMOKU_BUNRUI_CD";
                    condition[1] = "コード";
                    condition[2] = "KAMOKU_BUNRUI_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 5, code);
                    }
                    condition[5] = condition[0];
                    break;

                #endregion
                #region "給与"
                case "TB_KY_SHAIN_JOHO":
                    condition[0] = "SHAIN_CD";
                    condition[1] = "コード";
                    condition[2] = "SHAIN_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 6, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_BUMON":
                    condition[0] = "BUMON_CD";
                    condition[1] = "コード";
                    condition[2] = "BUMON_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_YAKUSHOKU":
                    condition[0] = "YAKUSHOKU_CD";
                    condition[1] = "コード";
                    condition[2] = "YAKUSHOKU_NM";
                    condition[3] = "名称";
                    condition[4] = "KAISHA_CD = @KAISHA_CD";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] += " AND " + condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_SHICHOSON":
                    condition[0] = "SHICHOSON_CD";
                    condition[1] = "コード";
                    condition[2] = "SHICHOSON_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 6, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_GINKO":
                    condition[0] = "GINKO_CD";
                    condition[1] = "コード";
                    condition[2] = "GINKO_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_F_KOYO_HOKEN_KEISAN_HOHO":
                    condition[0] = "KOYO_HOKEN_KEISAN_HOHO";
                    condition[1] = "コード";
                    condition[2] = "KOYO_HOKEN_KEISAN_HOHO_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_F_KOYO_HOKEN_GYOSHU":
                    condition[0] = "KOYO_HOKEN_GYOSHU";
                    condition[1] = "コード";
                    condition[2] = "KOYO_HOKEN_GYOSHU_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_F_SHOTOKUZEI_KEISAN_HOHO":
                    condition[0] = "SHOTOKUZEI_KEISAN_HOHO";
                    condition[1] = "コード";
                    condition[2] = "SHOTOKUZEI_KEISAN_HOHO_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_F_KYUYO_KEITAI":
                    condition[0] = "KYUYO_KEITAI";
                    condition[1] = "コード";
                    condition[2] = "KYUYO_KEITAI_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 1, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_F_HONNIN_KUBUN1":
                    condition[0] = "HONNIN_KUBUN1";
                    condition[1] = "区分";
                    condition[2] = "HONNIN_KUBUN1_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_F_HONNIN_KUBUN2":
                    condition[0] = "HONNIN_KUBUN2";
                    condition[1] = "区分";
                    condition[2] = "HONNIN_KUBUN2_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_F_HONNIN_KUBUN3":
                    condition[0] = "HONNIN_KUBUN3";
                    condition[1] = "区分";
                    condition[2] = "HONNIN_KUBUN3_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;

                case "TB_KY_F_HAIGUSHA_KUBUN":
                    condition[0] = "HAIGUSHA_KUBUN";
                    condition[1] = "区分";
                    condition[2] = "HAIGUSHA_KUBUN_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;
                #endregion
                #region
                case "TB_EDI_SHOHIN":
                    condition[0] = "SHOHIN_CD";
                    condition[1] = "コード";
                    condition[2] = "SHOHIN_NM";
                    condition[3] = "名称";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 13, code);
                    }
                    condition[5] = condition[0];
                    break;
                case "TB_EDI_TORIHIKISAKI":
                    condition[0] = "TORIHIKISAKI_CD";
                    condition[1] = "コード";
                    condition[2] = "TORIHIKISAKI_NM";
                    condition[3] = "名称";
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, uInfo.KaishaCd);
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 8, code);
                    }
                    condition[5] = condition[0];
                    break;
                case "VI_EDI_SHOHIN_KUBUN1":
                case "VI_EDI_SHOHIN_KUBUN2":
                case "VI_EDI_SHOHIN_KUBUN3":
                case "VI_EDI_SHOHIN_KUBUN4":
                case "VI_EDI_SHOHIN_KUBUN5":
                    condition[0] = "SHOHIN_KUBUN";
                    condition[1] = "区分";
                    condition[2] = "SHOHIN_KUBUN_NM";
                    condition[3] = "名称";
                    if (isToGetName)
                    {
                        condition[4] = condition[0] + " = @" + condition[0];
                        dpc.SetParam("@" + condition[0], SqlDbType.Decimal, 4, code);
                    }
                    condition[5] = condition[0];
                    break;
                #endregion
                default:
                    break;
            }

            return condition;
        }

        #endregion
    }
}
