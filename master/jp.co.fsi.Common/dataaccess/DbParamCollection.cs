﻿using System.Collections;
using System.Data;

namespace jp.co.fsi.common.dataaccess
{
    /// <summary>
    /// DBのバインドパラメータのコレクションを管理するクラス
    /// </summary>
    public class DbParamCollection
    {
        #region プロパティ
        private Hashtable _htParams;
        /// <summary>
        /// パラメータのコレクション
        /// </summary>
        public Hashtable Params
        {
            get
            {
                return this._htParams;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DbParamCollection()
        {
            this._htParams = new Hashtable();
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// バインドパラメータをセットします。
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="type">型</param>
        /// <param name="val">値</param>
        /// <remarks>Date型など、桁数を必要としない場合に用います。</remarks>
        public void SetParam(string key, SqlDbType type, object val)
        {
            SetParam(key, type, 0, val);
        }

        /// <summary>
        /// バインドパラメータをセットします。
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="type">型</param>
        /// <param name="length">桁数</param>
        /// <param name="val">値</param>
        /// <remarks>
        /// 整数部のみを扱うDecimal型、Varchar型、Char型など、桁数を必要とするが
        /// 小数部を必要としない場合に用います。
        /// </remarks>
        public void SetParam(string key, SqlDbType type, int length, object val)
        {
            SetParam(key, type, length, 0, val);
        }

        /// <summary>
        /// バインドパラメータをセットします。
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="type">型</param>
        /// <param name="fullLength">全体の桁数</param>
        /// <param name="decLength">小数部の桁数</param>
        /// <param name="val">値</param>
        /// <remarks>
        /// Decimal型などの数値型で、小数部を必要とする場合に用います。
        /// </remarks>
        public void SetParam(string key, SqlDbType type, int fullLength, int decLength, object val)
        {
            DbParam param = new DbParam();
            param.Type = type;
            param.FullLength = fullLength;
            param.DecLength = decLength;
            param.Val = val;
            this._htParams.Add(key, param);
        }
        #endregion
    }
}
