﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jp.co.fsi.common
{

	/// <summary>
	/// タブインデックス順にカーソル移動させる
	/// </summary>
	public interface IHasHierarchicalTabIndices:IWin32Window,IEnumerable<int>,IComparable,IComparable<IHasHierarchicalTabIndices>
	{

		IEnumerable<int> HasHierarchicalTabIndices { get; }


	}
}
