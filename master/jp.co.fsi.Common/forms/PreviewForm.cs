﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.report;

namespace jp.co.fsi.common.forms
{
    /// <summary>
    /// 帳票プレビュー用フォーム
    /// </summary>
    public partial class PreviewForm : Form
    {
        #region private変数
        /// <summary>
        /// 表示する帳票クラスのオブジェクト
        /// </summary>
		private BaseReport _rpt;
        private BaseReport _rpt2;
		//private object _rpt;
		//private object _rpt2;

		/// <summary>
		/// 表示対象のデータのキー(GUID)
		/// </summary>
		private string _guid;

        /// <summary>
        /// 帳票マージモード
        /// 1 後方追加 2 交互追加
        /// </summary>
        private string MergeMode;

		/// <summary>
		/// 外部設定のドキュメントオブジェクト
		/// 外部で帳票結合した場合は内部でRunせず本ドキュメントから表示
		/// </summary>
		private GrapeCity.ActiveReports.Document.SectionDocument _doc;
		#endregion

		#region publicメソッド
		/// <summary>
		/// 外部設定のドキュメントオブジェクトの設定
		/// </summary>
		/// <param name="doc">ドキュメントオブジェクト</param>
		public void setDocument(GrapeCity.ActiveReports.Document.SectionDocument doc)
		{
			this._doc = doc;
		}
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PreviewForm(BaseReport rpt, string guid)
		{
			this._rpt = rpt;
            this._guid = guid;
			this._doc = null;
            this._rpt2 = null;
            this.MergeMode = string.Empty;

            InitializeComponent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PreviewForm(BaseReport rpt, BaseReport rpt2, string guid)
        {
            this._rpt = rpt;
            this._guid = guid;
			this._doc = null;

            this._rpt2 = rpt2;

            this.MergeMode = string.Empty;

            InitializeComponent();
        }

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public PreviewForm(BaseReport rpt, BaseReport rpt2, string guid, string MergeMode)

        {
            this._rpt = rpt;
            this._guid = guid;
			this._doc = null;

            this._rpt2 = rpt2;

            this.MergeMode = MergeMode;

            InitializeComponent();
        }


        #endregion

        #region イベント
        /// <summary>
        /// フォーム起動時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private ToolStripButton tsbPrint = new System.Windows.Forms.ToolStripButton();

        private void PreviewForm_Load(object sender, EventArgs e)
        {

            // 帳票オブジェクトはインスタンス化されてる前提
            this.Text = this._rpt.Document.Name;

            this.Cursor = Cursors.WaitCursor;
            // 外部設定のドキュメントオブジェクト使う使わない

            if (this._doc == null)
            {
                this._rpt.Run(false);
                vwMain.Document = this._rpt.Document;

                if (null != this._rpt2)
                {
                    this._rpt.Run(false);
                    this._rpt2.Run(false);

                    int idxPage = 1;

                    // 後方追加の場合
                    if ("1" == MergeMode)
                    {

                        this._rpt.Document.Pages.AddRange(this._rpt2.Document.Pages);

                        var font = new System.Drawing.Font("ＭＳ 明朝", 10);


                        Single x = _rpt.PageSettings.Margins.Left + 5.89F;
                        Single y = _rpt.PageSettings.Margins.Top;
                        Single width = 7.38F;
                        Single height = 0.19F;
                        var drawRect = new System.Drawing.RectangleF(x, y, width, height);

                        for (int i = 0; i < this._rpt.Document.Pages.Count; i++)
                        {
                            this._rpt.Document.Pages[i].ForeColor = System.Drawing.Color.Black;
                            this._rpt.Document.Pages[i].Font = font;
                            this._rpt.Document.Pages[i].TextAlignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                            this._rpt.Document.Pages[i].VerticalTextAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Middle;

                            this._rpt.Document.Pages[i].DrawText((i + 1).ToString() + "頁", drawRect);

                        }

                    }
                    else
                    {
                        foreach (var doc in this._rpt2.Document.Pages)
                        {
                            this._rpt.Document.Pages.Insert(idxPage, doc);

                            idxPage += 2;

                        }

                    }
                    vwMain.Document = this._rpt.Document;
                }
            }
            else
            {
                vwMain.Document = this._doc;
                this._doc = null;
            }
            this.Cursor = Cursors.Arrow;
        }
        #endregion

        #region KeyDownイベント
        /// <summary>
        /// プレビュー画面表示後、キー押下”c”後、閉じる処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void PreviewForm_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode != Keys.C)
            {
                return;
            }
            else
            {
                this.Close();
            }
        }
        #endregion
    }
}
