﻿namespace jp.co.fsi.common.forms
{
    partial class BasePgForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnEsc = new System.Windows.Forms.Button();
            this.btnF4 = new System.Windows.Forms.Button();
            this.btnF3 = new System.Windows.Forms.Button();
            this.btnF2 = new System.Windows.Forms.Button();
            this.btnF1 = new System.Windows.Forms.Button();
            this.btnF8 = new System.Windows.Forms.Button();
            this.btnF7 = new System.Windows.Forms.Button();
            this.btnF6 = new System.Windows.Forms.Button();
            this.btnF5 = new System.Windows.Forms.Button();
            this.btnF12 = new System.Windows.Forms.Button();
            this.btnF11 = new System.Windows.Forms.Button();
            this.btnF10 = new System.Windows.Forms.Button();
            this.btnF9 = new System.Windows.Forms.Button();
            this.pnlDebug = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.LightCyan;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle.ForeColor = System.Drawing.Color.Navy;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(1119, 31);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "タイトル";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnEsc
            // 
            this.btnEsc.BackColor = System.Drawing.Color.SkyBlue;
            this.btnEsc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEsc.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEsc.ForeColor = System.Drawing.Color.Navy;
            this.btnEsc.Location = new System.Drawing.Point(4, 4);
            this.btnEsc.Margin = new System.Windows.Forms.Padding(4);
            this.btnEsc.Name = "btnEsc";
            this.btnEsc.Size = new System.Drawing.Size(87, 60);
            this.btnEsc.TabIndex = 0;
            this.btnEsc.TabStop = false;
            this.btnEsc.Text = "Esc\r\n\r\n終了";
            this.btnEsc.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEsc.UseCompatibleTextRendering = true;
            this.btnEsc.UseVisualStyleBackColor = false;
            this.btnEsc.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnEsc.Click += new System.EventHandler(this.btnEsc_Click);
            // 
            // btnF4
            // 
            this.btnF4.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF4.ForeColor = System.Drawing.Color.Navy;
            this.btnF4.Location = new System.Drawing.Point(260, 65);
            this.btnF4.Margin = new System.Windows.Forms.Padding(4);
            this.btnF4.Name = "btnF4";
            this.btnF4.Size = new System.Drawing.Size(87, 60);
            this.btnF4.TabIndex = 4;
            this.btnF4.TabStop = false;
            this.btnF4.Text = "F4\r\n\r\n更新";
            this.btnF4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF4.UseCompatibleTextRendering = true;
            this.btnF4.UseVisualStyleBackColor = false;
            this.btnF4.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF4.Click += new System.EventHandler(this.btnF4_Click);
            // 
            // btnF3
            // 
            this.btnF3.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF3.ForeColor = System.Drawing.Color.Navy;
            this.btnF3.Location = new System.Drawing.Point(175, 65);
            this.btnF3.Margin = new System.Windows.Forms.Padding(4);
            this.btnF3.Name = "btnF3";
            this.btnF3.Size = new System.Drawing.Size(87, 60);
            this.btnF3.TabIndex = 3;
            this.btnF3.TabStop = false;
            this.btnF3.Text = "F3\r\n\r\n登録";
            this.btnF3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF3.UseCompatibleTextRendering = true;
            this.btnF3.UseVisualStyleBackColor = false;
            this.btnF3.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF3.Click += new System.EventHandler(this.btnF3_Click);
            // 
            // btnF2
            // 
            this.btnF2.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF2.ForeColor = System.Drawing.Color.Navy;
            this.btnF2.Location = new System.Drawing.Point(89, 65);
            this.btnF2.Margin = new System.Windows.Forms.Padding(4);
            this.btnF2.Name = "btnF2";
            this.btnF2.Size = new System.Drawing.Size(87, 60);
            this.btnF2.TabIndex = 2;
            this.btnF2.TabStop = false;
            this.btnF2.Text = "F2\r\n\r\n削除";
            this.btnF2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF2.UseCompatibleTextRendering = true;
            this.btnF2.UseVisualStyleBackColor = false;
            this.btnF2.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF2.Click += new System.EventHandler(this.btnF2_Click);
            // 
            // btnF1
            // 
            this.btnF1.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF1.ForeColor = System.Drawing.Color.Navy;
            this.btnF1.Location = new System.Drawing.Point(4, 65);
            this.btnF1.Margin = new System.Windows.Forms.Padding(4);
            this.btnF1.Name = "btnF1";
            this.btnF1.Size = new System.Drawing.Size(87, 60);
            this.btnF1.TabIndex = 1;
            this.btnF1.TabStop = false;
            this.btnF1.Text = "F1\r\n\r\n検索";
            this.btnF1.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF1.UseCompatibleTextRendering = true;
            this.btnF1.UseVisualStyleBackColor = false;
            this.btnF1.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF1.Click += new System.EventHandler(this.btnF1_Click);
            // 
            // btnF8
            // 
            this.btnF8.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF8.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF8.ForeColor = System.Drawing.Color.Navy;
            this.btnF8.Location = new System.Drawing.Point(601, 65);
            this.btnF8.Margin = new System.Windows.Forms.Padding(4);
            this.btnF8.Name = "btnF8";
            this.btnF8.Size = new System.Drawing.Size(87, 60);
            this.btnF8.TabIndex = 8;
            this.btnF8.TabStop = false;
            this.btnF8.Text = "F8";
            this.btnF8.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF8.UseCompatibleTextRendering = true;
            this.btnF8.UseVisualStyleBackColor = false;
            this.btnF8.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF8.Click += new System.EventHandler(this.btnF8_Click);
            // 
            // btnF7
            // 
            this.btnF7.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF7.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF7.ForeColor = System.Drawing.Color.Navy;
            this.btnF7.Location = new System.Drawing.Point(516, 65);
            this.btnF7.Margin = new System.Windows.Forms.Padding(4);
            this.btnF7.Name = "btnF7";
            this.btnF7.Size = new System.Drawing.Size(87, 60);
            this.btnF7.TabIndex = 7;
            this.btnF7.TabStop = false;
            this.btnF7.Text = "F7";
            this.btnF7.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF7.UseCompatibleTextRendering = true;
            this.btnF7.UseVisualStyleBackColor = false;
            this.btnF7.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF7.Click += new System.EventHandler(this.btnF7_Click);
            // 
            // btnF6
            // 
            this.btnF6.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF6.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF6.ForeColor = System.Drawing.Color.Navy;
            this.btnF6.Location = new System.Drawing.Point(431, 65);
            this.btnF6.Margin = new System.Windows.Forms.Padding(4);
            this.btnF6.Name = "btnF6";
            this.btnF6.Size = new System.Drawing.Size(87, 60);
            this.btnF6.TabIndex = 6;
            this.btnF6.TabStop = false;
            this.btnF6.Text = "F6\r\n\r\nプレビュー";
            this.btnF6.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF6.UseCompatibleTextRendering = true;
            this.btnF6.UseVisualStyleBackColor = false;
            this.btnF6.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF6.Click += new System.EventHandler(this.btnF6_Click);
            // 
            // btnF5
            // 
            this.btnF5.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF5.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF5.ForeColor = System.Drawing.Color.Navy;
            this.btnF5.Location = new System.Drawing.Point(345, 65);
            this.btnF5.Margin = new System.Windows.Forms.Padding(4);
            this.btnF5.Name = "btnF5";
            this.btnF5.Size = new System.Drawing.Size(87, 60);
            this.btnF5.TabIndex = 5;
            this.btnF5.TabStop = false;
            this.btnF5.Text = "F5\r\n\r\n印刷";
            this.btnF5.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF5.UseCompatibleTextRendering = true;
            this.btnF5.UseVisualStyleBackColor = false;
            this.btnF5.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF5.Click += new System.EventHandler(this.btnF5_Click);
            // 
            // btnF12
            // 
            this.btnF12.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF12.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF12.ForeColor = System.Drawing.Color.Navy;
            this.btnF12.Location = new System.Drawing.Point(943, 65);
            this.btnF12.Margin = new System.Windows.Forms.Padding(4);
            this.btnF12.Name = "btnF12";
            this.btnF12.Size = new System.Drawing.Size(87, 60);
            this.btnF12.TabIndex = 12;
            this.btnF12.TabStop = false;
            this.btnF12.Text = "F12\r\n\r\nプリンタ";
            this.btnF12.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF12.UseCompatibleTextRendering = true;
            this.btnF12.UseVisualStyleBackColor = false;
            this.btnF12.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF12.Click += new System.EventHandler(this.btnF12_Click);
            // 
            // btnF11
            // 
            this.btnF11.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF11.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF11.ForeColor = System.Drawing.Color.Navy;
            this.btnF11.Location = new System.Drawing.Point(857, 65);
            this.btnF11.Margin = new System.Windows.Forms.Padding(4);
            this.btnF11.Name = "btnF11";
            this.btnF11.Size = new System.Drawing.Size(87, 60);
            this.btnF11.TabIndex = 11;
            this.btnF11.TabStop = false;
            this.btnF11.Text = "F11";
            this.btnF11.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF11.UseCompatibleTextRendering = true;
            this.btnF11.UseVisualStyleBackColor = false;
            this.btnF11.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF11.Click += new System.EventHandler(this.btnF11_Click);
            // 
            // btnF10
            // 
            this.btnF10.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF10.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF10.ForeColor = System.Drawing.Color.Navy;
            this.btnF10.Location = new System.Drawing.Point(772, 65);
            this.btnF10.Margin = new System.Windows.Forms.Padding(4);
            this.btnF10.Name = "btnF10";
            this.btnF10.Size = new System.Drawing.Size(87, 60);
            this.btnF10.TabIndex = 10;
            this.btnF10.TabStop = false;
            this.btnF10.Text = "F10";
            this.btnF10.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF10.UseCompatibleTextRendering = true;
            this.btnF10.UseVisualStyleBackColor = false;
            this.btnF10.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF10.Click += new System.EventHandler(this.btnF10_Click);
            // 
            // btnF9
            // 
            this.btnF9.BackColor = System.Drawing.Color.SkyBlue;
            this.btnF9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnF9.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF9.ForeColor = System.Drawing.Color.Navy;
            this.btnF9.Location = new System.Drawing.Point(687, 65);
            this.btnF9.Margin = new System.Windows.Forms.Padding(4);
            this.btnF9.Name = "btnF9";
            this.btnF9.Size = new System.Drawing.Size(87, 60);
            this.btnF9.TabIndex = 9;
            this.btnF9.TabStop = false;
            this.btnF9.Text = "F9";
            this.btnF9.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF9.UseCompatibleTextRendering = true;
            this.btnF9.UseVisualStyleBackColor = false;
            this.btnF9.EnabledChanged += new System.EventHandler(this.btn_EnabledChanged);
            this.btnF9.Click += new System.EventHandler(this.btnF9_Click);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlDebug.Controls.Add(this.btnEsc);
            this.pnlDebug.Controls.Add(this.btnF12);
            this.pnlDebug.Controls.Add(this.btnF1);
            this.pnlDebug.Controls.Add(this.btnF11);
            this.pnlDebug.Controls.Add(this.btnF2);
            this.pnlDebug.Controls.Add(this.btnF10);
            this.pnlDebug.Controls.Add(this.btnF3);
            this.pnlDebug.Controls.Add(this.btnF9);
            this.pnlDebug.Controls.Add(this.btnF4);
            this.pnlDebug.Controls.Add(this.btnF8);
            this.pnlDebug.Controls.Add(this.btnF5);
            this.pnlDebug.Controls.Add(this.btnF7);
            this.pnlDebug.Controls.Add(this.btnF6);
            this.pnlDebug.Location = new System.Drawing.Point(7, 715);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(4);
            this.pnlDebug.Name = "pnlDebug";
            this.pnlDebug.Size = new System.Drawing.Size(1108, 131);
            this.pnlDebug.TabIndex = 901;
            // 
            // BasePgForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1119, 851);
            this.Controls.Add(this.pnlDebug);
            this.Controls.Add(this.lblTitle);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "BasePgForm";
            this.Text = "BasePgForm";
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Button btnEsc;
        public System.Windows.Forms.Button btnF1;
        public System.Windows.Forms.Button btnF2;
        public System.Windows.Forms.Button btnF3;
        public System.Windows.Forms.Button btnF4;
        public System.Windows.Forms.Button btnF5;
        public System.Windows.Forms.Button btnF7;
        public System.Windows.Forms.Button btnF6;
        public System.Windows.Forms.Button btnF8;
        public System.Windows.Forms.Button btnF9;
        public System.Windows.Forms.Button btnF12;
        public System.Windows.Forms.Button btnF11;
        public System.Windows.Forms.Button btnF10;
        public jp.co.fsi.common.FsiPanel pnlDebug;
		protected System.Windows.Forms.Label lblTitle;
	}
}