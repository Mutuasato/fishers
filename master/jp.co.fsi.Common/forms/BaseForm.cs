﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Windows.Forms;
using System.Drawing.Text;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;
using System.Collections;
using System.Drawing;
using System.Collections.Generic;

namespace jp.co.fsi.common.forms
{
    /// <summary>
    /// 基底フォームクラス
    /// </summary>
    public partial class BaseForm : Form
    {

        #region プロパティ
        private UserInfo _uInfo;
        /// <summary>
        /// ユーザー情報
        /// </summary>
        public UserInfo UInfo
        {
            get
            {
                return this._uInfo;
            }
            set
            {
                this._uInfo = value;
            }
        }

        private DbAccess _dba;
        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        public DbAccess Dba
        {
            get
            {
                return this._dba;
            }
        }

        private ConfigLoader _config;
        /// <summary>
        /// 設定情報オブジェクト
        /// </summary>
        public ConfigLoader Config
        {
            get
            {
                return this._config;
            }
        }

        private MenuInfo _mInfo;
        /// <summary>
        /// メニュー情報オブジェクト
        /// </summary>
        public MenuInfo MInfo
        {
            get
            {
                return this._mInfo;
            }
        }

        private string _unqId;
        /// <summary>
        /// ユニークID
        /// </summary>
        public string UnqId
        {
            get
            {
                return this._unqId;
            }
        }

		private bool blDialogResult = false;
		public bool OrgnDialogResult
		{
			get { return blDialogResult; }
			set { blDialogResult = value; }
		}

		private bool _ipi = true;
        /// <summary>
        /// 親フォームからユーザー情報を引き継ぐか
        /// </summary>
        [Browsable(true)]
        [Description("親フォームからユーザー情報を引き継ぐかどうかを設定します。")]
        public bool InheritParentInfo
        {
            get
            {
                return this._ipi;
            }
            set
            {
                this._ipi = value;
            }
        }

        private BaseForm _menuFrm;
        /// <summary>
        /// メニュー画面のオブジェクト
        /// </summary>
        public BaseForm MenuFrm
        {
            get
            {
                return this._menuFrm;
            }
            set
            {
                this._menuFrm = value;
            }
        }

        private SjDataGridViewEx _sjDataGridViewEx;
        /// <summary>
        /// エンターキーとカーソルキーが使えるデータグリッドビューのオブジェクト
        /// </summary>
        public SjDataGridViewEx sjDataGridViewEx
        {
            get
            {
                return this._sjDataGridViewEx;
            }
            set
            {
                this._sjDataGridViewEx = value;
            }
        }

        private string _activeCtlNm;
        /// <summary>
        /// アクティブなコントロールの名前(対象のフォーム内のみを評価)
        /// </summary>
        public string ActiveCtlNm
        {
            get
            {
                return this._activeCtlNm;
            }
            set
            {
                this._activeCtlNm = value;
            }
        }

        #endregion


        #region ボタンフラグ

        public bool isBtnF1Enable = true;
        public bool isBtnF2Enable = true;
        public bool isBtnF3Enable = true;
        public bool isBtnF4Enable = true;
        public bool isBtnF5Enable = true;
        public bool isBtnF6Enable = true;
        public bool isBtnF7Enable = true;
        public bool isBtnF8Enable = true;
        public bool isBtnF9Enable = true;
        public bool isBtnF10Enable = true;
        public bool isBtnF11Enable = true;
        public bool isBtnF12Enable = true;

        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public BaseForm()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterScreen;
        }
        #endregion

        #region イベント
        /// <summary>
        /// フォームが表示されるときの処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseForm_Load(object sender, EventArgs e)
        {
            // 共通の初期処理
            if (!InitBase())
            {
                // 処理失敗時、フォームを閉じる
                this.Close();
            }

            //画面のコントロールの取得
            Control[] cnl = GetAllControls(this);
            AllCtlCheck(cnl);

            // 個別PGの初期処理
            InitForm();


		}


        /// <summary>
        /// Beep音制御
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyPress(KeyPressEventArgs e)
        {

            if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Escape)
            {

                e.Handled = true;

            }

            base.OnKeyPress(e);
        }

        /// <summary>
        /// フォームが表示されたときの処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseForm_Shown(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// フォームを閉じようとするときの処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        /// <summary>
        /// フォームを閉じた後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if ((this.MenuFrm != null || this.Owner != null) && this._ipi)
            {
				// 親フォームがない、もしくは親フォームから情報を受け取らない場合
				//this._dba.Disconnect();
				this._dba = null;
			}
			else
            {
                // 親フォームがない、もしくは親フォームから情報を受け取らない場合
                if (!this._dba.Disconnect())
                {
                    MessageBox.Show("DB接続の切断に失敗しました。");
                }
            }

            if (this._menuFrm != null)
            {
                Type typeMenu = this._menuFrm.GetType();
                PropertyInfo pi = typeMenu.GetProperty("RunningPg");
                if (pi != null)
                {
                    pi.SetValue(this._menuFrm, null, null);
                }
            }
        }

        /// <summary>
        /// KeyDown時の処理です。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseForm_KeyDown(object sender, KeyEventArgs e)
        {
            // メニューからのKeyDownは受け付けない
            if (this.ActiveControl != null &&
                this.ActiveControl.GetType().BaseType != null &&
                (this.ActiveControl.GetType().BaseType == typeof(BaseForm)
                || this.ActiveControl.GetType().BaseType == typeof(BasePgForm)))
            {
                return;
            }

            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (!e.Control && !(this.ActiveControl is DataGridView)
                        && !(this.ActiveControl is TextBox && ((TextBox)this.ActiveControl).Multiline)
                        && !(this.ActiveControl is FsiTextBox && ((TextBox)this.ActiveControl).Multiline))
                    {
                        if (!(this.ActiveControl is ListBox))
                        {
                            // TODO:Beep音抑止したい
                            this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
                        }
                    }
                    break;

                case Keys.Up:
                    if (!e.Control && !(this.ActiveControl is DataGridView)
                        && !(this.ActiveControl is CheckBox)
                        && !(this.ActiveControl is RadioButton))
                    {

                        this.SelectNextControl(this.ActiveControl, false, true, true, true);
                    }
                    break;

                case Keys.Down:
                    if (!e.Control && !(this.ActiveControl is DataGridView)
                        && !(this.ActiveControl is CheckBox)
                        && !(this.ActiveControl is RadioButton))
                    {
                        this.SelectNextControl(this.ActiveControl, true, true, true, true);
                    }
                    break;

                //case Keys.Left:
                //    if (!e.Control && !(this.ActiveControl is DataGridView))
                //    {
                //        this.SelectNextControl(this.ActiveControl, false, true, true, true);
                //    }
                //    break;

                //case Keys.Right:
                //    if (!e.Control && !(this.ActiveControl is DataGridView))
                //    {
                //        this.SelectNextControl(this.ActiveControl, true, true, true, true);
                //    }
                //    break;

                case Keys.Escape:

                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;

                    PressEsc();
					
                    if (null != this.MenuFrm)
					{
						this.MenuFrm.PressEsc();
					}
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F1:
                    this.Cursor = Cursors.WaitCursor;
					this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF1Enable) PressF1();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F2:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF2Enable) PressF2();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F3:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF3Enable) PressF3();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F4:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF4Enable) PressF4();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F5:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF5Enable) PressF5();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F6:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF6Enable) PressF6();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F7:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF7Enable) PressF7();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F8:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF8Enable) PressF8();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F9:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF9Enable) PressF9();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F10:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF10Enable) PressF10();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F11:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF11Enable) PressF11();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;

                case Keys.F12:
                    this.Cursor = Cursors.WaitCursor;
                    this.AutoValidate = AutoValidate.Disable;
                    if (this.isBtnF12Enable) PressF12();
					this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    this.Cursor = Cursors.Default;
                    break;
            }
        }

        /// <summary>
        /// フォーカス移動時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void control_GotFocus(object sender, EventArgs e)
        {
            if (this.ActiveControl != null)
            {
                this._activeCtlNm = this.ActiveControl.Name;
            }

            OnMoveFocus();

            if (this.GetType().BaseType != null && this.GetType().BaseType.Name.Equals("BasePgForm") && this._menuFrm != null)
            {
                SjButton tmpButton;

                foreach (Control ctrl in this._menuFrm.Controls)
                {
                    switch (ctrl.Name)
                    {
                        case "btnF1":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF1.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF1.Enabled;
                            break;

                        case "btnF2":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF2.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF2.Enabled;
                            break;

                        case "btnF3":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF3.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF3.Enabled;
                            break;

                        case "btnF4":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF4.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF4.Enabled;
                            break;

                        case "btnF5":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF5.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF5.Enabled;
                            break;

                        case "btnF6":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF6.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF6.Enabled;
                            break;

                        case "btnF7":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF7.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF7.Enabled;
                            break;

                        case "btnF8":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF8.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF8.Enabled;
                            break;

                        case "btnF9":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF9.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF9.Enabled;
                            break;

                        case "btnF10":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF10.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF10.Enabled;
                            break;

                        case "btnF11":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF11.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF11.Enabled;
                            break;

                        case "btnF12":
                            tmpButton = (SjButton)ctrl;
                            tmpButton.Text = ((BasePgForm)this).btnF12.Text;
                            tmpButton.Enabled = ((BasePgForm)this).btnF12.Enabled;
                            break;
                    }
                }
            }
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// WndProc処理
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            const int WM_SYSCOMMAND = 0x112;
            const int SC_CLOSE = 0xF060;

			try
			{
				if (m.Msg == WM_SYSCOMMAND && m.WParam.ToInt32() == SC_CLOSE)
				{
					this.AutoValidate = AutoValidate.Disable;
				}
			}
			catch (Exception ex)
			{

				Msg.Error(ex.Message);
			}

            base.WndProc(ref m);
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// Escキーが押された際の処理
        /// </summary>
        public virtual void PressEsc()
        {

            this.AutoValidate = AutoValidate.Disable;

			if (!this.OrgnDialogResult)
			{
				this.Close();
			}
		}

        /// <summary>
        /// F1キーが押された際の処理
        /// </summary>
        public virtual void PressF1()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F2キーが押された際の処理
        /// </summary>
        public virtual void PressF2()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F3キーが押された際の処理
        /// </summary>
        public virtual void PressF3()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F4キーが押された際の処理
        /// </summary>
        public virtual void PressF4()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F5キーが押された際の処理
        /// </summary>
        public virtual void PressF5()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F6キーが押された際の処理
        /// </summary>
        public virtual void PressF6()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F7キーが押された際の処理
        /// </summary>
        public virtual void PressF7()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F8キーが押された際の処理
        /// </summary>
        public virtual void PressF8()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F9キーが押された際の処理
        /// </summary>
        public virtual void PressF9()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F10キーが押された際の処理
        /// </summary>
        public virtual void PressF10()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F11キーが押された際の処理
        /// </summary>
        public virtual void PressF11()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// F12キーが押された際の処理
        /// </summary>
        public virtual void PressF12()
        {
            // 各プログラムで個別に実装
        }
        #endregion

        #region protectedメソッド
        /// <summary>
        /// 初期処理
        /// </summary>
        /// <remarks>必要に応じて各継承クラスにてオーバーライド</remarks>
        protected virtual bool InitBase()
        {
            if (this.MenuFrm != null && this.DesignMode == true)
            {
                // メニューからログイン情報や接続情報を受け取る
                this._uInfo = ((BaseForm)this.MenuFrm).UInfo;
                this._dba = ((BaseForm)this.MenuFrm).Dba;
            }
            else if (this.Owner != null && this._ipi)
            {
                // 親フォームからログイン情報や接続情報を受け取る設定の場合
                this._uInfo = ((BaseForm)this.Owner).UInfo;
                this._dba = ((BaseForm)this.Owner).Dba;
            }
            else
            {
                // 親フォームがない、もしくは親フォームから情報を受け取らない場合
                // DB接続
                if (this.DesignMode == false)
                {
                    this._dba = new DbAccess();
                    if (!this._dba.Connect())
                    {
                        MessageBox.Show("DB接続に失敗しました。");
                        return false;
                    }

                }
                // ユーザー情報
                this._uInfo = new UserInfo(this._dba);

            }

            // 設定情報をインスタンス化
            try
            {
                this._config = new ConfigLoader();
                this._mInfo = new MenuInfo();
            }
            catch (Exception)
            {
                this._config = null;
                this._mInfo = null;
                MessageBox.Show("設定情報の取得に失敗しました。");
                return false;
            }

            // プログラムごとを一意に識別する目的でGUIDを発行する
            Guid myGuid = Guid.NewGuid();
            this._unqId = myGuid.ToString();

            return true;
        }

        /// <summary>
        /// 各コントロールにGotFocusイベントをバインドします。
        /// </summary>
        /// <remarks>各PGのコンストラクタで呼び出してください。</remarks>
        protected void BindGotFocusEvent()
        {

            foreach (Control control in this.Controls)
            {
                NestBind(control);
            }
        }

        /// <summary>
        /// 初期処理(個別)
        /// </summary>
        protected virtual void InitForm()
        {
            // 継承された個別プログラムでオーバーライド
        }

        /// <summary>
        /// フォーカス移動時の処理
        /// </summary>
        protected virtual void OnMoveFocus()
        {
            // 各プログラムで個別に実装
        }

        /// <summary>
        /// 税率を取得します
        /// </summary>
        /// <param name="baseDate">基準日</param>
        /// <returns>税率</returns>
        protected decimal GetZeiritsu(DateTime baseDate)
        {
            // 概要：日付を元に消費税情報テーブルから税率を取得する

            // 適用開始日が基準日より小さいレコードを摘要開始日の降順で取得し、
            // その1件目の新消費税を使う
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this._uInfo.KaishaCd);
            dpc.SetParam("@TEKIYO_KAISHIBI", SqlDbType.DateTime, baseDate);
            DataTable dtZeiInfo = this._dba.GetDataTableByConditionWithParams(
                "*", "TB_ZM_SHOHIZEI_JOHO", "KAISHA_CD = @KAISHA_CD AND TEKIYO_KAISHIBI <= @TEKIYO_KAISHIBI", "TEKIYO_KAISHIBI desc", dpc);

            decimal taxRate = 0;
            if (dtZeiInfo.Rows.Count > 0)
            {
                taxRate = Util.ToDecimal(dtZeiInfo.Rows[0]["SHIN_SHOHIZEI_RITSU"]);
            }

            return taxRate;
        }

        /// <summary>
        /// 過去の税区分が入力された際に、確認メッセージを表示します。
        /// </summary>
        /// <param name="baseDate">基準日</param>
        /// <param name="zeiKbn">税区分</param>
        /// <returns>確認ダイアログの選択結果(確認ダイアログを出さない場合は常にOK、Noが帰ってきたらそもそも入力エラー)</returns>
        protected DialogResult ConfPastZeiKbn(DateTime baseDate, string zeiKbn, ref decimal zeiritsu)
        {
            zeiritsu = 0;

            // 概要：日付を元に、現在の税率と異なる税率を持つ税区分が選択されていれば確認メッセージを出す

            // 税区分を元にTB_ZM_F_ZEI_KUBUNから設定値を取得
            // ※万が一取得できなければNGを返す
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, zeiKbn);
            DataTable dtZeiKbn = this._dba.GetDataTableByConditionWithParams(
                "*", "TB_ZM_F_ZEI_KUBUN", "ZEI_KUBUN = @ZEI_KUBUN", dpc);

            if (dtZeiKbn.Rows.Count == 0) return DialogResult.No;

            // 非課税であればダイアログを出さない
            if (Util.ToInt(dtZeiKbn.Rows[0]["KAZEI_KUBUN"]) == 0) return DialogResult.OK;

            // 適用開始日が基準日より小さいレコードを摘要開始日の降順で取得し、
            // その1件目の新消費税を保持
            // 税区分から取得した税率と消費税情報テーブルから取得した税率を比較し、
            // 異なっていれば確認メッセージを表示
            decimal taxRateBase = GetZeiritsu(baseDate);
            decimal taxRateJudge = Util.ToDecimal(dtZeiKbn.Rows[0]["ZEI_RITSU"]);

            if (taxRateBase != taxRateJudge)
            {
                DialogResult result = Msg.ConfOKCancel("基準日時点の税率と異なる税率を持つ税区分が選択されています。"
                    + Environment.NewLine + "処理を続行してよろしいですか？");

                if (result == DialogResult.OK) zeiritsu = taxRateJudge;

                return result;
            }
            else
            {
                zeiritsu = taxRateJudge;
                return DialogResult.OK;
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// GotFocusイベントのバインドを最下層まで再帰的に呼び出します
        /// </summary>
        /// <param name="control"></param>
        private void NestBind(Control control)
        {
            if (control.HasChildren)
            {
                foreach (Control childControl in control.Controls)
                {
                    NestBind(childControl);
                }
            }
            else
            {
                if (!(control is SjButton))
                {
                    control.GotFocus += new EventHandler(control_GotFocus);

                }
            }
        }

        private Control[] GetAllControls(Control top)
        {
            ArrayList buf = new ArrayList();
            foreach (Control c in top.Controls)
            {
                buf.Add(c);
                buf.AddRange(GetAllControls(c));
            }
            return (Control[])buf.ToArray(typeof(Control));
        }

        private void AllCtlCheck(Control[] ctl)
        {
            //コントロールでラベルの時のみ背景色を緑
            //コントロールがフォームの時は背景色を青で文字色を白
            BaseForm tmpForm;
            foreach (Control ctrl in ctl)
            {
                if (this._menuFrm != null)
                {
                    //if (!(ctrl is TextBox || 
                    //      ctrl.BackColor.Equals(System.Drawing.Color.White) ||  
                    //      ctrl.BackColor.Equals(System.Drawing.Color.Transparent)))
                    //{
                    //    ctrl.BackColor = System.Drawing.Color.PaleGreen;
                    //    //SkyBlue
                    //}

                    if (!(ctrl is TextBox))
                    {
                        if (null != ctrl.Tag)
                        {
                            if (ctrl.Tag.ToString() == "CHANGE")
                            {
                                ctrl.BackColor = System.Drawing.Color.SkyBlue;
                                //SkyBlue
                                ctrl.ForeColor = System.Drawing.Color.Navy;
                            }
							if (ctrl.Tag.ToString() == "DISPNAME")
							{
								ctrl.BackColor = System.Drawing.Color.LightCyan;
								//SkyBlue
								ctrl.ForeColor = System.Drawing.Color.Navy;
								ctrl.Height = 24;
								ctrl.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
								FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
                                ((Label)ctrl).TextAlign = ContentAlignment.MiddleLeft;
                            }
						}
						if (ctrl.Name == "lblTitle")
						{
							ctrl.BackColor = System.Drawing.Color.LightCyan;
							//SkyBlue
							ctrl.ForeColor = System.Drawing.Color.Navy;
							ctrl.Font = new System.Drawing.Font("ＭＳ 明朝", 14.25F,
							FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));

						}
						//ctrl.Size = new System.Drawing.Size(00,24);
					}

                }

                if (ctrl is Panel || ctrl is GroupBox || ctrl is TableLayoutPanel)
                {

                    if (ctrl is TableLayoutPanel)
                    {

                        if (ctrl.Tag != null)
                        {

                            if (ctrl.Tag.ToString() == "CHANGE")
                            {
                                ctrl.BackColor = System.Drawing.Color.SkyBlue;
                                //SkyBlue
                                ctrl.ForeColor = System.Drawing.Color.Navy;
                            }
							if (ctrl.Tag.ToString() == "DISPNAME")
							{
								ctrl.BackColor = System.Drawing.Color.LightCyan;
								//SkyBlue
								ctrl.ForeColor = System.Drawing.Color.Navy;
								ctrl.Height = 24;
								ctrl.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
								FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
                                ((Label)ctrl).TextAlign = ContentAlignment.MiddleLeft;

                            }

                        }

					}
                    ctrl.SuspendLayout();
                    Control[] subs = GetAllControls(ctrl);
                    AllCtlCheck(subs);
                    ctrl.ResumeLayout(false);
                }
                else if (ctrl is Label)
                {

                    if (null != ctrl.Tag)
                    {
                        if (ctrl.Tag.ToString() == "CHANGE")
                        {
                            ctrl.BackColor = System.Drawing.Color.SkyBlue;
                            //SkyBlue
                            ctrl.ForeColor = System.Drawing.Color.Navy;
                        }
						if (ctrl.Tag.ToString() == "DISPNAME")
						{
							ctrl.BackColor = System.Drawing.Color.LightCyan;
							//SkyBlue
							ctrl.ForeColor = System.Drawing.Color.Navy;
							ctrl.Height = 24;
							ctrl.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
							FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));

                            ((Label)ctrl).TextAlign = ContentAlignment.MiddleLeft;
						}
						//ctrl.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
						//FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
						//ctrl.Size = new System.Drawing.Size(00, 24);
					}
					//ctrl.BackColor = System.Drawing.Color.PaleGreen;
				}
                else if (ctrl is TextBox)
                {
                    ctrl.Font = new System.Drawing.Font("ＭＳ 明朝", 12F,
                    FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
                }
                else
                {

                    if (null != ctrl.Tag)
                    {
                        if (ctrl.Tag.ToString() == "CHANGE")
                        {
                            ctrl.BackColor = System.Drawing.Color.SkyBlue;
                            //SkyBlue
                            ctrl.ForeColor = System.Drawing.Color.Navy;
                        }
						if (ctrl.Tag.ToString() == "DISPNAME")
						{
							ctrl.BackColor = System.Drawing.Color.LightCyan;
							//SkyBlue
							ctrl.ForeColor = System.Drawing.Color.Navy;
							ctrl.Height = 24;

                            if (ctrl is Label)
                            {
                                ((Label)ctrl).TextAlign = ContentAlignment.MiddleLeft;
                            }

                        }
					}
				}
            }
            if (this.GetType().BaseType != null && GetType().BaseType.Name.Equals("BaseForm") && this._menuFrm != null)
            {
                tmpForm = (BaseForm)_menuFrm;
                tmpForm.BackColor = System.Drawing.Color.LightSteelBlue;
                //tmpForm.ForeColor = System.Drawing.Color.White;
            }

        }
        #endregion
    }
}
